##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

######################################################
#
# Makefile for DOS utility to draw connections on Wargamer maps
#
######################################################

name = RoadMap

CDIR=roadmap\c;c
HDIR=roadmap\h;h;system;gamesup;campdata
LIBS += system.lib
ROOT=.

!include wgpaths.mif

!ifdef NODEBUG
ODIR=roadmap\o\final
!else
ODIR=roadmap\o\debug
!endif


LNKEXT=lk

LNK = $(name).$(LNKEXT)
lnk_dependencies = roadmap.mk dosnt.mif

CFLAGS = -fi=globals.hpp


OBJS = 	bmp.obj      &
	campdata.obj &
	except.obj   &
	file.obj     &
	mapdraw.obj  &
	mapwrite.obj &
	myassert.obj &
	roadmap.obj  &
	town.obj     &
	wldfile.obj  &
	measure.obj  &
	clog.obj
	
#	winerror.obj &
#	sllist.obj &
#	misc.obj     &
#	filedata.obj &
#	filebase.obj &
#	array.obj    &
#	trig.obj     &
#	random.obj   &
#	todolog.obj  &
#	gameob.obj   &
#	palette.obj  &
#	dib.obj      &
#	icompos.obj

!include dosnt.mif

linkit : .PROCEDURE
	@%append $(LNK) option stack=8k
	@%append $(LNK) debug all
	@%append $(LNK) LIBPATH $(LIBDIR)
	@for %i in ($(LIBS)) do @%append $(LNK) lib %i



