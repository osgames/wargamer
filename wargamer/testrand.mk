##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

######################################################
#
# Makefile for DOS utility to test Random Number Generator
#
######################################################

name = TestRand

CDIR=c
HDIR=h
ODIR=o\debug

LNKEXT=lk

LNK = $(name).$(LNKEXT)
lnk_dependencies = testrand.mk dos32.mif

CFLAGS =
OBJS = testrand.obj random.obj

!include dosnt.mif

linkit : .PROCEDURE
    @%append $(LNK) option stack=8k
    


