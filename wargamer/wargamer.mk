##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#####################################################################
# $Id$
#####################################################################
# $Author$
#####################################################################
#
# Makefile for Wargamer Project
#
# Most of the program is split into libraries
# Each library has it's own .MIF file, making it easier for the
# makefile to be updated without locking it all out
#
#####################################################################

fname = wargam
CDIR=src\c
HDIR=src\h
RDIR=res
rcname=wargamer.rc
RC=1
EXT=exe
ROOT=.

!ifdef NODEBUG
NAME = Wargamer
!else
!ifdef NOLOG
NAME = $(fname)NL
!else
NAME = $(fname)DB
!endif
!endif



!include $(ROOT)\config\wgpaths.mif

CFLAGS += -i=$(RDIR)

lnk_dependencies += wargamer.mk

resources =
# us_str.r7c

#	clogic.mif	&
#	exe95.mif	&
#	win95.mif 	&

################


OBJS = wargamer.obj
# OBJS += tactical.obj

TARGETS += makedll

SYSLIBS = COMCTL32.LIB VFW32.LIB DSOUND.LIB DXGUID.LIB


all :: $(TARGETS) .SYMBOLIC


###############################################################
# Libraries

!include src\system\system.mif
!include src\gamesup\gamesup.mif
!include src\ob\ob.mif
!include src\gwind\gwind.mif
!include src\batdata\batdata.mif
!include src\batlogic\batlogic.mif
!include src\batai\batai.mif
!include src\batdisp\batdisp.mif
!include src\batwind\batwind.mif
!include src\batgame\batgame.mif
!include src\campdata\campdata.mif
!include src\campdata\clogic.mif
!include src\camp_ai\camp_ai.mif
!include src\mapwind\mapwind.mif
!ifdef CUSTOMIZE
!include src\campedit\campedit.mif
!endif
!include src\campwind\campwind.mif
!include src\campgame\campgame.mif
!include src\frontend\frontend.mif

###############################################################
# Operating System stuff

!include $(ROOT)\config\exe95.mif

###############################################################
# Linking

linkit : .PROCEDURE
	 @%append $(LNK) option stack=64k
!ifdef CODEVIEW
	 @%append $(LNK) debug codeview
	 @%append $(LNK) option cvpack
!else
	 @%append $(LNK) debug DWARF all
!endif
!ifdef NODEBUG
!else
#	 @%append $(LNK) option INCREMENTAL
!endif
	 @%append $(LNK) option ELIMINATE
	 @%append $(LNK) option SYMFILE
#	 @%append $(LNK) OPTION VFREMOVAL

###############################################################
# Instructions for making DLLs

makedll : .SYMBOLIC
	@!wmake $(__MAKEOPTS__) $(MFLAGS) -f nap1813.mk

#	@!wmake $(__MAKEOPTS__) $(MFLAGS) -f acw.mk


ObsoleteFiles = obsolete.dat

.before:
	@if exist $(ObsoleteFiles) echo Deleting Obsolete Files listed in $(ObsoleteFiles)
    @if exist $(ObsoleteFiles) dellist $(ObsoleteFiles) /zy

#####################################################################
# $Log$
#####################################################################
