##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

######################################################
#
# Makefile for DOS utility to test Campaign Time
#
######################################################

name = TestTime

CDIR=c
HDIR=h
ODIR=o\debug

LNKEXT=lk

LNK = $(name).$(LNKEXT)
lnk_dependencies = testtime.mk dos32.mif

CFLAGS =
OBJS = testtime.obj camptime.obj misc.obj date.obj

!include dos32.mif

linkit : .PROCEDURE
    @%append $(LNK) system dos4g
    @%append $(LNK) option stack=8k
    

