##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

##############################################################################
# $Id$
##############################################################################
# $Author$
##############################################################################

##########################################
# Makefile for nap1813res.dll
#
# Contains resources for nap1813




name = nap1813res
ROOT=.
DLL_DIR=$(ROOT)\exe\nap1813
RDIR=$(ROOT)\res
CFLAGS += -i=$(RDIR)
CDIR=$(ROOT)\res

!include $(ROOT)\config\wgpaths.mif

# EXT=DLL
rcname=nap1813.rc
RC=1
OBJS = scn_dll.obj

# lnk_dependencies += nap1813.mk exe95.mif win95.mif
lnk_dependencies += $(ROOT)\nap1813.mk
# CFLAGS += -DDLL

# !include exe95.mif
# !include win95.mif

all :: $(TARGETS) .SYMBOLIC
	@%null

!include $(ROOT)\config\dll95.mif

# linkit : .PROCEDURE
#	 @%append $(LNK) option stack=64k
#	 @%append $(LNK) debug all
#
# copyit: .PROCEDURE
#	# Do nothing

##############################################################################
# $Log$
##############################################################################
