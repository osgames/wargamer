[DEFAULT]
{
MAP "frontend\Autumn1813.bmp"
TEXT "AUTUMN 1813 (Roads to Leipzig)"
DESCRIPTION "September 1813 to late Autumn 1813.
In the six weeks since the Armistice ended neither side has managed to deal a knockout blow to their opponent. The French thrust on Breslau met with disaster at the Katzbach where Blucher defeated Macdonald. The offensive on Berlin was first checked at Gross Beeren and then comprehensively defeated at Dennewitz. Davout (still based at Hamburg) was unable to effectively support the French operations against Berlin.
However, the main Allied offensive against Dresden (Napoleon's main base in Germany) was also defeated in a two day battle just outside the city. Indeed, the Army of Bohemia was perhaps fortunate to escape from complete disaster. Napoleon was unable to follow up this success due to the defeats inflicted on his subordinates at Gross Beeren and the Katzbach. The attempted pursuit of the Allied army by Vandamme was crushed at Kulm.
With the failure of their offensives the French are gradually being penned back into Saxony. Indeed, with the exception of Davout (F2) the bulk of the French forces (F1) are now centred around Dresden and Leipzig. This is starting to place an intolerable burden on the French supply system. French reinforcements (including another much needed cavalry corps) are heading for the front (F4) but may not arrive in time, especially as Napoleon's German Allies are wavering in their loyalty.
Meanwhile the French garrisons at Stettin. Glogau and Kustrin continue to hold out, but their resistance is weakening.
The Allies are still largely operating in three main armies - The Army of Bohemia (A1), which has recovered from its defeat at Dresden is once more ready to attack. The Army of Silesia (A2) is advancing into eastern Saxony. The Army of the North (A3 & A5) which having successfully defended Berlin is menacing northern Saxony. Wallmoden (A4) continues to keep Davout in check around Hamburg. A further Austrian force (the Army of the Donau, A8) is poised to invade Bavaria. Approaching the front is a fresh Russian army (A6) and Cossack raiders (A7) are preparing to menace both Napoleon's Allies and French supply lines.
The French position is now critical. With wavering allies and a perilous supply situation Napoleon cannot afford another defeat. The French however still hold the central position and the opportunity to defeat one or more of the Allied armies before they can unite remains. The Allies have victory within their grasp, but if they overreach themselves or make a false move they are vulnerable to a sudden counterattack which could rapidly reverse the situation.
The stage is set for the largest battle of the Napoleonic Wars - the three day 'Battle of the Nations' at Leipzig.
Key areas and locations are
Dresden and Leipzig. The 'keys' to Saxony and the main French supply centres.
Magdeberg, another French supply centre and the key fortress of central Germany.
Berlin, Breslau and Prague, the main Allied supply centres.
Hamburg, a major city, port and supply source
Bavaria, a major German kingdom whose support of the French is weakening daily."
FRENCH_KEY "F1 Grande Armee
F2 Davout and Danes
F3 Girard
F4 French Reinforcements
"
ALLIED_KEY "A1 Army of Bohemia & Russian Main Army
A2 Army of Silesia
A3 Army of the North
A4 Wallmoden
A5 Bulow and Taunentzien
A6 Russian Reserve Army
A7 Cossack Raiders
A8 Austrian Army of the Donau
"
}

