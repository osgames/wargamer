[DEFAULT]
{
MAP "frontend\August1813.bmp"
TEXT "AUGUST 1813 SHORT SCENARIO"
DESCRIPTION "August 10th 1813 to Autumn 1813.
Both sides have used the summer Armistice to rebuild their forces, raise fresh units and reorganise their armies. The Swedish army has mobilised and Austria has now joined the Allied cause. Denmark has sided with the French and their army is supporting Davout (F5) who has retaken Hamburg. So far the German kingdoms have remained loyal to Napoleon.
Both sides have now established regular supply lines and the main theatre of operations would appear to be Saxony, Silesia and the line of the Elbe.
The Allies have organised their forces in four main groups. The largest of these is the Army of Bohemia (A1) which is based in northern Bohemia. This force consists of the Austrian army supported by a substantial number of Russian formations (including the Guard - A2) and some Prussians. Holding the centre is the Army of Silesia commanded by Blucher (A3) which consists of Prussian and Russian formations. Protecting Berlin is the Army of the North (A5) which is still in the process of forming. This force will eventually consist of Prussians, Russians and Swedes. Finally, protecting Mecklenberg from Davout there is the force of Wallmoden (A6).
The French have largely adopted the strategy of the 'central position' with the bulk of the 'Grande Armee' based around Dresden and Leipzig. These two cities are the main French supply sources (supported by Magdeberg). The French army has been reorganised during the Armistice and several new corps have arrived (or been formed) including the Polish 8th Corps under Ponitatowski and the 3rd and 4th Cavalry Corps. The fortresses of Stettin, Glogau and Kustrin continue to hold out against Allied siege operations.
The French must aim to comprehenisvely defeat one (or more) of the main Allied armies ranged against them and capture any or all of the following - Berlin, Breslau and Prague. Losing Dresden, Leipzig, Hamburg or Magdeberg would be a major problem. The Allies must hold the vital cities of Berlin and Breslau and bring their superiority of numbers to bear against the revitalised French army. At least one army will probably have to operate defensively whilst the others aim at striking at the major French centres - Dresden, Leipzig, Magdeberg and Hamburg.

Key areas and locations are
Saxony, one of Napoleons most important allies and the main base of the French army in Germany. Berlin, Prussian capital and a major supply source.
Breslau, a major Allied base area.
Hamburg, a major city, port and supply source
Prague, the supply source for Austrian forces operating in central Germany."
FRENCH_KEY "F1 Main French Army
F2 French Army of Berlin
F3 French Reserves
F4 Girard
F5 Davout and Danish Corps
F6 French Reinforcements
"
ALLIED_KEY "A1 Allied Army of Bohemia
A2 Main Russian Army
A3 Army of Silesia
A4 Prussian Army of Berlin
A5 Army of the North
A6 Wallmoden
A7 Austrian Reserve
A8 Russian Reinforcements
"
}

[LANG_FRENCH]
{
MAP "frontend\August1813.bmp"
TEXT "AOUT 1813 SCENARIO COURT"
DESCRIPTION "du 10 Ao�t 1813 � l'automne 1813.
Les deux c�t�s ont utilis� l'Armistice de l'�t� pour reconstruire leurs forces, lever de nouvelles unit�s fra�ches et r�organiser leurs arm�es. L'arm�e su�doise a �t� mobilis�e et l'Autriche a maintenant rejoint la cause Alli�e. Le Danemark s'est mis du c�t� de les Fran�ais et leur arm�e soutient Davout (F5) qui a repris Hambourg. Les royaumes Allemands sont quant � eux rest�s fid�les � Napol�on.
Les deux camps ont maintenant �tabli des lignes de ravitaillement r�guli�res et le th��tre principal d'op�rations para�traient �tre la Saxe, la Sil�sie et la ligne de l'Elbe.
Les Alli�s ont organis� leurs forces en quatre groupes principaux. La plus grande de ceux-ci est l'Arm�e de Boh�me (A1) laquelle est bas�e en Boh�me du nord. Cette force consiste en l'arm�e Autrichienne support�e par un nombre substantiel de formations Russes (y compris la Garde - A2) et quelques Prussiens. le centre du dispositif Alli� est tenu par l'Arm�e de Sil�sie command�e par Bl�cher (A3) cette arm�e est essentiellement constitu�e de Prussiens et de formations Russes. Pour prot�ger Berlin l'Arm�e du Nord (A5) est encore en cours de formation. Cette force sera finalement constitu�e de Prussiens, Russes et Su�dois. Enfin, prot�geant Mecklenberg de Davout il y a les troupes de Wallmoden (A6).
Les Fran�ais ont adopt� pour une grande part la strat�gie dite de la 'position centrale' avec la masse de la Grande Arm�e bas�e autour de Dresde et Leipzig. Ces deux villes sont les sources d'approvisionnement fran�aises principales (support�e par Magdeberg). L'arm�e Fran�aise a �t� r�organis�e pendant l'Armistice et plusieurs nouveaux corps sont arriv�s (ou ont �t� form�s) y compris le 8e Corps Polonais sous les ordres de Ponitatowski et les 3e et 4e Corps de Cavalerie. Les forteresses de Stettin, Glogau et Kustrin continuent � tenir face � leur assi�geants.
Le Fran�ais ont l'objectif de battre compl�tement une (ou plusieurs) des principales arm�es Alli�es align�es contre eux et s'emparer de une ou plusieurs des villes suivantes - Berlin, Breslau et Prague. Perdre Dresde, Leipzig, Hambourg ou Magdeberg serait un probl�me majeur. Les Alli�s doivent tenir les villes primordiales de Berlin et Breslau, et profiter de leur sup�riorit� num�rique pour battre l'arm�e Fran�aise revigor�e. Au moins une arm�e devra probablement op�rer d�fensivement pendant que les autres auront pour objectif l'attaque des principaux centres d'approvisionnement Fran�ais  - Dresde, Leipzig, Magdeberg et Hambourg.

Les secteurs et les endroits principaux sont :
La Saxe, un des plus importants alli�s de Napol�on et la base d'op�ration principale de l'arm�e Fran�aise en Allemagne. Berlin, capitale de la Prusse et une source de ravitaillement importante.
Breslau, une base Alli�e importante.
Hambourg, une ville et un port importants, ainsi qu'une importante source de ravitaillement
Prague, la source d'apporvisionnement des forces Autrichiennes op�rant au centre de l'Allemagne."
FRENCH_KEY "F1 L'Arm�e Fran�aise principale
F2 L'Arm�e Fran�aise de Berlin
F3 Les R�serves Fran�aises
F4 Girard
F5 Davout et le Corps Danois
F6 Les renforts Fran�ais
"
ALLIED_KEY "A1 L'Arm�e de Boh�me
A2 L'Arm�e principale Russe
A3 L'Arm�e de Sil�sie
A4 L'Arm�e Prussienne de Berlin
A5 L'Arm�e du Nord
A6 Wallmoden
A7 La R�serve Autrichienne
A8 Les renforts Russes
"
}