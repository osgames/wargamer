[French]
{
MAP "frontend//cmap_1024.bmp"
TEXT "La Premi�re Campagne Napol�on 1813"
DESCRIPTION "La premi�re qualit� du soldat est la constance � supporter la fatigue, la valeur n'est que la seconde ; La pauvret�, la privation et le manque font l'�cole du bon soldat.
Valor belongs to the young soldier as well as to the veteran; but in the former it is more evanescent.  It is only by habits of service, and after several campaigns, that the soldier acquires that moral courage which makes him support the fatigues and privations of war without a murmur.
Experience by this time has instructed him to supply his own wants."
FRENCH_KEY "Fran�ais"
ALLIED_KEY "Alli�s"
}
