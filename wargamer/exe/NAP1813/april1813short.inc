[DEFAULT]
{
MAP "frontend\GrandCampApril1813.bmp"
TEXT "APRIL 1813 SPRING CAMPAIGN"
DESCRIPTION "March 31st 1813 to June 1813
The Russians, now joined by Prussia have followed the remnants of the French army that survived the retreat from Moscow into Germany. These forces under Prince Eugene have been unable to check the Allied advance and have fallen back to the vital city of Magdeberg (F2).
The Allied armies are in two main groups, Wittgenstein (A1) around Berlin and Blucher and Winzingerode (A2) at Dresden. Cossacks have liberated the important port of Hamburg (A7) and further Allied units are moving up to the front (A3, A5 and A6). The Allied advance has not been checked by French held fortresses (Glogau, Stettin, Kustrin and Spandau) as these have largely been bypassed by the main formations, leaving various 2nd line units to put them under siege. With Berlin, Dresden and Hamburg under Allied control and with many of the minor German states wavering in their loyalty towards Napoleon there is a chance that the French can be driven back behind the Rhine before summer.
However, Napoleon has managed to raise a fresh army and is now preparing to take the field once more (F1). Meanwhile, in the north Davout (F2) has scraped together a second new army and now threatens Hamburg.
The situation is still fluid. The Allies have the opportunity of pushing on into western Germany and possibly defeating the French before they are fully mobilised. This may convince some of the German states (especially Saxony) to defect. Failing that they must fight a defensive campaign to keep the resurgent French at bay until further reinforcements arrive and Austria can be persuaded to throw her forces into the fray on the Allied side.
The French must rapidly re-establish their control over the German states and seek the decisive battlefield victories that will allow Napoleon to declare 'I am once more Master of Europe'.

Key areas and locations are
Saxony which is rich in supplies and is one of the most important German kingdoms.
Berlin, Prussian capital and a major supply source.
Breslau, a major Allied base area.
Hamburg, a major city and port, through which Prussia receives equipment from Britain. "
FRENCH_KEY "F1 Armee de Mein
F2 Armee de l'Elbe
F3 32nd Army
F4 French reinforcements
F5 Vandamme
"
ALLIED_KEY "A1 Wittgenstein
A2 Czar and Allied Army
A3 Russian reinforcements
A4 Swedish Army
A5 Russian reserves
A6 Prussian reinforcements
A7 Wallmoden
"
}

