[DEFAULT]
{
MAP "frontend\GrandCampApril1813.bmp"
TEXT "APRIL 1813 GRAND CAMPAIGN"
DESCRIPTION "March 31st 1813 to Autumn 1814.
The French Army has still not recovered from it's disastrous invasion of Russia. Napoleon has returned to France to raise and organise fresh forces, leaving the survivors from 1812 in Germany under the command of Prince Eugene (Armee de l'Elbe).
Under pressure from the advancing Russians and Prussians Eugene (F2) has been forced to abandon Berlin and fall back behind the Elbe, basing his defence on the vital fortress of Magdeberg.
The Allies are advancing on two main groups, Wittgenstein (A1) around Berlin and Blucher and Winzingerode (A2) at Dresden. In the north fast moving Cossacks (A7) supported by popular uprisings have liberated the vital port of Hamburg from the French. Meanwhile the main Russian Army is finally preparing to leave Kalisch and enter Prussia. The main Allied forces have bypassed the eastern fortresses held by the French (Glogau, Stettin, Kustrin and Spandau), leaving various 2nd line units to put them under siege. The Swedish army (A4) is still mobilising and is not yet ready to take the field.
The various German states have generally remained loyal to Napoleon, but some no doubt are considering changing sides to preserve their kingdoms, especially if the Allies continue to advance unchecked. Saxony in particular (one of the most important German kingdoms) seems to be wavering and might desert at any moment. So far Austria and Denmark have remained neutral.
As the campaign starts Napoleon is preparing to re-enter Germany with the recently formed Armee de Mein (F1) whilst Davout is forming another army in the north (F3).
The situation is still very fluid, the forces on both sides are still not fully organised and supply lines have not been fully established.
The Allies must aim to hold the resurgent French at bay, if necessary trading space for time until their forces are fully mobilised. Then they can sweep through Germany and onto the ultimate objective - Paris!
The French must re-establish their hegemony over Germany, driving the Allies back and crushing Prussia once again. If they cannot then they face the prospect of fighting a desperate defensive campaign to keep the Allies from the gates of Paris, hoping that the fragile nature of the enemy coalition will eventually enable them to negotiate a 'peace with honour'.

Key areas and locations are
Saxony which is rich in supplies and is one of the most important German kingdoms.
Berlin, Prussian capital and a major supply source.
Breslau, a major Allied base area.
Hamburg, a major city and port, through which Prussia receives equipment from Britain. Denmark will not support France whilst the Allies hold this city.
Bohemia (if Austria enters the war).
The road from Mainz to Dresden that is the main French supply axis."
FRENCH_KEY "F1 Armee de Mein
F2 Armee de l'Elbe
F3 32nd Army
F4 French reinforcements
F5 Vandamme
"
ALLIED_KEY "A1 Wittgenstein
A2 Czar and Allied Army
A3 Russian reinforcements
A4 Swedish Army
A5 Russian reserves
A6 Prussian reinforcements
A7 Wallmoden
"
}

