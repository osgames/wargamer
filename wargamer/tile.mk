##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

######################################################
# $Id$
######################################################
#
# Makefile for DOS utility to expand the size of a BMP
# image.
#
######################################################
#
# $Log$
# Revision 1.1  1995/10/25 09:53:14  Steven_Green
# Initial revision
#
# Revision 1.1  1995/10/20 11:08:18  Steven_Green
# Initial revision
#
#
######################################################

name = Tile

CDIR=tile
HDIR=tile;h
ODIR=tile

LNKEXT=lk

LNK = $(name).$(LNKEXT)
lnk_dependencies = tile.mk dos32.mif

CFLAGS = -fh=tile.pch
OBJS = tile.obj

!include dos32.mif

linkit : .PROCEDURE
    @%append $(LNK) system dos4g
    @%append $(LNK) option stack=8k
    
