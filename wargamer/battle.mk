##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#################################################################
# Makefile for Wargamer Battle Player
#################################################################

fname = Battle
CDIR=src\c
HDIR=src\h
RDIR=res
rcname=battle.rc
RC=1
EXT=EXE
GNAME=battle
ROOT=.
# OBJ_PATH=$(ROOT)\o\$(SubType)
ONLYBATTLE=

!ifdef EDITOR
NAME = $(fname)ed
!else
!ifdef NODEBUG
NAME = $(fname)
!else
!ifdef NOLOG
NAME = $(fname)NL
!else
NAME = $(fname)DB
!endif
!endif
!endif




!include $(ROOT)\config\wgpaths.mif

lnk_dependencies += $(ROOT)\battle.mk

resources = btl_us_s.rc




################

OBJS = testbat.obj

###############################################################
# Libraries

TARGETS += makedll

SYSLIBS = COMCTL32.LIB VFW32.LIB DSOUND.LIB DXGUID.LIB

# all :: $(TARGETS) nap1813\batres.dll .SYMBOLIC
all :: $(TARGETS) .SYMBOLIC


testbat.obj : wargamer.cpp .AUTODEPEND
        $(CPP) $(CPPFLAGS) -DTEST_BATTLE $[* -fo$(ODIR)\$^.





# !include frontend.mif
!include src\system\system.mif
!include src\gamesup\gamesup.mif
!include src\ob\ob.mif
!include src\gwind\gwind.mif
!include src\batdata\batdata.mif
!include src\batlogic\batlogic.mif
!include src\batai\batai.mif
!include src\batdisp\batdisp.mif
!include src\batwind\batwind.mif
!include src\batgame\batgame.mif

CFLAGS += -i=h;res

!include $(ROOT)\config\exe95.mif

###############################################################
# Compile instructions


###############################################################
# Linking

linkit : .PROCEDURE
         @%append $(LNK) option stack=64k
!ifdef CODEVIEW
         @%append $(LNK) debug codeview all
         @%append $(LNK) option cvpack
!else
         @%append $(LNK) debug DWARF all
!endif
!ifdef NODEBUG
#        @%append $(LNK) OPTION VFREMOVAL
         @%append $(LNK) option ELIMINATE
!else
#        @%append $(LNK) option INCREMENTAL
!endif
         @%append $(LNK) option SYMFILE
         @%append $(LNK) LIB COMCTL32.LIB

makedll: .SYMBOLIC
	@!wmake $(__MAKEOPTS__) $(MFLAGS) -f nap1813.mk

#         @!wmake $(__MAKEOPTS__) $(MFLAGS) -f batres.mk

#####################################################################
# $Log$
#####################################################################
