setlocal
set batpath=%@PATH[%@TRUENAME[%_BATCHNAME]]
set dd=%_date
set dy=%@substr[%dd,0,2]%@substr[%dd,3,2]%@substr[%dd,6,2]
set BAKFILE=%BACKUP\wg_src_%@SUBSTR[%dd,0,2]%@SUBSTR[%dd,3,2].zip
rem set ZIPFLAGS=-add -rec -path -excl=@%batpath%zipext.lst -excl=@%batpath%nodata.lst
rem pkzip25 %ZIPFLAGS %BAKFILE *.*
rem set ZIPFLAGS=-add -rec -path -excl=@%batpath%zipext.lst
rem pkzip25 %ZIPFLAGS %BAKFILE res\*.*
set ZIPFLAGS=-aPr -x@%batpath%zipext.lst -x@%batpath%nodata.lst
wzzip %ZIPFLAGS %BAKFILE *.*
set ZIPFLAGS=-aPr -x@%batpath%zipext.lst
wzzip %ZIPFLAGS %BAKFILE res\*.*

endlocal

