rem Update graphic and data files
setlocal

set masterPath=\\Napoleon\vcs\datafiles
rem set masterPath=\\tigger\vcs\datafiles
rem set masterPath=v:\datafiles

pushd %masterpath
call dellist %masterPath\oldfiles.lst
popd

pushd %HOME
call dellist %masterPath\oldfiles.lst

unset /q NewVersion
IF "%_CMDPROC"=="4DOS" .AND. %_4VER ge 6.01 set NewVersion=1
IF "%_CMDPROC"=="4NT" .AND. %_4VER ge 3.01 set NewVersion=1

IFF DEFINED NewVersion THEN
  set COPYFLAGS=/e /u /s /z /h
ELSE
  set COPYFLAGS=/u /s /h
ENDIFF

rem Copy from Master to Local drive

*copy %COPYFLAGS /us %masterPath .

popd

endlocal