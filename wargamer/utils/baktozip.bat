if not defined ZIPDRIVE set ZIPDRIVE=g:


set batpath=%@PATH[%@TRUENAME[%_BATCHNAME]]
set datfile=%batpath%%@NAME[%_BATCHNAME].day

set dd=%_date
set dy=%@substr[%dd,0,2]%@substr[%dd,3,2]%@substr[%dd,6,2]
set BAKFILE=%ZIPDRIVE\wg_%@SUBSTR[%dd,0,2]%@SUBSTR[%dd,3,2]i.zip

set ZIPFLAGS=-add -rec -path -excl=@%batpath%zipext.lst

iff exist %datfile then
  set lastdate=%@line[%datfile,0]
  rem pkzip25 -t%lastdate %ZIPFLAGS %BAKFILE *.* 
  pkzip25 -after=%lastdate %ZIPFLAGS %BAKFILE *.* 
 else
  pkzip25 %ZIPFLAGS %BAKFILE *.*
endiff

echo %dy >! %datfile

