rem Delete any files from a list of files
rem
rem Usage: delList filelist

setlocal

iff "%1"=="" then
 echo Usage: delList filelist [/delOptions]
 quit -1
endiff


iff exist %1 then
  set FH=%@FILEOPEN[%1,read,t]
  if %FH=="-1" goto error

  set line=""
  DO UNTIL "%line"=="**EOF**"
    set line=%@FILEREAD[%FH]
    IFF NOT "%line"=="**EOF**" THEN
      IFF isdir %line THEN
        del %2 /sx %line
      ELSEIFF exist %line THEN
        del %2 %line
      ENDIFF
    ENDIFF
  ENDDO

  set err=%@FILECLOSE[%FH]
  if %err=="-1" goto error
else
  echo filelist %1 does not exist
endiff

quit 0


:error
echo error reading %1
quit -1


