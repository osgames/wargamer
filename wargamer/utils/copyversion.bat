rem -----------------------------------------------------
rem Copy from CD Install folder to version Folder
rem -----------------------------------------------------

setlocal

rem set pathnames

rem set INSTALL_DIR=f:\wginstall\game
rem set VERSION_DIR=\\LOVEBUG\WG_VERSIONS\wargamer

IFF defined WG_INSTALL THEN
    set INSTALL_DIR=%WG_INSTALL%\game
ELSE
    echo "You must set WG_INSTALL to the installation directory"
    quit -1
ENDIFF

IFF defined WG_VERSIONS THEN
    set VERSION_DIR=%WG_VERSIONS%
ELSE
    echo "You must set WG_VERSIONS to the directory where old versions are stored"
    quit -1
ENDIFF


rem Make a foldername based on the date ddmmmyy

set MonthNames="JanFebMarAprMayJunJulAugSepOctNovDec"
set MonthOffset=%@EVAL[1+(%_month-1)*3]
set MonthName=%@INSTR[%MonthOffset,3,%MonthNames]

set DEST_DIR=%VERSION_DIR\%_day%%MonthName%%_year

md %DEST_DIR
copy /s %INSTALL_DIR% %DEST_DIR%
copy %INSTALL_DIR%\..\readme.txt %DEST_DIR%


endlocal

