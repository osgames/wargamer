@echo off
rem Get Ben's latest Battles
setlocal
set SRCDIR=\\tigger\c\wargamer\nap1813\Histor~1
set DESTDIR=%HOME%\game\nap1813
unset /q DEBUG

set SRCNAME=BautzenDay1
set DESTNAME=BautzenDay1.wgb
gosub getFile
set SRCNAME=Bautzen2nd
set DESTNAME=BautzenDay2.wgb
gosub getFile
set SRCNAME=Denn
set DESTNAME=Dennewitz.wgb
gosub getFile
set SRCNAME=Dresden
set DESTNAME=Dresden.wgb
gosub getFile
set SRCNAME=Katz
set DESTNAME=Katzbach.wgb
gosub getFile
set SRCNAME=Leipzigfull
set DESTNAME=Leipzig.wgb
gosub getFile
set SRCNAME=Leipzig18
set DESTNAME=Leipzig18.wgb
gosub getFile
set SRCNAME=Liebertwolkwitz
set DESTNAME=Liebertwolkwitz.wgb
gosub getFile
set SRCNAME=Lutzen
set DESTNAME=Lutzen.wgb
gosub getFile
set SRCNAME=Wachau
set DESTNAME=Wachau.WGB
gosub getFile

quit

:getFile
    unset /q BESTFILE
    set BESTDATE=0
    rem Iterate through SRC files and pick one with newest date
    for %FILE in (%SRCDIR%\%SRCNAME%*.wgb) do gosub checkFile
    rem Copy to destination
    IFF DEFINED BESTFILE THEN
        set DESTFILE=%DESTDIR%\%DESTNAME%
        rem IFF NOT EXIST %DESTFILE% .OR. %@FILEAGE[%DESTFILE] lt %BESTDATE THEN
            copy %BESTFILE% %DESTDIR%\%DESTNAME%
        rem ELSE
        rem     echo %DESTNAME% is up to date
        rem ENDIFF
    ELSE
        echo No match found for %SRCNAME% to %DESTNAME%
    ENDIFF
    return

:checkFile
    set AGE=%@FILEAGE[%FILE]
    if defined DEBUG echo %FILE %AGE
    IFF not defined BESTFILE .or. %AGE% gt %BESTDATE THEN
        set BESTDATE=%AGE%
        set BESTFILE=%FILE
        if defined DEBUG echo Set %FILE as Best
    ENDIFF
    return

