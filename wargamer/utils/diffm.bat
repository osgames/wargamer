rem Show differences between local source and master
rem
rem Usage:
rem   diffm filename
rem====================================================

iff "%1"=="" THEN
  echo.
  echo Usage:
  echo   diffm filename
  echo.
  echo Displays differences between local file and master file
  echo.
  quit 1
else
  diff %1 d:\master\wg\master\%1
endiff
