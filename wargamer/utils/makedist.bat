setlocal

pushd %HOME\game

if NOT defined DIST set DIST=w:\game
if NOT defined SRCDIST set SRCDIST=w:\src

rem set COPYFLAGS=/u /q
rem set COPYFLAGS=/z /e /u /[!*.cam *.jbf *.gid *.sym *.asc]
set COPYFLAGS=/z /e /u /a: /[!*.cam *.jbf *.gid *.asc]
attrib /s /q -r %DIST

if not isdir %DIST         md /s %DIST
if not isdir %DIST\NAP1813 md /s %DIST\NAP1813
if not isdir %DIST\HELP    md /s %DIST\HELP
if not isdir %DIST\SOUNDS  md /s %DIST\SOUNDS

*copy %COPYFLAGS *.exe %DIST
*copy %COPYFLAGS *.dll %DIST
*copy %COPYFLAGS *.sym %DIST
*copy %COPYFLAGS code.key %DIST

*copy %COPYFLAGS nap1813.swg %DIST
*copy %COPYFLAGS *.wgc %DIST
*copy %COPYFLAGS *.wgb %DIST

*copy %COPYFLAGS /s nap1813\*.* %DIST\NAP1813
*copy %COPYFLAGS /s sounds\*.* %DIST\sounds

*copy %COPYFLAGS help\*.hlp %DIST\help
*copy %COPYFLAGS help\*.cnt %DIST\help

*copy %COPYFLAGS fort.bmp %DIST
*copy %COPYFLAGS readme.txt %DIST
*copy %COPYFLAGS *.dat %DIST

attrib /Q /S -r %DIST

popd

pushd %HOME
*copy %COPYFLAGS /s *.cpp *.hpp *.h %SRCDIST

endlocal
