#################################################
# Parse a logfile created by timecommand
#################################################

foreach (<>)
{
    chomp($line = $_);

    # A line is in the format:
    #    filename Timer 1 off: 17:43:26  Elapsed: 0:00:40.43
    # So split the line using spaces and get 1st and 7th word

    # ($file, $timer, $n, $off, $time, $elapsed, $exectime) = split(/\s+/, $line);
    ($file, $exectime) = (split(/\s+/, $line))[0,6];



    push(@list, join(' ', $exectime, $file));
}

# Display with largest times first

foreach (reverse sort @list)
{
    print "$_\n";
}