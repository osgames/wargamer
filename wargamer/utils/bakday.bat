set batpath=%@PATH[%@TRUENAME[%_BATCHNAME]]

set dd=%_date
set dy=%@substr[%dd,0,2]%@substr[%dd,3,2]%@substr[%dd,6,2]
set datfile=%batpath%bakday.day
set BAKFILE=%BACKUP\wg_%@SUBSTR[%dd,0,2]%@SUBSTR[%dd,3,2]i.zip

set ZIPFLAGS=-aPr -x@%batpath%zipext.lst
rem set ZIPFLAGS=-add -rec -path -excl=@%batpath%zipext.lst

iff exist %datfile then
  set lastdate=%@line[%datfile,0]
  rem pkzip25 -t%lastdate %ZIPFLAGS %BAKFILE *.* 
  rem pkzip25 -after=%lastdate %ZIPFLAGS %BAKFILE *.* 
  wzzip %ZIPFLAGS -t%lastdate %BAKFILE *.*
 else
  rem pkzip25 %ZIPFLAGS %BAKFILE *.*
  wzzip %ZIPFLAGS %BAKFILE *.*
endiff

echo %dy >! %datfile

