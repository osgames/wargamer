#!/usr/bin/perl
#
# Convert resource file headers so that they can be read with
# Visual Studio's resource compiler
#
# Scan through header file
# replace: #define symbol expression
# with: #define symbol <value of expression>

$inFile = $ARGV[0];
$outFile = $inFile . ".new";
if($debug)
{
	print "Reading from: $inFile\n";
	print "Writing to: $outFile\n";
}

open(INFILE, $inFile) || die "Can not open $inFile";
open(OUTFILE, ">$outFile") || die "Can not open $outFile";

while(<INFILE>)
{
	/([^\r\n]*)[\r\n]*/;
	$inLine = $1;

	# Match for #define SYMBOL expression // Optional comment
	if($inLine =~ /^\s*(#define\s+)(\S+)(\s+)([^\/]+)(.*)/)
	{
		$prolog=$1;
		$symbol=$2;
		$space=$3;
		$expr=$4;
		$comment=$5;

		# Attempt to strip space from end of expression
		# $expr =~ /^(.*)(\s*)$/;
		# $expr=$1;
		# $comment=$2 . $comment;

		# print "$prolog<$symbol>$space<$expr>$comment\n";

		# Evaluate expression...
		# Expression is of the form:
		#  (SYMBOL[+])*

		# Substitute symbols for $symbols{"symbol"}
		# print "$expr =>";
		$expr =~ s/([a-zA-Z_]\w*)/\$symbols{\"\1\"}/;
		# print "$expr\n";
		# Now execute it as an expression
		$value = eval($expr);
		if($debug)
		{
			print "$symbol => $value\n";
		}

		$symbols{$symbol}=$value;
		
		$outLine = "//convRes: " . $inLine . "\n";
		$outLine .= $prolog . $symbol . $space . $value;
		if($comment)
		{
			$outLine .= " " . $comment;
		}
	}
	else
	{
		$outLine = $inLine;
	}

	print OUTFILE "$outLine\n";
}

close(INFILE) || die "Error closing $inFIle";
close(OUTFILE) || die "Error closing $outFile";

# Print resulting symbols

if($debug)
{
	while (($symbol, $expr) = each (%symbols))
	{
		print "$symbol=$expr\n";
	}
}
