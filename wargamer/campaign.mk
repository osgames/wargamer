##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#####################################################################
# $Id$
#####################################################################
#
# Makefile for Wargamer Project
#
# Most of the program is split into libraries
# Each library has it's own .MIF file, making it easier for the
# makefile to be updated without locking it all out
#
#####################################################################
#
# $Log$
#
#####################################################################

ROOT=.

CAMPAIGN=1
fname = wgcamp
CDIR=src\c
HDIR=src\h
RDIR=res
rcname=campaign.rc
RC=1
EXT=exe
ROOT=.

!ifdef EDITOR
NAME = wgedit
!else
!ifdef NODEBUG
NAME = campaign
!else
!ifdef NOLOG
NAME = $(fname)NL
!else
NAME = $(fname)DB
!endif
!endif
!endif

!include $(ROOT)\config\wgpaths.mif

CFLAGS += -i=$(RDIR)

lnk_dependencies += campaign.mk

resources =
# us_str.rc

#	clogic.mif	&
#	exe95.mif	&
#	win95.mif 	&

################


!ifdef EDITOR
OBJS = wgedit.obj
!else
OBJS = testcamp.obj
!endif

# OBJS  += nulltact.obj


TARGETS += makedll

SYSLIBS = COMCTL32.LIB VFW32.LIB DSOUND.LIB DXGUID.LIB

all :: $(TARGETS) .SYMBOLIC

testcamp.obj : wargamer.cpp .AUTODEPEND
        $(CPP) $(CPPFLAGS) -DTEST_CAMPAIGN $[* -fo$(ODIR)\$^.


wgedit.obj : wargamer.cpp .AUTODEPEND
        $(CPP) $(CPPFLAGS) -DCAMPAIGN_EDITOR $[* -fo$(ODIR)\$^.

###############################################################
# Libraries

!include src\system\system.mif
!include src\gamesup\gamesup.mif
!include src\ob\ob.mif
!include src\gwind\gwind.mif
!include src\campdata\campdata.mif
!include src\campdata\clogic.mif
!ifndef EDITOR
!include src\camp_ai\camp_ai.mif
!endif
!include src\mapwind\mapwind.mif
!ifdef CUSTOMIZE
!include src\campedit\campedit.mif
!endif
!include src\campwind\campwind.mif
!include src\campgame\campgame.mif
!ifndef EDITOR
!include src\frontend\frontend.mif
!endif

###############################################################
# Operating System stuff

!include $(ROOT)\config\exe95.mif

###############################################################
# Linking

linkit : .PROCEDURE
	 @%append $(LNK) option stack=64k
!ifdef CODEVIEW
	 @%append $(LNK) debug codeview
	 @%append $(LNK) option cvpack
!else
	 @%append $(LNK) debug DWARF all
!endif
!ifdef NODEBUG
#	 @%append $(LNK) OPTION VFREMOVAL
	 @%append $(LNK) option ELIMINATE
!else
#	 @%append $(LNK) option INCREMENTAL
!endif
	 @%append $(LNK) option SYMFILE


#	 @%append $(LNK) LIB COMCTL32.LIB
#	 @%append $(LNK) LIB VFW32.LIB


###############################################################
# Instructions for making DLLs

makedll : .SYMBOLIC
	@!wmake $(__MAKEOPTS__) $(MFLAGS) -f nap1813.mk

#	@!wmake $(__MAKEOPTS__) $(MFLAGS) -f acw.mk


#####################################################################
# $Log$
#####################################################################
