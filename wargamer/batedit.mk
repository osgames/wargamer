##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#################################################################
# Makefile for Wargamer Battle Editor
#################################################################

ROOT=.

fname = Bated
CDIR=batedit
HDIR=batedit
RDIR=res
rcname=batedit.rc
RC=1
EXT=EXE
GNAME=bated
ROOT=.
# OBJ_PATH=$(ROOT)\o\$(SubType)

!ifdef EDITOR
NAME = $(fname)ed
!else
!ifdef NODEBUG
NAME = $(fname)
!else
!ifdef NOLOG
NAME = $(fname)NL
!else
NAME = $(fname)DB
!endif
!endif
!endif


!include $(ROOT)\config\wgpaths.mif

lnk_dependencies += batedit.mk

resources = btl_us_s.rc

################

OBJS = batedit.obj editwind.obj editgame.obj locator.obj btool.obj dialogs.obj sidebar.obj bat_oob.obj

###############################################################
# Libraries

TARGETS += makedll

SYSLIBS = COMCTL32.LIB VFW32.LIB DSOUND.LIB

# all :: $(TARGETS) nap1813\batres.dll .SYMBOLIC
all :: $(TARGETS) .SYMBOLIC

!include $(ROOT)\src\system\system.mif
!include $(ROOT)\src\gamesup\gamesup.mif
!include $(ROOT)\src\ob\ob.mif
!include $(ROOT)\src\batdata\batdata.mif
!include $(ROOT)\src\batlogic\batlogic.mif
!include $(ROOT)\src\batdisp\batdisp.mif
!include $(ROOT)\src\batai\batai.mif
# !include batwind.mif

CFLAGS += -i=$(ROOT)\src\h;res

!include $(ROOT)\config\exe95.mif

###############################################################
# Compile instructions


###############################################################
# Linking

linkit : .PROCEDURE
         @%append $(LNK) option stack=64k
!ifdef CODEVIEW
         @%append $(LNK) debug codeview
         @%append $(LNK) option cvpack
!else
         @%append $(LNK) debug DWARF all
!endif
!ifdef NODEBUG
#        @%append $(LNK) OPTION VFREMOVAL
         @%append $(LNK) option ELIMINATE
!else
#        @%append $(LNK) option INCREMENTAL
!endif
         @%append $(LNK) option SYMFILE
         @%append $(LNK) LIB COMCTL32.LIB

makedll: .SYMBOLIC
        @!wmake $(MFLAGS) -f batedres.mk
