##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

##########################################
# Makefile for towndial.dll
#
# Contains resources for town dialog

DLL=1
RC=1

name = towndial
rcname=towndial.rc

ROOT=.

CDIR=c
HDIR=h
RDIR=res
!ifdef NODEBUG
ODIR=o\final
LIBDIR=o\final
!else
ODIR=o\debug
LIBDIR=o\debug
!endif

LNKEXT=lk

LNK = $(name).$(LNKEXT)

CFLAGS = -i=$(HDIR) -bd
!ifdef CODEVIEW
CFLAGS += -hc
!endif

lnk_dependencies = $(ROOT)\towndial.mk $(ROOT)\config\win95.mif

resources = 

OBJS = td_dll.obj

!include $(ROOT)\config\win95.mif

linkit : .PROCEDURE
	 @%append $(LNK) option stack=64k
!ifdef CODEVIEW
	 @%append $(LNK) debug codeview
	 @%append $(LNK) option cvpack
!else
	 @%append $(LNK) debug all
!endif
	 # @%append $(LNK) LIB COMCTL32.LIB


copyit: .PROCEDURE
	# Do nothing
#####################################################################
# Precompiled Header names
#####################################################################

!ifdef NODEBUG
PCH_GLOBAL = glob_dll.pch
PCH_WINDOWS = wind_dll.pch
!else
PCH_GLOBAL = g_dll_d.pch
PCH_WINDOWS = w_dll_d.pch
!endif


#####################################################################
# Special Case compiles
#####################################################################

td_dll.obj: td_dll.cpp .AUTODEPEND
	$(CPP) $(CPPFLAGS) -fh=$(PCH_GLOBAL) $[* -fo$(ODIR)\$^.

