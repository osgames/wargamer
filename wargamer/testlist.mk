##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

######################################################
# $Id$
######################################################
#
# Makefile for testing Linked List
#
######################################################

name = testlist

CDIR=c
HDIR=h
ODIR=o
ROOT=.
EXE_DIR=.

all :: $(TARGETNAME) .SYMBOLIC
	@%null

!include $(ROOT)\wgpaths.mif

lnk_dependencies += testlist.mk

OBJS = testlist.obj



!include system.mif
!include dosnt.mif



linkit : .PROCEDURE
    @%append $(LNK) option stack=8k
    

