/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef EDITWIND_HPP
#define EDITWIND_HPP

#ifndef __cplusplus
#error batwind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game Windows
 *
 *----------------------------------------------------------------------
 */

#include "refptr.hpp"
#include "batdata.hpp"
#include "dialogs.hpp"
#include "savegame.hpp"

// class BattleGameInterface;

class BattleEditorInterface
{
    public:
        virtual void openBattle() = 0;
        virtual void saveBattle(bool promptName) = 0;
        virtual BattleData* battleData() = 0;
        virtual void convertAIScriptToBinary() = 0;
};



class FileReader;
class FileWriter;

/*
 * Classes for reading/writing Campaign games
 *
 * T should be derived from FileBase
 * getMode is a virtual function in FileBase
 */

template<class T>
class BattleFile : public T
{
                SaveGame::FileFormat d_mode;
        public:
                BattleFile(const char* fileName, SaveGame::FileFormat mode) :
                        T(fileName),
                        d_mode(mode)
                {
                }

                virtual ~BattleFile() { }

                virtual int getMode() const { return d_mode; }
};






class BattleMapWind;
// using namespace WG_BattleData;

namespace BattleWindows_Internal
{

class BattleWindowsImp;
 
class BattleWindows : 
        public RefBaseDel
{
                BattleWindowsImp* d_imp;                // Implementation class

        public:
                BattleWindows(BattleEditorInterface* data);
                ~BattleWindows();

                void timeChanged();
                        // The Battle Time has changed, so redraw clock & Map

                HWND getHWND() const;

                BattleMapWind * getBattleMap(void);

};

};      // namespace BattleWindows_Internal

/*
 * Public namespace for other modules to use
 */

namespace WG_BattleWindows
{

using BattleWindows_Internal::BattleWindows;

};      // namespace BattleWindows

#endif /* BATWIND_HPP */

