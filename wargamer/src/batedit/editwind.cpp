/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996-1998, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game Windows
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#define EditorCaption "BattleEditor Main Window"

// editor windows header file
#include "editwind.hpp"
#include "batctrl.hpp"
#include "batres.h"                             // res: Resources
#include "batedres.h"
#include "batres_s.h"                   // res: Resource Strings

#include "dialogs.hpp"
#include "bat_oob.hpp"

#include "bcreate.hpp"
#include "hexdata.hpp"
#include "building.hpp"

#include "sidebar.hpp"
#include "locator.hpp"                  // BatEdit: Locator window

#include "btwin_i.hpp"                  // batdisp: Interface for batmap user
#include "batmap.hpp"                   // batdisp: Battlefield display window

#include "batdata.hpp"                  // batdata: Battle Data

#include "scenario.hpp"                 // gamesup: Scenario information

#include "app.hpp"                              // system
#include "palette.hpp"                  // system: Palette manager
#include "wmisc.hpp"                            // system: Miscellaneous Windows utilities
#include "gamectrl.hpp"                 // system
#include "tooltip.hpp"                  // system
#include "help.h"                                       // system
#include "bmp.hpp"                              // system: BMP File utilities
#include "framewin.hpp"                 // system

#include "filecnk.hpp"
#include "savegame.hpp"

#include <memory>                   // stl:
#include <utility>
#include <commdlg.h>


#include "autoptr.hpp"                  // system/stl
#ifdef DEBUG                               //mtc122
#include "clog.hpp"                             // system: Logfile
static LogFileFlush bwLog("batWind.log");
static LogFileFlush obLog("OrderOfBattle.txt");
#endif

#include "maketer.hpp"
#include "pathfind.hpp"

#include "batcord.hpp"
#include "batarmy.hpp"

#include "hexmap.hpp"
#include "..\batlogic\bdeploy.hpp"
#include "bobutil.hpp"

#include "initunit.hpp"
#include "bdeploy.hpp"
#include "bobiter.hpp"

#include "pdeploy.hpp"


#ifdef _MSC_VER
using namespace std::rel_ops;   // Allows generic operator!=()
#endif


/*
 * Implementation class
 */

class BattleData;

namespace
{

class BattleChildWindow :
      public SimpleChildWindow,
      public RefBaseDel
{
    BattleChildWindow(const BattleChildWindow&);
    BattleChildWindow& operator=(const BattleChildWindow&);

  public:
    BattleChildWindow(HWND hParent);
    ~BattleChildWindow()
    {
    }
};

/*
 * Test Windows implementation
 */

BattleChildWindow::BattleChildWindow(HWND hParent) :
  SimpleChildWindow(scenario->getBorderColors())
{
  create(hParent);
}


}      // un-named namespace









namespace BattleWindows_Internal
{

class BattleWindowsImp :
      public BattleWindowsInterface,
      public WindowBaseND,
      public PaletteWindow,
      public CustomBorderWindow,
      public OBDeployUser
{
    // Unimplemented copy
    BattleWindowsImp(const BattleWindowsImp&);
    BattleWindowsImp& operator =(const BattleWindowsImp&);

  public:
    BattleWindowsImp(BattleEditorInterface* batGame);
    ~BattleWindowsImp();

    void makeOBDialog();
    void killOBDialog();

    /*
     * BattleWindows Implementation
     */
    void timeChanged();
    // Battle time has changed, so update clock

    void redrawMap(bool all)
    {
      d_mapWind->update(all);
    }

    /*
     * Implementation of BattleWindowsInterface
     */

    virtual HWND hwnd() const
    { 
      return getHWND();
    }
    // Get Window Handle

    BattleMapWind * getBattleMap(void)
    { 
      return d_mapWind;
    }

    virtual bool setMapLocation(const BattleLocation& l);
    // Set centre of main map, return true if has changed

    virtual void mapAreaChanged(const BattleArea& area);
    // MapWindow has changed what it is showing
    virtual void mapZoomChanged();
    // Get mapwindow zoom buttons updated

    virtual void onButtonDown(const BattleMapSelect& info);
    // sent by mapwind when Button is pressed
    virtual void onButtonUp(const BattleMapSelect& info);
    // sent by mapwind when Button is released while not dragging
    virtual void onStartDrag(const BattleMapSelect& info);
    // sent by mapwind when Mouse is moved while button is held
    virtual void onEndDrag(const BattleMapSelect& info);
    // sent by mapwind when button is released while dragging
    virtual void onMove(const BattleMapSelect& info);
    // sent by mapwind when Mouse has moved

    void orderCP(CRefBattleCP cp)
    {
    }
    void messageWindowUpdated()
    {
    }
    void windowDestroyed(HWND hwnd)
    {
    }
  private:
    bool doPlayerDeploy(RPBattleData batData, const RefBattleCP& cp, const BattleMeasure::HexCord& startHex, HexList* hl);

    void RemoveFromMap(BattleCP * cp, bool removechildren);
    void RemoveCP(BattleCP * cp);
    void RemoveSP(BattleSP * sp);
    void HighlightUnits(bool state);
    void HighlightCP(BattleCP * cp, bool state);
    void HighlightSP(BattleSP * sp, bool state);

    void OnSelChange(BattleUnit * unit);
    void OnDeployUnit(BattleCP * cp);
    void OnRemoveUnit(BattleCP * cp);
    void OnDragUnitFromTreeView(BattleCP * cp);

//      void deployUnitsFromSave(BattleData * batdata);
//      void deployChildrenFromSave(BattleData * batdata, BattleCP * cp);

    void SetHeightAreaSelection(void);
    void SetAreaHeight(void);

    void SetCropSelection(void);
    void CropMap(void);

    void RemoveModeHighlights(void);


    void SetCPNationalities(void);
    void FixDivisionalCPs(void);
	  void ResetReinforcements(void);  //mtc122


    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    static ATOM registerClass();
    virtual LPCSTR className() const
    { return s_className;
    }

    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd);
    void onPaint(HWND hWnd);
    void onNCPaint(HWND hwnd, HRGN hrgn);
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onSize(HWND hwnd, UINT state, int cx, int cy);
    void onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo);
    LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
    void onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild);
    void onClose(HWND hWnd);
    void onMenuSelect(HWND hwnd, HMENU hmenu, int item, HMENU hmenuPopup, UINT flags);
    BOOL onHelp(HWND hwnd, LPHELPINFO lParam);

    void positionWindows();
    // Set the positions of all the child windows

    void setZoomButtons();
    // Set up the zoom buttons and menu

    void setMapMode(BattleMapInfo::Mode mode);
    void startMapZoom();

    //---- WindowOwner virtual function
    // virtual void windowDestroyed(HWND hWnd);

    void checkMenu(int id, BOOL flag);
    void enableMenu(int id, BOOL flag);
    HMENU getMenu();

    std::string MapFilename;
    std::string OOBFilename;
    std::string BattleFilename;

    bool readMap();
    bool writeMap();

    bool readOOB();
    bool writeOOB();

    bool readData();
    bool writeData();

    void prepareForLoad();
    void afterLoad();
    void prepareForSave();
    void afterSave();


    virtual void onLButtonDown(const BattleMapSelect& info);// { onButtonDown(info); }
    // sent by mapwind when Button is pressed
    virtual void onLButtonUp(const BattleMapSelect& info);// { }
    // sent by mapwind when Button is released while not dragging
    virtual void onRButtonDown(const BattleMapSelect& info)
    {
      onButtonDown(info);
    }
    // sent by mapwind when Button is pressed
    virtual void onRButtonUp(const BattleMapSelect& info)
    {
    }
    // sent by mapwind when Button is released while not dragging
    virtual bool setCursor()
    { return FALSE;
    }
    virtual void updateAll()
    {
    }
    virtual bool updateTracking(const BattleMeasure::HexCord& hex)
    { return FALSE;
    }
    virtual const DrawDIBDC* mapDib()      const
    { return 0;
    }
    virtual BattleMapDisplay* mapDisplay() const
    { return 0;
    }

    void StoreMap(void);
    void RecallMap(void);

    void AddBorder(void);

    void removeOrphans();

  private:
    /*----------------------
     * Data member
     */

    static ATOM s_classAtom;
    static const char s_className[];

    PBattleData d_battleData;
    BattleEditorInterface* d_batGame;

    // main menu
    HMENU main_menu;

    // Child Windows
    BattleMapWind* d_mapWind;
    BW_Locator * d_locator;
    RefPtr<BattleSideBar> d_sideBar;

    // Dialogs
    EditorTerrainDialogClass * TerrainDialog;
    EditorHeightsDialogClass * HeightsDialog;
    EditorPathsDialogClass * PathsDialog;
    EditorSettlementsDialogClass * SettlementsDialog;
    EditorEdgesDialogClass * EdgesDialog;
    EditorLabelsDialogClass * LabelsDialog;
    EditorEraseDialogClass * EraseDialog;
    EditorCropMapDialogClass * CropMapDialog;
    EditorDateTimeDialogClass * DateTimeDialog;
    EditorWeatherDialogClass * WeatherDialog;
    EditorPathfindDialogClass * PathfindDialog;

    // OOB Dialog
    BattleOOBClass * d_obDialog;

    BattleMeasure::HexCord PathStartHex;
    BattleMeasure::HexCord PathEndHex;

    // Backup map for UNDO
    BattleMap UndoMap;


    BattleMeasure::BattleTime d_battleDuration;


    /*
     * Deployment Details
     */

    // unit currently being dragged
    BattleCP * m_lpSelectedUnit;
    BattleMeasure::HexCord m_SelectedUnitHex;
    bool m_bDragging;
    bool m_bUnitHighlighted;

    BattleMeasure::HexCord m_DeploymentHex;
    HexList m_DeploymentHexList;

};

/*
 * Implementation functions
 */

const char BattleWindowsImp::s_className[] = "MainBattle";
ATOM BattleWindowsImp::s_classAtom = NULL;

static const char battleWindowRegName[] = "BattleWindow";

BattleWindowsImp::BattleWindowsImp(BattleEditorInterface* batGame) :
  WindowBaseND(),
  CustomBorderWindow(scenario->getBorderColors()),
  d_batGame(batGame),
  d_battleData(batGame->battleData()),

  d_mapWind(0),
  d_locator(0),
  d_sideBar(0),
  d_obDialog(0)
{
  d_battleDuration.setTick(0);
  d_battleData->endGameIn(d_battleDuration.getTick() );

  // setup undo map
  BattleMeasure::HexCord size;
  size = d_battleData->map()->getSize();
  UndoMap.create(size);

  MapFilename[0] = 0;
  OOBFilename[0] = 0;
  BattleFilename[0] = 0;

  // Load a test palette
  BMP::getPalette("nap1813\\BattlePalette.bmp");



  registerClass();

  createWindow(
               0,
                   EditorCaption,
                   WS_OVERLAPPEDWINDOW |
                   WS_MAXIMIZE |                           // Start off maximized by default
                   WS_CLIPCHILDREN,
                   CW_USEDEFAULT,                  /* init. x pos */
                   CW_USEDEFAULT,                  /* init. y pos */
                   CW_USEDEFAULT,                  /* init. x size */
                   CW_USEDEFAULT,                  /* init. y size */
                   NULL,                           /* parent window */
                   NULL                           /* menu handle */
    );

  ASSERT(hWnd != NULL);
  if( hWnd == NULL )
    return;

  SetWindowText(hWnd, EditorCaption);

  if( getState(battleWindowRegName, WSAV_POSITION | WSAV_SIZE | WSAV_RESTORE) )
    ShowWindow(hWnd, SW_MAXIMIZE);

  show(true);

  UpdateWindow(hWnd);

  // get the menu
  main_menu = GetMenu(hWnd);

  // show the tiny window
  if( d_locator ) d_locator->show(true);


  /*
   * Create Editor dialogs
   */

  TerrainDialog = new EditorTerrainDialogClass(hwnd(), APP::instance() );
  HeightsDialog = new EditorHeightsDialogClass(hwnd(), APP::instance() );
  PathsDialog = new EditorPathsDialogClass(hwnd(), APP::instance() );
  SettlementsDialog = new EditorSettlementsDialogClass(hwnd(), APP::instance() );
  EdgesDialog = new EditorEdgesDialogClass(hwnd(), APP::instance() );
  LabelsDialog = new EditorLabelsDialogClass(hwnd(), APP::instance() );
  EraseDialog = new EditorEraseDialogClass(hwnd(), APP::instance() );
  CropMapDialog = new EditorCropMapDialogClass(hwnd(), APP::instance() );
  DateTimeDialog = new EditorDateTimeDialogClass(hwnd(), APP::instance() );
  PathfindDialog = new EditorPathfindDialogClass(hwnd(), APP::instance() );
  WeatherDialog = new EditorWeatherDialogClass(hwnd(), APP::instance() );
  DateTimeDialog->SetDateAndTime(d_battleData->getDateAndTime(), d_battleDuration.getTick() );

  makeOBDialog();

  /*
   * Reset Editor Parameteres
   */

  PrimaryTerrain = 1;
  SecondaryTerrain = 1;
  TerrainAreaSize = 5;
  SettlementsType = 0;
  SettlementsStatus = SETTLEMENT_NORMAL;
  PathsMode = PATHS_ROAD;
  TerrainHexMode = HEX_SINGLE;
  HeightsHexMode = HEX_SINGLE;
  SettlementsHexMode = HEX_SINGLE;
  EdgesType = EDGE_WALL;


  heightarea_center = BattleMeasure::HexCord(0,0);
  heightarea_size = BattleMeasure::HexCord(4,4);
  heightarea_height = 0;

  label_hex = BattleMeasure::HexCord(0,0);

  /*
   * Setup Deployment details
   */

  m_lpSelectedUnit = NULL;
  m_SelectedUnitHex = BattleMeasure::HexCord(0,0);
  m_bDragging = false;
  m_bUnitHighlighted = false;

  BobUtility::initUnitTypeFlags(d_battleData);

  d_mapWind->getDisplay()->ShowSelection(false);
}

BattleWindowsImp::~BattleWindowsImp()
{
  delete TerrainDialog;
  delete HeightsDialog;
  delete PathsDialog;
  delete SettlementsDialog;
  delete EdgesDialog;
  delete LabelsDialog;
  delete EraseDialog;
  delete CropMapDialog;
  delete DateTimeDialog;
  delete PathfindDialog;
  delete WeatherDialog;

  delete d_obDialog;

  delete d_locator;
  delete d_mapWind;
  d_sideBar = 0;
}

void BattleWindowsImp::makeOBDialog()
{
  ASSERT(d_obDialog == 0);
  d_obDialog = new BattleOOBClass(hwnd(), APP::instance(),d_battleData, d_mapWind, this );
}

void BattleWindowsImp::killOBDialog()
{
  BattleOOBClass* dial = d_obDialog;
  d_obDialog = 0;
  dial->destroy();
  delete dial;
}


/*
 * Register Main Class
 */


ATOM BattleWindowsImp::registerClass()
{
  if( !s_classAtom )
  {
    WNDCLASS wc;

    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = baseWindProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = sizeof(BattleWindowsImp*);
    wc.hInstance = APP::instance();
    wc.hIcon = LoadIcon(APP::instance(), MAKEINTRESOURCE(ICON_WARGAMER));
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH) (COLOR_BACKGROUND + 1);
    wc.lpszMenuName = MAKEINTRESOURCE(MENU_BATEDIT);
    wc.lpszClassName = s_className;
    s_classAtom = RegisterClass(&wc);
  }

  ASSERT(s_classAtom != NULL);

  return s_classAtom;
}




/*
 * Deployment helper functions
 */



void BattleWindowsImp::OnSelChange(BattleUnit * unit)
{

  BattleCP * old_cp = m_lpSelectedUnit;

  // if unit already selected
  if( m_lpSelectedUnit )
  {

    // remove highlights
    if( m_bUnitHighlighted )
    {
      HighlightUnits(false);
      m_bUnitHighlighted = false;
    }

    // reset dragging flag
    m_bDragging = false;

    // clear selected unit's hex
    m_SelectedUnitHex = BattleMeasure::HexCord(0,0);

    // clear selected unit
    m_lpSelectedUnit = NULL;
  }

  // ensure we are referencing a CP
  BattleCP * cp = dynamic_cast<BattleCP *>(unit);
  if( ! cp )
  {
    BattleSP * sp = dynamic_cast<BattleSP *>(unit);
    ASSERT(sp);
    cp = sp->parent();
    ASSERT(cp);
  }

  // set selected unit
  m_lpSelectedUnit = cp;

  // highlight unit's hexes
  HighlightUnits(cp != 0);
  m_bUnitHighlighted = true;

  // reset dragging flag
  m_bDragging = false;

  redrawMap(false);

}




void BattleWindowsImp::OnDeployUnit(BattleCP * cp)
{

  // remove from current position
  if( d_obDialog->CurrentSelectionCP )
  {
    if( d_obDialog->CurrentSelectionCP->sp() ) RemoveFromMap(d_obDialog->CurrentSelectionCP, true);
    else RemoveFromMap(d_obDialog->CurrentSelectionCP, false);
    d_mapWind->update(false);
  }
  else if( d_obDialog->CurrentSelectionSP )
  {
    if( d_obDialog->CurrentSelectionSP->parent() )
    {
      if( d_obDialog->CurrentSelectionSP->parent()->sp() ) RemoveFromMap(d_obDialog->CurrentSelectionSP->parent(), true);
      else RemoveFromMap(d_obDialog->CurrentSelectionSP->parent(), false);
      d_mapWind->update(false);
    }
  }

  // redeploy at destination
  if( d_obDialog->CurrentSelectionCP )
  {
    B_Logic::playerDeploy(d_obDialog->d_batData, d_obDialog->CurrentSelectionCP, d_obDialog->CurrentSelectionCP->hex(), 0);
    d_mapWind->update(false);
  }
  else if( d_obDialog->CurrentSelectionSP )
  {
    if( d_obDialog->CurrentSelectionSP->parent() )
    {
      doPlayerDeploy(d_obDialog->d_batData, d_obDialog->CurrentSelectionSP->parent(), d_obDialog->CurrentSelectionSP->parent()->hex(), 0);
      d_mapWind->update(false);
    }
  }

}




void BattleWindowsImp::OnRemoveUnit(BattleCP * cp)
{

  // remove units
  if( d_obDialog->CurrentSelectionCP )
  {
    if( d_obDialog->CurrentSelectionCP->sp() ) RemoveFromMap(d_obDialog->CurrentSelectionCP, true);
    else RemoveFromMap(d_obDialog->CurrentSelectionCP, false);
    d_obDialog->CurrentSelectionCP->hex(BattleMeasure::HexCord(0,0));
    d_mapWind->update(false);
  }
  else if( d_obDialog->CurrentSelectionSP )
  {
    if( d_obDialog->CurrentSelectionSP->parent()->sp() ) RemoveFromMap(d_obDialog->CurrentSelectionSP->parent(), true);
    else RemoveFromMap(d_obDialog->CurrentSelectionSP->parent(), false);
    d_obDialog->CurrentSelectionSP->parent()->hex(BattleMeasure::HexCord(0,0));
    d_mapWind->update(false);
  }
}



void BattleWindowsImp::OnDragUnitFromTreeView(BattleCP * cp)
{

}




/*
 * Wrapper to insulate deployment calls
 */
bool BattleWindowsImp::doPlayerDeploy(RPBattleData batData, const RefBattleCP& cp, const BattleMeasure::HexCord& startHex, HexList* hl)
{

  return(B_Logic::playerDeploy(batData, cp, startHex, hl) );

}


#if 0 // unused
void BattleWindowsImp::deployUnitsFromSave(BattleData * batdata)
{
  for( Side side=0; side<2; side++ )
  {
    deployChildrenFromSave(batdata, batdata->ob()->getTop(side));
  }
}




void BattleWindowsImp::deployChildrenFromSave(BattleData * batdata, BattleCP * cp)
{
  while( cp )
  {
    doPlayerDeploy(batdata, cp, cp->hex(), 0);
    if( cp->child() )
    {
      deployChildrenFromSave(batData, cp->child());
    }
    cp = cp->sister();
  }
}
#endif





void BattleWindowsImp::RemoveFromMap(BattleCP * cp, bool removechildren)
{

  if( d_battleData->hexMap()->isUnitPresent(cp) )
  {
    BattleHexMap::iterator map_iter = d_battleData->hexMap()->find(cp);
    d_battleData->hexMap()->remove(map_iter);
  }

  if( ! removechildren ) return;

  if( cp->sp() ) RemoveSP(cp->sp() );

  if( cp->child() ) RemoveCP(cp->child() );

}



void BattleWindowsImp::RemoveCP(BattleCP * cp)
{

  if( d_battleData->hexMap()->isUnitPresent(cp) )
  {
    BattleHexMap::iterator map_iter = d_battleData->hexMap()->find(cp);
    d_battleData->hexMap()->remove(map_iter);
  }

  if( cp->sp() ) RemoveSP(cp->sp() );

  if( cp->child() ) RemoveCP(cp->child() );

  if( cp->sister() ) RemoveCP(cp->sister() );


}


void BattleWindowsImp::RemoveSP(BattleSP * sp)
{

  while( sp )
  {

    if( d_battleData->hexMap()->isUnitPresent(sp) )
    {
      BattleHexMap::iterator map_iter = d_battleData->hexMap()->find(sp);
      d_battleData->hexMap()->remove(map_iter);
    }

    sp = sp->sister();
  }

}



void BattleWindowsImp::HighlightUnits(bool state)
{

  BattleCP * top_cp = m_lpSelectedUnit;

  if( state ) d_battleData->map()->highlightHex(top_cp->hex(), top_cp->getSide() | HIGHLIGHT_SET);
  else d_battleData->map()->unhighlightHex(top_cp->hex() );

  if( top_cp->sp() ) HighlightSP(top_cp->sp(), state);

  if( top_cp->child() ) HighlightCP(top_cp->child(), state);

}

void BattleWindowsImp::HighlightCP(BattleCP * cp, bool state)
{

  if( state ) d_battleData->map()->highlightHex(cp->hex(), cp->getSide() | HIGHLIGHT_SET);
  else d_battleData->map()->unhighlightHex(cp->hex() );
  if( cp->sp() ) HighlightSP(cp->sp(), state);

  if( cp->child() ) HighlightCP(cp->child(), state);

  if( cp->sister() ) HighlightCP(cp->sister(), state);

}

void BattleWindowsImp::HighlightSP(BattleSP * sp, bool state)
{

  if( state ) d_battleData->map()->highlightHex(sp->hex(), sp->parent()->getSide() | HIGHLIGHT_SET);
  else d_battleData->map()->unhighlightHex(sp->hex() );

  if( sp->sister() ) HighlightSP(sp->sister(), state);

}







void BattleWindowsImp::SetHeightAreaSelection(void)
{

  BattleMeasure::HexCord MapSize = d_battleData->map()->getSize();
  int mapwidth = MapSize.x();
  int mapheight = MapSize.y();

  int half_x = heightarea_size.x() / 2;
  int half_y = heightarea_size.y() / 2;

  // confine selection area to map
  int overlap;

  overlap = heightarea_center.x() - half_x;
  if( overlap < 0 ) heightarea_center.x( heightarea_center.x() + abs(overlap) );

  overlap = heightarea_center.x() + half_x;
  if( overlap > mapwidth ) heightarea_center.x( heightarea_center.x() - (overlap - mapwidth) );

  overlap = heightarea_center.y() - half_y;
  if( overlap < 0 ) heightarea_center.y( heightarea_center.y() + abs(overlap) );

  overlap = heightarea_center.y() + half_y;
  if( overlap > mapheight ) heightarea_center.y( heightarea_center.y() - (overlap - mapwidth) );

  // set selection area in display
  BattleMeasure::HexCord topleft = BattleMeasure::HexCord(heightarea_center.x() - (heightarea_size.x() / 2), heightarea_center.y() - (heightarea_size.y() / 2) );
  BattleMeasure::HexCord bottomright = BattleMeasure::HexCord(heightarea_center.x() + (heightarea_size.x() / 2), heightarea_center.y() + (heightarea_size.y() / 2) );

  d_mapWind->getDisplay()->SetSelectionArea(topleft, bottomright);
  d_mapWind->getDisplay()->ShowSelection(true);
}






void BattleWindowsImp::SetAreaHeight(void)
{

  BattleMeasure::HexCord MapSize = d_battleData->map()->getSize();
  int mapwidth = MapSize.x();
  int mapheight = MapSize.y();

  int half_x = (heightarea_size.x() / 2)+1;
  int half_y = (heightarea_size.y() / 2)+1;

  // confine selection area to map
  int overlap;

  overlap = heightarea_center.x() - half_x;
  if( overlap < 0 ) heightarea_center.x( heightarea_center.x() + abs(overlap) );

  overlap = heightarea_center.x() + half_x;
  if( overlap > mapwidth ) heightarea_center.x( heightarea_center.x() - (overlap - mapwidth) );

  overlap = heightarea_center.y() - half_y;
  if( overlap < 0 ) heightarea_center.y( heightarea_center.y() + abs(overlap) );

  overlap = heightarea_center.y() + half_y;
  if( overlap > mapheight ) heightarea_center.y( heightarea_center.y() - (overlap - mapwidth) );

  // set selection area in display
  BattleMeasure::HexCord topleft = BattleMeasure::HexCord(heightarea_center.x() - (heightarea_size.x() / 2), heightarea_center.y() - (heightarea_size.y() / 2) );
  BattleMeasure::HexCord bottomright = BattleMeasure::HexCord(heightarea_center.x() + (heightarea_size.x() / 2), heightarea_center.y() + (heightarea_size.y() / 2) );

  for( int y=topleft.y(); y<bottomright.y(); y++ )
  {

    for( int x=topleft.x(); x<bottomright.x(); x++ )
    {

      BattleTerrainHex& maphex = d_battleData->map()->get(BattleMeasure::HexCord(x,y) );
      maphex.d_height = heightarea_height;
    }
  }

  d_mapWind->getDisplay()->SetSelectionArea(topleft, bottomright);
  d_mapWind->getDisplay()->ShowSelection(false);

  d_mapWind->update(true);

}



void BattleWindowsImp::SetCropSelection(void)
{

  BattleMeasure::HexCord MapSize = d_battleData->map()->getSize();
  int mapwidth = MapSize.x();
  int mapheight = MapSize.y();

  int half_x = CropSelectSize.x() / 2;
  int half_y = CropSelectSize.y() / 2;

  // confine selection area to map
  int overlap;

  overlap = CropSelectCenter.x() - half_x;
  if( overlap < 0 ) CropSelectCenter.x( CropSelectCenter.x() + abs(overlap) );

  overlap = CropSelectCenter.x() + half_x;
  if( overlap > mapwidth ) CropSelectCenter.x( CropSelectCenter.x() - (overlap - mapwidth) );

  overlap = CropSelectCenter.y() - half_y;
  if( overlap < 0 ) CropSelectCenter.y( CropSelectCenter.y() + abs(overlap) );

  overlap = CropSelectCenter.y() + half_y;
  if( overlap > mapheight ) CropSelectCenter.y( CropSelectCenter.y() - (overlap - mapwidth) );

  // set selection area in display
  BattleMeasure::HexCord topleft = BattleMeasure::HexCord(CropSelectCenter.x() - (CropSelectSize.x() / 2), CropSelectCenter.y() - (CropSelectSize.y() / 2) );
  BattleMeasure::HexCord bottomright = BattleMeasure::HexCord(CropSelectCenter.x() + (CropSelectSize.x() / 2), CropSelectCenter.y() + (CropSelectSize.y() / 2) );

  d_mapWind->getDisplay()->SetSelectionArea(topleft, bottomright);
  d_mapWind->getDisplay()->ShowSelection(true);

}



void BattleWindowsImp::CropMap(void)
{

  delete d_mapWind;
  d_mapWind = 0;
  d_sideBar = 0;
  delete d_locator;
  d_locator = 0;

  // set selection area in display
  BattleMeasure::HexCord topleft = BattleMeasure::HexCord(CropSelectCenter.x() - (CropSelectSize.x() / 2), CropSelectCenter.y() - (CropSelectSize.y() / 2) );
  if( topleft.y() & 1 ) topleft.y( topleft.y() -1);
  BattleMeasure::HexCord bottomright = BattleMeasure::HexCord(CropSelectCenter.x() + (CropSelectSize.x() / 2), CropSelectCenter.y() + (CropSelectSize.y() / 2) );
  if( bottomright.y() & 1 ) bottomright.y( bottomright.y() -1);

  const BattleMeasure::HexCord size = d_battleData->map()->getSize();

  // crop battlemap to area
  d_battleData->map()->crop(topleft, bottomright);
  // crop buildings list
  d_battleData->buildingList()->crop(topleft, bottomright);
  // adjust remaining buildings positions
  d_battleData->buildingList()->adjust(topleft);
  // remove all units from hex-map
  d_battleData->hexMap()->clear(size);

  int minx = topleft.x();
  if( bottomright.x() < minx ) minx = bottomright.x();
  int miny = topleft.y();
  if( bottomright.y() < miny ) miny = bottomright.y();

  // adjust unit's positions
  for( BattleUnitIter iter(d_battleData->ob() ); !iter.isFinished(); iter.next() )
  {
    BattleMeasure::HexCord h = iter.cp()->hex();
    // only adjust if not at zero
    if( h.x() != 0 && h.y() != 0 )
    {
      h.x(  h.x() - minx);
      h.y( h.y() - miny);
      iter.cp()->hex(h);
    }
  }

  d_mapWind = new BattleMapWind(this, d_battleData);
  d_sideBar = new BattleSideBar(this, d_battleData);
  d_locator = new BW_Locator(this, d_battleData);

  setZoomButtons();

  d_mapWind->create();
  setMapMode(BattleMapInfo::OneMile);
  d_mapWind->show(true);  // ShowWindows();

  d_locator->show(true);

  positionWindows();

  d_locator->update(TRUE);
  d_mapWind->update(TRUE);

  d_mapWind->getDisplay()->ShowSelection(true);



}



void BattleWindowsImp::RemoveModeHighlights(void)
{

  d_battleData->map()->unhighlightHex(label_hex);
  d_mapWind->getDisplay()->ShowSelection(false);

  d_battleData->map()->unhighlightHex(PathFindStartHex);
  d_battleData->map()->unhighlightHex(PathFindEndHex);


  d_mapWind->update(false);
}



/*
 * Message Handler
 */

LRESULT BattleWindowsImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MAINWIND_MESSAGES
  debugLog("MainCampaignWindow::procMessage(%s)\n",
           getWMdescription(hWnd, msg, wParam, lParam));
#endif

  LRESULT l;
  if( handlePalette(hWnd, msg, wParam, lParam, l) )
    return l;

  // big fudge - but never mind, it's only the editor !
  if( d_sideBar() ) d_sideBar->SetModeText(ModeText);

  if( d_mapWind )
  {
    if( EditorMode == DIALOG_CROPMAP ) d_mapWind->getDisplay()->ShowSelection(true);
    else d_mapWind->getDisplay()->ShowSelection(false);
  }

  switch( msg )
  {
    HANDLE_MSG(hWnd, WM_CREATE,        onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY,       onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT,         onPaint);
    HANDLE_MSG(hWnd, WM_NCPAINT,       onNCPaint);
    HANDLE_MSG(hWnd, WM_COMMAND,       onCommand);
    HANDLE_MSG(hWnd, WM_SIZE,          onSize);
    HANDLE_MSG(hWnd, WM_GETMINMAXINFO, onGetMinMaxInfo);
    HANDLE_MSG(hWnd, WM_NOTIFY,        onNotify);
    HANDLE_MSG(hWnd, WM_PARENTNOTIFY,  onParentNotify);
    HANDLE_MSG(hWnd, WM_CLOSE,         onClose);
    HANDLE_MSG(hWnd, WM_MENUSELECT,    onMenuSelect);
    HANDLE_MSG(hWnd, WM_HELP,          onHelp);


    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}



BOOL BattleWindowsImp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  ASSERT(APP::getMainHWND() == NULL);
  APP::setMainHWND(hWnd);

  d_sideBar = new BattleSideBar(this, d_battleData);
  d_locator = new BW_Locator(this, d_battleData);
  d_mapWind = new BattleMapWind(this, d_battleData);

  d_mapWind->create();
  setMapMode(BattleMapInfo::OneMile);
  d_mapWind->show(true);  // ShowWindows();


  setZoomButtons();
  checkMenu(IDM_BAT_HEXOUTLINE, d_mapWind->hexOutline());

#ifdef DEBUG
  checkMenu(IDM_BAT_HEXDEBUG, d_mapWind->hexDebug());
#endif

  d_locator->show(true);


  return TRUE;
}

void BattleWindowsImp::onDestroy(HWND hWnd)
{

  saveState(battleWindowRegName, WSAV_POSITION | WSAV_SIZE | WSAV_RESTORE);

  ASSERT(APP::getMainHWND() == hWnd);
  if( APP::getMainHWND() == hWnd )
    APP::setMainHWND(0);
}

void BattleWindowsImp::onNCPaint(HWND hwnd, HRGN hrgn)
{
  FORWARD_WM_NCPAINT(hwnd, hrgn, defProc);

  HDC hdc = GetWindowDC(hwnd);
  RECT r;
  GetWindowRect(hwnd, &r);
  OffsetRect(&r, -r.left, -r.top);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  drawThickBorder(hdc, r);

  SelectPalette(hdc, oldPal, FALSE);

  ReleaseDC(hwnd, hdc);
}

void BattleWindowsImp::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;

  HDC hdc = BeginPaint(hwnd, &ps);


  EndPaint(hwnd, &ps);
}


/*
 * Commands & Controls given to BattleEditor main window
 */

void BattleWindowsImp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
#ifdef DEBUG
  bwLog.printf("onCommand(%d,%d)", (int) id, (int) codeNotify);
#endif



  // edit controls
  if( codeNotify == EN_CHANGE )
  {

    switch( id )
    {

      case IDC_EDIT_LABEL :
      {

        BattleMeasure::HexCord label_hex = LabelsDialog->Hex;
        const BattleMeasure::HexCord size = d_battleData->map()->getSize();
        // make sure its on map still
        if( label_hex.x() < size.x() && label_hex.y() < size.y() )
        {
          char tmpstr[256];
          BattleTerrainHex& maphex =  d_battleData->map()->get(label_hex);
          GetDlgItemText(LabelsDialog->hwnd, IDC_EDIT_LABEL, tmpstr, 256);
          if( maphex.d_label ) delete[] maphex.d_label;
          maphex.d_label = copyString(tmpstr);
        }
        d_mapWind->update(false);
        break;
      }

      case IDC_EDIT_POINTSVALUE :
      {

        BattleMeasure::HexCord label_hex = LabelsDialog->Hex;
        const BattleMeasure::HexCord size = d_battleData->map()->getSize();
        // make sure its on map still
        if( label_hex.x() < size.x() && label_hex.y() < size.y() )
        {
          BattleTerrainHex& maphex =  d_battleData->map()->get(label_hex);
          maphex.d_victoryPoints = (unsigned char) GetDlgItemInt(LabelsDialog->hwnd, IDC_EDIT_POINTSVALUE, NULL, TRUE);
          d_mapWind->update(false);
        }
        return;
      }

      case IDC_EDIT_NEWSIZEX :
      {

        int val = GetDlgItemInt(CropMapDialog->hwnd, IDC_EDIT_NEWSIZEX, NULL, TRUE);
        CropSelectSize.x(val);
        d_mapWind->getDisplay()->ShowSelection(true);
        SetCropSelection();
        d_mapWind->update(false);
        return;
      }

      case IDC_EDIT_NEWSIZEY :
      {

        int val = GetDlgItemInt(CropMapDialog->hwnd, IDC_EDIT_NEWSIZEY, NULL, TRUE);
        CropSelectSize.y(val);
        d_mapWind->getDisplay()->ShowSelection(true);
        SetCropSelection();
        d_mapWind->update(false);
        return;
      }

      case IDC_EDIT_DATE :
      {
        int val = GetDlgItemInt(DateTimeDialog->hwnd, IDC_EDIT_DATE, NULL, TRUE);
        BattleMeasure::BattleTime date_time = d_battleData->getDateAndTime();
        date_time.date(Greenius_System::Date(date_time.date().month(), val, date_time.date().year() ) );
        d_battleData->setDateAndTime(date_time);
        return;
      }
      case IDC_EDIT_YEAR :
      {
        int val = GetDlgItemInt(DateTimeDialog->hwnd, IDC_EDIT_YEAR, NULL, TRUE);
        BattleMeasure::BattleTime date_time = d_battleData->getDateAndTime();
        date_time.date(Greenius_System::Date(date_time.date().month(), date_time.date().day(), val ) );
        d_battleData->setDateAndTime(date_time);
        return;
      }

      case IDC_EDIT_HOURS :
      {
        int val = GetDlgItemInt(DateTimeDialog->hwnd, IDC_EDIT_HOURS, NULL, TRUE);
        BattleMeasure::BattleTime date_time = d_battleData->getDateAndTime();
        date_time.time(Greenius_System::Time(val, date_time.time().minutes(), date_time.time().seconds() ) );
        d_battleData->setDateAndTime(date_time);
        return;
      }
      case IDC_EDIT_MINS :
      {
        int val = GetDlgItemInt(DateTimeDialog->hwnd, IDC_EDIT_MINS, NULL, TRUE);
        BattleMeasure::BattleTime date_time = d_battleData->getDateAndTime();
        date_time.time(Greenius_System::Time(date_time.time().hours(), val, date_time.time().seconds() ) );
        d_battleData->setDateAndTime(date_time);
        return;
      }
      case IDC_EDIT_SECS :
      {
        int val = GetDlgItemInt(DateTimeDialog->hwnd, IDC_EDIT_SECS, NULL, TRUE);
        BattleMeasure::BattleTime date_time = d_battleData->getDateAndTime();
        date_time.time(Greenius_System::Time(date_time.time().hours(), date_time.time().minutes(), val ) );
        d_battleData->setDateAndTime(date_time);
        return;
      }

      /*
       * Battle Duration
       */
      case IDC_EDIT_DURATIONDAYS :
      case IDC_EDIT_DURATIONHOURS :
      case IDC_EDIT_DURATIONMINS :
      {

        int days = GetDlgItemInt(DateTimeDialog->hwnd, IDC_EDIT_DURATIONDAYS, NULL, TRUE);
        int hours = GetDlgItemInt(DateTimeDialog->hwnd, IDC_EDIT_DURATIONHOURS, NULL, TRUE);
        int mins = GetDlgItemInt(DateTimeDialog->hwnd, IDC_EDIT_DURATIONMINS, NULL, TRUE);

        d_battleDuration.setTick(BattleMeasure::battleTime((days * 24) + hours,mins,0));
        d_battleData->endGameIn(d_battleDuration.getTick() );
        return;
      }



      case IDC_EDIT_AREAHEIGHT :
      {
        int val = GetDlgItemInt(HeightsDialog->hwnd, IDC_EDIT_AREAHEIGHT, NULL, TRUE);
        heightarea_height = val;
        return;
      }
    }

  }  // end of edit controls notification messages

  if( codeNotify == CBN_SELCHANGE )
  {
    if( id == IDC_COMBO_POINTSSIDE )
    {
      BattleMeasure::HexCord label_hex = LabelsDialog->Hex;
      const BattleMeasure::HexCord size = d_battleData->map()->getSize();
      // make sure its on map still
      if( label_hex.x() < size.x() && label_hex.y() < size.y() )
      {

        BattleTerrainHex& maphex =  d_battleData->map()->get(label_hex);

        int item = SendDlgItemMessage(LabelsDialog->hwnd, IDC_COMBO_POINTSSIDE, CB_GETCURSEL, 0, 0);
        // Neutral (ie applies to both sides)
        if( item == 2 ) maphex.d_victorySide = SIDE_Neutral;
        else maphex.d_victorySide = (Side) item;

        d_mapWind->update(false);
      }
    }
    if( id == IDC_COMBO_MONTH )
    {
      int val = SendDlgItemMessage(DateTimeDialog->hwnd, IDC_COMBO_MONTH, CB_GETCURSEL, 0, 0);
      val += Greenius_System::DateDefinitions::January; // combo is zero-based, whilst month starts as Janurary = 1
      BattleMeasure::BattleTime date_time = d_battleData->getDateAndTime();
      date_time.date(Greenius_System::Date(static_cast<Greenius_System::Date::MonthEnum>(val), date_time.date().day(), date_time.date().year() ) );
      d_battleData->setDateAndTime(date_time);
      return;
    }
    return;
  }


  switch( id )
  {
    /*
     * File menu commands
     */

    case IDM_FILE_OPENMAP :
    {
      OpenFileDialog dial;
      if( dial.ask("Open Battle Map", "Battle Map\0*.map\0\0", MapFilename.c_str(), 0) )
      {
        MapFilename = dial.fileName();
        readMap();
      }
      break;
    }



    case IDM_FILE_SAVEMAP :
    {
      // if not saved yet, go to save as...
      if( MapFilename.empty() )
      {
        id = IDM_FILE_SAVEMAPAS;
        break;
      }
      // otherwise save using existing filename
      else
      {
        writeMap();
      }
      break;
    }

    case IDM_FILE_SAVEMAPAS:
    {
      SaveFileDialog  dial;
      if( dial.ask("Save Battle Map As...", "Battle Map\0*.map\0\0", MapFilename.c_str(), 0) )
      {
        MapFilename =  dial.fileName();
        writeMap();
      }
      break;
    }


    case IDM_OOB_OPEN :
    {
      OpenFileDialog dial;
      if( dial.ask("Open Battle OB", "Battle OB\0*.bob\0\0", OOBFilename.c_str(), 0) )
      {
        OOBFilename = dial.fileName();
        readOOB();
      }

      break;
    }

    case IDM_OOB_SAVE :
    {

      // if not saved yet, go to save as...
      if( OOBFilename.empty() )
      {
        id = IDM_OOB_SAVEAS;
        break;
      }
      // otherwise save using existing filename
      else
      {
        writeOOB();
      }

      break;
    }

    case IDM_OOB_SAVEAS :
    {
      SaveFileDialog dial;
      if( dial.ask("Save Battle OB As...", "Battle OB\0*.bob\0\0", OOBFilename.c_str(), 0) )
      {
        OOBFilename = dial.fileName();
        writeOOB();
      }

      break;
    }

    case IDM_FILE_OPENBATTLE :
    {
      prepareForLoad();
      d_batGame->openBattle();
      afterLoad();
      break;
    }
    case IDM_FILE_SAVEBATTLE :
    {
      prepareForSave();
      d_batGame->saveBattle(false);
      afterSave();
      break;
    }

    case IDM_FILE_SAVEBATTLEAS :
    {
      prepareForSave();
      d_batGame->saveBattle(true);
      afterSave();
      break;
    }

    case IDM_FILE_EXITMAPEDITOR:
    {
      FORWARD_WM_CLOSE(hWnd, SendMessage);
      break;
    }

    /*
     * Edit menu commands
     */

    case IDM_EDIT_UNDO:
    {
      RecallMap();
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_EDIT_TERRAIN:
    {
      if( IsWindowVisible(TerrainDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(TerrainDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_TERRAIN, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(TerrainDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_TERRAIN;
        ModeText = "Terrain Mode";
        CheckMenuItem(main_menu, IDM_EDIT_TERRAIN, MF_CHECKED);
      }
      break;
    }
    case IDM_EDIT_HEIGHTS:
    {
      if( IsWindowVisible(HeightsDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(HeightsDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_HEIGHTS, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(HeightsDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_HEIGHTS;
        ModeText = "Heights Mode";
        CheckMenuItem(main_menu, IDM_EDIT_HEIGHTS, MF_CHECKED);
      }
      break;
    }
    case IDM_EDIT_PATHS:
    {
      if( IsWindowVisible(PathsDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(PathsDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_PATHS, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(PathsDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_PATHS;
        ModeText = "Paths Mode";
        CheckMenuItem(main_menu, IDM_EDIT_PATHS, MF_CHECKED);
      }
      break;
    }
    case IDM_EDIT_SETTLEMENTS:
    {
      if( IsWindowVisible(SettlementsDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(SettlementsDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_SETTLEMENTS, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(SettlementsDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_SETTLEMENTS;
        ModeText = "Settlements Mode";
        CheckMenuItem(main_menu, IDM_EDIT_SETTLEMENTS, MF_CHECKED);
      }
      break;
    }
    case IDM_EDIT_EDGES:
    {
      if( IsWindowVisible(EdgesDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(EdgesDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_EDGES, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(EdgesDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_EDGES;
        ModeText = "Edges Mode";
        CheckMenuItem(main_menu, IDM_EDIT_EDGES, MF_CHECKED);
      }
      break;
    }
    case IDM_EDIT_LABELS:
    {
      if( IsWindowVisible(LabelsDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(LabelsDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_LABELS, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(LabelsDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_LABELS;
        ModeText = "Labels / Victory Points Mode";
        CheckMenuItem(main_menu, IDM_EDIT_LABELS, MF_CHECKED);
      }
      break;
    }
    case IDM_EDIT_ERASE:
    {
      if( IsWindowVisible(EraseDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(EraseDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_ERASE, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(EraseDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_ERASE;
        ModeText = "Erase Mode";
        CheckMenuItem(main_menu, IDM_EDIT_ERASE, MF_CHECKED);
      }
      break;
    }
    case IDM_EDIT_CROPMAP:
    {
      if( IsWindowVisible(CropMapDialog->hwnd) )
      {
        RemoveModeHighlights();
        d_mapWind->getDisplay()->ShowSelection(false);
        ShowWindow(CropMapDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_CROPMAP, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        d_mapWind->getDisplay()->ShowSelection(true);
        ShowWindow(CropMapDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_CROPMAP;
        ModeText = "CropMap Mode";
        CheckMenuItem(main_menu, IDM_EDIT_CROPMAP, MF_CHECKED);
      }
      break;
    }
    case IDM_EDIT_DATETIME :
    {
      if( IsWindowVisible(DateTimeDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(DateTimeDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_DATETIME, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(DateTimeDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_DATETIME;
        ModeText = "Set Date & Time Mode";
        CheckMenuItem(main_menu, IDM_EDIT_DATETIME, MF_CHECKED);
      }
      break;
    }
    case IDM_EDIT_WEATHER :
    {
      if( IsWindowVisible(WeatherDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(WeatherDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_EDIT_WEATHER, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(WeatherDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_WEATHER;
        ModeText = "Weather / Ground Mode";
        CheckMenuItem(main_menu, IDM_EDIT_WEATHER, MF_CHECKED);
      }
      break;
    }
    case IDM_TOOLS_PATHFINDING :
    {
      if( IsWindowVisible(PathfindDialog->hwnd) )
      {
        RemoveModeHighlights();
        ShowWindow(PathfindDialog->hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_TOOLS_PATHFINDING, MF_UNCHECKED);
      }
      else
      {
        RemoveModeHighlights();
        ShowWindow(PathfindDialog->hwnd, SW_SHOW);
        EditorMode = DIALOG_PATHFIND;
        ModeText = "Pathfinding mode";
        CheckMenuItem(main_menu, IDM_TOOLS_PATHFINDING, MF_CHECKED);
      }
      break;
    }

    /*
     * Misc Tools Commands
     */
    case IDM_TOOLS_CONVERTSCRIPT:
      d_batGame->convertAIScriptToBinary();
      break;

    case IDM_MAP_RANDOM:
    {
      // delete OB & other data
      BattleCreate::deleteRandomBattle(d_battleData);
      // clear out the crap
      d_battleData->map()->clear();
      d_battleData->buildingList()->clear();

      BattleInfo battleinfo;
      battleinfo.terrain( static_cast<BattleInfo::BattleTerrainEnum>(random(BattleInfo::Open,BattleInfo::BattleTerrainEnum_HowMany)) );
      battleinfo.weather( static_cast<BattleInfo::BattleWeatherEnum>(random(BattleInfo::SunnyDry,BattleInfo::BattleWeatherEnum_HowMany)) );
      battleinfo.population( static_cast<BattleInfo::BattlePopulationEnum>(random(BattleInfo::Sparse,BattleInfo::BattlePopulationEnum_HowMany)) );
      BattleCreate::createRandomBattle(d_battleData, battleinfo);

      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_MAP_OPEN:
    case IDM_MAP_ROUGH:
    case IDM_MAP_MOUNTAINPASS:
    case IDM_MAP_HILLY:
    case IDM_MAP_WOODED:
    case IDM_MAP_WOODEDHILLY:
    case IDM_MAP_MARSH:
    case IDM_OPENMAJORRIVER:
    case IDM_WOODEDMAJORRIVER:
    case IDM_ROUGHMAJORRIVER:
    {

      // delete OB & other data
      BattleCreate::deleteRandomBattle(d_battleData);
      // clear all hexes
      d_battleData->map()->clear();
      d_battleData->buildingList()->clear();

      int type = id - IDM_MAP_OPEN;

      BattleInfo battleinfo;
      battleinfo.terrain( static_cast<BattleInfo::BattleTerrainEnum>(type) );
      battleinfo.weather( static_cast<BattleInfo::BattleWeatherEnum>(random(BattleInfo::SunnyDry,BattleInfo::BattleWeatherEnum_HowMany)) );
      battleinfo.population( static_cast<BattleInfo::BattlePopulationEnum>(random(BattleInfo::Sparse,BattleInfo::BattlePopulationEnum_HowMany)) );
      BattleCreate::createRandomBattle(d_battleData, battleinfo);

      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_TOOLS_CLEARTERRAIN :
    {
      // clear all hexes
      d_battleData->map()->clear();
      // set all hexes to primary terrain type
      BattleCreate::ClearWholeMap(d_battleData->map(), PrimaryTerrain);
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_TOOLS_CLEARBUILDINGS :
    {
      // clear all buildings
      d_battleData->buildingList()->clear();
      d_locator->update(false);
      d_mapWind->update(false);
      break;
    }

    case IDM_TOOLS_CLEARPATHS :
    {
      // clear all paths
      BattleCreate::ClearAllPaths(d_battleData->map() );
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_TOOLS_CLEARCOASTS :
    {
      // clear all paths
      BattleCreate::ClearCoasts(d_battleData->map() );
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_TOOLS_CLEAREDGES :
    {
      // clear all edges (walls / hedges, etc.)
      BattleCreate::ClearAllEdges(d_battleData->map() );
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_TOOLS_ZEROHEIGHTS :
    {
      BattleCreate::SetAllHeights(d_battleData->map(), 0);
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_TOOLS_MAKECOASTS :
    {
      BattleCreate::makeCoasts(d_battleData->map(), d_battleData->terrainTable() );
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_TOOLS_FIXDODGYPATHS :
    {
      BattleCreate::FixDodgyPaths(d_battleData->map() );
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      break;
    }

    case IDM_TOOLS_REMOVEALLHIGHLIGHTS :
    {
      d_battleData->map()->removeAllHighlights();
      d_locator->update(FALSE);
      d_mapWind->update(FALSE);
      break;
    }


    /*
     * Add 8-hex border to map & adjust all units & reinforcements, etc - for historical battles
     */
    case IDM_TOOLS_ADDBORDER :
    {
      AddBorder();
      break;
    }

    /*
     * Dump OB out to log file - for Ben
     */
#ifdef DEBUG
    case IDM_TOOLS_DUMPOB :
    {

      obLog.printf("BattleOB Dump");
      obLog.printf("");
      obLog.printf("");

      TBattleUnitIter<BUIV_All> s0_iter(d_battleData->ob(), Side(0));
      obLog.printf("Side 0");
      obLog.printf("");
      obLog.printf("");

      while( !s0_iter.isFinished() )
      {

        BattleCP * cp = s0_iter.cp();

        // add corps or unattached XX's
        if( (cp->getRank().sameRank(Rank_Corps)) ||
            (cp->getRank().sameRank(Rank_Division) && (cp->parent() == NoBattleCP || cp->parent()->getRank().isHigher(Rank_Corps))) )
        {

          char string[1024];
          sprintf(string, "Unit \"%s\"",
                  cp->getName()
            );
          obLog.printf(string);

          sprintf(string, "Leader \"%s\"",
                  cp->leader()->getName()
            );
          obLog.printf(string);

          obLog.printf("");
        }

        s0_iter.next();
      }

      TBattleUnitIter<BUIV_All> s1_iter(d_battleData->ob(), Side(1));
      obLog.printf("");
      obLog.printf("");
      obLog.printf("Side 1");
      obLog.printf("");
      obLog.printf("");

      while( !s1_iter.isFinished() )
      {

        BattleCP * cp = s1_iter.cp();

        // add corps or unattached XX's
        if( (cp->getRank().sameRank(Rank_Corps)) ||
            (cp->getRank().sameRank(Rank_Division) && (cp->parent() == NoBattleCP || cp->parent()->getRank().isHigher(Rank_Corps))) )
        {

          char string[1024];
          sprintf(string, "Unit \"%s\"",
                  cp->getName()
            );
          obLog.printf(string);

          sprintf(string, "Leader \"%s\"",
                  cp->leader()->getName()
            );
          obLog.printf(string);

          obLog.printf("");
        }

        s1_iter.next();
      }

      obLog.printf("END");
      return;
    }
#endif


    case IDM_OOB_EDIT :
    {
      if( d_obDialog->isVisible() )
      {
        d_obDialog->hide();
        if( unit_page_status == TRUE ) ShowWindow(unit_hwnd, SW_HIDE);
        if( leader_page_status == TRUE ) ShowWindow(leader_hwnd, SW_HIDE);
        CheckMenuItem(main_menu, IDM_OOB_EDIT, MF_UNCHECKED);
      }
      else
      {
        d_obDialog->show();
        if( unit_page_status == TRUE )
        {
          ShowWindow(unit_hwnd, SW_SHOW);
          UpdateWindow(unit_hwnd);
        }
        if( leader_page_status == TRUE )
        {
          ShowWindow(leader_hwnd, SW_SHOW);
          UpdateWindow(leader_hwnd);
        }
        CheckMenuItem(main_menu, IDM_OOB_EDIT, MF_CHECKED);
      }
      return;
    }

    case IDM_OOB_DEPLOY :
    {

      d_battleData->hexMap()->clear(d_battleData->map()->getSize());

      B_Logic::deployUnits(d_battleData);
      BatUnitUtils::InitUnits(d_battleData);

      d_locator->update(false);
      d_mapWind->update(false);
      return;
    }



    case IDC_BUTTON_DOIT :
    {

      PathFinder pathfinder(d_battleData);
      HexList * hexlist = new HexList;

      int path_flags = 0;
      if( pathfind_avoidprohibited ) path_flags |= PATHFLAG_AVOIDPROHIBITED;
      if( pathfind_useroads ) path_flags |= PATHFLAG_USEROADS;
      if( pathfind_terraincost ) path_flags |= PATHFLAG_TERRAINCOST;
      if( pathfind_uselevelground )
      {
        path_flags |= PATHFLAG_AVOIDUPHILL;
        path_flags |= PATHFLAG_AVOIDDOWNHILL;
      }

      if( m_lpSelectedUnit ) pathfinder.FindPath(m_lpSelectedUnit, PathFindStartHex, PathFindEndHex, path_flags, NULL, hexlist);

      HexItem * tmp_item = hexlist->first();
      while( tmp_item )
      {
        d_battleData->map()->highlightHex(tmp_item->d_hex,0 | HIGHLIGHT_SET);
        tmp_item = hexlist->next();
      }

      d_mapWind->update(false);
      delete(hexlist);
      return;
    }

    /*
     * Dialog Controls
     */

    case IDC_BUTTON_CREATE :
    {
      // make sure start & end aren't the same
      if( PathStartHex != PathEndHex )
      {
        // set up pathfinder
        PathFinder pathfinder(d_battleData);
        SList<PathPoint> * Path;
        Path = new SList<PathPoint>;

        switch( PathsMode )
        {

          case PATHS_ROAD :
          {
            pathfinder.FindPath(PathStartHex, PathEndHex, 0, Path);
            if( PathsSunken ) BattleCreate::ConstructRoadPath(d_battleData->map(), d_battleData->terrainTable(), Path, LP_SunkenRoad);
            else BattleCreate::ConstructRoadPath(d_battleData->map(), d_battleData->terrainTable(), Path, LP_Road);
            BattleCreate::ReplaceBridgesWithBuildings(d_battleData->map(), d_battleData->terrainTable(), d_battleData->buildingTable(), d_battleData->buildingList());
            d_locator->update(TRUE);
            d_mapWind->update(TRUE);
            break;
          }
          case PATHS_RIVER :
          {
            pathfinder.FindPath(PathStartHex, PathEndHex, 0, Path);
            if( PathsSunken ) BattleCreate::ConstructRiverPath(d_battleData->map(), d_battleData->terrainTable(), Path, LP_SunkenRiver);
            else BattleCreate::ConstructRiverPath(d_battleData->map(), d_battleData->terrainTable(), Path, LP_River);
            BattleCreate::ReplaceBridgesWithBuildings(d_battleData->map(), d_battleData->terrainTable(), d_battleData->buildingTable(), d_battleData->buildingList());
            d_locator->update(TRUE);
            d_mapWind->update(TRUE);
            break;
          }
          case PATHS_TRACK :
          {
            pathfinder.FindPath(PathStartHex, PathEndHex, 0, Path);
            if( PathsSunken ) BattleCreate::ConstructRoadPath(d_battleData->map(), d_battleData->terrainTable(), Path, LP_SunkenTrack);
            else BattleCreate::ConstructRoadPath(d_battleData->map(), d_battleData->terrainTable(), Path, LP_Track);
            BattleCreate::ReplaceBridgesWithBuildings(d_battleData->map(), d_battleData->terrainTable(), d_battleData->buildingTable(), d_battleData->buildingList());
            d_locator->update(TRUE);
            d_mapWind->update(TRUE);
            break;
          }
          case PATHS_STREAM :
          {
            pathfinder.FindPath(PathStartHex, PathEndHex, 0, Path);
            if( PathsSunken ) BattleCreate::ConstructRiverPath(d_battleData->map(), d_battleData->terrainTable(), Path, LP_SunkenStream);
            else BattleCreate::ConstructRiverPath(d_battleData->map(), d_battleData->terrainTable(), Path, LP_Stream);
            BattleCreate::ReplaceBridgesWithBuildings(d_battleData->map(), d_battleData->terrainTable(), d_battleData->buildingTable(), d_battleData->buildingList());
            d_locator->update(TRUE);
            d_mapWind->update(TRUE);
            break;
          }

        } // end of switch on path mode

        delete(Path);
        break;
      }
      return;
    } // end of Path Creation


    // crop map to selection area
    case IDC_BUTTON_CROP :
    {
      // crop the map
      CropMap();
      // stick the units back on
      //                    B_Logic::deployUnitsFromSave(d_battleData);
      return;
    }


    case IDC_BUTTON_SETAREAHEIGHT :
    {
      SetAreaHeight();
      break;
    }




    /*
      Locator map commands
    */

    case IDM_BAT_ZOOMIN:
      startMapZoom();
      break;
    case IDM_BAT_ZOOMOVERVIEW:
      setMapMode(BattleMapInfo::Strategic);
      break;
    case IDM_BAT_ZOOMDETAIL:
      setMapMode(BattleMapInfo::OneMile);
      break;
    case IDM_BAT_ZOOMTWOMILE:
      setMapMode(BattleMapInfo::TwoMile);
      break;
    case IDM_BAT_ZOOMFOURMILE:
      setMapMode(BattleMapInfo::FourMile);
      break;
      /*
       * Hex outline & other debug commands
       */

    case IDM_TOOLS_HEXOUTLINE:
      d_mapWind->toggleHexOutline();
      checkMenu(IDM_BAT_HEXOUTLINE, d_mapWind->hexOutline());
      break;
#ifdef DEBUG
    case IDM_BAT_HEXDEBUG:
      d_mapWind->toggleHexDebug();
      checkMenu(IDM_BAT_HEXDEBUG, d_mapWind->hexDebug());
      break;
#endif

      /*
       * Miscellaneous commands, probably a good idea to leave in !
       */
    case IDM_BAT_PLAYERSETTINGS:
    {
      break;
    }
    case IDM_BAT_GAMEOPTIONS:
    {
      break;
    }
    case IDM_BAT_TINYMAPWINDOW:
    {
      break;
    }

    case IDM_BAT_CLOCKWINDOW:
    {
      break;
    }
    case IDM_BAT_MESSAGEWINDOW:
    {
      break;
    }

    case IDM_BAT_DEBUG:
    {
      break;
    }

    /*
     * If any commands slipped through & we're debugging, then report it
     */

#ifdef DEBUG
    default:
    {
      bwLog.printf(" ╬Unrecognised Command");
      break;
    }
#endif
  }
}




void BattleWindowsImp::onSize(HWND hwnd, UINT state, int cx, int cy)
{
  positionWindows();
}

void BattleWindowsImp::onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo)
{
  lpMinMaxInfo->ptMinTrackSize.x = 640;
  lpMinMaxInfo->ptMinTrackSize.y = 480;
}

LRESULT BattleWindowsImp::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
  switch( lpNMHDR->code )
  {
    case TTN_NEEDTEXT:                              // Tool Tip needed
    {
      TOOLTIPTEXT* ttt = (TOOLTIPTEXT*) lpNMHDR;

      // Set up default values if menuInfo fails

      ttt->lpszText = MAKEINTRESOURCE(id);
      ttt->hinst = APP::instance();

      // Get the actual Menu Text

      HMENU hMenu = GetMenu(hWnd);
      if( hMenu != NULL )
      {
        MENUITEMINFO mInfo;
        static TCHAR buffer[256];
        mInfo.cbSize = sizeof(MENUITEMINFO);
        mInfo.fMask = MIIM_TYPE;
        mInfo.dwTypeData = buffer;
        mInfo.cch = 256;
        if( GetMenuItemInfo(hMenu, id, FALSE, &mInfo) )
        {
          if( mInfo.fType == MFT_STRING )
          {
            ttt->lpszText = buffer;
            ttt->hinst = NULL;
          }
        }
      }

    }
    break;

    case TTN_POP:                           // Tool Tip Gone
      break;
  }

  return 0;
}

void BattleWindowsImp::onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild)
{
}

void BattleWindowsImp::onClose(HWND hWnd)
{

  Boolean doQuit = False;

  Boolean oldPause = GameControl::pause(True);

  doQuit = True;          // d_campWind->onClose();

  if( doQuit )
  {
    DestroyWindow(hWnd);
    PostQuitMessage(0);
  }
  else
  {
    GameControl::pause(oldPause);
  }
}

void BattleWindowsImp::onMenuSelect(HWND hwnd, HMENU hmenu, int item, HMENU hmenuPopup, UINT flags)
{
  if( (hmenuPopup == NULL) && ( ((UWORD)flags) == 0xffff) )
    g_toolTip.clearHint();
  else
    g_toolTip.showHint(APP::instance(), item);
}

BOOL BattleWindowsImp::onHelp(HWND hwnd, LPHELPINFO lparam)
{
  LPHELPINFO lphi = reinterpret_cast<LPHELPINFO>(lparam);

  if ( lphi->iContextType == HELPINFO_WINDOW )   // must be for a control
  {
    //         WinHelp ((HWND)lphi->hItemHandle, "help\\Wg95Help.hlp", HELP_CONTEXT, IDH_Wargamer);
  }
  return TRUE;
}


HMENU BattleWindowsImp::getMenu()
{
  ASSERT(hWnd != NULL);
  return GetMenu(hWnd);
}

void BattleWindowsImp::checkMenu(int id, BOOL flag)
{
  HMENU menu = getMenu();
  ASSERT(menu != NULL);

  if( menu )
    CheckMenuItem(menu, id, MF_BYCOMMAND | (flag ? MF_CHECKED : MF_UNCHECKED));
}

void BattleWindowsImp::enableMenu(int id, BOOL flag)
{
  ASSERT((flag == TRUE) || (flag == FALSE));

  HMENU menu = getMenu();
  ASSERT(menu != NULL);

  if( menu )
    EnableMenuItem(menu, id, MF_BYCOMMAND | (flag ? MF_ENABLED : MF_GRAYED));
}





void BattleWindowsImp::positionWindows()
{
  if( getHWND() )
  {
    const int nWindows = 10;

    {
      DeferWindowPosition dwp(nWindows);

      RECT rMain;
      GetClientRect(getHWND(), &rMain);



      // Tiny Map Window goes above side bar

      RECT rSide;
      rSide.left = rMain.right;
      rSide.top = rMain.top;
      rSide.right = rMain.right;
      rSide.bottom = rMain.bottom;

      if( d_sideBar() )
      {
        const int WidthSideBar = 192;
        int left = rSide.right - WidthSideBar;
        if( left < rSide.left ) rSide.left = left;
      }

      rMain.right = rSide.left;


      // sidebar

      if( d_sideBar() )
      {
        dwp.setPos(d_sideBar->getHWND(), HWND_TOP,
                   rSide.left, rSide.top, rSide.right-rSide.left, rSide.bottom-rSide.top,
                   0);
      }

      // mapwind

      if( d_mapWind )
      {
        dwp.setPos(d_mapWind->getHWND(), HWND_TOP,
                   rMain.left, rMain.top, rMain.right-rMain.left, rMain.bottom-rMain.top,
                   0);
      }

    }       // dwp gets destructed and EndDeferPosition is called

    ShowWindow(getHWND(), SW_SHOW);
  }
}

void BattleWindowsImp::setZoomButtons()
{
  if( d_mapWind )
  {
    BattleMapInfo::Mode mode = d_mapWind->mode();

    if( d_locator )
    {
      d_locator->setZoomButtons(mode);
    }

    checkMenu(IDM_BAT_ZOOMOVERVIEW, mode == BattleMapInfo::Strategic);
    checkMenu(IDM_BAT_ZOOMFOURMILE, mode == BattleMapInfo::FourMile);
    checkMenu(IDM_BAT_ZOOMTWOMILE, mode == BattleMapInfo::TwoMile);
    checkMenu(IDM_BAT_ZOOMDETAIL, mode == BattleMapInfo::OneMile);
  }
}

void BattleWindowsImp::setMapMode(BattleMapInfo::Mode mode)
{
  if( d_mapWind )
  {
    d_mapWind->mode(mode);
  }
  setZoomButtons();
}

void BattleWindowsImp::startMapZoom()
{
  if( d_mapWind )
  {
    d_mapWind->startZoom();
  }
  setZoomButtons();
}

/*
 * Implementation of BattleWindowsInterface function
 */

bool BattleWindowsImp::setMapLocation(const BattleLocation& l)
{
  bool flag = false;

  if( d_mapWind )
  {
    flag = d_mapWind->setLocation(l);
  }
  return flag;
}

void BattleWindowsImp::mapAreaChanged(const BattleArea& area)
{
  if( d_locator )
    d_locator->setLocation(area);
}

void BattleWindowsImp::mapZoomChanged()
{
  setZoomButtons();
}

/*
 * sent by mapwind when Button is pressed
 */


void BattleWindowsImp::onButtonDown(const BattleMapSelect& info)
{

  // get hex
  BattleMeasure::HexCord hex = d_battleData->getHex(info.location(), BattleData::Mid);

  switch( EditorMode )
  {

    /*
     * TERRAIN
     */
    case DIALOG_TERRAIN :
    {
      // create primary terrain
      if( info.button() == BattleMapSelect::LeftButton )
      {
        // single hex
        StoreMap();
        if( TerrainHexMode == HEX_SINGLE ) BattleCreate::CreateTerrainHex(d_battleData->map(), hex, PrimaryTerrain);
        if( TerrainHexMode == HEX_GROUP ) BattleCreate::EditorCreateTerrainArea(d_battleData->map(), hex, PrimaryTerrain, TerrainAreaSize, 5, 1);
        if( TerrainHexMode == HEX_CLUSTER ) BattleCreate::EditorCreateTerrainArea(d_battleData->map(), hex, PrimaryTerrain, TerrainAreaSize, 5, 0);
      }


      // create secondary terrain
      if( info.button() == BattleMapSelect::RightButton )
      {
        StoreMap();
        if( TerrainHexMode == HEX_SINGLE ) BattleCreate::CreateTerrainHex(d_battleData->map(), hex, SecondaryTerrain);
        if( TerrainHexMode == HEX_GROUP ) BattleCreate::EditorCreateTerrainArea(d_battleData->map(), hex, SecondaryTerrain, TerrainAreaSize, 5, 1);
        if( TerrainHexMode == HEX_CLUSTER ) BattleCreate::EditorCreateTerrainArea(d_battleData->map(), hex, SecondaryTerrain, TerrainAreaSize, 5, 0);
      }
      // redraw maps
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      return;
    }

    /*
     * HEIGHTS
     */

    case DIALOG_HEIGHTS :
    {
      // raise heights
      if( info.button() == BattleMapSelect::LeftButton )
      {
        if( HeightsHexMode == HEX_AREA )
        {
          heightarea_center.x(hex.x() );
          heightarea_center.y(hex.y() );
          SetHeightAreaSelection();
          d_mapWind->update(false);
          return;
        }
        StoreMap();
        // single hex
        if( HeightsHexMode == HEX_SINGLE ) BattleCreate::AlterHexHeight(d_battleData->map(), hex, 4);
        // group of hexes
        if( HeightsHexMode == HEX_GROUP ) BattleCreate::AlterHexAreaHeight(d_battleData->map(), hex,5, 0, 5);
        // cluster of hexes
        if( HeightsHexMode == HEX_CLUSTER ) BattleCreate::AlterHexClumpHeight(d_battleData->map(),hex, 5, 5, 5);
      }
      // lower heights
      if( info.button() == BattleMapSelect::RightButton )
      {
        if( HeightsHexMode == HEX_AREA )
        {
          heightarea_size.x( abs(heightarea_center.x() - hex.x() ) );
          heightarea_size.y( abs(heightarea_center.y() - hex.y() ) );
          SetHeightAreaSelection();
          d_mapWind->update(false);
          return;
        }
        StoreMap();
        // single hex
        if( HeightsHexMode == HEX_SINGLE ) BattleCreate::AlterHexHeight(d_battleData->map(), hex, -4);
        // group of hexes
        if( HeightsHexMode == HEX_GROUP ) BattleCreate::AlterHexAreaHeight(d_battleData->map(), hex,5, 0, -5);
        // cluster of hexes
        if( HeightsHexMode == HEX_CLUSTER ) BattleCreate::AlterHexClumpHeight(d_battleData->map(),hex, 5, 5, -5);
      }
      // redraw maps
      d_locator->update(TRUE);
      d_mapWind->update(TRUE);
      return;
    }

    /*
     *PATHS
     */

    case DIALOG_PATHS :
    {
      // select path start hex
      if( info.button() == BattleMapSelect::LeftButton )
      {
        StoreMap();
        if( d_battleData->map()->isHighlighted(PathStartHex) ) d_battleData->map()->unhighlightHex(PathStartHex);
        PathStartHex = hex;
        d_battleData->map()->highlightHex(hex,0 | HIGHLIGHT_SET);
      }
      // select path start hex
      if( info.button() == BattleMapSelect::RightButton )
      {
        StoreMap();
        if( d_battleData->map()->isHighlighted(PathEndHex) ) d_battleData->map()->unhighlightHex(PathEndHex);
        PathEndHex = hex;
        d_battleData->map()->highlightHex(hex,1 | HIGHLIGHT_SET);
      }
      // redraw maps
      d_mapWind->update(TRUE);
      return;
    }

    /*
     * SETTLEMENTS
     */
    case DIALOG_SETTLEMENTS :
    {
      // create settlement(s)
      if( info.button() == BattleMapSelect::LeftButton )
      {

        // create a single settlement cluster
        if( SettlementsHexMode == HEX_SINGLE )
        {

          StoreMap();

          BattleBuildings::BuildingListEntry entry;
          entry.index(SettlementsType);
          entry.frame(0);
          entry.timer(0);
          entry.effectCount(0);

          switch( SettlementsStatus )
          {
            case SETTLEMENT_NORMAL :
            {
              entry.status(BattleBuildings::BUILDING_NORMAL);
              break;
            }
            case SETTLEMENT_BURNING :
            {
              entry.status(BattleBuildings::BUILDING_BURNING);
              break;
            }
            case SETTLEMENT_SMOKING :
            {
              entry.status(BattleBuildings::BUILDING_SMOULDERING);
              break;
            }
            case SETTLEMENT_DESTROYED :
            {
              entry.status(BattleBuildings::BUILDING_DESTROYED);
              break;
            }
          }

          BattleCreate::CreateSingleSettlement(d_battleData->buildingTable(), d_battleData->buildingList(), hex, entry);

          // redraw maps
          d_locator->update(false);
          d_mapWind->update(false);
          break;
        }

        // create a cluster of settlements
        if( SettlementsHexMode == HEX_CLUSTER )
        {
          break;
        }

      } // end of left mouse in settlement creation

      // delete settlement
      if( info.button() == BattleMapSelect::RightButton )
      {
        StoreMap();
        if( SettlementsHexMode == HEX_SINGLE )
        {
          d_battleData->buildingList()->remove(hex);
          // redraw maps
          d_locator->update(false);
          d_mapWind->update(false);
          break;
        }
        if( SettlementsHexMode == HEX_CLUSTER )
        {
          d_battleData->buildingList()->remove(hex);
          // redraw maps
          d_locator->update(false);
          d_mapWind->update(false);
          break;
        }
      }

      return;
    }

    /*
     * HEDGES
     */
    case DIALOG_EDGES :
    {

      if( info.button() == BattleMapSelect::LeftButton )
      {

        // create walls in hex
        if( EdgesType == EDGE_WALL )
        {
          StoreMap();
          BattleCreate::SetEdges(d_battleData->map(), hex, EdgeBits);
        }

        // create walls in hex
        if( EdgesType == EDGE_HEDGE )
        {
          StoreMap();
          BattleCreate::SetEdges(d_battleData->map(), hex, EdgeBits);
        }

        // create walls in hex
        if( EdgesType == EDGE_FENCE )
        {
          StoreMap();
          BattleCreate::SetEdges(d_battleData->map(), hex, EdgeBits);
        }

        // redraw maps
        d_locator->update(false);
        d_mapWind->update(false);
        return;
      }


      if( info.button() == BattleMapSelect::RightButton ) {

      }

    } // end of hedges

    /*
      LABELS / VICTORY POINTS
    */
    case DIALOG_LABELS :
    {

      if( info.button() == BattleMapSelect::LeftButton )
      {
        d_battleData->map()->unhighlightHex(label_hex);
        label_hex = hex;
        BattleTerrainHex& maphex =  d_battleData->map()->get(hex);
        LabelsDialog->GetHexData(hex, maphex);
        d_battleData->map()->highlightHex(label_hex, 0 | HIGHLIGHT_SET);
      }

      if( info.button() == BattleMapSelect::RightButton ) {
      }

      return;
    } // end of labels / victory points


    case DIALOG_ERASE :
    {
      if( info.button() == BattleMapSelect::LeftButton )
      {
        BattleTerrainHex& maphex =  d_battleData->map()->get(hex);
        // erase terrain
        if( erase_terrain ) maphex.d_terrainType = 1;
        if( erase_heights ) maphex.d_height = 0;
        if( erase_paths1 )
        {
          maphex.d_path1.d_type = 0;
          maphex.d_path1.d_edges = 0;
        }
        if( erase_paths2 )
        {
          maphex.d_path2.d_type = 0;
          maphex.d_path2.d_edges = 0;
        }
        if( erase_edges ) maphex.d_edge.d_edges = 0;
        if( erase_buildings ) d_battleData->buildingList()->remove(hex);
        if( erase_labels )
        {
          if( maphex.d_label ) delete maphex.d_label;
          maphex.d_label = 0;
          maphex.d_victorySide = 0;
          maphex.d_victoryPoints = 0;
        }
        // redraw maps
        d_locator->update(TRUE);
        d_mapWind->update(TRUE);
      }
      return;
    }


    case DIALOG_CROPMAP :
    {
      if( info.button() == BattleMapSelect::LeftButton )
      {
        CropSelectCenter.x(hex.x() );
        CropSelectCenter.y(hex.y() );
        SetCropSelection();
        d_mapWind->update(false);
        break;
      }
      return;
    }

    case DIALOG_PATHFIND :
    {
      // select start hex
      if( info.button() == BattleMapSelect::LeftButton )
      {
        if( d_battleData->map()->isHighlighted(PathFindStartHex) ) d_battleData->map()->unhighlightHex(PathFindStartHex);
        PathFindStartHex = hex;
        d_battleData->map()->highlightHex(hex,0 | HIGHLIGHT_SET);
        d_mapWind->update(false);
      }
      // select end hex
      if( info.button() == BattleMapSelect::RightButton )
      {
        if( d_battleData->map()->isHighlighted(PathFindEndHex) ) d_battleData->map()->unhighlightHex(PathFindEndHex);
        PathFindEndHex = hex;
        d_battleData->map()->highlightHex(hex,0 | HIGHLIGHT_SET);
        d_mapWind->update(false);
      }
      return;
    }

    case DIALOG_OOB :
    {

      if( info.button() == BattleMapSelect::LeftButton )
      {
        OOB_MouseSelectHex = hex;
        OOB_MouseSelected = true;
        return;
      }

    }


  }
}




// sent by mapwind when Button is pressed
void BattleWindowsImp::onLButtonDown(const BattleMapSelect& info)
{

  if( EditorMode != DIALOG_OOB )
  {
    onButtonDown(info);
    return;
  }


  /*
    Check if mouse if over a unitba
  */

  BattleMeasure::HexCord hex = d_battleData->getHex(info.location(), BattleData::Mid);
  BattleHexMap::iterator lower_iter;
  BattleHexMap::iterator upper_iter;

  /*
    If mouse not over unit
  */

  if( ! d_battleData->hexMap()->find(hex, lower_iter, upper_iter) )
  {

    // if we already have a unit selected
    if( m_lpSelectedUnit )
    {

      // turn of highlights
      if( m_bUnitHighlighted )
      {
        HighlightUnits(false);
        m_bUnitHighlighted = false;
      }

      // reset dragging flag
      m_bDragging = false;

      // deselect item in treeview
      //            m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

      // clear selected unit
      m_lpSelectedUnit = NULL;

      // clear selected unit's hex
      m_SelectedUnitHex = BattleMeasure::HexCord(0,0);

      // clear deployment hex
      m_DeploymentHex = BattleMeasure::HexCord(0,0);

      // clear deployment hex list
      m_DeploymentHexList.reset();

      redrawMap(false);

      return;
    }
  }


  /*
    Mouse is over a unit
  */

  else
  {

    // if we already have a selected unit, deselect it
    if( m_lpSelectedUnit )
    {

      // turn of highlights
      if( m_bUnitHighlighted )
      {
        HighlightUnits(false);
        m_bUnitHighlighted = false;
      }

      // reset dragging flag
      m_bDragging = false;

      // deselect item in treeview
      //            m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

      // clear selected unit
      m_lpSelectedUnit = NULL;

      // clear selected unit's hex
      m_SelectedUnitHex = BattleMeasure::HexCord(0,0);
    }

    BattleUnit * unit = d_battleData->hexMap()->unit(lower_iter);

    // ensure we are refenencing a CP
    BattleCP * cp = dynamic_cast<BattleCP *>(unit);
    BattleSP * sp;
    if( ! cp )
    {
      sp = dynamic_cast<BattleSP *>(unit);
      ASSERT(sp);
      cp = sp->parent();
    }
    ASSERT(cp);

    // select this unit
    m_lpSelectedUnit = cp;

    // set selected unit's hex
    m_SelectedUnitHex = hex;

    // set dragging flag
    m_bDragging = true;

    // set deployment hex
    m_DeploymentHex = hex;

    // set up initial dpeloyment hex list
    m_DeploymentHexList.reset();

    bool result = PlayerDeploy::playerDeploy(
                                             d_battleData,
                                                 m_lpSelectedUnit,
                                                 hex,
                                                 &m_DeploymentHexList,
                                                 PlayerDeploy::FILLHEXLIST
      );

    // highlight units
    HighlightUnits(true);
    m_bUnitHighlighted = true;

    // select item in treeview
    //        m_lpSideBar->SelectUnit(m_lpSelectedUnit);

    redrawMap(false);

    d_obDialog->SetSelection(cp);

  }

}


// sent by mapwind when Button is pressed
void BattleWindowsImp::onLButtonUp(const BattleMapSelect& info)
{

  if( EditorMode != DIALOG_OOB )
  {
    onButtonDown(info);
    return;
  }

  /*
    Mouse released during a drag operation
  */
  if( m_bDragging )
  {

    BattleMeasure::HexCord hex = d_battleData->getHex(info.location(), BattleData::Mid);

    // if target hex is different from source hex
    if( hex != m_SelectedUnitHex )
    {

      // remove highlighting
      if( m_bUnitHighlighted )
      {
        HighlightUnits(false);
        m_bUnitHighlighted = false;
      }

      // test deployment to new location
      bool result;
      HexList hexlist;

      result = PlayerDeploy::playerDeploy(
                                          d_battleData,
                                              m_lpSelectedUnit,
                                              hex,
                                              &hexlist,
                                              PlayerDeploy::FILLHEXLIST
        );

      // if deployment was successful
      if( result )
      {

        // remove units from map
        if( m_lpSelectedUnit->sp() ) RemoveFromMap(m_lpSelectedUnit, true);
        else RemoveFromMap(m_lpSelectedUnit, false);

        // deploy at destination
        PlayerDeploy::playerDeploy(
                                   d_battleData,
                                       m_lpSelectedUnit,
                                       hex,
                                       NULL,
                                       PlayerDeploy::INITIALIZE // | PlayerDeploy::REORGANIZE
          );


        // set hex for this unit
        m_lpSelectedUnit->hex(hex);

        // bodge code to fix independent CP deployment bug
        if( m_lpSelectedUnit->getRank().isHigher(Rank_Division) )
        {
          BattleHexMap::iterator lower_iter;
          BattleHexMap::iterator upper_iter;
          if( d_battleData->hexMap()->find(m_SelectedUnitHex, lower_iter, upper_iter) )
            d_battleData->hexMap()->remove(lower_iter);
        }

      }

      // deselect item in treeview
      //            m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

      // reset dragging flag
      m_bDragging = false;

      // clear selected unit's hex
      m_SelectedUnitHex = BattleMeasure::HexCord(0,0);

      // deselect unit
      m_lpSelectedUnit = NULL;

      // reset deployment hex highlights
      SListIterR<HexItem> iter(&m_DeploymentHexList);
      while( ++iter )
      {
        d_battleData->map()->unhighlightHex(iter.current()->d_hex);
      }

      // clear deployment hex
      m_DeploymentHex = BattleMeasure::HexCord(0,0);

      // clear deployment hex list
      m_DeploymentHexList.reset();

      redrawMap(false);
    }
  }

  // reset dragging flag regardless, when left button goes up
  m_bDragging = false;


}


// sent by mapwind when Mouse has moved
void BattleWindowsImp::onMove(const BattleMapSelect& info)
{

  if( d_sideBar() ) d_sideBar->setHex(d_battleData->getHex(info.location()));

  if( EditorMode != DIALOG_OOB ) return;

  BattleMeasure::HexCord hex = d_battleData->getHex(info.location(), BattleData::Mid);

  /*
    If we are currently dragging a unit
  */
  if( m_bDragging )
  {

    // if hex is different from previous one
    if( hex != m_DeploymentHex )
    {

      // reset hex highlights
      SListIterR<HexItem> iter(&m_DeploymentHexList);
      while( ++iter )
      {
        d_battleData->map()->unhighlightHex(iter.current()->d_hex);
        if( ! m_lpSelectedUnit->sp() )
        {
          d_battleData->map()->unhighlightHex(m_DeploymentHex);
        }
      }

      // clear hex list
      m_DeploymentHexList.reset();

      // get deployment hexlist

      bool result = PlayerDeploy::playerDeploy(
                                               d_battleData,
                                                   m_lpSelectedUnit,
                                                   hex,
                                                   &m_DeploymentHexList,
                                                   PlayerDeploy::FILLHEXLIST
        );

      // bodge code to fix independent CP deployment bug
      if( m_lpSelectedUnit->getRank().isHigher(Rank_Division) )
      {
        BattleHexMap::iterator lower_iter;
        BattleHexMap::iterator upper_iter;
        if( d_battleData->hexMap()->find(hex, lower_iter, upper_iter) )
        {
          d_battleData->hexMap()->remove(lower_iter);
          d_battleData->map()->unhighlightHex(hex );
        }
      }
      d_battleData->map()->unhighlightHex(m_lpSelectedUnit->hex() );

      // set hex highlights
      iter = SListIterR<HexItem>(&m_DeploymentHexList);
      while( ++iter )
      {
        d_battleData->map()->highlightHex(iter.current()->d_hex, m_lpSelectedUnit->getSide() | HIGHLIGHT_SET);
      }

      // highlight units, to ensure not reset by mouse movement
      HighlightUnits(true);
      m_bUnitHighlighted = true;

      // set new value form deployment hex
      m_DeploymentHex = hex;

      redrawMap(false);
    }
  }

}






void BattleWindowsImp::StoreMap(void)
{

  BattleMeasure::HexCord MapSize = d_battleData->map()->getSize();
  int width = MapSize.x();
  int height = MapSize.y();


  for( int y=0; y<height; y++ )
  {

    for( int x=0; x<width; x++ )
    {

      BattleMeasure::HexCord hex(x,y);

      BattleTerrainHex & src_hex = d_battleData->map()->get(hex);
      BattleTerrainHex & dest_hex = UndoMap.get(hex);

      dest_hex.d_terrainType = src_hex.d_terrainType;
      dest_hex.d_path1.d_type = src_hex.d_path1.d_type;
      dest_hex.d_path1.d_edges= src_hex.d_path1.d_edges;
      dest_hex.d_path2.d_type = src_hex.d_path2.d_type;
      dest_hex.d_path2.d_edges = src_hex.d_path2.d_edges;
      dest_hex.d_height = src_hex.d_height;
      dest_hex.d_coast = src_hex.d_coast;
      dest_hex.d_edge.d_type = src_hex.d_edge.d_type;
      dest_hex.d_edge.d_edges = src_hex.d_edge.d_edges;

    }
  }

}



void BattleWindowsImp::RecallMap(void)
{

  BattleMeasure::HexCord MapSize = d_battleData->map()->getSize();
  int width = MapSize.x();
  int height = MapSize.y();


  for( int y=0; y<height; y++ )
  {

    for( int x=0; x<width; x++ )
    {

      BattleMeasure::HexCord hex(x,y);

      BattleTerrainHex & src_hex = UndoMap.get(hex);
      BattleTerrainHex & dest_hex = d_battleData->map()->get(hex);

      dest_hex.d_terrainType = src_hex.d_terrainType;
      dest_hex.d_path1.d_type = src_hex.d_path1.d_type;
      dest_hex.d_path1.d_edges= src_hex.d_path1.d_edges;
      dest_hex.d_path2.d_type = src_hex.d_path2.d_type;
      dest_hex.d_path2.d_edges = src_hex.d_path2.d_edges;
      dest_hex.d_height = src_hex.d_height;
      dest_hex.d_coast = src_hex.d_coast;
      dest_hex.d_edge.d_type = src_hex.d_edge.d_type;
      dest_hex.d_edge.d_edges = src_hex.d_edge.d_edges;


    }
  }

}




/*
 * sent by mapwind when Button is released while not dragging
 */

void BattleWindowsImp::onButtonUp(const BattleMapSelect& info)
{

}

/*
 * sent by mapwind when Mouse is moved while button is held
 */

void BattleWindowsImp::onStartDrag(const BattleMapSelect& info)
{

}

/*
 * sent by mapwind when button is released while dragging
 */

void
BattleWindowsImp::onEndDrag(const BattleMapSelect& info)
{

}


/*
 * Battle Time has changed, so get the map to be redrawn
 */

void BattleWindowsImp::timeChanged()
{
  if( d_mapWind )
    d_mapWind->update(false);
  if( d_locator )
    d_locator->update(false);
}











/*

Add a border to the map
Set the playing area to take into account the border
Adjust all units, reinforcements, & other data structures, by offset

*/



void BattleWindowsImp::AddBorder(void)
{

  BattleMap * tmp_map = new BattleMap;

  BattleMeasure::HexCord old_size = d_battleData->map()->getSize();
  BattleMeasure::HexCord new_size;
  new_size.x( old_size.x() + (BATTLEMAP_BUFFERHEXES*2) );
  new_size.y( old_size.y() + (BATTLEMAP_BUFFERHEXES*2) );

  tmp_map->create(new_size);

  /*
    Copy old map into center of a temporary map
  */
  int old_width = old_size.x();
  int old_height = old_size.y();
  int x,y;

  for( y=0; y<old_height; y++ )
  {

    for( x=0; x<old_width; x++ )
    {

      BattleMeasure::HexCord old_hex(x,y);
      BattleMeasure::HexCord new_hex(x+BATTLEMAP_BUFFERHEXES,y+BATTLEMAP_BUFFERHEXES);

      BattleTerrainHex & src_hex = d_battleData->map()->get(old_hex);
      BattleTerrainHex & dest_hex = tmp_map->get(new_hex);

      dest_hex.d_terrainType = src_hex.d_terrainType;
      dest_hex.d_path1.d_type = src_hex.d_path1.d_type;
      dest_hex.d_path1.d_edges= src_hex.d_path1.d_edges;
      dest_hex.d_path2.d_type = src_hex.d_path2.d_type;
      dest_hex.d_path2.d_edges = src_hex.d_path2.d_edges;
      dest_hex.d_height = src_hex.d_height;
      dest_hex.d_coast = src_hex.d_coast;
      dest_hex.d_edge.d_type = src_hex.d_edge.d_type;
      dest_hex.d_edge.d_edges = src_hex.d_edge.d_edges;

      dest_hex.d_highlightFlags = src_hex.d_highlightFlags;
      dest_hex.d_label = src_hex.d_label;
      dest_hex.d_victorySide = src_hex.d_victorySide;
      dest_hex.d_victoryPoints = src_hex.d_victoryPoints;
      dest_hex.d_visibility = src_hex.d_visibility;
      dest_hex.d_lastFullVisUnit = src_hex.d_lastFullVisUnit;
    }
  }

  /*
    Resize old map, & copy temporary map back into it
  */
  int new_width = new_size.x();
  int new_height = new_size.y();

  d_battleData->map()->create(new_size);

  for( y=0; y<new_height; y++ )
  {

    for( x=0; x<new_width; x++ )
    {

      BattleMeasure::HexCord hex(x,y);

      BattleTerrainHex & src_hex = tmp_map->get(hex);
      BattleTerrainHex & dest_hex = d_battleData->map()->get(hex);

      dest_hex.d_terrainType = src_hex.d_terrainType;
      dest_hex.d_path1.d_type = src_hex.d_path1.d_type;
      dest_hex.d_path1.d_edges= src_hex.d_path1.d_edges;
      dest_hex.d_path2.d_type = src_hex.d_path2.d_type;
      dest_hex.d_path2.d_edges = src_hex.d_path2.d_edges;
      dest_hex.d_height = src_hex.d_height;
      dest_hex.d_coast = src_hex.d_coast;
      dest_hex.d_edge.d_type = src_hex.d_edge.d_type;
      dest_hex.d_edge.d_edges = src_hex.d_edge.d_edges;

      dest_hex.d_highlightFlags = src_hex.d_highlightFlags;
      dest_hex.d_label = src_hex.d_label;
      dest_hex.d_victorySide = src_hex.d_victorySide;
      dest_hex.d_victoryPoints = src_hex.d_victoryPoints;
      dest_hex.d_visibility = src_hex.d_visibility;
      dest_hex.d_lastFullVisUnit = src_hex.d_lastFullVisUnit;
    }
  }

  /*
    Adjust all units in OB by (BATTLEMAP_BUFFERHEXES, BATTLEMAP_BUFFERHEXES)
  */

  BattleUnitIter cp_iter(d_battleData->ob());

  while( !cp_iter.isFinished() )
  {

    BattleCP * cp = cp_iter.cp();
    BattleMeasure::HexCord cp_hex = cp->hex();
    cp_hex.x( cp_hex.x() + BATTLEMAP_BUFFERHEXES);
    cp_hex.y( cp_hex.y() + BATTLEMAP_BUFFERHEXES);
    cp->hex(cp_hex);

    BattleSPIter sp_iter(d_battleData->ob(), cp_iter.cp());

    while( !sp_iter.isFinished() )
    {

      BattleSP * sp = sp_iter.sp();
      BattleMeasure::HexCord sp_hex = sp->hex();
      sp_hex.x( sp_hex.x() + BATTLEMAP_BUFFERHEXES);
      sp_hex.y( sp_hex.y() + BATTLEMAP_BUFFERHEXES);
      sp->hex(sp_hex);

      sp_iter.next();
    }

    cp_iter.next();
  }

  /*
    Adjust all units in HexMap
  */

  BattleHexMap * hexmap = d_battleData->hexMap();

  for( y=0; y<new_size.y(); y++ )
  {

    for( x=0; x<new_size.x(); x++ )
    {

      BattleMeasure::HexCord unit_hex(x,y);
      BattleHexMap::iterator lower_iter;
      BattleHexMap::iterator upper_iter;

      if( hexmap->find(unit_hex, lower_iter, upper_iter) )
      {

        BattleUnit * unit = hexmap->unit(lower_iter);
        hexmap->remove(lower_iter);

        BattleCP * cp = dynamic_cast<BattleCP *>(unit);
        BattleSP * sp = dynamic_cast<BattleSP *>(unit);

        /*
          NOTE : the unit->hex() member will already have been changed by the OB adjust routines above
        */
        if( cp )
        {
          hexmap->add(cp);
        }
        else if( sp )
        {
          hexmap->add(sp);
        }
      }
    }
  }

  /*
    Adjust all reinforcement hexes
  */

  B_ReinforceList & r_list = d_battleData->ob()->reinforceList();
  B_ReinforceItem * r_item = r_list.first();

  if( r_item )
  {

    do
    {

      r_item->d_hex.x( r_item->d_hex.x() + BATTLEMAP_BUFFERHEXES);
      r_item->d_hex.y( r_item->d_hex.y() + BATTLEMAP_BUFFERHEXES);

      r_item = r_list.next(r_item);

    } while( r_item );
  }

  /*
    Adjust all buildings in list
    NOTE : buildingList->adjust() SUBTRACTS the given hex value from each building (so use negative to add)
  */

  BattleMeasure::HexCord adjustment(-BATTLEMAP_BUFFERHEXES, -BATTLEMAP_BUFFERHEXES);
  d_battleData->buildingList()->adjust(adjustment);


  /*
    Set playing area
  */
  d_battleData->maximizePlayingArea();
}







/*
  This will set all CP nationalities to that of their leader
  This is to remedy a minor bug with the editor & historical battles
*/

void BattleWindowsImp::SetCPNationalities(void)
{

  TBattleUnitIter<BUIV_All> cp_iter(d_battleData->ob());

  while( !cp_iter.isFinished() )
  {

    BattleCP * cp = cp_iter.cp();
    cp->setNation(cp->leader()->nation());

    cp_iter.next();
  }
}





/*
  This will make sure that Divisional-level CPs ALWAYS have hex coord of (0,0)
  This is to remedy a bug where their hexes were being highlighted in historical battles
*/

void BattleWindowsImp::FixDivisionalCPs(void)
{

  TBattleUnitIter<BUIV_All> cp_iter(d_battleData->ob());

  while( !cp_iter.isFinished() )
  {

    BattleCP * cp = cp_iter.cp();

    // only if division
    if( cp->getRank().sameRank(Rank_Division) )
    {
      cp->hex(BattleMeasure::HexCord(0,0));
    }

    cp_iter.next();
  }
}





static UWORD map_FileVersion = 0x01;

bool BattleWindowsImp::readMap()
{

  prepareForLoad();
  WinFileBinaryChunkReader file(MapFilename.c_str());

  d_battleData->map()->readData(file);

  d_battleData->buildingList()->readData(file, d_battleData->buildingTable() );

  afterLoad();
  return TRUE;
}


bool BattleWindowsImp::writeMap()
{
  prepareForSave();
  WinFileBinaryChunkWriter file(MapFilename.c_str());

  d_battleData->map()->writeData(file);
  BattleMap::writeNameTable();

  d_battleData->buildingList()->writeData(file);

  afterSave();
  return TRUE;

}


bool BattleWindowsImp::readOOB()
{
  prepareForLoad();

  WinFileBinaryChunkReader file(OOBFilename.c_str());

  d_battleData->deleteOB();


  // OrderBattle * new_genericOB = new OrderBattle;
  // new_genericOB->createEmptyOB(2);
  OrderBattle* gob = new OrderBattle();
  gob->readData(file);

  BattleOB * new_oob = new BattleOB(gob);
  d_battleData->ob(new_oob);

  d_battleData->ob()->readData(file);
  d_battleData->setupUnits(true);

  afterLoad();
  return TRUE;
}


bool BattleWindowsImp::writeOOB()
{

  prepareForSave();
  OB_Names::writeFile();
  WinFileBinaryChunkWriter file(OOBFilename.c_str());

  BattleOB* ob =d_battleData->ob();
  OrderBattle* gob = ob->ob();
  gob->writeData(file);
  ob->writeData(file);

  afterSave();
  return true;
}

bool BattleWindowsImp::readData()
{

  WinFileBinaryReader file(BattleFilename.c_str());

  return TRUE;
}


bool BattleWindowsImp::writeData()
{

  WinFileBinaryWriter file(BattleFilename.c_str());

  return TRUE;
}



void BattleWindowsImp::prepareForLoad()
{
  // get rid of dependent dialogs
  killOBDialog();
  d_mapWind->getDisplay()->RequestTerrainAveraging();
  if( d_locator ) d_locator->show(false);  // hide();
  delete d_locator;
  m_lpSelectedUnit = NULL;
}

void BattleWindowsImp::afterLoad()
{
  removeOrphans();

  // setup undo map
  BattleMeasure::HexCord size;
  size = d_battleData->map()->getSize();
  UndoMap.clear();
  UndoMap.create(size);

  /*
   * Make sure all units are set as ACTIVE, so we can edit them
   */
#if 0
  // Had to comment this out because it was causing the
  // AI Script to binary converter to crash as it was accessing
  // inactive units (which had no deploymap initialized
  // If reserve unit SP's need to be edited,
  // this will need to be uncommented out again
  TBattleUnitIter<BUIV_All> unitIter(d_battleData->ob());
  while( !unitIter.isFinished() )
  {
    unitIter.cp()->active(true);
    unitIter.next();
  }
#endif

  d_battleData->map()->removeAllHighlights();
  DateTimeDialog->SetDateAndTime(d_battleData->getDateAndTime(), d_battleData->endGameIn() );
  makeOBDialog();
  BobUtility::initUnitTypeFlags(d_battleData);

  // this InitUnits sets up the number of sprites per CP/SP - but does so using the DeployItems - doesn't seem to work correctly
  BatUnitUtils::InitUnits(d_battleData);
  // this InitUnits sets up the number of sprites per CP/SP
  BatUnitUtils::InitUnits(d_battleData->ob() );

  //                        B_Logic::deployUnitsFromSave(d_battleData);

  d_locator = new BW_Locator(this, d_battleData);
  if( d_locator )
  {
    d_locator->show(true);
  }

  d_mapWind->update(TRUE);
  d_locator->update(TRUE);
}

void BattleWindowsImp::prepareForSave()
{
  removeOrphans();
  /*
   * These are remedies for various problems & 'gotchas' associated with edited battles !
   */
  d_battleData->map()->removeAllHighlights();
  SetCPNationalities();
  ResetReinforcements();   //mtc122
  //FixDivisionalCPs();

}

void BattleWindowsImp::afterSave()
{
}
/********************************************************** mtc122
   This identifies reinforcements and sets active back to false 
   (they are set active (true) to permit editing as a fudge)
   Added by TC 
*/

void BattleWindowsImp::ResetReinforcements()
{ 
// tc this is rather crude - at the end of an edit (when the file is saved)
//  all CP are set to active then reinforce items are set to inactive (false).

//  TO DO tidy up Remove from reinforcements to call active (false)


  TBattleUnitIter<BUIV_All> unitIter(d_battleData->ob());
	while( !unitIter.isFinished() )
  {
    unitIter.cp()->active(true);
    unitIter.next();
  }             

  B_ReinforceList & r_list = d_battleData->ob()->reinforceList();
  B_ReinforceItem * r_item = r_list.first();
  if( r_item )
  {
    do
    {
      r_item->d_cp->active(false);

      r_item = r_list.next(r_item);

    } while( r_item );
  }
   

}

//mtc122 end of resetreinforcements*****************************************


/**
 * Bodge to clean up data
 * Some files have units that are orphaned
 * Go through and remove such units
 */

void BattleWindowsImp::removeOrphans()
{
  d_battleData->ob()->removeOrphans();
}






/*
 * Insulated class functions
 */

BattleWindows::BattleWindows(BattleEditorInterface* gameData)
{
  d_imp = new BattleWindowsImp(gameData);
}

BattleWindows::~BattleWindows()
{
  delete d_imp;
}


void BattleWindows::timeChanged()
{
  ASSERT(d_imp != 0);
  d_imp->timeChanged();
}

HWND BattleWindows::getHWND() const
{
  ASSERT(d_imp != 0);
  return d_imp->getHWND();
}

BattleMapWind *
BattleWindows::getBattleMap(void)
{

  ASSERT(d_imp != 0);
  return d_imp->getBattleMap();

}

}      // namespace BattleWindows_Internal

/*
 * $Log$
 * Revision 1.4  2002/11/24 13:17:48  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/07/30 09:27:23  greenius
 * Various bug fixes
 *
 */
