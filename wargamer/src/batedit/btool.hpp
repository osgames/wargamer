/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BTOOL_HPP
#define BTOOL_HPP

#ifndef __cplusplus
#error BTOOL.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Zoom Icon window
 *
 *----------------------------------------------------------------------
 */

#include "..\batwind\battool.hpp"     // Use BATWIND version since it is identical

#if 0
#include "refptr.hpp"
// #include "btwin_i.hpp"

class HWNDbase;

namespace BattleWindows_Internal
{
class BattleToolWindImp;

class BattleToolWind :
	public RefBaseDel
{
		BattleToolWindImp* d_imp;
	public:
		BattleToolWind(HWNDbase* parent);	//, PBattleWindows batWind);
		~BattleToolWind();

		void enableButton(int id, bool enabled);
		void pushButton(int id, bool pushed);

		HWND getHWND() const;
};

};	// namespace BattleWindows_Internal
#endif

#endif /* BTOOL_HPP */

