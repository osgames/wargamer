/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Test Battle Program
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "app.hpp"
#include "gamectrl.hpp"
#include "except.hpp"
#include "myassert.hpp"

#include "refptr.hpp"
#include "scenario.hpp"
#include "bcreate.hpp"
#include "batdata.hpp"
// #include "generic.hpp"

// file to handle the editor's windows
#include "editwind.hpp"
// file to dummy initialise the game & windows
#include "editgame.hpp"

#include "hexdata.hpp"
#include "maketer.hpp"

#include "ob.hpp"
#include "batarmy.hpp"

class BattleGame;

class TestBattleApp :
        public APP,
        public GameControlBase
{
                RefPtr<BattleGame> d_battleGame;

        public:

                TestBattleApp();
                ~TestBattleApp();

                /*
                 * Implementation of APP virtual functions
                 */

                BOOL initApplication();
                BOOL initInstance();
                void endApplication();
                BOOL doCmdLine(LPCSTR cmd);

                Boolean processMessage(MSG* msg);
                        // Process Message.
                        //   Return True if application dealt with it
                        //   False to continue as usual

                /*
                 * Implementation of GameControl virtual functions
                 */

                void pauseGame(Boolean paused);
                        // Do whatever is neccessary to pause or unpause the game
                        // For example in a multi-threaded game, it may need to send
                        // a signal to each thread and wait until it has responded

                void updateTime(SysTick::Value ticks);
                        // Do whatever is needed when some time has passed

};





TestBattleApp::TestBattleApp() :
    d_battleGame(0)
{
}


TestBattleApp::~TestBattleApp()
{
        endApplication();
}


BOOL TestBattleApp::initApplication()
{
        // Register window classes...
        // if(!GenericNoBackClass::registerClass())
        //        return FALSE;

        return TRUE;
}

BOOL TestBattleApp::initInstance()
{
        InitCommonControls();

        // scenario = new Scenario("nap1813.swg");
        Scenario::create("nap1813.swg");
        
        ASSERT(d_battleGame.value() == 0);
        d_battleGame = new BattleGame;

        ASSERT(d_battleGame != 0);
        ASSERT(d_battleGame->battleData() != 0);

        BattleCreate::createRandomOB(d_battleGame->battleData(), True);

        ASSERT(d_battleGame->battleData()->ob() );

        // don't automatically create a random battle - this makes the editor very frustrating to load up frequently in debug !!
        BattleMeasure::HexCord mapsize(128,128);
        BattleCreate::MakeEmptyMap(d_battleGame->battleData()->map(), mapsize);

		d_battleGame->battleData()->maximizePlayingArea();

        d_battleGame->makeWindows();

        // Make User Interface more responsive
        SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);

        return TRUE;
}

void
TestBattleApp::endApplication() {

        if((d_battleGame != 0) && (d_battleGame->battleData() != 0)) {
            BattleCreate::deleteRandomBattle(d_battleGame->battleData());
            d_battleGame = 0;
        }

        // if(scenario)
        // {
        //         delete scenario;
        //         scenario = 0;
        // }
        Scenario::kill();

}

BOOL TestBattleApp::doCmdLine(LPCSTR cmd)
{
        return TRUE;
}

Boolean TestBattleApp::processMessage(MSG* msg)
{
        return False;
}

void TestBattleApp::pauseGame(Boolean paused)
{
//        if(d_battleGame != 0)
//                d_battleGame->pauseGame(paused);
}

void TestBattleApp::updateTime(SysTick::Value ticks)
{
//      if(d_battleGame != 0)
//              d_battleGame->addTime(ticks);
}


/*========================================================================
 * Main Windows Function
 */

int PASCAL WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
        int result = 0;
        try
        {
                TestBattleApp application;
                result = application.execute(hInstance, hPrevInstance, lpszCmdLine, nCmdShow);
        }
#if defined(DEBUG)
        catch(const AssertError& e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Quit Program", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
#endif
        catch(const GeneralError& e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "General Error Exception", e.get(), MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
        catch(...)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Unknown", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }

        return result;
}


