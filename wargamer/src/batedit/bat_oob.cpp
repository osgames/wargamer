/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Dialog & controls for Battle OOB editing
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bat_oob.hpp"

#include "hexmap.hpp"

#include "scenario.hpp"
#include "dialogs.hpp"
#include "bdeploy.hpp"

#include "initunit.hpp"
#include "b_morale.hpp"

#ifdef DEBUG                 //mtc122
#include "clog.hpp"
#endif

// ctrl wrappers
#include "winctrl.hpp"
#include "fileiter.hpp"

using namespace BOB_Definitions;

/*
 * What are all these globals doing here?
 * @todo: Put them into the OOBClass
 */

BOOL unit_page_status;
BOOL leader_page_status;

HWND unit_hwnd;
HWND leader_hwnd;

BOOL oob_init_ok = FALSE;

BattleOOBClass * oob_objptr = NULL;

BattleMeasure::HexCord OOB_MouseSelectHex;
BattleMeasure::HexCord OOB_OldMouseSelectHex;
bool OOB_MouseSelected = false;

bool movemode = false;
BattleCP * movemode_CP = NULL;


const char BattleOOBClass::s_defaultPortrait[] = "<Default>";

/*
  Constructor
*/

BattleOOBClass::BattleOOBClass(HWND parent, HINSTANCE  instance, PBattleData batData, BattleMapWind * mapWind, OBDeployUser * deployuser) :
  MenuSelectHItem(NULL)
{

  oob_objptr = this;

  m_DeployUser = deployuser;

  d_mapWind = mapWind;

  hwnd = NULL;
  unit_hwnd = NULL;
  leader_hwnd = NULL;
  unit_page_status = 1;
  leader_page_status = 0;

  CP_menu = NULL;
  CP_unittype_submenu = NULL;
  CP_infantry_submenu = NULL;
  CP_cavalry_submenu = NULL;
  CP_artillery_submenu = NULL;
  CP_special_submenu = NULL;

  CurrentSelectionCP = NULL;
  CurrentSelectionSP = NULL;
  CurrentSelectionHItem = NULL;

  MenuSelectCP = NULL;
  MenuSelectSP = NULL;


  bool tree_initialized;

  hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_OOB), parent, (DLGPROC) BattleOOBClass::DefaultDialogProc);
  unit_hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_OOB_UNIT), hwnd, (DLGPROC) BattleOOBClass::UnitDialogProc);
  leader_hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_OOB_LEADER), hwnd, (DLGPROC) BattleOOBClass::LeaderDialogProc);

    
  ASSERT(hwnd != NULL);
  ASSERT(unit_hwnd != NULL);
  ASSERT(leader_hwnd != NULL);

  d_batData = batData;
  d_ob = d_batData->ob();

  tree_initialized = false;

  // initialise controls
  InitControls();

  // show main window
  ShowWindow(hwnd, SW_HIDE);
  UpdateWindow(hwnd);

  // show units dialog window
  ShowWindow(unit_hwnd, SW_HIDE);
  UpdateWindow(unit_hwnd);

  // show units dialog window
  ShowWindow(leader_hwnd, SW_HIDE);
  UpdateWindow(leader_hwnd);

  oob_init_ok = 1;

}



/*
  Destructor
*/    
BattleOOBClass::~BattleOOBClass(void) {

}

/*
  Initialise all controls in dialog
*/
void
BattleOOBClass::InitControls(void) {

  HWND ctrl;
  int i;

  // initialize the treeview
  InitTreeView();

  // set up the tab control window
  tabctrl_hwnd = GetDlgItem(hwnd, IDC_TAB_DATA);
  ASSERT(tabctrl_hwnd != NULL);

  // insert the tabs into tab control
  CreateTabs();    
  // move sub-dialogs to fit tab display area
  MoveWindow(unit_hwnd, 345, 38, 376, 359, FALSE);
  MoveWindow(leader_hwnd, 345, 38, 376, 359, FALSE);

  // initialize controls in Unit dialog

  // fill rank combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_RANKLEVEL);
  SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
  for(i=0; i<Rank_HowMany; i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) GetRankString(static_cast<RankEnum>(i) ) ) )
      FORCEASSERT("Oops - Index mismatch when creating rank combo");

  // fill CPFormation combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPFORMATION);
  SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
  for(i=0; i<CPF_Last; i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) GetCPFormationString(static_cast<CPFormation>(i) ) ) )
      FORCEASSERT("Oops - Index mismatch when creating CPFormation combo");

  // fill CPDeploy combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPDEPLOY);
  SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
  for(i=0; i<CPD_Last; i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) GetCPDeployString(static_cast<CPDeployHow>(i) ) ) )
      FORCEASSERT("Oops - Index mismatch when creating CPDeployment combo");

  // fill CPLine combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPLINE);
  SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
  for(i=0; i<CPL_Last; i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) GetCPLineString(static_cast<CPLineHow>(i) ) ) )
      FORCEASSERT("Oops - Index mismatch when creating CPLine combo");


  // fill CPFacing combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPFACING);
  SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
  for(i=0; i<12; i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) GetSPFacingString(i) ) )
      FORCEASSERT("Oops - Index mismatch when creating SPFacing combo");

  // fill CPOrders combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPORDERS);
  SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
  SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Not Yet Implemented" );

  // fill leader Nationality combo
  ctrl = GetDlgItem(leader_hwnd, IDC_COMBO_NATIONALITY);
  for(i=0; i<scenario->getNumNations(); i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) scenario->getNationName(static_cast<Nationality>(i) ) ))
      FORCEASSERT("Oops - Index mismatch when creating Leader Specialist combo");
        
  // fill leader Specilization combo
  ctrl = GetDlgItem(leader_hwnd, IDC_COMBO_SPECIALIZATION);
  for(i=0; i<Specialist::HowMany; i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) GetSpecializationString(static_cast<Specialist::Type>(i)) ) )
      FORCEASSERT("Oops - Index mismatch when creating Leader Specialist combo");







  // fill CPFormation combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPFORMATION);
  SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
  for(i=0; i<SP_Formation_HowMany; i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) GetSPFormationString(static_cast<SPFormation>(i) ) ) )
      FORCEASSERT("Oops - Index mismatch when creating SPFormation combo");


  // fill SPFacing combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPFACING);
  SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
  for(i=0; i<12; i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) GetSPFacingString(i) ) )
      FORCEASSERT("Oops - Index mismatch when creating SPFacing combo");

  // fill SPDeploy combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPDEPLOY);
  SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
  for(i=0; i<CPD_Last; i++)
    if (i != SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) GetCPDeployString(static_cast<CPDeployHow>(i) ) ) )
      FORCEASSERT("Oops - Index mismatch when creating CPDeployment combo");

  // fill leader portrait combo

  ComboBox pCombo(leader_hwnd, IDC_COMBO_PORTRAIT);
                
  int index = SendMessage(pCombo.getHWND(), CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(s_defaultPortrait));
  ASSERT(index != CB_ERR);
  ASSERT(index != CB_ERRSPACE);

  SimpleString path = scenario->getPortraitPath();
  path += "\\*.bmp";

  for(FindFileIterator fIter(path.toStr()); fIter(); ++fIter) {

    const char* name = fIter.name();
    char buffer[MAX_PATH];
    strcpy(buffer, name);
    char* s = strrchr(buffer, '.');
    if(s) *s = 0;

    int index = SendMessage(pCombo.getHWND(), CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(buffer));
    ASSERT(index != CB_ERR);
    ASSERT(index != CB_ERRSPACE);
  }

}



void
BattleOOBClass::InitTreeView(void) {

  tree_initialized = false;
    
  // get the treeview handle
  treeview_hwnd = GetDlgItem(hwnd, IDC_TREEVIEW_UNITS);
  ASSERT(treeview_hwnd != NULL);

  TreeView_DeleteAllItems(treeview_hwnd);

  // fill treeview ctrl for side 0
  FillTreeFromOOB(0);

  // fill treeview ctrl for side 1
  FillTreeFromOOB(1);

  TreeView_SelectItem(treeview_hwnd, NULL);

  tree_initialized = true;

}



void
BattleOOBClass::FillTreeFromOOB(Side side) {

  if(d_ob)
  {

    // treeview data item
    HTREEITEM h_treeitem;

    // treeview data structure
    TV_INSERTSTRUCT tv_insertstruct;

    // data fields
    char * president_name;
    char * leader_name;
    char buffer[256];

    /*
      Put side president at root node
    */

    RefBattleCP president = d_ob->getTop(side);
    if(!president)
      return;
    RefGLeader leader = president->leader();

    // associated data
    president_name = const_cast<char *> (president->getName() );
    leader_name = const_cast<char *> (leader->getName() );

    strcpy(buffer,president_name);
    strcat(buffer," - ");
    strcat(buffer, leader_name);

    // treeview data under the OOB top for given side
    tv_insertstruct.hParent = TVI_ROOT;
    tv_insertstruct.hInsertAfter = TVI_SORT;
    tv_insertstruct.item.mask = TVIF_CHILDREN | TVIF_PARAM | TVIF_TEXT;
    tv_insertstruct.item.cChildren = BobUtility::countChildren(president) + BobUtility::countSP(president);  // CPs can have both CPs & SPs attatched as children
    tv_insertstruct.item.lParam = (LPARAM) new UnitDescriptor(president);
    tv_insertstruct.item.pszText = buffer;
    tv_insertstruct.item.cchTextMax = strlen(buffer);
            
    // insert president
    h_treeitem = (HTREEITEM) SendMessage(treeview_hwnd, TVM_INSERTITEM, 0, (LPARAM) (LPTV_INSERTSTRUCT) &tv_insertstruct );

    AddChildSPs(president, h_treeitem);

    AddCPs(president->child(), h_treeitem);
  }
}






void
BattleOOBClass::AddCPs(RefBattleCP cp, HTREEITEM h_parenttreeitem) {


// treeview data item
  HTREEITEM h_treeitem;

// treeview data structure
  TV_INSERTSTRUCT tv_insertstruct;

// data fields
  char * cp_name;
  char * leader_name;
  char buffer[256];


  // if CP doesn't exits return from this level of recursion
  if(cp == NoBattleCP) return;

// removed to avoid loading-problems
//        d_batData->map()->highlightHex(cp->hex(), 1);

  // deploy unit onto map
  /*
    BattleHexMap * hexmap = d_batData->hexMap();
    if(!hexmap->isUnitPresent(cp) ) hexmap->add(cp);
  */

  RefGLeader leader = cp->leader();
  ASSERT(leader != NoGLeader);

  // associated data
  cp_name = const_cast<char *> (cp->getName() );
  leader_name = const_cast<char *> (leader->getName() );

  strcpy(buffer,cp_name);
  strcat(buffer," - ");
  strcat(buffer, leader_name);

  // treeview data under the OOB top for given side
  tv_insertstruct.hParent = h_parenttreeitem;
  tv_insertstruct.hInsertAfter = TVI_SORT;
  tv_insertstruct.item.mask = TVIF_CHILDREN | TVIF_PARAM | TVIF_TEXT;
  tv_insertstruct.item.cChildren = BobUtility::countChildren(cp) + BobUtility::countSP(cp);  // CPs can have both CPs & SPs attatched as children
  tv_insertstruct.item.lParam = (LPARAM) new UnitDescriptor(cp);
  tv_insertstruct.item.pszText = buffer;
  tv_insertstruct.item.cchTextMax = strlen(buffer);
        
  // insert CP under side president
  h_treeitem = (HTREEITEM) SendMessage(treeview_hwnd, TVM_INSERTITEM, 0, (LPARAM) (LPTV_INSERTSTRUCT) &tv_insertstruct );

  // add all SPs under this CP
  AddChildSPs(cp, h_treeitem);

  // add CP under this CP
  AddCPs(cp->child(), h_treeitem);

  // add CP next to this CP (sister nodes)
  AddCPs(cp->sister(), h_parenttreeitem);


}



/*
  Add all SPs under a given CP
*/

void
BattleOOBClass::AddChildSPs(RefBattleCP parent_cp, HTREEITEM h_parenttreeitem) {

// treeview data item
//HTREEITEM h_treeitem;

// treeview data structure
  TV_INSERTSTRUCT tv_insertstruct;

// data fields
  int sp_index;
  int sp_type;
  char * sp_name;

//    for(BattleSPIter spIter(d_ob, parent_cp) ; !spIter.isFinished(); spIter.next()) {
//    for(BattleSPIter spIter(d_ob, parent_cp) ; !spIter.isFinished(); spIter.bodgedNext()) {


  RefBattleSP sp = parent_cp->sp();
  while(sp) {

// removed to avoid loading-problems
//        d_batData->map()->highlightHex(sp->hex(), 1);
        
    // get SPs
//        RefBattleSP sp = spIter.sp();

    // deploy unit onto map
    /*
      BattleHexMap * hexmap = d_batData->hexMap();
      if(! hexmap->isUnitPresent(sp) ) hexmap->add(sp);
    */

    // associated data
    sp_index = d_ob->getIndex(sp);
    sp_type = sp->getUnitType();
    sp_name = const_cast<char *> (d_ob->spName(sp) );

    // treeview data under the previously added CP
    tv_insertstruct.hParent = h_parenttreeitem;
    tv_insertstruct.hInsertAfter = TVI_SORT;
    tv_insertstruct.item.mask = TVIF_CHILDREN | TVIF_PARAM | TVIF_TEXT;
    tv_insertstruct.item.cChildren = 0;
    tv_insertstruct.item.lParam = (LONG) new UnitDescriptor(sp);
    tv_insertstruct.item.pszText = sp_name;
    tv_insertstruct.item.cchTextMax = strlen(sp_name);

    // insert SP under previous CP item
    SendMessage(treeview_hwnd, TVM_INSERTITEM, 0, (LPARAM) (LPTV_INSERTSTRUCT) &tv_insertstruct );

    sp = sp->sister();
  }

}








void
BattleOOBClass::MoveCPUnit(BattleCP * detatching_unit, BattleCP * attatchto_unit) {

	d_ob->unlinkUnit(detatching_unit);

	d_ob->addChild(attatchto_unit, detatching_unit);

}






void
BattleOOBClass::RenameTreeItem(HTREEITEM hitem) {

// data fields
  char * cp_name;
  char * leader_name;
  char buffer[256];
    
  // get the item data & see if it's a CP or a SP
  TV_ITEM item;
  item.hItem = hitem;
  item.mask = TVIF_PARAM;
  TreeView_GetItem(treeview_hwnd, &item);

  UnitDescriptor * desc = static_cast<UnitDescriptor *> ( (LPVOID) item.lParam);

  if(desc->type == CP) {

    RefBattleCP cp = desc->cp;
    if(cp == NoBattleCP) return;

    RefGLeader leader = cp->leader();
    ASSERT(leader != NoGLeader);

    // associated data
    cp_name = const_cast<char *> (cp->getName() );
    leader_name = const_cast<char *> (leader->getName() );

    strcpy(buffer,cp_name);
    strcat(buffer," - ");
    strcat(buffer, leader_name);

    item.mask = TVIF_TEXT;
    item.hItem = hitem;
    item.pszText = buffer;
    item.cchTextMax = strlen(item.pszText);

    TreeView_SetItem(treeview_hwnd, &item);

  }

}




/*
  Expand & select a given CP
*/
void
BattleOOBClass::SetSelection(BattleCP * cp) {

  HTREEITEM hitem;
  hitem = TreeView_GetRoot(treeview_hwnd);

  TV_ITEM tvi;
  tvi.hItem = hitem;
  tvi.mask = TVIF_PARAM;
  TreeView_GetItem(treeview_hwnd, &tvi);

  UnitDescriptor * desc = static_cast<UnitDescriptor *> ( (LPVOID) tvi.lParam);
  if(desc->type == CP) {
    if(desc->cp == cp) {
      TreeView_EnsureVisible(treeview_hwnd, hitem);
      TreeView_SelectItem(treeview_hwnd, hitem);
      return;
    }
  }

  FindItemNext(cp, hitem, true, false);
  FindItemPrev(cp, hitem, true, false);
}




/*
  Ensure a given CP is visible
*/
void
BattleOOBClass::EnsureVisible(BattleCP * cp) {

  HTREEITEM hitem;
  hitem = TreeView_GetRoot(treeview_hwnd);

  TV_ITEM tvi;
  tvi.hItem = hitem;
  tvi.mask = TVIF_PARAM;
  TreeView_GetItem(treeview_hwnd, &tvi);

  UnitDescriptor * desc = static_cast<UnitDescriptor *> ( (LPVOID) tvi.lParam);
  if(desc->type == CP) {
    if(desc->cp == cp) {
      TreeView_EnsureVisible(treeview_hwnd, hitem);
      return;
    }
  }

  FindItemNext(cp, hitem, false, false);
  FindItemPrev(cp, hitem, false, false);
}




/*
  Ensure a given CP is visible
*/
void
BattleOOBClass::EnsureVisibleAndExpanded(BattleCP * cp) {

  HTREEITEM hitem;
  hitem = TreeView_GetRoot(treeview_hwnd);

  TV_ITEM tvi;
  tvi.hItem = hitem;
  tvi.mask = TVIF_PARAM;
  TreeView_GetItem(treeview_hwnd, &tvi);

  UnitDescriptor * desc = static_cast<UnitDescriptor *> ( (LPVOID) tvi.lParam);
  if(desc->type == CP) {
    if(desc->cp == cp) {
      TreeView_EnsureVisible(treeview_hwnd, hitem);
      TreeView_Expand(treeview_hwnd, hitem, TVE_EXPAND);
      return;
    }
  }

  FindItemNext(cp, hitem, false, true);
  FindItemPrev(cp, hitem, false, true);
}






void
BattleOOBClass::FindItemNext(BattleCP * cp, HTREEITEM hitem, bool select_item, bool expand_item) {

  TV_ITEM tvi;
  tvi.hItem = hitem;
  tvi.mask = TVIF_PARAM;
  TreeView_GetItem(treeview_hwnd, &tvi);

  UnitDescriptor * desc = static_cast<UnitDescriptor *> ( (LPVOID) tvi.lParam);
  if(desc->type == CP) {
    if(desc->cp == cp) {
      TreeView_EnsureVisible(treeview_hwnd, hitem);
      if(select_item) TreeView_SelectItem(treeview_hwnd, hitem);
      if(expand_item) TreeView_Expand(treeview_hwnd, hitem, TVE_EXPAND);
      return;
    }
  }

  HTREEITEM nextitem;

  nextitem = TreeView_GetChild(treeview_hwnd, hitem);
  if(nextitem) FindItemNext(cp, nextitem, select_item, expand_item);


  nextitem = TreeView_GetNextSibling(treeview_hwnd, hitem);
  if(nextitem) FindItemNext(cp, nextitem, select_item, expand_item);

}


void
BattleOOBClass::FindItemPrev(BattleCP * cp, HTREEITEM hitem, bool select_item, bool expand_item) {

  TV_ITEM tvi;
  tvi.hItem = hitem;
  tvi.mask = TVIF_PARAM;
  TreeView_GetItem(treeview_hwnd, &tvi);

  UnitDescriptor * desc = static_cast<UnitDescriptor *> ( (LPVOID) tvi.lParam);
  if(desc->type == CP) {
    if(desc->cp == cp) {
      TreeView_EnsureVisible(treeview_hwnd, hitem);
      if(select_item) TreeView_SelectItem(treeview_hwnd, hitem);
      if(expand_item) TreeView_Expand(treeview_hwnd, hitem, TVE_EXPAND);
      return;
    }
  }

  HTREEITEM nextitem;

  nextitem = TreeView_GetChild(treeview_hwnd, hitem);
  if(nextitem) FindItemPrev(cp, nextitem, select_item, expand_item);


  nextitem = TreeView_GetPrevSibling(treeview_hwnd, hitem);
  if(nextitem) FindItemPrev(cp, nextitem, select_item, expand_item);

}





    

void
BattleOOBClass::FillDialogsFromSP(RefBattleSP sp) {

  if(!oob_init_ok) return;

  HWND ctrl;
  int temp_val;
//    int i;
    
  /*
    Fill out controls in units dialog
  */

  // x starting position
  temp_val = sp->hex().x();
  SetDlgItemInt(unit_hwnd, IDC_EDIT_SPHEX_X, temp_val, FALSE);
    
  // y starting position
  temp_val = sp->hex().y();
  SetDlgItemInt(unit_hwnd, IDC_EDIT_SPHEX_Y, temp_val, FALSE);

  // starting SP formation
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPFORMATION);
  temp_val = static_cast<int> (sp->formation() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);

  // starting facing
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPFACING);
  temp_val = FacingToIndex(sp->facing() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);

  // starting SP deployment
/*
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPDEPLOY);
  temp_val = static_cast<int> (cp->deployWhichWay() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);
*/

  // strength
  temp_val = static_cast<int> (sp->strength() );
  if(! SetDlgItemInt(unit_hwnd, IDC_EDIT_SPSTRENGTH, temp_val, FALSE) ) FORCEASSERT("Oh Shit !");


}



void
BattleOOBClass::FillDialogsFromCP(RefBattleCP cp) {

  if(!oob_init_ok) return;

  HWND ctrl;
  int temp_val;
//    int i;
    
  /*
    Fill out controls in units dialog
  */

  // name edit
  SetDlgItemText(unit_hwnd, IDC_EDIT_UNIT_NAME, cp->getName() );

  // rank level combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_RANKLEVEL);
  temp_val = static_cast<int> (cp->getRank().getRankEnum() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);

  // morale edit
  temp_val = static_cast<int> (cp->morale() );
  SetDlgItemInt(unit_hwnd, IDC_EDIT_MORALE, temp_val, FALSE);

  // fatigue edit
  temp_val = static_cast<int> (cp->fatigue() );
  SetDlgItemInt(unit_hwnd, IDC_EDIT_FATIGUE, temp_val, FALSE);

  // supply edit
  temp_val = static_cast<int> (cp->ammoSupply() );
  SetDlgItemInt(unit_hwnd, IDC_EDIT_SUPPLY, temp_val, FALSE);

  // x starting position
  temp_val = cp->hex().x();
  SetDlgItemInt(unit_hwnd, IDC_EDIT_CPHEX_X, temp_val, FALSE);
    
  // y starting position
  temp_val = cp->hex().y();
  SetDlgItemInt(unit_hwnd, IDC_EDIT_CPHEX_Y, temp_val, FALSE);

  // starting CP formation
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPFORMATION);
  temp_val = static_cast<int> (cp->formation() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);
    
  // starting CP deployment
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPDEPLOY);
  temp_val = static_cast<int> (cp->deployWhichWay() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);

  // starting CP line style
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPLINE);
    
  // starting SP formation
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPFORMATION);
  temp_val = static_cast<int> (cp->spFormation() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);
    
  // starting orders
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPORDERS);

  // starting facing
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPFACING);
  temp_val = FacingToIndex(cp->facing() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);
    
  /*
    Fill out controls in leader dialog
  */

  RefGLeader leader = cp->leader();
        
  // name edit
  SetDlgItemText(leader_hwnd, IDC_EDIT_LEADER_NAME, leader->getName() );
    
  // specilization combo
  ctrl = GetDlgItem(leader_hwnd, IDC_COMBO_SPECIALIZATION);
  temp_val = static_cast<int> (leader->getSpecialistType() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);

  // nationality combo
  ctrl = GetDlgItem(leader_hwnd, IDC_COMBO_NATIONALITY);
  temp_val = static_cast<int> (leader->nation() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);

  // rank trackbar
  ctrl = GetDlgItem(leader_hwnd, IDC_TRACKBAR_RANK);
  SendMessage(ctrl, TBM_SETRANGE, (WPARAM) (BOOL) TRUE, (LPARAM) MAKELONG(1,4) );
  SendMessage(ctrl, TBM_SETPOS, (WPARAM) (BOOL) TRUE, (LPARAM) (LONG) leader->getRankLevel().getRankEnum() );

  // supreme leader checkbox
  ctrl = GetDlgItem(leader_hwnd, IDC_CHECK_SUPREME);
  if(leader->isSupremeLeader() ) SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
  else SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);

  // lucky leader checkbox
  ctrl = GetDlgItem(leader_hwnd, IDC_CHECK_LUCKY);
  if(leader->isLucky() ) SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
  else SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);

  // initiative edit
  SetDlgItemInt(leader_hwnd, IDC_EDIT_INITIATIVE, leader->getInitiative(true), FALSE);

  // aggression edit
  SetDlgItemInt(leader_hwnd, IDC_EDIT_AGGRESSION, (int) leader->getAggression(true), FALSE);

  // radius edit
  SetDlgItemInt(leader_hwnd, IDC_EDIT_RADIUS, (int) leader->getCommandRadiusAttribute(true), FALSE);
    
  // staff edit
  SetDlgItemInt(leader_hwnd, IDC_EDIT_STAFF, (int) leader->getStaff(true), FALSE);
    
  // charisma edit
  SetDlgItemInt(leader_hwnd, IDC_EDIT_CHARISMA, (int) leader->getCharisma(true), FALSE);
    
  // rank edit
  SetDlgItemText(leader_hwnd, IDC_EDIT_RANK, getRankName(leader->getRankLevel(),FALSE) );
    
  // subordination edit
  SetDlgItemInt(leader_hwnd, IDC_EDIT_SUBORDINATION, (int) leader->getSubordination(), FALSE);

  // tactical edit
  SetDlgItemInt(leader_hwnd, IDC_EDIT_TACTICAL, (int) leader->getTactical(true), FALSE);
    
  // health edit
  SetDlgItemInt(leader_hwnd, IDC_EDIT_HEALTH, (int) leader->getHealth(), FALSE);

  // portrait combo
  StringPtr portText = leader->portrait();
  // if none, set to default
  if(! portText) portText = s_defaultPortrait;
    
  ComboBox pCombo(leader_hwnd, IDC_COMBO_PORTRAIT);
  int nitems = pCombo.getNItems();
  // find the item & set to index
  for(int f=0; f<nitems; f++) {
    if(strcmp(portText, pCombo.getText(f) ) == 0) pCombo.set(f);
  }
    
}




void
BattleOOBClass::ClearCPDialogs(void) {

  if(!oob_init_ok) return;

  HWND ctrl;
//    int temp_val;
//    int i;
    
  // name edit
  SetDlgItemText(unit_hwnd, IDC_EDIT_UNIT_NAME, "");

  // rank level combo
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_RANKLEVEL);
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) -1, 0);

  // morale edit
  SetDlgItemInt(unit_hwnd, IDC_EDIT_MORALE, 0, FALSE);

  // fatigue edit
  SetDlgItemInt(unit_hwnd, IDC_EDIT_FATIGUE, 0, FALSE);

  // supply edit
  SetDlgItemInt(unit_hwnd, IDC_EDIT_SUPPLY, 0, FALSE);

  // x starting position
  SetDlgItemInt(unit_hwnd, IDC_EDIT_CPHEX_X, 0, FALSE);
    
  // y starting position
  SetDlgItemInt(unit_hwnd, IDC_EDIT_CPHEX_Y, 0, FALSE);

  // starting CP formation
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPFORMATION);
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) -1, 0);
    
  // starting CP deployment
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPDEPLOY);
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) -1, 0);

  // starting CP line style
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPLINE);
    
  // starting SP formation
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPFORMATION);
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) -1, 0);
    
  // starting orders
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPORDERS);

  // starting facing
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_CPFACING);
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) -1, 0);

}



void
BattleOOBClass::ClearSPDialogs(void) {

  if(!oob_init_ok) return;

  HWND ctrl;
//    int temp_val;
//    int i;
    
  /*
    Fill out controls in units dialog
  */

  // x starting position
  SetDlgItemInt(unit_hwnd, IDC_EDIT_SPHEX_X, 0, FALSE);
    
  // y starting position
  SetDlgItemInt(unit_hwnd, IDC_EDIT_SPHEX_Y, 0, FALSE);

  // starting SP formation
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPFORMATION);
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) -1, 0);

  // starting facing
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPFACING);
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) -1, 0);

  // starting SP deployment
/*
  ctrl = GetDlgItem(unit_hwnd, IDC_COMBO_SPDEPLOY);
  temp_val = static_cast<int> (cp->deployWhichWay() );
  SendMessage(ctrl, CB_SETCURSEL, (WPARAM) temp_val, 0);
*/

  // strength
  SetDlgItemInt(unit_hwnd, IDC_EDIT_SPSTRENGTH, 0, FALSE);


}





bool
BattleOOBClass::IsReinforcement(BattleCP * cp) {

  B_ReinforceItem * item = d_ob->reinforceList().first();

  while(item != NULL) {

    // cp is found in list, so fill in dialog details of it's reinforcements
    if(item->d_cp == cp) return true;

    item = d_ob->reinforceList().next();
  }

  return false;
}



void
BattleOOBClass::AddToReinforcements(BattleCP * cp) {

  int x = GetDlgItemInt(unit_hwnd, IDC_EDIT_ARRIVALHEX_X, NULL, TRUE);
  int y = GetDlgItemInt(unit_hwnd, IDC_EDIT_ARRIVALHEX_Y, NULL, TRUE);
  BattleMeasure::HexCord reinforcement_hex(x,y);
    
  int days = GetDlgItemInt(unit_hwnd, IDC_EDIT_REINFORCEMENTDAYS, NULL, TRUE);
  int hours = GetDlgItemInt(unit_hwnd, IDC_EDIT_REINFORCEMENTHOURS, NULL, TRUE);
  int minutes = GetDlgItemInt(unit_hwnd, IDC_EDIT_REINFORCEMENTMINUTES, NULL, TRUE);
  BattleMeasure::BattleTime::Tick ticks = BattleMeasure::days(days) + BattleMeasure::hours(hours) + BattleMeasure::minutes(minutes);    

  d_ob->reinforceList().addItem(cp, ticks, reinforcement_hex);
}


bool
BattleOOBClass::RemoveFromReinforcements(BattleCP * cp) {

  return d_ob->reinforceList().removeItem(cp);
}

/*
  Given a CP, this fills in it's reinforcement details (if it's in the reinforcements list) & returns true. else returns false.
*/
bool
BattleOOBClass::FillReinforcementDetails(BattleCP * cp) {

  B_ReinforceItem * item = d_ob->reinforceList().first();

  while(item != NULL) {

    // cp is found in list, so fill in dialog details of it's reinforcements
    if(item->d_cp == cp) {

      BattleMeasure::BattleTime::Tick ticks = item->d_time;

      // time in days, minutes & hours after battle started
      unsigned int days = BattleMeasure::daysPerTick(ticks);
      ticks -= BattleMeasure::days(days);
      unsigned int hours = BattleMeasure::hoursPerTick(ticks);
      ticks -= BattleMeasure::hours(hours);
      unsigned int minutes = BattleMeasure::minutesPerTick(ticks);
      ticks -= BattleMeasure::minutes(minutes);

      HWND ctrl = GetDlgItem(unit_hwnd, IDC_CHECK_REINFORCEMENT);
      SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);

      SetDlgItemInt(unit_hwnd, IDC_EDIT_REINFORCEMENTDAYS, days, FALSE);
      SetDlgItemInt(unit_hwnd, IDC_EDIT_REINFORCEMENTHOURS, hours, FALSE);
      SetDlgItemInt(unit_hwnd, IDC_EDIT_REINFORCEMENTMINUTES, minutes, FALSE);

      SetDlgItemInt(unit_hwnd, IDC_EDIT_ARRIVALHEX_X, item->d_hex.x(), FALSE);
      SetDlgItemInt(unit_hwnd, IDC_EDIT_ARRIVALHEX_Y, item->d_hex.y(), FALSE);

      return true;
    }

    item = d_ob->reinforceList().next();
  }

  // make sure the user knows that there's no values here
  HWND ctrl = GetDlgItem(unit_hwnd, IDC_CHECK_REINFORCEMENT);
  SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
        
  SetDlgItemInt(unit_hwnd, IDC_EDIT_REINFORCEMENTDAYS, 0, FALSE);
  SetDlgItemInt(unit_hwnd, IDC_EDIT_REINFORCEMENTHOURS, 0, FALSE);
  SetDlgItemInt(unit_hwnd, IDC_EDIT_REINFORCEMENTMINUTES, 0, FALSE);

  SetDlgItemInt(unit_hwnd, IDC_EDIT_ARRIVALHEX_X, 0, FALSE);
  SetDlgItemInt(unit_hwnd, IDC_EDIT_ARRIVALHEX_Y, 0, FALSE);


  return false;
}
            
            
BOOL
BattleOOBClass::UpdateCPFromEditCtrl(int wID, HWND hwnd) {
  if(CurrentSelectionCP == NULL) return FALSE;


  char tmpstr[256];
  int tmpval;

  switch(wID) {
    // the unit dialog
    case IDC_EDIT_UNIT_NAME : {
      GetDlgItemText(unit_hwnd, IDC_EDIT_UNIT_NAME, tmpstr, 256);
      char * newstr = copyString(tmpstr);
      CurrentSelectionCP->generic()->setName(newstr);
      RenameTreeItem(CurrentSelectionHItem);
      return TRUE; }
    case IDC_EDIT_MORALE : {
      tmpval = GetDlgItemInt(unit_hwnd, IDC_EDIT_MORALE, NULL, TRUE);
      CurrentSelectionCP->morale(tmpval);
      return TRUE; }
    case IDC_EDIT_FATIGUE : {
      tmpval = GetDlgItemInt(unit_hwnd, IDC_EDIT_FATIGUE, NULL, TRUE);
      CurrentSelectionCP->fatigue(tmpval);
      return TRUE; }
    case IDC_EDIT_SUPPLY : {
      tmpval = GetDlgItemInt(unit_hwnd, IDC_EDIT_SUPPLY, NULL, TRUE);
      CurrentSelectionCP->ammoSupply(tmpval);
      return TRUE; }
    case IDC_EDIT_CPHEX_X : {
      BattleMeasure::HexCord::Cord x = static_cast<BattleMeasure::HexCord::Cord>(GetDlgItemInt(unit_hwnd, IDC_EDIT_CPHEX_X, NULL, TRUE));
      BattleMeasure::HexCord::Cord y = static_cast<BattleMeasure::HexCord::Cord>(GetDlgItemInt(unit_hwnd, IDC_EDIT_CPHEX_Y, NULL, TRUE));
      const BattleMeasure::HexCord hex(x,y);
      CurrentSelectionCP->hex(hex);
      return TRUE; }
    case IDC_EDIT_CPHEX_Y : {
      BattleMeasure::HexCord::Cord x = static_cast<BattleMeasure::HexCord::Cord>(GetDlgItemInt(unit_hwnd, IDC_EDIT_CPHEX_X, NULL, TRUE));
      BattleMeasure::HexCord::Cord y = static_cast<BattleMeasure::HexCord::Cord>(GetDlgItemInt(unit_hwnd, IDC_EDIT_CPHEX_Y, NULL, TRUE));
      const BattleMeasure::HexCord hex(x,y);
      CurrentSelectionCP->hex(hex);
      return TRUE; }
      // the leader dialog
    case IDC_EDIT_LEADER_NAME : {
      GetDlgItemText(leader_hwnd, wID, tmpstr, 256);
      char * newstr = copyString(tmpstr);
      CurrentSelectionCP->generic()->leader()->setName(newstr);
      RenameTreeItem(CurrentSelectionHItem);
      return TRUE; }
    case IDC_EDIT_INITIATIVE : {
      tmpval = GetDlgItemInt(leader_hwnd, IDC_EDIT_INITIATIVE, NULL, TRUE);
      CurrentSelectionCP->generic()->leader()->setInitiative(tmpval);
      return TRUE; }
    case IDC_EDIT_AGGRESSION : {
      tmpval = GetDlgItemInt(leader_hwnd, IDC_EDIT_AGGRESSION, NULL, TRUE);
      CurrentSelectionCP->generic()->leader()->setAggression(tmpval);
      return TRUE; }
    case IDC_EDIT_RADIUS : {
      tmpval = GetDlgItemInt(leader_hwnd, IDC_EDIT_RADIUS, NULL, TRUE);
      CurrentSelectionCP->generic()->leader()->setCommandRadius(tmpval);
      return TRUE; }
    case IDC_EDIT_STAFF : {
      tmpval = GetDlgItemInt(leader_hwnd, IDC_EDIT_STAFF, NULL, TRUE);
      CurrentSelectionCP->generic()->leader()->setStaff(tmpval);
      return TRUE; }
    case IDC_EDIT_CHARISMA : {
      tmpval = GetDlgItemInt(leader_hwnd, IDC_EDIT_CHARISMA, NULL, TRUE);
      CurrentSelectionCP->generic()->leader()->setCharisma(tmpval);
      return TRUE; }
    case IDC_EDIT_RANK : {
      // don't think this one belongs here...
      return TRUE; }
    case IDC_EDIT_SUBORDINATION : {
      tmpval = GetDlgItemInt(leader_hwnd, IDC_EDIT_SUBORDINATION, NULL, TRUE);
      CurrentSelectionCP->generic()->leader()->setSubordination(tmpval);
      return TRUE; }
    case IDC_EDIT_TACTICAL : {
      tmpval = GetDlgItemInt(leader_hwnd, IDC_EDIT_TACTICAL, NULL, TRUE);
      CurrentSelectionCP->generic()->leader()->setTactical(tmpval);
      return TRUE; }
    case IDC_EDIT_HEALTH : {
      tmpval = GetDlgItemInt(leader_hwnd, IDC_EDIT_HEALTH, NULL, TRUE);
      CurrentSelectionCP->generic()->leader()->setHealth(tmpval);
      return TRUE; }

    default : return FALSE;
  }
}

BOOL
BattleOOBClass::UpdateCPFromComboCtrl(int wID, HWND hwnd) {
  if(CurrentSelectionCP == NULL) return FALSE;

  int tmpval;
  tmpval = SendMessage(hwnd, CB_GETCURSEL, 0, 0);
  if(tmpval == CB_ERR) return FALSE;


  switch(wID) {

    // the unit dialog
        
    case IDC_COMBO_RANKLEVEL : {
      CurrentSelectionCP->setRank(static_cast<RankEnum>(tmpval));
      return TRUE; }

    case IDC_COMBO_CPFORMATION : {
      CurrentSelectionCP->formation(static_cast<CPFormation>(tmpval));
      BattleOrder order;
      order = CurrentSelectionCP->getCurrentOrder();
      order.divFormation(static_cast<CPFormation>(tmpval));
      CurrentSelectionCP->setCurrentOrder(order);
            
      return TRUE; }

    case IDC_COMBO_CPDEPLOY : {
//            CurrentSelectionCP->deployWhichWay(static_cast<CPDeployHow> (tmpval));
      BattleOrder order;
      order = CurrentSelectionCP->getCurrentOrder();
      order.deployHow(static_cast<CPDeployHow> (tmpval));
      CurrentSelectionCP->setCurrentOrder(order);
      return TRUE; }

    case IDC_COMBO_CPLINE : {
      BattleOrder order;
      order = CurrentSelectionCP->getCurrentOrder();
      order.lineHow(static_cast<CPLineHow> (tmpval));
      CurrentSelectionCP->setCurrentOrder(order);
      return TRUE; }

    case IDC_COMBO_SPFORMATION : {
      // set CP sp formation
      BattleOrder order;
      order = CurrentSelectionCP->getCurrentOrder();
      order.spFormation(static_cast<SPFormation> (tmpval));
      CurrentSelectionCP->setCurrentOrder(order);
      // set child SPs formations
      BattleSP * sp = CurrentSelectionCP->sp();
      while(sp) {
        sp->setFormation(static_cast<SPFormation> (tmpval));
        sp = sp->sister();
      }

      return TRUE; }

    case IDC_COMBO_SPORDERS : {
      return TRUE; }

    case IDC_COMBO_CPFACING : {
      CurrentSelectionCP->setFacing(IndexToFacing(tmpval) );
      BattleOrder order;
      order = CurrentSelectionCP->getCurrentOrder();
      order.facing(IndexToFacing(tmpval) );
      CurrentSelectionCP->setCurrentOrder(order);

      return TRUE; }

      // the leader dialog

    case IDC_COMBO_SPECIALIZATION : {
      CurrentSelectionCP->leader()->setAsSpecialist(static_cast<Specialist::Type> (tmpval));
      return TRUE; }

    case IDC_COMBO_NATIONALITY : {
      CurrentSelectionCP->leader()->nation(static_cast<Nationality> (tmpval));
      return TRUE; }

    case IDC_COMBO_PORTRAIT : {
      ComboBox pCombo(leader_hwnd, IDC_COMBO_PORTRAIT);
      StringPtr portText = pCombo.getText();
      if(strcmp(portText, s_defaultPortrait) == 0)
        CurrentSelectionCP->leader()->portrait(0);
      else
        CurrentSelectionCP->leader()->portrait(copyString(portText));
    }

    default : return FALSE;
  }
}

BOOL
BattleOOBClass::UpdateCPFromTrackbarCtrl(HWND hwnd) {
  if(hwnd == NULL || CurrentSelectionCP == NULL) return FALSE;

  int tmpval;
  HWND ctrl;
// no ID given with WM_SCROLL messages, so compane control handles instead
    
  ctrl = GetDlgItem(leader_hwnd, IDC_TRACKBAR_RANK);
  if(hwnd == ctrl) {
    tmpval = SendMessage(hwnd, TBM_GETPOS, 0, 0);
    RefGLeader leader = CurrentSelectionCP->leader();
    Rank r(static_cast<RankEnum>(tmpval));
    leader->setRankLevel(r);
    return TRUE; }
    
  return FALSE;
}




BOOL
BattleOOBClass::UpdateSPFromEditCtrl(int wID, HWND hwnd) {
  if(CurrentSelectionSP == NULL) return FALSE;

//char tmpstr[256];
  int tmpval;

  switch(wID) {
    // the unit dialog
    case IDC_EDIT_SPSTRENGTH : {
      tmpval = GetDlgItemInt(unit_hwnd, IDC_EDIT_SPSTRENGTH, NULL, TRUE);
      CurrentSelectionSP->strength(tmpval);
      return TRUE; }

    case IDC_EDIT_SPHEX_X : {
      BattleMeasure::HexCord::Cord x = static_cast<BattleMeasure::HexCord::Cord>(GetDlgItemInt(unit_hwnd, IDC_EDIT_SPHEX_X, NULL, TRUE));
      BattleMeasure::HexCord::Cord y = static_cast<BattleMeasure::HexCord::Cord>(GetDlgItemInt(unit_hwnd, IDC_EDIT_SPHEX_Y, NULL, TRUE));
      const BattleMeasure::HexCord hex(x,y);
      CurrentSelectionSP->hex(hex);
      return TRUE; }
    case IDC_EDIT_SPHEX_Y : {
      BattleMeasure::HexCord::Cord x = static_cast<BattleMeasure::HexCord::Cord>(GetDlgItemInt(unit_hwnd, IDC_EDIT_SPHEX_X, NULL, TRUE));
      BattleMeasure::HexCord::Cord y = static_cast<BattleMeasure::HexCord::Cord>(GetDlgItemInt(unit_hwnd, IDC_EDIT_SPHEX_Y, NULL, TRUE));
      const BattleMeasure::HexCord hex(x,y);
      CurrentSelectionSP->hex(hex);
      return TRUE; }

  }
  return FALSE;
}



BOOL
BattleOOBClass::UpdateSPFromComboCtrl(int wID, HWND hwnd) {

  BattleSP * sp = CurrentSelectionSP;

  if(!sp) {
    if(CurrentSelectionCP != NULL) sp = CurrentSelectionCP->sp();
  }


  if(!sp) return FALSE;



  int tmpval;
  tmpval = SendMessage(hwnd, CB_GETCURSEL, 0, 0);
  if(tmpval == CB_ERR) return FALSE;


  switch(wID) {

    // the unit dialog
    case IDC_COMBO_SPFORMATION : {
//            CurrentSelectionSP->setFormation(static_cast<SPFormation>(tmpval));

      BattleOrder order;
      order = sp->parent()->getCurrentOrder();
      order.spFormation(static_cast<SPFormation>(tmpval));
      sp->parent()->setCurrentOrder(order);

      return TRUE; }

    case IDC_COMBO_SPFACING : {
      sp->setFacing(IndexToFacing(tmpval) );
      return TRUE; }

    case IDC_COMBO_SPDEPLOY : {
      return TRUE; }

    case IDC_COMBO_SPORDERS : {
      return TRUE; }

  }

  return FALSE;
}



BOOL
BattleOOBClass::UpdateSPFromTrackbarCtrl(HWND hwnd) {
  return TRUE;
}


BOOL
BattleOOBClass::UpdateCPFromCheckBox(int wID, HWND hwnd) {
  if(CurrentSelectionCP == NULL) return FALSE;

  HWND ctrl;

  switch(wID) {

    case IDC_CHECK_SUPREME : {
      ctrl = GetDlgItem(leader_hwnd, IDC_CHECK_SUPREME);
      if(SendMessage(ctrl, BM_GETCHECK, 0, 0) == BST_UNCHECKED)
        CurrentSelectionCP->generic()->leader()->isSupremeLeader(false);
      else CurrentSelectionCP->generic()->leader()->isSupremeLeader(true);
      break;
    }

    case IDC_CHECK_LUCKY : {
      ctrl = GetDlgItem(leader_hwnd, IDC_CHECK_LUCKY);
      if(SendMessage(ctrl, BM_GETCHECK, 0, 0) == BST_UNCHECKED)
        CurrentSelectionCP->generic()->leader()->isLucky(false);
      else CurrentSelectionCP->generic()->leader()->isLucky(true);
      break;
    }

  }  // end of switch
  return TRUE;
}



BOOL
BattleOOBClass::UpdateSPFromCheckBox(int wID, HWND hwnd) {

  return TRUE;
}




/*
  Create tabs for tab control
*/

void
BattleOOBClass::CreateTabs(void) {

  TC_ITEM tc_item;
  int ret;

  // setup 'Unit' tab
  tc_item.mask = TCIF_TEXT | TCIF_IMAGE;
  tc_item.pszText = "Unit";
  tc_item.cchTextMax = strlen(tc_item.pszText);
  tc_item.iImage = -1;
  // insert at index 0
  ret = SendMessage(tabctrl_hwnd, TCM_INSERTITEM, (WPARAM) 0, (LPARAM) (const TC_ITEM FAR *) &tc_item);

  // setup 'Leader' tab
  tc_item.mask = TCIF_TEXT | TCIF_IMAGE;
  tc_item.pszText = "Leader";
  tc_item.cchTextMax = strlen(tc_item.pszText);
  tc_item.iImage = -1;
  // insert at index 1
  ret = SendMessage(tabctrl_hwnd, TCM_INSERTITEM, (WPARAM) 1, (LPARAM) (const TC_ITEM FAR *) &tc_item);

}



/*
  Handle messages
*/
BOOL APIENTRY
BattleOOBClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {


  switch(Msg) {

    case WM_MOUSEACTIVATE : {
      // set editor mode to this dialog, but don't process message
      EditorMode = DIALOG_OOB;
      ModeText = "Order Of Battle Mode";
      return MA_ACTIVATE;
    }



      // handle basic controls, or pass controls up to editor
    case WM_COMMAND : {

      int wNotifyCode = HIWORD(wParam);
      HWND ctrlhwnd = (HWND) lParam;
      int wID = LOWORD(wParam);

      // process shortcut menus

      if(wNotifyCode == 0) {
                
        // main CP shortcut menu
        if(wID >=MENU_CPMENU && wID <= IDM_CPMENU_MAXID) {
          oob_objptr->OnCPMenu(wID);
          return 0; }

        // main SP shortcut menu
        if(wID >= MENU_SPMENU && wID <= IDM_SPMENU_MAXID) {
          oob_objptr->OnSPMenu(wID);
          return 0; }
                    
        // CP menu infantry popup
        if(oob_objptr->CP_infantry_submenu != NULL)
          if(wID >= IDM_CPINFANTRYSUBMENU_FIRSTITEM && wID <= IDM_CPINFANTRYSUBMENU_MAXID) {
            oob_objptr->OnCPInfantrySubMenu(wID);
            return 0; }

        // CP menu cavalry popup
        if(oob_objptr->CP_cavalry_submenu != NULL)
          if(wID >= IDM_CPCAVALRYSUBMENU_FIRSTITEM && wID <= IDM_CPCAVALRYSUBMENU_MAXID) {
            oob_objptr->OnCPCavalrySubMenu(wID);
            return 0; }

        // CP menu artillery popup
        if(oob_objptr->CP_artillery_submenu != NULL)
          if(wID >= IDM_CPARTILLERYSUBMENU_FIRSTITEM && wID <= IDM_CPARTILLERYSUBMENU_MAXID) {
            oob_objptr->OnCPArtillerySubMenu(wID);
            return 0; }

        // CP menu special popup
        if(oob_objptr->CP_special_submenu != NULL)
          if(wID >= IDM_CPSPECIALSUBMENU_FIRSTITEM && wID <= IDM_CPSPECIALSUBMENU_MAXID) {
            oob_objptr->OnCPSpecialSubMenu(wID);
            return 0; }

      }


      // process any changes in tabbed-dialog controls & update CP accordingly

      // edit controls
      if(wNotifyCode == EN_CHANGE) {

        switch(wID) {
          // if we're typing in the reinforcements edit ctrls
          case IDC_EDIT_REINFORCEMENTDAYS :
          case IDC_EDIT_REINFORCEMENTHOURS :
          case IDC_EDIT_REINFORCEMENTMINUTES :
          case IDC_EDIT_ARRIVALHEX_X :
          case IDC_EDIT_ARRIVALHEX_Y : {

            BattleCP * cp = oob_objptr->CurrentSelectionCP;
            if(!cp) return 0;

            // if this unit is already a reinforcement, then remove it
            if(oob_objptr->IsReinforcement(cp)) {
              oob_objptr->RemoveFromReinforcements(cp);
              // add to reinforcements with new values
              oob_objptr->AddToReinforcements(cp);

              // set reinforcement check box tick
              HWND ctrl = GetDlgItem(unit_hwnd, IDC_CHECK_REINFORCEMENT);
              SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
            }
            return 0;
          }
        }

        // otherwise we're typing in another edit ctrl
        oob_objptr->UpdateCPFromEditCtrl(wID, ctrlhwnd);
        oob_objptr->UpdateSPFromEditCtrl(wID, ctrlhwnd);
        return 0;
      }

      if(wNotifyCode == CBN_SELCHANGE) {

        oob_objptr->UpdateCPFromComboCtrl(wID, ctrlhwnd);
        oob_objptr->UpdateSPFromComboCtrl(wID, ctrlhwnd);
        return 0;
      }

      if(wNotifyCode == BN_CLICKED) {
        switch(wID) {

          case IDC_BUTTON_CALCULATE : {
            if(oob_objptr->CurrentSelectionCP) {
              int temp_val = BattleMoraleUtility::CalcMorale(oob_objptr->d_ob, oob_objptr->CurrentSelectionCP);
              oob_objptr->CurrentSelectionCP->morale(temp_val);
              SetDlgItemInt(unit_hwnd, IDC_EDIT_MORALE, temp_val, FALSE);
            }
            else if(oob_objptr->CurrentSelectionSP) {
              int temp_val = BattleMoraleUtility::CalcMorale(oob_objptr->d_ob, oob_objptr->CurrentSelectionSP->parent() );
              oob_objptr->CurrentSelectionSP->parent()->morale(temp_val);
              SetDlgItemInt(unit_hwnd, IDC_EDIT_MORALE, temp_val, FALSE);
            }
          }
                
          case IDC_BUTTON_DEPLOY : {
            oob_objptr->m_DeployUser->OnDeployUnit(oob_objptr->CurrentSelectionCP);
            break;
          }

          case IDC_BUTTON_REMOVE : {
            oob_objptr->m_DeployUser->OnRemoveUnit(oob_objptr->CurrentSelectionCP);
            break;
          }

            // toggle reinforcement status for this CP - and add / remove from reinforcements list
          case IDC_CHECK_REINFORCEMENT : {
            BattleCP * cp = oob_objptr->CurrentSelectionCP;
            if(cp) {
              // this CP is already a reinforcement - so remove form list & reset controls
              if(oob_objptr->IsReinforcement(cp)) {

                oob_objptr->RemoveFromReinforcements(cp);
                oob_objptr->FillReinforcementDetails(cp);

              }
              // this CP is not a reinforcement - so add to list & set controls
              else {

                oob_objptr->AddToReinforcements(cp);

              }
            }
            break;
          }

          default : {
            oob_objptr->UpdateCPFromCheckBox(wID, ctrlhwnd);
            oob_objptr->UpdateSPFromCheckBox(wID, ctrlhwnd);
            return 0;
          }
        }
      }
      // switch(wID) {


      // anything not trapped, pass up
      // default : 
      FORWARD_WM_COMMAND(GetParent(hWnd), wID, (HWND) lParam, HIWORD(wParam), PostMessage);
      // }
      return 0;
    }



      // handle notify messages to tab windows
    case WM_NOTIFY : {
      int idCtrl = (int) wParam;
      switch(idCtrl) {
                
        /*
          user has pressed the tab control
        */
        case IDC_TAB_DATA : {
          // get the associated notification structure
          NMHDR * pnmh = (LPNMHDR) lParam;
          // user has changed the tab selection
          if(pnmh->code == TCN_SELCHANGE) {
            // get tab control current selection
            int tab = TabCtrl_GetCurSel(pnmh->hwndFrom);
            // show unit tab & hide leader tab
            if(tab == 0) {
              unit_page_status = TRUE; leader_page_status = FALSE;
              ShowWindow(leader_hwnd, SW_HIDE); ShowWindow(unit_hwnd, SW_SHOW); UpdateWindow(unit_hwnd); }
            // show leader tab & hide unit tab
            if(tab == 1) {
              leader_page_status = TRUE; unit_page_status = FALSE;
              ShowWindow(unit_hwnd, SW_HIDE); ShowWindow(leader_hwnd, SW_SHOW); UpdateWindow(leader_hwnd); }
          }
        } // end of tabcontrol sel change case


          /*
            user has done something with the units treeview control
          */
        case IDC_TREEVIEW_UNITS : {

          // make sure we're ready to process stuff
          if(! oob_objptr->tree_initialized) return 0;
                    
          // get the associated notification structure
          NM_TREEVIEW * pnmtv = (NM_TREEVIEW FAR *) lParam;

          /*

					check for a rightmouse click, to see if a context menu should appear

					*/

          if(pnmtv->hdr.code == NM_RCLICK) {

            // if the item is not selected, windows treats it as a drag & drop target
            HTREEITEM item_handle = NULL;
            item_handle = TreeView_GetDropHilight(oob_objptr->treeview_hwnd);
            // if it is selected, this will be NULL so it is treated as a 
            if(item_handle == NULL) item_handle = TreeView_GetSelection(oob_objptr->treeview_hwnd);
            // if item is still NULL, then give up & return 0
            if(item_handle == NULL) return 0;

            // get the rect for positioning the context menu
            RECT tree_rect;
            GetWindowRect(oob_objptr->treeview_hwnd, &tree_rect);
            RECT item_rect;
            TreeView_GetItemRect(oob_objptr->treeview_hwnd, item_handle, &item_rect, TRUE);
            int xpos = tree_rect.left + ( (item_rect.right - item_rect.left) /2);
            int ypos = tree_rect.top + ( (item_rect.bottom - item_rect.top) /2);
                        
            // get the item data & see if it's a CP or a SP
            TV_ITEM item;
            item.hItem = item_handle;
            item.mask = TVIF_PARAM;
            TreeView_GetItem(oob_objptr->treeview_hwnd, &item);

            UnitDescriptor * desc = static_cast<UnitDescriptor *> ( (LPVOID) item.lParam);

            if(desc->type == CP) {

              // construct a context menu for the selected CP
              BattleCP * cp = desc->cp;
              oob_objptr->MenuSelectHItem = item_handle;
              oob_objptr->ConstructCPShortcutMenu(cp,xpos,ypos);
            }

            if(desc->type == SP) {

              BattleSP * sp = desc->sp;
              oob_objptr->MenuSelectHItem = item_handle;
              oob_objptr->ConstructSPShortcutMenu(sp,xpos,ypos);
            }

       
            return 0;                        
          } // end of trapping rightmouse in treeview


          /*
					
					check to see if user has changed treview selection

					*/

          if(pnmtv->hdr.code == TVN_SELCHANGED) {
            // make sure our lParam field's valid
            if(pnmtv->itemNew.mask & TVIF_PARAM) {
              // if lParam isn't 0 this is a CP - cast 32bit val to CP & fill up dialogs from it
              UnitDescriptor * desc = static_cast<UnitDescriptor *> ( (LPVOID) pnmtv->itemNew.lParam);

              if(desc->type == CP) {

                /*
                  If we're in MoveCP mode...
                */
								if(movemode) {

									ASSERT(movemode_CP);
									oob_objptr->MoveCPUnit(movemode_CP, desc->cp);
									SetCursor(LoadCursor(NULL, IDC_ARROW));

									oob_objptr->InitTreeView();
									oob_objptr->EnsureVisibleAndExpanded(desc->cp);

									movemode = false;
									movemode_CP = NULL;

									return 0;
								}

                oob_objptr->CurrentSelectionCP = desc->cp;
                oob_objptr->CurrentSelectionSP = NULL;
                oob_objptr->CurrentSelectionHItem = pnmtv->itemNew.hItem;
                oob_objptr->FillDialogsFromCP(desc->cp);
                oob_objptr->FillReinforcementDetails(desc->cp);
                oob_objptr->ClearSPDialogs();

                oob_objptr->m_DeployUser->OnSelChange(desc->cp);
              }

              if(desc->type == SP) {

                oob_objptr->CurrentSelectionSP = desc->sp;
                oob_objptr->CurrentSelectionCP = NULL;
                oob_objptr->CurrentSelectionHItem = pnmtv->itemNew.hItem;
                oob_objptr->FillDialogsFromSP(desc->sp);
                oob_objptr->ClearCPDialogs();

                oob_objptr->m_DeployUser->OnSelChange(desc->sp);

              } // phew ! finished handling leftmouse selections in treeview
            }
          } // end of handling treeview selection change message


          // handle any other treeview notification messages...

        } // end of processing units treeview notification messages


                                

      } // end of switch on idCtrl
      return 0;
    } // end of WM_NOTIFY processing



    case WM_PAINT : {
//            if(oob_init_ok) ShowWindow(hWnd, SW_SHOW);
      if(oob_init_ok) {
        UpdateWindow(unit_hwnd);
        BringWindowToTop(unit_hwnd);
        UpdateWindow(leader_hwnd);
        BringWindowToTop(leader_hwnd);
      }
      return 0; }


      // hide dialog on close
    case WM_CLOSE : {
      ShowWindow(hWnd, SW_HIDE);
      return 0; }

      // default case
    default:
      return FALSE;

  } // end of switch
    
}




BOOL APIENTRY
BattleOOBClass::UnitDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

  if(hWnd == NULL) return FALSE;

  switch(Msg) {

    // handle basic controls, or pass controls up to editor
    case WM_COMMAND : {
      int wID = LOWORD(wParam);
      // switch(wID) {
      // anything not trapped, pass up
      // default : 
      FORWARD_WM_COMMAND(GetParent(hWnd), wID, (HWND) lParam, HIWORD(wParam), PostMessage);
      // }
      return 0;
    }

    default : return FALSE;

  } // end of switch

}



BOOL APIENTRY
BattleOOBClass::LeaderDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

  switch(Msg) {


    // handle basic controls, or pass controls up to editor
    case WM_COMMAND : {
      int wID = LOWORD(wParam);
      // switch(wID) {
      // anything not trapped, pass up
      // default : 
      FORWARD_WM_COMMAND(GetParent(hWnd), wID, (HWND) lParam, HIWORD(wParam), PostMessage);
      // }
      return 0;
    }


      // handle scrolling of trackbars, etc
    case WM_HSCROLL : {

      HWND ctrlhwnd = (HWND) lParam;
      int nScrollCode = (int) LOWORD(wParam);
      int nPos = (short int) HIWORD(wParam);

      if(oob_objptr->UpdateCPFromTrackbarCtrl(ctrlhwnd) == TRUE) return 0;
    }
        
    default : return FALSE;

  } // end of switch

}




void
BattleOOBClass::ConstructSPShortcutMenu(RefBattleSP sp, int xpos, int ypos) {

  if(SP_menu) { DestroyMenu(SP_menu); SP_menu = NULL; }
  SP_menu = CreatePopupMenu();

  MenuSelectSP = sp;

  MENUITEMINFO menuinfo;
  menuinfo.cbSize = sizeof(MENUITEMINFO);
  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  // normally set es enabled, but grey out if this is the top level CP
  menuinfo.fState = MFS_ENABLED;
  menuinfo.wID = IDM_SPMENU_DELETE;
  menuinfo.hSubMenu = NULL;
  menuinfo.dwTypeData = "Delete SP";
  menuinfo.cch = strlen(menuinfo.dwTypeData);

  InsertMenuItem(SP_menu, IDM_SPMENU_DELETE, FALSE, &menuinfo);

  TrackPopupMenuEx(SP_menu, TPM_LEFTBUTTON | TPM_LEFTALIGN, xpos, ypos, hwnd, NULL);
    
}



void
BattleOOBClass::ConstructCPShortcutMenu(RefBattleCP cp, int xpos, int ypos) {

  if(CP_menu) { DestroyMenu(CP_menu); CP_menu = NULL; }
  if(CP_unittype_submenu) { DestroyMenu(CP_unittype_submenu); CP_unittype_submenu = NULL; }
  if(CP_infantry_submenu) { DestroyMenu(CP_infantry_submenu); CP_infantry_submenu = NULL; }
  if(CP_cavalry_submenu) { DestroyMenu(CP_cavalry_submenu); CP_cavalry_submenu = NULL; }
  if(CP_artillery_submenu) { DestroyMenu(CP_artillery_submenu); CP_artillery_submenu = NULL; }
  if(CP_special_submenu) { DestroyMenu(CP_special_submenu); CP_special_submenu = NULL; }
                
  CP_menu = CreatePopupMenu();
  CP_unittype_submenu = CreatePopupMenu();
  CP_infantry_submenu = CreatePopupMenu();
  CP_cavalry_submenu = CreatePopupMenu();
  CP_artillery_submenu = CreatePopupMenu();
  CP_special_submenu = CreatePopupMenu();
    
  MenuSelectCP = cp;

/*
  Create the Main CP menu
  Delete
  Delete SPs
  Add CP
  Add SP >
*/

  MENUITEMINFO menuinfo;
  menuinfo.cbSize = sizeof(MENUITEMINFO);

  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  // normally set es enabled, but grey out if this is the top level CP
  if(cp->parent() != NULL) menuinfo.fState = MFS_ENABLED; else menuinfo.fState = MFS_GRAYED;
  menuinfo.wID = IDM_CPMENU_DELETE;
  menuinfo.hSubMenu = NULL;
  menuinfo.dwTypeData = "Delete CP";
  menuinfo.cch = strlen(menuinfo.dwTypeData);

  InsertMenuItem(CP_menu, IDM_CPMENU_DELETE, FALSE, &menuinfo);



  menuinfo.cbSize = sizeof(MENUITEMINFO);

  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  // normally set es enabled, but grey out if this Cp has no SPs
  if(cp->sp() != NULL) menuinfo.fState = MFS_ENABLED; else menuinfo.fState = MFS_GRAYED;
  menuinfo.wID = IDM_CPMENU_DELETESP;
  menuinfo.hSubMenu = NULL;
  menuinfo.dwTypeData = "Delete SPs";
  menuinfo.cch = strlen(menuinfo.dwTypeData);

  InsertMenuItem(CP_menu, IDM_CPMENU_DELETESP, FALSE, &menuinfo);



  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  menuinfo.fState = MFS_ENABLED;
  menuinfo.wID = IDM_CPMENU_NEWCP;
  menuinfo.hSubMenu = NULL;
  menuinfo.dwTypeData = "New CP";
  menuinfo.cch = strlen(menuinfo.dwTypeData);

  InsertMenuItem(CP_menu, IDM_CPMENU_NEWCP, FALSE, &menuinfo);

  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_SUBMENU | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  menuinfo.fState = MFS_ENABLED;
  menuinfo.wID = IDM_CPMENU_NEWSP;
  menuinfo.hSubMenu = CP_unittype_submenu;
  menuinfo.dwTypeData = "New SP";
  menuinfo.cch = strlen(menuinfo.dwTypeData);

  InsertMenuItem(CP_menu, IDM_CPMENU_NEWSP, FALSE, &menuinfo);


  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_SUBMENU | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  menuinfo.fState = MFS_ENABLED;
  menuinfo.wID = IDM_CPMENU_MOVECPTO;
  menuinfo.hSubMenu = NULL;
  menuinfo.dwTypeData = "Move CP To...";
  menuinfo.cch = strlen(menuinfo.dwTypeData);

  InsertMenuItem(CP_menu, IDM_CPMENU_MOVECPTO, FALSE, &menuinfo);


/*
  Create the UnitType Submenu
  Infantry >
  Cavalry >
  Artillery >
  Special >
*/

  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_SUBMENU | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  menuinfo.fState = MFS_ENABLED;
  menuinfo.wID = IDM_CPUNITTYPESUBMENU_INFANTRY;
  menuinfo.hSubMenu = CP_infantry_submenu;
  menuinfo.dwTypeData = "Infantry";
  menuinfo.cch = strlen(menuinfo.dwTypeData);
  InsertMenuItem(CP_unittype_submenu, IDM_CPUNITTYPESUBMENU_INFANTRY, FALSE, &menuinfo);


  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_SUBMENU | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  menuinfo.fState = MFS_ENABLED;
  menuinfo.wID = IDM_CPUNITTYPESUBMENU_CAVALRY;
  menuinfo.hSubMenu = CP_cavalry_submenu;
  menuinfo.dwTypeData = "Cavalry";
  menuinfo.cch = strlen(menuinfo.dwTypeData);
  InsertMenuItem(CP_unittype_submenu, IDM_CPUNITTYPESUBMENU_CAVALRY, FALSE, &menuinfo);

  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_SUBMENU | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  menuinfo.fState = MFS_ENABLED;
  menuinfo.wID = IDM_CPUNITTYPESUBMENU_ARTILLERY;
  menuinfo.hSubMenu = CP_artillery_submenu;
  menuinfo.dwTypeData = "Artillery";
  menuinfo.cch = strlen(menuinfo.dwTypeData);
  InsertMenuItem(CP_unittype_submenu, IDM_CPUNITTYPESUBMENU_ARTILLERY, FALSE, &menuinfo);

  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_SUBMENU | MIIM_ID;
  menuinfo.fType = MFT_STRING;
  menuinfo.fState = MFS_ENABLED;
  menuinfo.wID = IDM_CPUNITTYPESUBMENU_SPECIAL;
  menuinfo.hSubMenu = CP_special_submenu;
  menuinfo.dwTypeData = "Special";
  menuinfo.cch = strlen(menuinfo.dwTypeData);
  InsertMenuItem(CP_unittype_submenu, IDM_CPUNITTYPESUBMENU_SPECIAL, FALSE, &menuinfo);

  // create the submenu which contains SPs
  ConstructCPShortcutSubMenus(cp);


  TrackPopupMenuEx(CP_menu, TPM_LEFTBUTTON | TPM_LEFTALIGN, xpos, ypos, hwnd, NULL);
}




void
BattleOOBClass::ConstructCPShortcutSubMenus(RefBattleCP cp) {

  ASSERT(d_ob);

  char buffer[256];

  MENUITEMINFO menuinfo;
  menuinfo.cbSize = sizeof(MENUITEMINFO);
  menuinfo.fMask = MIIM_TYPE | MIIM_STATE | MIIM_ID | MIIM_DATA;
  menuinfo.fType = MFT_STRING;
  menuinfo.fState = MFS_ENABLED;
  menuinfo.hSubMenu = NULL;

  int infantry_id = 0;
  int cavalry_id = 0;
  int artillery_id = 0;
  int special_id = 0;

  const UnitTypeTable& table = d_ob->getUnitTypes();


//     int i = 0;
//     int index = 0;
  UnitType nTypes = table.entries();
  for(UnitType t = 0; t < nTypes; t++) {

    const UnitTypeItem& unitType = table[t];
//        if(unitType.isNationality(cp->getNation()) && unitType.getBasicType() == BasicUnitType::basicType) {


    // make sure this is a valid unit (ie. name isn't "Z")
    if(lstrcmp("z", unitType.getName() ) != 0) {

      // fill out name field            
      strcpy(buffer, unitType.getName() );
      menuinfo.dwTypeData = buffer;
      menuinfo.cch = strlen(menuinfo.dwTypeData);
    
      // decide which menu to add item to
      if(unitType.getBasicType() == BasicUnitType::Infantry) {
        menuinfo.wID = IDM_CPINFANTRYSUBMENU_FIRSTITEM + infantry_id;
        menuinfo.dwItemData = (int) t;
        InsertMenuItem(CP_infantry_submenu, menuinfo.wID, FALSE, &menuinfo);
        infantry_id++;
      }
    
      if(unitType.getBasicType() == BasicUnitType::Cavalry) {
        menuinfo.wID = IDM_CPCAVALRYSUBMENU_FIRSTITEM + cavalry_id;
        menuinfo.dwItemData = (int) t;
        InsertMenuItem(CP_cavalry_submenu, menuinfo.wID, FALSE, &menuinfo);
        cavalry_id++;
      }
    
      if(unitType.getBasicType() == BasicUnitType::Artillery) {
        menuinfo.wID = IDM_CPARTILLERYSUBMENU_FIRSTITEM + artillery_id;
        menuinfo.dwItemData = (int) t;
        InsertMenuItem(CP_artillery_submenu, menuinfo.wID, FALSE, &menuinfo);
        artillery_id++;
      }
    
      if(unitType.getBasicType() ==BasicUnitType::Special) {
        menuinfo.wID = IDM_CPSPECIALSUBMENU_FIRSTITEM + special_id;
        menuinfo.dwItemData = (int) t;
        InsertMenuItem(CP_special_submenu, menuinfo.wID, FALSE, &menuinfo);
        special_id++;
      }


    }

//            if(sp && (t == sp->getUnitType())) index = i;
//            i++;
//        }
  }

//    if(index < combo.getNItems()) combo.set(index);

  int nothing = 0;

}






void
BattleOOBClass::OnCPMenu(int wID) {

  ASSERT(d_ob);

  // see which item's been selected
  // the sortcut menus are all have zero-based indexes
  switch(wID) {

    case IDM_CPMENU_DELETE : { // Delete CP
      if(MenuSelectCP != NULL) {
        Writer lock(d_ob->ob());
        BattleCP * parent_cp = MenuSelectCP->parent();
        // remove from map
        m_DeployUser->OnRemoveUnit(MenuSelectCP);
        // remove form OB
        DeleteCP(MenuSelectCP);
//                d_ob->removeBattleUnits(MenuSelectCP, true);

        BobUtility::initUnitTypeFlags(d_batData);
                
        TV_ITEM item;
        item.hItem = MenuSelectHItem;
        item.mask = TVIF_PARAM;
        TreeView_GetItem(treeview_hwnd, &item);
        UnitDescriptor * desc = static_cast<UnitDescriptor *> ( (LPVOID) item.lParam);
        delete desc;                

        InitTreeView();
        if(parent_cp) EnsureVisibleAndExpanded(parent_cp);

        MenuSelectCP = NULL;
        MenuSelectHItem = NULL;
        CurrentSelectionCP = NULL;
        CurrentSelectionHItem = NULL;

        return;
      }
    }

    case IDM_CPMENU_DELETESP : { // Delete all SPs
      if(MenuSelectCP != NULL) {
        Writer lock(d_ob->ob());
        // remove from map
        m_DeployUser->OnRemoveUnit(MenuSelectCP);
        // delete SPs
        RemoveSPs(MenuSelectCP);
//                d_ob->removeStrengthPoints(MenuSelectCP);
        BobUtility::initUnitTypeFlags(d_batData);
        // redeploy to map
        m_DeployUser->OnDeployUnit(MenuSelectCP);
        InitTreeView();
        EnsureVisibleAndExpanded(MenuSelectCP);

        return;
      }
    }

    case IDM_CPMENU_NEWCP : { // New CP
      if(MenuSelectCP != NULL) {
        Writer lock(d_ob->ob());
        RefBattleCP newCP = d_ob->createUnit();
        Side side = MenuSelectCP->getSide();
        newCP->setSide(side);
        Nationality nationality = MenuSelectCP->getNation();
        newCP->setNation(nationality);
        Rank rank = MenuSelectCP->getRank();
        // demote rank to next level, if above division
        rank.demote();  //mtc122 - question why this demotion
        newCP->setRank(rank.getRankEnum() );
        newCP->visibility(Visibility::Full);
        // add CP as child of selected CP
        d_ob->makeUnitName(newCP);
        d_ob->createLeader(newCP);
        d_ob->addChild(MenuSelectCP, newCP);
        BatUnitUtils::InitUnits(d_ob);
        BobUtility::initEditUnitTypeFlags(d_batData);  //mtc122
        InitTreeView();
        EnsureVisibleAndExpanded(MenuSelectCP);
        MenuSelectCP = NULL;
//                TreeView_EnsureVisible(treeview_hwnd, MenuSelectHItem);
//                TreeView_Expand(treeview_hwnd, MenuSelectHItem, TVE_EXPAND);
        return;
      }
    }

    case IDM_CPMENU_NEWSP : { // New SP popup
      return;
    }


      /*
        Enter 'MoveCPTo' mode..
        The next SelChange WM_NOTIFY message to arrive at the treeview
        will siginify which CP this one is to be reattatched under
      */
		case IDM_CPMENU_MOVECPTO : {
			movemode = true;
			movemode_CP = MenuSelectCP;
			SetCursor(LoadCursor(NULL, IDC_CROSS));
			return;
		}


    default : {
      FORCEASSERT("Oops - Invalid menu item selected in CP shortcut menu");
      return;
    }

  } // end of switch

}




void
BattleOOBClass::OnSPMenu(int wID) {



  switch(wID) {

    case IDM_SPMENU_DELETE : { // Delete SP
      if(MenuSelectSP != NULL) {

        Writer lock(d_ob->ob());
        // remove from map
        BattleCP * parent = MenuSelectSP->parent();
        BattleMeasure::HexCord hex = parent->hex();
        m_DeployUser->OnRemoveUnit(parent);
        // delete SPs
        RemoveSP(MenuSelectSP);
        // redeploy to map
        BobUtility::initUnitTypeFlags(d_batData);

        CurrentSelectionCP = parent;
        parent->hex(hex);
        m_DeployUser->OnDeployUnit(parent);
        InitTreeView();
        EnsureVisibleAndExpanded(parent);

        return;
      }
    }

  }

}



void
BattleOOBClass::OnCPInfantrySubMenu(int wID) {
  ASSERT(d_ob);

  if(MenuSelectCP == NULL) return;
  // remove from map
//    m_DeployUser->OnRemoveUnit(MenuSelectCP);  //mtc122
//mtc122 - for some reason I commented out the line above in testing
  Writer lock(d_ob->ob());

  // set up structure fro recieving info about menu selection
  MENUITEMINFO menuinfo;
  menuinfo.cbSize = sizeof(MENUITEMINFO);
  menuinfo.fMask = MIIM_DATA;

  // get the unit type data
  GetMenuItemInfo(CP_infantry_submenu, wID, FALSE, &menuinfo);
  BasicUnitType::value unit_type = (BasicUnitType::value) menuinfo.dwItemData;

  /*
    Create generic SP
  */
  ISP generic_sp = d_ob->ob()->createStrengthPoint();
  d_ob->ob()->attachStrengthPoint(MenuSelectCP->generic(), generic_sp);

//    MenuSelectCP->generic()->makeInfantryType(true);

  /*
    Create battle SP
  */
    
  RefBattleSP sp = d_ob->createStrengthPoint();
  sp->setUnitType(unit_type);
  sp->visibility(Visibility::Full);
  d_ob->attachStrengthPoint(MenuSelectCP, sp);

  BatUnitUtils::InitUnits(d_batData);
  BobUtility::initUnitTypeFlags(d_batData);
//mtc122 maybe we need to do something with this one as well
  // redeploy to map    
  m_DeployUser->OnDeployUnit(MenuSelectCP);

  InitTreeView();
  EnsureVisibleAndExpanded(MenuSelectCP);
  MenuSelectCP = NULL;


//    TreeView_EnsureVisible(treeview_hwnd, MenuSelectHItem);
//    TreeView_Expand(treeview_hwnd, MenuSelectHItem, TVE_EXPAND);
    
}


void
BattleOOBClass::OnCPCavalrySubMenu(int wID) {
  ASSERT(d_ob);

  if(MenuSelectCP == NULL) return;

  Writer lock(d_ob->ob());

  // set up structure fro recieving info about menu selection
  MENUITEMINFO menuinfo;
  menuinfo.cbSize = sizeof(MENUITEMINFO);
  menuinfo.fMask = MIIM_DATA;

  // get the unit type data
  GetMenuItemInfo(CP_cavalry_submenu, wID, FALSE, &menuinfo);
  BasicUnitType::value unit_type = (BasicUnitType::value) menuinfo.dwItemData;

//    m_DeployUser->OnRemoveUnit(MenuSelectCP);   //mtc122

//    MenuSelectCP->generic()->makeCavalryType(true);
  RefBattleSP sp = d_ob->createStrengthPoint();
  sp->setUnitType(unit_type);
  sp->visibility(Visibility::Full);
  d_ob->attachStrengthPoint(MenuSelectCP, sp);

  m_DeployUser->OnDeployUnit(MenuSelectCP);
  BatUnitUtils::InitUnits(d_ob);
  BobUtility::initUnitTypeFlags(d_batData);

  InitTreeView();
  EnsureVisibleAndExpanded(MenuSelectCP);
  MenuSelectCP = NULL;

}


void
BattleOOBClass::OnCPArtillerySubMenu(int wID) {
  ASSERT(d_ob);

  if(MenuSelectCP == NULL) return;

  Writer lock(d_ob->ob());

  // set up structure fro recieving info about menu selection
  MENUITEMINFO menuinfo;
  menuinfo.cbSize = sizeof(MENUITEMINFO);
  menuinfo.fMask = MIIM_DATA;

  // get the unit type data
  GetMenuItemInfo(CP_artillery_submenu, wID, FALSE, &menuinfo);
  BasicUnitType::value unit_type = (BasicUnitType::value) menuinfo.dwItemData;

//   m_DeployUser->OnRemoveUnit(MenuSelectCP);  //mtc122

//    MenuSelectCP->generic()->makeArtilleryType(true);
  RefBattleSP sp = d_ob->createStrengthPoint();
  sp->setUnitType(unit_type);
  sp->visibility(Visibility::Full);
  d_ob->attachStrengthPoint(MenuSelectCP, sp);

  m_DeployUser->OnDeployUnit(MenuSelectCP);
  BatUnitUtils::InitUnits(d_ob);
  BobUtility::initUnitTypeFlags(d_batData);

  InitTreeView();
  EnsureVisibleAndExpanded(MenuSelectCP);
  MenuSelectCP = NULL;

}



void
BattleOOBClass::OnCPSpecialSubMenu(int wID) {
  ASSERT(d_ob);

  if(MenuSelectCP == NULL) return;

  Writer lock(d_ob->ob());

  // set up structure fro recieving info about menu selection
  MENUITEMINFO menuinfo;
  menuinfo.cbSize = sizeof(MENUITEMINFO);
  menuinfo.fMask = MIIM_DATA;

  // get the unit type data
  GetMenuItemInfo(CP_special_submenu, wID, FALSE, &menuinfo);
  BasicUnitType::value unit_type = (BasicUnitType::value) menuinfo.dwItemData;

//    m_DeployUser->OnRemoveUnit(MenuSelectCP);  //mtc122
    
  RefBattleSP sp = d_ob->createStrengthPoint();
  sp->setUnitType(unit_type);
  sp->visibility(Visibility::Full);
  d_ob->attachStrengthPoint(MenuSelectCP, sp);

  m_DeployUser->OnDeployUnit(MenuSelectCP);
  BatUnitUtils::InitUnits(d_ob);
  BobUtility::initUnitTypeFlags(d_batData);

  InitTreeView();
  EnsureVisibleAndExpanded(MenuSelectCP);
  MenuSelectCP = NULL;


}


/*
  Delete a CP - including all its's childern CPs and SPs
*/

void
BattleOOBClass::DeleteCP(RefBattleCP cp) {

  ASSERT(cp != NoBattleCP);

  // make sure CP isn't top of either side
  if(cp->parent() == NULL) return;

  // call recursively until all children removed
  while(cp->child() ) DeleteCP(cp->child() );

  if(cp->sp() ) RemoveSPs(cp);

  d_ob->removeBattleUnits(cp, true);

}


/*
  Delete all strength points under the given CP
  Both BATTLE & GENERIC units are deleted
*/

void
BattleOOBClass::RemoveSPs(RefBattleCP battle_cp) {

  ASSERT(battle_cp != NoBattleCP);
  if(! battle_cp->sp() ) return;

  OrderBattle * generic_ob = d_ob->ob();
  Writer lock(generic_ob);

  BattleSP * battle_sp;

  // get first sp
  while(battle_cp->sp() ) {

    battle_sp = battle_cp->sp();

    // get generic units
    ISP generic_sp = battle_sp->generic();
    RefGenericCP generic_cp = battle_cp->generic();

    /*
      Delete Battle SP
    */

    d_ob->removeStrengthPoint(battle_sp);

    /*
      Delete Generic SP
    */

    // detach generic_sp from generic_cp
    generic_ob->detachStrengthPoint(generic_cp, generic_sp);

    // delete genetic_sp
    generic_ob->deleteStrengthPoint(generic_sp);        
  }

}




void
BattleOOBClass::RemoveSP(RefBattleSP battle_sp) {

  if(! battle_sp) return;

  OrderBattle * generic_ob = d_ob->ob();
  Writer lock(generic_ob);

  // get generic units
  ISP generic_sp = battle_sp->generic();
  RefGenericCP generic_cp = battle_sp->parent()->generic();

  // delete battle_sp
  d_ob->removeStrengthPoint(battle_sp);
  // detach generic_sp from generic_cp
  generic_ob->detachStrengthPoint(generic_cp, generic_sp);
  // delete genetic_sp
  generic_ob->deleteStrengthPoint(generic_sp);
}



void
BattleOOBClass::RemoveUnit(const RefBattleCP cp) {
  ASSERT(d_ob);

  ASSERT(cp != NoBattleCP);

  Writer lock(d_ob->ob());

  /*
   * Unlink the strength points
   */

//        d_ob->ob()->deleteSPchain(cp->generic() );
  d_ob->removeStrengthPoints(cp);

  ASSERT(cp->sp() == NoBattleSP);

  /*
   * Unlink the leader
   */

  RefGLeader Leader = cp->leader();
  if(Leader != NoGLeader)
  {
    // unassignLeader(cpi);
    d_ob->ob()->detachLeader(Leader);
  }

  /*
   * Unlink the CommandPosition
   */

  d_ob->unlinkUnit(cp);

  d_ob->ob()->deleteCommand(cp->generic() );

}






char *
BattleOOBClass::GetFacingString(BattleMeasure::HexPosition::Facing facing) {

  switch(facing) {

    case BattleMeasure::HexPosition::NorthEastPoint : return "NorthEastPoint";
    case BattleMeasure::HexPosition::North : return "North";
    case BattleMeasure::HexPosition::NorthWestPoint : return "NorthWestPoint";
    case BattleMeasure::HexPosition::SouthWestPoint : return "SouthWestPoint";
    case BattleMeasure::HexPosition::South : return "South";
    case BattleMeasure::HexPosition::SouthEastPoint : return "SouthEastPoint";

    default : {
      FORCEASSERT("Oops - OOB function given invalid facing");
      return "Invalid Facing"; }

  } // end of switch

}


char *
BattleOOBClass::GetRankString(RankEnum rank) {

  switch(rank) {

    case Rank_God : return "God";
    case Rank_President : return "President";
    case Rank_ArmyGroup : return "ArmyGroup";
    case Rank_Army : return "Army";
    case Rank_Corps : return "Corps";
    case Rank_Division : return "Division";

    default : {
      FORCEASSERT("Oops - OOB function given invalid rank");
      return "Invalid Rank"; }

  } // end of switch

}


char *
BattleOOBClass::GetCPFormationString(CPFormation formation) {

  switch(formation) {

    case CPF_March : return "March";
    case CPF_Massed : return "Massed";
    case CPF_Deployed : return "Deployed";
    case CPF_Extended : return "Extended";
        
    default : {
      FORCEASSERT("Oops - OOB function given invalid CP formation");
      return "Invalid CP Formation"; }

  } // end of switch

}



char *
BattleOOBClass::GetCPDeployString(CPDeployHow deploy) {

  switch(deploy) {

    case CPD_DeployCenter : return "Center";
    case CPD_DeployRight : return "Right";
    case CPD_DeployLeft : return "Left";
        
    default : {
      FORCEASSERT("Oops - OOB function given invalid CP deployment");
      return "Invalid CP Deployment"; }

  } // end of switch

}


char *
BattleOOBClass::GetCPLineString(CPLineHow line) {

  switch(line) {

    case CPL_Line : return "Line";
    case CPL_RefuseR : return "RefuseRight";
    case CPL_RefuseL : return "RefuseLeft";
    case CPL_EchelonR : return "EchelonRight";
    case CPL_EchelonL : return "EchelonLeft";
        
    default : {
      FORCEASSERT("Oops - OOB function given invalid CP line");
      return "Invalid CP Line"; }

  } // end of switch

}



char *
BattleOOBClass::GetSPFormationString(SPFormation formation) {

  switch(formation) {

    case SP_MarchFormation : return "March / Limbered";
    case SP_ColumnFormation : return "Column";
    case SP_ClosedColumnFormation : return "ClosedColumn / Unlimbered";
    case SP_LineFormation : return "Line";
    case SP_SquareFormation : return "Square";
        
    default : {
      FORCEASSERT("Oops - OOB function given invalid CP formation");
      return "Invalid CP formation"; }

  } // end of switch

}


char *
BattleOOBClass::GetSpecializationString(int s) {

  switch(s) {

    case 0 : return "Cavalry Specialist";
    case 1 : return "Artillery Specialist";
    case 2 : return "Non-Specialist";

    default : {
      FORCEASSERT("Oops - OOB function given invalid leader specialization");
      return "Invalid Specialization"; }
  }
}


char *
BattleOOBClass::GetSPFacingString(int facing) {

  switch(facing) {

    case 0 : return "East";
    case 1 : return "NorthEastPoint";
    case 2 : return "NorthEastFace";
    case 3 : return "North";
    case 4 : return "NorthWestFace";
    case 5 : return "NorthWestPoint";
    case 6 : return "West";
    case 7 : return "SouthWestPoint";
    case 8 : return "SouthWestFace";
    case 9 : return "South";
    case 10 : return "SouthEastFace";
    case 11 : return "SouthEastPoint";
        
    default : {
      FORCEASSERT("Oops - OOB function given invalid SP facing");
      return "Invalid SP Facing"; }

  } // end of switch

}


BattleMeasure::HexPosition::Facing
BattleOOBClass::IndexToFacing(int index) {

  switch(index) {

    case 0 : return BattleMeasure::HexPosition::East;
    case 1 : return BattleMeasure::HexPosition::NorthEastPoint;
    case 2 : return BattleMeasure::HexPosition::NorthEastFace;
    case 3 : return BattleMeasure::HexPosition::North;
    case 4 : return BattleMeasure::HexPosition::NorthWestFace;
    case 5 : return BattleMeasure::HexPosition::NorthWestPoint;
    case 6 : return BattleMeasure::HexPosition::West;
    case 7 : return BattleMeasure::HexPosition::SouthWestPoint;
    case 8 : return BattleMeasure::HexPosition::SouthWestFace;
    case 9 : return BattleMeasure::HexPosition::South;
    case 10 : return BattleMeasure::HexPosition::SouthEastFace;
    case 11 : return BattleMeasure::HexPosition::SouthEastPoint;

    default : {
      FORCEASSERT("Oops - Invalid index in IndexToFacing");
      return 0;
    }
  }
}    


int
BattleOOBClass::FacingToIndex(BattleMeasure::HexPosition::Facing facing) {

  switch(facing) {

    case BattleMeasure::HexPosition::East : return 0;
    case BattleMeasure::HexPosition::NorthEastPoint : return 1;
    case BattleMeasure::HexPosition::NorthEastFace : return 2;
    case BattleMeasure::HexPosition::North : return 3;
    case BattleMeasure::HexPosition::NorthWestFace : return 4;
    case BattleMeasure::HexPosition::NorthWestPoint : return 5;
    case BattleMeasure::HexPosition::West : return 6;
    case BattleMeasure::HexPosition::SouthWestPoint : return 7;
    case BattleMeasure::HexPosition::SouthWestFace : return 8;
    case BattleMeasure::HexPosition::South : return 9;
    case BattleMeasure::HexPosition::SouthEastFace : return 10;
    case BattleMeasure::HexPosition::SouthEastPoint : return 11;

    default : {
      FORCEASSERT("Oops - Invalid facing in FacingToIndex");
      return 0;
    }
  }
}    




bool BattleOOBClass::isVisible() const
{
  return IsWindowVisible(hwnd) != 0;
}

void BattleOOBClass::show()
{
  ShowWindow(hwnd, SW_SHOW);
}

void BattleOOBClass::hide()
{
  ShowWindow(hwnd, SW_HIDE);
}

void BattleOOBClass::reset(BattleOB* ob)
{
  d_ob = ob;
  InitTreeView();
  CurrentSelectionCP = NoBattleCP;
  MenuSelectCP = NoBattleCP;
}

void BattleOOBClass::destroy()
{
  DestroyWindow(hwnd);
}


/*----------------------------------------------------------------------
 * $Log$
 * Revision 1.4  2002/11/24 13:17:48  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * mtc patch 122   //mtc122
 * added call to include clog.hpp for debug
 * added call to initEditUnitTypeFlags
 *----------------------------------------------------------------------
 */
