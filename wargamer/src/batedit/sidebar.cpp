/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Side Window (Editor Version)
 * Used to display information about what is under the cursor
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "sidebar.hpp"
// #include "generic.hpp"
#include "palwind.hpp"
#include "wmisc.hpp"
#include "palette.hpp"
#include "batcord.hpp"
#include "b_terr.hpp"
#include "hexdata.hpp"
#include "scenario.hpp"
#include "hexmap.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "bu_draw.hpp"
//#include <function.h>           // Use templated !=
#include <utility>

namespace BattleWindows_Internal
{

/*------------------------------------------------------------
 * Private Implementation class
 */

class BattleSideBarImp
:       public PaletteWindow,
        public WindowBaseND,
        public CustomBorderWindow
{
                PBattleWindows          d_batWind;
                CPBattleData            d_batData;
                BattleMeasure::HexCord d_hex;
                char * d_ModeText;

        public:
                BattleSideBarImp(RPBattleWindows batWind, const CPBattleData& battleData);
                ~BattleSideBarImp();

                void setHex(const BattleMeasure::HexCord& hex);
                void SetModeText(char * text);

        private:
                // disable copying
                BattleSideBarImp(const BattleSideBarImp&);
                BattleSideBarImp& operator = (const BattleSideBarImp&);

                LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

                BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
                void onSize(HWND hwnd, UINT state, int cx, int cy);
                void onPaint(HWND hWnd);
                // void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
                // void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
                // void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);

                void redraw();
};

#if 0   // def DEBUG
static CustomBorderInfo borderColors = {
        PALETTERGB(189, 165, 123),
        PALETTERGB(255, 247, 215),
        PALETTERGB(230, 214, 123),
        PALETTERGB(115, 90, 66)
};
#endif

BattleSideBarImp::BattleSideBarImp(RPBattleWindows batWind, const CPBattleData& battleData)
:       PaletteWindow(),
        WindowBaseND(),
        CustomBorderWindow(scenario->getBorderColors()),
        d_batWind(batWind),
        d_batData(battleData),
        d_hex(0,0)
{
        d_ModeText = "\n";
    
        HWND hParent = d_batWind->hwnd();
        ASSERT(hParent != NULL);

        HWND hWnd = createWindow(
                0,
                // className(),                    // Class Name
                "BattleEditor Toolbar",                                                       // Window Name
                WS_CHILD |
                WS_CLIPSIBLINGS,                        /* Style */
                0, 0, 0, 0,                                     // Position and width will get set from batwind
                hParent,                                        /* parent window */
                NULL
                // wndInstance(hParent)
        );

        ShowWindow(getHWND(), SW_SHOW);
}

inline BattleSideBarImp::~BattleSideBarImp()
{
}

LRESULT BattleSideBarImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        LRESULT l;
        if(PaletteWindow::handlePalette(hWnd, msg, wParam, lParam, l))
                return l;

        switch(msg)
        {
                HANDLE_MSG(hWnd, WM_CREATE,                     onCreate);
                // HANDLE_MSG(hWnd, WM_DESTROY,                 onDestroy);
           HANDLE_MSG(hWnd, WM_PAINT,                           onPaint);
                // HANDLE_MSG(hWnd, WM_ERASEBKGND,              onEraseBk);
                // HANDLE_MSG(hWnd, WM_COMMAND,                 onCommand);
                HANDLE_MSG(hWnd, WM_SIZE,                               onSize);
                // HANDLE_MSG(hWnd, WM_GETMINMAXINFO,   onGetMinMaxInfo);
                // HANDLE_MSG(hWnd, WM_NOTIFY,                  onNotify);
                // HANDLE_MSG(hWnd, WM_CLOSE,                           onClose);
                // HANDLE_MSG(hWnd, WM_HSCROLL, onHScroll);
                // HANDLE_MSG(hWnd, WM_VSCROLL, onVScroll);
                // HANDLE_MSG(hWnd, WM_LBUTTONDOWN,             onLButtonDown);
                // HANDLE_MSG(hWnd, WM_LBUTTONUP,               onLButtonUp);
                // HANDLE_MSG(hWnd, WM_MOUSEMOVE,               onMouseMove);

        default:
                // return SimpleChildWindow::procMessage(hWnd, msg, wParam, lParam);
                return defProc(hWnd, msg, wParam, lParam);
        }
}


BOOL BattleSideBarImp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
        return TRUE;
}

void BattleSideBarImp::onSize(HWND hwnd, UINT state, int cx, int cy)
{
}

void BattleSideBarImp::onPaint(HWND hWnd)
{
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);

        HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
        RealizePalette(hdc);



        RECT r;
        GetClientRect(hWnd, &r);
        
        HBRUSH hBrush = static_cast<HBRUSH>(GetStockObject(GRAY_BRUSH));
        FillRect(hdc, &r, hBrush);


        const BattleTerrainHex& terrain = d_batData->getTerrain(d_hex);
        const TerrainTable& tTable = d_batData->terrainTable();

        const TerrainTable::Info& tInfo = tTable[terrain.d_terrainType];


        wTextPrintf(hdc, 1, 1, d_ModeText);

        wTextPrintf(hdc, 1, 20, "Hex: %d,%d",
                (int) d_hex.x(),
                (int) d_hex.y());

        wTextPrintf(hdc, 1, 40, "Type: %d %s",
                (int) terrain.d_terrainType,
                (const char*) tInfo.name());

        wTextPrintf(hdc, 1, 60, "Path1: %d %s",
                (int) terrain.d_path1.d_type,
                (const char*) tTable.pathName(terrain.d_path1.d_type));
        wTextPrintf(hdc, 1, 80, "Edge1: %d", (int) terrain.d_path1.d_edges);

        wTextPrintf(hdc, 1, 100, "Path2: %d %s", 
                (int) terrain.d_path2.d_type,
                (const char*) tTable.pathName(terrain.d_path2.d_type));
        wTextPrintf(hdc, 1, 120, "Edge2: %d", (int) terrain.d_path2.d_edges);

        wTextPrintf(hdc, 1, 140, "Height: %d", (int) terrain.d_height);
        // wTextPrintf(hdc, 1, 140, "Offset: %d", (int) terrain.d_offset);
        wTextPrintf(hdc, 1, 160, "Coast: %d", (int) terrain.d_coast);

        wTextPrintf(hdc, 1, 180, "Edge: %d %s", 
                (int) terrain.d_edge.d_type,
                (const char*) tTable.edgeName(terrain.d_edge.d_type));
        wTextPrintf(hdc, 1, 200, "Edge: %d", (int) terrain.d_edge.d_edges);

        // TextOut(hdc, 1, 1, message, messageLength);

        /*
         * Display units in hex
         */

        const BattleHexMap* hexMap = d_batData->hexMap();
        BattleHexMap::const_iterator lower;
        BattleHexMap::const_iterator higher;
        if(hexMap->find(d_hex, lower, higher))
        {
                class ShowUnit : public UnitDrawer
                {
                        public:
                                ShowUnit(HDC hdc, int y, const BattleOB* ob) : d_hdc(hdc), d_y(y), d_ob(ob) { }
                                virtual ~ShowUnit() { }

                                // Implements UnitDrawer

                                virtual void draw(const BattleSP* sp)
                                {
                                        wTextPrintf(d_hdc, 1, d_y, "SP: %d %d %s",
                                                d_ob->getIndex(sp),
                                                static_cast<int>(sp->getUnitType()),
                                                d_ob->spName(sp) );
                                        nextline();
                                }

                                virtual void draw(const BattleCP* cp)
                                {
                                        wTextPrintf(d_hdc, 1, d_y, "CP: %s", cp->getName());
                                        nextline();
                                }

                                virtual void draw(const BattleUnit* unit)
                                {
                                        wTextPrintf(d_hdc, 1, d_y, "Unknown BattleUnit type at %p", unit);
                                        nextline();
                                }

                        private:
                                void nextline() { d_y += 20; }

                        private:
                                HDC d_hdc;
                                int d_y;
                                const BattleOB* d_ob;
                };

                ShowUnit showUnit(hdc, 260, d_batData->ob());

                while(lower != higher)
                {
                        hexMap->unit(lower)->draw(&showUnit);
                        ++lower;
                }
        }


        drawThinBorder(hdc, r);

        SelectPalette(hdc, oldPal, FALSE);

        EndPaint(hWnd, &ps);
}

/*
 * Get info window redrawn
 */

void BattleSideBarImp::redraw()
{
        InvalidateRect(getHWND(), NULL, TRUE);
}


void BattleSideBarImp::setHex(const BattleMeasure::HexCord& hex)
{
//        if(hex != d_hex)
        if(!(hex == d_hex))
        {
                d_hex = hex;
                redraw();
        }
}


void
BattleSideBarImp::SetModeText(char * text) {

    d_ModeText = text;

}


/*------------------------------------------------------------
 * Insulated implementation
 */

BattleSideBar::BattleSideBar(RPBattleWindows batWind, RCPBattleData battleData)
:       d_imp(new BattleSideBarImp(batWind, battleData))
{
        ASSERT(d_imp != 0);
}

BattleSideBar::~BattleSideBar()
{
        delete d_imp;
}

HWND BattleSideBar::getHWND() const
{
        ASSERT(d_imp != 0);
        return d_imp->getHWND();
}

void BattleSideBar::setHex(const BattleMeasure::HexCord& hex)
{
        ASSERT(d_imp != 0);
        d_imp->setHex(hex);
}

void
BattleSideBar::SetModeText(char * text) {

    ASSERT(d_imp != 0);
    d_imp->SetModeText(text);
}

};      // namespace BattleWindows_Internal

