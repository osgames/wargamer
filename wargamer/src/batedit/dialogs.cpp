/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
BATTLE EDITOR DIALOGS
*/


#include "stdinc.hpp"
#include "dialogs.hpp"



// Current working mode of editor
EditorDialogEnum EditorMode;

// Current creation mode for paths
PathsModeEnum PathsMode;
bool PathsSunken;

// Current creation mode for hexes
HexModeEnum TerrainHexMode;
HexModeEnum HeightsHexMode;
HexModeEnum SettlementsHexMode;

// height area data
BattleMeasure::HexCord heightarea_center;
BattleMeasure::HexCord heightarea_size;
int heightarea_height;

// labelling data
BattleMeasure::HexCord label_hex;

// Terrain Creation Types
TerrainType PrimaryTerrain;
TerrainType SecondaryTerrain;
// Terrain Area Creation SIze
int TerrainAreaSize;

// Settlement Creation Type
int SettlementsType;
// Settlement Creation Status
SettlementStatusEnum SettlementsStatus;

EdgesTypeEnum EdgesType;
EdgeInfo EdgeBits;

bool erase_terrain;
bool erase_heights;
bool erase_paths1;
bool erase_paths2;
bool erase_edges;
bool erase_buildings;
bool erase_labels;

BattleMeasure::HexCord CropSelectSize;
BattleMeasure::HexCord CropSelectCenter;

BattleMeasure::HexCord PathFindStartHex;
BattleMeasure::HexCord PathFindEndHex;
bool pathfind_avoidprohibited;
bool pathfind_useroads;
bool pathfind_terraincost;
bool pathfind_uselevelground;
int pathfind_directness;

char * ModeText = "\n";




/*
Global function to pass control input up to parent
*/
void
GenericOnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {

    // handle any basic functions
    if(hwndCtl != NULL) switch(codeNotify) {


    }


    FORWARD_WM_COMMAND(GetParent(hwnd), id, hwndCtl, codeNotify, PostMessage);
    
}


/*-----------------------------------------------------------------------------------

        TERRAIN DIALOG

-----------------------------------------------------------------------------------*/




/*
Constructor
*/
EditorTerrainDialogClass::EditorTerrainDialogClass(HWND parent, HINSTANCE  instance) {

    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_TERRAIN), parent, (DLGPROC) EditorTerrainDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}



/*
Destructor
*/
EditorTerrainDialogClass::~EditorTerrainDialogClass(void) { }



/*
Initialise dialog's controls, etc.
*/
void
EditorTerrainDialogClass::InitControls(void) {

    HWND ctrl;
//    int index;
    // init the primary terrain combo
    ctrl = GetDlgItem(hwnd, IDC_COMBO_PRIMARYTERRAIN);
    ASSERT(ctrl != NULL);
        SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Clear");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Water");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Orchard");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Vineyard");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "LightWood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "DenseWood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "PloughedField");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "CornField");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "WheatField");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Rough");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Marsh");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Cemetary");

        SendMessage(ctrl, CB_SETCURSEL, (WPARAM) 0, 0);


    // init the secondary terrain combo
    ctrl = GetDlgItem(hwnd, IDC_COMBO_SECONDARYTERRAIN);
    ASSERT(ctrl != NULL);
        SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Clear");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Water");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Orchard");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Vineyard");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "LightWood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "DenseWood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "PloughedField");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "CornField");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "WheatField");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Rough");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Marsh");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Cemetary");
        // tell combo box to redraw
        SendMessage(ctrl, CB_SETCURSEL, (WPARAM) 0, 0);

        
    // init the radio buttons
    ctrl = GetDlgItem(hwnd, IDC_RADIO_SINGLETERRAIN);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_INSERTTERRAIN);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_OVERWRITETERRAIN);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);

    // init the trackbar
    ctrl = GetDlgItem(hwnd, IDC_TRACKBAR_AREASIZE);
        SendMessage(ctrl, TBM_SETRANGE, (WPARAM) (BOOL) TRUE, (LPARAM) MAKELONG(1, 7) );
        SendMessage(ctrl, TBM_SETPOS, (WPARAM) (BOOL) TRUE, (LPARAM) (LONG) 4);

   
}



/*
Handle messages
*/
BOOL APIENTRY
EditorTerrainDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        // handle basic controls, or pass controls up to editor
        case WM_COMMAND : {
            int wID = LOWORD(wParam);
            switch(wID) {
                // process radio buttons
                case IDC_RADIO_SINGLETERRAIN : { TerrainHexMode = HEX_SINGLE; return 0; }
                case IDC_RADIO_INSERTTERRAIN : { TerrainHexMode = HEX_GROUP; return 0; }
                case IDC_RADIO_OVERWRITETERRAIN : { TerrainHexMode = HEX_CLUSTER; return 0; }
                // process combo boxes
                case IDC_COMBO_PRIMARYTERRAIN : {
                    // primary terrain
                    if(HIWORD(wParam) == CBN_SELCHANGE) {
                        PrimaryTerrain = static_cast <TerrainType> (SendMessage(HWND(lParam), CB_GETCURSEL, 0, 0) + 1);
                        return 0; }
                }
                case IDC_COMBO_SECONDARYTERRAIN : {
                    // secondary terrain
                    if(HIWORD(wParam) == CBN_SELCHANGE) {
                        SecondaryTerrain = static_cast <TerrainType> (SendMessage(HWND(lParam), CB_GETCURSEL, 0, 0) + 1);
                        return 0; }
                }
                
                default : {
                    HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0; }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_TERRAIN;
            ModeText = "Terrain Mode";
            return MA_ACTIVATE;
        }

        case WM_HSCROLL : {
            int scroll_code = (int) LOWORD(wParam);
            if(scroll_code == SB_THUMBPOSITION || scroll_code == SB_THUMBTRACK) { TerrainAreaSize = (short int) HIWORD(wParam); return 0; }
            else { HWND ctrl = (HWND) lParam; TerrainAreaSize = SendMessage(ctrl, TBM_GETPOS, 0, 0); return 0; }
        }
            


        default:
            return FALSE;
    }
    
}










/*-----------------------------------------------------------------------------------

        HEIGHTS DIALOG

-----------------------------------------------------------------------------------*/




/*
Constructor
*/
EditorHeightsDialogClass::EditorHeightsDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_HEIGHTS), parent, (DLGPROC) EditorHeightsDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorHeightsDialogClass::~EditorHeightsDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorHeightsDialogClass::InitControls(void) {

    HWND ctrl;
    
    ctrl = GetDlgItem(hwnd, IDC_RADIO_SINGLEHEIGHT);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_GROUPHEIGHT);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_CLUSTERHEIGHT);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_AREAHEIGHT);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
}


/*
Handle messages
*/
BOOL APIENTRY
EditorHeightsDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        // pass controls up to editor
        case WM_COMMAND : {
            int wID = LOWORD(wParam);
            switch(wID) {
                case IDC_RADIO_SINGLEHEIGHT : { HeightsHexMode = HEX_SINGLE; return 0; }
                case IDC_RADIO_GROUPHEIGHT : { HeightsHexMode = HEX_GROUP; return 0; }
                case IDC_RADIO_CLUSTERHEIGHT : { HeightsHexMode = HEX_CLUSTER; return 0; }
                case IDC_RADIO_AREAHEIGHT : { HeightsHexMode = HEX_AREA; return 0; }
                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0; }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_HEIGHTS;
            ModeText = "Heights Mode";
            return MA_ACTIVATE;
        }

        default:
            return FALSE;
    }
    
}













/*-----------------------------------------------------------------------------------

        PATHS DIALOG

-----------------------------------------------------------------------------------*/




/*
Constructor
*/
EditorPathsDialogClass::EditorPathsDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_PATHS), parent, (DLGPROC) EditorPathsDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorPathsDialogClass::~EditorPathsDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorPathsDialogClass::InitControls(void) {

    HWND ctrl;
    
    ctrl = GetDlgItem(hwnd, IDC_RADIO_ROAD);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_RIVER);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_TRACK);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_STREAM);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_CHECK_SUNKEN);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);

        PathsSunken = false;
}


/*
Handle messages
*/
BOOL APIENTRY
EditorPathsDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        case WM_COMMAND : {
            int wID = LOWORD(wParam);
            switch(wID) {
                case IDC_CHECK_SUNKEN : {
                    if(PathsSunken) PathsSunken = false;
                    else PathsSunken = true;
                    return 0;
                }
                case IDC_RADIO_ROAD : { PathsMode = PATHS_ROAD; return 0; }
                case IDC_RADIO_RIVER : { PathsMode = PATHS_RIVER; return 0; }
                case IDC_RADIO_TRACK : { PathsMode = PATHS_TRACK; return 0; }
                case IDC_RADIO_STREAM : { PathsMode = PATHS_STREAM; return 0; }
                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_PATHS;
            ModeText = "PathsMode";
            return MA_ACTIVATE;
        }
        
        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0; }

        default:
            return FALSE;
    }
    
}










/*-----------------------------------------------------------------------------------

        SETTLEMENTS DIALOG

-----------------------------------------------------------------------------------*/




/*
Constructor
*/
EditorSettlementsDialogClass::EditorSettlementsDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_SETTLEMENTS), parent, (DLGPROC) EditorSettlementsDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorSettlementsDialogClass::~EditorSettlementsDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorSettlementsDialogClass::InitControls(void) {

    HWND ctrl;

    ctrl = GetDlgItem(hwnd, IDC_COMBO_SETTLEMENTTYPE);
    ASSERT(ctrl != NULL);
        SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "0 : Town (small)");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "1 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "2 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "3 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "4 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "5 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "6 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "7 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "8 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "9 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "10 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "11 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "12 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "13 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "14 : Town");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "15 : Town (large)");
        
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "16 : Town Special (small)");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "17 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "18 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "19 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "20 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "21 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "22 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "23 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "24 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "25 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "26 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "27 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "28 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "29 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "30 : Town Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "31 : Town Special (large)");
        
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "32 : Village (small)");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "33 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "34 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "35 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "36 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "37 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "38 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "39 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "40 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "41 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "42 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "43 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "44 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "45 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "46 : Village");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "47 : Village (large)");
        
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "48 : Village Special (small)");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "49 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "50 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "51 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "52 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "53 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "54 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "55 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "56 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "57 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "58 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "59 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "60 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "61 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "62 : Village Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "63 : Village Special (large)");
        
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "64 : Farm (small)");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "65 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "66 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "67 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "68 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "69 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "70 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "71 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "72 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "73 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "74 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "75 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "76 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "77 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "78 : Farm");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "79 : Farm (large)");

        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "80 : Farm Special (small)");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "81 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "82 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "83 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "84 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "85 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "86 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "87 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "88 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "89 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "90 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "91 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "92 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "93 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "94 : Farm Special");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "95 : Farm Special (large)");
        
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "96 : Light Wood (small)");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "97 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "98 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "99 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "100 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "101 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "102 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "103 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "104 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "105 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "106 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "107 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "108 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "109 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "110 : Light Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "111 : Light Wood (large)");
        
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "112 : Dense Wood (small)");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "113 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "114 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "115 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "116 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "117 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "118 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "119 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "120 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "121 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "122 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "123 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "124 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "125 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "126 : Dense Wood");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "127 : Dense Wood (large)");

        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "128 : Track Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "129 : Track Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "130 : Track Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "131 : Track Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "132 : Track Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "133 : Track Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "134 : Track Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "135 : Track Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "136 : Track Bridge");
        
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "137 : Road Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "138 : Road Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "139 : Road Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "140 : Road Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "141 : Road Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "142 : Road Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "143 : Road Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "144 : Road Bridge");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "145 : Road Bridge");

		// roadside buildings
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "146 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "147 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "148 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "149 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "150 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "151 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "152 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "153 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "154 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "155 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "156 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "157 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "158 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "159 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "160 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "161 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "162 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "163 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "164 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "165 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "166 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "167 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "168 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "169 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "170 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "171 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "172 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "173 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "174 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "175 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "176 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "177 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "178 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "179 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "180 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "181 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "182 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "183 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "184 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "185 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "186 : Road-Side Cluster");
		SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "187 : Road-Side Cluster");

        // tell combo box to redraw
        SendMessage(ctrl, CB_SETCURSEL, (WPARAM) 0, 0);


    ctrl = GetDlgItem(hwnd, IDC_RADIO_SETTLEMENTNORMAL);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_SETTLEMENTBURNING);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_SETTLEMENTSMOKING);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_SETTLEMENTDESTROYED);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);

    ctrl = GetDlgItem(hwnd, IDC_RADIO_SINGLESETTLEMENT);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_RADIO_CLUSTERSETTLEMENT);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);

}


/*
Handle messages
*/
BOOL APIENTRY
EditorSettlementsDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        // pass controls up to editor
        case WM_COMMAND : {
            int wID = LOWORD(wParam);
            switch(wID) {
                case IDC_RADIO_SETTLEMENTNORMAL : { SettlementsStatus = SETTLEMENT_NORMAL; return 0; }
                case IDC_RADIO_SETTLEMENTBURNING : { SettlementsStatus = SETTLEMENT_BURNING; return 0; }
                case IDC_RADIO_SETTLEMENTSMOKING : { SettlementsStatus = SETTLEMENT_SMOKING; return 0; }
                case IDC_RADIO_SETTLEMENTDESTROYED : { SettlementsStatus = SETTLEMENT_DESTROYED; return 0; }
                case IDC_RADIO_SINGLESETTLEMENT : {
                    SettlementsHexMode = HEX_SINGLE;
                    HWND ctrl;
                    ctrl = GetDlgItem(hWnd, IDC_RADIO_SINGLESETTLEMENT);
                    SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
                    ctrl = GetDlgItem(hWnd, IDC_RADIO_CLUSTERSETTLEMENT);
                    SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
                    return 0; }
                case IDC_RADIO_CLUSTERSETTLEMENT : {
                    SettlementsHexMode = HEX_CLUSTER;
                    HWND ctrl;
                    ctrl = GetDlgItem(hWnd, IDC_RADIO_SINGLESETTLEMENT);
                    SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
                    ctrl = GetDlgItem(hWnd, IDC_RADIO_CLUSTERSETTLEMENT);
                    SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);
                    return 0; }
                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
                case IDC_COMBO_SETTLEMENTTYPE : {
                    // primary terrain
                    if(HIWORD(wParam) == CBN_SELCHANGE) {
                        SettlementsType = SendMessage(HWND(lParam), CB_GETCURSEL, 0, 0);
                        return 0; }
                }
            }
        }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_SETTLEMENTS;
            ModeText = "Settlements Mode";
            return MA_ACTIVATE;
        }

        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0; }

        default:
            return FALSE;
    }
    
}











/*
Constructor
*/
EditorEdgesDialogClass::EditorEdgesDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_EDGES), parent, (DLGPROC) EditorEdgesDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorEdgesDialogClass::~EditorEdgesDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorEdgesDialogClass::InitControls(void) {

    HWND ctrl;
    
    ctrl = GetDlgItem(hwnd, IDC_CHECK_NE);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_CHECK_E);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_CHECK_SE);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_CHECK_SW);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_CHECK_W);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);
    ctrl = GetDlgItem(hwnd, IDC_CHECK_NW);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);

    ctrl = GetDlgItem(hwnd, IDC_RADIO_WALLS);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_CHECKED, 0);

    ctrl = GetDlgItem(hwnd, IDC_RADIO_HEDGES);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);

    ctrl = GetDlgItem(hwnd, IDC_RADIO_FENCES);
        SendMessage(ctrl, BM_SETCHECK, (WPARAM) BST_UNCHECKED, 0);

}


/*
Handle messages
*/
BOOL APIENTRY
EditorEdgesDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        case WM_COMMAND : {
            int wID = LOWORD(wParam);
            switch(wID) {
                case IDC_RADIO_HEDGES : {
                    EdgesType = EDGE_HEDGE;
                    EdgeBits.d_type = 1;
                    return 0; }
                case IDC_RADIO_FENCES : {
                    EdgesType = EDGE_FENCE;
                    EdgeBits.d_type = 3;
                    return 0; }
                case IDC_RADIO_WALLS : {
                    EdgesType = EDGE_WALL;
                    EdgeBits.d_type = 2;
                    return 0; }

                case IDC_CHECK_E : {
                    if(EdgeBits.d_edges & 0x01) EdgeBits.d_edges -= 0x01;
                    else EdgeBits.d_edges |= 0x01;
                    return 0; }
                case IDC_CHECK_NE : {
                    if(EdgeBits.d_edges & 0x02) EdgeBits.d_edges -= 0x02;
                    else EdgeBits.d_edges |= 0x02;
                    return 0; }
                case IDC_CHECK_NW : {
                    if(EdgeBits.d_edges & 0x04) EdgeBits.d_edges -= 0x04;
                    else EdgeBits.d_edges |= 0x04;
                    return 0; }
                case IDC_CHECK_W : {
                    if(EdgeBits.d_edges & 0x08) EdgeBits.d_edges -= 0x08;
                    else EdgeBits.d_edges |= 0x08;
                    return 0; }
                case IDC_CHECK_SW : {
                    if(EdgeBits.d_edges & 0x10) EdgeBits.d_edges -= 0x10;
                    else EdgeBits.d_edges |= 0x10;
                    return 0; }
                case IDC_CHECK_SE : {
                    if(EdgeBits.d_edges & 0x20) EdgeBits.d_edges -= 0x20;
                    else EdgeBits.d_edges |= 0x20;
                    return 0; }
                
                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_EDGES;
            ModeText = "EdgesMode";
            return MA_ACTIVATE;
        }
        
        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0; }

        default:
            return FALSE;
    }
    
}

























/*
Constructor
*/
EditorLabelsDialogClass::EditorLabelsDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_LABELS), parent, (DLGPROC) EditorLabelsDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorLabelsDialogClass::~EditorLabelsDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorLabelsDialogClass::InitControls(void) {

    HWND ctrl;
    ctrl = GetDlgItem(hwnd, IDC_COMBO_POINTSSIDE);
    SendDlgItemMessage(ctrl, IDC_COMBO_POINTSSIDE, CB_RESETCONTENT, 0, 0);
    SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "VP for side 0");
    SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "VP for side 1");
    SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "VP for both sdes");

    SendMessage(ctrl, CB_SETCURSEL, (WPARAM) 2, 0);
}


void
EditorLabelsDialogClass::GetHexData(const BattleMeasure::HexCord& hex, BattleTerrainHex & maphex) {

    Hex = hex;

    // label text
    SetDlgItemText(hwnd, IDC_EDIT_LABEL, maphex.d_label);

    // victory points
    SetDlgItemInt(hwnd, IDC_EDIT_POINTSVALUE, maphex.d_victoryPoints, FALSE);

    // victory side
    HWND ctrl;
    ctrl = GetDlgItem(hwnd, IDC_COMBO_POINTSSIDE);
    SendMessage(ctrl, CB_SETCURSEL, (WPARAM) maphex.d_victorySide, 0);
}


/*
Handle messages
*/
BOOL APIENTRY
EditorLabelsDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        case WM_COMMAND : {
            int wID = LOWORD(wParam);
            int wNotifyCode = HIWORD(wParam);
/*
            switch(wNotifyCode) {
                case CBN_SELCHANGE : {
                }
*/
            switch(wID) {

                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_LABELS;
            ModeText = "Labels / Victory Points Mode";
            return MA_ACTIVATE;
        }
        
        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0;
        }

        default:
            return FALSE;
    }
    
}















/*
Constructor
*/
EditorEraseDialogClass::EditorEraseDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_ERASE), parent, (DLGPROC) EditorEraseDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorEraseDialogClass::~EditorEraseDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorEraseDialogClass::InitControls(void) {

//    HWND ctrl;

    erase_terrain = false;
    erase_heights = false;
	erase_paths1 = false;
	erase_paths2 = false;
    erase_edges = false;
    erase_buildings = false;
    erase_labels = false;

}


void
EditorEraseDialogClass::GetHexData(const BattleMeasure::HexCord& hex, BattleTerrainHex & maphex) {

}


/*
Handle messages
*/
BOOL APIENTRY
EditorEraseDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        case WM_COMMAND : {
            int wID = LOWORD(wParam);

                switch(wID) {

                case IDC_CHECK_ERASETERRAIN : {
                    if(!erase_terrain) erase_terrain = true;
                    else erase_terrain = false;
                    return 0; }
                case IDC_CHECK_ERASEHEIGHTS : {
                    if(!erase_heights) erase_heights = true;
                    else erase_heights = false;
                    return 0; }
                case IDC_CHECK_ERASEPATHS1 : {
                    if(!erase_paths1) erase_paths1 = true;
                    else erase_paths1 = false;
                    return 0; }
                case IDC_CHECK_ERASEPATHS2 : {
                    if(!erase_paths2) erase_paths2 = true;
                    else erase_paths2 = false;
                    return 0; }
                case IDC_CHECK_ERASEEDGES : {
                    if(!erase_edges) erase_edges = true;
                    else erase_edges = false;
                    return 0; }
                case IDC_CHECK_ERASEBUILDINGS : {
                    if(!erase_buildings) erase_buildings = true;
                    else erase_buildings = false;
                    return 0; }
                case IDC_CHECK_ERASELABELSANDPOINTS : {
                    if(!erase_labels) erase_labels = true;
                    else erase_labels = false;
                    return 0; }
                    
                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_ERASE;
            ModeText = "Erase Mode";
            return MA_ACTIVATE;
        }
        
        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0;
        }

        default:
            return FALSE;
    }
    
}








/*
Constructor
*/
EditorCropMapDialogClass::EditorCropMapDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_CROPMAP), parent, (DLGPROC) EditorCropMapDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorCropMapDialogClass::~EditorCropMapDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorCropMapDialogClass::InitControls(void) {

//    HWND ctrl;

   CropSelectSize   = BattleMeasure::HexCord(32,32);
   CropSelectCenter = BattleMeasure::HexCord(32,32);
}




/*
Handle messages
*/
BOOL APIENTRY
EditorCropMapDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        case WM_COMMAND : {
            int wID = LOWORD(wParam);

                switch(wID) {
                    
                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_CROPMAP;
            ModeText = "CropMap Mode";
            return MA_ACTIVATE;
        }
        
        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0;
        }

        default:
            return FALSE;
    }
    
}







/*
Constructor
*/
EditorDateTimeDialogClass::EditorDateTimeDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_DATETIME), parent, (DLGPROC) EditorDateTimeDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorDateTimeDialogClass::~EditorDateTimeDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorDateTimeDialogClass::InitControls(void) {

    HWND ctrl;
//    int index;
    // init the primary terrain combo
    ctrl = GetDlgItem(hwnd, IDC_COMBO_MONTH);
    ASSERT(ctrl != NULL);
        SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Janurary");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Feburary");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "March");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "April");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "May");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "June");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "July");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "August");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "September");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "October");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "November");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "December");

        SendMessage(ctrl, CB_SETCURSEL, (WPARAM) 0, 0);
}



void
EditorDateTimeDialogClass::SetDateAndTime(BattleMeasure::BattleTime date_time, BattleMeasure::BattleTime::Tick duration_ticks) {

    Greenius_System::Date date = date_time.date();
    Greenius_System::Time time = date_time.time();

    HWND ctrl;
    // date
    SetDlgItemInt(hwnd, IDC_EDIT_DATE, static_cast<int>(date.day() ), FALSE);
    // month
    ctrl = GetDlgItem(hwnd, IDC_COMBO_MONTH);
    SendMessage(ctrl, CB_SETCURSEL, (WPARAM) static_cast<int>(date.month()-date.January), 0);
    // year
    SetDlgItemInt(hwnd, IDC_EDIT_YEAR, static_cast<int>(date.year() ), FALSE);
    // hours
    SetDlgItemInt(hwnd, IDC_EDIT_HOURS, static_cast<int>(time.hours() ), FALSE);
    // mins
    SetDlgItemInt(hwnd, IDC_EDIT_MINS, static_cast<int>(time.minutes() ), FALSE);
    // secs
    SetDlgItemInt(hwnd, IDC_EDIT_SECS, static_cast<int>(time.seconds() ), FALSE);    

	int d_days, d_hours, d_mins;

	d_days = BattleMeasure::daysPerTick(duration_ticks);
	duration_ticks -= BattleMeasure::days(d_days);

	d_hours = BattleMeasure::hoursPerTick(duration_ticks);
	duration_ticks -= BattleMeasure::hours(d_hours);

	d_mins = BattleMeasure::minutesPerTick(duration_ticks);
	duration_ticks -= BattleMeasure::minutes(d_mins);


	SetDlgItemInt(hwnd, IDC_EDIT_DURATIONDAYS, d_days, FALSE);
	SetDlgItemInt(hwnd, IDC_EDIT_DURATIONHOURS, d_hours, FALSE);
	SetDlgItemInt(hwnd, IDC_EDIT_DURATIONMINS, d_mins, FALSE);

}




/*
Handle messages
*/
BOOL APIENTRY
EditorDateTimeDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        case WM_COMMAND : {
            int wID = LOWORD(wParam);

                switch(wID) {
                    
                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_DATETIME;
            ModeText = "Date&Time Mode";
            return MA_ACTIVATE;
        }
        
        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0;
        }

        default:
            return FALSE;
    }
    
}








/*
Constructor
*/
EditorWeatherDialogClass::EditorWeatherDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_WEATHER), parent, (DLGPROC) EditorWeatherDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorWeatherDialogClass::~EditorWeatherDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorWeatherDialogClass::InitControls(void) {

    HWND ctrl;
//    int index;
    // init the primary terrain combo
    ctrl = GetDlgItem(hwnd, IDC_COMBO_WEATHER);
    ASSERT(ctrl != NULL);
        SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Fine");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Light Rain");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Heavy Rain");
        SendMessage(ctrl, CB_SETCURSEL, (WPARAM) 0, 0);

    // init the primary terrain combo
    ctrl = GetDlgItem(hwnd, IDC_COMBO_GROUND);
    ASSERT(ctrl != NULL);
        SendMessage(ctrl, CB_RESETCONTENT, 0, 0);
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Fine");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Light Mud");
        SendMessage(ctrl, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) "Heavy Mud");
        SendMessage(ctrl, CB_SETCURSEL, (WPARAM) 0, 0);

}





/*
Handle messages
*/
BOOL APIENTRY
EditorWeatherDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        case WM_COMMAND : {
            int wID = LOWORD(wParam);

                switch(wID) {
                    
                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_WEATHER;
            ModeText = "Weather & Ground Mode";
            return MA_ACTIVATE;
        }
        
        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0;
        }

        default:
            return FALSE;
    }
    
}



void
EditorWeatherDialogClass::SetWeather(void) {

}

void
EditorWeatherDialogClass::SetGround(void) {

}







/*
Constructor
*/
EditorPathfindDialogClass::EditorPathfindDialogClass(HWND parent, HINSTANCE  instance) {


    hwnd = NULL;
    hwnd = CreateDialog(instance, MAKEINTRESOURCE(IDD_PATHFIND), parent, (DLGPROC) EditorPathfindDialogClass::DefaultDialogProc);

    ASSERT(hwnd != NULL);

    // initialise controls
    InitControls();

    // show window
    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

}


/*
Destructor
*/
EditorPathfindDialogClass::~EditorPathfindDialogClass(void) { }

/*
Initialise dialog's controls, etc.
*/
void
EditorPathfindDialogClass::InitControls(void) {

//    HWND ctrl;
//    int index;

    PathFindStartHex = BattleMeasure::HexCord(0,0);
    PathFindEndHex = BattleMeasure::HexCord(0,0);
    pathfind_avoidprohibited = false;
    pathfind_useroads = false;
    pathfind_terraincost = false;
    pathfind_uselevelground = false;
    pathfind_directness = 0;

}







/*
Handle messages
*/
BOOL APIENTRY
EditorPathfindDialogClass::DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {

    switch(Msg) {

        case WM_COMMAND : {
            int wID = LOWORD(wParam);

                switch(wID) {

                    case IDC_CHECK_AVOIDPROHIBITED : {
                        if(pathfind_avoidprohibited) pathfind_avoidprohibited = false;
                        else pathfind_avoidprohibited = true;
                        break;
                    }

                    case IDC_CHECK_USEROADS : {
                        if(pathfind_useroads) pathfind_useroads = false;
                        else pathfind_useroads = true;
                        break;
                    }

                    case IDC_CHECK_LEVELGROUND : {
                        if(pathfind_uselevelground) pathfind_uselevelground = false;
                        else pathfind_uselevelground = true;
                        break;
                    }

                    case IDC_CHECK_TERRAINCOST : {
                        if(pathfind_terraincost) pathfind_terraincost = false;
                        else pathfind_terraincost = true;
                        break;
                    }

                    
                        
                        
                    
                default : { HANDLE_WM_COMMAND(hWnd, wParam, lParam, GenericOnCommand); return 0; }
            }
        }

        case WM_MOUSEACTIVATE : {
            // set editor mode to this dialog, but don't process message
            EditorMode = DIALOG_PATHFIND;
            ModeText = "Pathfind Mode";
            return MA_ACTIVATE;
        }
        
        // hide dialog, set editor dialog status to hidden
        case WM_CLOSE : {
            ShowWindow(hWnd, SW_HIDE);
            return 0;
        }

        default:
            return FALSE;
    }
    
}



/*
 * $Log$
 */