/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DIALOGS_HPP
#define DIALOGS_HPP

#include "windows.h"
#include "windowsx.h"
#include "batedres.h"

#include "b_terr.hpp"
#include "hexdata.hpp"

#include "myassert.hpp"




enum EditorDialogEnum {
    DIALOG_NONE,
    DIALOG_TERRAIN,
    DIALOG_HEIGHTS,
    DIALOG_PATHS,
    DIALOG_SETTLEMENTS,
    DIALOG_EDGES,
    DIALOG_LABELS,
    DIALOG_OOB,
    DIALOG_ERASE,
    DIALOG_CROPMAP,
    DIALOG_DATETIME,
    DIALOG_WEATHER,
    DIALOG_PATHFIND,

    DIALOG_HOWMANY
};




/*
What mode path-creation is in
*/

enum PathsModeEnum {
    PATHS_ROAD,
    PATHS_RIVER,
    PATHS_TRACK,
    PATHS_STREAM
};

/*
What hex-creation mode editor is in
*/

enum HexModeEnum {
    HEX_SINGLE,
    HEX_GROUP,
    HEX_CLUSTER,
    HEX_AREA
};


enum SettlementStatusEnum {
    SETTLEMENT_NORMAL,
    SETTLEMENT_BURNING,
    SETTLEMENT_SMOKING,
    SETTLEMENT_DESTROYED
};

enum EdgesTypeEnum {
    EDGE_WALL,
    EDGE_HEDGE,
    EDGE_FENCE
};

// Current working mode of editor
extern EditorDialogEnum EditorMode;

// Current creation mode for paths
extern PathsModeEnum PathsMode;
extern bool PathsSunken;

// Current creation mode for hexes
extern HexModeEnum TerrainHexMode;
extern HexModeEnum HeightsHexMode;
extern HexModeEnum SettlementsHexMode;

// height area data
extern BattleMeasure::HexCord heightarea_center;
extern BattleMeasure::HexCord heightarea_size;
extern int heightarea_height;

// labelling data
extern BattleMeasure::HexCord label_hex;

// Terrain Creation Types
extern TerrainType PrimaryTerrain;
extern TerrainType SecondaryTerrain;
extern int TerrainAreaSize;

// Settlement Creation Type
extern int SettlementsType;
// Settlement Creation Status
extern SettlementStatusEnum SettlementsStatus;

extern EdgesTypeEnum EdgesType;
extern EdgeInfo EdgeBits;

extern bool erase_terrain;
extern bool erase_heights;
extern bool erase_paths1;
extern bool erase_paths2;
extern bool erase_edges;
extern bool erase_buildings;
extern bool erase_labels;

extern BattleMeasure::HexCord CropSelectSize;
extern BattleMeasure::HexCord CropSelectCenter;

extern BattleMeasure::HexCord PathFindStartHex;
extern BattleMeasure::HexCord PathFindEndHex;
extern bool pathfind_avoidprohibited;
extern bool pathfind_useroads;
extern bool pathfind_terraincost;
extern bool pathfind_uselevelground;
extern int pathfind_directness;

extern char * ModeText;

/*

Functions to control all the editor dialogs

*/

void GenericOnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);


/*
The terrain dialog
*/

class EditorDialogBase {

public:

        HWND hwnd;

};




        
class EditorTerrainDialogClass : public EditorDialogBase {

public :

        EditorTerrainDialogClass(HWND parent, HINSTANCE  instance);
        ~EditorTerrainDialogClass(void);

        void InitControls(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

};



class EditorHeightsDialogClass : public EditorDialogBase {

public :

        EditorHeightsDialogClass(HWND parent, HINSTANCE  instance);
        ~EditorHeightsDialogClass(void);

        void InitControls(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

};


class EditorPathsDialogClass : public EditorDialogBase {

public :

        EditorPathsDialogClass(HWND parent, HINSTANCE  instance);
        ~EditorPathsDialogClass(void);

        void InitControls(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

};


class EditorSettlementsDialogClass : public EditorDialogBase {

public:

        EditorSettlementsDialogClass(HWND parent, HINSTANCE instance);
        ~EditorSettlementsDialogClass(void);

        void InitControls(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

};


class EditorEdgesDialogClass : public EditorDialogBase {

public:

        EditorEdgesDialogClass(HWND parent, HINSTANCE instance);
        ~EditorEdgesDialogClass(void);

        void InitControls(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

};

class EditorLabelsDialogClass : public EditorDialogBase {

public:

        EditorLabelsDialogClass(HWND parent, HINSTANCE instance);
        ~EditorLabelsDialogClass(void);

        BattleMeasure::HexCord Hex;

        void InitControls(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
        void GetHexData(const BattleMeasure::HexCord& hex, BattleTerrainHex & maphex);

};



class EditorEraseDialogClass : public EditorDialogBase {

public:

        EditorEraseDialogClass(HWND parent, HINSTANCE instance);
        ~EditorEraseDialogClass(void);

        BattleMeasure::HexCord Hex;


        void InitControls(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
        void GetHexData(const BattleMeasure::HexCord& hex, BattleTerrainHex & maphex);

};


class EditorCropMapDialogClass : public EditorDialogBase {

public:

        EditorCropMapDialogClass(HWND parent, HINSTANCE instance);
        ~EditorCropMapDialogClass(void);

        BattleMeasure::HexCord Hex;


        void InitControls(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

};


class EditorDateTimeDialogClass : public EditorDialogBase {

public:

        EditorDateTimeDialogClass(HWND parent, HINSTANCE instance);
        ~EditorDateTimeDialogClass(void);

        void InitControls(void);
        void SetDateAndTime(BattleMeasure::BattleTime date_time, BattleMeasure::BattleTime::Tick duration_ticks);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

};



class EditorWeatherDialogClass : public EditorDialogBase {

public:

        EditorWeatherDialogClass(HWND parent, HINSTANCE instance);
        ~EditorWeatherDialogClass(void);

        void InitControls(void);
        void SetWeather(void);
        void SetGround(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

};



class EditorPathfindDialogClass : public EditorDialogBase {

public:

        EditorPathfindDialogClass(HWND parent, HINSTANCE instance);
        ~EditorPathfindDialogClass(void);

        void InitControls(void);
        static BOOL APIENTRY DefaultDialogProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

};


#endif

/*
 * $Log$
 */
