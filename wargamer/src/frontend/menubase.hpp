/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
MENU_SCR.HPP
Defines the MenuScreen class for the front-end, from which all further front-end menu screens are derived
*/

#ifndef MENUBASE_HPP
#define MENUBASE_HPP

#include "wind.hpp"

class DIB;
class DrawDIB;


class MenuScreenBaseClass : public WindowBaseND
{
	/*---------------------------------------------------------------
        VARIABLES
	---------------------------------------------------------------*/

	private:

    	HWND parent_window_handle;

	public:

    	DIB * bkDIB;  // static background dib
    	DrawDIBDC * screenDIB;  // dynamic screen dib

    	DrawDIBDC * Button_screenDIB;  // dynamic button screen dib

    	char ScreenNameString[128];

	/*---------------------------------------------------------------
        FUNCTION PROTOTYPES
	---------------------------------------------------------------*/

	public:
		MenuScreenBaseClass() : 
			parent_window_handle(NULL),
			bkDIB(0),
			screenDIB(0),
			Button_screenDIB(0)
		{ 
		}

		MenuScreenBaseClass(HWND parent) : 
			parent_window_handle(parent),
			bkDIB(0),
			screenDIB(0),
			Button_screenDIB(0)
		{ 
		}

		virtual ~MenuScreenBaseClass() = 0;

    	inline HWND GetWindowHandle(void) { return getHWND(); }
    	// inline void SetWindowHandle(HWND handle) { ASSERT(getHWND() == handle); }
    	inline HWND GetParentWindowHandle(void) { return(parent_window_handle); }
    	inline void SetParentWindowHandle(HWND parent) { parent_window_handle = parent; }

    	void DrawScreenDIB(void);
    	void DrawButtonBitmap(HWND button, DrawDIB * bitmap, const DRAWITEMSTRUCT* lpDrawItem, int blitmode);
    	void DrawButtonBitmapTransparent(HWND button, DrawDIB * button_bitmap, DrawDIBDC * button_bk, const DRAWITEMSTRUCT* lpDrawItem);
    	void DrawScreenName(void);

}; // end of MenuScreenClass


inline MenuScreenBaseClass::~MenuScreenBaseClass() { selfDestruct(); }

#endif


