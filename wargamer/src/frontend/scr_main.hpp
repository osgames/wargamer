/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCR_MAIN_HPP
#define SCR_MAIN_HPP

#include "menubase.hpp"
#include "wind.hpp"
#include "grtypes.hpp"

namespace WG_FrontEnd
{
	class MainButton;
};

class MenuBarClass : public WindowBaseND
{

   /*
    * Variables
    */

   public:

      HWND parent_window_handle;
      // HWND window_handle;
   
      WG_FrontEnd::MainButton* Button_A;
      WG_FrontEnd::MainButton* Button_B;
      WG_FrontEnd::MainButton* Button_C;
      WG_FrontEnd::MainButton* Button_D;

      DIB * Button_A_DIB, * Button_B_DIB, * Button_C_DIB, * Button_D_DIB;
      DIB * Button_A_pushed_DIB, * Button_B_pushed_DIB, * Button_C_pushed_DIB, * Button_D_pushed_DIB;

      DIB* bkDIB;
      DrawDIBDC * screenDIB;
      DrawDIBDC * button_screenDIB;

      enum ButtonId {  // ID no's for each button
            ButtonId_A = 10,
            ButtonId_B,
            ButtonId_C,
            ButtonId_D };

   /*
    * Functions
    */

   public:

      MenuBarClass(HWND parent, const PixelRect& rect);
      ~MenuBarClass();

      inline HWND GetWindowHandle(void) { return getHWND(); }   // (window_handle); }
      // inline void SetWindowHandle(HWND handle) { window_handle = handle; }
      inline HWND GetParentWindowHandle(void) { return(parent_window_handle); }
      inline void SetParentWindowHandle(HWND parent) { parent_window_handle = parent; }

      void SetButtonsPositions(void);
      void DrawScreenDIB(void);
      
   private:

      // HWND CreateButton(HWND parent, char * text, int button_id);
      void CreateButtons(void);

      void LoadButtonsBitmaps(void);
      void DrawButtonBitmap(HWND button, DIB * bitmap, const DRAWITEMSTRUCT* lpDrawItem, int blitmode);

      LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
      void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
      void onPaint(HWND handle);
      void onDrawItem(HWND handle, const DRAWITEMSTRUCT* lpDrawItem);
      HBRUSH onCtlColorBtn(HWND hwnd, HDC hdc, HWND hwndChild, int type);
      BOOL onEraseBkgnd(HWND hwnd, HDC hdc);

};  // end of MenuBar class





class MainMenuScreenClass : public MenuScreenBaseClass {


		/*
		Variables
		*/

		MenuBarClass * MenuBar;
		PixelRect d_screenRect;
		PixelRect d_barRect;
   

		/*
		Functions
		*/

	public:

    	MainMenuScreenClass(HWND parent);
    	~MainMenuScreenClass(void);

	private:

    	// void CalculateScreenSize(RECT * bar_rect);
    	// void CalculateBarSize(RECT * bar_rect);
	 	void calculateSizes();
    
    	LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    	void onPaint(HWND handle);
    	void onSize(HWND hwnd, UINT state, int cx, int cy);

};

#endif

