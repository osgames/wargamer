/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
MENU_SCR.CPP
Implements the MenuScreen class for the front-end, from which all further front-end menu screens are derived
*/

#include "stdinc.hpp"
#include "menubase.hpp"
// #include "generic.hpp"
// #include "app.hpp"
#include "scenario.hpp"
// #include "bmp.hpp"
#include "dib.hpp"
// #include "dib_util.hpp"
#include "dib_blt.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"





/*---------------------------------------------------------------

        FUNCTION DEFINITIONS

---------------------------------------------------------------*/




/*
Stretch the menu screen's background Dib into it's screen Dib ready for blitting into window
*/


void
MenuScreenBaseClass::DrawScreenDIB()
{

// can't proceed if no background Dib has been loaded
if(bkDIB == NULL) return;

    // get client area of menu screen
    RECT rect;
    GetClientRect(GetWindowHandle(), &rect);

    int width = rect.right - rect.left;
    int height = rect.bottom - rect.top;


    // allocate screenDib if NULL or has incorrect dimensions
    if(screenDIB == NULL) {
        screenDIB = new DrawDIBDC(width,height); }

    else if (screenDIB->getWidth() != width || screenDIB->getHeight() != height) {
        delete(screenDIB);
        screenDIB = new DrawDIBDC(width,height); }
        

    // stretch the background dib into the screen dib
    DIB_Utility::stretchDIB(screenDIB,0,0,width,height,bkDIB,0,0,bkDIB->getWidth(),bkDIB->getHeight());

}




void
MenuScreenBaseClass::DrawButtonBitmap(HWND button, DrawDIB * bitmap, const DRAWITEMSTRUCT* lpDrawItem, int blitmode) {

    if(bitmap == NULL) return;

    // get dimensions of the button from the windows structure
    int width = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
    int height = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

    // if no screenDIB defined, then allocate
    if(Button_screenDIB == NULL) {
        Button_screenDIB = new DrawDIBDC(width,height); }

    // or if screenDIB is the wrong size, delete & reallocate
    else if(Button_screenDIB->getWidth() != width || Button_screenDIB->getHeight() != height) {
        delete(Button_screenDIB);
        Button_screenDIB = new DrawDIBDC(width,height); }

    // stretch the static bitmap into the screen bitmap
    DIB_Utility::stretchDIB(Button_screenDIB,0,0,width,height,bitmap,0,0,bitmap->getWidth(),bitmap->getHeight());

    // blit screen DIBDC to button DC
    BitBlt(lpDrawItem->hDC,0,0,Button_screenDIB->getWidth(),Button_screenDIB->getHeight(),Button_screenDIB->getDC(),0,0,blitmode);

}







/*
Draws the buttons bitmap over the background bitmap, preserving transparent pixels
*/

void
MenuScreenBaseClass::DrawButtonBitmapTransparent(HWND button, DrawDIB * button_bitmap, DrawDIBDC * button_bk, const DRAWITEMSTRUCT* lpDrawItem) {

    if(button_bitmap == NULL || button_bk == NULL) return;

    // get dimensions of the button from the windows structure
    int width = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
    int height = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

    // if no screenDIB defined, then allocate
    if(Button_screenDIB == NULL) {
        Button_screenDIB = new DrawDIBDC(width,height); }

    // or if screenDIB is the wrong size, delete & reallocate
    else if(Button_screenDIB->getWidth() != width || Button_screenDIB->getHeight() != height) {
        delete(Button_screenDIB);
        Button_screenDIB = new DrawDIBDC(width,height); }


    // copy the background bitmap into the screen DIBDC
    Button_screenDIB->blitFrom(button_bk);
    
    // stretch the button's bitmap into the screen DIBDC preserving transparency
    DIB_Utility::stretchDIB(Button_screenDIB,0,0,width,height,button_bitmap,0,0,button_bitmap->getWidth(),button_bitmap->getHeight());

    // blit screen DIBDC to button DC
    BitBlt(lpDrawItem->hDC,0,0,Button_screenDIB->getWidth(),Button_screenDIB->getHeight(),Button_screenDIB->getDC(),0,0,SRCCOPY);
    
}





/*
Draw the ScreenNameString into the screenDib
*/

void
MenuScreenBaseClass::DrawScreenName(void) {

    LogFont logicalfont;
    logicalfont.height(32);
    logicalfont.weight(FW_MEDIUM);
    // obtain the default scenario font
    logicalfont.face(scenario->fontName(Font_Normal));

    Font screenfont;
    screenfont.set(logicalfont);

    screenDIB->setFont(screenfont);

    wTextOut(screenDIB->getDC(), 16, 16, ScreenNameString);

}

#if 0
void MenuScreenBaseClass::calculateScreenSize(RECT* screen_rect) 
{
#if 0
    // obtain parent window's dimensions
    RECT parent_rect;
    GetClientRect(GetParentWindowHandle(),&parent_rect);
    int parent_height = parent_rect.bottom;
    int height = parent_height - (parent_height / 16 );
        
    screen_rect->left = parent_rect.left;
    screen_rect->right = parent_rect.right;
    screen_rect->top = parent_rect.top;
    screen_rect->bottom = height;
#else
    GetClientRect(GetParentWindowHandle(), screen_rect);
#endif
}
#endif



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
