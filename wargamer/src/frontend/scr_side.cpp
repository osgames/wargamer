/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "scr_side.hpp"
#include "f_utils.hpp"
#include "dib_blt.hpp"
#include "fonts.hpp"

#include "control.hpp"

ChooseSidesScreenClass::ChooseSidesScreenClass(HWND parent) {

   d_mouseDownNapoleon = false;
   d_mouseDownPrussia = false;

    bkDIB = NULL;
    screenDIB = NULL;

   napoleonSelectedDIB = NULL;
   napoleonDeselectedDIB = NULL;
   prussiaSelectedDIB = NULL;
   prussiaDeselectedDIB = NULL;

   plaqueDIB = NULL;

    FrontEndUtilsClass::setPalette("ChooseSidesPalette.bmp");
   // load bitmaps
   bkDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\LeatherScreen.bmp");
   ASSERT(bkDIB);
    napoleonSelectedDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\NapoleonFlag.bmp");
   ASSERT(napoleonSelectedDIB);
    napoleonDeselectedDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\NapoleonFlag2.bmp");
   ASSERT(napoleonDeselectedDIB);
    prussiaSelectedDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\PrussianFlag.bmp");
   ASSERT(prussiaSelectedDIB);
    prussiaDeselectedDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\PrussianFlag2.bmp");
   ASSERT(prussiaDeselectedDIB);
   plaqueDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\Plaque.bmp");
   ASSERT(plaqueDIB);

   calculateWindowSizes();

    strcpy(ScreenNameString,"ChooseSidesScreen");

    createWindow(
        WS_EX_LEFT,
        // GenericNoBackClass::className(),
        ScreenNameString,
        WS_CHILD | WS_CLIPSIBLINGS,
        d_mainRect.left(), d_mainRect.top(), d_mainRect.width(), d_mainRect.height(),
        parent,
        NULL);
        // APP::instance() ));

    SetWindowText(getHWND(),ScreenNameString);

   // draw the background DIB to the screen DIB
    drawScreenDIB();

    // set draw state for this window
    ShowWindow(getHWND(), SW_SHOW);

}




ChooseSidesScreenClass::~ChooseSidesScreenClass(void) {

    // delete main screen bitmaps
    if(bkDIB) delete bkDIB;
    if(screenDIB) delete screenDIB;
   if(napoleonSelectedDIB) delete napoleonSelectedDIB;
   if(napoleonDeselectedDIB) delete napoleonDeselectedDIB;
   if(prussiaSelectedDIB) delete prussiaSelectedDIB;
   if(prussiaDeselectedDIB) delete prussiaDeselectedDIB;
   if(plaqueDIB) delete plaqueDIB;
}


void ChooseSidesScreenClass::calculateWindowSizes()
{
   PixelRect r;
   GetClientRect(GetParent(getHWND()), &r);

   int width = r.right();
   int height = r.bottom();

   // main rect
   d_mainRect = PixelRect(r.left(), r.top(), r.right(), r.bottom() );

   int half_width = width/2;
   int half_height = height/2;

   int flag_bottom = (height / 8) * 7;

   int xoffset = half_width / 8;
   int yoffset = half_height / 8;
   int xinset = half_width - (xoffset*2);

   RECT leftPic;
   RECT leftPlaque;
   RECT rightPic;
   RECT rightPlaque;

   // flag rects (x,y,width,height)
   leftPic.left = xoffset;
   leftPic.top = yoffset;
   leftPic.right = xinset;
   leftPic.bottom = flag_bottom;

   rightPic.left = half_width + xoffset;
   rightPic.top = yoffset;
   rightPic.right = xinset;
   rightPic.bottom = flag_bottom;

   // plaque rects
   leftPlaque.left = leftPic.left + (xoffset * 2);
   leftPlaque.top = flag_bottom + yoffset;
   leftPlaque.right = xoffset * 2;
   leftPlaque.bottom = yoffset;

   rightPlaque.left = rightPic.left + (xoffset * 2);
   rightPlaque.top = flag_bottom + yoffset;
   rightPlaque.right = xoffset * 2;
   rightPlaque.bottom = yoffset;

   if (GamePlayerControl::getDefaultSide() == 0)
   {
      napoleonRect = leftPic;
      frenchRect = leftPlaque;
      prussiaRect = rightPic;
      alliedRect = rightPlaque;
   }
   else
   {
      napoleonRect = rightPic;
      frenchRect = rightPlaque;
      prussiaRect = leftPic;
      alliedRect = leftPlaque;
   }



}

void ChooseSidesScreenClass::drawSide(Side side, DIB* portrait, const RECT& picRect, const RECT& plaqueRect)
{
   DIB_Utility::stretchDIB(
      screenDIB,
      picRect.left,
      picRect.top,
      picRect.right,
      picRect.bottom,
      portrait,
      0, 0, portrait->getWidth(), portrait->getHeight()
   );

   DIB_Utility::stretchDIB(
      screenDIB,
      plaqueRect.left,
      plaqueRect.top,
      plaqueRect.right,
      plaqueRect.bottom,
      plaqueDIB,
      0, 0, plaqueDIB->getWidth(), plaqueDIB->getHeight()
   );

   RECT textRect;
   textRect.left = plaqueRect.left;
   textRect.top = plaqueRect.top;
   textRect.right = plaqueRect.left + plaqueRect.right;
   textRect.bottom = plaqueRect.top + plaqueRect.bottom;

   const char* name = scenario->getSideName(side);
   HDC hdc = screenDIB->getDC();
   DrawText(hdc, name, lstrlen(name), &textRect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

   if (GamePlayerControl::getControl(side) != GamePlayerControl::Player)
   {
      // darken the allied plaque
      screenDIB->darkenRectangle(
         plaqueRect.left,
         plaqueRect.top,
         plaqueRect.right,
         plaqueRect.bottom,
         25
      );
   }
}


void ChooseSidesScreenClass::drawScreenDIB(void) {

   int width = d_mainRect.right();
   int height = d_mainRect.bottom();

   if(!screenDIB) {
      screenDIB = new DrawDIBDC(width, height);
   }

   else if(screenDIB->getWidth() != width || screenDIB->getHeight() != height) {
      delete screenDIB;
      screenDIB = new DrawDIBDC(width, height);
   }

   // blit bkground to DIB
   DIB_Utility::stretchDIB(screenDIB,0,0,width,height,bkDIB,0,0,bkDIB->getWidth(),bkDIB->getHeight());

   // set up a font
   HDC hdc = screenDIB->getDC();

   const int minFontHeight = 15;
   int fontHeight = 20;
   fontHeight = (fontHeight * (alliedRect.bottom)) / FrontEndUtils.s_baseWidth;
   int h = maximum(fontHeight, minFontHeight) + 5;

   LogFont logFont;
   logFont.height(h);
   logFont.weight(FW_MEDIUM);
   logFont.face(scenario->fontName(Font_Bold));

   Fonts font;
   font.set(hdc, logFont);

   COLORREF color = RGB(0,0,0);//RGB(148,115,33);
   COLORREF oldColor = SetTextColor(hdc, color);

   SetBkMode(hdc, TRANSPARENT);

   /*
    * Draw French Side
    */

   DIB* leftDIB;
   if(GamePlayerControl::getControl(0) == GamePlayerControl::Player)
   {
      leftDIB = napoleonSelectedDIB;
   }
   else
   {
      leftDIB = napoleonDeselectedDIB;
   }
   drawSide(0, leftDIB, napoleonRect, frenchRect);

   DIB* rightDIB;
   if(GamePlayerControl::getControl(1) == GamePlayerControl::Player)
   {
      rightDIB = prussiaSelectedDIB;
   }
   else
   {
      rightDIB = prussiaDeselectedDIB;
   }
   drawSide(1, rightDIB, prussiaRect, alliedRect);
}





LRESULT
ChooseSidesScreenClass::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

        switch(msg) {

//            HANDLE_MSG(hWnd, WM_CREATE, onCreate);
//            HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
//            HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
            HANDLE_MSG(hWnd, WM_SIZE, onSize);
            HANDLE_MSG(hWnd, WM_PAINT, onPaint);
//            HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
//            HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
//            HANDLE_MSG(hWnd, WM_CTLCOLORBTN, onCtlColorBtn);
            HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
            HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
//            HANDLE_MSG(hWnd, WM_SETCURSOR, onSetCursor);

            default:
                return defProc(hWnd, msg, wParam, lParam);
        }
}









void
ChooseSidesScreenClass::onSize(HWND hwnd, UINT state, int cx, int cy) {

   calculateWindowSizes();
    MoveWindow(getHWND(), d_mainRect.left(), d_mainRect.top(), d_mainRect.width(), d_mainRect.height(), TRUE);
    drawScreenDIB();
}



void
ChooseSidesScreenClass::onPaint(HWND handle) {


   if(!screenDIB) return;

    PAINTSTRUCT ps;

    HDC hdc = BeginPaint(handle, &ps);
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);


    BitBlt(hdc,0,0,screenDIB->getWidth(),screenDIB->getHeight(),screenDIB->getDC(),0,0,SRCCOPY);


    SelectPalette(hdc, oldPal, FALSE);
    EndPaint(handle, &ps);
}



void
ChooseSidesScreenClass::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags) {

   POINT pt;
   pt.x = x;
   pt.y = y;

   RECT flagRect;
   RECT plaqueRect;

   flagRect.left = napoleonRect.left;
   flagRect.top = napoleonRect.top;
   flagRect.right = napoleonRect.left + napoleonRect.right;
   flagRect.bottom = napoleonRect.top + napoleonRect.bottom;

   plaqueRect.left = frenchRect.left;
   plaqueRect.top = frenchRect.top;
   plaqueRect.right = frenchRect.left + frenchRect.right;
   plaqueRect.bottom = frenchRect.top + frenchRect.bottom;

   // mouse down on french
   if(PtInRect(&flagRect, pt) || PtInRect(&plaqueRect, pt)) {

      d_mouseDownNapoleon = true;
      d_mouseDownPrussia = false;
      return;
   }

   flagRect.left = prussiaRect.left;
   flagRect.top = prussiaRect.top;
   flagRect.right = prussiaRect.left + prussiaRect.right;
   flagRect.bottom = prussiaRect.top + prussiaRect.bottom;

   plaqueRect.left = alliedRect.left;
   plaqueRect.top = alliedRect.top;
   plaqueRect.right = alliedRect.left + alliedRect.right;
   plaqueRect.bottom = alliedRect.top + alliedRect.bottom;

   // mouse down on french
   if(PtInRect(&flagRect, pt) || PtInRect(&plaqueRect, pt)) {

      d_mouseDownPrussia = true;
      d_mouseDownNapoleon = false;
      return;
   }

}


void
ChooseSidesScreenClass::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags) {

   POINT pt;
   pt.x = x;
   pt.y = y;

   RECT flagRect;
   RECT plaqueRect;

   flagRect.left = napoleonRect.left;
   flagRect.top = napoleonRect.top;
   flagRect.right = napoleonRect.left + napoleonRect.right;
   flagRect.bottom = napoleonRect.top + napoleonRect.bottom;

   plaqueRect.left = frenchRect.left;
   plaqueRect.top = frenchRect.top;
   plaqueRect.right = frenchRect.left + frenchRect.right;
   plaqueRect.bottom = frenchRect.top + frenchRect.bottom;

   // mouse up on french
   if(PtInRect(&flagRect, pt) || PtInRect(&plaqueRect, pt)) {

      // if we clicked on french originally
      if(d_mouseDownNapoleon) {

         GamePlayerControl::setControl(0, GamePlayerControl::Player);
         GamePlayerControl::setControl(1, GamePlayerControl::AI);

         d_mouseDownNapoleon = false;
         d_mouseDownPrussia = false;
         drawScreenDIB();
         InvalidateRect(getHWND(), NULL, FALSE);
         return;
      }
   }

   flagRect.left = prussiaRect.left;
   flagRect.top = prussiaRect.top;
   flagRect.right = prussiaRect.left + prussiaRect.right;
   flagRect.bottom = prussiaRect.top + prussiaRect.bottom;

   plaqueRect.left = alliedRect.left;
   plaqueRect.top = alliedRect.top;
   plaqueRect.right = alliedRect.left + alliedRect.right;
   plaqueRect.bottom = alliedRect.top + alliedRect.bottom;

   // mouse up on french
   if(PtInRect(&flagRect, pt) || PtInRect(&plaqueRect, pt)) {

      // if we clicked on french originally
      if(d_mouseDownPrussia) {

         GamePlayerControl::setControl(0, GamePlayerControl::AI);
         GamePlayerControl::setControl(1, GamePlayerControl::Player);

         d_mouseDownPrussia = false;
         d_mouseDownNapoleon = false;
         drawScreenDIB();
         InvalidateRect(getHWND(), NULL, FALSE);
         return;
      }
   }
}




















/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
