/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
F_UTILS.CPP
Functions & Classes for loading of scenario files - copied directly from FRONTEND.CPP

19/5/98

Added campaign .INF extension type for front-end specific campaign data
Supplemented the 'addFolderAndExtension' with 'addFolderAndInfoExtension' to support reading the .INF files relating to each campaign

*/


#if defined(DEBUG) && !defined(NOLOG)
#include "stdinc.hpp"
#include "clog.hpp"
LogFileFlush ReadInfLog("read_inf.log");
#endif



#include "f_utils.hpp"
#include <memory>
#include "scenario.hpp"

// global instance of FrontEndUtilsClass
FrontEndUtilsClass FrontEndUtils;

const char FrontEndUtilsClass::s_slashStar[] = "\\*";
const char FrontEndUtilsClass::s_slash[] = "\\";
const char FrontEndUtilsClass::s_slashChar = '\\';
const char FrontEndUtilsClass::s_dotChar = '.';
const char FrontEndUtilsClass::s_campaignExt[] = ".WGC";
const char FrontEndUtilsClass::s_campaignInfoExt[] = ".INC";
const char FrontEndUtilsClass::s_campaignDefaultInfoFile[] = "default.inc";
const char FrontEndUtilsClass::s_battleExt[] = ".WGB";
const char FrontEndUtilsClass::s_battleInfoExt[] = ".INB";
const char FrontEndUtilsClass::s_battleDefaultInfoFile[] = "default.inb";
const char FrontEndUtilsClass::s_savedGamesRegistryName[] = "\\SavedGames";
const char FrontEndUtilsClass::s_scenarioRegistryName[] = "\\Scenarios";
const char FrontEndUtilsClass::s_richEditDLLName[] = "RICHED32.DLL";
const char FrontEndUtilsClass::s_cdRegistryPath[] = "\\CD_Path";
const char FrontEndUtilsClass::s_cdRegistryValue[] = "CD_Path";

const UWORD FrontEndUtilsClass::s_baseWidth = 1024;
const UWORD FrontEndUtilsClass::s_baseHeight = 768;
const UWORD FrontEndUtilsClass::s_baseButtonWidth = 150;
const UWORD FrontEndUtilsClass::s_baseButtonHeight = 50;
const UWORD FrontEndUtilsClass::s_screenOffsetX = 20;
const UWORD FrontEndUtilsClass::s_screenOffsetY = 20;
const UWORD FrontEndUtilsClass::s_baseListCX = 400;
const UWORD FrontEndUtilsClass::s_baseListCY = FrontEndUtilsClass::s_baseHeight/2;

// const char FrontEndUtilsClass::TOKEN_OpenBracket = '{';
// const char FrontEndUtilsClass::TOKEN_CloseBracket = '}';
// const char FrontEndUtilsClass::TOKEN_Quote = '"';
// const char FrontEndUtilsClass::TOKEN_Space = ' ';


#if 0
void
FrontEndUtilsClass::addCampaignFolderAndExtensions(char* buf) {

   ASSERT(buf);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slashStar);
   lstrcat(buf, s_campaignExt);
}

// new function to support campaign .INF files
void
FrontEndUtilsClass::addCampaignFolderAndInfoExtensions(char* buf) {

   ASSERT(buf);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slashStar);
   lstrcat(buf, s_campaignInfoExt);
}



void
FrontEndUtilsClass::addBattleFolderAndExtensions(char* buf) {

   ASSERT(buf);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slashStar);
   lstrcat(buf, s_battleExt);
}

// new function to support campaign .INF files
void
FrontEndUtilsClass::addBattleFolderAndInfoExtensions(char* buf) {

   ASSERT(buf);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slashStar);
   lstrcat(buf, s_battleInfoExt);
}




void
FrontEndUtilsClass::removeWGCextension(char * buf) {

   ASSERT(buf);

   int buflen = lstrlen(buf);
   int extlen = lstrlen(s_campaignInfoExt);
   int difference = (buflen - extlen+1);

   // make sure there's space for removig the extension
   ASSERT(buflen > extlen);

   char tmp_string[MAX_PATH];

   // copy buffer - extension - 1 characters, to temp (one less due to terminating character)
   lstrcpyn(tmp_string, buf, difference);

   // copy back into buffer
   lstrcpy(buf, tmp_string);

}


void
FrontEndUtilsClass::addINCextension(char * buf) {

   ASSERT(buf);

   lstrcat(buf, s_campaignInfoExt);
}





void
FrontEndUtilsClass::removeWGBextension(char * buf) {

   ASSERT(buf);

   int buflen = lstrlen(buf);
   int extlen = lstrlen(s_battleInfoExt);
   int difference = (buflen - extlen+1);

   // make sure there's space for removig the extension
   ASSERT(buflen > extlen);

   char tmp_string[MAX_PATH];

   // copy buffer - extension - 1 characters, to temp (one less due to terminating character)
   lstrcpyn(tmp_string, buf, difference);

   // copy back into buffer
   lstrcpy(buf, tmp_string);

}


void
FrontEndUtilsClass::addINBextension(char * buf) {

   ASSERT(buf);

   lstrcat(buf, s_battleInfoExt);
}



void
FrontEndUtilsClass::addFolder(char* buf, const char* fileName) {

   ASSERT(buf);
   ASSERT(fileName);

   lstrcpy(buf, scenario->getFolderName());
   lstrcat(buf, s_slash);
   lstrcat(buf, fileName);
}

#endif


void
FrontEndUtilsClass::baseToScreen(ControlInfo& info, const SIZE& s) {

   info.d_x = static_cast<UWORD>((info.d_x * s.cx) / s_baseWidth);
   info.d_y = static_cast<UWORD>((info.d_y * s.cy) / s_baseHeight);
   info.d_w = static_cast<UWORD>((info.d_w * s.cx) / s_baseWidth);
   info.d_h = static_cast<UWORD>((info.d_h * s.cy) / s_baseHeight);
}




void
FrontEndUtilsClass::baseToScreen(int& x, int& y, const SIZE& s) {

   x = (x * s.cx) / s_baseWidth;
   y = (y * s.cy) / s_baseHeight;
}




/*
 *  Parse fileName to name only. Takeout path and extension
 */

Boolean
FrontEndUtilsClass::parseFileName(SimpleString& dest, const char* fileName) {

   ASSERT(fileName != 0);

   const char* s = fileName;

   const char* slash = 0;
   const char* dot = 0;

   int length = lstrlen(fileName);

   /*
   *  Start at beginning of string and search for a '.'
   */

   int n = length;
   Boolean foundDot = False;
   while(n--)
   {
      if(*s == FrontEndUtilsClass::s_dotChar)
      {
         dot = s;
         break;
      }
      s++;
   }

   /*
   *  Now, back up and find a \
   */

   n = (dot == 0) ? length : (length-lstrlen(dot))+1;
   while(n--)
   {
      if(*s == FrontEndUtilsClass::s_slashChar)
      {
         slash = s;
         break;
      }
      s--;
   }

   /*
   *  If no dot or slash then name does't need parsing so return
   */

   if(dot == 0 && slash == 0)
   {
      dest = fileName;
      return True;
   }

   /*
   *  Copy parsed section to buffer
   */

   length = (dot != 0 && slash != 0) ? dot-slash : (dot != 0) ? (dot-fileName)+1 : (fileName+length)-slash;

   char buf[200];
   for(int i = 0; i < length; i++)
   {
      s++;
      buf[i] = (i+1 != length) ? *s : '\0';
   }

   dest = buf;
   return True;
}


#if 0
void
FrontEndUtilsClass::insertCDPath(char* fileName) {


   ASSERT(fileName);
   char cdPath[MAX_PATH];

   if((!getRegistryString(FrontEndUtilsClass::s_cdRegistryValue, cdPath, FrontEndUtilsClass::s_cdRegistryPath)) || (cdPath[0] == '\0'))
   {

      FORCEASSERT("CD drive not found, and CD path not in registry");
   }

   char* s = cdPath;
   char c;
   while(*s != '\0')
   {
      c = *s++;
   }

   if(c != '\\')
      *s++ = '\\';

   lstrcpy(s, fileName);
   lstrcpy(fileName, cdPath);
}
#endif



/*
This function initialises the global strings / filenames for the campaign specific data, andmust be called when the FrontEndClass is constructed
*/

void
FrontEndUtilsClass::InitialiseGlobals(void) {
#if 0    // Rewritten for better INF file parsing
   // make sure the filenames & strings pointers start out as NULL
   FILENAME_Map = NULL;
   STRING_Text = NULL;
   STRING_Description = NULL;
   STRING_FrenchKey = NULL;
   STRING_AlliedKey = NULL;

   // setup the token strings
   TOKEN_Map = strdup("MAP");
   TOKEN_Text = strdup("TEXT");
   TOKEN_Description = strdup("DESCRIPTION");
   TOKEN_FrenchKey = strdup("FRENCH_KEY");
   TOKEN_AlliedKey = strdup("ALLIED_KEY");
#endif
}



void
FrontEndUtilsClass::DeallocateGlobals(void) {
#if 0    // Rewritten for better INF file parsing

   if(TOKEN_Map != NULL)
   {
      free(TOKEN_Map);
      TOKEN_Map=NULL;
   }

   if(TOKEN_Text != NULL)
   {
      free(TOKEN_Text);
      TOKEN_Text=NULL;
   }

   if(TOKEN_Description != NULL)
   {
      free(TOKEN_Description);
      TOKEN_Description=NULL;
   }

   if(TOKEN_FrenchKey != NULL)
   {
      free(TOKEN_FrenchKey);
      TOKEN_FrenchKey=NULL;
   }

   if(TOKEN_AlliedKey != NULL)
   {
      free(TOKEN_AlliedKey);
      TOKEN_AlliedKey=NULL;
   }
#endif
}







void
FrontEndUtilsClass::ReadLocaleSetings(void) {
#if 0    // Rewritten for better INF file parsing

   // character buffer for revieving string (only 3 chars needed)
   char buffer[8];

   int result;

   // get locale info into buffer
   result = GetLocaleInfo(LOCALE_USER_DEFAULT,
      LOCALE_SABBREVLANGNAME,
      buffer,
      64);

   // convert the string to upper-case
   CharUpper(buffer);

   // put two-character base country in square brackets & add terminator
   LOCALECODE_String[0] = '\[';
   LOCALECODE_String[1] = buffer[0];
   LOCALECODE_String[2] = buffer[1];
   LOCALECODE_String[3] = '\]';
   LOCALECODE_String[4] = '\0';
#endif
}

















DIB* FrontEndUtilsClass::loadBMPDIB(const char* name, bool defPalette)
{
   BMP::ReadMode mode = defPalette ? BMP::RBMP_ForcePalette : BMP::RBMP_Normal;
   // std::auto_ptr<char> fName(scenario->makeScenarioFileName(name));
   String fName(scenario->makeScenarioFileName(name));
   DIB* dib = BMP::newDIB(fName.c_str(), mode);
   return dib;
}

DIBDC* FrontEndUtilsClass::loadBMPDIBDC(const char* name, bool defPalette)
{
   BMP::ReadMode mode = defPalette ? BMP::RBMP_ForcePalette : BMP::RBMP_Normal;
   // std::auto_ptr<char> fName(scenario->makeScenarioFileName(name));
   String fName(scenario->makeScenarioFileName(name));
   DIBDC* dib = BMP::newDIBDC(fName.c_str(), mode);
   return dib;
}

void FrontEndUtilsClass::setPalette(const char* name)
{

   char tmp[MAX_PATH+256];
   sprintf(tmp, "frontend\\%s", name);

   CString palName(scenario->makeScenarioFileName(tmp));
   BMP::getPalette(palName);
}





/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
