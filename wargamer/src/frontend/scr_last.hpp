/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCR_LAST_HPP
#define SCR_LAST_HPP

#include "menubase.hpp"


class StringPtr;

class LastGameScreenClass : public MenuScreenBaseClass {

public:

    LastGameScreenClass(HWND parent, StringPtr* result);
    ~LastGameScreenClass(void);

};

#endif
