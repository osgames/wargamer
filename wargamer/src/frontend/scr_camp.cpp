/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      FrontEnd : Choose Campaign Screen
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "scr_camp.hpp"
#include "filecnk.hpp"
#include "wldfile.hpp"


void
ChooseCampaignScreenClass::setPalette(void) {

   FrontEndUtilsClass::setPalette("ChooseCampaignPalette.bmp");
}


/*
 * Read in all .WGC files in scenario directory
 */

void ChooseCampaignScreenClass::constructMissionFilesList()
{
#if defined(DEBUG) && !defined(NOLOG)
        ChooseMissionLog.printf("\nSCANNING FOR .WGC & .INC FILES");
#endif

   makeMissionList(FrontEndUtilsClass::s_campaignExt);
}


#if 0 // moved to ch_miss
        char path[MAX_PATH];
        char WGC_filemask[MAX_PATH];
        char INC_filemask[MAX_PATH];

        // add the wargamer default folder & WGC extension to the path
        FrontEndUtils.addCampaignFolderAndExtensions(WGC_filemask);
        FrontEndUtils.addCampaignFolderAndInfoExtensions(INC_filemask);

        // add the wargamer default folder
        lstrcpy(path, scenario->getFolderName());
        lstrcat(path, FrontEndUtils.s_slash);

        // the windows find structures for the two files to be found .wgc & .inf
        WIN32_FIND_DATA findWGCdata;
        WIN32_FIND_DATA findINCdata;

        // locate directory containing .wgc files
        HANDLE WGC_filehandle;
        HANDLE INC_filehandle;

        // find first foile satifsfying this path & mask
        WGC_filehandle = FindFirstFile(WGC_filemask, &findWGCdata);

        // if not files found, check on CD
        if(WGC_filehandle == INVALID_HANDLE_VALUE)
        {
        FrontEndUtils.insertCDPath(path);
        FrontEndUtils.insertCDPath(WGC_filemask);
        FrontEndUtils.insertCDPath(INC_filemask);

        WGC_filehandle = FindFirstFile(WGC_filemask, &findWGCdata);

        if(WGC_filehandle == INVALID_HANDLE_VALUE)
                {
        // throw StartScreenError("WGC file cannot be found on hard disc or CD");
        FORCEASSERT("WGC file cannot be found on hard disc or CD");
        }
        }


        ASSERT(WGC_filehandle != INVALID_HANDLE_VALUE);

        /*
         * Read version number from each WGC file to establish compatibility
         */

        do
        {
        MissionInfo info;
        char WGC_filename[MAX_PATH];
        char INC_filename[MAX_PATH];

        try
                {

        // retrieve the actual filename for the .wgc file
        FrontEndUtils.addFolder(WGC_filename, findWGCdata.cFileName);

        WinFileBinaryChunkReader fr(WGC_filename);

                        /*
                         * Extract Mission version. If this is < FileVersion
                         * Mission data does not have MissionInfo
                         */

        FileChunkReader r(fr, campaignChunkName);

        if(!r.isOK()) { throw FileError(); }

        UWORD version;
        if(!fr.getUWord(version)) { throw FileError(); }

        if(!info.read(fr)) { throw FileError(); }

        }
        catch(FileError) { throw GeneralError("Unable to read Campaign Info"); }

                /*
                 * Search for a .inc file with the same name as the .wgc file
                 */

        // copy the Campaign filename to the INF filemask
        lstrcpy(INC_filemask, WGC_filename);
        // remove the .WGC extension
        FrontEndUtils.removeWGCextension(INC_filemask);
        // add the .INF extension
        FrontEndUtils.addINCextension(INC_filemask);
        INC_filehandle = FindFirstFile(INC_filemask, &findINCdata);

        // if no .inf file is found, use default.inf
        if(INC_filehandle == INVALID_HANDLE_VALUE)
                {
        lstrcpy(INC_filename, path);
        lstrcat(INC_filename, FrontEndUtils.s_campaignDefaultInfoFile);
        }
        else FrontEndUtils.addFolder(INC_filename, findINCdata.cFileName);

        // fill in the mission info field from inf file
        FrontEndUtils.ParseInfoFile(INC_filename);
        info.setDescription(FrontEndUtils.STRING_Text);

        // add the filenames to the list
        InfoItem* item = new InfoItem(info, WGC_filename, INC_filename);
        ASSERT(item != 0);
        GetMissionInfoList().append(item);

        #if defined(DEBUG) && !defined(NOLOG)
                ChooseMissionLog.printf(WGC_filename);
                ChooseMissionLog.printf(INC_filename);
        #endif

        // repeat until no more .wgc files can be found
        } while(FindNextFile(WGC_filehandle, &findWGCdata));

        // set the current item to first in list
        const InfoItem* item = GetMissionInfoList().first();
        CurrentItem(item);
}
#endif



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
