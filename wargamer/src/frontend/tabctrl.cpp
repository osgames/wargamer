/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "tabctrl.hpp"
// #include "uinfowin.hpp"
#include "dib.hpp"
#include "palwind.hpp"
#include "scenario.hpp"
#include "app.hpp"
#include "palette.hpp"
// #include "generic.hpp"
#include "fonts.hpp"
#include "ctab.hpp"
#include "cbutton.hpp"
#include "dib_util.hpp"
#include "dib_blt.hpp"
#include "scrnbase.hpp"
#include "bmp.hpp"
#include "f_utils.hpp"
#include "scn_img.hpp"

const LONG dbUnits = GetDialogBaseUnits();
const LONG dbX = LOWORD(dbUnits);
const LONG dbY = HIWORD(dbUnits);

/*----------------------------------------------------------------------
 * tabbed info window
 */

DrawDIBDC* FE_TabbedInfoWindow::s_dib = 0;
int FE_TabbedInfoWindow::s_instanceCount = 0;

const int tBorderWidth = 1;

FE_TabbedInfoWindow::FE_TabbedInfoWindow(HWND hParent, RECT * size, int nItems, const char** text) :
  d_hParent(hParent),
  d_infoFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
//  d_bkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground)),
  d_bkDib(FrontEndUtilsClass::loadBMPDIB("frontend\\LeatherScreen.bmp")),
  d_nItems(nItems),
  d_tabText(text),
/* my alterations - as well as the RECT structure passed to the constructor */
  d_winCX(size->right),
  d_winCY(size->bottom),
  d_offsetX((ScreenBase::dbX() * 3) / ScreenBase::baseX()),
  d_offsetY((ScreenBase::dbY() * 3) / ScreenBase::baseY()),
  d_shadowWidth((ScreenBase::dbX() * 2) / ScreenBase::baseX())
{
  ASSERT(d_hParent);
  ASSERT(d_infoFillDib);

  s_instanceCount++;

  SetRect(&d_dRect, 0, 0, 0, 0);

  createWindow(
                0,
                // GenericNoBackClass::className(), 
                     NULL,
                WS_CHILD,
                size->left, size->top, d_winCX, d_winCY,
                d_hParent, NULL    // , APP::instance()
  );

SetWindowText(hWnd,"TabOuterWindow");

}


FE_TabbedInfoWindow::~FE_TabbedInfoWindow()
{
    selfDestruct();

  ASSERT((s_instanceCount - 1) >= 0);

  if(--s_instanceCount == 0)
  {
         if(s_dib)
                delete s_dib;

         s_dib = 0;
  }
}

LRESULT FE_TabbedInfoWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
         HANDLE_MSG(hWnd, WM_CREATE,    onCreate);
         HANDLE_MSG(hWnd, WM_NOTIFY, onNotify);
         HANDLE_MSG(hWnd, WM_PAINT, onPaint);
         HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
         HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);

         default:
                return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL FE_TabbedInfoWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  d_cTab.images(ScenarioImageLibrary::get(ScenarioImageLibrary::CTabImages));

  /*
        * Create a tabbed window
        */

  hwndTab = CreateWindow(
                WC_TABCONTROL, "TabInnerWindow",
                WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE |
                        TCS_TOOLTIPS | TCS_FIXEDWIDTH,
                d_offsetX, d_offsetY,
                lpCreateStruct->cx - ((2 * d_offsetX) + d_shadowWidth),
                lpCreateStruct->cy - ((2 * d_offsetY) + d_shadowWidth),
                hWnd,
                (HMENU) ID_Tab,
                APP::instance(),
                NULL);

  ASSERT(hwndTab != NULL);

  /*
        * Set up the tabbed titles
        *
        * This must be done before using AdjustRect
        */

  TC_ITEM tie;
  tie.mask = TCIF_TEXT | TCIF_PARAM;

  for(int i = 0; i < d_nItems; i++)
  {
         tie.pszText = (char*)(d_tabText[i]);
         tie.lParam = i;

         TabCtrl_InsertItem(hwndTab, i, &tie);
  }

  /*
   * Set size of tabs (in Dialog Units)
   */

  // const LONG tw = (ScreenBase::dbX() * 27) / ScreenBase::baseX();
  const LONG tw = (ScreenBase::dbX() * 50) / ScreenBase::baseX();
  const LONG th = (ScreenBase::dbY() * 13) / ScreenBase::baseY();

  TabCtrl_SetItemSize(hwndTab, tw, th);


/*
Initialize custom drawn tab class
*/

  d_cTab.tabHWND(hwndTab);

  LogFont lf;
  lf.height((6 * ScreenBase::dbY()) / ScreenBase::baseY());
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font font;
  font.set(lf);

  d_cTab.setFont(font);
  // d_cTab.setFlags(CustomDrawnTab::NoBorder | CustomDrawnTab::ShadowTab | CustomDrawnTab::StretchTab);
  d_cTab.setFlags(CustomDrawnTab::NoBorder | CustomDrawnTab::StretchTab);

  /*
        * Get display area
        */

  d_cTab.getDisplayRect(d_dRect);
  /*
        * Now, allocate dib
        */

  DIB_Utility::allocateDib(&s_dib, lpCreateStruct->cx, lpCreateStruct->cy);
  ASSERT(s_dib);

  s_dib->setBkMode(TRANSPARENT);


  
  return TRUE;
}


/*
Sets the size of the window
*/

void
FE_TabbedInfoWindow::setSize(const PixelRect& size) {

   // set these
   d_winCX = size.width();
   d_winCY = size.height();

    // resize main window
    MoveWindow(getHWND(), size.left(), size.top(), size.width(), size.height(), TRUE);
    // resize tab window
    MoveWindow(hwndTab, d_offsetX, d_offsetY, size.width() - ((2 * d_offsetX) + d_shadowWidth), size.height() - ((2 * d_offsetY) + d_shadowWidth), TRUE);
    // recalc the display area rect

    // reallocate DIB at new size
    if(s_dib != NULL)
    {
      // delete s_dib; s_dib = NULL; 
         s_dib->resize(size.width(), size.height());
    }
    else
       DIB_Utility::allocateDib(&s_dib, size.width(), size.height());

    ASSERT(s_dib);

    s_dib->setBkMode(TRANSPARENT);

    // stretch the DIB to fill the DIBDC
    DIB_Utility::stretchDIB(s_dib, 0, 0, size.width(), size.height(), d_bkDib, 0, 0, d_bkDib->getWidth(), d_bkDib->getHeight());

}

/*
Gets the size if the display area
*/
const PixelRect&
FE_TabbedInfoWindow::getSize()
{

    GetClientRect(hwndTab, &d_dRect);
    int client_top = d_dRect.top();
    
    TabCtrl_AdjustRect(hwndTab, FALSE, &d_dRect);
    // size is relative to display area's top, so subtract the difference between client's top edge
//    d_dRect.bottom -= (d_dRect.top - client_top);

    return d_dRect;
    
}



UINT FE_TabbedInfoWindow::onNCHitTest(HWND hwnd, int x, int y)
{
  static POINT p;
  p.x = x;
  p.y = y;

  ScreenToClient(hwnd, &p);

  if(PtInRect(&d_dRect, p))
  {
//         return HTCAPTION;
         return HTNOWHERE;
  }
  else
  {
         return HTCLIENT;
  }
}

void FE_TabbedInfoWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
/*
         case ID_Close:
                hide();
                break;
*/
  }
}

void FE_TabbedInfoWindow::onMove(HWND hwnd, int x, int y)
{
}

LRESULT FE_TabbedInfoWindow::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
        // Assume its from the tabbed dialog
        if(lpNMHDR->idFrom == ID_Tab)
        {
                switch(lpNMHDR->code)
                {
                case TCN_SELCHANGING:
                        return FALSE;

                case TCN_SELCHANGE:
                        onSelChange();
                        break;
                }
        }

        return TRUE;
}








void FE_TabbedInfoWindow::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  if(s_dib)
  {
         BitBlt(hdc, 0, 0, d_winCX, d_winCY, s_dib->getDC(), 0, 0, SRCCOPY);
  }

  SelectPalette(hdc, oldPal, FALSE);
  EndPaint(hwnd, &ps);
}







void FE_TabbedInfoWindow::forceRedraw()
{
  InvalidateRect(getHWND(), NULL, FALSE);
  UpdateWindow(getHWND());
}








void FE_TabbedInfoWindow::run(const PixelPoint& p)
{
  const int offset = (ScreenBase::dbY() * 10) / ScreenBase::baseY();

  /*
        * make sure entire window is within mainwindows rect
        */

  RECT r;
  GetWindowRect(APP::getMainHWND(), &r);

  int x = p.getX();
  int y = p.getY();

  // check right side
  if((x + d_winCX) > r.right)
  {
         x -= (d_winCX + offset);
  }

  // check bottom
  if((y + d_winCY) > r.bottom)
  {
         y -= (d_winCY + offset);
  }

//  SetForegroundWindow(getHWND());
  SetWindowPos(getHWND(), HWND_TOPMOST, x, y, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);
}







// void FE_TabbedInfoWindow::hide()
// {
//   ShowWindow(getHWND(), SW_HIDE);
// }










// void FE_TabbedInfoWindow::destroy()
// {
//   DestroyWindow(getHWND());
// }







int FE_TabbedInfoWindow::currentTab()
{
  int curSel = TabCtrl_GetCurSel(d_cTab.tabHWND());
  ASSERT(curSel < d_nItems);

  return curSel;
}


/*
Draw the given bitmap into the tab window's display rect & apply shadowing
*/


void FE_TabbedInfoWindow::fillDisplayArea(DrawDIB * bitmap) {


/*
Stretch blit
*/
    DIB_Utility::stretchDIB(s_dib, d_dRect.left() + d_shadowWidth, d_dRect.top() + 2, d_dRect.width(), d_dRect.height(), bitmap, 0, 0, bitmap->getWidth(), bitmap->getHeight());


/*
Normal blit
*/
//        s_dib->blit(bitmap, d_dRect.left(), d_dRect.top());

         // right side shadow
/*
         s_dib->darkenRectangle(d_dRect.right() + tBorderWidth, d_dRect.top() + d_shadowWidth,
                        d_shadowWidth, d_dRect.height() + tBorderWidth, 50);
         // bottom shadow
         s_dib->darkenRectangle(d_dRect.left() + d_shadowWidth, d_dRect.bottom() + tBorderWidth,
                        d_dRect.width(), d_shadowWidth, 50);
*/

        int shadow = 0;

         s_dib->darkenRectangle(d_dRect.right(), d_dRect.top(), d_dRect.right() + shadow, d_dRect.bottom() + shadow, 50);

         s_dib->darkenRectangle(d_dRect.left(), d_dRect.bottom(), d_dRect.right(), d_dRect.bottom() + shadow, 50);


         /*
          * draw borders
          */
/*
         static RECT r;
         GetClientRect(getHWND(), &r);

         static CustomBorderWindow bw(scenario->getBorderColors());
         bw.drawThinBorder(s_dib->getDC(), r);
*/
}




/*
NOTE : these should StretchBlit to support a resizable tab control
*/

void FE_TabbedInfoWindow::fillStatic()
{
  if(s_dib && d_bkDib)
  {
         static RECT r;
         GetClientRect(getHWND(), &r);

         /*
          * fill with window background
          */

       DIB_Utility::stretchDIB(s_dib, 0, 0, r.right - r.left, r.bottom - r.top, d_bkDib, 0, 0, d_bkDib->getWidth(), d_bkDib->getHeight());



  }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
