/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
NAV_BAR.CPP
Implements the NavigateBar at the bottom of the font-end menu screens.
*/

#include "stdinc.hpp"
#include "navbar.hpp"
#include "f_utils.hpp"
#include "DIB.hpp"
// #include "generic.hpp"
// #include "app.hpp"
// #include "scenario.hpp"
// #include "bmp.hpp"
// #include "DIB_util.hpp"
#include "DIB_blt.hpp"
// #include "palette.hpp"
// #include "cbutton.hpp"


/*---------------------------------------------------------------

        FUNCTION DEFINITIONS

---------------------------------------------------------------*/

/*
Default Constructor for NavigateBar
*/

NavigateBarClass::NavigateBarClass(void) { }


/*
Constructor for NavigateBar
*/


NavigateBarClass::NavigateBarClass(HWND parent, BarModeEnum mode, RECT * rect) :
    parent_window_handle(parent),
    BarMode(mode),
    Button_A(NULL),
    Button_B(NULL),
    Button_C(NULL),
    Button_D(NULL),
    prev_DIB(0),
    next_DIB(0),
    help_DIB(0),
    quit_DIB(0),
    start_DIB(0),
    prev_pushed_DIB(0),
    next_pushed_DIB(0),
    help_pushed_DIB(0),
    quit_pushed_DIB(0),
    start_pushed_DIB(0),
    buttonA_DIB(0),
    buttonB_DIB(0),
    buttonC_DIB(0),
    buttonD_DIB(0),
    buttonApushed_DIB(0),
    buttonBpushed_DIB(0),
    buttonCpushed_DIB(0),
    buttonDpushed_DIB(0),
    BarWidth(0),
    ButtonSpacing(0),
    bkDIB(0),
    screenDIB(0),
    button_screenDIB(0)
{
    // make sure paramaters are valid
    ASSERT(parent != NULL && (mode >= Bar_Hidden && mode <= Bar_Final));

    // create this window
    createWindow(
        WS_EX_LEFT,
        "NavigateBar",
        WS_CHILD | WS_CLIPSIBLINGS,
        rect->left, rect->top, rect->right, rect->bottom,
        parent,
        NULL);

    SetWindowText(GetWindowHandle(),"NavigateBar");

    // set navbar background bitmap
    bkDIB = FrontEndUtilsClass::loadBMPDIB("frontend\\LeatherBar.bmp");
    ASSERT(bkDIB != NULL);
    // draw the background DIB to the screen DIB
    DrawScreenDIB();

    LoadButtonsBitmaps();
    // create the relevant buttons dependent upon bar mode
    CreateButtons();
    // set the buttons' positions relative to size of bar window
    SetButtonsPositions();
    // load the appropriate button bitmaps according to bar mode
    SetButtonsBitmaps();


}




/*
Destructor for Navigate Bar
*/


NavigateBarClass::~NavigateBarClass(void) 
{
    DestroyWindow(Button_A);
    DestroyWindow(Button_B);
    DestroyWindow(Button_C);
    DestroyWindow(Button_D);

    selfDestruct();

    delete(bkDIB);
    delete(screenDIB);

    delete(button_screenDIB);

    delete prev_DIB;
    delete next_DIB;
    delete help_DIB;
    delete quit_DIB;
    delete start_DIB;
    delete prev_pushed_DIB;
    delete next_pushed_DIB;
    delete help_pushed_DIB;
    delete quit_pushed_DIB;
    delete start_pushed_DIB;

    // delete(buttonA_DIB);
    // delete(buttonB_DIB);
    // delete(buttonC_DIB);
    // delete(buttonD_DIB);

    // delete(buttonApushed_DIB);
    // delete(buttonBpushed_DIB);
    // delete(buttonCpushed_DIB);    
    // delete(buttonDpushed_DIB);

}





/*
Set the Navigate Bar's mode
*/

void
NavigateBarClass::SetBarMode(BarModeEnum mode) {

    ASSERT ((mode >= Bar_Hidden) && (mode <= Bar_Final));
    BarMode = mode;

    SetButtonsBitmaps();

}




/*
Stretch the menu screen's background DIB into it's screen DIB ready for blitting into window
*/


void
NavigateBarClass::DrawScreenDIB(void) {

// can't proceed if no background DIB has been loaded
if(bkDIB == NULL) return;

    // get client area of bar
    RECT rect;
    GetClientRect(GetWindowHandle(), &rect);

    int width = rect.right - rect.left;
    int height = rect.bottom - rect.top;


    // allocate screenDIB if NULL or has incorrect dimensions
    if(screenDIB == NULL) {
        screenDIB = new DrawDIBDC(width,height); }

    else if (screenDIB->getWidth() != width || screenDIB->getHeight() != height) 
    {
        delete(screenDIB);
        screenDIB = new DrawDIBDC(width,height); 
    }
        

    // stretch the background DIB into the screen DIB
    DIB_Utility::stretchDIB(screenDIB,0,0,width,height,bkDIB,0,0,bkDIB->getWidth(),bkDIB->getHeight());

}





/*
Creates an ownerdrawn button within the bar
*/

HWND
NavigateBarClass::CreateButton(HWND parent, char * text, int button_id) {

    HWND button_handle = CreateWindow(
        "BUTTON",
        text,
        BS_OWNERDRAW | WS_CHILD,
        0, 0, 0, 0,
        parent,
        (HMENU) button_id,
        APP::instance(),
        NULL);

    return button_handle;
}






/*
Set the given rectangle's dimensions equal to the bar's size relative to the parent window
*/


void
NavigateBarClass::setSize(RECT * rect) {

   MoveWindow(getHWND(), rect->left, rect->top, rect->right, rect->bottom, FALSE);

    SetButtonsPositions();
    DrawScreenDIB();

}






/*
Load all the bitmaps for the navbar's buttons
*/


void
NavigateBarClass::LoadButtonsBitmaps(void) {

        // just make sure that these are NULL - they should be anyway
        delete prev_DIB;
        prev_DIB = 0;

        delete prev_pushed_DIB;
        prev_pushed_DIB = 0;

        delete next_DIB;
        next_DIB = 0;

        delete next_pushed_DIB;        
        next_pushed_DIB = 0;

        delete help_DIB;
        help_DIB = 0;

        delete help_pushed_DIB;
        help_pushed_DIB = 0;

        delete quit_DIB;
        quit_DIB = 0;

        delete quit_pushed_DIB;
        quit_pushed_DIB = 0;

        delete start_DIB;
        start_DIB = 0;

        delete start_pushed_DIB;
        start_pushed_DIB = 0;

        // load the relevant bitmaps
        prev_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Back-out.bmp");
        prev_pushed_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Back-in.bmp");

        next_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Forward-Out.bmp");
        next_pushed_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Forward-In.bmp");

        help_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Help-out.bmp");
        help_pushed_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Help-in.bmp");

        quit_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Quit-out.bmp");
        quit_pushed_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Quit-in.bmp");

        start_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Ok-out.bmp");
        start_pushed_DIB = FrontEndUtilsClass::loadBMPDIBDC("frontend\\Ok-in.bmp");

}



/*
Assign bitmaps to the buttons DIBs dependent on bar state
*/

void
NavigateBarClass::SetButtonsBitmaps(void) {

    // setup the buttons according to which mode the bar is in
    switch(BarMode) {
        
        case Bar_Hidden : {  // in this mode all bitmaps are deleted & made NULL
            
            buttonA_DIB = NULL;  buttonApushed_DIB = NULL;
            buttonB_DIB = NULL;  buttonBpushed_DIB = NULL;
            buttonC_DIB = NULL;  buttonCpushed_DIB = NULL;
            buttonD_DIB = NULL;  buttonDpushed_DIB = NULL;

        break; }


        case Bar_Normal : {  // in this mode bar displays Prev, Next, Help & Quit

            buttonA_DIB = prev_DIB;
            buttonApushed_DIB = prev_pushed_DIB;

            buttonB_DIB = next_DIB;
            buttonBpushed_DIB = next_pushed_DIB;

            buttonC_DIB = help_DIB;
            buttonCpushed_DIB = help_pushed_DIB;

            buttonD_DIB = quit_DIB;
            buttonDpushed_DIB = quit_pushed_DIB;

        break; }

        case Bar_Final : {  // in this mode bar displays Prev, Next, Start & Quit

            buttonA_DIB = prev_DIB;
            buttonApushed_DIB = prev_pushed_DIB;

            buttonB_DIB = start_DIB;
            buttonBpushed_DIB = start_pushed_DIB;

            buttonC_DIB = help_DIB;
            buttonCpushed_DIB = help_pushed_DIB;

            buttonD_DIB = quit_DIB;
            buttonDpushed_DIB = quit_pushed_DIB;

        break; }

        default : {
            FORCEASSERT("ERROR : Invalid NavBar mode trying to set bitmaps");
        break; }


    }
}






/*
Create the four buttons.
*/


void
NavigateBarClass::CreateButtons(void) {

    Button_A = CreateButton(GetWindowHandle(),"Button A",BarButtonId_A);
    Button_B = CreateButton(GetWindowHandle(),"Button B",BarButtonId_B);
    Button_C = CreateButton(GetWindowHandle(),"Button C",BarButtonId_C);
    Button_D = CreateButton(GetWindowHandle(),"Button D",BarButtonId_D);

}






/*
Stretch a bitmap into a owner-drawn button
*/

void
NavigateBarClass::DrawButtonBitmap(HWND button, DIB * bitmap, const DRAWITEMSTRUCT* lpDrawItem, int blitmode) {

    if(bitmap == NULL) return;

    // get dimensions of the button from the windows structure
    int width = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
    int height = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

    // if no screenDIB defined, then allocate
    if(button_screenDIB == NULL) {
        button_screenDIB = new DrawDIBDC(width,height); }

    // or if screenDIB is the wrong size, delete & reallocate
    else if(button_screenDIB->getWidth() != width || button_screenDIB->getHeight() != height) {
        delete(button_screenDIB);
        button_screenDIB = new DrawDIBDC(width,height); }

    // stretch the static bitmap into the screen bitmap
    DIB_Utility::stretchDIB(button_screenDIB,0,0,width,height,bitmap,0,0,bitmap->getWidth(),bitmap->getHeight());

    // blit screen DIBDC to button DC
    BitBlt(lpDrawItem->hDC,0,0,button_screenDIB->getWidth(),button_screenDIB->getHeight(),button_screenDIB->getDC(),0,0,blitmode);


}



/*
Set the positions of the bar's buttons dependent upon window size
*/

void
NavigateBarClass::SetButtonsPositions(void) {
       
    // get the dimensions of this window
    RECT rect;
    GetClientRect(GetWindowHandle(),&rect);
    int width = rect.right - rect.left;
    int height = rect.bottom - rect.top;
    // calculate the horizontal spacing & insets for four buttons
    int horiz_spacing = width / 4;
    int horiz_inset = horiz_spacing / 4;
    int button_width = horiz_inset * 2;

    // calculate vertical spacing, top & bottom of buttons
    int vert_spacing = height / 4;
    int button_top = rect.top + vert_spacing;
    int button_bottom = rect.bottom - vert_spacing;
    int button_height = (button_bottom - button_top);

    int button_xpos;


    // move the buttons' windows to new dimensions - but don't sent a new paint mesage
    button_xpos = (horiz_spacing * 0) + horiz_inset;
    MoveWindow(Button_A,button_xpos,button_top,button_width,button_height,FALSE);

    button_xpos = (horiz_spacing * 1) + horiz_inset;
    MoveWindow(Button_B,button_xpos,button_top,button_width,button_height,FALSE);

    button_xpos = (horiz_spacing * 2) + horiz_inset;
    MoveWindow(Button_C,button_xpos,button_top,button_width,button_height,FALSE);

    button_xpos = (horiz_spacing * 3) + horiz_inset;
    MoveWindow(Button_D,button_xpos,button_top,button_width,button_height,FALSE);

}




/*
Set the ShowState of the NavBar & Buttons, dependent upon its mode
*/


void
NavigateBarClass::DisplayBar(void) {

if(BarMode != Bar_Hidden) {

    // set navbar & buttons show states
    ShowWindow(GetWindowHandle(), SW_SHOW);
    ShowWindow(Button_A, SW_SHOW);
    ShowWindow(Button_B, SW_SHOW);
    ShowWindow(Button_C, SW_HIDE);
    ShowWindow(Button_D, SW_SHOW);

    // mark all of navbar for a repaint EXCEPT for areas occupied by buttons

    // first invalidate the whole area
    InvalidateRect(GetWindowHandle(),NULL,FALSE);

    // get the absolute coords of navbar
    RECT rect;
    GetWindowRect(GetWindowHandle(),&rect);
    int x=rect.left;
    int y=rect.top;

    // validate the area of button A
    GetWindowRect(Button_A,&rect);
    rect.left -= x;
    rect.top -= y;
    rect.right -=x;
    rect.bottom -=y;
    ValidateRect(GetWindowHandle(),&rect);

    // validate the area of button B
    GetWindowRect(Button_B,&rect);
    rect.left -= x;
    rect.top -= y;
    rect.right -=x;
    rect.bottom -=y;
    ValidateRect(GetWindowHandle(),&rect);

    // validate the area of button C
    GetWindowRect(Button_C,&rect);
    rect.left -= x;
    rect.top -= y;
    rect.right -=x;
    rect.bottom -=y;
    ValidateRect(GetWindowHandle(),&rect);

    // validate the area of button D
    GetWindowRect(Button_D,&rect);
    rect.left -= x;
    rect.top -= y;
    rect.right -=x;
    rect.bottom -=y;
    ValidateRect(GetWindowHandle(),&rect);

    // now invalidate the whole of each button's area, forcing a repaint
    InvalidateRect(Button_A,NULL,FALSE);
    InvalidateRect(Button_B,NULL,FALSE);
    InvalidateRect(Button_C,NULL,FALSE);    
    InvalidateRect(Button_D,NULL,FALSE);

    // make sure windows knows we want all of these updated NOW !    
    UpdateWindow(GetWindowHandle());
    UpdateWindow(Button_A);
    UpdateWindow(Button_B);
    UpdateWindow(Button_C);
    UpdateWindow(Button_D);        
}

else {

    // if bar is hidden, set the navbar & buttons show state accordingly
    ShowWindow(GetWindowHandle(), SW_HIDE);
    ShowWindow(Button_A, SW_HIDE);
    ShowWindow(Button_B, SW_HIDE);
    ShowWindow(Button_C, SW_HIDE);
    ShowWindow(Button_D, SW_HIDE);

}    

}







/*
Message processing is derived from the "procMessage" virtual base function
*/

LRESULT
NavigateBarClass::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

        switch(msg) {

//            HANDLE_MSG(hWnd, WM_CREATE, onCreate);
//            HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
            HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
            HANDLE_MSG(hWnd, WM_SIZE, onSize);
            HANDLE_MSG(hWnd, WM_PAINT, onPaint);
//            HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
//            HANDLE_MSG(hWnd, WM_INITDIALOG, onInitDialog);
            HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
//            HANDLE_MSG(hWnd, WM_CTLCOLORBTN, onCtlColorBtn);
//            HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
//            HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
//            HANDLE_MSG(hWnd, WM_SETCURSOR, onSetCursor);

            default:
                return defProc(hWnd, msg, wParam, lParam);
        }
}







/*
Process any commands - button presses
All button presses are forwarded to parent FrontEndClass
*/


void
NavigateBarClass::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {

FORWARD_WM_COMMAND(GetParentWindowHandle(), id, hwndCtl, codeNotify, PostMessage);

}




/*
Process the resizing of the bar, including resetting the buttons' positions
*/

void
NavigateBarClass::onSize(HWND hwnd, UINT state, int cx, int cy) {



}








void
NavigateBarClass::onPaint(HWND handle) {

// if DIBs aren't loaded then return
if(bkDIB == NULL || screenDIB == NULL) return;


    // allocate device context for painting
    PAINTSTRUCT ps;
    // begin paint session
    HDC hdc = BeginPaint(handle, &ps);

    // integrate palettes
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);

    // blit screen DIB to window
    BitBlt(hdc,0,0,screenDIB->getWidth(),screenDIB->getHeight(),screenDIB->getDC(),0,0,SRCCOPY);

    // revert to old palette
    SelectPalette(hdc, oldPal, FALSE);

    // end paint session
    EndPaint(handle, &ps);

}









/*
Process drawing of UsedDrawn buttons
Each button may be normal or pushed
*/


void
NavigateBarClass::onDrawItem(HWND handle, const DRAWITEMSTRUCT* lpDrawItem) {
int pushed=0;

switch(lpDrawItem->CtlID) {

    case BarButtonId_A : {  // Button A
        pushed = SendMessage(Button_A, BM_GETSTATE, 0, 0);
        if(pushed & BST_PUSHED) DrawButtonBitmap(Button_A, buttonApushed_DIB, lpDrawItem, SRCCOPY);
        else DrawButtonBitmap(Button_A, buttonA_DIB, lpDrawItem,SRCCOPY);
    break; }


    case BarButtonId_B : {  // Button B
        pushed = SendMessage(Button_B, BM_GETSTATE, 0, 0);
        if(pushed & BST_PUSHED) DrawButtonBitmap(Button_B, buttonBpushed_DIB, lpDrawItem,SRCCOPY);
        else DrawButtonBitmap(Button_B, buttonB_DIB, lpDrawItem,SRCCOPY);
    break; }


    case BarButtonId_C : {  // Button C
        pushed = SendMessage(Button_C, BM_GETSTATE, 0, 0);
        if(pushed & BST_PUSHED) DrawButtonBitmap(Button_C, buttonCpushed_DIB, lpDrawItem,SRCCOPY);
        else DrawButtonBitmap(Button_C, buttonC_DIB, lpDrawItem,SRCCOPY);
    break; }


    case BarButtonId_D : {  // Button D
        pushed = SendMessage(Button_D, BM_GETSTATE, 0, 0);
        if(pushed & BST_PUSHED) DrawButtonBitmap(Button_D, buttonDpushed_DIB, lpDrawItem,SRCCOPY);
        else DrawButtonBitmap(Button_D, buttonD_DIB, lpDrawItem,SRCCOPY);
    break; }



    default : {
        FORCEASSERT("ERROR - invalid button ID in owner-draw situation !");
    break; }

}  // end of switch

}



/*
Trap the CTLCOLORBUTTON message & do nothing with it - to stop button's background filling with default colour on paint
*/

HBRUSH
NavigateBarClass::onCtlColorBtn(HWND parent, HDC hdc, HWND button, int type) {

return NULL;
// hopefully this won't default to the system colours
}






/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
