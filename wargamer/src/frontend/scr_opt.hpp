/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCR_OPT_HPP
#define SCR_OPT_HPP

#include "menubase.hpp"
#include "palette.hpp"
#include "optwind.hpp"

class OptionsScreenClass : public MenuScreenBaseClass, public OptionsWindowInterface {

public:

    OptionsScreenClass(HWND parent);
    ~OptionsScreenClass(void);

private:

	PixelRect d_mainRect;

	// info rects
	RECT selectDifficultyRect;

	RECT noviceRect;
	RECT noviceTextRect;
	RECT normalRect;
	RECT normalTextRect;
	RECT expertRect;
	RECT expertTextRect;

	RECT customGameRect;
	RECT customGameTextRect;

	RECT optionsRect;

	OptionsWindow * d_optionsWindow;

	DIB * d_textBoxDIB;
	DIB * d_noviceDIB;
	DIB * d_normalDIB;
	DIB * d_expertDIB;
	DIB * d_plaqueDIB;

	int d_darkenAmmount;

	enum MouseDownBox {
		None,
		Novice,
		Normal,
		Expert
	};

	MouseDownBox d_mouseDownBox;

	virtual void SettingsChanged(void);

	void calculateWindowSizes(void);
	void drawScreenDIB(void);

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onSize(HWND hwnd, UINT state, int cx, int cy);
    void onPaint(HWND handle);
	void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);

};

#endif
