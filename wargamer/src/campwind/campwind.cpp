/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Windows
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campwind.hpp"
#include "app.hpp"
#include "gamectrl.hpp"
#include "mapwind.hpp"
#include "mw_data.hpp"
#ifdef CUSTOMIZE
#include "mw_edit.hpp"
#endif
#include "savegame.hpp"
#include "palette.hpp"
#include "mainwind.hpp"
#include "framewin.hpp"
#include "scenario.hpp"
#include "bmp.hpp"
#include "msgwin.hpp"
#include "obwin.hpp"
#include "resdef.h"
#include "campint.hpp"
#include "campdint.hpp"
#include "optdial.hpp"
#if !defined(EDITOR)
#include "combinedInterface.hpp"
#endif
#include "victint.hpp"
#include "camptool.hpp"
#include "toolbar.hpp"
#include "wmisc.hpp"
#include "tooltip.hpp"
#include "weathwin.hpp"
#include "wg_msg.hpp"
#include "victwind.hpp"
#include "cbatdial.hpp"
#include "cbr_wind.hpp"
#include "reorgldr.hpp"
#include "find.hpp"
#include "town.hpp"
#if !defined(EDITOR)
#include "nm_wind.hpp"
#include "victory.hpp"
#endif
#include "spwind.hpp"
#include "s_result.hpp"
#if defined(CUSTOMIZE)
#include "editnat.hpp"
#include "chckdata.hpp"
#endif
#ifdef DEBUG
#include "cmenu.hpp"
#include "todolog.hpp"
#include "logw_imp.hpp"
#include "palwin.hpp"
#endif
#include "armies.hpp"
#include "options.hpp"
#include "alertbox.hpp"
#if defined(CUSTOMIZE)
#include "editdate.hpp"
#include "editinfo.hpp"
#endif
#include "plyrdlg.hpp"
#include "resstr.hpp"

/*
 * Some constants to help in positioning windows
 */

#ifdef DEBUG

enum DebugMenuItems
{
   IDM_StartArmistice = IDM_LAST,
};

enum MenuSections
{
    IDM_MENU_FILE,
    IDM_MENU_VIEW,
    IDM_MENU_SETTINGS,
    IDM_MENU_WINDOW,
#if defined(CUSTOMIZE)
    IDM_MENU_CUSTOMIZE,
#endif
    IDM_MENU_INFO,
#ifdef DEBUG
    IDM_MENU_DEBUG,
    IDM_MENU_HELP
#endif
};

#endif



/*==============================================================
 * Campaign Windows
 */

const CampaignData* CampaignWindows::getCampaignData() const
{
   return d_campaignGame->getCampaignData();
}

#ifdef CUSTOMIZE
CampaignData* CampaignWindows::getCampaignData()
{
   return d_campaignGame->getCampaignData();
}
#endif



/*
 * Constructor
 */

CampaignWindows::CampaignWindows(CampaignInterface* game, HWND hMain) :
   d_campaignGame(game),
   d_mainWind(0),
   d_toolbar(0),
#if !defined(EDITOR)
   d_statWind(0),
#endif
   d_mapWind(0),
   d_tinyMap(0),
   d_obWind(0),
// d_cClockWind(0),
#if defined(DEBUG)
   d_palWind(0),
#endif
#ifdef CUSTOMIZE
   d_mapEdit(0),
#endif
#if !defined(EDITOR)
   d_nsWind(0),
//   d_currentUser(0),
   d_weatherWindow(0),
   d_messageWindow(0),
   d_iLeaderWind(0),
   d_victoryWindow(0),
   d_ui(0),
#endif
   d_poDial(0),
#ifdef DEBUG
   // d_psDial(0),
   d_suspended(true),
#endif
   d_findItem(0)
{
   ASSERT(game != 0);

#if !defined(EDITOR)
//   for(int i = 0; i < OrderTB_HowMany; i++)
//     d_userInterface[i] = 0;
#endif

   create(hMain);
   updateAll();
}

CampaignWindows::~CampaignWindows()
{
   destroy();

#if !defined(EDITOR)
   if (d_victoryWindow)
   {
      delete d_victoryWindow;
      d_victoryWindow = 0;
   }
#endif   // !defined(EDITOR)

   if(d_mainWind != 0)
   {
     delete d_mainWind;
     d_mainWind = 0;
   }

#if 0       // Done as part of destroy()
   if(d_toolbar)
     delete d_toolbar;

#if !defined(EDITOR)
   if(d_nsWind)
     delete d_nsWind;

   if(d_weatherWindow)
     delete d_weatherWindow;

   if(d_iLeaderWind)
     delete d_iLeaderWind;

   if(d_spWind)
     delete d_spWind;

   if(d_statWind)
     delete d_statWind;
#endif

   if(d_tinyMap)
     delete d_tinyMap;

   if(d_findItem)
     delete d_findItem;
#endif

#ifdef CUSTOMIZE
   if(d_mapEdit)
     delete d_mapEdit;
#endif

}


/*
 * Create Campaign Game Windows
 * Parameters:
 *    Main Window to use as parent
 * Returns:
 *    FALSE : Error
 *    TRUE  : OK
 *
 * Each window should ideally be set up not to display until
 * we get around to positionWindows()
 *
 */

void CampaignWindows::create(HWND hMain)
{
#ifdef DEBUG
   debugLog("+CampaignWindows::create()\n");
#endif

   loadPalette();

#if !defined(NOLOG)
//   g_logWindow.set(new WindowsLogWindow(hMain, "Log Window", "w_debug.log"));
   g_logWindow.set(WindowsLogWindow::make(hMain, "Log Window", "w_debug.log"));
#endif

   /*
    * Map Window
    */

   d_mapWind = MapWindow::make(this, getCampaignData(), hMain);
   ASSERT(d_mapWind != 0);

#ifdef CUSTOMIZE
   d_mapEdit = new MapWindowEditors(d_mapWind, getCampaignData(), this);
#endif


   /*
    * Things that go in sideBar
    */

   d_tinyMap = new TinyMap(hMain, this, d_mapWind);
   ASSERT(d_tinyMap);

#if !defined(EDITOR)
   /*
    * Message Window
    */

   d_messageWindow = MessageWindow::make(this, getCampaignData(), hMain);
   ASSERT(d_messageWindow);

   /*
    * Weather Window
    */

   d_weatherWindow = new CampaignWeatherWindow(this, getCampaignData(), hMain);
   ASSERT(d_weatherWindow);

   /*
    * Order Dialogs
    */

   makeOrderWindows();

   createCampaignBattleWindows();

   /*
    * Reorg leader window
    */

   d_iLeaderWind = new ReorgLeader_Int(hMain, this, getCampaignData());
   ASSERT(d_iLeaderWind);

   // sp window
   d_spWind = new RepoSP_Int(hMain, this, getCampaignData());
   ASSERT(d_spWind);

   /*
    * Nation Status window
    */

   d_nsWind = new NationStatus_Int(hMain, this, getCampaignData());
   ASSERT(d_nsWind);


#endif

   /*
    * Status Window
    */

   /*
    * Status Window
    */

// d_statWind = new StatusWindow(hMain, this);
#if !defined(EDITOR)
   d_statWind = new GameStatus_Int(hMain, this, getCampaignData(),
      d_campaignGame->getTimeControl(), getCampaignData()->getTimeData());
   ASSERT(d_statWind != 0);
#endif

   /*
    * create find town / leader windows
    */

   d_findItem = new FindItem(this, getCampaignData());
   ASSERT(d_findItem);

   /*
    * Create mainWindow
    */

   d_mainWind = new MainCampaignWindow(hMain, this, SW_SHOWDEFAULT);
   ASSERT(d_mainWind != 0);

#ifdef DEBUG
        // Create extra menu items for finishing the game
      {
         CustomMenu* menu = d_mainWind->getMenu();
        MenuWindow* hMenu = menu->getSubMenu(0,0);
        MenuWindow* debugMenu = menu->getSubMenu(hMenu, IDM_MENU_DEBUG);
        int i = menu->getMenuItemCount(debugMenu);

        MenuItemInfo miSep;
        miSep.d_id = -1;
        miSep.d_state = MenuItemInfo::CMS_Seperator;
        miSep.d_subMenu = 0;
        miSep.d_text = "---";
        SetRect(&miSep.d_rect, 0,0,0,0);
        menu->insertMenuItem(debugMenu, i++, TRUE, &miSep);

        MenuItemInfo mi;
        mi.d_state = MenuItemInfo::CMS_Enabled;
        mi.d_subMenu = 0;
        SetRect(&mi.d_rect, 0,0,0,0);

        mi.d_id = IDM_StartArmistice;
        mi.d_text = "Start Armistice";
        menu->insertMenuItem(debugMenu, i++, TRUE, &mi);
    }
#endif

   /*
    * Create Tool bar
    */

   d_toolbar = new ToolBar(hMain, this);
   ASSERT(d_toolbar != 0);

   positionWindows();

#ifdef DEBUG
   debugLog("-CampaignWindows::create()\n");
#endif
}

void CampaignWindows::removeGameWindows()
{
#if !defined(EDITOR)
//    for(int i = 0; i < OrderTB_HowMany; i++)
//    {
//      if(d_userInterface[i] != 0)
//      {
//        delete d_userInterface[i];
//        d_userInterface[i] = 0;
//      }
//    }
//    d_currentUser = 0;
      delete d_ui;
      d_ui = 0;
#endif

   if(d_tinyMap)
   {
     // d_tinyMap->destroy();
     delete d_tinyMap;
     d_tinyMap = 0;
   }

   if(d_obWind)
   {
      // DestroyWindow(d_obWind->getHWND());
      delete d_obWind;
      d_obWind = 0;
   }

   if(d_toolbar)
   {
      // d_toolbar->destroy();
      delete d_toolbar;
      d_toolbar = 0;
   }

#if !defined(EDITOR)
   if(d_statWind)
   {
     // d_statWind->destroy();
     delete d_statWind;
     d_statWind = 0;
   }
#endif

   if(d_findItem)
   {
     // d_findItem->destroy();
     delete d_findItem;
     d_findItem = 0;
    }

#if !defined(EDITOR)

   if(d_nsWind)
   {
     // d_nsWind->destroy();
     delete d_nsWind;
     d_nsWind = 0;
    }

   if(d_iLeaderWind)
   {
     // d_iLeaderWind->destroy();
     delete d_iLeaderWind;
     d_iLeaderWind = 0;
    }

   if(d_spWind)
   {
     // d_spWind->destroy();
     delete d_spWind;
     d_spWind = 0;
    }

   if(d_messageWindow)
   {
     // d_messageWindow->destroy();
     delete d_messageWindow;
     d_messageWindow = 0;
   }

   if(d_weatherWindow)
   {
     // d_weatherWindow->destroy();
     delete d_weatherWindow;
     d_weatherWindow = 0;
   }

#endif   // !EDITOR

   if(d_mapWind)
   {
      // DestroyWindow(d_mapWind->getHWND());
      delete d_mapWind;
      d_mapWind = 0;
   }

#if !defined(EDITOR)
   if (d_victoryWindow)
   {
      delete d_victoryWindow;
      d_victoryWindow = 0;
   }
#endif

}

void CampaignWindows::destroy()
{
#ifdef DEBUG
   debugLog("+CampaignWindows::destroy()\n");
#endif

#if !defined(NOLOG)
   g_logWindow.clear();
//   if(d_logWin)
//   {
//     delete d_logWin;   // DestroyWindow(d_logWin->getHWND());
//     d_logWin = 0;
//     logWindow = 0;
//   }
#endif

#if !defined(EDITOR)
   destroyCampaignBattleWindows();
#endif

   removeGameWindows();


   //---- Palette Management has been rewritten
   // Palette::set(NULL);

#ifdef DEBUG
   debugLog("-CampaignWindows::destroy()\n");
#endif
}

void CampaignWindows::suspend(bool visible)
{
   ASSERT(d_suspended != visible);

   if(visible)
      loadPalette();

   if(visible)
   {
      // Enable subclass
      d_mainWind->enable();
   }
   else
   {
      // disable subclass
      d_mainWind->disable();
   }

  if(d_mapWind)
      d_mapWind->show(visible);

  if(d_toolbar)
      d_toolbar->show(visible);

#if !defined(EDITOR)
  if(d_statWind)
      d_statWind->show(visible);

  if(d_nsWind)
      d_nsWind->show(visible);

  if(d_weatherWindow)
      d_weatherWindow->show(visible);

  if(d_messageWindow)
      d_messageWindow->show(visible);

#endif

  if(d_tinyMap)
      d_tinyMap->show(visible);


   if(visible)
   {
      ASSERT(d_findItem == 0);
      d_findItem = new FindItem(this, getCampaignData());
      ASSERT(d_findItem != 0);

#if !defined(EDITOR)
//       ASSERT(d_userInterface[UnitOrder] == 0);
//       d_userInterface[UnitOrder] = new CampaignUnitInterface(this, getCampaignData(), d_mapWind);
//       ASSERT(d_userInterface[UnitOrder] != 0);
//
//       ASSERT(d_userInterface[TownOrder] == 0);
//       d_userInterface[TownOrder] = new CampaignTownInterface(this, getCampaignData(), d_mapWind);
//       ASSERT(d_userInterface[TownOrder] != 0);
      d_ui = new CombinedCInterface(this, getCampaignData(), d_mapWind);

//      toggleOrderWindow();

      ASSERT(d_iLeaderWind == 0);
      d_iLeaderWind = new ReorgLeader_Int(d_mainWind->getHWND(), this, getCampaignData());
      ASSERT(d_iLeaderWind != 0);

      ASSERT(d_spWind == 0);
      d_spWind = new RepoSP_Int(d_mainWind->getHWND(), this, getCampaignData());
      ASSERT(d_spWind != 0);
#endif
   }
   else
   {
      if(d_findItem)
      {
         // d_findItem->destroy();
         delete d_findItem;
         d_findItem = 0;
      }

#if !defined(EDITOR)
//       for(int i = 0; i < OrderTB_HowMany; i++)
//       {
//       if(d_userInterface[i] != 0)
//       {
//          delete d_userInterface[i];
//          d_userInterface[i] = 0;
//       }
//       }
//       d_currentUser = 0;
      delete d_ui;
      d_ui = 0;

      if(d_iLeaderWind)
      {
         // d_iLeaderWind->destroy();
         delete d_iLeaderWind;
         d_iLeaderWind = 0;
      }
      if(d_spWind)
      {
         // d_spWind->destroy();
         delete d_spWind;
         d_spWind = 0;
      }
#endif

   }

#ifdef DEBUG
   d_suspended = visible;
#endif
}

void CampaignWindows::showWindows()
{
  if(d_mapWind)
    d_mapWind->show(true);

    // ShowWindow(d_mapWind->getHWND(), SW_SHOW);

  if(d_toolbar)
    d_toolbar->show(true);

#if !defined(EDITOR)
  if(d_statWind)
    d_statWind->show(true);

  if(d_nsWind)
    d_nsWind->show(true);

  if(d_weatherWindow)
    d_weatherWindow->show(true);

  if(d_messageWindow)
    d_messageWindow->show(true);

//  toggleOrderWindow();

#endif

  if(d_tinyMap)
    d_tinyMap->show(true);

}

void CampaignWindows::loadPalette()
{
   /*
    * Set up palette
    *
    * readBMP automatically sets gApp.hPalette
    */

   ASSERT(scenario != 0);

   const char* fileName = scenario->getPaletteFileName();
   ASSERT(fileName != 0);

   try {
      BMP::getPalette(fileName);
   }
   catch(const BMP::BMPError& e)
   {
      throw CampaignWindowError();
   }
}


/*
 * This is called from MainWindow::onSize
 * and also just after creating all the windows
 *
 * Note: getWindowRect returns Screen coordinates of window's
 *       top-left, and bottom-right coordinate NOT the size.
 */


/*
 * Convert Screen coordinate Rectangle into a sized rectangle
 * relative to parent window
 */

void ScreenToClient(HWND hwnd, RECT* r)
{
   POINT p;
   p.x = r->left;
   p.y = r->top;
   ScreenToClient(hwnd, &p);
   r->left = p.x;
   r->top = p.y;

   p.x = r->right;
   p.y = r->bottom;
   ScreenToClient(hwnd, &p);
   r->right = p.x - r->left;
   r->bottom = p.y - r->top;
}


void CampaignWindows::positionWindows()
{

   if(d_mainWind)
   {
      HDWP hDefer = BeginDeferWindowPos(10);

      RECT rMain;

      GetClientRect(d_mainWind->getHWND(), &rMain);
      SetRect(&rMain, rMain.left, d_mainWind->menuHeight(), rMain.right, rMain.bottom);

      /*
       * Toolbar
       */

      if(d_toolbar != 0)
      {
         // if(d_toolbar->shouldShow())
         {
           PixelPoint p(rMain.left, rMain.top);

           d_toolbar->position(p);

           rMain.top += d_toolbar->height();
         }
      }

#if !defined(EDITOR)
      /*
       * Nation Status Window
       */

      if(d_nsWind)
      {
         PixelPoint p(rMain.left, rMain.top);
         d_nsWind->position(p);

         rMain.top += d_nsWind->height();
      }

      /*
       * Status Window
       */

      if(d_statWind)
      {
        PixelPoint p(rMain.left, rMain.bottom - d_statWind->height());
        d_statWind->position(p);

        rMain.bottom -= d_statWind->height();
      }
#endif

      /*
       * Map Window
       */

      if(d_mapWind)
      {
         hDefer = DeferWindowPos(hDefer,
            d_mapWind->getHWND(), HWND_TOP,
            rMain.left, rMain.top,
            rMain.right-rMain.left, rMain.bottom-rMain.top,
            0);
      }

      EndDeferWindowPos(hDefer);
   }
}

/*
 * Update Magnification Icons
 */

void CampaignWindows::magnificationUpdated()
{
   enableMenu(IDM_ZOOMIN,  (d_mapWind != 0) && d_mapWind->canZoomIn());
   enableMenu(IDM_ZOOMOUT, (d_mapWind != 0) && d_mapWind->canZoomOut());
// checkMenu (IDM_ZOOMIN,  (d_mapWind != 0) && d_mapWind->isZooming());

   if(d_tinyMap)
     d_tinyMap->magnificationUpdated();
}

/*
 * Update Tiny Map Icons
 */


#if !defined(EDITOR)


void CampaignWindows::updateWeather()
{
  if(d_weatherWindow)
    d_weatherWindow->update();
}
#endif   // !EDITOR

/*
 * The time or date has changed
 *
 * For now we just put it in the status line
 */

void CampaignWindows::timeUpdated()
{
#if !defined(EDITOR)
   if(d_statWind)
      d_statWind->updateClock();
#endif

#if 0
   if(d_cClockWind)
      d_cClockWind->update();
#endif
}

/*
 * Update all icons
 */

void CampaignWindows::updateAll()
{
  tinyMapUpdated();

#if !defined(EDITOR)
//  campaignModeUpdated();
  weatherWindowUpdated();
  messageWindowUpdated();
  reorgLeaderUpdated();
  repoSPUpdated();
  showOrdersUpdated();
#endif   // !EDITOR
  findItemUpdated();
  obWindowUpdated();

#ifdef DEBUG
   checkMenu(IDM_PALETTEWINDOW, d_palWind != 0);
   updateGridMenu();
#endif

  checkMenu(IDM_SHOWSUPPLY, Options::get(OPT_ShowSupply));
  checkMenu(IDM_SHOWCHOKEPOINTS, Options::get(OPT_ShowChokePoints));
  checkMenu(IDM_SHOWSUPPLYLINES, Options::get(OPT_ShowSupplyLines));
  checkMenu(IDM_INSTANTORDERS, CampaignOptions::get(OPT_InstantOrders));
#ifdef DEBUG
  checkMenu(IDM_INSTANTMOVE, Options::get(OPT_InstantMove));
#endif
  checkMenu(IDM_COLORCURSOR, Options::get(OPT_ColorCursor));

  checkMenu(IDM_GAMEOPTIONS, d_poDial != 0);
}


void CampaignWindows::editOptions(int page)
{
   ASSERT((page >= 0) && (page < OptionPage_HowMany));

   if(d_poDial != 0)
   {
      d_poDial->setPage(OptionPageEnum(page));
   }
   else
   {
      if(d_mainWind)
      {
         d_poDial = new PlayerOptionsDial(d_campaignGame, d_mainWind->getHWND(), OptionPageEnum(page));
         ASSERT(d_poDial != 0);
      }
   }
}


#if !defined(EDITOR)
#endif   // !EDITOR

/*
 * redraw Campaign Map
 */

void CampaignWindows::redrawMap()
{
   if(d_mapWind)
   {
      d_mapWind->requestRedraw(FALSE);
#if 0
      if(d_currentUser)
        d_currentUser->update();
#endif
   }
}

void CampaignWindows::rebuildMap()
{
   if(d_mapWind)
   {
      d_mapWind->requestRedraw(TRUE);
   }
}

#if !defined(EDITOR)

void CampaignWindows::makeOrderWindows()
{
//   for(int i = 0; i < OrderTB_HowMany; i++)
//     d_userInterface[i] = 0;
//
//   d_userInterface[UnitOrder] = new CampaignUnitInterface(this, getCampaignData(), d_mapWind);
//   ASSERT(d_userInterface[UnitOrder] != 0);
//
//   d_userInterface[TownOrder] = new CampaignTownInterface(this, getCampaignData(), d_mapWind);
//   ASSERT(d_userInterface[TownOrder] != 0);

   ASSERT(!d_ui);
   d_ui = new CombinedCInterface(this, getCampaignData(), d_mapWind);
}


void CampaignWindows::createCampaignBattleWindows()
{
  CampaignBattleDial::create(this, getCampaignData(), d_mapWind);
  CBattleResultInterface::create(d_mapWind->getHWND(), getCampaignData());
  SiegeResult_Int::create(d_mapWind->getHWND(), this, getCampaignData());
}

void CampaignWindows::destroyCampaignBattleWindows()
{
  CampaignBattleDial::destroy();
  CBattleResultInterface::destroy();
  SiegeResult_Int::destroy();
}

// void CampaignWindows::toggleOrderWindow()
// {
//   if(d_campaignGame->getMode() == CampaignInterface::CM_UNITS)
//     d_currentUser = d_userInterface[UnitOrder];
//   else
//     d_currentUser = d_userInterface[TownOrder];
//
//   ASSERT(d_currentUser != 0);
// }
#if 0
/*
 * Set up Order Window for specified Town
 *
 * These functions are probably the same as setTown() and setLeader()
 */

void CampaignWindows::orderTown(ITown itown)
{
//    if(d_campaignGame->getMode() != CampaignInterface::CM_RESOURCE)
//    {
//       d_campaignGame->setMode(CampaignInterface::CM_RESOURCE);
//       toggleOrderWindow();
//    }

//    if(d_currentUser)
//    {
//      d_currentUser->addObject(itown);
//    }

      ASSERT(d_ui);
      d_ui->orderTown(itown);

   // campaignModeUpdated();
}

/*
 * Set up Order Window for specified Unit
 */

void CampaignWindows::orderUnit(ICommandPosition cpi)
{
//    if(d_campaignGame->getMode() != CampaignInterface::CM_UNITS)
//    {
//       d_campaignGame->setMode(CampaignInterface::CM_UNITS);
//       toggleOrderWindow();
//    }
//
//    if(d_currentUser)
//    {
//      d_currentUser->addObject(cpi);
//    }
//
//    campaignModeUpdated();
      ASSERT(d_ui);
      d_ui->orderUnit(cpi);
}
#endif

/*
 * Run pop-up order menu
 */

void CampaignWindows::orderUnit(ConstParamCP cpi)
{
   if(d_ui)
   {
     d_ui->orderUnit(cpi);
   }
}

void CampaignWindows::orderTown(ITown itown)
{
   if(d_ui)
   {
     d_ui->orderTown(itown);
   }
}




void CampaignWindows::addMessage(const CampaignMessageInfo& msg)
{
  if(d_messageWindow)
    d_messageWindow->addMessage(msg);
}

int CampaignWindows::getNumCampaignMessages()
{
  if(d_messageWindow)
    return d_messageWindow->getNumMessages();
  else
    return 0;
}

int CampaignWindows::getCampaignMessagesRead()
{
  if(d_messageWindow)
    return d_messageWindow->getMessagesRead();
  else
    return 0;
}

#endif   // !EDITOR


#if defined(DEBUG)
void CampaignWindows::updateGridMenu()
{
   checkMenu(IDM_GRID, (d_mapWind != 0) && d_mapWind->isGridOn());
}

#endif   // DEBUG

bool CampaignWindows::windowDestroyed(HWND hwnd, int id)
{
   if(d_obWind && (hwnd == d_obWind->getHWND()))
   {
        d_obWind = 0;
        obWindowUpdated();
        return true;
   }
   else if(d_poDial && (hwnd == d_poDial->getHWND()))
   {
      d_poDial = 0;
      // checkMenu(IDM_GAMEOPTIONS, false);
      updateAll();
      rebuildMap();
      return true;
   }
#ifdef DEBUG
   else if(d_palWind && (hwnd == d_palWind->getHWND()))
   {
        d_palWind = 0;
        checkMenu(IDM_PALETTEWINDOW, false);
        return true;
   }
#endif

   return false;
}

#if 0
void CampaignWindows::windowClosed(HWND hwnd)
{
   if(hwnd == d_obWind)
   {
        delete d_obWind;
        d_obWind = 0;
        obWindowUpdated();
   }
   else if(hwnd == d_palWind)
   {
        delete d_palWind;
        d_palWind = 0;
        checkMenu(IDM_PALETTEWINDOW, false);
   }
}
#endif


#if defined(CUSTOMIZE)

void CampaignWindows::invalidateWindows()
{

#if !defined(EDITOR)
   d_messageWindow->clear();
#endif

   if(d_mapWind != 0)
      d_mapWind->requestRedraw(TRUE);
}

#endif

/*
 * Process a Menu Item Command
 *
 * return TRUE if processed, false if not.
 */

bool CampaignWindows::processCommand(HWND hWnd, int id)
{
   switch(id)
   {

    case IDM_ABANDONGAME: {

//    bool doAbandon = False;
      bool oldPause = GameControl::pause(True);

//       if(Options::get(OPT_AlertBox) && AlertBoxOptions::get(AB_ExitGame))
//       {
//          long val = alertBox(APP::getMainHWND(), InGameText::get(IDS_SureAbandonGame),
//                    InGameText::get(IDS_AbandonGame), MB_OKCANCEL);
//
//          AlertBoxOptions::set(AB_ExitGame, (bool)HIWORD(val));
//          doAbandon = (LOWORD(val) == IDOK);
//       }
//       else doAbandon = True;
//
//       if(doAbandon)

         int val = alertBox(APP::getMainHWND(), InGameText::get(IDS_SureAbandonGame),
                    InGameText::get(IDS_AbandonGame), MB_OKCANCEL,
                    AB_AbandonGame,
                    IDOK);

      if(val == IDOK)
      {
         #ifdef DEBUG
         debugLog("really abandoning game\n");
         #endif
         WargameMessage::postAbandon();
      }
      else
      {
         GameControl::pause(oldPause);
         #ifdef DEBUG
         debugLog("Not abandoning\n");
         #endif
      }

        break;
   }

#if 0   // These moved to subclass in gamesup\wg_main
   case IDM_EXIT:
      SendMessage(hWnd, WM_CLOSE, 0, 0);
      break;

   case IDM_ABOUT:
      // DialogBox(APP::instance(), MAKEINTRESOURCE(DLG_ABOUTBOX), hWnd, (DLGPROC) aboutProc);
      aboutDialog();
      break;

   case IDM_HELP_CONTENTS:
      {
//         BOOL bResult = WinHelp(hWnd, scenario->getHelpFileName(), HELP_FINDER, 0L);
//         ASSERT(bResult != FALSE);
      }
      break;

   case IDM_HELP_HISTORY:
      {
//         BOOL bResult = WinHelp(hWnd, scenario->getHistoryFileName(), HELP_FINDER, 0L);
//         ASSERT(bResult != FALSE);
      }
      break;
#ifdef DEBUG
    case IDM_DEBUG:
      throw GeneralError("Forced Error\n");
#endif
#endif

   case IDM_INSTANTORDERS:
      CampaignOptions::toggle(OPT_InstantOrders);
      updateAll();      // Update ticks
      break;

#ifdef DEBUG
   case IDM_INSTANTMOVE:
      Options::toggle(OPT_InstantMove);
      updateAll();      // Update ticks
      break;
#endif

//    case IDM_FULLROUTE:
//       Options::toggle(OPT_FullRoute);
//       break;

   case IDM_COLORCURSOR:
      Options::toggle(OPT_ColorCursor);
      updateAll();      // Update ticks
      break;

#if !defined(EDITOR)
#ifdef DEBUG
   case IDM_PLAYERSETTINGS:
   {
        showPlayerSettings();
#if 0
      if(!d_psDial)
      {
        d_psDial = new PlayerSettingsDial(hWnd, d_campaignGame);
        // d_psDial = new PlayerSettingsDial(hWnd);
        ASSERT(d_psDial != 0);
      }
#endif
      break;
   }
#endif
#endif

// #if !defined(EDITOR)
   case IDM_GAMEOPTIONS:
   {
      togglePlayerOptionsDial();
      break;
   }
// #endif
   case IDM_TINYMAPWINDOW:
      toggleTinyMap();
      break;

   case IDM_FINDTOWN:
      toggleFindTown();
      break;

   case IDM_FINDLEADER:
      toggleFindLeader();
      break;

#ifdef DEBUG
   case IDM_PALETTEWINDOW:
      togglePaletteWindow();
      break;
#endif   // DEBUG

#if !defined(EDITOR)
   case IDM_MESSAGEWINDOW:
      toggleMessageWindow();
      break;

   case IDM_WEATHERWINDOW:
      toggleWeatherWindow();
      break;

//    case IDM_RESOURCEMODE:
//       if(d_campaignGame->getMode() != CampaignInterface::CM_RESOURCE)
//         toggleCampaignMode();
//       else
//         campaignModeUpdated();
//       break;
//
//    case IDM_UNITMODE:
//       if(d_campaignGame->getMode() != CampaignInterface::CM_UNITS)
//         toggleCampaignMode();
//       else
//         campaignModeUpdated();
//       break;

   case IDM_ILEADERS:
      toggleReorgLeaders();
      break;

   case IDM_REPOPOOL:
      toggleRepoSP();
      break;

#endif
#ifdef DEBUG
   case IDM_GRID:
      ASSERT(d_mapWind != 0);
      if(d_mapWind != 0)
         d_mapWind->toggleGrid();
      break;
#endif
   case IDM_ZOOMIN:
      ASSERT(d_mapWind != 0);
      if(d_mapWind != 0)
        if(d_mapWind->canZoomIn())
         d_mapWind->initZoomIn();
         // d_mapWind->toggleZoomMode();
      break;
   case IDM_ZOOMOUT:
      ASSERT(d_mapWind != 0);
      if(d_mapWind != 0)
        if(d_mapWind->canZoomOut())
         d_mapWind->zoomOut();
         // d_mapWind->zoomOut();
      break;

   case IDM_SHOWALLORDERS:
   {
#if !defined(EDITOR)
      toggleShowOrders();
#endif
      break;
   }

   case IDM_SHOWSUPPLY:
      Options::toggle(OPT_ShowSupply);
      rebuildMap();
      updateAll();      // Update ticks
      break;
   case IDM_SHOWCHOKEPOINTS:
      Options::toggle(OPT_ShowChokePoints);
      rebuildMap();
      updateAll();      // Get ticks setup
      break;
   case IDM_SHOWSUPPLYLINES:
      Options::toggle(OPT_ShowSupplyLines);
      rebuildMap();
      updateAll();      // Update ticks
      break;

#ifdef CUSTOMIZE
   case IDM_PRINTMAP:
     ASSERT(d_mapWind);
     d_mapWind->printMap();
     break;
#endif

#if defined(CUSTOMIZE)
   case IDM_LOADWORLD:
      WargameMessage::postNewCampaign();
      // SaveGame::loadWorld();
      break;

   case IDM_SAVEWORLD:
      // SaveGame::saveWorld(d_campaignGame);
      d_campaignGame->requestSaveWorld();
      break;
#endif

   case IDM_LOADGAME:
#if defined(EDITOR)
      WargameMessage::postNewCampaign();
#else
      WargameMessage::postLoadGame();
#endif
      break;

   case IDM_SAVEGAME:
#if defined(EDITOR)
      d_campaignGame->requestSaveWorld();
#else
      d_campaignGame->requestSaveGame(true);
#endif
      break;

//   case IDM_ABANDONGAME:
//      WargameMessage::postAbandon();
//      break;

#if !defined(EDITOR)
#ifdef DEBUG        // for testing
   case IDM_LOADVICTORYSCREEN_0:
      d_campaignGame->endCampaign(GameVictory(0, static_cast<GameVictory::VictoryLevel>(random(GameVictory::Minor, GameVictory::Crushing + 1))));
      break;

   case IDM_LOADVICTORYSCREEN_1:
      d_campaignGame->endCampaign(GameVictory(1, static_cast<GameVictory::VictoryLevel>(random(GameVictory::Minor, GameVictory::Crushing + 1))));
      break;

   case IDM_LOADVICTORYSCREEN_DRAW:
      d_campaignGame->endCampaign(GameVictory(SIDE_Neutral, GameVictory::NoVictor));
      break;

   case IDM_StartArmistice:
      {
         const CampaignData* campData = d_campaignGame->getCampaignData();
         CampaignData* ncCampData = const_cast<CampaignData*>(campData);
         ncCampData->startArmistice(campData->getTick() + WeeksToTicks(1));
      }
      break;

#endif
#endif

#if defined(CUSTOMIZE)
   case IDM_EDIT_TOWNS:
      if(d_mapEdit != 0)
         d_mapEdit->toggleEditMode(PM_EditTown);
      break;

   case IDM_EDIT_PROVINCES:
      if(d_mapEdit != 0)
         d_mapEdit->toggleEditMode(PM_EditProvince);
      break;

   case IDM_EDIT_CONNECTIONS:
      if(d_mapEdit != 0)
         d_mapEdit->toggleEditMode(PM_EditConnection);
      break;

   case IDM_EDIT_DATE:
      if(DateEditor::edit(hWnd, getCampaignData()->getTimeData()))
         timeUpdated();
      break;

   case IDM_EDITINFO:
      CampaignInfoEditor::edit(hWnd, getCampaignData());
      break;

   case IDM_EDITCONDITIONS:
      if(d_mapEdit != 0)
      {
          d_mapEdit->toggleEditMode(PM_EditConditions);
          d_mapEdit->editObject();
      }

      break;

   case IDM_EDITALLEGIANCE:
      NationAllegianceEditor::edit(hWnd, getCampaignData());
      break;

   case IDM_EDIT_OB:
      if(d_mapEdit != 0)
         d_mapEdit->toggleEditMode(PM_EditOB);
      break;

   case IDM_NEW_OBJECT:
      if(d_mapEdit != 0)
         d_mapEdit->newObject();
      break;

#if !defined(EDITOR)
   case IDM_EDIT_NOTHING:
      if(d_mapEdit != 0)
         d_mapEdit->toggleEditMode(PM_Playing);
      break;
#endif

#ifdef CUSTOMIZE
   case IDM_DATACHECK:
     runDataSanityCheck();
     break;
#endif


   case IDM_REMOVE_TOWNS:
      d_campaignGame->removeAllTowns();
      rebuildMap();
      break;

   case IDM_REMOVE_UNITS:
      d_campaignGame->removeAllUnits();
      rebuildMap();
      break;

#endif   // CUSTOMIZE

   case IDM_OB:
      toggleOrderBattle();
      break;

   case IDM_MENU:
     d_mainWind->toggleMenu();
     break;


       default:
            return false;

   }

    return true;
}

/*
 * Display Hint Line about menu item
 *
 */

void CampaignWindows::showHint(int id)
{
   if(id == 0)
      g_toolTip.clearHint();
   else
      g_toolTip.showHint(APP::instance(), id);
}

/*
 * Update Status Window's MessageWindow section
 */

void CampaignWindows::msgWinUpdated()
{
#if !defined(EDITOR)
   if(d_statWind)
      d_statWind->updateMessageWindow();
#endif
}

/*
 * Message Handler for DIALOG can't use message crackers because
 * it must return TRUE or FALSE depending on whether message
 * was processed or not.
 */


static BOOL CALLBACK saveQuitProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
   switch( msg )
   {
   case WM_INITDIALOG:
      return TRUE;

   case WM_COMMAND:
#ifdef DEBUG
      debugLog("WM_COMMAND %d\n", (int) LOWORD(wParam));
#endif
      EndDialog(hwnd, (int) LOWORD(wParam));
      break;

   case WM_CLOSE:
      EndDialog(hwnd, IDSQ_CANCEL);
      break;

   default:
      return FALSE;
   }

   return TRUE;
}


/*
 * Do actions on closing window
 * Return True if window is really closing, False to continue
 */

bool CampaignWindows::onClose(HWND hwnd)
{
   bool doQuit = False;
   bool oldPause = GameControl::pause(True);

   /*
    * Ask user if they want to save the game first
    */

   if(getCampaignData() && getCampaignData()->hasChanged())
   {
#ifdef DEBUG
      debugLog("Campaign has changed\n");
#endif
      int value = DialogBox(APP::instance(), MAKEINTRESOURCE(DLG_SAVEQUIT), APP::getMainHWND(), (DLGPROC) saveQuitProc);
      switch(value)
      {
         case IDSQ_SAVE:
            d_campaignGame->requestSaveWorld();
         case IDSQ_QUIT:
            doQuit = True;
            break;
      }
   }
   else
   {
      GameControl::pause(oldPause);
      return false;
   }

   if(doQuit)
   {
#ifdef DEBUG
      debugLog("really quitting\n");
#endif
      PostQuitMessage(0);
   }
   else
   {
      GameControl::pause(oldPause);
#ifdef DEBUG
      debugLog("Not quitting\n");
#endif
   }

   return true;
}


/*
 * MapWindowUser Implementation
 */

/*
 * Left button clicked
 */

void CampaignWindows::onLClick(MapSelect& info)
{
#if !defined(EDITOR)
  if(d_ui)
  {
    d_ui->onLClick(info);
  }

#endif
}

/*
 * Zoom Area has changed
 */

void CampaignWindows::updateZoom()
{
#if !defined(EDITOR)
   if(d_ui)
     d_ui->updateZoom();
#endif
}

/*
 * Mouse is over a unit
 */

void CampaignWindows::overObject(MapSelect& info)
{
#if !defined(EDITOR)
  if(d_ui)
  {
    d_ui->overObject(info);
  }
#endif
}

void CampaignWindows::notOverObject()
{
#if !defined(EDITOR)
  if(d_ui)
    d_ui->notOverObject();
#endif // !defined(EDITOR)
}


/*
 * Map is being drawn into dib
 * User can do any drawing it wants to do
 */

void CampaignWindows::onDraw(DrawDIBDC* dib, MapSelect& info)
{
#if !defined(EDITOR)
   if(d_ui)
     d_ui->onDraw(dib, info);
#endif

   if(d_tinyMap)
      d_tinyMap->draw(info.getCP());
}

/*
 * Map Window has been redrawn...
 * Update tiny map Window
 */

void CampaignWindows::mapRedrawn()
{
   if(d_tinyMap)
      d_tinyMap->areaChanged();
}

/*
 * Left button is pressed
 */

void CampaignWindows::onButtonDown(MapSelect& info)
{
#if !defined(EDITOR)
  if(d_ui)
    d_ui->onButtonDown(info);
#endif
}

#ifdef CUSTOMIZE
void CampaignWindows::editObject(const MapSelect& info, const PixelPoint& p)
{
   if(d_mapEdit)
      d_mapEdit->editObject(info, p);
}
#endif

/*
 * Start dragging
 */

bool CampaignWindows::onStartDrag(MapSelect& info)
{
#if !defined(EDITOR)
   if(d_ui)
     return d_ui->onStartDrag(info);
#endif
   return false;
}
/*
 * Stop dragging
 */

void CampaignWindows::onEndDrag(MapSelect& info)
{
#if !defined(EDITOR)
   if(d_ui)
     d_ui->onEndDrag(info);
#endif
}

/*
 * Mouse has moved while dragging
 */

void CampaignWindows::onDrag(MapSelect& info)
{
#if !defined(EDITOR)
   if(d_ui)
     d_ui->onDrag(info);
#endif
}
/*
 * Right Button clicked
 */

void CampaignWindows::onRClick(MapSelect& info)
{
#if !defined(EDITOR)
   if(d_ui)
     d_ui->onRClick(info);
#endif
}
/*
 * Return how to do tracking
 */

TrackMode CampaignWindows::getTrack()
{
#if !defined(EDITOR)
   if(d_ui)
      return d_ui->getTrack();
   else
#endif
      return MWTM_None;
}



void CampaignWindows::checkMenu(int id, bool flag)
{
   if(d_mainWind != 0)
      d_mainWind->checkMenu(id, flag);

   if(d_toolbar != 0)
      d_toolbar->setCheck(id, flag);
}

void CampaignWindows::enableMenu(int id, bool flag)
{
   if(d_mainWind != 0)
      d_mainWind->enableMenu(id, flag);

   if(d_toolbar != 0)
      d_toolbar->enable(id, flag);
}

#if !defined(EDITOR)

void CampaignWindows::runVictoryScreen()
{
   removeGameWindows();
   d_victoryWindow = CampaignVictoryWindow::create(this);
   d_victoryWindow->show(true);
   ASSERT(d_victoryWindow);
}
void CampaignWindows::endVictoryScreen()
{
   ASSERT(d_victoryWindow);
   delete d_victoryWindow;
   d_victoryWindow = 0;
   d_campaignGame->endVictoryScreen();
}

#endif      // EDITOR

#ifdef CUSTOMIZE   // used by Data Sanity Check routines
void CampaignWindows::runOBEdit(ICommandPosition cpi)
{
  d_mapEdit->runOBEdit(cpi);
}

void CampaignWindows::runTownEdit(ITown iTown)
{
  d_mapEdit->runTownEdit(iTown);
}

void CampaignWindows::runProvinceEdit(IProvince iProv)
{
  d_mapEdit->runProvinceEdit(iProv);
}

void CampaignWindows::runConnectionEdit(ITown iTown)
{
  d_mapEdit->runConnectionEdit(iTown);
}

void CampaignWindows::runConditionEdit(WG_CampaignConditions::Conditions* condition)
{
  d_mapEdit->runConditionEdit(condition);
}

void CampaignWindows::runDataSanityCheck()
{
   if(!CampaignDataSanityCheck::run(d_campaignGame->getCampaignData(), this))
   {
     WargameMessage::postAbandon();
   }

}
#endif

void CampaignWindows::centerOnMap(const Location& l, UBYTE magEnum)
{
  d_mapWind->centerOnMap(l, static_cast<MagEnum>(magEnum));
}

// void CampaignWindows::centerMapOnUnit(ConstICommandPosition cpi, const Location& l, UBYTE magEnum)
// {
//   d_mapWind->setHighlightedUnit(cpi);
//   d_mapWind->centerOnMap(l, static_cast<MagEnum>(magEnum));
// }
//
// void CampaignWindows::centerMapOnTown(ITown iTown, const Location& l, UBYTE magEnum)
// {
//   d_mapWind->setHighlightedTown(iTown);
//   d_mapWind->centerOnMap(l, static_cast<MagEnum>(magEnum));
// }

void CampaignWindows::centerMapOnUnit(ConstICommandPosition cpi)
{
  ASSERT(cpi != NoCommandPosition);
  d_mapWind->setHighlightedUnit(cpi);

  Location l;
  cpi->getLocation(l);

  MagEnum magLevel = d_mapWind->getZoomEnum();
  if (d_mapWind->canZoomIn())
  {
      ++magLevel;
  }

  d_mapWind->centerOnMap(l, magLevel);
}

void CampaignWindows::centerMapOnTown(ITown iTown)
{
  ASSERT(iTown != NoTown);
  d_mapWind->setHighlightedTown(iTown);

  const Town& t = d_campaignGame->getCampaignData()->getTown(iTown);
  const Location& l = t.getLocation();

  MagEnum magLevel = d_mapWind->getZoomEnum();
  if (d_mapWind->canZoomIn())
  {
      ++magLevel;
  }

  d_mapWind->centerOnMap(l, magLevel);
}

/*
 * toggle functions
 */

void CampaignWindows::toggleTinyMap()
{
  if(d_tinyMap)
  {
    d_tinyMap->toggle();
  }

  tinyMapUpdated();
}

#if !defined(EDITOR)
// void CampaignWindows::toggleCampaignMode()
// {
//   CampaignInterface::CampaignMode mode = (d_campaignGame->getMode() == CampaignInterface::CM_RESOURCE) ?
//       CampaignInterface::CM_UNITS : CampaignInterface::CM_RESOURCE;
//
//   d_campaignGame->setMode(mode);
//   toggleOrderWindow();
//
//   campaignModeUpdated();
// }

void CampaignWindows::toggleMessageWindow()
{
  d_messageWindow->toggle();
  messageWindowUpdated();
}

void CampaignWindows::toggleWeatherWindow()
{
  if(d_weatherWindow)
    d_weatherWindow->toggle();

  weatherWindowUpdated();
}
#endif    // EDITOR!

void CampaignWindows::togglePlayerOptionsDial()
{
  if(d_mainWind)
  {
    if(!d_poDial)
    {
      d_poDial = new PlayerOptionsDial(d_campaignGame, d_mainWind->getHWND(), DefaultPage);
      ASSERT(d_poDial != 0);
    }
    else
    {
      // DestroyWindow(d_poDial->getHWND());
      delete d_poDial;
      d_poDial = 0;
    }
    // checkMenu(IDM_GAMEOPTIONS, d_poDial != 0);
    updateAll();
  }
}

#if !defined(EDITOR)

void CampaignWindows::toggleShowOrders()
{
  ASSERT(d_mapWind != 0);
  if(d_mapWind != 0)
    d_mapWind->toggleShowAllRoute();

  showOrdersUpdated();
  redrawMap();
}

void CampaignWindows::toggleReorgLeaders()
{
  if(d_iLeaderWind)
  {
    BOOL isShowing = IsWindowVisible(d_iLeaderWind->getHWND());
    if(!isShowing)
      d_iLeaderWind->run();
    else
      d_iLeaderWind->hide();
  }

  reorgLeaderUpdated();
}

void CampaignWindows::toggleRepoSP()
{
  if(d_spWind)
  {
    BOOL isShowing = IsWindowVisible(d_spWind->getHWND());
    if(!isShowing)
      d_spWind->run();
    else
      d_spWind->hide();
  }

  repoSPUpdated();
}

#endif    // EDITOR!

void CampaignWindows::toggleOrderBattle()
{
  /*
   * TODO: OBWindow needs to be rewritten to customized version
   */

  if(d_obWind)
  {   // Force it to top
    // DestroyWindow(d_obWind->getHWND());
    delete d_obWind;
    d_obWind = 0;
  }

  else if(d_mainWind)
  {
    d_obWind = new OBWindow(this, d_mainWind->getHWND());
    ASSERT(d_obWind != 0);
  }

  obWindowUpdated();
}

void CampaignWindows::toggleFindLeader()
{
  if(d_findItem)
  {
    BOOL isShowing = IsWindowVisible(d_findItem->getHWND());

    if(!isShowing || d_findItem->getType() == FindItem::FindTown)
    {
      d_findItem->run(FindItem::FindLeader);
    }
    else
      d_findItem->hide();
  }

  findItemUpdated();
}

void CampaignWindows::toggleFindTown()
{
  if(d_findItem)
  {
    BOOL isShowing = IsWindowVisible(d_findItem->getHWND());

    if(!isShowing || d_findItem->getType() == FindItem::FindLeader)
    {
      d_findItem->run(FindItem::FindTown);
    }
    else
      d_findItem->hide();
  }

  findItemUpdated();
}

#ifdef DEBUG
void CampaignWindows::togglePaletteWindow()
{
   if(d_palWind)
   {
      // delete palWind;
      // DestroyWindow(d_palWind->getHWND());
      delete d_palWind;
      d_palWind = 0;
   }
   else if(d_mainWind)
   {
      d_palWind = new PalWindow(d_mainWind->getHWND());
      ASSERT(d_palWind != 0);
   }

   checkMenu(IDM_PALETTEWINDOW, d_palWind != 0);
}
#endif

/*
 * update menus and toolbars
 */

void CampaignWindows::tinyMapUpdated()
{
  bool flag = (d_tinyMap) ? d_tinyMap->isVisible() : False;
  checkMenu(IDM_TINYMAPWINDOW, flag);

  enableMenu(IDM_TINYMAPWINDOW, (d_tinyMap != 0));
  enableMenu(IDM_ZOOMOUT, d_mapWind->canZoomOut());
  enableMenu(IDM_ZOOMIN, d_mapWind->canZoomIn());
}

#if !defined(EDITOR)
// void CampaignWindows::campaignModeUpdated()
// {
//   checkMenu(IDM_RESOURCEMODE, (d_campaignGame->getMode() == CampaignInterface::CM_RESOURCE));
//   checkMenu(IDM_UNITMODE, (d_campaignGame->getMode() == CampaignInterface::CM_UNITS));
// }

void CampaignWindows::messageWindowUpdated()
{
  checkMenu(IDM_MESSAGEWINDOW, d_messageWindow->isVisible());
}

void CampaignWindows::weatherWindowUpdated()
{
  checkMenu(IDM_WEATHERWINDOW, ((d_weatherWindow) && (d_weatherWindow->isVisible())));
}

void CampaignWindows::showOrdersUpdated()
{
  checkMenu(IDM_SHOWALLORDERS, ((d_mapWind) && (d_mapWind->mapData().showAllRoutes)));
}

void CampaignWindows::reorgLeaderUpdated()
{
  bool flag = (d_iLeaderWind) ? (IsWindowVisible(d_iLeaderWind->getHWND()) != 0) : False;
  checkMenu(IDM_ILEADERS, flag);

  enableMenu(IDM_ILEADERS, (d_iLeaderWind != 0));
}


void CampaignWindows::repoSPUpdated()
{
  bool flag = (d_spWind) ? (IsWindowVisible(d_spWind->getHWND()) != 0): False;
  checkMenu(IDM_REPOPOOL, flag);

  enableMenu(IDM_REPOPOOL, (d_spWind != 0));
}

void CampaignWindows::moraleBarUpdated()
{
  if(d_nsWind)
    d_nsWind->update();
}

#endif

void CampaignWindows::findItemUpdated()
{
  bool flag = (d_findItem) ? (IsWindowVisible(d_findItem->getHWND()) != 0) : False;
  checkMenu(IDM_FINDLEADER, (flag && d_findItem->getType() == FindItem::FindLeader));
  enableMenu(IDM_FINDLEADER, (d_findItem != 0));

  checkMenu(IDM_FINDTOWN, (flag && d_findItem->getType() == FindItem::FindTown));
  enableMenu(IDM_FINDTOWN, (d_findItem != 0));
}


void CampaignWindows::obWindowUpdated()
{
  checkMenu(IDM_OB, (d_obWind != 0));
}

#if !defined(EDITOR)

/*
 * PlayerSettings Implementation
 */

void CampaignWindows::setController(Side side, GamePlayerControl::Controller control)
{
    d_campaignGame->setController(side, control);
}

HWND CampaignWindows::getHWND() const
{
    return d_mainWind->getHWND();
}

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
