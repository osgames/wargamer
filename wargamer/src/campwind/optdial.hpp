/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/


#ifndef _OPTDIAL_HPP
#define _OPTDIAL_HPP

#include "cwdll.h"
#include "dialog.hpp"
#include "options.hpp"		// for GameOptionEnum
#include "campmsg.hpp"

/*
 * Forward references
 */

class OptionManager;
class CampaignInterface;

/*
 * Local stuff
 */

namespace OptionsDialog {

	class PlayerOptionPage;
};

using namespace OptionsDialog;


class PlayerOptionsDial : public ModelessDialog
{
public:
	typedef OptionPageEnum OptionPage;

private:

	 HWND hwndParent;
//	 OptionManager* d_optManager;	// Way of manipulating Options
	 CampaignInterface* d_campaign;

	 // static const int nPages;
	 PlayerOptionPage** d_pages;
	 HWND tabHwnd;						// Tab Dialog handle
	 int curPage;						// Current tabbed dialog Page
	 RECT tdRect;						// Area for tabbed dialogues to fit into

	 enum Mode {
		HasButtons,
		NoButtons
	 } d_mode;

	 struct HelpIDList {
		DWORD s_control;
		DWORD s_topic;
	 }* d_helpIDs;				// Array of Help IDs

  public:
	 PlayerOptionsDial(CampaignInterface* campWind, HWND parent, OptionPage page);
	 PlayerOptionsDial(HWND parent);
	 PlayerOptionsDial(); // {}
	 ~PlayerOptionsDial();
	 void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	 const RECT& getDialogRect() const { return tdRect; }

	 void onOK();
	 void setPage(OptionPage page);

	 ModelessDialog* getPage(OptionPage page) const { return (ModelessDialog*)d_pages[page]; }

	 // const char* getPageResName(OptionPage page) const { return 0; }
	 const DLGTEMPLATE* getPageTemplate(OptionPage page) const;

  private:
	 BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	 void onDestroy(HWND hwnd);
	 void onClose(HWND hwnd);
	 void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
	 LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);

	 void onSelChanged();
	 void onOK(HWND hwnd);

	 void setOption(GameOptionEnum what, Boolean how);

	 void makePages();
			// Create the pages
};

class RealCampaignOptionDial;

class CampaignOptionDial : public Window
{
	 RealCampaignOptionDial* d_campOptionDial;
  public:
	 CAMPWIND_DLL CampaignOptionDial(HWND hwnd);
	 CAMPWIND_DLL ~CampaignOptionDial();

	 // void destroy();
	 // CAMPWIND_DLL void show(int x, int y);
	 // void hide();
     // CAMPWIND_DLL HWND getHWND() const;

	    virtual HWND getHWND() const;
        virtual bool isVisible() const;
        virtual bool isEnabled() const;
        virtual void show(bool visible);
        virtual void enable(bool enable);

};

#endif
