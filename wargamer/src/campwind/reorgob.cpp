/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Reorganise Order of Battle Dialog
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "reorgob.hpp"
#include "campdint.hpp"
#include "cwin_int.hpp"
#include "org_list.hpp"
#include "org_ordr.hpp"
#include "compos.hpp"
#include "wind.hpp"
#include "app.hpp"
#include "scenario.hpp"
#include "scrnbase.hpp"
#include "cbutton.hpp"
#include "wmisc.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "itemwind.hpp"
#include "dragwin.hpp"
#include "unitlist.hpp"
#include "fillwind.hpp"
#include "palwind.hpp"
#include "fonts.hpp"
#include "grtypes.hpp"
#include "palette.hpp"
#include "scn_res.h"
#include "imglib.hpp"
#include "armies.hpp"
#include "sp.hpp"
#include "scn_img.hpp"
#include "tbldimg.hpp"
#include "colours.hpp"
#include "control.hpp"
#include "armyutil.hpp"
#include "resstr.hpp"


#ifdef DEBUG
#include "logwin.hpp"
#endif

/*-----------------------------------------------------------------
 *  Local utilitues
 */

struct ROBControl {
  UWORD d_x;
  UWORD d_y;
  UWORD d_cx;
  UWORD d_cy;
};

class ROBUtil {
public:
  // const int dbX;
  // const int dbY;
  // const int baseX;
  // const int baseY;
  const int shadowWidth;
  const int shadowHeight;
  const int scrollCX;
  const int headerFontCY;
  const int headerY;
  const int infoX;
  const int infoY;
  const int infoCX;
  const int infoCY;

  ROBUtil::ROBUtil() :
    // dbX(ScreenBase::dbX()),
    // dbY(ScreenBase::dbY()),
    // baseX(ScreenBase::baseX()),
    // baseY(ScreenBase::baseY()),
    shadowWidth(ScreenBase::x(3)),      // * dbX) / baseX),
    shadowHeight(ScreenBase::x(3)),     // shadowWidth),
    scrollCX(ScreenBase::x(5)),        // * dbX) / baseX),
    headerFontCY(ScreenBase::y(11)),   // * dbY) / baseY),
    headerY(ScreenBase::y(3)),         // * dbY) / baseY),
    infoX(ScreenBase::x(3)),           // * dbX) / baseX),
    infoY(ScreenBase::y(60)),          // * dbY) / baseY),
    infoCX(ScreenBase::x(100)),        // * dbX) / baseX),
    infoCY(ScreenBase::y(80))          // * dbY) / baseY)
  {
  }

  static HWND createButton(HWND hParent, const char* text, int id, const ROBControl& ct);
  static HWND createOwnerDrawStatic(HWND hParent, int id, const ROBControl& ci);
  static void drawInfo(const CampaignData* campData, HDC hdc, ROBUtil& ru);
  static void drawAttributeGraph(HDC hdc, DrawDIB* dib, const char* text, int x, int y, Attribute value, ROBUtil& ru);
};

void ROBUtil::drawInfo(const CampaignData* campData, HDC hdc, ROBUtil& ru)
{
  LONG x = ScreenBase::x(2);    // * ru.dbX) / ru.baseX;
  LONG y = ScreenBase::y(2);    // * ru.dbY) / ru.baseY;

  COLORREF color;
  if(!scenario->getColour("MapInsert", color))
    color = RGB(255,255,255);

  COLORREF oldColor = SetTextColor(hdc, color);

  /*
   * draw header text
   */

  LogFont lf;
  lf.height(ru.headerFontCY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  // lf.italic(true);
  lf.italic(false);
  lf.underline(true);

  Font font;
  font.set(lf);

  SetBkMode(hdc, TRANSPARENT);
  HFONT oldFont = reinterpret_cast<HFONT>(SelectObject(hdc, font));

  /*
   * Put header text
   */

  // TODO: needs to go in string resource
  const char* text = InGameText::get(IDS_ReorganizeOB);
  wTextPrintf(hdc, (x + FI_Army_CX) + ScreenBase::x(3),
     ru.headerY, "%s", text);

  SetTextColor(hdc, oldColor);
  SelectObject(hdc, oldFont);
}

HWND ROBUtil::createButton(HWND hParent, const char* text, int id, const ROBControl& ct)
{
  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
            ScreenBase::x(ct.d_x),  // * ScreenBase::dbX()) / ScreenBase::baseX(),
            ScreenBase::y(ct.d_y),  // * ScreenBase::dbY()) / ScreenBase::baseY(),
            ScreenBase::x(ct.d_cx), // * ScreenBase::dbX()) / ScreenBase::baseX(),
            ScreenBase::y(ct.d_cy),  // * ScreenBase::dbY()) / ScreenBase::baseY(),
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  // cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground));
  cb.setBorderColours(scenario->getBorderColors());

  return hButton;
}

HWND ROBUtil::createOwnerDrawStatic(HWND hParent, int id, const ROBControl& ci)
{
  HWND h = CreateWindow("Static", NULL,
            SS_OWNERDRAW | SS_NOTIFY | WS_CHILD | WS_VISIBLE,
                ScreenBase::x(ci.d_x),
                ScreenBase::y(ci.d_y),
                ScreenBase::x(ci.d_cx),
                ScreenBase::y(ci.d_cy),
            // (ScreenBase::dbX() * ci.d_x) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_y) / ScreenBase::baseY(),
            // (ScreenBase::dbX() * ci.d_cx) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_cy) / ScreenBase::baseY(),
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(h != 0);
  return h;
}

void ROBUtil::drawAttributeGraph(HDC hdc, DrawDIB* dib, const char* text, int x, int y, Attribute value, ROBUtil& ru)
{
  ASSERT(hdc);
  ASSERT(dib);

  const int barOffsetX = x + ScreenBase::x(40); // * ru.dbX) / ru.baseX);
  const int barCX = ScreenBase::x(50);      // * ru.dbX) / ru.baseX);
  const int barCY = ScreenBase::y(4);       // * ru.dbY) / ru.baseY);

  wTextOut(hdc, x, y, text);

  SIZE s;
  GetTextExtentPoint32(hdc, text, lstrlen(text), &s);

  y += (s.cy - barCY) / 2;
  dib->drawBarChart(barOffsetX, y, value, Attribute_Range, barCX, barCY);
}

/*----------------------------------------------------------------
 * Our Drag List windows
 */


class ReorgOBItemListBox : public ItemWindow {
  const CampaignData* d_campData;

  OrgList& d_orgList;

  const UINT d_listCX;
  const UINT d_listCY;

public:
  enum Type {
    First,
    UnitList = First,
    SPList,
    AttachToList,

    HowMany,
    Undefined = HowMany
  };

private:
  Type d_type;

public:
  ReorgOBItemListBox(const ItemWindowData& data,
    const CampaignData* campData,
   const SIZE& s,
    Type type,
    OrgList& orgList) :
    ItemWindow(data, s),
    d_campData(campData),
    d_orgList(orgList),
    d_listCX(s.cx),
    d_listCY(s.cx),
    d_type(type)
  {
  }

  ~ReorgOBItemListBox() {}

//  CampaignData* campData() const { return d_campData; }

  void init(const void* data);
  void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
  void drawCombo(DrawDIBDC* dib, const void* data) {}

  virtual void drawDragItem(DrawDIBDC* dib, ListBox& lb, int id);

private:
  void initUnitList(ListBox& lb);
  void initSPList(const OrgObject& ob, ListBox& lb);
  void initAttachToList(ListBox& lb);

  void drawItem(DrawDIBDC* dib, ListBox& lb, int id, const PixelRect& r, bool isDragging);
  void drawUnitItem(DrawDIBDC* dib, const char* text, const OrgObject& ob, const PixelRect& rect, bool isDragging);
  void drawSPItem(DrawDIBDC* dib, const char* text, const OrgObject& ob, const PixelRect& rect, bool isDragging);
};

typedef ReorgOBItemListBox R_OBType;

inline LPARAM obToLPARAM(const OrgObject* ob)
{
   return reinterpret_cast<LPARAM>(const_cast<OrgObject*>(ob));
}

void ReorgOBItemListBox::init(const void* dataPtr)
{
  ASSERT(d_campData);

  ListBox lb(getHWND(), ItemListBox);
  lb.reset();


  switch(d_type)
  {
    case UnitList:
    {
      initUnitList(lb);
      break;
    }

    case SPList:
    {
      const OrgObject* ob = reinterpret_cast<const OrgObject*>(dataPtr);
      ASSERT(ob);
      initSPList(*ob, lb);
      break;
    }

    case AttachToList:
    {
      initAttachToList(lb);
      break;
    }
  }

  if(lb.getNItems() > 0)
  {
    lb.set(0);
    setCurrentValue();
  }

#if 0   // Always show full size
  /*
   * Set position of list box
   */

  int itemsShowing = minimum(data().d_maxItemsShowing, lb.getNItems());
  const LONG itemHeight = data().d_itemCY*itemsShowing;
  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, itemHeight, SWP_NOMOVE);
#endif
  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, d_listCY, SWP_NOMOVE);
}

void ReorgOBItemListBox::drawDragItem(DrawDIBDC* dib, ListBox& lb, int id)
{
    ColourIndex mask = *fillDib()->getBits();
    dib->fill(mask);
    dib->setMaskColour(mask);

    drawItem(dib, lb, id, PixelRect(0,0,dib->getWidth(),dib->getHeight()), true);
}

void ReorgOBItemListBox::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  ListBox lb(lpDrawItem->hwndItem);
  const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
  const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

  /*
   * Allocate dib if needed
   */

  allocateItemDib(itemCX, itemCY);

  ASSERT(itemDib());
  ASSERT(fillDib());

  itemDib()->rect(0, 0, itemCX, itemCY, fillDib());

  if( !(lpDrawItem->itemState & ODS_SELECTED) )
    itemDib()->darkenRectangle(0, 0, itemCX, itemCY, 20);

  drawItem(itemDib(), lb, lpDrawItem->itemID, lpDrawItem->rcItem, false);

    BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
         itemDib()->getDC(), 0, 0, SRCCOPY);
}

void ReorgOBItemListBox::drawItem(DrawDIBDC* dib, ListBox& lb, int id, const PixelRect& r, bool isDragging)
{
  if(lb.getNItems() > 0)
  {
    OrgCPI cpi(lb.getValue(id));
    ASSERT(d_orgList.isValid(cpi));

    const OrgObject& ob = d_orgList[cpi];

    /*
     * Get item text
     */

    char text[200];
    SendMessage(lb.getHWND(), LB_GETTEXT, id, (LPARAM)text);

    if(d_type == UnitList ||
       d_type == AttachToList)
    {
      drawUnitItem(dib, text, ob, r, isDragging);
    }
    else
    {
      ASSERT(d_type == SPList);
      drawSPItem(dib, text, ob, r, isDragging);
    }

  }
}

/*
 * draw unitlist and attacthtolist items
 */

void ReorgOBItemListBox::drawUnitItem(DrawDIBDC* dib, const char* text, const OrgObject& ob, const PixelRect& rect, bool isDragging)
{
    ASSERT(text);
    ASSERT(ob.what() == OrgObject::OOW_CommandPosition || ob.what() == OrgObject::OOW_Leader);

    /*
    * Get screen base and other useful constants
    */

    TEXTMETRIC tm;
    GetTextMetrics(dib->getDC(), &tm);
    const int xMargin = ScreenBase::x(2);
    const int xOffset = ScreenBase::y(8);

    /*
    * get image
    */

    static int imageRankOffset[Rank_HowMany] = {
                   FI_ArmyGroup,          // God
                   FI_ArmyGroup,          // President
                   FI_ArmyGroup,          // Executive
                   FI_Army,            // Army
                   FI_Corps,        // Corps
                   FI_Division      // Division
    };

    const ImageLibrary* images = scenario->getNationFlag(ob.nation(), True);

    UWORD imageID = (ob.what() == OrgObject::OOW_CommandPosition) ?
       imageRankOffset[ob.rank()] : FI_Pennant;

   /*
    * Set x offset
    */

    int flagX = xMargin;
    int nOffsets = 0;

    // first, count the nunber of offsets
    OrgCPI parentCPI = ob.parent();
    while(parentCPI != OrgCPI::None)
    {
       ASSERT(d_orgList.isValid(parentCPI));

       flagX += xOffset;
       nOffsets++;
       parentCPI = d_orgList[parentCPI].parent();
    }

    if(!isDragging)
    {
        // more useful constants
        const int xLineMargin = xMargin + (FI_Army_CX / 2);
        const int vw = ScreenBase::x(5);
        int vh = rect.height();   // itemCY;
        const ColourIndex ci = dib->getColour(PALETTERGB(0, 0, 0));

        // now draw appropriate lines
        parentCPI = ob.parent();
        OrgCPI thisCPI = parentCPI;
        while(nOffsets--)
        {
            ASSERT(parentCPI != OrgCPI::None);
            ASSERT(d_orgList.isValid(parentCPI));

            const int x = xLineMargin + (xOffset * nOffsets);
            const int y = 0;

            // if current objects parent
            if(parentCPI == ob.parent())
            {
               if(ob.sister() != OrgCPI::None)
               {
                   OrgCPI sisCPI = ob.sister();
                   ASSERT(d_orgList.isValid(sisCPI));

                   Boolean endOfCP = True;

                   while(sisCPI != OrgCPI::None)
                   {
                      if(d_orgList[sisCPI].what() == OrgObject::OOW_CommandPosition)
                      {
                         endOfCP = False;
                         break;
                      }

                      sisCPI = d_orgList[sisCPI].sister();
                   }

                   if(endOfCP)
                      vh = rect.height() / 2;
               }
               else
                   vh = rect.height() / 2;

               dib->hLine(x, rect.height() / 2, vw, ci);
               dib->vLine(x, y, vh, ci);
            }
            else
            {
               ASSERT(thisCPI != OrgCPI::None);
               ASSERT(d_orgList.isValid(thisCPI));

               OrgCPI sisCPI = d_orgList[thisCPI].sister();

               Boolean endOfCP = True;
               if(sisCPI != OrgCPI::None)
               {
                   ASSERT(d_orgList.isValid(sisCPI));
                   while(sisCPI != OrgCPI::None)
                   {
                      if(d_orgList[sisCPI].what() == OrgObject::OOW_CommandPosition)
                      {
                         endOfCP = False;
                         break;
                      }

                      sisCPI = d_orgList[sisCPI].sister();
                   }

                   if(!endOfCP)
                      dib->vLine(x, y, rect.height(), ci);
               }
            }

            thisCPI = parentCPI;
            parentCPI = d_orgList[parentCPI].parent();
        }
    }

  // get image size
  int w;
  int h;
  images->getImageSize(imageID, w, h);

  int y = (rect.height() - h) / 2;
  images->blit(dib, imageID, flagX, y, True);

  y = (rect.height() - tm.tmHeight) / 2;

  dib->setBkMode(TRANSPARENT);
  dib->setTextAlign(TA_LEFT | TA_TOP);
  dib->setTextColor(PaletteColours::Black);
  wTextOut(dib->getDC(), flagX + FI_Army_CX + xMargin, y, text);
}

/*
 * draw splist items
 */

void ReorgOBItemListBox::drawSPItem(DrawDIBDC* dib, const char* text, const OrgObject& ob, const PixelRect& rect, bool isDragging)
{
    ASSERT(text);
    ASSERT(ob.what() == OrgObject::OOW_StrengthPoint);

    /*
    * Get screen base and other useful constants
    */

    TEXTMETRIC tm;
    GetTextMetrics(dib->getDC(), &tm);
    const int xMargin = ScreenBase::x(2);
    const int yMargin = 1;
    const int xText = ScreenBase::x(12);

    /*
     * get image
     */

        TownBuildImages images;

    const UnitTypeItem& uti = d_campData->getUnitType(ob.sp()->getUnitType());

    TownBuildImages::Value imageID = images.getUnitTypeIndex(uti.getBasicType());

    // get image size
    int w;
    int h;
    images.getImageSize(imageID, w, h);

    // get stretch size
    if(h > (rect.height() - (2 * yMargin)))
    {
       int oldH = h;
       h = (rect.height() - (2 * yMargin));

       ASSERT(oldH);
       w = (w * h) / oldH;
    }

    int y = (rect.height() - h) / 2;
    images.stretchBlit(dib, imageID, xMargin, y, w, h);

    y = (rect.height() - tm.tmHeight) / 2;
    dib->setBkMode(TRANSPARENT);
    dib->setTextAlign(TA_LEFT | TA_TOP);
    dib->setTextColor(PaletteColours::Black);
    wTextOut(dib->getDC(), xText, y, text);
}

void ReorgOBItemListBox::initUnitList(ListBox& lb)
{
//  ASSERT(d_orgList.d_unitInfoWindow() > 1);

  // start at index 1 of orgList. Index 0 is this sides 'Executive'
  OrgCPI orgCPI(0);
  OrgObject& obj = d_orgList[orgCPI];
  orgCPI = obj.child();

  OrgListIter iter(d_orgList, orgCPI);
  while(iter.sister())
  {
    OrgListIter cIter(d_orgList, iter.currentCPI());

    while(cIter.next())
    {
      const OrgObject& ob = cIter.currentObject();
      if(ob.what() == OrgObject::OOW_CommandPosition || ob.what() == OrgObject::OOW_Leader)
      {
        const char* text = d_orgList.getName(cIter.currentCPI());
        ASSERT(text);

        lb.add(text, cIter.currentCPI());
      }
    }
  }
}

void ReorgOBItemListBox::initSPList(const OrgObject& ob, ListBox& lb)
{
  ASSERT(ob.what() == OrgObject::OOW_CommandPosition);

  /*
   * Find sp's
   */

  OrgCPI iChild = ob.child();
  ASSERT(iChild != OrgCPI::None);

  OrgListIter iter(d_orgList, iChild);
  while(iter.sister())
  {
    const OrgObject& cOb = iter.currentObject();
    if(cOb.what() == OrgObject::OOW_StrengthPoint)
    {
      const UnitTypeItem& uti = d_campData->getUnitType(cOb.sp()->getUnitType());
      lb.add(uti.getName(), iter.currentCPI());
    }
  }
}

void ReorgOBItemListBox::initAttachToList(ListBox& lb)
{
//  ASSERT(d_orgList.entries() > 0);

  // add 'Executive
  {
    OrgCPI orgCPI(0);
    ASSERT(d_orgList[orgCPI].what() == OrgObject::OOW_CommandPosition);
    lb.add(d_orgList.getName(orgCPI), orgCPI);
  }

  // start at index 1 of orgList. Index 0 is this sides 'Executive'
//  OrgCPI orgCPI(1);
  OrgCPI orgCPI(0);
  OrgObject& obj = d_orgList[orgCPI];
  orgCPI = obj.child();

  OrgListIter iter(d_orgList, orgCPI);
  while(iter.sister())
  {
    OrgListIter cIter(d_orgList, iter.currentCPI());
    while(cIter.next())
    {
      OrgObject& obj = cIter.currentObject();
      if(obj.what() == OrgObject::OOW_CommandPosition)
      {
        lb.add(d_orgList.getName(cIter.currentCPI()), cIter.currentCPI());
      }
    }
  }
}


/*--------------------------------------------------------
 * Implementation class
 */

class ReorgOBWin : public DragWindow
{
    typedef DragWindow Super;

    HWND                      d_hParent;     // Handle of Parent
    CampaignUserInterface*    d_owner;
    CampaignWindowsInterface* d_campWind;
    const CampaignData*             d_campData;
    OrgList                   d_orgList;        // List of units under control of dialog
    OrgCPI                    d_currentCPI;

    ReorgOBItemListBox* d_listBoxes[R_OBType::HowMany];   // listboxes
    // HFONT               d_dragFont;                       // font used for drag item
     Font                d_itemFont;
    FillWindow          d_fillWind;                       // background dib

    // UINT d_dragMsgID;                    // msg id for drag message

    // DrawDIB* d_dragItemDib;
    DrawDIB* d_infoDib;
    HDC d_dibDC;

     //--- Moved to DragWindow
    // int d_itemID;

    PixelPoint d_listP[R_OBType::HowMany];
     //--- Moved to DragWindow
    // POINT d_lastP;

    Boolean d_orderGiven;
    ROBUtil d_ru;                             // useful constants

    enum ID {
       Button_First = R_OBType::HowMany,
       Cancel = Button_First,
       // Apply,
       Send,
       Button_Last = Send,
       Info,
       ID_Last,
       Button_HowMany = (Button_Last - Button_First) + 1
    };

    static ROBControl s_listBoxCords[ID_Last];

  public:
    ReorgOBWin(CampaignUserInterface* owner, HWND hParent,
      CampaignWindowsInterface* campWind, const CampaignData* campData);

    ~ReorgOBWin()
    {
        selfDestruct();

        //--- Moved to DragWindow
      // if(d_dragItemDib)
      //  delete d_dragItemDib;

      if(d_infoDib)
        delete d_infoDib;

      if(d_dibDC)
        DeleteDC(d_dibDC);
    }

    void init(const StackedUnitList& list);
    void run(const PixelPoint& p);

    const OrgObject* currentObj();
    const OrgObject* currentCP();

  private:

        /*
         * Virtual functions to implement DragWindow
         */

        virtual void dropObject(const DragWindowObject& from, const DragWindowObject& dest);
        virtual bool overObject(const PixelPoint& p, const DragWindowObject& from, DragWindowObject* object);
        virtual bool canDrag(const DragWindowObject& ob) { return true; }


       /*
       * Window Messages
       */

       LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
       BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
       void onPaint(HWND hwnd);
       BOOL onEraseBk(HWND hwnd, HDC hdc);
       UINT onNCHitTest(HWND hwnd, int x, int y);
       void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
       // LRESULT onDragOp(int id, DRAGLISTINFO* dli);
       void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* di);
       void onDestroy(HWND hwnd)
       {
          for(int i = 0; i < R_OBType::HowMany; i++)
          {
          if(d_listBoxes[i])
          {
             delete d_listBoxes[i];
             d_listBoxes[i] = 0;
          }
          }
       }

       /*
       * useful functions
       */

       // void doDragging(int id, DRAGLISTINFO* dli);
       // BOOL doBeginDrag(int id, DRAGLISTINFO* dli);
       // void doDropped(int id, DRAGLISTINFO* dli);
       void addOrganization(ConstICommandPosition cpi, OrgCPI iParent);
       void fillLeaderList();
       void fillUnitList();
       void fillSPList();
       void fillAttachToList();
       void onSend();
       void onCancel();
        void onApply();
       void drawInfo();
       void enableControls();
       void fillLists();

        OrgCPI getObject(const DragWindowObject& ob) const;

};

ROBControl ReorgOBWin::s_listBoxCords[ID_Last] = {
  {   3,   16, 108,  6*9 },    // unitlist box
  {   3,   79, 108,  4*9 },    // SP Window
  { 120,   16, 108,  11*9 },    // attach to
  { 181,  126,  48, 13 },   // Cancel
  { 181,  143,  48, 13 },   // Send
  {   3,  126, 176, 30 }    // Info
//  { 194,  120,  35, 10 },   // Cancel
  // { 194,  133,  35, 10 },   // Apply
//  { 194,  146,  35, 10 },   // Send
//  {   8,  120, 184, 35 }    // Info
};

ReorgOBWin::ReorgOBWin(CampaignUserInterface* owner, HWND hParent,
      CampaignWindowsInterface* campWind, const CampaignData* campData) :
  d_hParent(hParent),
  d_owner(owner),
  d_campWind(campWind),
  d_campData(campData),
  d_orgList(campData),
  // d_dragMsgID(RegisterWindowMessage(DRAGLISTMSGSTRING)),
  // d_dragItemDib(0),
  // d_dragFont(0),
  d_infoDib(0),
  d_dibDC(CreateCompatibleDC(NULL)),
  // d_itemID(-1),
  d_orderGiven(False)
{
  ASSERT(d_hParent);
  ASSERT(d_campData);
  // ASSERT(d_dragMsgID != 0);
  ASSERT(d_dibDC);

  // d_lastP.x = -1;
  // d_lastP.y = -1;

  const int cx = ScreenBase::x(240);
  const int cy = ScreenBase::y(162);    // * d_ru.dbY) / d_ru.baseY;

  HWND hWnd = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_POPUP,
      0, 0, cx, cy,
      d_hParent, NULL // , APP::instance()
  );
  ASSERT(hWnd != NULL);

//  d_stWind.parent(d_hParent);
}

LRESULT ReorgOBWin::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

#if 0   // Done in DragWindow
  // check for drag message.
  // note: dragMsgID is not constant so it can't be part of the switch statement
  if(msg == d_dragMsgID)
  {
    return onDragOp(static_cast<int>(wParam), reinterpret_cast<DRAGLISTINFO*>(lParam));
  }
#endif

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
     HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
    default:
      // return defProc(hWnd, msg, wParam, lParam);
        return Super::procMessage(hWnd, msg, wParam, lParam);
  }
}

BOOL ReorgOBWin::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
  const int cx = lpCreateStruct->cx;
  const int cy = lpCreateStruct->cy;

  /*
   * init dib's
   */

//  d_stWind.allocateDib(cx, cy);
  /*
   * init background dib
   */

  d_fillWind.setShadowWidth(d_ru.shadowWidth);
  d_fillWind.allocateDib(cx, cy);
  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());

  /*
   * Set item font
   */

  LogFont lf;
  lf.height(ScreenBase::y(7));      // (7*d_ru.dbY)/d_ru.baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(false);
  lf.underline(false);

  d_itemFont.set(lf);

  setDragFont(lf);

#if 0       // Moved to DragWindow
  d_dragFont = font;
#endif

  ItemWindowData ld;
  ld.d_hParent = hwnd;
  ld.d_style = WS_CHILD | WS_VISIBLE;
  ld.d_hFont = d_itemFont;        // d_dragFont;
  ld.d_scrollCX = d_ru.scrollCX;
  // ld.d_itemCY = static_cast<UWORD>((9 * d_ru.dbY) / d_ru.baseY);
  ld.d_itemCY = ScreenBase::y(9);
  ld.d_flags =  ItemWindowData::HasScroll |
                ItemWindowData::NoAutoHide |
                ItemWindowData::InclusiveSize |
                ItemWindowData::NoBorder |
                ItemWindowData::DragList |
                ItemWindowData::OwnerDraw;
  ld.d_windowFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ld.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ld.d_scrollBtns = ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons);
  ld.d_scrollThumbDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground);
  ld.d_scrollFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground);
  ld.d_bColors = scenario->getBorderColors();

  /*
   * create list boxes
   */

  for(R_OBType::Type t  = R_OBType::First; t < R_OBType::HowMany; INCREMENT(t))
  {
    ld.d_id = t;
    // ld.d_maxItemsShowing = s_maxItems[t];

    SIZE s;
    s.cx = ScreenBase::x(s_listBoxCords[t].d_cx);
    s.cy = ScreenBase::y(s_listBoxCords[t].d_cy);
    ld.d_maxItemsShowing = static_cast<UWORD>(s.cy / ld.d_itemCY);
    s.cy = ld.d_maxItemsShowing * ld.d_itemCY;
    s.cy += 2 * ld.borderHeight();

    d_listBoxes[t] = new ReorgOBItemListBox(ld, d_campData, s, t, d_orgList);
    ASSERT(d_listBoxes[t]);

    // Set height to size of items adjusted for screen size + 2 for a border.

    int x = ScreenBase::x(s_listBoxCords[t].d_x);
    int y = ScreenBase::y(s_listBoxCords[t].d_y);

    d_listP[t].set(x, y);
  }

  /*
   * create buttons
   */

  // TODO: move to string resource
  static const InGameText::ID s_buttonTxt[Button_HowMany] = {
     IDS_Cancel,
     // "Apply",
     IDS_UOP_Send
  };

  int i = 0;
  for(int id = Button_First; id < Button_Last + 1; id++, i++)
  {
    ROBUtil::createButton(hwnd, InGameText::get(s_buttonTxt[i]), id, s_listBoxCords[id]);
  }

  /*
   * info section
   */

  ROBUtil::createOwnerDrawStatic(hwnd, Info, s_listBoxCords[Info]);

  int infoCX = ScreenBase::x(s_listBoxCords[Info].d_cx);    // * d_ru.dbX) / d_ru.baseX;
  int infoCY = ScreenBase::y(s_listBoxCords[Info].d_cy);    // * d_ru.dbY) / d_ru.baseY;
  d_infoDib = new DrawDIB(infoCX, infoCY);
  ASSERT(d_infoDib);

  enableControls();
  return TRUE;
}

UINT ReorgOBWin::onNCHitTest(HWND hwnd, int x, int y)
{
  return HTCAPTION;
}

BOOL ReorgOBWin::onEraseBk(HWND hwnd, HDC hdc)
{
  d_fillWind.paint(hdc);
  return True;
}

void ReorgOBWin::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
  RealizePalette(hdc);

//  d_stWind.paint(hdc);
  ROBUtil::drawInfo(d_campData, hdc, d_ru);

  SelectPalette(hdc, oldPal, False);

  EndPaint(hwnd, &ps);
}

void ReorgOBWin::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* di)
{
  HPALETTE oldPal = SelectPalette(di->hDC, Palette::get(), FALSE);
  RealizePalette(di->hDC);

  if(di->CtlID == Info)
  {
    ASSERT(d_infoDib);
    ASSERT(d_dibDC);

    SelectObject(d_dibDC, d_infoDib->getHandle());

    BitBlt(di->hDC, 0, 0, d_infoDib->getWidth(), d_infoDib->getHeight(),
      d_dibDC, 0, 0, SRCCOPY);
  }

  SelectPalette(di->hDC, oldPal, False);
}

void ReorgOBWin::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case Cancel:
    {
      onCancel();
      break;
    }

    case Send:
    {
      onSend();
      break;
    }
#if 0
    case Apply:
    {
      onApply();
      enableControls();
      break;
    }
#endif

    case R_OBType::UnitList:
    {
      const OrgObject* obj = currentObj();
      ASSERT(obj);

      OrgCPI cpi(d_listBoxes[id]->currentValue());
      if(obj->what() == OrgObject::OOW_CommandPosition)
      {
        d_currentCPI = cpi;
      }
      else
      {
        d_currentCPI = d_orgList[cpi].parent();
      }

      /*
       * reset splist box
       */

      d_listBoxes[R_OBType::SPList]->init(currentCP());
      d_listBoxes[R_OBType::SPList]->show(d_listP[R_OBType::SPList]);
      drawInfo();

      break;
    }

    case R_OBType::AttachToList:
      break;
  }
}

void ReorgOBWin::run(const PixelPoint& p)
{
  ASSERT(d_hParent);

  /*
   * initialize lists
   */

  for(int t = 0; t < R_OBType::HowMany; t++)
  {
    d_listBoxes[t]->show(d_listP[t]);
  }

  /*
   * Convert to screen and show
   */

  const int offset = (ScreenBase::dbY() * 10) / ScreenBase::baseY();

  /*
   * make sure entire window is within mainwindows rect
   */

  RECT r;
  GetClientRect(getHWND(), &r);
  const int winCX = r.right - r.left;
  const int winCY = r.bottom - r.top;

  GetWindowRect(APP::getMainHWND(), &r);

  int x = p.getX();
  int y = p.getY();

  // check right side
  if((x + winCX) > r.right)
  {
    x -= (winCX + offset);
  }

  // check bottom
  if((y + winCY) > r.bottom)
  {
    y -= (winCY + offset);
  }

  SetWindowPos(getHWND(), HWND_TOPMOST, x, y, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);
}

void ReorgOBWin::init(const StackedUnitList& ul)
{
  ASSERT(ul.unitCount() > 0);

  if(ul.unitCount() > 0)
  {
    d_orgList.initUnits(ul.getUnit(0));
    for(UnitListID i = 0; i < ul.unitCount(); i++)
    {
      ConstICommandPosition top = ul.getUnit(i);

      if(CampaignArmy_Util::isUnitOrderable(top))
         addOrganization(top, OrgCPI::None);
    }

    /*
     * set top executive
     */

    /*
     * fill lists
     */

    d_currentCPI = 1;
    fillLists();
  }
}

/*
 * Recursively Add units
 */

void ReorgOBWin::addOrganization(ConstICommandPosition cpi, OrgCPI iParent)
{
  OrgCPI iOrg = d_orgList.addCP(cpi, iParent);

  const CommandPosition* cp = d_campData->getCommand(cpi);

  /*
   * Add Leader
   */

  ILeader iLeader = cp->getLeader();
  if(iLeader != NoLeader)
  {
    OrgCPI iOrgLeader = d_orgList.addLeader(iLeader, iOrg);
  }

  /*
   * Add children
   */

  ICommandPosition cpiChild = cp->getChild();
  while(cpiChild != NoCommandPosition)
  {
    addOrganization(cpiChild, iOrg);

    const CommandPosition* cpChild = d_campData->getCommand(cpiChild);
    cpiChild = cpChild->getSister();
  }

  /*
   * Add Strength Points
   */

  ISP isp = cp->getSPEntry();
  while(isp != NoStrengthPoint)
  {
    OrgCPI orgSP = d_orgList.addSP(isp, iOrg);

    StrengthPointItem* sp = d_campData->getArmies().getStrengthPoint(isp);
    isp = sp->getNext();
  }
}

#if 0   // Done in DragWindow
/*
 * process drag message
 */

LRESULT ReorgOBWin::onDragOp(int id, DRAGLISTINFO* dli)
{
  switch(dli->uNotification)
  {
    case DL_BEGINDRAG:
    {
      return doBeginDrag(id, dli);
    }

    case DL_DRAGGING:
    {
      doDragging(id, dli);
      return DL_COPYCURSOR;
    }

    case DL_DROPPED:
    {
      doDropped(id, dli);
#ifdef DEBUG
      logWindow->printf("--- DL_DROPPED x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
#endif
      return True;
    }
  }

  return 1;
}


BOOL ReorgOBWin::doBeginDrag(int id, DRAGLISTINFO* dli)
{
#ifdef DEBUG
  logWindow->printf("--- DL_BEGINDRAG x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
#endif

  d_itemID = LBItemFromPt(dli->hWnd, dli->ptCursor, FALSE);

  ASSERT(d_itemID >= 0);
  if(d_itemID < 0)
    return FALSE;

#ifdef DEBUG
  logWindow->printf("---------- itemID = %d", d_itemID);
#endif

  return TRUE;
}


void ReorgOBWin::doDragging(int id, DRAGLISTINFO* dli)
{
  ASSERT(d_itemID >= 0);
  if(d_itemID < 0)
    return;

  /*
   * Convert to client cordinates
   */

  POINT p = dli->ptCursor;
  ScreenToClient(getHWND(), &p);

  const int oldOffsetX = (d_dragItemDib) ? d_dragItemDib->getWidth() : 0;
  const int oldOffsetY = (d_dragItemDib) ? d_dragItemDib->getHeight() : 0;

  RECT r;
  GetWindowRect(getHWND(), &r);
  if(!PtInRect(&r, dli->ptCursor) ||
     ((p.x - oldOffsetX) == d_lastP.x && (p.y - oldOffsetY) == d_lastP.y))
  {
    return;
  }

  HDC hdc = GetDC(getHWND());
  ASSERT(hdc);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
  RealizePalette(hdc);

  /*
   * Put back last background
   */

  if(d_lastP.x >= 0 && d_lastP.y >= 0)
  {
    ASSERT(d_dragItemDib);
    ASSERT(d_dibDC);

    SelectObject(d_dibDC, d_dragItemDib->getHandle());
    BitBlt(hdc, d_lastP.x, d_lastP.y, d_dragItemDib->getWidth(), d_dragItemDib->getWidth(),
       d_dibDC, 0, 0, SRCCOPY);
  }

  /*
   * See if we are over the attachto listbox
   */

  if(d_listBoxes[R_OBType::AttachToList]->overItem(dli->ptCursor))
  {
#ifdef DEBUG
    logWindow->printf("---------- Over AttachTo List (index = %d)",
        d_listBoxes[R_OBType::AttachToList]->currentIndex());
#endif
  }


  /*
   * Set font
   */

  ASSERT(d_dragFont);
  HFONT oldFont = (HFONT)SelectObject(hdc, d_dragFont);
  ASSERT(oldFont);

  SetBkMode(hdc, TRANSPARENT);

  ListBox lb(dli->hWnd);

  char text[200];
  SendMessage(lb.getHWND(), LB_GETTEXT, d_itemID, reinterpret_cast<LPARAM>(text));

  SIZE s;
  GetTextExtentPoint32(hdc, text, lstrlen(text), &s);

  if(!d_dragItemDib ||
     s.cx > d_dragItemDib->getWidth() ||
     s.cy > d_dragItemDib->getHeight())
  {
    SIZE newS;

    if(!d_dragItemDib)
      newS = s;
    else
    {
      newS.cx = d_dragItemDib->getWidth();
      newS.cy = d_dragItemDib->getHeight();

      if(s.cx > d_dragItemDib->getWidth())
        newS.cx = s.cx;

      if(s.cy > d_dragItemDib->getHeight())
        newS.cy = s.cy;
    }

    if(d_dragItemDib)
    {
      delete d_dragItemDib;
      d_dragItemDib = 0;
    }

    ASSERT(newS.cx > 0);
    ASSERT(newS.cy > 0);
    d_dragItemDib = new DrawDIB(newS.cx, newS.cy);
  }

  ASSERT(d_dragItemDib);
  SelectObject(d_dibDC, d_dragItemDib->getHandle());

  p.x -= d_dragItemDib->getWidth();
  p.y -= d_dragItemDib->getHeight();

  BitBlt(d_dibDC, 0, 0, d_dragItemDib->getWidth(), d_dragItemDib->getHeight(),
     hdc, p.x, p.y, SRCCOPY);

  wTextOut(hdc, p.x, p.y, text);

  d_lastP = p;

  SelectObject(hdc, oldFont);
  SelectPalette(hdc, oldPal, FALSE);
  ReleaseDC(getHWND(), hdc);

#ifdef DEBUG
  logWindow->printf("--- DL_DRAGGING x = %ld, y = %ld", dli->ptCursor.x, dli->ptCursor.y);
#endif
}

void ReorgOBWin::doDropped(int id, DRAGLISTINFO* dli)
{
  /*
   * Put back last background
   */

  if(d_lastP.x >= 0 && d_lastP.y >= 0)
  {
    HDC hdc = GetDC(getHWND());
    ASSERT(hdc);

    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
    RealizePalette(hdc);

    ASSERT(d_dragItemDib);

    SelectObject(d_dibDC, d_dragItemDib->getHandle());
    BitBlt(hdc, d_lastP.x, d_lastP.y, d_dragItemDib->getWidth(), d_dragItemDib->getWidth(),
       d_dibDC, 0, 0, SRCCOPY);

    SelectPalette(hdc, oldPal, FALSE);
    ReleaseDC(getHWND(), hdc);
  }

  /*
   * See if we are over a listbox
   */

  if(d_listBoxes[R_OBType::AttachToList]->overItem(dli->ptCursor))
  {
#ifdef DEBUG
    logWindow->printf("--- Dropped over item# = %d",
        d_listBoxes[R_OBType::AttachToList]->currentIndex());
#endif

    /*
     * Reorganize
     */

    ListBox lb(dli->hWnd);
    OrgCPI fromCPI(lb.getValue());
    ASSERT(d_orgList.isValid(fromCPI));

    OrgCPI toCPI = d_listBoxes[R_OBType::AttachToList]->currentValue();
    ASSERT(d_orgList.isValid(toCPI));

    if(d_orgList.canTransfer(fromCPI, toCPI))
    {
      d_orgList.transfer(fromCPI, toCPI);
      d_orderGiven = True;
      fillLists();
    }
    else if(d_orgList.canMerge(fromCPI, toCPI))
    {
      d_orgList.merge(fromCPI, toCPI);
      d_orderGiven = True;
      fillLists();
    }
  }

  d_lastP.x = -1;
  d_lastP.y = -1;
  d_itemID = -1;

  enableControls();
}
#endif

/*
 * Implement DragWindow
 */

void ReorgOBWin::dropObject(const DragWindowObject& dragObject, const DragWindowObject& dropObject)
{
    OrgCPI fromCPI = getObject(dragObject);
    OrgCPI toCPI = getObject(dropObject);

   if(d_orgList.canTransfer(fromCPI, toCPI))
   {
      d_orgList.transfer(fromCPI, toCPI);
      d_orderGiven = True;
      fillLists();
   }
   else if(d_orgList.canMerge(fromCPI, toCPI))
   {
      d_orgList.merge(fromCPI, toCPI);
      d_orderGiven = True;
      fillLists();
   }

    enableControls();
}

OrgCPI ReorgOBWin::getObject(const DragWindowObject& ob) const
{
    ListBox lb(ob.hwnd());
    OrgCPI fromCP(lb.getValue(ob.id()));
   ASSERT(d_orgList.isValid(fromCP));
    return fromCP;
}

bool ReorgOBWin::overObject(const PixelPoint& p, const DragWindowObject& dragObject, DragWindowObject* object)
{
    DragWindowObject dropObject;

    if(d_listBoxes[R_OBType::AttachToList]->overItem(p, &dropObject) ||
       d_listBoxes[R_OBType::UnitList]->overItem(p, &dropObject) )
    {
        OrgCPI fromCP = getObject(dragObject);
        OrgCPI toCP = getObject(dropObject);

        if(d_orgList.canTransfer(fromCP, toCP) ||
           d_orgList.canMerge(fromCP, toCP) )
        {
            ListBox lb(dropObject.hwnd());
            if (lb.getIndex() != dropObject.id())
            {
               startDraw();
               lb.set(dropObject.id());
               endDraw();
            }
            *object = dropObject;
            return true;
        }
    }

    return false;
}


void ReorgOBWin::fillLists()
{
  // init unit list
  d_listBoxes[R_OBType::UnitList]->init(NULL);

  // set current unit
  if(d_currentCPI == OrgCPI::None)  // || !d_orgList.isValid(d_currentCPI))
    d_currentCPI = 1;

  d_listBoxes[R_OBType::UnitList]->setCurrentValue(d_currentCPI);

  // init attachto list
  d_listBoxes[R_OBType::AttachToList]->init(NULL);

  // init sp list
  ASSERT(currentCP());
  d_listBoxes[R_OBType::SPList]->init(currentCP());

  // redraw lists
  for(int i = 0; i < R_OBType::HowMany; i++)
  {
    d_listBoxes[i]->show(d_listP[i]);
  }

  drawInfo();
}

/*
 * Send order to leader
 */

void ReorgOBWin::onSend()
{
  onApply();
  onCancel();
}

void ReorgOBWin::onApply()
{
  // RWLock lock;
  // lock.startWrite();

  OrgCPI cpi(1);
  ASSERT(d_orgList[cpi].what() == OrgObject::OOW_CommandPosition);
  ASSERT(!d_orgList[cpi].isFictional());
  OrgOrders::sendOrders(d_orgList, d_orgList[cpi].cp());

  d_orderGiven = False;

  // lock.endWrite();
}

void ReorgOBWin::onCancel()
{
    show(false);

    // Remove CP/SP References

    d_currentCPI = OrgCPI::None;
    d_orgList.reset();


    // ShowWindow(getHWND(), SW_HIDE);

  // d_campWind->obWindowUpdated();
}

void ReorgOBWin::drawInfo()
{
  if(currentCP())
  {
    const OrgObject* ob = currentCP();
    ASSERT(ob->what() == OrgObject::OOW_CommandPosition);

    const COLORREF ourColor = scenario->getSideColour(scenario->nationToSide(ob->nation()));
    const COLORREF black = PALETTERGB(0, 0, 0);

    /*
     * fill info background
     */

    ASSERT(d_infoDib);
    d_infoDib->fill(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground));

    // draw border
    ColourIndex ci = d_infoDib->getColour(black);
    d_infoDib->frame(0, 0, d_infoDib->getWidth(), d_infoDib->getHeight(), ci);

    /*
     * Now draw info
     */

    SelectObject(d_dibDC, d_infoDib->getHandle());

    // set font
    LogFont lf;
    lf.height(ScreenBase::y(8));    // * d_ru.dbY) / d_ru.baseY);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    // lf.italic(true);
    lf.italic(false);
    lf.underline(true);

    Font font;
    font.set(lf);

    HFONT oldFont = reinterpret_cast<HFONT>(SelectObject(d_dibDC, font));
    SetBkMode(d_dibDC, TRANSPARENT);

    const int headerX = ScreenBase::x(1);  // d_ru.dbX) / d_ru.baseX;
    const int headerY = ScreenBase::y(1);  // d_ru.dbY) / d_ru.baseY;
    const int strengthY = headerY + ScreenBase::y(10); // * d_ru.dbY) / d_ru.baseY);
    const int gunsX = ScreenBase::x(40);   // * d_ru.dbX) / d_ru.baseX;
    const int moraleY = ScreenBase::y(20); // * d_ru.dbY) / d_ru.baseY);

    COLORREF oldColor = SetTextColor(d_dibDC, ourColor);

    /*
     * Put nation flag
     */

    const ImageLibrary* flagIcons = scenario->getNationFlag(ob->nation(), True);
    flagIcons->blit(d_infoDib, FI_Army, headerX, headerY, True);

    /*
     * Put Unit name
     */

    const char* leaderName = 0;

    OrgListIter iter(d_orgList, ob->child());
    while(iter.sister())
    {
      const OrgObject& cObj = iter.currentObject();
      if(cObj.what() == OrgObject::OOW_Leader)
        leaderName = d_orgList.getName(iter.currentCPI());
    }

    ASSERT(leaderName);
    if(!leaderName)
      leaderName = InGameText::get(IDS_NoLeader);

    ASSERT(d_orgList.isValid(d_currentCPI));
    wTextPrintf(d_dibDC, headerX + FI_Army_CX + ScreenBase::x(5), headerY,
         "%s (%s)", d_orgList.getName(d_currentCPI), leaderName);

    /*
     * Put strength
     */

    lf.height(ScreenBase::y(7));   // * d_ru.dbY) / d_ru.baseY);
    lf.italic(false);
    lf.underline(false);

    font.set(lf);
    SelectObject(d_dibDC, font);
    SetTextColor(d_dibDC, black);

    // TODO: get from resource
    const char* sText = InGameText::get(IDS_UI_STR);
//  const char* gText = InGameText::get(IDS_UI_GUNS);
    const char* gText = InGameText::get(IDS_Guns);

    /*
     * Calculate numerical strength
     */

    int strength = 0;
    int guns = 0;

    iter.rewind();
    while(iter.sister())
    {
      const OrgObject& cObj = iter.currentObject();
      if(cObj.what() == OrgObject::OOW_StrengthPoint)
      {
        const UnitTypeItem& uti = d_campData->getUnitType(cObj.sp()->getUnitType());

        if(uti.getBasicType() == BasicUnitType::Infantry)
          strength += UnitTypeConst::InfantryPerSP;
        else if(uti.getBasicType() == BasicUnitType::Cavalry)
          strength += UnitTypeConst::CavalryPerSP;
        else if(uti.getBasicType() == BasicUnitType::Artillery)
        {
          strength += UnitTypeConst::ArtilleryPerSP;
          guns += UnitTypeConst::GunsPerSP;
        }
        else
          strength += UnitTypeConst::SpecialPerSP;
      }
    }

    wTextPrintf(d_dibDC, headerX, strengthY, "%s %d", sText, strength);
    wTextPrintf(d_dibDC, gunsX, strengthY, "%s: %d", gText, guns);

    /*
     * Put Base Morale
     */

    //TODO: get this from string resource
    const char* text = InGameText::get(IDS_MORALE);
    Attribute morale = (!ob->isFictional()) ? ob->cp()->getMorale() : 0;
    ROBUtil::drawAttributeGraph(d_dibDC, d_infoDib, text, headerX, moraleY,
        morale, d_ru);

    SetTextColor(d_dibDC, oldColor);
    SelectObject(d_dibDC, oldFont);

    HWND hInfo = GetDlgItem(getHWND(), Info);
    ASSERT(hInfo);
    InvalidateRect(hInfo, NULL, FALSE);

  }
}

void ReorgOBWin::enableControls()
{
#if 0
  {
    CustomButton b(getHWND(), Apply);
    b.enable(d_orderGiven);
  }
#endif

  {
    CustomButton b(getHWND(), Send);
    b.enable(d_orderGiven);
  }
}

const OrgObject* ReorgOBWin::currentObj()
{
  if(d_listBoxes[R_OBType::UnitList])
  {
    OrgCPI cpi(d_listBoxes[R_OBType::UnitList]->currentValue());
    return &d_orgList[cpi];
  }
  else
    return 0;
}

const OrgObject* ReorgOBWin::currentCP()
{
  if(d_listBoxes[R_OBType::UnitList])
  {
    const OrgObject& ob = d_orgList[d_currentCPI];
    return (ob.what() == OrgObject::OOW_CommandPosition) ? &ob : &d_orgList[ob.parent()];
  }
  else
    return 0;
}

/*--------------------------------------------------------
 *  Client access
 */

ReorgOB_Int::ReorgOB_Int(CampaignUserInterface* owner, HWND hParent,
     CampaignWindowsInterface* campWind, const CampaignData* campData) :
  d_obWind(new ReorgOBWin(owner, hParent, campWind, campData))
{
  ASSERT(d_obWind);
}

ReorgOB_Int::~ReorgOB_Int()
{
  // destroy();
  delete d_obWind;
}

void ReorgOB_Int::init(const StackedUnitList& units)
{
  if(d_obWind)
    d_obWind->init(units);
}

void ReorgOB_Int::run(const PixelPoint& p)
{
  if(d_obWind)
    d_obWind->run(p);
}

void ReorgOB_Int::destroy()
{
  if(d_obWind)
  {
    // DestroyWindow(d_obWind->getHWND());
     delete d_obWind;
    d_obWind = 0;
  }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
