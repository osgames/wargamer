/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ORG_LIST_HPP
#define ORG_LIST_HPP

#ifndef __cplusplus
#error org_list.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Temporary Order of Battle for doing reorganizations
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "poolarry.hpp"
#include "rank.hpp"

class CampaignData;

/*
 * An object within the Organization Dialog
 */

/*
 * A way of identifying Command Positions
 */

class OrgCPI {
	PoolIndex d_index;

 public:
	enum {
		None = 0			// 1st item in list is a dummy entry point
	};

	OrgCPI() : d_index(None) { }
 	// OrgCPI(PoolIndex i) : d_index(i) { }
  	OrgCPI(int i) : d_index(static_cast<PoolIndex>(i))
	{
		// ASSERT((i >= 0) && (i <= PoolIndex_MAX));
		ASSERT(i == d_index);		// Checks that didn't get truncated
	}

  	// OrgCPI(LPARAM i) : d_index(PoolIndex(i)) { ASSERT((i >= 0) && (i <= PoolIndex_MAX)); }
	OrgCPI(const OrgCPI& cp) : d_index(cp.d_index) { }

	OrgCPI& operator = (const OrgCPI& cp)
	{
		d_index = cp.d_index;
		return *this;
	}

	operator int() const { return d_index; }
};

class OrgObject
{
	friend class OrgList;

 public:
	enum What {
		OOW_CommandPosition,
		OOW_Leader,
		OOW_StrengthPoint,
		OOW_Nothing,

		OrgObject_Last = OOW_Nothing
	};

 private:
	What		d_what;

	ConstILeader				d_leader;			// NoLeader if new
	ConstISP					d_sp;					// NoSP if new
	ConstICommandPosition	d_cp;					// NoCommandPosition if new

	ConstICommandPosition	d_originalParent;		// NoCommandPosition if new

	OrgCPI	d_parent;
	OrgCPI	d_sister;
	OrgCPI	d_child;

	Nationality d_nation;
	RankEnum d_rank;

	HTREEITEM	d_treeIndex;					// HTREEITEM in main TreeView, NULL if not in window
	HTREEITEM	d_contentIndex;				// HTREEITEM in contents window

 public:
 // private:		// Only OrgList may manipulate OrgObjects

	OrgObject();

	OrgCPI	child() const { return d_child; }
	OrgCPI	sister() const { return d_sister; }
	OrgCPI	parent() const { return d_parent; }
	ConstICommandPosition originalParent() const { return d_originalParent; }

	What		what() const { return d_what; }

	// const char* getName(const CampaignData* campData, const char* text = 0) const;
	// int getImage(const CampaignData* campData) const;
	// int getSelectedImage(const CampaignData* campData) const;
	// Side getSide(const CampaignData* campData) const;

	HTREEITEM treeIndex() const { return d_treeIndex; }
	void treeIndex(HTREEITEM hItem) { d_treeIndex = hItem; }

	HTREEITEM contentIndex() const { return d_contentIndex; }
	void contentIndex(HTREEITEM hItem) { d_contentIndex = hItem; }

	Nationality nation() const { return d_nation; }
	RankEnum rank() const { return d_rank; }

	bool isFictional() const
	{
		return	(d_what == OOW_Nothing)
				||	( (d_what == OOW_CommandPosition) && (d_cp == NoCommandPosition)	)
				|| ( (d_what == OOW_Leader) 			 && (d_leader == NoLeader) 		)
				|| ( (d_what == OOW_StrengthPoint) 	 && (d_sp == NoStrengthPoint) 	);
	}

	ConstICommandPosition cp() const
	{
		ASSERT(d_what == OOW_CommandPosition);
		if(d_what == OOW_CommandPosition)
			return d_cp;
		else
			return NoCommandPosition;
	}

	ConstILeader leader() const
	{
		ASSERT(d_what == OOW_Leader);
		if(d_what == OOW_Leader)
			return d_leader;
		else
			return NoLeader;
	}

	ConstISP sp() const
	{
		ASSERT(d_what == OOW_StrengthPoint);
		if(d_what == OOW_StrengthPoint)
			return d_sp;
		else
			return NoStrengthPoint;
	}
};

class OrgList
{
	PoolArray<OrgObject> d_objects;
	const CampaignData* d_campData;
 public:
	OrgList(const CampaignData* campData);
	OrgList(const CampaignData* campData, ConstParamCP topCPI);
	~OrgList() { }

	void initUnits(ConstParamCP topCPI);
    void reset() { d_objects.reset(); }

	OrgCPI addCP(ConstParamCP cpi, OrgCPI iParent);
			// Add command position as a child to parent

	void remove(OrgCPI cpi) { d_objects.remove(cpi); }

	OrgCPI addLeader(ConstILeader il, OrgCPI iParent);
			// Add command position as a child to parent

	OrgCPI addSP(ConstISP isp, OrgCPI iParent);
			// Add Strength Point to list

	const OrgObject& operator [] (OrgCPI i) const { return d_objects[i]; }
	OrgObject& operator [] (OrgCPI i) { return d_objects[i]; }

	Boolean hasCPChild(OrgCPI i) const;
		// Return True if object has children that are CommandPositions

	void transfer(OrgCPI from, OrgCPI dest, Boolean replaceLeader = True);
		// Transfer an object to be a child of dest
		// Note that this does not alter the treeview items!

	Boolean canTransfer(OrgCPI from, OrgCPI to);
		// Return True if it is possible to transfer <from> to <to>

	Boolean canDetach(OrgCPI iOrg);
		// Return True if Org is allowed to detach

	Boolean canMerge(OrgCPI iOrg);

	Boolean canMerge(OrgCPI from, OrgCPI to);

	void merge(OrgCPI from, OrgCPI to);

		// Return True if Org may potentially merge with something else

	const char* getName(OrgCPI iOrg) const;
		// Return Name of object
	int getImage(OrgCPI iOrg) const;
		// return Image for object
	int getSelectedImage(OrgCPI iOrg) const;
		// return Selected Image for object
	Side getSide(OrgCPI iOrg) const;
		// Get side of object

#ifdef DEBUG
//	int entries() const { return d_objects.used(); }
   bool isValid(OrgCPI ob) const
   {
      return d_objects.isUsed(ob);
   }
#endif

	enum { Top = OrgCPI::None };

 private:
	void addChild(OrgCPI iParent, OrgCPI iChild);
			// Add OrgObject as the last child of another OrgObject

	void detach(OrgCPI iOrg);
			// Detach Unit from it's parent

	ConstICommandPosition getCP(OrgCPI iOrg) const;
			// Return ConstICommandPosition belonging to iOrg
			// Returns NoCommandPosition of iOrg is not a CommandPosition

	Boolean canTransferSP(OrgCPI from, OrgCPI to) const;
	Boolean canTransferLeader(OrgCPI from, OrgCPI to) const;
	void addNewLeader(const OrgObject& oldLeader);
	OrgCPI addNewUnit(OrgCPI parent);

};

class OrgListIter {
	 OrgList& d_list;
	 OrgCPI d_startCPI;
	 OrgCPI d_currentCPI;
  public:

	 OrgListIter(OrgList& ol, OrgCPI startCPI) :
		d_list(ol),
		d_startCPI(startCPI),
		d_currentCPI(OrgCPI::None) {}

	 Boolean next();
	 Boolean sister();

	 OrgCPI currentCPI() { return d_currentCPI; }
	 OrgObject& currentObject()
	 {
		ASSERT(d_currentCPI != OrgCPI::None);
		ASSERT(d_list.isValid(d_currentCPI));
		return d_list[d_currentCPI];
	 }

	 void rewind() { d_currentCPI = OrgCPI::None; }
};


#endif /* ORG_LIST_HPP */

