/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BLD_DIAL_HPP
#define BLD_DIAL_HPP

#include "grtypes.hpp"
#include "townbld.hpp"
//#include "gamedefs.hpp"

/*
 * Pop-up build item dialog, called from town-order dialog
 */

class CampaignData;
class BuildItemWindow;

class BuildItemInterface
{
   public:
     struct Data {
	    Nationality d_nation;
	    BuildWhat d_what;
	    UnitType d_currentType;
     };


      BuildItemInterface(HWND hParent, UWORD id, const CampaignData* campData, const SIZE& s);
      ~BuildItemInterface();
      void init(const Data& data);
      void show(PixelPoint& p);
      void hide();
      UnitType currentType();
//   static void create(HWND hParent, UWORD id, const CampaignData* campData, const SIZE& s);
//   static void init(const Data& data);
//   static void show(PixelPoint& p);
//   static void hide();
//   static UnitType currentType();
   private:
      BuildItemWindow* d_imp;

};

#endif
