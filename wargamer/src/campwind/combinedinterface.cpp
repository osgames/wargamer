/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 * Combined Campaign User Interface
 *
 * A combination of the old dual mode system
 */

#include "stdinc.hpp"
#include "combinedInterface.hpp"
#include "uinfowin.hpp"
#include "uordrwin.hpp"
#include "infowind.hpp"
#include "unitmenu.hpp"
#include "reorgob.hpp"
#include "mapwind.hpp"
#include "mw_data.hpp"
#include "campdint.hpp"
#include "camp_snd.hpp"
#include "armyutil.hpp"
#include "route.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "cwin_int.hpp"
#include "ds_unit.hpp"
#include "tordrwin.hpp"
#include "tinfowin.hpp"
#include "control.hpp"

namespace {
   /*
   * Make sure route doesn't double back on itself
   * if it does, fix it
   */

   void makeRouteLogical(const CampaignData* campData, ConstParamCP cpi, CampaignOrder& order)
   {
      ASSERT(campData);
      ASSERT(cpi != NoCommandPosition);
      ASSERT(order.isMoveOrder());
      ASSERT(order.getNVias() > 0);

      /*
      * First, plot a route using currrent order
      */

      RouteList rl;
      CampaignRouteUtil::plotRoute(campData, rl, cpi->getPosition(), order, cpi->getSide());
//      ASSERT(rl.entries() >= 2);

      /*
      * Now, go through and remove any repeating nodes
      */

      RouteNode* first = rl.first();
      // Boolean remove = False;

      RouteListIterNLW iter(&rl);
      while(++iter)
      {
         if(first != iter.current() &&
            first->d_town == iter.current()->d_town)
         {

            RouteNode* removeNode = iter.current();

            /*
            * we also have to remove all previous nodes to this one
            */

            RouteListIterNLW iter2(&rl);
            while(++iter2)
            {
               RouteNode* node = iter2.current();
               if(node == removeNode)
                  break;
               else
               {
                  iter2.remove();
                  iter2.rewind();
               }
            }

            iter.remove();
            iter.rewind();
         }
      }

      /*
      * Now make sure unit is not on a connection between the first 2 nodes
      * or at the first node
      */

      if(rl.entries() >= 2)
      {
         RouteNode* node1 = rl.first();
         ASSERT(node1);

         ITown town1 = node1->d_town;

         Boolean remove = False;

         if(cpi->atTown())
         {
            remove = (town1 == cpi->getTown());
         }
         else
         {
            const Connection& con = campData->getConnection(cpi->getConnection());
            ITown town2 = rl.next()->d_town;

            remove = ( (con.node1 == town1 && con.node2 == town2) ||
               (con.node1 == town2 && con.node2 == town1) );
         }

         if(remove)
            rl.remove(node1);
      }

      /*
      * Finally, remove any vias that are not in the route list
      */

      Boolean found = False;

      while(!found)
      {
         ITown viaTown = order.getVia(0);
         if(viaTown == NoTown)
            break;

         iter.rewind();
         while(++iter)
         {
            ITown town = iter.current()->d_town;
            if(town == viaTown)
            {
               found = True;
               break;
            }
         }

         if(!found)
            order.getVias()->remove(0);
      }
   }

};    // Private local functions

CombinedCInterface::CombinedCInterface(CampaignWindowsInterface* cwi, const CampaignData* campData, MapWindow* mapWindow) :
   d_campWind(cwi),
   d_campData(campData),
   d_mapWindow(mapWindow),

   d_trackMode(MWTM_None),
   d_mode(Mode_Waiting),
   d_startMouseLocation(),

   d_unitInfoWindow(0),
   d_unitOrderWindow(0),
   d_trackingWindow(0),
   d_menu(0),
   d_obWindow(0),

   d_townOrderWind(0),
   d_townInfoWind(0),

   d_showTown(NoTown),
//   d_orderedTown(NoTown),
//   d_townUnderCursor(NoTown),
   d_showUnit(NoCommandPosition),
//   d_unitUnderCursor(NoCommandPosition),
//   d_orderedUnit(NoCommandPosition),
   d_stackedUnits(),
   d_unitID(NoUnitListID),
   d_orderValues(),
   d_routeList(),

   d_trackingUnit(false),
   d_trackingTown(false),
   d_canOrderUnit(false)
{
   d_unitInfoWindow = new UnitInfo_Int(this, d_mapWindow->getHWND(), d_campData);
   d_unitOrderWindow = new UnitOrder_Int(this, d_mapWindow->getHWND(), d_campWind, d_campData, d_orderValues);
   d_trackingWindow = new TrackingWindow(mapWindow->getHWND(), d_campData);
   d_menu = new UnitMenu_Int(this, mapWindow->getHWND(), d_campWind, d_campData, d_orderValues);
   d_obWindow = new ReorgOB_Int(this, d_mapWindow->getHWND(), d_campWind, d_campData);

   d_townOrderWind = new TownOrder_Int(this, d_mapWindow->getHWND(), d_campWind, d_campData);
   d_townInfoWind = new TownInfo_Int(this, d_mapWindow->getHWND(), d_campData);

}


CombinedCInterface::~CombinedCInterface()
{
   delete d_unitInfoWindow;
   d_unitInfoWindow = 0;

   delete d_unitOrderWindow;
   d_unitOrderWindow = 0;

   delete d_trackingWindow;
   d_trackingWindow = 0;

   delete d_obWindow;
   d_obWindow = 0;

   delete d_menu;
   d_menu = 0;

   delete d_townOrderWind;
   d_townOrderWind = 0;

   delete d_townInfoWind;
   d_townInfoWind = 0;

   d_campWind = 0;
   d_campData = 0;
   d_mapWindow = 0;
}

/*
 * virtual functions
 */


/*
 * Left button cycles through stacked units or town
 */

void CombinedCInterface::onLClick(MapSelect& info)
{
   if(d_mode == Mode_Waiting)
   {
      d_startMouseLocation = info.mouseLocation;

      if(d_unitID == NoUnitListID)
      {
         if(info.trackedUnits->unitCount() > 0)
            d_unitID = 0;
      }
      else
      {
         ++d_unitID;
         if (d_unitID >= info.trackedUnits->unitCount())
         {
            if((info.selectedTown != NoTown) || (info.trackedUnits->unitCount() == 0))
               d_unitID = NoUnitListID;
            else
               d_unitID = 0;
         }
      }

      ASSERT( (d_unitID == NoUnitListID) || (d_unitID < info.trackedUnits->unitCount()));

      if((d_unitID == NoUnitListID) && (info.selectedTown != NoTown))
      {
         d_showTown = info.selectedTown;
         d_showUnit = NoCommandPosition;
         d_trackingWindow->update(d_showTown);
      }
      else if(d_unitID != NoUnitListID)
      {
         d_showTown = NoTown;
         d_showUnit = info.trackedUnits->getUnit(d_unitID);
         d_trackingWindow->update(d_showUnit);
      }
      else
      {
         d_showTown = NoTown;
         d_showUnit = NoCommandPosition;
         d_unitID = NoUnitListID;
      }
   }

   clearTrackingFlags();
}

/*
 * Right Button brings up popup menu
 */


void CombinedCInterface::onRClick(MapSelect& info)
{
   if (d_mode == Mode_Waiting)
   {
      ConstICommandPosition orderedUnit = NoCommandPosition;

      if ((d_showTown != NoTown) || (d_showUnit != NoCommandPosition))
      {
         d_showTown = NoTown;
         d_showUnit = NoCommandPosition;
         d_trackingWindow->hide();
      }

      if(info.trackedUnits->unitCount() > 0)
      {
         d_stackedUnits = *info.trackedUnits;   // array copy

         if ( (d_unitID == NoUnitListID) ||
             (d_unitID >= d_stackedUnits.unitCount()) )
         {
            d_unitID  = 0;
         }
         // d_orderedUnit = d_stackedUnits.getUnit(d_unitID);
         orderedUnit = d_stackedUnits.getUnit(d_unitID);
         d_orderValues = *orderedUnit->getLastOrder();
      }
      else
      {
         d_unitID = NoUnitListID;
      }

      PixelPoint p1;
      d_mapWindow->mapData().locationToPixel(info.mouseLocation, p1);

      POINT p = p1;;

      ClientToScreen(d_mapWindow->getHWND(), &p);

      p1.setX(p.x);
      p1.setY(p.y);


      d_menu->runMenu(orderedUnit, info.selectedTown, p1);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_WINDOWOPEN, CAMPAIGNSOUNDPRIORITY_INSTANT);
   }
}

/*
 * Button Down initialises values in case a drag is started
 */

void CombinedCInterface::onButtonDown(MapSelect& info)
{
   // if we're waiting then check for a clicked-on unit
   if(d_mode == Mode_Waiting)
   {
      d_trackMode = MWTM_Unit;

      if( (info.trackedUnits->unitCount() > 0) &&
          (d_unitID != NoUnitListID))
      {
         if(d_unitID >= info.trackedUnits->unitCount())
            d_unitID = 0;

         ConstICommandPosition cp = info.trackedUnits->getUnit(d_unitID);

         d_trackingUnit = true;
         d_stackedUnits = *info.trackedUnits;   // array copy

         d_canOrderUnit = CampaignArmy_Util::isUnitOrderable(info.trackedUnits->getUnit(d_unitID));

         d_orderValues = *cp->getLastOrder();

         if(d_orderValues.isMoveOrder())
         {
            CampaignRouteUtil::plotRoute(d_campData, d_routeList, cp->getPosition(),
               d_orderValues, cp->getSide());
         }

//         initOrderWindow();
      }
   }

   // otherwise check for clicked-on Last Destination

   else if(d_mode == Mode_SettingUpMove)
   {
      ASSERT(d_trackMode == MWTM_Town);
      if(info.selectedTown != NoTown && info.selectedTown == d_orderValues.getDestTown())
      {
         d_trackingTown = true;
      }
   }

   // otherwise check for clicked on current unit

   else if(d_mode == Mode_SettingUpOrder)
   {
      ASSERT(d_trackMode == MWTM_Unit);
      if(info.trackedUnits->unitCount() > 0)
      {
         // see if stacked units contains current unit
         ConstICommandPosition currentCPI = d_unitOrderWindow->currentUnit(); //d_orderDial[UnitDialog::UnitDialogType::MainOrderDialog]->currentUnit();

         currentCPI = d_campData->getArmies().getTopParent(currentCPI);
         ASSERT(currentCPI != NoCommandPosition);
         for(int i = 0; i < info.trackedUnits->unitCount(); i++)
         {
            if(currentCPI == info.trackedUnits->getUnit(i))
            {
               d_trackingUnit = true;
               d_canOrderUnit = CampaignArmy_Util::isUnitOrderable(currentCPI);
            }
         }
      }
   }

   if(d_trackingUnit || d_trackingTown)
   {
      d_unitOrderWindow->show(false);
      d_startMouseLocation = info.mouseLocation;
   }
}

bool CombinedCInterface::onStartDrag(MapSelect& info)
{
   if((d_trackingUnit || d_trackingTown) && d_canOrderUnit)
   {
      d_trackMode = MWTM_Dragging;
      return true;
   }

   return false;


   // todo: Check if dragged object is valid
   //   not valid if mode is not waiting and unit is different

//   else
//         d_trackMode = MWTM_None;
}

void CombinedCInterface::onDrag(MapSelect& info)
{
   if (d_trackMode == MWTM_Dragging)
   {
      if (d_stackedUnits.getUnit(d_unitID)->isDead())
      {
         d_trackMode = MWTM_None;
      }
   }
}

void CombinedCInterface::onEndDrag(MapSelect& info)
{
   if(d_trackMode == MWTM_Dragging)
   {
      Location location;

      ASSERT(d_trackingUnit || d_trackingTown);

      /*
       * if we can't order unit, return
       */
      // If unit has died while being dragged then give up

      if (d_canOrderUnit && !d_stackedUnits.getUnit(d_unitID)->isDead())
      {
         bool canGo = false;
         ITown itown = NoTown;

         if(info.hasTown())
            itown = info.selectedTown;
         else if(info.hasUnit())
            itown = info.getCP()->getCloseTown();

         if (itown != NoTown)
         {
            const Town& town = d_campData->getTown(itown);
            canGo = !town.isOffScreen();
         }

         // if((info.hasTown() || info.hasUnit()))        // and we have a destTown or unit
         if(canGo)
         {
            // if tracking unit then clear previous via's
            // if(d_flags & TrackingUnit)
            if(d_trackingUnit)
               d_orderValues.getVias()->clear();

            // if we already have a destination then make it a via

            if(d_trackingTown &&
               d_orderValues.hasDestTown())
            {
               if(d_orderValues.getNVias() < Orders::Vias::MaxVia)
               {
                  d_orderValues.setVia(d_orderValues.getDestTown(), d_orderValues.getNVias());
               }
            }

            if(info.hasTown())
            {
               d_orderValues.setDestTown(info.selectedTown);
            }
            else
            {
               ASSERT(info.hasUnit());
               d_orderValues.setTargetUnit(info.getCP());
            }

            d_mode = Mode_SettingUpMove;
            d_orderValues.setType(Orders::Type::MoveTo);
            d_trackMode = MWTM_Town;
            location = info.mouseLocation;
         }
         else
         {
            // if we were tracking a dest town then go back to last town and reopen move dialog

            if(d_trackingTown)
            {
               ASSERT(d_orderValues.hasDestTown());

               const Town& t = d_campData->getTown(d_orderValues.getDestTown());
               location = t.getLocation();
               d_mode = Mode_SettingUpMove;
               d_trackMode = MWTM_Town;
            }
            else
               d_trackMode = MWTM_Unit;
         }

         if(d_mode == Mode_SettingUpMove)
         {
            ASSERT(d_stackedUnits.unitCount() > 0);
            ASSERT(d_unitID != NoUnitListID);
            ASSERT(d_unitID < d_stackedUnits.unitCount());

            ConstICommandPosition cp = d_stackedUnits.getUnit(d_unitID);
            CampaignRouteUtil::plotRoute(d_campData, d_routeList, cp->getPosition(),
               d_orderValues, cp->getSide());

            PixelPoint p;
            calcMoveOrderDialPosition(location, p);

            d_campData->soundSystem()->TriggerSound(SOUNDTYPE_WINDOWOPEN, CAMPAIGNSOUNDPRIORITY_INSTANT);

            UBYTE flag;

            if(d_mode != Mode_Waiting)
            {
               flag = UnitOrder_Int::MoveOrder;
            }
            else
            {
               ASSERT(d_mode == Mode_Waiting);

               d_mode = Mode_SettingUpOrder;
               d_trackMode = MWTM_Unit;
               flag = UnitOrder_Int::FullOrder;
            }

            d_unitOrderWindow->init(d_stackedUnits, d_unitID);
            d_unitOrderWindow->run(p, flag);
            d_campWind->redrawMap();
         }
      }
   }

   clearTrackingFlags();
}

/*
 * Over an object... set up info window
 */

void CombinedCInterface::overObject(MapSelect& info)
{
   if(d_mode == Mode_Waiting &&
      !d_trackingUnit && !d_trackingTown)
   {
      // Return if it is the same object as before

      if( (d_showTown != NoTown) && (d_showTown == info.selectedTown))
      {
         return;
      }

      if( (d_unitID != NoUnitListID) &&
          (d_unitID < info.trackedUnits->unitCount()) &&
          (d_showUnit == info.trackedUnits->getUnit(d_unitID)))
      {
            return;
      }

      if(info.trackedUnits->unitCount() > 0)
      {
         if(d_unitID >= info.trackedUnits->unitCount())
            d_unitID = 0;

         d_showUnit = info.trackedUnits->getUnit(d_unitID);
         d_showTown = NoTown;

         Location l;
         d_showUnit->getLocation(l);

         PixelPoint p;
         calcTrackingDialPosition(d_mapWindow, l, p,
            d_trackingWindow->width(), d_trackingWindow->height());

         d_trackingWindow->show(p, d_showUnit);
      }
      else if(info.selectedTown)
      {
         d_showTown = info.selectedTown;
         d_showUnit = NoCommandPosition;
         d_unitID = NoUnitListID;

         const Town& t = d_campData->getTown(d_showTown);
         Location l = t.getLocation();
         PixelPoint p;
         calcTrackingDialPosition(d_mapWindow, l, p,
            d_trackingWindow->width(), d_trackingWindow->height());
         d_trackingWindow->show(p, d_showTown);
      }
   }
}

void CombinedCInterface::notOverObject()
{
   ASSERT(d_trackingWindow);

   if( (d_showUnit != NoCommandPosition) ||
       (d_showTown != NoTown) )
   {
      d_trackingWindow->hide();
      d_showUnit = NoCommandPosition;
      d_showTown = NoTown;
   }


   if (!d_trackingUnit &&
      !d_trackingTown &&
      (d_mode == Mode_Waiting))
   {
      d_unitID = NoUnitListID;
   }
}

/*
 * Map Drawing
 */

void CombinedCInterface::onDraw(DrawDIBDC* dib, MapSelect& info)
{
   if(d_stackedUnits.unitCount() > 0)
   {
      UnitListID unitID = d_unitID;
      if( (unitID == NoUnitListID) || (unitID >= d_stackedUnits.unitCount()) )
         unitID = 0;

      ConstICommandPosition cp = d_stackedUnits.getUnit(unitID);
      d_mapWindow->drawRoute(dib, cp->getPosition(), d_orderValues, cp->getSide(), d_routeList);
   }
}

void CombinedCInterface::update()
{
   d_campWind->redrawMap();
}              // Info may have changed

#if 0 // TODO
void CombinedCInterface::orderUnit(ConstParamCP cpi)
{
   d_stackedUnits.reset();
   d_stackedUnits.addUnit(cpi);
   d_unitID = 0;
//   d_orderedTown = NoTown;
//   d_overThisUnit = cpi;

   d_orderedUnit = d_stackedUnits.getUnit(d_unitID);
   d_orderValues = *d_orderedUnit->getLastOrder();
//   initOrderWindow();
}
#endif

void CombinedCInterface::infoTown(ITown town)
{
   ASSERT(town != NoTown);
   const Town& t = d_campData->getTown(town);

   Location l;
   l = t.getLocation();
   PixelPoint p;
   calcMoveOrderDialPosition(l, p);

   d_townInfoWind->run(town, p);
}

void CombinedCInterface::infoUnit(ConstParamCP cp)
{
   ASSERT(cp != NoCommandPosition);

   Location l;
   cp->getLocation(l);
   PixelPoint p;
   calcMoveOrderDialPosition(l, p);
   d_unitInfoWindow->run(cp, p);
}

void CombinedCInterface::orderTown(ITown town)
{
   ASSERT(town != NoTown);
   const Town& t = d_campData->getTown(town);

   const Province& prov = d_campData->getProvince(t.getProvince());
   Side side = prov.getSide();
   if (GamePlayerControl::canControl(side))
   {
      Location l;
      l = t.getLocation();
      PixelPoint p;
      calcMoveOrderDialPosition(l, p);

      d_townOrderWind->init(town);
      d_townOrderWind->run(p);
   }
}

void CombinedCInterface::updateZoom()
{
}        // update map arrows when zooming


/*
 * Sending Orders
 */

void CombinedCInterface::sendOrder(ConstParamCP cpi)
{
   ASSERT(cpi != NoCommandPosition);

   /*
   * if this is a move order, and we have via towns,
   * make sure the route doesn't double back on itself.
   */

   if(d_orderValues.isMoveOrder() && d_orderValues.getNVias() > 0)
   {
      makeRouteLogical(d_campData, cpi, d_orderValues);
   }

   sendUnitOrder(cpi, &d_orderValues);
   cancelOrder();
}

void CombinedCInterface::cancelOrder()
{
   d_trackMode = MWTM_Unit;
   d_mode = Mode_Waiting;
   d_orderValues.clearValues();
   d_stackedUnits.reset();
   d_canOrderUnit = False;

   d_campData->soundSystem()->TriggerSound(SOUNDTYPE_WINDOWCLOSE, CAMPAIGNSOUNDPRIORITY_INSTANT);

   d_campWind->redrawMap();
}

/*
 * Misc.
 */

/*
 * Functions Dialogs use
 */
#if 0    // TODO
void CombinedCInterface::runOrders(const PixelPoint& p)
{
   if((d_unitID != NoUnitListID) && d_unitOrderWindow)
   {
      UBYTE flag;

      if(d_mode != Mode_Waiting)
      {
         flag = UnitOrder_Int::MoveOrder;
      }
      else
      {
         ASSERT(d_mode == Mode_Waiting);

         d_mode = Mode_SettingUpOrder;
         d_trackMode = MWTM_Unit;
         flag = UnitOrder_Int::FullOrder;
      }

      d_unitOrderWindow->init(d_stackedUnits, d_unitID);
      d_unitOrderWindow->run(p, flag);
      d_campWind->redrawMap();
   }
   else if ((d_orderedTown != NoTown) && d_townOrderWindow)
   {
      d_townOrderWindow->init(d_orderedTown);
      d_townOrderWindow->run(p);
   }
}
#endif

#if 0    // TODO
void CombinedCInterface::runInfo(const PixelPoint& p)
{


   if((d_stackedUnits.unitCount() > 0) && d_unitInfoWindow)
   {
      // Restructured to use if/else instead of ?:
      // to avoid compiler optimization bug

      UnitListID id;
      if((d_unitID != NoUnitListID) &&
         (d_unitID < d_stackedUnits.unitCount()))
      {
         id = d_unitID;
      }
      else
      {
         id = 0;
      }

      ConstICommandPosition cpi;
      if((d_mode == Mode_SettingUpOrder) ||
         (d_mode == Mode_SettingUpMove))
      {
         cpi = d_unitOrderWindow->currentUnit();
      }
      else
      {
         cpi = d_stackedUnits.getUnit(id);
      }

      d_unitInfoWindow->run(cpi, p);
   }
}
#endif

void CombinedCInterface::reorgOB(ConstParamCP cp)
{
   ASSERT(d_obWindow);
   setUnit(cp);

   PixelPoint p;
   Location l;
   cp->getLocation(l);
   calcMoveOrderDialPosition(l, p);

   d_obWindow->init(d_stackedUnits);
   d_obWindow->run(p);
}


void CombinedCInterface::orderUnit(ConstParamCP cpi)
{
   // Work out position of Unit

   // d_menu->runMenu(cpi, NoTown, p);

   PixelPoint p;
   Location l;
   cpi->getLocation(l);
   calcMoveOrderDialPosition(l, p);

   setUnit(cpi);
   d_unitOrderWindow->init(d_stackedUnits, d_unitID);

   d_unitOrderWindow->run(p, UnitOrder_Int::FullOrder);
   d_campWind->redrawMap();

   d_mode = Mode_SettingUpOrder;
   d_trackMode = MWTM_Unit;
}



/*
 * Clean up
 */

// void CombinedCInterface::destroy()
// {
// }



#if 0
void CombinedCInterface::initOrderWindow()
{
   if(d_stackedUnits.unitCount() > 0)
   {
      ASSERT(d_unitID != NoUnitListID);
      d_unitID = (d_unitID < d_stackedUnits.unitCount()) ? d_unitID : 0;

      d_unitOrderWindow->init(d_stackedUnits, d_unitID);
   }
}
#endif

/*
 * Calculate position of pop-up move-order dialog
 * This will work as follows:
 *  1. If dragging left to right dialog will popup to the right of the mouse
 *  2. If dragging right to left dialog will popup with its right edge to
 *     the left of the mouse,
 *  3. The edge of the screen will also have to be taken into account
 */

void CombinedCInterface::calcMoveOrderDialPosition(const Location& endLocation, PixelPoint& endP)
{
   /*
   * Convert locations to pixels
   */

   PixelPoint startP;
   d_mapWindow->mapData().locationToPixel(d_startMouseLocation, startP);
   d_mapWindow->mapData().locationToPixel(endLocation, endP);

   /*
   * Calculate offset
   */

   int xOffset = 0;
   int yOffset = 0;
   const int offset = 15;

   if(startP.getX() < endP.getX())  // mouse was dragged left to right
   {
      xOffset += offset;
      yOffset += offset;
   }
   else // mouse was dragged right to left
   {
      xOffset -= ((d_unitOrderWindow->width()) + offset);
      yOffset += offset;
   }

   endP.setX(endP.getX() + xOffset);
   endP.setY(endP.getY() + yOffset);

   /*
   * Convert to screen
   */

   POINT p = endP;
   ClientToScreen(d_mapWindow->getHWND(), &p);

   // clip
   RECT r;
   GetWindowRect(d_mapWindow->getHWND(), &r);

   if(p.x > r.right - d_unitOrderWindow->width())
      p.x = r.right - d_unitOrderWindow->width();
   else if(p.x < r.left)
      p.x = r.left;

   if(p.y > r.bottom - d_unitOrderWindow->fullHeight())
      p.y = r.bottom - d_unitOrderWindow->fullHeight();
   else if(p.y < r.top)
      p.y = r.top;

   endP = p;
}


void CombinedCInterface::setUnit(ConstParamCP cpi)
{
   // If cpi is in d_stackedUnits then make it current

   for(int i = 0; i < d_stackedUnits.unitCount(); ++i)
   {
      ConstICommandPosition cp = d_stackedUnits.getUnit(i);
      if (cp == cpi)
      {
         d_unitID = i;
         return;
      }
   }

   // otherwise set up a new stackedUnitList

   d_stackedUnits.reset();
   d_stackedUnits.addUnit(cpi);
   d_unitID = 0;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
