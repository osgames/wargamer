/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Town Information Window
 *
 * This needs rewriting, so that it can adjust itself to the size
 * of the window.
 *
 * There are currently some hardwired coordinates, which should be
 * obtained from the window it is being displayed in.
 *
 * Also, it will need to be more graphical.
 * All graphics should be pulled from resource file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "towninfo.hpp"
#include "tbldimg.hpp"
#include "gamedefs.hpp"
#include "townprop.hpp"
#include "scrnbase.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "scenario.hpp"
#include "wmisc.hpp"
#include "resdef.h"
#include "armies.hpp"
#include "bmp.hpp"
#include "dib.hpp"
#include "dib_blt.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "options.hpp"
#include "terrain.hpp"
#include "simpstr.hpp"
#include "scn_img.hpp"
#include "fonts.hpp"
#include "resstr.hpp"
#include "piclib.hpp"
#include "dc_hold.hpp"
#include "colours.hpp"
#include "spacing.hpp"
#include "bargraph.hpp"
#include <stdio.h>      // for sprintf()
#include "winfile.hpp"

class TownAttributes
{
    public:
       enum Situation {
          First = 0,
          Unoccupied = First,
          Occupied,
          Raided,
          Besieged
       };

    public:
        TownAttributes(ITown town, const CampaignData* campData);

        ITown itown() const { return d_itown; }
        const Town* town() const { return d_town; }
        IProvince iprovince() const { return d_iprovince; }
        const Province* province() const { return d_province; }

        const BuildItem* builds() const { return d_province->builds(); }

        /*
         * information functions
         */

      Side getStartSide() const { return d_province->getStartSide(); }
       bool isTransferable() const { return d_town->getMpTransferable(); }
      Attribute supplyRate() const { return d_town->getSupplyRate(); }
        SupplyLevel getSupplyLevel() const { return d_town->getSupplyLevel(); }
        Attribute strength() const { return d_town->getStrength(); }

       Attribute getForage() const;

       AttributePoints victoryPoints(Side s) const;
        AttributePoints totalVictory(Side s);
        AttributePoints controlledVictory(Side s);
       Attribute politicalPoints() const;
        AttributePoints totalPolitical();
        AttributePoints controlledPolitical(Side s);

        AttributePoints availableManpower();
        AttributePoints totalManpower();
        AttributePoints availableResources();
        AttributePoints totalResources();

       Attribute manpowerRate() const { return d_town->getManPowerRate(); }
       Attribute resourceRate() const { return d_town->getResourceRate(); }

        bool isSeen();

        int getFortCompletionWeeks() const
        {
            TimeTick week = WeeksToTicks(1);
            return (d_fortCompletionTime + week - 1) / week;
        }

        int getDepotCompletionWeeks() const
        {
            TimeTick week = WeeksToTicks(1);
            return (d_depotCompletionTime + week - 1) / week;
        }

        Town::FortLevel getFortLevel() const;
       SPCount getEffectiveGarrisonSize();
      SPCount getGarrisonSizeSP();
       SPCount getMaxGarrisonSize();

        Situation getSituation();
        bool hasCatastrophe() const;


        void calculateProvince();
        void calculateResources();
        void calculateGarrison();

        int getTimeToGo(const BuildItem* builds, const BuildItem& b);
        float getBuildComplete(const BuildItem b);

    private:
        const CampaignData* d_campData;
      ITown d_itown;
      const Town* d_town;
      IProvince d_iprovince;
      const Province* d_province;

        enum { NumSides = 2 };

        bool d_isProvinceCalculated;
        AttributePoints d_totalVictory[NumSides];
        AttributePoints d_controlledVictory[NumSides];
        AttributePoints d_totalPolitical;
        AttributePoints d_controlledPolitical[NumSides];

        bool d_isResourceCalculated;
        ResourceSet d_total;
        ResourceSet d_available;

        bool d_isSeen;
        TimeTick d_fortCompletionTime;
        TimeTick d_depotCompletionTime;

        bool d_garrisonCalculated;
       ULONG d_garrisonSizeMP;
       SPCount d_garrisonSizeSP;
       SPCount d_maxGarrisonSize;

        Situation d_situation;

};


TownAttributes::TownAttributes(ITown town, const CampaignData* campData) :
    d_campData(campData),
   d_itown(town),
   d_town(0),
   d_iprovince(NoProvince),
   d_province(0),
    d_isProvinceCalculated(false),
    d_isResourceCalculated(false),
    d_isSeen(false),
    d_fortCompletionTime(0),
    d_depotCompletionTime(0),
    d_garrisonCalculated(false),
    d_garrisonSizeMP(0),
    d_garrisonSizeSP(0),
    d_maxGarrisonSize(0)
{
   ASSERT(d_itown != NoTown);
   ASSERT(d_campData != 0);

   d_town = &campData->getTown(d_itown);
   d_iprovince = d_town->getProvince();
   ASSERT(d_iprovince != NoProvince);
   d_province = &campData->getProvince(d_iprovince);

   ASSERT(d_town != 0);
   ASSERT(d_province != 0);


    ASSERT(NumSides == scenario->getNumSides());

#if defined(EDITOR)
    d_isSeen = true;
#else
    d_isSeen = ( (!CampaignOptions::get(OPT_FogOfWar)) ||
             (d_campData->isTownOrderable(d_itown)) ||
             (d_town->isSeen()) );
#endif

#if !defined(EDITOR)

    const int completeRange = 255;
    ICommandPosition cp = d_town->getFortUpgrader();
    if(cp != NoCommandPosition)
    {
         const Town_COrder* to = cp->townOrders().find(d_itown, Town_COrder::BuildFort);
         d_fortCompletionTime = to->d_tillWhen - campData->getTick();
    }

   cp = d_town->getSupplyInstaller();
   if(cp != NoCommandPosition)
   {
       const Town_COrder* to = cp->townOrders().find(d_itown, Town_COrder::BuildDepot);
       d_depotCompletionTime = to->d_tillWhen - campData->getTick();
   }

#endif

    d_situation = (d_town->isSieged()) ? Besieged :
                  (d_town->isRaided()) ? Raided :
                  (d_town->isOccupied()) ? Occupied :
                  Unoccupied;
}

/*
 * Information Functions
 */

void TownAttributes::calculateProvince()
{
    if(!d_isProvinceCalculated)
    {
        d_totalPolitical = 0;
        for(Side s = 0; s < NumSides; ++s)
        {
            d_totalVictory[s] = 0;
            d_controlledVictory[s] = 0;
            d_controlledPolitical[s] = 0;
        }


        const Town& town = d_campData->getTown(d_itown);
        const Province& prov = d_campData->getProvince(town.getProvince());

        ITown stop = static_cast<ITown>(prov.getTown() + prov.getNTowns());

        for(ITown tIndex = prov.getTown(); tIndex < stop; tIndex++)
        {
           const Town& t = d_campData->getTown(tIndex);

           d_totalPolitical += t.getPolitical();

           for(s = 0; s < NumSides; s++)
           {
              d_totalVictory[s] += t.getVictory(s);

              if(s == t.getSide())
              {
                  d_controlledVictory[s] += t.getVictory(s);
                  d_controlledPolitical[s] += t.getPolitical();
              }
           }
        }

        d_isProvinceCalculated = true;
    }
}

AttributePoints TownAttributes::victoryPoints(Side s) const
{
    return d_town->getVictory(s);
}

AttributePoints TownAttributes::totalVictory(Side s)
{
    calculateProvince();
    return d_totalVictory[s];
}

AttributePoints TownAttributes::controlledVictory(Side s)
{
    calculateProvince();
    return d_controlledVictory[s];
}

Attribute TownAttributes::politicalPoints() const
{
    return d_town->getPolitical();
}

AttributePoints TownAttributes::totalPolitical()
{
    calculateProvince();
    return d_totalPolitical;
}

AttributePoints TownAttributes::controlledPolitical(Side s)
{
    calculateProvince();
    return d_controlledPolitical[s];
}


void TownAttributes::calculateResources()
{
    if(!d_isResourceCalculated)
    {
        const Town& town = d_campData->getTown(d_itown);
        IProvince iprov = town.getProvince();

        TownProcess::getAvailableResources(d_campData, iprov, d_available);
        TownProcess::getTotalResources(d_campData, iprov, d_total);

        d_isResourceCalculated = true;
    }
}

AttributePoints TownAttributes::availableManpower()
{
    calculateResources();
    return d_available.manPower;
}

AttributePoints TownAttributes::totalManpower()
{
    calculateResources();
    return d_total.manPower;
}

AttributePoints TownAttributes::availableResources()
{
    calculateResources();
    return d_available.resource;
}

AttributePoints TownAttributes::totalResources()
{
    calculateResources();
    return d_total.resource;
}

bool TownAttributes::isSeen()
{
    return d_isSeen;
}

Attribute TownAttributes::getForage() const
{
    return d_town->getForage();
}

Town::FortLevel TownAttributes::getFortLevel() const
{
    return d_town->getFortifications();
}

SPCount TownAttributes::getEffectiveGarrisonSize()
{
    calculateGarrison();
    return minimum(d_maxGarrisonSize, d_garrisonSizeSP);
}

SPCount TownAttributes::getGarrisonSizeSP()
{
    calculateGarrison();
    return d_garrisonSizeSP;
}

SPCount TownAttributes::getMaxGarrisonSize()
{
    calculateGarrison();
    return d_maxGarrisonSize;
}

/*
 * If town has a garrison, get its size
 */

void TownAttributes::calculateGarrison()
{
    if(!d_garrisonCalculated)
    {
        d_garrisonCalculated = true;

        d_maxGarrisonSize = d_town->getStrength() * getFortLevel();
#if !defined(EDITOR)
        if(d_town->isGarrisoned())
        {
           ASSERT(d_town->getSide() != SIDE_Neutral);

           // go through units and find our garrison
#ifdef DEBUG
           Boolean found = False;
#endif
            const Armies* army = &d_campData->getArmies();

           ConstUnitIter iter(army, army->getFirstUnit(d_town->getSide()));
           while(iter.sister())
           {
               const CommandPosition* cp = iter.currentCommand();

               if(cp->isGarrison())
               {
                  ASSERT(cp->atTown());
                  if(cp->getTown() == d_itown)
                  {
#ifdef DEBUG
                     found = True;
#endif
                     /*
                      * Get garrison strength in sp
                      * strength cannot exceed max garrison value
                      * this is town strength * fortlevel
                      *
                      */

                     d_garrisonSizeSP = army->getUnitSPCount(iter.current(), True);

                     /*
                      * Get garrison strength in actual manpower
                      */

                     ConstSPIter spIter(army, iter.current());
                     while(++spIter)
                     {
                         const UnitTypeItem& uti = d_campData->getUnitType(spIter.current()->getUnitType());
                         BasicUnitType::value ut = uti.getBasicType();

                         d_garrisonSizeMP += (ut == BasicUnitType::Infantry) ? UnitTypeConst::InfantryPerSP :
                                              (ut == BasicUnitType::Cavalry) ? UnitTypeConst::CavalryPerSP :
                                              (ut == BasicUnitType::Artillery) ? UnitTypeConst::ArtilleryPerSP :
                                              UnitTypeConst::SpecialPerSP;
                     }
                  }
               }
            }
        }
#endif
    }
}

TownAttributes::Situation TownAttributes::getSituation()
{
    return d_situation;
}

bool TownAttributes::hasCatastrophe() const
{
#if defined(EDITOR)
    return false;
#else
    return d_town->hasCatastrophe(d_campData->getTick());
#endif
}

float TownAttributes::getBuildComplete(const BuildItem b)
{
  ASSERT(b.getWhat() < d_campData->getMaxUnitType());
  const UnitTypeItem& ut = d_campData->getUnitType(b.getWhat());

  AttributePoints tManpower = 0;
  AttributePoints tResource = 0;
  ut.getTotalResource(tManpower, tResource);
  tManpower *= b.getQuantity();
  tResource *= b.getQuantity();

  AttributePoints needMan = b.getManpower();
  AttributePoints needRes = b.getResource();

  ASSERT(tManpower >= needMan);
  ASSERT(tResource >= needRes);

  float manRatio = float(tManpower - needMan) / tManpower;
  float resRatio = float(tResource - needRes) / tResource;

  return minimum(manRatio, resRatio);
}

/*
 * Calculate estimated time for build item
 */

int TownAttributes::getTimeToGo(const BuildItem* builds, const BuildItem& b)
{
  /*
   * First, we need to calculate estimated time to completion
   * This needs to take other builds and available resources into account
   */

  ASSERT(b.getWhat() < d_campData->getMaxUnitType());
  const UnitTypeItem& ut = d_campData->getUnitType(b.getWhat());

  // get unmodified time in weeks. this is total needed / weekly
  AttributePoints wManpower = 0;
  AttributePoints wResource = 0;
  ut.getWeeklyResource(wManpower, wResource);
  wManpower = (b.getQuantity() * wManpower);
  wResource = (b.getQuantity() * wResource);

  if(wManpower > 0 && wResource > 0)
  {
    int mWeeks = ((b.getManpower() % wManpower)) ?
       (b.getManpower() / wManpower) + 1 : b.getManpower() / wManpower;

    int rWeeks = ((b.getResource() % wResource)) ?
       (b.getResource() / wResource) + 1 : b.getResource() / wResource;

    int timeToGo = maximum(1, maximum(mWeeks, rWeeks));

       ResourceSet wn;
       TownProcess::resourcesWeeklyNeed(d_campData, builds, wn);

        AttributePoints onHandManpower = province()->getManPower();
        AttributePoints onHandResource = province()->getResource();
        AttributePoints availManpower = availableManpower();
        AttributePoints availResource = availableResources();

       // if our need is greater than our resource
       if( ((wn.resource > availResource) || (wn.manPower > availManpower)) &&
          (wn.resource != 0) &&
          (wn.manPower != 0) &&
          (availManpower != 0) &&
          (availResource != 0) )
       {
          // find our number of weeks we can survive on the stockpile
          int wos = minimum(onHandResource / wn.resource, onHandManpower / wn.manPower);

          // if it is less than raw time we have to adjust
          if(wos < timeToGo)
          {
              // get % of timeToGo that we cannot get from stockpile
              int tDif = timeToGo - wos;
              int tPercent = MulDiv(tDif, 100, timeToGo);

              // get difference in percent of what we need, and our weekly allotment
              int rDif = wn.resource - availResource;
              int mDif = wn.manPower - availManpower;

              int wDif = maximum(rDif, mDif);
              int wDiv = (rDif >= mDif) ? availResource : availManpower;
              int wPercent = MulDiv(wDif, 100, wDiv);

              // get final percent we adjust timeToGo by
              int percent = MulDiv(tPercent, wPercent, 100);
              timeToGo += MulDiv(timeToGo, percent, 100);
          }
       }

        return timeToGo;
  }

  return 0;
}

namespace      // Private Namespace
{

/*
 *---------------------------------------------------------
 * Utility classes
 */

/*
 * class to manage resource strings
 */

class TownInfoString {
public:
  enum ID {
    // popup

    ControlledBy,
    Garrison,
    Supply,
    Victory,

    Source,
    Depot,
    BuildingDepot,
    Yes,
    No,
    None,

    Capital,

    ToCompletion,
    NotBuilding,

    NumberTowns,
    Manpower,
    Resources,
    VictoryPoints,

    FortLevel,
    Terrain,
//  Wks,

    Forage,
    InGarrison,
    Situation,
    BridgeCollapse,
    PoliticalPoints,
    SupplySource,
    Strength,

    // town sizes
    T_Capital,
    T_City,
    T_Town,
    T_Location,

    // town status
    TownStatusFirst,
    Unoccupied = TownStatusFirst,
    Occupied,
    Raided,
    Besieged,

    HowMany
  };
};

const int stringIDs[TownInfoString::HowMany] = {

   // Popup

   IDS_ControlledBy_S,
   IDS_Garrison,
   IDS_Supply,
   IDS_Victory,

   IDS_Source,
   IDS_Depot,
   IDS_BuildingDepot,

   IDS_Yes,
   IDS_No,
   IDS_None,

   IDS_Capital,

   IDS_n_WeeksToCompletion,
   IDS_BID_NOTBUILDING,

   IDS_NTowns,
   IDS_Manpower,
   IDS_Resources,
   IDS_VictoryPoints,

   IDS_FortLevel,
   IDS_Terrain,
//    IDS_PERCENT_complete,

   IDS_ForageValue,
   IDS_InGarrison,
   IDS_Situation,
   IDS_BridgeCollapsed,
   IDS_PoliticalPoints,
   IDS_SupplySource,
   IDS_TownStrength,

   // town sizes
   IDS_TOWNTYPE_CAPITAL,
   IDS_TOWNTYPE_CITY,
   IDS_TOWNTYPE_TOWN,
   IDS_TOWNTYPE_OTHER,

   // town status strings
   IDS_TI_UNOCCUPPIED,
   IDS_TI_OCCUPIED,
   IDS_TI_RAIDED,
   IDS_TI_BESIEGED,

};

static ResourceStrings s_strings(stringIDs, TownInfoString::HowMany);


};    // namespace

namespace      // private namespace
{

/*
 * Get Province Picture
 */


PicLibrary::Picture getPicture(const Province* province)
{
   const char* picName = province->picture();

   if( (picName == 0) || (picName[0] == 0) )
      return 0;

   SimpleString fileNameStr;
   fileNameStr << scenario->getProvincePicturePath() << "\\" <<   picName << ".BMP";

   bool exists = FileSystem::fileExists(fileNameStr.toStr());
   ASSERT(exists);
   if(!exists)
      return 0;

   return PicLibrary::get(fileNameStr.toStr());
}

static const char s_strColon[] = ": ";

/*
 * Display Text aligned against right edge with an added colon.
 */

void rightAlignText(HDC hdc, int x, int y, const char* text)
{
    x -= textWidth(hdc, s_strColon);
    wTextOut(hdc, x, y, s_strColon);

    x -= textWidth(hdc, text);
    wTextOut(hdc, x, y, text);
}


};    // private namespace


/*
 * Static Data
 */

/*
 * Constructor
 */

TownInfoData::TownInfoData(ITown town, const CampaignData* campData, DrawDIBDC* dib, const PixelRect& rect) :
   InfoDrawBase(dib, campData, rect),
   d_mainRect(rect),
    d_ta(new TownAttributes(town, campData))
{
   ASSERT(d_campData != 0);
   ASSERT(d_dRect.width() > 0);
   ASSERT(d_dRect.height() > 0);
   ASSERT(d_dib != 0);
}

TownInfoData::~TownInfoData()
{
    delete d_ta;
}


/*
 * Draw Header area
 */

void TownInfoData::drawNameDib(bool compact)
{
   /*
    * Calculate size of name Rect and adjust mainRect
    */

   ASSERT(d_mainRect.width() > 0);
   ASSERT(d_mainRect.height() > 0);

   int offsetX = ScreenBase::x(1);
   int offsetY = ScreenBase::y(1);
   int nameHeight = ScreenBase::y(2*8+2);

   PixelRect nameRect(d_mainRect.left() + offsetX, d_mainRect.top() + offsetY,
                      d_mainRect.right() - offsetX, d_mainRect.top() + offsetY + nameHeight);

   d_mainRect = PixelRect(nameRect.left(), nameRect.bottom() + offsetY,
                        nameRect.right(), d_mainRect.bottom() - offsetY);


   const char* townName = d_ta->town()->getName();
   const char* provName = d_ta->province()->getName();
   const char* strTown = 0;
   const char* strProv = 0;

   Nationality nation = d_ta->province()->getNationality();

   drawNames(nameRect, townName, provName, strTown, strProv, nation, compact);
}

/*
 * Draw Tracking DIB
 */

void TownInfoData::drawTrackingDIB()
{
   TextColorHolder textCol(d_dib->getDC(), Colours::Black);
   BkModeHolder bkHold(d_dib->getDC(), TRANSPARENT);

   drawNameDib(true);

   drawThickBorder(d_mainRect);
   shrinkRect(d_mainRect, s_borderThickness);

   enum {
      SectionControlled,
      SectionNationality,
      SectionGarrison,
      SectionSupply,
      SectionVictory,
      SectionCount
   };

   int fontHeight = d_mainRect.height() / SectionCount;
   fontHeight = minimum(fontHeight, ScreenBase::y(7));
   int margin = 1;

   LogFont lf;
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);

   Font nFont;
   nFont.set(lf);
   DCFontHolder fontHold(d_dib->getDC(), nFont);

   /*
    * get strings
    */

   const char* strGarrison = s_strings.get(TownInfoString::Garrison);
   const char* strSupply = s_strings.get(TownInfoString::Supply);
   const char* strVictory = s_strings.get(TownInfoString::Victory);

   /*
    * Get width of maximum string
    */

   int tw = textWidth(d_dib->getDC(), strGarrison);
   tw = maximum(tw, textWidth(d_dib->getDC(), strSupply));
   tw = maximum(tw, textWidth(d_dib->getDC(), strVictory));
   tw += textWidth(d_dib->getDC(), s_strColon);

   int x = d_mainRect.left() + margin;
   int xValue = x + tw;

   EvenSpacing yGen(SectionCount, d_mainRect.top(), d_mainRect.height(), fontHeight);

   /*
    * Nationality: e.g. "French City"
    */

   {
      int y = yGen.nextY();

      static int townTypeIndex[] = {
         TownInfoString::T_Capital,
         TownInfoString::T_City,
         TownInfoString::T_Town,
         TownInfoString::T_Location
      };

      Nationality nation = d_ta->province()->getNationality();
      const char* strNationality = scenario->getNationAdjective(nation);
      const char* strType = s_strings.get(townTypeIndex[d_ta->town()->getSize()]);

      wTextPrintf(d_dib->getDC(), x, y, "%s %s", strNationality, strType);
   }

   /*
    * Controlled by France
    */

   {
      int y = yGen.nextY();
      Side side = d_ta->town()->getSide();
      const char* strControlledBy = s_strings.get(TownInfoString::ControlledBy);
      const char* strSide = scenario->getSideName(side);
      wTextPrintf(d_dib->getDC(), x, y, strControlledBy, strSide);
   }

   /*
    * Garrison: None
    * Garrison: Yes
    */

   {
      int y = yGen.nextY();
      wTextPrintf(d_dib->getDC(), x, y, "%s%s", strGarrison, s_strColon);
      const char* strHow;
      if(d_ta->town()->isGarrisoned())
         strHow = s_strings.get(TownInfoString::Yes);
      else
         strHow = s_strings.get(TownInfoString::None);
      wTextOut(d_dib->getDC(), xValue, y, strHow);
   }

   /*
    * Supply: Source, Depot % or None
    */

   {
      int y = yGen.nextY();
      wTextPrintf(d_dib->getDC(), x, y, "%s%s", strSupply, s_strColon);

      if(d_ta->town()->getIsSupplySource())
      {
         const char* strSource = s_strings.get(TownInfoString::Source);
         int supplyLevel = d_ta->town()->getSupplyLevel();

         wTextPrintf(d_dib->getDC(), xValue, y, "%s %d",
            strSource, supplyLevel);
      }
      else if(d_ta->town()->getIsDepot())
      {
         const char* strDepot = s_strings.get(TownInfoString::Depot);
         wTextOut(d_dib->getDC(), xValue, y, strDepot);
#if 0
         int supplyLevel = d_ta->town()->getSupplyLevel();

         wTextPrintf(d_dib->getDC(), xValue, y, "%s %d",
            strDepot, supplyLevel);
#endif
      }
      else if(d_ta->town()->isBuildingDepot())
      {
         const char* strBuilding = s_strings.get(TownInfoString::BuildingDepot);
         wTextOut(d_dib->getDC(), xValue, y, strBuilding);
      }
      else
      {
         const char* strNone = s_strings.get(TownInfoString::None);
         wTextOut(d_dib->getDC(), xValue, y, strNone);
      }
   }

   /*
    * Victory Points
    */

   {
      int y = yGen.nextY();
      wTextPrintf(d_dib->getDC(), x, y, "%s%s", strVictory, s_strColon);

      int w = (d_mainRect.right() - xValue) / scenario->getNumSides();
      int x1 = xValue;

      for(Side s = 0; s < scenario->getNumSides(); ++s)
      {
         // Display tiny flag for side

         Nationality n = scenario->getDefaultNation(s);
         const ImageLibrary* imgLib = scenario->getNationFlag(n, true);

         const UWORD imgIndex = FI_Division;
         int flagW;
         int flagH;
         imgLib->getImageSize(imgIndex, flagW, flagH);

         int flagDestH = fontHeight;
         int flagDestW = MulDiv(flagW, flagDestH, flagH);

         imgLib->stretchBlit(d_dib, imgIndex, x1, y, flagDestW, flagDestH);
         int x2 = x1 + flagDestW + 1;

         int vp = d_ta->town()->getVictory(s);
         wTextPrintf(d_dib->getDC(), x2, y, "%d", vp);
         x1 += w;
      }
   }




}


void TownInfoData::drawSummaryDib()
{
    drawNameDib(false);
    drawThickBorder(d_mainRect);
    shrinkRect(d_mainRect, s_borderThickness);
    ASSERT(d_campData);
    ASSERT(d_dib);


    enum Sections
    {
        SectionControllingSide,
        SectionFortification,
        SectionTerrain,
        SectionSupply,
        SectionForage,
        SectionGarrison,

        NumSections
    };

    int fontHeight = ScreenBase::y(8);
    fontHeight = minimum(fontHeight, d_mainRect.height() / NumSections);

    EvenSpacing yGen(NumSections, d_mainRect.top(), d_mainRect.height(), fontHeight);

    LogFont lf;
    lf.height(fontHeight);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    lf.italic(false);
    lf.underline(false);

    Font font;
    font.set(lf);

    HFONT oldFont = d_dib->setFont(font);

    TownBuildImages buildImages;

    int xBorder = ScreenBase::x(1);

    const char* strFort = s_strings.get(TownInfoString::FortLevel);
    const char* strTerrain = s_strings.get(TownInfoString::Terrain);
    const char* strSupply = s_strings.get(TownInfoString::Supply);
    const char* strForage = s_strings.get(TownInfoString::Forage);
    const char* strGarrison = s_strings.get(TownInfoString::InGarrison);

    int titleWidth = textWidth(d_dib->getDC(), strFort);
    titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strTerrain));
    titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strSupply));
    titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strForage));
    titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strGarrison));
    titleWidth += textWidth(d_dib->getDC(), s_strColon);

    const LONG titleOffsetX = d_mainRect.left() + xBorder;
    const LONG titleRightX = titleOffsetX + titleWidth;

    const LONG infoOffsetX = titleRightX + xBorder;
    const LONG iconOffsetX = infoOffsetX + ScreenBase::x(45);

    /*
    * Put nation flag, town name and province
    */



    /*
    * Put Controlling side text
    */

    LONG y = yGen.nextY();

    wTextPrintf(d_dib->getDC(), titleOffsetX, y,
        s_strings.get(TownInfoString::ControlledBy),
        scenario->getSideName(d_ta->town()->getSide()));

    /*
    * Put Fortification level
    */

    lf.height(ScreenBase::y(7));
    lf.italic(false);
    font.set(lf);
    d_dib->setFont(font);

    y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strFort);

    if(d_ta->isSeen())
    {
        wTextPrintf(d_dib->getDC(), infoOffsetX, y, "%d", static_cast<int>(d_ta->town()->getFortifications()));

        /*
          * If forts are currently being constructed
          */

        if(d_ta->town()->isBuildingFort())
        {
            /*
             * Put under construction icon and completions time text
             */

            buildImages.blit(d_dib, TownBuildImages::UnderConstructionImage,
                titleOffsetX + iconOffsetX, y - ScreenBase::y(2));

            int w;
            int h;
            buildImages.getImageSize(TownBuildImages::UnderConstructionImage, w, h);

            // put it in a smaller text

            lf.height(ScreenBase::y(5));
            lf.face(scenario->fontName(Font_SmallBold));
            font.set(lf);

            HFONT oldFont = d_dib->setFont(font);

//            int weeksToComplete = d_ta->getFortCompletionTime() / WeeksToTicks(1);
            int weeksToComplete = d_ta->getFortCompletionWeeks();

            wTextPrintf(d_dib->getDC(), iconOffsetX + w + ScreenBase::x(5), y,
                InGameText::get(IDS_n_WeeksToCompletion),       /* "%d weeks to completion" */
                weeksToComplete);

            d_dib->setFont(oldFont);
        }
    }
    else
        wTextOut(d_dib->getDC(), infoOffsetX, y, "???");

    /*
    * Put Terrain
    */

    y = yGen.nextY();

    const TerrainTypeItem& item = d_campData->getTerrainType(d_ta->town()->getTerrain());

    rightAlignText(d_dib->getDC(), titleRightX, y, strTerrain);
    wTextOut(d_dib->getDC(), infoOffsetX, y, Terrain::groundTypeName(item.getGroundType()));

    /*
    * Put icon if this is a choke point
    */

    if(item.isChokePoint())
    {
        buildImages.blit(d_dib, TownBuildImages::BridgeImage, iconOffsetX,
            y - ScreenBase::y(4));
    }

    /*
    * Put supply level
    */

    y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strSupply);

    if(d_ta->isSeen())
    {
        if(d_ta->town()->getIsSupplySource())
        {
            /*
             * Put barrel icons to represent supply level
             * For now, each 55 points of supply = 1 barrel for a max of 5 barrels
             */

            int nBarrels = clipValue((d_ta->town()->getSupplyLevel()/(Attribute_Range/5))+1, 0, 5);

            int barrelX = infoOffsetX;
            int w;
            int h;
            buildImages.getImageSize(TownBuildImages::BarrelImage, w, h);

            w += ScreenBase::x(1);

            while(nBarrels--)
            {
                buildImages.blit(d_dib, TownBuildImages::BarrelImage, barrelX, y);
                barrelX += w;
            }
        }
        else if(d_ta->town()->getIsDepot())
            wTextOut(d_dib->getDC(), infoOffsetX, y, s_strings.get(TownInfoString::Depot));
        else if(d_ta->town()->isBuildingDepot())
        {
            /*
             * Put under construction icon and completions time text
             */

            buildImages.blit(d_dib, TownBuildImages::UnderConstructionImage,
                iconOffsetX, y - ScreenBase::y(2));

            int w;
            int h;
            buildImages.getImageSize(TownBuildImages::UnderConstructionImage, w, h);
            // put it in a smaller text
            lf.height(ScreenBase::y(5));
            lf.face(scenario->fontName(Font_SmallBold));
            font.set(lf);

            HFONT oldFont = d_dib->setFont(font);

//            int timeToComplete = d_ta->getDepotCompletionTime() / WeeksToTicks(1);
            int timeToComplete = d_ta->getDepotCompletionWeeks();

            wTextPrintf(d_dib->getDC(),
                iconOffsetX + w + ScreenBase::x(5),
                y,
                InGameText::get(IDS_n_WeeksToCompletion), /* "%d weeks to completion", */
                timeToComplete);

            d_dib->setFont(oldFont);
        }
    }
    else
        wTextOut(d_dib->getDC(), infoOffsetX, y, "???");

    /*
    * Put forage value
    */

    y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strForage);

    if(d_ta->getForage() > 0)
    {
        /*
          * Put sheave icons to represent forage level
          * For now, each 55 points of forage = 1 sheave for a max of 5 sheaves
          */

        int nSheaves = clipValue((d_ta->getForage()/(Attribute_Range/5)) + 1, 0, 5);

        int sheaveX = infoOffsetX;
        int w;
        int h;
        buildImages.getImageSize(TownBuildImages::CornImage, w, h);

        w += ScreenBase::x(1);

        while(nSheaves--)
        {
            buildImages.blit(d_dib, TownBuildImages::CornImage, sheaveX, y);
            sheaveX += w;
        }

    }
    else
        wTextOut(d_dib->getDC(), infoOffsetX, y, s_strings.get(TownInfoString::None));


    /*
    * Put Garrison size
    */

    y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strGarrison);

    if(d_ta->isSeen())
    {
        if(d_ta->getEffectiveGarrisonSize() > 0)
        {
            /*
             * For now I'll put one soldier icon for very 10 SPs in garrison
             * for a max of 10 icons
             */

            int nIcons = clipValue((d_ta->getEffectiveGarrisonSize()/10) + 1, 0, 10);
            int iconX = infoOffsetX;
            int w;
            int h;
            buildImages.getImageSize(TownBuildImages::ManpowerImage, w, h);

            w += ScreenBase::x(1);

            while(nIcons--)
            {

                buildImages.blit(d_dib, TownBuildImages::ManpowerImage, iconX, y);
                iconX += w;
            }
        }
        else
            wTextOut(d_dib->getDC(), infoOffsetX, y, s_strings.get(TownInfoString::None));
    }
    else
        wTextOut(d_dib->getDC(), infoOffsetX, y, "???");

    d_dib->setFont(oldFont);
}

void TownInfoData::drawBuildDib()
{
    drawNameDib(false);

    const int border = 1;

    int h = (d_mainRect.height() - border) / 4 - border;
    int w = d_mainRect.width() - border * 2;
    int x = d_mainRect.left() + border;
    int y = d_mainRect.top() + border;

    EvenSpacing yGen(BasicUnitType::HowMany, y, d_mainRect.height() - border, h);

    ColourIndex col = d_dib->getColour(Colours::Black);

    for(BasicUnitType::value basicType = static_cast<BasicUnitType::value>(0); basicType < BasicUnitType::HowMany; INCREMENT(basicType))
    {
        int newY = yGen.nextY();

        PixelRect rect(x, newY, x + w, newY + h);

        d_dib->frame(rect.left(), rect.top(), w, h, col);
        shrinkRect(rect, border);

        drawBuildTypeInfoDib(d_ta->builds(), basicType, true, rect);
    }
}

void TownInfoData::drawStatusDib()
{
   drawNameDib(false);
   drawThickBorder(d_mainRect);
   shrinkRect(d_mainRect, s_borderThickness);

  ASSERT(d_campData);
  ASSERT(d_dib);

    int fontHeight = ScreenBase::y(8);

    LogFont lf;
    lf.height(fontHeight);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    lf.underline(false);
    lf.italic(false);

    Font font;
    font.set(lf);

    HFONT oldFont = d_dib->setFont(font);

  TownBuildImages buildImages;

  int xBorder = ScreenBase::x(2);
  int yBorder = ScreenBase::y(2);

  const char* strGarrison = s_strings.get(TownInfoString::Garrison);
  const char* strSituation = s_strings.get(TownInfoString::Situation);

  int titleWidth = textWidth(d_dib->getDC(), strGarrison);
  titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strSituation));
  titleWidth += textWidth(d_dib->getDC(), s_strColon);

  const LONG titleRightX = d_mainRect.left() + xBorder + titleWidth;
  const LONG infoOffsetX = titleRightX + xBorder;
  const LONG iconOffsetX = infoOffsetX + ScreenBase::x(45);

  LONG y = d_mainRect.top() + yBorder;

  /*
   * Put nation flag, town name and province
   */

    int spacing = fontHeight + yBorder;


  /*
   * Put Garrison
   */

    rightAlignText(d_dib->getDC(), titleRightX, y, strGarrison);

  if(d_ta->getGarrisonSizeSP() > 0)
  {
    /*
     * For now I'll put one soldier icon for very 10 SPs in garrison
     * for a max of 10 icons
     */

    int nIcons = clipValue((d_ta->getGarrisonSizeSP() / 10) + 1, 0, 10);
    int iconX = infoOffsetX;
    int w;
    int h;
    buildImages.getImageSize(TownBuildImages::ManpowerImage, w, h);

     w += ScreenBase::x(1);

    while(nIcons--)
    {
      buildImages.blit(d_dib, TownBuildImages::ManpowerImage, iconX, y);
      iconX += w;
    }
  }
  else
    wTextOut(d_dib->getDC(), infoOffsetX, y, s_strings.get(TownInfoString::None));

  /*
   * Put Situation
   */

  y += spacing;

    rightAlignText(d_dib->getDC(), titleRightX, y, strSituation);

  TownInfoString::ID stringIndex = static_cast<TownInfoString::ID>(TownInfoString::TownStatusFirst+d_ta->getSituation());
  wTextOut(d_dib->getDC(), infoOffsetX, y, s_strings.get(stringIndex));

  if(d_ta->hasCatastrophe())
  {
    y += spacing;
    wTextOut(d_dib->getDC(), infoOffsetX, y, s_strings.get(TownInfoString::BridgeCollapse));
  }
}

void TownInfoData::drawTownDib()
{
    drawNameDib(false);
    drawThickBorder(d_mainRect);
    shrinkRect(d_mainRect, s_borderThickness);

    ASSERT(d_campData);
    ASSERT(d_dib);

    enum Sections
    {
        SectionVictory,
        SectionPolitical,
        SectionResource,
        SectionSupply,
        SectionStrength,
        SectionGarrison,
        SectionCount
    };

    int fontHeight = ScreenBase::y(8);
    fontHeight = minimum(fontHeight, d_mainRect.height() / SectionCount);

    LogFont lf;
    lf.height(fontHeight);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    lf.italic(true);
    lf.underline(false);
    lf.italic(false);

    Font font;
    font.set(lf);

    HFONT oldFont = d_dib->setFont(font);

    TownBuildImages buildImages;

    const char* strVictory   = s_strings.get(TownInfoString::VictoryPoints);
    const char* strPolitical = s_strings.get(TownInfoString::PoliticalPoints);
    const char* strResources = s_strings.get(TownInfoString::Resources);
    const char* strSupply    = s_strings.get(TownInfoString::SupplySource);
    const char* strStrength  = s_strings.get(TownInfoString::Strength);
    const char* strGarrison  = s_strings.get(TownInfoString::Garrison);

    int titleWidth = textWidth(d_dib->getDC(), strVictory);
    titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strPolitical));
    titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strResources));
    titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strSupply));
    titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strStrength));
    titleWidth = maximum(titleWidth, textWidth(d_dib->getDC(), strGarrison));
    titleWidth += textWidth(d_dib->getDC(), s_strColon);

    const LONG xBorder = ScreenBase::x(1);

    const LONG titleRightX = d_mainRect.left() + xBorder + titleWidth;

    const LONG infoOffsetX = titleRightX + xBorder;

    /*
    * Put nation flag, town name and province
    */



    EvenSpacing yGen(SectionCount, d_mainRect.top(), d_mainRect.height(), fontHeight);

    /*
    * Put Victory Points
    */

    LONG y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strVictory);

    // put side victory point
    int flagX  = infoOffsetX;

    for(Side s = 0; s < scenario->getNumSides(); s++)
    {
        const ImageLibrary* flags = scenario->getNationFlag(s, True);
        ASSERT(flags != 0);

        flags->blit(d_dib, FI_Army, flagX, y);

        int w;
        int h;
        flags->getImageSize(FI_Army, w, h);

        int x = flagX + w + xBorder;

        wTextPrintf(d_dib->getDC(), x, y, "%d",
            static_cast<int>(d_ta->victoryPoints(s)));

        flagX = x + ScreenBase::x(12);
    }

    /*
    * Put political points
    */

    y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strPolitical);

    wTextPrintf(d_dib->getDC(), infoOffsetX, y, "%d",
        static_cast<int>(d_ta->politicalPoints()));

    /*
    * Put Resources
    */

    y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strResources);

    // put manpower icon and rate
    int resourceX = infoOffsetX;

    int w;
    int h;
    buildImages.getImageSize(TownBuildImages::ManpowerImage, w, h);

    w = MulDiv(w, fontHeight, h);
    h = fontHeight;

    buildImages.stretchBlit(d_dib, TownBuildImages::ManpowerImage, resourceX, y, w, h);

    resourceX += ScreenBase::x(5) + w;
    wTextPrintf(d_dib->getDC(), resourceX, y, "%d",
        static_cast<int>(d_ta->manpowerRate()));

    // put resource icon and rate
    resourceX += ScreenBase::x(13);

    buildImages.getImageSize(TownBuildImages::ResourceImage, w, h);
    w = MulDiv(w, fontHeight, h);
    h = fontHeight;
    buildImages.stretchBlit(d_dib, TownBuildImages::ResourceImage, resourceX, y, w, h);

    resourceX += w + ScreenBase::x(5);
    wTextPrintf(d_dib->getDC(), resourceX, y, "%d",
        static_cast<int>(d_ta->resourceRate()));

    // put side flag for each side eligible to build here
    resourceX += ScreenBase::x(12);
    for(s = 0; s < scenario->getNumSides(); s++)
    {
        if((s == d_ta->getStartSide()) || (d_ta->isTransferable()))
        {
            const ImageLibrary* flags = scenario->getNationFlag(scenario->getDefaultNation(s), True);
            ASSERT(flags != 0);

            flags->blit(d_dib, FI_Army, resourceX, y);

            if(s == 0)
            {
                flags->getImageSize(FI_Army, w, h);
                resourceX += w + ScreenBase::x(5);
            }
        }
    }

    /*
    * Put Supply source info
    */

    y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strSupply);

    int supplyX = infoOffsetX;
    wTextPrintf(d_dib->getDC(), supplyX, y, "%d", static_cast<int>(d_ta->supplyRate()));


    supplyX += ScreenBase::x(15);
    if(d_ta->isSeen())
    {
        if(d_ta->town()->getIsSupplySource())
        {
            /*
             * Put barrel icons to represent supply level
             * For now, each 55 points of supply = 1 barrel for a max of 5 barrels
             */

            int nBarrels = clipValue((d_ta->getSupplyLevel()/(Attribute_Range/5))+1, 0, 8);

            buildImages.getImageSize(TownBuildImages::BarrelImage, w, h);

            w += ScreenBase::x(1);

            while(nBarrels--)
            {
                buildImages.blit(d_dib, TownBuildImages::BarrelImage, supplyX, y);
                supplyX += w;
            }
        }
        else if(d_ta->town()->getIsDepot())
            wTextOut(d_dib->getDC(), supplyX, y, s_strings.get(TownInfoString::Depot));
        else if(d_ta->town()->isBuildingDepot())
        {
            /*
             * Put under construction icon and completions time text
             */

            buildImages.blit(d_dib, TownBuildImages::UnderConstructionImage, supplyX + xBorder,
                y - ScreenBase::y(2));

            buildImages.getImageSize(TownBuildImages::UnderConstructionImage, w, h);
            // put it in a smaller text
            int fontHeight = ScreenBase::y(5);

            LogFont lf;
            lf.height(fontHeight);
            lf.weight(FW_MEDIUM);
            lf.face(scenario->fontName(Font_Bold));

            Font font;
            font.set(lf);

            HFONT oldFont = d_dib->setFont(font);

//            int timeToComplete = d_ta->getDepotCompletionTime() / WeeksToTicks(1);
            int timeToComplete = d_ta->getDepotCompletionWeeks();

            wTextPrintf(d_dib->getDC(), supplyX+w+fontHeight, y,
                InGameText::get(IDS_n_WeeksToCompletion),
                // "%d %s",
                timeToComplete);
#if 0
            wTextPrintf(d_dib->getDC(), supplyX+w+fontHeight, y, "%d %s",
                timeToComplete,
                s_strings.get(TownInfoString::Wks));
#endif

            d_dib->setFont(oldFont);
        }
        else
            wTextOut(d_dib->getDC(), supplyX, y, s_strings.get(TownInfoString::None));

    }
    else
        wTextOut(d_dib->getDC(), supplyX, y, "???");

    /*
    * Put Strength
    */

    y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strStrength);

    wTextPrintf(d_dib->getDC(), infoOffsetX, y, "%d", static_cast<int>(d_ta->strength()));

    /*
    * Put Garrison
    */

    y = yGen.nextY();

    rightAlignText(d_dib->getDC(), titleRightX, y, strGarrison);

    if(d_ta->isSeen())
    {
        if(d_ta->getMaxGarrisonSize() > 0)
        {
            /*
             * For now I'll put one soldier icon for very 10 SPs in garrison
             * for a max of 10 icons
             */

            int nIcons = clipValue((d_ta->getMaxGarrisonSize()/10)+1, 0, 10);
            int iconX = infoOffsetX;
            int w;
            int h;
            buildImages.getImageSize(TownBuildImages::AllowedInGarrisonImage, w, h);
            w = MulDiv(w, fontHeight, h);
            h = fontHeight;

            int iconSpacing = w + ScreenBase::x(1);

            while(nIcons--)
            {

                buildImages.stretchBlit(d_dib, TownBuildImages::AllowedInGarrisonImage, iconX, y, w, h);
                iconX += iconSpacing;
            }
        }
        else
            wTextOut(d_dib->getDC(), infoOffsetX, y, s_strings.get(TownInfoString::None));
    }
    else
        wTextOut(d_dib->getDC(), infoOffsetX, y, "???");
}

/*
 * Draw info tab about Province
 */

void TownInfoData::drawProvinceDib()
{
   drawNameDib(false);

   enum Sections
   {
      SectionCapital,
      SectionTownCount,
      SectionManPower,
      SectionResource,
      SectionVictory1,
      SectionVictory2,
      SectionControlled,
      SectionPolitical1,
      SectionPolitical2,
      SectionCount,

      Section1Count = SectionControlled,
      Section2Count = SectionCount-Section1Count
   };

   const int margin = 1;
   int fontHeight = (d_mainRect.height() - s_borderThickness * 4 - margin) / SectionCount;
   fontHeight = minimum(fontHeight, ScreenBase::y(7));

   int ySplit = d_mainRect.top() + MulDiv(d_mainRect.height(), Section1Count, SectionCount);

   PixelRect picRect = d_mainRect;
   PixelRect infoRect = d_mainRect;
   PixelRect controlRect = d_mainRect;

   infoRect.bottom(ySplit);
   picRect.bottom(ySplit);
   controlRect.top(ySplit + margin);

   PicLibrary::Picture picture = getPicture(d_ta->province());
   bool hasPic = (picture != 0);

   int picHeight = 0;
   int picWidth = 0;
   if(hasPic)
   {
      picHeight = picRect.height() - s_borderThickness * 2;
      picWidth = MulDiv(picture->getWidth(), picHeight, picture->getHeight());

      picRect.right(picRect.left() + picWidth + s_borderThickness * 2);
      infoRect.left(picRect.right() + margin);

      drawThickBorder(picRect);
      shrinkRect(picRect, s_borderThickness);
      DIB_Utility::stretchDIBNoMask(d_dib,
         picRect.left(), picRect.top(),
         picRect.width(), picRect.height(),
         picture,
         0, 0,
         picture->getWidth(), picture->getHeight());
   }


  ASSERT(d_campData);
  ASSERT(d_dib);

   LogFont lf;
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);
   lf.underline(false);

   Font nFont;
   nFont.set(lf);
   DCFontHolder fontHold(d_dib->getDC(), nFont);

   /*
    * Display Info Rectangle
    */

   {
      drawThickBorder(infoRect);
        shrinkRect(infoRect, s_borderThickness);

      EvenSpacing yGen(Section1Count, infoRect.top(), infoRect.height(), fontHeight);

        const char* strCapital = s_strings.get(TownInfoString::Capital);
        const char* strNTowns = s_strings.get(TownInfoString::NumberTowns);
        const char* strManpower = s_strings.get(TownInfoString::Manpower);
        const char* strResource = s_strings.get(TownInfoString::Resources);
        const char* strVictory = s_strings.get(TownInfoString::Victory);

        int titleOffsetX = infoRect.left() + margin;

        int tw = textWidth(d_dib->getDC(), strCapital);
        tw = maximum(tw, textWidth(d_dib->getDC(), strNTowns));
        tw = maximum(tw, textWidth(d_dib->getDC(), strManpower));
        tw = maximum(tw, textWidth(d_dib->getDC(), strResource));
        tw = maximum(tw, textWidth(d_dib->getDC(), strVictory));
        tw += textWidth(d_dib->getDC(), s_strColon);

        int infoOffsetX = titleOffsetX + tw;

        /*
        * Put Capital
        */


        int y = yGen.nextY();

        const Town& capitalTown = d_campData->getTown(d_ta->province()->getCapital());
        const char* capitalName = capitalTown.getNameNotNull();


        rightAlignText(d_dib->getDC(), infoOffsetX, y, strCapital);
        wTextOut(d_dib->getDC(), infoOffsetX, y, capitalName);

        /*
        * Put number of towns
        */

        y = yGen.nextY();

        int nTowns = d_ta->province()->getNTowns();

        rightAlignText(d_dib->getDC(), infoOffsetX, y, strNTowns);
        wTextPrintf(d_dib->getDC(), infoOffsetX, y, "%d", nTowns);

        /*
        * Put manpower. total/available
        */

        y = yGen.nextY();

        rightAlignText(d_dib->getDC(), infoOffsetX, y, strManpower);
        wTextPrintf(d_dib->getDC(), infoOffsetX, y, "%d/%d",
           static_cast<int>(d_ta->availableManpower()),
           static_cast<int>(d_ta->totalManpower()));

        /*
        * Put resources. total/available
        */

        y = yGen.nextY();



        rightAlignText(d_dib->getDC(), infoOffsetX, y, strResource);
        wTextPrintf(d_dib->getDC(), infoOffsetX, y, "%d/%d",
           static_cast<int>(d_ta->availableResources()),
           static_cast<int>(d_ta->totalResources()));

        y = yGen.nextY();

        /*
         * Victory
         */

        rightAlignText(d_dib->getDC(), infoOffsetX, y, strVictory);

        for(Side s = 0; s < scenario->getNumSides(); ++s)
        {
            if(s != 0)
                y = yGen.nextY();

           const ImageLibrary* flags = scenario->getNationFlag(scenario->getDefaultNation(s), True);

           // put side flag

         const UWORD imgIndex = FI_Division;
         int flagW;
         int flagH;
         flags->getImageSize(imgIndex, flagW, flagH);

         int flagDestH = fontHeight;
         int flagDestW = MulDiv(flagW, flagDestH, flagH);

           int itemX = infoOffsetX;

         flags->stretchBlit(d_dib, imgIndex, itemX, y, flagDestW, flagDestH);

           itemX += flagDestW + 1;

           // put victory. total/sideTotal

           wTextPrintf(d_dib->getDC(), itemX, y, "%d/%d",
              static_cast<int>(d_ta->controlledVictory(s)),
              static_cast<int>(d_ta->totalVictory(s)) );

        }
    }

    /*
    * Put political control
    */

    {
      drawThickBorder(controlRect);
        shrinkRect(controlRect, s_borderThickness);

      EvenSpacing yGen(Section2Count, controlRect.top(), controlRect.height(), fontHeight);

        // Controlled by
        int y = yGen.nextY();

        const char* strControlledBy = s_strings.get(TownInfoString::ControlledBy);
        Side controlSide = d_ta->province()->getSide();
      const char* strSide = scenario->getSideName(controlSide);

        int left = controlRect.left() + margin;
        int right = controlRect.right() - margin;

      wTextPrintf(d_dib->getDC(), left, y, strControlledBy, strSide);

        // bargraph
        y = yGen.nextY();

        // Flags

        ASSERT(scenario->getNumSides() == 2);

        int barLeft = 0;
        int barRight = 0;

        for(Side side = 0; side < scenario->getNumSides(); ++side)
        {
           const ImageLibrary* flags = scenario->getNationFlag(scenario->getDefaultNation(side), True);
           // put side flag

         const UWORD imgIndex = FI_Division;
         int flagW;
         int flagH;
         flags->getImageSize(imgIndex, flagW, flagH);

         int flagDestH = fontHeight;
         int flagDestW = MulDiv(flagW, flagDestH, flagH);

            int x;
            if(side == 0)
            {
                x = left;
                barLeft = x + flagDestW + 1;
            }
            else
            {
                x = right - flagDestW;
                barRight = x - 1;
            }

         flags->stretchBlit(d_dib, imgIndex, x, y, flagDestW, flagDestH);
        }

        int barWidth = barRight - barLeft;
        int barHeight = fontHeight;

        // Draw border

      ColourIndex col = d_dib->getColour(Colours::Black);

        d_dib->frame(barLeft, y, barWidth, barHeight, col);

        ++barLeft;
        --barRight;
        ++y;
        barHeight -= 2;
        ASSERT(barHeight > 0);

        int x1;
        int x2;

        ASSERT(d_ta->totalPolitical() != 0);
        if(d_ta->totalPolitical() == 0)
        {
            x1 = barLeft;
            x2 = barRight;
        }
        else
        {
            x1 = barLeft + MulDiv(d_ta->controlledPolitical(0), barWidth, d_ta->totalPolitical());
            x2 = barRight - MulDiv(d_ta->controlledPolitical(1), barWidth, d_ta->totalPolitical());
        }

        // Draw French side

        if(x1 != barLeft)
        {
            col = d_dib->getColour(scenario->getSideColour(0));
            d_dib->rect(barLeft, y, x1-barLeft, barHeight, col);
        }
        if(x1 != x2)
        {
            col = d_dib->getColour(scenario->getSideColour(SIDE_Neutral));
            d_dib->rect(x1, y, x2-x1, barHeight, col);
        }
        if(x2 != barRight)
        {
            col = d_dib->getColour(scenario->getSideColour(1));
            d_dib->rect(x2, y, barRight-x2, barHeight, col);
        }

        // percentages
        y = yGen.nextY();


        for(side = 0; side < scenario->getNumSides(); ++side)
        {
            int controlPercent = MulDiv(d_ta->controlledPolitical(side), 100, d_ta->totalPolitical());

            char buf[5];    // space for "100%"
            sprintf(buf, "%d%%", controlPercent);

            int x;
            if(side == 0)
                x = left;
            else
            {
                x = right - textWidth(d_dib->getDC(), buf);
            }

            wTextOut(d_dib->getDC(), x, y, buf);
        }
    }
}

/*
 * Draw Header of Build Dialog
 */

void TownInfoData::drawBuildInfoDib(const BuildItem* builds)
{
    ASSERT(d_campData != 0);

    /*
     * set font
     */

    int fontHeight = ScreenBase::y(9);
    fontHeight = minimum(d_mainRect.height() / 2, fontHeight);

    LogFont lf;
    lf.height(fontHeight);
    lf.weight(FW_MEDIUM);
    lf.italic(true);
    lf.underline(true);
    lf.face(scenario->fontName(Font_Bold));

    Font font;
    font.set(lf);

    HFONT oldFont = d_dib->setFont(font);
    ASSERT(oldFont);

    TownBuildImages buildImages;

    /*
     * Put Province capital
     */

    const int xBorder = ScreenBase::x(2);
    const int yBorder = ScreenBase::y(1);

    LONG x = d_mainRect.left() + xBorder;
    LONG y = d_mainRect.top() + yBorder;

    LONG xInfo = d_mainRect.left() + (d_mainRect.width() * 5 ) / 12;


    COLORREF color = scenario->getSideColour(d_ta->province()->getSide());
    COLORREF oldColor = d_dib->setTextColor(color);

    d_dib->setTextAlign(TA_TOP | TA_LEFT);

    const Town& cap = d_campData->getTown(d_ta->province()->getCapital());
    wTextPrintf(d_dib->getDC(), x, y, "%s: %s", s_strings.get(TownInfoString::Capital), cap.getNameNotNull());


    y += fontHeight + ScreenBase::y(1);

    // Put Province Map

    PicLibrary::Picture picture = getPicture(d_ta->province());
    bool hasPic = (picture != 0);

    if(hasPic)
    {
        PixelRect picRect;
        picRect.left(x);
        picRect.top(y);
        picRect.bottom(d_mainRect.height() - yBorder);
        picRect.right(xInfo - xBorder);

      DIB_Utility::stretchDIBNoMask(d_dib,
         picRect.left(), picRect.top(),
         picRect.width(), picRect.height(),
         picture,
         0, 0,
         picture->getWidth(), picture->getHeight());
    }
    else
    {
        // Display Province Name
        wTextPrintf(d_dib->getDC(), x, y, "%s", d_ta->province()->getName());
    }

    d_dib->setTextColor(PaletteColours::Black);

    /*
     * Put Manpower Icon and text
     */

    int itemHeight = d_mainRect.height() / 3;
    fontHeight = minimum(ScreenBase::y(8), itemHeight);
    lf.height(fontHeight);
    lf.italic(false);
    lf.underline(false);
    lf.face(scenario->fontName(Font_Bold));

    font.set(lf);
    d_dib->setFont(font);

   const char* str_ManPower = s_strings.get(TownInfoString::Manpower);
   const char* str_Resources = s_strings.get(TownInfoString::Resources);
    const char* str_Amount = InGameText::get(IDS_Amount);
    const char* str_Available = InGameText::get(IDS_Available);
    const char* str_Province = InGameText::get(IDS_Province);
    const char* str_Total = InGameText::get(IDS_Total);

    int mpLen = maximum(textWidth(d_dib->getDC(), str_ManPower),
                    textWidth(d_dib->getDC(), str_Resources));

    int xIcon = xInfo + mpLen;
    ASSERT(xIcon < d_mainRect.right());

    int itemY1 = d_mainRect.top() + itemHeight;
    int itemY2 = itemY1 + itemHeight;
    int itemMid1 = itemY1 + itemHeight / 2;
    int itemMid2 = itemY2 + itemHeight / 2;
    int itemT1 = itemMid1 - fontHeight / 2;
    int itemT2 = itemMid2 - fontHeight / 2;

    d_dib->setTextAlign(TA_RIGHT | TA_TOP);
    wTextOut(d_dib->getDC(), xIcon, itemT1, str_ManPower);
    wTextOut(d_dib->getDC(), xIcon, itemT2, str_Resources);

    xIcon += 2;

    /*
     * Icons
     */

    int w;
    int h;
    buildImages.getImageSize(TownBuildImages::ManpowerImage, w, h);

    LONG yIcon = itemMid1 - h / 2;

    buildImages.blit(d_dib, TownBuildImages::ManpowerImage, xIcon, yIcon);

    int iconWidth = w;

    buildImages.getImageSize(TownBuildImages::ResourceImage, w, h);
    yIcon = itemMid2 - h / 2;
    buildImages.blit(d_dib, TownBuildImages::ResourceImage, xIcon, yIcon);

    iconWidth = maximum(iconWidth, w);

    /*
     * Numbers
     */

    int xAvail = xIcon + iconWidth;
    int nWidth = (d_mainRect.right() - xBorder - xAvail) / 2;
    ASSERT(nWidth > 0);
    int xTotal = xAvail + nWidth;


    /*
     * Get Amount needed to build current items
     */

    ResourceSet total;
    total.manPower = d_ta->availableManpower();
    total.resource = d_ta->availableResources();

    ResourceSet needed;
    TownProcess::resourcesWeeklyNeed(d_campData, builds, needed);

    d_dib->setTextAlign(TA_CENTER | TA_TOP);

    // Display total

    wTextPrintf(d_dib->getDC(), xTotal + nWidth/2, itemT1, "%d", total.manPower);
    wTextPrintf(d_dib->getDC(), xTotal + nWidth/2, itemT2, "%d", total.resource);

    // Display available

    COLORREF inBlack = PaletteColours::Black;
    COLORREF inRed = PaletteColours::Red;

    AttributePoints avail;
    if(total.manPower > needed.manPower)
    {
        avail = total.manPower - needed.manPower;
        d_dib->setTextColor(inBlack);
    }
    else
    {
        avail = 0;
        d_dib->setTextColor(inRed);
    }
    wTextPrintf(d_dib->getDC(), xAvail + nWidth/2, itemT1, "%d", avail);

    if(total.resource > needed.resource)
    {
        avail = total.resource - needed.resource;
        d_dib->setTextColor(inBlack);
    }
    else
    {
        avail = 0;
        d_dib->setTextColor(inRed);
    }
    wTextPrintf(d_dib->getDC(), xAvail + nWidth/2, itemT2, "%d", avail);


    /*
     * Title of Resource stuff
     */

    d_dib->setTextColor(PaletteColours::Black);

    fontHeight = minimum(ScreenBase::y(6), itemHeight / 2);
    lf.height(fontHeight);
    lf.italic(false);
    lf.underline(false);
    lf.face(scenario->fontName(Font_Bold));

    font.set(lf);
    d_dib->setFont(font);

    y = itemY1;
    d_dib->hLine(xAvail, y, nWidth * 2, d_dib->getColour(PaletteColours::Black));

    d_dib->setTextAlign(TA_LEFT | TA_TOP);
    y -= fontHeight;
    wTextOut(d_dib->getDC(), xAvail, y, str_Available);
    wTextOut(d_dib->getDC(), xTotal, y, str_Total);

    y -= fontHeight;
    wTextOut(d_dib->getDC(), xAvail, y, str_Amount);
    wTextOut(d_dib->getDC(), xTotal, y, str_Province);


  d_dib->setTextColor(oldColor);
  d_dib->setFont(oldFont);
}


/*
 * Draw information about particular item being built
 */

void TownInfoData::drawBuildTypeInfoDib(const BuildItem* builds, BasicUnitType::value basicType, bool drawIcon)
{
    drawBuildTypeInfoDib(builds, basicType, drawIcon, d_mainRect);
}


void TownInfoData::drawBuildTypeInfoDib(const BuildItem* builds, BasicUnitType::value basicType, bool drawIcon, const PixelRect& rect)
{

    ASSERT(d_campData != 0);

    /*
    * Useful values
    */

    const BuildItem& b = builds[basicType];

    /*
     * Calculate Y values
     */

    const int border = 1;
    int midH = rect.height() / 2;
    int midY = rect.top() + midH;
    int maxFontHeight = midH - border;

    ASSERT(midH > 0);
    ASSERT(maxFontHeight > 0);

    /*
    * Set font
    */

    int fontHeight = ScreenBase::y(8);
    fontHeight = minimum(maxFontHeight, fontHeight);

    LogFont lf;
    lf.height(fontHeight);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));

    Font font;
    font.set(lf);

    DCFontHolder fontHold(d_dib->getDC(), font);

    /*
    * Put basic type image icon
     * Stretch to fit space but with a maximum size
    */

    LONG x = rect.left();
    LONG y = rect.top();
    LONG leftX = x;

    if(drawIcon)
    {

        TownBuildImages buildImages;

        TownBuildImages::Value index = buildImages.getUnitTypeIndex(basicType);

        int w;
        int h;
        buildImages.getImageSize(index, w, h);

        // Set width/height

        int typeHeight = rect.height();
        int typeWidth = MulDiv(typeHeight, w, h);

        // May need to fiddle with width/height to fit better

        ASSERT(typeHeight <= rect.height());

        x = rect.left();
        y = rect.top() + (rect.height() - typeHeight) / 2;

        buildImages.stretchBlit(d_dib, index, x, y, typeWidth, typeHeight);

        leftX = x + typeWidth;
    }

    UnitType unitType = b.getWhat();
    int nTotalBuilds = b.getQuantity();

    d_dib->setTextAlign(TA_TOP | TA_LEFT);

    if((unitType == NoUnitType) || (nTotalBuilds == 0))
    {
        x = leftX + border;
        y = rect.top() + (rect.height() - fontHeight) / 2;
       wTextOut(d_dib->getDC(), x, y, s_strings.get(TownInfoString::NotBuilding));
    }
    else
    {
       ASSERT(unitType < d_campData->getMaxUnitType());

       const UnitTypeItem& item = d_campData->getUnitType(unitType);

        /*
         * Calculate values
         */

       // get raw time to go
       int timeToGo = d_ta->getTimeToGo(builds, b);
        int buildPercent = 100 * d_ta->getBuildComplete(b);

       /*
        * Put number of builds and unit type description
        */

        x = leftX + border;
        y = rect.top() + border;

        if(drawIcon)
        {
            char buffer[10];
            wsprintf(buffer, "%d ", static_cast<int>(b.getQuantity()));

          wTextOut(d_dib->getDC(), x, y, buffer);

            x += textWidth(d_dib->getDC(), buffer);
        }

       wTextOut(d_dib->getDC(), x, y, item.getName());

       /*
        * Put time remaining for build
         *
         * This seems uneccesarily complex and should be part of the
         * resource logic rather than being here
        */

       int fontHeight = ScreenBase::y(8);
        fontHeight = minimum(fontHeight, maxFontHeight);

       lf.height(fontHeight);
       lf.face(scenario->fontName(Font_Bold));

        Font smallFont;
       smallFont.set(lf);
       d_dib->setFont(smallFont);

        x = leftX + border;
        y = midY + (midH - fontHeight) / 2;

        char buffer[100];
        wsprintf(buffer, s_strings.get(TownInfoString::ToCompletion), timeToGo);

        wTextOut(d_dib->getDC(), x, y, buffer);

        if(buildPercent != 0)
        {
            x += textWidth(d_dib->getDC(), buffer);

            sprintf(buffer, " (%d%%), ", buildPercent);
            wTextOut(d_dib->getDC(), x, y, buffer);
        }
    }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
