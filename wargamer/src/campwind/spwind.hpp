/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef SPWIND_HPP
#define SPWIND_HPP

#include "mytypes.h"

class CampaignData;
class CampaignWindowsInterface;
class RepoSP_Window;

class  RepoSP_Int {
    RepoSP_Window* d_rWindow;
  public:
    RepoSP_Int(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData);
    ~RepoSP_Int();

    void run();
    void hide();
    void destroy();
    Boolean initiated() const;
    HWND getHWND() const;
};

#endif



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
