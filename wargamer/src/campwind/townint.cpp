/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "townint.hpp"
//#include "towndial.hpp"
#include "campdint.hpp"
#include "grtypes.hpp"
#include "mapwind.hpp"
#include "mw_data.hpp"
#include "infowind.hpp"
#include "town.hpp"
#include "townmenu.hpp"
#include "tinfowin.hpp"
#include "tordrwin.hpp"

CampaignTownInterface::CampaignTownInterface(CampaignWindowsInterface* cwi, const CampaignData* campData, MapWindow* mapWindow) :
  d_campWind(cwi),
  d_campData(campData),
  d_mapWindow(mapWindow),
  d_orderWind(0),
  d_townInfoWind(0),
  d_menu(0),
  d_trackingDial(0),
  d_mode(TownInterfaceMode::Waiting),
  d_trackMode(MWTM_Town),
  d_overThisTown(NoTown),
  d_town(NoTown)
{
  ASSERT(d_campWind != 0);
  ASSERT(d_campData != 0);
  ASSERT(d_mapWindow != 0);

  d_trackingDial = new TrackingWindow(mapWindow->getHWND(), d_campData);
  ASSERT(d_trackingDial);

  // order window
  d_orderWind = new TownOrder_Int(this, d_mapWindow->getHWND(), d_campWind, d_campData);
  ASSERT(d_orderWind);

  // info window
  d_townInfoWind = new TownInfo_Int(this, d_mapWindow->getHWND(), d_campData);
  ASSERT(d_townInfoWind);

  // popup menu
  d_menu = new TownMenu_Int(this, mapWindow->getHWND(), d_campWind, d_campData);
  ASSERT(d_menu);
}

CampaignTownInterface::~CampaignTownInterface()
{
//  if(d_townDial)
//  d_townDial->destroy();

  if(d_trackingDial)
    delete d_trackingDial;

  if(d_orderWind)
    delete d_orderWind;

  if(d_townInfoWind)
    delete d_townInfoWind;

  if(d_menu)
    delete d_menu;
}

void CampaignTownInterface::onLClick(MapSelect& info)
{
  if(info.hasTown())
  {
    d_trackingDial->hide();
    d_overThisTown = NoTown;

    d_town = info.selectedTown;

    RECT r;
    GetWindowRect(d_orderWind->getHWND(), &r);
    PixelPoint p;
    calcDialPosition(d_mapWindow, r, info.mouseLocation, p);

    runOrders(p);

    d_mode = TownInterfaceMode::SettingUpBuild;
  }
}

void CampaignTownInterface::addObject(ITown town)
{
  if(town != NoTown)
  {
    d_town = town;
    d_mode = TownInterfaceMode::SettingUpBuild;
  }
}

void CampaignTownInterface::onRClick(MapSelect& info)
{
  if(info.selectedTown != NoTown)
  {
    d_trackingDial->hide();
    d_overThisTown = NoTown;

    PixelPoint p1;
    d_mapWindow->mapData().locationToPixel(info.mouseLocation, p1);

    POINT p = p1;;
    ClientToScreen(d_mapWindow->getHWND(), &p);

    p1.setX(p.x);
    p1.setY(p.y);

    d_town = info.selectedTown;
    runMenu(p1);
  }
}

void CampaignTownInterface::runMenu(const PixelPoint& p)
{
  if(d_town != NoTown)
    d_menu->run(d_town, p);
}

void CampaignTownInterface::cancelOrder()
{
  d_mode = TownInterfaceMode::Waiting;
}

void CampaignTownInterface::overObject(MapSelect& info)
{
  if(d_mode == TownInterfaceMode::Waiting)
  {
    ASSERT(d_trackingDial);
    if(info.selectedTown != NoTown && d_overThisTown != info.selectedTown)
    {
        d_overThisTown = info.selectedTown;

        const Town& t = d_campData->getTown(d_overThisTown);
        const Location& l = t.getLocation();

        PixelPoint p;
        calcTrackingDialPosition(d_mapWindow, l, p,
            d_trackingDial->width(), d_trackingDial->height());

        d_trackingDial->show(p, d_overThisTown);
    }
  }
}

void CampaignTownInterface::update()
{
  if(d_overThisTown == NoTown)
  {
    d_trackingDial->hide();
    d_overThisTown = NoTown;
  }
}

void CampaignTownInterface::notOverObject()
{
  ASSERT(d_trackingDial);
  d_trackingDial->hide();
  d_overThisTown = NoTown;
}

void CampaignTownInterface::runInfo(const PixelPoint& p)
{
  if(d_town != NoTown && d_townInfoWind)
  {
    d_townInfoWind->run(d_town, p);
  }
}

void CampaignTownInterface::runOrders(const PixelPoint& p)
{
  if(d_town != NoTown && d_orderWind)
  {
    d_orderWind->init(d_town);
    d_orderWind->run(p);
  }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
