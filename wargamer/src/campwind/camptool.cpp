/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Campaign Floating Tool bar
 *
 * Contains a set of buttons and an optional mini map.
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "camptool.hpp"
#include "app.hpp"
#include "palette.hpp"
#include "campint.hpp"
#include "scenario.hpp"
#include "res_str.h"       // Resource Strings
#include "winctrl.hpp"
#include "tooltip.hpp"
#include "options.hpp"
#include "mapwind.hpp"
#include "dib.hpp"
#include "button.hpp"
#include "scn_res.h"
#include "wmisc.hpp"
#include "registry.hpp"
#include "imglib.hpp"
#include "help.h"
#include "tinymap.hpp"
#include "palwind.hpp"
#include "cust_dlg.hpp"
#include "fonts.hpp"
#include "dib_util.hpp"
#include "scn_img.hpp"
#include "cwin_int.hpp"
#include "resstr.hpp"

#ifdef DEBUG
#include "msgenum.hpp"
#if !defined(EDITOR)
#include "logwin.hpp"
#endif
#endif

#ifdef DEBUG
#endif

const int tinyMapCX = 128;
const int tinyMapCY = 96;
const int ftWindowCY = 28;

class FTWindow : public WindowBaseND {
   MapWindow* d_mapWindow;

   const ImageLibrary* d_images;
   DrawDIBDC* d_dib;
   const DrawDIB* d_fillDib;

public:
   FTWindow(HWND parent, MapWindow* map);
   ~FTWindow()
   {
        selfDestruct();
     if(d_dib)
       delete d_dib;
   }

   void magnificationUpdated();
   void setupButtonStates();

private:
   LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
   void onDestroy(HWND hWnd);
   void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);
   void onPaint(HWND hWnd);
   void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
   LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
   void onMove(HWND hWnd, int x, int y);
   BOOL onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg);
   BOOL FTWindow::onEraseBk(HWND hwnd, HDC hdc);

#ifdef DEBUG
   void checkPalette() { }
#endif
};

/*
 * Define size of window
 */

const int SZ_H_CAMPTOOL = 24;      // was 64

FTWindow::FTWindow(HWND parent, MapWindow* map) :
  d_mapWindow(map),
  d_images(ScenarioImageLibrary::get(ScenarioImageLibrary::ZoomButtonImages)),
  d_dib(0),
  d_fillDib(scenario->getSideBkDIB(SIDE_Neutral))
{
#ifdef DEBUG
   debugLog("FTWindow::create\n");
#endif
   ASSERT(d_fillDib);
   ASSERT(d_images);

   HWND hWnd = createWindow(
      0,
      NULL,                   // Window Name
      WS_CHILD |
      WS_CLIPSIBLINGS,        /* Style */
      0, 0, 0, 0,    // Position will get set from campwind
      parent,                 /* parent window */
      NULL
   );


   ASSERT(hWnd != NULL);

}


LRESULT FTWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_FTWINDOW
   debugLog("FTWindow::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   LRESULT l;

   if(PaletteWindow::handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
      HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
      HANDLE_MSG(hWnd, WM_CONTEXTMENU, onContextMenu);
      HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
      HANDLE_MSG(hWnd, WM_SETCURSOR, onSetCursor);
   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}


/*
 *  Control ids can't start with 0 because Context help will not work if
 *  it does
 */

enum FT_ID {
   FTI_First = 1,
   FTI_ZOOMOUT = FTI_First,
   FTI_ZOOM1,
   FTI_ZOOM2,
   FTI_ZOOM3,
   FTI_ZOOM4,

   FTI_Last
};


// Tooltip & StatusWindow related arrays


static TipData tipData[] = {
   {  FTI_ZOOMOUT,         TTS_FTI_ZOOMOUT,       SWS_FTI_ZOOMOUT       },
   {  FTI_ZOOM1,           TTS_FTI_ZOOM1,         SWS_FTI_ZOOM1         },
   {  FTI_ZOOM2,           TTS_FTI_ZOOM2,         SWS_FTI_ZOOM2         },
   {  FTI_ZOOM3,           TTS_FTI_ZOOM3,         SWS_FTI_ZOOM3         },
   {  FTI_ZOOM4,           TTS_FTI_ZOOM4,         SWS_FTI_ZOOM4         },

   EndTipData
};

BOOL FTWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#ifdef DEBUG
   debugLog("FTWindow created (HWND=%p)\n", hWnd);
#endif

   /*
    * Add some buttons
    */

   initToolButton(APP::instance());


   /*
    * Zoom buttons
    */

   const int buttonCX = 21;
   const int buttonCY = 20;

   const int nButtons = FTI_Last - FTI_First;
   const int spacing = (tinyMapCX - (nButtons * buttonCX)) / (nButtons + 1);

   int x = spacing;
   const int y = (ftWindowCY - buttonCY) / 2;

   for(int id = FTI_First; id < FTI_Last; id++)
   {
     HWND hButton = addToolButton(hWnd, id, x, y, buttonCX, buttonCY);
     x += spacing + buttonCX;
   }

   /*
    * Setup the initial states
    */

   setupButtonStates();

#if !defined(OLD_TOOLTIP)
   g_toolTip.addTips(hWnd, tipData);
#endif

   return TRUE;
}

void FTWindow::onDestroy(HWND hWnd)
{
#ifdef DEBUG
   debugLog("FTWindow destroyed (HWND=%p)\n", hWnd);
#endif

#if !defined(OLD_TOOLTIP)
   g_toolTip.delTips(hWnd);
#endif
}

static DWORD ids[] = {
  FTI_ZOOMOUT,  IDH_ZoomOut,
  FTI_ZOOM1,    IDH_ZoomIn,
  FTI_ZOOM2,    IDH_ZoomIn,
  FTI_ZOOM3,    IDH_ZoomIn,
  FTI_ZOOM4,    IDH_ZoomIn,
  0, 0
};

void FTWindow::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
}

BOOL FTWindow::onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg)
{
   if(d_mapWindow->isZooming())
   {
      ASSERT(scenario->getZoomCursor() != NULL);
      SetCursor(scenario->getZoomCursor());
      return TRUE;
   }
   else
      return FORWARD_WM_SETCURSOR(hwnd, hwndCursor, codeHitTest, msg, defProc);
}

const int ButtonOnOffset = 5;
const int ButtonDisabledOffset = 10;

void FTWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem)
{
#ifdef DEBUG_FTWINDOW
   debugLog("FTWindow::onDrawItem(%d, 0x%04x %d)\n",
      (int) lpDrawItem->CtlID,
      (int) lpDrawItem->itemState,
      (int) lpDrawItem->itemAction);
#endif

   HPALETTE oldPalette = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
   RealizePalette(lpDrawItem->hDC);

   const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
   const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

   DIB_Utility::allocateDib(&d_dib, cx, cy);
   ASSERT(d_dib);
   d_dib->rect(0, 0, cx, cy, d_fillDib);

   UWORD image;

   if(lpDrawItem->itemState & (ODS_SELECTED | ODS_CHECKED))
     image = UWORD((lpDrawItem->CtlID-1) + ButtonOnOffset);
   else if(lpDrawItem->itemState & ODS_DISABLED)
     image = UWORD((lpDrawItem->CtlID-1) + ButtonDisabledOffset);
   else
     image = UWORD(lpDrawItem->CtlID-1);

   d_images->stretchBlit(d_dib, image, 0, 0, cx, cy);

   BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, cx, cy,
       d_dib->getDC(), 0, 0, SRCCOPY);

   SelectPalette(lpDrawItem->hDC, oldPalette, TRUE);
   RealizePalette(lpDrawItem->hDC);
}


BOOL FTWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  ASSERT(d_fillDib);

  RECT r;
  GetClientRect(hwnd, &r);

  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;

  DIB_Utility::allocateDib(&d_dib, cx, cy);
  ASSERT(d_dib);

  d_dib->rect(0, 0, cx, cy, d_fillDib);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  BitBlt(hdc, 0, 0, cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);

  return TRUE;
}

/*
 * Setup buttons as pushed or not, depending on current mode
 */

void setButton(HWND hwnd, int id, BOOL flag)
{
#ifdef DEBUG_FTWINDOW
   debugLog("setButton(%d, %d)\n", (int) id, (int) flag);
#endif

   HWND hButton = GetDlgItem(hwnd, id);
   ASSERT(hButton != NULL);
   SendMessage(hButton, BM_SETCHECK, flag ? BST_CHECKED : BST_UNCHECKED, 0);
   SendMessage(hButton, BM_SETSTATE, flag ? TRUE : FALSE, 0);
}

void FTWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
#ifdef DEBUG_FTWINDOW
   debugLog("FTWindow::onCommand(%p, %d, %p, %u)\n", hWnd, id, hwndCtl, codeNotify);
#endif

   if(codeNotify == BN_CLICKED)
   {
      switch(id)
      {

      case FTI_ZOOMOUT:
#ifdef DEBUG_FTWINDOW
         debugLog("FTWindow::zoomOut\n");
#endif
         d_mapWindow->initZoomIn();
         break;

      case FTI_ZOOM1:
      case FTI_ZOOM2:
      case FTI_ZOOM3:
      case FTI_ZOOM4:
         {
            MagEnum wantZoom = MagEnum(id - FTI_ZOOM1);

#ifdef DEBUG_FTWINDOW
            debugLog("FTWindow::zoomTo(%d)\n", (int) wantZoom);
#endif
            if(d_mapWindow->getZoomEnum() == wantZoom)
               d_mapWindow->initZoom(wantZoom);

            d_mapWindow->zoomTo(wantZoom);      // Immediate zoom to new level
            setButton(getHWND(), id, True);
         }
         break;
#ifdef DEBUG_FTWINDOW
      default:
         debugLog("FTWindow::onCommand(), unknown ID %d\n", id);
#endif
      }
   }
}



void enableButton(HWND hwnd, int id, BOOL flag)
{
   HWND hButton = GetDlgItem(hwnd, id);
   ASSERT(hButton != NULL);
   EnableWindow(hButton, flag);
}

void FTWindow::setupButtonStates()
{
   magnificationUpdated();
}

void FTWindow::magnificationUpdated()
{
   MagEnum magLevel = d_mapWindow->getZoomEnum();


   if(d_mapWindow->canZoomIn())
   {
      enableButton(getHWND(), FTI_ZOOMOUT, TRUE);
      setButton(getHWND(), FTI_ZOOMOUT, FALSE);
   }
   else
      enableButton(getHWND(), FTI_ZOOMOUT, FALSE);

   setButton(getHWND(), FTI_ZOOM1,        magLevel == 0);
   setButton(getHWND(), FTI_ZOOM2,        magLevel == 1);
   setButton(getHWND(), FTI_ZOOM3,        magLevel == 2);
   setButton(getHWND(), FTI_ZOOM4,        magLevel == 3);
}


/*-----------------------------------------------------------------------
 *              Combined tinymap and camptool
 */



class CompleteTinyMapWindow :
   public WindowBaseND, public PaletteWindow   //, public SuspendableWindow
{
  CampaignWindowsInterface* d_campWind;
  TinyMapWindow* d_tinyMap;
  FTWindow* d_ftWindow;
  MapWindow* d_mapWindow;

  Greenius_System::CustomDialog d_customDial;

public:
   CompleteTinyMapWindow(HWND parent, CampaignWindowsInterface* cw, MapWindow* map);
    ~CompleteTinyMapWindow();

   void areaChanged()
   {
     if(d_tinyMap)
       d_tinyMap->areaChanged();
   }                 // Tell TinyMap that zoom area has changed


   void draw(ConstICommandPosition cpi)
   {
     if(d_tinyMap)
       d_tinyMap->draw(cpi);
   }     // Thread function to do the actual draw

   void magnificationUpdated()
   {
     if(d_ftWindow)
       d_ftWindow->magnificationUpdated();
   }

   void setupButtonStates()
   {
     if(d_ftWindow)
       d_ftWindow->setupButtonStates();
   }

private:
   LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   /*
    * Message Handlers
    */

   void onDestroy(HWND hWnd);
   BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
   void onClose(HWND hWnd)
   {
      enable(false);
     d_campWind->tinyMapUpdated();
   }

};

static const char registryName[] = "\\tinyMap";
static WSAV_FLAGS s_saveFlags = WSAV_POSITION | WSAV_ENABLED;

CompleteTinyMapWindow::CompleteTinyMapWindow(HWND parent, CampaignWindowsInterface* cw, MapWindow* map) :
   d_campWind(cw),
   d_mapWindow(map),
   d_customDial(scenario->getBorderColors())
{
   ASSERT(map != 0);

   const int borderWidth = GetSystemMetrics(SM_CXBORDER);
   const int captionCY = GetSystemMetrics(SM_CYCAPTION);

   int cx = (tinyMapCX+(2*borderWidth));
   int cy = ((tinyMapCY+ftWindowCY)+(2*borderWidth)+captionCY);

   RECT r;
   // set up default position
   SetRect(&r, 400, 200, cx, cy);

   HWND hWnd = createWindow(
      0,
      "Overview Map",
         WS_POPUP |
         WS_CAPTION |
         WS_CLIPSIBLINGS,
      r.left,
      r.top,
      cx,
      cy,
      parent,
      NULL     // (HMENU) WID_TinyMap,
   );

   getState(registryName, s_saveFlags, true);

   ASSERT(hWnd != NULL);
}

CompleteTinyMapWindow::~CompleteTinyMapWindow()
{
    delete d_tinyMap;
    d_tinyMap = 0;
    delete d_ftWindow;
    d_ftWindow = 0;

    selfDestruct();
}


LRESULT CompleteTinyMapWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   LRESULT l;

   if(handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      HANDLE_MSG(hWnd, WM_CLOSE, onClose);
   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}


BOOL CompleteTinyMapWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
   /*
    * Set Caption Dat
    */

   // font
   int capCY = GetSystemMetrics(SM_CYCAPTION);
   const int fontHeight = capCY - 2;
   ASSERT(fontHeight >= 3);      // check for silly size?

   LogFont lf;
   lf.face(scenario->fontName(Font_Bold));
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.charset(Greenius_System::ANSI);     // otherwise we may get symbols!

   // other stuff
   d_customDial.setCaptionBkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::MenuBarBackground));
   d_customDial.setCaption(InGameText::get(IDS_Overview));
   d_customDial.init(hWnd);
   d_customDial.setStyle(Greenius_System::CustomDialog::Close);
   d_customDial.setCaptionFont(lf);

   d_tinyMap = new TinyMapWindow(hWnd, d_mapWindow);
   ASSERT(d_tinyMap);

   d_ftWindow = new FTWindow(hWnd, d_mapWindow);
   ASSERT(d_ftWindow);

   SetWindowPos(d_tinyMap->getHWND(), HWND_TOP, 0, 0, tinyMapCX, tinyMapCY, SWP_SHOWWINDOW);
   SetWindowPos(d_ftWindow->getHWND(), HWND_TOP, 0, tinyMapCY, tinyMapCX, ftWindowCY, SWP_SHOWWINDOW);

   return TRUE;
}


void CompleteTinyMapWindow::onDestroy(HWND hWnd)
{
  /*
   * Set registry
   */

  saveState(registryName, s_saveFlags);

}


/*----------------------------------------------------------------
 * Tinymap access class
 */

void TinyMap::create(HWND parent, CampaignWindowsInterface* cw, MapWindow* map)
{
  ASSERT(d_tinyMap == 0);

  d_tinyMap = new CompleteTinyMapWindow(parent, cw, map);
  ASSERT(d_tinyMap);
}

TinyMap::~TinyMap()
{
    delete d_tinyMap;
}

void TinyMap::areaChanged()
{
  if(d_tinyMap)
    d_tinyMap->areaChanged();
}

void TinyMap::draw(ConstICommandPosition cpi)
{
  if(d_tinyMap)
    d_tinyMap->draw(cpi);
}

void TinyMap::magnificationUpdated()
{
  if(d_tinyMap)
    d_tinyMap->magnificationUpdated();
}

HWND TinyMap::getHWND() const
{
    return d_tinyMap->getHWND();
}

bool TinyMap::isVisible() const
{
  return d_tinyMap->isVisible();
}

bool TinyMap::isEnabled() const
{
  return d_tinyMap->isEnabled();
}

void TinyMap::show(bool visible)
{
  d_tinyMap->show(visible);
}

void TinyMap::enable(bool enable)
{
  d_tinyMap->enable(enable);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
