/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "weathwin.hpp"
#include "weather.hpp"
#include "campdint.hpp"
#include "myassert.hpp"
#include "imglib.hpp"
#include "wmisc.hpp"
#include "scenario.hpp"
#include "app.hpp"
#include "scn_res.h"
#include "palette.hpp"
#include "winctrl.hpp"
#include "registry.hpp"
#include "cwin_int.hpp"
#include "dib.hpp"
#include "colours.hpp"
#include "tooltip.hpp"
#include "res_str.h"
#include "scn_img.hpp"

#include "palwind.hpp"

#include "cust_dlg.hpp"
#include "fonts.hpp"

class WeatherWindow : public WindowBaseND, public PaletteWindow
{
    const ImageLibrary* d_images;
    DrawDIBDC* d_bitmap;
    const CampaignData* d_campData;
    CampaignWindowsInterface* d_campWind;
    Greenius_System::CustomDialog d_customDial;

    bool    d_needUpdate;

    enum IDs {
       ID_Weather = 100,
       ID_GroundCondition
    };

    static const char registryName[];
    static WSAV_FLAGS s_saveFlags;

  public:
    WeatherWindow(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND parent);
    ~WeatherWindow();

    void update();

#ifdef DEBUG
    void checkPalette() {} // Force compiler to error if i havn't added a call to handlePalette
#endif


  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd);
    void onClose(HWND hWnd);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);
    void onPaint(HWND hwnd);
    LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);

};

const int nWeatherImages = 6;

const char WeatherWindow::registryName[] = "\\WeatherWindow";
WSAV_FLAGS WeatherWindow::s_saveFlags = WSAV_POSITION | WSAV_ENABLED;

WeatherWindow::WeatherWindow(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND parent) :
   d_images(ScenarioImageLibrary::get(ScenarioImageLibrary::WeatherWindowImages)),
   d_bitmap(0),
   d_campData(campData),
   d_campWind(campWind),
   d_customDial(scenario->getBorderColors()),
   d_needUpdate(true)
{
  ASSERT(parent != NULL);
  ASSERT(d_campData != 0);
  ASSERT(d_campWind != 0);


  /*
   *  Get Images
   */

  ASSERT(d_images != 0);

  const int imageCX = 100;
  const int imageCY = 67;

  d_bitmap = new DrawDIBDC(imageCX, imageCY);
  ASSERT(d_bitmap != 0);

  const int borderWidth = GetSystemMetrics(SM_CXSIZEFRAME);
  const int captionCY = GetSystemMetrics(SM_CYCAPTION);

  int cx = (imageCX+(2*borderWidth)) -1;
  int cy = (imageCY+(2*borderWidth)+captionCY) -1;

  RECT r;
  // set up default position
  SetRect(&r, 400, 200, cx, cy);

  HWND hWnd = createWindow(
      0,
        NULL,
      WS_POPUP | WS_CAPTION,
      r.left, r.top, cx, cy,
      parent, NULL
  );
  ASSERT(hWnd != NULL);

  getState(registryName, s_saveFlags, true);

  update();
}

WeatherWindow::~WeatherWindow()
{
    selfDestruct();

  if(d_bitmap)
    delete d_bitmap;
}

void WeatherWindow::onDestroy(HWND hWnd)
{
  /*
   * Set registry
   */

  saveState(registryName, s_saveFlags);

}

LRESULT WeatherWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   LRESULT result;

   if(handlePalette(hWnd, msg, wParam, lParam, result))
      return result;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      HANDLE_MSG(hWnd, WM_CLOSE,    onClose);

      default:
        return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL WeatherWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  // caption font
  int capCY = GetSystemMetrics(SM_CYCAPTION);
  const int fontHeight = capCY - 2;
  ASSERT(fontHeight >= 3);    // check for silly size?

  LogFont lf;
  lf.face(scenario->fontName(Font_Bold));
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.charset(Greenius_System::ANSI);      // otherwise we may get symbols!

  // other stuff
  d_customDial.setCaptionBkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::MenuBarBackground));
  d_customDial.init(hWnd);
  d_customDial.setStyle(Greenius_System::CustomDialog::Close);
  d_customDial.setCaptionFont(lf);

  /*
  * NB : from Jim - I changed the with & height to be +2 to eliminate the missing pixels at the right & bottom borders
  */

  ASSERT(d_bitmap);
  {
    HWND h = CreateWindow("STATIC", NULL, WS_CHILD | WS_VISIBLE | SS_OWNERDRAW,
        0,
        0,
        d_bitmap->getWidth(),
        d_bitmap->getHeight(),
        hWnd,
        (HMENU)ID_Weather,
        APP::instance(),
        NULL
    );

    ASSERT(h);
  }

  return TRUE;
}


void WeatherWindow::onClose(HWND hWnd)
{
    enable(false);
    d_campWind->weatherWindowUpdated();
}

const int GroundConditionsOffset = 3;

void WeatherWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem)
{
   if(d_needUpdate)
   {
      d_needUpdate = false;

      ASSERT(d_campData != 0);
      ASSERT(d_bitmap);

      const CampaignWeather& weather = d_campData->getWeather();

      UWORD wIndex = static_cast<UWORD>(weather.getWeather());
      UWORD gcIndex = static_cast<UWORD>(weather.getGroundConditions()+GroundConditionsOffset);

      d_images->blit(d_bitmap, wIndex, 0, 0);
      d_images->blit(d_bitmap, gcIndex, 0, 22);

      d_customDial.setCaption(CampaignWeather::getWeatherText(weather.getWeather()));
      SendMessage(getHWND(), WM_NCPAINT, 0, 0);
   }

   HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
   RealizePalette(lpDrawItem->hDC);

   ASSERT(d_bitmap);
   if(d_bitmap)
   {
      BitBlt(lpDrawItem->hDC, 0, 0, d_bitmap->getWidth(), d_bitmap->getHeight(),
         d_bitmap->getDC(), 0, 0, SRCCOPY);
   }

   SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

void WeatherWindow::update()
{
   d_needUpdate = true;
  InvalidateRect(getHWND(), NULL, FALSE);
}


/*----------------------------------------------------------------
 *  CampaignWeatherWindow implementation
 *  This is our access to weather window
 */

CampaignWeatherWindow::CampaignWeatherWindow(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND parent) :
  d_weatherWindow(0)
{
  create(campWind, campData, parent);
  ASSERT(d_weatherWindow);
}

CampaignWeatherWindow::~CampaignWeatherWindow()
{
    delete d_weatherWindow;
}

void CampaignWeatherWindow::create(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND parent)
{
  d_weatherWindow = new WeatherWindow(campWind, campData, parent);
  ASSERT(d_weatherWindow);
}

void CampaignWeatherWindow::update()
{
  if(d_weatherWindow)
    d_weatherWindow->update();
}

HWND CampaignWeatherWindow::getHWND() const
{
    return d_weatherWindow->getHWND();
}

bool CampaignWeatherWindow::isVisible() const
{
  return d_weatherWindow->isVisible();
}

bool CampaignWeatherWindow::isEnabled() const
{
  return d_weatherWindow->isEnabled();
}

void CampaignWeatherWindow::show(bool visible)
{
  d_weatherWindow->show(visible);
}

void CampaignWeatherWindow::enable(bool enable)
{
  d_weatherWindow->enable(enable);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
