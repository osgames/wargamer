/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef UORDRWIN_HPP
#define UORDRWIN_HPP

#include "cpidef.hpp"
#include "wind.hpp"

class CampaignOrder;
class StackedUnitList;
class UnitOrderWindow;
class PixelPoint;
class CampaignData;
class CampaignWindowsInterface;
class CampaignUserInterface;

class UnitOrder_Int : public Window
{
	 UnitOrderWindow* d_orderWind;
  public:
	 enum Mode {
		 MoveOrder = 0x01,
		 FullOrder = 0x02
	 };

	 UnitOrder_Int(CampaignUserInterface* owner, HWND hParent,
		  CampaignWindowsInterface* campWind, const CampaignData* campData, CampaignOrder& order);
	 ~UnitOrder_Int();

    void init(StackedUnitList& units, int id);
	 void run(const PixelPoint& p, UBYTE flags);
	 // void hide();
	 // void destroy();
	 ConstICommandPosition currentUnit();
	 // HWND getHWND() const;
	 int width() const;
	 int fullHeight() const;
	 int moveHeight() const;

	    virtual HWND getHWND() const;
        virtual bool isVisible() const;
        virtual bool isEnabled() const;
        virtual void show(bool visible);
        virtual void enable(bool enable);

};

#endif
