/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef UINFOWIN_HPP
#define UINFOWIN_HPP

//#include "cpidef.hpp"
#include "grtypes.hpp"
#include "compos.hpp"

class UnitInfoWindow;
class CampaignUserInterface;

/*
 * Interface to unit-information window
 */

class UnitInfo_Int {
	 UnitInfoWindow* d_infoWind;
  public:
	 UnitInfo_Int(CampaignUserInterface* owner, HWND hParent, const CampaignData* campData);
	 ~UnitInfo_Int();

	 void run(const ConstICommandPosition& cpi, const PixelPoint& p);
    void destroy();
};

#endif
