/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef INFOBASE_HPP
#define INFOBASE_HPP

#ifndef __cplusplus
#error infobase.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Base class for Unit and Town Information
 *
 *----------------------------------------------------------------------
 */

#include "grtypes.hpp"
#include "gamedefs.hpp"

class DrawDIBDC;
class CampaignData;

class InfoDrawBase
{
	public:
		InfoDrawBase(DrawDIBDC* dib, const CampaignData* campData, const PixelRect& rect) :
			d_dib(dib),
			d_campData(campData),
			d_dRect(rect)
		{
			ASSERT(dib != 0);
			ASSERT(campData != 0);
			ASSERT(rect.width() > 0);
			ASSERT(rect.height() > 0);
		}

		virtual ~InfoDrawBase() = 0;		// Prevent stand-alone instantiation

		void drawThickBorder(const PixelRect& r);
		static void shrinkRect(PixelRect& r, int border);

		void drawNames(const PixelRect& r, const char* name1, const char* name2, const char* title1, const char* title2, Nationality nation, bool compact);

	public:
		const CampaignData* d_campData;
		DrawDIBDC* d_dib;			// Where to draw to
		PixelRect d_dRect;		// display area of DIB

		static const int s_borderThickness;
};

inline InfoDrawBase::~InfoDrawBase() { }

#endif /* INFOBASE_HPP */

