/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef TORDRWIN_HPP
#define TORDRWIN_HPP

#include "gamedefs.hpp"

class TownOrderWindow;
class PixelPoint;
class CampaignData;
class CampaignWindowsInterface;
class CampaignUserInterface;

class TownOrder_Int {
    TownOrderWindow* d_orderWind;
  public:
    TownOrder_Int(CampaignUserInterface* owner, HWND hParent,
        CampaignWindowsInterface* campWind, const CampaignData* campData);
    ~TownOrder_Int();

    void init(ITown town);
    void run(const PixelPoint& p);
    void hide();
    void destroy();
    HWND getHWND() const;
    int width() const;
    int height() const;
};



#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
