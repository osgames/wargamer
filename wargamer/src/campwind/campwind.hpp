/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPWIND_HPP
#define CAMPWIND_HPP

#ifndef __cplusplus
#error campwind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Windows
 *
 *----------------------------------------------------------------------
 */

#include "cwdll.h"
#include "cwin_int.hpp"       // Campaign Window Interface
#include "winerror.hpp"
#include "gamedefs.hpp"
#include "mw_user.hpp"
#include <windows.h>
#include "plyrdlg.hpp"
#ifdef DEBUG
         #include "c_cond.hpp"
#endif
#include "victwind.hpp"

/*-------------------------------------------------------------------
 * Forward References
 */

class MainCampaignWindow;
class ToolBar;
#if !defined(EDITOR)
class GameStatus_Int;
#endif
class MapWindow;
class OBWindow;
//class ClockWindow;
class PlayerOptionsDial;
class FindItem;
class WindowsLogWindow;
class OrderTB;
class CampaignData;           // Obsolete
class RealCampaignData;       // Obsolete
class CampaignInterface;
class CampaignUserInterface;
class CampaignWeatherWindow;
class TinyMap;
class CampaignToolBox;
class MessageWindow;
class CampaignMessageInfo;
class ReorgLeader_Int;
class NationStatus_Int;
class RepoSP_Int;
#ifdef CUSTOMIZE
class MapWindowEditors;
#endif
#ifdef DEBUG
class PalWindow;
#endif

/*
 * Campaign Windows
 */

class CampaignWindows :
   public CampaignWindowsInterface,    // Implements this
#if !defined(EDITOR)
    public PlayerSettingsContainer,
    private CampaignVictoryUser,
#endif
   public MapWindowUser                // Implements this
{
   private:
//       enum {              // enum for OrderTB array
//          UnitOrder,
//          TownOrder,
//          OrderTB_HowMany
//       };

      CampaignInterface* d_campaignGame;
      MainCampaignWindow* d_mainWind;           // Parent window
      ToolBar* d_toolbar;
#if !defined(EDITOR)
      GameStatus_Int* d_statWind;
      // StatusWindow* d_statWind;
#endif
      MapWindow*     d_mapWind;
      TinyMap* d_tinyMap;
      OBWindow*      d_obWind;
      // ClockWindow* d_cClockWind;
#ifdef DEBUG
      PalWindow*     d_palWind;
#endif
#ifdef CUSTOMIZE
      MapWindowEditors* d_mapEdit;
#endif
#if !defined(EDITOR)
      NationStatus_Int* d_nsWind;
      // CampaignUserInterface* d_userInterface[OrderTB_HowMany];
      // CampaignUserInterface* d_currentUser;
      CampaignUserInterface* d_ui;     // User Interface

      CampaignWeatherWindow* d_weatherWindow;
      MessageWindow* d_messageWindow;
      ReorgLeader_Int* d_iLeaderWind;
      RepoSP_Int* d_spWind;

      CampaignVictoryWindow* d_victoryWindow;
#endif   // !EDITOR


      PlayerOptionsDial*   d_poDial;

      FindItem*            d_findItem;

#ifdef DEBUG
      bool d_suspended;
#endif

      // Unimplemented Functions declared Private
      CampaignWindows(const CampaignWindows& campWind);
      const CampaignWindows& operator = (const CampaignWindows& campWind);

   public:
      CAMPWIND_DLL CampaignWindows(CampaignInterface* game, HWND hMain);
      CAMPWIND_DLL ~CampaignWindows();

      /*
       * Virtual Functions for CampaignWindowsInterface
       */

      // void windowClosed(HWND hwnd);
      bool windowDestroyed(HWND hwnd, int id);
         // Let CampaignWindows know that a window has been destroyed

      bool processCommand(HWND hWnd, int id);
         // Process a Menu Item Command

      void showHint(int id);
         // Display Hint Line about menu item

      void positionWindows();
         // Position all windows within the main window

      bool onClose(HWND hwnd);
         // Do actions on closing window
         // Return True if window is really closing, False to continue

      void editOptions(int page);
         // Bring up Player Options Dialog

#if !defined(EDITOR)
//      void orderTown(ITown itown);
//       void orderUnit(ICommandPosition cpi);
         // Set up Order Window for specified Town or Unit

//      void runOrderMenu(const PixelPoint& p, ConstParamCP cp);
      void orderUnit(ConstParamCP cp);
      void orderTown(ITown itown);

      int getNumCampaignMessages();
      int getCampaignMessagesRead();
#endif

      CAMPWIND_DLL void suspend(bool visible);     // Hide Campaign Windows while battle game is playing

      CAMPWIND_DLL void showWindows();
      void updateInfo() {}

      void checkMenu(int id, bool flag);
      void enableMenu(int id, bool flag);
         // Update Pull Down Menu and Tool bar states

      /*
       * Implementation of MapWindowUser
       */

      void onLClick(MapSelect& info);
         // Left button clicked
      void updateZoom();
         // Zoom Area has changed
         // update map arrows when zooming
      void overObject(MapSelect& info);
      void notOverObject();
         // Mouse is over a town
         // Mouse is over a unit
      void onDraw(DrawDIBDC* dib, MapSelect& info);
         // Map is being drawn into dib
         // User can do any drawing it wants to do
      void mapRedrawn();
         // Map Window has been redrawn...
         // Update tiny map Window
      void onButtonDown(MapSelect& info);
         // Left button is pressed
      bool onStartDrag(MapSelect& info);
         // Start dragging
      void onEndDrag(MapSelect& info);
         // Stop dragging
      void onDrag(MapSelect& info);
         // Mouse has moved while dragging
      void onRClick(MapSelect& info);
         // Right Button clicked
      TrackMode getTrack();
         // Return how to do tracking

#ifdef CUSTOMIZE
      void editObject(const MapSelect& info, const PixelPoint& p);
         // Edit object under mouse
#endif

      /*
       * Other Functions
       */

      void create(HWND hMain);
      void destroy();
      void removeGameWindows();

      MainCampaignWindow* getMainWindow() { return d_mainWind; }

      void redrawMap();
      CAMPWIND_DLL void rebuildMap();

      /*
       * toggle functions
       */

   private:

      void loadPalette();
      void toggleTinyMap();
#if !defined(EDITOR)
      // void toggleOrderWindows();
      void toggleCampaignMode();
      void toggleMessageWindow();
      void toggleWeatherWindow();
      void toggleShowOrders();
      void toggleReorgLeaders();
      void toggleRepoSP();
#endif
      void togglePlayerOptionsDial();
#ifdef DEBUG
      void togglePaletteWindow();
#endif
      void toggleOrderBattle();
      void toggleFindLeader();
      void toggleFindTown();

public:

      /*
       * Update windows
       */

      void msgWinUpdated();
      void magnificationUpdated();
      CAMPWIND_DLL void timeUpdated();
#if !defined(EDITOR)
      void updateWeather();
#endif

      /*
       * Update menu / toolbar functions
       */

      void tinyMapUpdated();

#if !defined(EDITOR)
      // void campaignModeUpdated();
      void weatherWindowUpdated();
      void messageWindowUpdated();
      void reorgLeaderUpdated();
      void showOrdersUpdated();
      void repoSPUpdated();

      void moraleBarUpdated();
#endif   // !EDITOR
      void findItemUpdated();
      void obWindowUpdated();


#if !defined(EDITOR)
       CAMPWIND_DLL void runVictoryScreen();
       void endVictoryScreen();
#endif   // !EDITOR

      void updateGridMenu();        // Update Grid Icons

      void updateAll();             // Update all Icons and menus

#if !defined(EDITOR)
      void makeOrderWindows();
      void createCampaignBattleWindows();
      void destroyCampaignBattleWindows();
//      void toggleOrderWindow();
      CAMPWIND_DLL void addMessage(const CampaignMessageInfo& msg);
#endif   // !EDITOR

#if defined(CUSTOMIZE)
      CAMPWIND_DLL void invalidateWindows();    // Make sure no window references a unit or town
#endif

      const CampaignData* getCampaignData() const;
#ifdef CUSTOMIZE
      CampaignData* getCampaignData();
#endif

#ifdef CUSTOMIZE   // used by Data Sanity Check routines
      void runOBEdit(ICommandPosition cpi);
      void runTownEdit(ITown iTown);
      void runProvinceEdit(IProvince iProv);
      void runConnectionEdit(ITown iTown);
      void runConditionEdit(WG_CampaignConditions::Conditions* condition);
      CAMPWIND_DLL void runDataSanityCheck();
#endif

      void centerOnMap(const Location& l, UBYTE magLevel);
      // void centerMapOnUnit(ConstICommandPosition cpi, const Location& l, UBYTE magLevel);
      // void centerMapOnTown(ITown iTown, const Location& l, UBYTE magLevel);
      void centerMapOnUnit(ConstICommandPosition cpi);
      void centerMapOnTown(ITown iTown);

#if !defined(EDITOR)
        /*
         * PlayerSettings Implementation
         */

        virtual void setController(Side side, GamePlayerControl::Controller control);
        virtual HWND getHWND() const;
#endif

      class CampaignWindowError : public WinError
      {
         public: CampaignWindowError() : WinError("Error in Campaign Windows") { }
      };
};

/*
 * Windows that are children of a campaign should be derived from CampaignWindow
 */

class CampaignWindow {
public:
   CampaignWindows* campWind;
protected:
   CampaignWindow(CampaignWindows* owner) { campWind = owner; }

   // CampaignData* getCampaignData() const { return campWind->getCampaignData(); }
   // RealCampaignData* getRealCampaignData() const { return campWind->getRealCampaignData(); }
   const CampaignData* getCampaignData() const;
   // RealCampaignData* getRealCampaignData();
};

// extern CampaignWindows* campWind;

#endif /* CAMPWIND_HPP */

