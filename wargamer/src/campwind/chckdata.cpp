/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/

#include "stdinc.hpp"
#ifdef CUSTOMIZE
#include "chckdata.hpp"
#include "campdint.hpp"
#include "campint.hpp"
#include "cwin_int.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "scenario.hpp"
#include "resdef.h"
#include "misc.hpp"
#ifdef DEBUG
#include "clog.hpp"
#endif
#include "dialog.hpp"
#include "app.hpp"
#include "winfile.hpp"  // for fileExists()
#include "c_cond.hpp"
#include <stdarg.h>
#include <stdio.h>

#ifdef DEBUG
static LogFileFlush dsLog("DataSanity.log");
#endif

class DataCheckDial : public ModalDialog {
    char* d_dialogText;
  public:

    DataCheckDial(const char* text) : d_dialogText(copyString(text)) {}
    ~DataCheckDial() { delete[] d_dialogText; }

   int run(HWND hParent);
private:
   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
   void onClose(HWND hwnd);
   void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

};

class DataCheck {
    CampaignData* d_campData;
    CampaignWindowsInterface* d_campWindows;
    CString d_dialogText;
    Boolean d_shouldRunDial;
  public:
    DataCheck(CampaignData* campData, CampaignWindowsInterface* cwi) :
      d_campData(campData),
      d_campWindows(cwi),
      d_shouldRunDial(True)
    {}

    ~DataCheck() {}

   Boolean run();
private:

   void setDialogText(const char* format, ...);

   Boolean runDial(Boolean& shouldFix);

   Boolean hasOnlyOneSHQ(Side side);
   Boolean allTownsConnected();
   Boolean staffRatingsExceeded();
   Boolean offScreenIsOffScreen();
   Boolean nationRulesEnforced();
   Boolean allUnitsHaveLeaders();
   Boolean properUnitTypes();
   Boolean allChokePointsValid();
   Boolean properConditions();
   void removeTrailingSpaces();
};

BOOL DataCheckDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_DLG_MSG(WM_INITDIALOG, onInitDialog);
      HANDLE_DLG_MSG(WM_DESTROY, onDestroy);
      HANDLE_DLG_MSG(WM_COMMAND, onCommand);
      HANDLE_DLG_MSG(WM_CLOSE, onClose);
      default:
         return FALSE;
   }
   return TRUE;
}

BOOL DataCheckDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  ASSERT(d_dialogText != 0);

  SetDlgItemText(hwnd, DCD_TEXT, d_dialogText);
  return TRUE;
}

void DataCheckDial::onDestroy(HWND hwnd)
{

}

void DataCheckDial::onClose(HWND hwnd)
{

}

void DataCheckDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case DCD_IGNORE:
    case DCD_IGNOREALL:
    case DCD_FIX:
    case DCD_CANCEL:
      EndDialog(hwnd, id);
      break;
  }
}

int DataCheckDial::run(HWND hParent)
{
  return createDialog(dataCheckDialog, hParent);
}

Boolean DataCheck::run()
{
  ASSERT(d_campData != 0);
  ASSERT(d_campWindows != 0);

#ifdef DEBUG
  dsLog.printf("=========== Beginning Data Sanity Check ================");
#endif

  if(!properConditions())
    return False;
     
  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    if(!hasOnlyOneSHQ(s))
      return False;
  }

  if(!allTownsConnected())
    return False;

  if(!staffRatingsExceeded())
    return False;

  if(!offScreenIsOffScreen())
    return False;

  if(!properUnitTypes())
    return False;

  if(!allUnitsHaveLeaders())
    return False;

  if(!nationRulesEnforced())
    return False;

  if(!allChokePointsValid())
    return False;    

  removeTrailingSpaces();

#ifdef DEBUG
  dsLog.printf("=========== Ending Data Sanity Check ================\n");
#endif

  return True;
}

/*
 * Run the dialog. Returns False if Data Check is canceled
 *
 */

Boolean DataCheck::runDial(Boolean& shouldFix)
{
  ASSERT(d_dialogText != 0);

  if(d_shouldRunDial)
  {

    DataCheckDial dial(d_dialogText);

    int result = dial.run(APP::getMainHWND());
    ASSERT(result > DCD_FIRST);
    ASSERT(result < DCD_LAST);

    d_shouldRunDial = ( (result == DCD_IGNORE) || (result == DCD_FIX) );
    shouldFix = ( result == DCD_FIX );
    return (result != DCD_CANCEL);
  }
  else
  {
    shouldFix = false;
    return true;
  }
}

void DataCheck::setDialogText(const char* format, ...)
{
  ASSERT(format != 0);

  char buffer[500];

  va_list vaList;
  va_start(vaList, format);
  vbprintf(buffer, 500, format, vaList);
  va_end(vaList);

#ifdef DEBUG
  dsLog.printf("-------------- %s", buffer);
#endif
  d_dialogText = copyString(buffer);
  ASSERT(d_dialogText != 0);
}

Boolean DataCheck::properConditions()
{
#ifdef DEBUG
   dsLog.printf("\n----- Checking for Proper Campaign Conditions");
#endif
//   bool loop; 
//   do
//   {
//      loop = False;
      WG_CampaignConditions::ConditionList& clist = d_campData->getConditions();
      SListIter<WG_CampaignConditions::Conditions> iter(&clist);
      while(++iter)
      {
         if(!iter.current()->checkData())
         {
            if(WG_CampaignConditions::getError())
            {
               setDialogText(WG_CampaignConditions::getError());
#ifdef DEBUG
               dsLog.printf(WG_CampaignConditions::getError());
#endif
            }            
            
            if(d_shouldRunDial)
            {
//               loop = True;

               Boolean shouldFix = False;
               if(!runDial(shouldFix))
                  return False;

               if(shouldFix)
               {
                  d_campWindows->runConditionEdit(iter.current());
               }
            }
         }
      }
//   } while (loop);

   return True;
}

Boolean DataCheck::hasOnlyOneSHQ(Side side)
{
  ASSERT(side != SIDE_Neutral);
#ifdef DEBUG
  dsLog.printf("\n----- Checking for one SHQ for %s", scenario->getSideName(side));
#endif

  ConstILeader sLeader = d_campData->getArmies().getSupremeLeaderID(side);
  if(sLeader == NoLeader)
  {
    setDialogText("%s has no Supreme Leader", scenario->getSideName(side));
    if(d_shouldRunDial)
    {
      Boolean shouldFix = False;
      if(!runDial(shouldFix))
        return False;

      if(shouldFix)
      {
        d_campWindows->runOBEdit(NoCommandPosition);
      }
    }
  }
  else
  {
   // ConstILeader sLeader = d_campData->getArmies().getSupremeLeader(side);

#ifdef DEBUG
   dsLog.printf("------------- %s's supreme leader is %d(%s)",
      scenario->getSideName(side),
      d_campData->getArmies().getIndex(sLeader),
      sLeader->getName());
#endif
   }

  /*
   * Go through every leader for this side and make sure there is only one SHQ
   */

  UnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getPresident(side), true);


  while(iter.next())
  {
    CommandPosition* cp = iter.currentCommand();

    if(cp->isLower(Rank_President))
    {
      ILeader iLeader = cp->getLeader();
      ASSERT(iLeader != NoLeader);

      Leader* leader = d_campData->getLeader(iLeader);
      if(leader->isSupremeLeader() && iLeader != sLeader)
      {
        setDialogText("%s has more than one Supreme Leader! %s and %s",
           scenario->getSideName(side), sLeader->getName(), leader->getName());

        if(d_shouldRunDial)
        {
          Boolean shouldFix = False;
          if(!runDial(shouldFix))
            return False;

          if(shouldFix)
          {
            d_campWindows->runOBEdit(leader->getCommand());
          }
        }
      }
    }
  }

  return True;
}

Boolean DataCheck::allTownsConnected()
{
#ifdef DEBUG
  dsLog.printf("\n----- Checking that all Towns are Connected");
#endif

  TownList& tl = d_campData->getTowns();
  ConnectionList& cl = d_campData->getConnections();

  /*
   * Check all province pictures exist
   * and that all provinces have a capital city
   */

   ProvinceList& pl = d_campData->getProvinces();
   for(IProvince ip = 0; ip < pl.entries(); ++ip)
   {
        if(pl[ip].getCapital() == NoTown)
        {
            setDialogText("Province %s has no capital",
            (const char*) pl[ip].getName());

         if(d_shouldRunDial)
         {
            Boolean fix = false;
            if(!runDial(fix))
               return false;
            if(fix)
               d_campWindows->runProvinceEdit(ip);
         }
        }

      const char* picName = pl[ip].picture();
      bool ok = false;
      if(picName == 0)  // allowed to not have a picture
         ok = true;
      else  // if(picName[0] != 0)
      {
         SimpleString fileName;
         fileName << scenario->getFolderName() << "\\Provinces\\" << pl[ip].picture() << ".BMP";
         if(FileSystem::fileExists(fileName.toStr()))
            ok = true;
      }

      if(!ok)
      {
         setDialogText("Province %s has bad picture %s.bmp",
            (const char*) pl[ip].getName(),
            (picName == 0) ? "<Null>" : picName);

         if(d_shouldRunDial)
         {
            Boolean fix = false;
            if(!runDial(fix))
               return false;
            if(fix)
               d_campWindows->runProvinceEdit(ip);
         }
      }
   }

  /*
   * Check All towns have a province
   */

  for(ITown iTown = 0; iTown < tl.entries(); iTown++)
  {
    Town& town = d_campData->getTown(iTown);

    if(town.getProvince() == NoProvince)
    {
      setDialogText("Town %s has no province", (const char*) town.getName());
      if(d_shouldRunDial)
      {
         Boolean fix = false;
         if(!runDial(fix))
            return false;
         if(fix)
            d_campWindows->runTownEdit(iTown);
      }
    }

    /*
     * Check connections
     */

    Boolean hasConnection = False;
    for(int i = 0; i < MaxConnections; i++)
    {
      if(town.getConnection(i) != NoConnection)
      {
        if(town.getConnection(i) >= cl.entries())
        {
          setDialogText("Improper Connection Value(%d of %d entries) for %s",
             static_cast<int>(town.getConnection(i)), cl.entries(), town.getName());

          if(d_shouldRunDial)
          {
            Boolean shouldFix = False;
            if(!runDial(shouldFix))
              return False;

            if(shouldFix)
            {
              d_campWindows->runConnectionEdit(iTown);
            }
          }
        }

        hasConnection = True;
      }
      else
        break;
    }

    if(!hasConnection)
    {
      setDialogText("No Connection found for %s", town.getName());

      if(d_shouldRunDial)
      {
        Boolean shouldFix = False;
        if(!runDial(shouldFix))
          return False;

        if(shouldFix)
        {
          d_campWindows->runConnectionEdit(iTown);
        }
      }
    }
  }
  return True;
}

Boolean DataCheck::allChokePointsValid()
{
#ifdef DEBUG
  dsLog.printf("\n----- Checking that all ChokePoints are valid");
#endif

  TownList& tl = d_campData->getTowns();
  ConnectionList& cl = d_campData->getConnections();

  // check for proper connection
  for(ITown iTown = 0; iTown < tl.entries(); iTown++)
  {
    Town& town = d_campData->getTown(iTown);

    // test choke points
    if(d_campData->isChokePoint(iTown))
    {
      Boolean hasThisSide = False;
      Boolean hasThatSide = False;
      for(int i = 0; i < MaxConnections; i++)
      {
        if(town.getConnection(i) != NoConnection)
        {
          Connection& con = cl[town.getConnection(i)];

          if(con.whichSideChokePoint(iTown) == WhichSideChokePoint::NoChokePoint)
          {
            setDialogText("Chokepoint(%s) has improper connection(%d)",
               town.getName(), static_cast<int>(town.getConnection(i)));

            if(d_shouldRunDial)
            {
              Boolean shouldFix = False;
              if(!runDial(shouldFix))
                return False;

              if(shouldFix)
              {
                d_campWindows->runConnectionEdit(iTown);
              }
            }
          }
          else
          {
            if(con.whichSideChokePoint(iTown) == WhichSideChokePoint::ThisSide)
              hasThisSide = True;
            else
              hasThatSide = True;
          }
        }
        else
          break;
      }

      // make sure it has both types
      if(!hasThisSide || !hasThatSide)
      {
        setDialogText("Chokepoint(%s) doesn't have both types of chokepoint-connections",
               town.getName());

        if(d_shouldRunDial)
        {
          Boolean shouldFix = False;
          if(!runDial(shouldFix))
            return False;

          if(shouldFix)
          {
            d_campWindows->runConnectionEdit(iTown);
          }
        }
      }
    }

    // test non-chokepoints
    else
    {
      for(int i = 0; i < MaxConnections; i++)
      {
        if(town.getConnection(i) != NoConnection)
        {
          Connection& con = cl[town.getConnection(i)];

          if(con.whichSideChokePoint(iTown) != WhichSideChokePoint::NoChokePoint)
          {
            setDialogText("Non-Chokepoint(%s) has improper connection(%d)",
               town.getName(), static_cast<int>(town.getConnection(i)));

            if(d_shouldRunDial)
            {
              Boolean shouldFix = False;
              if(!runDial(shouldFix))
                return False;

              if(shouldFix)
              {
                d_campWindows->runConnectionEdit(iTown);
              }
            }
          }
        }
        else
          break;
      }
    }
  }
  return True;
}

Boolean DataCheck::staffRatingsExceeded()
{
#ifdef DEBUG
  dsLog.printf("\n----- Check for Staff Ratings");
#endif

  /*
   * See if a units direct subordinates combined subordination rating
   * is greater than staff rating
   */

  UnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getTop(), true);
  while(iter.next())
  {
    CommandPosition* cp = iter.currentCommand();

    if(cp->isLower(Rank_President))
    {
      if(cp->getChild() != NoCommandPosition)
      {
        UWORD subordination = d_campData->getArmies().getCommandSubordination(iter.current());

        Leader* leader = d_campData->getArmies().getUnitLeader(iter.current());

        if(leader->getStaff(True) < subordination)
        {
          setDialogText("%s's staff(%d) is less than subordinates combined subordination rating(%d)",
             (const char*)d_campData->getUnitName(leader->getCommand()).toStr(), leader->getStaff(True), subordination);

          if(d_shouldRunDial)
          {
            Boolean shouldFix = False;
            if(!runDial(shouldFix))
              return False;

            if(shouldFix)
            {
              d_campWindows->runOBEdit(iter.current());
            }
          }
        }

      }
    }
  }
  return True;
}

Boolean DataCheck::allUnitsHaveLeaders()
{
#ifdef DEBUG
  dsLog.printf("\n----- Checking that all units have leaders");
#endif

  UnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getTop(), true);
  while(iter.next())
  {
    CommandPosition* cp = iter.currentCommand();

    if(cp->isLower(Rank_President))
    {
      if(cp->getLeader() == NoLeader)
      {
        setDialogText("%s(%s) has no Leader!", cp->getName(), scenario->getSideName(cp->getSide()));

        if(d_shouldRunDial)
        {
          Boolean shouldFix = False;
          if(!runDial(shouldFix))
            return False;

          if(shouldFix)
          {
            d_campWindows->runOBEdit(iter.current());
          }
        }
      }

    }
  }

  return True;

}

Boolean DataCheck::properUnitTypes()
{
#ifdef DEBUG
  dsLog.printf("\n----- Checking for proper unit-types");
#endif
  // Bodge for unittype info
  {
    UnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getTop(), true);
    while(iter.next())
    {
      if(iter.current()->getRank().isLower(Rank_President))
        d_campData->getArmies().setCommandSpecialistFlags(iter.current());
    }
  }

  UnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getTop(), true);
  while(iter.next())
  {
    CommandPosition* cp = iter.currentCommand();

    if(cp->isLower(Rank_President))
    {
      if(!d_campData->getArmies().unitTypeSanityCheck(iter.current())) //cp->getLeader() == NoLeader)
      {
        setDialogText("%s(%s) has Improper Unit-Type!", cp->getName(), scenario->getSideName(cp->getSide()));

        if(d_shouldRunDial)
        {
          Boolean shouldFix = False;
          if(!runDial(shouldFix))
            return False;

          if(shouldFix)
          {
            d_campWindows->runOBEdit(iter.current());
          }
        }
      }

    }
  }

  return True;

}

Boolean DataCheck::nationRulesEnforced()
{
#ifdef DEBUG
  dsLog.printf("\n----- Checking Nationality rules are properly applied");
#endif
  /*
   * First, go through all leaders and make sure they can control their unit
   */

  UnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getTop(), true);
  while(iter.next())
  {
    CommandPosition* cp = iter.currentCommand();

    if(cp->isLower(Rank_President))
    {
      if(!d_campData->getArmies().canLeaderTakeControl(cp->getLeader(), iter.current()))
      {
        Leader* leader = d_campData->getLeader(cp->getLeader());
        setDialogText("%s of %s cannot control %s",
            leader->getName(), scenario->getNationName(leader->getNation()), cp->getName());

        if(d_shouldRunDial)
        {
          Boolean shouldFix = False;
          if(!runDial(shouldFix))
            return False;

          if(shouldFix)
          {
            d_campWindows->runOBEdit(iter.current());
          }
        }

      }
    }
  }

  /*
   * Now, go through all units and make sure they can control their SP's
   */

  UnitIter uiter(&d_campData->getArmies(), d_campData->getArmies().getTop(), true);
  while(uiter.next())
  {
    CommandPosition* cp = uiter.currentCommand();

     if(cp->isHigher(Rank_Division))
     {
        if(cp->getSPEntry() != NoStrengthPoint)
        {
            setDialogText("%s is not a division, but has SPs attached to it",
                cp->getName());

            bool shouldFix = false;
            if(!runDial(shouldFix))
                return false;
            if(shouldFix)
            {
                d_campWindows->runOBEdit(cp);
            }
        }
     }
     else
    // if(cp->isLower(Rank_President))
    {
      StrengthPointIter spIter(&d_campData->getArmies(), uiter.current());

      while(++spIter)
      {
        const StrengthPoint* sp = spIter.current();

        const UnitTypeItem& unitType = d_campData->getUnitType(sp->getUnitType());

        Boolean canControlNation = False;
        for(Nationality n = 0; n < scenario->getNumNations(); n++)
        {
          if(unitType.isNationality(n))
          {
            if(d_campData->getArmies().canNationControlNation(cp->getNation(), n))
            {
              canControlNation = True;
              break;
            }

          }
        }

        if(!canControlNation)
        {
          setDialogText("%s from %s cannot control a %s from %s",
                 cp->getName(), scenario->getNationName(cp->getNation()),
                 unitType.getName(), scenario->getNationName(n));

          if(d_shouldRunDial)
          {
            Boolean shouldFix = False;
            if(!runDial(shouldFix))
              return False;

            if(shouldFix)
            {
              d_campWindows->runOBEdit(uiter.current());
            }
          }

        }
      }
    }
  }

  return True;
}

Boolean DataCheck::offScreenIsOffScreen()
{
#ifdef DEBUG
  dsLog.printf("\n----- Checking all offscreen locations are properly set");
#endif
  /*
   * First, find all offscreen provinces and make sure all its towns are offscreen
   */

  for(IProvince iProv = 0; iProv < d_campData->getProvinces().entries(); iProv++)
  {
    Province prov = d_campData->getProvince(iProv);
    if(prov.isOffScreen())
    {

      UWORD nTowns = prov.getNTowns();

      ITown stop = static_cast<ITown>(prov.getTown() + nTowns);

      for(ITown tIndex = prov.getTown(); tIndex < stop; tIndex++)
      {
        Town& town = d_campData->getTown(tIndex);

        if(!town.isOffScreen())
        {
          setDialogText("OffScreen province(%s) has an OnScreen town(%s)",
              prov.getName(), town.getName());

          if(d_shouldRunDial)
          {
            Boolean shouldFix = False;
            if(!runDial(shouldFix))
              return False;

            if(shouldFix)
            {
              d_campWindows->runTownEdit(tIndex);
            }
          }
        }

      }
    }

  }

  /*
   * Now make sure all offscreen towns, have offscreen province
   */

  for(ITown iTown = 0; iTown < d_campData->getTowns().entries(); iTown++)
  {
    Town& town = d_campData->getTown(iTown);

    if(town.isOffScreen())
    {
      if(town.getProvince() != NoProvince)
      {
        Province& prov = d_campData->getProvince(town.getProvince());

        if(!prov.isOffScreen())
        {
          setDialogText("OffScreen Town(%s) has an OnScreen Province(%s)",
             town.getName(), prov.getName());

          if(d_shouldRunDial)
          {
            Boolean shouldFix = False;
            if(!runDial(shouldFix))
              return False;

            if(shouldFix)
            {
              d_campWindows->runProvinceEdit(town.getProvince());
            }
          }
        }

      }
    }
  }

  /*
   * Now make sure all offscreen connections are connected to an offscreen town
   */

  for(IConnection iCon = 0; iCon < d_campData->getConnections().entries(); iCon++)
  {
    Connection& con = d_campData->getConnection(iCon);

    if(con.offScreen)
    {
      ASSERT(con.node1 != NoTown);
      ASSERT(con.node2 != NoTown);

      Town& town1 = d_campData->getTown(con.node1);
      Town& town2 = d_campData->getTown(con.node2);

      if(!town1.isOffScreen() && !town2.isOffScreen())
      {
        setDialogText("OffScreen Connection(%d) is connected to 2 OnScreen Towns(%s and %s)",
           static_cast<int>(iCon), town1.getName(), town2.getName());

        if(d_shouldRunDial)
        {
          Boolean shouldFix = False;
          if(!runDial(shouldFix))
            return False;

          if(shouldFix)
          {
            d_campWindows->runConnectionEdit( (!town1.isOffScreen()) ? con.node1 : con.node2);
          }
        }
      }

    }
  }

  return True;
}

Boolean removeSpaces(const char* text, char* buffer)
{
  const char space = ' ';

  ASSERT(text != 0);

  lstrcpy(buffer, text);

  char* ptr = buffer;
  ptr += lstrlen(buffer);
  ASSERT(*ptr == '\0');

  if( *(ptr-1) != space)
    return False;

  while( (ptr > buffer) && (*(--ptr) == space) )
  {
    *ptr = '\0';
  }

  return True;

}

void DataCheck::removeTrailingSpaces()
{
#ifdef DEBUG
  dsLog.printf("\n----- Removing Trailing spaces from Town, Province, Unit, and Leader names");
#endif
  char buffer[500];

  /*
   * First, go through all towns
   */

  for(ITown iTown = 0; iTown < d_campData->getTowns().entries(); iTown++)
  {
    Town& town = d_campData->getTown(iTown);

    if(removeSpaces(town.getName(), buffer))
    {
      town.setName(buffer);
#ifdef DEBUG
      dsLog.printf("----------- Removed trailing spaces from %s", town.getName());
#endif
    }
  }

  /*
   * Now, go through all provinces
   */

  for(IProvince iProv = 0; iProv < d_campData->getProvinces().entries(); iProv++)
  {
    Province& prov = d_campData->getProvince(iProv);

    if(removeSpaces(prov.getName(), buffer))
    {
      prov.setName(copyString(buffer));
#ifdef DEBUG
      dsLog.printf("----------- Removed trailing spaces from %s", prov.getName());
#endif
    }
  }

  UnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getTop(), true);
  while(iter.next())
  {
    CommandPosition* cp = iter.currentCommand();

    if(cp->isLower(Rank_President))
    {
      // Leader* leader = d_campData->getLeader(cp->getLeader());
      ILeader leader = cp->getLeader();

      if(removeSpaces(cp->getName(), buffer))
      {
        cp->setName(copyString(buffer));
#ifdef DEBUG
        dsLog.printf("------------ Removed trailing spaces from %s", cp->getName());
#endif
      }

      if(removeSpaces(leader->getName(), buffer))
      {
        leader->setName(copyString(buffer));
#ifdef DEBUG
        dsLog.printf("------------ Removed trailing spaces from %s", leader->getName());
#endif
      }
    }

  }
}



Boolean CampaignDataSanityCheck::run(CampaignData* campData, CampaignWindowsInterface* cwi)
{
  ASSERT(campData != 0);
  ASSERT(cwi != 0);

  DataCheck dataCheck(campData, cwi);
  return dataCheck.run();
}

#endif // !CUSTOMIZE

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
