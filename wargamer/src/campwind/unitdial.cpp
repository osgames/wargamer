/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "unitdial.hpp"
#include "app.hpp"
#include "resdef.h"
#include "winctrl.hpp"
#include "fonts.hpp"
#include "imglib.hpp"
#include "scenario.hpp"
#include "dib.hpp"
#include "scn_res.h"
#include "ccombo.hpp"
#include "compos.hpp"
#include "campdint.hpp"
#include "campord.hpp"
#include "realord.hpp"     // CampaignOrderUtil::getDescription
#include "userint.hpp"
#include "palette.hpp"
#include "armies.hpp"
#include "wmisc.hpp"
#include "bmp.hpp"
#include "ctab.hpp"
#include "to_win.hpp"
#include "town.hpp"
#include "button.hpp"
#include "simpstr.hpp"
#include "tooltip.hpp"
#include "unitinfo.hpp"
#include "org_dial.hpp"
#include "options.hpp"
#include "scn_img.hpp"
#include "cbutton.hpp"
#include "itemwind.hpp"
#include "res_str.h"
#include "resstr.hpp"
#include "armyutil.hpp"

/*-------------------------- Resource String management ------------------------------
 *
 */

class UnitDialogStrings {
public:
  enum {
     UnknownLeader,
     Orders,
     Activity,
     Current,
     InRoute,
     Actioning,
     Summary,
     Leader,
     OB,

     HowMany
  };
};

static int ids[UnitDialogStrings::HowMany] = {
   IDS_UI_LEADERUNKNOWN,
   IDS_UI_ORDERS,
   IDS_UI_ACTIVITY,
   IDS_UI_CURRENT,
   IDS_UI_INROUTE,
   IDS_UI_ACTIONING,
   IDS_UI_SUMMARY,
   IDS_UI_LEADER,
   IDS_UI_OB
};

static ResourceStrings s_strings(ids, UnitDialogStrings::HowMany);

/*-------------------------- UnitDialog Base class -------------------------
 *
 */

DrawDIBDC* UnitDialog::s_dib = 0;

void UnitDialog::updateCaption()
{
  ICommandPosition cpi = lparamToCP(d_combos.getValue(CampaignComboInterface::UnitList));
  Boolean limitedInfo = (CampaignOptions::get(OPT_LimitedUnitInfo) && !CampaignArmy_Util::isUnitOrderable(cpi));

  CommandPosition* cp = d_campData->getCommand(cpi);
  Leader* leader = d_campData->getArmies().getLeader(cp->getLeader());

  const char* text = (limitedInfo && cp->getInfoQuality() == CommandPosition::NoInfo)
    ? s_strings.get(UnitDialogStrings::UnknownLeader) : leader->getName();

//  SetWindowText(getHWND(), text);
  d_customDialog.setCaption(text);
}

void UnitDialog::drawCombo(const DrawDIB* fillDib, const DRAWITEMSTRUCT* lpDrawItem, Boolean showManpower)
{
  ASSERT(fillDib);

  CampaignComboData data;
  data.d_currentUnit = currentUnit();
  data.d_showManpower = showManpower;

  data.d_itemCX = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  data.d_itemCY = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  if(data.d_currentUnit != NoCommandPosition)
  {
    allocateStaticDib(data.d_itemCX, data.d_itemCY);
    ASSERT(s_dib);

    LONG dbUnits = GetDialogBaseUnits();
    LONG dbY = HIWORD(dbUnits);
    int fontHeight = (dbY*7)/8;

    LogFont lf;
    lf.height(fontHeight);
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));

    Font font;
    font.set(lf);

    s_dib->setFont(font);
    s_dib->setBkMode(TRANSPARENT);

    d_combos.drawCombo(CampaignComboInterface::UnitList, s_dib, fillDib, data);
    CampaignUserInterface::blitDibToControl(s_dib, lpDrawItem);
  }
}


void UnitDialog::initCombo(StackedUnitList& list, Boolean all, Boolean showManpower)
{
  ASSERT(list.unitCount() > 0);

  d_unitListExpanded = all;

  CampaignComboData data;
  data.d_unitList = &list;
  data.d_currentUnit = list.getUnit(0);
  data.d_showAll = all;

  d_combos.init(CampaignComboInterface::UnitList, data);

  updateCaption();
}

void UnitDialog::hide()
{
  if(d_combos.created(CampaignComboInterface::UnitList))
    d_combos.hide(CampaignComboInterface::UnitList);

  ShowWindow(getHWND(), SW_HIDE);
}

void UnitDialog::show(const PixelPoint& p) //const MapWindowData& mapData, const Location& l)
{
  SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);
}

void UnitDialog::destroy()
{
  DestroyWindow(getHWND());
}

void UnitDialog::showComboList(HWND hwndCtl)
{
  RECT r;
  GetWindowRect(hwndCtl, &r);

  PixelPoint p(r.left, r.top);
  d_combos.show(CampaignComboInterface::UnitList, p);
}

ICommandPosition UnitDialog::currentUnit() const
{
  if(d_combos.created(CampaignComboInterface::UnitList))
    return lparamToCP(d_combos.getValue(CampaignComboInterface::UnitList));
  else
    return NoCommandPosition;
}

CampaignWindowsInterface* UnitDialog::campWindows() const
{
  return d_owner->campWindows();
}

const StackedUnitList& UnitDialog::getUnitList()
{
  return *(reinterpret_cast<const StackedUnitList*>(d_owner->getData()));
}

void UnitDialog::allocateStaticDib(int cx, int cy)
{
  if(!s_dib ||
     s_dib->getWidth() < cx || s_dib->getHeight() < cy)
  {
    int oldCX = 0;
    int oldCY = 0;

    if(s_dib)
    {
      oldCX = s_dib->getWidth();
      oldCY = s_dib->getHeight();

      delete s_dib;
      s_dib = 0;
    }
    s_dib = new DrawDIBDC(maximum(oldCX, cx), maximum(oldCY, cy));

    s_dib->setBkMode(TRANSPARENT);
  }

  ASSERT(s_dib);
}

void UnitDialog::deleteStaticDib()
{
  if(s_dib)
    delete s_dib;

  s_dib = 0;
}


/* ------------------------- Move-Order Dialog ----------------------------
 *
 */

class MoveOrderDial : public UnitDialog  {
    const DrawDIB* d_fillDib;
//  DrawDIBDC* d_itemDib;

  public:
    MoveOrderDial(CampaignUserInterface* owner, const CampaignData* campData, CompleteOrder& order, CampaignComboInterface& combos);
    ~MoveOrderDial(); // {}

    void initControls();
    void enableControls();
    void redrawCombo();

  private:
    BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
    void onDestroy(HWND hwnd) {}
    void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);

    void show(const PixelPoint& p);
};

MoveOrderDial::MoveOrderDial(CampaignUserInterface* owner, const CampaignData* campData, CompleteOrder& order, CampaignComboInterface& combos) :
  UnitDialog(owner, campData, order, combos),
  d_fillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground))
{
  HWND dhwnd = createDialog(moveOrderDialog, APP::getMainHWND(), False);
  ASSERT(dhwnd != NULL);
}

MoveOrderDial::~MoveOrderDial()
{
}

BOOL MoveOrderDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL MoveOrderDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().setBkDib(scenario->getSideBkDIB(SIDE_Neutral));
  customDialog().setCaptionBkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  customDialog().init(hwnd);

  HWND hLCombo = GetDlgItem(hwnd, UMD_UNITS);  // main unit combo
  ASSERT(hLCombo != 0);

  // buttons
  for(int id = UMD_BUTTONID_FIRST; id < UMD_BUTTONID_LAST; id++)
  {
    CustomButton cb(hwnd, id);
    cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
    cb.setBorderColours(scenario->getBorderColors());
  }

  /*
   * Add tooltips
   */

  static TipData tipData[] = {
    { UMD_UNITS,    TTS_UMD_UNITS,    SWS_UMD_UNITS     },
    { UMD_SEND,     TTS_UMD_SEND,     SWS_UMD_SEND      },
    { UMD_CANCEL,   TTS_UMD_CANCEL,   SWS_UMD_CANCEL    },
    { UMD_FULLDIAL, TTS_UMD_MAINDIAL, SWS_UMD_MAINDIAL  },
    EndTipData
  };

  g_toolTip.addTips(hWnd, tipData);

  return True;
}


void MoveOrderDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case UMD_UNITS:
    {
      if(codeNotify == STN_CLICKED)
      {
        showComboList(hwndCtl);
        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }
      break;
    }

    case UMD_SEND:
    {
      owner()->sendOrder(currentUnit());
      break;
    }

    case UMD_CANCEL:
    {
      owner()->cancelOrder();
      break;
    }

    case UMD_FULLDIAL:
    {
      owner()->toggle();
      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUPOPUP, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;
    }
  }
}

void MoveOrderDial::redrawCombo()
{
  HWND h = GetDlgItem(hWnd, UMD_UNITS);
  ASSERT(h);

  InvalidateRect(h, NULL, FALSE);
}

void MoveOrderDial::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  switch(lpDrawItem->CtlID)
  {
    case UMD_UNITS:
      drawCombo(d_fillDib, lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

void MoveOrderDial::initControls()
{
  enableControls();
}

void MoveOrderDial::enableControls()
{
  ICommandPosition cpi = currentUnit();
  Boolean underSiege = False;

  if(cpi != NoCommandPosition)
  {
    CommandPosition* cp = campData()->getCommand(cpi);
    if(cp->isGarrison())
    {
      ASSERT(cp->atTown());

      Town& t = campData()->getTown(cp->getTown());

      underSiege = t.isSieged();
    }
  }

  {
    CustomButton b(getHWND(), UMD_SEND);
    b.enable(!underSiege);
  }
}

void MoveOrderDial::show(const PixelPoint& p)
{
  initControls();
  UnitDialog::show(p);
}

/* ------------------------- Main Dialog ----------------------------
 *
 */

/*
 * Dialog Page Base class
 */

class UnitDialogPage : public ModelessDialog {
   const CampaignData* d_campData;
   DLGTEMPLATE* d_dialog;
   UnitDialog* d_owner;
   CustomDialog d_customDialog;
public:
   enum Page {
      First = 0,
      SummaryPage = First,
      LeaderPage,

      PlayerUnitOnly_First,
      ActivityPage = PlayerUnitOnly_First,
      OrderPage,
      OBPage,

      HowMany
   };

   UnitDialogPage(const CampaignData* campData, UnitDialog* p, const char* dlgName);
   ~UnitDialogPage();

   DLGTEMPLATE* getDialog() const { return d_dialog; }
   virtual const char* getTitle() const = 0;
   HWND create();
   void enable();
   void disable();
   virtual void updateValues() = 0;
   virtual void initControls() = 0;
   virtual void enableControls() = 0;

   static UnitDialogPage* createPage(Page page, const CampaignData* campData, UnitDialog* p);

protected:
   void setPosition();

   UnitDialog* owner() const { return d_owner; }
   const CampaignData* campData() const { return d_campData; }
   CustomDialog& customDialog() { return d_customDialog; }
};

UnitDialogPage::UnitDialogPage(const CampaignData* campData, UnitDialog* p, const char* dlgName) :
  d_campData(campData),
  d_owner(p)
{
   HRSRC rhTep = FindResource(NULL, dlgName, RT_DIALOG);
   ASSERT(rhTep != NULL);
   HGLOBAL lphTep = (HGLOBAL) LoadResource(NULL, rhTep);
   ASSERT(lphTep != NULL);
   d_dialog = (LPDLGTEMPLATE) LockResource(lphTep);
   ASSERT(d_dialog != NULL);

   /*
    * Initialize Custom Dialog settings
    */

// d_customDialog.setBkDib(scenario->getSideBkDIB(SIDE_Neutral));
}

UnitDialogPage::~UnitDialogPage()
{
//  if(d_pageBkDib)
//    delete d_pageBkDib;
}

HWND UnitDialogPage::create()
{
   ASSERT(hWnd == NULL);      // Already created?

   HWND hDialog = createDialogIndirect(d_dialog, d_owner->getHWND());

   ASSERT(hDialog != NULL);

   return hDialog;
}

void UnitDialogPage::enable()
{
   ASSERT(hWnd != NULL);
   ShowWindow(hWnd, SW_SHOW);
}

void UnitDialogPage::disable()
{
   ASSERT(hWnd != NULL);
   ShowWindow(hWnd, SW_HIDE);
}

void UnitDialogPage::setPosition()
{
   const RECT& r = d_owner->tabbedDialogRect();

   SetWindowPos(getHWND(), HWND_TOP, r.left, r.top, r.right-r.left, r.bottom-r.top, 0);
}

/*
 * Orders Page
 */

class UnitOrderPage : public UnitDialogPage {
   const DrawDIB* d_destTextFillDib;
   const DrawDIB* d_dateTextFillDib;

   TimedOrderWindow d_calender;

   Boolean d_toUnit;
public:
   enum { NumStringIDs = 6 };
   enum { NCheckButtonImages = 2 };

   UnitOrderPage(const CampaignData* campData, UnitDialog* p) :
     UnitDialogPage(campData, p, unitOrderPage),
     d_destTextFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
     d_dateTextFillDib(scenario->getSideBkDIB(SIDE_Neutral)),
     d_toUnit(False) {}

   ~UnitOrderPage();

   const char* getTitle() const { return s_strings.get(UnitDialogStrings::Orders); }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void showCalender(HWND hButton);

   void drawDestinationText(const DRAWITEMSTRUCT* lpDrawItem);
   void drawDateText(const DRAWITEMSTRUCT* lpDrawItem);

   void drawCombo(CampaignComboInterface::Type type, const DRAWITEMSTRUCT* lpDrawItem);

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
   void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
   void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
};

UnitOrderPage::~UnitOrderPage()
{
}

BOOL UnitOrderPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_MEASUREITEM:
         HANDLE_WM_MEASUREITEM(hWnd, wParam, lParam, onMeasureItem);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}


BOOL UnitOrderPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  /*
   * Initalize combos
   */

  // get font size and font for combos
  // get base font size (in dialog units)
  LONG dbUnits = GetDialogBaseUnits();
  const LONG dbY = HIWORD(dbUnits);


  /*
   * Create our combo-like controls
   */

  int id = UOP_FIRSTCOMBO;
  SIZE s;

  HWND hLCombo = GetDlgItem(hwnd, UOP_ORDERONARRIVAL);
  ASSERT(hLCombo != 0);
  RECT r1;
  GetClientRect(hLCombo, &r1);

  HWND hRCombo = GetDlgItem(hwnd, UOP_AGGRESSION);
  ASSERT(hRCombo != 0);
  RECT r2;
  GetClientRect(hRCombo, &r2);

  for(CampaignComboInterface::Type t = CampaignComboInterface::OrderPageFirst;
      t < CampaignComboInterface::OrderPageLast;
      INCREMENT(t), id++)
  {
    s.cx = (t == CampaignComboInterface::Aggression) ? r2.right - r2.left : r1.right - r1.left;
    s.cy = (t == CampaignComboInterface::Aggression) ? r2.bottom - r2.top : r1.bottom - r1.top;

    ASSERT(id < UOP_LASTCOMBO);
    owner()->combos().create(hwnd, t, id, campData(), s, ItemWindowData::ShadowBackground);
  }

  /*
   * Create our check boxes
   */

  int fontHeight = (dbY*6)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  for(id = UOP_CHECKBOX_FIRST; id < UOP_CHECKBOX_LAST; id++)
  {
    CustomButton b(hwnd, id);

    b.setFillDib(scenario->getSideBkDIB(SIDE_Neutral));
    b.setBorderColours(scenario->getBorderColors());
    b.setCheckBoxImages(ScenarioImageLibrary::get(ScenarioImageLibrary::CheckButtons));
    b.setFont(font);
  }

  /*
   *  Create calender window
   */

  d_calender.create(hwnd, -1, owner()->order(), campData()->getDate());

  static TipData tipData[] = {
    { UOP_ORDERTYPE,       TTS_UOP_ORDER,         SWS_UOP_ORDER        },
    { UOP_MOVEHOW,         TTS_UOP_MOVEHOW,       SWS_UOP_MOVEHOW   },
    { UOP_ORDERONARRIVAL,  TTS_UOP_ONARRIVAL,     SWS_UOP_ONARRIVAL    },
    { UOP_PURSUE,          TTS_UOP_PURSUE,        SWS_UOP_PURSUE       },
    { UOP_SOUNDGUNS,       TTS_UOP_SOUNDGUNS,     SWS_UOP_SOUNDGUNS    },
    { UOP_SIEGEACTIVE,     TTS_UOP_SIEGEACTIVE,   SWS_UOP_SIEGEACTIVE },
    { UOP_ORDERTEXT,       TTS_UOP_ORDERTEXT,     SWS_UOP_ORDERTEXT    },
    { UOP_ORDERDATE,       TTS_UOP_ORDERDATE,     SWS_UOP_ORDERDATE    },
    { UOP_DESTINATION,     TTS_UOP_DESTINATION,   SWS_UOP_DESTINATION },
    { UOP_TOUNIT,          TTS_UOP_TOUNIT,        SWS_UOP_TOUNIT      },
    { UOP_AGGRESSION,      TTS_UOP_AGGRESSION,    SWS_UOP_AGGRESSION  },
    { UOP_OFFENSIVE,       TTS_UOP_OFFENSIVE,     SWS_UOP_OFFENSIVE   },
    { UOP_DEFENSIVE,       TTS_UOP_DEFENSIVE,     SWS_UOP_DEFENSIVE   },
    EndTipData
  };

  g_toolTip.addTips(hWnd, tipData);

  setPosition();
  return TRUE;
}

void UnitOrderPage::onDestroy(HWND hwnd)
{
  d_calender.destroy();
}

CampaignComboInterface::Type idToComboItem(int id)
{
  if(id == UOP_MOVEHOW)
    return CampaignComboInterface::MoveHow;
  else if(id == UOP_ORDERONARRIVAL)
    return CampaignComboInterface::OnArrival;
  else if(id == UOP_ORDERTYPE)
    return CampaignComboInterface::OrderType;
  else if(id == UOP_AGGRESSION)
    return CampaignComboInterface::Aggression;
#ifdef DEBUG
  else
    FORCEASSERT("Improper ID in idToComboItem(int id)");
#endif

  return CampaignComboInterface::OrderPageFirst;
}

void UnitOrderPage::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {

    case UOP_ORDERDATE:
    {
      if(codeNotify == BN_CLICKED)
      {
        showCalender(hwndCtl);

        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }
      break;
    }

    case UOP_PURSUE:
    {
      if(codeNotify == BN_CLICKED)
      {
        owner()->order().setPursue(!owner()->order().getPursue());

        CustomButton b(hwndCtl);
        b.setCheck(owner()->order().getPursue());

        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }
      break;
    }

    case UOP_SOUNDGUNS:
    {
      if(codeNotify == BN_CLICKED)
      {
        owner()->order().setSoundGuns(!owner()->order().getSoundGuns());

        CustomButton b(hwndCtl);
        b.setCheck(owner()->order().getSoundGuns());

        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }
      break;
    }

    case UOP_SIEGEACTIVE:
    {
      if(codeNotify == BN_CLICKED)
      {
        owner()->order().setSiegeActive(!owner()->order().getSiegeActive());

        CustomButton b(hwndCtl);
        b.setCheck(owner()->order().getSiegeActive());

        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }
      break;
    }

    case UOP_TOUNIT:
    {
      if(codeNotify == BN_CLICKED)
      {
        d_toUnit = !d_toUnit;

        CustomButton b(hwndCtl);
        b.setCheck(d_toUnit);

        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }
      break;
    }

    case UOP_OFFENSIVE:
    case UOP_DEFENSIVE:
    {
      if(codeNotify == BN_CLICKED)
      {
        Orders::Posture::Type p = owner()->order().posture();
        if(p == Orders::Posture::Offensive)
          p = Orders::Posture::Defensive;
        else
          p = Orders::Posture::Offensive;

        owner()->order().posture(p);

        CustomButton ob(getHWND(), UOP_OFFENSIVE);
        CustomButton db(getHWND(), UOP_DEFENSIVE);

        ob.setCheck(p == Orders::Posture::Offensive);
        db.setCheck(p == Orders::Posture::Defensive);

        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }
      break;
    }

    case UOP_MOVEHOW:
    case UOP_ORDERONARRIVAL:
    case UOP_ORDERTYPE:
    case UOP_AGGRESSION:
    {
      if(codeNotify == STN_CLICKED)
      {
        RECT r;
        GetWindowRect(hwndCtl, &r);

        PixelPoint p(r.left, r.top);
        owner()->combos().show(idToComboItem(id), p);

        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }
      break;
    }

    case UOP_MOVEHOWLIST:
    {
      Orders::AdvancedOrders::MoveHow mh = static_cast<Orders::AdvancedOrders::MoveHow>(owner()->combos().getValue(CampaignComboInterface::MoveHow));
      owner()->order().setMoveHow(mh);

      HWND h = GetDlgItem(hWnd, UOP_MOVEHOW);
      ASSERT(h);

      InvalidateRect(h, NULL, FALSE);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;
    }

    case UOP_ONARRIVALLIST:
    {
      Orders::AdvancedOrders::OrderOnArrival oa = static_cast<Orders::AdvancedOrders::OrderOnArrival>(owner()->combos().getValue(CampaignComboInterface::OnArrival));
      owner()->order().setOrderOnArrival(oa);

      HWND h = GetDlgItem(hWnd, UOP_ORDERONARRIVAL);
      ASSERT(h);

      InvalidateRect(h, NULL, FALSE);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;
    }

    case UOP_ORDERTYPELIST:
    {
      Orders::Type::Value cbo = static_cast<Orders::Type::Value>(owner()->combos().getValue(CampaignComboInterface::OrderType));
      Orders::Type::Value co = owner()->order().getType();

      if(co == Orders::Type::MoveTo && cbo != Orders::Type::MoveTo)
      {
        /*
         * Clear out any move related values
         */

        owner()->order().setDestTown(NoTown);
        owner()->order().setTarget(NoCommandPosition);
        owner()->order().getVias()->clear();
      }

      owner()->order().setType(cbo);

      initControls();

      HWND h = GetDlgItem(hWnd, UOP_ORDERTYPE);
      ASSERT(h);

      InvalidateRect(h, NULL, FALSE);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;
    }

    case UOP_AGGRESSIONLIST:
    {
      Orders::Aggression::Value ag = static_cast<Orders::Aggression::Value>(owner()->combos().getValue(CampaignComboInterface::Aggression));
      owner()->order().setAggressLevel(ag);

      HWND h = GetDlgItem(hWnd, UOP_AGGRESSION);
      ASSERT(h);

      InvalidateRect(h, NULL, FALSE);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;
    }
  }
}

void UnitOrderPage::showCalender(HWND hButton)
{
  RECT r;
  GetWindowRect(hButton, &r);

  POINT p;
  p.x = r.left;
  p.y = r.top;

  ASSERT(d_calender.hwnd());
  GetWindowRect(d_calender.hwnd(), &r);

  CampaignUserInterface::clipPoint(r, p);

  d_calender.init(campData()->getDate());
  d_calender.show(p);
}

/*
 *  Can we give this order on arrival
 */


void UnitOrderPage::initControls()
{
  /*
   * Fill in Combos
   */

  CampaignComboData data;
  data.d_currentUnit = owner()->currentUnit();
  data.d_order = &owner()->order();

  for(CampaignComboInterface::Type t = CampaignComboInterface::OrderPageFirst;
      t < CampaignComboInterface::OrderPageLast;
      INCREMENT(t))
  {
    owner()->combos().init(t, data); //owner()->currentUnit(), owner()->order());
  }


  /*
   * Set Pursue Button
   */

  {
    CustomButton b(getHWND(), UOP_PURSUE);
    b.setCheck(owner()->order().getPursue());
  }

  /*
   * Set Sound of Guns Button
   */

  {
    CustomButton b(getHWND(), UOP_SOUNDGUNS);
    b.setCheck(owner()->order().getSoundGuns());
  }

  /*
   * Set Siege Active Button
   */

  {
    CustomButton b(getHWND(), UOP_SIEGEACTIVE);
    b.setCheck(owner()->order().getSiegeActive());
  }

  {
    Date date = (owner()->order().getDate().year != 0) ? owner()->order().getDate() : campData()->getDate();
    char buf[200];

    SetDlgItemText(getHWND(), UOP_ORDERTEXT, date.toAscii(buf));
  }

  {
    SetDlgItemText(getHWND(), UOP_DESTINATION,
      CampaignOrderUtil::getDestinationText(campData(), &owner()->order()).toStr());
  }

  /*
   * init posture check boxes
   */

  {
    Orders::Posture::Type p = owner()->order().posture();

    CustomButton ob(getHWND(), UOP_OFFENSIVE);
    CustomButton db(getHWND(), UOP_DEFENSIVE);

    ob.setCheck(p == Orders::Posture::Offensive);
    db.setCheck(p == Orders::Posture::Defensive);
  }


  enableControls();
}

void UnitOrderPage::enableControls()
{
  const Boolean hasMoveOrder = (owner()->order().getType() == Orders::Type::MoveTo);

  {
    WindowControl cb(getHWND(), UOP_MOVEHOW);
    cb.enable(hasMoveOrder);
  }

  {
    WindowControl cb(getHWND(), UOP_ORDERONARRIVAL);
    cb.enable(hasMoveOrder);
  }

}

void UnitOrderPage::updateValues()
{
  const Boolean hasMoveOrder = (owner()->order().getType() == Orders::Type::MoveTo);

  if(hasMoveOrder)
  {
    /*
     * Set OrderOnArrival combo
     */

    Boolean targetToUnit = False;

    /*
     * If we are attaching on arrival and we don't already have a target, get one
     * Go through all sister units and find all who are at our dest town
     */

    if( (owner()->combos().getValue(CampaignComboInterface::OnArrival) == Orders::AdvancedOrders::Attach) &&
        (owner()->order().getTargetUnit() == NoCommandPosition) )
    {
        targetToUnit = True;
    }


    /*
     *  see if toUnit button is checked
     */

    {
      if(d_toUnit)
      {
        if(owner()->order().getTargetUnit() == NoCommandPosition)
        {
          targetToUnit = True;
        }

        d_toUnit = False;
      }
    }

    /*
     * Set target to a unit(friendly) at destination town
     */

    if(targetToUnit)
    {
      ASSERT(owner()->order().getDestTown() != NoTown);
      ICommandPosition cpi = owner()->currentUnit();
      if(cpi != NoCommandPosition)
      {
        CommandPosition* cp = campData()->getCommand(cpi);

        /*
         * Pick the highest ranking to attach to
         */

        ICommandPosition bestCPI  = NoCommandPosition;
        RankEnum bestRank = Rank_Brigade;

        UnitIter iter(&campData()->getArmies(), campData()->getArmies().getFirstUnit(cp->getSide()));
        while(iter.sister())
        {
          CommandPosition* sisterCP = iter.currentCommand();

          if( (sisterCP->getCloseTown() == owner()->order().getDestTown()) )
          {
            if(bestCPI == NoCommandPosition || sisterCP->getRankEnum() <= bestRank)
            {
              bestCPI = iter.current();
              bestRank = sisterCP->getRankEnum();
            }

          }
        }

        if(bestCPI != NoCommandPosition)
          owner()->order().setTargetUnit(bestCPI);
      }

    }
  }

  /*
   * close date window if it is open
   */

  d_calender.hide();

  for(CampaignComboInterface::Type t = CampaignComboInterface::OrderPageFirst;
      t < CampaignComboInterface::OrderPageLast;
      INCREMENT(t))
  {
    owner()->combos().hide(t);
  }

}

void UnitOrderPage::onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem)
{
  // set combo size (convert from dialog units)
  LONG dbUnits = GetDialogBaseUnits();
  const LONG dbY = HIWORD(dbUnits);

  const int size = (5*dbY)/8;

  switch(lpMeasureItem->CtlID)
  {
    case UOP_ORDERTYPE:
    case UOP_ORDERONARRIVAL:
      lpMeasureItem->itemHeight = size;
      break;
  }
}


void UnitOrderPage::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  switch(lpDrawItem->CtlID)
  {
    case UOP_ORDERTYPE:
    case UOP_MOVEHOW:
    case UOP_ORDERONARRIVAL:
    case UOP_AGGRESSION:
      drawCombo(idToComboItem(lpDrawItem->CtlID), lpDrawItem);
      break;

    case UOP_DESTINATION:
      drawDestinationText(lpDrawItem);
      break;

    case UOP_ORDERTEXT:
      drawDateText(lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}


void UnitOrderPage::drawDestinationText(const DRAWITEMSTRUCT* lpDrawItem)
{
  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  owner()->dib()->rect(0, 0, cx, cy, scenario->getSideBkDIB(SIDE_Neutral));

  RECT r;
  GetWindowRect(lpDrawItem->hwndItem, &r);

  PixelPoint p(r.left, r.top);
//  CampaignUserInterface::transferBits(owner()->dib(), p, cx, cy, 10, 10);

  LONG dbUnits = GetDialogBaseUnits();
  const LONG dbY = HIWORD(dbUnits);

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);
  CampaignUserInterface::drawText(getHWND(), lpDrawItem, owner()->dib(), d_destTextFillDib, True);
}

void UnitOrderPage::drawDateText(const DRAWITEMSTRUCT* lpDrawItem)
{
  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  LONG dbUnits = GetDialogBaseUnits();
  const LONG dbY = HIWORD(dbUnits);

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);

  CampaignUserInterface::drawText(getHWND(), lpDrawItem, owner()->dib(), d_dateTextFillDib, False);
}


void UnitOrderPage::drawCombo(CampaignComboInterface::Type type, const DRAWITEMSTRUCT* lpDrawItem)
{
  ASSERT(d_destTextFillDib);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  CampaignComboData data;
  data.d_currentUnit = owner()->currentUnit();
  data.d_order = &owner()->order();
  data.d_itemCX = cx;
  data.d_itemCY = cy;

  LONG dbUnits = GetDialogBaseUnits();
  const LONG dbY = HIWORD(dbUnits);

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);

  owner()->combos().drawCombo(type, owner()->dib(), d_destTextFillDib, data);
  CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
}


/*----------------------------------------------------------------------------
 * Activity Page
 */

class UnitActivityPage : public UnitDialogPage {
   const DrawDIB* d_modeTextFillDib;
   const DrawDIB* d_orderTextFillDib;

   CustomDrawnTab d_customTab;
public:
   class OrderPages {
   public:
     enum Page {
       First = 0,
       CurrentOrder = First,
       OrderInRoute,
       ActioningOrder,

       HowMany,

       StringIDOffset = UnitDialogStrings::Current
     };
   };
private:
   OrderPages::Page d_page;

   enum { UAP_TABBED = 100 };

   const CampaignOrder* d_order;

public:
   UnitActivityPage(const CampaignData* campData, UnitDialog* p) :
     UnitDialogPage(campData, p, unitActivityPage),
     d_modeTextFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
     d_orderTextFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
     d_page(OrderPages::First),
     d_order(0) {}

   ~UnitActivityPage();

   const char* getTitle() const { return s_strings.get(UnitDialogStrings::Activity); }
private:
   void initControls();
   void updateValues() {}
   void enableControls() {}
   void onSelChanged();

   void drawOrderDib(int cx, int cy);

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify) {}
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
   LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
};

UnitActivityPage::~UnitActivityPage()
{
}

BOOL UnitActivityPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

#if 0
// TODO: these need to go in the string resource
static const char* tabText[UnitActivityPage::OrderPages::HowMany] = {
     "Current",
     "In Route",
     "Actioning"
};
#endif

BOOL UnitActivityPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  SetDlgItemText(hwnd, UAP_ACTIVITYTEXT, "");

  /*
   * Create tab controls for Order text
   */

  // top left corner of page (in dialog units)
  static LONG dbUnits = GetDialogBaseUnits();
  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  const LONG baseX = 3;
  const LONG baseY = 32;

  DLGTEMPLATE* dialog = getDialog();

  RECT r = owner()->tabbedDialogRect();
  SetRect(&r, baseX, baseY, (baseX+dialog->cx)-3, (baseY+dialog->cy)-2);

  // convert dialog units to pixel coordinates
  MapDialogRect(hwnd, &r);

  /*
   * Create a tabbed window
   */

  HWND hwndTab = CreateWindow(
      WC_TABCONTROL, "",
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
         TCS_TOOLTIPS | TCS_FIXEDWIDTH,
      0, 0, r.right-r.left, 100,
      hwnd,
      (HMENU) UAP_TABBED,
      APP::instance(),
      NULL);

   ASSERT(hwndTab != NULL);

   /*
    * Set up the tabbed titles
    *
    * This must be done before using AdjustRect
    *
    * Current Order is the only tab that will always be available
    */


   TC_ITEM tie;
   tie.mask = TCIF_TEXT | TCIF_PARAM;
   tie.lParam = OrderPages::CurrentOrder;

   tie.pszText = const_cast<char*>(s_strings.get(OrderPages::CurrentOrder + OrderPages::StringIDOffset));
   TabCtrl_InsertItem(hwndTab, 0, &tie);

   /*
    * Set size of tabs (in Dialog Units)
    */

   LONG tabWidth = 40;
   LONG tabHeight = 10;

   tabWidth = (tabWidth*dbX)/4;
   tabHeight = (tabHeight*dbY)/8;

   TabCtrl_SetItemSize(hwndTab, tabWidth, tabHeight);

   /*
    * Convert coordinates to pixel coordinates
    * and Move the window
    */

  // Work out complete size with tabs, and shift to desired location
  TabCtrl_AdjustRect(hwndTab, TRUE, &r);

  // Move tabbed Window
  SetWindowPos(hwndTab, NULL, r.left, r.top, r.right - r.left, r.bottom - r.top, SWP_NOZORDER);

  // Get display area
  TabCtrl_AdjustRect(hwndTab, FALSE, &r);    // Get display area

  /*
   * Position static text control within display rect
   */

  HWND hText = GetDlgItem(hwnd, UAP_ACTIVITYTEXT);
  ASSERT(hText);

  MoveWindow(hText, r.left, r.top, r.right-r.left, r.bottom-r.top, FALSE);

  /*
   * Initialize custom drawn tab class
   */

  d_customTab.tabHWND(hwndTab);
  d_customTab.setBkFillDib(scenario->getSideBkDIB(SIDE_Neutral));
//  d_customTab.setTabFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground));

  int fontSize = (6*dbY)/8;
  HFONT hFont = FontManager::getFont(scenario->fontName(Font_Decorative), fontSize);
  d_customTab.setFont(hFont);

  setPosition();

  return TRUE;
}

void UnitActivityPage::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  LONG dbUnits = GetDialogBaseUnits();
  const LONG dbY = HIWORD(dbUnits);

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);

  switch(lpDrawItem->CtlID)
  {
    case UAP_MODETIMETEXT:
    {
      RECT r;
      GetWindowRect(lpDrawItem->hwndItem, &r);
      PixelPoint p(r.left, r.top);

      owner()->dib()->rect(0, 0, cx, cy, scenario->getSideBkDIB(SIDE_Neutral));
//    CampaignUserInterface::transferBits(owner()->dib(), p, cx, cy, 10, 10);
      CampaignUserInterface::drawText(hwnd, lpDrawItem, owner()->dib(), d_modeTextFillDib, True);
      break;
    }

    case UAP_ACTIVITYTEXT:
      drawOrderDib(cx, cy);
      CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}


void UnitActivityPage::drawOrderDib(int cx, int cy)
{
  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  ASSERT(d_orderTextFillDib);

  /*
   * Fill in background
   */

  owner()->dib()->rect(0, 0, cx, cy, d_orderTextFillDib);

  UnitInfoDib::drawActivityDib(campData(), owner()->dib(), d_order, dbX, dbY);
}


void UnitActivityPage::initControls()
{
  ICommandPosition cpi = owner()->currentUnit();
  if(cpi != NoCommandPosition)
  {
    ICommandPosition topCPI = campData()->getArmies().getTopParent(cpi);

    CommandPosition* cp = campData()->getCommand(topCPI);

    /*
     * Set mode text
     */

    SetDlgItemText(getHWND(), UAP_MODETIMETEXT, cp->getActionDescription((OD_TYPE)0, campData()->getTick()));
    cp->clearActionDescription();

    /*
     * Add any needed tabs
     */

    HWND hwndTab = d_customTab.tabHWND();
    int nTabs = TabCtrl_GetItemCount(hwndTab);

    // delete prior tabs(except current order)
    while(--nTabs)
    {
      TabCtrl_DeleteItem(hwndTab, nTabs);
    }

    CampaignOrderList& list = cp->getOrders();

    // see if we need OrderInRoute tab
    if(list.entries() > 0)
    {
      TC_ITEM tie;
      tie.mask = TCIF_TEXT | TCIF_PARAM;
      tie.lParam = OrderPages::OrderInRoute;

      tie.pszText = const_cast<char*>(s_strings.get(UnitDialogStrings::InRoute));
      TabCtrl_InsertItem(hwndTab, ++nTabs, &tie);
    }

    // see if we need ActioningOrder tab
    if(list.isActing())
    {
      ASSERT(list.getActioningOrder());
      TC_ITEM tie;

      tie.mask = TCIF_TEXT | TCIF_PARAM;
      tie.lParam = OrderPages::ActioningOrder;

      tie.pszText = const_cast<char*>(s_strings.get(UnitDialogStrings::Actioning));
      TabCtrl_InsertItem(hwndTab, ++nTabs, &tie);
    }

    TabCtrl_SetCurSel(hwndTab, OrderPages::First);

    /*
     * Set Action text
     */

    onSelChanged();
  }
}

LRESULT UnitActivityPage::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{

   // Assume its from the tabbed dialog

   if(lpNMHDR->idFrom == UAP_TABBED)
   {
      switch(lpNMHDR->code)
      {
      case TCN_SELCHANGING:
         return FALSE;

      case TCN_SELCHANGE:
         onSelChanged();
         break;
      }
   }

   return TRUE;
}

void UnitActivityPage::onSelChanged()
{

  ICommandPosition cpi = owner()->currentUnit();
  if(cpi != NoCommandPosition)
  {

    int iSel = TabCtrl_GetCurSel(d_customTab.tabHWND());
    TC_ITEM tci;

    tci.mask = TCIF_PARAM;

    TabCtrl_GetItem(d_customTab.tabHWND(), iSel, &tci);

    d_page = static_cast<OrderPages::Page>(tci.lParam);

    Boolean redraw = False;
    switch(d_page)
    {
      case OrderPages::CurrentOrder:
      {
        /*
         * Only top parent has a current order
         */

        CommandPosition* cp = campData()->getCommand(campData()->getArmies().getTopParent(cpi));
        CampaignOrderList& list = cp->getOrders();;

        d_order = list.getCurrentOrder();
        redraw = True;
        break;
      }

      case OrderPages::OrderInRoute:
      {
        CommandPosition* cp = campData()->getCommand(cpi);
        CampaignOrderList& list = cp->getOrders();;

        if(list.entries() > 0)
        {
          d_order = list.getLastOrder();
          redraw = True;
        }

        break;
      }

      case OrderPages::ActioningOrder:
      {
        CommandPosition* cp = campData()->getCommand(cpi);
        CampaignOrderList& list = cp->getOrders();;

        if(list.isActing())
        {
          ASSERT(list.getActioningOrder());

          d_order = list.getActioningOrder();
          redraw = True;
        }
        break;
      }
    }

    if(redraw)
    {
//    drawOrderDib();
      HWND hText = GetDlgItem(getHWND(), UAP_ACTIVITYTEXT);
      ASSERT(hText != 0);

      InvalidateRect(hText, NULL, FALSE);
    }
  }
}

/*----------------------------------------------------------------------------
 * Summary Page
 */

class UnitSummaryPage : public UnitDialogPage {
   const DrawDIB* d_textFillDib;
// DrawDIBDC* d_textDib;

public:
   UnitSummaryPage(const CampaignData* campData, UnitDialog* p) :
     UnitDialogPage(campData, p, unitSummaryPage),
     d_textFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)) {}

    ~UnitSummaryPage();

   const char* getTitle() const { return s_strings.get(UnitDialogStrings::Summary); }
private:
   void initControls() {}
   void updateValues() {}
   void enableControls() {}

// void drawSummaryText(const DRAWITEMSTRUCT* lpDrawItem);
   void drawSummaryDib(int cx, int cy);

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
};

UnitSummaryPage::~UnitSummaryPage()
{
}

BOOL UnitSummaryPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL UnitSummaryPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  setPosition();
  return TRUE;
}

void UnitSummaryPage::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  LONG dbUnits = GetDialogBaseUnits();
  LONG dbY = HIWORD(dbUnits);

  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);

  if(lpDrawItem->CtlID == USP_SUMMARYTEXT)
  {
    drawSummaryDib(cx, cy);
    CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

void UnitSummaryPage::drawSummaryDib(int cx, int cy)
{
  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  ASSERT(d_textFillDib);

  /*
   * Fill in background
   */

  owner()->dib()->rect(0, 0, cx, cy, d_textFillDib);

  ICommandPosition cpi = owner()->currentUnit();
  if(cpi != NoCommandPosition)
  {
    UnitInfoDib::drawSummaryDib(campData(), owner()->dib(), cpi, dbX, dbY);
  }
}

/*----------------------------------------------------------------------
 * Leader Page
 */

class UnitLeaderPage : public UnitDialogPage {
   const DrawDIB* d_leaderTextFillDib;
public:
   UnitLeaderPage(const CampaignData* campData, UnitDialog* p) :
     UnitDialogPage(campData, p, unitLeaderPage),
     d_leaderTextFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)) {}

   ~UnitLeaderPage();

   const char* getTitle() const { return s_strings.get(UnitDialogStrings::Leader); }
private:
   void initControls() {}
   void updateValues() {}
   void enableControls() {}

   void drawLeaderDib(int cx, int cy);
   void drawLeaderText(const DRAWITEMSTRUCT* lpDrawItem);

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
};

UnitLeaderPage::~UnitLeaderPage()
{
}

BOOL UnitLeaderPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL UnitLeaderPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  setPosition();
  return TRUE;
}

void UnitLeaderPage::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  owner()->allocateStaticDib(cx, cy);
  ASSERT(owner()->dib());

  LONG dbUnits = GetDialogBaseUnits();
  LONG dbY = HIWORD(dbUnits);
  int fontHeight = (dbY*7)/8;

  LogFont lf;
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  owner()->dib()->setFont(font);

  switch(lpDrawItem->CtlID)
  {
    case ULP_LEADERTEXT:
      drawLeaderDib(cx, cy);
      CampaignUserInterface::blitDibToControl(owner()->dib(), lpDrawItem);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

void UnitLeaderPage::drawLeaderDib(int cx, int cy)
{
  /*
   * Some useful values
   * Note: All positioning is done with dialog units
   */

  static LONG dbUnits = GetDialogBaseUnits();

  const LONG dbX = LOWORD(dbUnits);
  const LONG dbY = HIWORD(dbUnits);

  ASSERT(d_leaderTextFillDib);

  /*
   * Fill in background
   */

  owner()->dib()->rect(0, 0, cx, cy, d_leaderTextFillDib);

  ICommandPosition cpi = owner()->currentUnit();
  if(cpi != NoCommandPosition)
  {
    UnitInfoDib::drawLeaderDib(campData(), owner()->dib(), cpi, dbX, dbY);
  }
}

/*
 * Order of Battle Page
 */

class UnitOBPage : public UnitDialogPage {
   ReorganizeDialog* d_orgWin;
public:
   UnitOBPage(const CampaignData* campData, UnitDialog* p) :
     UnitDialogPage(campData, p, unitOBPage),
     d_orgWin(0) {}

    ~UnitOBPage();

   const char* getTitle() const { return s_strings.get(UnitDialogStrings::OB); }
private:

   void initControls(); // {}
   void updateValues() {}
   void enableControls() {}

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
};

UnitOBPage::~UnitOBPage()
{
  if(d_orgWin)
    DestroyWindow(d_orgWin->getHWND());
}

BOOL UnitOBPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
#if 0
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
#endif
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL UnitOBPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  customDialog().init(hwnd);

  /*
   * Create Unit reorganize window
   */

  RECT r;
  GetClientRect(getHWND(), &r);

  d_orgWin = new ReorganizeDialog(getHWND(), campData(), r);
  ASSERT(d_orgWin);

  setPosition();
  return TRUE;
}

void UnitOBPage::initControls()
{
  ICommandPosition cpi = owner()->currentUnit();
  if(cpi != NoCommandPosition)
  {
    ASSERT(d_orgWin);

    d_orgWin->initUnits(owner()->getUnitList());
  }
}

/*
 * Unit Dialog allocation ----------------
 */

UnitDialogPage* UnitDialogPage::createPage(UnitDialogPage::Page page, const CampaignData* campData, UnitDialog* p)
{
  ASSERT(page < HowMany);

  if(page == OrderPage)
    return new UnitOrderPage(campData, p);
  else if(page == ActivityPage)
    return new UnitActivityPage(campData, p);
  else if(page == SummaryPage)
    return new UnitSummaryPage(campData, p);
  else if(page == LeaderPage)
    return new UnitLeaderPage(campData, p);
  else
    return new UnitOBPage(campData, p);
}

/*---------------------------------- UnitOrderDial --------------------------
 */

/*
 * Main Dialog
 */

class UnitOrderDial : public UnitDialog  {
    const DrawDIB* d_fillDib;
//  DrawDIBDC* d_itemDib;
    CustomDrawnTab d_customTab;
    UnitDialogPage* d_pages[UnitDialogPage::HowMany];
    UnitDialogPage::Page d_currentPage;
    Boolean d_hasOrderPage;

    enum { UID_TABBED = 200 };
  public:
    UnitOrderDial(CampaignUserInterface* owner, const CampaignData* campData, CompleteOrder& order, CampaignComboInterface& combos);
    ~UnitOrderDial();

    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void initControls();
    void enableControls();
    void redrawCombo();

  private:
    BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
    void onDestroy(HWND hwnd) {}
    void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
    LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);

    void setTabs();
    void onSelChanged();
    void showOrderPage(const PixelPoint& p);
    void show(const PixelPoint& p);
};

UnitOrderDial::UnitOrderDial(CampaignUserInterface* owner, const CampaignData* campData, CompleteOrder& order, CampaignComboInterface& combos) :
  UnitDialog(owner, campData, order, combos),
  d_fillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
  d_currentPage(UnitDialogPage::OrderPage),
  d_hasOrderPage(True)
{
  for(int i = 0; i < UnitDialogPage::HowMany; i++)
  {
    d_pages[i] = 0;
  }

  HWND dhwnd = createDialog(unitOrderDialog, APP::getMainHWND(), False);
  ASSERT(dhwnd != NULL);
}

UnitOrderDial::~UnitOrderDial()
{
  deleteStaticDib();
}

BOOL UnitOrderDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;
      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;
      default:
         return FALSE;
   }

   return TRUE;
}

BOOL UnitOrderDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

  customDialog().setBkDib(scenario->getSideBkDIB(SIDE_Neutral));
  customDialog().setCaptionBkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  customDialog().init(hwnd);

  // get base font size (in dialog units)
  const LONG dbUnits = GetDialogBaseUnits();
  const LONG dbY = HIWORD(dbUnits);

  /*
   * Allocate dib for drawing unitlist combo
   */


  int fontSize = (6*dbY)/8;
  HFONT hFont = FontManager::getFont(scenario->fontName(Font_Decorative), fontSize);

  /*
   * Create unitlist combo
   * Note: this is shared by this dialog and the pop-up move-order dialog
   */

  HWND hCombo = GetDlgItem(hwnd, UID_UNITS);
  ASSERT(hCombo);

  RECT cRect;
  GetWindowRect(hCombo, &cRect);

  SIZE s;
  s.cx = cRect.right - cRect.left;
  s.cy = cRect.bottom - cRect.top;

  combos().create(hwnd, CampaignComboInterface::UnitList, UID_UNITLIST,
     campData(), s, ItemWindowData::HasScroll | ItemWindowData::ShadowBackground);

  /*
   * Create tabbed pages
   */

  for(int i = 0; i < UnitDialogPage::HowMany; i++)
  {
    d_pages[i] = UnitDialogPage::createPage(static_cast<UnitDialogPage::Page>(i), campData(), this);
    ASSERT(d_pages[i] != 0);
  }


  /*
   * Set Position of page
   */

  // top left corner of page (in dialog units)
  const LONG baseX = 3;
  const LONG baseY = 32;

  // get size of dialog(use first page)
  // this is in dialog units
  DLGTEMPLATE* dialog = d_pages[UnitDialogPage::First]->getDialog();

  RECT& r = tabbedDialogRect();
  SetRect(&r, baseX, baseY, baseX+dialog->cx, baseY+dialog->cy);

  // convert dialog units to pixel coordinates
  MapDialogRect(hwnd, &r);

  /*
   * Create a tabbed window
   */

  HWND hwndTab = CreateWindow(
      WC_TABCONTROL, "",
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
         TCS_TOOLTIPS | TCS_FIXEDWIDTH,
      0, 0, r.right-r.left, 100,
      hwnd,
      (HMENU) UID_TABBED,
      APP::instance(),
      NULL);

   ASSERT(hwndTab != NULL);

   /*
    * Set up the tabbed titles
    *
    * This must be done before using AdjustRect
    */

   TC_ITEM tie;
   tie.mask = TCIF_TEXT | TCIF_PARAM;

   for(i = 0; i < UnitDialogPage::HowMany; i++)
   {
      tie.pszText = (char*) d_pages[i]->getTitle();

      tie.lParam = i;
      TabCtrl_InsertItem(hwndTab, i, &tie);
   }

   /*
    * Set size of tabs (in Dialog Units)
    */

   const LONG tabWidth = 40;
   const LONG tabHeight = 13;

   RECT tRect;
   SetRect(&tRect, 0, 0, tabWidth, tabHeight);

   MapDialogRect(hwnd, &tRect);

   TabCtrl_SetItemSize(hwndTab, tRect.right, tRect.bottom);


   /*
    * Convert coordinates to pixel coordinates
    * and Move the window
    */

  // Work out complete size with tabs, and shift to desired location
  TabCtrl_AdjustRect(hwndTab, TRUE, &r);

  // Move tabbed Window
  SetWindowPos(hwndTab, NULL, r.left, r.top, r.right - r.left, r.bottom - r.top, SWP_NOZORDER);

  // Get display area
  TabCtrl_AdjustRect(hwndTab, FALSE, &r);    // Get display area

  /*
   * Initialize custom drawn tab class
   */

  d_customTab.tabHWND(hwndTab);
  d_customTab.setBkFillDib(scenario->getSideBkDIB(SIDE_Neutral));
  hFont = FontManager::getFont(scenario->fontName(Font_Decorative), fontSize);
  d_customTab.setFont(hFont);

  /*
   * Initialize tabbed pages
   */

  for(i = 0; i < UnitDialogPage::HowMany; i++)
  {
    d_pages[i]->create();
  }

  onSelChanged();

  // buttons
  for(int id = UID_BUTTONID_FIRST; id < UID_BUTTONID_LAST; id++)
  {
    CustomButton cb(hwnd, id);
    cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
    cb.setBorderColours(scenario->getBorderColors());
  }

  /*
   * Add tooltips
   */

  static TipData tipData[] = {
    { UID_UNITS,    TTS_UID_UNITS,    SWS_UID_UNITS     },
    { UID_SHOWALL,  TTS_UID_SHOWALL,  SWS_UID_SHOWALL   },
    { UID_SEND,     TTS_UID_SEND,     SWS_UID_SEND      },
    { UID_CANCEL,   TTS_UID_CANCEL,   SWS_UID_CANCEL    },
    { UID_CLOSE,    TTS_UID_CLOSE,    SWS_UID_CLOSE     },
    EndTipData
  };

  g_toolTip.addTips(hWnd, tipData);


  return True;
}

void UnitOrderDial::initControls()
{

  setTabs();

  d_pages[d_currentPage]->initControls();

  enableControls();
}

void UnitOrderDial::setTabs()
{
  /*
   * Add or remove 'Orders', 'Actions', and 'OB' tabs depending on whether unit is orderable
   */

  ICommandPosition cpi = currentUnit();
  if(cpi != NoCommandPosition)
  {
    if(CampaignArmy_Util::isUnitOrderable(cpi))
    {
      if(!d_hasOrderPage)
      {
        TC_ITEM tie;
        tie.mask = TCIF_TEXT | TCIF_PARAM;

        for(int i = UnitDialogPage::PlayerUnitOnly_First; i < UnitDialogPage::HowMany; i++)
        {
          tie.lParam = i;

          tie.pszText = (char*) d_pages[i]->getTitle();
          TabCtrl_InsertItem(d_customTab.tabHWND(), i, &tie);
        }

        d_hasOrderPage = True;
      }
    }
    else
    {
      if(d_hasOrderPage)
      {
        for(int i = UnitDialogPage::PlayerUnitOnly_First; i < UnitDialogPage::HowMany; i++)
        {
          TabCtrl_DeleteItem(d_customTab.tabHWND(), UnitDialogPage::PlayerUnitOnly_First);
        }

        d_hasOrderPage = False;
      }
    }
  }
}

void UnitOrderDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case UID_UNITS:
    {
      if(codeNotify == STN_CLICKED)
      {
        showComboList(hwndCtl);
      }
      break;
    }

    case UID_UNITLIST:
    {
      owner()->syncCombos(0);
      initControls();
      break;
    }

    case UID_SEND:
    {
      d_pages[UnitDialogPage::OrderPage]->updateValues();

      owner()->sendOrder(currentUnit());
      break;
    }

    case UID_CANCEL:
    {
      d_pages[UnitDialogPage::OrderPage]->updateValues();

      owner()->cancelOrder();
      break;
    }

    case UID_CLOSE:
    {
      d_pages[UnitDialogPage::OrderPage]->updateValues();

      owner()->cancelOrder();
      break;
    }

    case UID_SHOWALL:
    {
      if(!unitListExpanded())
      {
        owner()->initUnits(True);

        HWND hControl = GetDlgItem(hwnd, UID_UNITS);
        ASSERT(hControl);

        showComboList(hControl);
        enableControls();
      }

      break;
    }
  }
}

void UnitOrderDial::redrawCombo()
{
  HWND h = GetDlgItem(hWnd, UID_UNITS);
  ASSERT(h);

  InvalidateRect(h, NULL, FALSE);
}


void UnitOrderDial::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  switch(lpDrawItem->CtlID)
  {
    case UID_UNITS:
      drawCombo(d_fillDib, lpDrawItem, True);
      break;
  }

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

LRESULT UnitOrderDial::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{

   // Assume its from the tabbed dialog

   if(lpNMHDR->idFrom == UID_TABBED)
   {
      switch(lpNMHDR->code)
      {
      case TCN_SELCHANGING:
         return FALSE;

      case TCN_SELCHANGE:
         onSelChanged();
         break;
      }
   }

   return TRUE;
}

void UnitOrderDial::onSelChanged()
{
  ICommandPosition cpi = currentUnit();

  if(cpi != NoCommandPosition)
  {
    int iSel = TabCtrl_GetCurSel(d_customTab.tabHWND());
    TC_ITEM tci;

    tci.mask = TCIF_PARAM;

    TabCtrl_GetItem(d_customTab.tabHWND(), iSel, &tci);

    UnitDialogPage::Page newPage = static_cast<UnitDialogPage::Page>(tci.lParam);

    if(d_currentPage != newPage)
      d_pages[d_currentPage]->disable();

    d_pages[newPage]->enable();

    d_currentPage = newPage;
    initControls();
  }
}

void UnitOrderDial::showOrderPage(const PixelPoint& p)
{
  setTabs();

  if(d_currentPage != UnitDialogPage::OrderPage)
    d_pages[d_currentPage]->disable();

  d_currentPage = UnitDialogPage::OrderPage;

  d_pages[d_currentPage]->enable();

  TabCtrl_SetCurSel(d_customTab.tabHWND(), UnitDialogPage::OrderPage);

  d_pages[d_currentPage]->initControls();

  enableControls();
//  onSelChanged();
  UnitDialog::show(p);
}

void UnitOrderDial::show(const PixelPoint& p)
{
  TabCtrl_SetCurSel(d_customTab.tabHWND(), UnitDialogPage::First);

  onSelChanged();
  UnitDialog::show(p);
}

void UnitOrderDial::enableControls()
{
  ICommandPosition cpi = currentUnit();
  Boolean underSiege = False;

  /*
   * If under siege we can't do anything
   */

  if(cpi != NoCommandPosition)
  {
    CommandPosition* cp = campData()->getCommand(cpi);
    if(cp->isGarrison())
    {
      ASSERT(cp->atTown());

      Town& t = campData()->getTown(cp->getTown());

      underSiege = t.isSieged();
    }
  }

  {
    CustomButton b(getHWND(), UID_SEND);
    b.enable( !underSiege && (d_currentPage == UnitDialogPage::OrderPage) );
  }

  {
    CustomButton b(getHWND(), UID_CANCEL);
    b.enable( (d_currentPage == UnitDialogPage::OrderPage) );
  }

  {
    CustomButton b(getHWND(), UID_SHOWALL);
    b.enable(!unitListExpanded());
  }
}

/* ------------------------------ Dialog Creation function ---------------
 *
 */

UnitDialog* UnitDialog::create(UnitDialogType::Type type, CampaignUserInterface* owner, const CampaignData* campData, CompleteOrder& order, CampaignComboInterface& combos)
{
  ASSERT(type == UnitDialogType::MoveOrderDialog || type == UnitDialogType::MainOrderDialog);

  if(type == UnitDialogType::MoveOrderDialog)
  {
    return new MoveOrderDial(owner, campData, order, combos);
  }
  else
  {
    return new UnitOrderDial(owner, campData, order, combos);
  }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
