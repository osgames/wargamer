/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Order of Battle Window
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "obwin.hpp"
#include "app.hpp"
#include "cwin_int.hpp"
#include "resdef.h"
#include "campdint.hpp"
#include "scenario.hpp"
#include "armies.hpp"
#include "options.hpp"
#include "alertbox.hpp"
#include "scn_res.h"
#include "town.hpp"
#include "wmisc.hpp"
#include "resdef.h"
#include "resstr.hpp"


OBTreeView::OBTreeView()
{
  cpi = NoCommandPosition;
  side = SIDE_Neutral;
  iTown = NoTown;
  imgList = 0;
  independentLeaders = False;
  hTreeItem = 0;
}

OBTreeView::OBTreeView(ICommandPosition from)
{
  cpi = from;
  const CommandPosition* cp = campaignData->getCommand(cpi);
  side = cp->getSide();
  iTown = NoTown;
  imgList = 0;
  independentLeaders = True;
  hTreeItem = 0;
}


const char OBWindow::s_className[] = "OBWindow";

ATOM OBWindow::classAtom = 0;
static const char obWindowRegName[] = "OBWindow";

ATOM OBWindow::registerClass()
{
   if(!classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_OWNDC | CS_DBLCLKS;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(OBWindow*);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = (HBRUSH) (1 + COLOR_3DFACE);
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;
      classAtom = RegisterClass(&wc);
   }

   ASSERT(classAtom != 0);

   return classAtom;
}

OBWindow::OBWindow(CampaignWindowsInterface* game, HWND parent) :
   d_campWind(game)
{
   debugLog("OBWindow::create\n");

   hdc = 0;

   createdFlag = FALSE;
   obWinType = OBW_MENU;

   hwndParent = parent;


   registerClass();

   HWND hWnd = createWindow(
      WS_EX_CONTEXTHELP,
         NULL,
         WS_POPUP |
         WS_CAPTION |
         WS_BORDER |
         WS_SYSMENU |
         WS_VISIBLE |
         WS_SIZEBOX |
         WS_MINIMIZEBOX |
         WS_CLIPSIBLINGS,     /* Style */
      10,                     /* init. x pos */
      60,                     /* init. y pos */
      200,                    /* init. x size */
      200,                    /* init. y size */
      parent,                 /* parent window */
      NULL
   );

   ASSERT(hWnd != NULL);

      SetWindowPos(hWnd, HWND_TOP, 0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
   SetWindowText(hWnd, InGameText::get(IDS_OB_Window));
}

OBWindow::OBWindow(CampaignWindowsInterface* cwind, HWND parent, OrderTB* get, ICommandPosition from) :
   OBTreeView(from),
   d_campWind(cwind)
{
   debugLog("OBWindow::create\n");

   hdc = 0;

   hwndParent = parent;

   createdFlag = FALSE;
   obWinType = OBW_DIALOG;

   HWND hWnd = createWindow(
      WS_EX_CONTEXTHELP,
         NULL,
         WS_POPUP |
         WS_CAPTION |
         WS_BORDER |
         WS_SYSMENU |
         WS_VISIBLE |
         WS_SIZEBOX |
         WS_MINIMIZEBOX |
         WS_CLIPSIBLINGS,     /* Style */
      10,                     /* init. x pos */
      60,                     /* init. y pos */
      200,                    /* init. x size */
      200,                    /* init. y size */
      parent,                 /* parent window */
      NULL
   );

   ASSERT(hWnd != NULL);

   SetWindowPos(hWnd, HWND_TOP, 0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
   SetWindowText(hWnd, InGameText::get(IDS_OB_Window));
}


LRESULT OBWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   debugLog("OBWindow::procMessage(%p, %u, %d, %ld)\n",
      hWnd, msg, (int) wParam, (long) lParam);

   LRESULT l;
   if(handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      HANDLE_MSG(hWnd, WM_NOTIFY,   onNotify);

      HANDLE_MSG(hWnd, WM_SIZE,     onSize);
      HANDLE_MSG(hWnd, WM_COMMAND, onCommand);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL OBWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
   debugLog("OBWindow::onCreate(%p)\n", hWnd);

   hdc = GetDC(hWnd);

   if(obWinType == OBW_MENU)
     alterWindowSize(hWnd, 200, 300, lpCreateStruct);

   /*
    * Put OK and cancel buttons at bottom
    */

   HWND hwndButton = CreateWindow(
      "BUTTON", InGameText::get(IDS_OK), WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
         100/3 - 32, 276, 64, 20,
         hWnd, (HMENU) IDOB_OK, wndInstance(hWnd), NULL);

   ASSERT(hwndButton != NULL);

   if(obWinType == OBW_DIALOG) {
     hwndButton = CreateWindow(
        "BUTTON", InGameText::get(IDS_Detach), WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,
        120, 276, 64, 20,
        hWnd, (HMENU) IDOB_DETACH, wndInstance(hWnd), NULL);

     ASSERT(hwndButton != NULL);
   }

   if(obWinType == OBW_MENU) {
     hwndButton = CreateWindow(
        "BUTTON", InGameText::get(IDS_UI_ORDERS), WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,
        100 - (100/3 - 32), 276, 64, 20,
        hWnd, (HMENU) IDOB_ORDERS, wndInstance(hWnd), NULL);

     ASSERT(hwndButton != NULL);

     hwndButton = CreateWindow(
        "BUTTON", "L", WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,
        200 - 20 - 4, 276, 20, 20,
        hWnd, (HMENU) IDOB_ILEADERS, wndInstance(hWnd), NULL);

     ASSERT(hwndButton != NULL);
   }



   /*
    * Create the treeview
    */

   HWND hwndTV = createTV(hWnd, IDOB_TREEVIEW);
   /*
    * Set up image list
    */
   createImageList(hWnd);
   TreeView_SetImageList(hwndTV, getImgList(), TVSIL_NORMAL);

   /*
    * Add items to the tree view
    */

   if(obWinType == OBW_MENU) {
     addItems(hwndTV);
   }
   else {
     addDetachItems(hwndTV);
     SetWindowPos(hWnd, HWND_TOP, 200,200,300,150, SWP_SHOWWINDOW);
     SetWindowText(hWnd, InGameText::get(IDS_DetachUnits));
   }
   createdFlag = TRUE;

   debugLog("OBWindow::onCreate()... End\n");

   return TRUE;
}

void OBWindow::onDestroy(HWND hWnd)
{
#ifdef DEBUG
   debugLog("OBWindow::onDestroy(%p)\n", hWnd);
#endif

   FORWARD_WM_PARENTNOTIFY(hwndParent, WM_DESTROY, hWnd, 0, SendMessage);
   d_campWind->obWindowUpdated();

   if(hdc)
   {
      ReleaseDC(hWnd, hdc);
      hdc = NULL;
   }
}


void OBWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
   switch(id)
   {
   case IDOB_OK:
      if(codeNotify == BN_CLICKED)
         DestroyWindow(hWnd);
#ifdef DEBUG
      else
         debugLog("OBWindow::onCommand codeNotify is not BN_CLICKED %u\n", codeNotify);
#endif
      break;

   case IDOB_ORDERS:
      if(codeNotify == BN_CLICKED)

         if(getCPI() != NoCommandPosition)
           orderUnit(0, NM_DBLCLK);
#ifdef DEBUG
      else
         debugLog("OBWindow::onCommand codeNotify is not BN_CLICKED %u\n", codeNotify);
#endif
      break;


   case IDOB_DETACH:
      if(codeNotify == BN_CLICKED)
         detachUnit(hWnd);
#ifdef DEBUG
      else
         debugLog("OBWindow::onCommand codeNotify is not BN_CLICKED %u\n", codeNotify);
#endif
      break;

   case IDOB_ILEADERS:
      if(codeNotify == BN_CLICKED)
      {
         if(getIndependentLeaders())
           setIndependentLeaders(False);
         else
           setIndependentLeaders(True);

         HWND hwndTV = GetDlgItem(hWnd, IDOB_TREEVIEW);
         ASSERT(hwndTV != 0);
         TreeView_DeleteAllItems(hwndTV);

         if(obWinType == OBW_MENU)
           addItems(hwndTV);
         else
           addDetachItems(hwndTV);

         RECT r;
         GetClientRect(hWnd, &r);
         onSize(hWnd, NULL, r.right - r.left, r.bottom - r.top);

      }
#ifdef DEBUG
      else
         debugLog("OBWindow::onCommand codeNotify is not BN_CLICKED %u\n", codeNotify);
#endif
      break;

#ifdef DEBUG
   default:
      debugLog("OBWindow::onCommand(), unknown ID %d\n", id);
#endif
   }
}


void OBWindow::onSize(HWND hwnd, UINT state, int cx, int cy)
{
  if(createdFlag) {

    HWND hButton = GetDlgItem(hwnd, IDOB_OK);
    ASSERT(hButton != 0);
    SetWindowPos(hButton, HWND_TOP, cx/3 - 32, cy - 24, 64, 20, SWP_SHOWWINDOW);

    if(obWinType == OBW_MENU)
      hButton = GetDlgItem(hwnd, IDOB_ORDERS);
    else
      hButton = GetDlgItem(hwnd, IDOB_DETACH);
    ASSERT(hButton != 0);
    SetWindowPos(hButton, HWND_TOP, (cx - cx/3) - 32, cy - 24, 64, 20, SWP_SHOWWINDOW);

    if(obWinType == OBW_MENU)
    {
      hButton = GetDlgItem(hwnd, IDOB_ILEADERS);
      ASSERT(hButton != 0);
      SetWindowPos(hButton, HWND_TOP, cx - 24, cy - 24, 20, 20, SWP_SHOWWINDOW);
    }

    HWND hTree = GetDlgItem(hwnd, IDOB_TREEVIEW);
    ASSERT(hTree != 0);
    SetWindowPos(hTree, HWND_TOP, 0, 0, cx, cy - 28, SWP_SHOWWINDOW);
    RECT r;
    r.left = 0;
    r.top = cy - 24;
    r.right = cx;
    r.bottom = cy;
    InvalidateRect(hwnd, &r, TRUE);
  }
}


HWND OBTreeView::createTV(HWND hwnd, int id)
{
   HWND hwndTV = CreateWindowEx(0, "SysTreeView32"/*WC_TREEVIEW*/, "OB Tree View",
      /*WS_VISIBLE |*/ WS_CHILD | WS_BORDER |
      TVS_HASLINES | TVS_HASBUTTONS | TVS_LINESATROOT,
      0, 0, 0, 0,
      hwnd, (HMENU) id, wndInstance(hwnd), NULL);

   ASSERT(hwndTV != NULL);
   return hwndTV;
}

void OBTreeView::createImageList(HWND hwnd)
{
   imgList = ImageList_LoadBitmap(
      scenario->getScenarioResDLL(),
      MAKEINTRESOURCE(BM_OBICONS),
      OBI_CX,
      0,
      CLR_NONE);
   ASSERT(imgList != NULL);
}

void OBWindow::detachUnit(HWND hWnd)
{
//   int result = IDYES;
//   if(Options::get(OPT_AlertBox) && AlertBoxOptions::get(AB_DetachUnit))
//   {
//      long get = alertBox(hWnd, InGameText::get(IDS_SureDetachUnit),
//                         InGameText::get(IDS_DetachUnit), MB_YESNO);
//      result = LOWORD(get);
//      AlertBoxOptions::set(AB_DetachUnit, (Boolean)HIWORD(get));
//   }

  int result = alertBox(hWnd, InGameText::get(IDS_SureDetachUnit),
                        InGameText::get(IDS_DetachUnit), MB_YESNO,
                        AB_DetachUnit,
                        IDYES);

  switch(result) {
    case IDYES:
      orderUnit(0, NM_DBLCLK);
      break;
    case IDNO:
      break;
  }
}

void OBWindow::orderUnit(unsigned int id, unsigned int code)
{
#if !defined(EDITOR)
   if(code == NM_DBLCLK)
   {
      ICommandPosition cpi = getCPI();
      if(cpi != NoCommandPosition)
      {
         d_campWind->orderUnit(cpi);
         DestroyWindow(getHWND());
      }
   }
#endif   // !EDITOR
}


/*
 * Add Items to Treeview control
 */


void OBTreeView::addItems(HWND hwndTV)
{
   TV_INSERTSTRUCT tvi;

   tvi.hInsertAfter = TVI_LAST;

   const Armies& armies = campaignData->getArmies();

   /*
    * For each Side
    */

   NationIter niter = &armies;
   while(++niter)
   {
      Side side = niter.current();

      tvi.hParent = TVI_ROOT;
      tvi.item.pszText = LPSTR_TEXTCALLBACK;
      ICommandPosition cpSide = armies.getPresident(side);
      tvi.item.lParam = cpToLPARAM(cpSide);

      tvi.item.state = TVIS_BOLD | TVIS_EXPANDED;
      tvi.item.stateMask = TVIS_BOLD | TVIS_EXPANDED;
      tvi.item.iImage = side * OBI_IconsPerSide + OBI_Side;
      tvi.item.iSelectedImage = side * OBI_IconsPerSide + OBI_SideSelected;
      tvi.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_STATE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;

      HTREEITEM tiSide = TreeView_InsertItem(hwndTV, &tvi);

      tvi.item.state &= ~(TVIS_BOLD | TVIS_EXPANDED);

      /*
       * For each army
       *
       * This could be better done with a recursive algorithm
       * or a stack.
       */

      ICommandPosition iArmy = armies.getFirstUnit(side);      // Start with 1st unit
      while(iArmy != NoCommandPosition)
      {
         if((!getIndependentLeaders() && armies.isRealUnit(iArmy)) ||
            getIndependentLeaders())
         {
           addUnit(hwndTV, tvi, iArmy, tiSide);
         }
         const CommandPosition* cp = armies.getCommand(iArmy);
         iArmy = cp->getSister();
      }
   }
}

void OBTreeView::addUnit(HWND hwndTV, TV_INSERTSTRUCT& tvi, ICommandPosition cpi, HTREEITEM hti)
{
   const Armies& armies = campaignData->getArmies();

   const CommandPosition* cp = armies.getCommand(cpi);
   Side side = cp->getSide();

   tvi.hParent = hti;
   tvi.item.lParam = cpToLPARAM(cpi);


   RankEnum rank = armies.getRank(cpi).getRankEnum();

   switch (rank)
   {
      case Rank_God:
      case Rank_President:
      case Rank_Army:
      case Rank_ArmyGroup:
         tvi.item.iImage = side * OBI_IconsPerSide + OBI_Army;
         tvi.item.iSelectedImage = side * OBI_IconsPerSide + OBI_ArmySelected;
         break;
      case Rank_Corps:
         tvi.item.iImage = side * OBI_IconsPerSide + OBI_Corps;
         tvi.item.iSelectedImage = side * OBI_IconsPerSide + OBI_CorpsSelected;
         break;
      case Rank_Division:
         tvi.item.iImage = side * OBI_IconsPerSide + OBI_Division;
         tvi.item.iSelectedImage = side * OBI_IconsPerSide + OBI_DivisionSelected;
         break;
      default:
#ifdef DEBUG
         throw GeneralError("Missing Rank (%d) in OBWindow::addUnit()\n", (int) rank);
#else
         tvi.item.iImage = side * OBI_IconsPerSide + OBI_Brigade;
         tvi.item.iSelectedImage = side * OBI_IconsPerSide + OBI_BrigadeSelected;
         break;
#endif

   }

   HTREEITEM ti = TreeView_InsertItem(hwndTV, &tvi);

   tvi.hInsertAfter = TVI_LAST;
   tvi.item.state &= ~(TVIS_BOLD | TVIS_EXPANDED);

   ICommandPosition child = cp->getChild();

   while(child != NoCommandPosition)
   {
      addUnit(hwndTV, tvi, child, ti);
      cp = armies.getCommand(child);
      child = cp->getSister();
   }
}


void OBTreeView::addDetachItems(HWND hwndTV)
{
   TV_INSERTSTRUCT tvi;
   tvi.hInsertAfter = TVI_LAST;
   tvi.item.pszText = LPSTR_TEXTCALLBACK;
   tvi.item.state = TVIS_BOLD | TVIS_EXPANDED;
   tvi.item.stateMask = TVIS_BOLD | TVIS_EXPANDED;
   tvi.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_STATE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
   addUnit(hwndTV, tvi, cpi, TVI_ROOT);
}


#if defined(CUSTOMIZE)
void OBTreeView::addTownItems(HWND hwndTV)
{
   TV_INSERTSTRUCT tvi;

   tvi.hInsertAfter = TVI_LAST;
   tvi.item.pszText = LPSTR_TEXTCALLBACK;
   tvi.item.state = TVIS_BOLD | TVIS_EXPANDED;
   tvi.item.stateMask = TVIS_BOLD | TVIS_EXPANDED;
   tvi.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_STATE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;


   addTown(hwndTV, tvi, TVI_ROOT);
}

#define PROV 0
#define TOWN 1

void OBTreeView::addTown(HWND hwndTV, TV_INSERTSTRUCT& tvi, HTREEITEM hti)
{
   ProvinceList& pl = campaignData->getProvinces();
   ProvinceIter provIter(pl);
   while(++provIter)
   {
     const Province& prov = provIter.current();
     IProvince iProv = provIter.currentID();
     Side side = prov.getSide();

     tvi.hParent = TVI_ROOT;
     tvi.item.lParam = MAKELPARAM(iProv, PROV);
     tvi.item.state = TVIS_BOLD | TVIS_EXPANDED;
     tvi.item.stateMask = TVIS_BOLD;
     tvi.item.iImage = side * OBI_IconsPerSide + OBI_Side;
     tvi.item.iSelectedImage = side * OBI_IconsPerSide + OBI_SideSelected;
     tvi.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_STATE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;

     HTREEITEM hProv = TreeView_InsertItem(hwndTV, &tvi);

     tvi.item.state &= ~(TVIS_BOLD | TVIS_EXPANDED);

     UWORD nTowns = prov.getNTowns();

     ITown stop = ITown(prov.getTown() + prov.getNTowns());

     for(ITown tIndex = prov.getTown(); tIndex < stop; tIndex++)
     {
       Town town = campaignData->getTown(tIndex);
       Side side = town.getSide();
       tvi.hParent = hProv;
       tvi.item.lParam = MAKELPARAM(tIndex, TOWN);
       tvi.item.state = TVIS_BOLD | TVIS_EXPANDED;
       tvi.item.stateMask = TVIS_BOLD;
       tvi.item.iImage = side * OBI_IconsPerSide + OBI_Side;
       tvi.item.iSelectedImage = side * OBI_IconsPerSide + OBI_SideSelected;
       tvi.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_STATE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;

       HTREEITEM hTown = TreeView_InsertItem(hwndTV, &tvi);
     }
   }
}
#endif   // CUSTOMIZE

LRESULT OBTreeView::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
#ifdef DEBUG_TOOLTIP
   debugLog("OBWindow::onNotify(id=%d, hwndFrom=%p, code=%d, id=%d %s)\n",
      id,
      lpNMHDR->hwndFrom,
      lpNMHDR->code,
      lpNMHDR->idFrom,
      getNotifyName(lpNMHDR->code));
#endif

   switch(lpNMHDR->idFrom)
   {
   case IDOB_TREEVIEW:
#if defined(CUSTOMIZE)  // IDOBE Are from OB Editor!
   case IDOBE_UNITS_NAME:
   case IDOBE_UNITS_ATTACHTO:
   case IDOBE_SP_ATTACHTO:
#endif

#ifdef DEBUG
      debugLog("WM_NOTIFY from IDOB_TREEVIEW\n");
#endif



      switch(lpNMHDR->code)
      {
      case TVN_GETDISPINFO:
         {
            TV_DISPINFO* tvdi = (TV_DISPINFO*) lpNMHDR;

            Armies& army = campaignData->getArmies();

            if(tvdi->item.mask & TVIF_TEXT)
            {
               ICommandPosition cpi = lparamToCP(tvdi->item.lParam);

               ASSERT(cpi != NoCommandPosition);

               RankEnum rank = cpi->getRankEnum();
               if(rank == Rank_President)
               {
                  Side side = cpi->getSide();
                  lstrcpy(tvdi->item.pszText, scenario->getSideName(side));
               }
               else
               {
                  ILeader gi = cpi->getLeader();

                  {
                    army.getUnitName(cpi, tvdi->item.pszText);

                    lstrcat(tvdi->item.pszText, ", ");
                    if(gi == NoLeader)
                     lstrcat(tvdi->item.pszText, InGameText::get(IDS_NoLeader));
                    else
                    {
                      lstrcat(tvdi->item.pszText, InGameText::get(IDS_General));
                      lstrcat(tvdi->item.pszText, " ");
                      const Leader* gen = army.getLeader(gi);
                      lstrcat(tvdi->item.pszText, gen->getName());
                    }
                  }
               }
            }
         }
         break;

      case TVN_SELCHANGED:
         {
            NM_TREEVIEW* pnmtv = (NM_TREEVIEW *) lpNMHDR;
            ICommandPosition cpi = lparamToCP(pnmtv->itemNew.lParam);

            hTreeItem = pnmtv->itemNew.hItem;

#ifdef REWRITE_CAMPAIGN_WINDOWS
            /*
             * For debugging, just stick the item's name into
             * a status bar window
             */

            HWND hWndStatus = campWind->getStatusBarHandle();
            ASSERT(hWndStatus != NULL);

            const char* name;

            if(cpi->getRankEnum() == Rank_President)
            {
               cpi = NoCommandPosition;
               side = lparamToSide(pnmtv->itemNew.lParam);
               name = scenario->getSideName(side);
            }
            else
            {
               name = cpi->getNameNotNull();
            }
            orderUnit(lpNMHDR->idFrom, lpNMHDR->code);
            SendMessage(hWndStatus, SB_SETTEXT, 3, (LPARAM) name);
#endif   // REWRITE
         }
         break;

      case NM_DBLCLK:
         orderUnit(lpNMHDR->idFrom, lpNMHDR->code);
         break;

#ifdef DEBUG
      default:
         debugLog("Unknown code\n");
         break;
#endif
      }


      break;

#if defined(CUSTOMIZE)
   case IDOBE_UNITS_NEWTOWN:
      switch(lpNMHDR->code)
      {
      case TVN_GETDISPINFO:
         {
            TV_DISPINFO* tvdi = (TV_DISPINFO*) lpNMHDR;

            if(tvdi->item.mask & TVIF_TEXT)
            {
              int type = Side(HIWORD(tvdi->item.lParam));

              if(type == PROV)
              {
                IProvince iProv = LOWORD(tvdi->item.lParam);
                ASSERT(iProv != NoProvince);
                Province prov = campaignData->getProvince(iProv);
                strcpy(tvdi->item.pszText, prov.getName());
              }
              else if(type == TOWN)
              {
                ITown iTown = LOWORD(tvdi->item.lParam);
                ASSERT(iTown != NoTown);
                Town town = campaignData->getTown(iTown);
                strcpy(tvdi->item.pszText, town.getName());
              }
              else
              {
                GeneralError error("Improper id in OBTreeview::addTowns");
              }
            }
         }
         break;

      case TVN_SELCHANGED:
         {
            NM_TREEVIEW* pnmtv = (NM_TREEVIEW *) lpNMHDR;
            int type = Side(HIWORD(pnmtv->itemNew.lParam));
            if(type == TOWN)
            {
              iTown = LOWORD(pnmtv->itemNew.lParam);
            }
         }
         break;

      case NM_DBLCLK:
         orderUnit(lpNMHDR->idFrom, lpNMHDR->code);
         break;

#ifdef DEBUG
      default:
         debugLog("Unknown code\n");
         break;
#endif
      }
      break;
#endif   // CUSTOMIZE

#ifdef DEBUG
   default:
      debugLog("Unknown id\n");
      break;
#endif
   }

   return 0;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
