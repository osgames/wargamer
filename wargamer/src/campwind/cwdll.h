/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CWDLL_H
#define CWDLL_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * CAMPWIND DLL defines
 *
 *----------------------------------------------------------------------
 */

#if defined(NO_WG_DLL)
    #define CAMPWIND_DLL
#elif defined(EXPORT_CAMPWIND_DLL)
    #define CAMPWIND_DLL __declspec(dllexport)
#else
    #define CAMPWIND_DLL __declspec(dllimport)
#endif


#endif /* CWDLL_H */


