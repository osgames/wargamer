/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Time Control
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ctimctrl.hpp"
#include "ctimdata.hpp"
#include "registry.hpp"
#include "myassert.hpp"
#include "scenario.hpp"

/*
 * registry Strings
 */

static const char campaignRegName[] = "Campaign";
static const char regTimeRate[] = "Timerate";
static const char regFreeze[] = "Freeze";

/*
 * Values in scenario file
 */

static const char s_minRatio[] = "CampaignTimeRatioMinimum";
static const char s_maxRatio[] = "CampaignTimeRatioMaximum";
static const char s_ratio[] =    "CampaignTimeRatioResolution";
// static const char s_jump1[] = "CampaignAdvanceTimeSlow";
// static const char s_jump2[] = "CampaignAdvanceTimeMedium";
// static const char s_jump3[] = "CampaignAdvanceTimeFast";








/*
 * Time Control
 */

bool CampaignTimeControl::s_initialised = false;
int CampaignTimeControl::s_range = 25;
int CampaignTimeControl::s_defaultRate = 9;

int* CampaignTimeControl::s_rateTable;
// [s_range] = {
//    300,
//    270,
//    240,
//    210,
//    180,
//    150,
//    120,
//    90,
//    75,
//    60,
//    55,
//    50,
//    45,
//    40,
//    35,
//    30,
//    25,
//    20,
//    15,
//    10,
//    5,
//    4,
//    3,
//    2,
//    1
// };


/*
 * Constructor
 */

CampaignTimeControl::CampaignTimeControl() :
   d_frozen(False),
   d_timeJump(0)
{
   initSettings();

   int newRate = s_defaultRate;
   if (getRegistry(regTimeRate, &newRate, sizeof(newRate), campaignRegName))
   {
      if((newRate < 0) || (newRate > s_range))
         newRate = s_defaultRate;
   }
   setTimeRate(newRate);

   if(!getRegistry(regFreeze, &d_frozen, sizeof(d_frozen), campaignRegName))
      d_frozen = False;
}


/*
 * Destructor
 */

CampaignTimeControl::~CampaignTimeControl()
{
   setRegistry(regTimeRate, &d_rate,   sizeof(d_rate),   campaignRegName);
   setRegistry(regFreeze,   &d_frozen, sizeof(d_frozen), campaignRegName);
}


// Pause or Unpause the game

void CampaignTimeControl::freezeTime(Boolean state)
{
   d_frozen = state;
}

/*
 * Get a new time from an existing time and number of system ticks
 */

// GameTick CampaignTimeControl::addTicks(CampaignTimeData& campTime, SysTick::Value ticks)
CampaignTime CampaignTimeControl::addTicks(const CampaignTime& lastWantTime, const CampaignTime& realTime, SysTick::Value ticks)
{
//   CampaignTime& cTime = campTime.getFineTime();
//   const CampaignTime& logicTime = campTime.getCTime();

//   GameTick gTicks = cTime;

   CampaignTime retVal = lastWantTime;

   /*
    * If jumping Time then return the jumpDay
    */

   if(d_timeJump)
   {
//      ASSERT(d_timeJump >= lastWantTime.getDate());

      if (realTime.getDate() >= d_timeJump)
      {
         d_timeJump = 0;
         retVal = realTime;
      }
      else
      {
         retVal.set(d_timeJump, 0);
      }
   }
   else if(!d_frozen)
   {
#if defined(USE_MM_TIMER)
      ULONG gameTicksPerTick = (GameTicksPerDay * gApp.getMMTickRate()) / (1000 * d_secondsPerDay);
#else
      ULONG gameTicksPerTick = SysTick::TicksPerDay / (SysTick::TicksPerSecond * d_secondsPerDay);
#endif
      GameTick gameTicks = ticks * gameTicksPerTick;
      retVal += gameTicks;
   }

   return retVal;



//    GameDay day = campTime.getDate();
//    if(d_timeJump > day)
//    {
//       // Update to next day if logic is ready...
//
//       GameDay logicDay = logicTime.getDate();
//       if(logicDay >= day)
//       {
//          cTime.set(GameDay(day+1), 0);
//          gTicks = cTime;
//       }
//    }
//    else
//    {
//       d_timeJump = 0;
//
//       if(!d_frozen)
//       {
//          /*
//           * If Logic is not keeping up, then do not add time
//           */
//
//
//          GameTick logicDelay = cTime - logicTime;
//          GameDay logicDay = logicDelay.getDate();
//
//          if(logicDay < 1)
//          {
// #if defined(USE_MM_TIMER)
//             ULONG gameTicksPerTick = (GameTicksPerDay * gApp.getMMTickRate()) / (1000 * d_secondsPerDay);
//             ULONG gameTicks = ticks * gameTicksPerTick;
// #else
//             ULONG gameTicksPerTick = SysTick::TicksPerDay / (SysTick::TicksPerSecond * d_secondsPerDay);
//             GameTick gameTicks = ticks * gameTicksPerTick;
// #endif
//
//             gTicks = cTime;
//             gTicks += gameTicks;
//             cTime = gTicks;
//          }
//       }
//    }
//
//   return gTicks;
}


int CampaignTimeControl::setTimeRate(int n)
{
   ASSERT(n >= 0);
   ASSERT(n < s_range);

   d_rate = n;
   d_secondsPerDay = s_rateTable[n];

   return d_secondsPerDay;
}

#if 0
// void CampaignTimeControl::setTimeJump(int days)
void CampaignTimeControl::setTimeJump(GameDay day)
{
   // d_timeJump = GameDay(d_campData->getCTime().getDate() + days);
   d_timeJump = day;
}
#endif

int CampaignTimeControl::getTimeRange() const
{
   return s_range;
}

int CampaignTimeControl::getSecondsPerDay(int rate) const
{
    ASSERT(rate >= 0);
    ASSERT(rate < s_range);

    if((rate >= 0) && (rate < s_range))
        return s_rateTable[rate];
    else
        return 0;
}

void CampaignTimeControl::initSettings()
{
   if (!s_initialised)
   {
      s_initialised = true;

      int minRatio = scenario->getInt(s_minRatio);
      int maxRatio = scenario->getInt(s_maxRatio);
      int range = scenario->getInt(s_ratio);
//      int j1 = scenario->getInt(s_jump1);
//      int j2 = scenario->getInt(s_jump2);
//      int j3 = scenario->getInt(s_jump3);

      ASSERT(!s_rateTable);
      ASSERT(range > 1);
      ASSERT(minRatio > 0);
      ASSERT(maxRatio > 0);
      ASSERT(minRatio <= maxRatio);

      s_rateTable = new int[range];

      s_range = range;

      int spread = maxRatio - minRatio;

      int range2 = range -1;  // so that table[range-1] is maxRatio
      range2 *= range2;

      for(int i = 0; i < range; ++i)
      {
         // s_rateTable[i] = minRatio + (i * spread) / range;

         int n = range - 1 - i;     // strt with high value

         // square function rounded up so biassed towards smal values
         s_rateTable[i] = minRatio + (n * n * spread + range2 - 1) / range2;

      }
   }

   s_defaultRate = s_range / 2;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
