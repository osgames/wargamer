/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "spwind.hpp"
#include "wind.hpp"
#include "winctrl.hpp"
#include "app.hpp"
#include "cbutton.hpp"
#include "scenario.hpp"
#include "fillwind.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "campdint.hpp"
#include "palette.hpp"
#include "scn_res.h"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "itemwind.hpp"
#include "dragwin.hpp"
#include "armies.hpp"
#include "armyutil.hpp"
#include "imglib.hpp"
#include "control.hpp"
#include "route.hpp"
#include "gamectrl.hpp"
#include "scrnbase.hpp"
#include "cwin_int.hpp"
#include "scn_img.hpp"
#include "town.hpp"
#include "tbldimg.hpp"
#include "ds_repoSP.hpp"
#include "resstr.hpp"

#ifdef DEBUG
#include "logwin.hpp"
#endif

/*
 * Internal items, containers
 */

// for leader item list
struct RepoSPItem : public SLink {
   CRefPtr<RepoSP> d_sp;

   enum { ChunkSize = 10 };

   RepoSPItem(const RepoSP* sp) :
    d_sp(sp)
    {
    }

   ~RepoSPItem()
   {
   }

   /*
    * Allocators
    */

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif
};

#ifdef DEBUG
static FixedSizeAlloc<RepoSPItem> s_rAlloc("RepoSPItem");
#else
static FixedSizeAlloc<RepoSPItem> s_rAlloc;
#endif

void* RepoSPItem::operator new(size_t size)
{
  return s_rAlloc.alloc(size);
}
#ifdef _MSC_VER
void RepoSPItem::operator delete(void* deadObject)
{
  s_rAlloc.release(deadObject);
}
#else
void RepoSPItem::operator delete(void* deadObject, size_t size)
{
  s_rAlloc.release(deadObject, size);
}
#endif


class RepoSPItemList : public SList<RepoSPItem> {
};

// for unit item list---------------------------
struct RSPUnitItem : public SLink {
   ConstICommandPosition d_cpi;
   RepoSPItemList d_newSP;

   enum { ChunkSize = 25 };

   RSPUnitItem() :
     d_cpi(NoCommandPosition) {}

   ~RSPUnitItem() {}

   /*
    * Allocators
    */

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif
};

#ifdef DEBUG
static FixedSizeAlloc<RSPUnitItem> s_uAlloc("RSPUnitItem");
#else
static FixedSizeAlloc<RSPUnitItem> s_uAlloc;
#endif  // DEBUG

void* RSPUnitItem::operator new(size_t size)
{
  return s_uAlloc.alloc(size);
}

#ifdef _MSC_VER
void RSPUnitItem::operator delete(void* deadObject)
{
  s_uAlloc.release(deadObject);
}
#else
void RSPUnitItem::operator delete(void* deadObject, size_t size)
{
  s_uAlloc.release(deadObject, size);
}
#endif

class RSPUnitItemList : public SList<RSPUnitItem> {
};

/*-----------------------------------------------------------------
 * Utilities
 */

class RSPUtil {
public:
  const int shadowWidth;
  const int shadowHeight;
  const int scrollCX;
  const int headerFontCY;
  const int headerY;
  const int spInfoX;
  const int infoX;
  const int infoY;
  const int infoCX;
  const int infoCY;

  RSPUtil::RSPUtil() :
    shadowWidth(ScreenBase::x(3)),
    shadowHeight(ScreenBase::x(3)),
    scrollCX(ScreenBase::x(5)),
    headerFontCY(ScreenBase::y(11)),
    headerY(ScreenBase::y(3)),
    spInfoX(ScreenBase::x(3)),
    infoX(ScreenBase::x(123)),
    infoY(ScreenBase::y(100)),
    infoCX(ScreenBase::x(108)),
    infoCY(ScreenBase::y(33))
    {
    }

  static HWND createButton(HWND hParent, const char* text, int id, int x, int y, int cx, int cy);
  static void drawInfo(const CampaignData* campData, HDC hdc, RSPUtil& ru);
  static void drawAttributeGraph(HDC hdc, DrawDIB* dib, const char* text, int x, int y, Attribute value, RSPUtil& ru);
};

void RSPUtil::drawInfo(const CampaignData* campData, HDC hdc, RSPUtil& ru)
{
  LONG x = ScreenBase::x(2);
  LONG y = ScreenBase::y(2);

  COLORREF color;
  if(!scenario->getColour("MapInsert", color))
    color = RGB(255,255,255);

  COLORREF oldColor = SetTextColor(hdc, color);

  /*
   * draw header text
   */

  LogFont lf;
  lf.height(ru.headerFontCY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(false);
  lf.underline(true);

  Font font;
  font.set(lf);

  SetBkMode(hdc, TRANSPARENT);
  HFONT oldFont = (HFONT)SelectObject(hdc, font);

  const char* text = InGameText::get(IDS_ReplacementPool);
  wTextPrintf(hdc, x + FI_Army_CX + ScreenBase::x(3),
     ru.headerY, "%s", text);

  SelectObject(hdc, oldFont);
  SetTextColor(hdc, oldColor);
}


HWND RSPUtil::createButton(HWND hParent, const char* text, int id, int x, int y, int cx, int cy)
{
  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
            x, y, cx, cy,
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  cb.setBorderColours(scenario->getBorderColors());

  return hButton;
}

void RSPUtil::drawAttributeGraph(HDC hdc, DrawDIB* dib, const char* text, int x, int y, Attribute value, RSPUtil& ru)
{
  ASSERT(hdc);
  ASSERT(dib);

  const int barOffsetX = x + ScreenBase::x(50);
  const int barCX = ScreenBase::x(50);
  const int barCY = ScreenBase::y(5);

  wTextOut(hdc, x, y, text);
  dib->drawBarChart(barOffsetX, y + 1, value, Attribute_Range, barCX, barCY);
}

/*----------------------------------------------------------------
 * Our Drag List windows
 */

struct RSPItemData {
  const RepoSPItemList* d_sps;
  const RSPUnitItemList* d_units;

  RSPItemData() :
    d_sps(0),
    d_units(0) {}
};

class RSPItemListBox : public ItemWindow {
        const CampaignData* d_campData;
        const UINT d_listCX;
        const UINT d_listCY;
        enum Type {
           SPList,
           UnitList
        } d_type;

    public:
        RSPItemListBox(const ItemWindowData& data, const CampaignData* campData, const SIZE& s) :
           ItemWindow(data, s),
           d_campData(campData),
           d_listCX(s.cx),
           d_listCY(s.cx),
           d_type(SPList) {}
        ~RSPItemListBox() {}

        const CampaignData* campData() const { return d_campData; }

        void init(const void* data);

  private:
    void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
    void drawCombo(DrawDIBDC* dib, const void* data) {}
    void drawItem(DrawDIBDC* dib, const PixelRect& r, ListBox& lb, int id, bool dragMode);

    virtual void drawDragItem(DrawDIBDC* dib, ListBox& lb, int id);

};

inline LPARAM spToLPARAM(const RepoSPItem* sp)
{
   return reinterpret_cast<LPARAM>(const_cast<RepoSPItem*>(sp));
}

inline LPARAM uiToLPARAM(const RSPUnitItem* unit)
{
   return reinterpret_cast<LPARAM>(const_cast<RSPUnitItem*>(unit));
}

void RSPItemListBox::init(const void* dataPtr)
{
  ASSERT(d_campData);
  ASSERT(dataPtr);

  const RSPItemData* ld = reinterpret_cast<const RSPItemData*>(dataPtr);
  ASSERT(ld->d_sps || ld->d_units);

  d_type = (ld->d_sps) ? SPList : UnitList;

  ListBox lb(getHWND(), ItemListBox);
  lb.reset();

  if(d_type == SPList)
  {
    ASSERT(ld->d_sps);
    ASSERT(!ld->d_units);

    /*
     * Fill List box
     */

    SListIterR<RepoSPItem> iter(ld->d_sps);
    while(++iter)
    {
      ISP sp = iter.current()->d_sp->sp();
      const UnitTypeItem& ui = d_campData->getUnitType(sp->getUnitType());

      lb.add(ui.getName(), spToLPARAM(iter.current()));
    }
  }
  else
  {
    ASSERT(ld->d_units);

    /*
     * Fill List box
     */

    SListIterR<RSPUnitItem> iter(ld->d_units);
    while(++iter)
    {
      lb.add("", uiToLPARAM(iter.current()));
    }
  }

  if(lb.getNItems() > 0)
  {
    lb.set(0);
    setCurrentValue();
  }

  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, d_listCY, SWP_NOMOVE);
}

void RSPItemListBox::drawDragItem(DrawDIBDC* dib, ListBox& lb, int id)
{
    PixelRect r;
    ListBox_GetItemRect(lb.getHWND(), id, &r);

    ColourIndex mask = *fillDib()->getBits();
    dib->fill(mask);
    dib->setMaskColour(mask);

    drawItem(dib,
        PixelRect(0, 0, r.width(), r.height()),
        lb, id, true);
}

void RSPItemListBox::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
   ListBox lb(lpDrawItem->hwndItem);
   const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
   const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

  /*
   * Allocate dib if needed
   */

   DrawDIBDC* dib = allocateItemDib(itemCX, itemCY);

   ASSERT(dib);
   ASSERT(fillDib());

   itemDib()->rect(0, 0, itemCX, itemCY, fillDib());

   if(!(lpDrawItem->itemState & ODS_SELECTED))
      dib->darkenRectangle(0, 0, itemCX, itemCY, 20);

   drawItem(dib, PixelRect(0,0,itemCX,itemCY), lb, lpDrawItem->itemID, false);

   BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
      dib->getDC(), 0, 0, SRCCOPY);
}

void RSPItemListBox::drawItem(DrawDIBDC* dib, const PixelRect& r, ListBox& lb, int id, bool dragMode)
{
  if(lb.getNItems() > 0)
  {
    char text[200];

    /*
     * Get screen base and other useful constants
     */

    TEXTMETRIC tm;
    GetTextMetrics(dib->getDC(), &tm);
    const int xMargin = ScreenBase::x(2);
    const int xOffset = ScreenBase::x(8);
    const int yMargin = 1;

    int flagX = xMargin;
    if(d_type == SPList)
    {
      const RepoSPItem* rsp = reinterpret_cast<const RepoSPItem*>(lb.getValue(id));
      ISP sp = rsp->d_sp->sp();
      const UnitTypeItem& ui = d_campData->getUnitType(sp->getUnitType());
      lstrcpy(text, ui.getName());

      /*
       *  get image
       */

        TownBuildImages images;
        TownBuildImages::Value imageID = images.getUnitTypeIndex(ui.getBasicType());

      // get image size
      int w;
      int h;
      images.getImageSize(imageID, w, h);

      // get stretch size
      if(h > (r.height() - (2 * yMargin)))
      {
        int oldH = h;
        h = (r.height() - (2 * yMargin));

        ASSERT(oldH);
        w = (w * h) / oldH;
      }

      int y = (r.height() - h) / 2;
      images.stretchBlit(dib, imageID, xMargin, y, w, h);

      /*
       * If SP is travelling then...
       * Draw red frame around image
       */

      if(!dragMode &&
         (rsp->d_sp->isCompleted() || (rsp->d_sp->travelTime() > d_campData->getTick())) )
      {
         ColourIndex col = dib->getColour(PALETTERGB(255,0,0));
         dib->frame(xMargin, y, w, h, col);
         dib->frame(xMargin+1, y+1, w-2, h-2, col);
      }
    }
    else
    {
      static int imageRankOffset[Rank_HowMany] = {
               FI_ArmyGroup,           // God
               FI_ArmyGroup,           // President
               FI_ArmyGroup,           // Group
               FI_Army,    // Army
               FI_Corps,         // Corps
               FI_Division       // Division
      };

      RSPUnitItem* ui = reinterpret_cast<RSPUnitItem*>(lb.getValue(id));

      UWORD imageID = imageRankOffset[ui->d_cpi->getRank().getRankEnum()];
      const ImageLibrary* flags = scenario->getNationFlag(ui->d_cpi->getNation(), True);

      /*
       * Draw lines
       *
       *
       * Set x offset
       */

        if(!dragMode)
        {
          flagX = xMargin;
          int nOffsets = 0;

          // first, count the nunber of offsets
          ICommandPosition parentCPI = ui->d_cpi->getParent();
          while(parentCPI->isLower(Rank_President))
          {
          flagX += xOffset;
          nOffsets++;
          parentCPI = parentCPI->getParent();
          }

          // more useful constants
          const int xLineMargin = xMargin + (FI_Army_CX / 2);
          const int vw = ScreenBase::x(5);
          int vh = r.height();
          const ColourIndex ci = dib->getColour(PALETTERGB(0, 0, 0));

          // now draw appropriate lines
          parentCPI = ui->d_cpi->getParent();
          ICommandPosition thisCPI = parentCPI;
          while(nOffsets--)
          {
          ASSERT(parentCPI != NoCommandPosition);

          const int x = xLineMargin + (xOffset * nOffsets);
          const int y = 0;

          // if current objects parent
          if(parentCPI == ui->d_cpi->getParent())
          {
             if(ui->d_cpi->getSister() == NoCommandPosition)
             {
                vh = r.height() / 2;
             }

             dib->hLine(x, r.height() / 2, vw, ci);
             dib->vLine(x, y, vh, ci);
          }
          else
          {
             ASSERT(thisCPI != NoCommandPosition);

             ICommandPosition sisCPI = thisCPI->getSister();
             if(sisCPI != NoCommandPosition)
             {
                dib->vLine(x, y, r.height(), ci);
             }
          }

          thisCPI = parentCPI;
          parentCPI = parentCPI->getParent();
          }
        }

      wsprintf(text, "%s (%s)",
         ui->d_cpi->getNameNotNull(),
         ui->d_cpi->getLeader()->getNameNotNull());

      int w;
      int h;
      flags->getImageSize(imageID, w, h);

      int y = (r.height() - h) / 2;
      flags->blit(dib, imageID, flagX, y, True);
    }


    int y = (r.height() - tm.tmHeight) / 2;
    wTextOut(dib->getDC(), flagX + FI_Army_CX + xMargin, y, text);

  }
}


/*----------------------------------------------------------
 * Actual Reorganize leader window
 */

class RepoSP_Window : public DragWindow     // WindowBaseND
{
    typedef DragWindow Super;

    HWND d_hParent;
    const CampaignData* d_campData;
    CampaignWindowsInterface* d_campWind;

    FillWindow d_fillWind;

    RSPItemListBox* d_spLB;
    RSPItemListBox* d_unitsLB;

    RepoSPItemList d_sps;
    RSPUnitItemList d_units;

    PixelPoint d_p1;
    PixelPoint d_p2;

    HFONT d_dragFont;

    DrawDIB* d_infoDib;
    HDC d_dibDC;


    Boolean d_orderGiven;
    enum ID {
       List1,
       List2,
       Button_First,
       Cancel = Button_First,
       Send,
       Button_Last = Send,
       ID_HowMany,
       Button_HowMany = (Button_Last - Button_First) + 1
    };

    RSPUtil d_ru;

  public:
    RepoSP_Window(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData);
    ~RepoSP_Window()
    {
       delete d_spLB;
       delete d_unitsLB;

        selfDestruct();

      if(d_infoDib)
        delete d_infoDib;

      if(d_dibDC)
        DeleteDC(d_dibDC);
    }

    void run();

  private:

        /*
         * Virtual functions to implement DragWindow
         */

        virtual void dropObject(const DragWindowObject& from, const DragWindowObject& dest);
        virtual bool overObject(const PixelPoint& p, const DragWindowObject& from, DragWindowObject* object);
        virtual bool canDrag(const DragWindowObject& ob);

    /*
     * Window Messages
     */

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
    void onPaint(HWND hwnd);
    BOOL onEraseBk(HWND hwnd, HDC hdc);
    UINT onNCHitTest(HWND hwnd, int x, int y);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

    /*
     * useful functions
     */

    void fillSPList();
    void fillUnitList();
    void onSend();
    void onCancel();
    void drawUnitInfo(HDC hdc);
    void drawSPInfo(HDC hdc);
    void enableControls();
    void redrawUnitInfo();
    void redrawSPInfo();
    void unitTypeListCount(const RSPUnitItem* ui, SPCount& nInf,
       SPCount& nCav, SPCount& nArt, SPCount& nTotal);
};

RepoSP_Window::RepoSP_Window(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData) :
  d_hParent(hParent),
  d_campData(campData),
  d_campWind(cw),
  d_spLB(0),
  d_unitsLB(0),
  d_dragFont(0),
  d_infoDib(0),
  d_dibDC(CreateCompatibleDC(NULL)),
  d_orderGiven(False)
{
  ASSERT(d_hParent);
  ASSERT(d_campData);
  ASSERT(d_dibDC);

  const int cx = ScreenBase::x(240);
  const int cy = ScreenBase::y(150);

  HWND hWnd = createWindow(
      0,
        NULL,
      WS_POPUP,
      0, 0, cx, cy,
      d_hParent, NULL
  );
  ASSERT(hWnd != NULL);

}

LRESULT RepoSP_Window::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE, onCreate);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    default:
        return Super::procMessage(hWnd, msg, wParam, lParam);
  }
}

BOOL RepoSP_Window::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
  const int cx = lpCreateStruct->cx;
  const int cy = lpCreateStruct->cy;


  /*
   * init background dib
   */

  d_fillWind.setShadowWidth(d_ru.shadowWidth);
  d_fillWind.allocateDib(cx, cy);
  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());


  d_infoDib = new DrawDIB(d_ru.infoCX, d_ru.infoCY);
  ASSERT(d_infoDib);

  /*
   * Set font
   */

  LogFont lf;
  lf.height(ScreenBase::y(7));
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(false);
  lf.underline(false);

  Font font;
  font.set(lf);

  d_dragFont = font;

  const int offset = ScreenBase::x(2);

  ItemWindowData ld;
  ld.d_hParent = hwnd;
  ld.d_style = WS_CHILD | WS_VISIBLE;
  ld.d_id = List1;
  ld.d_hFont = d_dragFont;
  ld.d_scrollCX = d_ru.scrollCX;
  ld.d_itemCY = ScreenBase::y(9);
  ld.d_maxItemsShowing = 9;
  ld.d_flags =  ItemWindowData::HasScroll |
                ItemWindowData::NoAutoHide |
             ItemWindowData::NoBorder |
                ItemWindowData::DragList |
                ItemWindowData::InclusiveSize |
                ItemWindowData::OwnerDraw;
  ld.d_windowFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ld.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ld.d_scrollBtns = ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons);
  ld.d_scrollThumbDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground);
  ld.d_scrollFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground);
  ld.d_bColors = scenario->getBorderColors();

  ASSERT(ld.d_flags & ItemWindowData::HasScroll);

  const int borderCX = 0;
  SIZE s;
  s.cx = (((cx - (4 * offset)) - d_ru.shadowWidth) / 2) - ((2 * borderCX) + ld.d_scrollCX);
  s.cy = ld.d_itemCY * ld.d_maxItemsShowing;
  s.cy += 2 * ld.borderHeight();
  d_spLB = new RSPItemListBox(ld, d_campData, s);
  ASSERT(d_spLB);

  d_p1.setX(offset);
  d_p1.setY(d_ru.headerY + d_ru.headerFontCY);

  ld.d_id = List2;
  ld.d_maxItemsShowing = 9;
  ld.d_flags =  ItemWindowData::HasScroll |
                ItemWindowData::NoAutoHide |
            ItemWindowData::NoBorder |
                ItemWindowData::InclusiveSize |
                ItemWindowData::OwnerDraw;

  d_unitsLB = new RSPItemListBox(ld, d_campData, s);
  ASSERT(d_unitsLB);

  d_p2.setX(d_p1.getX() + ((4 * offset) + s.cx));
  d_p2.setY(d_p1.getY());

//  const int bcx = ScreenBase::x(30);
  const int bcx = ScreenBase::x(48);
  const int bcy = ScreenBase::y(10);
  int buttonY = cy - bcy - ScreenBase::y(5);

  int i = 0;
  int spacing = (cx - (bcx * Button_HowMany)) / (Button_HowMany + 1);
  for(int id = Button_First; id < Button_Last + 1; id++, i++)
  {
    const char* text;
    if(id == Cancel)
        text = InGameText::get(IDS_Cancel);
    else
#if defined(DEBUG)
    if(id == Send)
#endif
        text = InGameText::get(IDS_UOP_Send);
#if defined(DEBUG)
    else
        FORCEASSERT("id is invalid");
#endif

    int x = (spacing * (i + 1)) + (i * bcx);
    RSPUtil::createButton(hwnd, text, id,
      x, buttonY, bcx, bcy);
  }

  enableControls();
  return TRUE;
}

void RepoSP_Window::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
  RealizePalette(hdc);

  // draw header text
  RSPUtil::drawInfo(d_campData, hdc, d_ru);

  // draw leader info
  drawSPInfo(hdc);
  drawUnitInfo(hdc);

  SelectPalette(hdc, oldPal, False);

  EndPaint(hwnd, &ps);
}

BOOL RepoSP_Window::onEraseBk(HWND hwnd, HDC hdc)
{
  d_fillWind.paint(hdc);
  return True;
}

UINT RepoSP_Window::onNCHitTest(HWND hwnd, int x, int y)
{
  return HTCAPTION;
}

void RepoSP_Window::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case Cancel:
    {
      onCancel();
      break;
    }

    case Send:
    {
      onSend();
      onCancel();
      break;
    }

    case List1:
    case List2:
    {
      if(id == List1)
        redrawSPInfo();
      else
        redrawUnitInfo();
      break;
    }
  }
}

void RepoSP_Window::run()
{
  ASSERT(d_hParent);


  /*
   * First transfer bits from mainwind hdc to our local dib
   */

  // calculate top corner position
  RECT r;
  GetClientRect(getHWND(), &r);
  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;

  RECT pr;
  GetClientRect(d_hParent, &pr);
  const int x = ((pr.right - pr.left) - cx) / 2;
  const int y = ((pr.bottom - pr.top) - cy) / 2;
  PixelPoint p(x, y);


  /*
   * initialize lists
   */

  fillSPList();
  fillUnitList();

  RSPItemData ld;
  ld.d_sps = &d_sps;
  d_spLB->init(&ld);

  ld.d_sps = 0;
  ld.d_units = &d_units;
  d_unitsLB->init(&ld);

  d_spLB->show(d_p1);
  d_unitsLB->show(d_p2);

  /*
   * Convert to screen and show
   */

  POINT p2;
  p2.x = p.getX();
  p2.y = p.getY();

  ClientToScreen(d_hParent, &p2);
  SetWindowPos(getHWND(), HWND_TOPMOST, p2.x, p2.y, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
}

void RepoSP_Window::redrawUnitInfo()
{
  RECT r;
  SetRect(&r, d_ru.infoX, d_ru.infoY, d_ru.infoX + d_ru.infoCX, d_ru.infoY + d_ru.infoCY);

  InvalidateRect(getHWND(), &r, FALSE);
  UpdateWindow(getHWND());
}

void RepoSP_Window::redrawSPInfo()
{
  RECT r;
  SetRect(&r, d_ru.spInfoX, d_ru.infoY, d_ru.infoX + d_ru.infoCX, d_ru.infoY + d_ru.infoCY);

  InvalidateRect(getHWND(), &r, FALSE);
  UpdateWindow(getHWND());
}

void RepoSP_Window::fillSPList()
{
  ASSERT(d_campData);

  /*
   * Get independent leader list
   */

  const Armies& armies = d_campData->getArmies();
  const RepoSPList& list = armies.repoSPList();

  /*
   * reset our internal list
   */

  d_sps.reset();

  /*
   * Fill our internal list
   */

  ArmyReadLocker lock(armies);
  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    SListIterR<RepoSP> iter(&list);
    while(++iter)
    {
      if (!iter.current()->isCompleted())
      {
         ASSERT(iter.current()->town() != NoTown);
         const Town& t = d_campData->getTown(iter.current()->town());

         if(s == t.getSide() &&
            GamePlayerControl::canControl(t.getSide()))
         {

           RepoSPItem* il = new RepoSPItem(iter.current());
           ASSERT(il);

           d_sps.append(il);
         }
      }
    }
  }
}

void RepoSP_Window::fillUnitList()
{
  ASSERT(d_campData);

  /*
   * Get independent leader list
   */

  const Armies& armies = d_campData->getArmies();
  /*
   * reset our internal list
   */

  d_units.reset();

  /*
   * Fill our internal list
   */

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    if(GamePlayerControl::canControl(s))
    {
      ConstUnitIter iter(&armies, armies.getPresident(s));
      while(iter.next())
      {
        if(iter.current()->getRank().getRankEnum() != Rank_President)
        {
          RSPUnitItem* ui = new RSPUnitItem();
          ASSERT(ui);

          ui->d_cpi = iter.current();
          d_units.append(ui);
        }
      }
    }
  }
}


bool RepoSP_Window::canDrag(const DragWindowObject& ob)
{
   ListBox lb(ob.hwnd());

   RSPUnitItem* ui = reinterpret_cast<RSPUnitItem*>(lb.getValue(ob.id()));
   RepoSPItem* li = reinterpret_cast<RepoSPItem*>(lb.getValue(ob.id()));

   return(!li->d_sp->isCompleted() && (li->d_sp->travelTime() <= d_campData->getTick()));
}

void RepoSP_Window::dropObject(const DragWindowObject& from, const DragWindowObject& dest)
{
    ListBox fromLB(from.hwnd());
    ListBox toLB(dest.hwnd());

   RSPUnitItem* ui = reinterpret_cast<RSPUnitItem*>(toLB.getValue(dest.id()));
   RepoSPItem* li = reinterpret_cast<RepoSPItem*>(fromLB.getValue(from.id()));

    RepoSPItem* newSP = new RepoSPItem(li->d_sp);
    ASSERT(newSP);

    ui->d_newSP.append(newSP);
    d_sps.remove(li);

    // reset sp list box
    RSPItemData ld;
    ld.d_sps = &d_sps;
    d_spLB->init(&ld);
    d_spLB->show(d_p1);

    // redraw unit box
    d_unitsLB->setCurrentIndex(d_unitsLB->currentIndex());

    d_orderGiven = True;
    redrawUnitInfo();
    redrawSPInfo();

    enableControls();
}

bool RepoSP_Window::overObject(const PixelPoint& p, const DragWindowObject& from, DragWindowObject* object)
{
    DragWindowObject dropOb;

    if(d_unitsLB->overItem(p, &dropOb))
    {
        ListBox fromLB(from.hwnd());
        ListBox toLB(dropOb.hwnd());

       RSPUnitItem* ui = reinterpret_cast<RSPUnitItem*>(toLB.getValue(dropOb.id()));
       RepoSPItem* li = reinterpret_cast<RepoSPItem*>(fromLB.getValue(from.id()));

       const UnitTypeItem& uti = d_campData->getUnitType(li->d_sp->sp()->getUnitType());

       SPCount nInf = 0;
       SPCount nCav = 0;
       SPCount nArt = 0;
       SPCount nTotal = 0;
       unitTypeListCount(ui, nInf, nCav, nArt, nTotal);

       if( (li->d_sp->travelTime() <= d_campData->getTick()) &&
          (CampaignArmy_Util::canAttachTo(d_campData, ui->d_cpi, uti,  maximum(nInf, nCav), nTotal)) )
       {
            if (toLB.getIndex() != dropOb.id())
            {
               startDraw();
               toLB.set(dropOb.id());
               endDraw();
            }
            *object = dropOb;
            return true;
        }
    }

    return false;
}


/*
 * Send order to leader
 */

void RepoSP_Window::onSend()
{
  /*
   * go through SPlist and find any that need to attach
   */

  SListIter<RSPUnitItem> iter(&d_units);
  while(++iter)
  {
    RSPUnitItem* ui = iter.current();
    if(ui->d_newSP.entries() > 0)
    {
      SListIter<RepoSPItem> rIter(&ui->d_newSP);
      while(++rIter)
      {
         RepoSPOrder::send(ui->d_cpi, rIter.current()->d_sp);
      }
    }
  }

  d_orderGiven = False;
}

void RepoSP_Window::onCancel()
{
  /*
   * reset lists and hide
   */

  d_sps.reset();
  d_units.reset();

  ShowWindow(getHWND(), SW_HIDE);

  d_campWind->repoSPUpdated();
}

void RepoSP_Window::drawUnitInfo(HDC hdc)
{
  if(d_unitsLB->entries())
  {
    const RSPUnitItem* rspi = reinterpret_cast<const RSPUnitItem*>(d_unitsLB->currentValue());
    ASSERT(rspi);

    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
    RealizePalette(hdc);

    /*
     * fill info background
     */

    d_infoDib->fill(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground));

    // draw a black border
    ColourIndex ci = d_infoDib->getColour(PALETTERGB(0, 0, 0));
    d_infoDib->frame(0, 0, d_infoDib->getWidth(), d_infoDib->getHeight(), ci);

    SelectObject(d_dibDC, d_infoDib->getHandle());

    /*
     * Now draw info
     */

    LogFont lf;
    lf.height(ScreenBase::y(8));
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    lf.italic(false);
    lf.underline(true);

    Font font;
    font.set(lf);

    HFONT oldFont = reinterpret_cast<HFONT>(SelectObject(d_dibDC, font));
    SetBkMode(d_dibDC, TRANSPARENT);

    const COLORREF ourColor = scenario->getSideColour(rspi->d_cpi->getSide());
    const COLORREF black = PALETTERGB(0, 0, 0);
    const int headerX = ScreenBase::x(1);
    const int headerY = ScreenBase::y(1);
    const int statusY = headerY + ScreenBase::y(10);
    const int spaceY = ScreenBase::y(8);
    const int specialistY = statusY + spaceY;
    const int attributeY = specialistY + (1.5 * float(spaceY));

    COLORREF oldColor = SetTextColor(d_dibDC, ourColor);

    int x = headerX;
    int y = headerY;

    /*
     * Put nation flag
     */

    const ImageLibrary* flagIcons = scenario->getNationFlag(rspi->d_cpi->getNation(), True);
    flagIcons->blit(d_infoDib, FI_Corps, headerX, headerY, True);

    /*
     * Put units name and type
     */

    const char* infantry = InGameText::get(IDS_UI_INFANTRY);
    const char* cavalry = InGameText::get(IDS_UI_CAVALRY);
    const char* artillery = InGameText::get(IDS_UI_ARTY);
    const char* combinedArms = InGameText::get(IDS_UI_CombinedArms);

    const char* text = (rspi->d_cpi->isCombinedArms()) ? combinedArms :
                       (rspi->d_cpi->isInfantry()) ? infantry :
                       (rspi->d_cpi->isCavalry()) ? cavalry : artillery;

    x = headerX + FI_Corps_CX + ScreenBase::x(5);
    wTextPrintf(d_dibDC, x, headerY, (d_ru.infoCX - x) - 3, "%s (%s)",
       rspi->d_cpi->getNameNotNull(), text);

    lf.height(ScreenBase::y(8));
    lf.italic(false);
    lf.underline(false);

    font.set(lf);
    SelectObject(d_dibDC, font);
    SetTextColor(d_dibDC, black);

    /*
     * Put leaders name
     */

    y += spaceY;
    wTextOut(d_dibDC, x, y, (d_ru.infoCX - x) - 3, rspi->d_cpi->getLeader()->getNameNotNull());

    /*
     * Put unit-type strengths
     */

    const char* inf = InGameText::get(IDS_UI_INF);
    const char* cav = InGameText::get(IDS_UI_CAV);
    const char* guns = InGameText::get(IDS_GunsAbrev);

    ConstICommandPosition cpi = static_cast<ConstICommandPosition>(rspi->d_cpi);
    LONG nInf = CampaignArmy_Util::manpowerByType(d_campData, cpi, BasicUnitType::Infantry);
    LONG nCav = CampaignArmy_Util::manpowerByType(d_campData, cpi, BasicUnitType::Cavalry);
    LONG nGuns = CampaignArmy_Util::totalGuns(d_campData, cpi);

    // add new attach orders
    SPCount nInfSP = 0;
    SPCount nCavSP = 0;
    SPCount nArtSP = 0;
    SPCount nTotalSP = 0;
    unitTypeListCount(rspi, nInfSP, nCavSP, nArtSP, nTotalSP);

    nInf += (nInfSP * UnitTypeConst::InfantryPerSP);
    nCav += (nCavSP * UnitTypeConst::CavalryPerSP);
    nGuns += (nArtSP * UnitTypeConst::GunsPerSP);

    lf.underline(true);
    lf.height(ScreenBase::y(7));

    font.set(lf);
    SelectObject(d_dibDC, font);

    x = ScreenBase::x(2);
    y += spaceY;
    const int typeSpace = ScreenBase::x(30);

    wTextOut(d_dibDC, x, y, inf);
    x += typeSpace;

    wTextOut(d_dibDC, x, y, cav);
    x += typeSpace;

    wTextOut(d_dibDC, x, y, guns);

    y += spaceY;
    x = ScreenBase::x(2);

    lf.underline(false);

    font.set(lf);
    SelectObject(d_dibDC, font);

    wTextPrintf(d_dibDC, x, y, "%ld", nInf);
    x += typeSpace;

    wTextPrintf(d_dibDC, x, y, "%ld", nCav);
    x += typeSpace;

    wTextPrintf(d_dibDC, x, y, "%ld", nGuns);


    BitBlt(hdc, d_ru.infoX, d_ru.infoY,
      d_infoDib->getWidth(), d_infoDib->getHeight(),
      d_dibDC, 0, 0, SRCCOPY);

    /*
     * Clean up
     */

    SetTextColor(d_dibDC, oldColor);
    SelectObject(d_dibDC, oldFont);
    SelectPalette(hdc, oldPal, FALSE);
  }
}

void RepoSP_Window::unitTypeListCount(const RSPUnitItem* ui, SPCount& nInf,
  SPCount& nCav, SPCount& nArt, SPCount& nTotal)
{
  SListIterR<RepoSPItem> rIter(&ui->d_newSP);
  while(++rIter)
  {
    const RepoSP* rsp = rIter.current()->d_sp;
    const UnitTypeItem& uti = d_campData->getUnitType(rsp->sp()->getUnitType());

    if(uti.getBasicType() == BasicUnitType::Infantry)
      nInf++;
    else if(uti.getBasicType() == BasicUnitType::Cavalry)
      nCav++;
    else if(uti.getBasicType() == BasicUnitType::Artillery)
      nArt++;

    nTotal++;
  }

  // cannot have infantry and cav in same list
  ASSERT(nInf == 0 || nCav == 0);
}


void RepoSP_Window::drawSPInfo(HDC hdc)
{
  if(d_spLB->entries())
  {
    const RepoSPItem* rspi = reinterpret_cast<const RepoSPItem*>(d_spLB->currentValue());
    ASSERT(rspi);

    const RepoSP* rsp = rspi->d_sp;
    ISP sp = rsp->sp();
    const UnitTypeItem& uti = d_campData->getUnitType(sp->getUnitType());
    const Town& t = d_campData->getTown(rsp->town());
    const Province& p = d_campData->getProvince(t.getProvince());

    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
    RealizePalette(hdc);

    /*
     * fill info background
     */

    d_infoDib->fill(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground));

    // draw a black border
    ColourIndex ci = d_infoDib->getColour(PALETTERGB(0, 0, 0));
    d_infoDib->frame(0, 0, d_infoDib->getWidth(), d_infoDib->getHeight(), ci);

    SelectObject(d_dibDC, d_infoDib->getHandle());

    /*
     * Now draw info
     */

    LogFont lf;
    lf.height(ScreenBase::y(8));
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    lf.italic(false);
    lf.underline(true);

    Font font;
    font.set(lf);

    HFONT oldFont = reinterpret_cast<HFONT>(SelectObject(d_dibDC, font));
    SetBkMode(d_dibDC, TRANSPARENT);

    const COLORREF ourColor = scenario->getSideColour(t.getSide());
    const COLORREF black = PALETTERGB(0, 0, 0);
    const int headerX = ScreenBase::x(1);
    const int headerY = ScreenBase::y(1);
    const int statusY = headerY + ScreenBase::y(10);
    const int spaceY = ScreenBase::y(8);
    const int specialistY = statusY + spaceY;
    const int attributeY = specialistY + (1.5 * float(spaceY));

    COLORREF oldColor = SetTextColor(d_dibDC, ourColor);

    int x = headerX;
    int y = headerY;

    /*
     * Put nation flag
     */

    const ImageLibrary* flagIcons = scenario->getNationFlag(p.getNationality(), True);
    flagIcons->blit(d_infoDib, FI_Corps, headerX, headerY, True);

    /*
     * Put sp name and type
     */

    x = headerX + FI_Corps_CX + ScreenBase::x(5);
    wTextOut(d_dibDC, x, headerY, (d_ru.infoCX - x) - 3, uti.getName());

    lf.height(ScreenBase::y(8));
    lf.italic(false);
    lf.underline(false);

    font.set(lf);
    SelectObject(d_dibDC, font);
    SetTextColor(d_dibDC, black);

    /*
     * Put status
     */

    y += spaceY;
    x = ScreenBase::x(2);

    enum { Available, Traveling, Completed } status;

    if (rsp->isCompleted())
    {
      status = Completed;
    }
    else
    {
       status = (rsp->travelTime() <= d_campData->getTick()) ? Available : Traveling;
    }

    if(status == Available)
    {
      const char* text = t.getNameNotNull();

      wTextPrintf(d_dibDC, x, y, d_ru.infoCX - (x + 3),
         "%s %s",
         InGameText::get(IDS_AvailableAt),
          text);
    }
    else if(status == Traveling)
    {
      const TimeTick timeRemaining = rsp->travelTime() - d_campData->getTick();

      const char* text = (rsp->attachTo() != NoCommandPosition) ?
                 rsp->attachTo()->getNameNotNull() : t.getNameNotNull();

      wTextPrintf(d_dibDC, x, y, d_ru.infoCX - (x + 3), "%s %s (%ld %s)",
         InGameText::get(IDS_TravellingTo),
         text,
         timeRemaining / TicksPerHour,
         InGameText::get(IDS_Hours) );
    }
    else
    {
      wTextPrintf(d_dibDC, x, y, d_ru.infoCX - (x + 3), "%s",
         InGameText::get(IDS_Attached));
    }



    /*
     * Put Attributes
     */

    y += (2 * spaceY);

    const char* text = InGameText::get(IDS_BaseMorale);
    RSPUtil::drawAttributeGraph(d_dibDC, d_infoDib, text, x, y, uti.getBaseMorale(), d_ru);

    BitBlt(hdc, d_ru.spInfoX, d_ru.infoY,
      d_infoDib->getWidth(), d_infoDib->getHeight(),
      d_dibDC, 0, 0, SRCCOPY);

    /*
     * Clean up
     */

    SetTextColor(d_dibDC, oldColor);
    SelectObject(d_dibDC, oldFont);
    SelectPalette(hdc, oldPal, FALSE);
  }
}

void RepoSP_Window::enableControls()
{
  {
    CustomButton b(getHWND(), Send);
    b.enable(d_orderGiven);
  }
}

/*-----------------------------------------------------------
 * Container class for Reorganize leader window
 */

RepoSP_Int::RepoSP_Int(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData)
{
  d_rWindow = new RepoSP_Window(hParent, cw, campData);
  ASSERT(d_rWindow);
}

RepoSP_Int::~RepoSP_Int()
{
  delete d_rWindow;
}

void RepoSP_Int::run()
{
  d_rWindow->run();
}

void RepoSP_Int::hide()
{
  ShowWindow(d_rWindow->getHWND(), SW_HIDE);
}

void RepoSP_Int::destroy()
{
  if(d_rWindow)
  {
    delete d_rWindow;
    d_rWindow = 0;
  }
}

Boolean RepoSP_Int::initiated() const
{
  return (d_rWindow != 0);
}

HWND RepoSP_Int::getHWND() const
{
  return (d_rWindow) ? d_rWindow->getHWND() : 0;
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
