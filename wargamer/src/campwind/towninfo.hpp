/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOWNINFO_HPP
#define TOWNINFO_HPP

#ifndef __cplusplus
#error towninfo.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Town Info display
 *
 *----------------------------------------------------------------------
 */

#include "infobase.hpp"
#include "gamedefs.hpp"
#include "grtypes.hpp"
#include "unittype.hpp"

class Town;
class Province;
class DrawDIBDC;
class CampaignData;
class BuildItem;

class TownAttributes;


class TownInfoData : private InfoDrawBase
{
	public:
        TownInfoData(ITown town, const CampaignData* campData, DrawDIBDC* dib, const PixelRect& rect);
        ~TownInfoData();

        void drawTrackingDIB();

        void drawSummaryDib();
        void drawBuildDib();
        void drawStatusDib();
        void drawTownDib();
        void drawProvinceDib();

        void drawBuildInfoDib(const BuildItem* builds);
        void drawBuildTypeInfoDib(const BuildItem* builds, BasicUnitType::value basicType, bool drawIcon);

	private:
		void drawNameDib(bool compact);

        void drawBuildTypeInfoDib(const BuildItem* builds, BasicUnitType::value basicType, bool drawIcon, const PixelRect& r);

        /*
         * General information
         */

		PixelRect d_mainRect;
        TownAttributes* d_ta;
};


#endif /* TOWNINFO_HPP */

