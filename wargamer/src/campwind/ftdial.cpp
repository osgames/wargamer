/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Find Town and Find Leader Dialogs
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ftdial.hpp"
#include "cwin_int.hpp"
#include "armies.hpp"
#include "resdef.h"
#include "town.hpp"
#include "app.hpp"
#include "campdint.hpp"
#include "mapwind.hpp"
#include "scenario.hpp"
#include "scn_res.h"
#include "grtypes.hpp"
#include "help.h"
#include "control.hpp"

#undef SORT_ON_SURNAME
// #define SORT_ON_SURNAME


const int OBIconWidth = OBI_CX;
static char DefaultProvinceName[] = "_Independant";


FindDial::FindDial(CampaignWindowsInterface* owner, MapWindow* mapWind, HWND parent) :
   d_campWind(owner),
   d_mapWind(mapWind),
   d_hwndParent(parent),
   currentItem(-1),
   lastItem(-1)
{
}

VOID APIENTRY FindDial::findDialPopupMenu(HWND hwnd, PixelPoint& pt, int id)
{
   HMENU hmenu;            // top-level menu
   HMENU hmenuTrackPopup;  // pop-up menu

   // Load the menu resource.

   hmenu = LoadMenu(APP::instance(), MAKEINTRESOURCE(id));

   ASSERT(hmenu != NULL);

   // TrackPopupMenu cannot display the top-level menu, so get
   // the handle of the first pop-up menu.

   hmenuTrackPopup = GetSubMenu(hmenu, 0);

   ASSERT(hmenuTrackPopup != NULL);

   HMENU hSort = GetSubMenu(hmenuTrackPopup, 1);
   ASSERT(hSort != NULL);


   // Display the floating pop-up menu. Track the right mouse
   // button on the assumption that this function is called
   // during WM_CONTEXTMENU processing.

   POINT p = pt;
   ListView listView(hwnd, FTD_LISTVIEW);
   ClientToScreen(listView.getHWND(), &p);

   TrackPopupMenuEx(hmenuTrackPopup,
         TPM_LEFTALIGN | TPM_RIGHTBUTTON,
         p.x, p.y, hwnd, NULL);


   DestroyMenu(hmenu);
}

void FindDial::findItem(HWND hwnd, const char* text)
{
  ListView listView(hwnd, FTD_LISTVIEW);

  LV_FINDINFO fi;
  fi.flags = LVFI_PARTIAL | LVFI_WRAP;
  fi.psz = text;

  lastItem = -1;

  int index = ListView_GetTopIndex(listView.getHWND());
  int item = ListView_FindItem(listView.getHWND(), index, &fi);

  if(item >= 0)
  {
    lastItem = item;
    RECT r;
    ListView_GetItemRect(listView.getHWND(), 0, &r, LVIR_LABEL);
    int width = r.bottom - r.top;
    ListView_Scroll(listView.getHWND(), 0, (item  - index) * width);
  }
}

FindTownDial::FindTownDial(CampaignWindowsInterface* owner, MapWindow* mapWind, HWND parent) : 
   FindDial(owner, mapWind, parent)
{
  iTown = NoTown;

  images.loadBitmap(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_OBICONS), OBIconWidth);

  HWND dhwnd = createDialog(findTownDlgName, parent);
  ASSERT(dhwnd != NULL);

}

FindTownDial::~FindTownDial()
{
}

BOOL FindTownDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_CLOSE:
         HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
         break;
      case WM_HELP:
         HANDLE_WM_HELP(hWnd, wParam, lParam, onHelp);
         break;
      case WM_CONTEXTMENU:
         HANDLE_WM_CONTEXTMENU(hWnd, wParam, lParam, onContextMenu);
         break;
      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

const DWORD FindTownDial::ids[] = {
    FTD_SEARCH,    IDH_FTD_Search,
    FTD_LISTVIEW,  IDH_FTD_TownList,
    FTD_FIND,      IDH_FTD_FindTown,
    0, 0
};

BOOL FindTownDial::onHelp(HWND hwnd, LPHELPINFO lparam)
{
  LPHELPINFO lphi = (LPHELPINFO)lparam;
  if (lphi->iContextType == HELPINFO_WINDOW)   // must be for a control
  {
//  WinHelp ((HWND)lphi->hItemHandle, "help\\Wg95Help.hlp", HELP_WM_HELP, (DWORD)(LPVOID)ids);
  }
  return TRUE;
}

void FindTownDial::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
  ListView listView(hwnd, FTD_LISTVIEW);
  if(hwndCtl != listView.getHWND())
  {
//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
  }
}

BOOL FindTownDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

  WindowControl edit(hwnd, FTD_SEARCH);
  edit.enable(True);

  ListView listView(hwnd, FTD_LISTVIEW);
  RECT r;
  GetClientRect(listView.getHWND(), &r);

  LV_COLUMN lvc;

  // Initialize the LV_COLUMN structure.
  lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
  lvc.fmt = LVCFMT_LEFT;
  lvc.cx = r.right/3;

  // Add the columns.
  for (int i = 0; i < 3; i++)
  {
    lvc.iSubItem = i;

    char colText[100];

    LoadString(APP::instance(), IDS_FTD_COLSTR_TOWN + i,
                   colText, sizeof(colText));

    lvc.pszText = colText;
    if (ListView_InsertColumn(listView.getHWND(), i, &lvc) == -1)
      return FALSE;
  }

  ASSERT(images.getHandle() != NULL);
  listView.setImageList(images.getHandle());

  ASSERT(ListView_GetImageList(listView.getHWND(), LVSIL_SMALL) == images.getHandle());

  addItems(hwnd);

  return TRUE;
}

void FindTownDial::onDestroy(HWND hwnd)
{
  SendMessage(d_hwndParent, WM_PARENTNOTIFY, (WPARAM)WM_DESTROY, (LPARAM)hwnd);
}

static int CALLBACK CompareTownProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
  ASSERT(lParamSort >= 0);
  ASSERT(lParamSort <= 2);
  ITown t1 = (ITown)lParam1;
  ASSERT(t1 != NoTown);
  ITown t2 = (ITown)lParam2;
  ASSERT(t2 != NoTown);

  const Town& town1 = campaignData->getTown(t1);
  const Town& town2 = campaignData->getTown(t2);

  switch(lParamSort)
  {
    // sort by city
    case 0:
      return lstrcmpi(town1.getName(), town2.getName());

    // sort by state, and cities within state
    case 1:
    {
      const char* s1 = DefaultProvinceName;
      const char* s2 = DefaultProvinceName;

      if(town1.getProvince() != NoProvince)
         s1 = campaignData->getProvince(town1.getProvince()).getName();

      if(town2.getProvince() != NoProvince)
         s2 = campaignData->getProvince(town2.getProvince()).getName();

      int result = lstrcmpi(s1, s2);
      if(result != 0)
        return result;
      else
        return lstrcmpi(town1.getName(), town2.getName());

    }
    // sort by type, and cities within type
    case 2:
    {
      if(town1.getSize() < town2.getSize())
        return -1;
      if(town1.getSize() > town2.getSize())
        return 1;

      return lstrcmpi(town1.getName(), town2.getName());

    }
  }
  return 0;
}

int menuToColumn(int id)
{
  if(id == FTD_IDM_SORTCOL1)
    return 0;
  if(id == FTD_IDM_SORTCOL2)
    return 1;
  if(id == FTD_IDM_SORTCOL3)
    return 2;

  return -1;
}

void FindTownDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case FTD_SEARCH:
    {
      if(codeNotify == EN_CHANGE)
      {
        char text[100];
        GetDlgItemText(hwnd, FTD_SEARCH, text, 99);
        findItem(hwnd, text);
      }
      break;
    }

    case FTD_IDM_FIND:
    case FTD_FIND:
      onFindTown();
      onClose(hwnd);
      break;


    case FTD_IDM_SORTCOL1:
    case FTD_IDM_SORTCOL2:
    case FTD_IDM_SORTCOL3:
    {
      ListView listView(hwnd, FTD_LISTVIEW);
      ListView_SortItems(listView.getHWND(), CompareTownProc, menuToColumn(id));
      break;
    }

    case FTD_OK:
      onOK(hwnd);
      break;
  }
}

void FindTownDial::onClose(HWND hwnd)
{
  DestroyWindow(hwnd);
}

void FindTownDial::addItems(HWND hwnd)
{
  ListView listView(hwnd, FTD_LISTVIEW);

  ArrayIter<Town> tl = campaignData->getTowns();
  int i = 0;
  while(++tl)
  {
    ITown iTown = tl.currentID();
    ASSERT(iTown != NoTown);
    Town town = campaignData->getTown(iTown);

#if !defined(CUSTOMIZE)
    if(town.getSize() != TOWN_Other)
#endif
    {
      LV_ITEM lvi;
      lvi.mask = LVIF_TEXT |  LVIF_PARAM | LVIF_STATE;
      lvi.state = 0;

      lvi.iItem = i;
      lvi.iSubItem = 0;
      lvi.stateMask = 0;
      lvi.pszText = LPSTR_TEXTCALLBACK;

      if(town.getSide() != SIDE_Neutral)
      {
         lvi.iImage = (int)(town.getSide() * OBI_IconsPerSide + OBI_Side);
         lvi.mask |= LVIF_IMAGE;
      }

      lvi.lParam = iTown;
      listView.addItem(lvi);

      i++;
    }
  }
}

void FindTownDial::onOK(HWND hwnd)
{
  onClose(hwnd);
}

static const char* townTypeName[] = {
  "Capital",
  "City",
  "Town",
  "Other"
};



LRESULT FindTownDial::onNotify(HWND hwnd, int id, NMHDR* lpNMHDR)
{
   switch(lpNMHDR->idFrom)
   {
     case FTD_LISTVIEW:
     {
#ifdef DEBUG
       debugLog("WM_NOTIFY from FTD_LISTVIEWd\n");
#endif


       switch(lpNMHDR->code)
       {
         case LVN_GETDISPINFO:
         {

           LV_DISPINFO* pnmv = (LV_DISPINFO*)lpNMHDR;
           if(pnmv->item.mask & LVIF_TEXT)
           {
             ITown iTown = (ITown)pnmv->item.lParam;
             ASSERT(iTown != NoTown);
             Town town = campaignData->getTown(iTown);
             switch(pnmv->item.iSubItem)
             {
               case 0:
               {
//               pnmv->item.iImage = (int)town.getSide();
                 lstrcpy(pnmv->item.pszText, town.getName());
                 break;
               }

               case 1:
               {
                 // ASSERT(town.getProvince() != NoProvince);
                 if(town.getProvince() != NoProvince)
                 {
                    Province prov = campaignData->getProvince(town.getProvince());
                    lstrcpy(pnmv->item.pszText, prov.getName());
                 }
                 else
                    lstrcpy(pnmv->item.pszText, DefaultProvinceName);
                 break;
               }

               case 2:
               {
                 lstrcpy(pnmv->item.pszText, townTypeName[(int)town.getSize()]);
                 break;
               }

             }
           }
           break;
         }

         case LVN_COLUMNCLICK:
         {
           NM_LISTVIEW* lv = (NM_LISTVIEW*)lpNMHDR;
           ListView_SortItems(lpNMHDR->hwndFrom, CompareTownProc, lv->iSubItem);

           break;
         }

         case LVN_ITEMCHANGED:
         {
           NM_LISTVIEW* lv = (NM_LISTVIEW*)lpNMHDR;

           ListView listView(lpNMHDR->hwndFrom);
           currentItem = lv->iItem;
           iTown = (ITown)listView.getItem(lv->iItem);
           ASSERT(iTown != NoTown);

           d_campWind->setTown(iTown);
           // d_campWind->orderTown(iTown);

           break;
         }

         case NM_RCLICK:
         {
           ASSERT(iTown != NoTown);
           Town town = campaignData->getTown(iTown);
           RECT r;
           ListView_GetItemRect(lpNMHDR->hwndFrom, currentItem, &r, LVIR_LABEL);
           HDC hdc = GetDC(lpNMHDR->hwndFrom);
           SIZE s;
           GetTextExtentPoint32(hdc, town.getName(), lstrlen(town.getName()), &s);
           PixelPoint p(r.left + s.cx, r.bottom);
           findDialPopupMenu(hwnd, p, MENU_FINDTOWNPOPUP);
           ReleaseDC(lpNMHDR->hwndFrom, hdc);
           break;
         }
       }
       break;
     }
   }
   return 0;
}

void FindTownDial::onFindTown()
{
  if(iTown != NoTown)
  {
    Town town = campaignData->getTown(iTown);

    MagEnum magLevel = (town.getSize() == TOWN_Capital)?(MagEnum)(town.getSize() + 1):(MagEnum)town.getSize();

    MapWindow* mw = d_mapWind;   // campWind->getMapWindow();
    if(mw)
    {

      const Location& l = town.getLocation();
      MapPoint mp;
      mw->locationToMap(l, mp);

      if(magLevel > mw->getZoomEnum())
         mw->zoomTo(magLevel, mp);
        // mw->doZoom(magLevel, mp);

      else
        mw->setMapPos(mp.getX(), mp.getY());

      mw->setHighlightedTown(iTown);
    }
  }
}

//=====================================================================

FindLeaderDial::FindLeaderDial(CampaignWindowsInterface* owner, MapWindow* mapWind, HWND parent) : 
   FindDial(owner, mapWind, parent)
{
  iLeader = NoLeader;

  images.loadBitmap(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_OBICONS), OBIconWidth);

  HWND dhwnd = createDialog(findTownDlgName, parent);
  ASSERT(dhwnd != NULL);

  SetWindowText(dhwnd, "Find Leader");
}

FindLeaderDial::~FindLeaderDial()
{
}

BOOL FindLeaderDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;
      case WM_CLOSE:
         HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
         break;
      case WM_HELP:
         HANDLE_WM_HELP(hWnd, wParam, lParam, onHelp);
         break;
      case WM_CONTEXTMENU:
         HANDLE_WM_CONTEXTMENU(hWnd, wParam, lParam, onContextMenu);
         break;
      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

const DWORD FindLeaderDial::ids[] = {
    FTD_SEARCH,    IDH_FLD_Search,
    FTD_LISTVIEW,  IDH_FLD_LeaderList,
    FTD_FIND,      IDH_FLD_FindLeader,
    0, 0
};

BOOL FindLeaderDial::onHelp(HWND hwnd, LPHELPINFO lparam)
{
  LPHELPINFO lphi = (LPHELPINFO)lparam;
  if (lphi->iContextType == HELPINFO_WINDOW)   // must be for a control
  {
//  WinHelp ((HWND)lphi->hItemHandle, "help\\Wg95Help.hlp", HELP_WM_HELP, (DWORD)(LPVOID)ids);
  }
  return TRUE;
}


void FindLeaderDial::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
  ListView listView(hwnd, FTD_LISTVIEW);
  if(hwndCtl != listView.getHWND())
  {
//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
  }
}

BOOL FindLeaderDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

  WindowControl edit(hwnd, FTD_SEARCH);
  edit.enable(True);

  ListView listView(hwnd, FTD_LISTVIEW);
  RECT r;
  GetClientRect(listView.getHWND(), &r);

  LV_COLUMN lvc;

  // Initialize the LV_COLUMN structure.
  lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
  lvc.fmt = LVCFMT_LEFT;
  lvc.cx = r.right/3;

  // Add the columns.
  for (int i = 0; i < 3; i++)
  {
    lvc.iSubItem = i;

    char colText[100];

    LoadString(APP::instance(), IDS_FTD_COLSTR_LEADER + i,
                   colText, sizeof(colText));

    lvc.pszText = colText;
    if (ListView_InsertColumn(listView.getHWND(), i, &lvc) == -1)
      return FALSE;
  }

  ASSERT(images.getHandle() != NULL);
  listView.setImageList(images.getHandle());

  ASSERT(ListView_GetImageList(listView.getHWND(), LVSIL_SMALL) == images.getHandle());

  addItems(hwnd);

  return TRUE;
}

void FindLeaderDial::onDestroy(HWND hwnd)
{
  SendMessage(d_hwndParent, WM_PARENTNOTIFY, (WPARAM)WM_DESTROY, (LPARAM)hwnd);
}


/*
 * Utility Function to use instead of strrchr
 * Returns beginning of surname
 *
 * Handles trailing spaces
 * Returns beginning of name if no spaces
 */

   /*
    * Forget about surname because it doesn't work with the strange
    * European names.
    * e.g. "Prince Charles of Somewhere"
    *      "Charles V"
    *      "William of Orange"   
    * Probably better off just sorting on the name as given unless
    * we find a better way of specifying the sort key.
    * Options are:
    *   1. Have a special character to specift the sort key
    *       e.g. "Prince %Charles of Somewhere"
    *   2. Detailed knowledge of naming, e.g. knowing that I,II,III,V
    *       are added to the end of a name, and that the words "of"
    *       "van", "von", etc, come after the real name.
    *   3. Different Hard and Soft Space characters
    *       e.g. "Prince Charles_of_Somewhere"
    *            "Charles_V"
    *   4. Sort key character added to leader data, e.g. 'C'
    *   5. Sorting is very simple, but looking for matches based on
    *      what user types, searches each individual word in the names.
    *      e.g. Typing "Pr" or "Ch" will both find Prince Charles.
    *   6. Ignoring the whole problem and just sort on 1st character.
    *      Game created names will be "Sample I", "Sample II", etc
    *      instead of "A.B. Sample"
    */

#if defined(SORT_ON_SURNAME)

static const char* getStartSurName(const char* name)
{

   // const char* p = strrchr(name, ' ');
   // return p ? (p + 1) : name;

   const char* p = name;
   Boolean inSpace = False;
   for(const char* s = name; *s; s++)
   {
      if(*s == ' ')
      {
         inSpace = True;
      }
      else if(inSpace)
      {
         inSpace = False;
         p = s;
      }
   }

   return p;
}
#endif


static int CALLBACK CompareLeaderProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
  ASSERT(lParamSort >= 0);
  ASSERT(lParamSort <= 2);
  // ILeader l1 = static_cast<LeaderListBase::Index>(lParam1);
  ILeader leader1 = lparamToLeader(lParam1); // static_cast<ILeader>(lParam1);
  ASSERT(leader1 != NoLeader);
  ILeader leader2 = lparamToLeader(lParam2);
  ASSERT(leader2 != NoLeader);

  // const Leader* leader1 = campaignData->getLeader(l1);
  // const Leader* leader2 = campaignData->getLeader(l2);

  // extract last name

#ifdef DEBUG
  int length1 = lstrlen(leader1->getName());
  int length2 = lstrlen(leader2->getName());
#endif

#if defined(SORT_ON_SURNAME)
  const char* p1 = getStartSurName(leader1->getName());
  const char* p2 = getStartSurName(leader2->getName());

  ASSERT(p1 < leader1->getName() + length1);
  ASSERT(p2 < leader2->getName() + length2);
#else
  const char* p1 = leader1->getName();
  const char* p2 = leader2->getName();
#endif

  switch(lParamSort)
  {
       // sort by leader
    case 0:
    {
      int result = lstrcmpi(p1, p2);
      if(result != 0)
        return result;
      else
        return lstrcmpi(leader1->getName(), leader2->getName());
    }

    // sort by unit, and leaders within unit
    case 1:
    {
      ASSERT(leader1->getCommand() != NoCommandPosition);
      ASSERT(leader2->getCommand() != NoCommandPosition);

      CommandPosition* cp1 = campaignData->getCommand(leader1->getCommand());
      CommandPosition* cp2 = campaignData->getCommand(leader2->getCommand());

      Rank rank1 = leader1->getRankLevel();
      Rank rank2 = leader2->getRankLevel();

      if(rank1.isLower(rank2))
        return 1;
      if(rank1.isHigher(rank2))
        return -1;
#if 0
      RankEnum rank1 = cp1->getRankEnum();
      RankEnum rank2 = cp2->getRankEnum();

      if(rank1 > rank2)
        return 1;
      if(rank1 < rank2)
        return -1;
#endif

#if 0    // Can't do this... well we could, and use ->getSelf()
         // What we really want to do is link up to common parent
         // and then sort on which child it is
      // this will have to be done differently, probably using some kind of parser
      if(leader1->getCommand() > leader2->getCommand())
        return 1;
      if(leader1->getCommand() < leader2->getCommand())
        return -1;
      else
#endif
      {
        int result = lstrcmpi(p1, p2);
        if(result != 0)
          return result;
        else
          return lstrcmpi(leader1->getName(), leader2->getName());
      }
    }
    // sort by rank, and leaders within rank
    case 2:
    {
      Rank rank1 = leader1->getRankLevel();
      Rank rank2 = leader2->getRankLevel();
      if(rank1.isLower(rank2))
        return 1;
      if(rank1.isHigher(rank2))
        return -1;

      int result = lstrcmpi(p1, p2);
      if(result != 0)
        return result;
      else
        return lstrcmpi(leader1->getName(), leader2->getName());
    }
  }
  return 0;
}

void FindLeaderDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case FTD_SEARCH:
    {
      if(codeNotify == EN_CHANGE)
      {
        char text[100];
        GetDlgItemText(hwnd, FTD_SEARCH, text, 99);
        findItem(hwnd, text);
      }
      break;
    }

    case FTD_IDM_FIND:
    case FTD_FIND:
      onFindLeader();
      onClose(hwnd);
      break;


    case FTD_IDM_SORTCOL1:
    case FTD_IDM_SORTCOL2:
    case FTD_IDM_SORTCOL3:
    {
      ListView listView(hwnd, FTD_LISTVIEW);
      ListView_SortItems(listView.getHWND(), CompareLeaderProc, menuToColumn(id));
      break;
    }

    case FTD_OK:
      onOK(hwnd);
      break;
  }
}

void FindLeaderDial::onClose(HWND hwnd)
{
   // Go through ListView and free up references

   ListView listView(hwnd, FTD_LISTVIEW);
   int count = ListView_GetItemCount(listView.getHWND());
   for(int i = 0; i < count; ++i)
   {
      LV_ITEM item;
      ListView_GetItem(listView.getHWND(), &item);

      ILeader leader = lparamToLeader(item.lParam);

      leader->delRef();

   }

  DestroyWindow(hwnd);
}

void FindLeaderDial::addItems(HWND hwnd)
{
  ListView listView(hwnd, FTD_LISTVIEW);

  const Armies& armies = campaignData->getArmies();

   /*
    * For each Side depending on realism setting
    */

  NationIter nIter = &armies;
  while(++nIter)
  {
    Side side = nIter.current();
    // Nation* nation = armies.getNation(side);

    int i = 0;

    // if(nation->getControl() == CTL_Player)
    if(GamePlayerControl::getControl(side) == GamePlayerControl::Player)
    {
      // ICommandPosition iPresident = nation->getPresident();    // Start with 1st unit
      ICommandPosition iPresident = armies.getPresident(side);

      ConstUnitIter uIter(&armies, iPresident);

      while(uIter.next())
      {
        if(uIter.current() != iPresident)
        {
          const CommandPosition* cp = uIter.currentCommand();
          ASSERT(cp->getLeader() != NoLeader);

          LV_ITEM lvi;
          lvi.mask = LVIF_TEXT | LVIF_IMAGE |  LVIF_PARAM | LVIF_STATE;
          lvi.state = 0;
          lvi.iItem = i;
          lvi.iSubItem = 0;
          lvi.stateMask = 0;
          lvi.pszText = LPSTR_TEXTCALLBACK;

          RankEnum rank = armies.getRank(uIter.current()).getRankEnum();

          switch (rank)
          {
            case Rank_God:
            case Rank_President:
            case Rank_Army:
            case Rank_ArmyWing:
            case Rank_Executive:
              lvi.iImage = side * OBI_IconsPerSide + OBI_Army;
              break;
            case Rank_Corps:
              lvi.iImage = side * OBI_IconsPerSide + OBI_Corps;
              break;
            case Rank_Division:
              lvi.iImage = side * OBI_IconsPerSide + OBI_Division;
              break;
            case Rank_Brigade:
              lvi.iImage = side * OBI_IconsPerSide + OBI_Brigade;
              break;
            default:
#ifdef DEBUG
              throw GeneralError("Missing Rank (%d) in FindLeaderDial::addUnit()\n", (int) rank);
#else
              lvi.iImage = side * OBI_IconsPerSide + OBI_Brigade;
              break;
#endif
          }

          ILeader leader = cp->getLeader();
          leader->addRef();
          lvi.lParam = leaderToLPARAM(leader);
          listView.addItem(lvi);

          i++;
        }
      }
    }
  }
}

void FindLeaderDial::onOK(HWND hwnd)
{
  onClose(hwnd);
}

LRESULT FindLeaderDial::onNotify(HWND hwnd, int id, NMHDR* lpNMHDR)
{
   switch(lpNMHDR->idFrom)
   {
     case FTD_LISTVIEW:
     {
#ifdef DEBUG
       debugLog("WM_NOTIFY from FTD_LISTVIEWd\n");
#endif


       switch(lpNMHDR->code)
       {
         case LVN_GETDISPINFO:
         {

           LV_DISPINFO* pnmv = (LV_DISPINFO*)lpNMHDR;
           if(pnmv->item.mask & LVIF_TEXT)
           {
             ILeader leader = lparamToLeader(pnmv->item.lParam);
             ASSERT(leader != NoLeader);
             ASSERT(leader->getCommand() != NoCommandPosition);

             switch(pnmv->item.iSubItem)
             {
               case 0:
               {
//               pnmv->item.iImage = (int)town.getSide();

                  /*
                   * Fiddle name so surname comes first
                   * e.g. "P. Sample" becomes "Sample P."
                   * so that they are sorted alphabetically
                   */

                 const char* leaderName = leader->getName();
                 // int length = lstrlen(leaderName);

#if defined(SORT_ON_SURNAME)
                 const char* p = getStartSurName(leaderName);
                 char* dest = pnmv->item.pszText;
                 const char* s = p;
                 while(*s)
                  *dest++ = *s++;
                 if(p != leaderName)
                 {
                  *dest++ = ' ';

                  while( (p > leaderName) && (p[-1] == ' ') )
                     p--;

                  s = leaderName;
                  while(s < p)
                     *dest++ = *s++;
                 }
                 *dest = 0;


#if 0
                 /*
                  * find last space within name
                  */

                 const char* p = strrchr(leaderName, ' ');

                 /*
                  * If no space is in the name, then simply copy the name
                  */

                 if(p == 0)
                 {
                  lstrcpy(pnmv->item.pszText, leaderName);
                 }
                 else
                 {
                  /*
                   * Otherwise:
                   *    Copy the surname
                   *    Add a space
                   *    Copy the start of the name
                   */

                  char* dest = pnmv->item.pszText;
                  const char* s = p + 1;
                  while(*s)
                     *dest++ = *s++;
                  *dest++ = ' ';
                  s = leaderName;
                  while(s < p)
                  {
                     *dest++ = *s++;
                  }
                  *dest = 0;

#if 0
                  char buf[100];
                  lstrcpyn(buf, leaderName, (p - leaderName) + 1);
                  p++;

                  ASSERT(p < leaderName + length);
                  lstrcpy(pnmv->item.pszText, p);
                  lstrcat(pnmv->item.pszText, " ");
                  lstrcat(pnmv->item.pszText, buf);
#endif
                 }
#endif
#else
                  // Simple method that doesn't bother with surnames

                  lstrcpy(pnmv->item.pszText, leaderName);
#endif
                 break;
               }

               case 1:
               {
                 CommandPosition* cp = campaignData->getCommand(leader->getCommand());
                 if(cp->isRealUnit())
                   lstrcpy(pnmv->item.pszText, cp->getName());
                 else
                   lstrcpy(pnmv->item.pszText, "Unattached");
                 break;
               }
               case 2:
               {

                 Rank rank = leader->getRankLevel();
                 lstrcpy(pnmv->item.pszText, rank.getRankName(True));
//               rankName(rank, pnmv->item.pszText);
                 break;
               }
             }
           }
           break;
         }

         case LVN_COLUMNCLICK:
         {
           NM_LISTVIEW* lv = (NM_LISTVIEW*)lpNMHDR;
           ListView_SortItems(lpNMHDR->hwndFrom, CompareLeaderProc, lv->iSubItem);

           break;
         }

         case LVN_ITEMCHANGED:
         {
           NM_LISTVIEW* lv = (NM_LISTVIEW*)lpNMHDR;

           ListView listView(lpNMHDR->hwndFrom);
           currentItem = lv->iItem;
           iLeader = lparamToLeader(listView.getItem(lv->iItem));
           ASSERT(iLeader != NoLeader);

           d_campWind->setLeader(iLeader);

           break;
         }

         case NM_RCLICK:
         {
           ASSERT(iLeader != NoLeader);
           Leader* leader = campaignData->getLeader(iLeader);
           RECT r;
           ListView_GetItemRect(lpNMHDR->hwndFrom, currentItem, &r, LVIR_LABEL);
           HDC hdc = GetDC(lpNMHDR->hwndFrom);
           SIZE s;
           GetTextExtentPoint32(hdc, leader->getName(), lstrlen(leader->getName()), &s);
           PixelPoint p(r.left + s.cx, r.bottom);
           findDialPopupMenu(hwnd, p, MENU_FINDLEADERPOPUP);
           ReleaseDC(lpNMHDR->hwndFrom, hdc);
           break;
         }
       }
       break;
     }
   }
   return 0;
}

void FindLeaderDial::onFindLeader()
{
  if(iLeader != NoLeader)
  {
    Leader* leader = campaignData->getLeader(iLeader);

//  MagEnum magLevel = (town.getSize() == TOWN_Capital)?(MagEnum)(town.getSize() + 1):(MagEnum)town.getSize();

    MapWindow* mw = d_mapWind;   // campWind->getMapWindow();
    if(mw)
    {
      ASSERT(leader->getCommand() != NoCommandPosition);
      CommandPosition* cp = campaignData->getCommand(leader->getCommand());
      Location l;
      cp->getLocation(l);
      MapPoint mp;
      mw->locationToMap(l, mp);

      if(mw->getZoomEnum() == 0)
        mw->zoomTo( MagEnum(1), mp);
        // mw->doZoom((MagEnum)(mw->getZoomEnum() + 1), mp);

      else
        mw->setMapPos(mp.getX(), mp.getY());

      mw->setHighlightedUnit(leader->getCommand());
    }
  }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
