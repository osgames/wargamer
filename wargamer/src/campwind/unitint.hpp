/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"

#ifndef UNITINT_HPP
#define UNITINT_HPP

#include "userint.hpp"
#include "campord.hpp"
#include "grtypes.hpp"
#include "routelst.hpp"

/*
 * Unit Iterface class. Replaces UnitOrderTB
 */

class CampaignWindowsInterface;
class CampaignData;
class MapWindow;
class CompleteOrder;
class UnitDialog;
class TrackingWindow;
class CampaignComboInterface;
class DrawDIBDC;
class UnitMenu_Int;
class UnitInfo_Int;
class UnitOrder_Int;
class ReorgOB_Int;

class CampaignUnitInterface : public CampaignUserInterface {
   public:
      class UnitInterfaceMode
      {
         public:
         enum Mode
         {
            Waiting,        // waiting for player to do something
            //        OverUnit,       // mouse if over unit
            SettingUpMove,  // player has plotted a route
            SettingUpOrder, // player has clicked on unit

            HowMany
         };
      };

   private:

      /*
      * Interface windows
      */

      UnitInfo_Int* d_infoWindow;          // detailed
      UnitOrder_Int* d_orderWindow;        // orders
      TrackingWindow* d_trackingWindow;    // pop-up info
      UnitMenu_Int* d_menu;                // orders menu
      ReorgOB_Int* d_obWindow;

      /*
      * Interface to other parts of game
      */

      CampaignWindowsInterface* d_campWind;
      const CampaignData* d_campData;
      MapWindow* d_mapWindow;

      /*
      * Interface mode
      */

      UnitInterfaceMode::Mode d_mode;

      /*
      * Current Data
      */

      StackedUnitList d_stackedUnits;
      UnitListID d_unitID;
      CompleteOrder d_orderValues;
      ConstICommandPosition d_overThisUnit;
      RouteList d_routeList;

      /*
      * Map tracking
      */

      TrackMode d_trackMode;         // are we tracking units or towns?
      Location d_startMouseLocation; // mouse location at the start of a drag

      /*
      * flags
      */

      enum
      {
         TrackingUnit      = 0x01,  // tracking a unit?
            TrackingDestTown  = 0x02,  // or tracking dest town?
            CanOrderUnit      = 0x04   // can we order current unit?
      };

      UBYTE d_flags;

      public:
      CampaignUnitInterface(CampaignWindowsInterface* cwi, const CampaignData* campData, MapWindow* mapWindow);
      ~CampaignUnitInterface();

      /* ---------------------------------------------
      * virtual functions from CampaignUserInterface
      */

      TrackMode getTrack()
      { return d_trackMode;
      }      // Return how to do tracking

      //  void hide() {}
      //  void show() {}

      /*
      * Mouse Tracking functions
      */

      void onLClick(MapSelect& info);
      void onRClick(MapSelect& info);
      void onButtonDown(MapSelect& info);
      void onStartDrag();
      void onDrag(MapSelect& info);// {}
      void onEndDrag(MapSelect& info);

      void overObject(MapSelect& info);
      void notOverObject();

      /*
      * Map Drawing
      */

      void onDraw(DrawDIBDC* dib, MapSelect& info);

      void update();// {}             // Info may have changed
      //   void addObject(ICommandPosition cpi);
      //   void addObject(ITown town) {}
      void updateZoom()
      {
      }       // update map arrows when zooming

      /*
      * Sending Orders
      */

      //  void setOrderDate(Date& date);
      void sendOrder(ICommandPosition cpi);
      void cancelOrder();

      /*
      * calling various windows
      */

      void runOrders(const PixelPoint& p);
      void runInfo(const PixelPoint& p);
      void runOB(const PixelPoint& p);
      void runMenu(const PixelPoint& p);


      /*
      * Misc.
      */

      void initOrderWindow();
      CampaignWindowsInterface* campWindows() const
      { return d_campWind;
      }
      const void* getData() const
      { return &d_stackedUnits;
      }   // derived-class defined data
      void CampaignUnitInterface::clearTrackingFlags()
      {
         d_flags &= ~TrackingDestTown;
         d_flags &= ~TrackingUnit;
      }

      /*
      * Clean up
      */

      void destroy();

      /*
      * Fuctions Unit Dialogs call
      */

      void toggle()
      {
      }
      void initUnits(Boolean all)
      {
      }
      void syncCombos(int i)
      {
      }

   private:

   /*
    * Utilities
    */

      void calcMoveOrderDialPosition(const Location& endLocation, PixelPoint& endP);
};







#endif

