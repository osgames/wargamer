/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "townmenu.hpp"
#include "wind.hpp"
#include "scenario.hpp"
// #include "generic.hpp"
#include "app.hpp"
#include "fonts.hpp"
#include "resdef.h"
#include "wmisc.hpp"
#include "cmenu.hpp"
#include "userint.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "scrnbase.hpp"
#include "scn_img.hpp"

/*----------------------------------------------------------
 * Unit Menu Implementation
 */

class TownMenu_Imp : public WindowBaseND {
    CampaignUserInterface* d_owner;
    HWND d_hParent;
    CampaignWindowsInterface* d_campWind;
    const CampaignData* d_campData;
    CustomMenu d_menu;

    ITown d_town;

    PixelPoint d_point;

  public:
    TownMenu_Imp(CampaignUserInterface* owner, HWND hParent, CampaignWindowsInterface* campWind,
       const CampaignData* campData);
    ~TownMenu_Imp();

    void run(ITown town, const PixelPoint& p);

    // void destroy()
    // {
    //   d_menu.destroy();
    //   DestroyWindow(getHWND());
    // }

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd) {}
    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);
    void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);

    void showInfo();
    void showBuilds();
    void updateMenu();
};

TownMenu_Imp::TownMenu_Imp(CampaignUserInterface* owner, HWND hParent,
   CampaignWindowsInterface* campWind, const CampaignData* campData) :
  d_owner(owner),
  d_hParent(hParent),
  d_campWind(campWind),
  d_campData(campData),
  d_town(NoTown)
{
  HWND hWnd = createWindow(
      0,
      // GenericClass::className(), 
        NULL,
      WS_POPUP,
      0, 0, 0, 0,
      hParent, NULL   // , APP::instance()
  );

  ASSERT(hWnd != NULL);
}

TownMenu_Imp::~TownMenu_Imp()
{
    d_menu.destroy();
    selfDestruct();
}

LRESULT TownMenu_Imp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_COMMAND,  onCommand);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);

      default:
        return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL TownMenu_Imp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  const LONG dbY = ScreenBase::dbY();
  const LONG baseY = ScreenBase::baseY();

  /*
   * Set menu data
   */

  CustomMenuData md;
  md.d_type = CustomMenuData::CMT_Popup;
  md.d_menuID = MENU_TOWNORDERPOPUP;
  md.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
  md.d_checkImages = ScenarioImageLibrary::get(ScenarioImageLibrary::CustomMenuImages);
  md.d_hCommand = hWnd;
  md.d_hParent = d_hParent;
  md.d_borderColors = scenario->getBorderColors();
  scenario->getColour("MapInsert", md.d_color);
  scenario->getColour("Orange", md.d_hColor);

  /*
   * Set menu fonts (one normal, the other underlined
   */

  LogFont lf;
  lf.height((8 * dbY) / baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);
  md.d_hFont = font;

  lf.underline(true);
  font.set(lf);
  md.d_hULFont = font;

  /*
   * initialize menu
   */

  d_menu.init(md);

  return TRUE;
}

void TownMenu_Imp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case IDM_TM_INFO:
      showInfo();
      break;

    case IDM_TM_BUILDS:
      showBuilds();
      break;
  }
}

void TownMenu_Imp::showInfo()
{
  d_owner->runInfo(d_point);
}

void TownMenu_Imp::showBuilds()
{
  d_owner->runOrders(d_point);
}

void TownMenu_Imp::run(ITown town, const PixelPoint& pt)
{
  d_town = town;
  d_point = pt;

  updateMenu();
  d_menu.run(pt);
}

void TownMenu_Imp::updateMenu()
{
  ASSERT(d_town != NoTown);
}

/*----------------------------------------------------------
 * Unit Menu Interface
 */

TownMenu_Int::TownMenu_Int(CampaignUserInterface* owner, HWND hParent,
     CampaignWindowsInterface* campWind, const CampaignData* campData) :
  d_townMenu(new TownMenu_Imp(owner, hParent, campWind, campData))
{
  ASSERT(d_townMenu);
}

TownMenu_Int::~TownMenu_Int()
{
    delete d_townMenu;
  // if(d_townMenu)
  //   d_townMenu->destroy();
}

void TownMenu_Int::run(ITown town, const PixelPoint& p)
{
  if(d_townMenu)
    d_townMenu->run(town, p);
}

// void TownMenu_Int::destroy()
// {
//   if(d_townMenu)
//   {
//     d_townMenu->destroy();
//     d_townMenu = 0;
//   }
// }

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
