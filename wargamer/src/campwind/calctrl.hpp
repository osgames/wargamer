/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef CALCTRL_HPP
#define CALCTRL_HPP


/*
 * A Calender Custom Control
 */

struct Date;
class DrawDIB;

namespace CalenderControl
{
  HWND make(HINSTANCE instance, HWND hParent, int id, const Date& date, int x, int y, int cx, int cy);
  void setMonth(HWND hwnd, const Date& date);
  void setBk(HWND hwnd, const DrawDIB* fill);
  void getCurrentDate(HWND hwnd, Date& date);

  void initCustomScrollBar(HINSTANCE instance);
  extern const char CALENDERCONTROLCLASS[];
}

using namespace CalenderControl;

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
