/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef TOOLBOX_HPP
#define TOOLBOX_HPP

/*
 * Campaign Tool Box Window
 * Includes buttons for
 *   1. Resource Mode
 *   2. Unit Mode
 *   3. Toggle Message Window
 *   4. Toggle Weather Window
 *   5. Toggle Tiny Map
 */


class CampaignInterface;
class CampaignWindowsInterface;
class ToolBoxWindow;

class CampaignToolBox {
    ToolBoxWindow* d_toolBox;
  public:
    CampaignToolBox() :
      d_toolBox(0) {}
    CampaignToolBox(HWND parent, CampaignInterface* campGame, CampaignWindowsInterface* campWind) :
      d_toolBox(0) { create(parent, campGame, campWind); }

    ~CampaignToolBox() {}

    void create(HWND parent, CampaignInterface* campGame, CampaignWindowsInterface* campWind);
    void show();
    void destroy();
};

#if 0
class CampaignToolBox {
  public:
    static void create(HWND parent, CampaignInterface* campGame, CampaignWindowsInterface* campWind);
    static void show();
    static void destroy();
};
#endif

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
