/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Unit Information Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.4  2002/11/16 18:03:32  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "unitinfo.hpp"
#include "scenario.hpp"
#include "wmisc.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "campord.hpp"
#include "realord.hpp"     // CampaignOrderUtil::getDescription
#include "logwin.hpp"
#include "res_str.h"       // Resource Strings
#include "scn_res.h"
#include "colours.hpp"
#include "imglib.hpp"
#include "random.hpp"
#include "options.hpp"
#include "simpstr.hpp"
#include "town.hpp"
#include "unitlist.hpp"
#include "palette.hpp"
#include "fonts.hpp"
#include "resstr.hpp"
#include "scn_img.hpp"
#include "armyutil.hpp"
#include "scrnbase.hpp"
#include "piclib.hpp"
#include "dib_blt.hpp"
#include "dc_hold.hpp"
#include "spacing.hpp"
// #include "winfile.hpp"
#include "gLeaderInfo.hpp"

#include <sstream>

namespace      // Internal namespace
{


/*----------------------------------------------
 * Unit Attributes class
 */

// const LONG dbUnits = GetDialogBaseUnits();
// const LONG dbX = LOWORD(dbUnits);
// const LONG dbY = HIWORD(dbUnits);

class UnitAttributes {
   Attribute morale;
   Attribute fatigue;
   Attribute supply;

   Attribute initiative;
   Attribute aggression;
   Attribute charisma;
   Attribute ability;
   Attribute health;

   UINT numCorps;
   UINT numDivs;

   SPCount numInfantry;          // SP Counts (excluding Stragglers)
   SPCount numCavalry;
   SPCount numArtillery;
   SPCount numOther;

   SPCount spCount;              // Total SP (excluding Stragglers)

   StringPtr orders;
   StringPtr action;

   Boolean limitedInfo;

public:
   UnitAttributes();
   void makeAttributes(const ConstICommandPosition& cpi);

   Attribute getMorale() const { return morale; }
   Attribute getFatigue() const { return fatigue; }
   Attribute getSupply() const { return supply; }

   Attribute getInitiative() const { return initiative; }
   Attribute getAggression() const { return aggression; }
   Attribute getCharisma() const { return charisma; }
   Attribute getAbility() const { return ability; }
   Attribute getHealth() const { return health; }

   UINT getNumCorps() const {return numCorps;}
   UINT getNumDivs() const {return numDivs;}

   UINT getNumInfantry() const {return numInfantry;}
   UINT getNumCavalry() const {return numCavalry;}
   UINT getNumArtillery() const {return numArtillery;}
   UINT getNumOther() const {return numOther;}

   SPCount getSPCount() const { return spCount; }

   const char* getOrders() const { return orders; }
   const char* getAction() const { return action; }

   Boolean operator == (const UnitAttributes& u) const;

   Boolean operator != (const UnitAttributes& u) const
   {
      return !(*this == u);
   }

   Boolean isLimitedInfo() const { return limitedInfo; }

   void reset();
};

/*=============================================================
 * UnitAttributes Class Implementation
 */

UnitAttributes::UnitAttributes()
{
   reset();
}

void UnitAttributes::reset()
{
   // strength = 0;
   // experience = 0;
   morale = 0;
   fatigue = 0;
   supply = 0;

   initiative = 0;
   aggression = 0;
   charisma = 0;
   ability = 0;
   health = 0;

   numCorps = 0;
   numDivs = 0;

   numInfantry = 0;
   numCavalry = 0;
   numArtillery = 0;
   numOther = 0;

   spCount = 0;
   // stragglerCount = 0;

   orders = "???";
   action = "???";

   limitedInfo = False;
}


void UnitAttributes::makeAttributes(const ConstICommandPosition& cpi)
{
#if !defined(EDITOR)
   reset();

   const Armies* armies = &campaignData->getArmies();
   const CommandPosition* cp = armies->getCommand(cpi);

   limitedInfo = static_cast<Boolean>(CampaignOptions::get(OPT_FogOfWar) && !CampaignArmy_Util::isUnitOrderable(cpi));


   /*
    * Get Unit's attributes
    *
    * If no Fog of War or unit is orderable get all attributes,
    * Otherwise just get valid info according to InfoQuality level
    */


   if(!limitedInfo || cp->getInfoQuality() == CommandPosition::FullInfo)
   {
     morale = cp->getMorale();
     fatigue = cp->getFatigue();
     supply = cp->getSupply();

     // orders = (const char*)cp->getCurrentOrder()->getDescription(OD_BRIEF);
     orders = CampaignOrderUtil::getDescription(campaignData, cp->getCurrentOrder(), OD_BRIEF).toStr();
   }

   /*
    * Get Leader's attributes
    */

   if(!limitedInfo || cp->getInfoQuality() >= CommandPosition::VeryLimitedInfo)
   {
     if(cp->getLeader() != NoLeader)
     {
       const Leader* leader = armies->getLeader(cp->getLeader());

       aggression = leader->getAggression();
       initiative = leader->getInitiative(True);
       charisma = leader->getCharisma(True);
       ability = leader->getTactical(True);
       health = leader->getHealth();
     }
   }

   /*
    * If info level is VeryLimitedInfo then only get total SP Count
    *
    * and add or subtract a random amount within 20% of actual count
    */

   if(limitedInfo && cp->getInfoQuality() == CommandPosition::VeryLimitedInfo)
   {
     spCount = armies->getUnitSPCount(cpi, True);
   }

   /*
    * else, if not limited info or limited info is >= LimitedInfo, get
    * break down of SPs, and subordinate units
    */

   if(!limitedInfo || cp->getInfoQuality() >= CommandPosition::LimitedInfo)
   {
     /*
      * Get info about strength points
      */

     ConstUnitIter iter(armies, cpi);
     while(iter.next())
     {
       const CommandPosition* cp = iter.currentCommand();
       RankEnum rank  = cp->getRankEnum();

       if(rank == Rank_Corps)
         numCorps++;
       else if(rank == Rank_Division)
         numDivs++;

       ConstStrengthPointIter spiter(armies, cp);
       while(++spiter)
       {
         // const StrengthPoint* sp = spiter.current();
         ConstISP sp = spiter.current();

         {
            spCount++;

            const UnitTypeTable& table = campaignData->getUnitTypes();
            ASSERT(sp->getUnitType() < table.entries());

            const UnitTypeItem& item = table[sp->getUnitType()];
            switch(item.getBasicType())
            {
            case BasicUnitType::Infantry:
               numInfantry++;
               break;
            case BasicUnitType::Cavalry:
               numCavalry++;
               break;
            case BasicUnitType::Artillery:
               numArtillery++;
               break;
            case BasicUnitType::Special:
               numOther++;
               break;
            }

         }
       }
     }

     /*
      * Get Action
      */

     action = (const char*)cp->getActionDescription(OD_BRIEF, campaignData->getTick());
     // cp->clearActionDescription();

   }
#endif

}

Boolean UnitAttributes::operator == (const UnitAttributes& u) const
{
  if(u.getMorale() == morale &&
     u.getFatigue() == fatigue &&
     u.getSupply() == supply &&
     u.getInitiative() == initiative &&
     u.getAbility() == ability &&
     u.getCharisma() == charisma &&
     u.getAggression() == aggression &&
     u.getHealth() == health &&
     u.getNumCorps() == numCorps &&
     u.getNumDivs() == numDivs &&
     u.getNumInfantry() == numInfantry &&
     u.getNumCavalry() == numCavalry &&
     u.getNumArtillery() == numArtillery &&
     u.getNumOther() == numOther &&
     u.getSPCount() == spCount &&
     lstrcmp(u.getOrders(), orders) == 0 &&
     lstrcmp(u.getAction(), action) == 0)

    return True;
  else
    return False;
}


/*-------------------------- Display Strings --------------------------------
 *
 */

// class to hold array indeces for unit info resource strings

class UnitInfoString {
public:
  enum ID {
    Morale,
    Fatigue,
    Supply,
    Initiative,
    Orders,

    Corps,
    Divisions,
    Infantry,
    Cavalry,
    Artillery,
    Other,

    Strength,

    Leader,
    SupremeLeader,
    Aggression,
    Charisma,
    Ability,

    LeaderUnknown,
    UnidentifiedUnit,

    MovingTo,
    ForceMarchTo,
    EasyMarchTo,
    NormalMarchTo,

    InfoUnavailable,

    OrderParagraph,
    PostureParagraph,
    EnemyParagraph,
    AdvancedOrderParagraph,

    Unit,
    Formations,

    Guns,

    CanControl,

    Staff,
    Subordination,
    CommandAbility,

    Action,
    StrengthUnknown,
    Str,

    Inf,
    Cav,

    Nationality,
    Unknown,

    HowMany
  };
};

// resource ids
static const int s_resourceIDs[UnitInfoString::HowMany] = {
   IDS_UI_MORALE,
   IDS_UI_FATIGUE,
   IDS_UI_SUPPLY,
   IDS_UI_INITIATIVE,
   IDS_UI_ORDERS,
   IDS_UI_CORPS,
   IDS_UI_DIVISIONS,
   IDS_UI_INFANTRY,
   IDS_UI_CAVALRY,
   IDS_UI_ARTY,
   IDS_UI_OTHER,
   IDS_UI_STRENGTH,
   IDS_UI_LEADER,
   IDS_UI_SUPREMELEADER,
   IDS_UI_AGGRESSION,
   IDS_UI_CHARISMA,
   IDS_UI_ABILITY,
   IDS_UI_LEADERUNKNOWN,
   IDS_UI_UNIDUNIT,
   IDS_UI_MOVINGTO,
   IDS_UI_FORCEMARCHTO,
   IDS_UI_EASYMARCHTO,
   IDS_UI_NORMALMARCHTO,
   IDS_UI_INFOUNAVAILABLE,
   IDS_UI_ORDERPARAGRAPH,
   IDS_UI_POSTUREPARAGRAPH,
   IDS_UI_ENEMYPARAGRAPH,
   IDS_UI_AOPARAGRAPH,
   IDS_UI_UNIT,
   IDS_UI_FORMATIONS,
   IDS_GunsAbrev,
   IDS_UI_CANCONTROL,
   IDS_UI_STAFF,
   IDS_UI_SUBORDINATION,
   IDS_UI_COMMANDABILITY,
   IDS_UI_ACTION,
   IDS_UI_STRENGTHUNKNOWN,
   IDS_UI_STR,
   IDS_UI_INF,
   IDS_UI_CAV,
   IDS_UI_NATIONALITY,
   IDS_UI_UNKNOWN,
};

static ResourceStrings s_strings(s_resourceIDs, UnitInfoString::HowMany);

/*
 * Utility functions
 */

/*
 * Draw Graph:
 *
 *   x,y = position of top left of entire thing
 *   barOffsetX = x coordinate of actual bar
 *   barCX,barCY = size of actual bar
 *
 */


void drawAttributeGraph(DrawDIBDC* dib, const char* text,
    int x, int y, int barOffsetX, int barCX, int barCY, Attribute value, Boolean noInfo)
{
  ASSERT(dib);
  wTextOut(dib->getDC(), x, y, text);

  static SIZE s;
  GetTextExtentPoint32(dib->getDC(), text, lstrlen(text), &s);

  const int barY = y + ((s.cy - barCY) / 2);
  dib->drawBarChart(barOffsetX, barY, (noInfo) ? 0 : value, Attribute_Range, barCX, barCY);

  if(noInfo)
    dib->darkenRectangle(barOffsetX, barY, barCX, barCY, 50);
}

#if 0
void drawAttributeGraph(DrawDIBDC* dib, const char* text, int x, int y, Attribute value, Boolean noInfo)
{
  ASSERT(dib);

  const int barOffsetX = x + ((30 * ScreenBase::dbX()) / ScreenBase::baseX());
  const int barCX = ((40 * ScreenBase::dbX()) / ScreenBase::baseX());
  const int barCY = ((5 * ScreenBase::dbY()) / ScreenBase::baseY());

  drawAttributeGraph(dib, text, x, y, barOffsetX, barCX, barCY, value, noInfo);
}
#endif

const char* enemyUnitOrderText(const CampaignData* campData, const ConstICommandPosition& cpi)
{
#ifdef EDITOR
  static SimpleString text;
  text += "?";
  return text.toStr();
#else
  ASSERT(campData != 0);
  ASSERT(cpi != NoCommandPosition);
  const CommandPosition* cp = campData->getCommand(cpi);

  static SimpleString text;

  if(cp->getInfoQuality() >= CommandPosition::LimitedInfo)
  {
    if(cp->isMoving())
    {
      text = (cp->getInfoQuality() == CommandPosition::LimitedInfo) ? s_strings.get(UnitInfoString::MovingTo) : //"Moving to " :
             (cp->doingForceMarch()) ? s_strings.get(UnitInfoString::ForceMarchTo) : //"Force Marching to " :
             (cp->getCurrentOrder()->shouldEasyMarch()) ? s_strings.get(UnitInfoString::EasyMarchTo) : // "Easy Marching to " :
             s_strings.get(UnitInfoString::NormalMarchTo); // "Normal Marching to ";

      ASSERT(cp->getDestTown() != NoTown);

      const Town& t = campData->getTown(cp->getDestTown());
      text += t.getNameNotNull();
    }
    else
    {
      text = cp->getActionDescription(OD_LONG, campData->getTick());
      // cp->clearActionDescription();
    }
  }
  else
  {
    text = s_strings.get(UnitInfoString::InfoUnavailable); //"Information unavailable";
  }

  ASSERT(text.toStr());
  return text.toStr();
#endif
}



void drawTextRect(DrawDIBDC* dib, const PixelRect& r, const SimpleString& title, const SimpleString& value, ColourIndex ulColor, int fontHeight)
{
   SimpleString s(title);
   s += " : ";
   s += value;

   int w = textWidth(dib->getDC(), title.toStr());

   wTextOut(dib->getDC(), r, s.toStr());
   dib->hLine(r.left(), r.top() + fontHeight, w, ulColor);
}


};    // end of private namespace




void UnitInfoData::init()
{
   d_limitedInfo = (CampaignOptions::get(OPT_FogOfWar) &&
                         !CampaignArmy_Util::isUnitOrderable(d_cpi));

   if(d_dRect.width() == 0)
   {
      d_dRect = PixelRect(0, 0, ScreenBase::x(80), ScreenBase::y(62));
   }

   d_offsetX = ScreenBase::x(1);
   d_offsetY = ScreenBase::y(1);

   int nameHeight = ScreenBase::y(2*8+2);    // 2 * 10 + 2

   d_nameRect = PixelRect(d_dRect.left() + d_offsetX, d_dRect.top() + d_offsetY,
                          d_dRect.right() - d_offsetX, d_dRect.top() + d_offsetY + nameHeight);

   d_mainRect = PixelRect(d_dRect.left() + d_offsetX, d_nameRect.bottom() + d_offsetY,
                          d_dRect.right() - d_offsetX, d_dRect.bottom() - d_offsetY);

}


void UnitInfoData::drawNameDib(const PixelRect& rect, bool forLeader, CompactMode compact)
{
#if !defined(EDITOR)
   ASSERT(d_campData != 0);
   ASSERT(d_dib != 0);
   ASSERT(d_cpi != NoCommandPosition);

   ConstILeader leader = d_cpi->getLeader();

   ASSERT(leader != NoLeader);

   /*
    * Put flag icon
    */

   Nationality nationality;
   if(!d_limitedInfo || (d_cpi->getInfoQuality() > CommandPosition::NoInfo))
      nationality = forLeader ? leader->getNation() : d_cpi->getNation();
   else
      nationality = scenario->getDefaultNation(d_cpi->getSide());

   /*
    * Put leader name
    */

   const char* leaderName;
   const char* unitName;

   const char* strUnit = s_strings.get(UnitInfoString::Unit);
   const char* strLeader = s_strings.get(UnitInfoString::Leader);

   if(!d_limitedInfo || (d_cpi->getInfoQuality() >= CommandPosition::VeryLimitedInfo))
   {
      leaderName = leader->getNameNotNull();
      unitName = d_cpi->getNameNotNull();
   }
   else
   {
      leaderName = s_strings.get(UnitInfoString::LeaderUnknown);
      unitName = s_strings.get(UnitInfoString::UnidentifiedUnit);
   }

   if(forLeader)
   {
      std::swap(leaderName, unitName);
      std::swap(strLeader, strUnit);
   }

   drawNames(rect, unitName, leaderName, strUnit, strLeader, nationality, compact==Compact);

   /*
    * If leader is a specialist, put icon
    */

   if(!compact && forLeader)
   {
      if(leader->isSpecialist())
      {
         const ImageLibrary* specialistIcons = ScenarioImageLibrary::get(ScenarioImageLibrary::SpecialistIcons);

         // x = (110 * ScreenBase::dbX()) / ScreenBase::baseX();


         // temporary until this is straightened out in compos.hpp
         UWORD index = 0;

         if(leader->isSpecialistType(Specialist::Cavalry))
         {
            index = (leader->isCommandingType()) ? 0 : 2;
         }
         else if(leader->isSpecialistType(Specialist::Artillery))
         {
            index = (leader->isCommandingType()) ? 1 : 3;
         }


         int w;
         int h;
         specialistIcons->getImageSize(index, w, h);

         int x = rect.right() - w - 2 - s_borderThickness;
         int y = rect.top() + (rect.height() - h) / 2;

         specialistIcons->blit(d_dib, index, x, y);
      }
  }

  // d_dib->setFont(oldFont);

#endif
}


/*
 * Draw Summary Tab
 */

void UnitInfoData::drawSummaryDib()
{
#if !defined(EDITOR)
  ASSERT(d_campData != 0);
  ASSERT(d_dib != 0);
  ASSERT(d_cpi != NoCommandPosition);

  drawNameDib(d_nameRect, false);

  /*
   * get strings
   */

   const char* strNationality = s_strings.get(UnitInfoString::Nationality);

  /*
   * Calculate sizes
   */

   const int ySpacing = ScreenBase::y(7);    // was 8
   const int x1 = ScreenBase::x(80);
   const int x2 = x1-2;
   const int h1 = ySpacing + (s_borderThickness + 1) * 2;
   const int margin = s_borderThickness + 2;

   PixelRect nationalityRect(d_mainRect.left(), d_mainRect.top(), x2, d_mainRect.top() + h1);
   PixelRect ordersRect(d_mainRect.left(), d_mainRect.bottom()-h1, x2, d_mainRect.bottom());
   PixelRect attrRect(d_mainRect.left(), nationalityRect.bottom()+2, x2, ordersRect.top()-2);
   PixelRect formationRect(x1, d_mainRect.top(), d_mainRect.right(), d_mainRect.bottom());

   /*
    * Draw Rectangles
    */

   // ColourIndex borderColor = d_dib->getColour(Colours::Black);
   // drawThickBorder(d_dib, nationalityRect, s_borderThickness, borderColor);
   // drawThickBorder(d_dib, ordersRect, s_borderThickness, borderColor);
   // drawThickBorder(d_dib, attrRect, s_borderThickness, borderColor);
   // drawThickBorder(d_dib, formationRect, s_borderThickness, borderColor);
   drawThickBorder(nationalityRect);
   drawThickBorder(ordersRect);
   drawThickBorder(attrRect);
   drawThickBorder(formationRect);


   /*
    * Create Fonts
    */

   LogFont lf;
   lf.height(ySpacing);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);

   Font nFont;
   nFont.set(lf);

   DCFontHolder fontHold(d_dib->getDC(), nFont);

   // HFONT oldFont = d_dib->setFont(nFont);

   /*
    * get Attributes
    */

   UnitAttributes ua;
   ua.makeAttributes(d_cpi);

   /*
    * Put nationality
    */

   {
      // d_dib->setFont(nFont);
      fontHold.set(nFont);

      int x = nationalityRect.left() + margin;
      int y = nationalityRect.top() + (nationalityRect.height() - ySpacing) / 2;

      const char* natName;

      if(d_limitedInfo && d_cpi->getInfoQuality() == CommandPosition::NoInfo)
         natName = scenario->getSideName(d_cpi->getSide());
      else
         natName = scenario->getNationAdjective(d_cpi->getNation());

      wTextPrintf(d_dib->getDC(), x, y, "%s: %s",
         (const char*) strNationality,
         (const char*) natName);
   }

   /*
    * Put Attributes
    */

   {
      bool isLimited = d_limitedInfo && (d_cpi->getInfoQuality() < CommandPosition::FullInfo);

      int x = attrRect.left() + margin;
      int y = attrRect.top() + margin;
      int h = attrRect.bottom() - margin - y;

      // d_dib->setFont(nFont);
      fontHold.set(nFont);

      EvenSpacing yGen(4, y, h, ySpacing);

      const char* strMorale = s_strings.get(UnitInfoString::Morale);
      const char* strFatigue = s_strings.get(UnitInfoString::Fatigue);
      const char* strSupply = s_strings.get(UnitInfoString::Supply);
      const char* strInitiative = s_strings.get(UnitInfoString::Initiative);

      int tw = textWidth(d_dib->getDC(), strMorale);
      tw = maximum(tw, textWidth(d_dib->getDC(), strFatigue));
      tw = maximum(tw, textWidth(d_dib->getDC(), strSupply));
      tw = maximum(tw, textWidth(d_dib->getDC(), strInitiative));

      int barXOffset = x + tw + margin;
      int barWidth = attrRect.right() - barXOffset - margin;
      int barHeight = ySpacing - 2;


      y = yGen.nextY();
      drawAttributeGraph(d_dib, strMorale,
         x, y, barXOffset, barWidth, barHeight, ua.getMorale(), isLimited);

      /*
       * Put Fatigue
       */

      y = yGen.nextY();
      drawAttributeGraph(d_dib, strFatigue,
         x, y, barXOffset, barWidth, barHeight, ua.getFatigue(), isLimited);

      /*
       * Put Supply
       */

      y = yGen.nextY();
      drawAttributeGraph(d_dib, strSupply,
         x, y, barXOffset, barWidth, barHeight, ua.getSupply(), isLimited);

      /*
       * Put Leader's initiative
       */

      y = yGen.nextY();
      drawAttributeGraph(d_dib, strInitiative,
         x, y, barXOffset, barWidth, barHeight, ua.getInitiative(), isLimited);
   }

   /*
    * Put brief order description
    */

   {
      // d_dib->setFont(nFont);
      fontHold.set(nFont);

      LONG x = ordersRect.left() + margin;
      LONG y = ordersRect.top() + (ordersRect.height() - ySpacing) / 2;

      const char* text = s_strings.get(UnitInfoString::Orders);
      static const char* strColon = ": ";

      wTextOut(d_dib->getDC(), x, y, text);
      x += textWidth(d_dib->getDC(), text);
      wTextOut(d_dib->getDC(), x, y, strColon);
      x += textWidth(d_dib->getDC(), strColon);

      const CampaignOrder* order = d_cpi->getCurrentOrder();
      SimpleString orderText = (d_limitedInfo) ?
            enemyUnitOrderText(d_campData, d_cpi) :
            CampaignOrderUtil::getOrderText(d_campData, order, OD_BRIEF);

      RECT r;
      SetRect(&r, x, y, ordersRect.right()-margin, ordersRect.bottom());
      DrawText(d_dib->getDC(), orderText.toStr(), orderText.length(), &r, DT_WORDBREAK);
   }

   /*
    * Put number of formations
    */

   {
      bool isLimited = d_limitedInfo && (d_cpi->getInfoQuality() < CommandPosition::LimitedInfo);

      int y = formationRect.top() + margin;
      int h = formationRect.bottom() - y - margin;

      const int nSections = 9;

      int fontHeight = h / nSections;

      LogFont lf;
      lf.height(fontHeight);
      lf.weight(FW_MEDIUM);
      lf.face(scenario->fontName(Font_Bold));
      lf.italic(false);

      Font attrFont;
      attrFont.set(lf);

      lf.underline(true);
      Font lFont;
      lFont.set(lf);

      EvenSpacing yGen(9, y, h, fontHeight);

      // d_dib->setFont(lFont);
      fontHold.set(lFont);

      int x = formationRect.left() + margin;
      y = yGen.nextY();    // 1

      wTextOut(d_dib->getDC(), x, y, s_strings.get(UnitInfoString::Formations));

      const char* strCorps = s_strings.get(UnitInfoString::Corps);
      const char* strDivisions = s_strings.get(UnitInfoString::Divisions);
      static const char* strColon = " : ";

      // d_dib->setFont(attrFont);
      fontHold.set(attrFont);

      int w = maximum(textWidth(d_dib->getDC(), strCorps),
                  textWidth(d_dib->getDC(), strDivisions) );
      int x1 = x + w;
      int x2 = x1 + textWidth(d_dib->getDC(), strColon);

      y = yGen.nextY();    // 2

      // Corps

      wTextOut(d_dib->getDC(), x, y, strCorps);
      wTextOut(d_dib->getDC(), x1, y, strColon);

      if(isLimited)
         wTextOut(d_dib->getDC(), x2, y, "???");
      else
         wTextPrintf(d_dib->getDC(), x2, y, "%d", ua.getNumCorps());

      // divisions

      y = yGen.nextY();    // 3

      wTextOut(d_dib->getDC(), x, y, strDivisions);
      wTextOut(d_dib->getDC(), x1, y, strColon);

      if(isLimited)
         wTextOut(d_dib->getDC(), x2, y, "???");
      else
         wTextPrintf(d_dib->getDC(), x2, y, "%d", ua.getNumDivs());

      /*
       * Put number of each basic unit types.
       * Infantry value is in # of men. Each SP = 1000 men
       * Cavalry value is in # of men. Each SP = 500 men
       * Artillery value is in # of guns. Each SP = 12 guns
       * Other value is in # of men. Each SP = 250 men
       */

      // d_dib->setFont(lFont);
      fontHold.set(lFont);

      y = yGen.nextY();    // 4: skip row
      y = yGen.nextY();    // 5

      wTextOut(d_dib->getDC(), x, y, s_strings.get(UnitInfoString::Strength));


      // d_dib->setFont(attrFont);
      fontHold.set(attrFont);

      const char* strInf = s_strings.get(UnitInfoString::Infantry);
      const char* strCav = s_strings.get(UnitInfoString::Cavalry);
      const char* strArt = s_strings.get(UnitInfoString::Artillery);
      const char* strOther = s_strings.get(UnitInfoString::Other);

      w = textWidth(d_dib->getDC(), strInf);
      w = maximum(w, textWidth(d_dib->getDC(), strCav));
      w = maximum(w, textWidth(d_dib->getDC(), strArt));
      w = maximum(w, textWidth(d_dib->getDC(), strOther));
      x1 = x + w;
      x2 = x1 + textWidth(d_dib->getDC(), strColon);

      // infantry

      y = yGen.nextY();    // 6

      wTextOut(d_dib->getDC(), x, y, strInf);
      wTextOut(d_dib->getDC(), x1, y, strColon);

      if(isLimited)
         wTextOut(d_dib->getDC(), x2, y, "???");
      else
      {
         LONG mp = static_cast<LONG>(ua.getNumInfantry() * UnitTypeConst::InfantryPerSP);
         if(d_limitedInfo)
            mp += (maximum(0, MulDiv(mp, d_cpi->fowPercent(), 100)));

         wTextPrintf(d_dib->getDC(), x2, y, "%ld", mp);
      }

      // cavalry
      y = yGen.nextY();    // 7

      wTextOut(d_dib->getDC(), x, y, strCav);
      wTextOut(d_dib->getDC(), x1, y, strColon);
      if(isLimited)
         wTextOut(d_dib->getDC(), x2, y, "???");
      else
      {
         LONG mp = static_cast<LONG>(ua.getNumCavalry() * UnitTypeConst::CavalryPerSP);
         if(d_limitedInfo)
            mp += (maximum(0, MulDiv(mp, d_cpi->fowPercent(), 100)));

         wTextPrintf(d_dib->getDC(), x2, y, "%ld", mp);
      }

      // Artillery

      y = yGen.nextY();    // 8

      wTextOut(d_dib->getDC(), x, y, strArt);
      wTextOut(d_dib->getDC(), x1, y, strColon);
      if(isLimited)
         wTextOut(d_dib->getDC(), x2, y, "???");
      else
      {
         LONG mp = static_cast<LONG>(ua.getNumArtillery() * UnitTypeConst::GunsPerSP);

         if(d_limitedInfo)
            mp += (maximum(0, MulDiv(mp, d_cpi->fowPercent(), 100)));

         wTextPrintf(d_dib->getDC(), x2, y, "%ld %s", mp,
            s_strings.get(UnitInfoString::Guns));
      }

      // Other
      y = yGen.nextY();    // 9

      wTextOut(d_dib->getDC(), x, y, strOther);
      wTextOut(d_dib->getDC(), x1, y, strColon);
      if(isLimited)
         wTextOut(d_dib->getDC(), x2, y, "???");
      else
      {
         LONG mp = static_cast<LONG>(ua.getNumOther() * UnitTypeConst::SpecialPerSP);
         if(d_limitedInfo)
            mp += (maximum(0, MulDiv(mp, d_cpi->fowPercent(), 100)));

         wTextPrintf(d_dib->getDC(), x2, y, "%ld", static_cast<LONG>(ua.getNumOther() * 250));
      }
   }

  // d_dib->setFont(oldFont);
#endif
}

/*
 * Draw Leader Tab
 */

void UnitInfoData::drawLeaderDib()
{
#if !defined(EDITOR)
   ASSERT(d_campData != 0);
   ASSERT(d_dib != 0);
   ASSERT(d_cpi != NoCommandPosition);

   /*--------------------------------------------
    * Put name header
    */

   drawNameDib(d_nameRect, true);

   /*
    * some useful values
    */

   ILeader leader = d_cpi->getLeader();

   /*
    * Load in the portrait
    */

   PicLibrary::Picture portrait = GLeaderInfo::getPortrait(generic(leader));

   /*
    * calculate sizes
    */

   const int ySpacing = ScreenBase::y(7);    // was 8
   const int h1 = ySpacing + (s_borderThickness + 1) * 2;
   const int margin = s_borderThickness + 2;

   const int y1 = d_mainRect.top();
   const int y2 = y1 + h1;
   const int y3 = y2 + 2;
   const int y6 = d_mainRect.bottom();
   const int y5 = y6 - h1;
   const int y4 = y5 - 2;

   const int portH = y4 - y1 - s_borderThickness*2;      // height of portrait
   const int portW = MulDiv(portrait->getWidth(), portH, portrait->getHeight());

   const int x1 = d_mainRect.left();
   const int x4 = d_mainRect.right();
   const int x3 = x4 - portW - s_borderThickness * 2;
   const int x2 = x3 - 2;

   PixelRect rankRect(x1, y1, x2, y2);
   PixelRect attrRect(x1, y3, x2, y4);
   PixelRect portraitRect(x3, y1, x4, y4);
   PixelRect controlRect(x1, y5, x4, y6);

   /*
    * Draw Rectangles
    */

   // ColourIndex borderColor = d_dib->getColour(Colours::Black);
   // drawThickBorder(d_dib, controlRect, s_borderThickness, borderColor);
   // drawThickBorder(d_dib, rankRect, s_borderThickness, borderColor);
   // drawThickBorder(d_dib, attrRect, s_borderThickness, borderColor);
   // drawThickBorder(d_dib, portraitRect, s_borderThickness, borderColor);
   drawThickBorder(controlRect);
   drawThickBorder(rankRect);
   drawThickBorder(attrRect);
   drawThickBorder(portraitRect);

   // Adjust rectangles to remove borders

   shrinkRect(portraitRect, s_borderThickness);
   shrinkRect(controlRect, margin);
   shrinkRect(rankRect, margin);
   shrinkRect(attrRect, margin);


   /*
    * Create Font
    */

   LogFont lf;
   lf.height(ySpacing);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);

   Font nFont;
   nFont.set(lf);

   // HFONT oldFont = d_dib->setFont(nFont);
   DCFontHolder fontHold(d_dib->getDC(), nFont);

   /*
    * Draw Rank
    * No.. draw the formations can control instead
    */

   {
      const int divisor = 100;
      const int roundOutValue = (divisor / 2) - 1;

      int nCanControl = (leader->getInitiative() + leader->getStaff() + roundOutValue) / divisor;

      const char* format = s_strings.get(UnitInfoString::CanControl);
//    wTextPrintf(d_dib->getDC(), controlRect.left(), controlRect.top(), format, nCanControl);
      wTextPrintf(d_dib->getDC(), controlRect.left(), controlRect.top(), controlRect.width(), format, nCanControl);
   }

   /*
    * Draw Attributes
    */

   {
      int y = attrRect.top();
      int h = attrRect.height();

      /*
       * Create a new font if not enough space
       */

      const int nSections = 6;

      int spacing = minimum(ySpacing, h / nSections);
      lf.height(spacing);

      Font aFont;
      aFont.set(lf);
      // d_dib->setFont(aFont);
      fontHold.set(aFont);

      EvenSpacing yGen(nSections, y, h, spacing);

      /*
       * get strings
       */

      const char* strInitiative = s_strings.get(UnitInfoString::Initiative);
      const char* strStaff = s_strings.get(UnitInfoString::Staff);
      const char* strSubordination = s_strings.get(UnitInfoString::Subordination);
      const char* strAggression = s_strings.get(UnitInfoString::Aggression);
      const char* strCharisma = s_strings.get(UnitInfoString::Charisma);
      const char* strAbility = s_strings.get(UnitInfoString::Ability);

      int width = textWidth(d_dib->getDC(), strInitiative);
      width = maximum(width, textWidth(d_dib->getDC(), strStaff));
      width = maximum(width, textWidth(d_dib->getDC(), strSubordination));
      width = maximum(width, textWidth(d_dib->getDC(), strAggression));
      width = maximum(width, textWidth(d_dib->getDC(), strCharisma));
      width = maximum(width, textWidth(d_dib->getDC(), strAbility));

      int x = attrRect.left();
      int barXOffset = x + width + 2;
      int barWidth = attrRect.right() - barXOffset;
      int barHeight = ySpacing - 2;

      bool isLimited = d_limitedInfo && (d_cpi->getInfoQuality() < CommandPosition::FullInfo);

      /*
       * calculate bargraph offsets
       */

      y = yGen.nextY();
      drawAttributeGraph(d_dib, strInitiative,
        x, y, barXOffset, barWidth, barHeight, leader->getInitiative(True), isLimited);

      y = yGen.nextY();
      drawAttributeGraph(d_dib, strStaff,
         x, y, barXOffset, barWidth, barHeight, leader->getStaff(True), isLimited);

      y = yGen.nextY();
      drawAttributeGraph(d_dib, strSubordination,
         x, y, barXOffset, barWidth, barHeight, leader->getSubordination(), isLimited);

      y = yGen.nextY();
      drawAttributeGraph(d_dib, strAggression,
         x, y, barXOffset, barWidth, barHeight, leader->getAggression(True), isLimited);

      y = yGen.nextY();
      drawAttributeGraph(d_dib, strCharisma,
         x, y, barXOffset, barWidth, barHeight, leader->getCharisma(True), isLimited);

      y = yGen.nextY();
      drawAttributeGraph(d_dib, strAbility,
         x, y, barXOffset, barWidth, barHeight, leader->getTactical(True), isLimited);
   }

   /*
    * Draw Command Ability
    */

   {
      // d_dib->setFont(nFont);
      fontHold.set(nFont);

      int x = rankRect.left();
      int y = rankRect.top() + (rankRect.height() - ySpacing) / 2;

      const char* text;


      if(d_limitedInfo && (d_cpi->getInfoQuality() < CommandPosition::VeryLimitedInfo))
         text = s_strings.get(UnitInfoString::Unknown);
      else
         text = leader->getRankLevel().getRankName(False);

      wTextPrintf(d_dib->getDC(), x, y, "%s: %s",
         s_strings.get(UnitInfoString::CommandAbility), text);
   }

   /*
    * Draw Portrait
    */

   {
      DIB_Utility::stretchDIBNoMask(d_dib,
         portraitRect.left(), portraitRect.top(),
         portraitRect.width(), portraitRect.height(),
         portrait,
         0, 0,
         portrait->getWidth(), portrait->getHeight());
   }


  // d_dib->setFont(oldFont);
#endif
}



/*
 * Draw Activity Tab
 */


void UnitInfoData::drawActivityDib()
{
   ASSERT(d_campData != 0);
   ASSERT(d_dib != 0);

   drawNameDib(d_nameRect, false);

   /*
    * Make rectangles
    */

   const int nSections = 4;      // 5 blocks of info
   const int nMargins = nSections - 1;    // Number of margins
   const int margin = 2;
   // const int spacing = 2 * (s_borderThickness + margin);
   const int spacing = 2 * margin;
   const int orderRows = 3;
   const int advOrderRows = 2;
   const int nTextRows = orderRows + 1 + 1 + advOrderRows;

   int fixedHeight = nSections * spacing + nMargins * margin;
   int textSize = d_mainRect.height() - fixedHeight;

   ASSERT(textSize > 0);

   int fontHeight = textSize / nTextRows;
   fontHeight = minimum(fontHeight, ScreenBase::y(8));

   ASSERT(fontHeight > 0);

   int hOrder = orderRows * fontHeight + spacing;
   int hAdvOrder = advOrderRows * fontHeight + spacing;
   int h1 = fontHeight + spacing;

   int y8 = d_mainRect.bottom() - margin;
   int y7 = y8 - hAdvOrder;

   int y6 = y7 - margin;
   int y5 = y6 - h1;

   int y4 = y5 - margin;
   int y3 = y4 - h1;

   int y2 = y3 - margin;
   int y1 = d_mainRect.top() + margin;

   int x1 = d_mainRect.left() + margin;
   int x2 = d_mainRect.right() - margin;

   PixelRect orderRect     (x1, y1, x2, y2);
   PixelRect postureRect   (x1, y3, x2, y4);
   PixelRect agressRect    (x1, y5, x2, y6);
   PixelRect contingentRect(x1, y7, x2, y8);

   ColourIndex borderColor = d_dib->getColour(Colours::Black);

   // drawThickBorder(d_dib, d_mainRect, s_borderThickness, borderColor);
   drawThickBorder(d_mainRect);

   // drawThickBorder(d_dib, orderRect, s_borderThickness, borderColor);
   // drawThickBorder(d_dib, postureRect, s_borderThickness, borderColor);
   // drawThickBorder(d_dib, agressRect, s_borderThickness, borderColor);
   // drawThickBorder(d_dib, contingentRect, s_borderThickness, borderColor);

   // shrinkRect(orderRect, margin + s_borderThickness);
   // shrinkRect(postureRect, margin + s_borderThickness);
   // shrinkRect(agressRect, margin + s_borderThickness);
   // shrinkRect(contingentRect, margin + s_borderThickness);

   /*
    * Make Fonts
    */

   LogFont lf;
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);
   lf.underline(false);

   Font nFont;
   nFont.set(lf);

   // lf.underline(true);
   // Font ulFont;
   // ulFont.set(lf);

   // HFONT oldFont = d_dib->setFont(nFont);
   DCFontHolder fontHold(d_dib->getDC(), nFont);

   /*
    * Get Order
    */

   const CampaignOrder* order = d_cpi->getCurrentOrder();

   /*
    * put 'Order' text
    */

   {
      SimpleString text = s_strings.get(UnitInfoString::OrderParagraph);
      SimpleString value = CampaignOrderUtil::getOrderText(d_campData, order, OD_LONG);
      drawTextRect(d_dib, orderRect, text, value, borderColor, fontHeight);
   }

   /*
    * Draw 'Posture Text'
    */

   {
      SimpleString text = s_strings.get(UnitInfoString::PostureParagraph);
      SimpleString value = Orders::Posture::postureName(order->getPosture());
      drawTextRect(d_dib, postureRect, text, value, borderColor, fontHeight);
   }

  /*
   * Draw 'Aggression' text
   */

   {
      SimpleString text = s_strings.get(UnitInfoString::EnemyParagraph);
      SimpleString value = Orders::Aggression::getName(order->getAggressLevel());
      drawTextRect(d_dib, agressRect, text, value, borderColor, fontHeight);
   }

   /*
    * Draw 'Contingencies' text
    */

   {
      SimpleString text = s_strings.get(UnitInfoString::AdvancedOrderParagraph);
      SimpleString value = CampaignOrderUtil::getAdvancedOrderText(order);
      drawTextRect(d_dib, contingentRect, text, value, borderColor, fontHeight);
   }

  // d_dib->setFont(oldFont);
}


/*
 * Draw Orders Tab
 */


void UnitInfoData::drawOrdersDib()
{
   drawNameDib(d_nameRect, false);

   /*
    * Draw border around main area
    */

   // ColourIndex borderColor = d_dib->getColour(Colours::Black);
   // drawThickBorder(d_dib, d_mainRect, s_borderThickness, borderColor);
   drawThickBorder(d_mainRect);

   /*
    * Get orders
    * Copy it, to prevent problems of data changing while processing it
    */

   const CampaignOrderList orderList = d_cpi->getOrders();

   /*
    * Calculate rectangles?
    */

   const int margin = 2;

   /*
    * set up font
    */

   int fontHeight = ScreenBase::y(8);

   LogFont lf;
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);
   lf.underline(false);

   Font nFont;
   nFont.set(lf);

   // HFONT oldFont = d_dib->setFont(nFont);
   DCFontHolder fontHold(d_dib->getDC(), nFont);

   /*
    * Get strings
    */

   ResString strCurrent(IDS_UI_CURRENT);
   ResString strActioning(IDS_UI_ACTIONING);

   /*
    * Display Current Order
    */

   int x = d_mainRect.left() + s_borderThickness + margin;
   int y = d_mainRect.top() + s_borderThickness + margin;

   const CampaignOrder* order = orderList.getCurrentOrder();
   SimpleString text = CampaignOrderUtil::getDescription(d_campData, order, OD_BRIEF);
   wTextPrintf(d_dib->getDC(), x,y, "%s: %s", strCurrent.c_str(), text.toStr());

   /*
    * Display Actioning Order
    */

   y += fontHeight;
   if(orderList.isActing())
   {
      order = orderList.getActioningOrder();
      text = CampaignOrderUtil::getDescription(d_campData, order, OD_BRIEF);

      wTextPrintf(d_dib->getDC(), x,y, "%s: %s", strActioning.c_str(), text.toStr());
   }

   /*
    * Display Orders in Queue
    */

   CampaignOrderList::ConstIter iter(orderList);
   while(++iter)
   {
      y += fontHeight;
      if(y >= (d_mainRect.bottom() - fontHeight) )
         break;

      const CampaignOrderNode* node = iter.current();

      TimeTick tick = node->arrival();
      order = node->order();

      GameTick gtick = ticksToGameTicks(tick);
      Date date;
      gtick.getDate(&date);
      // char timeText[] = "Jan 99th";
      // sprintf(timeText, "%s %d%s",
      //    (const char*) getMonthName(date.month, true),
      //    (int) date.day,
      //    (const char*) getNths(date.day));

      // ostrstream text;
      std::ostringstream text;
      text << getMonthName(date.month, true) << " " << static_cast<int>(date.day) << getNths(date.day)
            << ": " << CampaignOrderUtil::getDescription(d_campData, order, OD_BRIEF).toStr()
            << '\0';

      wTextOut(d_dib->getDC(), x, y, text.str());
   }




  // d_dib->setFont(oldFont);

}


/*
 * Draw OB Tab
 */

void UnitInfoData::drawOBDib()
{
#if !defined(EDITOR)
   ASSERT(d_campData != 0);
   ASSERT(d_dib != 0);
   ASSERT(d_cpi != NoCommandPosition);

   const char* strInf = s_strings.get(UnitInfoString::Inf);
   const char* strCav = s_strings.get(UnitInfoString::Cav);
   const char* strGuns = s_strings.get(UnitInfoString::Guns);
   const char* strStr = s_strings.get(UnitInfoString::Str);
   const char* strMorale = s_strings.get(UnitInfoString::Morale);


   const int x = d_mainRect.left() + s_borderThickness + 2;
   int y = d_mainRect.top() + s_borderThickness + 1;

   const int barCX = ScreenBase::x(17);
   const int barCY = ScreenBase::y(3);
   const int spacing = ScreenBase::y(7);
   const int itemSpacing = ScreenBase::y(16);

   /*
    * Get fonts. One underlined, one not
    */

   LogFont lf;
   lf.height(spacing);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);

   Font nFont;
   nFont.set(lf);

   // get underlined

   Font lFont;

   lf.underline(true);
   lf.height(spacing + 1);
   lFont.set(lf);

   // HFONT oldFont = d_dib->setFont(nFont);
   DCFontHolder fontHold(d_dib->getDC(), nFont);

   /*
    * get X column positions
    */

   const int barOffsetX = d_mainRect.right() - barCX - s_borderThickness - 1;
   const int moraleX = barOffsetX - textWidth(d_dib->getDC(), strMorale) - 2;
   const int strengthX = d_mainRect.right() - s_borderThickness - textWidth(d_dib->getDC(), strStr) - textWidth(d_dib->getDC(), "888888") - 2;

   const int infSpacing = (moraleX - x) / 3;

   const int infX = x;
   const int cavX = x + infSpacing;
   const int gunsX = cavX + infSpacing;


  /*--------------------------------------------
   * Put name header
   */

   drawNameDib(d_nameRect, false);

   /*
    * Draw border around main area
    */

   // ColourIndex borderColor = d_dib->getColour(Colours::Black);
   // drawThickBorder(d_dib, d_mainRect, s_borderThickness, borderColor);
   drawThickBorder(d_mainRect);

   /*
    * Go through and list each immediate subordinate
    * if we run out of room some of them will be left off
    */


  ConstICommandPosition cpiChild = d_cpi->getChild();
  while(cpiChild != NoCommandPosition)
  {
#if 0
      const ImageLibrary* flagIcons = scenario->getNationFlag(cpiChild->getNation(),
         (d_limitedInfo && cpi->getInfoQuality() == CommandPosition::NoInfo) ? False : True);
      // x = d_dRect.left() + ((2 * dbX) / baseX);

      /*
       * Put flags
       */

      flagIcons->blit(dib, FI_Division, x, y, True);
#endif

      LONG nInf  = CampaignArmy_Util::manpowerByType(d_campData, cpiChild, BasicUnitType::Infantry);
      LONG nArt  = CampaignArmy_Util::manpowerByType(d_campData, cpiChild, BasicUnitType::Artillery);
      LONG nGuns = CampaignArmy_Util::totalGuns(d_campData, cpiChild);
      LONG nCav  = CampaignArmy_Util::manpowerByType(d_campData, cpiChild, BasicUnitType::Cavalry);
      LONG totalStrength = nInf + nArt + nCav;

      /*
       * Put name
       */

      // d_dib->setFont(lFont);
      fontHold.set(lFont);
      wTextPrintf(d_dib->getDC(), x, y, "%s (%s)",
         cpiChild->getNameNotNull(), cpiChild->getLeader()->getNameNotNull());

      /*
       * Put total strength
       */

      // d_dib->setFont(nFont);
      fontHold.set(nFont);
      wTextPrintf(d_dib->getDC(), strengthX, y, "%s %ld", strStr, totalStrength);

      /*
       * Put type strengths
       */

      // inf
      int workY = y + (spacing);
      wTextPrintf(d_dib->getDC(), infX, workY, "%s %ld", strInf, nInf);

      // Cav

      wTextPrintf(d_dib->getDC(), cavX, workY, "%s %ld", strCav, nCav);

      // guns

      wTextPrintf(d_dib->getDC(), gunsX, workY, "%s %d", strGuns, nGuns);

      drawAttributeGraph(d_dib, strMorale,
         moraleX, workY, barOffsetX, barCX, barCY,
         cpiChild->getMorale(),
         (d_limitedInfo && (cpiChild->getInfoQuality() < CommandPosition::FullInfo)));

      cpiChild = cpiChild->getSister();

      y += itemSpacing;

      if((y + itemSpacing) >= d_mainRect.bottom())
         break;
   }

   // d_dib->setFont(oldFont);

#endif
}



/*
 * Draw the Tracking informationwindow
 */


void UnitInfoData::drawTrackingDib()
{
#if !defined(EDITOR)

   ASSERT(d_campData);
   ASSERT(d_dib);
   ASSERT(d_cpi != NoCommandPosition);

   TextColorHolder textCol(d_dib->getDC(), Colours::Black);
   BkModeHolder bkHold(d_dib->getDC(), TRANSPARENT);

   // d_dib->setBkMode(TRANSPARENT);
   // d_dib->setTextColor(Colours::Black);   // scenario->getSideColour(d_cpi->getSide()));

   /*
    * Draw Name Header
    */

   drawNameDib(d_nameRect, true, Compact);

   /*
    * get useful values
    */

   ILeader leader = d_cpi->getLeader();
   bool limited = d_limitedInfo && (d_cpi->getInfoQuality() < CommandPosition::FullInfo);

   /*
    * Load in the portrait
    */

   PicLibrary::Picture portrait = GLeaderInfo::getPortrait(generic(leader));

   /*
    * Set up rectangles
    */

   const int nSections = 5;   // 5 things to display
   int margin = 1;
   int edging = margin + s_borderThickness;

   int y1 = d_mainRect.top();    // + edging;
   int y2 = d_mainRect.bottom(); // - edging;
   int h1 = y2 - y1;

   int fontHeight = h1 / nSections;
   fontHeight = minimum(fontHeight, ScreenBase::y(8));

   int portH = h1;
   int portW = MulDiv(portrait->getWidth(), portH, portrait->getHeight());

   int x1 = d_mainRect.left();   // + edging;
   int x4 = d_mainRect.right();  // - edging;
   int x3 = x4 - portW - s_borderThickness * 2;
   int x2 = x3 - margin;

   PixelRect rectPortrait(x3, y1, x4, y2);
   PixelRect rectAttr(x1, y1, x2, y2);

   /*
    * Make Font
    */

   LogFont lf;
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);
   lf.underline(false);
   Font font;
   font.set(lf);

   // HFONT oldFont = d_dib->setFont(font);
   DCFontHolder fontHold(d_dib->getDC(), font);

   /*
    * Draw Portrait
    */

   // ColourIndex borderColor = d_dib->getColour(Colours::Black);
   // drawThickBorder(d_dib, rectPortrait, s_borderThickness, borderColor);
   drawThickBorder(rectPortrait);

   shrinkRect(rectPortrait, s_borderThickness);
   DIB_Utility::stretchDIBNoMask(d_dib,
         rectPortrait.left(), rectPortrait.top(),
         rectPortrait.width(), rectPortrait.height(),
         portrait,
         0, 0,
         portrait->getWidth(), portrait->getHeight());

   /*
    * Draw Attributes
    */

   EvenSpacing yGen(nSections, rectAttr.top(), rectAttr.height(), fontHeight);

   const char* strMorale = s_strings.get(UnitInfoString::Morale);
   const char* strFatigue = s_strings.get(UnitInfoString::Fatigue);
   const char* strSupply = s_strings.get(UnitInfoString::Supply);
   const char* strStrength = s_strings.get(UnitInfoString::Strength);
   const char* strUnknown = s_strings.get(UnitInfoString::Unknown);
   const char* strAction = s_strings.get(UnitInfoString::Action);

   int tw = textWidth(d_dib->getDC(), strMorale);
   tw = maximum(tw, textWidth(d_dib->getDC(), strFatigue));
   tw = maximum(tw, textWidth(d_dib->getDC(), strSupply));

   int x = rectAttr.left();
   int barXOffset = x + tw + margin;
   int barWidth = rectAttr.right() - barXOffset - margin;
   int barHeight = fontHeight - 2;

   /*
    * Strength
    */

   int y = yGen.nextY();

   int textCX = rectPortrait.left() - (rectAttr.left() + 2);
   // if(d_limitedInfo && d_cpi->getInfoQuality() == CommandPosition::NoInfo)
   if(d_limitedInfo && d_cpi->getInfoQuality() <= CommandPosition::LimitedInfo)
   {
      wTextPrintf(d_dib->getDC(), x, y, textCX, "%s: %s", strStrength, strUnknown);
   }
   else
   {
      LONG strength = CampaignArmy_Util::totalManpower(d_campData, d_cpi);

      if(d_limitedInfo)
      {
         strength += maximum(0, (MulDiv(strength, d_cpi->fowPercent(), 100)));
      }

      wTextPrintf(d_dib->getDC(), x, y, textCX, "%s: %ld%s",
         strStrength,
         (long) strength,
         d_limitedInfo ? "?" : "");
   }

   /*
    * Action
    */

   y = yGen.nextY();
   const char* actionText = (d_limitedInfo && d_cpi->getInfoQuality() <= CommandPosition::VeryLimitedInfo) ?
      "???" : d_cpi->getActionDescription(OD_BRIEF, campaignData->getTick());
   wTextPrintf(d_dib->getDC(), x, y, textCX, "%s: %s", strAction, actionText);
   // d_cpi->clearActionDescription();

   /*
    * Morale
    */

   y = yGen.nextY();
   drawAttributeGraph(d_dib, strMorale, x, y, barXOffset, barWidth, barHeight, d_cpi->getMorale(), limited);

   /*
    * fatigue
    */

   y = yGen.nextY();
   drawAttributeGraph(d_dib, strFatigue, x, y, barXOffset, barWidth, barHeight, d_cpi->getFatigue(), limited);

   /*
    * Supply
    */

   y = yGen.nextY();
   drawAttributeGraph(d_dib, strSupply, x, y, barXOffset, barWidth, barHeight, d_cpi->getSupply(), limited);

#if 0
  const int mapX = 0;
  const int mapY = 0;

  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();

  const int width = (dbX * 80) / baseX;



  HFONT hFont = font;

  d_dib->setBkMode(TRANSPARENT);
  d_dib->setTextColor(scenario->getSideColour(d_cpi->getSide()));
  HFONT oldFont = d_dib->setFont(font);

  LONG x = mapX + (dbX / baseX);
  LONG y = mapY + (dbY / baseY);

  LONG spacing = (5 * dbY) / baseY;

  /*
   * Put flag and side text
   */

  Nationality n = d_cpi->getNation();

  const ImageLibrary* flags = scenario->getNationFlag(n,
        !(d_limitedInfo && d_cpi->getInfoQuality() == CommandPosition::NoInfo));

  flags->blit(d_dib, FI_Army, x, y, True);

  wTextPrintf(d_dib->getDC(), (x + FI_Army_CX) + ((3 * dbX) / baseX), y, "%s %s",
       scenario->getSideName(d_cpi->getSide()),
       s_strings.get(UnitInfoString::Unit));

  y += (2 * spacing);

  lf.height((7 * dbY) / baseY);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);
  lf.underline(false);

  font.set(lf);

  d_dib->setFont(font);
  d_dib->setTextColor(PALETTERGB(0, 0, 0));

  /*
   * Put unit name
   */

  if(d_limitedInfo && d_cpi->getInfoQuality() == CommandPosition::NoInfo)
  {
    wTextOut(d_dib->getDC(), x, y, (width - x) - 2,
       s_strings.get(UnitInfoString::UnidentifiedUnit));
  }
  else
  {
    wTextPrintf(d_dib->getDC(), x, y, (width - x) - 2, "%s %s",
      scenario->getNationAdjective(d_cpi->getNation()),
      d_cpi->getNameNotNull());
  }

  /*
   * Put leaders name
   */

  y += spacing;

  const Leader* leader = d_campData->getLeader(d_cpi->getLeader());

  const char* text = (d_limitedInfo && d_cpi->getInfoQuality() == CommandPosition::NoInfo) ?
       s_strings.get(UnitInfoString::LeaderUnknown) : leader->getNameNotNull();

  wTextOut(d_dib->getDC(), x, y, (width - x) - 2, text);


  /*
   * Put Strength
   */

  lf.height((7 * dbY) / baseY);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(false);
  lf.underline(false);

  font.set(lf);
  d_dib->setFont(font);

  char buf[200];

  y += (2*spacing);

  if(d_limitedInfo && d_cpi->getInfoQuality() == CommandPosition::NoInfo)
  {
    lstrcpy(buf, s_strings.get(UnitInfoString::StrengthUnknown));
  }

  else
  {
    LONG strength = CampaignArmy_Util::totalManpower(d_campData, d_cpi);
    const char* str = s_strings.get(UnitInfoString::Str);

    Boolean draw = True;
    if(d_limitedInfo)
    {
      strength += maximum(0, (MulDiv(strength, d_cpi->fowPercent(), 100)));

      if(d_cpi->getInfoQuality() <= CommandPosition::LimitedInfo)
      {
        wsprintf(buf, "%s %ld ???", str, strength);
        draw = False;
      }
    }

    if(draw)
      wsprintf(buf, "%s %ld", str, strength);
  }

  wTextOut(d_dib->getDC(), x, y, buf);

  /*
   * Put action
   */

  y += spacing;

  const char* actionText = (d_limitedInfo && d_cpi->getInfoQuality() <= CommandPosition::VeryLimitedInfo) ?
    "???" : d_cpi->getActionDescription(OD_BRIEF, campaignData->getTick());

  wTextPrintf(d_dib->getDC(), x, y, "%s: %s", s_strings.get(UnitInfoString::Action), actionText);
  // d_cpi->clearActionDescription();

  const int barOffsetX = x + ((25 * ScreenBase::dbX()) / ScreenBase::baseX());
  const int barCX = ((25 * ScreenBase::dbX()) / ScreenBase::baseX());
  const int barCY = ((3 * ScreenBase::dbY()) / ScreenBase::baseY());

  /*----------------
   * Put morale. Includes text and bargraph
   */

  // put text
  y += (2 * spacing);
  drawAttributeGraph(d_dib, s_strings.get(UnitInfoString::Morale),
     x, y, barOffsetX, barCX, barCY, d_cpi->getMorale(), (d_limitedInfo && d_cpi->getInfoQuality() < CommandPosition::FullInfo));

  /*------------------------
   * Put Fatigue
   */

  // put text
  y += spacing;
  drawAttributeGraph(d_dib, s_strings.get(UnitInfoString::Fatigue),
     x, y, barOffsetX, barCX, barCY, d_cpi->getFatigue(), (d_limitedInfo && d_cpi->getInfoQuality() < CommandPosition::FullInfo));

  /*------------------------------------------
   * Put Supply
   */

  // put text
  y += spacing;
  drawAttributeGraph(d_dib, s_strings.get(UnitInfoString::Supply),
     x, y, barOffsetX, barCX, barCY, d_cpi->getSupply(), (d_limitedInfo && d_cpi->getInfoQuality() < CommandPosition::FullInfo));



#endif

  // d_dib->setFont(oldFont);

#endif // EDITOR!
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.4  2002/11/16 18:03:32  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
