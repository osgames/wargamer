/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef UNITMENU_HPP
#define UNITMENU_HPP

#include "grtypes.hpp"
#include "cpidef.hpp"

class UnitMenu_Imp;
class CampaignData;
class StackedUnitList;
class CampaignWindowsInterface;
class CampaignUserInterface;
class CampaignOrder;
//class ICommandPosition;

class UnitMenu_Int
{
	 UnitMenu_Imp* d_unitMenu;

  public:
	 UnitMenu_Int(CampaignUserInterface* owner, HWND hParent, CampaignWindowsInterface* campWind,
			const CampaignData* campData, CampaignOrder& order);
	 ~UnitMenu_Int();

	 void runMenu(ConstParamCP cpi, ITown itown, const PixelPoint& p);
	 // void destroy();
};

#endif
