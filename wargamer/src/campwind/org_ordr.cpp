/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Organisation Dialog: Sending of orders
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "org_ordr.hpp"
#include "org_list.hpp"
#include "ds_reorg.hpp"

#ifdef DEBUG
#define DEBUG_ORGORDER

#ifdef DEBUG_ORGORDER
#include "compos.hpp"
#include "clog.hpp"
#include "msgenum.hpp"
LogFileFlush orgLog("orgDial.log");
// extern LogFileFlush orgLog;
#endif
#endif

// using namespace OrgOrders;

namespace OrgOrderImplementation {

using namespace DS_Reorg;

static HOrganize getHandle(ConstParamCP cpi, HOrganize& currentHandle, HOrganize parentHandle, bool& pushed)
{
   if(currentHandle == hNULL)
   {
      if(parentHandle == hNULL)
         currentHandle = start(cpi);
      else
      {
         currentHandle = parentHandle;
         if(!pushed)
         {
            push(currentHandle);
            pushed = true;
         }
      }
   }

   return currentHandle;
}

#if 0
static void closeHandle(HOrganize currentHandle, HOrganize parentHandle, bool& pushed)
{
   if(currentHandle != hNULL)
   {
      if(parentHandle == hNULL)
         end(currentHandle);
      else if(pushed)
      {
         pop(currentHandle);
         pushed = false;
      }
   }
}
#endif

/*
 * Send orders to unit and its subordinates
 */

static void sendOrders(const OrgList& orgList, OrgCPI iOrg, DS_Reorg::HOrganize parentHandle,
   ConstParamCP parentCP, ConstParamCP topCP, bool& pushed)
{
   ASSERT(topCP != NoCommandPosition);

   ConstICommandPosition seniorCP = topCP;

   HOrganize dsHandle = hNULL;

   const OrgObject* ob = &orgList[iOrg];

   if(iOrg != OrgCPI::None)
   {
      if(ob->isFictional())
      {
         /*
          * Fictional objects send a createOrg order
          */

         ASSERT( (ob->what() == OrgObject::OOW_CommandPosition) ||
                 (ob->what() == OrgObject::OOW_Leader) );

         // Despatch a CreateOrg order

#ifdef DEBUG_ORGORDER
         orgLog.printf("Create CP under %s", static_cast<const char*>(orgList.getName(ob->parent())));
#endif

         getHandle(parentCP, dsHandle, parentHandle, pushed);

         if(ob->what() == OrgObject::OOW_CommandPosition)
           DS_Reorg::createCP(dsHandle, seniorCP, ob->nation(), ob->rank(), (ob->parent() == OrgCPI::None));
         else if(ob->parent() != OrgCPI::None)
           DS_Reorg::createLeader(dsHandle, seniorCP, ob->nation(), ob->rank());

      }
      else
      {
         /*
          * Is its parent different?
          */

         if(parentCP != ob->originalParent())
         {
            getHandle(parentCP, dsHandle, parentHandle, pushed);

            switch(ob->what())
            {
               case OrgObject::OOW_CommandPosition:
#ifdef DEBUG_ORGORDER
                  orgLog.printf("Transfer CP(%s)",
                     static_cast<const char*>(orgList.getName(iOrg)));
#endif
                  DS_Reorg::transferCP(dsHandle, ob->cp());
                  break;
               case OrgObject::OOW_StrengthPoint:
#ifdef DEBUG_ORGORDER
                  orgLog.printf("Transfer SP(%s)",
                     static_cast<const char*>(orgList.getName(iOrg)));
#endif
                  DS_Reorg::transferSP(dsHandle, ob->originalParent(), ob->sp());
                  break;
               case OrgObject::OOW_Leader:
#ifdef DEBUG_ORGORDER
                  orgLog.printf("Transfer Leader(%s)",
                     static_cast<const char*>(orgList.getName(iOrg)));
#endif
//                if(ob->parent() != OrgCPI::None)
                    DS_Reorg::transferLeader(dsHandle, ob->leader(), seniorCP);
                  break;
               default:
#ifdef DEBUG_ORGORDER
                  orgLog.printf("Illegal Object (%s)",
                     static_cast<const char*>(orgList.getName(iOrg)));
#endif
                  FORCEASSERT("Invalid OrgObject type");
            }
         }

      }
   }

   /*
    * Iterate through children
    */

   OrgCPI iChild = ob->child();
   if(iChild != OrgCPI::None)
   {

      ConstICommandPosition us = NoCommandPosition;

      if(ob->what() == OrgObject::OOW_CommandPosition)
      {
         us = ob->cp();
         if( (us != NoCommandPosition) && (iOrg != OrgList::Top))
            seniorCP = us;
      }

      bool pushed = false;

      while(iChild != OrgCPI::None)
      {
         sendOrders(orgList, iChild, dsHandle, us, seniorCP, pushed);

         const OrgObject* obChild = &orgList[iChild];
         iChild = obChild->sister();
      }

      if(pushed)
         pop(dsHandle);
   }

   if((parentHandle) == hNULL && (dsHandle != hNULL))
      end(dsHandle);

   // closeHandle(dsHandle, parentHandle);
}



}; // namespace OrgOrderImplementation



void OrgOrders::sendOrders(const OrgList& orgList, ConstParamCP seniorCP)
{
#ifdef DEBUG_ORGORDER
   orgLog.printf(">OrgOrders::sendOrders()");
#endif

   /*
    * Iterate through list from top to bottom
    */

   bool pushed = false;
   OrgOrderImplementation::sendOrders(orgList, OrgList::Top, DS_Reorg::hNULL, NoCommandPosition, seniorCP, pushed);


#ifdef DEBUG_ORGORDER
   orgLog.printf("<OrgOrders::sendOrders()");
#endif
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
