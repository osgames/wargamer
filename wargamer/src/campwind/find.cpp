/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "find.hpp"
#include "wind.hpp"
#include "app.hpp"
#include "cbutton.hpp"
#include "itemwind.hpp"
#include "palette.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "scrnbase.hpp"
#include "wmisc.hpp"
#include "fillwind.hpp"
#include "scenario.hpp"
#include "fonts.hpp"
#include "simpstr.hpp"
#include "palwind.hpp"
#include "control.hpp"
#include "fsalloc.hpp"
#include "sllist.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "leader.hpp"
#include "compos.hpp"
#include "armies.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "cedit.hpp"
#include "cwin_int.hpp"
#include "scn_img.hpp"
#include "resstr.hpp"

/*-----------------------------------------------------
 * Local utility structs and classes
 *
 */

struct FControl {
  UWORD d_x;
  UWORD d_y;
  UWORD d_cx;
  UWORD d_cy;
};

class FUtil {
public:
  static HWND createButton(HWND hParent, const char* text, int id,
       DWORD style, const DrawDIB* fillDib, const FControl& ci);

   static void deleteButton(HWND hParent, int id);
};

HWND FUtil::createButton(HWND hParent, const char* text, int id, DWORD style,
   const DrawDIB* fillDib, const FControl& ci)
{
  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
            style,
            (ScreenBase::dbX() * ci.d_x) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_y) / ScreenBase::baseY(),
            (ScreenBase::dbX() * ci.d_cx) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_cy) / ScreenBase::baseY(),
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  cb.setFillDib(fillDib);
  cb.setBorderColours(scenario->getBorderColors());

  /*
   * Set up font
   */

  const int fontCY = (8 * ScreenBase::dbY()) / ScreenBase::baseY();

  LogFont lf;
  lf.height(fontCY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Normal));

  Font font;
  font.set(lf);
  cb.setFont(font);

  return hButton;
}

void FUtil::deleteButton(HWND hParent, int id)
{
   CustomButton cb(hParent, id);
   ASSERT(cb.getHWND() != 0);
   DestroyWindow(cb.getHWND());
}

/*----------------------------------------------------------------
 * Structure to hold listview items
 */

struct ListViewItem : public SLink{
  enum { ChunkSize = 50 };

  LPARAM d_itemData;
  Boolean d_added;
  FindItem::Type d_type;

  ListViewItem() :
    d_itemData(0),
    d_added(False),
    d_type(FindItem::FindTown) {}

  /*
   * Allocators
   */

  void* operator new(size_t size);
#ifdef _MSC_VER
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif
};

#ifdef DEBUG
static FixedSizeAlloc<ListViewItem> s_alloc("ListViewItem");
#else
static FixedSizeAlloc<ListViewItem> s_alloc;
#endif  // DEBUG

void* ListViewItem::operator new(size_t size)
{
  return s_alloc.alloc(size);
}

#ifdef _MSC_VER
void ListViewItem::operator delete(void* deadObject)
{
  s_alloc.release(deadObject);
}
#else
void ListViewItem::operator delete(void* deadObject, size_t size)
{
  s_alloc.release(deadObject, size);
}
#endif

class ListViewItemList : public SList<ListViewItem> {
};


/*-----------------------------------------------------------------
 * List window
 */

struct FindItemData {
  FindItem::Type d_type;

  enum Column {
    Column1,
    Column2,
    Column3
  } d_column;

  FindItemData() :
    d_type(FindItem::FindTown),
    d_column(Column1) {}
};

class FindItemListBox : public ItemWindow {
  const CampaignData* d_campData;
  const UINT d_listCX;
  const UINT d_listCY;
  ListViewItemList& d_list;
  FindItemData d_fd;

public:
  FindItemListBox(const CampaignData* campData, const ItemWindowData& data, const SIZE& s, ListViewItemList& list) :
    ItemWindow(data, s),
    d_campData(campData),
    d_listCX(s.cx),
    d_listCY(s.cx),
    d_list(list) {}

  ~FindItemListBox() {}

  /*
   * virtual functions from ItemWind
   */

  void init(const void* data);
  void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
  void drawCombo(DrawDIBDC* dib, const void* data) {}

  void findItem(const String& text);

private:
  void sort(ListBox& lb);

  Boolean compareTown(const ListViewItem* item1, const ListViewItem* item2);
  int compareTown(ITown town1, ITown town2);
  int compareProv(ITown town1, ITown town2);
  int compareSize(ITown town1, ITown town2);

  Boolean compareLeader(const ListViewItem* item1, const ListViewItem* item2);
  int compareLeader(const ILeader& leader1, const ILeader& leader2);
  int compareUnit(const ILeader& leader1, const ILeader& leader2);
  int compareRank(const ILeader& leader1, const ILeader& leader2);

  void drawTown(const ListViewItem* lvi, int itemCY);
  void drawLeader(const ListViewItem* lvi, int itemCY);
};

inline LPARAM itemToLPARAM(const ListViewItem* lvi)
{
  return reinterpret_cast<LPARAM>(const_cast<ListViewItem*>(lvi));
}

void FindItemListBox::init(const void* dataPtr)
{
  ASSERT(dataPtr);
  const FindItemData* fd = reinterpret_cast<const FindItemData*>(dataPtr);
  d_fd = *fd;

  ASSERT(d_fd.d_type < FindItem::HowMany);

  // reset list box
  ListBox lb(getHWND(), ItemListBox);
  lb.reset();

  // reset added flags in list
  SListIter<ListViewItem> iter(&d_list);
  while(++iter)
  {
    iter.current()->d_added = False;
  }

  sort(lb);

  if(lb.getNItems() > 0)
  {
    lb.set(0);
    setCurrentValue();
  }

  /*
   * Set position of list box
   */

  int itemsShowing = minimum(data().d_maxItemsShowing, lb.getNItems());
  const LONG itemHeight = data().d_itemCY*itemsShowing;
  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, itemHeight, SWP_NOMOVE);

  if(showing())
    showScroll();
}

void FindItemListBox::sort(ListBox& lb)
{
  /*
   * Sort towns
   *
   * We either sort by town, province, or size
   */

  int nAdded = 0;

  while(nAdded < d_list.entries())
  {
    ListViewItem* li = 0;

    SListIter<ListViewItem> iter(&d_list);
    while(++iter)
    {
      if(!iter.current()->d_added)
      {
        if(!li)
          li = iter.current();
        else
        {
          if(d_fd.d_type == FindItem::FindTown)
          {
            if(compareTown(iter.current(), li))
            {
              li = iter.current();
            }
          }
          else
          {
            if(compareLeader(iter.current(), li))
            {
              li = iter.current();
            }
          }
        }
      }
    }

    ASSERT(li);
    if(li)
    {
      li->d_added = True;

      const char* text = 0;

      if(d_fd.d_type == FindItem::FindTown)
      {
        ITown town = static_cast<ITown>(li->d_itemData);
        text = d_campData->getTownName(town);
      }
      else
      {
        ASSERT(d_fd.d_type == FindItem::FindLeader);
        ILeader leader = reinterpret_cast<Leader*>(li->d_itemData);
        text = leader->getNameNotNull();
      }

      ASSERT(text);

      lb.add(text, itemToLPARAM(li));
      nAdded++;
    }
  }
}

/*
 * Sort by town
 */

Boolean FindItemListBox::compareTown(const ListViewItem* item1, const ListViewItem* item2)
{
  ASSERT(item1);
  ASSERT(item2);

  ITown town1 = static_cast<ITown>(item1->d_itemData);
  ITown town2 = static_cast<ITown>(item2->d_itemData);

  /*
   * Sort by town
   */

  if(d_fd.d_column == FindItemData::Column1)
  {
    int result = compareTown(town1, town2);

    if(result < 0)
      return True;

    else if(result == 0)
    {
      int pResult = compareProv(town1, town2);
      if(pResult <= 0)
      {
        return True;
      }
    }
  }

  /*
   * Sort by prov
   */

  else if(d_fd.d_column == FindItemData::Column2)
  {
    int result = compareProv(town1, town2);

    if(result < 0)
      return True;

    else if(result == 0)
    {
      int pResult = compareTown(town1, town2);
      if(pResult <= 0)
      {
        return True;
      }
    }
  }

  /*
   * Sort by size
   */

  else
  {
    int result = compareSize(town1, town2);

    if(result < 0)
      return True;

    else if(result == 0)
    {
      int tResult = compareTown(town1, town2);

      if(tResult < 0)
        return True;

      else if(tResult == 0)
      {
        int pResult = compareProv(town1, town2);

        if(pResult <= 0)
          return True;
      }
    }
  }

  return False;
}

int FindItemListBox::compareTown(ITown town1, ITown town2)
{
  ASSERT(town1 != NoTown);
  ASSERT(town2 != NoTown);

  const Town& t1 = d_campData->getTown(town1);
  const Town& t2 = d_campData->getTown(town2);

  const char* text1 = t1.getNameNotNull();
  const char* text2 = t2.getNameNotNull();

  // compare strings
  return lstrcmp(text1, text2);
}

int FindItemListBox::compareProv(ITown town1, ITown town2)
{
  ASSERT(town1 != NoTown);
  ASSERT(town2 != NoTown);

  const Town& t1 = d_campData->getTown(town1);
  const Town& t2 = d_campData->getTown(town2);

  const Province& p1 = d_campData->getProvince(t1.getProvince());
  const Province& p2 = d_campData->getProvince(t2.getProvince());

  const char* text1 = p1.getNameNotNull();
  const char* text2 = p2.getNameNotNull();

  // compare strings
  return lstrcmp(text1, text2);
}

int FindItemListBox::compareSize(ITown town1, ITown town2)
{
  ASSERT(town1 != NoTown);
  ASSERT(town2 != NoTown);

  const Town& t1 = d_campData->getTown(town1);
  const Town& t2 = d_campData->getTown(town2);

  return (t1.getSize() < t2.getSize()) ? -1 :
         (t1.getSize() == t2.getSize()) ? 0 : 1;
}

/*
 * Sort by leader
 */

Boolean FindItemListBox::compareLeader(const ListViewItem* item1, const ListViewItem* item2)
{
  ASSERT(item1);
  ASSERT(item2);

  ILeader leader1 = reinterpret_cast<Leader*>(item1->d_itemData);
  ILeader leader2 = reinterpret_cast<Leader*>(item2->d_itemData);

  /*
   * Sort by leader
   */

  if(d_fd.d_column == FindItemData::Column1)
  {
    int result = compareLeader(leader1, leader2);

    if(result < 0)
      return True;

    else if(result == 0)
    {
      int uResult = compareUnit(leader1, leader2);
      if(uResult < 0)
      {
        return True;
      }

      else if(uResult == 0)
      {
        int rResult = compareRank(leader1, leader2);
        if(rResult <= 0)
          return True;
      }
    }
  }

  /*
   * Sort by unit
   */

  else if(d_fd.d_column == FindItemData::Column2)
  {
    int result = compareUnit(leader1, leader2);

    if(result < 0)
      return True;

    else if(result == 0)
    {
      int lResult = compareLeader(leader1, leader2);
      if(lResult < 0)
      {
        return True;
      }

      else if(lResult == 0)
      {
        int rResult = compareRank(leader1, leader2);
        if(rResult <= 0)
          return True;
      }
    }
  }

  /*
   * Sort by rank
   */

  else
  {
    int result = compareRank(leader1, leader2);

    if(result < 0)
      return True;

    else if(result == 0)
    {
      int lResult = compareLeader(leader1, leader2);
      if(lResult < 0)
      {
        return True;
      }

      else if(lResult == 0)
      {
        int uResult = compareUnit(leader1, leader2);
        if(uResult <= 0)
          return True;
      }
    }
  }

  return False;
}

int FindItemListBox::compareLeader(const ILeader& leader1, const ILeader& leader2)
{
  return lstrcmp(leader1->getNameNotNull(), leader2->getNameNotNull());
}

int FindItemListBox::compareUnit(const ILeader& leader1, const ILeader& leader2)
{
  // TODO: get from resource
  static const char* ind = InGameText::get(IDS_UI_ILEADER); // "Unattached";

  const char* text1 = (leader1->getCommand() != NoCommandPosition) ?
      leader1->getCommand()->getNameNotNull() : ind;

  const char* text2 = (leader2->getCommand() != NoCommandPosition) ?
      leader2->getCommand()->getNameNotNull() : ind;

  return lstrcmp(text1, text2);
}

int FindItemListBox::compareRank(const ILeader& leader1, const ILeader& leader2)
{
  RankEnum r1 = leader1->getRankLevel().getRankEnum();
  RankEnum r2 = leader2->getRankLevel().getRankEnum();

  return (r1 < r2) ? -1 : (r1 == r2) ? 0 : 1;
}

void FindItemListBox::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  ListBox lb(lpDrawItem->hwndItem);
  const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
  const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

  /*
   * Allocate dib if needed
   */

  allocateItemDib(itemCX, itemCY);

  ASSERT(itemDib());
  ASSERT(fillDib());

  itemDib()->rect(0, 0, itemCX, itemCY, fillDib());

  if( !(lpDrawItem->itemState & ODS_SELECTED) )
    itemDib()->darkenRectangle(0, 0, itemCX, itemCY, 20);

  if(lb.getNItems() > 0)
  {
    const ListViewItem* lvi = reinterpret_cast<const ListViewItem*>(lb.getValue(lpDrawItem->itemID));
    ASSERT(lvi);

    if(d_fd.d_type == FindItem::FindTown)
      drawTown(lvi, itemCY);
    else
      drawLeader(lvi, itemCY);

    BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
         itemDib()->getDC(), 0, 0, SRCCOPY);
  }
}

void FindItemListBox::drawTown(const ListViewItem* lvi, int itemCY)
{
  ASSERT(lvi);

  const int dbX = ScreenBase::dbX();
  const int baseX = ScreenBase::baseX();

  TEXTMETRIC tm;
  GetTextMetrics(itemDib()->getDC(), &tm);

  const int xOffset = (75 * dbX) / baseX;

#if 0
  // TODO move to resource
  static const char* s_sizeTxt[TownSize_MAX] = {
      "Capital",
      "City",
      "Town",
      "Village"
  };
#endif

  ITown town = static_cast<ITown>(lvi->d_itemData);
  const Town& t = d_campData->getTown(town);
  const Province& p = d_campData->getProvince(t.getProvince());

  // put flag
  int x = (1 * dbX) / baseX;
  int y = (itemCY - FI_Corps_CY) / 2;
  const ImageLibrary* images = scenario->getNationFlag(p.getNationality(), True);
  images->blit(itemDib(), static_cast<UWORD>(FI_Corps), x, y, True);

  // put column text
  y = (itemCY - tm.tmHeight) / 2;
  wTextOut(itemDib()->getDC(), x + (((2 * dbX) / baseX) + FI_Army_CX), y, t.getNameNotNull());

  x += xOffset;
  wTextOut(itemDib()->getDC(), x, y, p.getNameNotNull());

  x += xOffset;
//  wTextOut(itemDib()->getDC(), x, y, s_sizeTxt[t.getSize()]);
  wTextOut(itemDib()->getDC(), x, y, InGameText::get(IDS_TOWNTYPE_CAPITAL + t.getSize()));
}

void FindItemListBox::drawLeader(const ListViewItem* lvi, int itemCY)
{
  ASSERT(lvi);

  const int dbX = ScreenBase::dbX();
  const int baseX = ScreenBase::baseX();

  TEXTMETRIC tm;
  GetTextMetrics(itemDib()->getDC(), &tm);

  const int xOffset = (75 * dbX) / baseX;

  ILeader leader = reinterpret_cast<Leader*>(lvi->d_itemData);

  // put flag
  int x = (1 * dbX) / baseX;
  int y = (itemCY - FI_Pennant_CX) / 2;

  const ImageLibrary* images = scenario->getNationFlag(leader->getNation(), True);
  images->blit(itemDib(), static_cast<UWORD>(FI_Pennant), x, y, True);

  // put column text
  y = (itemCY - tm.tmHeight) / 2;
  int cx = (xOffset - ((2 * dbX) / baseX)) - (x + ((2 * dbX) / baseX) + FI_Pennant_CX);

  wTextOut(itemDib()->getDC(), x + (((2 * dbX) / baseX) + FI_Pennant_CX), y,
     cx, leader->getNameNotNull());

  x += xOffset;

  // TODO: move to resource
  static const char* ind = InGameText::get(IDS_UI_ILEADER); // "Unattached";

  const char* text = (leader->getCommand() != NoCommandPosition) ?
       leader->getCommand()->getNameNotNull() : ind;

  wTextOut(itemDib()->getDC(), x, y, xOffset - ((2 * dbX) / baseX), text);

  x += xOffset;
  wTextOut(itemDib()->getDC(), x, y, leader->getRankLevel().getRankName(True));

}

/*
 * Find town
 */

void FindItemListBox::findItem(const String& text)
{
//  ASSERT(text);

  /*
   * Search through list and find closest match
   */

  HWND h = GetDlgItem(getHWND(), ItemListBox);
  ASSERT(h);

  int index = SendMessage(h, LB_FINDSTRING, 0, reinterpret_cast<LPARAM>(text.c_str()));
  if(index != LB_ERR)
    setCurrentIndex(index);
}

/*-----------------------------------------------------------------
 * A class to handle right-click popup menu
 */

class FindItemMenu : public SubClassWindow {
    CampaignWindowsInterface* d_campWind;
    FindItemListBox* d_listBox;
    FindItem::Type d_type;

  public:
    FindItemMenu(CampaignWindowsInterface* campWind, FindItemListBox* fb) :
      d_campWind(campWind),
      d_listBox(fb),
      d_type(FindItem::FindTown),
      SubClassWindow(GetDlgItem(fb->getHWND(), ItemWindow::ItemListBox))
    {
    }

    ~FindItemMenu() {}

    void setType(FindItem::Type t) { d_type = t; }
    FindItem::Type getType() const { return d_type; }

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
};


LRESULT FindItemMenu::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_RBUTTONDOWN, onRButtonDown);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

void FindItemMenu::onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  /*
   * Find out what item we are over and set that as current item
   */

  POINT p;
  p.x = x;
  p.y = y;

  ClientToScreen(hwnd, &p);
  int itemID = LBItemFromPt(hwnd, p, FALSE);

  if(itemID >= 0 && itemID < d_listBox->entries())
  {
    // Boolean runMenu = False;

    ListBox lb(hwnd);
    const ListViewItem* lvi = reinterpret_cast<const ListViewItem*>(lb.getValue(itemID));
    ASSERT(lvi);

#if !defined(EDITOR)
    if(d_type == FindItem::FindTown)
    {
      ITown itown = static_cast<ITown>(lvi->d_itemData);
      d_campWind->centerMapOnTown(itown);
      // d_campWind->orderTown(itown);

      // runMenu = True;
    }
    else
    {
      ASSERT(d_type == FindItem::FindLeader);

      ILeader leader = reinterpret_cast<Leader*>(lvi->d_itemData);

      if(leader->getCommand() != NoCommandPosition)
      {
        d_campWind->centerMapOnUnit(leader->getCommand());
        // d_campWind->orderUnit(leader->getCommand());
        // runMenu = True;
      }
    }

//     if(runMenu)
//     {
//       // convert POINT to screen coord. and run menu
//       PixelPoint p2 = p;
//       d_campWind->runOrderMenu(p);
//     }
#endif
  }
}

/*------------------------------------------------------------------
 * Actual find item window
 *
 */

class FindWind : public WindowBaseND {
    CampaignWindowsInterface* d_campWind;
    const CampaignData* d_campData;
    CEdit* d_edit;

    ListViewItemList d_list;
    FindItemListBox* d_listBox;

    FindItemMenu* d_menu;

    FindItem::Type d_type;

    enum ID {
       ID_First = 100,

       ID_Edit = ID_First,
       ID_ListBox,
       ID_FirstButton,
       ID_Column1 = ID_FirstButton,
       ID_Column2,
       ID_Column3,
       ID_Find,
       ID_Cancel,
       ID_LastButton = ID_Cancel,

       ID_Last,
    };

    FillWindow d_fillWind;
    // Palette::PaletteID d_palID;

    static const FControl s_cInfo[ID_Last - ID_First];
//  static const char* s_townStrings[ID_Last - ID_First];
//  static const char* s_leaderStrings[ID_Last - ID_First];

    const int d_shadowCX;
    const int d_cx;
    const int d_cy;

  public:

    FindWind(CampaignWindowsInterface* campWind, const CampaignData* d_campData);

    ~FindWind()
    {
      delete d_menu;       // NB: Important for Menu to be deleted before listbox
       delete d_listBox;    //     Because it is subclassed from it.
                            //     Alternative is put it in onDestroy()
      delete d_edit;

       selfDestruct();
    }

    void run(FindItem::Type type);
    // void destroy();
    void hide();
    FindItem::Type getType() const { return d_type; }

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    BOOL onEraseBk(HWND hwnd, HDC hdc);
    void onPaint(HWND hwnd);
//  void onDestroy(HWND hWnd);
    UINT onNCHitTest(HWND hwnd, int x, int y) { return HTCAPTION; }

    const FControl& controlInfo(ID id) const
    {
      ASSERT(id < ID_Last);
      return s_cInfo[id - ID_First];
    }

    const char* idToString(ID id);

    void initTownList();
    void initLeaderList();
    void onFindTown();
    void onFindLeader();

    // void updateGraphics();
    void initWindow(int cx, int cy);
};

/*
 * Coodinates (in dialog units) of controls
 */

const FControl FindWind::s_cInfo[ID_Last - ID_First] = {
  {  57,   8, 140, 10  },     // ID_EDit
  {   4,  33, 188,  0  },     // ID_ListBox
  {   4,  22,  75, 10  },     // ID_Col1
  {  79,  22,  75, 10  },     // ID_Col2
  { 154,  22,  45, 10  },     // ID_Col3
//  { 127, 217,  35, 10  },     // ID_Find
//  { 164, 217,  35, 10  }      // ID_Cancel
  { 101, 217,  48, 10  },     // ID_Find
  { 151, 217,  48, 10  }      // ID_Cancel
};

FindWind::FindWind(CampaignWindowsInterface* campWind, const CampaignData* campData) :
  d_campWind(campWind),
  d_campData(campData),
  d_edit(new CEdit),
  d_listBox(0),
  d_menu(0),
  d_type(FindItem::FindTown),
  d_shadowCX((ScreenBase::dbX() * 3) / ScreenBase::baseX()),
  d_cx(((ScreenBase::dbX() * 202) / ScreenBase::baseX()) + d_shadowCX),
  d_cy(((ScreenBase::dbY() * 230) / ScreenBase::baseY()) + d_shadowCX)
  // d_palID(0)
{
  ASSERT(d_campWind);
  ASSERT(d_campData);
  ASSERT(d_edit);

  HWND hWnd = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_POPUP | WS_CLIPSIBLINGS,
      0, 0, d_cx, d_cy,
      APP::getMainHWND(), NULL    // , APP::instance()
  );
}

LRESULT FindWind::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE,  onCreate);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
    HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

#if 0
// TODO: get this from string resource
const char* FindWind::s_townStrings[ID_Last - ID_First] = {
   NULL,
   NULL,
   "Town",
   "Province",
   "Type",
   "Find",
   "Cancel"
};

// TODO: get this from string resource
const char* FindWind::s_leaderStrings[ID_Last - ID_First] = {
   NULL,
   NULL,
   "Leader",
   "Command",
   "Rank",
   "Find",
   "Cancel"
};
#endif

const char* FindWind::idToString(ID id)
{
  ASSERT(id < ID_Last);


   static const InGameText::ID s_townStrings[ID_Last - ID_First] = {
      InGameText::Null,
      InGameText::Null,
      IDS_Town,
      IDS_Province,
      IDS_Type,
      IDS_Find,
      IDS_Cancel
   };

   static const InGameText::ID s_leaderStrings[ID_Last - ID_First] = {
      InGameText::Null,
      InGameText::Null,
      IDS_UI_LEADER,
      IDS_Command,
      IDS_Rank,
      IDS_Find,
      IDS_Cancel
   };


  if(d_type == FindItem::FindTown)
  {
    // ASSERT(s_townStrings[id - ID_First]);
    // return s_townStrings[id - ID_First];
    return InGameText::get(s_townStrings[id - ID_First]);
  }
  else
  {
//  ASSERT(s_leaderStrings[id - ID_First]);
//  return s_leaderStrings[id - ID_First];
    return InGameText::get(s_leaderStrings[id - ID_First]);
  }
}

BOOL FindWind::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  int cx = lpCreateStruct->cx;
  int cy = lpCreateStruct->cy;

  initWindow(cx, cy);

  SetFocus(GetDlgItem(hWnd, ID_Edit));

  return TRUE;
}

void FindWind::initWindow(int cx, int cy)
{
  /*
   * Set font
   */

  LogFont lf;
  lf.height((8 * ScreenBase::dbY()) / ScreenBase::baseY());
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(false);
  lf.underline(false);

  Font font;
  font.set(lf);

  /*
   * init background dib
   */

  d_fillWind.setShadowWidth(d_shadowCX);
  d_fillWind.allocateDib(cx, cy);
  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());

  /*
   * Create edit control
   */

  const FControl& ci = controlInfo(ID_Edit);

  CEditData ed;
  ed.d_parent = hWnd;
  ed.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ed.d_bc = scenario->getBorderColors();
  ed.d_x = ci.d_x;
  ed.d_y = ci.d_y;
  ed.d_cx = ci.d_cx;
  ed.d_cy = ci.d_cy;
  ed.d_id = ID_Edit;

  d_edit->create(ed);
  d_edit->setFont(font);


  /*
   * Create list box
   */

  lf.height((7 * ScreenBase::dbY()) / ScreenBase::baseY());
  font.set(lf);

  ItemWindowData ld;
  ld.d_hParent = hWnd;
  ld.d_style = WS_CHILD | WS_VISIBLE;
  ld.d_id = ID_ListBox;
  ld.d_hFont = font;
  ld.d_scrollCX = static_cast<UWORD>((5 * ScreenBase::dbX()) / ScreenBase::baseX());
  ld.d_itemCY = static_cast<UWORD>((9 * ScreenBase::dbY()) / ScreenBase::baseY());
  ld.d_maxItemsShowing = 20;
  ld.d_flags = ItemWindowData::HasScroll | ItemWindowData::NoAutoHide |
               ItemWindowData::NoBorder | ItemWindowData::OwnerDraw;
  ld.d_windowFillDib = scenario->getSideBkDIB(SIDE_Neutral);
  ld.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ld.d_scrollBtns = ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons);
  ld.d_scrollThumbDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground);
  ld.d_scrollFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground);
  ld.d_bColors = scenario->getBorderColors();

  const FControl& ct = controlInfo(ID_ListBox);

  SIZE s;
  s.cx = (ct.d_cx * ScreenBase::dbX()) / ScreenBase::baseX();
  s.cy = ld.d_itemCY * ld.d_maxItemsShowing;
  d_listBox = new FindItemListBox(d_campData, ld, s, d_list);
  ASSERT(d_listBox);

  /*
   * create pop-up menu
   */

  d_menu = new FindItemMenu(d_campWind, d_listBox);
  ASSERT(d_menu);

  /*
   * create push buttons
   */

  for(ID id = ID_FirstButton; id < (ID_LastButton + 1); INCREMENT(id))
  {
    const FControl& ci = controlInfo(id);

    // const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
    const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);

    FUtil::createButton(hWnd, idToString(id), id,
         BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE, fill, ci);
  }
}
#if 0
void FindWind::updateGraphics()
{
   if(d_palID != Palette::paletteID())
   {
      // Remove old things...

      // fillwind
         // .. nothing to do

      // edit control
         d_edit->destroy();
      // list box
         DestroyWindow(d_listBox->getHWND());
         delete d_listBox;
      // popup menu
         delete d_menu;
      // push buttons
         for(ID id = ID_FirstButton; id < (ID_LastButton + 1); INCREMENT(id))
         {
            const FControl& ci = controlInfo(id);

            FUtil::deleteButton(hWnd, id);
         }


      HWND hwnd = getHWND();
      RECT r;
      GetClientRect(hwnd, &r);
      int w = r.right - r.left;
      int h = r.bottom - r.top;

      initWindow(w, h);
   }
}
#endif

void FindWind::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case ID_Edit:
    {
      if(codeNotify == EN_CHANGE)
      {
        const String& text = d_edit->getText();

        if(!text.empty()) {
           int n = text.size();
           /*
           Pressed RETURN
           Delete the character, then do ID_Find
           */
           if(text[n-1] == 13) {
              PostMessage(hwndCtl, WM_KEYDOWN, VK_BACK, MAKELPARAM(1,0) );
              PostMessage(
                 hwnd,
                 WM_COMMAND,
                 MAKEWPARAM(ID_Find, codeNotify),
                 (LPARAM) GetDlgItem(hwnd, ID_Find)
              );
           }
           /*
           Pressed ESCAPE
           Delete the character, then do ID_Cancel
           */
           else if(text[n-1] == 27) {
              PostMessage(hwndCtl, WM_KEYDOWN, VK_BACK, MAKELPARAM(1,0) );
              PostMessage(
                 hwnd,
                 WM_COMMAND,
                 MAKEWPARAM(ID_Cancel, codeNotify),
                 (LPARAM) GetDlgItem(hwnd, ID_Cancel)
              );
           }
           /*
           Typed a normal character
           */
           else {
              d_listBox->findItem(text);
           }
        }
      }

      break;
    }

    case ID_Column1:
    case ID_Column2:
    case ID_Column3:
    {
      FindItemData fd;
      fd.d_type = d_type;
      fd.d_column = (id == ID_Column1) ? FindItemData::Column1 :
                    (id == ID_Column2) ? FindItemData::Column2 :
                    FindItemData::Column3;

      d_listBox->init(&fd);
      break;
    }

    case ID_Find:
      if(d_type == FindItem::FindTown)
        onFindTown();
      else
        onFindLeader();

    case ID_Cancel:
      hide();
      break;
  }
}


BOOL FindWind::onEraseBk(HWND hwnd, HDC hdc)
{
  d_fillWind.paint(hdc);
  return True;
}

void FindWind::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);
  ASSERT(hdc);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  LONG x = (3 * ScreenBase::dbX()) / ScreenBase::baseX();
  LONG y = (8 * ScreenBase::dbY()) / ScreenBase::baseY();

  COLORREF color;
  if(!scenario->getColour("MapInsert", color))
    color = RGB(255,255,255);

  COLORREF oldColor = SetTextColor(hdc, color);

  /*
   * draw header text
   */

  LogFont lf;
  lf.height((10 * ScreenBase::dbX()) / ScreenBase::baseX());
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));
  lf.italic(true);
  lf.underline(false);

  Font font;
  font.set(lf);

  SetBkMode(hdc, TRANSPARENT);
  HFONT oldFont = reinterpret_cast<HFONT>(SelectObject(hdc, font));

  /*
   * Put header text
   */

  // TODO: needs to go in string resource
  const char* text = InGameText::get(IDS_SearchFor);
  wTextOut(hdc, x, y, text);

  SetTextColor(hdc, oldColor);
  SelectObject(hdc, oldFont);

  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hwnd, &ps);
}

void FindWind::run(FindItem::Type type)
{
   // updateGraphics();

  d_type = type;

  d_menu->setType(d_type);

  /*
   *  push buttons text
   */

  for(ID id = ID_FirstButton; id < (ID_LastButton + 1); INCREMENT(id))
  {
    SetDlgItemText(getHWND(), id, idToString(id));
  }

  /*
   * Initialize list
   */

  d_list.reset();

  if(d_type == FindItem::FindTown)
    initTownList();
  else
    initLeaderList();

  /*
   * Initialize listbox
   */

  FindItemData fd;
  fd.d_type = d_type;
  fd.d_column = FindItemData::Column1;

  d_listBox->init(&fd);

  // show listbox
  const FControl& ci = controlInfo(ID_ListBox);
  PixelPoint p((ci.d_x * ScreenBase::dbX()) / ScreenBase::baseX(),
               (ci.d_y * ScreenBase::dbY()) / ScreenBase::baseY());

  d_listBox->show(p);

  // center in main window
  RECT r;
  GetClientRect(APP::getMainHWND(), &r);

  int x = (r.right - d_cx) / 2;
  int y = (r.bottom - d_cy) / 2;
  SetWindowPos(getHWND(), HWND_TOP, x, y, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);
}

void FindWind::initTownList()
{
  ASSERT(d_campData);

  /*
   * Go through towns and add to list
   */

  for(ITown town = 0; town < d_campData->getTowns().entries(); town++)
  {
    const Town& t = d_campData->getTown(town);
    const Province& p = d_campData->getProvince(t.getProvince());

    ListViewItem* lvi = new ListViewItem;
    ASSERT(lvi);

    lvi->d_itemData = static_cast<LPARAM>(town);
    d_list.append(lvi);
  }
}

void FindWind::initLeaderList()
{
  ASSERT(d_campData);

  /*
   * Go through attached leaders and add to list
   */

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    if(GamePlayerControl::canControl(s))
    {
      ConstUnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getPresident(s));
      while(iter.next())
      {
        // skip the pres
        if(iter.current() != d_campData->getArmies().getPresident(s))
        {
          ILeader leader = iter.current()->getLeader();
          ASSERT(leader != NoLeader);

          ListViewItem* lvi = new ListViewItem;
          ASSERT(lvi);

          lvi->d_itemData = reinterpret_cast<LPARAM>(leader.value());
          d_list.append(lvi);
        }
      }
    }
  }

  /*
   * Add independent leaders
   */
}

void FindWind::onFindLeader()
{
  const ListViewItem* lvi = reinterpret_cast<const ListViewItem*>(d_listBox->currentValue());
  ASSERT(lvi);

  ILeader leader = reinterpret_cast<Leader*>(lvi->d_itemData);

  if(leader != NoLeader)
  {
    /*
     * if not an independent leader, center map on leader's unit,
     * otherwise center on Leader Pool
     */

    if(leader->getCommand() != NoCommandPosition)
      d_campWind->centerMapOnUnit(leader->getCommand());
  }
}

void FindWind::onFindTown()
{
  const ListViewItem* lvi = reinterpret_cast<const ListViewItem*>(d_listBox->currentValue());
  ASSERT(lvi);

  ITown town = static_cast<ITown>(lvi->d_itemData);

  if(town != NoTown)
  {
    d_campWind->centerMapOnTown(town);
  }
}

#if 0
void FindWind::destroy()
{
  delete d_edit;
  d_edit = 0;
  DestroyWindow(getHWND());
}
#endif

void FindWind::hide()
{
  // ShowWindow(getHWND(), SW_HIDE);

  show(false);
  d_edit->setText("");
#if !defined(EDITOR)
  d_campWind->findItemUpdated();             
#endif
}

/*--------------------------------------------------------
 * Client access
 */

FindItem::FindItem(CampaignWindowsInterface* campWind, const CampaignData* campData) :
  d_findWind(new FindWind(campWind, campData))
{
  ASSERT(d_findWind);
}

FindItem::~FindItem()
{
  // destroy();
  delete d_findWind;
}

// void FindItem::init()
// {
// }

void FindItem::run(Type type)
{
  d_findWind->run(type);
}

// void FindItem::destroy()
// {
//   if(d_findWind)
//   {
//     delete d_findWind;
//     d_findWind = 0;
//   }
// }

void FindItem::hide()
{
    d_findWind->hide();
}

FindItem::Type FindItem::getType() const
{
  return d_findWind->getType();
}

HWND FindItem::getHWND() const
{
  return d_findWind->getHWND();
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.4  2001/07/30 09:27:23  greenius
 * Various bug fixes
 *
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
