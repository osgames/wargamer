/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CCLOCK_H
#define CCLOCK_H

#ifndef __cplusplus
#error cclock.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Campaign Clock Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.1  1996/06/22 19:04:48  sgreen
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

class CampaignClockWindow;
class CampaignTimeControl;
class CampaignTimeData;
class PixelRect;

class ClockWindow  {
      CampaignClockWindow* d_clockWind;

	public:
		ClockWindow(HWND hwndParent, CampaignTimeControl* control, const CampaignTimeData* date);
		~ClockWindow();

		void show();
		void update();
        void destroy();

        int getWidth() const;
        void setPosition(LONG x, LONG y, LONG w, LONG h);
};

//BOOL CALLBACK addCClockToolTips(HWND hwndCtrl, LPARAM lParam);


#endif /* CCLOCK_H */

