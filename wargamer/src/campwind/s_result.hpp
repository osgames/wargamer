/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef S_RESULT_HPP
#define S_RESULT_HPP

/*
 * Siege result window.
 * Displays final results of a siege battle
 */

#include "cwdll.h"
#include "wg_msg.hpp"

namespace WG_CampaignSiege { struct SiegeInfoData; };
class CampaignWindowsInterface;
class CampaignData;

class SiegeResultMsg : public WargameMessage::WaitableMessage 
{
		const WG_CampaignSiege::SiegeInfoData& d_data;	// info for battle in progress
	public:
		SiegeResultMsg(const WG_CampaignSiege::SiegeInfoData& data) : d_data(data) { }
		CAMPWIND_DLL void run();
};


class SiegeResult_Int {
  public:
	 static void create(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData);
	 static void destroy();
};

#endif
