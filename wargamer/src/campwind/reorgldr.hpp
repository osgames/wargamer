/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef REORGLDR_HPP
#define REORGLDR_HPP

#include "mytypes.h"

class CampaignData;
class CampaignWindowsInterface;
class Reorg_LeaderWindow;

class  ReorgLeader_Int {
    Reorg_LeaderWindow* d_cbrWindow;
  public:
    ReorgLeader_Int(HWND hParent, CampaignWindowsInterface* cw, const CampaignData* campData);
    ~ReorgLeader_Int();

    void run();
    void hide();
    void destroy();
    Boolean initiated() const;
    HWND getHWND() const;
};

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
