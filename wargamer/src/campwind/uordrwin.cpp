/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "uordrwin.hpp"
#include "campdint.hpp"
#include "compos.hpp"
#include "wind.hpp"
#include "campord.hpp"
#include "fonts.hpp"
#include "app.hpp"
#include "cbutton.hpp"
#include "scenario.hpp"
#include "dib.hpp"
#include "fillwind.hpp"
#include "ccombo.hpp"
// #include "generic.hpp"
#include "itemwind.hpp"
#include "userint.hpp"
#include "armies.hpp"
#include "dib_util.hpp"
#include "palette.hpp"
#include "scn_img.hpp"
#include "scrnbase.hpp"
#include "to_win.hpp"
#include "wmisc.hpp"
#include "armyutil.hpp"
#include "cwin_int.hpp"
#include "res_str.h"
#include "resstr.hpp"
#include "camp_snd.hpp"
#include "control.hpp"

/*---------------------------------------------------------------
 * Utilities and handy structs
 */

const int tBorderWidth = 1;

enum ShowType {
  Full
};

struct CInfo {
  UWORD d_x;
  UWORD d_y;
  UWORD d_cx;
  UWORD d_cy;
  UBYTE d_flags;
};

class Util {
public:
  static HWND createButton(HWND hParent, const char* text, int id,
       DWORD style, const DrawDIB* fillDib, const CInfo& ci);

  static HWND createOwnerDrawStatic(HWND hParent, int id, const CInfo& ci);

  static void drawOrderInfo(HWND hwnd, DrawDIBDC* dib, const DrawDIB* fillDib, const DrawDIB* shadowFillDib,
       const CampaignData* campData, ConstParamCP cpi, const CampaignOrder& order, int cx, int cy, int shadowCX);
};

HWND Util::createButton(HWND hParent, const char* text, int id, DWORD style, const DrawDIB* fillDib, const CInfo& ci)
{
  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, text,
            style,
            (ScreenBase::dbX() * ci.d_x) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_y) / ScreenBase::baseY(),
            (ScreenBase::dbX() * ci.d_cx) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_cy) / ScreenBase::baseY(),
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  cb.setFillDib(fillDib);
  cb.setBorderColours(scenario->getBorderColors());
  if(style & BS_AUTOCHECKBOX)
    cb.setCheckBoxImages(ScenarioImageLibrary::get(ScenarioImageLibrary::CheckButtons));

  /*
   * Set up font
   */

  const int fontCY = (style & BS_AUTOCHECKBOX) ?
      (7 * ScreenBase::dbY()) / ScreenBase::baseY() :
      (8 * ScreenBase::dbY()) / ScreenBase::baseY();

  LogFont lf;
  lf.height(fontCY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Normal));

  Font font;
  font.set(lf);
  cb.setFont(font);

  return hButton;
}

HWND Util::createOwnerDrawStatic(HWND hParent, int id, const CInfo& ci)
{
  HWND h = CreateWindow("Static", NULL,
            SS_OWNERDRAW | SS_NOTIFY | WS_CHILD | WS_VISIBLE,
            (ScreenBase::dbX() * ci.d_x) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_y) / ScreenBase::baseY(),
            (ScreenBase::dbX() * ci.d_cx) / ScreenBase::baseX(), (ScreenBase::dbY() * ci.d_cy) / ScreenBase::baseY(),
            hParent, (HMENU)id, APP::instance(), NULL);

  ASSERT(h != 0);


  return h;
}

void Util::drawOrderInfo(HWND hwnd, DrawDIBDC* dib, const DrawDIB* fillDib, const DrawDIB* shadowFillDib,
       const CampaignData* campData, ConstParamCP cpi, const CampaignOrder& order, int cx, int cy, int shadowCX)
{
   ASSERT(dib);
   ASSERT(fillDib);
   ASSERT(cx <= dib->getWidth());
   ASSERT(cy <= dib->getHeight());

   const int dbX = ScreenBase::dbX();
   const int dbY = ScreenBase::dbY();
   const int baseX = ScreenBase::baseX();
   const int baseY = ScreenBase::baseY();

   /*
   * Fill in background
   */

   const int dispCX = cx - shadowCX;
   const int dispCY = cy - shadowCX;

   dib->rect(0, 0, dispCX, dispCY, fillDib);

   ColourIndex ci = dib->getColour(PALETTERGB(0, 0, 0));
   dib->frame(0, 0, dispCX, dispCY, ci);

   /*
   * Fill in our info
   */

   int fontCX = (9 * dbY) / baseY;
   const int cutoff = 10;
   LogFont lf;
   lf.height(fontCX);
   lf.weight(FW_MEDIUM);
   lf.italic(true);
   lf.face(scenario->fontName((fontCX > cutoff) ? Font_Bold : Font_SmallBold));

   Font font;
   font.set(lf);
   HFONT oldFont = dib->setFont(font);

   // starting point
   int x =  (3 * dbX) / baseX;
   int y =  (3 * dbY) / baseY;
   int spacing = (7 * dbY) / baseY;

   /*
   * If move order put destination
   */

   if(order.isMoveOrder())
   {
      ASSERT(order.hasTargetUnit() || order.getDestTown() != NoTown);
      const char* text = (order.hasTargetUnit()) ?
         order.getTargetUnit()->getNameNotNull() :
      campData->getTownName(order.getDestTown());

      wTextOut(dib->getDC(), x, y, text);
      y += (1.5 * spacing);

      fontCX = (7 * dbY) / baseY;
      lf.height(fontCX);
      lf.italic(false);
      lf.face(scenario->fontName((fontCX > cutoff) ? Font_Bold : Font_SmallBold));

      font.set(lf);
      dib->setFont(font);

      int nVias = order.getNVias();
      if(nVias > 0)
      {
         ResString r_via(IDS_UOP_Via);
         const char* s_via = r_via.c_str();
         // static const char* s_via = "Via:";

         int vx = x * 2;

         for(int i = 0; i < nVias; i++)
         {
            text = campData->getTownName(order.getVia(i));
            wTextPrintf(dib->getDC(), vx, y, "%s: %s", s_via, text);
            y += spacing;
         }
      }
   }

   /*
   * Put action date
   */

   fontCX = (7 * dbY) / baseY;
   lf.height(fontCX);
   lf.italic(false);
   lf.face(scenario->fontName((fontCX > cutoff) ? Font_Bold : Font_SmallBold));

   font.set(lf);
   dib->setFont(font);

   // TODO: get this from resource
   // static const char* s_on = "On";

   ResString r_on(IDS_UOP_On);
   const char* s_on = r_on.c_str();


   char buf[100];
   Date date = (order.isTimedOrder()) ? order.getDate() : campData->getDate();
   const char* text = date.toAscii(buf);

   wTextPrintf(dib->getDC(), x, y, "%s %s", s_on, text);

   dib->setFont(oldFont);

   /*
   * Put unit-typpe strengths
   */

   // TODO: get from resource
   // const char* inf = "Inf";
   // const char* cav = "Cav";
   // const char* guns = "Guns";

   //   ResString r_inf(IDS_UOP_Inf);
   //   ResString r_cav(IDS_UOP_Cav);
   //   ResString r_guns(IDS_UOP_Guns);
   //   const char* inf = r_inf;
   //   const char* cav = r_cav;
   //   const char* guns = r_guns;

   const char* inf  = InGameText::get(IDS_UOP_Inf);
   const char* cav  = InGameText::get(IDS_UOP_Cav);
   const char* guns = InGameText::get(IDS_GunsAbrev);

   const LONG nInf = CampaignArmy_Util::manpowerByType(campData, cpi, BasicUnitType::Infantry);
   const LONG nCav = CampaignArmy_Util::manpowerByType(campData, cpi, BasicUnitType::Cavalry);
   const LONG nGuns = CampaignArmy_Util::totalGuns(campData, cpi);

   lf.underline(true);

   font.set(lf);
   dib->setFont(font);

   y = (40 * dbY) / baseY;

   // draw a line
   dib->hLine(0, y, dispCX, ci);

   y += (2 * dbY) / baseY;
   const int typeSpace = (23 * dbX) / baseX;

   wTextOut(dib->getDC(), x, y, inf);
   x += typeSpace;

   wTextOut(dib->getDC(), x, y, cav);
   x += typeSpace;

   wTextOut(dib->getDC(), x, y, guns);

   y += spacing;
   x = (2 * dbX) / baseX;

   lf.underline(false);

   font.set(lf);
   dib->setFont(font);

   wTextPrintf(dib->getDC(), x, y, "%ld", nInf);
   x += typeSpace;

   wTextPrintf(dib->getDC(), x, y, "%ld", nCav);
   x += typeSpace;

   wTextPrintf(dib->getDC(), x, y, "%ld", nGuns);

   /*
   * Apply shadow
   */

   // first fill in shadow filler

   // rightside
   dib->rect(dispCX, 0, shadowCX, dispCY + shadowCX, shadowFillDib);
   // bottom
   dib->rect(0, dispCY, dispCX, shadowCX, shadowFillDib);

   // now apply shadow
   const int darkenBy = 60;

   // right side
   dib->darkenRectangle(dispCX, shadowCX, shadowCX, dispCY, darkenBy);
   // bottom
   dib->darkenRectangle(shadowCX, dispCY, dispCX - shadowCX, shadowCX, darkenBy);
}

/*---------------------------------------------------------------
 * Pop-up advanced order window
 */

class AdvancedOrderWindow : public WindowBaseND {
    HWND d_hParent;
    CampaignOrder* d_order;
     Orders::AdvancedOrders d_newOrder;

    enum ID {
       ID_First = 100,
       ID_FirstCheckBox = ID_First,
       ID_SoundGuns = ID_FirstCheckBox,    // set sound of guns option
       ID_Pursue,                          // set pursue option
       ID_SiegeActive,                     // set siege-active option
       ID_DropOffGarrison,                 // set drop-off garrison option
       ID_AutoStorm,                       // set auto-storm option
       ID_LastCheckBox = ID_AutoStorm,
       ID_FirstButton,

       // ID_Send = ID_FirstButton,
       ID_Cancel = ID_FirstButton,
       ID_OK,
       ID_LastButton = ID_OK,

       ID_Last,
    };

    FillWindow d_fillWind;
    static const CInfo s_cInfo[ID_Last - ID_First];
    // static const char* s_strings[ID_Last - ID_First];
    static const int s_strings[ID_Last - ID_First];

    const int d_shadowCX;
    const int d_cx;
    const int d_cy;

    const int d_id;
    bool d_changed;
  public:

    AdvancedOrderWindow(HWND hParent, CampaignOrder* order, int id);
    ~AdvancedOrderWindow() { selfDestruct(); }

    void run(const PixelPoint& p, ConstParamCP cpi);
    // void destroy();
    // void hide();

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    BOOL onEraseBk(HWND hwnd, HDC hdc);

    void enableButtons();
//  void onDestroy(HWND hWnd);
//  UINT onNCHitTest(HWND hwnd, int x, int y);

    const CInfo& controlInfo(ID id) const
    {
      ASSERT(id < ID_Last);
      return s_cInfo[id - ID_First];
    }

    const char* idToString(ID id);
    Boolean idToCheck(ID id);
};

/*
 * Coodinates (in dialog units) of controls
 */

const CInfo AdvancedOrderWindow::s_cInfo[ID_Last - ID_First] = {
  {  3,   3,  67,  9, UnitOrder_Int::FullOrder },      // sound-guns option
  {  3,  14,  67,  9, UnitOrder_Int::FullOrder },      // pursue option
  {  3,  25,  67,  9, UnitOrder_Int::FullOrder },      // siege-active option
  {  3,  36,  67,  9, UnitOrder_Int::FullOrder },      // drop-off garrison option
  {  3,  47,  67,  9, UnitOrder_Int::FullOrder },      // auto-storm option
//  {  5,  61,  30, 10, UnitOrder_Int::FullOrder },      // cancel advanced order
//  { 38,  61,  30, 10, UnitOrder_Int::FullOrder }       // ok advanced order
  {  5,  61,  48, 10, UnitOrder_Int::FullOrder },      // cancel advanced order
  { 55,  61,  48, 10, UnitOrder_Int::FullOrder }       // ok advanced order
};


AdvancedOrderWindow::AdvancedOrderWindow(HWND hParent, CampaignOrder* order, int id) :
  d_hParent(hParent),
  d_order(order),
  d_newOrder(order->getAdvancedOrders()),
  d_shadowCX((ScreenBase::dbX() * 3) / ScreenBase::baseX()),
//  d_cx(((ScreenBase::dbX() * 73) / ScreenBase::baseX()) + d_shadowCX),
  d_cx(((ScreenBase::dbX() * 113) / ScreenBase::baseX()) + d_shadowCX),
  d_cy(((ScreenBase::dbY() * 74) / ScreenBase::baseY()) + d_shadowCX),
  d_id(id),
  d_changed(False)
{
  ASSERT(d_hParent);

  HWND hWnd = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_POPUP | WS_CLIPSIBLINGS,
      0, 0, d_cx, d_cy,
      d_hParent, NULL // , APP::instance()
  );
}

LRESULT AdvancedOrderWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE,  onCreate);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
//  HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
//  HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
//  HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
//  HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}


#if 0
// TODO: get this from string resource
const char* AdvancedOrderWindow::s_strings[ID_Last - ID_First] = {
   "Sound of Guns",
   "Pursue",
   "Siege Active",
   "Drop-Off",
   "Auto-Storm",
   "Send",
   "OK"
};
#endif

const int AdvancedOrderWindow::s_strings[ID_Last - ID_First] = {
   IDS_UOP_SoundOfGuns,
   IDS_UOP_Pursue,
   IDS_UOP_SiegeActive,
   IDS_UOP_DropOff,
   IDS_UOP_AutoStorm,
   IDS_UOP_Cancel,
   IDS_UOP_OK,     // IDS_UOP_Send,
};

const char* AdvancedOrderWindow::idToString(ID id)
{
  ASSERT(id < ID_Last);
  ASSERT(s_strings[id - ID_First]);

  static ResourceStrings res(s_strings, ID_Last - ID_First);

  return res.get(id - ID_First);

}

Boolean AdvancedOrderWindow::idToCheck(ID id)
{
  ASSERT(id >= ID_FirstCheckBox);
  ASSERT(id <= ID_LastCheckBox);

  if(id == ID_SoundGuns)
    return d_newOrder.soundGuns();
  else if(id == ID_Pursue)
    return d_newOrder.pursue();
  else if(id == ID_SiegeActive)
    return d_newOrder.siegeActive();
  else if(id == ID_DropOffGarrison)
    return d_newOrder.dropOffGarrison();
  else if(id == ID_AutoStorm)
    return d_newOrder.autoStorm();

  return False;
}

BOOL AdvancedOrderWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  int cx = lpCreateStruct->cx;
  int cy = lpCreateStruct->cy;

  /*
   * init background dib
   */

  d_fillWind.setShadowWidth(d_shadowCX);
  d_fillWind.allocateDib(cx, cy);
  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());

  /*
   * create check boxes
   */

  for(ID id = ID_FirstCheckBox; id < (ID_LastCheckBox + 1); INCREMENT(id))
  {
    const CInfo& ci = controlInfo(id);

    // const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
    const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);
    Util::createButton(hWnd, idToString(id), id,
         BS_AUTOCHECKBOX | WS_CHILD | WS_VISIBLE, fill, ci);
  }

  /*
   * create push buttons
   */

  for(id = ID_FirstButton; id < (ID_LastButton + 1); INCREMENT(id))
  {
    const CInfo& ci = controlInfo(id);

    // const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
    const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);

    Util::createButton(hWnd, idToString(id), id,
         BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE, fill, ci);
  }

  return TRUE;
}

void AdvancedOrderWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case ID_SoundGuns:
    case ID_Pursue:
    case ID_SiegeActive:
    case ID_DropOffGarrison:
    case ID_AutoStorm:
    {
      CustomButton cb(hwndCtl);
      Boolean checked = cb.getCheck();

      if(id == ID_SoundGuns)
        d_newOrder.soundGuns(checked);
      else if(id == ID_Pursue)
        d_newOrder.pursue(checked);
      else if(id == ID_SiegeActive)
        d_newOrder.siegeActive(checked);
      else if(id == ID_DropOffGarrison)
        d_newOrder.dropOffGarrison(checked);
      else
        d_newOrder.autoStorm(checked);

      d_changed = True;
      enableButtons();
      break;
    }

    // case ID_Send:
    case ID_OK:
      // d_order.advancedOnly(True);
      show(false);    // hide();
      if(d_changed)
      {
         if(!d_order->changed())
         {
            d_order->advancedOnly(True);
         }

         d_order->copyAdvanced(d_newOrder);
         SendMessage(d_hParent, WM_COMMAND, MAKEWPARAM(d_id, 0), reinterpret_cast<LPARAM>(hwnd));
      }
      break;

    case ID_Cancel:
      show(false);    // hide();
      break;
  }
}


void AdvancedOrderWindow::enableButtons()
{
  CustomButton b(getHWND(), ID_OK);     // Send);
  b.enable(d_changed);
}

BOOL AdvancedOrderWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  d_fillWind.paint(hdc);
  return True;
}

void AdvancedOrderWindow::run(const PixelPoint& p, ConstParamCP cpi)
{
  d_changed = False;

#if 0
    // TODO: Check this out with BEN...
    //       If so, then we should have a method of storing individual
    //       orders for each unit and sending them seperately.


  // copy units last advanced orders
  // this is done because each unit in a command
  // can have seperate advanced orders (i.e. pursue, sound of battle, etc.)
  // all other order values remain the same for all units in a command
  d_order.copyAOFlags(*cpi->getLastOrder());
#endif
    d_newOrder = d_order->getAdvancedOrders();

  /*
   * Set option buttons
   */

  for(int id = ID_FirstCheckBox; id < (ID_LastCheckBox + 1); id++)
  {
    CustomButton cb(getHWND(), id);
    cb.setCheck(idToCheck(static_cast<ID>(id)));
  }

  enableButtons();

  SetWindowPos(getHWND(), HWND_TOP, p.getX(), p.getY(), 0, 0, SWP_NOSIZE);
  show(true);
}

// void AdvancedOrderWindow::destroy()
// {
//   DestroyWindow(getHWND());
// }
//
// void AdvancedOrderWindow::hide()
// {
//   ShowWindow(getHWND(), SW_HIDE);
// }

/*---------------------------------------------------------------
 * Our actual order window
 */

class UnitOrderWindow : public WindowBaseND {
    CampaignUserInterface* d_owner;
    HWND d_hParent;
    CampaignWindowsInterface* d_campWind;
    const CampaignData* d_campData;
    CampaignOrder& d_order;
    CampaignComboInterface* d_combos;  // order combos.
    Boolean d_changed;

    DrawDIBDC* d_dib;
    const DrawDIB* d_comboFillDib;

    UBYTE d_flags;

    AdvancedOrderWindow* d_sOrderWind;
    TimedOrderWindow d_calender;

    enum ID {
       ID_First = 100,
       ID_FirstCombo = ID_First,
       ID_Units = ID_FirstCombo,           // current unit
       ID_Orders,                          // current order-type
       ID_MoveHow,                         // move how ? (i.e. force-march)
       ID_OnArrival,                       // order on arrival
       ID_Avoid,                           // avoid combo
       ID_LastCombo = ID_Avoid,
       ID_FirstButton,
       ID_ShowAdvanced= ID_FirstButton,
       ID_ShowInfo,                        // show info-window
       ID_OB,                              // show reorganize / ob window
       ID_Date,
       ID_BRButtonFirst,
       ID_FullOrder = ID_BRButtonFirst,
       ID_Send,
       ID_Cancel,
       ID_LastButton = ID_Cancel,
       ID_FirstCheckBox,
       ID_ToUnit = ID_FirstCheckBox,
       ID_BRButtonLast = ID_ToUnit,
       ID_Offensive,
       ID_Defensive,
       ID_LastCheckBox = ID_Defensive,
       ID_Info,
       ID_Last,

       ID_Advanced,


       ID_ListBoxOffset = 100
    };

    enum {
      TimerID = 500,
      ID_Calender
    };

    PixelPoint d_point;

    FillWindow d_fillWind;
    static const CInfo s_cInfo[ID_Last - ID_First];
    // static const char* s_strings[ID_Last - ID_First];
    static const int s_strings[ID_Last - ID_First];

    const int d_shadowCX;
    const int d_cx;
    const int d_cy;
    const int d_moveCY;

  public:

    UnitOrderWindow(CampaignUserInterface* owner, HWND hParent,
        CampaignWindowsInterface* campWind, const CampaignData* campData, CampaignOrder& order);
    ~UnitOrderWindow()
    {
        selfDestruct();
      if(d_dib)
        delete d_dib;
    }

    void init(StackedUnitList& units, UnitListID unitID);
    void run(const PixelPoint& p, UBYTE flags);
    // void destroy();
    void hide();
    ConstICommandPosition currentUnit();
    int width() const { return d_cx; }
    int fullHeight() const { return d_cy; }
    int moveHeight() const { return d_moveCY; }

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    BOOL onEraseBk(HWND hwnd, HDC hdc);

    void onDestroy(HWND hWnd)
    {
      if(d_sOrderWind)
      {
        delete d_sOrderWind;
        d_sOrderWind = 0;
      }

       delete d_combos;
       d_combos = 0;

       delete d_sOrderWind;
       d_sOrderWind = 0;
    }

    UINT onNCHitTest(HWND hwnd, int x, int y);
    void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* di);
    void onTimer(HWND hwnd, UINT id);

    void updateValues();
    void enableControls();

    const CInfo& controlInfo(ID id) const
    {
      ASSERT(id < ID_Last);
      return s_cInfo[id - ID_First];
    }

    void redrawControl(int id);
    CampaignComboInterface::Type idToType(ID id);
    const char* idToString(ID id);
    Boolean idToCheck(ID id);
    void sizeWindow();
};

/*
 * Coodinates (in base units) of controls
 */

const CInfo UnitOrderWindow::s_cInfo[ID_Last - ID_First] = {
  {   3,   3, 154, 13, UnitOrder_Int::FullOrder | UnitOrder_Int::MoveOrder },      // units
  {   3,  32,  74, 10, UnitOrder_Int::FullOrder                            },      // orders
  {   3,  57,  74, 10, UnitOrder_Int::FullOrder                            },      // move how
  {   3,  69,  74, 10, UnitOrder_Int::FullOrder                            },      // onarrival
  {   3,  81,  74, 10, UnitOrder_Int::FullOrder                            },      // avoid
  {   3,  18,  37, 10, UnitOrder_Int::FullOrder                            },      // show advanced-orders
  {  42,  18,  37, 10, UnitOrder_Int::FullOrder                            },      // show info
  {  81,  18,  37, 10, UnitOrder_Int::FullOrder                            },      // show ob
  { 120,  18,  37, 10, UnitOrder_Int::FullOrder                            },      // show date window
//  {  70,  98,  25, 10, UnitOrder_Int::MoveOrder                            },      // send
//  { 100,  98,  25, 10, UnitOrder_Int::FullOrder | UnitOrder_Int::MoveOrder },      // send
//  { 130,  98,  25, 10, UnitOrder_Int::FullOrder | UnitOrder_Int::MoveOrder },      // cancel
  {  45,  98,  36, 10, UnitOrder_Int::MoveOrder                            },      // send
  {  83,  98,  36, 10, UnitOrder_Int::FullOrder | UnitOrder_Int::MoveOrder },      // send
  { 121,  98,  36, 10, UnitOrder_Int::FullOrder | UnitOrder_Int::MoveOrder },      // cancel
//  {   5,  98,  48, 10, UnitOrder_Int::MoveOrder                            },      // send
//  {  55,  98,  48, 10, UnitOrder_Int::FullOrder | UnitOrder_Int::MoveOrder },      // send
//  { 105,  98,  48, 10, UnitOrder_Int::FullOrder | UnitOrder_Int::MoveOrder },      // cancel
//  {   5,  99,  34,  8, UnitOrder_Int::FullOrder | UnitOrder_Int::MoveOrder },      // to unit
  {   3,  99,  38,  8, UnitOrder_Int::FullOrder | UnitOrder_Int::MoveOrder },      // to unit
  {  11,  45,  25,  8, UnitOrder_Int::FullOrder                            },      // offensive
  {  38,  45,  25,  8, UnitOrder_Int::FullOrder                            },      // defensive
  {  79,  32,  75, 63, UnitOrder_Int::FullOrder                            },
};

//const int winCX = (ScreenBase::dbX() * 160) / ScreenBase::baseX();
//const int winCY = (ScreenBase::dbY() * 110) / ScreenBase::baseY();

UnitOrderWindow::UnitOrderWindow(CampaignUserInterface* owner, HWND hParent, CampaignWindowsInterface* campWind, const CampaignData* campData, CampaignOrder& order) :
  d_owner(owner),
  d_hParent(hParent),
  d_campWind(campWind),
  d_campData(campData),
  d_changed(False),
  d_order(order),
  d_combos(new CampaignComboInterface),
  d_dib(0),
  d_comboFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
  d_flags(UnitOrder_Int::FullOrder),
  d_sOrderWind(0),
  d_shadowCX((ScreenBase::dbX() * 2) / ScreenBase::baseX()),
  d_cx((ScreenBase::dbX() * 160) / ScreenBase::baseX()),
  d_cy((ScreenBase::dbY() * 110) / ScreenBase::baseY()),
  d_moveCY((ScreenBase::dbY() * 32) / ScreenBase::baseY())
{
  ASSERT(d_hParent);
  ASSERT(d_campData);
  ASSERT(d_comboFillDib);

  HWND hWnd = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_POPUP | WS_CLIPSIBLINGS,
      0, 0, d_cx, d_cy,
      d_hParent, NULL // , APP::instance()
  );
}

LRESULT UnitOrderWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
    HANDLE_MSG(hWnd, WM_CREATE,  onCreate);
    HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
    HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
    HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
    HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
    HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
    HANDLE_MSG(hWnd, WM_TIMER, onTimer);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

CampaignComboInterface::Type UnitOrderWindow::idToType(ID id)
{
  ASSERT(id <= ID_LastCombo);
  if(id == ID_Units)
    return CampaignComboInterface::UnitList;
  else if(id == ID_Orders)
    return CampaignComboInterface::OrderType;
  else if(id == ID_MoveHow)
    return CampaignComboInterface::MoveHow;
  else if(id == ID_OnArrival)
    return CampaignComboInterface::OnArrival;
  else //if(id == ID_Avoid)
    return CampaignComboInterface::Aggression;
}

#if 0
// TODO: get this from string resource
const char* UnitOrderWindow::s_strings[ID_Last - ID_First] = {
   0,
   0,
   0,
   0,
   0,
   "Static",
   "Info",
   "OB",
   "Date",
   "Full",
   "Send",
   "Cancel",
   "To Unit",
   "Off",
   "Def",
   0,
};
#endif


const int UnitOrderWindow::s_strings[ID_Last - ID_First] = {
   -1,
   -1,
   -1,
   -1,
   -1,
   IDS_UOP_Static,
   IDS_UOP_Info,
   IDS_UOP_OB,
   IDS_UOP_Date,
   IDS_UOP_Full,
   IDS_UOP_Send,
   IDS_UOP_Cancel,
   IDS_UOP_ToUnit,
   IDS_UOP_Off,
   IDS_UOP_Def,
   -1,
};

const char* UnitOrderWindow::idToString(ID id)
{
  ASSERT(id < ID_Last);
  ASSERT(s_strings[id - ID_First]);

    static ResourceStrings res(s_strings, ID_Last - ID_First);

    return res.get(id - ID_First);
}

Boolean UnitOrderWindow::idToCheck(ID id)
{
  ASSERT(id >= ID_FirstCheckBox);
  ASSERT(id <= ID_LastCheckBox);

  if(id == ID_Offensive)
    return (d_order.posture() == Orders::Posture::Offensive);
  else if(id == ID_Defensive)
    return (d_order.posture() == Orders::Posture::Defensive);
  else
    return False;
}

BOOL UnitOrderWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  int cx = lpCreateStruct->cx;
  int cy = lpCreateStruct->cy;

  /*
   * init background dib
   */

  d_fillWind.allocateDib(cx, cy);
  d_fillWind.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground));
  d_fillWind.setBorderColors(scenario->getBorderColors());

  /*
   * create combos
   */

  for(ID id = ID_FirstCombo; id < (ID_LastCombo + 1); INCREMENT(id))
  {
    const CInfo& ci = controlInfo(id);

    Util::createOwnerDrawStatic(hWnd, id, ci);

    SIZE s;
    s.cx = (ci.d_cx * ScreenBase::dbX()) / ScreenBase::baseX();
    s.cy = (ci.d_cy * ScreenBase::dbY()) / ScreenBase::baseY();

    d_combos->create(hWnd, idToType(id), id + ID_ListBoxOffset,
       d_campData, s, ItemWindowData::HasScroll | ItemWindowData::ShadowBackground | ItemWindowData::OwnerDraw);
  }

  /*
   * Initialize Movehow and Aggresion Combos here since their values
   * do not change
   */

  CampaignComboData data;
  data.d_order = &d_order;

  d_combos->init(CampaignComboInterface::MoveHow, data);
  d_combos->init(CampaignComboInterface::Aggression, data);

  /*
   * create check boxes
   */

  for(id = ID_FirstCheckBox; id < (ID_LastCheckBox + 1); INCREMENT(id))
  {
    const CInfo& ci = controlInfo(id);

    // const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
    const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);
    Util::createButton(hWnd, idToString(id), id,
         BS_AUTOCHECKBOX | WS_CHILD | WS_VISIBLE, fill, ci);
  }

  /*
   * create push buttons
   */

  for(id = ID_FirstButton; id < (ID_LastButton + 1); INCREMENT(id))
  {
    const CInfo& ci = controlInfo(id);

    // const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
    const DrawDIB* fill = scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground);

    Util::createButton(hWnd, idToString(id), id,
         BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE, fill, ci);
  }

  /*
   * Create static info section
   */

  const CInfo& ci = controlInfo(ID_Info);
  Util::createOwnerDrawStatic(hWnd, ID_Info, ci);

  /*
   * Create pop-up advanced-order window
   */

  d_sOrderWind = new AdvancedOrderWindow(hWnd, &d_order, ID_Advanced);
  ASSERT(d_sOrderWind);

  /*
   * Create calender
   */

  d_calender.create(hWnd, ID_Calender, d_order, d_campData->getDate());

  return TRUE;
}

void UnitOrderWindow::hide()
{
   show(false);    // hide();
   d_sOrderWind->show(false);
   d_calender.show(false);
}

void UnitOrderWindow::onTimer(HWND hwnd, UINT id)
{
  ASSERT(id == TimerID);

  SetWindowPos(getHWND(), HWND_TOPMOST, d_point.getX(), d_point.getY(), 0, 0, SWP_NOSIZE);
  show(true);

  BOOL result = KillTimer(hwnd, id);
  ASSERT(result);
}

void UnitOrderWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case ID_ShowAdvanced:
    {
      if(d_sOrderWind)
      {
        POINT pt;
        pt.x = 0;
        pt.y = 0;

        ClientToScreen(hwndCtl, &pt);

        PixelPoint p(pt.x, pt.y);
        d_sOrderWind->run(p, currentUnit());

        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUPOPUP, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }

      break;
    }

    case ID_ShowInfo:
    {
      if(d_owner)
      {
         ConstICommandPosition cp = currentUnit();
         if ((cp != NoCommandPosition) && !cp->isDead())
         {

           POINT pt;
           pt.x = 0;
           pt.y = 0;

           ClientToScreen(hwndCtl, &pt);

           // PixelPoint p(pt.x, pt.y);
           // d_owner->runInfo(p);
           d_owner->infoUnit(cp);

           d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUPOPUP, CAMPAIGNSOUNDPRIORITY_INSTANT);
         }
      }

      break;
    }

    case ID_OB:
    {
      if(d_owner)
      {
         ConstICommandPosition cp = currentUnit();
         if((cp != NoCommandPosition) && !cp->isDead())
         {
           POINT pt;
           pt.x = 0;
           pt.y = 0;

           ClientToScreen(hwndCtl, &pt);

           // PixelPoint p(pt.x, pt.y);
           // d_owner->runOB(p);
           d_owner->reorgOB(cp);

           d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUPOPUP, CAMPAIGNSOUNDPRIORITY_INSTANT);
         }
      }

      break;
    }

    case ID_Date:
    {
      d_calender.init(d_campData->getDate());

      POINT pt;
      pt.x = 0;
      pt.y = 0;

      ClientToScreen(hwndCtl, &pt);

      PixelPoint p(pt.x, pt.y);
      d_calender.show(p);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUPOPUP, CAMPAIGNSOUNDPRIORITY_INSTANT);

      break;
    }

    case ID_Calender:
    {
      redrawControl(ID_Info);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUPOPUP, CAMPAIGNSOUNDPRIORITY_INSTANT);

      break;
    }

    case ID_Units:
    case ID_MoveHow:
    case ID_OnArrival:
    case ID_Orders:
    case ID_Avoid:
    {
      if(codeNotify == STN_CLICKED)
      {
        RECT r;
        GetWindowRect(hwndCtl, &r);

        PixelPoint p(r.left, r.top);
        d_combos->show(idToType(static_cast<ID>(id)), p);

      }
      break;
    }

    case ID_Offensive:
    case ID_Defensive:
    {
      if(codeNotify == BN_CLICKED)
      {
        Orders::Posture::Type p = (d_order.posture() == Orders::Posture::Offensive) ?
           Orders::Posture::Defensive : Orders::Posture::Offensive;

        d_order.posture(p);

        CustomButton ob(getHWND(), ID_Offensive);
        CustomButton db(getHWND(), ID_Defensive);

        ob.setCheck(p == Orders::Posture::Offensive);
        db.setCheck(p == Orders::Posture::Defensive);

        d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      }

      break;
    }

    case (ID_Units + ID_ListBoxOffset):
       {
         redrawControl(ID_Info);

         d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);

         break;
       }

    case (ID_Orders + ID_ListBoxOffset):
    {
      Orders::Type::Value cbo = static_cast<Orders::Type::Value>(d_combos->getValue(CampaignComboInterface::OrderType));
      Orders::Type::Value co = d_order.getType();

      d_order.setType(cbo);

      if(co == Orders::Type::MoveTo && cbo != Orders::Type::MoveTo)
      {
        /*
         * Clear out any move related values
         */

        d_order.setDestTown(NoTown);
        d_order.setTarget(NoCommandPosition);
        d_order.getVias()->clear();
        d_campWind->redrawMap();

        CampaignComboData data;
        data.d_currentUnit = currentUnit();
        data.d_order = &d_order;
        d_combos->init(CampaignComboInterface::OrderType, data); //owner()->currentUnit(), owner()->order());
      }

      redrawControl(ID_Orders);
      redrawControl(ID_Info);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);

      break;
    }

    case (ID_MoveHow + ID_ListBoxOffset):
    {
      d_order.setMoveHow(static_cast<Orders::AdvancedOrders::MoveHow>(d_combos->getValue(CampaignComboInterface::MoveHow)));
      redrawControl(ID_MoveHow);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;
    }

    case (ID_OnArrival + ID_ListBoxOffset):
    {
      d_order.setOrderOnArrival(static_cast<Orders::Type::Value>(d_combos->getValue(CampaignComboInterface::OnArrival)));
      redrawControl(ID_OnArrival);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;
    }


    case (ID_Avoid + ID_ListBoxOffset):
    {
      d_order.setAggressLevel(static_cast<Orders::Aggression::Value>(d_combos->getValue(CampaignComboInterface::Aggression)));
      redrawControl(ID_Avoid);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;
    }

    case ID_FullOrder:
      d_flags = UnitOrder_Int::FullOrder;
      sizeWindow();

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUPOPUP, CAMPAIGNSOUNDPRIORITY_INSTANT);

      break;

    case ID_Send:
      updateValues();
      hide();
      d_owner->sendOrder(currentUnit());
      break;

    case ID_Advanced:
      // d_owner->sendOrder(currentUnit());
        d_changed = true;
      break;

    case ID_Cancel:
      hide();
      d_owner->cancelOrder();
      break;
  }
}

void UnitOrderWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* di)
{
  ASSERT(d_comboFillDib);

  const int cx = di->rcItem.right - di->rcItem.left;
  const int cy = di->rcItem.bottom - di->rcItem.top;

  DIB_Utility::allocateDib(&d_dib, cx, cy);
  ASSERT(d_dib);

  d_dib->setBkMode(TRANSPARENT);

  /*
   * Set up item data
   */

  CampaignComboData data;
  data.d_currentUnit = currentUnit();
  data.d_order = &d_order;
  data.d_itemCX = cx;
  data.d_itemCY = cy;
  data.d_showManpower = True;

  /*
   * Set up font
   */

  const int fontCX = (8 * ScreenBase::dbY()) / ScreenBase::baseY();
  const int cutoff = 10;
  LogFont lf;
  lf.height(fontCX);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName((fontCX > cutoff) ? Font_Bold : Font_SmallBold));

  Font font;
  font.set(lf);
  d_dib->setFont(font);

  HPALETTE oldPal = SelectPalette(di->hDC, Palette::get(), FALSE);
  RealizePalette(di->hDC);

  switch(di->CtlID)
  {
    case ID_Units:
    case ID_Orders:
    case ID_MoveHow:
    case ID_OnArrival:
    case ID_Avoid:
      d_combos->drawCombo(idToType(static_cast<ID>(di->CtlID)), d_dib, d_comboFillDib, data);
      break;

    case ID_Info:
      Util::drawOrderInfo(di->hwndItem, d_dib, d_comboFillDib, d_fillWind.getFillDib(),
          d_campData, data.d_currentUnit, d_order, cx, cy, d_shadowCX);
      break;
  }

  CampaignUserInterface::blitDibToControl(d_dib, di);

  SelectPalette(di->hDC, oldPal, FALSE);
}

BOOL UnitOrderWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  d_fillWind.paint(hdc);
  return True;
}

UINT UnitOrderWindow::onNCHitTest(HWND hwnd, int x, int y)
{
  return HTCAPTION;
}

void UnitOrderWindow::redrawControl(int id)
{
  ASSERT(id < ID_Last);

  HWND h = GetDlgItem(hWnd, id);
  ASSERT(h);

  InvalidateRect(h, NULL, FALSE);
  UpdateWindow(h);
}

void UnitOrderWindow::enableControls()
{
  const Boolean hasMoveOrder = (d_order.getType() == Orders::Type::MoveTo);

  {
    WindowControl cb(getHWND(), ID_MoveHow);
    cb.enable(hasMoveOrder);
  }

  {
    WindowControl cb(getHWND(), ID_OnArrival);
    cb.enable(hasMoveOrder);
  }
}

void UnitOrderWindow::updateValues()
{
  const Boolean hasMoveOrder = (d_order.getType() == Orders::Type::MoveTo);

  if(hasMoveOrder)
  {
    /*
     * Set OrderOnArrival combo
     */

    Boolean targetToUnit = False;

    /*
     * If we are attaching on arrival and we don't already have a target, get one
     * Go through all sister units and find all who are at our dest town
     */

    if( (d_combos->getValue(CampaignComboInterface::OnArrival) == Orders::Type::Attach) &&
        (d_order.getTargetUnit() == NoCommandPosition) )
    {
        targetToUnit = True;
    }


    /*
     *  see if toUnit button is checked
     */

    {
      CustomButton cb(getHWND(), ID_ToUnit);

      if(cb.getCheck())
      {
        if(d_order.getTargetUnit() == NoCommandPosition)
        {
          targetToUnit = True;
        }
      }
    }

    /*
     * Set target to a unit(friendly) at destination town
     */

    if(targetToUnit)
    {
      ASSERT(d_order.getDestTown() != NoTown);
      ConstICommandPosition cpi = currentUnit();
      if(cpi != NoCommandPosition)
      {
        // CommandPosition* cp = d_campData->getCommand(cpi);

        /*
         * Pick the highest ranking to attach to
         */

        ConstICommandPosition bestCPI  = NoCommandPosition;
        RankEnum bestRank = Rank_Division;

        ConstUnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getFirstUnit(cpi->getSide()));
        while(iter.sister())
        {
          ConstICommandPosition sisterCP = iter.current();

          if( (sisterCP->getCloseTown() == d_order.getDestTown()) )
          {
            if (bestCPI != cpi)     // Disallow attach to self!
            {
               if((bestCPI == NoCommandPosition) || (sisterCP->getRankEnum() <= bestRank))
               {
                 bestCPI = sisterCP;
                 bestRank = sisterCP->getRankEnum();
               }
            }
          }
        }

        if(bestCPI != NoCommandPosition)
        {
          d_order.setTarget(bestCPI);
        }
      }

    }
  }

  /*
   * close date window if it is open
   */

  d_calender.show(false);   // hide();

  for(CampaignComboInterface::Type t = CampaignComboInterface::OrderPageFirst;
      t < CampaignComboInterface::OrderPageLast;
      INCREMENT(t))
  {
    d_combos->hide(t);
  }
}

void UnitOrderWindow::init(StackedUnitList& units, UnitListID unitID)
{
  ASSERT(unitID != NoUnitListID);
  ASSERT(unitID < units.unitCount());

  ConstICommandPosition cp = units.getUnit(unitID);
  if(cp->isDead())
   return;

  if(!GamePlayerControl::canControl(cp->getSide()))
   return;

  /*
   * Init unit Combo
   */

  CampaignComboData data;
  data.d_unitList = &units;
  data.d_order = &d_order;
  data.d_currentUnit = units.getUnit(unitID);;
  data.d_showAll = True;

  d_combos->init(CampaignComboInterface::UnitList, data);

  /*
   * Set check buttons
   */

  for(int id = ID_FirstCheckBox; id < (ID_LastCheckBox + 1); id++)
  {
    CustomButton cb(getHWND(), id);
    cb.setCheck(idToCheck(static_cast<ID>(id)));
  }

}

void UnitOrderWindow::run(const PixelPoint& p, UBYTE flags)
{

  ConstICommandPosition cp = currentUnit();
  if(cp->isDead())
   return;

  if(!GamePlayerControl::canControl(cp->getSide()))
   return;


  d_flags = flags;
  sizeWindow();

  /*
   * Init orders and on-arrival Combo's
   */

  CampaignComboData data;
  data.d_order = &d_order;
  data.d_currentUnit = currentUnit();

  d_combos->init(CampaignComboInterface::OrderType, data);
  d_combos->init(CampaignComboInterface::OnArrival, data);

  const int offset = (ScreenBase::dbY() * 10) / ScreenBase::baseY();

  /*
   * make sure entire window is within mainwindows rect
   */

  RECT r;
  GetWindowRect(APP::getMainHWND(), &r);

  int x = p.getX();
  int y = p.getY();

  // check right side
  if((x + d_cx) > r.right)
  {
    x -= (d_cx + offset);
  }

  // check bottom
  if((y + d_cy) > r.bottom)
  {
    y -= (d_cy + offset);
  }

  d_point.set(x, y);

  enableControls();

  int result = SetTimer(getHWND(), TimerID, 150, NULL);
  ASSERT(result);
}

void UnitOrderWindow::sizeWindow()
{
  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();

  /*
   * Set size depending on whether this is a move order
   */

  int cy = (d_flags & UnitOrder_Int::FullOrder) ? d_cy : d_moveCY;
  SetWindowPos(getHWND(), HWND_TOP, 0, 0, d_cx, cy, SWP_NOMOVE);

  /*
   * Show or hide controls as needed
   */

  for(ID id = ID_First; id < ID_Last; INCREMENT(id))
  {
    const CInfo& ci = controlInfo(id);

    HWND h = GetDlgItem(getHWND(), id);
    ASSERT(h);

    /*
     * Adjust position of bottom row buttons
     */

    if(id >= ID_BRButtonFirst && id <= ID_BRButtonLast)
    {
      int y = (d_flags & UnitOrder_Int::FullOrder) ?
        (ci.d_y * dbY) / baseY : ((ci.d_y * dbY) / baseY) - (d_cy - d_moveCY);

      SetWindowPos(h, HWND_TOP, (ci.d_x * dbX) / baseX, y, 0, 0, SWP_NOSIZE);
    }


     // bool how = ((ci.d_flags & UnitOrder_Int::FullOrder) && (d_flags & UnitOrder_Int::FullOrder)) ||
    //     ((ci.d_flags & UnitOrder_Int::MoveOrder) && (d_flags & UnitOrder_Int::MoveOrder));

    int how;
    if( ((ci.d_flags & UnitOrder_Int::FullOrder) && (d_flags & UnitOrder_Int::FullOrder)) ||
        ((ci.d_flags & UnitOrder_Int::MoveOrder) && (d_flags & UnitOrder_Int::MoveOrder)) )
    {
      how = SW_SHOW;
    }
    else
      how = SW_HIDE;

    ShowWindow(h, how);
     // show(how);
  }
}

// void UnitOrderWindow::destroy()
// {
//   DestroyWindow(getHWND());
// }
//
// void UnitOrderWindow::show(bool visible)
// {
//   WindowBaseND::show(visible);
//   d_calender->show(visible);
//   d_sOrderWind->show(visible);
//
//   // ShowWindow(getHWND(), SW_HIDE);
//   // d_calender.hide();
//   // d_sOrderWind->hide();
// }

ConstICommandPosition UnitOrderWindow::currentUnit()
{
  ASSERT(d_combos->created(CampaignComboInterface::UnitList));
  return lparamToCP(d_combos->getValue(CampaignComboInterface::UnitList));
}

/*--------------------------------------------------------
 *  Client access
 */

UnitOrder_Int::UnitOrder_Int(CampaignUserInterface* owner, HWND hParent, CampaignWindowsInterface* campWind, const CampaignData* campData, CampaignOrder& order) :
  d_orderWind(new UnitOrderWindow(owner, hParent, campWind, campData, order))
{
  ASSERT(d_orderWind);
}

UnitOrder_Int::~UnitOrder_Int()
{
  // destroy();
  delete d_orderWind;
}

void UnitOrder_Int::init(StackedUnitList& units, int id)
{
  if(d_orderWind)
    d_orderWind->init(units, static_cast<UnitListID>(id));
}

void UnitOrder_Int::run(const PixelPoint& p, UBYTE flags)
{
  if(d_orderWind)
    d_orderWind->run(p, flags);
}

// void UnitOrder_Int::hide()
// {
//   if(d_orderWind)
//     d_orderWind->hide();
// }
//
// void UnitOrder_Int::destroy()
// {
//   if(d_orderWind)
//   {
//     d_orderWind->destroy();
//     d_orderWind = 0;
//   }
// }
//
// HWND UnitOrder_Int::getHWND() const
// {
//   return d_orderWind->getHWND();
// }

int UnitOrder_Int::width() const
{
  return d_orderWind->width();
}

int UnitOrder_Int::moveHeight() const
{
  return d_orderWind->moveHeight();
}

int UnitOrder_Int::fullHeight() const
{
  return d_orderWind->fullHeight();
}

ConstICommandPosition UnitOrder_Int::currentUnit()
{
  return d_orderWind->currentUnit();
}


HWND UnitOrder_Int::getHWND() const
{
    return d_orderWind->getHWND();
}

bool UnitOrder_Int::isVisible() const
{
  return d_orderWind->isVisible();
}

bool UnitOrder_Int::isEnabled() const
{
  return d_orderWind->isEnabled();
}

void UnitOrder_Int::show(bool visible)
{
  d_orderWind->show(visible);
}

void UnitOrder_Int::enable(bool enable)
{
  d_orderWind->enable(enable);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
