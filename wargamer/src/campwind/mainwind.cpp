/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Main Window for Wargamer Campaign Game
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mainwind.hpp"
#include "cwin_int.hpp"
#include "app.hpp"
#include "gamectrl.hpp"
// #include "windsave.hpp"
#include "palette.hpp"
#include "tooltip.hpp"
#include "resdef.h"        // Resource Definitions
#include "help.h"
#include "todolog.hpp"
#include "scenario.hpp"
#include "fonts.hpp"
#include "options.hpp"
#include "wind.hpp"
#include "palwind.hpp"
#include "cmenu.hpp"
#include "scrnbase.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "scn_img.hpp"
#include "wmisc.hpp"
// #include "cdialog.hpp"

#include "cbutton.hpp"
// #include "generic.hpp"
//#include "cdialog.hpp"
#ifdef DEBUG
#include "msgenum.hpp"
#include "logwin.hpp"
// #define DEBUG_MAINWIND_MESSAGES
#endif

/*
 * This is not in Windowsx.h
 */

#define HANDLE_WM_EXITMENULOOP(hwnd, wParam, lParam, fn) \
    (LRESULT)(DWORD)(int)(fn)((hwnd), (BOOL)(wParam))

class MainWindow_Imp : public SubClassWindowBase, public PaletteWindow {
   HWND d_hMain;
   CampaignWindowsInterface* d_campWind;

   CustomMenu d_menu;

   enum Flags {
     MenuHidden = 0x01
   };

   UBYTE d_flags;

#ifdef DEBUG
   bool d_subClassed;
#endif

 public:
   MainWindow_Imp(HWND hMain, CampaignWindowsInterface* campWind, int nCmdShow);
   ~MainWindow_Imp();

   HWND getHWND() const { return d_hMain; }

   void checkMenu(int id, bool state);
   void enableMenu(int id, bool flag);
   void toggleMenu();

   void optionsUpdated();
      // Implements virtual OptionManager::optionsUpdated
      // Sets up the check state of the menus properly

   int menuHeight() const { return d_menu.height(); }
//   void showMenu();

    void enable();      // setup subclass
    void disable();     // remove subclass

   CustomMenu* getMenu() { return &d_menu; }

private:
   LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
// void onPaint(HWND hWnd);
   void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
   void onSize(HWND hwnd, UINT state, int cx, int cy);
   BOOL onQueryNewPalette(HWND hwnd);
    // LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
   // void onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild);
    bool onUserWindowDestroyed(HWND hwnd, HWND hChild, int idChild);
   void onClose(HWND hWnd);
   // void onTimer(HWND hwnd, UINT id);
   void onMenuSelect(HWND hwnd, HMENU hmenu, int item, HMENU hmenuPopup, UINT flags);

   void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
   BOOL onHelp(HWND hwnd, LPHELPINFO lParam);

   void init();
   void showMenu();
   void setCaption();

#ifdef DEBUG
   void checkPalette() { }
#endif
};


MainWindow_Imp::MainWindow_Imp(HWND hMain, CampaignWindowsInterface* campWind, int nCmdShow) :
   SubClassWindowBase(hMain),
#ifdef DEBUG
   d_subClassed(true),
#endif
   d_hMain(hMain),
   d_campWind(campWind),
   d_flags(0)
{
  ASSERT(hMain != 0);
  ASSERT(campWind != 0);

   setCaption();

  /*
   * Alter class
   */

  // for now
  int result = SetMenu(hMain, NULL);
  ASSERT(result);

#if 0
  result = SetWindowLong(hMain, GWL_STYLE,
     WS_CAPTION | WS_MAXIMIZE | WS_THICKFRAME | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE);

  ASSERT(result);
#endif

  /*
   * Set menu data
   */

  CustomMenuData md;
  md.d_type = CustomMenuData::CMT_BarMenu;
  md.d_menuID = MENU_START;
  md.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
  md.d_checkImages = ScenarioImageLibrary::get(ScenarioImageLibrary::CustomMenuImages);
  md.d_hCommand = hMain;
  md.d_hParent = hMain;
  md.d_borderColors = scenario->getBorderColors();
  scenario->getColour("MenuText", md.d_color);
  scenario->getColour("MenuHilite", md.d_hColor);

  /*
   * Set menu fonts (one normal, the other underlined
   */

  LogFont lf;
  lf.height((8 * ScreenBase::dbY()) / ScreenBase::baseY());
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);
  md.d_hFont = font;

  lf.underline(true);
  font.set(lf);
  md.d_hULFont = font;

  /*
   * initialize menu
   */

  d_menu.init(md);
  showMenu();

#ifdef DEBUG
   DWORD id = GetCurrentThreadId();
   ASSERT(id == APP::getInterfaceThreadID());
#endif
}


MainWindow_Imp::~MainWindow_Imp()
{
#ifdef DEBUG
   ASSERT(d_subClassed);
#endif

  SubClassWindowBase::clear(getHWND());

}

void MainWindow_Imp::init()
{
}

void MainWindow_Imp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
   ASSERT(d_campWind != 0);

#ifdef DEBUG
   DWORD threadID = GetCurrentThreadId();
   DWORD threadID2 = GetWindowThreadProcessId(hWnd, NULL);
   ASSERT(threadID == threadID2);
   ASSERT(threadID == APP::getInterfaceThreadID());
#endif

   if(!d_campWind->processCommand(hWnd, id))
    {
        FORWARD_WM_COMMAND(hWnd, id, hwndCtl, codeNotify, defProc);
    }
}

void MainWindow_Imp::onMenuSelect(HWND hwnd, HMENU hmenu, int item, HMENU hmenuPopup, UINT flags)
{
   // d_campWind->showHint(item);

   /*
    * SDK says that flags is 0xFFFF and not to use (UINT)-1,
    * but in actual fact it is 0xFFFFFFFF (message cracker explicitly sets it!)
    * So to make it work in both cases, I cast flags to UWORD so that
    * only the lower 16 bits are tested.
    */


   if((hmenuPopup == NULL) && ( ((UWORD)flags) == 0xffff))
      g_toolTip.clearHint();
   else
      g_toolTip.showHint(APP::instance(), item);

}


void MainWindow_Imp::toggleMenu()
{
  RECT r;
  GetWindowRect(getHWND(), &r);

  if(d_flags & MenuHidden)
  {
    showMenu();
    d_flags &= ~MenuHidden;
  }
  else
  {
    d_menu.hide();
    d_flags |= MenuHidden;
  }

  SetWindowPos(getHWND(), HWND_TOP, r.left, r.top, r.right-r.left, r.bottom-r.top, SWP_FRAMECHANGED);
}

void MainWindow_Imp::showMenu()
{
  PixelPoint p(0, 0);
  d_menu.run(p);
}


void MainWindow_Imp::enable()
{
   ASSERT(!d_subClassed);
#ifdef DEBUG
   d_subClassed = true;
#endif
   SubClassWindowBase::init(getHWND());
   setCaption();
   SendMessage(getHWND(), WM_NCPAINT, 0, 0);

}

void MainWindow_Imp::disable()
{
   ASSERT(d_subClassed);
#ifdef DEBUG
   d_subClassed = false;
#endif
   SubClassWindowBase::clear(getHWND());
}

/*
 * Main Window is responsible for the tool bar.
 * Everything else is looked after by CampaignWindows
 *
 * the status window is an odd case, because originally it
 * used up the entire width, but now that it is more of a
 * rectangle, it is not possible to have a nice rectangle
 * left over for all the other windows.  Also seeing as
 * it as the Message Window Icon in it, I would say the status
 * window really belongs with the campaign windows.
 */

void MainWindow_Imp::onSize(HWND hwnd, UINT state, int cx, int cy)
{
   if((cx == 0) || (cy == 0) || (state == SIZE_MINIMIZED))
      return;

   // GameControl::pause((state == SIZE_MINIMIZED) ? True : False);
   // setSysButtonPos();
   // SendMessage(hwnd, WM_NCPAINT, 0, 0);

   d_menu.run(PixelPoint(0,0));
   d_campWind->positionWindows();

   // Forward on so wg_main can deal with the Caption Bar

   FORWARD_WM_SIZE(hwnd, state, cx, cy, defProc);
}


void MainWindow_Imp::onClose(HWND hWnd)
{
   if(!d_campWind->onClose(hWnd))
      FORWARD_WM_CLOSE(hWnd, defProc);
}

/*
 * Handle messages from Children
 */

#if 0
void MainWindow_Imp::onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild)
{
#ifdef DEBUG_MAINWIND_MESSAGES
   debugLog("MainWindow_Imp::ParentNotify from %p (%d)\n", hwndChild, idChild);
#endif

   if(msg == WM_CLOSE)     // WM_DESTROY
   {
     d_campWind->windowClosed(hwndChild);
   }

   if(msg == WM_DESTROY)
   {
     d_campWind->windowDestroyed(hwndChild);
   }

}
#endif

bool MainWindow_Imp::onUserWindowDestroyed(HWND hwnd, HWND hChild, int idChild)
{
    return d_campWind->windowDestroyed(hChild, idChild);
}


#if 0    // moved to wg_main
void MainWindow_Imp::onTimer(HWND hwnd, UINT id)
{
   GameControl::onTimer(); // gApp.runGame();
}
#endif

/*
 * Message Handler
 */

LRESULT MainWindow_Imp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MAINWIND_MESSAGES
   debugLog("MainWindow_Imp::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   LRESULT l;
   if(handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_COMMAND,        onCommand);
      HANDLE_MSG(hWnd, WM_SIZE,           onSize);
      // HANDLE_MSG(hWnd, WM_PARENTNOTIFY,   onParentNotify);
        HANDLE_MSG(hWnd, WM_USER_WINDOWDESTROYED, onUserWindowDestroyed);
      HANDLE_MSG(hWnd, WM_CLOSE,          onClose);
      // HANDLE_MSG(hWnd, WM_TIMER,          onTimer);
      HANDLE_MSG(hWnd, WM_MENUSELECT,     onMenuSelect);
      HANDLE_MSG(hWnd, WM_HELP,     onHelp);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL MainWindow_Imp::onHelp(HWND hwnd, LPHELPINFO lparam)
{
  LPHELPINFO lphi = (LPHELPINFO)lparam;

  if (lphi->iContextType == HELPINFO_WINDOW)   // must be for a control
  {
//  WinHelp ((HWND)lphi->hItemHandle, "help\\Wg95Help.hlp", HELP_CONTEXT, IDH_Wargamer);
  }
  return TRUE;

}

void MainWindow_Imp::checkMenu(int id, bool flag)
{
   ASSERT((id >= IDM_FIRST) && (id <= IDM_LAST));

#ifdef DEBUG_MAINWIND_MESSAGES
   debugLog("checkMenu(%d, %s)\n", id, flag ? "TRUE" : "FALSE");
#endif

   d_menu.checked(id, flag);
}

void MainWindow_Imp::enableMenu(int id, bool flag)
{
   ASSERT((flag == TRUE) || (flag == FALSE));
   ASSERT((id >= IDM_FIRST) && (id <= IDM_LAST));

#ifdef DEBUG_MAINWIND_MESSAGES
   debugLog("enableMenu(%d, %s)\n", id, flag ? "TRUE" : "FALSE");
#endif
   d_menu.enable(id, flag);
}

 /*
  * Implements virtual OptionManager::optionsUpdated
  * Sets up the check state of the menus properly
  */

void MainWindow_Imp::optionsUpdated()
{
#ifdef DEBUG
   d_menu.checked(IDM_INSTANTMOVE, Options::get(OPT_InstantMove));
#endif
   d_menu.checked(IDM_INSTANTORDERS,   CampaignOptions::get(OPT_InstantOrders));
   d_menu.checked(IDM_COLORCURSOR, Options::get(OPT_ColorCursor));
}

void MainWindow_Imp::setCaption()
{
   ResString title(IDS_Caption);
   SetWindowText(getHWND(), title.c_str());

}

/*---------------------------------------------------------------
 * Client Access
 */

MainCampaignWindow::MainCampaignWindow(HWND hMain, CampaignWindowsInterface* campWind, int nCmdShow) :
  d_mainWind(new MainWindow_Imp(hMain, campWind, nCmdShow))
{
  ASSERT(d_mainWind);
}

MainCampaignWindow::~MainCampaignWindow()
{
  if(d_mainWind)
    delete d_mainWind;
}

HWND MainCampaignWindow::getHWND() const
{
  return d_mainWind->getHWND();
}

void MainCampaignWindow::checkMenu(int id, bool state)
{
  d_mainWind->checkMenu(id, state);
}

void MainCampaignWindow::enableMenu(int id, bool flag)
{
  d_mainWind->enableMenu(id, flag);
}

void MainCampaignWindow::toggleMenu()
{
  d_mainWind->toggleMenu();
}

void MainCampaignWindow::optionsUpdated()
{
  d_mainWind->optionsUpdated();
}

int MainCampaignWindow::menuHeight() const
{
  return d_mainWind->menuHeight();
}

void MainCampaignWindow::enable()
{
  d_mainWind->enable();
}

void MainCampaignWindow::disable()
{
  d_mainWind->disable();
}

CustomMenu* MainCampaignWindow::getMenu()
{
   return d_mainWind->getMenu();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
