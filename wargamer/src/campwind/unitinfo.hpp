/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef UNITINFO_HPP
#define UNITINFO_HPP

#ifndef __cplusplus
#error unitinfo.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Unit Information Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "infobase.hpp"
#include "cpdef.hpp"
#include "grtypes.hpp"

class DrawDIBDC;
class CampaignData;

struct UnitInfoData : private InfoDrawBase
{
	public:
#if 0
		UnitInfoData(const ConstICommandPosition& cpi) :
			d_cpi(cpi),
			d_campData(0),
			d_dib(0),
			d_limitedInfo(false)
		{
			SetRect(&d_dRect, 0, 0, 0, 0);
			init();
		}
#endif

		UnitInfoData(const ConstICommandPosition& cpi, const CampaignData* campData, DrawDIBDC* dib, const PixelRect& r) :
			InfoDrawBase(dib, campData, r),
			d_cpi(cpi),
			d_limitedInfo(false)
		{
			init();
		}


		void drawTrackingDib();
		void drawSummaryDib();
		void drawLeaderDib();
		void drawOBDib();
		void drawActivityDib();
		void drawOrdersDib();

	private:
		enum CompactMode
		{
			Compact=true,
			Normal=false 
		};


		void init();
		void drawNameDib(const PixelRect& rect, bool forLeader, CompactMode compact = Normal);


	private:
		ConstICommandPosition d_cpi;

		bool d_limitedInfo;

		int d_offsetX;
		int d_offsetY;
		PixelRect d_nameRect;
		PixelRect d_mainRect;
};

#endif /* UNITINFO_HPP */

