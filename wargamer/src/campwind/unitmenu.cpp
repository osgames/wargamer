/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "unitmenu.hpp"
#include "wind.hpp"
#include "scenario.hpp"
// #include "generic.hpp"
#include "app.hpp"
#include "fonts.hpp"
#include "resdef.h"
#include "wmisc.hpp"
#include "cmenu.hpp"
#include "scn_img.hpp"
#include "userint.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "compos.hpp"
#include "realord.hpp"
#include "campord.hpp"
#include "scrnbase.hpp"
#include "armyutil.hpp"
#include "camp_snd.hpp"
#include "control.hpp"

/*----------------------------------------------------------
 * Unit Menu Implementation
 */

class UnitMenu_Imp : public WindowBaseND {
   CampaignUserInterface* d_owner;
   HWND d_hParent;
   CampaignWindowsInterface* d_campWind;
   const CampaignData* d_campData;
   CustomMenu d_menu;

   CampaignOrder& d_order;
   ConstICommandPosition d_cpi;
   ITown d_itown;

   PixelPoint d_point;

   public:
   UnitMenu_Imp(CampaignUserInterface* owner, HWND hParent, CampaignWindowsInterface* campWind,
      const CampaignData* campData, CampaignOrder& order);
   ~UnitMenu_Imp();

   void run(ConstParamCP cpi, ITown itown, const PixelPoint& p);

   // void destroy()
   // {
   //   d_menu.destroy();
   //   DestroyWindow(getHWND());
   // }

   private:
   LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
   void onDestroy(HWND hWnd)
   {
   }
   void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);
   void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);

   void showInfo();
   void showOrders();
   void showOB();
   void updateMenu();
   //  Boolean canGiveOnArrival(Orders::AdvancedOrders::OrderOnArrival o);
   Boolean idToOption(int id);
   void setOrder(int id);
   void setAggression(int id);
   void setOption(int id);
   void setMoveHow(int id);
   void setPosture(int id);
   void setOnArrival(int id);
};

UnitMenu_Imp::UnitMenu_Imp(CampaignUserInterface* owner, HWND hParent,
   CampaignWindowsInterface* campWind, const CampaignData* campData, CampaignOrder& order) :
  d_owner(owner),
  d_hParent(hParent),
  d_campWind(campWind),
  d_campData(campData),
  d_cpi(NoCommandPosition),
  d_itown(NoTown),
  d_order(order)
{
   HWND hWnd = createWindow(0,
      // GenericClass::className(),
      NULL,
      WS_POPUP,
      0, 0, 0, 0,
      hParent, NULL   // , APP::instance()
      );

   ASSERT(hWnd != NULL);
}

UnitMenu_Imp::~UnitMenu_Imp()
{
   d_menu.destroy();
   selfDestruct();
}

LRESULT UnitMenu_Imp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
   HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
   HANDLE_MSG(hWnd, WM_COMMAND,  onCommand);
   HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL UnitMenu_Imp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
   const LONG dbY = ScreenBase::dbY();
   const LONG baseY = ScreenBase::baseY();

   /*
   * Set menu data
   *
   * Should build the menu depending on whether we are given a unit / town
   */

   CustomMenuData md;
   md.d_type = CustomMenuData::CMT_Popup;
   md.d_menuID = MENU_UNITORDERPOPUP;
   md.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
   md.d_checkImages = ScenarioImageLibrary::get(ScenarioImageLibrary::CustomMenuImages);
   md.d_hCommand = hWnd;
   md.d_hParent = d_hParent;
   md.d_borderColors = scenario->getBorderColors();
   scenario->getColour("MapInsert", md.d_color);
   scenario->getColour("Orange", md.d_hColor);

   /*
   * Set menu fonts (one normal, the other underlined
   */

   LogFont lf;
   lf.height((8 * dbY) / baseY);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));

   Font font;
   font.set(lf);
   md.d_hFont = font;

   lf.underline(true);
   font.set(lf);
   md.d_hULFont = font;

   /*
   * initialize menu
   */

   d_menu.init(md);

   return TRUE;
}

void UnitMenu_Imp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
   switch(id)
   {
   case IDM_UTB_INFO:
      showInfo();
      break;

   case IDM_UTB_ORDERSWIN:
      showOrders();
      break;

   case IDM_UTB_REORG:
      showOB();
      break;

   case IDM_UTB_HOLD:
   case IDM_UTB_RESTRALLY:
   case IDM_UTB_DETACHLEADER:
   case IDM_UTB_GARRISON:
   case IDM_UTB_INSTALLSUPPLY:
   case IDM_UTB_UPGRADEFORT:
      setOrder(id);
      //    d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;

   case IDM_UTB_TIMID:
   case IDM_UTB_DEFEND:
   case IDM_UTB_ATTACKSMALL:
   case IDM_UTB_ATTACK:
      setAggression(id);
      //    d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;

   case IDM_UTB_MARCHTO:
   case IDM_UTB_PURSUE:
   case IDM_UTB_SIEGEACTIVE:
   case IDM_UTB_AUTOSTORM:
   case IDM_UTB_DROPOFFGARRISON:
      setOption(id);
      //    d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;

   case IDM_UTB_EASYMARCH:
   case IDM_UTB_NORMALMARCH:
   case IDM_UTB_FORCEMARCH:
      setMoveHow(id);
      //    d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;

   case IDM_UTB_HOLD_OA:
   case IDM_UTB_RESTRALLY_OA:
   case IDM_UTB_GARRISON_OA:
   case IDM_UTB_ATTACH_OA:
      setOnArrival(id);
      //    d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;

   case IDM_UTB_OFFENSIVE:
   case IDM_UTB_DEFENSIVE:
      setPosture(id);
      //    d_campData->soundSystem()->TriggerSound(SOUNDTYPE_MENUSELECT, CAMPAIGNSOUNDPRIORITY_INSTANT);
      break;

   case IDM_TM_INFO:
      // d_owner->runInfo(d_point);
      d_owner->infoTown(d_itown);
      break;

   case IDM_TM_BUILDS:
      // d_owner->runOrders(d_point);
      d_owner->orderTown(d_itown);
      break;

   }

   d_campData->soundSystem()->TriggerSound(SOUNDTYPE_WINDOWCLOSE, CAMPAIGNSOUNDPRIORITY_INSTANT);
   d_menu.hide();
}

void UnitMenu_Imp::setOrder(int id)
{
   Orders::Type::Value order;
   if(id == IDM_UTB_HOLD)
      order = Orders::Type::Hold;
   else if(id == IDM_UTB_RESTRALLY)
      order = Orders::Type::RestRally;
   else if(id == IDM_UTB_DETACHLEADER)
      order = Orders::Type::Leader;
   else if(id == IDM_UTB_GARRISON)
      order = Orders::Type::Garrison;
   else if(id == IDM_UTB_INSTALLSUPPLY)
      order = Orders::Type::InstallSupply;
   else
   {
      ASSERT(id == IDM_UTB_UPGRADEFORT);
      order = Orders::Type::UpgradeFort;
   }

   d_order.setType(order);
   d_owner->sendOrder(d_cpi);
}

void UnitMenu_Imp::setAggression(int id)
{
   Orders::Aggression::Value aggress;
   if(id == IDM_UTB_TIMID)
      aggress = Orders::Aggression::Timid;
   else if(id == IDM_UTB_DEFEND)
      aggress = Orders::Aggression::DefendSmall;
   else if(id == IDM_UTB_ATTACKSMALL)
      aggress = Orders::Aggression::AttackSmall;
   else
   {
      ASSERT(id == IDM_UTB_ATTACK);
      aggress = Orders::Aggression::Attack;
   }

   d_order.setAggressLevel(aggress);
   d_owner->sendOrder(d_cpi);
}

void UnitMenu_Imp::setOption(int id)
{
   if(id == IDM_UTB_MARCHTO)
      d_order.setSoundGuns(!d_order.getSoundGuns());
   else if(id == IDM_UTB_PURSUE)
      d_order.setPursue(!d_order.getPursue());
   else if(id == IDM_UTB_SIEGEACTIVE)
      d_order.setSiegeActive(!d_order.getSiegeActive());
   else if(id == IDM_UTB_AUTOSTORM)
   {
      ;
   }
   else if(id == IDM_UTB_DROPOFFGARRISON)
   {
      ;
   }

   d_owner->sendOrder(d_cpi);
}

void UnitMenu_Imp::setMoveHow(int id)
{
   Orders::AdvancedOrders::MoveHow mh;

   if(id == IDM_UTB_EASYMARCH)
      mh = Orders::AdvancedOrders::Easy;
   else if(id == IDM_UTB_NORMALMARCH)
      mh = Orders::AdvancedOrders::Normal;
   else
   {
      ASSERT(id == IDM_UTB_FORCEMARCH);
      mh = Orders::AdvancedOrders::Force;
   }

   d_order.setMoveHow(mh);
   d_owner->sendOrder(d_cpi);
}

void UnitMenu_Imp::setPosture(int id)
{
   Orders::Posture::Type p = (id == IDM_UTB_OFFENSIVE) ?
      Orders::Posture::Offensive : Orders::Posture::Defensive;

   d_order.setPosture(p);
   d_owner->sendOrder(d_cpi);
}

void UnitMenu_Imp::setOnArrival(int id)
{
   Orders::Type::Value oa;

   if(id == IDM_UTB_HOLD_OA)
      oa = Orders::Type::Hold;
   else if(id == IDM_UTB_RESTRALLY_OA)
      oa = Orders::Type::RestRally;
   else if(id == IDM_UTB_GARRISON_OA)
      oa = Orders::Type::Garrison;
   else
   {
      ASSERT(id == IDM_UTB_ATTACH_OA);
      oa = Orders::Type::Attach;
   }

   d_order.setOrderOnArrival(oa);
   d_owner->sendOrder(d_cpi);
}

void UnitMenu_Imp::showInfo()
{
   // d_owner->runInfo(d_point);
   d_owner->infoUnit(d_cpi);
}

void UnitMenu_Imp::showOrders()
{
   // d_owner->runOrders(d_point);
   d_owner->orderUnit(d_cpi);
}

void UnitMenu_Imp::showOB()
{
   //  d_owner->runOB(d_point);
   d_owner->reorgOB(d_cpi);
}

void UnitMenu_Imp::run(ConstParamCP cpi, ITown itown, const PixelPoint& pt)
{
   d_cpi = cpi;
   d_point = pt;
   d_itown = itown;

   updateMenu();
   d_menu.run(pt);
}

/*
 * Set enabled, and checked state
 */

int orderTypeToID(Orders::Type::Value t)
{
   if(t == Orders::Type::MoveTo)
      return IDM_UTB_MOVETO;
   else if(t == Orders::Type::Hold)
      return IDM_UTB_HOLD;
   else if(t == Orders::Type::RestRally)
      return IDM_UTB_RESTRALLY;
   else if(t == Orders::Type::Garrison)
      return IDM_UTB_GARRISON;
   else if(t == Orders::Type::Leader)
      return IDM_UTB_DETACHLEADER;
   else if(t == Orders::Type::InstallSupply)
      return IDM_UTB_INSTALLSUPPLY;
   else if(t == Orders::Type::UpgradeFort)
      return IDM_UTB_UPGRADEFORT;

   return -1;
}

int moveHowToID(Orders::AdvancedOrders::MoveHow t)
{
   if(t == Orders::AdvancedOrders::Easy)
      return IDM_UTB_EASYMARCH;
   else if(t == Orders::AdvancedOrders::Normal)
      return IDM_UTB_NORMALMARCH;
   else if(t == Orders::AdvancedOrders::Force)
      return IDM_UTB_FORCEMARCH;

   return -1;
}

int onArrivalToID(Orders::Type::Value t)
{
   if(t == Orders::Type::Hold)
      return IDM_UTB_HOLD_OA;
   else if(t == Orders::Type::RestRally)
      return IDM_UTB_RESTRALLY_OA;
   else if(t == Orders::Type::Attach)
      return IDM_UTB_ATTACH_OA;
   else if(t == Orders::Type::Garrison)
      return IDM_UTB_GARRISON_OA;

   return -1;
}

int postureToID(Orders::Posture::Type t)
{
   if(t == Orders::Posture::Offensive)
      return IDM_UTB_OFFENSIVE;
   else if(t == Orders::Posture::Defensive)
      return IDM_UTB_DEFENSIVE;

   return -1;
}

int aggressToID(Orders::Aggression::Value t)
{
   if(t == Orders::Aggression::Timid)
      return IDM_UTB_TIMID;
   else if(t == Orders::Aggression::DefendSmall)
      return IDM_UTB_DEFEND;
   else if(t == Orders::Aggression::AttackSmall)
      return IDM_UTB_ATTACKSMALL;
   else if(t == Orders::Aggression::Attack)
      return IDM_UTB_ATTACK;

   return -1;
}

Boolean UnitMenu_Imp::idToOption(int id)
{
   ASSERT(id >= IDM_UTB_MARCHTO && id <= IDM_UTB_DROPOFFGARRISON);

   if(id == IDM_UTB_MARCHTO)
      return d_order.getSoundGuns();
   else if(id == IDM_UTB_PURSUE)
      return d_order.getPursue();
   else if(id == IDM_UTB_SIEGEACTIVE)
      return d_order.getSiegeActive();
   else if(id == IDM_UTB_AUTOSTORM)
      return False;
   else if(id == IDM_UTB_DROPOFFGARRISON)
      return False;

   return False;
}

#if 0
Boolean UnitMenu_Imp::canGiveOnArrival(Orders::AdvancedOrders::OrderOnArrival o)
{
   /*
   * Hold and Rest\Rally we can always give, test only for Garrison, Attach
   */

   if(o == Orders::AdvancedOrders::Garrison)
   {
      /*
      * To Give a Garrison on Arrival order
      * order must have a destination town that has fortifications
      */

      if(d_order.hasDestTown())
      {
         Town& t = d_campData->getTown(d_order.getDestTown());

         return (t.getFortifications() > 0);
      }
      else
         return False;
   }
   else if(o == Orders::AdvancedOrders::Attach)
   {
      /*
      * If we have a target unit we can attach
      */

      if(d_order.hasTargetUnit())
         return True;

      /*
      * Otherwise, if we have a destination town we can
      * attach if town has a friendly unit
      */

      if(d_order.hasDestTown())
      {
         if(d_cpi != NoCommandPosition)
         {
            UnitIter iter(&d_campData->getArmies(), d_campData->getArmies().getFirstUnit(d_cpi->getSide()));
            while(iter.sister())
            {
               CommandPosition* sisterCP = iter.currentCommand();

               if((sisterCP->atTown()) && (sisterCP->getTown() == d_order.getDestTown()))
               {
                  return True;
               }
            }
         }
      }
      return False;
   }

   return True;
}
#endif

void UnitMenu_Imp::updateMenu()
{
   // ASSERT(d_cpi != NoCommandPosition);

   /*
   * if this is not a players unit allow only info to be accessed
   */

   Boolean canOrder = (d_cpi != NoCommandPosition) && CampaignArmy_Util::isUnitOrderable(d_cpi);

   d_menu.enable(IDM_UTB_ORDERSWIN, canOrder);
   d_menu.enable(IDM_UTB_INFO, d_cpi != NoCommandPosition);
   d_menu.enable(IDM_UTB_REORG, canOrder);

   d_menu.enable(6, canOrder, True);    // submenu

   /*
   * Disable any non-allowed orders
   */

   if(canOrder)
   {
      for(Orders::Type::Value t = Orders::Type::First; t < Orders::Type::HowMany; INCREMENT(t))
      {
         if(Orders::Type::canGiveToUnit(t))
         {
            Boolean add = False;

            if(d_order.getType() == Orders::Type::MoveTo)
            {
               add = CampaignOrderUtil::allowed(t, d_cpi, d_campData);
            }
            else
            {
               if(t != Orders::Type::MoveTo)
               {
                  add = CampaignOrderUtil::allowed(t, d_cpi, d_campData);
               }
            }

            int id = orderTypeToID(t);
            if(id != -1)
            {
               d_menu.enable(id, add);
               d_menu.checked(id, (t == d_order.getType()));
            }
         }
      }

      /*
      * Set advanced options
      */

      for(int id = IDM_UTB_MARCHTO; id < IDM_UTB_DROPOFFGARRISON + 1; id++)
      {
         Boolean checked = idToOption(id) && (d_cpi != NoCommandPosition);
         d_menu.enable(id, true);
         d_menu.checked(id, checked);
      }

      /*
      * enable move how
      */

      Boolean isMoveOrder = (d_order.getType() == Orders::Type::MoveTo);

      const int moveHowID = 21;   // 19;
      d_menu.enable(moveHowID, isMoveOrder, True);

      const int onArrivalID = 25; // 23;
      d_menu.enable(onArrivalID, isMoveOrder, True);

      if(isMoveOrder)
      {
         /*
         * set checks for movehow
         */

         for(int i = 0; i < Orders::AdvancedOrders::MoveHow_HowMany; i++)
         {
            int id = moveHowToID(static_cast<Orders::AdvancedOrders::MoveHow>(i));
            if(id != -1)
            {
               d_menu.checked(id, (i == d_order.getMoveHow()));
            }
         }

         /*
         * set checks on arrival
         */

         for(Orders::Type::Value o = Orders::Type::First;
            o < Orders::Type::HowMany; INCREMENT(o))
         {
            int id = onArrivalToID(o);

            if(id != -1)
            {
               Boolean canGive = CampaignOrderUtil::canGiveOnArrival(d_campData, d_cpi, d_order, o);

               d_menu.enable(id, canGive);
               d_menu.checked(id, (o == d_order.getOrderOnArrival()));
            }
         }
      }

      /*
      * Aggression
      */

      for(Orders::Aggression::Value v = Orders::Aggression::First;
         v < Orders::Aggression::HowMany; INCREMENT(v))
      {
         int id = aggressToID(v);
         if(id != -1)
         {
            d_menu.checked(id, (v == d_order.getAggressLevel()));
         }
      }

      /*
      * Posture
      */

      for(Orders::Posture::Type p = Orders::Posture::First;
         p < Orders::Posture::HowMany; INCREMENT(p))
      {
         int id = postureToID(p);
         if(id != -1)
         {
            d_menu.checked(id, (p == d_order.posture()));
         }
      }
   }

   bool canBuild = false;
   if(d_itown != NoTown)
   {
      const Town& town = d_campData->getTown(d_itown);

      if(town.getIsCapital())
      {
         const Province& prov = d_campData->getProvince(town.getProvince());
         Side side = prov.getSide();
         // Side side = town.getSide();
         if(side != SIDE_Neutral)
            canBuild = (GamePlayerControl::getControl(side) == GamePlayerControl::Player);
      }
   }

   d_menu.enable(IDM_TM_BUILDS, canBuild);
   d_menu.enable(IDM_TM_INFO, d_itown != NoTown);
}

/*----------------------------------------------------------
 * Unit Menu Interface
 */

UnitMenu_Int::UnitMenu_Int(CampaignUserInterface* owner, HWND hParent,
     CampaignWindowsInterface* campWind, const CampaignData* campData, CampaignOrder& order) :
  d_unitMenu(new UnitMenu_Imp(owner, hParent, campWind, campData, order))
{
   ASSERT(d_unitMenu);
}

UnitMenu_Int::~UnitMenu_Int()
{
   delete d_unitMenu;
   // if(d_unitMenu)
   //   d_unitMenu->destroy();
}

void UnitMenu_Int::runMenu(ConstParamCP cpi, ITown itown, const PixelPoint& p)
{
   if(d_unitMenu)
      d_unitMenu->run(cpi, itown, p);
}

// void UnitMenu_Int::destroy()
// {
//   if(d_unitMenu)
//   {
//     d_unitMenu->destroy();
//     d_unitMenu = 0;
//   }
// }



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
