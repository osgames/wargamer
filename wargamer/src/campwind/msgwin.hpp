/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef MSGWIN_HPP
#define MSGWIN_HPP

#ifndef __cplusplus
#error msgwin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Messages from Unit's Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "wind.hpp"
#include <windef.h>
#include <stdarg.h>

class CampaignWindowsInterface;
class CampaignData;
class CampaignMessageInfo;
class RealMessageWindow;

class MessageWindow : public Window
{
  RealMessageWindow* d_msgWindow;
  public:
    MessageWindow(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND hwnd);
    ~MessageWindow();

    void addMessage(const CampaignMessageInfo& msg);
    void clear();
    int getNumMessages() const;
    int getMessagesRead() const;

    // void toggle();
    // void show();
    // void hide();
    // void destroy();
    // Boolean isShowing() const;
     // 
    // void suspend(bool visible);

       virtual HWND getHWND() const;
        virtual bool isVisible() const;
        virtual bool isEnabled() const;
        virtual void show(bool visible);
        virtual void enable(bool enable);


    static MessageWindow* make(CampaignWindowsInterface* campWind, const CampaignData* campData, HWND hwnd);

};


#endif /* MSGWIN_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
