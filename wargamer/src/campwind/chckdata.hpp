/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CHCKDATA_HPP
#define CHCKDATA_HPP

#ifdef CUSTOMIZE

#include "mytypes.h"
#include <windef.h>
#include "cwdll.h"

class CampaignData;
class CampaignWindowsInterface;

class CAMPWIND_DLL CampaignDataSanityCheck {
 public:
	static Boolean run(CampaignData* campData, CampaignWindowsInterface* cwi);
};

#endif // !CUSTOMIZE

#endif
