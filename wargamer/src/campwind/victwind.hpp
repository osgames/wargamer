/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef VICTWIND_HPP
#define VICTWIND_HPP

#include "wind.hpp"

class CampaignData;

class CampaignVictoryUser
{
   public:
      virtual ~CampaignVictoryUser()  { }
      virtual void endVictoryScreen() = 0;
      virtual const CampaignData* getCampaignData() const = 0;
      virtual HWND getHWND() const = 0;
};

class CampaignVictoryWindow : public Window
{
   public:
      static CampaignVictoryWindow* create(CampaignVictoryUser* user);
      virtual ~CampaignVictoryWindow() = 0;
};

inline CampaignVictoryWindow::~CampaignVictoryWindow() { }


#if 0

class StatisticWindow;
class AnimationWindow;
class CampaignData;

class VictoryRequest : public WargameMessage::MessageBase
{
		CampaignInterface* d_campGame;
	public:
		// VictoryRequest(CampaignData* campData) : d_campData(campData) { }
		VictoryRequest(CampaignInterface* campGame) : d_campGame(campGame) { }

		void run();
		void clear() { delete this; }
};


class VictoryWindow : public WindowBaseND {
	 static ATOM classAtom;
	 static const char s_className[];

	 AnimationWindow* d_animationWindow;
	 StatisticWindow* d_statisticWindow;

	 const CampaignData* d_campData;

  public:
	 VictoryWindow(HWND parent, const CampaignData* campData);
	 ~VictoryWindow() { selfDestruct(); }

     virtual const char* className() const { return s_className; }

	 void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
	 void killAnimation();

	 const CampaignData* getCampaignData() const { return d_campData; }
  private:
	 static ATOM registerClass();
	 // static LPCSTR getClassName() { return className; }

	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	 void onDestroy(HWND hWnd);
	 void onClose(HWND hwnd);
	 void onMCINotifyMode(HWND hwnd, HWND hwndMCI, LONG mode);
//	 void onSize(HWND hWnd, UINT state, int cx, int cy);
//	 void onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild);
//	 void onMove(HWND hWnd, int x, int y);

	 void onQuit();
    void displayStatistics();


  public:
    static void runVictoryWindow(const CampaignData* campData);

};
#endif

#endif
