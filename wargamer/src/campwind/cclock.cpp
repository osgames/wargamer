/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Campaign Clock Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.1  1996/06/22 19:02:35  sgreen
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "cclock.hpp"

#if !defined(EDITOR)
#include "ctimctrl.hpp"
#endif

#include "ctimdata.hpp"

#include "timewind.hpp"
#include "resdef.h"
#include "resstr.hpp"
#include "misc.hpp"

#include "scenario.hpp"

// #include <strstrea.h>
#include <sstream>

class CampaignClockWindow : public TimeControlWindow
{
    public:
        // Constructor

        CampaignClockWindow(CampaignTimeControl* control, const CampaignTimeData* date);
        ~CampaignClockWindow();

#if !defined(EDITOR)
        // Implement TimeControlUser

        virtual bool isTimeFrozen() const;
        virtual void freezeTime(bool freeze);

        virtual int getTimeRange() const;               // How many different rates are there?
        virtual void setRate(RateValue n);              // Set rate to value 0..getTimeRange()
        virtual RateValue getRate() const;

        virtual void setJump(JumpValue n);               // Jump time, e.g. 0=day, 1=week, 2=fortnight
        virtual bool isTimeJumping();
        virtual void stopJump();

        // virtual int getValue() const;                   // Value to display to user for given value
        // virtual int getValue(RateValue rate) const;     // Value to display to user for given value
        virtual TimeValue getTime() const;              // get the current date/time
        virtual String displayTime() const;            // convert time to string
        virtual String displayRate() const;               // e.g. "seconds per day"
#endif
        virtual bool hasJumps() const { return true; }

        // Pass through functions

        // int getWidth() const { return d_timeWindow->getWidth(); }
        // void setPosition(LONG x, LONG y, LONG w, LONG h) { d_timeWindow->setPosition(x, y, w, h); }
        // void update() { d_timeWindow->update(false); }

    private:
        CampaignTimeControl* d_control;
        const CampaignTimeData* d_date;

        static int s_minRate;
        static int s_maxRate;
        static int s_resolution;
        static int s_timeRange;

        static JumpValue s_nJumpValues;
        static GameDay s_jumpValues[];
};

int CampaignClockWindow::s_minRate = 1;
int CampaignClockWindow::s_maxRate = 30;
int CampaignClockWindow::s_resolution = 1;
int CampaignClockWindow::s_timeRange = (1 + s_maxRate - s_minRate) / s_resolution;

TimeControlWindow::JumpValue CampaignClockWindow::s_nJumpValues = 3;
GameDay CampaignClockWindow::s_jumpValues[] =
{
    1, 7, 14
};

/*
 * Constructor
 */

CampaignClockWindow::CampaignClockWindow(CampaignTimeControl* control, const CampaignTimeData* date) :
    d_control(control),
    d_date(date),
    TimeControlWindow()
{
   /*
   * Read in values from scenario file
   */

   // NOTE : not yet done - must be implemented in CTIMCTRL.CPP

/*
   s_minRate = scenario->getInt("CampaignTimeRatioMinimum");
   s_maxRate = scenario->getInt("CampaignTimeRatioMaximum");
   s_resolution = scenario->getInt("CampaignTimeRatioResolution");
   s_timeRange = ((s_maxRate - s_minRate) / s_resolution) + 1;
*/

   s_jumpValues[0] = scenario->getInt("CampaignAdvanceTimeSlow");
   s_jumpValues[1] = scenario->getInt("CampaignAdvanceTimeMedium");
   s_jumpValues[2] = scenario->getInt("CampaignAdvanceTimeFast");
}

CampaignClockWindow::~CampaignClockWindow()
{
}

/*
 * Implement ClockUser functions
 */

#if !defined(EDITOR)

bool CampaignClockWindow::isTimeFrozen() const
{
    return d_control ? d_control->isTimeFrozen() : CampaignClockWindow::isTimeFrozen();
}

void CampaignClockWindow::freezeTime(bool freeze)
{
    if(d_control)
       d_control->freezeTime(freeze);
    else
       CampaignClockWindow::freezeTime(freeze);
}

int CampaignClockWindow::getTimeRange() const
{
    return d_control ? d_control->getTimeRange() : CampaignClockWindow::getTimeRange();
}

void CampaignClockWindow::setRate(RateValue n)
{
    if(d_control)
        d_control->setTimeRate(n);
    else
       CampaignClockWindow::setRate(n);
}

CampaignClockWindow::RateValue CampaignClockWindow::getRate() const
{
    return d_control ? d_control->getTimeRate() : CampaignClockWindow::getRate();
}

void CampaignClockWindow::setJump(JumpValue n)
{
    if(d_control)
    {
       ASSERT(n < s_nJumpValues);
       d_control->setTimeJump(d_date->getCTime().getDate() + s_jumpValues[n]);
    }
    else
       TimeControlWindow::setJump(n);
}

bool CampaignClockWindow::isTimeJumping()
{
    return d_control ? d_control->isTimeJumping() : TimeControlWindow::isTimeJumping();
}

void CampaignClockWindow::stopJump()
{
   if(d_control)
       d_control->setTimeJump(d_date->getCTime().getDate());
}


// virtual int CampaignClockWindow::getValue() const
// {
//     return getValue(getRate());
// }
//
// virtual int CampaignClockWindow::getValue(RateValue rate) const
// {
//     return d_control ? d_control->getSecondsPerDay(rate) : CampaignClockWindow::getValue(rate);
// }

CampaignClockWindow::TimeValue CampaignClockWindow::getTime() const
{
    return d_date->getCTime().getDate();
}

String CampaignClockWindow::displayTime() const
{
   const Date& date = d_date->getDate();
   char buffer[80];
   wsprintf(buffer, "%s %d%s %d",
            getMonthName(date.month, False),
            (int) date.day, getNths(date.day),
            (int) date.year);
    return buffer;
}

/*
 * Strings
 */

// enum IDEnum {
//    Frozen,
//    SecPerDay,
//    IDEnum_HowMany
// };
//
// static const int s_stringIDs[IDEnum_HowMany] = {
//   IDS_CCLOCK_FROZEN,
//   IDS_CCLOCK_SECPERDAY
// };
//
// static ResourceStrings s_strings(s_stringIDs, IDEnum_HowMany);
//
String CampaignClockWindow::displayRate() const
{
//     if(isTimeFrozen())
//         return s_strings.get(Frozen);
//     else
//     {
       char buf[80];
       std::ostringstream buffer(buf, 80);
//       buffer << d_control->getSecondsPerDay() << " " << s_strings.get(SecPerDay) << '\0';
       buffer << d_control->getSecondsPerDay() << " " << InGameText::get(IDS_CCLOCK_SECPERDAY) << '\0';
       return buffer.str(); // String(buffer.str());
        // return s_strings.get(SecPerDay);
//    }
}

#endif  // EDITOR

/*
 * Public Functions
 */


ClockWindow::ClockWindow(HWND hwndParent, CampaignTimeControl* control, const CampaignTimeData* date) :
  d_clockWind(new CampaignClockWindow(control, date))
{
  ASSERT(d_clockWind);
  d_clockWind->init(hwndParent);
}

ClockWindow::~ClockWindow()
{
    delete d_clockWind;
}

void ClockWindow::update()
{
  if(d_clockWind)
    d_clockWind->update(false);
}

void ClockWindow::destroy()
{
  if(d_clockWind)
  {
    // DestroyWindow(d_clockWind->getHWND());
     delete d_clockWind;
    d_clockWind = 0;
  }
}

int ClockWindow::getWidth() const
{
    if(d_clockWind)
        return d_clockWind->getWidth();
    else
        return 0;
}

void ClockWindow::setPosition(LONG x, LONG y, LONG w, LONG h)
{
    if(d_clockWind)
        d_clockWind->setPosition(x, y, w, h);
}


#if 0   // Old version before making abstract


#include "app.hpp"
#include "resdef.h"
#include "palette.hpp"
#include "wmisc.hpp"
// #include "campint.hpp"
#include "tooltip.hpp"
// #include "bargraph.hpp"
#include "fonts.hpp"
#include "dib.hpp"
#include "scn_res.h"
#include "scenario.hpp"
#include "ctbar.hpp"
#include "imglib.hpp"
#include "button.hpp"
#include "help.h"
#include "palwind.hpp"
#include "winctrl.hpp"
#include "scn_img.hpp"
#include "dib_util.hpp"
// #include "generic.hpp"
#include "resstr.hpp"

// #define OLD_TOOLTIP     // Define this for old style tooltips
static TipData tipData[] = {
   { CC_SPEED, TTS_CC_SPEED,  SWS_CC_SPEED },
   { CC_PAUSE, TTS_CC_PAUSE,  SWS_CC_PAUSE },
   { CC_DATE1, TTS_CC_DATE1,  SWS_CC_DATE1 },
   { CC_DATE2, TTS_CC_DATE2,  SWS_CC_DATE2 },
   { CC_DATE3, TTS_CC_DATE3,  SWS_CC_DATE3 },
   EndTipData
};

enum {
  CCI_DayOff,
  CCI_WeekOff,
  CCI_FortNightOff,
  CCI_DayOn,
  CCI_WeekOn,
  CCI_FortNightOn,
  CCI_FreezeOff,
  CCI_FreezeOn,
  CCI_HowMany
};

class CampaignClockWindow : public WindowBaseND
{
#if !defined(EDITOR)
   CampaignTimeControl* d_control;
#endif
   const CampaignTimeData*    d_time;
   HWND d_parent;
   CustomTrackBar* d_trackBar;
   const ImageLibrary* d_images;

   DrawDIBDC* d_dib;
   const DrawDIB* d_fillDib;

   UWORD d_jumping;
   enum { NotJumping = UWORD_MAX };
   GameDay d_lastDay;


public:
#if !defined(EDITOR)
   CampaignClockWindow(HWND hwndParent, CampaignTimeControl* control, const CampaignTimeData* date);
#else
   CampaignClockWindow(HWND hwndParent, const CampaignTimeData* date);
#endif
   ~CampaignClockWindow();// { }

   void update(bool force);         // Redraw Date
private:
   LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
   void onPaint(HWND hWnd);
   void onDestroy(HWND hWnd);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
   void onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
   void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
   void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
   LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
   void onSize(HWND hWnd, UINT state, int cx, int cy);
   void onPaint(HWND hWnd);
   BOOL onEraseBk(HWND hwnd, HDC hdc);

   void drawDate(const DRAWITEMSTRUCT* lpDrawItem);
   void drawRate(const DRAWITEMSTRUCT* lpDrawItem);
   void updateFreeze();

   void setJump(int days, int id);

#ifdef DEBUG
   void checkPalette() { }
#endif
};

/*
 * Constructor
 */



// static const char clockWindowRegName[] = "clockWindow";

#if !defined(EDITOR)
CampaignClockWindow::CampaignClockWindow(HWND hWndParent, CampaignTimeControl* control, const CampaignTimeData* date) :
   d_control(control),
   d_time(date),
#else
CampaignClockWindow::CampaignClockWindow(HWND hWndParent, const CampaignTimeData* date) :
   d_time(date),
#endif
   d_parent(hWndParent),
   d_trackBar(0),
   d_images(0),
   d_dib(0),
   d_fillDib(scenario->getSideBkDIB(SIDE_Neutral)),
   d_jumping(NotJumping),
   d_lastDay(GameDay_MAX)
{
   ASSERT(d_fillDib);

   initToolButton(APP::instance());

   HWND hWnd = createWindow(
      WS_EX_LEFT,
      // GenericNoBackClass::className(),
         NULL,
         WS_CHILD |
//       WS_BORDER |
         WS_CLIPSIBLINGS,     /* Style */
      0,                      /* init. x pos */
      0,                      /* init. y pos */
      219,                    /* init. x size */
      48,                     /* init. y size */
      hWndParent,             /* parent window */
      NULL
      // wndInstance(hWndParent)
   );

   ASSERT(hWnd != NULL);

}

CampaignClockWindow::~CampaignClockWindow()
{
    selfDestruct();

  if(d_trackBar)
    delete d_trackBar;

  if(d_dib)
    delete d_dib;
}

/*
 * Windows message handler
 */

LRESULT CampaignClockWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGES
   debugLog("CampaignClockWindow::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   LRESULT l;
   if(PaletteWindow::handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
//    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
#if !defined(EDITOR)
      HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
      HANDLE_MSG(hWnd, WM_HSCROLL, onHScroll);
#endif      // !EDITOR
      HANDLE_MSG(hWnd, WM_CONTEXTMENU, onContextMenu);
      HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
      HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);

      default:
        return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL CampaignClockWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  ASSERT(d_fillDib);

  RECT r;
  GetClientRect(hwnd, &r);

  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;

  DIB_Utility::allocateDib(&d_dib, cx, cy);
  ASSERT(d_dib);

  d_dib->rect(0, 0, cx, cy, d_fillDib);

  CustomBkWindow cw(scenario->getBorderColors());
  cw.drawThinBorder(d_dib->getDC(), r);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  BitBlt(hdc, 0, 0, cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);

  return TRUE;
}

/*
 * ONCREATE Message
 */

//const int CClockIcons = CCI_HowMany;

BOOL CampaignClockWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
   /*
    *  Now allocate and create Image libraries
    */

   // for order icons
   d_images = ScenarioImageLibrary::get(ScenarioImageLibrary::CClockIcons);
   ASSERT(d_images != 0);

#if !defined(EDITOR)

   HWND hFreeze = addToolButton(hWnd, CC_PAUSE, 3, 22, 15, 15);
   {
      ToolButton bFreeze = hFreeze;
      bFreeze.setCheck(d_control->isTimeFrozen());
   }

   HWND hTrack = CreateWindow(TRACKBAR_CLASS, (LPSTR)NULL,
      TBS_AUTOTICKS | TBS_HORZ | TBS_AUTOTICKS | WS_CHILD | WS_VISIBLE | WS_TABSTOP,
      25, 22, 110, 15, hWnd, (HMENU)CC_SPEED, wndInstance(hWnd), NULL);
   ASSERT(hTrack != 0);
   SendMessage(hTrack, TBM_SETRANGE, TRUE, MAKELONG(0, d_control->getTimeRange() - 1));
   SendMessage(hTrack, TBM_SETPOS, TRUE, d_control->getTimeRate());

   d_trackBar = new CustomTrackBar(hWnd, hTrack, d_fillDib, scenario->getBorderColors());
   ASSERT(d_trackBar != 0);
#endif   // !EDITOR

   const int monthCX = 100;
   const int monthCY = 18;

   addOwnerDrawStatic(hWnd, CC_MONTH, 100, 2, monthCX, monthCY);

#if !defined(EDITOR)
   const int rateCX = 80;
   const int rateCY = 18;

   addOwnerDrawStatic(hWnd, CC_RATE, 8, 2, rateCX, rateCY);

   addOwnerDrawButton(hWnd, 0, CC_DATE1, 145, 22, 19, 19);
   addOwnerDrawButton(hWnd, 0, CC_DATE2, 169, 22, 19, 19);
   addOwnerDrawButton(hWnd, 0, CC_DATE3, 193, 22, 19, 19);


   updateFreeze();
#endif   // !EDITOR

   g_toolTip.addTips(hWnd, tipData);


   return TRUE;

}

void CampaignClockWindow::onDestroy(HWND hWnd)
{
#ifdef DEBUG
   debugLog("CampaignClockWindow::onDestroy()\n");
#endif

   g_toolTip.delTips(hWnd);

}

static DWORD ids[] = {
  CC_RATE,   IDH_GameSpeed,
  CC_PAUSE,  IDH_Pause,
  CC_MONTH,  IDH_GameDate,
  CC_SPEED,  IDH_GameSpeedSlider,
  CC_DATE1,  IDH_AdvTimeDay,
  CC_DATE2,  IDH_AdvTimeWeek,
  CC_DATE3,  IDH_AdvTimeFortnight,
  0, 0
};

void CampaignClockWindow::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
}



static UWORD idToCCImage(int id, bool focus)
{
  if(id == CC_DATE1)
    return focus ? CCI_DayOn : CCI_DayOff;
  else if(id == CC_DATE2)
    return focus ? CCI_WeekOn : CCI_WeekOff;
  else
    return focus ? CCI_FortNightOn : CCI_FortNightOff;
}

void CampaignClockWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem)
{
#ifdef DEBUG_MESSAGES
   debugLog("CampaignClockWindow::onDrawItem(%d, 0x%04x %d)\n",
      (int) lpDrawItem->CtlID,
      (int) lpDrawItem->itemState,
      (int) lpDrawItem->itemAction);
#endif

   const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
   const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

   HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
   RealizePalette(lpDrawItem->hDC);

   switch(lpDrawItem->CtlID)
   {
#if !defined(EDITOR)
      case CC_DATE1:
      case CC_DATE2:
      case CC_DATE3:
      {
        DIB_Utility::allocateDib(&d_dib, cx, cy);
        ASSERT(d_dib);

        d_dib->rect(0, 0, cx, cy, d_fillDib);

        bool focus = (lpDrawItem->itemState & ODS_SELECTED) ? true : false;

        if(d_jumping == lpDrawItem->CtlID)
        {
            focus = true;
        }

        UWORD index = idToCCImage(lpDrawItem->CtlID, focus);
        d_images->blit(d_dib, index, 0, 0);

        BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, cx, cy,
           d_dib->getDC(), 0, 0, SRCCOPY);

        break;
      }
      case CC_RATE:
        drawRate(lpDrawItem);
        break;

      case CC_PAUSE:
      {
        DIB_Utility::allocateDib(&d_dib, cx, cy);
        ASSERT(d_dib);

        d_dib->rect(0, 0, cx, cy, d_fillDib);

        UWORD index = (UWORD)(lpDrawItem->itemState & (ODS_SELECTED | ODS_CHECKED) ? CCI_FreezeOn : CCI_FreezeOff);

        d_images->blit(d_dib, index, 0, 0);

        BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top,
               cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);


        break;
      }
#endif   // !EDITOR
      case CC_MONTH:
        drawDate(lpDrawItem);
        break;

      default:
#ifdef DEBUG
        wTextOut(lpDrawItem->hDC, 0, 0,"User Drawn");
        debugMessage("CampaignClockWindow::onDrawItem Unknown User Drawn ID (%d)\n", (int) lpDrawItem->CtlID);
#else
        break;
#endif
   }
}

void CampaignClockWindow::drawDate(const DRAWITEMSTRUCT* lpDrawItem)
{
   HDC hdc = lpDrawItem->hDC;
   const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
   const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

   char buffer[80];

   const Date& date = d_time->getDate();

   wsprintf(buffer, "%s %d%s %d",
            getMonthName(date.month, False),
            (int) date.day, getNths(date.day),
            (int) date.year);


   DIB_Utility::allocateDib(&d_dib, cx, cy);

   ASSERT(d_dib);
   ASSERT(d_fillDib);
   d_dib->rect(0, 0, cx, cy, d_fillDib);
   d_dib->setBkMode(TRANSPARENT);

   int fontHeight = 14;

   LogFont lf;
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));

   Font font;
   font.set(lf);

   d_dib->setFont(font);

   COLORREF color;

   if(d_jumping == NotJumping)
      color = scenario->getColour("MenuText");
   else
      color = RGB(255,0,0);

   d_dib->setTextColor(color);

   wTextOut(d_dib->getDC(), 2, 2, buffer);

   BitBlt(hdc, 0, 0, cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);
}

#if !defined(EDITOR)

enum IDEnum {
   Frozen,
   SecPerDay,
   IDEnum_HowMany
};

static const int s_stringIDs[IDEnum_HowMany] = {
  IDS_CCLOCK_FROZEN,
  IDS_CCLOCK_SECPERDAY
};
static ResourceStrings s_strings(s_stringIDs, IDEnum_HowMany);

void CampaignClockWindow::drawRate(const DRAWITEMSTRUCT* lpDrawItem)
{
   HDC hdc = lpDrawItem->hDC;
   const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
   const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

   char buffer[80];

   int rate = d_control->getSecondsPerDay();
   if((rate == 0) || d_control->isTimeFrozen())
      wsprintf(buffer, s_strings.get(Frozen));
   else
   {
      wsprintf(buffer, "%d %s", rate, s_strings.get(SecPerDay));
   }

   DIB_Utility::allocateDib(&d_dib, cx, cy);

   ASSERT(d_dib);
   ASSERT(d_fillDib);
   d_dib->rect(0, 0, cx, cy, d_fillDib);
   d_dib->setBkMode(TRANSPARENT);

   int fontHeight = 14;

   LogFont lf;
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));

   Font font;
   font.set(lf);

   d_dib->setFont(font);
   d_dib->setTextColor(scenario->getColour("MenuText"));

   wTextOut(d_dib->getDC(), 2, 2, buffer);

   BitBlt(hdc, 0, 0, cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);
}

void CampaignClockWindow::updateFreeze()
{
   Boolean frozen = d_control->isTimeFrozen();

   ToolButton b(getHWND(), CC_PAUSE);
   b.setCheck(d_control->isTimeFrozen());

   TrackBar tb(getHWND(), CC_SPEED);
   tb.enable(!frozen);
}
#endif   // !EDITOR

/*
 * Date has been updated
 */

void CampaignClockWindow::update(bool force)
{
#ifdef DEBUG_UPDATE
   debugLog("CampaignClockWindow::update()\n");
#endif

#if !defined(EDITOR)
   /*
    * Turn off button when time jumping has finished
    */

   bool jumpFinished = (d_jumping != NotJumping) && !d_control->isTimeJumping();

   if(force || jumpFinished)
   {
      HWND button = GetDlgItem(getHWND(), d_jumping);
      ASSERT(button);
      if(button)
         InvalidateRect(button, NULL, FALSE);

      if(jumpFinished)
      {
         d_jumping = NotJumping;
         force = true;     // make sure date is redisplayed in correct colours
      }
   }
#endif   // !EDITOR

   GameDay day = d_time->getCTime().getDate();
   if(force || (day != d_lastDay))
   {
      d_lastDay = day;
      HWND hDate = GetDlgItem(getHWND(), CC_MONTH);
      ASSERT(hDate != NULL);
      if(hDate)
         InvalidateRect(hDate, NULL, FALSE);
   }
}

#if !defined(EDITOR)

void CampaignClockWindow::onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
#ifdef DEBUG_MESSAGES
   debugLog("ClockWindow::onHScroll(%p %p %u %d)\n",
      hwnd, hwndCtl, code, pos);

   HWND hTrack = GetDlgItem(hwnd, CC_SPEED);
   ASSERT(hTrack != NULL);
   ASSERT(hTrack == hwndCtl);
#endif

   /*
    * Simplest method is to simply get current position on any message
    * Alternatively, if only want to change speed when dragging has finished
    * then check on TB_ENDTRACK
    */

   switch(code)
   {
   case TB_ENDTRACK:
   case TB_BOTTOM:
   case TB_LINEDOWN:
   case TB_LINEUP:
   case TB_PAGEDOWN:
   case TB_PAGEUP:
   case TB_TOP:
      pos = SendMessage(hwndCtl, TBM_GETPOS, 0, 0);
      break;
   case TB_THUMBPOSITION:
   case TB_THUMBTRACK:
      break;
   }

#ifdef DEBUG_MESSAGES
   debugLog("Setting speed to %d\n", pos);
#endif

   d_control->setTimeRate(pos);
   HWND hRate = GetDlgItem(hwnd, CC_RATE);
   ASSERT(hRate != 0);
   InvalidateRect(hRate, NULL, TRUE);
}

void CampaignClockWindow::setJump(int days, int id)
{
   d_control->setTimeJump(GameDay(d_time->getCTime().getDate() + days));
   d_jumping = id;
   update(true);
}

void CampaignClockWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
#ifdef DEBUG_MESSAGES
   debugLog("ClockWindow::onCommand(%p %d %p %u)\n",
      hwnd, id, hwndCtl, codeNotify);
#endif
   switch(id)
   {
   case CC_DATE1:
      if(codeNotify == BN_CLICKED)
      {
#ifdef DEBUG_MESSAGES
         debugLog("Advance by %d days pressed\n", 1);
#endif
         setJump(1, id);
      }
      break;

   case CC_DATE2:
      if(codeNotify == BN_CLICKED)
      {
#ifdef DEBUG_MESSAGES
         debugLog("Advance by %d days pressed\n", 7);
#endif
         setJump(7, id);
      }
      break;

   case CC_DATE3:
      if(codeNotify == BN_CLICKED)
      {
#ifdef DEBUG_MESSAGES
         debugLog("Advance by %d days pressed\n", 14);
#endif
         setJump(14, id);
      }
      break;

   case CC_PAUSE:
      if(codeNotify == BN_CLICKED)
      {
#ifdef DEBUG_MESSAGES
         debugLog("Freeze button pressed\n");
#endif
         d_control->freezeTime(!d_control->isTimeFrozen());
         updateFreeze();
         HWND hRate = GetDlgItem(hwnd, CC_RATE);
         ASSERT(hRate != 0);
         InvalidateRect(hRate, NULL, FALSE);
      }
      break;

   default:
#ifdef DEBUG_MESSAGES
      debugLog("ClockWindow::onCommand, unknown id %d\n", id);
#endif
      break;
   }

}


#endif   // !EDITOR

#if !defined(EDITOR)
ClockWindow::ClockWindow(HWND hwndParent, CampaignTimeControl* control, const CampaignTimeData* date)
#else
ClockWindow::ClockWindow(HWND hwndParent, const CampaignTimeData* date)
#endif
{
    d_clockWind = new CampaignClockWindow;
  ASSERT(d_clockWind);

  d_clockWind->init(hwndParent, control, date);

}

ClockWindow::~ClockWindow()
{
  delete d_clockWind;
}

void ClockWindow::show()
{
  if(d_clockWind)
    ShowWindow(d_clockWind->getHWND(), SW_SHOW);
}

void ClockWindow::update()
{
  if(d_clockWind)
    d_clockWind->update(false);
}

void ClockWindow::destroy()
{
  if(d_clockWind)
  {
    delete d_clockWind;
    d_clockWind = 0;
  }
}

HWND ClockWindow::getHWND() const
{
  return (d_clockWind) ? d_clockWind->getHWND() : 0;
}

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
