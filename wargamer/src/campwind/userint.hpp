/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef USERINT_HPP
#define USERINT_HPP

#include "mw_user.hpp"
//#include "campord.hpp"
#include "cpidef.hpp"

/*
 * Virtual interface to user interface
 * Replaces old OrderTB
 */

struct MapSelect;
class StackedUnitList;
class DrawDIBDC;
struct Date;
class CampaignWindowsInterface;
class DrawDIB;
class DrawDIBDC;
//class ICommandPosition;


class MapWindow;

class CampaignUserInterface {
  public:
	 virtual ~CampaignUserInterface() {}

	 /*
	  * virtual functions
	  */

	 virtual TrackMode getTrack() = 0;		// Return how to do tracking

	 /*
	  * Mouse Tracking functions
	  * These shouldn't all be pure virtual since only unitint needs most of these
	  */

	 virtual void onLClick(MapSelect& info) = 0;
	 virtual void onRClick(MapSelect& info) = 0;
	 virtual void onButtonDown(MapSelect& info) = 0;
	 virtual bool onStartDrag(MapSelect& info) = 0;
	 virtual void onDrag(MapSelect& info) = 0;
	 virtual void onEndDrag(MapSelect& info) = 0;

	 virtual void overObject(MapSelect& info) = 0;
	 virtual void notOverObject() = 0;

	 /*
	  * Map Drawing
	  */

	 virtual void onDraw(DrawDIBDC* dib, MapSelect& info) = 0;

	 virtual void update() = 0;					// Info may have changed

	 virtual void updateZoom() = 0;        // update map arrows when zooming


	 /*
	  * Sending Orders
	  */

	 virtual void sendOrder(ConstParamCP cpi) = 0;
	 virtual void cancelOrder() = 0;

	 /*
	  * Misc.
	  */

	 virtual CampaignWindowsInterface* campWindows() const = 0;
//	 virtual const void* getData() const = 0;   // derived-class defined data

	 /*
	  * Functions Dialogs use
	  */

//	 virtual void toggle() = 0;
//	 virtual void initUnits(Boolean all) = 0;
//	 virtual void syncCombos(int i) = 0;

//	 virtual void runOrders(const PixelPoint& p) = 0;
//	 virtual void runInfo(const PixelPoint& p) = 0;
//	 virtual void runOB(const PixelPoint& p) = 0;
	 virtual void orderUnit(ConstParamCP cpi) = 0;
	 virtual void orderTown(ITown itown) = 0;
	 virtual void infoUnit(ConstParamCP cpi) = 0;
	 virtual void infoTown(ITown itown) = 0;
    virtual void reorgOB(ConstParamCP cpi) = 0;

	 /*
	  * Clean up
	  */

//	 virtual void destroy() = 0;

	 /*
	  * some utility functions
	  */

	 static void drawText(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem, DrawDIBDC* dib, const DrawDIB* fillDib, Boolean shadow);
	 static void blitDibToControl(const DrawDIBDC* dib, const DRAWITEMSTRUCT* lpDrawItem);
	 static void transferBits(DrawDIBDC* dib, PixelPoint& p, int cx, int cy, int shadowCX, int shadowCY);

	 static void convertToScreen(HWND hwnd, const RECT& r, PixelPoint& endP);
	 static void clipPoint(const RECT& r, POINT& p);
	 static void allocateControlDib(HWND hControl, DrawDIBDC** dib, const LONG dbY);

  protected:
	 void calcDialPosition(MapWindow* mapWindow, const RECT& r, const Location& endLocation, PixelPoint& endP);
	 // void calcTrackingDialPosition(MapWindow* mapWindow, const RECT& r, const Location& endLocation, PixelPoint& endP) {}
	 void calcTrackingDialPosition(const MapWindow* mapWindow, const Location& l, PixelPoint& p, const int trackCX, const int trackCY);
};

#endif
