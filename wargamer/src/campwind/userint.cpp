/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "userint.hpp"
//#include "campdint.hpp"
//#include "compos.hpp"
//#include "town.hpp"
#include "simpstr.hpp"
#include "dib.hpp"
#include "fonts.hpp"
#include "mapwind.hpp"
#include "app.hpp"
#include "mw_data.hpp"
//#include "campord.hpp"
#include "dib_util.hpp"


//----------------------- User-Interface ----------------------------------

/*
 * Make sure point is totally onscreen
 */

void CampaignUserInterface::clipPoint(const RECT& r, POINT& p)
{
  /*
   * Get some useful values
   */

  ASSERT(APP::getMainHWND());

  RECT mRect;
  GetClientRect(APP::getMainHWND(), &mRect);

  const int screenCX = mRect.right - mRect.left;
  const int screenCY = mRect.bottom - mRect.top;

  // for left side
  p.x = maximum(0, p.x);

  // for right side
  p.x = minimum(screenCX - (r.right-r.left), p.x);

  // for top
  p.y = maximum(0, p.y);

  // for bottom
  p.y = minimum(screenCY - (r.bottom-r.top), p.y);
}

/*
 *  Convert to screen coordinates and adjust for screen edges
 */

void CampaignUserInterface::convertToScreen(HWND hwnd, const RECT& r, PixelPoint& endP)
{
  POINT p;
  p.x = endP.getX();
  p.y = endP.getY();

  ClientToScreen(hwnd, &p);

  /*
   * Clip it
   */

  clipPoint(r, p);

  endP.setX(p.x);
  endP.setY(p.y);
}

void CampaignUserInterface::drawText(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem, DrawDIBDC* dib, const DrawDIB* fillDib, Boolean shadow)
{
  ASSERT(dib);
  ASSERT(fillDib);

  /*
   * Some useful values
   */

  LONG dbUnits = GetDialogBaseUnits();
  const LONG dbY = HIWORD(dbUnits);
  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  /*
   * Fill in background
   */

  dib->rect(0, 0, cx, cy, fillDib);

  if(shadow)
  {
    DIB_Utility::InsertData id;
    id.d_x = 0;
    id.d_y = 0;
    id.d_cx = cx;
    id.d_cy = cy;
    id.d_shadowCX = 3;
    id.d_shadowCY = 3;
    id.d_mode = DIB_Utility::InsertData::ShadowOnly;

    DIB_Utility::drawInsert(dib, id);
  }

  /*
   * Put text
   */

  char text[500];
  GetDlgItemText(hwnd, lpDrawItem->CtlID, text, 500);

  RECT r;
  SetRect(&r, 0, 0, cx, cy);
  DrawText(dib->getDC(), text, lstrlen(text), &r, 0);

  blitDibToControl(dib, lpDrawItem);
}

void CampaignUserInterface::blitDibToControl(const DrawDIBDC* dib, const DRAWITEMSTRUCT* lpDrawItem)
{
  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  /*
   * Blt dib
   */

  if(dib)
  {
    BitBlt(lpDrawItem->hDC, 0, 0, cx, cy,
      dib->getDC(), 0, 0, SRCCOPY);
  }
}

// transfer bits from mainwindow's hdc to draw shadow
void CampaignUserInterface::transferBits(DrawDIBDC* dib, PixelPoint& p, int cx, int cy, int shadowCX, int shadowCY)
{
  POINT p2;
  p2.x = p.getX();
  p2.y = p.getY();

  ScreenToClient(APP::getMainHWND(), &p2);

  HDC hdc = GetDC(APP::getMainHWND());

  int x = cx - shadowCX;
  int y = 0;
  BitBlt(dib->getDC(), x, y, shadowCX, cy,
     hdc, x + p2.x, y + p2.y, SRCCOPY);

  x = 0;
  y = cy - shadowCY;
  BitBlt(dib->getDC(), x, y, cx, shadowCY,
     hdc, x + p2.x, y + p2.y, SRCCOPY);

  ReleaseDC(APP::getMainHWND(), hdc);
}


void CampaignUserInterface::allocateControlDib(HWND hControl, DrawDIBDC** dib, const LONG dbY)
{
  DrawDIBDC* aDib = *dib;

  RECT r;
  GetClientRect(hControl, &r);

  const int width = r.right-r.left;
  const int height = r.bottom-r.top;

  /*
   * if needed, allocate dib, set font, etc.
   */

  if(!aDib ||
     aDib->getWidth() != width ||
     aDib->getHeight() != height)
  {
    if(aDib)
    {
      delete aDib;
      aDib = 0;
    }

    aDib = new DrawDIBDC(width, height);

    /*
     * Set font
     */


    const int fontSize = (6*dbY)/8;
    HFONT hFont = FontManager::getFont("Copperplate Gothic Bold", fontSize);

    aDib->setFont(hFont);
    aDib->setBkMode(TRANSPARENT);

    *dib = aDib;
  }
}

/*
 * Position main pop-up dialog
 */

void CampaignUserInterface::calcDialPosition(MapWindow* mapWindow, const RECT& r, const Location& endLocation, PixelPoint& endP)
{
  /*
   * Convert locations to pixels
   */

  mapWindow->mapData().locationToPixel(endLocation, endP);

  /*
   * Calculate offset
   */

  const int xOffset = 15;
  const int yOffset = 15;


  endP.setX(endP.getX() + xOffset);
  endP.setY(endP.getY() + yOffset);

  /*
   * Convert to screen
   */

  convertToScreen(mapWindow->getHWND(), r, endP);
}

void CampaignUserInterface::calcTrackingDialPosition(const MapWindow* mapWindow, const Location& l, PixelPoint& p, const int trackCX, const int trackCY)
{
  /*
   * Convert locations to pixels
   */

  mapWindow->mapData().locationToPixel(l, p);

  /*
   * Add offset
   */

  const int xOffset = 30;
  const int yOffset = -trackCY;


  p.setX(p.getX() + xOffset);
  p.setY(p.getY() + yOffset);

  /*
   * clip
   */

  RECT r;
  GetClientRect(mapWindow->getHWND(), &r);

  const scrollWidth = 15;
  const int mapCX = (r.right - r.left) - scrollWidth;
  const int mapCY = (r.bottom - r.top) - scrollWidth;

  if(p.getX() > (mapCX - trackCX))
    p.setX(p.getX() - (trackCX + (2*xOffset)));
  if(p.getY() < 0)
    p.setY(p.getY() - yOffset);

  if(p.getY() > (mapCY - trackCY))
    p.setY(mapCY - trackCY);          // shouldn't reach here
  if(p.getX() < 0)
    p.setX(0);                        // shouldn't reach here
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
