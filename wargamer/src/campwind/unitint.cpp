/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "unitint.hpp"
#include "mw_route.hpp"
#include "mw_data.hpp"
#include "armies.hpp"
#include "compos.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "mapwind.hpp"
#include "ds_unit.hpp"
#include "app.hpp"
#include "infowind.hpp"
#include "route.hpp"
#include "cwin_int.hpp"
#include "armyutil.hpp"
#include "unitmenu.hpp"
#include "uordrwin.hpp"
#include "uinfowin.hpp"
#include "reorgob.hpp"
#include "camp_snd.hpp"

#ifdef DEBUG
#include "logwin.hpp"
#endif

/* ------------------------- CampaignUnitInterface -------------------------
 *
 */

CampaignUnitInterface::CampaignUnitInterface(CampaignWindowsInterface* cwi, const CampaignData* campData, MapWindow* mapWindow) :
  d_infoWindow(0),
  d_orderWindow(0),
  d_trackingWindow(0),
  d_menu(0),
  d_obWindow(0),
  d_campWind(cwi),
  d_campData(campData),
  d_mapWindow(mapWindow),
  d_mode(UnitInterfaceMode::Waiting),
  d_unitID(NoUnitListID),
  d_trackMode(MWTM_Unit),
  d_overThisUnit(NoCommandPosition),
  d_flags(CanOrderUnit)
{
//  ASSERT(d_orderDial != 0);
  ASSERT(d_campWind != 0);
  ASSERT(d_campData != 0);
  ASSERT(d_mapWindow != 0);

  // info window
  d_infoWindow = new UnitInfo_Int(this, d_mapWindow->getHWND(), d_campData);
  ASSERT(d_infoWindow);

  // order window
  d_orderWindow = new UnitOrder_Int(this, d_mapWindow->getHWND(), d_campWind, d_campData, d_orderValues);
  ASSERT(d_orderWindow);

  // tracking window
  d_trackingWindow = new TrackingWindow(mapWindow->getHWND(), d_campData);
  ASSERT(d_trackingWindow);

  // popup menu
  d_menu = new UnitMenu_Int(this, mapWindow->getHWND(), d_campWind, d_campData, d_orderValues);
  ASSERT(d_menu);

  // ob window
  d_obWindow = new ReorgOB_Int(this, d_mapWindow->getHWND(), d_campWind, d_campData);
  ASSERT(d_obWindow);
}

CampaignUnitInterface::~CampaignUnitInterface()
{
  destroy();
}

void CampaignUnitInterface::destroy()
{
  if(d_infoWindow)
    delete d_infoWindow;

  if(d_orderWindow)
    delete d_orderWindow;

  if(d_trackingWindow)
    delete d_trackingWindow;

  if(d_obWindow)
    delete d_obWindow;

  if(d_menu)
    delete d_menu;
}

void CampaignUnitInterface::overObject(MapSelect& info)
{
  if(d_mode == UnitInterfaceMode::Waiting &&
     !(d_flags & TrackingUnit) && !(d_flags & TrackingDestTown) )
  {
    if( info.trackedUnits->unitCount() > 0 )// &&
    {
      if(d_overThisUnit != info.trackedUnits->getUnit(0))
      {
        d_overThisUnit = info.trackedUnits->getUnit(0);
        Location l;
        d_overThisUnit->getLocation(l);

        PixelPoint p;
        calcTrackingDialPosition(d_mapWindow, l, p,
            d_trackingWindow->width(), d_trackingWindow->height());

        d_trackingWindow->show(p, d_overThisUnit);
      }
    }
  }
}

void CampaignUnitInterface::notOverObject()
{
  ASSERT(d_trackingWindow);

  if(d_overThisUnit != NoCommandPosition)
  {
    d_trackingWindow->hide();
    d_overThisUnit = NoCommandPosition;
  }

  if(d_flags & TrackingUnit ||
     d_flags & TrackingDestTown ||
     d_mode != UnitInterfaceMode::Waiting)
  {
    // do nothing
  }
  else
    d_unitID = NoUnitListID;
}

void CampaignUnitInterface::onButtonDown(MapSelect& info)
{
  // if we're waiting then check for a clicked-on unit
  if(d_mode == UnitInterfaceMode::Waiting) //  || d_mode == UnitInterfaceMode::OverUnit)
  {
    // make sure we're in proper mode
    if(d_trackMode != MWTM_Unit)
      d_trackMode = MWTM_Unit;

    if( info.trackedUnits->unitCount() > 0 )// &&
    {
      if(d_unitID == NoUnitListID || d_unitID >= info.trackedUnits->unitCount())
        d_unitID = 0;

      ConstICommandPosition cp = info.trackedUnits->getUnit(d_unitID);
      d_flags |= TrackingUnit;
      d_stackedUnits = *info.trackedUnits;   // array copy

      if(CampaignArmy_Util::isUnitOrderable(info.trackedUnits->getUnit(d_unitID)))
        d_flags |= CanOrderUnit;
      else
        d_flags &= ~CanOrderUnit;

      d_orderValues = *cp->getLastOrder();

      if(d_orderValues.isMoveOrder())
      {
        CampaignRouteUtil::plotRoute(d_campData, d_routeList, cp->getPosition(),
             d_orderValues, cp->getSide());
      }

      initOrderWindow();
    }
  }

  // otherwise check for clicked-on Last Destination
  else if(d_mode == UnitInterfaceMode::SettingUpMove)
  {
    ASSERT(d_trackMode == MWTM_Town);
    if(info.selectedTown != NoTown && info.selectedTown == d_orderValues.getDestTown())
    {
      d_flags |= TrackingDestTown;
    }
  }

  // otherwise check for clicked on current unit
  else if(d_mode == UnitInterfaceMode::SettingUpOrder)
  {
    ASSERT(d_trackMode == MWTM_Unit);
    if(info.trackedUnits->unitCount() > 0)
    {
      // see if stacked units contains current unit
      ConstICommandPosition currentCPI = d_orderWindow->currentUnit(); //d_orderDial[UnitDialog::UnitDialogType::MainOrderDialog]->currentUnit();

      currentCPI = d_campData->getArmies().getTopParent(currentCPI);
      ASSERT(currentCPI != NoCommandPosition);
      for(int i = 0; i < info.trackedUnits->unitCount(); i++)
      {
        if(currentCPI == info.trackedUnits->getUnit(i))
        {
          d_flags |= TrackingUnit;
        }
      }
    }
  }

  if(d_flags & TrackingUnit || d_flags & TrackingDestTown)
  {
//  notOverObject();
    d_orderWindow->show(false);    // hide();
    d_startMouseLocation = info.mouseLocation;
  }
}

void CampaignUnitInterface::onStartDrag()
{
  if( (d_flags & TrackingUnit || d_flags & TrackingDestTown) &&
      d_flags & CanOrderUnit )
  {
    d_trackMode = MWTM_Dragging;
  }
}

void CampaignUnitInterface::onDrag(MapSelect& info)
{
}

void CampaignUnitInterface::onEndDrag(MapSelect& info)
{
  if(d_trackMode == MWTM_Dragging)
  {
    Location location;

    ASSERT(d_flags & TrackingUnit || d_flags & TrackingDestTown);

    /*
     * if we can't order unit, return
     */

    if(!(d_flags & CanOrderUnit))
      return;

    /*
     *  TODO: Check for offscreen location, if so return
     */

    if( (info.hasTown() || info.hasUnit()) )        // and we have a destTown or unit
    {
      // if tracking unit then clear previous via's
      if(d_flags & TrackingUnit)
        d_orderValues.getVias()->clear();

      // if we already have a destination then make it a via
      if(d_flags & TrackingDestTown &&
         d_orderValues.hasDestTown())
      {
        if(d_orderValues.getNVias() < Orders::Vias::MaxVia)
        {
          d_orderValues.setVia(d_orderValues.getDestTown(), d_orderValues.getNVias());
        }
      }

      if(info.hasTown())
      {
        d_orderValues.setDestTown(info.selectedTown);
      }

      else
      {
        ASSERT(info.hasUnit());
        d_orderValues.setTargetUnit(info.getCP());
      }

      d_mode = UnitInterfaceMode::SettingUpMove;
      d_orderValues.setType(Orders::Type::MoveTo);
      d_trackMode = MWTM_Town;
      location = info.mouseLocation;
    }
    else
    {
      // if we were tracking a dest town then go back to last town and reopen move dialog
      if(d_flags & TrackingDestTown)
      {
        ASSERT(d_orderValues.hasDestTown());

        const Town& t = d_campData->getTown(d_orderValues.getDestTown());
        location = t.getLocation();
        d_mode = UnitInterfaceMode::SettingUpMove;
        d_trackMode = MWTM_Town;
      }
      else
        d_trackMode = MWTM_Unit;
    }

    if(d_mode == UnitInterfaceMode::SettingUpMove)
    {
      ASSERT(d_stackedUnits.unitCount() > 0);
      ASSERT(d_unitID != NoUnitListID);
      ASSERT(d_unitID < d_stackedUnits.unitCount());

      ConstICommandPosition cp = d_stackedUnits.getUnit(d_unitID);
      CampaignRouteUtil::plotRoute(d_campData, d_routeList, cp->getPosition(),
         d_orderValues, cp->getSide());

      PixelPoint p;
      calcMoveOrderDialPosition(location, p);

      d_campData->soundSystem()->TriggerSound(SOUNDTYPE_WINDOWOPEN, CAMPAIGNSOUNDPRIORITY_INSTANT);

      runOrders(p);
    }
  }

  clearTrackingFlags();
}

void CampaignUnitInterface::onLClick(MapSelect& info)
{
  if(d_flags & TrackingUnit &&
     d_mode == UnitInterfaceMode::Waiting)
  {
    d_startMouseLocation = info.mouseLocation;

    ASSERT(d_stackedUnits.unitCount() > 0);
    ASSERT(d_unitID != NoUnitListID);
    if((d_unitID + 1) < d_stackedUnits.unitCount())
      d_unitID++;
    else
      d_unitID = 0;

    d_trackingWindow->update(d_stackedUnits.getUnit(d_unitID));
  }

  clearTrackingFlags();
}

void CampaignUnitInterface::onRClick(MapSelect& info)
{
  if(info.trackedUnits->unitCount() > 0)
  {
    // hide pop-up window
    if(d_overThisUnit != NoCommandPosition)
      d_trackingWindow->hide();

    d_stackedUnits = *info.trackedUnits;   // array copy

    PixelPoint p1;
    d_mapWindow->mapData().locationToPixel(info.mouseLocation, p1);

    POINT p = p1;;

    ClientToScreen(d_mapWindow->getHWND(), &p);

    p1.setX(p.x);
    p1.setY(p.y);

    d_unitID = (d_unitID != NoUnitListID && d_unitID < d_stackedUnits.unitCount()) ?
       d_unitID : 0;

    d_orderValues = *d_stackedUnits.getUnit(d_unitID)->getLastOrder();

    initOrderWindow();
    d_menu->runMenu(d_stackedUnits.getUnit(d_unitID), p1);

    d_campData->soundSystem()->TriggerSound(SOUNDTYPE_WINDOWOPEN, CAMPAIGNSOUNDPRIORITY_INSTANT);
  }
}

void CampaignUnitInterface::addObject(ICommandPosition cpi)
{
  d_stackedUnits.reset();
  d_stackedUnits.addUnit(cpi);

  d_unitID = 0;

  d_orderValues = *d_stackedUnits.getUnit(d_unitID)->getLastOrder();
  initOrderWindow();
}

void CampaignUnitInterface::runMenu(const PixelPoint& p)
{
  if(d_stackedUnits.unitCount() > 0)
  {
    d_unitID = (d_unitID != NoUnitListID && d_unitID < d_stackedUnits.unitCount()) ?
       d_unitID : 0;

    d_menu->runMenu(d_stackedUnits.getUnit(d_unitID), p);
  }
}

void CampaignUnitInterface::initOrderWindow()
{
  if(d_stackedUnits.unitCount() > 0)
  {
    ASSERT(d_unitID != NoUnitListID);
    d_unitID = (d_unitID < d_stackedUnits.unitCount()) ? d_unitID : 0;

    d_orderWindow->init(d_stackedUnits, d_unitID);
  }
}

/*
 * Calculate position of pop-up move-order dialog
 * This will work as follows:
 *  1. If dragging left to right dialog will popup to the right of the mouse
 *  2. If dragging right to left dialog will popup with its right edge to
 *     the left of the mouse,
 *  3. The edge of the screen will also have to be taken into account
 */

void CampaignUnitInterface::calcMoveOrderDialPosition(const Location& endLocation, PixelPoint& endP)
{
  /*
   * Convert locations to pixels
   */

  PixelPoint startP;
  d_mapWindow->mapData().locationToPixel(d_startMouseLocation, startP);
  d_mapWindow->mapData().locationToPixel(endLocation, endP);

  /*
   * Calculate offset
   */

  int xOffset = 0;
  int yOffset = 0;
  const int offset = 15;

  if(startP.getX() < endP.getX())  // mouse was dragged left to right
  {
    xOffset += offset;
    yOffset += offset;
  }
  else // mouse was dragged right to left
  {
    xOffset -= ((d_orderWindow->width()) + offset);
    yOffset += offset;
  }

  endP.setX(endP.getX() + xOffset);
  endP.setY(endP.getY() + yOffset);

  /*
   * Convert to screen
   */

  POINT p = endP;
  ClientToScreen(d_mapWindow->getHWND(), &p);

  // clip
  RECT r;
  GetWindowRect(d_mapWindow->getHWND(), &r);

  if(p.x > r.right - d_orderWindow->width())
    p.x = r.right - d_orderWindow->width();
  else if(p.x < r.left)
    p.x = r.left;

  if(p.y > r.bottom - d_orderWindow->fullHeight())
    p.y = r.bottom - d_orderWindow->fullHeight();
  else if(p.y < r.top)
    p.y = r.top;

  endP = p;
}

void CampaignUnitInterface::update()
{
  d_campWind->redrawMap();
}

/*
 * Draw Route Arrows as Currently set up in the interface
 */

void CampaignUnitInterface::onDraw(DrawDIBDC* dib, MapSelect& info)
{
  if(d_stackedUnits.unitCount() > 0)
  {
    ConstICommandPosition cp = d_stackedUnits.getUnit(0);
    d_mapWindow->drawRoute(dib, cp->getPosition(), d_orderValues, cp->getSide(), d_routeList);
  }
}

/*
 * Make sure route doesn't double back on itself
 * if it does, fix it
 */

void makeRouteLogical(const CampaignData* campData, ICommandPosition cpi, CampaignOrder& order)
{
  ASSERT(campData);
  ASSERT(cpi != NoCommandPosition);
  ASSERT(order.isMoveOrder());
  ASSERT(order.getNVias() > 0);

  /*
   * First, plot a route using currrent order
   */

  RouteList rl;
  CampaignRouteUtil::plotRoute(campData, rl, cpi->getPosition(), order, cpi->getSide());
  ASSERT(rl.entries() >= 2);

  /*
   * Now, go through and remove any repeating nodes
   */

  RouteNode* first = rl.first();
  Boolean remove = False;

  RouteListIterNLW iter(&rl);
  while(++iter)
  {
    if(first != iter.current() &&
       first->d_town == iter.current()->d_town)
    {

      RouteNode* removeNode = iter.current();

      /*
       * we also have to remove all previous nodes to this one
       */

      RouteListIterNLW iter2(&rl);
      while(++iter2)
      {
        RouteNode* node = iter2.current();
        if(node == removeNode)
          break;
        else
        {
          iter2.remove();
          iter2.rewind();
        }
      }

      iter.remove();
      iter.rewind();
    }
  }

  /*
   * Now make sure unit is not on a connection between the first 2 nodes
   * or at the first node
   */

  if(rl.entries() >= 2)
  {
    RouteNode* node1 = rl.first();
    ASSERT(node1);

    ITown town1 = node1->d_town;

    Boolean remove = False;

    if(cpi->atTown())
    {
      remove = (town1 == cpi->getTown());
    }
    else
    {
      const Connection& con = campData->getConnection(cpi->getConnection());
      ITown town2 = rl.next()->d_town;

      remove = ( (con.node1 == town1 && con.node2 == town2) ||
                 (con.node1 == town2 && con.node2 == town1) );
    }

    if(remove)
      rl.remove(node1);
  }

  /*
   * Finally, remove any vias that are not in the route list
   */

  Boolean found = False;

  while(!found)
  {
    ITown viaTown = order.getVia(0);
    if(viaTown == NoTown)
      break;

    iter.rewind();
    while(++iter)
    {
      ITown town = iter.current()->d_town;
      if(town == viaTown)
      {
        found = True;
        break;
      }
    }

    if(!found)
      order.getVias()->remove(0);
  }
}

void CampaignUnitInterface::sendOrder(ICommandPosition cpi)
{
  ASSERT(cpi != NoCommandPosition);

  /*
   * if this is a move order, and we have via towns,
   * make sure the route doesn't double back on itself.
   */

  if(d_orderValues.isMoveOrder() && d_orderValues.getNVias() > 0)
  {
    makeRouteLogical(d_campData, cpi, d_orderValues);
  }

  sendUnitOrder(cpi, &d_orderValues);
  cancelOrder();
}

void CampaignUnitInterface::runOrders(const PixelPoint& p)
{
  if(d_stackedUnits.unitCount() > 0 && d_orderWindow)
  {
    UBYTE flag;

    if(d_mode != UnitInterfaceMode::Waiting)
    {
      flag = UnitOrder_Int::MoveOrder;
    }

    else
    {
      ASSERT(d_mode == UnitInterfaceMode::Waiting);

      d_mode = UnitInterfaceMode::SettingUpOrder;
      d_trackMode = MWTM_Unit;
      flag = UnitOrder_Int::FullOrder;
    }

    d_orderWindow->run(p, flag);
    d_campWind->redrawMap();
  }
}

void CampaignUnitInterface::runInfo(const PixelPoint& p)
{
    if(d_stackedUnits.unitCount() > 0 && d_infoWindow)
    {
        // Restructured to use if/else instead of ?:
        // to avoid compiler optimization bug

       UnitListID id;
        if( (d_unitID != NoUnitListID) &&
            (d_unitID < d_stackedUnits.unitCount()) )
        {
            id = d_unitID;
        }
        else
        {
            id = 0;
        }

       ICommandPosition cpi;
        if( (d_mode == UnitInterfaceMode::SettingUpOrder) ||
            (d_mode == UnitInterfaceMode::SettingUpMove) )
        {
            cpi = d_orderWindow->currentUnit();
        }
        else
        {
            cpi = d_stackedUnits.getUnit(id);
        }

       d_infoWindow->run(cpi, p);
    }
}

void CampaignUnitInterface::runOB(const PixelPoint& p)
{
  ASSERT(d_obWindow);
  d_obWindow->init(d_stackedUnits);
  d_obWindow->run(p);
}

void CampaignUnitInterface::cancelOrder()
{
  d_trackMode = MWTM_Unit;
  d_mode = UnitInterfaceMode::Waiting;
  d_orderValues.clearValues();
  d_stackedUnits.reset();

  d_campData->soundSystem()->TriggerSound(SOUNDTYPE_WINDOWCLOSE, CAMPAIGNSOUNDPRIORITY_INSTANT);

  d_campWind->redrawMap();
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
