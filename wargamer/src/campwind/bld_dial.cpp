/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bld_dial.hpp"
#include "palwind.hpp"
#include "app.hpp"
#include "campdint.hpp"
#include "dib.hpp"
#include "wmisc.hpp"
#include "res_str.h"
#include "simpstr.hpp"
#include "scenario.hpp"
#include "winctrl.hpp"
#include "palette.hpp"
#include "fonts.hpp"
#include "userint.hpp"
#include "itemwind.hpp"
#include "scn_img.hpp"
#include "scrnbase.hpp"
#include "resstr.hpp"

/*-------------------------------------------------------------------
 *
 */

// class to manage resource strings

class BuildString {
public:
  enum ID {
    Description,
    Time,
    MP,
    Res,
    Eff,
    Morale,

    NotBuilding,

    HowMany
  };

private:
  static Boolean s_initialized;
  static const int s_resourceIDs[HowMany];
  static SimpleString s_strings[];
public:
  static const char* text(ID id);

};

Boolean BuildString::s_initialized = False;
SimpleString BuildString::s_strings[BuildString::HowMany];

const int BuildString::s_resourceIDs[BuildString::HowMany] = {
   IDS_BID_DESCRIPTION,
   IDS_BID_TIME,
   IDS_BID_MP,
   IDS_BID_RES,
   IDS_BID_EFF,
   IDS_BID_MORALE,
   IDS_BID_NOTBUILDING
};

const char* BuildString::text(BuildString::ID id)
{
  ASSERT(id < HowMany);

  if(!s_initialized)
  {
    for(int i = 0; i < HowMany; i++)
    {
      idsToString(s_strings[i], s_resourceIDs[i]);
      ASSERT(s_strings[i].toStr());
    }

    s_initialized = True;
  }

  return s_strings[id].toStr();
}



class BuildItemWindow : public ItemWindow { //CustomBkWindow  {
    const CampaignData* d_campData;

    enum {
      First = 0,
      DescriptionCX = First,
      TimeCX,
      MPCX,
      ResCX,
      EffCX,
      MoraleCX,

      Items_HowMany,

      SpaceCX = 15
    };

    UWORD d_textCX[Items_HowMany];

  public:
    BuildItemWindow(const ItemWindowData& data, const CampaignData* campData, const SIZE& s);
    ~BuildItemWindow();

    void init(const void* data); //ICommandPosition cpi, CampaignOrder& order);
    void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
    void drawHeader(const DRAWITEMSTRUCT* lpDrawItem);
    void drawCombo(DrawDIBDC* dib, const void* data) {}

    void drawHeaderDib(int cx, int cy);
};


BuildItemWindow::BuildItemWindow(const ItemWindowData& data, const CampaignData* campData, const SIZE& s) :
  ItemWindow(data, s),
  d_campData(campData)
{
  for(int i = First; i < Items_HowMany; i++)
  {
    d_textCX[i] = 0;
  }
}

BuildItemWindow::~BuildItemWindow()
{
   selfDestruct();
   d_campData = 0;
}


void BuildItemWindow::init(const void* dp)
{
  ASSERT(dp);
  const BuildItemInterface::Data& d = *(reinterpret_cast<const BuildItemInterface::Data*>(dp));

  ASSERT(d.d_what < BasicUnitType::HowMany);

  // in case we have no dib
  if(!itemDib())
    allocateItemDib(10, 10);

  for(int i = First; i < Items_HowMany; i++)
  {
    d_textCX[i] = static_cast<UWORD>(ScreenBase::x(4) + textWidth(itemDib()->getDC(), BuildString::text(static_cast<BuildString::ID>(i))));
    // d_textCX[i] = static_cast<UWORD>(((4*dbX()) / 4) + textWidth(itemDib()->getDC(), BuildString::text(static_cast<BuildString::ID>(i))));
  }

  /*
   * Some useful values
   */

  const LONG offsetX = ScreenBase::x(2);    // (2*dbX())/4;

  /*
   * Fill List box
   */

  ListBox lb(getHWND(), ItemListBox);
  lb.reset();

  /*
   * Scan UnitType table for matching items
   */

  UnitType nTypes = d_campData->getMaxUnitType();

  UnitType best = NoUnitType;

  lb.add(BuildString::text(BuildString::NotBuilding), NoUnitType);

  for(UnitType t = 0; t < nTypes; t++)
  {
    const UnitTypeItem& unitType = d_campData->getUnitType(t);

    if(unitType.isNationality(d.d_nation) &&
      (unitType.getBasicType() == d.d_what) &&
      (lstrcmp("z", unitType.getName()) != 0) )
    {
      if(t == d.d_currentType)
        best = t;

      lb.add(unitType.getName(), t);

      /*
       * Set array for auto size of control
       */

      const UnitTypeItem& item = d_campData->getUnitType(t);

      /*
       * Get description length
       */

      LONG length = textWidth(itemDib()->getDC(), item.getName());
      d_textCX[DescriptionCX] = maximum(length, d_textCX[DescriptionCX]);
    }
  }

  lb.setValue(best);

  /*
   * Set size of list box
   */

  int itemsShowing = minimum(data().d_maxItemsShowing, lb.getNItems());
  const LONG itemHeight = data().d_itemCY*itemsShowing;

  int textCX = 0;
  for(i = 0; i < Items_HowMany; i++)
  {
    textCX += d_textCX[i];
  }

  LONG itemWidth = (2*offsetX)+((Items_HowMany - 1)*SpaceCX) + textCX;

  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, itemWidth, itemHeight, SWP_NOMOVE);
}

void BuildItemWindow::drawHeaderDib(int cx, int cy)
{
  /*
   * draw header text
   */

  const LONG offsetX = ScreenBase::x(2);    // *dbX())/4;

  TEXTMETRIC tm;
  GetTextMetrics(itemDib()->getDC(), &tm);

  LONG x = offsetX;
  LONG y = (cy - tm.tmHeight) / 2;

  // put 'Description'.
  const char* text = BuildString::text(BuildString::Description);
  wTextOut(itemDib()->getDC(), x, y, text);

  x += d_textCX[DescriptionCX] + SpaceCX;

  // put 'Time'
  text = BuildString::text(BuildString::Time);
  wTextOut(itemDib()->getDC(), x, y, text);

  x += d_textCX[TimeCX] + SpaceCX;

  // put 'MP'
  text = BuildString::text(BuildString::MP);
  wTextOut(itemDib()->getDC(), x, y, text);

  x += d_textCX[MPCX] + SpaceCX;

  // put 'Res'
  text = BuildString::text(BuildString::Res);
  wTextOut(itemDib()->getDC(), x, y, text);

  x += d_textCX[ResCX] + SpaceCX;

  // put 'Eff'
  text = BuildString::text(BuildString::Eff);
  wTextOut(itemDib()->getDC(), x, y, text);

  x += d_textCX[EffCX] + SpaceCX;

  // put 'Morale'
  text = BuildString::text(BuildString::Morale);
  wTextOut(itemDib()->getDC(), x, y, text);
}

void BuildItemWindow::drawHeader(const DRAWITEMSTRUCT* lpDrawItem)
{
  const itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
  const itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

  allocateItemDib(itemCX, itemCY);

  ASSERT(itemDib());
  ASSERT(fillDib());

  itemDib()->rect(0, 0, itemCX, itemCY, fillDib());

  drawHeaderDib(itemCX, itemCY);

  BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
         itemDib()->getDC(), 0, 0, SRCCOPY);

}

void BuildItemWindow::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  const itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
  const itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

  allocateItemDib(itemCX, itemCY);

  ASSERT(itemDib());
  ASSERT(fillDib());

  itemDib()->rect(0, 0, itemCX, itemCY, fillDib());

  if( !(lpDrawItem->itemState & ODS_SELECTED) )
    itemDib()->darkenRectangle(0, 0, itemCX, itemCY, 20);

  ListBox lb(lpDrawItem->hwndItem);
  int nItems = lb.getNItems();

  if(nItems > 0)
  {
    ASSERT(lpDrawItem->itemID < nItems);

    UnitType t = static_cast<UnitType>(lb.getValue(lpDrawItem->itemID));

    LONG x = ScreenBase::x(2); // (2*dbX())/4;
    LONG y = ScreenBase::y(2); // (2*dbY())/8;

    if(t != NoUnitType)
    {
      ASSERT(t < d_campData->getMaxUnitType());
      const UnitTypeItem& item = d_campData->getUnitType(t);

      SIZE s;
      GetTextExtentPoint32(itemDib()->getDC(), item.getName(), lstrlen(item.getName()), &s);

      y = (itemCY - s.cy) / 2;

      /*
       * Put description
       */

      wTextOut(itemDib()->getDC(), x, y, item.getName());

      /*
       * Put weeks
       */

      x += d_textCX[DescriptionCX] + SpaceCX;

      AttributePoints wRes;
      AttributePoints wMan;
      AttributePoints tRes;
      AttributePoints tMan;
      item.getWeeklyResource(wMan, wRes);
      item.getTotalResource(tMan, tRes);

      int weeks = (wMan > 0) ? tMan/wMan : 0;
      weeks = maximum(weeks, (wRes > 0) ? tRes/wRes : 0);

      wTextPrintf(itemDib()->getDC(), x, y, "%d %s",
         weeks,
         InGameText::get(IDS_WeekAbrev));

      /*
       * Put MP points i.e. total/weekly
       */

      x += d_textCX[TimeCX] + SpaceCX;;
      wTextPrintf(itemDib()->getDC(), x, y, "%d", static_cast<int>(tMan));

      /*
       * Put Res points i.e. total/weekly
       */

      x += d_textCX[MPCX] + SpaceCX;;
      wTextPrintf(itemDib()->getDC(), x, y, "%d", static_cast<int>(tRes));

      /*
       * Put Eff
       */

      x += d_textCX[ResCX] + SpaceCX;;
      SPValue spValue = item.getEffectiveValue();
      wTextPrintf(itemDib()->getDC(), x, y, "%d.%d",
        static_cast<int>(spValue/10), static_cast<int>(spValue%10));

      /*
       * Put morale graph
       */

      x += d_textCX[EffCX] + SpaceCX;;

      const int barCY = itemCY - 4;

      y = (itemCY - barCY) / 2;

      // const DrawDIB* barBkDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground);
      const DrawDIB* barBkDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::BarChartBackground);
      const DrawDIB* barFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::BarChartForeground);

      itemDib()->drawBarChart(barBkDib, barFillDib, x, y, item.getBaseMorale(), Attribute_Range, d_textCX[MoraleCX], barCY);
    }
    else
      wTextOut(itemDib()->getDC(), x, y, BuildString::text(BuildString::NotBuilding));
  }

  BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
         itemDib()->getDC(), 0, 0, SRCCOPY);
}

#if 0
/*----------------------------------------------------------------
 * Simple class to hold the actual instance of this window
 * There will be only one instance in the game
 */

class BuildItemInstance {
    BuildItemWindow* d_window;
  public:
    BuildItemInstance() :
      d_window(0) {}

    ~BuildItemInstance();

    void set(BuildItemWindow* window) { d_window = window; }
    BuildItemWindow* get() const { return d_window; }

    Boolean created() const { return (d_window != 0); }
};

BuildItemInstance::~BuildItemInstance()
{
  if(d_window)
  {
    delete d_window;
    d_window = 0;
  }
}

static BuildItemInstance s_buildItemInstance;

void BuildItemInterface::create(HWND hParent, UWORD id, const CampaignData* campData, const SIZE& s)
{
  const LONG dbUnits = GetDialogBaseUnits();

  if(!s_buildItemInstance.created())
  {
    ItemWindowData ld;

  /*
   * Set font
   */

    LogFont lf;
    lf.height(ScreenBase::y(8));   // (8 * ScreenBase::dbY()) / ScreenBase::baseY());
    lf.weight(FW_MEDIUM);
    lf.face(scenario->fontName(Font_Bold));
    lf.italic(false);
    lf.underline(false);

    Font font;
    font.set(lf);

    ld.d_hParent = hParent;
    ld.d_id = id;
    ld.d_scrollCX = 10;
    ld.d_hFont = font;
    ld.d_itemCY = ScreenBase::y(9);    // static_cast<UWORD>((9 * ScreenBase::dbY()) / ScreenBase::baseY());
    ld.d_maxItemsShowing = 10;
    ld.d_flags = ItemWindowData::HasScroll | ItemWindowData::ShadowBackground |
         ItemWindowData::TextHeader | ItemWindowData::OwnerDraw;
    ld.d_windowFillDib = scenario->getSideBkDIB(SIDE_Neutral);
    ld.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
    ld.d_scrollBtns = ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons);
    ld.d_scrollThumbDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground);
    ld.d_scrollFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground);
    ld.d_bColors = scenario->getBorderColors();

    BuildItemWindow* biw = new BuildItemWindow(ld, campData, s);
    ASSERT(biw);

    s_buildItemInstance.set(biw);
  }
}

void BuildItemInterface::init(const BuildItemInterface::Data& data)
{
  ASSERT(s_buildItemInstance.created());

  if(s_buildItemInstance.created())
  {
    s_buildItemInstance.get()->init(&data);
  }
}

void BuildItemInterface::show(PixelPoint& p)
{
  ASSERT(s_buildItemInstance.created());

  if(s_buildItemInstance.created())
  {
    s_buildItemInstance.get()->show(p);
    SetWindowPos(s_buildItemInstance.get()->getHWND(), HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
  }
}

void BuildItemInterface::hide()
{
  ASSERT(s_buildItemInstance.created());

  if(s_buildItemInstance.created())
  {
    s_buildItemInstance.get()->hide();
  }
}

UnitType BuildItemInterface::currentType()
{
  return static_cast<UnitType>(s_buildItemInstance.get()->currentValue());
}

#endif



BuildItemInterface::BuildItemInterface(HWND hParent, UWORD id, const CampaignData* campData, const SIZE& s)
{
  const LONG dbUnits = GetDialogBaseUnits();

   ItemWindowData ld;

   /*
   * Set font
   */

   LogFont lf;
   lf.height(ScreenBase::y(8));   // (8 * ScreenBase::dbY()) / ScreenBase::baseY());
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));
   lf.italic(false);
   lf.underline(false);

   Font font;
   font.set(lf);

   ld.d_hParent = hParent;
   ld.d_id = id;
   ld.d_scrollCX = 10;
   ld.d_hFont = font;
   ld.d_itemCY = ScreenBase::y(9);    // static_cast<UWORD>((9 * ScreenBase::dbY()) / ScreenBase::baseY());
   ld.d_maxItemsShowing = 10;
   ld.d_flags = ItemWindowData::HasScroll | ItemWindowData::ShadowBackground |
      ItemWindowData::TextHeader | ItemWindowData::OwnerDraw;
   ld.d_windowFillDib = scenario->getSideBkDIB(SIDE_Neutral);
   ld.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
   ld.d_scrollBtns = ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons);
   ld.d_scrollThumbDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground);
   ld.d_scrollFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground);
   ld.d_bColors = scenario->getBorderColors();

   d_imp = new BuildItemWindow(ld, campData, s);
   ASSERT(d_imp);
}

BuildItemInterface::~BuildItemInterface()
{
   delete d_imp;
   d_imp = 0;
}

void BuildItemInterface::init(const Data& data)
{
   if(d_imp)
      d_imp->init(&data);
}

void BuildItemInterface::show(PixelPoint& p)
{
   if (d_imp)
   {
    d_imp->show(p);
    SetWindowPos(d_imp->getHWND(), HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
   }
}

void BuildItemInterface::hide()
{
   if(d_imp)
      d_imp->hide();
}

UnitType BuildItemInterface::currentType()
{
   if(d_imp)
      return static_cast<UnitType>(d_imp->currentValue());
   else
      return NoUnitType;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
