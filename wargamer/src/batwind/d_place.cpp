/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "d_place.hpp"
#include "hexdata.hpp"
#include "hexmap.hpp"
#include "d_plcsde.hpp"
#include "batmap.hpp"
#include "pdeploy.hpp"
#include "bobutil.hpp"
#include "initunit.hpp"
#include "app.hpp"
#include "bud_info.hpp"
#include "batinfo.hpp"
#include "wmisc.hpp"
#include "bud_ordr.hpp"
#include "bud_info.hpp"
#include "batres.h"
#include "options.hpp"
#include "resstr.hpp"

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif


using namespace BattleMeasure;

DeployPlacementWindows::DeployPlacementWindows(BW_ModeOwner* user, const BattleInfo& batInfo) :
    BW_Mode(user),
    m_lpBattleInfo(batInfo),
    m_lpSideBar(0),
   m_TitleBar(0),
    m_MapWind(user->mapWindow()),
    m_lpBattleData(user->battleData()),
    m_BottomLeftPlayerDeployArea(),
    m_TopRightPlayerDeployArea(),
    m_DarkenedAreaTop(0),
    m_DarkenedAreaBottom(0),
    m_lpSelectedUnit(NoBattleCP),
    m_SelectedUnitHex(0,0),
   d_leftButtonDragging(false),
   d_rightButtonDragging(false),
    m_bUnitHighlighted(false),
    m_DeploymentHex(),
    m_DeploymentHexList(),
    d_orderWind(0),
    d_currentOrder(),
    d_orderInfo(),
    m_bIssuingOrders(false),
    d_BUIMode(BUI_UnitInterface::Normal) {


      d_hexVis = m_MapWind->getDisplay()->isHexVisibilityOn();

        /*
      set units' initial visibility
      */
        SetSideVisibility(m_lpBattleInfo.playerSide(), Visibility::Full);
        Side enemy_side = otherSide(m_lpBattleInfo.playerSide() );
//        SetSideVisibility(enemy_side, Visibility::NotSeen);
        SetSideVisibility(enemy_side, Visibility::NeverSeen);

      /*
      Initially, set all map as non-visible
      */
      HexCord mapsize = m_lpBattleData->map()->getSize();
      for(int y=0; y<mapsize.y(); y++) {
         for(int x=0; x<mapsize.x(); x++) {
            BattleTerrainHex& maphex = m_lpBattleData->map()->get(HexCord(x,y));
            maphex.d_visibility = Visibility::NeverSeen;
            maphex.d_lastFullVisUnit = NULL;
         }
      }


        /*
      Calc allowable deployment area for player
      */
      HexCord play_bottomleft;
      HexCord play_size;
      m_lpBattleData->getPlayingArea(&play_bottomleft, &play_size);
      HexCord play_topright = HexCord(
         play_bottomleft.x() + play_size.x(),
         play_bottomleft.y() + play_size.y()
      );

        int deploywidth = play_size.x();
      // Units must be deployable 9 to 12 hexes from opponents
        int deployheight = (play_size.y() / 2) - 5;

#if 0
      /*
      Shade in hexes on appropriate side
      */
        if(m_lpBattleInfo.playerSide() == 0) {

         m_BottomLeftPlayerDeployArea = play_bottomleft;
         m_TopRightPlayerDeployArea = HexCord(play_topright.x(), play_bottomleft.y() + deployheight);

         Visibility::Value vis = Visibility::Poor;
         for(int y=m_TopRightPlayerDeployArea.y(); y>=m_BottomLeftPlayerDeployArea.y(); y--) {
            for(int x=m_BottomLeftPlayerDeployArea.x(); x<m_TopRightPlayerDeployArea.x(); x++) {
               BattleTerrainHex& maphex = m_lpBattleData->map()->get(HexCord(x,y));
               maphex.d_visibility = vis;
               maphex.d_lastFullVisUnit = NULL;
            }
            if(vis < Visibility::Full) INCREMENT(vis);
         }
      }

        else {

         m_BottomLeftPlayerDeployArea = HexCord(0,(play_topright.y()-deployheight));
         m_TopRightPlayerDeployArea = HexCord(play_topright.x(), play_topright.y());

         Visibility::Value vis = Visibility::Poor;
         for(int y=m_BottomLeftPlayerDeployArea.y(); y<m_TopRightPlayerDeployArea.y(); y++) {
            for(int x=m_TopRightPlayerDeployArea.x(); x<m_TopRightPlayerDeployArea.x(); x++) {
               BattleTerrainHex& maphex = m_lpBattleData->map()->get(HexCord(x,y));
               maphex.d_visibility = vis;
               maphex.d_lastFullVisUnit = NULL;
            }
            if(vis < Visibility::Full) INCREMENT(vis);
         }
      }
#endif


      /*
      Shade in hexes (PLAYER ALWAYS PLAYS FROM BOTTOM UP)
      */
      m_BottomLeftPlayerDeployArea = play_bottomleft;
      m_TopRightPlayerDeployArea = HexCord(play_topright.x(), play_bottomleft.y() + deployheight);

      Visibility::Value vis = Visibility::Poor;
      for(y=m_TopRightPlayerDeployArea.y(); y>=m_BottomLeftPlayerDeployArea.y(); y--) {
         for(int x=m_BottomLeftPlayerDeployArea.x(); x<m_TopRightPlayerDeployArea.x(); x++) {
            BattleTerrainHex& maphex = m_lpBattleData->map()->get(HexCord(x,y));
            maphex.d_visibility = vis;
            maphex.d_lastFullVisUnit = NULL;
         }
         if(vis < Visibility::Full) INCREMENT(vis);
      }


        CreateWindows();

      /*
      * for this screen we always show the visibility hexes
      * since they represent the allowable deployment area
      * but we keep the visibility thread OFF
      */
      m_MapWind->getDisplay()->showHexVisibility(true);
      m_MapWind->stopVisibilityChecks();


    /*
    * Load colour cursors
    */
   if(Options::get(OPT_ColorCursor))
   {
      hCursor_Normal = LoadCursor(NULL, IDC_ARROW);
      ASSERT(hCursor_Normal);
      hCursor_OverUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_OVERUNIT_COLOR));
      ASSERT(hCursor_OverUnit);
      hCursor_DragUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_DRAGUNIT_COLOR));
      ASSERT(hCursor_DragUnit);
      hCursor_BadHex = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_BADHEX_COLOR));
      ASSERT(hCursor_BadHex);
      hCursor_TargetMode = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_TARGETMODE_COLOR));
      ASSERT(hCursor_TargetMode);
      hCursor_ValidTarget = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_VALIDTARGET_COLOR));
      ASSERT(hCursor_ValidTarget);
      hCursor_InvalidTarget = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_INVALIDTARGET_COLOR));
      ASSERT(hCursor_InvalidTarget);
      hCursor_AttatchUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_ATTATCHUNIT_COLOR));
      ASSERT(hCursor_AttatchUnit);
   }
    /*
    * Load monochrome cursors
    */
   else {
      hCursor_Normal = LoadCursor(NULL, IDC_ARROW);
      ASSERT(hCursor_Normal);
      hCursor_OverUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_OVERUNIT_MONO));
      ASSERT(hCursor_OverUnit);
      hCursor_DragUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_DRAGUNIT_MONO));
      ASSERT(hCursor_DragUnit);
      hCursor_BadHex = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_BADHEX_MONO));
      ASSERT(hCursor_BadHex);
      hCursor_TargetMode = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_TARGETMODE_MONO));
      ASSERT(hCursor_TargetMode);
      hCursor_ValidTarget = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_VALIDTARGET_MONO));
      ASSERT(hCursor_ValidTarget);
      hCursor_InvalidTarget = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_INVALIDTARGET_MONO));
      ASSERT(hCursor_InvalidTarget);
      hCursor_AttatchUnit = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_BAT_ATTATCHUNIT_MONO));
      ASSERT(hCursor_AttatchUnit);
   }

   d_currentCursorMode = Cursor_Normal;
   d_desiredCursorMode = Cursor_Normal;


}

DeployPlacementWindows::~DeployPlacementWindows()
{

   if(m_TitleBar) delete m_TitleBar;
    delete m_lpSideBar;
    m_lpSideBar = 0;
    delete d_orderWind;
    d_orderWind = 0;
}

void DeployPlacementWindows::positionWindows(const PixelRect& r, DeferWindowPosition* def)
{
    // if(def)
    //     def->setPos(d_owner->mapWindow()->getHWND(), HWND_TOP, r.left(), r.top(), r.width(), r.height(), 0);
    // else
    //     SetWindowPos(d_owner->mapWindow()->getHWND(), HWND_TOP, r.left(), r.top(), r.width(), r.height(), SWP_SHOWWINDOW);

    // m_WindowRect.left = rect.left;
    // m_WindowRect.top = rect.top;
    // m_WindowRect.right = rect.right;
    // m_WindowRect.bottom = rect.bottom;
    //
    // if(getHWND()) {
    //     MoveWindow(getHWND(), m_WindowRect.left, m_WindowRect.top, m_WindowRect.right, m_WindowRect.bottom, TRUE);
    // }
    //
    // int xpos, ypos, width, height;

    int width = (r.width() * 5) / 6;
   int title_height = (r.height() / 16);

    if(m_MapWind && m_MapWind->getHWND())
    {
        if(def)
            def->setPos(
            m_MapWind->getHWND(),
            HWND_TOP,
            r.left(),
            r.top() + title_height,
            width,
            r.height() - title_height,
            0
         );
        else
            SetWindowPos(
            m_MapWind->getHWND(),
            HWND_TOP,
            r.left(),
            r.top() + title_height,
            width,
            r.height() - title_height,
            SWP_SHOWWINDOW
         );
        redrawMap(true);
    }

    if(m_lpSideBar)
    {
        PixelRect rSide = r;
        rSide.left(r.left() + width);
      rSide.top(r.top() + title_height);

        m_lpSideBar->SetSize(rSide);
    }

   if(m_TitleBar) {
      m_TitleBarRect.top = r.top();
      m_TitleBarRect.left = r.left();
      m_TitleBarRect.right = r.width();
      m_TitleBarRect.bottom = title_height;
      m_TitleBar->setSize(&m_TitleBarRect);
   }

}


void DeployPlacementWindows::SetSideVisibility(Side side, Visibility::Value visibility)
{
    BattleCP* cp = m_lpBattleData->ob()->getTop(side);

    if(cp)
    {
        cp->visibility(visibility);
        BattleSP * sp = cp->sp();
        while(sp)
        {
            sp->visibility(visibility);
            sp = sp->sister();
        }

        if(cp->child() != NoBattleCP)
             SetChildVisibility(cp->child(), visibility);
    }
}

void DeployPlacementWindows::SetChildVisibility(BattleCP* cp, Visibility::Value visibility)
{
    while(cp != NoBattleCP)
    {
       cp->visibility(visibility);
       BattleSP * sp = cp->sp();
       while(sp)
       {
          sp->visibility(visibility);
          sp = sp->sister();
       }

       if(cp->child() != NoBattleCP)
          SetChildVisibility(cp->child(), visibility );

       cp = cp->sister();
    }

}



void DeployPlacementWindows::CreateWindows()
{
    // SetWindowText(owner()->hwnd(), "DeployPlacementWindows");

    // m_MapWind = new BattleMapWind(this, m_lpBattleData);
    // m_MapWind->create();

    // set map to far zoom
    setMapMode(BattleMapInfo::FourMile);

    // center map
   HexCord play_bottomleft;
   HexCord play_size;
   m_lpBattleData->getPlayingArea(&play_bottomleft, &play_size);

   HexCord center_hex(
      play_bottomleft.x() + (play_size.x()/2),
      play_bottomleft.y() + (play_size.y()/2)
   );
    setMapLocation(BattleLocation(xHexes(center_hex.x()), yHexes(center_hex.y()) ) );

   // show hex visibility
   if(m_MapWind->isVisibilityRunning()) m_MapWind->stopVisibilityChecks();
   m_MapWind->getDisplay()->showHexVisibility(true);

    // m_lpSideBar = new PlacementSideWindow(m_lpWindowChangeUser, this, owner()->hwnd(), m_lpBattleData, m_lpBattleInfo.PlayerSide);
    m_lpSideBar = new PlacementSideWindow(this, m_lpBattleInfo.playerSide() );
    d_orderWind = new BattleOrder_Int(this, m_lpBattleData, d_currentOrder);

   m_TitleBarRect.left = 0;
   m_TitleBarRect.top = 0;
   m_TitleBarRect.right  = 128;
   m_TitleBarRect.bottom = 32;

   m_TitleBar = new TitleBarClass(d_owner->hwnd(), TitleBarClass::Normal, &m_TitleBarRect);
   m_TitleBar->setText(InGameText::get(IDS_TitleTroopDeployment)); // "Wargamer : Napoleon 1813 - Troop Deployment");
   m_TitleBar->displayBar();
}


void DeployPlacementWindows::show(bool visible)
{
    if(m_MapWind) m_MapWind->show(visible);
    if(m_lpSideBar) m_lpSideBar->show(visible);
}




























void
DeployPlacementWindows::CopyOrderFromCP(BattleCP * cp) {

    BattleOrder order = cp->getCurrentOrder();

    d_orderInfo.d_mode = order.mode();
    d_orderInfo.d_aggression = order.aggression();
    d_orderInfo.d_posture = order.posture();
    d_orderInfo.d_spFormation = order.spFormation();
    d_orderInfo.d_divFormation = order.divFormation();
    d_orderInfo.d_deployHow = order.deployHow();
    d_orderInfo.d_lineHow = order.lineHow();
    d_orderInfo.d_manuever = order.manuever();
    d_orderInfo.d_facing = order.facing();

}



void
DeployPlacementWindows::CopyOrderToCP(BattleCP * cp) {

    BattleOrder previous_order;
    previous_order = cp->getCurrentOrder();

    BattleOrder order;

    order.mode(d_orderInfo.d_mode);
    order.aggression(d_orderInfo.d_aggression);
    order.posture(d_orderInfo.d_posture);
    order.spFormation(d_orderInfo.d_spFormation);
    order.divFormation(d_orderInfo.d_divFormation);
    order.deployHow(d_orderInfo.d_deployHow);
    order.lineHow(d_orderInfo.d_lineHow);
    order.manuever(d_orderInfo.d_manuever);
    order.facing(d_orderInfo.d_facing);
//    order.whatOrder(d_orderInfo.d_whatOrder);

    // set order
    cp->setCurrentOrder(order);

    // set all sp formations
    BattleSP * sp = cp->sp();
    while(sp) {
        sp->setFormation(d_orderInfo.d_spFormation);
        sp = sp->sister();
    }

    // reset deployment hex highlights
    SListIterR<HexItem> iter(&m_DeploymentHexList);
    while(++iter) {
        m_lpBattleData->map()->unhighlightHex(iter.current()->d_hex);
    }

   HighlightUnits(false);
   m_bUnitHighlighted = false;

    // test deployment to new location
    bool result;
    HexList hexlist;
//    result = doPlayerDeploy(m_lpBattleData, m_lpSelectedUnit, m_SelectedUnitHex, &hexlist);
    result = PlayerDeploy::playerDeploy(m_lpBattleData, m_lpSelectedUnit, m_SelectedUnitHex, &hexlist, PlayerDeploy::FILLHEXLIST);

    // if deployment was successful
    if(result) {

        // remove units from map
        if(m_lpSelectedUnit->sp() ) RemoveFromMap(m_lpSelectedUnit, true);
        else RemoveFromMap(m_lpSelectedUnit, false);

        // deploy at destination
        //doPlayerDeploy(m_lpBattleData, m_lpSelectedUnit, m_SelectedUnitHex, 0);
        PlayerDeploy::playerDeploy(m_lpBattleData, m_lpSelectedUnit, m_SelectedUnitHex, 0, PlayerDeploy::INITIALIZE);

/*
        // bodge code to fix independent CP deployment bug
        if(m_lpSelectedUnit->getRank().isHigher(Rank_Division)  ) {
            BattleHexMap::iterator lower_iter;
            BattleHexMap::iterator upper_iter;
            if(m_lpBattleData->hexMap()->find(m_SelectedUnitHex, lower_iter, upper_iter)) m_lpBattleData->hexMap()->remove(lower_iter);
        }
*/
        // deselect item in treeview
        m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

        // reset issuing orders flag
        m_bIssuingOrders = false;

        // clear selected unit's hex
        m_SelectedUnitHex = HexCord(0,0);

        // deselect unit
        m_lpSelectedUnit = NULL;
/*
        // reset deployment hex highlights
        SListIterR<HexItem> iter(&m_DeploymentHexList);
        while(++iter) {
            m_lpBattleData->map()->unhighlightHex(iter.current()->d_hex);
        }
*/
  // clear deployment hex
        m_DeploymentHex = HexCord(0,0);

        // clear deployment hex list
        m_DeploymentHexList.reset();

        redrawMap(false);
    }

    else cp->setCurrentOrder(previous_order);
}



    /*
    Orders Windows & Battle-Unit-Interface Functions
    */


void
DeployPlacementWindows::sendOrder() {

    if(!m_lpSelectedUnit) return;

    CopyOrderToCP(m_lpSelectedUnit);

}




void
DeployPlacementWindows::cancelOrder() {

   m_lpBattleData->soundSystem()->triggerSound(SOUNDTYPE_WINDOWCLOSE);
   //d_orderWind->hide();
}

void
DeployPlacementWindows::mode(Mode m) {

}

BUI_UnitInterface::Mode
DeployPlacementWindows::mode() const {

    return d_BUIMode;

}

void
DeployPlacementWindows::showOrders() {

}

void
DeployPlacementWindows::showDeployment() {

}

void
DeployPlacementWindows::showInfo() {

}

void
DeployPlacementWindows::redrawMap() {

    redrawMap(false);

}


void
DeployPlacementWindows::highlightUnit(bool on) {

    // turn of highlights
    if(m_bUnitHighlighted) {
        HighlightUnits(false);
        m_bUnitHighlighted = false;
    }

   m_DeploymentHexList.reset();

   HexCord hex_offset(m_DeploymentHex.x() - d_deploymentMouseOffset.x(), m_DeploymentHex.y() - d_deploymentMouseOffset.y() );

    const BattleData * bd = m_lpBattleData;
// doPlayerDeploy(const_cast<BattleData *>(bd), m_lpSelectedUnit, hex_offset, &m_DeploymentHexList);
    bool result = PlayerDeploy::playerDeploy(m_lpBattleData, m_lpSelectedUnit, hex_offset, &m_DeploymentHexList, PlayerDeploy::FILLHEXLIST | PlayerDeploy::NEVERFAIL);

    //highlightUnitWithoutDeploy(on);
    HighlightUnits(true);
    m_bUnitHighlighted = true;

}













/*
Wrapper to insulate deployment calls
*/
/*
bool
DeployPlacementWindows::doPlayerDeploy(RPBattleData batData, const RefBattleCP& cp, const HexCord& startHex, HexList* hl) {

    return(B_Logic::playerDeploy(batData, cp, startHex, hl) );

}
*/



void
DeployPlacementWindows::HighlightUnits(bool state) {

    BattleCP * top_cp = m_lpSelectedUnit;

    if(! top_cp->sp() ) {
        if(state) m_lpBattleData->map()->highlightHex(top_cp->hex(), top_cp->getSide() | HIGHLIGHT_SET);
        else m_lpBattleData->map()->unhighlightHex(top_cp->hex() );
    }

    if(top_cp->sp() ) HighlightSP(top_cp->sp(), state);

    if(top_cp->child() ) HighlightCP(top_cp->child(), state);

}

void
DeployPlacementWindows::HighlightCP(BattleCP * cp, bool state) {

    if(! cp->sp() ) {
        if(state) m_lpBattleData->map()->highlightHex(cp->hex(), cp->getSide() | HIGHLIGHT_SET);
        else m_lpBattleData->map()->unhighlightHex(cp->hex() );
    }

    if(cp->sp() ) HighlightSP(cp->sp(), state);

    if(cp->child() ) HighlightCP(cp->child(), state);

    if(cp->sister() ) HighlightCP(cp->sister(), state);

}

void
DeployPlacementWindows::HighlightSP(BattleSP * sp, bool state) {

    if(state) m_lpBattleData->map()->highlightHex(sp->hex(), sp->parent()->getSide() | HIGHLIGHT_SET);
    else m_lpBattleData->map()->unhighlightHex(sp->hex() );

    if(sp->sister() ) HighlightSP(sp->sister(), state);

}



void
DeployPlacementWindows::RemoveFromMap(BattleCP * cp, bool removechildren) {

    if(m_lpBattleData->hexMap()->isUnitPresent(cp) ) {
        BattleHexMap::iterator map_iter = m_lpBattleData->hexMap()->find(cp);
        m_lpBattleData->hexMap()->remove(map_iter);
    }

    if(! removechildren) return;

    if(cp->sp() ) RemoveSP(cp->sp() );

    if(cp->child() ) RemoveCP(cp->child() );

}



void
DeployPlacementWindows::RemoveCP(BattleCP * cp) {

    if(m_lpBattleData->hexMap()->isUnitPresent(cp) ) {
        BattleHexMap::iterator map_iter = m_lpBattleData->hexMap()->find(cp);
        m_lpBattleData->hexMap()->remove(map_iter);
    }

    if(cp->sp() ) RemoveSP(cp->sp() );

    if(cp->child() ) RemoveCP(cp->child() );

    if(cp->sister() ) RemoveCP(cp->sister() );


}


void
DeployPlacementWindows::RemoveSP(BattleSP * sp) {

    while(sp) {

        if(m_lpBattleData->hexMap()->isUnitPresent(sp) ) {
            BattleHexMap::iterator map_iter = m_lpBattleData->hexMap()->find(sp);
            m_lpBattleData->hexMap()->remove(map_iter);
        }

    sp = sp->sister();
    }

}



/*

OB Treevioew interface functions

*/

void DeployPlacementWindows::onSelectUnit(BattleUnit * unit) {

    BattleCP * old_cp = m_lpSelectedUnit;

    // if unit already selected
    if(m_lpSelectedUnit) {

        // remove highlights
        if(m_bUnitHighlighted) {
            HighlightUnits(false);
            m_bUnitHighlighted = false;
        }

        // reset dragging flags
        d_leftButtonDragging = false;
      d_rightButtonDragging = false;

        // clear selected unit's hex
        m_SelectedUnitHex = HexCord(0,0);

        // clear selected unit
        m_lpSelectedUnit = NULL;
    }

    // ensure we are referencing a CP
    BattleCP * cp = dynamic_cast<BattleCP *>(unit);
    if(! cp) {
        BattleSP * sp = dynamic_cast<BattleSP *>(unit);
        ASSERT(sp);
        cp = sp->parent();
        ASSERT(cp);
    }

    // set selected unit
    m_lpSelectedUnit = cp;

    // highlight unit's hexes
    HighlightUnits(!!cp);
    m_bUnitHighlighted = true;

    // reset dragging flags
    d_leftButtonDragging = false;
   d_rightButtonDragging = false;

    redrawMap(false);

}









/*
BattleMap Interface Functions
*/




// Set centre of main map, return true if has changed
bool
DeployPlacementWindows::setMapLocation(const BattleLocation& l) {
    bool flag = false;

    if(m_MapWind ){
        flag = m_MapWind->setLocation(l);
    }
    return flag;
}




// sent by mapwind when Button is pressed
void
DeployPlacementWindows::onLButtonDown(const BattleMapSelect& info) {

    /*
    If we were issuing orders
    */
    if(m_bIssuingOrders) {

            // get rid of dialog
            d_orderWind->hide();

            // turn of highlights
            if(m_bUnitHighlighted) {
                HighlightUnits(false);
                m_bUnitHighlighted = false;
            }

            m_bIssuingOrders = false;

            // deselect item in treeview
            m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

            // clear selected unit
            m_lpSelectedUnit = NULL;

            // clear selected unit's hex
            m_SelectedUnitHex = HexCord(0,0);

            // clear deployment hex
            m_DeploymentHex = HexCord(0,0);

            // clear deployment hex list
            m_DeploymentHexList.reset();

            redrawMap(false);
    }

    /*
    Check if mouse if over a unit
    */

    HexCord hex = m_lpBattleData->getHex(info.location(), BattleData::Mid);
    BattleHexMap::iterator lower_iter;
    BattleHexMap::iterator upper_iter;

    /*
    If mouse not over unit
    */

    if(! m_lpBattleData->hexMap()->find(hex, lower_iter, upper_iter)) {

        // if we already have a unit selected
        if(m_lpSelectedUnit) {

            // turn of highlights
            if(m_bUnitHighlighted) {
                HighlightUnits(false);
                m_bUnitHighlighted = false;
            }

            // reset dragging flag
            d_leftButtonDragging = false;

            // deselect item in treeview
            m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

            // clear selected unit
            m_lpSelectedUnit = NULL;

            // clear selected unit's hex
            m_SelectedUnitHex = HexCord(0,0);

            // clear deployment hex
            m_DeploymentHex = HexCord(0,0);

            // clear deployment hex list
            m_DeploymentHexList.reset();

            redrawMap(false);

            return;
        }
    }


    /*
    Mouse is over a unit
    */

    else {

        // if we already have a selected unit, deselect it
        if(m_lpSelectedUnit) {

            // turn of highlights
            if(m_bUnitHighlighted) {
                HighlightUnits(false);
                m_bUnitHighlighted = false;
            }

            // reset dragging flag
            d_leftButtonDragging = false;

            // deselect item in treeview
            m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

            // clear selected unit
            m_lpSelectedUnit = NULL;

            // clear selected unit's hex
            m_SelectedUnitHex = HexCord(0,0);
        }

        BattleUnit* unit = m_lpBattleData->hexMap()->unit(lower_iter);

        // ensure we are refenencing a CP
        BattleCP * cp = dynamic_cast<BattleCP *>(unit);
        BattleSP * sp;
        if(! cp) {
            sp = dynamic_cast<BattleSP *>(unit);
            ASSERT(sp);
            cp = sp->parent();
        }
        ASSERT(cp);

        // select this unit
        m_lpSelectedUnit = cp;

        // set selected unit's hex
        m_SelectedUnitHex = hex;

        // set dragging flag
        d_leftButtonDragging = true;
      d_rightButtonDragging = false;

        // set deployment hex
        m_DeploymentHex = hex;
        // set up initial dpeloyment hex list
        m_DeploymentHexList.reset();

      /*
      Setup hex-offsets
      */
        // DeployItem * di = m_lpSelectedUnit->mapBegin();
        std::vector<DeployItem>::iterator di = m_lpSelectedUnit->mapBegin();
        // if(di) {
        if(di != m_lpSelectedUnit->mapEnd())
        {
          HexCord di_hex = di->d_hex;
          d_deploymentMouseOffset.x( m_DeploymentHex.x() - di_hex.x() );
          d_deploymentMouseOffset.y( m_DeploymentHex.y() - di_hex.y() );
        }
        else d_deploymentMouseOffset = HexCord(0,0);


        //bool result = doPlayerDeploy(m_lpBattleData, m_lpSelectedUnit, hex, &m_DeploymentHexList);
      bool result = PlayerDeploy::playerDeploy(m_lpBattleData, m_lpSelectedUnit, hex, &m_DeploymentHexList, PlayerDeploy::FILLHEXLIST);

        // highlight units
        HighlightUnits(true);
        m_bUnitHighlighted = true;

        // select item in treeview
        m_lpSideBar->SelectUnit(m_lpSelectedUnit);

        redrawMap(false);

    }

}


// sent by mapwind when Button is released
void
DeployPlacementWindows::onLButtonUp(const BattleMapSelect& info) {

    /*
    Mouse released during a drag operation
    */
    if(d_leftButtonDragging) {

        HexCord hex = m_lpBattleData->getHex(info.location(), BattleData::Mid);

        // if target hex is different from source hex
        if(hex != m_SelectedUnitHex) {

            // remove highlighting
            if(m_bUnitHighlighted) {
                HighlightUnits(false);
                m_bUnitHighlighted = false;
            }

            // check that target hex is within the allowable deployment area for player (1/3 of map)
            bool within_deploy_area = false;
            if(
                hex.x() >= m_BottomLeftPlayerDeployArea.x() &&
                hex.y() >= m_BottomLeftPlayerDeployArea.y() &&
                hex.x() <= m_TopRightPlayerDeployArea.x() &&
                hex.y() <= m_TopRightPlayerDeployArea.y()
            ) within_deploy_area = true;

         /*
         Get hex coords
         */
         HexCord hex_offset(hex.x() - d_deploymentMouseOffset.x(), hex.y() - d_deploymentMouseOffset.y() );

            // test deployment to new location
            bool result;
            HexList hexlist;
            //result = doPlayerDeploy(m_lpBattleData, m_lpSelectedUnit, hex, &hexlist);
         result = PlayerDeploy::playerDeploy(m_lpBattleData, m_lpSelectedUnit, hex_offset, &hexlist, PlayerDeploy::FILLHEXLIST);

            // if deployment was successful
            if(within_deploy_area && result) {

                // remove units from map
                if(m_lpSelectedUnit->sp() ) RemoveFromMap(m_lpSelectedUnit, true);
                else RemoveFromMap(m_lpSelectedUnit, false);

                // deploy at destination
                //doPlayerDeploy(m_lpBattleData, m_lpSelectedUnit, hex, 0);
            PlayerDeploy::playerDeploy(m_lpBattleData, m_lpSelectedUnit, hex_offset, 0, PlayerDeploy::INITIALIZE);

                // bodge code to fix independent CP deployment bug
/*
                if(m_lpSelectedUnit->getRank().isHigher(Rank_Division)  ) {
                    BattleHexMap::iterator lower_iter;
                    BattleHexMap::iterator upper_iter;
                    if(m_lpBattleData->hexMap()->find(m_SelectedUnitHex, lower_iter, upper_iter))
                        m_lpBattleData->hexMap()->remove(lower_iter);
                }
*/

            }

            // deselect item in treeview
            m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

            // reset dragging flag
            d_leftButtonDragging = false;

            // clear selected unit's hex
            m_SelectedUnitHex = HexCord(0,0);

            // deselect unit
            m_lpSelectedUnit = NULL;

            // reset deployment hex highlights
            SListIterR<HexItem> iter(&m_DeploymentHexList);
            while(++iter) {
                m_lpBattleData->map()->unhighlightHex(iter.current()->d_hex);
            }

            // clear deployment hex
            m_DeploymentHex = HexCord(0,0);

            // clear deployment hex list
            m_DeploymentHexList.reset();

            redrawMap(false);

         /*
         force mouse to be redrawn
         */
         onMove(info);
        }
    }

    // reset dragging flag regardless, when left button goes up
    d_leftButtonDragging = false;

}




// sent by mapwind when Button is pressed
void
DeployPlacementWindows::onRButtonDown(const BattleMapSelect& info) {

    /*
    Check if mouse if over a unit
    */

    HexCord hex = m_lpBattleData->getHex(info.location(), BattleData::Mid);
    BattleHexMap::iterator lower_iter;
    BattleHexMap::iterator upper_iter;

    /*
    If mouse not over unit
    */

    if(! m_lpBattleData->hexMap()->find(hex, lower_iter, upper_iter)) {

        // if we already have a unit selected
        if(m_lpSelectedUnit && (!d_leftButtonDragging)) {

            // turn of highlights
            if(m_bUnitHighlighted) {
                HighlightUnits(false);
                m_bUnitHighlighted = false;
            }

            // reset dragging flag
            d_rightButtonDragging = false;

            m_bIssuingOrders = false;

            // deselect item in treeview
            m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

            // clear selected unit
            m_lpSelectedUnit = NULL;

            // clear selected unit's hex
            m_SelectedUnitHex = HexCord(0,0);

            // clear deployment hex
            m_DeploymentHex = HexCord(0,0);

            // clear deployment hex list
            m_DeploymentHexList.reset();

            redrawMap(false);

            return;
        }
    }


    /*
    Mouse is over a unit
    */

    else {

        // if we already have a selected unit, deselect it
        if(m_lpSelectedUnit && (!d_leftButtonDragging)) {

            // turn of highlights
            if(m_bUnitHighlighted) {
                HighlightUnits(false);
                m_bUnitHighlighted = false;
            }

            // reset dragging flag
            d_rightButtonDragging = false;

            m_bIssuingOrders = false;

            // deselect item in treeview
            m_lpSideBar->DeselectUnit(m_lpSelectedUnit);

            // clear selected unit
            m_lpSelectedUnit = NULL;

            // clear selected unit's hex
            m_SelectedUnitHex = HexCord(0,0);
        }

        BattleUnit * unit = m_lpBattleData->hexMap()->unit(lower_iter);

        // ensure we are refenencing a CP
        BattleCP * cp = dynamic_cast<BattleCP *>(unit);
        BattleSP * sp;
        if(! cp) {
            sp = dynamic_cast<BattleSP *>(unit);
            ASSERT(sp);
            cp = sp->parent();
        }
        ASSERT(cp);

        // select this unit
        m_lpSelectedUnit = cp;

        // set selected unit's hex
        m_SelectedUnitHex = hex;

        // set dragging flag to false
        d_rightButtonDragging = false;

        // set orders flaf to true
        m_bIssuingOrders = true;

        // set deployment hex
        m_DeploymentHex = hex;

        // set up initial dpeloyment hex list
        m_DeploymentHexList.reset();
        bool result = PlayerDeploy::playerDeploy(m_lpBattleData, m_lpSelectedUnit, hex, &m_DeploymentHexList, PlayerDeploy::FILLHEXLIST);

        // highlight units
        HighlightUnits(true);
        m_bUnitHighlighted = true;

        // select item in treeview
        m_lpSideBar->SelectUnit(m_lpSelectedUnit);

        /*
        Set up orders dialog info
        */
        BUD_Info budinfo;
        budinfo.setup(m_lpBattleData, info);
        budinfo.cp(m_lpSelectedUnit);
        budinfo.hex(hex);

        CopyOrderFromCP(m_lpSelectedUnit);


      m_lpBattleData->soundSystem()->triggerSound(SOUNDTYPE_WINDOWOPEN);
        BattleLocation loc(hex.x() * BattleMeasure::QuantaPerXHex, hex.y() * BattleMeasure::QuantaPerYHex);
        d_orderWind->run(m_MapWind->getDisplay()->locationToPixel(loc), budinfo, &d_orderInfo);

        redrawMap(false);
    }

}

// sent by mapwind when Button is released while not dragging
void
DeployPlacementWindows::onRButtonUp(const BattleMapSelect& info) {
//    if(d_unitUI.get()) d_unitUI->onRButtonUp(info);
}

// sent by mapwind when Mouse is moved while button is held
void
DeployPlacementWindows::onStartDrag(const BattleMapSelect& info) {
//    if(d_unitUI.get()) d_unitUI->onStartDrag(info);
}

// sent by mapwind when button is released while dragging
void
DeployPlacementWindows::onEndDrag(const BattleMapSelect& info) {

}

// sent by mapwind when Mouse has moved
void
DeployPlacementWindows::onMove(const BattleMapSelect& info) {

    HexCord hex = m_lpBattleData->getHex(info.location(), BattleData::Mid);

    /*
    If we are currently dragging a unit
    */
    if(d_leftButtonDragging) {

        // if hex is different from previous one
        if(hex != m_DeploymentHex) {

            // reset hex highlights
            SListIterR<HexItem> iter(&m_DeploymentHexList);
            while(++iter) {
                m_lpBattleData->map()->unhighlightHex(iter.current()->d_hex);
                if(! m_lpSelectedUnit->sp() ) {
                    m_lpBattleData->map()->unhighlightHex(m_DeploymentHex);
                }
            }

            // clear hex list
            m_DeploymentHexList.reset();

         /*
         Get hex coords
         */
         HexCord hex_offset(hex.x() - d_deploymentMouseOffset.x(), hex.y() - d_deploymentMouseOffset.y() );

         bool result = PlayerDeploy::playerDeploy(m_lpBattleData, m_lpSelectedUnit, hex_offset, &m_DeploymentHexList, PlayerDeploy::FILLHEXLIST | PlayerDeploy::NEVERFAIL);
/*
                // bodge code to fix independent CP deployment bug
                if(m_lpSelectedUnit->getRank().isHigher(Rank_Division) ) {
                    BattleHexMap::iterator lower_iter;
                    BattleHexMap::iterator upper_iter;
                    if(m_lpBattleData->hexMap()->find(hex, lower_iter, upper_iter)) {
                        m_lpBattleData->hexMap()->remove(lower_iter);
                        m_lpBattleData->map()->unhighlightHex(hex );
                    }
                }
*/

         m_lpBattleData->map()->unhighlightHex(m_lpSelectedUnit->hex() );

            // set hex highlights
            iter = SListIterR<HexItem>(&m_DeploymentHexList);
            while(++iter) {
                m_lpBattleData->map()->highlightHex(iter.current()->d_hex, m_lpSelectedUnit->getSide() | HIGHLIGHT_SET);
            }

            // highlight units, to ensure not reset by mouse movement
            HighlightUnits(true);
            m_bUnitHighlighted = true;

            // set new value form deployment hex
            m_DeploymentHex = hex;

            redrawMap(false);
        }

      /*
      * Set cursor to indicate Dragging
      */
      d_desiredCursorMode = Cursor_DragUnit;
      setCursor();

      return;
   }


   /*
   * We're not dragging, but we're over a potential unit
   */
   BattleHexMap::const_iterator lower_iter;
    BattleHexMap::const_iterator upper_iter;

   const BattleData * bd = m_lpBattleData;
//   if(const_cast<BattleData *>(bd)->hexMap()->find(hex, lower_iter, upper_iter)) {
   if(bd->hexMap()->find(hex, lower_iter, upper_iter)) {

      const BattleUnit * unit = m_lpBattleData->hexMap()->unit(lower_iter);

      // ensure we are refenencing a CP
      const BattleCP * cp = dynamic_cast<const BattleCP *>(unit);

      if(! cp) {
         const BattleSP * sp;
         sp = dynamic_cast<const BattleSP *>(unit);
         ASSERT(sp);
         cp = sp->parent();
      }
      ASSERT(cp);

      // if the unit belongs to player's side, then change cursor
      if(GamePlayerControl::canControl(cp->getSide())) {

         // OverUnit cursor
         d_desiredCursorMode = Cursor_OverUnit;
         setCursor();
         return;
      }
   }

   /*
   * just the Normal cursor
   */
   d_desiredCursorMode = Cursor_Normal;
   setCursor();
   return;
}




bool
DeployPlacementWindows::setCursor(void) {

   if(d_currentCursorMode == d_desiredCursorMode) return true;

   d_currentCursorMode = d_desiredCursorMode;
//   HCURSOR cursor;

   switch(d_currentCursorMode) {

      case Cursor_Normal : {

         SetCursor(hCursor_Normal);
         return true;
      }

      case Cursor_OverUnit : {

         SetCursor(hCursor_OverUnit);
         return true;
      }

      case Cursor_DragUnit : {

         SetCursor(hCursor_DragUnit);
         return true;
      }

      case Cursor_BadHex : {

         SetCursor(hCursor_BadHex);
         return true;
      }

      case Cursor_TargetMode : {

         SetCursor(hCursor_TargetMode);
         return true;
      }

      case Cursor_ValidTarget : {

         SetCursor(hCursor_ValidTarget);
         return true;
      }

      case Cursor_InvalidTarget : {

         SetCursor(hCursor_InvalidTarget);
         return true;
      }

      case Cursor_AttatchUnit : {

         SetCursor(hCursor_AttatchUnit);
         return true;
      }

      default : {
         FORCEASSERT("Oops - DeploymentScreen trying to set cursor for mode which doesn't exist.\n");
         return true;
      }
   }

}

void
DeployPlacementWindows::redrawMap(bool all) {
    m_MapWind->update(all);
}



void
DeployPlacementWindows::setMapMode(BattleMapInfo::Mode mode) {

    if(m_MapWind) {
        m_MapWind->mode(mode);
    }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.5  2002/11/16 18:03:30  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.4  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
