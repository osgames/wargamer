/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Time Control Window
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "bclock.hpp"
#include "btimctrl.hpp"
#include "timewind.hpp"
#include "misc.hpp"
#include "scenario.hpp"
#include "resstr.hpp"
#include <sstream>


using namespace BattleMeasure;

class BattleClock_Wind : public TimeControlWindow
{
    public:
        BattleClock_Wind(RCPBattleData batData, BatTimeControl* tc);
        ~BattleClock_Wind();

        // Implement TimeControlUser

        virtual bool isTimeFrozen() const;
        virtual void freezeTime(bool freeze);

        virtual int getTimeRange() const;               // How many different rates are there?
        virtual void setRate(RateValue n);              // Set rate to value 0..getTimeRange()
        virtual RateValue getRate() const;

        virtual void setJump(JumpValue n);               // Jump time, e.g. 0=day, 1=week, 2=fortnight
        virtual bool isTimeJumping();

        virtual TimeValue getTime() const;              // get the current date/time
        virtual String displayTime() const;            // convert time to string
        virtual String displayRate() const;               // e.g. "seconds per day"
        virtual bool hasJumps() const { return false; }
    private:

        CPBattleData d_batData;
        BatTimeControl* d_control;

        static int s_minRate;
        static int s_maxRate;
        static int s_resolution;
        static int s_timeRange;

        static JumpValue s_nJumpValues;
        static BattleTime::Tick s_jumpValues[];

};

int BattleClock_Wind::s_minRate = 1;
int BattleClock_Wind::s_maxRate = 60;
int BattleClock_Wind::s_resolution = 1;
int BattleClock_Wind::s_timeRange = (1 + s_maxRate - s_minRate) / s_resolution;


TimeControlWindow::JumpValue BattleClock_Wind::s_nJumpValues = 3;
BattleTime::Tick BattleClock_Wind::s_jumpValues[] =
{
    BattleMeasure::minutes(1),
    BattleMeasure::minutes(5),
    BattleMeasure::minutes(10)
};

BattleClock_Wind::BattleClock_Wind(RCPBattleData batData, BatTimeControl* tc) :
    d_batData(batData),
    d_control(tc),
    TimeControlWindow()
{
   /*
   * Read in values from scenario file
   */
   s_minRate = scenario->getInt("BattleTimeRatioMinimum");
   s_maxRate = scenario->getInt("BattleTimeRatioMaximum");
   s_resolution = scenario->getInt("BattleTimeRatioResolution");
   s_timeRange = ((s_maxRate - s_minRate) / s_resolution) + 1;

   s_jumpValues[0] = scenario->getInt("BattleAdvanceTimeSlow");
   s_jumpValues[1] = scenario->getInt("BattleAdvanceTimeMedium");
   s_jumpValues[2] = scenario->getInt("BattleAdvanceTimeFast");
}

BattleClock_Wind::~BattleClock_Wind()
{
}

// Implement TimeControlUser

bool BattleClock_Wind::isTimeFrozen() const
{
    return d_control->isFrozen();
}

void BattleClock_Wind::freezeTime(bool freeze)
{
    d_control->freeze(freeze);
}

int BattleClock_Wind::getTimeRange() const
{
    return s_timeRange;
}

void BattleClock_Wind::setRate(RateValue n)
{
    ASSERT(n >= 0);
    ASSERT(n < s_timeRange);
    d_control->setRatio(n * s_resolution + s_minRate);
}

TimeControlWindow::RateValue BattleClock_Wind::getRate() const
{
    return (d_control->ratio() - s_minRate) / s_resolution;
}

void BattleClock_Wind::setJump(JumpValue n)
{
    ASSERT(n >= 0);
    ASSERT(n < s_nJumpValues);

    if( (n >= 0) && (n < s_nJumpValues))
        d_control->setJump(s_jumpValues[n]);
}

bool BattleClock_Wind::isTimeJumping()
{
    return d_control->isJumping();
}

TimeControlWindow::TimeValue BattleClock_Wind::getTime() const
{
    return d_batData->getTick();
}

String BattleClock_Wind::displayTime() const
{
    BattleMeasure::BattleTime batTime = d_batData->getDateAndTime();
    Greenius_System::Time theTime = batTime.time();
    Greenius_System::Date date = batTime.date();

    char buf[80];
//     wsprintf(buf,
//         "%s %d%s %d, %d:%02d",
//          date.monthName(true),
//          (int) date.day(),
//             (const char*) getNths(date.day()),
//          (int) date.year(),
//             (int) theTime.hours(),
//             (int) theTime.minutes());
    wsprintf(buf,
        "%d:%02d",
            (int) theTime.hours(),
            (int) theTime.minutes());

    if(d_control->ratio() < 20)
        wsprintf(buf + strlen(buf), ":%02d", (int) theTime.seconds());

    return buf;
}

String BattleClock_Wind::displayRate() const
{
    // char buf[80];
    // std::ostrstream buffer(buf, 80);
//    buffer << InGameText::get(IDS_Ratio) << " " << d_control->ratio() << ":1" << '\0';
  std::ostringstream buffer;
    buffer << d_control->ratio() << ":1"; //  << '\0';
    return buffer.str();
    // return String(buf);
}

/*
 * Public Functions
 */


BattleClock_Int::BattleClock_Int(HWND parent, RCPBattleData batData, BatTimeControl& tc) :
  d_clockWind(new BattleClock_Wind(batData, &tc))
{
  ASSERT(d_clockWind);
  d_clockWind->init(parent);
}

BattleClock_Int::~BattleClock_Int()
{
    delete d_clockWind;
}

void BattleClock_Int::update()
{
  if(d_clockWind)
    d_clockWind->update(false);
}

void BattleClock_Int::destroy()
{
  if(d_clockWind)
  {
     delete d_clockWind;
    d_clockWind = 0;
  }
}

int BattleClock_Int::getWidth() const
{
    if(d_clockWind)
        return d_clockWind->getWidth();
    else
        return 0;
}

void BattleClock_Int::setPosition(LONG x, LONG y, LONG w, LONG h)
{
    if(d_clockWind)
        d_clockWind->setPosition(x, y, w, h);
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
