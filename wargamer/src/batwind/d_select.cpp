/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Deployment Select Area of Map
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "d_select.hpp"
#include "wmisc.hpp"

#include "d_selsde.hpp"
#include "building.hpp"
#include "key_terr.hpp"
#include "scenario.hpp"
#include "app.hpp"
#include "batmap.hpp"
#include "batdata.hpp"
#include "hexdata.hpp"
#include "batinfo.hpp"
#include "resstr.hpp"

/*------------------------------------------------------------------------------------------------

        Main deployment windows class

--------------------------------------------------------------------------------------------------*/


DeploySelectionWindows::DeploySelectionWindows(BW_ModeOwner* owner, const BattleInfo& batInfo) :
    BW_Mode(owner),
    m_lpSideBar(0),
    m_MapWind(owner->mapWindow()),
    m_lpBattleData(owner->battleData()),
    m_lpBattleInfo(batInfo),
   m_TitleBar(0),
    select_center(),
    select_size()
{
    select_size = HexCord(m_lpBattleInfo.battleWidth(), m_lpBattleInfo.battleHeight() );
    select_center  = HexCord( (m_lpBattleInfo.deploymentWidth() / 2), (m_lpBattleInfo.deploymentHeight() / 2) );

    m_lpSideBar = new SelectionSideWindow(this, m_lpBattleData, m_lpBattleInfo);

   m_TitleBarRect.left = 0;
   m_TitleBarRect.top = 0;
   m_TitleBarRect.right  = 128;
   m_TitleBarRect.bottom = 32;

   m_TitleBar = new TitleBarClass(d_owner->hwnd(), TitleBarClass::Normal, &m_TitleBarRect);
   m_TitleBar->setText(InGameText::get(IDS_TitleSelectArea));   // "Wargamer : Napoleon 1813 - Battlefield Selection");
   m_TitleBar->displayBar();

    setMapMode(BattleMapInfo::Strategic);
    SetSelection();
    m_MapWind->getDisplay()->ShowSelection(true);
}

DeploySelectionWindows::~DeploySelectionWindows()
{
   if(m_TitleBar) delete m_TitleBar;
    delete m_lpSideBar;
    m_MapWind->getDisplay()->ShowSelection(false);
}


void
DeploySelectionWindows::SetPlayingArea(void) {

   HexCord bottomleft = HexCord(select_center.x() - (select_size.x() / 2), select_center.y() - (select_size.y() / 2) );
   m_lpBattleData->setPlayingArea(bottomleft, select_size);
}



#if 0
void DeploySelectionWindows::cropMap(void)
{
    // set selection area in display
   HexCord topleft = HexCord(select_center.x() - (select_size.x() / 2), select_center.y() - (select_size.y() / 2) );
    HexCord bottomright = HexCord(select_center.x() + (select_size.x() / 2), select_center.y() + (select_size.y() / 2) );

   // ensure we're even, otherwise strange things happen
   if(topleft.x() & 1) topleft.x( topleft.x()-1 );
   if(topleft.y() & 1) topleft.y( topleft.y()-1 );
   if(bottomright.x() & 1) bottomright.x( bottomright.x()-1 );
   if(bottomright.y() & 1) bottomright.y( bottomright.y()-1 );

    // crop battlemap to area
    m_lpBattleData->map()->crop(topleft, bottomright);
    m_lpBattleData->buildingList()->crop(topleft, bottomright);
    m_lpBattleData->buildingList()->adjust(topleft);

    HexCord bottom_left = HexCord(topleft.x(), topleft.y() );
    HexCord top_right = HexCord(bottomright.x(), bottomright.y() );

    m_lpBattleData->keyTerrainList()->crop(bottom_left, top_right);
   m_lpBattleData->keyTerrainList()->checkEdges(m_lpBattleData->map());
#ifdef DEBUG
   m_lpBattleData->keyTerrainList()->labelKeyTerrain(m_lpBattleData->map());
#endif
    m_lpBattleData->keyTerrainList()->check();
}

/*
Static function called by BATGAME
*/
void DeploySelectionWindows::cropMap(BattleData * batData, HexCord center, HexCord size)
{
    // set selection area in display
   HexCord topleft = HexCord(center.x() - (size.x() / 2), center.y() - (size.y() / 2) );
    HexCord bottomright = HexCord(center.x() + (size.x() / 2), center.y() + (size.y() / 2) );

   // ensure we're even, otherwise strange things happen
   if(topleft.x() & 1) topleft.x( topleft.x()-1 );
   if(topleft.y() & 1) topleft.y( topleft.y()-1 );
   if(bottomright.x() & 1) bottomright.x( bottomright.x()-1 );
   if(bottomright.y() & 1) bottomright.y( bottomright.y()-1 );

    // crop battlemap to area
    batData->map()->crop(topleft, bottomright);
    batData->buildingList()->crop(topleft, bottomright);
    batData->buildingList()->adjust(topleft);

    HexCord bottom_left = HexCord(topleft.x(), topleft.y() );
    HexCord top_right = HexCord(bottomright.x(), bottomright.y() );

    batData->keyTerrainList()->crop(bottom_left, top_right);
   batData->keyTerrainList()->checkEdges(batData->map());
#ifdef DEBUG
   batData->keyTerrainList()->labelKeyTerrain(batData->map());
#endif
    batData->keyTerrainList()->check();
}
#endif


void DeploySelectionWindows::show(bool visible)
{
    if(m_MapWind) m_MapWind->show(visible);
    if(m_lpSideBar) m_lpSideBar->show(visible);
}


void DeploySelectionWindows::positionWindows(const PixelRect& r, DeferWindowPosition* def)
{
    int width = (r.width() * 5) / 6;
   int title_height = (r.height() / 16);

    if(m_MapWind && m_MapWind->getHWND())
    {
        if(def) {
            def->setPos(
            m_MapWind->getHWND(),
            HWND_TOP,
            r.left(),
            (r.top() + title_height),
            width,
            (r.height() - title_height),
            0
         );
      }
        else {
            SetWindowPos(
            m_MapWind->getHWND(),
            HWND_TOP,
            r.left(),
            (r.top() + title_height),
            width,
            (r.height() - title_height),
            SWP_SHOWWINDOW
         );
      }
        redrawMap(true);
    }


    if(m_lpSideBar)
    {
        PixelRect rSide = r;
        rSide.left(r.left() + width);
      rSide.top(r.top() + title_height);
      //rSide.bottom(r.height() - title_height);

        m_lpSideBar->SetSize(rSide);
    }

   if(m_TitleBar) {
      m_TitleBarRect.top = r.top();
      m_TitleBarRect.left = r.left();
      m_TitleBarRect.right = r.width();
      m_TitleBarRect.bottom = title_height;
      m_TitleBar->setSize(&m_TitleBarRect);
   }
}


void
DeploySelectionWindows::SetSelection(void) {

   HexCord map_bottomleft;
   HexCord map_size;

    m_lpBattleData->getPlayingArea(&map_bottomleft, &map_size);
   HexCord map_topright = HexCord(map_bottomleft.x() + map_size.x(), map_bottomleft.y() + map_size.y());

   int x_adjust = 0;
   int y_adjust = 0;

   int left = select_center.x() - (select_size.x() / 2);
   int bottom = select_center.y() - (select_size.y() / 2);
   int right = select_center.x() + (select_size.x() / 2);
   int top = select_center.y() + (select_size.y() / 2);

   /*
   Adjust to fit playing area
   */
   if(bottom < map_bottomleft.y()) y_adjust = map_bottomleft.y() - bottom;
   if(left < map_bottomleft.x()) x_adjust = map_bottomleft.x() - left;
   if(right > map_topright.x()) x_adjust = map_topright.x() - right;
   if(top > map_topright.y()) y_adjust = map_topright.y() - top;

   select_center.x( select_center.x() + x_adjust);
   select_center.y( select_center.y() + y_adjust);
   /*
   Check we fitted in
   */
   HexCord sel_bottomleft = HexCord(select_center.x() - (select_size.x() / 2), select_center.y() - (select_size.y() / 2) );
   HexCord sel_topright = HexCord(sel_bottomleft.x() + select_size.x(), sel_bottomleft.y() + select_size.y());

   ASSERT(sel_bottomleft.x() >= map_bottomleft.x());
   ASSERT(sel_bottomleft.y() >= map_bottomleft.y());
   ASSERT(sel_topright.x() <= map_topright.x());
   ASSERT(sel_topright.y() <= map_topright.y());

   m_MapWind->getDisplay()->SetSelectionArea(sel_bottomleft, select_size);
}






// sent by mapwind when Button is pressed
void
DeploySelectionWindows::onLButtonDown(const BattleMapSelect& info) {

    HexCord hex = m_lpBattleData->getHex(info.location(), BattleData::Mid);

    select_center = hex;

    SetSelection();

    redrawMap(false);

}

// sent by mapwind when Button is released while not dragging
void
DeploySelectionWindows::onLButtonUp(const BattleMapSelect& info) {
//    if(d_unitUI.get()) d_unitUI->onLButtonUp(info);
}

// sent by mapwind when Button is pressed
void
DeploySelectionWindows::onRButtonDown(const BattleMapSelect& info) {
//  if(d_unitUI.get()) d_unitUI->onRButtonDown(info);
}

// sent by mapwind when Button is released while not dragging
void
DeploySelectionWindows::onRButtonUp(const BattleMapSelect& info) {
//    if(d_unitUI.get()) d_unitUI->onRButtonUp(info);
}

// sent by mapwind when Mouse is moved while button is held
void
DeploySelectionWindows::onStartDrag(const BattleMapSelect& info) {
//    if(d_unitUI.get()) d_unitUI->onStartDrag(info);
}

// sent by mapwind when button is released while dragging
void
DeploySelectionWindows::onEndDrag(const BattleMapSelect& info) {
//    if(d_unitUI.get()) d_unitUI->onEndDrag(info);
}

// sent by mapwind when Mouse has moved
void
DeploySelectionWindows::onMove(const BattleMapSelect& info) {
//    if(d_unitUI.get()) d_unitUI->onMove(info);

//    if(d_sideBar()) d_sideBar->setHex(d_battleData->getHex(info.location()));
}

bool
DeploySelectionWindows::setCursor() {
//  return (d_unitUI.get()) ? d_unitUI->setCursor() : False;
    return false;
}

void
DeploySelectionWindows::redrawMap(bool all) {
    m_MapWind->update(all);
}



void
DeploySelectionWindows::setMapMode(BattleMapInfo::Mode mode) {

    if(m_MapWind) {
        m_MapWind->mode(mode);
    }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
