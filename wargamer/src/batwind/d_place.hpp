/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef D_PLACE_HPP
#define D_PLACE_HPP

/*
 * Unit Deployment Window
 */


#include "bw_mode.hpp"
// #include "dplyuser.hpp"
// #include "wind.hpp"
// #include "palwind.hpp"
// #include "batw_oob.hpp"
// #include "batord.hpp"
// #include "btwin_i.hpp"
#include "batmap_i.hpp"
#include "bu_dial.hpp"
// #include "bud_ordr.hpp"
#include "batunit.hpp"
#include "d_plcsde.hpp"
#include "titlebar.hpp"
#include "bud_info.hpp"

class BattleMapWind;
class PlacementSideWindow;
struct BattleInfo;
class BattleOrder_Int;
class PlacementSideWindow;

class DeployPlacementWindows :
    public BW_Mode,
    // public BattleWindowsInterface,
    // public WindowBaseND,
    // public PaletteWindow,
    // public CustomBorderWindow,
    // public BattleOBUserInterface,
    public BUI_UnitInterface,
    public PlacementSideUser
{

    public:

       // DeployPlacementWindows(DeploymentUser* deployuser, WindowChangeUser* window_user, HWND hparent, BattleData* batdata, const BattleInfo* batinfo);
       DeployPlacementWindows(BW_ModeOwner* user, const BattleInfo& batinfo);
       ~DeployPlacementWindows(void);

       /*
        * BW_Mode Implementation
        */

       virtual void show(bool visible);

       virtual void positionWindows(const PixelRect& r, DeferWindowPosition* def);

       virtual void onLButtonDown(const BattleMapSelect& info);
       virtual void onLButtonUp(const BattleMapSelect& info);
       virtual void onRButtonDown(const BattleMapSelect& info);
       virtual void onRButtonUp(const BattleMapSelect& info);
       virtual void onStartDrag(const BattleMapSelect& info);
       virtual void onEndDrag(const BattleMapSelect& info);
       virtual void onMove(const BattleMapSelect& info);
       virtual bool setCursor();

       virtual bool hasWindow(BW_ID id) = 0;  // Implemented higher up

    private:
       // PlacementSideUser Implementation

        virtual void onDragUnit(BattleUnit * unit) { }
        virtual void onDragEnd(void) { }
        virtual void onSelectUnit(BattleUnit * unit);
        virtual void onRightClickUnit(BattleUnit * unit) { }
        virtual void onDoubleClickUnit(BattleUnit * unit) { }

        virtual const BattleData* battleData() const { return d_owner->battleData(); }
        virtual BattleData* battleData() { return d_owner->battleData(); }
        virtual HWND hwnd() const { return d_owner->hwnd(); }
        virtual void onFinish() = 0;        // batwind handles this


       // Private Local functions

       void SetSideVisibility(Side side, Visibility::Value visibility);
       void SetChildVisibility(BattleCP* cp, Visibility::Value visibility);

       void setMapMode(BattleMapInfo::Mode mode);
       virtual bool setMapLocation(const BattleLocation& l);
             // Set centre of main map, return true if has changed

       void sendOrder();
       void cancelOrder();
       void mode(Mode m);
       Mode mode() const;
       void showOrders();
       void showDeployment();
       void showInfo();
       void redrawMap();
       void highlightUnit(bool on);


       // void DestroyWindows();
       // void SetSize(RECT rect);
       // void ShowWindows(void);

       void CreateWindows(void);
       // void DestroyWindows(void);

       // void ShowFacingDialog(void);
       // void HideFacingDialog(void);

//       bool doPlayerDeploy(RPBattleData batData, const RefBattleCP& cp, const HexCord& startHex, HexList* hl);

       void RemoveFromMap(BattleCP * cp, bool removechildren);
       void RemoveCP(BattleCP * cp);
       void RemoveSP(BattleSP * sp);

       /*
        * Orders Windows & Battle-Unit-Interface Functions
        */

       void CopyOrderFromCP(BattleCP * cp);
       void CopyOrderToCP(BattleCP * cp);

       /*
       Message Processing
       */

       LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
       void onNCPaint(HWND hwnd, HRGN hrgn);
       void onPaint(HWND hwnd);


       // virtual HWND hwnd() const { return getHWND(); }
    
       // virtual void mapAreaChanged(const BattleArea& area);
             // Sent by mapwind when it has changed its area
       // virtual void mapZoomChanged();
             // Get mapwindow zoom buttons updated

       virtual void redrawMap(bool all);

       // virtual void orderCP(CRefBattleCP cp);
       // virtual void messageWindowUpdated();
       // virtual void windowDestroyed(HWND hwnd);

       // virtual void positionWindows();
       // virtual void updateAll();

       // virtual bool updateTracking(const BattleMeasure::HexCord& hex);

       // virtual const DrawDIBDC* mapDib() const;
       // virtual BattleMapDisplay* mapDisplay() const;

       void HighlightUnits(bool state);
       void HighlightCP(BattleCP * cp, bool state);
       void HighlightSP(BattleSP * sp, bool state);

    private:
       const BattleInfo& m_lpBattleInfo;

       // WindowChangeUser * m_lpWindowChangeUser;
       // DeploymentUser * m_lpDeployUser;
    
       // HWND m_ParentHwnd;

       // RECT m_WindowRect;
       // RECT m_MapRect;
       /// RECT m_SideBarRect;

       PlacementSideWindow* m_lpSideBar;

       BattleMapWind* m_MapWind;
       BattleData* m_lpBattleData;

	   TitleBarClass * m_TitleBar;
	   RECT m_TitleBarRect;

       /*
        * Deployment Details
        */

       // rectangle in which user can deploy units (1/3 of map)
       BattleMeasure::HexCord m_BottomLeftPlayerDeployArea;
       BattleMeasure::HexCord m_TopRightPlayerDeployArea;

       int m_DarkenedAreaTop;
       int m_DarkenedAreaBottom;

       // unit currently being dragged
       BattleCP * m_lpSelectedUnit;
       BattleMeasure::HexCord m_SelectedUnitHex;

       bool d_leftButtonDragging;
       bool d_rightButtonDragging;

       bool m_bUnitHighlighted;

       BattleMeasure::HexCord m_DeploymentHex;
       HexList m_DeploymentHexList;

       /*
        * Orders
        */

       BattleOrder_Int* d_orderWind;

       BattleOrder d_currentOrder;
       BattleOrderInfo d_orderInfo;
       bool m_bIssuingOrders;

       BUI_UnitInterface::Mode d_BUIMode;


	   BattleMeasure::HexCord d_deploymentMouseOffset;


	/*
	cached cursors
	*/

	BattleCursorEnum d_currentCursorMode;
	BattleCursorEnum d_desiredCursorMode;

	HCURSOR hCursor_Normal;
	HCURSOR hCursor_OverUnit;
	HCURSOR hCursor_DragUnit;
	HCURSOR hCursor_BadHex;
	HCURSOR hCursor_TargetMode;
	HCURSOR hCursor_ValidTarget;
	HCURSOR hCursor_InvalidTarget;
	HCURSOR hCursor_AttatchUnit;

	// for restoring Vis state after this screen
	bool d_hexVis;

};


#if 0     // Jim's original

class DeployPlacementWindows :
    public BattleWindowsInterface,
    public WindowBaseND,
    public PaletteWindow,
    public CustomBorderWindow,
    public BattleOBUserInterface,
    public BUI_UnitInterface {

public:

    DeployPlacementWindows(DeploymentUser * deployuser, WindowChangeUser * window_user, HWND hparent, BattleData * batdata, const BattleInfo * batinfo);
    ~DeployPlacementWindows(void);

    
    void SetSize(RECT rect);
    void ShowWindows(void);

private:

    void CreateWindows(void);
    void DestroyWindows(void);

    void ShowFacingDialog(void);
    void HideFacingDialog(void);

    WindowChangeUser * m_lpWindowChangeUser;
    
    HWND m_ParentHwnd;

    RECT m_WindowRect;
    RECT m_MapRect;
    RECT m_SideBarRect;

    BattleMapWind * m_MapWind;
    PlacementSideWindow * m_lpSideBar;

    BattleData * m_lpBattleData;
    const BattleInfo* m_lpBattleInfo;

    DeploymentUser * m_lpDeployUser;

    /*
     * Deployment Details
     */

    // rectangle in which user can deploy units (1/3 of map)
    HexCord m_BottomLeftPlayerDeployArea;
    HexCord m_TopRightPlayerDeployArea;

    int m_DarkenedAreaTop;
    int m_DarkenedAreaBottom;

    // unit currently being dragged
    BattleCP * m_lpSelectedUnit;
    HexCord m_SelectedUnitHex;
    bool m_bDragging;
    bool m_bUnitHighlighted;

    HexCord m_DeploymentHex;
    HexList m_DeploymentHexList;










    void SetSideVisibility(BattleData * batdata, Side side, Visibility::Value visibility);
    void SetChildVisibility(BattleData * batdata, BattleCP * cp, Visibility::Value visibility);

    bool doPlayerDeploy(RPBattleData batData, const RefBattleCP& cp, const HexCord& startHex, HexList* hl);

    void RemoveFromMap(BattleCP * cp, bool removechildren);
    void RemoveCP(BattleCP * cp);
    void RemoveSP(BattleSP * sp);

    /*
    Orders Windows & Battle-Unit-Interface Functions
    */

    BattleOrder_Int * d_orderWind;

    BattleOrder d_currentOrder;
    BattleOrderInfo d_orderInfo;
    bool m_bIssuingOrders;

    BUI_UnitInterface::Mode d_BUIMode;

    void CopyOrderFromCP(BattleCP * cp);
    void CopyOrderToCP(BattleCP * cp);

    void sendOrder();
    void cancelOrder();
    void mode(Mode m);
    Mode mode() const;
    void showOrders();
    void showDeployment();
    void showInfo();
    void redrawMap();
    void highlightUnit(bool on);

    /*
    Message Processing
    */

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    void onNCPaint(HWND hwnd, HRGN hrgn);
    void onPaint(HWND hwnd);



    /*
    BattleMap Interface Functions
    */
    
    virtual HWND hwnd() const { return getHWND(); }
    
    virtual bool setMapLocation(const BattleLocation& l);
            // Set centre of main map, return true if has changed
    virtual void mapAreaChanged(const BattleArea& area);
            // Sent by mapwind when it has changed its area
    virtual void mapZoomChanged();
            // Get mapwindow zoom buttons updated
public:
    virtual void onLButtonDown(const BattleMapSelect& info);
            // sent by mapwind when Button is pressed
    virtual void onLButtonUp(const BattleMapSelect& info);
            // sent by mapwind when Button is released while not dragging
    virtual void onRButtonDown(const BattleMapSelect& info);
            // sent by mapwind when Button is pressed
    virtual void onRButtonUp(const BattleMapSelect& info);
            // sent by mapwind when Button is released while not dragging
    virtual void onStartDrag(const BattleMapSelect& info);
            // sent by mapwind when Mouse is moved while button is held
    virtual void onEndDrag(const BattleMapSelect& info);
            // sent by mapwind when button is released while dragging
    virtual void onMove(const BattleMapSelect& info);
            // sent by mapwind when Mouse has moved
    virtual bool setCursor();
private:
    virtual void redrawMap(bool all);

    virtual void orderCP(CRefBattleCP cp);
    virtual void messageWindowUpdated();
    // virtual void windowDestroyed(HWND hwnd);

    virtual void positionWindows();
    virtual void updateAll();

    virtual bool updateTracking(const BattleMeasure::HexCord& hex);

    virtual const DrawDIBDC* mapDib() const;
    virtual BattleMapDisplay* mapDisplay() const;

    void setMapMode(BattleMapInfo::Mode mode);


    /*
    Battle OB Interface functions
    */
//    virtual BattleOBUserInterface::~BattleOBUserInterface(void) { }

    virtual void onDragUnit(BattleUnit * unit) { }

    virtual void onDragEnd(void) { }

    virtual void onSelectUnit(BattleUnit * unit);

    virtual void onRightClickUnit(BattleUnit * unit) { }

    virtual void onDoubleClickUnit(BattleUnit * unit) { }

    void HighlightUnits(bool state);
    void HighlightCP(BattleCP * cp, bool state);
    void HighlightSP(BattleSP * sp, bool state);


};


#endif


#endif




