/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Tracking window is now derived from the same class (TrackWind) as
 * Campaign Tracking windows (infowind.cpp/hpp)
 * This solves the below problems
 *
 * -- Paul
 *
 *      Battle Tracking/Info window
 *
 * This is only a prototype of what it will look like.
 *
 * The update() function and the ShowUnit::draw(...) functions should
 * be rewritten to display more appropriate information in a more
 * pleasant way.
 *
 * The transparency effect may need to be done some other way.
 * The problems now are that:
 *  - If the window is updated or redrawn overlapping its old position
 *    then the previus version of the window is used as part of the
 *    background
 *  - If objects are moving underneath the window, then they do not
 *    get displayed.
 *
 * A method to get around this might be:
 *   Set the WS_EX_TRANSPARENT flag when creating the window
 *
 *   Redraw the window from scratch in WM_PAINT including getting
 *   bits from the background and merging colours.
 *
 *   The may be speeded up by having a DIB containing the foreground
 *   information, which is transparently blitted onto the window
 *   after the background/merging has been done.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "b_info.hpp"
#include "btwin_i.hpp"          // batdisp/ BattleMapSelect
#include "batarmy.hpp"          // batdata/ Order of Battle
#include "bu_draw.hpp"          // batdata/ UnitDrawer
#include "scenario.hpp"         // gamesup/
#include "app.hpp"                      // system/
#include "dib.hpp"                      // system/
#include "dib_util.hpp"         // system/
#include "wmisc.hpp"                    // system/
#include "palette.hpp"          // system/
#include "fonts.hpp"                    // system/
#include "trackwin.hpp"
#include "scrnbase.hpp"
#include "scn_res.h"
#include "imglib.hpp"
#include "scn_img.hpp"
#include "registry.hpp"
#include "hexmap.hpp"
#include "hexdata.hpp"
#include "b_terr.hpp"
#include "batdisp.hpp"
#include "dib_blt.hpp"
#include "cmbtutil.hpp"
#include "unittype.hpp"
#include "b_tables.hpp"
#include "moveutil.hpp"
#include "bobutil.hpp"
#include "resstr.hpp"
#include "simpstr.hpp"

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

/*
 * Class to Implement UnitDrawer, so that info can display correct information
 */


/*
 * Class for drawing into info window
 */

class ShowObject {
                // static stuff
                static SWG_Sprite::SpriteLibrary * s_hexGraphics;
                static const char* s_terrainSpriteFileName;

                 // high-zoom map window
                 BattleMapDisplay d_zoomwindow;
                 // Area shown by main map
                 PixelRect d_zoomRect;

                DrawDIBDC* d_dib;
                BattleWindowsInterface* d_batWind;
                CPBattleData d_batData;
                const BattleOB* d_ob;

                PixelDimension d_x;
                PixelDimension d_y;
                const PixelDimension d_cx;
                const PixelDimension d_cy;
                const int d_dbX;
                const int d_dbY;
                const int d_baseX;
                const int d_baseY;
                const int d_xMargin;
                const int d_yMargin;
                const int d_headerX;
                const int d_headerY;
                const int d_headerCX;
                const int d_headerCY;
                const int d_textX;
                const int d_textY;
                const int d_textCX;
                const int d_textCY;

  public:
         ShowObject(DrawDIBDC* dib, BattleWindowsInterface* bw,
                 CPBattleData bd, PixelDimension cx, PixelDimension cy) :
                        d_dib(dib),
                        d_batWind(bw),
                        d_batData(bd),
                        d_ob(bd->ob()),
                        d_x(0),
                        d_y(0),
                        d_cx(cx),
                        d_cy(cy),
                        d_dbX(ScreenBase::dbX()),
                        d_dbY(ScreenBase::dbY()),
                        d_baseX(ScreenBase::baseX()),
                        d_baseY(ScreenBase::baseY()),
                        d_xMargin((4 * d_dbX) / d_baseX),
                        d_yMargin((4 * d_dbY) / d_baseY),
                        d_headerX(d_xMargin),
                        d_headerY(d_yMargin),
                        d_headerCX(cx - (2 * d_headerX)),
                        d_headerCY((18 * d_dbY) / d_baseY),
                        d_textX(d_headerX),
                        d_textY((26 * d_dbY) / d_baseY),
                        d_textCX((68 * d_dbX) / d_baseX),
                        d_textCY((45 * d_dbX) / d_baseX),

                        d_zoomwindow(bd, BattleMapInfo::OneHex) {

                     if(!s_hexGraphics) {
                        StringPtr fileName = scenario->makeScenarioFileName(s_terrainSpriteFileName);
                        s_hexGraphics = new SWG_Sprite::SpriteLibrary(fileName);
                     }

                  }

         ~ShowObject() {
          if(s_hexGraphics) {
             delete s_hexGraphics;
             s_hexGraphics = 0;
          }
       }

         void drawUnitInfo(const CRefBattleCP& cp, const HexCord& hex);
         void drawHexInfo(const HexCord& hex);

  private:
         void fillHeader();
         void drawUnitHeader(const CRefBattleCP& dp);
         void drawHexHeader(const HexCord& hex);
         void drawTerrainGraphic(const HexCord& hex);
         void drawTextBox();
         const char* getMoveCostText(BasicUnitType::value v, const BattleTerrainHex& hexInfo);
};

const char* ShowObject::s_terrainSpriteFileName = "terrain.spr";
SWG_Sprite::SpriteLibrary * ShowObject::s_hexGraphics = 0;


const char* ShowObject::getMoveCostText(BasicUnitType::value v, const BattleTerrainHex& hexInfo)
{
  ASSERT(v <= BasicUnitType::Artillery);
  static char s_buf[50];

  // convert unit type flags to local enum
  enum {
         Inf,
         LCav,
         HCav,
         FootArt,
         HvyArt,
         HorseArt,

         End,
         Undefined = End
  } unitType = (v == BasicUnitType::Infantry) ? Inf :
                                        (v == BasicUnitType::Cavalry)  ? HCav : FootArt;

  // get modifier value from table and convert to a string
  const Table3D<UWORD>& tTable = BattleTables::terrainModifiers();
  const Table3D<UWORD>& pTable = BattleTables::pathModifiers();
  int value = tTable.getValue(unitType, hexInfo.d_terrainType, 0);

  // get path modifiers (i.e streams, rivers)

  bool isRoad = False;
  if(hexInfo.d_path1.d_type != LP_None)
  {
         int v2 = pTable.getValue(unitType, hexInfo.d_path1.d_type, 0);
         int v = (value * v2) / pTable.getResolution();
         if(v < value)
                value = v;

         isRoad = ( (hexInfo.d_path1.d_type == LP_Road) ||
                                        (hexInfo.d_path1.d_type == LP_Track) ||
                                        (hexInfo.d_path1.d_type == LP_RoadBridge) ||
                                        (hexInfo.d_path1.d_type == LP_TrackBridge) );
  }

  if(hexInfo.d_path2.d_type != LP_None)
  {
         int v2 = pTable.getValue(unitType, hexInfo.d_path2.d_type, 0);
         int v = (value * v2) / pTable.getResolution();
         if(v < value)
                value = v;

         if(!isRoad)
         {
                isRoad = ( (hexInfo.d_path2.d_type == LP_Road) ||
                                          (hexInfo.d_path2.d_type == LP_Track) ||
                                          (hexInfo.d_path2.d_type == LP_RoadBridge) ||
                                          (hexInfo.d_path2.d_type == LP_TrackBridge) );
         }
  }

  if(value == 0 && isRoad)
  {
         const char* text = InGameText::get(IDS_RoadOnly);
         lstrcpy(s_buf, text);
  }

  else if(value == 100)
  {
         const char* text = InGameText::get(IDS_NoCost);
         lstrcpy(s_buf, text);
  }

  else if(value == 0)
  {
         const char* text = InGameText::get(IDS_Prohibited);
         lstrcpy(s_buf, text);
  }

  else
  {
         wsprintf(s_buf, "%s .%d", InGameText::get(IDS_Multiplier), value);
  }

  return s_buf;
}

void ShowObject::drawUnitInfo(const CRefBattleCP& cp, const HexCord& hex)
{
  /*
   * Draw unit header
   */

  d_x = d_xMargin;
  d_y = d_yMargin;
  drawUnitHeader(cp);

  // draw info in text box
  drawTextBox();

  int xOffset = (2 * d_dbX) / d_baseX;

  d_x = d_xMargin + xOffset;
  d_y = d_textY + xOffset;

  // put strength
  // first calc strength in raw manpower

  BobUtility::UnitCounts uc;
  BobUtility::unitsInManpower(cp, d_batData, uc);

  COLORREF oldColor = d_dib->setTextColor(scenario->getColour("MenuText"));

  // put unit strength
  // first, calc strength in manpower
  LogFont lf;
  lf.height((7 * d_dbX) / d_baseX);
  lf.weight(FW_MEDIUM);
  lf.italic(false);
  lf.underline(true);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);
  HFONT oldFont = d_dib->setFont(font);

  const int infPosX = d_xMargin + ((5 * d_dbX) / d_baseX);
  const int cavPosX = d_xMargin + ((28 * d_dbX) / d_baseX);
  const int artPosX = d_xMargin + ((51 * d_dbX) / d_baseX);
  int spacing = (8 * d_dbY) / d_baseY;

  int y = d_y;
  int x = infPosX;
  wTextOut(d_dib->getDC(), x, y, InGameText::get(IDS_Inf));
  x = cavPosX;
  wTextOut(d_dib->getDC(), x, y, InGameText::get(IDS_Cav));
  x = artPosX;
  wTextOut(d_dib->getDC(), x, y, InGameText::get(IDS_GunsAbrev));


  lf.underline(false);

  font.set(lf);
  d_dib->setFont(font);

  y += spacing;
  x = infPosX;
  wTextPrintf(d_dib->getDC(), x, y, "%d", uc.d_nInf);
  x = cavPosX;
  wTextPrintf(d_dib->getDC(), x, y, "%d", uc.d_nCav);
  x = artPosX;
  wTextPrintf(d_dib->getDC(), x, y, "%d", uc.d_nGuns);

  // draw attribute bargraphs
  const int barChartW = (33 * d_dbX) / d_baseX;
  const int barChartH = (5 * d_dbY) / d_baseY;
  const int barChartOffsetX = (30 * d_dbX) / d_baseX;

  // morale
  x = d_x;
  y += spacing;
  spacing = (7 * d_dbY) / d_baseY;
  const char* text = InGameText::get(IDS_Morale);
  wTextOut(d_dib->getDC(), x, y, text);
  d_dib->drawBarChart(x + barChartOffsetX, y + 2, cp->morale(), Attribute_Range, barChartW, barChartH);

  // fatigue
  y += spacing;
  text = InGameText::get(IDS_Fatigue);
  wTextOut(d_dib->getDC(), x, y, text);
  d_dib->drawBarChart(x + barChartOffsetX, y + 2, cp->fatigue(), Attribute_Range, barChartW, barChartH);

  // fire Value
  y += spacing;
  text = InGameText::get(IDS_Quality);
  wTextOut(d_dib->getDC(), x, y, text);
  Attribute bm = BobUtility::baseMorale(const_cast<BattleOB*>(d_batData->ob()), const_cast<RefBattleCP>(cp));
  d_dib->drawBarChart(x + barChartOffsetX, y + 2, bm, Attribute_Range, barChartW, barChartH);

  // clean up
  d_dib->setFont(oldFont);
  d_dib->setTextColor(oldColor);

  // draw terrain graphic
  drawTerrainGraphic(hex);

}

void ShowObject::drawHexInfo(const HexCord& hex)
{

  const BattleTerrainHex& hexInfo = d_batData->getTerrain(hex);
  const TerrainTable& tTable = d_batData->terrainTable();
  const TerrainTable::Info& tInfo = tTable[hexInfo.d_terrainType];

  /*
   *---------------------------------
   * Draw Header and Header info
   */

  drawHexHeader(hex);

  /*----------------------------------------------
   * Terrain Description section
   */

  drawTextBox();
  COLORREF oldColor = d_dib->setTextColor(scenario->getColour("MenuText"));

  LogFont lf;
  lf.height((8 * d_dbY) / d_baseY);
  lf.face(scenario->fontName(Font_Bold));
  lf.weight(FW_MEDIUM);
  Font font;
  font.set(lf);
  HFONT oldFont = d_dib->setFont(font);

  const char* text = InGameText::get(IDS_CoverColon);
  const int tOffsetX = (2 * d_dbX) / d_baseX;
  const int spacing = ((7 * d_dbY) / d_baseY);
  int x = d_textX + tOffsetX;
  int y = d_textY + tOffsetX;
  wTextOut(d_dib->getDC(), x, y, text);

  lf.underline(true);
  font.set(lf);
  d_dib->setFont(font);

  y += ((3 * spacing) / 2);
  text = InGameText::get(IDS_MovementCost);
  wTextOut(d_dib->getDC(), x, y, text);

  lf.height((7 * d_dbY) / d_baseY);
  lf.underline(false);
  font.set(lf);
  d_dib->setFont(font);

  x = d_textX + ((32 * d_dbX) / d_baseX);
  y = d_textY + tOffsetX;

  wTextOut(d_dib->getDC(), x, y,
   Combat_Util::coverTypeName(
      Combat_Util::hexCoverType(hex, d_batData, true)));
//                Combat_Util::coverTypeName(Combat_Util::hexCoverType(hex, const_cast<BattleData*>(reinterpret_cast<const BattleData*>(d_batData)), True)));

  y += ((5 * spacing) / 2);

  const int cOffsetX = ((15 * d_dbX) / d_baseX);
  x = d_textX + (2 * tOffsetX);
  text = InGameText::get(IDS_InfColon);
  wTextOut(d_dib->getDC(), x, y, text);
  wTextOut(d_dib->getDC(), x + cOffsetX, y, getMoveCostText(BasicUnitType::Infantry, hexInfo));

  y += spacing;
  text = InGameText::get(IDS_CavColon);
  wTextOut(d_dib->getDC(), x, y, text);
  wTextOut(d_dib->getDC(), x + cOffsetX, y, getMoveCostText(BasicUnitType::Cavalry, hexInfo));

  y += spacing;
  text = InGameText::get(IDS_ArtColon);
  wTextOut(d_dib->getDC(), x, y, text);
  wTextOut(d_dib->getDC(), x + cOffsetX, y, getMoveCostText(BasicUnitType::Artillery, hexInfo));

  /*-----------------------------------------------
   * Fill in Terrain Graphic
   */

  drawTerrainGraphic(hex);

  d_dib->setFont(oldFont);
  d_dib->setTextColor(oldColor);
}

void ShowObject::drawUnitHeader(const CRefBattleCP& cp)
{
  fillHeader();

  /*
   * Set initial font
   */

  LogFont lf;
  lf.height((8 * d_dbY) / d_baseY);
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);

  HFONT oldFont = d_dib->setFont(font);
  COLORREF oldColor = d_dib->setTextColor(scenario->getSideColour(cp->getSide()));

  // draw nation flag
  RefGLeader leader = cp->leader();
  const ImageLibrary* il = scenario->getNationFlag(leader->getNation(), True);
  ASSERT(il);

  const int xOffset = (2 * d_dbX) / d_baseX;

  int y = d_y + ((d_headerCY - FI_Army_CY) / 2);
  int x = d_x + xOffset;

  il->blit(d_dib, FI_Army, x, y, True);

  // draw unit name
  x += FI_Army_CX + (2 * xOffset);
  y = d_y + xOffset;
  const int textCX = d_cx - (x + (2 * xOffset));
  wTextOut(d_dib->getDC(), x, y, textCX, cp->getName());

  // draw leader name
  y += (8 * d_dbY) / d_baseY;
  wTextOut(d_dib->getDC(), x, y, textCX, cp->leader()->getName());

  // clean up
  d_dib->setFont(oldFont);
  d_dib->setTextColor(oldColor);
}

void ShowObject::drawHexHeader(const HexCord& hex)
{
  const BattleTerrainHex& hexInfo = d_batData->getTerrain(hex);
  const TerrainTable& tTable = d_batData->terrainTable();
  const TerrainTable::Info& tInfo = tTable[hexInfo.d_terrainType];

  fillHeader();

  // get font
  LogFont lf;
  lf.height((8 * d_dbY) / d_baseY);
  lf.weight(FW_MEDIUM);
  lf.italic(false);
  lf.face(scenario->fontName(Font_Bold));

  Font font;
  font.set(lf);
  HFONT oldFont = d_dib->setFont(font);

  const char* text = InGameText::get(IDS_Terrain);
  const int aOffsetX = (30 * d_dbX) / d_baseX;
  const int hOffsetX = (35 * d_dbX) / d_baseX;
  int x = d_headerX + ((2 * d_dbX) / d_baseX);
  int y = d_headerY + ((2 * d_dbY) / d_baseY);
  wTextOut(d_dib->getDC(), x, y, text);
  text = ":";
  wTextOut(d_dib->getDC(), x + aOffsetX, y, text);
  y += ((8 * d_dbY) / d_baseY);
  wTextOut(d_dib->getDC(), x + aOffsetX, y, text);
  text = InGameText::get(IDS_Location);
  wTextOut(d_dib->getDC(), x, y, text);

  lf.height((7 * d_dbY) / d_baseY);
  font.set(lf);
  d_dib->setFont(font);

  // TODO: get from string resource
  static const char* s_pathAbrev[LP_HowMany] = {
          " ",
          "(Rd)",
          "(Tr)",
          "(Rvr)",
          "(Str)",
          "(RB)",
          "(TB)"
  };

  SimpleString str;
  str = tInfo.name();
  if (hexInfo.d_path1.d_type != LP_None)
  {
   str << " (" << tTable.pathNameAbrev(hexInfo.d_path1.d_type) << ")";
  }
  if (hexInfo.d_path2.d_type != LP_None)
  {
   str << " (" << tTable.pathNameAbrev(hexInfo.d_path2.d_type) << ")";
  }

  y = d_headerY + ((2 * d_dbY) / d_baseY);
  wTextOut(d_dib->getDC(), x + hOffsetX, y, str.toStr());
//  wTextPrintf(d_dib->getDC(), x + hOffsetX, y, " %s %s %s",
//          tInfo.name(), s_pathAbrev[hexInfo.d_path1.d_type], s_pathAbrev[hexInfo.d_path2.d_type]);

  y += ((8 * d_dbY) / d_baseY);
  wTextPrintf(d_dib->getDC(), x + hOffsetX, y, "X%d, Y%d",
         static_cast<int>(hex.x()), static_cast<int>(hex.y()));

  const int elOffsetX = (75 * d_dbX) / d_baseX;
  text = InGameText::get(IDS_Elev);

  wTextPrintf(d_dib->getDC(), x + elOffsetX, y, "%s: %d %s.",
         text,
         static_cast<int>(hexInfo.d_height),
         InGameText::get(IDS_Ft));

  // clean up
  d_dib->setFont(oldFont);
}

void ShowObject::fillHeader()
{
  ASSERT(d_dib);

  // draw a frame around header area
  ColourIndex ci = d_dib->getColour(PALETTERGB(0, 0, 0));
  d_dib->frame(d_headerX - 1, d_headerY - 1, d_headerCX + 2, d_headerCY + 2, ci);

  // fill in header area
  const DrawDIB* fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  d_dib->rect(d_headerX, d_headerY, d_headerCX, d_headerCY, fillDib);
}





void
ShowObject::drawTerrainGraphic(const HexCord& hex) {

    const int terrainGraphicX  = d_cx - (d_headerX + ((45 * d_dbX) / d_baseX));
    const int terrainGraphicY  = d_textY;
    const int terrainGraphicCX = (45 * d_dbX) / d_baseX;
    const int terrainGraphicCY = d_textCY;

    PixelRect windowrect(
        terrainGraphicX,
        terrainGraphicY,
        terrainGraphicX + terrainGraphicCX,
        terrainGraphicY + terrainGraphicCY
    );

    d_zoomwindow.setSize(windowrect);

    BattleLocation hexlocation = d_batData->getLocation(hex);

    d_zoomwindow.setPosition(hexlocation);

    d_zoomwindow.redraw(TRUE);


    d_zoomwindow.draw();
    DrawDIBDC* hexdib = d_zoomwindow.mainDIB();


    DIB_Utility::stretchDIB(
        d_dib,
        terrainGraphicX,
        terrainGraphicY,
        terrainGraphicCX,
        terrainGraphicCY,
        hexdib,
        0,
        0,
        hexdib->getWidth(),
        hexdib->getHeight()
    );


}

void ShowObject::drawTextBox()
{
  // draw frame and fill in background
  ColourIndex ci = d_dib->getColour(scenario->getColour("MenuText")); //PALETTERGB(0, 0, 0));
  d_dib->frame(d_textX - 1, d_textY - 1, d_textCX + 2, d_textCY + 2, ci);
}


/*=====================================================
 * Tracking window
 */

/*
 * Constructor
 */


class BTW_Imp : public WindowBaseND
{
        public:
                /*
                 * Public Functions
                 */

                BTW_Imp(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData);       // Construct as a child of hParent
                ~BTW_Imp();

                void reset();

                bool update(const HexCord& hex);
                int width() const { return d_cx; }
                int height() const { return d_cy; }

                void drawInfo();

                HWND getHWND() const { return WindowBaseND::getHWND(); }
        private:
                /*
                 * Private Functions
                 */

                // No copying allowed
                BTW_Imp(const BTW_Imp&);
                BTW_Imp& operator = (const BTW_Imp&);

                // Windows message handlers
                LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
                void onPaint(HWND hwnd);
                BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
                BOOL onEraseBk(HWND hwnd, HDC hdc);
                void onDestroy(HWND hWnd);
                UINT onNCHitTest(HWND hwnd, int x, int y);

                // misc
                void redraw();
                void drawBackground();

                /*
                 * Data
                 */

                BattleWindowsInterface* d_batWind;
                CPBattleData            d_batData;   // Battle Data
                CRefBattleUnit          d_unit;              // Unit currently being displayed
                HexCord                 d_hex;
                bool                    d_hexValid;

                const int               d_cx;
                const int               d_cy;

                DrawDIBDC*              d_dib;
                ShowObject              d_draw;

                static const char      s_registryName[];
               static WSAV_FLAGS s_saveFlags;
};

const char BTW_Imp::s_registryName[] = "BattleTracking";
WSAV_FLAGS BTW_Imp::s_saveFlags = WSAV_ENABLED | WSAV_POSITION;


BTW_Imp::BTW_Imp(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData) :
  d_batWind(bw),
  d_batData(batData),
  d_unit(NoBattleUnit),
  d_hex(),
  d_hexValid(false),
  d_cx((ScreenBase::dbX() * 125) / ScreenBase::baseX()),
  d_cy((ScreenBase::dbY() * 77) / ScreenBase::baseY()),
  d_dib(new DrawDIBDC(d_cx, d_cy)),
  d_draw(d_dib, d_batWind, d_batData, d_cx, d_cy)
//  d_draw(ShowObject(d_dib, d_batWind, d_batData, d_cx, d_cy))
{
  RECT r;
  // set up default position
  SetRect(&r, 400, 200, d_cx, d_cy);

  HWND hWnd = createWindow(
                0,
                NULL,
                WS_POPUP | WS_CLIPSIBLINGS,
                r.left, r.top, d_cx, d_cy,
                hParent, NULL   // , APP::instance()
  );
  ASSERT(hWnd != NULL);

  getState(s_registryName, s_saveFlags, true);
}

BTW_Imp::~BTW_Imp()
{
    selfDestruct();
    if(d_dib)
        delete d_dib;
}

LRESULT BTW_Imp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//  LRESULT result;

  switch(msg)
  {
         HANDLE_MSG(hWnd, WM_CREATE,  onCreate);
         HANDLE_MSG(hWnd, WM_PAINT,   onPaint);
         HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
         HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
         HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
         default:
                return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL BTW_Imp::onCreate(HWND hwnd, CREATESTRUCT* cs)
{
  // allocate dib
  ASSERT(d_dib);
  drawBackground();
  d_dib->setBkMode(TRANSPARENT);

  return TRUE;
}

void BTW_Imp::onDestroy(HWND hwnd)
{
  saveState(s_registryName, s_saveFlags);
}

BOOL BTW_Imp::onEraseBk(HWND hwnd, HDC hdc)
{
  return True;
}

UINT BTW_Imp::onNCHitTest(HWND hwnd, int x, int y)
{
  return HTCAPTION;
}

void BTW_Imp::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  if(d_dib)
  {
         BitBlt(hdc, 0, 0, d_cx, d_cy,
                d_dib->getDC(), 0, 0, SRCCOPY);
  }

  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hwnd, &ps);
}

void BTW_Imp::redraw()
{
  InvalidateRect(getHWND(), NULL, FALSE);
  UpdateWindow(getHWND());
}

void BTW_Imp::reset()
{
    d_hexValid = false;
}

// return True if hex contains a unit
bool BTW_Imp::update(const HexCord& hex)
{
  if(!d_hexValid || (hex != d_hex))
  {
       IFDEBUG(d_batData->assertHexOnMap(hex));

       d_hexValid = true;
       d_hex = hex;

       if(isVisible())
       {
          drawInfo();
       }
  }

  return (d_unit != NoBattleUnit);
}

void BTW_Imp::drawInfo()
{
    // first find any unit that may be in the hex
    const BattleHexMap* hexMap = d_batData->hexMap();
    BattleHexMap::const_iterator lower;
    BattleHexMap::const_iterator higher;

    CRefBattleUnit unit = NoBattleUnit;

    if(d_hexValid)
    {
        if(hexMap->find(d_hex, lower, higher))
           unit = hexMap->unit(lower);

       d_unit = unit;

       drawBackground();

       /*
       * Display Unit Information
       */

       if(d_unit != NoBattleUnit)
       {
          CRefBattleCP cp = BobUtility::getCP(d_unit);
          d_draw.drawUnitInfo(cp, d_hex);
       }

       else
       {
          d_draw.drawHexInfo(d_hex);
       }
    }

    redraw();
}

void BTW_Imp::drawBackground()
{
  const DrawDIB* fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground);
  ASSERT(fillDib);

  ASSERT(d_dib);
  d_dib->rect(0, 0, d_cx, d_cy, fillDib);

  RECT r;
  SetRect(&r, 0, 0, d_cx, d_cy);

  CustomBorderWindow cw(scenario->getBorderColors());
  cw.drawThinBorder(d_dib->getDC(), r);
}

/*------------------------------------------------------
 * Access
 */

BattleTrackingWindow::BattleTrackingWindow(HWND hParent, BattleWindowsInterface* bw, RCPBattleData batData) :
  d_trackWindow(new BTW_Imp(hParent, bw, batData))
{
    ASSERT(d_trackWindow);
}

BattleTrackingWindow::~BattleTrackingWindow()
{
    delete d_trackWindow;
}

void BattleTrackingWindow::reset()
{
    d_trackWindow->reset();
}

bool BattleTrackingWindow::update(const HexCord& hex)
{
    return d_trackWindow->update(hex);
}

void BattleTrackingWindow::show(bool visible)
{
    d_trackWindow->show(visible);
    if(visible && d_trackWindow->isEnabled())
        d_trackWindow->drawInfo();
}

void BattleTrackingWindow::toggle()
{
    show(!isVisible());
}


int BattleTrackingWindow::width()
{
  return d_trackWindow->width();
}

int BattleTrackingWindow::height()
{
  return d_trackWindow->height();
}

bool BattleTrackingWindow::isVisible() const
{
  return d_trackWindow->isVisible();
}

bool BattleTrackingWindow::isEnabled() const
{
  return d_trackWindow->isVisible();
}

void BattleTrackingWindow::enable(bool visible)
{
    d_trackWindow->enable(visible);
}

HWND BattleTrackingWindow::getHWND() const
{
    return d_trackWindow->getHWND();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
