/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATSIDE_HPP
#define BATSIDE_HPP

#ifndef __cplusplus
#error batside.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Side Window
 *
 * Used to dispay information about what is under the cursor
 *
 *----------------------------------------------------------------------
 */

#include "refptr.hpp"
#include "batdata.hpp"	// for CPBattleData
#include "btwin_i.hpp"	// for PBattleWindows

namespace BattleMeasure { class HexCord; };

namespace BattleWindows_Internal
{

class BattleSideBar : public RefBaseDel
{
	public:
		BattleSideBar(RPBattleWindows batWind, RCPBattleData battleData);
		~BattleSideBar();

		HWND getHWND() const;

		void setHex(const BattleMeasure::HexCord& hex);
			// Set up current hex for terrain info display

	private:
		// Disable copying
		BattleSideBar(const BattleSideBar&);
		BattleSideBar& operator = (const BattleSideBar&);

		class BattleSideBarImp* d_imp;		// Hidden Implementation
};

};	// namespace BattleWindows_Internal

#endif /* BATSIDE_HPP */

