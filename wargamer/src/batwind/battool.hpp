/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATTOOL_HPP
#define BATTOOL_HPP

#ifndef __cplusplus
#error battool.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Zoom Icon window
 *
 *----------------------------------------------------------------------
 */

#include "refptr.hpp"
#include "wind.hpp"


class HWNDbase;

namespace BattleWindows_Internal
{
class BattleToolWindImp;

class BattleToolWind :
	public RefBaseDel,
    public Window
{
		BattleToolWindImp* d_imp;
	public:
		BattleToolWind(HWNDbase* parent);	//, PBattleWindows batWind);
		~BattleToolWind();

		void enableButton(int id, bool enabled);
		void pushButton(int id, bool pushed);

		HWND getHWND() const;
        virtual bool isVisible() const;
        virtual bool isEnabled() const;
        virtual void show(bool visible);
        virtual void enable(bool enable);
};

};	// namespace BattleWindows_Internal


#endif /* BATTOOL_HPP */

