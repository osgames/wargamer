/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATW_OOB
#define BATW_OOB

#include "batdata.hpp"
#include "wind.hpp"
#include "palwind.hpp"

#include "bob_cp.hpp"
#include "bob_sp.hpp"


/*

        Battle OOB User Interface

*/

class BattleOBUserInterface {

public:

    BattleOBUserInterface(void) { }
    virtual ~BattleOBUserInterface(void) { }

    virtual void onDragUnit(BattleUnit * unit) = 0;

    virtual void onDragEnd(void) = 0;

    virtual void onSelectUnit(BattleUnit * unit) = 0;

    virtual void onRightClickUnit(BattleUnit * unit) = 0;

    virtual void onDoubleClickUnit(BattleUnit * unit) = 0;

};



class BattleOBTreeViewInfo {
public:

    BattleOBTreeViewInfo(BattleCP * bat_cp) {
        cp = bat_cp;
        sp = 0;
    }

    BattleOBTreeViewInfo(BattleSP * bat_sp) {
        cp = 0;
        sp = bat_sp;
    }
    
    BattleCP * cp;
    BattleSP * sp;
};

/*

        Battle OOB treeview window
        
*/

class BattleOBTreeView {

public:

    BattleOBUserInterface * m_OBUser;

    HIMAGELIST m_ImageList;
    HTREEITEM m_hTreeItem;
    BattleUnit * m_Unit;

    bool m_bUseImages;
    bool m_bShowStrengthPoints;
    bool m_bShowTopLeader;

    BattleCP * m_RootNode;

    BattleOBTreeView(BattleOBUserInterface * ob_user) {
        m_OBUser = ob_user;
        m_bUseImages = true;
        m_bShowStrengthPoints = true;
        m_bShowTopLeader = false;
    }


    virtual ~BattleOBTreeView(void) { }
    
    HWND createTV(HWND hparent, int id);

    void createImageList(HWND hwnd);
    
    void setTreeRoot(BattleCP * cp) { m_RootNode = cp; }
    BattleCP * getTreeRoot(void) { return m_RootNode; }

    HTREEITEM getHTreeItem(void) { return m_hTreeItem; }
    BattleUnit * getUnit(void) { return m_Unit; }

    void addItems(HWND hwndTV, BattleData * batdata);
    void addCommandPositions(HWND hwndTV, BattleCP * cp, TV_INSERTSTRUCT& tvi, HTREEITEM hparent, BattleData * batdata);
    void addStrengthPoints(HWND hwndTV, BattleCP * cp, TV_INSERTSTRUCT& tvi, HTREEITEM hparent, BattleData * batdata);
    HTREEITEM addUnit(HWND hwndTV, TV_INSERTSTRUCT& tvi, BattleUnit * unit, BattleData * batdata);

    void SelectUnit(BattleCP * cp);
    void DeselectUnit(BattleCP * cp);
    
    LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);


};



#endif
