/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATWIND_HPP
#define BATWIND_HPP

#ifndef __cplusplus
#error batwind.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game Windows
 *
 *----------------------------------------------------------------------
 */

#include "batwind_dll.h"
#include "refptr.hpp"
#include "batdata.hpp"
#include "control.hpp"
#include "b_result.hpp" 

#include "bw_tiny.hpp"

using namespace BattleWindows_Internal;

class BattleMapWind;
class BatTimeControl;
class BattleGameInterface;
class BattleMessageInfo;
// class BResultWindowUser;
struct BattleInfo;
class BattleFinish;

namespace WG_BattleWindows
{

class BResultWindowUser : public BattleResultTypes
{
    public:
        virtual const BattleResults* battleResults() const = 0;
        virtual void resultsFinished(BattleContinueMode mode) = 0;
        virtual bool isHistorical() const = 0;
};


class BattleWindowUser : public BResultWindowUser
{
    public:
        virtual void requestBattleOver(const BattleFinish& result) = 0;
            // Battle finishes somehow.. this can be called from another thread

        virtual void requestOpenBattle() = 0;
            // Open selected from File Menu

        virtual void requestSaveBattle(bool prompt) = 0;
            // Save or Save-As selected from File Menu

        virtual void setController(Side side, GamePlayerControl::Controller control) = 0;

        virtual BatTimeControl* timeControl() = 0;
        virtual const BatTimeControl* timeControl() const = 0;

        virtual BattleData* battleData() = 0;
        virtual const BattleData* battleData() const  = 0;

        virtual void selectMapFinished() = 0;
        virtual void deployFinished() = 0;

};

class BattleWindowsImp;

class BattleWindows :
		  public RefBaseDel
{
        BattleWindowsImp* d_imp;                // Implementation class

	public:
        BATWIND_DLL BattleWindows(HWND hMain, BattleWindowUser* batGame);
        BATWIND_DLL ~BattleWindows();

        BATWIND_DLL void setGameMode();
        BATWIND_DLL void setMapSelectMode(const BattleInfo& info);
        BATWIND_DLL void setDeployMode(const BattleInfo& info);
        BATWIND_DLL void showResults(BResultWindowUser* user);

        BATWIND_DLL void timeChanged();
            // The Battle Time has changed, so redraw clock & Map

        BATWIND_DLL HWND getHWND() const;

        BATWIND_DLL BattleMapWind * getBattleMap(void);
		BATWIND_DLL BW_Locator * getTinyMap(void);
        BATWIND_DLL void sendPlayerMessage(const BattleMessageInfo&);

};

};      // namespace WG_BattleWindows


#endif /* BATWIND_HPP */

