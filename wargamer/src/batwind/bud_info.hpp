/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BUD_INFO_HPP
#define BUD_INFO_HPP

#ifndef __cplusplus
#error bud_info.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Information shared between Battle User Dialogs
 *
 *----------------------------------------------------------------------
 */

// #include "batarmy.hpp"               // batdata: Order of Battle
#include "bob_cp.hpp"           // batdata: CommandPosition
#include "bob_sp.hpp"           // batdata: CommandPosition
// #include "bobdef.hpp"                // batdata: CommandPosition

class BattleData;
class BattleMapSelect;

/*
 * Information passed to show function
 */

class BUD_Info
{
        public:

                /*
                 * Constructor
                 */

                BUD_Info()
                {
                }

                /*
                 * Functions to set up data
                 */

                bool setup(const BattleData* batData, const BattleMapSelect& info);

                CRefBattleCP cp() const { return d_unit; }
                void cp(CRefBattleCP cp) { d_unit = cp; }
                const BattleMeasure::HexCord& hex() const { return d_hex; }
                void hex(const BattleMeasure::HexCord& h) { d_hex = h; }

   private:
                CRefBattleCP d_unit;            // Unit involved with dialog
                BattleMeasure::HexCord d_hex;
};













/*
Simple list of CPs
*/
class CPListEntry : public SLink {

public:

	CPListEntry(void) { }
	~CPListEntry(void) { }

	const BattleCP * cp() const { return d_cp; }
	void cp(const BattleCP * cp) { d_cp = cp; }

private:

	const BattleCP * d_cp;
};


/*
Cursor mode enumerations
*/
enum BattleCursorEnum {
	Cursor_Normal,
	Cursor_OverUnit,
	Cursor_DragUnit,
	Cursor_BadHex,
	Cursor_TargetMode,
	Cursor_ValidTarget,
	Cursor_InvalidTarget,
	Cursor_AttatchUnit
};



#endif /* BUD_INFO_HPP */

