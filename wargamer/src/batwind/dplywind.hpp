/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"

#ifndef DPLYWIND_HPP
#define DPLYWIND_HPP

#include "batwind_dll.h"
#include "dplyuser.hpp"
#include "wind.hpp"
#include "palwind.hpp"

class BattleData;
class DeploySelectionWindows;
class DeployPlacementWindows;

/*------------------------------------------------------------------------------------------------

        Main deployment windows class

--------------------------------------------------------------------------------------------------*/

enum DeployScreenEnum {
    SelectionScreen,
    PlacementScreen
};

class DeploymentWindows : public WindowChangeUser, public SubClassWindow, public PaletteWindow 
{

public:

    BATWIND_DLL DeploymentWindows(DeploymentUser* deployuser, HWND hparent, BattleData* batdata, const BattleInfo& battleinfo);
    BATWIND_DLL ~DeploymentWindows();
    BATWIND_DLL void SetSize();

    BATWIND_DLL void NextScreen(void);
    BATWIND_DLL void LastScreen(void);

private:

    HWND m_ParentHwnd;
    
    DeployScreenEnum m_DeployScreen;
    DeploymentUser* m_lpDeployUser;

    BattleData* m_lpBattleData;
    BattleInfo m_lpBattleInfo;

    RECT m_WindowRect;

    DeploySelectionWindows * m_lpDeploySelectionWindows;
    DeployPlacementWindows * m_lpDeployPlacementWindows;

    void CreateWindows(void);
    void DestroyWindows(void);

    void CreateSubScreen(void);


    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hWnd);
    void onPaint(HWND hWnd);
    void onNCPaint(HWND hwnd, HRGN hrgn);
    BOOL onEraseBk(HWND hwnd, HDC hdc);
    void onSize(HWND hwnd, UINT state, int cx, int cy);


};


#endif



    
