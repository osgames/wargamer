/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Information shared between Unit User Interface Dialogs
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bud_info.hpp"
#include "btwin_i.hpp"     // batdisp: Interface for batmap user
#include "batdata.hpp"     // batdata: Battle Data
#include "hexmap.hpp"      // batdata: BattleHexMap
#include "bobutil.hpp"     // batdata: Order of Battle Helper functions

//namespace BattleWindows_Internal
//{

/*
 * Set up data from a given hex
 *
 * Returns true if there are any units in the hex
 */

bool BUD_Info::setup(const BattleData* batData, const BattleMapSelect& info)
{
   BattleMeasure::HexCord hex = batData->getHex(info.location());
   d_hex = hex;

   const BattleHexMap* hexMap = batData->hexMap();
   BattleHexMap::const_iterator lower;
   BattleHexMap::const_iterator higher;
   if(hexMap->find(hex, lower, higher))
   {
      // We really want to add a complete list of units

      const BattleUnit* unit = hexMap->unit(lower);
      d_unit = BobUtility::getCP(unit);
      return true;
   }

   return false;
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
