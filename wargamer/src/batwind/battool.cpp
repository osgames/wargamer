/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Tool Window for zoom icons
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "battool.hpp"
#include "framewin.hpp"
#include "imglib.hpp"
#include "autoptr.hpp"
// #include "generic.hpp"
#include "palwind.hpp"
#include "palette.hpp"
#include "dib.hpp"
#include "wmisc.hpp"
#include "batres.h"
// #include "bscn_res.h"
#include "batres_s.h"
#include "scn_res.h"
#include "app.hpp"
#include "button.hpp"
#include "tooltip.hpp"
#include "help.h"
#include "msgenum.hpp"
#include "scenario.hpp"
#include "dib_util.hpp"

#ifdef DEBUG

#include "clog.hpp"
static LogFileFlush btLog("batTool.log");

#endif   // DEBUG


namespace BattleWindows_Internal
{

class BattleToolWindImp :
   public PaletteWindow,
   public WindowBaseND,
   public CustomBorderWindow
{
   enum ButtonID
   {         // Button IDs, must match up with the images
      ZoomNone = 0,
         ZoomFirst = 1,

         ZoomTo       = 1,
         ZoomDetail   = 2,
         ZoomTwoMile  = 3,
         ZoomFourMile = 4,
         ZoomOverview = 5,
         ZoomLast,
         ZoomHowMany = ZoomLast - ZoomFirst
   };

   std::auto_ptr<ImageLibrary> d_images;
   DrawDIBDC*    d_dib;
   const DIB*    d_fillDIB;

   // PBattleWindows         d_batWind;

   static DWORD helpIDs[2 * (ZoomHowMany+1)];
   static TipData tipData[ZoomHowMany+1];
   static const int ButtonOnOffset;
   static const int ButtonDisabledOffset;

   static int messageID[ZoomHowMany];

   public:
   BattleToolWindImp(HWNDbase* hParent);  //, PBattleWindows batWind);
   ~BattleToolWindImp();

   void enableButton(int id, bool enabled);
   void pushButton(int id, bool pushed);

   private:

   LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
   void onDestroy(HWND hWnd);
   void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem);
   void onPaint(HWND hWnd);
   void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
   LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
   void onMove(HWND hWnd, int x, int y);
   BOOL onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg);
   BOOL onEraseBk(HWND hwnd, HDC hdc);

   // void setupButtonStates();

   static int getMenuID(ButtonID id);
   static ButtonID getButtonID(int menuID);
};


const int BattleToolWindImp::ButtonOnOffset = 5;
const int BattleToolWindImp::ButtonDisabledOffset = 10;


DWORD BattleToolWindImp::helpIDs[2 * (ZoomHowMany+1)] = {
  ZoomTo,         IDH_ZoomOut,
  ZoomOverview,   IDH_ZoomIn,
  ZoomTwoMile,    IDH_ZoomIn,
  ZoomFourMile,   IDH_ZoomIn,
  ZoomDetail,     IDH_ZoomIn,
  0, 0
};

TipData BattleToolWindImp::tipData[ZoomHowMany+1] = {
   {  ZoomTo,        TTS_BZ_ZoomTo,       SWS_BZ_ZoomTo        },
   {  ZoomOverview,  TTS_BZ_ZoomOverview, SWS_BZ_ZoomOverview  },
   {  ZoomTwoMile,   TTS_BZ_ZoomTwoMile,  SWS_BZ_ZoomTwoMile   },
   {  ZoomFourMile,  TTS_BZ_ZoomFourMile, SWS_BZ_ZoomFourMile  },
   {  ZoomDetail,    TTS_BZ_ZoomDetail,   SWS_BZ_ZoomDetail    },

   EndTipData
};

/*
 * Menu ID's associated with each button
 * This must match up with ButtonID enumeration
 */

int BattleToolWindImp::messageID[] =
{
   IDM_BAT_ZOOMIN,
   IDM_BAT_ZOOMDETAIL,
   IDM_BAT_ZOOMTWOMILE,
   IDM_BAT_ZOOMFOURMILE,
   IDM_BAT_ZOOMOVERVIEW,
};

BattleToolWindImp::BattleToolWindImp(HWNDbase* hParent) : // , PBattleWindows batWind) :
   PaletteWindow(),
   WindowBaseND(),
   CustomBorderWindow(scenario->getBorderColors()),
   d_dib(0),
   d_fillDIB(scenario->getSideBkDIB(SIDE_Neutral))
{
   static const int SZ_H_BATTLE_TOOL = 32;

   HWND hWnd = createWindow(0,
      // className(),         // Class Name
      "Battle Toolbar",                   // Window Name
      WS_CHILD |
      WS_CLIPSIBLINGS,        /* Style */
      0, 0, 128, SZ_H_BATTLE_TOOL,     // Position and width will get set from campwind
      hParent->getHWND(),                 /* parent window */
      NULL
      // hParent->getInstance()
      );

   ShowWindow(getHWND(), SW_SHOW);
}


BattleToolWindImp::~BattleToolWindImp()
{
   selfDestruct();
   delete d_dib; d_dib = 0;
   // delete d_fillDIB;
   d_fillDIB = 0;
}


int BattleToolWindImp::getMenuID(ButtonID id)
{
   ASSERT((id >= ZoomFirst) && (id < ZoomLast));

   if((id >= ZoomFirst) && (id < ZoomLast))
      return messageID[id - ZoomFirst];
   else
      return 0;
}

BattleToolWindImp::ButtonID BattleToolWindImp::getButtonID(int menuID)
{
   for(int i = 0; i < ZoomHowMany; i++)
   {
      if(messageID[i] == menuID)
         return static_cast<ButtonID>(i + ZoomFirst);
   }

   return ZoomNone;
}


LRESULT BattleToolWindImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG
   btLog.printf("procMessage(%s)",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   LRESULT l;

   if(handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
   HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
   HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
   HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
   HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
   HANDLE_MSG(hWnd, WM_PAINT, onPaint);
   HANDLE_MSG(hWnd, WM_CONTEXTMENU, onContextMenu);
   HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
   HANDLE_MSG(hWnd, WM_SETCURSOR, onSetCursor);
   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL BattleToolWindImp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
   ASSERT(scenario != 0);

   // #if 0 // def DEBUG
   //    HINSTANCE batResDLL = xScenario->getBattleResDLL();
   // #else
   //    HINSTANCE batResDLL = scenario->getBattleResDLL();
   // #endif
   // SWG:04Aug98 : battleResDLL is obsolete
   HINSTANCE batResDLL = scenario->getScenarioResDLL();


   ImagePos* imagePos = reinterpret_cast<ImagePos*>(loadResource(batResDLL, MAKEINTRESOURCE(PR_BATTLE_ZOOMICONS), RT_IMAGEPOS));
   ASSERT(imagePos != NULL);

#ifdef _MSC_VER
   d_images = std::auto_ptr<ImageLibrary>(new ImageLibrary(batResDLL, MAKEINTRESOURCE(BM_BATTLE_ZOOMICONS), NumBattleZoomImages, imagePos, True));
#else
   d_images.reset(new ImageLibrary(batResDLL, MAKEINTRESOURCE(BM_BATTLE_ZOOMICONS), NumBattleZoomImages, imagePos, True));
#endif
// #ifdef __SGI_STL_AUTO_PTR_H
//    d_images = auto_ptr<ImageLibrary>(new ImageLibrary(batResDLL, MAKEINTRESOURCE(BM_BATTLE_ZOOMICONS), NumBattleZoomImages, imagePos, True));
// #else
//    d_images = new ImageLibrary(batResDLL, MAKEINTRESOURCE(BM_BATTLE_ZOOMICONS), NumBattleZoomImages, imagePos, True);
// #endif
   ASSERT(d_images.get() != 0);

   // #ifdef __SGI_STL_AUTO_PTR_H
   //    d_dib = auto_ptr<DrawDIBDC>(new DrawDIBDC(21, 20));
   // #else
   //    d_dib = new DrawDIBDC(21, 20);
   // #endif

   /*
   * Add some buttons
   */

   HWND hButton;
   initToolButton(APP::instance());

   /*
   * Zoom buttons
   */

   int buttonWidth;
   int buttonHeight;
   d_images->getImageSize(ZoomTo, buttonWidth, buttonHeight);

   int buttonY = (lpCreateStruct->cy - buttonHeight) / 2;

   hButton = addToolButton(hWnd, ZoomTo,
      2 + ThinBorder,buttonY,buttonWidth,buttonHeight);

   int buttonX = lpCreateStruct->cx - buttonWidth - (2 + ThinBorder);

   hButton = addToolButton(hWnd, ZoomOverview,   buttonX, buttonY, buttonWidth, buttonHeight);
   buttonX -= buttonWidth + 2;
   hButton = addToolButton(hWnd, ZoomFourMile,   buttonX, buttonY, buttonWidth, buttonHeight);
   buttonX -= buttonWidth + 2;
   hButton = addToolButton(hWnd, ZoomTwoMile,    buttonX, buttonY, buttonWidth, buttonHeight);
   buttonX -= buttonWidth + 2;
   hButton = addToolButton(hWnd, ZoomDetail,     buttonX, buttonY, buttonWidth, buttonHeight);


   /*
   * Setup the initial states
   */

   // setupButtonStates();

   g_toolTip.addTips(hWnd, tipData);

   return TRUE;
}

void BattleToolWindImp::onDestroy(HWND hWnd)
{
   g_toolTip.delTips(hWnd);

   // Everything else is auto deleted from auto_ptr
}

void BattleToolWindImp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
#ifdef DEBUG
   btLog.printf("ToolWindWindImp::onCommand(%d,%d)", (int) id, (int) codeNotify);
#endif

   ASSERT((id >= ZoomFirst) && (id < ZoomLast));
   ButtonID bID = static_cast<ButtonID>(id);

   FORWARD_WM_COMMAND(GetParent(hWnd), getMenuID(bID), hwndCtl, codeNotify, SendMessage);
}

void BattleToolWindImp::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem)
{
   HPALETTE oldPalette = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
   RealizePalette(lpDrawItem->hDC);

   ASSERT(lpDrawItem->CtlID != 0);
   UWORD image = static_cast<UWORD>(lpDrawItem->CtlID - 1);

   if(lpDrawItem->itemState & (ODS_SELECTED | ODS_CHECKED))
      image += ButtonOnOffset;
   else if(lpDrawItem->itemState & ODS_DISABLED)
      image += ButtonDisabledOffset;

   // I'm going to blit straight to screen
   //---- Can't do that because it isn't masked then!
   // d_images->blit(lpDrawItem->hDC, image, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top);

   const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
   const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

   DIB_Utility::allocateDib(&d_dib, cx, cy);

   ASSERT(d_dib);


   // d_images->blit(d_dib, image, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top);
   d_images->stretchBlit(d_dib, image, 0, 0, cx, cy);
   // d_images->blit(d_dib, image, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top);
   BitBlt(lpDrawItem->hDC,
      lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, cx, cy,
      d_dib->getDC(), 0, 0, SRCCOPY);
   //    lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, SRCCOPY);

   SelectPalette(lpDrawItem->hDC, oldPalette, FALSE);

}

void BattleToolWindImp::onPaint(HWND hWnd)
{
   PAINTSTRUCT ps;
   HDC hdc = BeginPaint(hWnd, &ps);

   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);

   RECT r;
   GetClientRect(hWnd, &r);

   const int cx = r.right - r.left;
   const int cy = r.bottom - r.top;

   DIB_Utility::allocateDib(&d_dib, cx, cy);

   ASSERT(d_dib);
   ASSERT(d_fillDIB);
   d_dib->rect(0, 0, cx, cy, d_fillDIB);
   BitBlt(hdc, 0, 0, cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);

   drawThinBorder(hdc, r);
   SelectPalette(hdc, oldPal, FALSE);

   EndPaint(hWnd, &ps);
}

void BattleToolWindImp::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
//   WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, reinterpret_cast<DWORD>(helpIDs));
}


BOOL BattleToolWindImp::onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg)
{
#if 0
      if(mapWindow->isZooming())
   {
      ASSERT(scenario->getZoomCursor() != NULL);
      SetCursor(scenario->getZoomCursor());
      return TRUE;
   }
   else
#endif
      return FORWARD_WM_SETCURSOR(hwnd, hwndCursor, codeHitTest, msg, defProc);
}

BOOL BattleToolWindImp::onEraseBk(HWND hwnd, HDC hdc)
{
   return TRUE;
}


#if 0
void BattleToolWindImp::setupButtonStates()
{
}
#endif

void BattleToolWindImp::enableButton(int id, bool enabled)
{
   ButtonID bID = getButtonID(id);
   if(bID != ZoomNone)
   {
      HWND hButton = GetDlgItem(getHWND(), bID);

      ASSERT(hButton != NULL);

      if(hButton != NULL)
         EnableWindow(hButton, enabled ? TRUE : FALSE);
   }
}

void BattleToolWindImp::pushButton(int id, bool pushed)
{
   ButtonID bID = getButtonID(id);
   if(bID != ZoomNone)
   {
      HWND hButton = GetDlgItem(getHWND(), bID);

      ASSERT(hButton != NULL);

      if(hButton != NULL)
      {
         SendMessage(hButton, BM_SETCHECK, pushed ? BST_CHECKED : BST_UNCHECKED, 0);
         SendMessage(hButton, BM_SETSTATE, pushed ? TRUE : FALSE, 0);
      }
   }
}

/*
 * Insulated class Implementation
 */


BattleToolWind::BattleToolWind(HWNDbase* hParent)  //, PBattleWindows batWind)
{
   d_imp = new BattleToolWindImp(hParent);   //, batWind);
}


BattleToolWind::~BattleToolWind()
{
   delete d_imp;
}


HWND BattleToolWind::getHWND() const
{
   return d_imp->getHWND();
}


bool BattleToolWind::isVisible() const
{
   return d_imp->isVisible();
}

bool BattleToolWind::isEnabled() const
{
   return d_imp->isEnabled();
}

void BattleToolWind::show(bool visible)
{
   d_imp->show(visible);
}

void BattleToolWind::enable(bool enable)
{
   d_imp->enable(enable);
}



void BattleToolWind::enableButton(int id, bool enabled)
{
   d_imp->enableButton(id, enabled);
}

void BattleToolWind::pushButton(int id, bool pushed)
{
   d_imp->pushButton(id, pushed);
}

}; // namespace BattleWindows_Internal

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
