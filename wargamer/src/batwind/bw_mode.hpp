/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BW_MODE_H
#define BW_MODE_H

#ifndef __cplusplus
#error bw_mode.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * BattleWindow Mode
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "btwin_i.hpp"      // batdisp: Interface for batmap user
#include "batmap_i.hpp"
#include "bw_tiny.hpp"

using namespace BattleWindows_Internal;

class DeferWindowPosition;
class BattleData;
class BattleMapWind;
class BattleUnitUI;

class BW_ModeOwner : virtual public BattleWindowsInterface
{
    public:
        virtual ~BW_ModeOwner() { }
        virtual const BattleData* battleData() const = 0;
        virtual BattleData* battleData() = 0;
        virtual BattleMapWind* mapWindow() = 0;
		virtual BW_Locator* tinyMap() = 0;
        virtual HWND hwnd() const = 0;

        virtual void selectMapFinished() = 0;
        virtual void deployFinished() = 0;

		virtual void setZoom(BattleMapInfo::Mode mode) = 0;
};

class BW_Mode
{
    public:

        enum BW_ID
        {
            BW_VictoryLevel,
            BW_StatusWindow,
            BW_TrackingWindow,
            BW_MapWindow,
            BW_Locator,
            BW_MessageWindow,
            BW_ToolBar
        };

        BW_Mode(BW_ModeOwner* owner) : d_owner(owner) { }
        virtual ~BW_Mode() = 0;

        virtual void positionWindows(const PixelRect& r, DeferWindowPosition* def=0) = 0;
            // Position internal windows within rectangle
        virtual void show(bool visible) = 0;

		virtual void onLButtonDown(const BattleMapSelect& info) = 0;
		virtual void onLButtonUp(const BattleMapSelect& info) = 0;
		virtual void onRButtonDown(const BattleMapSelect& info) = 0;
		virtual void onRButtonUp(const BattleMapSelect& info) = 0;
		virtual void onStartDrag(const BattleMapSelect& info) = 0;
	 	virtual void onEndDrag(const BattleMapSelect& info) = 0;
	 	virtual void onMove(const BattleMapSelect& info) = 0;
		virtual bool setCursor() = 0;

        virtual bool hasWindow(BW_ID id) = 0;

		virtual void timeChanged(void) = 0;
		virtual void mapZoomChanged(void) = 0;
		virtual BattleUnitUI * getUserInferface(void) = 0;


    protected:
        BW_ModeOwner* d_owner;
};

inline BW_Mode::~BW_Mode() { }

#endif /* BW_MODE_H */

