/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BFACECTL_HPP
#define BFACECTL_HPP

#include "batcord.hpp"

class BFacing_Imp;
class PixelPoint;

using BattleMeasure::HexPosition;

class BFacing_Ctl {
    BFacing_Imp* d_wind;
  public:
    BFacing_Ctl(HWND parent, int id);
    ~BFacing_Ctl();

    void position(const PixelPoint& p);
    void initFace(HexPosition::Facing f);
    HexPosition::Facing currentFace() const;
    void destroy();
    void hide();

    int width() const;
    int height() const;
    HWND getHWND() const;
};

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
