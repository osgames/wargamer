/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef D_SELSDE_HPP
#define D_SELSDE_HPP

#include "batwind_dll.h"
#include "wind.hpp"
#include "palwind.hpp"
#include "dib.hpp"
#include "piclib.hpp"
#include "batdata.hpp"

class WindowChangeUser;
struct BattleInfo;


class SelectionSideUser
{
    public:
        virtual HWND hwnd() const = 0;
        virtual void onFinish() = 0;
};


class SelectionSideWindow :
    public WindowBaseND,
    public PaletteWindow,
    public CustomBorderWindow
{
public:

    BATWIND_DLL SelectionSideWindow(SelectionSideUser *user, BattleData * batdata, const BattleInfo& batinfo);
    BATWIND_DLL ~SelectionSideWindow();
    BATWIND_DLL void SetSize(RECT rect);


private:

    void CreateWindows(void);
    // void DestroyWindows(void);
    void DrawDib(void);
    void DrawTextLines(DrawDIBDC * dib, RECT device_rect, char ** text_lines, int num);

    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

    void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
    void onNCPaint(HWND hwnd, HRGN hrgn);
    BOOL onEraseBk(HWND hwnd, HDC hdc);
    void onPaint(HWND hwnd);


    /*
     * Data
     */



	BattleData * d_batData;

    SelectionSideUser* d_owner;
    HWND d_parentHwnd;
	HWND d_OKButton;

	int d_fontMinimumHeight;

	RECT d_OKRect;

	RECT d_windowRect;
	RECT d_insideRect;

	RECT d_battlefieldTitleRect;
	RECT d_battlefieldRect;

	RECT d_terrainDescriptionRect;
	const char * d_fullTerrainDescription;

	RECT d_regionTitleRect;
	RECT d_regionRect;
	const char * d_regionText;

	RECT d_terrainTitleRect;
	RECT d_terrainRect;
	const char * d_terrainText;

	RECT d_elevationTitleRect;
	RECT d_elevationRect;
	const char * d_elevationText;

	RECT d_populationTitleRect;
	RECT d_populationRect;
	const char * d_populationText;

	RECT d_dateFrameRect;
	RECT d_dateTitleRect;
	RECT d_dateRect;
	char d_dateText[1024];

	RECT d_weatherFrameRect;
	RECT d_weatherTitleRect;
	RECT d_weatherTextRect;
	RECT d_weatherRect;
	const char * d_weatherText;

	RECT d_armiesFrameRect;
	RECT d_armiesTitleRect;
	RECT d_armiesSide0TextRect;
	RECT d_armiesSide0BarRect;
	RECT d_armiesSide1TextRect;
	RECT d_armiesSide1BarRect;



    const BattleInfo& d_lpBattleInfo;

    DrawDIBDC * d_screenDIB;
	DIB * d_regionDIB;
	DIB * d_terrainDIB;
	DIB * d_elevationDIB;
	DIB * d_populationDIB;
	DIB * d_weatherDIB;

};






#endif

