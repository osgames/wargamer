/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BU_DIAL_HPP
#define BU_DIAL_HPP

#ifndef __cplusplus
#error bu_dial.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Unit Dialog
 *
 * Similar to Campaign's UnitDialog
 *
 * It is a base class for some other dialogs, that provides support
 * for stacked units, and other common features
 *
 *----------------------------------------------------------------------
 */

#include "dialog.hpp"           // system: dialog box base classes
#include "cust_dlg.hpp"         // system: custom dialog box helper
#include "batord.hpp"

class PixelPoint;
using Greenius_System::CustomDialog;    // crashes if this done inside namespace

class BattleOrder;

//namespace BattleWindows_Internal
//{

class BUD_Info;

/*
 * Pure virtual class for windows within BattleUnitUI to use to
 * pass information upwards.
 */

class BUI_UnitInterface
{
        public:
                enum Mode {

					Normal,
					Attaching,
					Targetting
                };

                virtual ~BUI_UnitInterface() { }

//              virtual void sendOrder(const BattleOrder& order) = 0;
                virtual void sendOrder() = 0;
                virtual void cancelOrder() = 0;
                virtual void mode(Mode m) = 0;
                virtual Mode mode() const = 0;
                virtual void showOrders() = 0;
                virtual void showDeployment() = 0;
                virtual void showInfo() = 0;
                virtual void showLOS() {} // Unchecked added by Paul
                virtual void redrawMap() = 0;
                virtual void highlightUnit(bool on) = 0;
//              virtual void showOrderDial(const BattleOrder& order) = 0;
};

#if 0
class BattleUnitDial : public ModelessDialog
{
        public:
                BattleUnitDial(BUI_UnitInterface* owner);
                virtual ~BattleUnitDial();

                void show(const PixelPoint& p, const BUD_Info& info, const BattleOrder& order);
                        // Show new dialog
                void hide();
                        // Remove box from screen.

        private:
                virtual void onCreate(HWND hParent) = 0;
                        // Create actual window
                virtual void onShow(const BUD_Info& info) = 0;
                        // Called from show() to initialise dialog box
                virtual void onHide() = 0;
                        // Called from hide() to free up resources

        protected:
                BUI_UnitInterface* owner() { return d_owner; }
                BattleOrder* getOrder() { return &d_order; }
                void sendOrder() { d_owner->sendOrder(d_order); }
                void cancelOrder() { d_owner->cancelOrder(); }
                void showOrderDial() { d_owner->showOrderDial(d_order); }

        private:
                CustomDialog d_customDialog;    // dialog subclass routines
                bool d_shown;                                           // true if window is shown
                BUI_UnitInterface* d_owner;                     // UnitInterface
                BattleOrder d_order;
};
#endif

//};            // namespace BattleWindows_Internal

#endif /* BU_DIAL_HPP */

