/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BCLOCK_HPP
#define BCLOCK_HPP

#include "batdata.hpp"

class BattleClock_Wind;
class BatTimeControl;

class BattleClock_Int {
	    BattleClock_Wind* d_clockWind;
    public:

	    BattleClock_Int(HWND parent, RCPBattleData batData, BatTimeControl& tc);
	    ~BattleClock_Int();

        void show();
        void update();
        void destroy();

        int getWidth() const;
        void setPosition(LONG x, LONG y, LONG w, LONG h);

	    // void run();
	    // void hide();
	    // void destroy();
        // void update();

        // HWND getHWND() const;
};


#endif
