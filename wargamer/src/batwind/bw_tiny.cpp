/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Tiny Map window with controls
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bw_tiny.hpp"     // batwind:
#include "battool.hpp"     // batwind:
#include "battmap.hpp"     // batdisp:
#include "scenario.hpp"    // gamesup:
#include "wind.hpp"        // System:
#include "cust_dlg.hpp"    // System:
#include "palwind.hpp"     // System:
#include "registry.hpp"    // System:
#include "grtypes.hpp"     // System:
#include "app.hpp"         // System:
#include "batres.h"        // res:
#include "resstr.hpp"
#include <memory>    // STL:

#if !defined(NOLOG)
// #define LOG_TINYMAP
#endif


#if defined(LOG_TINYMAP)
#include "clog.hpp"
#include "msgenum.hpp"


template<class T>
LogFile& operator << (LogFile& file, const Rect<T>& r)
{
   file << '[' << r.left() << ',' << r.top() << ',' << r.width() << ',' << r.height() << ']';
   return file;
}


namespace { // Own private namespace
   LogFile logFile("bw_tiny.log");
};
#endif   // NOLOG

namespace BattleWindows_Internal
{

using Greenius_System::CustomDialog;




/*
 * Implementation of Battle Locator Window
 */

class BW_LocatorImp :
   public WindowBaseND,
   public PaletteWindow
{
   public:
      BW_LocatorImp();
      ~BW_LocatorImp();

        void create(RPBattleWindows batWind, RCPBattleData batData);
        void reset() { d_tinyWind->reset(); }

        virtual HWND getHWND() const { return WindowBaseND::getHWND(); }

      void setZoomButtons(BattleMapInfo::Mode mode)
      {
#if defined(LOG_TINYMAP)
         logFile << "setZoomButtons(" << int(mode) << "), " << sysTime << endl;
#endif
         if(d_toolWind.get())
         {
            d_toolWind->pushButton(IDM_BAT_ZOOMOVERVIEW, mode == BattleMapInfo::Strategic);
            d_toolWind->pushButton(IDM_BAT_ZOOMFOURMILE, mode == BattleMapInfo::FourMile);
            d_toolWind->pushButton(IDM_BAT_ZOOMTWOMILE, mode == BattleMapInfo::TwoMile);
            d_toolWind->pushButton(IDM_BAT_ZOOMDETAIL, mode == BattleMapInfo::OneMile);
         }
      }

      void setLocation(const BattleArea& area)
      {
#if defined(LOG_TINYMAP)
         logFile << "setLocation(" << area << "), " << sysTime << endl;
#endif
         if(d_tinyWind.get())
            d_tinyWind->setLocation(area);
      }

      void update(bool all)
      {
#if defined(LOG_TINYMAP)
         logFile << "update(" << all << "), " << sysTime << endl;
#endif
         if(d_tinyWind.get())
            d_tinyWind->update(all);
      }

#if 0
      void toggle()
      {
#if defined(LOG_TINYMAP)
         logFile << "toggle(), " << sysTime << endl;
#endif
         d_show = !d_show;
         ShowWindow(getHWND(), (d_show) ? SW_SHOW : SW_HIDE);
      }

      void show()
      {
#if defined(LOG_TINYMAP)
         logFile << "show(), " << sysTime << endl;
#endif
         if(d_show)
            ShowWindow(getHWND(), SW_SHOW);
      }

      void hide()
      {
#if defined(LOG_TINYMAP)
         logFile << "hide(), " << sysTime << endl;
#endif
         ShowWindow(getHWND(), SW_HIDE);
         d_show = False;
      }

      bool isShowing() const { return d_show; }
#endif


   private:
      LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

      /*
       * Message Handlers
       */

      void onDestroy(HWND hWnd);
      BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
      void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
      void resize();

   private:
      std::auto_ptr<TinyBattleMapWind> d_tinyWind;
      std::auto_ptr<BattleToolWind> d_toolWind;
      CustomDialog d_customDial;
      // bool d_show;

      static const char registryName[];
      static WSAV_FLAGS s_saveFlags;
};

const char BW_LocatorImp::registryName[] = "BattleLocator";
WSAV_FLAGS BW_LocatorImp::s_saveFlags = WSAV_ENABLED | WSAV_POSITION;

BW_LocatorImp::BW_LocatorImp() :
   d_customDial(scenario->getBorderColors())
   // d_show(true)
{
}

void BW_LocatorImp::create(RPBattleWindows batWind, RCPBattleData batData)
{
// const int borderWidth = GetSystemMetrics(SM_CXBORDER);
// const int captionCY = GetSystemMetrics(SM_CYCAPTION);

   PixelRect r(400, 200, 528, 300);

   try
   {
//       PixelPoint p;
//       if(getRegistry("position", &p, sizeof(POINT), registryName))
//       {
//          r.left(p.x());
//          r.top(p.y());
//       }
//
//       if(getRegistry("screen", &p, sizeof(POINT), registryName))
//             checkResolution(r, p);

      // getRegistry("showstate", &d_show, sizeof(bool), registryName);
      //  getState(registryName, s_saveFlags, true);

      // if(!getRegistry("showstate", &d_show, sizeof(bool), registryName))
      //    d_show = True;
   }
   catch(const WinError& e)
   {
      // Ignore registry errors...

   }

   HWND hWnd = createWindow(
      0,
      // className(),      // GenericClass::className(), //className(),
      InGameText::get(IDS_Overview),   // "Overview Map",
         WS_POPUP |
         WS_CAPTION |
         WS_CLIPSIBLINGS,
      r.left(),
      r.top(),
      r.width(),
      r.height(),
      batWind->hwnd(),
      NULL
      // APP::instance()
   );

   ASSERT(hWnd != NULL);

   getState(registryName, s_saveFlags, true);

   try
   {
#ifdef _MSC_VER
      d_tinyWind = std::auto_ptr<TinyBattleMapWind>(new TinyBattleMapWind(this, batWind, batData));
      d_toolWind = std::auto_ptr<BattleToolWind>(new BattleToolWind(this));
#else
      d_tinyWind.reset(new TinyBattleMapWind(this, batWind, batData));
      d_toolWind.reset(new BattleToolWind(this));
#endif
      resize();
#if 0 // moved to resize
      // SetWindowPos(d_tinyWind->getHWND(), HWND_TOP, 0,0, ?,?, SWP_SHOWWINDOW);
      PixelRect rTiny;
      GetWindowRect(d_tinyWind->getHWND(), &rTiny);
      PixelRect rTool;
      GetWindowRect(d_toolWind->getHWND(), &rTool);
      PixelDimension width = maximum(rTiny.width(), rTool.width());

      PixelDimension x = (width - rTiny.width()) / 2; // +borderWidth
      PixelDimension y = 0;   // borderWidth;

      SetWindowPos(d_tinyWind->getHWND(), HWND_TOP, x, y, rTiny.width(), rTiny.height(), 0);

      y += rTiny.height();

      SetWindowPos(d_toolWind->getHWND(), HWND_TOP, 0,y, width, rTool.height(), 0);

      y += rTool.height();

      int fullWidth = width + 2 * borderWidth;
      int fullHeight = y + 2 * borderWidth + captionCY;
      SetWindowPos(hWnd, HWND_TOP, 0,0, fullWidth, fullHeight, SWP_NOMOVE);
#endif   // resize

        d_tinyWind->show(true);
        d_toolWind->show(true);

        //---- Parent will show the outer window
      // show(true);
   }
   catch(const WinError& e)
   {
      DestroyWindow(hWnd);
      throw;
   }
}

BW_LocatorImp::~BW_LocatorImp()
{
    selfDestruct();
}

void BW_LocatorImp::resize()
{
   const int borderWidth = GetSystemMetrics(SM_CXBORDER);
   const int captionCY = GetSystemMetrics(SM_CYCAPTION);

   PixelRect rTiny;
   GetWindowRect(d_tinyWind->getHWND(), &rTiny);
   PixelRect rTool;
   GetWindowRect(d_toolWind->getHWND(), &rTool);
   PixelDimension width = maximum(rTiny.width(), rTool.width());

   PixelDimension x = (width - rTiny.width()) / 2; // +borderWidth
   PixelDimension y = 0;   // borderWidth;

   SetWindowPos(d_tinyWind->getHWND(), HWND_TOP, x, y, rTiny.width(), rTiny.height(), 0);

   y += rTiny.height();

   SetWindowPos(d_toolWind->getHWND(), HWND_TOP, 0,y, width, rTool.height(), 0);

   y += rTool.height();

   int fullWidth = width + 2 * borderWidth;
   int fullHeight = y + 2 * borderWidth + captionCY;
   SetWindowPos(hWnd, HWND_TOP, 0,0, fullWidth, fullHeight, SWP_NOMOVE);
}

LRESULT BW_LocatorImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#if defined(LOG_TINYMAP)
   logFile << "procMessage(" << getWMdescription(hWnd, msg, wParam, lParam) << ", " << sysTime << endl;
#endif

   LRESULT l;

   if(handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      HANDLE_MSG(hWnd, WM_COMMAND,  onCommand);
   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL BW_LocatorImp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
    d_customDial.setCaptionBkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
    d_customDial.setCaption(InGameText::get(IDS_Overview));
    d_customDial.init(hWnd);

   return TRUE;
}

void BW_LocatorImp::onDestroy(HWND hWnd)
{
  /*
   * Set registry
   */

//   RECT r;
//   GetWindowRect(hWnd, &r);
//
//   // set position
//   POINT p;
//   p.x = r.left;
//   p.y = r.top;
//   setRegistry("position", &p, sizeof(POINT), registryName);

  // set show state
  // setRegistry("showstate", &d_show, sizeof(Boolean), registryName);
  saveState(registryName, s_saveFlags);

  // set resolution
//   p.x = GetSystemMetrics(SM_CXSCREEN);
//   p.y = GetSystemMetrics(SM_CYSCREEN);
//   setRegistry("screen", &p, sizeof(POINT), registryName);

#if 0 // This will get done automatically, by windows and by auto_ptr
  if(d_tinyWind)
    DestroyWindow(d_tinyWind->getHWND());
  if(d_ftWindow)
    DestroyWindow(d_toolWind->getHWND());
#endif
}

/*
 * Messages from button window come through here
 * Just pass them on to parent
 */

void BW_LocatorImp::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
#if defined(LOG_TINYMAP)
   logFile << "onCommand(" << hWnd << ',' << id << ',' << hwndCtl << '.' << codeNotify << ')' << endl;
#endif

   if(codeNotify == WM_SIZE)
      resize();
   else
      FORWARD_WM_COMMAND(GetParent(hWnd), id, hwndCtl, codeNotify, SendMessage);
}

/*
 * Public Interface functions
 */

/*
 * Constructor
 */

BW_Locator::BW_Locator(RPBattleWindows batWind, RCPBattleData batData) :
   d_imp(new BW_LocatorImp())
{
    d_imp->create(batWind, batData);
}

/*
 * Destructor
 */

BW_Locator::~BW_Locator()
{
   delete d_imp;
}

void BW_Locator::reset()
{
    d_imp->reset();
}

void BW_Locator::setZoomButtons(BattleMapInfo::Mode mode)
{
   d_imp->setZoomButtons(mode);
}

void BW_Locator::setLocation(const BattleArea& area)
{
   d_imp->setLocation(area);
}

void BW_Locator::update(bool all)
{
   d_imp->update(all);
}

void BW_Locator::toggle()
{
   d_imp->toggle();
}

void BW_Locator::show(bool visible)
{
   d_imp->show(visible);
}

void BW_Locator::enable(bool visible)
{
   d_imp->enable(visible);
}


bool BW_Locator::isVisible() const
{
   return d_imp->isVisible();
}

bool BW_Locator::isEnabled() const
{
   return d_imp->isEnabled();
}

HWND BW_Locator::getHWND() const
{
    return d_imp->getHWND();
}

}; // namespace BattleWindows_Internal


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
