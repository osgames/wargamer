/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Combat Constants
 * It would be better to define these in the header file so that
 * the compiler can optimize their usage, but that leads to the
 * linker complaining about redefinitions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "c_const.hpp"

/*-------------------------------------------------------------------------
 * Distance at which enemy is spotted or lost
 *
 * This enables some hysteresis so that an enemy hovering at around
 * 24 miles won't keep triggering an aggression check and then instantly
 * lost.
 *
 * The difference between these 2 values should be more than the distance
 * that can be moved in an hour
 */

const Distance CampaignConst::enemyNearDistance = MilesToDistance(24);
const Distance CampaignConst::enemyLoseDistance = MilesToDistance(28);
const Distance CampaignConst::enemyBattleDistance = MilesToDistance(8);
const Distance CampaignConst::enemyLoseBattleDistance = MilesToDistance(12);
const Distance CampaignConst::instantBattleDistance = MilesToDistance(4);

const Distance CampaignConst::enemySightingDistanceArmy = MilesToDistance(48);
const Distance CampaignConst::enemySightingDistanceCorp = MilesToDistance(24);
const Distance CampaignConst::enemySightingDistanceDivision = MilesToDistance(8);
const Distance CampaignConst::trafficJamDistance = MilesToDistance(2);
const Distance CampaignConst::enemyTownInfoDistance = MilesToDistance(24);


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
