/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CC_CLOSE_HPP
#define CC_CLOSE_HPP

#ifndef __cplusplus
#error cc_close.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Close Unit List
 *
 *----------------------------------------------------------------------
 */

#include "sllist.hpp"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "measure.hpp"

class CampaignData;

class CloseUnitItem : public SLink {
public:
   enum { ChunkSize = 100 };

	ICommandPosition cpi;
	Distance distance;
	SPCount spCount;
	Boolean seen;

	CloseUnitItem(ICommandPosition _cpi, Distance _dist, SPCount _spCount, Boolean _seen)
	{
		cpi = _cpi;
		distance = _dist;
		spCount = _spCount;
		seen = _seen;
	}

	void* operator new(size_t size);
#ifdef _MSC_VER
	void operator delete(void* deadObject);
#else
	void operator delete(void* deadObject, size_t size);
#endif
};

class CloseUnits {
	SList<CloseUnitItem> cpList;
	ICommandPosition best;              // closest unit
	ICommandPosition bestSeen;          // closest seen unit
	Distance bestDistance;              // closest distance
	Distance bestSeenDistance;          // closest seen distance
	SPCount spCount;							// Total SP's within 24 miles
public:
	CloseUnits();
	~CloseUnits();
	void reset();

	void add(CampaignData* campData, ICommandPosition cpi, Distance d, SPCount sp);
	ICommandPosition getClosest() const;
	ICommandPosition getClosestSeen() const;
	Distance getClosestDist() const;
	Distance getClosestSeenDist() const;
	SPCount getSeenSP(Distance d) const;
	SPCount getSP() const { return spCount; }

	/*
	 * Iterator Functions
	 */

	void rewind();
	Boolean operator ++();    // Iterates through all
   Boolean seenOnly();       // Iterates through only seen units

	const CloseUnitItem& current();


#ifdef DEBUG
public:
	ICommandPosition currentUnit;
#endif


   private:
      void removeDead() const;
};



#endif /* CC_CLOSE_HPP */

