# Microsoft Developer Studio Generated NMAKE File, Based on clogic.dsp
!IF "$(CFG)" == ""
CFG=clogic - Win32 Debug
!MESSAGE No configuration specified. Defaulting to clogic - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "clogic - Win32 Release" && "$(CFG)" != "clogic - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "clogic.mak" CFG="clogic - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "clogic - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "clogic - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "clogic - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\clogic.dll"

!ELSE 

ALL : "system - Win32 Release" "ob - Win32 Release" "gamesup - Win32 Release" "campdata - Win32 Release" "$(OUTDIR)\clogic.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"campdata - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\atrition.obj"
	-@erase "$(INTDIR)\c_const.obj"
	-@erase "$(INTDIR)\c_supply.obj"
	-@erase "$(INTDIR)\c_weathr.obj"
	-@erase "$(INTDIR)\campcomb.obj"
	-@erase "$(INTDIR)\cbatmsg.obj"
	-@erase "$(INTDIR)\cbatproc.obj"
	-@erase "$(INTDIR)\cc_avoid.obj"
	-@erase "$(INTDIR)\cc_close.obj"
	-@erase "$(INTDIR)\cc_town.obj"
	-@erase "$(INTDIR)\cc_util.obj"
	-@erase "$(INTDIR)\cmsgutil.obj"
	-@erase "$(INTDIR)\condutil.obj"
	-@erase "$(INTDIR)\cu_data.obj"
	-@erase "$(INTDIR)\cu_fog.obj"
	-@erase "$(INTDIR)\cu_ldr.obj"
	-@erase "$(INTDIR)\cu_move.obj"
	-@erase "$(INTDIR)\cu_order.obj"
	-@erase "$(INTDIR)\cu_rand.obj"
	-@erase "$(INTDIR)\cu_repo.obj"
	-@erase "$(INTDIR)\cu_speed.obj"
	-@erase "$(INTDIR)\defect.obj"
	-@erase "$(INTDIR)\despatch.obj"
	-@erase "$(INTDIR)\ds_reorg.obj"
	-@erase "$(INTDIR)\ds_reposp.obj"
	-@erase "$(INTDIR)\ds_town.obj"
	-@erase "$(INTDIR)\ds_unit.obj"
	-@erase "$(INTDIR)\losses.obj"
	-@erase "$(INTDIR)\realord.obj"
	-@erase "$(INTDIR)\recover.obj"
	-@erase "$(INTDIR)\sched.obj"
	-@erase "$(INTDIR)\siege.obj"
	-@erase "$(INTDIR)\townprop.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\clogic.dll"
	-@erase "$(OUTDIR)\clogic.exp"
	-@erase "$(OUTDIR)\clogic.lib"
	-@erase "$(OUTDIR)\clogic.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CLOGIC_DLL" /Fp"$(INTDIR)\clogic.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\clogic.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:no /pdb:"$(OUTDIR)\clogic.pdb" /debug /machine:I386 /out:"$(OUTDIR)\clogic.dll" /implib:"$(OUTDIR)\clogic.lib" 
LINK32_OBJS= \
	"$(INTDIR)\atrition.obj" \
	"$(INTDIR)\c_const.obj" \
	"$(INTDIR)\c_supply.obj" \
	"$(INTDIR)\c_weathr.obj" \
	"$(INTDIR)\campcomb.obj" \
	"$(INTDIR)\cbatmsg.obj" \
	"$(INTDIR)\cbatproc.obj" \
	"$(INTDIR)\cc_avoid.obj" \
	"$(INTDIR)\cc_close.obj" \
	"$(INTDIR)\cc_town.obj" \
	"$(INTDIR)\cc_util.obj" \
	"$(INTDIR)\cmsgutil.obj" \
	"$(INTDIR)\condutil.obj" \
	"$(INTDIR)\cu_data.obj" \
	"$(INTDIR)\cu_fog.obj" \
	"$(INTDIR)\cu_ldr.obj" \
	"$(INTDIR)\cu_move.obj" \
	"$(INTDIR)\cu_order.obj" \
	"$(INTDIR)\cu_rand.obj" \
	"$(INTDIR)\cu_repo.obj" \
	"$(INTDIR)\cu_speed.obj" \
	"$(INTDIR)\defect.obj" \
	"$(INTDIR)\despatch.obj" \
	"$(INTDIR)\ds_reorg.obj" \
	"$(INTDIR)\ds_reposp.obj" \
	"$(INTDIR)\ds_town.obj" \
	"$(INTDIR)\ds_unit.obj" \
	"$(INTDIR)\losses.obj" \
	"$(INTDIR)\realord.obj" \
	"$(INTDIR)\recover.obj" \
	"$(INTDIR)\sched.obj" \
	"$(INTDIR)\siege.obj" \
	"$(INTDIR)\townprop.obj" \
	"$(OUTDIR)\campdata.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\system.lib"

"$(OUTDIR)\clogic.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "clogic - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\clogicDB.dll"

!ELSE 

ALL : "system - Win32 Debug" "ob - Win32 Debug" "gamesup - Win32 Debug" "campdata - Win32 Debug" "$(OUTDIR)\clogicDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"campdata - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\atrition.obj"
	-@erase "$(INTDIR)\c_const.obj"
	-@erase "$(INTDIR)\c_supply.obj"
	-@erase "$(INTDIR)\c_weathr.obj"
	-@erase "$(INTDIR)\campcomb.obj"
	-@erase "$(INTDIR)\cbatmsg.obj"
	-@erase "$(INTDIR)\cbatproc.obj"
	-@erase "$(INTDIR)\cc_avoid.obj"
	-@erase "$(INTDIR)\cc_close.obj"
	-@erase "$(INTDIR)\cc_town.obj"
	-@erase "$(INTDIR)\cc_util.obj"
	-@erase "$(INTDIR)\cmsgutil.obj"
	-@erase "$(INTDIR)\condutil.obj"
	-@erase "$(INTDIR)\cu_data.obj"
	-@erase "$(INTDIR)\cu_fog.obj"
	-@erase "$(INTDIR)\cu_ldr.obj"
	-@erase "$(INTDIR)\cu_move.obj"
	-@erase "$(INTDIR)\cu_order.obj"
	-@erase "$(INTDIR)\cu_rand.obj"
	-@erase "$(INTDIR)\cu_repo.obj"
	-@erase "$(INTDIR)\cu_speed.obj"
	-@erase "$(INTDIR)\defect.obj"
	-@erase "$(INTDIR)\despatch.obj"
	-@erase "$(INTDIR)\ds_reorg.obj"
	-@erase "$(INTDIR)\ds_reposp.obj"
	-@erase "$(INTDIR)\ds_town.obj"
	-@erase "$(INTDIR)\ds_unit.obj"
	-@erase "$(INTDIR)\losses.obj"
	-@erase "$(INTDIR)\realord.obj"
	-@erase "$(INTDIR)\recover.obj"
	-@erase "$(INTDIR)\sched.obj"
	-@erase "$(INTDIR)\siege.obj"
	-@erase "$(INTDIR)\townprop.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\clogicDB.dll"
	-@erase "$(OUTDIR)\clogicDB.exp"
	-@erase "$(OUTDIR)\clogicDB.ilk"
	-@erase "$(OUTDIR)\clogicDB.lib"
	-@erase "$(OUTDIR)\clogicDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CLOGIC_DLL" /Fp"$(INTDIR)\clogic.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\clogic.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:yes /pdb:"$(OUTDIR)\clogicDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\clogicDB.dll" /implib:"$(OUTDIR)\clogicDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\atrition.obj" \
	"$(INTDIR)\c_const.obj" \
	"$(INTDIR)\c_supply.obj" \
	"$(INTDIR)\c_weathr.obj" \
	"$(INTDIR)\campcomb.obj" \
	"$(INTDIR)\cbatmsg.obj" \
	"$(INTDIR)\cbatproc.obj" \
	"$(INTDIR)\cc_avoid.obj" \
	"$(INTDIR)\cc_close.obj" \
	"$(INTDIR)\cc_town.obj" \
	"$(INTDIR)\cc_util.obj" \
	"$(INTDIR)\cmsgutil.obj" \
	"$(INTDIR)\condutil.obj" \
	"$(INTDIR)\cu_data.obj" \
	"$(INTDIR)\cu_fog.obj" \
	"$(INTDIR)\cu_ldr.obj" \
	"$(INTDIR)\cu_move.obj" \
	"$(INTDIR)\cu_order.obj" \
	"$(INTDIR)\cu_rand.obj" \
	"$(INTDIR)\cu_repo.obj" \
	"$(INTDIR)\cu_speed.obj" \
	"$(INTDIR)\defect.obj" \
	"$(INTDIR)\despatch.obj" \
	"$(INTDIR)\ds_reorg.obj" \
	"$(INTDIR)\ds_reposp.obj" \
	"$(INTDIR)\ds_town.obj" \
	"$(INTDIR)\ds_unit.obj" \
	"$(INTDIR)\losses.obj" \
	"$(INTDIR)\realord.obj" \
	"$(INTDIR)\recover.obj" \
	"$(INTDIR)\sched.obj" \
	"$(INTDIR)\siege.obj" \
	"$(INTDIR)\townprop.obj" \
	"$(OUTDIR)\campdataDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\systemDB.lib"

"$(OUTDIR)\clogicDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("clogic.dep")
!INCLUDE "clogic.dep"
!ELSE 
!MESSAGE Warning: cannot find "clogic.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "clogic - Win32 Release" || "$(CFG)" == "clogic - Win32 Debug"
SOURCE=.\atrition.cpp

"$(INTDIR)\atrition.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\c_const.cpp

"$(INTDIR)\c_const.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\c_supply.cpp

"$(INTDIR)\c_supply.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\c_weathr.cpp

"$(INTDIR)\c_weathr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campcomb.cpp

"$(INTDIR)\campcomb.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cbatmsg.cpp

"$(INTDIR)\cbatmsg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cbatproc.cpp

"$(INTDIR)\cbatproc.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cc_avoid.cpp

"$(INTDIR)\cc_avoid.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cc_close.cpp

"$(INTDIR)\cc_close.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cc_town.cpp

"$(INTDIR)\cc_town.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cc_util.cpp

"$(INTDIR)\cc_util.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cmsgutil.cpp

"$(INTDIR)\cmsgutil.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\condutil.cpp

"$(INTDIR)\condutil.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cu_data.cpp

"$(INTDIR)\cu_data.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cu_fog.cpp

"$(INTDIR)\cu_fog.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cu_ldr.cpp

"$(INTDIR)\cu_ldr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cu_move.cpp

"$(INTDIR)\cu_move.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cu_order.cpp

"$(INTDIR)\cu_order.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cu_rand.cpp

"$(INTDIR)\cu_rand.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cu_repo.cpp

"$(INTDIR)\cu_repo.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cu_speed.cpp

"$(INTDIR)\cu_speed.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\defect.cpp

"$(INTDIR)\defect.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\despatch.cpp

"$(INTDIR)\despatch.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ds_reorg.cpp

"$(INTDIR)\ds_reorg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ds_reposp.cpp

"$(INTDIR)\ds_reposp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ds_town.cpp

"$(INTDIR)\ds_town.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ds_unit.cpp

"$(INTDIR)\ds_unit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\losses.cpp

"$(INTDIR)\losses.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\realord.cpp

"$(INTDIR)\realord.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\recover.cpp

"$(INTDIR)\recover.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sched.cpp

"$(INTDIR)\sched.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\siege.cpp

"$(INTDIR)\siege.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\townprop.cpp

"$(INTDIR)\townprop.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "clogic - Win32 Release"

"campdata - Win32 Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" 
   cd "..\campLogic"

"campdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campLogic"

!ELSEIF  "$(CFG)" == "clogic - Win32 Debug"

"campdata - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" 
   cd "..\campLogic"

"campdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campLogic"

!ENDIF 

!IF  "$(CFG)" == "clogic - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\campLogic"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campLogic"

!ELSEIF  "$(CFG)" == "clogic - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\campLogic"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campLogic"

!ENDIF 

!IF  "$(CFG)" == "clogic - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\campLogic"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campLogic"

!ELSEIF  "$(CFG)" == "clogic - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\campLogic"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campLogic"

!ENDIF 

!IF  "$(CFG)" == "clogic - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\campLogic"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campLogic"

!ELSEIF  "$(CFG)" == "clogic - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\campLogic"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campLogic"

!ENDIF 


!ENDIF 

