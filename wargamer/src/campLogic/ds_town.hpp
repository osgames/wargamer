/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DS_TOWN_HPP
#define DS_TOWN_HPP

#ifndef __cplusplus
#error ds_town.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Despatch Module for orders to towns
 *
 *----------------------------------------------------------------------
 */

#include "clogic_dll.h"
#include "townbld.hpp"

#include "despatch.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "c_obutil.hpp"


class DS_TownOrder : public DespatchMessage {
	ITown d_town;
	BuildWhat d_what;
	BuildItem d_buildItem;

	// Unimplemented copy constructor and assignment operator

	CLOGIC_DLL DS_TownOrder(const DS_TownOrder&);	
	CLOGIC_DLL DS_TownOrder& operator = (const DS_TownOrder&);
 public:
 	CLOGIC_DLL DS_TownOrder(ITown town, BuildWhat what, const BuildItem& b);
	CLOGIC_DLL DS_TownOrder(void) { }
		// Constructor

	CLOGIC_DLL void process(CampaignData* campData);
		// Virtual process function

	CLOGIC_DLL virtual int pack(void * buffer, void * gamedata);
	CLOGIC_DLL virtual void unpack(void * buffer, void * gamedata);

#ifdef DEBUG
	CLOGIC_DLL String getName();
		// virtual debug function to display messages
#endif
};




struct TownOrder 		// Used for a namespace
{
	CLOGIC_DLL static void send(ITown town, BuildWhat what, const BuildItem& b);
};

#endif /* DS_TOWN_HPP */

