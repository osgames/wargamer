/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "condutil.hpp"
#include "campdint.hpp"
#include "campctrl.hpp"
#include "town.hpp"
#include "scenario.hpp"
#include "defect.hpp"
#include "nations.hpp"

using namespace WG_CampaignConditions;

void CampaignConditionUtil::instantUpdate(CampaignLogicOwner* campGame, ConditionType::Type typeID, ConditionTypeData* data)
{
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

  ConditionList& conditions = campData->getConditions();
  conditions.update(campGame, typeID, data);

  // see if we have any defecting nations
  for(Nationality n = 0; n < scenario->getNumNations(); n++)
  {
      if(campData->getNations().wantsToDefect(n))
         NationDefectProc::nationDefects(campGame, n);
  }
}


void CampaignConditionUtil::dailyUpdate(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

  ConditionList& conditions = campData->getConditions();

  conditions.dailyUpdate(campGame);

  // see if we have any defecting nations
  for(Nationality n = 0; n < scenario->getNumNations(); n++)
  {
      if(campData->getNations().wantsToDefect(n))
         NationDefectProc::nationDefects(campGame, n);
  }
}

#if 0
static void CampaignConditionUtil::beginAction(CampaignLogicOwner* campGame, Conditions* condition)
{
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

  ASSERT(condition != 0);

  condition->doAction(campData);
}
#endif


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
