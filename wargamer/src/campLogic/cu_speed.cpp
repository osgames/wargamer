/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cu_speed.hpp"
#include "cu_data.hpp"
#include "c_const.hpp"
#include "cc_util.hpp"
#include "campdint.hpp"
#include "scenario.hpp"
#include "town.hpp"
#include "compos.hpp"
#include "armies.hpp"
#include "weather.hpp"
#include "wg_rand.hpp"

#ifdef DEBUG_SPEED
 #include "clog.hpp"
 static LogFileFlush speedLog("speed.log");
#endif

//const int AbilityRatings = 5;
//const int NSpeedValues = 10;

int getSpeed(Attribute ability)
{
  enum {
    Bad,
    Poor,
    Fair,
    Good,
    Excellent,

    ABR_HowMany
  } abilityRating;

  if(ability > 204)
    abilityRating = Bad;
  else if(ability > 153)
    abilityRating = Poor;
  else if(ability > 102)
    abilityRating = Fair;
  else if(ability > 51)
    abilityRating = Good;
  else
    abilityRating = Excellent;

  const Table2D<int>& table = scenario->getBasicSpeedTable();

  int dieRoll = CRandom::get(table.getWidth());

  return table.getValue(abilityRating, dieRoll);
}


int getQualityMultiplier(ConnectQuality q)
{
  ASSERT(q < CQ_Max);

  const Table1D<int>& table = scenario->getConnectQualityTable();

  return table.getValue(q);
}

int getConditionToSpeedMultiplier(ConnectQuality q, CampaignWeather::GroundConditions c)
{
  ASSERT(q < CQ_Max);

  const Table2D<int>& table = scenario->getConditionToSpeedTable();

  return table.getValue(q, c);

}

const Speed LeaderSpeed = MPH(8);
const Speed EasyMarchSpeed = YPH((YardsPerMile*10)/8);  // 10 miles per day
const int MaxCapacityModifier = 50;

Speed StrategicSpeed::getStrategicSpeed(ICommandPosition cpi, IConnection iCon, CampUnitMoveData* moveData)
{
  ASSERT(cpi != NoCommandPosition);
  ASSERT(iCon != NoConnection);
  ASSERT(moveData!= 0);

  CampaignData* campData = moveData->d_campData;

  CommandPosition* cp = campData->getCommand(cpi);
  OrderBase* order = cp->getCurrentOrder();

#ifdef DEBUG_SPEED
  speedLog.printf("\nGetting StrategicSpeed for %s", cp->getNameNotNull());
  speedLog.printf("---------------------------------------------------");
#endif

  /*
   *  Do special cases.
   *  Special cases are:
   *    1. Unit is an indepenent leader, return LeaderSpeed (8 mph)
   */

  if(!cp->isRealUnit())
  {
#ifdef DEBUG_SPEED
    Leader* leader = campData->getArmies().getUnitLeader(cpi);
    speedLog.printf("%s is an indepedent leader", leader->getNameNotNull());
#endif
    return LeaderSpeed;
  }

//  RandomNumber rand;

  int rawSpeed = EasyMarchSpeed;
  if(!order->shouldEasyMarch())
  {
    /*
     *  Get leaders ability.
     *  This is (initiative + staff)/2
     *  Then get a random number between 0 - 9
     */

    Leader* leader = campData->getArmies().getUnitLeader(cpi);

    Attribute ability = Attribute((leader->getInitiative()+leader->getStaff())/2);

    rawSpeed = getSpeed(ability);
  }
#ifdef DEBUG_SPEED
  else
    speedLog.printf("%s is Easy Marching", cp->getNameNotNull());
#endif

#ifdef DEBUG_SPEED
  speedLog.printf("Raw Speed for %s is %f", cp->getNameNotNull(), float(rawSpeed)/10);
#endif


  /*
   *  Apply modifer for road quality
   *
   *  Poor is rawSpeed * .5
   *  Average is rawSpeed * .75
   *  Good is no modification
   *
   *  Actual multiplier is tableValue/100
   */

  const Connection& con = campData->getConnection(iCon);

  int qualityMultiplier = getQualityMultiplier(con.quality);

  int adjustedSpeed = MulDiv(rawSpeed, qualityMultiplier, 100);

#ifdef DEBUG_SPEED
  speedLog.printf("Speed after connection quality(%d) modifier is %f", int(con.quality), float(adjustedSpeed)/10);
#endif

  /*
   * Apply modifier for connection capacity
   *
   * First, check if any other friendly units are at current location
   * If so, and they are not at a town, then they must also be added to capacity check
   *
   * Capacity is checked by comparing Unit SPCount with connection capacity
   * Speed is reduced .05% for each 1% over capacity up to a limit of 50%
   *
   * Find difference between SPCount and capacity
   * If difference is greater than 0 modify.
   *
   *
   */

  SPCount spCount = 0;

  if(!cp->atTown())
  {
    moveData->d_closeFriendlyUnits.rewind();

    while(++moveData->d_closeFriendlyUnits)
    {
      const CloseUnitItem& item = moveData->d_closeFriendlyUnits.current();
      if(item.distance <= CampaignConst::trafficJamDistance)
      {
        spCount += item.spCount;
#ifdef DEBUG_SPEED
        speedLog.printf("%s is in a traffic jam with %s", cp->getNameNotNull(), (const char*)campData->getUnitName(item.cpi).toStr());
#endif
      }
    }
  }

  spCount += campData->getArmies().getUnitSPCount(cpi, True);

  int difference = spCount - con.capacity;

  if(difference > 0)
  {
    int percentMore = 0;

    /*
     * First see if SPCount is 50% greater than capacity
     *
     */

    if( (difference >= (con.capacity/2)) )
      percentMore = MaxCapacityModifier;

    else
    {
      percentMore = MulDiv(difference, 100, con.capacity);
    }

    if(adjustedSpeed > 0)
    {
      adjustedSpeed -= MulDiv(percentMore/2, 100, adjustedSpeed);
    }

//  adjustedSpeed *= ( 1-((percentMore/2)/100) );

#ifdef DEBUG_SPEED
    speedLog.printf("SPCount is %d, Capacity is %d", (int)spCount, (int)con.capacity);
    speedLog.printf("SPCount is %d%% greater than road capacity", percentMore);
    speedLog.printf("Speed after capacity modifier is %f", float(adjustedSpeed)/10);
#endif

  }

  /*
   * Unit composed entirely of cavalry and horse artillery is speed X 1.4
   */

  Boolean allMounted = campData->getArmies().isForceMounted(cpi);
  if(allMounted)
  {
    adjustedSpeed *= 1.4;
#ifdef DEBUG_SPEED
    speedLog.printf("Force is completely mounted. Speed after adjustment is %f", float(adjustedSpeed)/10);
#endif
  }

  /*
   *  force with a siege-train or bridge-train is speed X .85
   */

  Boolean hasSiegeTrain = campData->getArmies().hasSiegeTrain(cpi);
  Boolean hasBridgeTrain = campData->getArmies().hasBridgeTrain(cpi);

  if(hasSiegeTrain || hasBridgeTrain)
  {
    adjustedSpeed *= .85;
#ifdef DEBUG_SPEED
    speedLog.printf("Force has siege-train or depot. Speed after adjustment is %f", float(adjustedSpeed)/10);
#endif
  }

  /*
   *  If unit has forceMarch orders and is not yet forcemarching
   *  see if it will implement its order.
   *
   *  Chance of implementing forcemarch is % leaders initiative of 255
   */

  if(!cp->doingForceMarch() && order->shouldForceMarch())
  {
    /*
     * Units with siege-trains or depots cannot force-march
     */

    if(hasSiegeTrain || hasBridgeTrain)
    {
      // if here, reset order to normal march
      order->setMoveHow(Orders::AdvancedOrders::Normal);
    }

    // do test
    else
    {
      Leader* leader = campData->getArmies().getUnitLeader(cpi);

      int chance = MulDiv((int)leader->getInitiative(), 100, Attribute_Range);
      int dieRoll = CRandom::get(100);

#ifdef DEBUG_SPEED
      speedLog.printf("%s has force marching orders. Chance of implementing is %d, die roll is %d", cp->getNameNotNull(), chance, dieRoll);
#endif

      if(dieRoll < chance)
      {
        cp->setForceMarch(True);
      }
    }
  }

  /*
   * Unit force marching is speed * 1.25 if mounted, 1.5 otherwise
   */

  if(cp->doingForceMarch())
  {
    adjustedSpeed *= (allMounted == False) ? 1.5 : 1.25;
#ifdef DEBUG_SPEED
    speedLog.printf("Force is forcemarching. Speed after adjustment is %f", float(adjustedSpeed)/10);
#endif
  }

  /*
   *  Check for quality modifier
   */

  UWORD multiplier = campData->getArmies().getForceSpeedModifer(cpi);
  adjustedSpeed = MulDiv(adjustedSpeed, multiplier, 100);

#ifdef DEBUG_SPEED
  speedLog.printf("Speed after quality modifier is %f", float(adjustedSpeed)/10);
#endif

  /*
   * Check for weather conditions
   *
   */

  adjustedSpeed = MulDiv(rawSpeed, getConditionToSpeedMultiplier(con.quality, campData->getWeather().getGroundConditions()), 100);

#ifdef DEBUG_SPEED
  speedLog.printf("Speed after Weather conditions modifier is %f", float(adjustedSpeed)/10);
#endif

  /*
   * Now apply accumilated modifier
   */

  return YPH((YardsPerMile*(adjustedSpeed/10))/8);
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
