/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Unit Move Data
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cu_data.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "armies.hpp"

CampUnitMoveData::CampUnitMoveData(CampaignLogicOwner* campGame) :
   d_campaignGame(campGame),
   d_campData(campGame->campaignData()),
   d_armies(&(d_campData->getArmies())),
   d_ticks(0),
   d_closeUnits(),
   d_closeFriendlyUnits(),
   d_nightTime(False)
{
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
