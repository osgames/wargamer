/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cu_rand.hpp"
#include "randevnt.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "wg_rand.hpp"
#include "scenario.hpp"
#include "campmsg.hpp"

#ifdef DEBUG
// extern LogFileFlush rLog;
#endif

/*
 * Random Events
 *
 * A test is made each day for each side
 */

//const int nTableValues = 20;
//const int nSides = 2;

RandomEvent::Events::Event getEvent(Side s)
{

  const Table2D<UBYTE>& table = scenario->getRandomEventTable();

  int dieRoll = CRandom::get(table.getWidth());
  ASSERT(dieRoll < table.getWidth());
  ASSERT(s < table.getHeight());

  return static_cast<RandomEvent::Events::Event>(table.getValue(s, dieRoll));
}

void CampaignRandomEventUtil::process(CampaignLogicOwner* campGame)
{
  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  rLog.printf("========= Processing Random Events for %s", campData->asciiTime());
#endif

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {

    /*
     * See if a random event occurs
     *
     * Chance of this happening is 10%
     *
     * TODO: get chance from scenario table
     */

    int chance = CRandom::get(10);
    if(CRandom::get(100) <= chance)
//  if(CRandom::get(10) == 1)
    {
#ifdef DEBUG
      rLog.printf("Random Event for %s has occurred", scenario->getSideName(s));
#endif

      /*
       * A random event has occurred
       *
       * Roll die again to get exactly what event
       */

      RandomEvent::Events::Event type = getEvent(s);

      RandomEvent& event = RandomEventAllocator::newEvent(type);

      event.runEvent(campGame, s);
    }
  }

#ifdef DEBUG
  rLog.printf("========= End of Process ====================\n");
#endif
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
