/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CC_UTIL_HPP
#define CC_UTIL_HPP

#ifndef __cplusplus
#error cc_util.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Combat Utility Functions
 *
 * These are functions that are used by several different components
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "c_const.hpp"
#include "camppos.hpp"

class CloseUnits;
class CampaignData;

struct CloseUnitData {
  CampaignData* d_campData;
  CampaignPosition* d_from;
  ICommandPosition d_cpi;
  CloseUnits* d_closeUnits;
  Side d_ourSide;
  Side d_closeUnitSide;
  Distance d_findDistance;
  UBYTE d_flags;
  enum Flags {
	 AllAdjacent      = 0x01, // add all units that are at adjacent location regardless of distance
    MovingIntoBattle = 0x02  // add only units moving into battle
  };

  CloseUnitData() :
	 d_campData(0),
	 d_from(0),
	 d_cpi(NoCommandPosition),
	 d_closeUnits(0),
	 d_closeUnitSide(SIDE_Neutral),
	 d_findDistance(Distance_MAX),
	 d_flags(0) {}
};

class CloseUnitUtil {
public:
  static void findCloseUnits(CloseUnitData& data);
  static Boolean findCloseEnemy(CampaignData* campData, ITown itown, Distance maxDistance);
};

class CampaignCombatUtility {
 public:
	static SPCount calcSPCount(CampaignData* campData, const ICommandPosition& cpi, Boolean actual = False);
#if 0

	static void findCloseUnits(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits, Distance findDistance = CampaignConst::enemyLoseDistance);
	static void findEnemyUnits(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits, Distance findDistance = CampaignConst::enemyLoseDistance);
	static void findCloseFriendlyUnits(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits, Distance findDistance = CampaignConst::enemyLoseDistance);
#endif
};

#endif /* CC_UTIL_HPP */

