/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Orders to towns Despatcher
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ds_town.hpp"



// Constructor

DS_TownOrder::DS_TownOrder(ITown town, BuildWhat what, const BuildItem& b) :
   d_town(town),
   d_what(what),
   d_buildItem(b)
{
}

// Virtual process function

void DS_TownOrder::process(CampaignData* campData)
{
   const UnitTypeItem& uit = campData->getUnitType(d_buildItem.getWhat());

   AttributePoints tRes = 0;
   AttributePoints tMan = 0;

   uit.getTotalResource(tMan, tRes);
   d_buildItem.setResources(tMan * d_buildItem.getQuantity(), tRes * d_buildItem.getQuantity());

   Province& province = campData->getProvince(campData->getTownProvince(d_town));
   province.getBuild(d_what) = d_buildItem;
    province.orderOnTheWay(False);
}





/*
Pack a multiplayer message into buffer
NOTE : buffer points to MP_MSG_CAMPAIGNORDER
*/
int DS_TownOrder::pack(void * buffer, void * gamedata) {

   MP_MSG_CAMPAIGNORDER * msg = (MP_MSG_CAMPAIGNORDER *) buffer;

   // order type
   msg->order_type = (unsigned int) CMP_MSG_TOWN;

   int * data_buffer = (int *) msg->data;

   *data_buffer = (int) d_town;
   data_buffer++;

   *data_buffer = (int) d_what;
   data_buffer++;

   int bytes = d_buildItem.pack(data_buffer);
   data_buffer += bytes/4;

   return 12 + bytes;
}

/*
Unpack a multiplayer message from buffer
NOTE : buffer points to MP_MSG_CAMPAIGNORDER
*/
void DS_TownOrder::unpack(void * buffer, void * gamedata) {

   MP_MSG_CAMPAIGNORDER * msg = (MP_MSG_CAMPAIGNORDER *) buffer;

   int * data_buffer = (int *) msg->data;

   d_town = (ITown) *data_buffer;
   data_buffer++;

   d_what = (BuildWhat) *data_buffer;
   data_buffer++;

   int bytes = d_buildItem.unpack(data_buffer);
   data_buffer += bytes/4;
}



#ifdef DEBUG

String DS_TownOrder::getName()
{
   return String("Town order");
}

#endif

void TownOrder::send(ITown town, BuildWhat what, const BuildItem& b)
{
   ASSERT(town != NoTown);
   ASSERT(&b != 0);
   ASSERT((what >= 0) && (what < BasicUnitType::HowMany));

   DS_TownOrder* msg = new DS_TownOrder(town, what, b);
   ASSERT(msg != 0);
   Despatcher::sendMessage(msg);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
