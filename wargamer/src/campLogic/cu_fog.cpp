/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cu_fog.hpp"
#include "myassert.hpp"
#include "campdint.hpp"
#include "cc_util.hpp"
#include "cc_close.hpp"
#include "cu_data.hpp"
#include "c_const.hpp"
#include "weather.hpp"
#include "wg_rand.hpp"
#include "armies.hpp"
#include "scenario.hpp"
#include "town.hpp"
#include "terrain.hpp"
#include "route.hpp"
#include "routelst.hpp"
#include "camppos.hpp"
#include "sync.hpp"

#ifdef DEBUG
#include "clog.hpp"
static LogFileFlush fLog("FogOfWar.log");
#endif

/*-------------------------------------------------------------------------
 * see if an enemy unit is within spotting range
 *
 */


static int getInfoDistanceValue(int miles)
{
  enum {
    Within8,
    Within24,
    Over24
  };

  if(miles < 9)
    return Within8;

  if(miles < 25)
    return Within24;

  return Over24;
}

const int nInformationTableValues = 20;
void processInfoQuality(CampaignData* campData, const ICommandPosition& cpi, CloseUnits& cu)
{
  // RWLock lock;
  // lock.startWrite();

  /*
   * if here then we have either been seen or are still seen
   *
   * Some useful values
   */

  const Table2D<UBYTE>& table = scenario->getInformationQualityTable();

  ICommandPosition enemyCPI = cu.getClosest();
  ASSERT(enemyCPI != NoCommandPosition);
  CommandPosition* enemyCP = campData->getCommand(enemyCPI);
  CommandPosition* cp = campData->getCommand(cpi);

  Distance closeDistance = cu.getClosestDist();

  /*
   * raw die roll
   */

  int dieRoll = CRandom::get(100);

#ifdef DEBUG
  fLog.printf("Initial Die Roll of InfoQuality is %d", dieRoll);
#endif

  /*
   * apply modifiers if within 24 miles
   */

  if(closeDistance <= CampaignConst::enemyNearDistance)
  {
    const Table1D<SWORD>& modifierTable = scenario->getInfoQualityModifierTable();

    /*
     * Get number of light cavalry for both sides
     */

    SPCount ourCavalry = campData->getArmies().getNumLightCavalry(cpi);
    SPCount theirCavalry = campData->getArmies().getNumLightCavalry(enemyCPI);

#ifdef DEBUG
    fLog.printf("Light Cavalry for %s is %d", (const char*)campData->getUnitName(cpi).toStr(), static_cast<int>(ourCavalry));
    fLog.printf("Light Cavalry for %s is %d", (const char*)campData->getUnitName(enemyCPI).toStr(), static_cast<int>(theirCavalry));
#endif

    /*
     * If enemy has a 3:1, 2:1, 3:2, 1:3, or 1:2 superiority/inferiority in light cavalry
     */

    FogOfWarUtil::InformationModifiers::Value ratioValue = FogOfWarUtil::InformationModifiers::HowMany;
    if(ourCavalry != 0 && theirCavalry != 0)
    {
      if(theirCavalry >= (ourCavalry*3))             // 3:1
        ratioValue = FogOfWarUtil::InformationModifiers::Cavalry3To1;
      else if(theirCavalry >= (ourCavalry*2))        // 2:1
        ratioValue = FogOfWarUtil::InformationModifiers::Cavalry2To1;
      else if(theirCavalry >= ((ourCavalry*3)/2) )   // 3:2
        ratioValue = FogOfWarUtil::InformationModifiers::Cavalry3To2;
      else if(ourCavalry >= (theirCavalry*3))        // 1:3
        ratioValue = FogOfWarUtil::InformationModifiers::Cavalry1To3;
      else if(ourCavalry >= (theirCavalry*2))        // 1:2
        ratioValue = FogOfWarUtil::InformationModifiers::Cavalry1To2;
    }
    else if(theirCavalry != 0)
    {
      if(theirCavalry >= 2)
        ratioValue = FogOfWarUtil::InformationModifiers::Cavalry3To1;
      else
        ratioValue = FogOfWarUtil::InformationModifiers::Cavalry2To1;
    }
    else if(ourCavalry != 0)
    {
      if(ourCavalry >= 2)
        ratioValue = FogOfWarUtil::InformationModifiers::Cavalry1To3;
      else
        ratioValue = FogOfWarUtil::InformationModifiers::Cavalry1To2;
    }

    if(ratioValue != FogOfWarUtil::InformationModifiers::HowMany)
    {
      dieRoll += modifierTable.getValue(ratioValue);
#ifdef DEBUG
      fLog.printf("Info Die Roll after cavalry modifier is %d", dieRoll);
#endif
    }

    /*
     * If there is a choke point between investigating unit(enemyCPI)
     * and our unit(cpi)
     */

    /*
     *  find a route to enemy
     */

    ITown startTown = cp->getLastTown();
    ITown enemyTown = enemyCP->getCloseTown();
    ITown chokePointTown = NoTown;
    ASSERT(startTown != NoTown);
    ASSERT(enemyTown != NoTown);
    const Town& st = campData->getTown(startTown);
    const Town& et = campData->getTown(enemyTown);

    /*
     * Special cases...
     * First, if towns are the same
     */

    // if towns are the same
    if(enemyTown == startTown)
    {
      if(enemyCP->getSide() != st.getSide() && campData->isChokePoint(startTown))
        chokePointTown = startTown;
    }

    // see if we're on the same connection
    else if(!cp->atTown() && !enemyCP->atTown() && cp->getConnection() == enemyCP->getConnection())
      ;// do nothing

    // or our town is a choke point
    else if(cp->atTown() && enemyCP->getSide() != st.getSide() && campData->isChokePoint(startTown))
      chokePointTown = startTown;

    // or enemy town is a choke point
    else if(enemyCP->atTown() && enemyCP->getSide() != et.getSide() && campData->isChokePoint(enemyTown))
      chokePointTown = enemyTown;

    // they're connected to us
    else if(cp->atTown() && !enemyCP->atTown() && enemyCP->isConnected(startTown))
      ; // do nothing

    // we're connected to them or at the end of the same connection
    else if( (!cp->atTown() && cp->isConnected(enemyTown)) ||
             (cp->atTown() && enemyCP->atTown() && CampaignRouteUtil::areTownsAdjacent(campData, startTown, enemyTown)) )
    {
      if(enemyCP->getSide() != et.getSide() && campData->isChokePoint(enemyTown))
         chokePointTown = enemyTown;
    }

    // otherwise plot route
    else
    {
      ASSERT(startTown != enemyTown);

      RouteList rl;
      IConnection iCon = (!cp->atTown()) ? cp->getConnection() : NoConnection;
      CampaignRouteUtil::plotRoute(campData, rl, startTown, enemyTown, iCon);

      /*
       * iter through route to see if we run across a chokepoint
       */

      RouteListIterR iter(&rl);

      ITown lastTown = startTown;
      while(++iter)
      {
        const Town& t = campData->getTown(iter.current()->d_town);

        Boolean shouldCheck = True;

        // if this is the last item then don't add if enemy is on final connection
        if(iter.current() == rl.getLast() && !enemyCP->atTown())
        {
          shouldCheck = (CampaignRouteUtil::commonConnection(campData, lastTown, iter.current()->d_town) != enemyCP->getConnection());
        }

        if(shouldCheck)
        {
          if(enemyCP->getSide() != t.getSide() && campData->isChokePoint(iter.current()->d_town))
          {
            chokePointTown = iter.current()->d_town;
            break;
          }
        }

        lastTown = iter.current()->d_town;
      }
    }

    if(chokePointTown != NoTown)
    {
      dieRoll += modifierTable.getValue(FogOfWarUtil::InformationModifiers::ChokePoint);
#ifdef DEBUG
      const Town& t = campData->getTown(chokePointTown);
      fLog.printf("%s is a choke point between %s and %s",
           t.getNameNotNull(), cp->getNameNotNull(), enemyCP->getNameNotNull());

        fLog.printf("Info Die Roll after chokePoint modifier is %d", dieRoll);
#endif
    }

    /*
     * If there is Heavy Rain and\or HeavyMud
     */

    CampaignWeather& weather = campData->getWeather();
    if(weather.getWeather() == CampaignWeather::HeavyRain)
    {
      dieRoll += modifierTable.getValue(FogOfWarUtil::InformationModifiers::HeavyRain);
#ifdef DEBUG
      fLog.printf("Info Die Roll after weather modifier is %d", dieRoll);
#endif
    }

    if(weather.getGroundConditions() == CampaignWeather::VeryMuddy)
    {
      dieRoll += modifierTable.getValue(FogOfWarUtil::InformationModifiers::HeavyMud);
#ifdef DEBUG
      fLog.printf("Info Die Roll after ground conditions modifier is %d", dieRoll);
#endif
    }

    /*
     * Enemy Commanders initiative is above 200, or below 100
     */

    Leader* enemyLeader = campData->getArmies().getUnitLeader(enemyCPI);

#ifdef DEBUG
    fLog.printf("Enemy Leaders initiative is %d", static_cast<int>(enemyLeader->getInitiative()));
#endif

    FogOfWarUtil::InformationModifiers::Value initValue = FogOfWarUtil::InformationModifiers::HowMany;

    if(enemyLeader->getInitiative() < 100)
      initValue = FogOfWarUtil::InformationModifiers::InitBelow100;
    else if(enemyLeader->getInitiative() < 130)
      initValue = FogOfWarUtil::InformationModifiers::InitBelow130;
    else if(enemyLeader->getInitiative() >= 200)
      initValue = FogOfWarUtil::InformationModifiers::InitAbove200;


    if(initValue != FogOfWarUtil::InformationModifiers::HowMany)
    {
      dieRoll += modifierTable.getValue(initValue);
#ifdef DEBUG
      fLog.printf("Info Die Roll after Enemy Leaders initiative modifier is %d", dieRoll);
#endif
    }

    /*
     * If enemies morale is less than 50
     */

#ifdef DEBUG
    fLog.printf("Enemy morale is %d", static_cast<int>(enemyCP->getMorale()));
#endif

    if(enemyCP->getMorale() < 50)
    {
      dieRoll += modifierTable.getValue(FogOfWarUtil::InformationModifiers::MoraleBelow50);
#ifdef DEBUG
      fLog.printf("Info Die Roll after Enemies Morale modifier is %d", dieRoll);
#endif
    }
  }

  /*
   *  If already seen, then test to see if we increment info level
   */

  if(cp->isSeen())
  {
    if(dieRoll > 50)
    {
      CommandPosition::InfoQuality level = cp->getInfoQuality();

      if(level < CommandPosition::FullInfo)
      {
        INCREMENT(level);
        cp->setInfoQuality(level);
#ifdef DEBUG
        fLog.printf("Info level for %s has been incremented to %d",
          (const char*)campData->getUnitName(cpi).toStr(),
          static_cast<int>(level));
#endif
      }
    }

  }

  /*
   * Otherwise, get it from the table
   */

  else
  {
    /*
     * Make sure die Roll is between 0 - 99, then divide by 5
     *
     * Get table value and assign to cp
     */

    dieRoll = maximum(0, minimum(dieRoll, 99));
    dieRoll /= 5;

    ASSERT(dieRoll >= 0);
    ASSERT(dieRoll < nInformationTableValues);

    CommandPosition::InfoQuality info = static_cast<CommandPosition::InfoQuality>(table.getValue(getInfoDistanceValue(DistanceToMile(closeDistance)), dieRoll));
    ASSERT(info < CommandPosition::InfoQuality_HowMany);

#ifdef DEBUG
    fLog.printf("Info quality is %d", static_cast<int>(info));
#endif
    cp->setInfoQuality(info);
  }

  SPCount c = CampaignCombatUtility::calcSPCount(campData, cpi);

#ifdef DEBUG
  SPCount rc = campData->getArmies().getUnitSPCount(cpi, True);
  fLog.printf("%s -- actual SP count = %d, seen by enemy = %d",
     cpi->getNameNotNull(), static_cast<int>(rc), static_cast<int>(c));
#endif
  cpi->setSeen(campData->getTick());

  // lock.endWrite();
}


const int nDistanceValues = 5;

enum {
  Within8,
  Within24,
  Within48,
  Within96,
  Over96
};

int getVisibilityDistanceValue(int miles)
{
  if(miles < 9)
    return Within8;
  if(miles < 25)
    return Within24;
  if(miles < 49)
    return Within48;
  if(miles < 97)
    return Within96;

  return Over96;
}

void FogOfWarUtil::processDailyUnits(CampUnitMoveData* moveData, ICommandPosition cpi)
{
  ASSERT(moveData != 0);
  ASSERT(cpi != NoCommandPosition);

  CampaignData* campData = moveData->d_campData;

  CommandPosition* cp = campData->getCommand(cpi);

#ifdef DEBUG
  fLog.printf("=======Processing Fog of War for %s", (const char*)campData->getUnitName(cpi).toStr());
#endif

  /*
   * Get closest enemy
   */

  ICommandPosition cpiEnemy = moveData->d_closeUnits.getClosest();
  ASSERT(cpiEnemy != NoCommandPosition);
  Distance closeDistance = moveData->d_closeUnits.getClosestDist();

  /*
   * If we are already seen, and enemy is within 24 miles
   * then we remain seen, process info quality to see if it increments
   */

  if(cp->isSeen() && closeDistance <= CampaignConst::enemyNearDistance)
  {
    processInfoQuality(campData, cpi, moveData->d_closeUnits);
    return;
  }

#ifdef DEBUG
  fLog.printf("Closest enemy(%s) is at a distance of %d", (const char*)campData->getUnitName(cpiEnemy).toStr(), DistanceToMile(closeDistance));
#endif


  /*
   * Get raw chance
   */

  int distanceValue = getVisibilityDistanceValue(DistanceToMile(closeDistance));

  ASSERT(distanceValue < nDistanceValues);
  ASSERT(cp->getSide() < scenario->getNumSides());

  const Table2D<UBYTE>& table = scenario->getSpottingEnemyTable();

  int chance = table.getValue(distanceValue, cp->getSide());

#ifdef DEBUG
  fLog.printf("Chance of being seen before modifiers is %d%%", chance);
#endif

  /*
   * Test for modifiers if within 48 miles
   */

  if(distanceValue <= Within48)
  {
    const Table1D<SWORD>& modifierTable = scenario->getSpottingEnemyModifierTable();

    /*
     * Apply modifier for unit being spotted in territory friendly to the enemy
     *
     * TODO: implement this.
     */


    /*
     * Apply modifier for unit size
     *
     * A unit of 1 - 6 SP  is a SmallUnit
     * A unit of 49 - 100 SP is a LargeUnit
     * A unit over 100 SP is a VeryLargeUnit
     */

    SPCount count = campData->getArmies().getUnitSPCount(cpi, True);

    FogOfWarUtil::SpottingEnemyModifiers::Value m = FogOfWarUtil::SpottingEnemyModifiers::HowMany;
    if(count < 7)
      m = FogOfWarUtil::SpottingEnemyModifiers::SmallUnit;
    else if(count >= 49 && count <= 100)
      m = FogOfWarUtil::SpottingEnemyModifiers::LargeUnit;
    else if(count > 100)
      m = FogOfWarUtil::SpottingEnemyModifiers::VeryLargeUnit;

    if(m != FogOfWarUtil::SpottingEnemyModifiers::HowMany)
    {

      chance += modifierTable.getValue(m);
#ifdef DEBUG
      fLog.printf("Chance after Unit Size Modifier is %d, SP Count is %d", chance, static_cast<int>(count));
#endif
    }

    /*
     * Apply modifier for Heavy Rain
     */

    if(campData->getWeather().getWeather() == CampaignWeather::HeavyRain)
    {
      chance += modifierTable.getValue(FogOfWarUtil::SpottingEnemyModifiers::HeavyRain);
#ifdef DEBUG
      fLog.printf("Heavy Rain. Chance after  Modifier is %d", chance);
#endif
    }

    /*
     * Apply modifier for French Units in France
     *
     * TODO: implement this
     */

    const Nationality french = 0;    // this needs to be a table or something

    if(cp->getNation() == french)
    {
      ITown iTown = cp->getCloseTown();
      ASSERT(iTown != NoTown);

      Town& town = campData->getTown(iTown);
      if(town.getProvince() != NoTown)
      {
        Province& prov = campData->getProvince(town.getProvince());

        if(prov.getNationality() == french)
        {

          chance += modifierTable.getValue(FogOfWarUtil::SpottingEnemyModifiers::FrenchInFrance);
#ifdef DEBUG
          fLog.printf("French Unit in France. Chance after  Modifier is %d", chance);
#endif

        }
      }
    }

    /*
     * Modifier for Unit already being spotted
     */

    if(cp->isSeen())
    {
      chance += modifierTable.getValue(FogOfWarUtil::SpottingEnemyModifiers::AlreadySeen);

#ifdef DEBUG
      fLog.printf("Unit already seen. Chance after modifier is %d", chance);
#endif

    }
  }

  /*
   * See if unit is to be spotted
   */

  int dieRoll = CRandom::get(100);
#ifdef DEBUG
  fLog.printf("Die roll is %d", dieRoll);
#endif

  if(dieRoll < chance)
  {
#ifdef DEBUG
    fLog.printf("%s has been spotted", (const char*)campData->getUnitName(cpi).toStr());
#endif

    /*
     * Get info quality and set unit as seen
     */

    processInfoQuality(campData, cpi, moveData->d_closeUnits);
  }
  else
    cp->setNotSeen();
}

void FogOfWarUtil::processHourlyUnits(CampUnitMoveData* moveData, ICommandPosition cpi)
{
  ASSERT(moveData != 0);
  ASSERT(cpi != NoCommandPosition);

  CampaignData* campData = moveData->d_campData;
  CommandPosition* cp = campData->getCommand(cpi);

  /*
   * If unit is already seen, update last seen position
   */

  if(cp->isSeen())
    cp->setLastSeenLocation();

  /*
   * If unit is within 8 miles it is automatically sighted
   */

  if(moveData->d_closeUnits.getClosestDist() <= CampaignConst::enemyBattleDistance)
  {

#ifdef DEBUG
    fLog.printf("\n========= Processing Hourly Fog of War for %s ==========", (const char*)moveData->d_campData->getUnitName(cpi).toStr());
    fLog.printf("%s", campData->asciiTime());
    fLog.printf("%s is with 8 miles", (const char*)campData->getUnitName(cpi).toStr());
#endif

    /*
     * Process info quality
     */

    processInfoQuality(campData, cpi, moveData->d_closeUnits);
  }
}

void FogOfWarUtil::processDailyTowns(CampaignData* campData)
{
  ASSERT(campData != 0);

#ifdef DEBUG
  fLog.printf("======= Daily Towns Fog of War ============");
#endif

  TownList& tl = campData->getTowns();

  for(ITown t = 0; t < tl.entries(); t++)
  {
    Town& town = campData->getTown(t);

#ifdef DEBUG
    fLog.printf("=== Processing %s", town.getNameNotNull());
#endif

    town.isSeen(CloseUnitUtil::findCloseEnemy(campData, t, CampaignConst::enemyTownInfoDistance));
  }

#ifdef DEBUG
  fLog.printf("======== End of Town Fog of War =============\n");
#endif
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
