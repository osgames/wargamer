/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "recover.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "cbattle.hpp"
#include "town.hpp"
#include "weather.hpp"
#include "cu_mode.hpp"
#include "c_const.hpp"
#include "cc_close.hpp"
#include "cc_util.hpp"
#include "cu_data.hpp"
#include "wg_rand.hpp"
#include "cu_order.hpp"
#include "scenario.hpp"
#include <range.hpp> // from system

#ifdef DEBUG_RECOVER
#include "clog.hpp"
static LogFileFlush rLog("recover.log");
#endif

//! @todo move this into a header file
typedef Range<Attribute, Attribute_Range> Morale;


/*
 *  Process Fatigue. Called every 1 hour.
 */

void FatigueProc::procHourlyFatigue(ICommandPosition cpi, CampaignData* campData)
{
#ifdef DEBUG_FATIGUE
   rLog.printf("================Processing Fatigue===================");
#endif

   ASSERT(cpi != NoCommandPosition);
   ASSERT(campData != 0);

   Armies& armies = campData->getArmies();

   CommandPosition* cp = campData->getCommand(cpi);

#ifdef DEBUG_FATIGUE
   rLog.printf("\nAssessing fatigue for %s of %s", cp->getNameNotNull(), scenario->getSideName(cp->getSide()));
#endif

   /*
    * If unit is at an offscreen location, return
    */

   Boolean offScreen = False;
   if(cp->atTown())
   {
     Town& t = campData->getTown(cp->getTown());

     offScreen = t.isOffScreen();
   }
   else
   {
     Connection& c = campData->getConnection(cp->getConnection());

     offScreen = c.offScreen;
   }

   if(offScreen)
     return;

   int recover = 0;

   /*
    * Find out what unit is up to and assess appropriate loss\recovery
    *
    * First, check if it is in battle
    */

   if(cp->isInBattle())
   {
     /*
      *  find which battle. If unit has deployed, loss is 8,
      *  if it is a brushaside battle loss is 4, if not deployed 2
      */

     CampaignBattleList& batList = campData->getBattleList();
     CampaignBattle* battle = batList.findBattle(cpi);
     ASSERT(battle != 0);
     if(battle != 0)
     {

       recover = (battle->brushAside()) ? -4 : (battle->hasBattleBegun()) ? -8 : -2;

#ifdef DEBUG_FATIGUE
       rLog.printf("%s is engaged in battle, loss is %d", cp->getNameNotNull(), -recover);
#endif
     }
   }

   /*
    * Check if it is moving
    */

   else if(cp->isMoving())
   {
     /*
      * If it is forcemarching loss is 4 otherwise 2
      *
      */

     recover = (cp->doingForceMarch()) ? -4 : (cp->getCurrentOrder()->shouldEasyMarch()) ? 0 : -2;

#ifdef DEBUG_FATIGUE
     rLog.printf("%s is moving, loss is %d", cp->getNameNotNull(), -recover);
#endif
   }

   /*
    *  Check if it is holding
    */

   else if(cp->isHolding())
   {

     recover = 2;

#ifdef DEBUG_FATIGUE
     rLog.printf("%s is holding,  recovers %d", cp->getNameNotNull(), recover);
#endif
   }

   else if(cp->isResting())
   {

     recover = 12;

#ifdef DEBUG_FATIGUE
     rLog.printf("%s is resting,  recovers %d", cp->getNameNotNull(), recover);
#endif
   }

   /*
    *  No loss or recovery if Sieging.
    *  Not sure about Garrison
    */


   /*
    *  Now apply loss\recover to unit subordinates
    */

   if(recover != 0)
   {
#ifdef DEBUG_FATIGUE
     rLog.printf("Average Fatigue for %s before is %d", cp->getNameNotNull(), (int)cp->getFatigue());
#endif

     /*
      *  if we have a fatigue loss and there is light mud, loss is increased by 10%
      *  if there is heavy mud, loss is increased by 20%
      */

     if(recover < 0)
     {
       if(campData->getWeather().getGroundConditions() == CampaignWeather::Muddy)
         recover *= 1.1;
       else if(campData->getWeather().getGroundConditions() == CampaignWeather::VeryMuddy)
         recover *= 1.2;
     }

     // if withdrawing halv fatigue recovery
     else if(cp->isWithdrawing())
     {
       recover *= .5;
     }

     cp->addToFatigueCount(recover);
//   armies.applyFatigue(cpi, recover);
#ifdef DEBUG_FATIGUE
     rLog.printf("Fatigue loss\recovery for %s is %d", cp->getNameNotNull(), recover);
#endif
   }
}

void FatigueProc::procDailyFatigue(CampaignData* campData, ICommandPosition cpi)
{
#ifdef DEBUG_RECOVER
  rLog.printf("============ Processing Daily Fatigue for %s on %s ==========",
        (const char*)campData->getUnitName(cpi).toStr(), campData->asciiTime());
#endif

  {
    CommandPosition* cp = campData->getCommand(cpi);

    int todaysFatigue = cp->getFatigueCount();
    int fatigueRecovered = 0;
#ifdef DEBUG_RECOVER
    rLog.printf("Todays Fatigue loss-recovery for %s is %d",
         (const char*)campData->getUnitName(cpi).toStr(), todaysFatigue);
#endif

    /*
     * If negative then we have a fatigue loss
     */

    if(todaysFatigue < 0)
    {
      /*
       * Force will recover (loss/2)*%supply
       * Half this value if morale is less than 65
       * If leaders charisma+staff is between 350-389, then above value*1.1
       * if over 390 then value*1.2
       * if less than 195 value-(value*.15)
       */

      fatigueRecovered = todaysFatigue / 2;

      int supplyPercent = MulDiv(cp->getSupply(), 100, Attribute_Range);

#ifdef DEBUG_RECOVER
      rLog.printf("Supply percent is %d", supplyPercent);
#endif

      fatigueRecovered = MulDiv(fatigueRecovered, supplyPercent, 100);

#ifdef DEBUG_RECOVER
      rLog.printf("Fatigue loss after supply modifier is %d", todaysFatigue-fatigueRecovered);
#endif

      /*
       * if current morale is less than 65, then multiply by 1.5
       */

      if(cp->getMorale() < 65)
      {
        fatigueRecovered *= 1.5;

#ifdef DEBUG_RECOVER
        rLog.printf("Fatigue loss after morale modifier is %d", todaysFatigue-fatigueRecovered);
#endif
      }

      /*
       * Check for charisma+staff
       */

      ASSERT(cp->getLeader() != NoLeader);
      Leader* leader = campData->getLeader(cp->getLeader());

      int value = leader->getCharisma()+leader->getStaff();

      if(value >= 390)
        fatigueRecovered *= .8;
      else if(value >= 350)
        fatigueRecovered *= .9;
      else if(value <= 195)
        fatigueRecovered *= 1.15;

#ifdef DEBUG_RECOVER
      rLog.printf("Fatigue loss after charisma-staff modifier is %d", todaysFatigue-fatigueRecovered);
#endif

    }

    /*
     * Apply loss and reset fatigue-counter back to 0
     */

#ifdef DEBUG_RECOVER
    rLog.printf("Current fatigue before %d", static_cast<int>(cp->getFatigue()));
#endif

    campData->getArmies().applyFatigue(cpi, todaysFatigue-fatigueRecovered);

#ifdef DEBUG_RECOVER
    rLog.printf("Current fatigue after %d", static_cast<int>(cp->getFatigue()));
#endif

    cp->clearFatigueCount();
  }

}

/*
 * see if units should go into auto-rest mode
 */

void FatigueProc::procUnitFatigueLimit(CampaignLogicOwner* ci, ICommandPosition cpi)
{
  ASSERT(cpi != NoCommandPosition);

  CampaignData* campData = ci->campaignData();
  ASSERT(campData != 0);

#ifdef DEBUG_RECOVER
  rLog.printf("-------- Processing Fatigue Limits for %s", (const char*)campData->getUnitName(cpi).toStr());
#endif

  CommandPosition* cp = campData->getCommand(cpi);
  ASSERT(cp->shouldCheckForAutoRest());

  /*
   * If unit is at an offscreen location, return
   */

  Boolean offScreen = False;
  if(cp->atTown())
  {
     Town& t = campData->getTown(cp->getTown());

     offScreen = t.isOffScreen();
  }
  else
  {
     Connection& c = campData->getConnection(cp->getConnection());

     offScreen = c.offScreen;
  }

  if(offScreen)
     return;

  /*
   * Test to see if unit should go int auto-rest mode. Test is:
   *
   * Add leaders charisma and initiative. divide by 4. This is the leader value
   *
   * if fatigue loss(255-currentFatigue) is less than leader value, no check is made
   * if loss is up to 25% greater than leader value, there's a 10% chance(for auto-rest)
   * if loss is up to 50% greater, there's a 20% chance
   * if loss is up to 75% greater, there's a 40% chance
   * if loss is up to double, then  80% chance
   * over double is automatic
   *
   */

  ASSERT(cp->getLeader() != NoLeader);
  Leader* leader = campData->getLeader(cp->getLeader());
  int leaderValue = (leader->getInitiative()+leader->getCharisma())/4;
  int fatigueLoss = Attribute_Range-cp->getFatigue();
  ASSERT(fatigueLoss >= 0);

  int chance = 0;
  if(fatigueLoss >= leaderValue)
  {
    if(fatigueLoss <= leaderValue*1.25)
      chance = 10;
    else if(fatigueLoss <= leaderValue*1.5)
      chance = 20;
    else if(fatigueLoss <= leaderValue*1.75)
      chance = 40;
    else if(fatigueLoss <= leaderValue*2)
      chance = 80;
    else
      chance = 100;
  }

  if(CRandom::get(100) < chance)
  {
#ifdef DEBUG_RECOVER
    rLog.printf("Unit(%s) will rest", cp->getName());
#endif
//  cp->setMode(CampaignMovement::CPM_Resting);
    CP_ModeUtil::setMode(campData, cpi, CampaignMovement::CPM_Resting);
  }
  else
  {
    /*
     * if we're already in auto-rest mode, then set it back to move mode
     */

    if(cp->getMode() == CampaignMovement::CPM_Resting)
    {
      CampaignOrderUtil::orderUpdated(ci, cpi);

#if 0
      CampaignMovement::CP_Mode mode;

      if(cp->hasPursuitTarget())
        mode = CampaignMovement::CPM_Pursuing;

      else if(cp->getCurrentOrder()->isMoveOrder())
        mode = CampaignMovement::CPM_Moving;

      else
        mode = CampaignMovement::CPM_None;


      CP_ModeUtil::setMode(campData, cpi, mode);

      cp->clearEnemyRatio();  // recheck for contact
#endif
#ifdef DEBUG_RECOVER
      rLog.printf("Unit(%s) has resumed its march", cp->getName());
#endif
    }
  }
}

void FatigueProc::procDailyFatigueLimits(CampaignLogicOwner* ci, ICommandPosition cpi)
{
  ASSERT(ci);

  CampaignData* campData = ci->campaignData();
  ASSERT(campData != 0);

#ifdef DEBUG_RECOVER
  rLog.printf("============ Processing Daily Fatigue Limits for %s on %s======",
      (const char*)campData->getUnitName(cpi).toStr(), campData->asciiTime());
#endif

  /*
   * No limits if unit is currently sieging, in battle,
   * garrison, or routing(after a losing battle), etc.
   */

  if(cpi->shouldCheckForAutoRest())
  {
    procUnitFatigueLimit(ci, cpi);
  }
#ifdef DEBUG_RECOVER
  else
  {
    rLog.printf("%s is not in proper mode(%s) to set limit",
        (const char*)campData->getUnitName(cpi).toStr(), cpi->getModeDescription(cpi->getMode()));
  }

  rLog.printf("=========== End of Daily Fatigue Limit Proc ========");
#endif
}

//====================== RecoverMoraleUtil=======================

class RecoverMoraleUtil {
public:
  static void recoverUnitMorale(CampaignData* campData, ICommandPosition cpi);
  static void recoverMoraleFromResting(CampaignData* campData, ICommandPosition cpi);
  static void recoverMoraleRetreating(CampaignData* campData, ICommandPosition cpi, Distance);
  static int getOrgCharisma(CampaignData* campData, ICommandPosition cpi);
};

/*
 * Get Combined Leader Charisma of a Division, and all(if any) of its higher HQ,s
 *
 */

int RecoverMoraleUtil::getOrgCharisma(CampaignData* campData, ICommandPosition cpi)
{
  ASSERT(campData != 0);
  ASSERT(cpi != NoCommandPosition);

  CommandPosition* cp = campData->getCommand(cpi);
  ASSERT(cp->sameRank(Rank_Division));

  /*
   * Division leaders charisma is doubled
   */

  Leader* leader = campData->getLeader(cp->getLeader());
  int combinedCharisma = leader->getCharisma()*2;
  ICommandPosition cpiParent = cp->getParent();

  for(;;)
  {
    ASSERT(cpiParent != NoCommandPosition);
    CommandPosition* cpParent = campData->getCommand(cpiParent);

    /*
     *  If not President or God
     */

    if(cpParent->isLower(Rank_President))
    {
      Leader* leader = campData->getLeader(cpParent->getLeader());

      /*
       * If leader's subordinates subordination rating is greater than leaders staff
       * add leaders charisma/2, otherwise add charisma
       */

      if(campData->getArmies().getCommandSubordination(cpiParent) > leader->getStaff())
        combinedCharisma += leader->getCharisma()/2;
      else
        combinedCharisma += leader->getCharisma();
    }
    else
      break;

    cpiParent = cpParent->getParent();

  };

  /*
   * value is combinedCharisma/130 (rounded off)
   *
   * Value is the amount of morale this division will recover
   */

  const int divisor = 130;

  
#ifdef DEBUG_RECOVER
  rLog.printf("\nCharisma for %s = %d", (const char*)campData->getUnitName(cpi).toStr(),
	  (combinedCharisma+((divisor/2)-1))/divisor);
#endif
return (combinedCharisma+((divisor/2)-1))/divisor;

}

void RecoverMoraleUtil::recoverUnitMorale(CampaignData* campData, ICommandPosition cpi)
{
  ASSERT(campData != 0);
  ASSERT(cpi != NoCommandPosition);

  // minimum morale for unlocking minimum aggression values
  const Attribute minMorale = 41;

#ifdef DEBUG_RECOVER
  rLog.printf("\nProcessing Daily Morale for %s -----------------------", (const char*)campData->getUnitName(cpi).toStr());
#endif

  CommandPosition* cp = campData->getCommand(cpi);

#ifdef DEBUG__RECOVER
  rLog.printf("morale before recovery = %d", static_cast<int>(cp->getMorale()));
#endif

  /*
   * Apply the following only to divisional leaders
   *
   *
   *
   */

  UnitIter iter(&campData->getArmies(), cpi);

  while(iter.next())
  {
    CommandPosition* cp = iter.currentCommand();

    if(cp->sameRank(Rank_Division))
    {
#ifdef DEBUG_RECOVER
      rLog.printf("----- Apply morale recovery for %s", (const char*)campData->getUnitName(iter.current()).toStr());
#endif


      int recoverBy = getOrgCharisma(campData, iter.current());

#ifdef DEBUG_RECOVER
      rLog.printf("------------ recoverBy(unmodified) = %d", recoverBy);
#endif

      /*
       * There is a 30% chance that recoverBy will be multiplied by 1.5
       * and a 30% chance it will be multiplied by 0.75
       */

      const int percentChance = 30;

      int dieRoll = CRandom::get(100);
      if(dieRoll < percentChance)
      {
        recoverBy *= 1.5;
#ifdef DEBUG_RECOVER
        rLog.printf("------------------- recoverBy = %d, multiplied by 1.5", recoverBy);
#endif
      }

      else if(dieRoll < (percentChance * 2))
      {
        recoverBy *= .75;
#ifdef DEBUG_RECOVER
        rLog.printf("-------------------- recoverBy = %d, multiplied by .75", recoverBy);
#endif
      }

#ifdef DEBUG_RECOVER
        rLog.printf("-------------------- oldMorale = %d", (cp->getMorale()));
#endif

  // Alternate way using Range<> class
  //! @todo Replace all morale manipulations with Range<>
  cp->setMorale(Morale(cp->getMorale()) += recoverBy);

#ifdef DEBUG_RECOVER
        rLog.printf("-------------------- newMorale = %d", (cp->getMorale()));
#endif
      /*
       * Unlock aggression values if morale is over 41
       */

      if(cp->aggressionLocked() && cp->getMorale() >= minMorale)
      {
        cp->aggressionLocked(False);
#ifdef DEBUG_RECOVER
        rLog.printf("--------------------- Aggression unlocked");
#endif
      }
    }
  }

  campData->getArmies().averageCommandMorale(cpi);

  /*
   * Unlock aggression values if morale is over 41
   */

  if(cpi->aggressionLocked() && cpi->getMorale() >= minMorale)
  {
    cpi->aggressionLocked(False);
  }

#ifdef DEBUG_RECOVER
  rLog.printf("morale after recovery = %d\n", static_cast<int>(cp->getMorale()));
#endif

}

void RecoverMoraleUtil::recoverMoraleFromResting(CampaignData* campData, ICommandPosition cpi)
{
  ASSERT(campData != 0);
  ASSERT(cpi != NoCommandPosition);

#ifdef DEBUG_RECOVER
  rLog.printf("Recovering morale from resting for %s -----------------------", (const char*)campData->getUnitName(cpi).toStr());
#endif

  CommandPosition* cp = campData->getCommand(cpi);

#ifdef DEBUG_RECOVER
  rLog.printf("hours spent resting = %d", static_cast<int>(cp->hoursResting()));
#endif

  /*
   * If hours spent resting are less than 2 then return
   */

  const int minHoursResting = 2;

  if(cp->hoursResting() < minHoursResting)
    return;

  Leader* leader = campData->getLeader(cp->getLeader());

#ifdef DEBUG_RECOVER
  rLog.printf("morale before recovery = %d", static_cast<int>(cp->getMorale()));
#endif

  /*
   * Get leader ability level. this is leader's (initiative+staff+charisma*2)/4
   *
   */

  enum {
    UpTo51,
    UpTo102,
    UpTo153,
    UpTo204,
    UpTo255,

    AbilityLevel_HowMany
  } abilityLevel;


  int ability = (leader->getInitiative()+leader->getStaff()+(leader->getCharisma()*2))/4;

  if(ability < 52)
    abilityLevel = UpTo51;
  else if(ability < 103)
    abilityLevel = UpTo102;
  else if(ability < 154)
    abilityLevel = UpTo153;
  else if(ability < 205)
    abilityLevel = UpTo204;
  else
    abilityLevel = UpTo255;


  // values are amount of morale recovered for each 2 hours of resting & rallying

  const Table2D<UBYTE>& table = scenario->getRecoverMoraleFromRestingTable();
  ASSERT(AbilityLevel_HowMany == table.getHeight());

  const int maxDieRoll = table.getWidth();

  /*
   * Die Roll modifiers applied to CinC
   */

  int cincModifier = 0;
  // CinC's staff rating is less than subordinated subordination rating (-1)
  if(campData->getArmies().getCommandSubordination(cpi))
    cincModifier--;

  // In enemy territory (-1)
  const Province& prov = campData->getProvince(campData->getTownProvince(cp->getCloseTown()));
  if(prov.getSide() != cp->getSide())
    cincModifier--;

  // Heavy Rain and unit not at supply source (-1)
  if(campData->getWeather().getWeather() == CampaignWeather::HeavyRain)
  {
    if(cp->atTown())
    {
      const Town& t = campData->getTown(cp->getTown());

      if(!t.getIsDepot() && !t.getIsSupplySource())
        cincModifier--;
    }
    else
      cincModifier--;
  }

  // At a depot location (+1)
  if(cp->atTown())
  {
    const Town& t = campData->getTown(cp->getTown());

    if(t.getIsDepot())
      cincModifier++;
  }


  // No enemy force within 24 miles (+1)
  // If enemyRation = FR_None then no enemy is near
  if(cp->enemyRatio() > FR_None)
    cincModifier++;


  /*
   * Get supply multiplier. Multiplier is applied to morale recovered
   * Multiplier is -- for each 2% below 100% supply morale recovered is
   * reduced by 1%
   */

  int supplyPercent = MulDiv(cp->getSupply(), 100, Attribute_Range);
  supplyPercent += (100-supplyPercent)/2;

  /*
   * Recovery is to applied to each unit in the command individually
   */

  UnitIter iter(&campData->getArmies(), cpi);
  while(iter.next())
  {
    CommandPosition* cpSub = iter.currentCommand();
    Leader* leader = campData->getLeader(cpSub->getLeader());

#ifdef DEBUG_RECOVER
    rLog.printf("processing %s", cpSub->getName());
#endif

    int dieRoll = (CRandom::get(maxDieRoll)) + cincModifier;

    /*
     * Die Roll modifiers that apply to subordinate commander
     */

    // if a Division
    if(cpSub->sameRank(Rank_Division))
    {
      // base morale(theoretical maximum) is greater than 170 (+2)
      if(campData->getArmies().getBaseMorale(iter.current()) > 170)
        dieRoll += 2;

      // leader has charisma over 170 (+1)
      if(leader->getCharisma() > 170)
        dieRoll++;
    }

    // if a Corps
    if(cpSub->sameRank(Rank_Corps))
    {
      // if leader is not CinC, and leader has charisma over 170 (+1)
      if(iter.current() != cpi && leader->getCharisma() > 170)
        dieRoll++;
    }

    /*
     * modifiers for Nation in Nation
     *
     */

    const Table2D<int>& nTable = scenario->getRecoverMoraleRestingNationTable();
    dieRoll += nTable.getValue(cpSub->getNation(), prov.getNationality());

    dieRoll = clipValue(dieRoll, 0, maxDieRoll-1);

    ASSERT(abilityLevel < AbilityLevel_HowMany);
    ASSERT(dieRoll < maxDieRoll);

#ifdef DEBUG_RECOVER
    rLog.printf("Ability level = %d, final dieRoll = %d",
      static_cast<int>(abilityLevel),
      dieRoll);
#endif

    /*
     * Morale recovers by table value * every 2 hours resting
     */


    int recoverBy = (table.getValue(abilityLevel, dieRoll) * (cp->hoursResting()/2));

    /*
     * Apply supply multiplier
     */

    recoverBy = MulDiv(recoverBy, supplyPercent, 100);

#ifdef DEBUG_RECOVER
    rLog.printf("recoverBy = %d", recoverBy);
#endif

    cpSub->setMorale(Morale(cpSub->getMorale()) += recoverBy);

  }

  campData->getArmies().averageCommandMorale(cpi);

#ifdef DEBUG_RECOVER
  rLog.printf("morale after recovery = %d\n", static_cast<int>(cp->getMorale()));
#endif

  cp->resetHoursResting();
}

void RecoverMoraleProc::recoverDailyMorale(CampUnitMoveData* moveData, ICommandPosition cpi)
{
  ASSERT(moveData != 0);

  CampaignData* campData = moveData->d_campData;

#ifdef DEBUG_RECOVER
  rLog.printf("Recovering Daily Morale for %s on %s",
      (const char*)campData->getUnitName(cpi).toStr(), campData->asciiTime());
#endif

  /*
   * If unit isn't in battle & isn't routing it recovers some morale
   */

  if(cpi->getMorale() > 0)   // if morale = 0 its routing
  {
    if(!cpi->isInBattle())
    {
      RecoverMoraleUtil::recoverUnitMorale(campData, cpi);
    }

    RecoverMoraleUtil::recoverMoraleFromResting(campData, cpi);
  }
  // if retreating
  else
  {
    Distance closeEnemyDistance = moveData->d_closeUnits.getClosestDist();
    RecoverMoraleUtil::recoverMoraleRetreating(campData, cpi, closeEnemyDistance);
  }

#ifdef DEBUG_RECOVER
  rLog.printf("============ End Recovering Daily Morale ===========\n");
#endif
}

void RecoverMoraleProc::recoverHourlyMorale(CampaignData* campData, ICommandPosition cpi)
{
  ASSERT(campData != 0);
  ASSERT(cpi != NoCommandPosition);

  CommandPosition* cp = campData->getCommand(cpi);

  if(cp->isResting())
  {
    cp->incHoursResting();
  }
}

void RecoverMoraleUtil::recoverMoraleRetreating(CampaignData* campData, ICommandPosition cpi, Distance enemyDistance)
{
  ASSERT(cpi != NoCommandPosition);
  CommandPosition* cp = campData->getCommand(cpi);
  ASSERT(cp->getMorale() == 0);
  Leader* cinc = campData->getLeader(cp->getLeader());
  const Province& prov = campData->getProvince(campData->getTownProvince(cp->getCloseTown()));

#ifdef DEBUG_RECOVER
  rLog.printf("\nTesting for morale recovery for retreating unit(%s)", (const char*)campData->getUnitName(cpi).toStr());
#endif

  /*
   * Chance of morale recovery is %charisma X %aggression
   */

  const int percentCharisma = MulDiv(cinc->getCharisma(), 100, Attribute_Range);
  const int percentAggression = MulDiv(cinc->getAggression(), 100, Attribute_Range);
  int chance = MulDiv(percentCharisma, percentAggression, 100);

#ifdef DEBUG_RECOVER
  rLog.printf("Chance of recovering morale before modifiers = %d", chance);
#endif

  /*
   * Modifiers to %chance
   */

  // subordination rating of units under leaders command exceed staff (X0.9)
  UWORD combinedSubordination = campData->getArmies().getCommandSubordination(cpi);
  if(cinc->getStaff() < combinedSubordination)
    chance *= .9;

  // current weather is heavey rain (reduce by X0.8)
  if(campData->getWeather().getWeather() == CampaignWeather::HeavyRain)
    chance -= chance*.8;

  // for each 20% below 100% supply (reduce by X0.1)
  int supplyPercent = MulDiv(cp->getSupply(), 100, Attribute_Range);
  int pointsBelow = (100-supplyPercent)/20;
  if(pointsBelow > 0)
  {
    chance -= chance*(.1*pointsBelow);
  }

  // No enemy within 48 miles  (X1.2)
  if(enemyDistance > CampaignConst::enemySightingDistanceArmy)
    chance *= 1.2;

  // if we're at a friendly supply source (X1.2)
  if(cp->atTown())
  {
    Town& t = campData->getTown(cp->getTown());

    if(t.getIsSupplySource())
      chance *= 1.2;
  }

  // if enemy within 4 miles
  if(enemyDistance < MilesToDistance(4))
  {
    // if inconsequential (X0.8)
    if(cp->enemyRatio() == FR_Inconsequential)
      chance *= .8;

    // else (X0.5)
    else
      chance *= .5;
  }

#ifdef DEBUG_RECOVER
  rLog.printf("Chance of recovering morale after modifiers = %d", chance);
#endif

  if(CRandom::get(100) < chance)
  {
#ifdef DEBUG_RECOVER
    rLog.printf("Passed test. Will recover morale");
#endif

    Boolean isHeavyRain = (campData->getWeather().getWeather() == CampaignWeather::HeavyRain);

    const Table1D<UBYTE>& table = scenario->getRecoverMoraleRetreatingTable();

    /*
     * Recovery is to applied to each unit in the command individually
     */

    UnitIter iter(&campData->getArmies(), cpi);
    while(iter.next())
    {
      CommandPosition* cpSub = iter.currentCommand();
      Leader* leader = campData->getLeader(cpSub->getLeader());

#ifdef DEBUG_RECOVER
      rLog.printf("processing %s", cpSub->getName());
#endif

      Attribute maxMorale = campData->getArmies().getBaseMorale(iter.current());

      const int nTableValues = table.getWidth();
      int dieRoll = CRandom::get(nTableValues);

      /*
       * Die roll modifiers that apply to cinc
       */

      // if cinc has charisma greater than 200 (+2)
      if(cinc->getCharisma() > 200)
        dieRoll += 2;
      // or if charisma greater 179  (+1)
      else if(cinc->getCharisma() > 179)
        dieRoll += 1;

      /*
       * Die Roll modifiers that apply to subordinate commander
       */

      // if a Division
      if(cpSub->sameRank(Rank_Division))
      {
        // base morale(theoretical maximum) is greater than 170 (+1)
        if(maxMorale > 170)
          dieRoll += 1;
         // base morale is less than 90 (-1)
        else if(maxMorale < 90)
          dieRoll -= 1;

        // leader has charisma over 179 (+1)
        if(leader->getCharisma() > 179)
          dieRoll += 1;
      }

      // if a Corps
      if(cpSub->sameRank(Rank_Corps))
      {
        // if leader is not CinC, and leader has charisma over 170 (+1)
        if(iter.current() != cpi && leader->getCharisma() > 170)
          dieRoll++;
      }

      // if supply is below 50% (-1)
      if(supplyPercent < 50)
        dieRoll -= 1;

      // current weather is heavy rain (-1)
      if(isHeavyRain)
        dieRoll -= 1;

      /*
       * modifiers for Nation in Nation
       */

      const Table2D<int>& nTable = scenario->getRecoverMoraleRetreatingNationTable();
      dieRoll += nTable.getValue(cpSub->getNation(), prov.getNationality());

      dieRoll = clipValue(dieRoll, 0, nTableValues-1);

#ifdef DEBUG_RECOVER
      rLog.printf("final dieRoll = %d", dieRoll);
#endif

      /*
       * Morale recovers by table value * theoretical maximum morale
       */


      int tableValue = table.getValue(dieRoll);
      Attribute newMorale = clipValue<int>(MulDiv(tableValue, maxMorale, 100), 0, Attribute_Range);

#ifdef DEBUG_RECOVER
      rLog.printf("new morale = %d", static_cast<int>(newMorale));
#endif

      cpSub->setMorale(newMorale);
    }

    campData->getArmies().averageCommandMorale(cpi);

#ifdef DEBUG_RECOVER
    rLog.printf("morale after recovery = %d\n", static_cast<int>(cp->getMorale()));
#endif

  }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
