/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ds_repoSP_HPP
#define ds_repoSP_HPP
/*
 * Despatch Order to RepoSP
 */

#include "clogic_dll.h"
#include "cpidef.hpp"

#include "despatch.hpp"
#include "repopool.hpp"
#include "campdint.hpp"
#include "route.hpp"
#include "unittype.hpp"
#include "logwin.hpp"
#include "c_obutil.hpp"

class RepoSP;

class RepoDespatch : public DespatchMessage
{
   public:
      CLOGIC_DLL RepoDespatch(ConstParamCP cp, const RepoSP* rsp) : d_cp(cp), d_rsp(rsp) { }
	  CLOGIC_DLL RepoDespatch(void) { }

   	CLOGIC_DLL void process(CampaignData* campData);

	CLOGIC_DLL virtual int pack(void * buffer, void * gamedata);
	CLOGIC_DLL virtual void unpack(void * buffer, void * gamedata);
	
#ifdef DEBUG
	   CLOGIC_DLL String getName() { return "RepoDespatch"; }
#endif
   private:
      ConstICommandPosition d_cp;
      CRefPtr<RepoSP> d_rsp;     // reference pointer to avoid it being deleted in cu_repo
};



namespace RepoSPOrder
{
   CLOGIC_DLL void send(ConstParamCP cp, const RepoSP* rsp);
};



#endif   // ds_repoSP_HPP
