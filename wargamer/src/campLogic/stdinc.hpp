/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#if !defined(stdinc_H)
#define stdinc_H

/*
 *------------------------------------------------------------------------------
 * $Header$
 *------------------------------------------------------------------------------
 * $Author$
 *------------------------------------------------------------------------------
 * Copyright (C) 2001, Steven Green, all rights reserved
 *------------------------------------------------------------------------------
 * Precompiled Header include for Microsoft Visual C++
 *------------------------------------------------------------------------------
 */

#include <globals.hpp>

/*
 *------------------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *------------------------------------------------------------------------------
 */

#endif   // defined(stdinc_H)