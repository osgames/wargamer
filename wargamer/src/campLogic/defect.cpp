/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "defect.hpp"
#include "campdint.hpp"
#include "campctrl.hpp"
#include "armies.hpp"
#include "scenario.hpp"
#include "town.hpp"
#include "wg_rand.hpp"
#include "armyutil.hpp"
#include "cc_util.hpp"
#include "sync.hpp"

#ifdef DEBUG
#include "clog.hpp"
static LogFileFlush dLog("Defection.log");
#endif

class DefectUtil {
  static bool removeUnits(CampaignData* campData, const ICommandPosition& cpi,
     Side fromSide, Side startSide, Nationality n, const ICommandPosition& parentCPI);
  static Side getOtherSide(Side s) { return (s == 0) ? 1 : 0; }
public:
  static void process(CampaignLogicOwner* campGame, Side s);
  static void processDefectingNations(CampaignLogicOwner* campGame);
  static void nationDefects(CampaignLogicOwner* campGame, Nationality n);
  static void removeSPs(CampaignData* campData, Side fromSide, Nationality n);
  static void removeUnits(CampaignData* campData, Side side, Nationality n);
  static void changeTownSides(CampaignData* campData, Side fromSide, Nationality n);
  static void nationMorale(CampaignLogicOwner* campGame, Side newSide);
};

void DefectUtil::process(CampaignLogicOwner* campGame, Side s)
{
  static UWORD s_weeks = 0;
  CampaignData* campData = campGame->campaignData();

  /*
   * Go through each minor nation that belongs to this side
   * and test to see if it may defect
   */

  for(Nationality n = 0; n < scenario->getNumNations(); n++)
  {
    const Table1D<UBYTE>& nTable = scenario->getNationDefectsTable();

    if( (nTable.getValue(n)) &&
        (campData->getArmies().getNationAllegiance(n) == s) )
    {
#ifdef DEBUG
      dLog.printf("-------- testing %s", scenario->getNationName(n));
#endif

      /*
       * Get chance that this nation may defect
       */

      int chance = 0;

      /*
       * Modifiers for nation morale
       */

      // use sides default nation (i.e. French)
      Attribute nMorale = campData->getArmies().getNationMorale(scenario->getDefaultNation(s));
      enum {
        MoraleOver200,
        MoraleOver180,
        MoraleBelow180,
        MoraleBelow100,
        MoraleBelow65,
        MoraleBelow40,
        MoraleBelow20,

        EnemyControlsProv,
        EnemyControlsCapital,
        EnemyControlsCity,

        Per4WeeksOfWar,
        MajorNationEntersAgainst

      } nmLevel;


      if(nMorale < 20)
        nmLevel = MoraleBelow20;
      else if(nMorale < 40)
        nmLevel = MoraleBelow40;
      else if(nMorale < 65)
        nmLevel = MoraleBelow65;
      else if(nMorale < 100)
        nmLevel = MoraleBelow100;
      else if(nMorale < 180)
        nmLevel = MoraleBelow180;
      else if(nMorale < 200)
        nmLevel = MoraleOver180;
      else
        nmLevel = MoraleOver200;

      // add table value to chance;
      const Table1D<SWORD>& nmTable = scenario->getNationDefectsModifierTable();
      chance += nmTable.getValue(nmLevel);

#ifdef DEBUG
      dLog.printf("chance after %s morale(%d) = %d",
        scenario->getNationName(scenario->getDefaultNation(s)),
        static_cast<int>(nMorale),
        chance);
#endif

      /*
       * Province related stuff
       */

      for(IProvince ip = 0; ip < campData->getProvinces().entries(); ip++)
      {
        const Province& p = campData->getProvince(ip);
        if(p.getNationality() == n)
        {
          // if other side controls provice
          if(p.getSide() != s)
            chance += nmTable.getValue(EnemyControlsProv);

#ifdef DEBUG
          dLog.printf("chance after province side = %d", chance);
#endif

          // if other side controls capital
          const Town& cap = campData->getTown(p.getCapital());
          if(cap.getSide() != s)
            chance += nmTable.getValue(EnemyControlsCapital);

#ifdef DEBUG
          dLog.printf("chance after capital side = %d", chance);
#endif

          // for each city that enemy controls
          ITown stop = static_cast<ITown>(p.getTown() + p.getNTowns());
          for(ITown tIndex = p.getTown(); tIndex < stop; tIndex++)
          {
            const Town& t = campData->getTown(tIndex);
            if(t.getSize() == TOWN_City &&
               t.getSide() != s)
            {
              chance += nmTable.getValue(EnemyControlsCity);
            }
          }

#ifdef DEBUG
          dLog.printf("chance after cities side = %d", chance);
#endif
        }
      }

      /*
       * For weeks that the campaign has been lasting
       */

      int nLoop = s_weeks / 4;
      while(nLoop--)
      {
        chance += nmTable.getValue(Per4WeeksOfWar);
      }

#ifdef DEBUG
      dLog.printf("chance after per 4 weeks of war = %d", chance);
#endif

      /*
       * If an enemy nation is in the war
       */

      const Table1D<UBYTE>& enTable = scenario->getNationDefectsEnemyNationsTable();
      for(int i = 0; i < enTable.getWidth(); i++)
      {
        Nationality en = static_cast<Nationality>(enTable.getValue(i));
        if(campData->getArmies().isNationActive(en))
          chance += nmTable.getValue(MajorNationEntersAgainst);
      }

#ifdef DEBUG
      dLog.printf("chance after enemy nations(final chance) = %d", chance);
#endif

      int value = clipValue(CRandom::get(100) - chance, 0, 100);

      // see if we defect
      int roll = CRandom::get(100);
      if(roll >= value &&
         roll < value + chance)
      {
        nationDefects(campGame, n);
      }
    }
  }

  s_weeks++;
}

void DefectUtil::nationDefects(CampaignLogicOwner* campGame, Nationality n)
{
  ASSERT(campGame);
  ASSERT(n < scenario->getNumNations());

  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  dLog.printf("%s is Defecting!", scenario->getNationName(n));
#endif

  /*
   * The following happens when a nation defects
   * 1. Nations allegiance is set to enemies side
   * 2. All nation's units are detached and are 'given' to the
   *    other side (to reappear in a week or so)
   * 3. All nation's SP's that are attached to other nation's units
   *    are for lost
   * 4. All other nation SP's attached to defecting nations units
   *    are lost
   * 5. All towns not occupied by former side, reverts to new side
   * 6. The major nations of former side suffer morale loss
   *
   */

  Side dSide = campData->getArmies().getNationAllegiance(n);
  Side aSide = getOtherSide(dSide);

  // reset allegiance
  campData->getArmies().setNationAllegiance(n, aSide);

  // remove strength points
  removeSPs(campData, dSide, n);

  // remove units
  removeUnits(campData, dSide, n);

  // change town sides
  changeTownSides(campData, dSide, n);

  nationMorale(campGame, aSide);

  // reset time to reactivate units
  // we'll say a random number of days from 7 - 14
  int days = CRandom::get(7, 15);
  campData->getArmies().ractivateNationIn(n, campData->getTick() + DaysToTicks(days));
//  campGame->moraleUpdated();
}

void DefectUtil::nationMorale(CampaignLogicOwner* campGame, Side newSide)
{
  CampaignData* campData = campGame->campaignData();

  /*
   * Defections effect national morale
   */

  // TODO: get table from scenario tables
  // value is % increase / decrease in national morale (using a base of 10)
  // i.e. % = value / base
  static int base = 10;
  const UBYTE value = 150;

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    Boolean victor = (s == newSide);

    // apply moraleChange / 2 to all allied nations
    for(Nationality an = 0; an < scenario->getNumNations(); an++)
    {
      if( (campData->getArmies().getNationAllegiance(an) == s) &&
          (scenario->getNationType(an) == MajorNation) )
      {
        Attribute nMorale = campData->getNationMorale(an);

        // adjust attribute
        Attribute moraleChange = MulDiv(nMorale, value, base * 100);// / 2;

        nMorale = (victor) ? minimum<int>(Attribute_Range, nMorale + moraleChange) :
               maximum(0, nMorale - moraleChange);

        campData->setNationMorale(an, nMorale);
      }
    }
  }
  campGame->moraleUpdated();
}

// go through each unit and eliminate other Nation SP's attached
// to defecting nation units and vice versa
void DefectUtil::removeSPs(CampaignData* campData, Side fromSide, Nationality n)
{
  UnitIter iter(&campData->getArmies(), campData->getArmies().getPresident(fromSide));

  while(iter.next())
  {
    const ICommandPosition& cpi = iter.current();

    if(cpi->sameRank(Rank_President))
      continue;

    StrengthPointIter sIter(&campData->getArmies(), cpi);
    while(++sIter)
    {
      const ISP& sp = sIter.current();
      const UnitTypeItem& uti = campData->getUnitType(sp->getUnitType());

      Boolean shouldDetach = False;

      // if defecting nationality...
      if(cpi->getNation() == n)
      {
        shouldDetach = (!uti.isNationality(n));

#ifdef DEBUG
        if(shouldDetach)
        {
          dLog.printf("Detaching %s from %s unit - unit is lost",
            uti.getName(), scenario->getNationAdjective(n));
        }
#endif
      }
      else
      {
        shouldDetach = (uti.isNationality(n) && !uti.isNationality(cpi->getNation()));
#ifdef DEBUG
        if(shouldDetach)
        {
          dLog.printf("Detaching %s from %s unit - unit is lost",
            uti.getName(), scenario->getNationAdjective(cpi->getNation()));
        }
#endif
      }

      if(shouldDetach)
      {
        sIter.setSP(sp->getNext());

        campData->getArmies().detachStrengthPoint(cpi, sp);
        campData->getArmies().deleteStrengthPoint(sp);
      }
    }
  }
}

bool DefectUtil::removeUnits(CampaignData* campData, const ICommandPosition& cpi,
  Side fromSide, Side startSide, Nationality n, const ICommandPosition& parentCPI)
{

  ICommandPosition cpChild = cpi->getChild();

  Boolean shouldTransfer = (fromSide == startSide) ?
    (cpi->getNation() == n) : (cpi->getNation() != n);

  ICommandPosition attachTo = (fromSide == startSide) ?
          cpChild : parentCPI;

  Side toSide = fromSide;

  if(shouldTransfer)
  {
    toSide = getOtherSide(fromSide);
    if(fromSide == startSide)
    {
#ifdef DEBUG
      dLog.printf("%s is changing sides", cpi->getName());
#endif
      cpi->isActive(False);
      cpi->setSide(toSide);

      /*
       * Added SWG: 12Jul99:
       * Also change Leader's side:
       */

      ILeader leader = cpi->getLeader();
      if (leader != NoLeader)
         leader->setSide(toSide);
    }

    const ICommandPosition& toCPI = (parentCPI == cpi) ?
      campData->getArmies().getPresident(toSide) : parentCPI;

    CampaignArmy_Util::attachUnits(campData, cpi, toCPI);

    return True;

  }

  if(cpChild != NoCommandPosition)
  {
    bool unitsRemoved;
    do
    {
      unitsRemoved = False;
      UnitIter iter(&campData->getArmies(), cpChild);
      while(iter.sister())
      {
         const ICommandPosition& cpi = iter.current();

         // warning! recursive!!!
         if(removeUnits(campData, cpi, toSide, startSide, n, cpi))
         {
            unitsRemoved = True;
            break;
         }
      }
    } while(unitsRemoved);
  }

  return False;
}

void DefectUtil::changeTownSides(CampaignData* campData, Side fromSide, Nationality n)
{
  for(IProvince pi = 0; pi < campData->getProvinces().entries(); pi++)
  {
    const Province& p = campData->getProvince(pi);

    if(p.getNationality() == n)
    {
      ITown stop = static_cast<ITown>(p.getTown() + p.getNTowns());
      for(ITown tIndex = p.getTown(); tIndex < stop; tIndex++)
      {
        Town& t = campData->getTown(tIndex);
        Boolean atTown = False;

        // if any enemy are here, we cannot change sides
        UnitIter iter(&campData->getArmies(), campData->getArmies().getFirstUnit(fromSide));
        while(iter.sister())
        {
          if(iter.current()->atTown() &&
             iter.current()->getTown() == tIndex)
          {
            atTown = True;
            break;
          }
        }

        if(!atTown)
        {
          t.setSide(getOtherSide(fromSide));

#ifdef DEBUG
          dLog.printf("%s is changing sides to %s",
            t.getNameNotNull(), scenario->getSideName(getOtherSide(fromSide)));
#endif
        }
#ifdef DEBUG
        else
        {
          dLog.printf("%s can't change sides, enemy is at town", t.getNameNotNull());
        }
#endif
      }
    }
  }
}

void DefectUtil::removeUnits(CampaignData* campData, Side fromSide, Nationality n)
{
   // now go through units again detaching any defecting nation units
   // and detaching any non-defectors from defectors
   bool unitsRemoved;
   do
   {
      unitsRemoved = False;
      UnitIter iter(&campData->getArmies(), campData->getArmies().getFirstUnit(fromSide));
      while(iter.sister())
      {
         const ICommandPosition& cpi = iter.current();
#ifdef DEBUG
         dLog.printf("--- Checking %s for defecting units", cpi->getName());
#endif
         if(removeUnits(campData, cpi, fromSide, fromSide, n, cpi))
         {
            unitsRemoved = True;
            break;
         }
      }

   }  while(unitsRemoved);
}

void DefectUtil::processDefectingNations(CampaignLogicOwner* campGame)
{
  CampaignData* campData = campGame->campaignData();
  NationsList& nl = campData->getNations();

  for(Nationality n = 0; n < nl.entries(); n++)
  {
    /*
     * If nation is reactivating...
     */

    if(campData->getArmies().shouldReactivateNation(n, campData->getTick()))
    {
      // first we need to decide where to put it
      Side s = campData->getNationAllegiance(n);

      ITown bestTown = NoTown;

      // First choice is province capital
      // Second is largest available city
      for(IProvince ip = 0; ip < campData->getProvinces().entries(); ip++)
      {
        const Province& p = campData->getProvince(ip);

        if(p.getNationality() == n)
        {
          // for each city that enemy controls
          TownSize bestSize = TownSize_MAX;

          ITown stop = static_cast<ITown>(p.getTown() + p.getNTowns());
          for(ITown tIndex = p.getTown(); tIndex < stop; tIndex++)
          {
            const Town& t = campData->getTown(tIndex);

            // if enemy controls town or is close by
            // we cannot show up here
            if( (t.getSide() == s) &&
                (!CloseUnitUtil::findCloseEnemy(campData, tIndex, CampaignConst::enemyLoseBattleDistance)) )
            {
              if(tIndex == p.getCapital())
              {
                bestTown = p.getCapital();
                break;
              }

              else if(t.getSize() < bestSize)
              {
                bestTown = tIndex;
                bestSize = t.getSize();
              }
            }
          }
        }
      }

      // go through and set all units to active, placing them in dest city
      UnitIter iter(&campData->getArmies(),
          campData->getArmies().getFirstUnit(campData->getNationAllegiance(n)), True);

      while(iter.sister())
      {
        const ICommandPosition& cpi = iter.current();

        if(cpi->getNation() == n)
        {
          // if we have a town, set position to town
          if(bestTown != NoTown)
          {
            Side s = campData->getNationAllegiance(n);

            // set each unit in command
            UnitIter cIter(&campData->getArmies(), cpi);
            while(cIter.next())
            {
              iter.current()->setSide(s);
              iter.current()->isActive(True);
              iter.current()->getPosition().setPosition(bestTown);
              iter.current()->getPosition().clearLastConnection();
            }

            campGame->redrawMap();
          }

          // if we don't have a town, then unit is 'lost'
          else
            campData->getArmies().removeUnitAndChildren(cpi);
        }
      }
    }
  }
}

void NationDefectProc::process(CampaignLogicOwner* campGame)
{
  // RWLock lock;
  // lock.startWrite();

  // test only sides that have nations that may defect
  // i.e. in Nap1813 only the French have Alllies that may defect
  const Table1D<UBYTE>& table = scenario->getNationDefectsSideTable();
  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
#ifdef DEBUG
    dLog.printf("\nTesting %s for defecting allies", scenario->getSideName(s));
#endif

    if(table.getValue(s))
      DefectUtil::process(campGame, s);
  }

  DefectUtil::processDefectingNations(campGame);

  // lock.endWrite();
}

void NationDefectProc::nationDefects(CampaignLogicOwner* campGame, Nationality n)
{
   DefectUtil::nationDefects(campGame, n);
   campGame->campaignData()->getNations().wantsToDefect(n, False);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
