/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cu_ldr.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "campctrl.hpp"
#include "campmsg.hpp"
#include "wg_rand.hpp"
#include "scenario.hpp"
#include "armyutil.hpp"
#ifdef DEBUG
#include "clog.hpp"
static LogFileFlush lLog("LeaderProc.log");
#endif


/*
 * Local utility class
 */

class LeaderProcUtil {
public:
  static void checkSickLeader(CampaignLogicOwner* campGame, ILeader leader, ICommandPosition cpi);
  static void procLeaderOrders(CampaignLogicOwner* campGame);
  static void recoverSickLeaders(CampaignLogicOwner* campGame);
};

/*
 * Process independent leaders
 */

void LeaderProcUtil::checkSickLeader(CampaignLogicOwner* campGame, ILeader leader, ICommandPosition cpi)
{
  ASSERT(leader != NoLeader);
  ASSERT(leader->isSick() || leader->isSeriouslyWounded());

  /*
   * Chance of Sick Leader recovering is (health% of 255)
   */

  int chance = MulDiv(leader->getHealth(), 100, Attribute_Range);

  // chance is halved if he is sick and wounded
  if(leader->isSick() && leader->isSeriouslyWounded())
    chance /= 2;

  if(CRandom::get(100) < chance)
  {
    /*
     * Leader has recovered from his illness / wounds
     */

    leader->isSick(False);
    leader->isSeriouslyWounded(False);

    /*
     * Let the player know
     */

    CampaignMessageInfo msgInfo(leader->getSide(), CampaignMessages::LeaderRecovered);

    msgInfo.cp(cpi);
    msgInfo.leader(leader);
    campGame->sendPlayerMessage(msgInfo);

#ifdef DEBUG
    lLog.printf("%s %s has recovered from his illnes",
             scenario->getSideName(leader->getSide()), leader->getNameNotNull());
#endif
  }
}

void LeaderProcUtil::procLeaderOrders(CampaignLogicOwner* campGame)
{
  ASSERT(campGame);

  // RWLock lock;
  // lock.startWrite();

  CampaignData* campData = campGame->campaignData();
  Armies& armies = campData->getArmies();

  SListIter<IndependentLeader> iter(&armies.independentLeaders());
  while(++iter)
  {
    /*
     * Find attaching leaders and see if they have arrived
     */

    IndependentLeader* il = iter.current();
    if(il->attachTo() != NoCommandPosition &&
       il->travelTime() <= campData->getTick() &&
       !il->attachTo()->trapped())
    {
      /*
       * Leader has arrived. Now take over
       */

      // let player know
      CampaignMessageInfo msgInfo(il->attachTo()->getSide(), CampaignMessages::TakenCommand);
      msgInfo.cp(il->attachTo());
      msgInfo.cpTarget(il->attachTo());
      msgInfo.leader(il->leader());
      campGame->sendPlayerMessage(msgInfo);

      // transfer
      CampaignArmy_Util::transferLeader(campData, il->leader(), il->attachTo(), True);

      // remove from list
      iter.remove();
    }
  }

  // lock.endWrite();
}

void LeaderProcUtil::recoverSickLeaders(CampaignLogicOwner* campGame)
{
  ASSERT(campGame);

  CampaignData* campData = campGame->campaignData();
  Armies& armies = campData->getArmies();

  /*
   * Test independent leaders first
   */


  SListIter<IndependentLeader> iter(&armies.independentLeaders());
  while(++iter)
  {
    /*
     * Find attaching leaders and see if they have arrived
     */

    IndependentLeader* il = iter.current();

    if(il->leader()->isSick() ||
       il->leader()->isSeriouslyWounded())
    {
      ICommandPosition presCPI = campData->getArmies().getPresident(il->leader()->getSide());
      checkSickLeader(campGame, il->leader(), presCPI);
    }
  }

  /*
   * Test attached leaders
   */

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    UnitIter uiter(&campData->getArmies(), campData->getArmies().getPresident(s));

    while(uiter.next())
    {
      ICommandPosition cp = uiter.current();

      if(cp->isLower(Rank_President))       // skip past president
      {
        ASSERT(cp->getLeader() != NoLeader);

        ILeader leader = campData->getLeader(cp->getLeader());

        if(leader->isSick() ||
           leader->isSeriouslyWounded())
        {
          checkSickLeader(campGame, leader, cp);
        }
      }
    }
  }
}

/*--------------------------------------------------------
 * Access functions
 */

void LeaderProc::processHourly(CampaignLogicOwner* campGame)
{
  ASSERT(campGame);

  /*
   * Process independent leader orders
   */

  LeaderProcUtil::procLeaderOrders(campGame);
}

void LeaderProc::processDaily(CampaignLogicOwner* campGame)
{
  ASSERT(campGame);

  /*
   * Check to see if any sick leaders have recovered
   * Iterate through all leaders and find any who are sick or wounded
   */

  LeaderProcUtil::recoverSickLeaders(campGame);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
