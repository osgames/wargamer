/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CMSGUTIL_HPP
#define CMSGUTIL_HPP

#ifndef __cplusplus
#error cmsgutil.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Utility class to convert Campaign Message into human readable form
 *
 *----------------------------------------------------------------------
 */

#include "clogic_dll.h"

class SimpleString;
class CampaignMessageInfo;
class CampaignData;

class CampaignMessageFormat
{
	 public:
	 	CLOGIC_DLL static void messageToText(SimpleString& s, const CampaignMessageInfo& msg, const CampaignData* campData);
};


#endif /* CMSGUTIL_HPP */

