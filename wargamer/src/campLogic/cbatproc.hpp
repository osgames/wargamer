/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CBATPROC_HPP
#define CBATPROC_HPP

#ifndef __cplusplus
#error cbatproc.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Battle Processing
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"

class CampaignLogicOwner;
class CampaignBattleList;
class CampaignBattle;
struct CampUnitMoveData;
class Armies;
class CampaignData;

/*
 * This should probably be 2 seperate classes
 * procBattles() is an external function used by campaign Logic
 * the other functions are used by Campaign Combat.
 */


class CampaignBattleProc {
 public:
 	static void procBattles(CampaignLogicOwner* campGame, CampaignBattleList* battleList);
   static Boolean procBreakOutBattle(CampaignData* campData, CampaignBattle* battle, Side sideBreakingOut);
	static void addUnit(CampaignLogicOwner* campGame, CampaignBattle* battle, ICommandPosition cpi);
	static ICommandPosition getFriendlyUnit(CampUnitMoveData* combData, CampaignBattle* battle, Side side);
	static SPCount  getFriendlySPCount(CampUnitMoveData* combData, CampaignBattle* battle, Side side);

	static void prepareToWithdraw(Armies* armies, CampaignBattle* battle, ICommandPosition hunit);
	static void removeFromBattle(CampaignData* campData, ICommandPosition cpi);

   static void startBattles(CampaignLogicOwner* campGame);
   static bool canPlayTactical(CampaignData* campData, CampaignBattle* battle);
 private:
    static void startBattle(CampaignLogicOwner* campGame, CampaignBattle* battle);

};

#endif /* CBATPROC_HPP */

