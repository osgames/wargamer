/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Game Scheduler
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.1  1996/06/22 19:02:35  sgreen
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "sched.hpp"
#include "filebase.hpp"

GameSchedule::GameSchedule(TimeTick _interval)
{
   nextProcess = 0;
   interval = _interval;
}


Boolean GameSchedule::run(TimeTick now)
{
   Boolean processed = False;

   if(nextProcess == 0)
      nextProcess = now;

   while(now >= nextProcess)
   {
      process(interval);
      nextProcess += interval;
      processed = True;
   }

   return processed;
}

UWORD GameSchedule::s_fileVersion = 0x0000;

bool GameSchedule::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);
   f >> nextProcess;
   f >> interval;
   return f.isOK();
}

bool GameSchedule::writeData(FileWriter& f) const
{
   f << s_fileVersion;
   f << nextProcess;
   f << interval;
   return f.isOK();
}


GameScheduleList::~GameScheduleList()
{
   reset();    // Deletes all of list
}

void GameScheduleList::add(GameSchedule* proc)
{
   append(proc);
}

void GameScheduleList::run(TimeTick now)
{
   GameSchedule* sched = first();
   while(sched != 0)
   {
      sched->run(now);
      sched = next();
   }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
