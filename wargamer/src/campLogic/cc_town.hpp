/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CC_TOWN_HPP
#define CC_TOWN_HPP

#ifndef __cplusplus
#error cc_town.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Combat: Taking over towns
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"

class CampaignData;
class CampaignLogicOwner;
class UnitToTownRatio {
	public:
	enum Value {
		LessThan,
		LessThan3To2,
		LessThan2To1,
		LessThan3To1,
		LessThan4To1,
		Over4,

		HowMany
	};
#ifdef DEBUG
	static const char* getText(Value v);
#endif
};

class CampUnitTown {
 public:
	static void updateTownUnitMode(CampaignData* campData);
	static UnitToTownRatio::Value getUnitToTownRatio(CampaignData* campData, ITown iTown, Side side);
	static void enterTown(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit);
	static void takeTown(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit);
//	static void takeTown(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit);
	static void destroySupply(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit);
//	static void captureSupply(CampaignLogicOwner* campGame, ITown itown, ICommandPosition hunit);
};

#endif /* CC_TOWN_HPP */

