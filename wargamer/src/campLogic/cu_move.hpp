/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CU_MOVE_HPP
#define CU_MOVE_HPP

#ifndef __cplusplus
#error cu_move.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Unit Movement
 *
 * This was originally muddled up as a mixture of functions from
 * CommandPosition, CampaignMovement and RealCampaignData
 *
 *----------------------------------------------------------------------
 */


#include "clogic_dll.h"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "measure.hpp"		// For TimeTick

class CampaignLogicOwner;
struct CampUnitMoveData;
class CampaignData;

class CampUnitMovement {
	CampUnitMoveData* d_moveData;
   Boolean d_shouldRedrawMap;

	// Prevent Copy Constructor
	CampUnitMovement(const CampUnitMovement& cuMove);
	const CampUnitMovement& operator = (const CampUnitMovement& cuMove);

 public:
 	CLOGIC_DLL CampUnitMovement(CampaignLogicOwner* campGame);
		// Constructor
	CLOGIC_DLL ~CampUnitMovement();
		// Destructor

	CLOGIC_DLL void hourlyUnits(TimeTick _ticks);
		// Move All Units Each hour

	CLOGIC_DLL void dailyUnits();
		// Move Units each day

 private:
	// These should 7 be defined locally in cu_move

	void moveCampaignUnits();
	void processCampaignOrders(ICommandPosition hunit);
	void orderUpdated(ICommandPosition cpi);
	void finishOrder(ICommandPosition cpi);
	void moveCampaignUnit(ICommandPosition hunit);
	void doCampaignMoveUnit(ICommandPosition hunit);
	void setUnitOnRoute(ICommandPosition cpi, ITown iTown = NoTown);
	void moveOffConnection(ICommandPosition cpi);
	void doCampaignSiege(ICommandPosition hunit);
	void doCampaignGarrison(ICommandPosition hunit);
	// void doCampaignDestroyUnit(ICommandPosition hunit, Boolean surrendered);
//	void doCampaignInstallSupply(ICommandPosition hunit);
//	void doCampaignInstallFort(ICommandPosition hunit);
//	void doCampaignDigIn(ICommandPosition cpi);
   void doCampaignAbnormalActivity(ICommandPosition cpi);
	void holdUnit(ICommandPosition cpi);
	void adjustDistance(ICommandPosition cpi, Distance& distance);
	Boolean adjustDistance(ICommandPosition cp1, ICommandPosition cp2, Distance& distance, Distance distanceToTravel);
	void findCloseUnits(ICommandPosition cpi);
};






#endif /* CU_MOVE_HPP */

