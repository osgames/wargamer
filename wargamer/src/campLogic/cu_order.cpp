/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Unit Order Reception
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "cu_order.hpp"
#include "cc_close.hpp"
#include "cc_util.hpp"
#include "c_const.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "campord.hpp"
#include "realord.hpp"  // CampaignOrderUtil
#include "options.hpp"
#include "route.hpp"
#include "scenario.hpp"
#include "town.hpp"
#include "weather.hpp"
#include "cu_data.hpp"
#include "wg_rand.hpp"
//#include "cu_mode.hpp"
#ifdef DEBUG
#include "unitlog.hpp"
#endif

namespace CampaignOrderUtil
{


void CampaignOrderUtil::processCampaignOrders(CampUnitMoveData* moveData, ICommandPosition hunit)
{
   ASSERT(hunit != NoCommandPosition);
   ASSERT(moveData != 0);

   if(OrderListUtil::process(hunit, moveData))
     orderUpdated(moveData->d_campaignGame, hunit);
}

/*
 * Set up correct mode flags for order being changed
 *
 * Be very careful... some actions may result in orders being changed
 * making this function recursive.
 */

void CampaignOrderUtil::orderUpdated(CampaignLogicOwner* campGame, ICommandPosition cpi)
{
   CampaignData* campData = campGame->campaignData();
   Armies& armies = campData->getArmies();

   CommandPosition* cp = armies.getCommand(cpi);
   CampaignOrder* order = cp->getCurrentOrder();

   /*
    * Special cases:
    *
    * If we are under siege, or in battle, then we cannot update-order
    */

   if(cp->isInBattle())
     return;

   else if(cp->isGarrison())
   {
     ASSERT(cp->atTown());

     const Town& t = campData->getTown(cp->getTown());
     if(t.isSieged())
       return;
   }

#ifdef DEBUG

   cuLog.printf(cpi, "\nsetting up for new order (%s)",
      static_cast<const char*>(CampaignOrderUtil::getDescription(campData, order, OD_LONG).toStr()));
      // (const char*) order->getDescription(OD_LONG).toStr() );
#endif

   /*
    * Some previous orders, will need to update things
    * e.g. if it was upgrading a fort, then need to inform
    *      town, that it is not being built any more.
    */


   /*
    * Clear out values
    */

   cp->setNextDest(NoTown);
   cp->setTarget(NoCommandPosition);
   cp->hasPursuitTarget(False);
   cp->marchingToSoundOfBattle(False);
   cp->retreatTown(NoTown);
   cp->clearEnemyRatio();
   cp->trapped(False);


   // Note: Force march isn't set until leader passes forcemarch test
   cp->setForceMarch(False);
   cp->setSiegeActive(order->getSiegeActive());

   cp->setAggressionValues(order->getAggressLevel(), order->getPosture());
// cp->posture(order->getPosture());


   /*
    * Apply Order specific Operation if we don't have a timed-order
    * Note that finishOrder calls orderUpdated
    * so it is important that the holdOrder::beginOrder returns True!
    */

   if(order->shouldCarryOut(campData->getDate()))
   {
      if(!CampaignOrderUtil::beginOrder(campGame, order, cpi))
      {
         finishOrder(campGame, cpi);
      }
   }
}


/*
 * Unit has completed it's orders... set it to some new mode?
 *
 * Unit should keep it's agression and pursue mode, etc.
 * In most cases unit should revert to a hold mode
 *
 * Simplified 11Sep97
 */

void CampaignOrderUtil::finishOrder(CampaignLogicOwner* campGame, ICommandPosition cpi)
{
   /*
    * Immediately action OrderOnArrival wherever unit is
    */

   CampaignData* campData = campGame->campaignData();
   Armies& armies = campData->getArmies();

   CommandPosition* cp = armies.getCommand(cpi);
   CampaignOrder* order = cp->getCurrentOrder();

   /*
    * Make a new Order
    *
    * First, get type and make new order
    */

   Orders::Type::Value type = order->getOrderOnArrival();

   if(!CampaignOrderUtil::allowed(type, cpi, campGame->campaignData()))
      type = Orders::Type::Hold;

   order->setType(type);
   order->setOrderOnArrival(Orders::Type::Hold);
   order->getVias()->clear();
   order->setDestTown(NoTown);

   if(type != Orders::Type::Attach)
     order->setTarget(NoCommandPosition);

   orderUpdated(campGame, cpi);
}


void CampaignOrderUtil::processTimedOrders(CampaignLogicOwner* campGame, ICommandPosition cpi)
{
  ASSERT(campGame != 0);
  ASSERT(cpi != NoCommandPosition);
  CommandPosition* cp = campGame->campaignData()->getCommand(cpi);

  CampaignOrder* order = cp->getCurrentOrder();
  if(order->isTimedOrder())
  {
    if(order->shouldCarryOut(campGame->campaignData()->getDate()))
    {
      // if(!order->beginOrder(campGame, cpi))
      if(!CampaignOrderUtil::beginOrder(campGame, order, cpi))
        finishOrder(campGame, cpi);
    }
  }
}

/*
 * Setup a unit after loading from a new Campaign Scenario
 */

void initCampaignOrder(CampaignLogicOwner* campGame, ICommandPosition cp)
{
    if(cp->isLower(Rank_President))
    {
      CampaignOrder* order = cp->getCurrentOrder();
        if(order->getOrderOnArrival() != Orders::Type::Hold)
            finishOrder(campGame, cp);
    }
}

};    // namespace CampaignOrderUtil



const int nSides = 2;
const int range = 10;

static int getCourierSpeedAdjustment(Side side, int value)
{
  ASSERT(side < nSides);
  ASSERT(value >= 0);
  ASSERT(value < range);

  const Table2D<int>& table = scenario->getCourierSpeedAdjustmentTable();

  return table.getValue(side, value);
}


void OrderListUtil::sendOrder(const CampaignOrder* order, ICommandPosition cpi, CampaignData* campData)
{
   ASSERT(order != 0);
   ASSERT(cpi != NoCommandPosition);
   ASSERT(campData != 0);

   if(cpi->isDead())
      return;

   CommandPosition* cp = campData->getCommand(cpi);
   ASSERT(cp != 0);

#ifdef DEBUG
   cuLog.printf("\n======================= Orders Processed ==================");
   cuLog.printf("Orders sent for %s", cp->getName());
#endif
   /*
    * Calculate messenger arrival time
    */

   TimeTick arrival = campData->getTick();
   TimeTick tryAgainAt = arrival;             // if close enemy units try actioning again at

   if(!CampaignOptions::get(OPT_InstantOrders))
   {
      /*
       *  Calculate time to get from SHQ to unit
       *
       */

      TimeTick travelTime = 0;
      Distance routeDistance = 0;
      CampaignRoute route(campData);

      const CommandPosition* shqCP = campData->getArmies().getSHQ(cp->getSide());

      if(route.getRouteDistance(shqCP->location(), cp->location(), Distance_MAX, routeDistance, True))
      {
         int miles = DistanceToMile(routeDistance);
#ifdef DEBUG
         Distance slDist = 0;
         route.getRouteDistance(shqCP->location(), cp->location(), Distance_MAX, slDist);

         int slMiles = DistanceToMile(slDist);
         cuLog.printf("Distance from %s to Supreme HQ(%s) = %d miles(Route), %d miles(Straight Line)",
             cp->getNameNotNull(), shqCP->getNameNotNull(), miles, slMiles);
#endif

         /*
          *  Travel time is 12 mph for 8 hours per day(96 mpd).
          *  timeToTravel Function caculates raw time,
          *  so set MPH macro to MPH(4) to account for courier rest time
          */

         travelTime = timeToTravel(routeDistance, MPH(4));
#ifdef DEBUG
         TimeTick rawTime = travelTime;
#endif

         /*
          *  adjust travelTime
          */

         int value = CRandom::get(10);

         int adjustBy = getCourierSpeedAdjustment(cp->getSide(), value);

         UINT adjustPercent = (adjustBy < 0) ? -adjustBy : adjustBy;

         ULONG adjustTimeBy = MulDiv(adjustPercent, travelTime, 100);

         travelTime = (adjustBy < 0) ? travelTime-adjustTimeBy : travelTime+adjustTimeBy;

#ifdef DEBUG
         cuLog.printf("Courier raw Travel time is %ld, adjusted by %d%%, actual time is %ld",
                        rawTime, adjustBy, travelTime);
#endif

         /*
          *  If Ground Conditions are very muddy, courier time is multiplied by .75
          */

         if(campData->getWeather().getGroundConditions() == CampaignWeather::VeryMuddy)
         {
           travelTime *= 1.75;
#ifdef DEBUG
           cuLog.printf("It is very muddy. Travel is now %ld", travelTime);
#endif
         }

         /*
          * If SHQ is currently in a state chaos courier time is doubled
          */

         if(campData->isSHQInChaos(cp->getSide(), campData->getTick()))
         {
           travelTime *= 2;

#ifdef DEBUG
           cuLog.printf("%s SHQ is in chaos. Travel is now %ld",
              scenario->getSideName(cp->getSide()), travelTime);
#endif
         }

         /*
          * If Enemy plans have been captured then courier time is cut in half
          */

         if(campData->isEnemyPlansCaptured(cp->getSide(), campData->getTick()))
         {
           travelTime /= 2;

#ifdef DEBUG
           cuLog.printf("%s has captured enemy plans. Travel is now %ld",
              scenario->getSideName(cp->getSide()), travelTime);
#endif
         }

      }

      /*
       *  See if leader can think of order himself
       */
#if 0
      CloseUnits cl;

      CloseUnitData data;
      data.d_campData = campData;
      data.d_from = cpi->location();
      data.d_closeUnits = &cl;
      data.d_closeUnitSide = (cpi->getSide() == 0) ? 1 : 0;
      data.d_findDistance = MilesToDistance(24);

      CloseUnitUtil::findCloseUnits(data);
#endif
      if(closeEnemy(cpi, campData))
      {
        if(!didLeaderChangeOrder(cpi, campData, tryAgainAt))
        {
          arrival = campData->getTick() + travelTime;
        }
      }
      else
      {
        arrival = campData->getTick() + travelTime;
        tryAgainAt = arrival;
      }

   }

#ifdef DEBUG
   else
   {
      cuLog.printf("Instant orders option active");
   }
#endif

   /*
    * Send message
    */

   cp->sendOrder(order, arrival, tryAgainAt);

}

Boolean OrderListUtil::process(ICommandPosition cpi, CampUnitMoveData* moveData)
{
   ASSERT(cpi != NoCommandPosition);
   ASSERT(moveData != 0);

   if(processOrder(cpi, moveData))
   {
#ifdef DEBUG
      cuLog.printf(cpi, "\nactioning new orders");
#endif

      CommandPosition* cp = moveData->d_campData->getCommand(cpi);

      CampaignOrder* order = cp->getCurrentOrder();

      if(order->requiresDetach() && !order->advancedOnly())
      {
         moveData->d_campData->getArmies().detachCommand(cpi);
         order->advancedOnly(False);
      }

      return True;
   }

   return False;
}

Boolean OrderListUtil::processOrder(ICommandPosition cpi, CampUnitMoveData* moveData)
{

   ASSERT(cpi != NoCommandPosition);
   ASSERT(moveData != 0);

   CampaignData* campData = moveData->d_campData;

   Boolean changed = False;

   TimeTick theTime = campData->getTick();

   CommandPosition* cp = campData->getCommand(cpi);

   /*
    * If unit is in auto-rest mode, it cannot process anything
    */

   if(cp->getMode() == CampaignMovement::CPM_Resting)
     return False;

   CampaignOrderList& list = cp->getOrders();

   /*
    * Convert messenger to in-tray
    */

   for(CampaignOrderNode* node = list.first(); node != 0; node = list.next())
   {
      if( (node->mode() == CampaignOrderNode::Messenger) && (theTime >= node->arrival()) )
      {
#ifdef DEBUG
         const CampaignOrder* order = node->order();

         cuLog.printf("\nprocessOrders: Order transferred from messenger to in-tray");
         cuLog.printf("\nOrder=%s",
            static_cast<const char*>(CampaignOrderUtil::getDescription(campData, order, OD_LONG).toStr()));
         // (const char*) order->getDescription(OD_LONG).toStr());
#endif

         node->mode(CampaignOrderNode::InTray);
         node->arrival(theTime);

         break;      // Only process one messenger
      }

      /*
       *
       */

      else if((node->mode() == CampaignOrderNode::Messenger) && (theTime >= node->tryAgainAt()))
      {
        Boolean close = closeEnemy(cpi, campData);

        if(close)
        {
            TimeTick tryAgainAt = node->tryAgainAt();
            if(didLeaderChangeOrder(cpi, campData, tryAgainAt))
            {

               node->mode(CampaignOrderNode::InTray);
               node->arrival(theTime);
               node->tryAgainAt(theTime);

               break;      // Only process one messenger
            }
            else
               node->tryAgainAt(tryAgainAt);
        }
      }
   }

   /*
    * Transfer first in-tray to current
    */

   node = list.first();

   if(node && (node->mode() == CampaignOrderNode::InTray) && (theTime >= node->arrival()) )
   {
#ifdef DEBUG
      const CampaignOrder* order = node->order();
      cuLog.printf("\nprocessOrders: Order moved from in-tray to acting");
      cuLog.printf("\nOrder=%s",
         static_cast<const char*>(CampaignOrderUtil::getDescription(campData, order, OD_LONG).toStr()));
      // (const char*) order->getDescription(OD_LONG).toStr());
#endif

      if(list.getCurrentOrder() != node->order())     // Is it same order?
      {
         const CampaignOrder* currentOrder = node->order();
         TimeTick tick;

         // Also set a acting on order timer

         if(CampaignOptions::get(OPT_InstantOrders))
         {
            tick = theTime;
         }
         else
         {
           tick = calcActionTime(cpi, currentOrder, campData);
         }

         list.setActioningOrder(currentOrder, tick);
      }
#ifdef DEBUG
      else
      {
         const CampaignOrder* order = node->order();
         cuLog.printf("\nprocessOrders: Ignoring same order");
         cuLog.printf("\nOrder=%s",
            static_cast<const char*>(CampaignOrderUtil::getDescription(campData, order, OD_LONG).toStr()));
            // (const char*) order->getDescription(OD_LONG).toStr());
      }
#endif

      list.remove();
   }

   /*
    * Check current order
    */

   if(list.isActing())
   {
      if(list.getActing() <= theTime)
      {
#ifdef DEBUG
         const CampaignOrder* order = list.getCurrentOrder();  // node->order;
         cuLog.printf("\nprocessOrders: Order converted from acting to current");
         cuLog.printf("\nOrder=%s",
            static_cast<const char*>(CampaignOrderUtil::getDescription(campData, order, OD_LONG).toStr()));
            // (const char*) order->getDescription(OD_LONG).toStr());
#endif
         CampaignOrder* aOrder = list.getActioningOrder();
         ASSERT(aOrder);

         if(aOrder->advancedOnly())
         {
           list.getCurrentOrder()->copyAOFlags(*aOrder);
           list.getCurrentOrder()->advancedOnly(True);
         }
         else
           list.setCurrentOrder(aOrder);

         // clear ratio so enemy checks will be done again
         cpi->clearEnemyRatio();
         changed = True;
         list.resetActing();
      }
   }

   return changed;

}

static TimeTick getActionTime(RankEnum rank)
{
  ASSERT(rank < Rank_HowMany);

  const Table1D<UBYTE>& table = scenario->getActionTimeTable();

  return table.getValue(rank);
}

const int StaffRates = 5;
const int nValues    = 10;

static int getActionTimeAdjustment(Attribute staff, int value)
{
  const Table2D<int>& table = scenario->getActionTimeAdjustmentTable();

  ASSERT(value < nValues);

  enum {
    Poor,
    Fair,
    Good,
    VeryGood,
    Excellent
  } staffRate;

  if(staff > 204)
    staffRate = Excellent;
  else if(staff > 153)
    staffRate = VeryGood;
  else if(staff > 102)
    staffRate = Good;
  else if(staff > 51)
    staffRate = Fair;
  else
    staffRate = Poor;


  return table.getValue(staffRate, value);
}


TimeTick OrderListUtil::calcActionTime(ICommandPosition cpi, const CampaignOrder* order, const CampaignData* campData)
{
   ASSERT(cpi != NoCommandPosition);
   ASSERT(campData != 0);

   Boolean changed = False;

   const TimeTick theTime = campData->getTick();
   TimeTick acting = theTime;

   const CommandPosition* cp = campData->getCommand(cpi);

   /*
    * RestRally, Attach, AttachSP, and Leader orders are actioned immediately
    */

   if(order->getType() == Orders::Type::RestRally ||
      order->getType() == Orders::Type::Attach    ||
      order->getType() == Orders::Type::AttachSP  ||
      order->getType() == Orders::Type::Leader )

     ;
   else
   {
     TimeTick unModified = getActionTime(cp->getRank().getRankEnum());

     if(unModified > 0)
     {

       /*
        *  Modify time by staff rating of unit commander
        */

       Leader* leader = campData->getArmies().getUnitLeader(cpi);
       Attribute staff = leader->getStaff();

       int value = CRandom::get(10);

       int adjustBy = getActionTimeAdjustment(staff, value);

       TimeTick modified = (unModified*adjustBy)/100;

       /*
        *  Modify time by subordination rating of direct subordinates
        *  Modification applies only if subordination is greater than staff
        *
        *  Modification is 2 times the %difference between the two
        */

       UWORD subordination = campData->getArmies().getCommandSubordination(cpi);

       if(subordination > 0 && staff > 0 && subordination > staff)
       {

         int difference = subordination-staff;

         /*
          *  If 100% or more greater multiply unmodified by 3
          *  Ben didn't say do this but it keeps values sensible while
          *  staff\subordination values are still set randomly
          *
          */

         if(difference >= staff)
         {
           // to keep values to sensible limit

           modified += unModified*3;
         }
         else
         {
           int percentDifference = MulDiv(difference,  100, staff);

           modified += MulDiv(2*percentDifference, unModified, 100);
         }
       }
#ifdef DEBUG
       cuLog.printf("\nAction Order for %s. Unmodified time is %ld hours, modified time is %ld hours",
           cp->getName(), unModified, modified);
#endif

       /*
        * If SHQ is currently in a state of chaos, actioning takes twice as long
        */

       if(campData->isSHQInChaos(cp->getSide(), theTime))
       {

         modified *= 2;

#ifdef DEBUG
         cuLog.printf("\n%s SHQ is in chaos. Modified action time is %ld hours",
             scenario->getSideName(cp->getSide()), modified);
#endif
       }

       /*
        * If we have captured enemy plans, actioning takes half as long
        */

       if(campData->isEnemyPlansCaptured(cp->getSide(), theTime))
       {

         modified /= 2;

#ifdef DEBUG
         cuLog.printf("\n%s has captured enemy plans. Modified action time is %ld hours",
             scenario->getSideName(cp->getSide()), modified);
#endif
       }

       acting += modified;
     }

   }

   return acting;
}


Distance getFindDistance(RankEnum rank)
{
  switch(rank)
  {
    case Rank_ArmyGroup:
    case Rank_Army:
      return CampaignConst::enemySightingDistanceArmy;

    case Rank_Corps:
      return CampaignConst::enemySightingDistanceCorp;

    case Rank_Division:
//  case Rank_Brigade:
      return CampaignConst::enemySightingDistanceDivision;
  }

  FORCEASSERT("RankEnum not found in getFindDistance()");
  return CampaignConst::enemySightingDistanceArmy;
}

TimeTick tryAgainWhen(RankEnum rank)
{
  switch(rank)
  {
    case Rank_ArmyGroup:
    case Rank_Army:
      return HoursToTicks(8);

    case Rank_Corps:
      return HoursToTicks(4);

    case Rank_Division:
//  case Rank_Brigade:
      return HoursToTicks(2);
  }

  FORCEASSERT("RankEnum not found in tryAgainWhen()");
  return HoursToTicks(8);
}

/*
 *  Find out if a leader changes his orders on his own
 */

Boolean OrderListUtil::didLeaderChangeOrder(ICommandPosition cpi, const CampaignData* campData, TimeTick& tryAgain)
{
  ASSERT(cpi != NoCommandPosition);
  ASSERT(campData != 0);

  const CommandPosition* cp = campData->getCommand(cpi);

  /*
   *  If we're here then enemy units are near
   *
   *  Chance of leader changing his orders are initiative % of 255
   */

  Leader* leader = campData->getArmies().getUnitLeader(cpi);

#ifdef DEBUG
  cuLog.printf("\nChecking to see if %s will change his orders", leader->getName());
#endif

  int percent = MulDiv(leader->getInitiative(), 100, 255);

  /*
   *  See if he will actually change orders
   */

  int chance = CRandom::get(100);

  if(chance < percent)
  {
#ifdef DEBUG
    cuLog.printf("\n%s changed his orders", leader->getName());
#endif
    return True;
  }
  else
    tryAgain += tryAgainWhen(cp->getRank().getRankEnum());

#ifdef DEBUG
  cuLog.printf("\n%s did not change his orders", leader->getName());
#endif

  return False;
}

Boolean OrderListUtil::closeEnemy(ICommandPosition cpi, const CampaignData* campData)
{
  ASSERT(cpi != NoCommandPosition);
  ASSERT(campData != 0);

  CloseUnits cl;

  CloseUnitData data;
  data.d_campData = const_cast<CampaignData*>(campData);
  data.d_cpi = cpi;
  data.d_from = cpi->location();
  data.d_closeUnits = &cl;
  data.d_closeUnitSide = (cpi->getSide() == 0) ? 1 : 0;
  data.d_findDistance = MilesToDistance(24);
  if(cpi->isRetreating())
    data.d_flags |= CloseUnitData::AllAdjacent;

  CloseUnitUtil::findCloseUnits(data);

  return  static_cast<Boolean>( (cl.getClosestSeen() != NoCommandPosition) &&
                                (cl.getClosestSeenDist() < getFindDistance(cpi->getRank().getRankEnum())) );
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
