/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef LOSSES_HPP
#define LOSSES_HPP

#ifndef __cplusplus
#error losses.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Campaign Losses, Stragglers and Attrition support functions
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "unittype.hpp"
#include "loss_def.hpp"

class Armies;

// class Losses {
//  public:

namespace Losses
{
	SPCount makeLosses(Armies* army, ICommandPosition cpi, SPLoss losses);
	SPCount makeLosses(Armies* army, ICommandPosition cpi, SPLoss losses, BasicUnitType::value basicType, SpecialUnitType::value specialType);
#if 0	// There are no stragglers any more!
	static SPCount makeStragglers(Armies* army, ICommandPosition cpi, SPLoss losses);
	static SPCount recoverStragglers(Armies* army, ICommandPosition cpi, SPLoss gains);
#endif
	SPCount makeAttrition(Armies* army, ICommandPosition cpi, SPLoss losses, AttritionClass::value c);
};

#endif /* LOSSES_HPP */

