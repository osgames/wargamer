/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CU_ORDER_HPP
#define CU_ORDER_HPP

#ifndef __cplusplus
#error cu_order.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Unit's Order reception
 *
 *----------------------------------------------------------------------
 */

#include "clogic_dll.h"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "measure.hpp"

// class CampaignLogicOwner;
class CampaignLogicOwner;
class CampaignOrder;
class CampaignOrderList;
struct CampUnitMoveData;
class CloseUnits;

namespace CampaignOrderUtil {
	void processCampaignOrders(CampUnitMoveData* campGame, ICommandPosition hunit);
	void processTimedOrders(CampaignLogicOwner* campData, ICommandPosition cpi);
	CLOGIC_DLL void orderUpdated(CampaignLogicOwner* campData, ICommandPosition cpi);
	void finishOrder(CampaignLogicOwner* campData, ICommandPosition cpi);
	// static void holdUnit(CampaignLogicOwner* campData, ICommandPosition cpi);

    CLOGIC_DLL void initCampaignOrder(CampaignLogicOwner* campGame, ICommandPosition cpi);
        // Initialise newly loaded campaign unit

};


class OrderListUtil {
  public:
	 static void sendOrder(const CampaignOrder* order, ICommandPosition cpi, CampaignData* campData);
	 static Boolean process(ICommandPosition, CampUnitMoveData* moveData);
  private:
	 static Boolean processOrder(ICommandPosition cpi, CampUnitMoveData* moveData);
	 static TimeTick calcActionTime(ICommandPosition cpi, const CampaignOrder* order, const CampaignData* campData);
	 static Boolean didLeaderChangeOrder(ICommandPosition cpi, const CampaignData* campData, TimeTick& tryAgain);
	 static Boolean closeEnemy(ICommandPosition cpi, const CampaignData* campData);
};

#endif /* CU_ORDER_HPP */

