/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CONDUTIL_HPP
#define CONDUTIL_HPP

#include "c_cond.hpp"

using namespace WG_CampaignConditions;

class CampaignLogicOwner;

class CampaignConditionUtil {
  public:
	 static void instantUpdate(CampaignLogicOwner* campGame, ConditionType::Type typeID, ConditionTypeData* data);
	 static void dailyUpdate(CampaignLogicOwner* campGame);
	 static void monthlyUpdate(CampaignLogicOwner* campGame);
  private:
	 // static void beginAction(CampaignLogicOwner* campGame, Conditions* condition);

};


#endif
