/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Despatching Reorganization Messages
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ds_reorg.hpp"

#ifdef DEBUG
#include "clog.hpp"
#endif


namespace DS_Reorg {

#ifdef DEBUG
LogFileFlush reorgLog("reorg.log");
#endif

/*--------------------------------------------------------------
 * Reorganization List
 */



ReorgList::ReorgList(ConstParamCP cp) :
   d_dest(constCPtoCP(cp)),
   d_items()
{
#ifdef DEBUG
   reorgLog.printf("Created ReorgList(%d)", static_cast<int>(cp->getSelf()));
#endif
   d_items.reserve(8);     // More than a Typical size, but less than STL's default 4096/sizeof()
}


ReorgList::ReorgList(void) :
   d_dest(NULL),
   d_items() {

   d_items.reserve(8);
}



ReorgList::~ReorgList()
{
#ifdef DEBUG
   reorgLog.printf("Destroyed ReorgList");
#endif
}

void ReorgList::addPush()
{
#ifdef DEBUG
   reorgLog.printf("ReorgList::push");
#endif
//   ReorgItem* newItem = d_items.insert(d_items.end(), ReorgItem());
//   newItem->s_what = ReorgItem::Push;
   Container::iterator newItem = d_items.insert(d_items.end(), ReorgItem());
   newItem->s_what = ReorgItem::Push;
}

void ReorgList::addPop()
{
#ifdef DEBUG
   reorgLog.printf("ReorgList::pop");
#endif
   Container::iterator newItem = d_items.insert(d_items.end(), ReorgItem());
   newItem->s_what = ReorgItem::Pop;
}

void ReorgList::addCreateCP(ConstParamCP cp, Nationality n, RankEnum rank, bool independent)
{
#ifdef DEBUG
   reorgLog.printf("ReorgList::createCP(%d)", static_cast<int>(cp->getSelf()));
#endif
   Container::iterator newItem = d_items.insert(d_items.end(), ReorgItem());
   newItem->s_what = ReorgItem::NewCP;
   newItem->s_cp = constCPtoCP(cp);
   newItem->s_nation = n;
   newItem->s_rank = rank;
   newItem->s_independent = independent;
}

void ReorgList::addCreateLeader(ConstParamCP cp, Nationality n, RankEnum rank)
{
#ifdef DEBUG
   reorgLog.printf("ReorgList::createCP(%d)", static_cast<int>(cp->getSelf()));
#endif
   Container::iterator newItem = d_items.insert(d_items.end(), ReorgItem());
   newItem->s_what = ReorgItem::NewLeader;
   newItem->s_cp = constCPtoCP(cp);
   newItem->s_rank = rank;
   newItem->s_nation = n;
}

void ReorgList::addTransferCP(ConstParamCP cp)
{
#ifdef DEBUG
   reorgLog.printf("ReorgList::transferCP(%d)", static_cast<int>(cp->getSelf()));
#endif
   Container::iterator newItem = d_items.insert(d_items.end(), ReorgItem());
   newItem->s_what = ReorgItem::AttachCP;
   newItem->s_cp = constCPtoCP(cp);
}

void ReorgList::addTransferSP(ConstParamCP cp, const ConstISP& isp)
{
#ifdef DEBUG
   reorgLog.printf("ReorgList::transferSP(%d,%d)",
      static_cast<int>(cp->getSelf()),
      static_cast<int>(isp->getID()));
#endif
   Container::iterator newItem = d_items.insert(d_items.end(), ReorgItem());
   newItem->s_what = ReorgItem::AttachSP;
   newItem->s_cp = constCPtoCP(cp);
   newItem->s_sp = constSPtoSP(isp);
}

void ReorgList::addTransferLeader(const ConstILeader& iLeader, ConstParamCP cpi)
{
#ifdef DEBUG
   reorgLog.printf("ReorgList::transferLeader(%d)", static_cast<int>(iLeader->getID()));
#endif
   Container::iterator newItem = d_items.insert(d_items.end(), ReorgItem());
   newItem->s_what = ReorgItem::AttachLeader;
   newItem->s_leader = constLeaderToLeader(iLeader);
   newItem->s_cp = constCPtoCP(cpi);
}


void ReorgList::process(CampaignData* campData)
{
   /*
    * A simple stack to avoid recursion
    */

   const int StackSize = 16;        // We shouldnt' really need more than about 5!

   struct StackItem
   {
      ICommandPosition s_cp;  // Current unit being attached to
   } stack[StackSize];

   int stackTop = 0;

   ICommandPosition currentCPI = d_dest;
   ICommandPosition lastCP = d_dest;

   Armies* army = &campData->getArmies();

   for(std::vector<ReorgItem>::const_iterator i = d_items.begin(); i != d_items.end(); ++i)
   {
      switch(i->s_what)
      {
      case ReorgItem::NewCP:
         {
#ifdef DEBUG
            reorgLog.printf("Processing NewCP(%d)",
               static_cast<int>(i->s_cp->getSelf()));
#endif
            ICommandPosition attachTo = (i->s_independent) ?
               army->getPresident(currentCPI->getSide()) :
               currentCPI;

            if((attachTo != NoCommandPosition) && !attachTo->isDead())
            {
               ICommandPosition newCP = army->createChildCP(attachTo);

               newCP->setSide(currentCPI->getSide());
               newCP->setRank(i->s_rank);
               newCP->setPosition(i->s_cp->getPosition());
               newCP->setSupply(i->s_cp->getSupply());
               newCP->setNation(i->s_nation);
               newCP->isActive(True);
               army->makeUnitName(newCP, i->s_rank);

               /*
                * Create a default leader...
                */

               ILeader leader = army->createLeader(currentCPI->getSide(), newCP->getNation(), Rank_Division);
               CampaignArmy_Util::transferLeader(campData, leader, newCP, False, False);
               lastCP = newCP;
            }
            else
               lastCP = NoCommandPosition;
         }
         break;

      case ReorgItem::NewLeader:
         {
#ifdef DEBUG
            reorgLog.printf("Processing NewLeader(%d)",
               static_cast<int>(i->s_cp->getSelf()));
#endif
            if((currentCPI != NoCommandPosition) && !currentCPI->isDead())
            {
               ILeader leader = army->createLeader(currentCPI->getSide(), i->s_nation, i->s_rank);
               CampaignArmy_Util::transferLeader(campData, leader, i->s_cp, False, False);
            }
            lastCP = currentCPI;
            break;
         }
      case ReorgItem::AttachCP:
         {
#ifdef DEBUG
            reorgLog.printf("Processing AttachCP(%d)",
               static_cast<int>(i->s_cp->getSelf()));
#endif
            // If at same location... otherwise ignore the order

            if((currentCPI != NoCommandPosition) &&
               !currentCPI->isDead() &&
               !i->s_cp->isDead() &&
               (currentCPI->getPosition() == i->s_cp->getPosition()) )
            {
               CampaignArmy_Util::attachUnits(campData, currentCPI, i->s_cp);
            }
            lastCP = i->s_cp;

         }
         break;
      case ReorgItem::AttachSP:
         {
#ifdef DEBUG
            reorgLog.printf("Processing AttachSP(%d)",
               static_cast<int>(i->s_sp->getID()));
#endif
            if ( (currentCPI != NoCommandPosition) &&
               !currentCPI->isDead() &&
               !i->s_cp->isDead() &&
               (currentCPI->getPosition() == i->s_cp->getPosition()) &&
               i->s_cp->isSPattached(i->s_sp) )
            {
               army->transferStrengthPoint(i->s_cp, currentCPI, i->s_sp);
            }
            lastCP = currentCPI;
         }
         break;
      case ReorgItem::AttachLeader:
         {
#ifdef DEBUG
            reorgLog.printf("Processing AttachLeader(%d)",
               static_cast<int>(i->s_leader->getID()));
#endif
            ASSERT(i->s_cp != NoCommandPosition);

            if ((currentCPI != NoCommandPosition) &&
               !currentCPI->isDead() &&
               !i->s_leader->isDead())
            {
               if(currentCPI != army->getPresident(i->s_leader->getSide()))
                 CampaignArmy_Util::transferLeader(campData, i->s_leader, currentCPI, True, False);
               else
               {
                  if(!i->s_cp->isDead())
                    CampaignArmy_Util::makeIndependentLeader(campData, i->s_leader, i->s_cp->getPosition());
               }
            }

            lastCP = currentCPI;
         }
         break;
      case ReorgItem::Push:
         {
#ifdef DEBUG
            reorgLog.printf("Processing Push");
#endif
            ASSERT(stackTop < StackSize);
            stack[stackTop++].s_cp = currentCPI;
            currentCPI = lastCP;
         }
         break;
      case ReorgItem::Pop:
         {
#ifdef DEBUG
            reorgLog.printf("Processing Pop");
#endif
            ASSERT(stackTop > 0);
            currentCPI = stack[--stackTop].s_cp;
         }
         break;
      default:
         FORCEASSERT("Illegal what in Reorglist");
         break;
      }
   }

   ASSERT(stackTop == 0);
}

/*--------------------------------------------------------------
 * Actual Despatcher Message
 */



/*
 * Actually perform the desired transfer orders!
 *
 * Problem is that orders may not actually be valid because
 * units may have moved away, or even died in the time it took
 * the player to use the Reorganize dialog.
 *
 * We could either convert them to conventional orders in which
 * case units will have to march about to get to where they are
 * attaching to.  Or we can just check for being at the same
 * place and ignore the order if they are not at the same location.
 *
 * Converting to conventional orders is awkward because it means the
 * units will have to stack up lots of orders, e.g. take command of SP
 * attach to, etc...  If the units have moved about then they will have
 * to wander about picking up their strength points, etc.
 *
 * Another problem is that temporary leaders may be created, or leaders
 * auto-promoted/demoted.  e.g. CreateCP will make a temporary leader,
 * Then a following attachleader will cause the new leader to be demoted
 * displacing some already existing real leaders.
 *
 * Solutions to this could be either:
 *    - Lock the army preventing any movement or combat while the
 *      reorganization takes place.  Do not create new leaders until
 *      all reorganization items have been processed.  Then fill in
 *      or autopromote any missing slots.
 *    - Mark temporary leaders with a flag.  The flag can be cleared
 *      as soon as they are processed by the game logic movement.
 *      If the flag is set, then they are deleted instead of being
 *      demoted.
 */

void DS_Reorganize::process(CampaignData* campData)
{
#ifdef DEBUG
   reorgLog.printf("DS_Reorganize::process()");
#endif

   d_orgList.process(campData);
}


/*
Pack a multiplayer message into buffer
NOTE : buffer points to MP_MSG_CAMPAIGNORDER
*/
int DS_Reorganize::pack(void * buffer, void * gamedata) {

   CampaignData * campData = (CampaignData *) gamedata;

   MP_MSG_CAMPAIGNORDER * msg = (MP_MSG_CAMPAIGNORDER *) buffer;

   msg->order_type = CMP_MSG_REORGANIZE;

   int * dataptr = (int *) msg->data;
   int bytes=0;

   *dataptr = (int) CampaignOBUtil::cpiToCPIndex(d_orgList.d_dest, *campData->getArmies().ob() );
   dataptr++;
   bytes+=4;

   // num in array
   *dataptr = d_orgList.d_items.size();
   dataptr++;
   bytes+=4;

   for(int i=0; i<d_orgList.d_items.size(); i++) {
      
      ReorgList::ReorgItem & item = d_orgList.d_items[i];

      *dataptr = (int) item.s_what;
      dataptr++;
      *dataptr = (int) CampaignOBUtil::cpiToCPIndex(item.s_cp, *campData->getArmies().ob() );
      dataptr++;
      *dataptr = (int) CampaignOBUtil::leaderToLeaderIndex(item.s_leader, *campData->getArmies().ob() );
      dataptr++;
      *dataptr = (int) CampaignOBUtil::spiToSPIndex(item.s_sp, *campData->getArmies().ob() );
      dataptr++;
      *dataptr = (int) item.s_rank;
      dataptr++;
      *dataptr = (int) item.s_nation;
      dataptr++;
      *dataptr = (int) item.s_independent;
      dataptr++;

      bytes += 28;
   }

   return bytes;
}

/*
Unpack a multiplayer message from buffer
NOTE : buffer points to MP_MSG_CAMPAIGNORDER
*/
void DS_Reorganize::unpack(void * buffer, void * gamedata) {

   CampaignData * campData = (CampaignData *) gamedata;

   MP_MSG_CAMPAIGNORDER * msg = (MP_MSG_CAMPAIGNORDER *) buffer;

   int * dataptr = (int *) msg->data;

   d_orgList.d_dest = CampaignOBUtil::cpIndexToCPI(*dataptr, *campData->getArmies().ob() );
   dataptr++;

   int num = *dataptr;
   dataptr++;

   for(int i=0; i<num; i++) {

      ReorgList::ReorgItem item;

      item.s_what = (DS_Reorg::ReorgList::ReorgItem::WhatEnum) *dataptr;
      dataptr++;
      item.s_cp = CampaignOBUtil::cpIndexToCPI(*dataptr, *campData->getArmies().ob() );
      dataptr++;
      item.s_leader = CampaignOBUtil::leaderIndexToLeader(*dataptr, *campData->getArmies().ob() );
      dataptr++;
      item.s_sp = CampaignOBUtil::spIndexToISP(*dataptr, *campData->getArmies().ob() );
      dataptr++;
      item.s_rank = (RankEnum) *dataptr;
      dataptr++;
      item.s_nation = *dataptr;
      dataptr++;
      item.s_independent = (*dataptr != 0);
      dataptr++;

      d_orgList.d_items.push_back(item);
   }

}



#ifdef DEBUG
String DS_Reorganize::getName()
{
   String s;

   s = "Reorganize order";

   // We could list all the orders here, or at least the top unit

   return s;
}
#endif


/*-----------------------------------------------------------------
 * Functions called by User Interface, AI, etc...
 */

HOrganize start(ConstParamCP dest)
{
#ifdef DEBUG
   reorgLog.printf(">start(%d)", static_cast<int>(dest->getSelf()));
#endif

   return new DS_Reorganize(dest);
}

void end(HOrganize handle)
{
#ifdef DEBUG
   reorgLog.printf("<end(%p)", static_cast<void*>(handle));
#endif
   Despatcher::sendMessage(handle);
}

void push(HOrganize handle)
{
#ifdef DEBUG
   reorgLog.printf(">push(%p)", static_cast<void*>(handle));
#endif

   handle->orgList()->addPush();
}

void pop(HOrganize handle)
{
#ifdef DEBUG
   reorgLog.printf("<pop(%p)", static_cast<void*>(handle));
#endif
   handle->orgList()->addPop();
}

void createCP(HOrganize handle, ConstParamCP cp, Nationality n, RankEnum rank, bool independent)
{
#ifdef DEBUG
   reorgLog.printf("createCP(%p, %d)",
      static_cast<void*>(handle),
      static_cast<int>(cp->getSelf()));
#endif
   handle->orgList()->addCreateCP(cp, n, rank, independent);
}

void createLeader(HOrganize handle, ConstParamCP cp, Nationality n, RankEnum rank)
{
#ifdef DEBUG
   reorgLog.printf("createCP(%p, %d)",
      static_cast<void*>(handle),
      static_cast<int>(cp->getSelf()));
#endif
   handle->orgList()->addCreateLeader(cp, n, rank);
}

void transferCP(HOrganize handle, ConstParamCP cp)
{
#ifdef DEBUG
   reorgLog.printf("transferCP(%p, %d)", static_cast<void*>(handle), static_cast<int>(cp->getSelf()));
#endif
   handle->orgList()->addTransferCP(cp);
}

void transferSP(HOrganize handle, ConstParamCP cp, const ConstISP& isp)
{
#ifdef DEBUG
   reorgLog.printf("transferSP(%p, %d,%d)",
      static_cast<void*>(handle),
      static_cast<int>(cp->getSelf()),
      static_cast<int>(isp->getID()));
#endif
   handle->orgList()->addTransferSP(cp, isp);
}

void transferLeader(HOrganize handle, const ConstILeader& iLeader, ConstParamCP cpi)
{
#ifdef DEBUG
   reorgLog.printf("transferLeader(%p, %d)", static_cast<void*>(handle), static_cast<int>(iLeader->getID()));
#endif
   handle->orgList()->addTransferLeader(iLeader, cpi);
}


}; // namespace DS_Reorg

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
