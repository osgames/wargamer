/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef DESPATCH_H
#define DESPATCH_H

#ifndef __cplusplus
#error despatch.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Message Despatcher
 *
 * Any action that a player does that is to affect the world data
 * is done by sending a message to this module.
 *
 * "player" means either the human player, computer AI or remote
 * computer player.
 *
 * Part of this module's function is to coordinate messages from
 * remote computers so that they all get actioned in the same
 * game tick.
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.3  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/28 09:33:33  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/23 14:08:58  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "clogic_dll.h"
#include "MultiplayerMsg.hpp"
#include "gdespat.hpp"     // Generic Despatcher

// #include "ds_unit.hpp"
#include "myassert.hpp"

#include "campdint.hpp"

#include "directplay.hpp"
#include "MultiplayerConnection.hpp"

// #include "mytypes.h"
// #include "llist.hpp"
// #include "critical.hpp"
//
// #ifdef DEBUG
// #include <string.hpp>
// #endif

class CampaignData;

/*
 * List of messages to be processed next time frame
 */

struct DespatchMessage : public GDespatchMessage
{
public:
   virtual ~DespatchMessage() = 0;
   virtual void process(CampaignData* campData) = 0;

#ifdef DEBUG
   virtual String getName() = 0;
#endif

};

inline DespatchMessage::~DespatchMessage()
{
}


/*
 * Despatcher Class
 */

class CLOGIC_DLL Despatcher : private GDespatcher
{
public:
   static void process(CampaignData* campData) { instance()->doProcess(campData);   }     // Called by game logic each tick
   static void sendMessage(DespatchMessage* msg) { instance()->doSendMessage(msg); }
   static void slaveSend(DespatchMessage * msg, ULONG activationTime) { instance()->doSlaveSend(msg, activationTime); }
   static void clear();

   static Despatcher* instance();

   // The ticks sent to the Slave must NEVER exceed this interval
   // otherwise order procesing cannot be guaranteed to be synchronised
   // (this interval is added to the activation time of all DespatchMessages in Battles)
   unsigned int maxSyncInterval;

private:
   Despatcher() : d_campData(0) {
      maxSyncInterval = SysTick::TicksPerSecond;
   }
   ~Despatcher() { }


   void doProcess(CampaignData* campData);
   void doSendMessage(DespatchMessage* msg);
   void doSlaveSend(DespatchMessage* msg, ULONG activationTime);

   virtual void actionMessage(GDespatchMessage* msg);



private:

   CampaignData* d_campData;

   MP_MSG_CAMPAIGNORDER d_MultiplayerCampaignOrder;

   static Despatcher* s_instance;

};

/*
 * Global Instance of Despatcher.
 */

// CLOGIC_DLL extern Despatcher despatch;

#endif /* DESPATCH_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 *----------------------------------------------------------------------
 */
