/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Combat Utilities
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cc_util.hpp"
#include "cc_close.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "scenario.hpp"
#include "options.hpp"
#include "route.hpp"
#include "town.hpp"
#include "wg_rand.hpp"
#include "sync.hpp"

#ifdef DEBUG
 #include "logwin.hpp"
 #include "unitlog.hpp"

//#define DEBUG_SPCOUNT
#endif
#include "todolog.hpp"

/*
 * Estimate a units spcount taking fog of war into account
 */

SPCount CampaignCombatUtility::calcSPCount(CampaignData* campData, const ICommandPosition& cpi, Boolean actual)
{
  ASSERT(cpi != NoCommandPosition);
  ASSERT(campData != 0);

  SPCount spCount = campData->getArmies().getUnitSPCount(cpi, True);

  // if there is no fog of war, return actual value
  if(actual || !CampaignOptions::get(OPT_FogOfWar) || spCount == 0)
  {
    return spCount;
  }

#ifdef DEBUG_SPCOUNT
  cuLog.printf("\nAdjusting ForceRatio for FogOfWar for %s", (const char*)campData->getUnitName(cpi).toStr());
  cuLog.printf("\nActual SPCount is %d", static_cast<int>(spCount));
#endif

  /*
   * If VeryLimitedInfo then adjust enemySP by a random number between + or - 30% of SPCount
   */

  SPCount c = spCount;

  if(cpi->getInfoQuality() >= CommandPosition::VeryLimitedInfo)
  {
    if(cpi->getInfoQuality() == CommandPosition::VeryLimitedInfo)
    {
      int percent = MulDiv(spCount, 3, 10);

      c = static_cast<SPCount>(CRandom::get(spCount-percent, spCount+percent));

#ifdef DEBUG_SPCOUNT
      cuLog.printf("\nInfoQuality is VeryLimitedInfo. 30%% is %d. Adjusted SPCount is %d",
          percent, static_cast<int>(c));
#endif
    }

    // if our leader is feared, then SP count is doubled if under 30, otherwise
    // it is increased by 30
    const int increaseBy = 30;
    if(cpi->getLeader()->isFeared())
    {
      c = (spCount >= increaseBy) ? c + increaseBy : c * 2;
#ifdef DEBUG_SPCOUNT
      cuLog.printf("\nCount increased to %d (%s is feared by Enemy)",
        static_cast<int>(c), cpi->getLeader()->getNameNotNull());
#endif
    }
  }

  /*
   * Otherwise, count number of enemy divisions, get random number between 4 & 13,
   * and multiply
   */

  else if(cpi->getInfoQuality() <= CommandPosition::NoInfo)
  {
    UnitIter iter(&campData->getArmies(), cpi);

    int nDivisions = 0;
    while(iter.next())
    {
      CommandPosition* cp = iter.currentCommand();

      if(cp->getRankEnum() == Rank_Division)
      {
        nDivisions++;
      }
    }

    c = nDivisions * CRandom::get(4, 13);

#ifdef DEBUG_SPCOUNT
    cuLog.printf("\nNumber of divisions is %d, estimated SPCount is %d",
      nDivisions, static_cast<int>(spCount));
#endif
  }

  // set fog of war percent. Note: this is used by info displays
  SWORD fowPercent = 0;
  if(c > 0 && c != spCount)
  {
    SWORD dif = c - spCount;
    fowPercent = static_cast<SWORD>(MulDiv(dif, 100, spCount));
  }

  cpi->fowPercent(fowPercent);

  return c;
}

/*-------------------------------------------------------------------------
 * Collect Information about close units
 */

void CloseUnitUtil::findCloseUnits(CloseUnitData& data)
{
  ASSERT(data.d_campData);
  ASSERT(data.d_from);
  ASSERT(data.d_closeUnits);
  ASSERT(data.d_closeUnitSide != SIDE_Neutral);

  // RWLock lock;
  // lock.startWrite();

  Boolean opposingSides = (data.d_cpi != NoCommandPosition && data.d_cpi->getSide() != data.d_closeUnitSide);

#ifdef DEBUG_CLOSE_ENEMY
  if(data.d_cpi != NoCommandPosition && opposingSides)
  {
    cuLog.printf("\nFinding close enemy units for %s", (const char*)data.d_campData->getUnitName(data.d_cpi).toStr());
  }
#endif

  /*
   * reset our list
   */

  data.d_closeUnits->reset();

  /*
   * Iterate through top level of close unit side
   */

  Armies* armies = &data.d_campData->getArmies();
  ICommandPosition topCPI = armies->getFirstUnit(data.d_closeUnitSide);

  ICommandPosition closestCPI = NoCommandPosition;
  Distance closestDist = Distance_MAX;

#ifdef DEBUG
#define DEBUG_COUNTUNITS
#ifdef DEBUG_COUNTUNITS
  int nUnitsChecked = 0;
#endif
#endif

  UnitIter uIter(armies, topCPI);
  while(uIter.sister())
  {
    ICommandPosition cpi = uIter.current();
    if(data.d_flags & CloseUnitData::MovingIntoBattle &&
       cpi->getMode() != CampaignMovement::CPM_AcceptBattle &&
       cpi->getMode() != CampaignMovement::CPM_IntoBattle)
    {
      continue;
    }

    if(cpi != data.d_cpi && !cpi->isSurrendering())
    {
#ifdef DEBUG_COUNTUNITS
       nUnitsChecked++;
#endif

       static CampaignRoute route(data.d_campData);
       Distance routeDistance = 0;

       Boolean shouldAdd = False;
       if(route.getRouteDistance(data.d_from, cpi->location(), data.d_findDistance, routeDistance))
       {

          /*
           * Add unit to list of units...
           */

          shouldAdd = True;
       }

       else
       {
          /*
           * or if we should add units that are connected to us
           * and we have some info on the unit
           */

          if(cpi->isSeen() && data.d_flags & CloseUnitData::AllAdjacent)
          {
             shouldAdd = CampaignRouteUtil::isAdjacent(data.d_campData, *data.d_from, cpi->getPosition());
          }

          if(!shouldAdd && routeDistance < closestDist)
//        else if( (!shouldAdd) &&
//                 (closestDist == Distance_MAX || routeDistance < closestDist) )
          {
             closestDist = routeDistance;
             closestCPI = cpi;
          }
       }

       if(shouldAdd)
       {
          SPCount spCount = data.d_campData->getArmies().getUnitSPCount(cpi, True);
//        SPCount spCount = CampaignCombatUtility::calcSPCount(data.d_campData, cpi, !opposingSides);
//#if 0
          if(opposingSides && (spCount > 0))
          {
            spCount += static_cast<SPCount>(MulDiv(cpi->fowPercent(), spCount, 100));
          }
//#endif
          data.d_closeUnits->add(data.d_campData, cpi, routeDistance, spCount);
       }
    }
  }

  /*
   * If no units in range, add closest found
   */

  if(data.d_closeUnits->getClosest() == NoCommandPosition)
  {
#ifdef DEBUG_UNITCOUNTS
    cuLog.printf("Units checked = %d", nUnitsChecked);
#endif

     //---- This can happen if only one unit at top of list
    ASSERT(!opposingSides || (closestCPI != NoCommandPosition));

     if(closestCPI != NoCommandPosition)
     {
       SPCount spCount = data.d_campData->getArmies().getUnitSPCount(closestCPI, True);

       if(opposingSides && (spCount > 0))
       {
          spCount += static_cast<SPCount>(MulDiv(closestCPI->fowPercent(), spCount, 100));
       }

       data.d_closeUnits->add(data.d_campData, closestCPI, closestDist, spCount);
    }
  }

  // lock.endWrite();
}

Boolean CloseUnitUtil::findCloseEnemy(CampaignData* campData, ITown itown, Distance maxDistance)
{
   Armies& armies = campData->getArmies();

   const Town& town = campData->getTown(itown);
   Location location = town.getLocation();
   Side side = town.getSide();

   /*
    * Iterate through top level of enemy side(s)
    */

   NationIter nIter(&armies);
   while(++nIter)
   {
      Side unitSide = nIter.current();

      if(side != unitSide)
      {
         ICommandPosition topUnit = armies.getFirstUnit(unitSide);

         UnitIter uIter(&armies, topUnit);
         while(uIter.sister())
         {
            ICommandPosition cpiUnit = uIter.current();

            static CampaignRoute s_route(campData);
            const CampaignPosition& townPos = itown;
            const CampaignPosition& unitPos = cpiUnit->getPosition();

            Distance d = Distance_MAX;
            if(s_route.getRouteDistance(&townPos, &unitPos, maxDistance, d))
            {
               ASSERT(d <= maxDistance);
               return True;
            }
         }
      }
   }

   return False;
}

#if 0 // old way
static void CampaignCombatUtility::findCloseUnits(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits, Distance findDistance)
{
   ASSERT(closeUnits.currentUnit == NoCommandPosition);
   closeUnits.reset();

#ifdef DEBUG
   closeUnits.currentUnit = cpi;
#endif

   // Armies* armies = combData->d_armies;
   Armies* armies = &campData->getArmies();

   CommandPosition* cp = armies->getCommand(cpi);
   Side side = cp->getSide();

   Location hLocation;
   Location tLocation;
   cp->getHeadLocation(hLocation);
   cp->getTailLocation(tLocation);

   /*
    * Iterate through top level of enemy side(s)
    */

   NationIter nIter(armies);
   while(++nIter)
   {
      Side enemySide = nIter.current();

      if(scenario->isEnemy(side, enemySide))
      {
         ICommandPosition topEnemy = armies->getFirstUnit(enemySide);


         UnitIter uIter(armies, topEnemy);
         while(uIter.sister())
         {
            ICommandPosition cpiEnemy = uIter.current();
            CommandPosition* cpEnemy = armies->getCommand(cpiEnemy);

            if(!cpEnemy->isGarrison())
            {
               /*
                * First do a quick check for physical proximity
                */

               Location ehLocation;
               Location etLocation;
               cpEnemy->getHeadLocation(ehLocation);
               cpEnemy->getTailLocation(etLocation);

               if( (distanceLocation(hLocation, ehLocation) < findDistance) ||
                  (distanceLocation(hLocation, etLocation) <  findDistance) ||
                  (distanceLocation(tLocation, ehLocation) <  findDistance) ||
                  (distanceLocation(tLocation, etLocation) <  findDistance) )
               {
#ifdef DEBUG_DISTANCE
                  cuLog.printf("Physical distance between %s and %s is < %ld",
                     (const char*) getUnitName(cpi).toStr(),
                     (const char*) getUnitName(cpiEnemy).toStr(),
                     (long) findDistance);
#endif
                  /*
                   * Now for the complicated task of finding the actual route
                   * distance.
                   */

                  Distance routeDistance = 0;
                  CampaignRoute route(campData);
                  if(route.getRouteDistance(cp->location(), cpEnemy->location(), findDistance, routeDistance))
                  {
#ifdef DEBUG_DISTANCE
                     cuLog.printf("Actual distance between %s and %s is < %ld (%ld miles)",
                        (const char*) getUnitName(cpi).toStr(),
                        (const char*) getUnitName(cpiEnemy).toStr(),
                        (long) routeDistance,
                        (long) DistanceToMile(routeDistance));
#endif

                     /*
                      * Add unit to list of units...
                      */

                     // closeUnits.add(cpiEnemy, routeDistance, armies->getUnitSPValue(cpiEnemy, True));
                     closeUnits.add(campData, cpiEnemy, routeDistance, armies->getUnitSPValue(cpiEnemy));

                     campData->setLastSeenLocation(cpiEnemy);
                     // CampaignPosition pos = (CampaignPosition)*cpEnemy;
                     // cpEnemy->setLastSeenLocation(pos);
#ifdef DEBUG
                     logWindow->printf("%s is seen", cpEnemy->getName());
#endif
                  }
               }
            }
         }
      }
   }

}


/*
 * Finds all enemy within range in addition the closest
 * enemy regardless of range(only if none are within range).
 * Called by CampaignMovement::moveCampaignUnit
 * which passes CloseUnits to various campaign utilities
 */

static void CampaignCombatUtility::findEnemyUnits(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits, Distance findDistance)
{
   ASSERT(closeUnits.currentUnit == NoCommandPosition);
   closeUnits.reset();

#ifdef DEBUG
   closeUnits.currentUnit = cpi;
#endif

   // Armies* armies = combData->d_armies;
   Armies* armies = &campData->getArmies();

   ICommandPosition closestEnemy = NoCommandPosition;
   Distance closestEnemyDist = Distance_MAX;

   CommandPosition* cp = armies->getCommand(cpi);
   Side side = cp->getSide();

   Location hLocation;
   Location tLocation;
   cp->getHeadLocation(hLocation);
   cp->getTailLocation(tLocation);

   /*
    * Iterate through top level of enemy side(s)
    */

   NationIter nIter(armies);
   while(++nIter)
   {
      Side enemySide = nIter.current();

      if(scenario->isEnemy(side, enemySide))
      {
         ICommandPosition topEnemy = armies->getFirstUnit(enemySide);


         UnitIter uIter(armies, topEnemy);
         while(uIter.sister())
         {
            ICommandPosition cpiEnemy = uIter.current();
            CommandPosition* cpEnemy = armies->getCommand(cpiEnemy);

            /*
             * Now for the complicated task of finding the actual route
             * distance.
             */

            Distance routeDistance = 0;
            static CampaignRoute route(campData);
            if(route.getRouteDistance(cp->location(), cpEnemy->location(), findDistance, routeDistance))
            {

                /*
                 * Add unit to list of units...
                 */

                closeUnits.add(campData, cpiEnemy, routeDistance, armies->getUnitSPCount(cpiEnemy));

            }
            else
            {
               if(routeDistance < closestEnemyDist)
               {
                 closestEnemyDist = routeDistance;
                 closestEnemy = cpiEnemy;
               }
            }

#ifdef DEBUG_DISTANCE
            cuLog.printf("Actual distance between %s and %s is < %ld (%ld miles)",
                        (const char*) getUnitName(cpi).toStr(),
                        (const char*) getUnitName(cpiEnemy).toStr(),
                        (long) routeDistance,
                        (long) DistanceToMile(routeDistance));
#endif
         }

         /*
          * If no units in range, add closest found
          */

         if(closeUnits.getClosest() == NoCommandPosition)
         {
           ASSERT(closestEnemy != NoCommandPosition);
           CommandPosition* cEnemy = armies->getCommand(closestEnemy);
           closeUnits.add(campData, closestEnemy, closestEnemyDist, armies->getUnitSPValue(closestEnemy));
         }
      }
   }

}


static void CampaignCombatUtility::findCloseFriendlyUnits(CampaignData* campData, ICommandPosition cpi, CloseUnits& closeUnits, Distance findDistance)
{
   ASSERT(closeUnits.currentUnit == NoCommandPosition);
   closeUnits.reset();

#ifdef DEBUG
   closeUnits.currentUnit = cpi;
#endif

   // Armies* armies = combData->d_armies;
   Armies* armies = &campData->getArmies();

   CommandPosition* cp = armies->getCommand(cpi);
   Side side = cp->getSide();

   Location hLocation;
   Location tLocation;
   cp->getHeadLocation(hLocation);
   cp->getTailLocation(tLocation);

   /*
    * Iterate through top level of friendly side(s)
    */

   NationIter nIter(armies);
   while(++nIter)
   {
      Side friendlySide = nIter.current();

      if(!scenario->isEnemy(side, friendlySide))
      {
         ICommandPosition topFriend = armies->getFirstUnit(friendlySide);


         UnitIter uIter(armies, topFriend);
         while(uIter.sister())
         {
            ICommandPosition cpiFriend = uIter.current();
            CommandPosition* cpFriend = armies->getCommand(cpiFriend);

            if(cpi != cpiFriend && !cpFriend->isGarrison())
            {
               /*
                * First do a quick check for physical proximity
                */

               Location ehLocation;
               Location etLocation;
               cpFriend->getHeadLocation(ehLocation);
               cpFriend->getTailLocation(etLocation);

               if( (distanceLocation(hLocation, ehLocation) < findDistance) ||
                  (distanceLocation(hLocation, etLocation) <  findDistance) ||
                  (distanceLocation(tLocation, ehLocation) <  findDistance) ||
                  (distanceLocation(tLocation, etLocation) <  findDistance) )
               {
#ifdef DEBUG_DISTANCE
                  cuLog.printf("Physical distance between %s and %s is < %ld",
                     (const char*) getUnitName(cpi).toStr(),
                     (const char*) getUnitName(cpiFriend).toStr(),
                     (long) findDistance);
#endif
                  /*
                   * Now for the complicated task of finding the actual route
                   * distance.
                   */

                  Distance routeDistance = 0;
                  static CampaignRoute route(campData);
                  if(route.getRouteDistance(cp->location(), cpFriend->location(), findDistance, routeDistance))
                  {
#ifdef DEBUG_DISTANCE
                     cuLog.printf("Actual distance between %s and %s is < %ld (%ld miles)",
                        (const char*) getUnitName(cpi).toStr(),
                        (const char*) getUnitName(cpiFriend).toStr(),
                        (long) routeDistance,
                        (long) DistanceToMile(routeDistance));
#endif

                     /*
                      * Add unit to list of units...
                      */

                     // closeUnits.add(cpiFriend, routeDistance, armies->getUnitSPCount(cpiFriend, True, False));
                     closeUnits.add(campData, cpiFriend, routeDistance, armies->getUnitSPCount(cpiFriend, True));

                  }
               }
            }
         }
      }
   }

}
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
