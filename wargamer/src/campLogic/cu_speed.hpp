/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CU_SPEED_HPP
#define CU_SPEED_HPP

#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "measure.hpp"

class CampaignData;
struct CampUnitMoveData;

class StrategicSpeed {
  public:
	 static Speed getStrategicSpeed(ICommandPosition cpi, IConnection iCon, CampUnitMoveData* moveData);
};

#endif
