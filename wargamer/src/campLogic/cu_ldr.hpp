/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CU_LDR_HPP
#define CU_LDR_HPP

class CampaignLogicOwner;

class LeaderProc {
public:
  static void processHourly(CampaignLogicOwner* d_campData);
  static void processDaily(CampaignLogicOwner* d_campData);
};


#endif
