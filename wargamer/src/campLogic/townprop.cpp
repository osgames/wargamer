/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Started by Paul Sample.
 *----------------------------------------------------------------------
 *
 * Town Resource Processing
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "townprop.hpp"
#include "town.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "armies.hpp"      // for createStrengthPoint
#include "campmsg.hpp"
#include "scenario.hpp"
#include "armyutil.hpp"
#include "terrain.hpp"
#include "wg_rand.hpp"
#include "campmsg.hpp"
#if !defined(EDITOR)
#include "defect.hpp"
#include "campside.hpp"
#endif  // !defined(EDITOR)

#ifdef DEBUG
#include "todolog.hpp"
#include "options.hpp"

#include "clog.hpp"
LogFile rLog("resource.log");

#if !defined(EDITOR)
LogFileFlush toLog("TownOrder.log");
#endif  // !defined(EDITOR)

// Until I can get in to add some new debug options
#define OPT_InstantBuild OPT_InstantMove

#endif

/*
 * Local functions:
 */

namespace   // private namespace
{


#ifdef DEBUG

// for debugging

const char* getReasonsNotAdded(Boolean noControl, Boolean isSieged, Boolean isRaided)
{
   ASSERT(noControl == True || isSieged == True || isRaided == True);

   static char reasons[200];

   lstrcpy(reasons, "");

   if(noControl)
      lstrcat(reasons, "Side does not control Province ");
   if(isSieged)
      lstrcat(reasons, "Town is sieged ");
   if(isRaided)
      lstrcat(reasons, "Town is raided");

   return reasons;
}

#endif

void countResources(const CampaignData* campData, IProvince p, ResourceSet& result)
{
   const Province& province = campData->getProvince(p);
   const TownList& townList = campData->getTowns();

   UWORD nTowns = province.getNTowns();
   ITown stop = ITown(province.getTown() + nTowns);

   /*
   * Collect new resources from towns in province
   */

   ULONG resources = 0;
   ULONG manPower  = 0;

   if(province.getSide() != SIDE_Neutral)
   {
      for(ITown tIndex = province.getTown(); tIndex < stop; tIndex++)
      {
         const Town& town = townList[tIndex]; // getTown(tIndex);     // ... towns[tIndex];

         /*
         * Only towns controlled by same side as province should be added
         * unless town is a capitol. If town is not controlled by its
         * start side check to see if manpower is tranferable before
         * adding it up.
         * Towns that are being raided or sieged to not contribute
         */

         if(((town.getSide() == province.getSide()) || (town.getIsCapital())) &&
            (!town.isSieged() && !town.isRaided()))
         {
            if((province.getSide() == province.getStartSide()) || town.getMpTransferable())
            {
               manPower += town.getManPowerRate();
               resources += town.getResourceRate();
            }
#ifdef DEBUG
            else
            {
               rLog.printf("%s's manpower not transferable", town.getNameNotNull());
               rLog.printf("Province start side is %s, current side is %s",
                  scenario->getSideName(province.getStartSide()),
                  scenario->getSideName(province.getSide()));
            }
#endif

         }
#ifdef DEBUG
         else
         {
            rLog.printf("%s's(%s) resources not added because:", town.getNameNotNull(),
               scenario->getSideName(town.getSide()));
            rLog.printf("%s", getReasonsNotAdded(town.getSide() != province.getSide(),
               town.isSieged(), town.isRaided()));
         }
#endif
      }
   }
#ifdef DEBUG
   else
      rLog.printf("%s is neutral. No resources added", province.getNameNotNull());
#endif

   // clip values
   result.manPower = clipValue<AttributePoints>(manPower, 0, AttributePoint_Range);
   result.resource = clipValue<AttributePoints>(resources, 0, AttributePoint_Range);
}

void needResources(const CampaignData* campData, const BuildItem* builds, ResourceSet& result)
{
   result.manPower = 0;
   result.resource = 0;

   for(int i = 0; i < BasicUnitType::HowMany; i++)
   {
      const BuildItem& b = builds[i];

      if(b.isEnabled())
      {
         /*
         * Get Weekly resource/manpower cost for given unit type
         * This is the maximum we can spend this week on this type(*quantity)
         */

         ASSERT(b.getWhat() < campData->getMaxUnitType());
         const UnitTypeItem& ut = campData->getUnitType(b.getWhat());

         AttributePoints manpower = 0;
         AttributePoints resource = 0;
         ut.getWeeklyResource(manpower, resource);

         result.manPower += (manpower * b.getQuantity());
         result.resource += (resource * b.getQuantity());
      }
   }
}



#if !defined(EDITOR)


/*
 * Local Functions defined within a class to keep them tidy
 * and keep shared data handy
 */

class TownProcessUtil {
   CampaignLogicOwner* d_campGame;
   CampaignData* d_campData;
   ProvinceList& d_provinces;
   TownList& d_towns;
   ResourceSet d_resources;
   static Date s_date;
   Boolean d_havingCallup;
   public:
   TownProcessUtil(CampaignLogicOwner* campgame, CampaignData* campData);

   void processProvince(IProvince pIndex);

   void collectResources(IProvince pIndex);
   void spendResources(IProvince pIndex);
   // void needResources(const BuildItem* builds);

   const ResourceSet& getResources()
   { return d_resources;
   }
   void flagNextCallup()
   { d_havingCallup = True;
   }
   Boolean havingCallup() const
   { return d_havingCallup;
   }
   void getNextCallup();
   private:
   void doPolitical(IProvince pIndex);

   void buildItem(Province& p, BuildItem& b);
   // void createStrengthPoint(ITown itown, UnitType what);
   Boolean buildAnother(BuildItem& item);
};


Date TownProcessUtil::s_date(1, DateDefinitions::June, 1813);


TownProcessUtil::TownProcessUtil(CampaignLogicOwner* campGame, CampaignData* campData) :
   d_campGame(campGame),
   d_campData(campData),
   d_provinces(campData->getProvinces()),
   d_towns(campData->getTowns())
{
}


/*
 * Process one province for one week
 */

void TownProcessUtil::processProvince(IProvince pIndex)
{
   doPolitical(pIndex);
   collectResources(pIndex);
   spendResources(pIndex);
}



/*
 * Collect new resources from towns in province
 */

void TownProcessUtil::collectResources(IProvince pIndex)
{
   countResources(d_campData, pIndex, d_resources);

   ULONG manPower = d_resources.manPower;
   ULONG resources = d_resources.resource;

   Province& province = d_provinces[pIndex];

   if(manPower > 0 || resources > 0)
   {
      /*
      * If this is the semi-annual callup, the
      * manpower and resources are multiplied
      */

      const Date& d = d_campData->getDate();
      if(s_date < d)
      {
         // TODO: get this from the scenario tables
         static const UBYTE s_table[] = {
            3, 5, 2, 3, 2, 2, 3, 2, 2, 2, 1, 3, 3, 1
         };

         const UBYTE mult = s_table[province.getNationality()];
#ifdef DEBUG
         rLog.printf("Semi-Annual callup. Manpower / Resources X %d",
            static_cast<int>(mult));
#endif

         manPower *= mult;
         resources *= mult;

         /*
         * reset static date 6 months
         */

         if(!havingCallup())
            flagNextCallup();
      }

      // adjust manpower for national morale
      Nationality mn = (scenario->getNationType(province.getNationality()) == MajorNation) ?
         province.getNationality() : scenario->getDefaultNation(province.getSide());

      Attribute nMorale = d_campData->getNationMorale(mn);

      // if it is greater than 200, manpower * 20%
      if(nMorale >= 220)
         manPower *= 1.3;
      else if(nMorale >= 180)
         manPower *= 1.1;
      else if(nMorale < 50)
         manPower *= .6;
      else if(nMorale < 100)
         manPower *= .8;

      // clip values
      d_resources.manPower = clipValue<AttributePoints>(manPower, 0, AttributePoint_Range);
      d_resources.resource = clipValue<AttributePoints>(resources, 0, AttributePoint_Range);
      province.addManPower(d_resources.manPower);
      province.addResource(d_resources.resource);
   }
#ifdef DEBUG
   rLog.printf("Province %s: total manPower %ld manPower added %ld",
      province.getName(),
      province.getManPower(),
      manPower);
   rLog.printf("Total resource %ld resource added %ld",
      province.getResource(),
      resources);
#endif
}

void TownProcessUtil::getNextCallup()
{
   // next callup is in 6 months
   Month month = s_date.month;
   Year year = s_date.year;

   const UBYTE callUpIn = 6;

   if(month + callUpIn < 12)
      month = static_cast<Month>(month + callUpIn);
   else
   {
      month = static_cast<Month>(month - callUpIn);
      year++;
   }

   s_date.set(1, month, year);
   d_havingCallup = False;
}

void TownProcessUtil::doPolitical(IProvince iProv)
{
   ASSERT(iProv != NoProvince);
   Province& province = d_campData->getProvince(iProv);

#ifdef DEBUG
   rLog.printf("======= Processing %s ===========", province.getName());
#endif

   /*
   *  Add up political and victory points and see who controls province
   *
   *  If side has > 50% of political points and owns capital it controls
   *  the province.
   *
   *  If side owns capital and not 50% of political points province is neutral
   *
   */

   ULONG political1 = 0;
   ULONG political2 = 0;
   ULONG totalPolitical = 0;

   ULONG victory1 = 0;
   ULONG victory2 = 0;

   Side side1 = 0;
   Side side2 = 1;
   Side whoHasCapital = SIDE_Neutral;

   bool setVictory = (d_campData->getVictoryLevel(0) == -1);
   ITown stop = ITown(province.getTown() + province.getNTowns());

   for(ITown tIndex = province.getTown(); tIndex < stop; tIndex++)
   {
      const Town& town = d_towns[tIndex]; // getTown(tIndex);     // ... towns[tIndex];

      if(town.getSide() != SIDE_Neutral)
      {

         /*
         *  If capital who has it?
         */

         if(town.getIsCapital())
         {
            whoHasCapital = town.getSide();
         }

         /*
         *  Add up political & victory points depending on side
         */

         if(town.getSide() == side1)
         {
            political1 += town.getPolitical();
            victory1 += town.getVictory(side1);
         }
         else if(town.getSide() == side2)
         {
            political2 += town.getPolitical();
            victory2 += town.getVictory(side2);
         }
#ifdef DEBUG
         else
            FORCEASSERT("Side not found");
#endif
      }

      totalPolitical += town.getPolitical();
   }

   /*
   *  Determine exactly who owns Province
   */

   Side whoHasProvince = SIDE_Neutral;

   if(political1 > totalPolitical / 2)
   {
      if(side1 == whoHasCapital)
         whoHasProvince = side1;
   }
   else if(political2 > totalPolitical / 2)
   {
      if(side2 == whoHasCapital)
         whoHasProvince = side2;
   }

#ifdef DEBUG
   if(whoHasProvince != SIDE_Neutral)
      ASSERT(whoHasProvince == whoHasCapital);
#endif

   /*
   * If province is a minor  nation and
   * has changed sides, eliminate all units of that nation
   *
   * If province is a generic nation and has changed sides
   * generic nation becomes generic nation of the other side
   */

   if(whoHasProvince != SIDE_Neutral && whoHasProvince != province.getSide())
   {

      Nationality n = province.getNationality();

      NationType nType = scenario->getNationType(n);

      /*
      * Eliminate units
      */

      if(nType == MinorNation)
      {
#ifdef DEBUG
         rLog.printf("%s has switched sides(%s to %s)",
            scenario->getNationName(n),
            scenario->getSideName(whoHasProvince),
            scenario->getSideName(province.getSide()));
#endif
         CampaignArmy_Util::eliminateNationUnits(d_campData, n, province.getSide());
      }

      /*
      * If generic switch generic type
      */

      else if(nType == GenericNation)
      {
         Nationality newNation = scenario->getGenericNation(whoHasProvince);
         province.setNationality(newNation);
#ifdef DEBUG
         rLog.printf("switching from %s to %s",
            scenario->getNationName(n),
            scenario->getNationName(newNation));
#endif
      }

   }
   province.setSide(whoHasProvince);


   /*
   * Add victory points to total.
   */

   if(setVictory)
   {
      d_campData->addVictoryLevel(victory1, side1);
      d_campData->addVictoryLevel(victory2, side2);
   }

#ifdef DEBUG
   rLog.printf("Total political points for %s are %d", province.getNameNotNull(), (int)totalPolitical);
   rLog.printf("%s has %d political and %d victory, %s has %d political and %d victory",
      scenario->getSideName(side1), (int)political1, (int)victory1,
      scenario->getSideName(side2), (int)political2, (int)victory2);
   rLog.printf("%s controls Province. %s controls Capital",
      scenario->getSideName(whoHasProvince), scenario->getSideName(whoHasCapital));
   rLog.printf("Total victory points for %s is %ld, for %s is %ld",
      scenario->getSideName(side1), d_campData->getVictoryLevel(side1),
      scenario->getSideName(side2), d_campData->getVictoryLevel(side2));
#endif
}

/*
 * Simple sorted array class
 */

template<class T, int n>
class SortArray {
   T items[n];
   int nextPut;
   int nextGet;
   public:
   SortArray()
   { nextPut = 0; nextGet = 0;
   }

   void add(T item)
   {
      ASSERT(nextPut < n);

      for(int i = 0; i < nextPut; i++)
      {
         if(item.compare(items[i]))
            break;
      }

      for(int j = nextPut; j > i; j--)
         items[j] = items[j-1];

      items[i] = item;
      nextPut++;
   }

   /*
   * Simple iterator
   */

   Boolean next()
   {
      if(nextGet < nextPut)
         return True;
      else
         return False;
   }

   T& current()
   {
      ASSERT(nextGet < nextPut);
      return items[nextGet++];
   }


};

/*---------------------------------------------------------------
 * Use resources for building things, or
 * if countOnly is true it counts resources being used(for display info)
 */

void TownProcessUtil::spendResources(IProvince pIndex)
{
   Province& p = d_provinces[pIndex];

#ifdef DEBUG
   rLog.printf("spending resource for %s", p.getName());
#endif

   // ULONG totalMP = 0;      // Sum of ManPower * priority
   // ULONG totalRP = 0;      // Sum of Resource * priority

   /*
   * Resources are split equally amongst all items being built
   * First we need to count total number of items being built
   */

   int nItems = 0;
   for(int i = 0; i < BasicUnitType::HowMany; i++)
   {
      const BuildItem& b = p.getBuild(i);
      nItems += b.getQuantity();
   }

#ifdef DEBUG
   rLog.printf("%s is building %d SP", p.getName(), nItems);
#endif

   /*
   * Now allocate the resources accordingly
   */

   d_resources.manPower = p.getManPower();
   d_resources.resource = p.getResource();

   for(i = 0; i < BasicUnitType::HowMany; i++)
   {
      if(nItems > 0)
      {
         /*
         *  Each items share
         */

         const int shareOfManpower =  d_resources.manPower / nItems;
         const int shareOfResources = d_resources.resource / nItems;

         BuildItem& b = p.getBuild(i);

         if(b.isEnabled())
         {
            /*
            * Get Weekly resource/manpower cost for given unit type
            * This is the maximum we can spend this week on this type(*quantity)
            */

            ASSERT(b.getWhat() < d_campData->getMaxUnitType());
            const UnitTypeItem& ut = d_campData->getUnitType(b.getWhat());

            AttributePoints manpower = 0;
            AttributePoints resource = 0;
            ut.getWeeklyResource(manpower, resource);

#ifdef DEBUG
            rLog.printf("Building  %d %s, have %d mp, and %d res",
               b.getQuantity(), ut.getName(),
               d_resources.manPower, d_resources.resource);
            rLog.printf("costing %d mp. and %d res. weekly",
               manpower, resource);
            rLog.printf("can spend %d mp. and %d res. this week",
               shareOfManpower, shareOfResources);
#endif

            manpower = minimum(manpower * b.getQuantity(), shareOfManpower * b.getQuantity());
            resource = minimum(resource * b.getQuantity(), shareOfResources * b.getQuantity());

#ifdef DEBUG
            rLog.printf("Actual manpower spent = %d, resources = %d",
               manpower, resource);
#endif

            /*
            * Subtract this from total resources
            */

            d_resources.manPower = maximum(0, d_resources.manPower - manpower);
            d_resources.resource = maximum(0, d_resources.resource - resource);

            /*
            * Now subtract from our build and see if its done
            */

            if(b.reduceNeeded(manpower, resource))
            {
#ifdef DEBUG
               rLog.printf("%d %s built", b.getQuantity(), ut.getName());
#endif
               // Build item!
               buildItem(p, b);
            }

            nItems = maximum(0, nItems - b.getQuantity());
         }
      }
      else
         break;
   }

#ifdef DEBUG
   rLog.printf("mp remaining %d, res remaining %d", d_resources.manPower, d_resources.resource);
#endif
   p.setManPower(d_resources.manPower);
   p.setResource(d_resources.resource);

}

void TownProcessUtil::buildItem(Province& p, BuildItem& b)
{
   UnitType what = b.getWhat();

#ifdef DEBUG
   const UnitTypeTable& unitTypes = d_campData->getUnitTypes();
   debugLog("buildItem(%s)", unitTypes[what].getName());
#endif

   ITown itown = p.getCapital();

   for(int i = 0; i < b.getQuantity(); i++)
      CampaignArmy_Util::buildStrengthPoint(d_campGame, itown, b);

   CampaignMessageInfo msgInfo(p.getSide(), CampaignMessages::NewUnitBuilt);
   msgInfo.where(itown);
   msgInfo.what(what);
   d_campGame->sendPlayerMessage(msgInfo);

   buildAnother(b);
}

Boolean TownProcessUtil::buildAnother(BuildItem& item)
{
   if(item.getAuto())   // || --quantity)
   {
      const UnitTypeItem& ut = d_campData->getUnitType(item.getWhat());
      AttributePoints manTotal;
      AttributePoints resTotal;
      ut.getTotalResource(manTotal, resTotal);
      item.setResources(manTotal, resTotal);

      return True;
   }
   else
   {
      //    item.setEnable(False);
      item.setQuantity(0);
      return False;
   }
}



/*--------------------------------------------------------------------
 * A base class for Town Order Implementation
 */

class RealTownOrder {
   public:
   RealTownOrder()
   {
   }

   void runBridgeOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order);
   virtual void runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order) = 0;
   virtual void stopOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order) = 0;
};

// TODO: get tables from scenario
enum {
   StaffOver170,
   HasEngineers,
   HasInfantry,
   HasBridgeTrain,
   HasOldGuard,
   SP6orLess,
   SupplyLessThan50,
   StaffLessThan90,
   CavalryOnly,
   EnemyOwnedLoc,
   EnemyWithin8,

   Modifiers_HowMany
};

static const int s_mTable[Modifiers_HowMany] = {
   10, 10, 5, 10, 5, -10, -15, -10, -15, -15, -10
};

static const UBYTE s_nTable[] = {
   40, 25, 30,  30,  25, 20, 20, 20, 20, 20, 20, 20, 20, 20
};

void RealTownOrder::runBridgeOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order)
{
   CampaignData* campData = campGame->campaignData();
   Town& t = campData->getTown(order->d_town);

#ifdef DEBUG
   const char* text = (order->d_type == Town_COrder::BlowBridge) ?
      "blow bridge" : "repair bridge";

   toLog.printf("-- Trying to %s", text);
#endif

#ifdef DEBUG
   const TerrainTypeItem& ti = campData->getTerrainType(t.getTerrain());
   ASSERT(ti.isBridge());
#endif

//   const ICommandPosition& cpi = order->cpi(campData->getArmies());

   /*
   * Special cases...
   */

   // can't repair or blow bridge if we're retreating
   if(cpi->isRetreating())
      return;

   // can't repair or blow bridge if in battle
   if(cpi->isInBattle())
   {
#ifdef DEBUG
      toLog.printf("---- We're in battle, can't %s, will try again in 24 hours", text);
#endif

      order->d_tillWhen = campData->getTick() + HoursToTicks(24);
      return;
   }


   // get raw chance depending on nationality
   UBYTE chance = s_nTable[cpi->getNation()];

#ifdef DEBUG
   toLog.printf("---- Raw chance to %s is %d%%", text, static_cast<int>(chance));
#endif

   // get modified chance
   int modifyBy = 0;

   const ILeader& leader = cpi->getLeader();
   ASSERT(leader != NoLeader);

   // if leader has staff rating of over 170 or less than 90
   if(leader->getStaff(True) > 170)
      modifyBy += s_mTable[StaffOver170];
   else if(leader->getStaff(True) < 90)
      modifyBy += s_mTable[StaffLessThan90];

   // if unit has engineers
   if(campData->getArmies().hasEngineer(cpi))
      modifyBy += s_mTable[HasEngineers];

   // if we have an infantry
   if(campData->getArmies().hasInfantry(cpi))
      modifyBy += s_mTable[HasInfantry];

   // unit has less than 6 SP
   SPCount spCount = campData->getArmies().getUnitSPCount(cpi, True);
   if(spCount <= 6)
      modifyBy += s_mTable[SP6orLess];

   // if unit has less than 50% supply
   if(MulDiv(cpi->getSupply(), 100, Attribute_Range) < 50)
      modifyBy += s_mTable[SupplyLessThan50];

   // for repairing bridge only
   if(order->d_type == Town_COrder::RepairBridge)
   {
      // if we have a bridge train
      if(campData->getArmies().hasBridgeTrain(cpi))
         modifyBy += s_mTable[HasBridgeTrain];

      // if unit is old guard
      if(cpi->isOldGuard())
         modifyBy += s_mTable[HasOldGuard];

      // if unit is cavalry only
      if(cpi->isCavalry() && !cpi->isCombinedArms())
         modifyBy += s_mTable[CavalryOnly];

      // enemy owned location
      if(t.getSide() != cpi->getSide())
         modifyBy += s_mTable[EnemyOwnedLoc];
   }
   else // blowing bridge
   {
      // if enemy is near
      if(cpi->getMode() == CampaignMovement::CPM_AcceptBattle ||
         cpi->getMode() == CampaignMovement::CPM_IntoBattle)
      {
         modifyBy += s_mTable[EnemyWithin8];
      }

      // TODO: modify for Center of Operations
   }

   // adjust chance to no lower than 5
   chance = clipValue(chance + modifyBy, 5, 100);

#ifdef DEBUG
   toLog.printf("---- Modified chance to %s is %d%%", text, static_cast<int>(chance));
#endif

   if(CRandom::get(100) < chance)
   {
#ifdef DEBUG
      toLog.printf("---- Bridge order completed");
#endif

      t.isBridgeUp((order->d_type == Town_COrder::RepairBridge));

      // TODO: We need to send the player a message letting him know
   }
   else
   {
#ifdef DEBUG
      toLog.printf("---- Bridge order not completed, will try again in 24 hours");
#endif

      order->d_tillWhen = campData->getTick() + HoursToTicks(24);
   }

}

// derived order classes
class TO_RepairBridge : public RealTownOrder {
   public:
   TO_RepairBridge()
   {
   }
   void runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order);
   void stopOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order) {}
};

void TO_RepairBridge::runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order)
{
   ASSERT(campGame);
   ASSERT(order);

   runBridgeOrder(campGame, cpi, order);
}


// derived order classes
class TO_BlowBridge : public RealTownOrder {
   public:
   TO_BlowBridge()
   {
   }
   void runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order);
   void stopOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order) {}
};

void TO_BlowBridge::runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order)
{
   ASSERT(campGame);
   ASSERT(order);

   runBridgeOrder(campGame, cpi, order);
}

class TO_BuildDepot : public RealTownOrder {
   public:
   TO_BuildDepot()
   {
   }
   void runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order);
   void stopOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order);
};

void TO_BuildDepot::runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order)
{
   ASSERT(campGame);
   ASSERT(order);

   CampaignData* campData = campGame->campaignData();
   Town& t = campData->getTown(order->d_town);

   if(t.getIsDepot() || t.getIsSupplySource())
       return;

//   const ICommandPosition& cpi = order->cpi(campData->getArmies());
#if 0
      const Table1D<UBYTE>& table = scenario->getSideDepotLimitTable();
   const int nDepots = campData->getSideData().getSideDepotsBuilt(cpi->getSide());
   const int nDepotsUC = campData->getSideData().getSideDepotsUnderConstruction(cpi->getSide());
#endif

   Province& p = campData->getProvince(t.getProvince());
   Nationality n = cpi->getNation(); //p.getNationality();

   const int nDepotsAllowed = campData->getNations().getNDepotsAllowed(n);
   const int nDepotsBuilt = campData->getNations().getNDepotsBuilt(n);
   const int nDepotsUC = campData->getNations().getNDepotsUC(n);


#ifdef DEBUG
   toLog.printf("-- Trying to build Depot in %s. Allowed = %d, Built = %d, Under construction = %d",
      scenario->getNationName(n), nDepotsAllowed, nDepotsBuilt, nDepotsUC);
#endif

   //  campData->getSideData().decSideDepotsUnderConstruction(cpi->getSide());
   campData->getNations().decNDepotsUC(n);

   if((cpi->atTown()) &&
      (cpi->getSide() == t.getSide()) &&
      (cpi->getTown() == order->d_town) &&
      (nDepotsBuilt + nDepotsUC < nDepotsAllowed))
   {
      t.setIsDepot(True);
      t.setisBuildingDepot(False);

      // unchecked ammended by Paul - 12/9/99
      t.setSupplyInstaller(NoCommandPosition);
      // end unchecked ammended by Paul - 12/9/99

      //    campData->getSideData().incSideDepotBuilt(t.getSide());
      campData->getNations().incNDepotsBuilt(n);

      /*
      * Tell the player
      */

      CampaignMessageInfo msgInfo(t.getSide(), CampaignMessages::SupplyDepotInstalled);
      msgInfo.cp(cpi);
      msgInfo.where(cpi->getTown());
      msgInfo.value1(t.getFortifications());
      campGame->sendPlayerMessage(msgInfo);

#ifdef DEBUG
      toLog.printf("------ Depot Built");
#endif
   }
}

void TO_BuildDepot::stopOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order)
{
   Nationality n = cpi->getNation(); //p.getNationality();
   CampaignData* campData = campGame->campaignData();
   Town& t = campData->getTown(order->d_town);
   t.setisBuildingDepot(False);
   campData->getNations().incNDepotsBuilt(n);
   // unchecked ammended by Paul - 12/9/99
   t.setSupplyInstaller(NoCommandPosition);
   // end unchecked ammended by Paul - 12/9/99
}

class TO_RemoveDepot : public RealTownOrder {
   public:
   TO_RemoveDepot()
   {
   }
   void runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order);
   void stopOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order)
   {
      // unchecked ammended by Paul - 12/9/99
      Town& t = campGame->campaignData()->getTown(order->d_town);
      t.setSupplyInstaller(NoCommandPosition);
      // end unchecked ammended by Paul - 12/9/99

   }
};

void TO_RemoveDepot::runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order)
{
   ASSERT(campGame);
//   ASSERT(town != NoTown);
   ASSERT(order);

   CampaignData* campData = campGame->campaignData();
   Town& t = campData->getTown(order->d_town);
   ASSERT(t.getIsDepot());

#ifdef DEBUG
   toLog.printf("-- Trying to remove Depot");
#endif

//   const ICommandPosition& cpi = order->cpi(campData->getArmies());
   if((cpi->atTown()))//&&
   {

      Province& p = campData->getProvince(t.getProvince());
      Nationality n = cpi->getNation(); //p.getNationality();

      if((cpi->getSide() == t.getSide()) &&
         (cpi->getTown() == order->d_town) &&
         (campData->getNations().getNDepotsBuilt(n) > 0))
      {
         t.setIsDepot(False);
         //      campData->getSideData().decSideDepotsBuilt(cpi->getSide());
         campData->getNations().decNDepotsBuilt(n);
         // unchecked ammended by Paul - 12/9/99
         t.setSupplyInstaller(NoCommandPosition);
         // end unchecked ammended by Paul - 12/9/99

#if 0

            /*
         * Tell the player
         */

            CampaignMessageInfo msgInfo(t.getSide(), CampaignMessages::SupplyDepotInstalled);
         msgInfo.cp(cpi);
         msgInfo.where(cpi->getTown());
         msgInfo.value1(t.getFortifications());
         campGame->sendPlayerMessage(msgInfo);
#endif

#ifdef DEBUG
         toLog.printf("------ Depot Removed");
#endif
      }
   }
}

class TO_BuildFort : public RealTownOrder {
   public:
   TO_BuildFort()
   {
   }
   void runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order);
   void stopOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order);
};

void TO_BuildFort::runOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order)
{
   ASSERT(campGame);
//   ASSERT(town != NoTown);
   ASSERT(order);

   CampaignData* campData = campGame->campaignData();
   Town& t = campData->getTown(order->d_town);
   ASSERT(t.getFortifications() < Town::MaxFortifications);

#ifdef DEBUG
   toLog.printf("-- Trying to build Fort");
#endif

//   const ICommandPosition& cpi = order->cpi(campData->getArmies());
   if((cpi->atTown()) &&
      (cpi->getSide() == t.getSide()) &&
      (cpi->getTown() == order->d_town))
   {
      t.setFortifications(t.getFortifications() + 1);
      t.setisBuildingFort(False);

      // unchecked ammended by Paul - 12/9/99
      t.setFortUpgrader(NoCommandPosition);
      // end unchecked ammended by Paul - 12/9/99

      /*
      * Tell the player
      */

      CampaignMessageInfo msgInfo(t.getSide(), CampaignMessages::FortUpgraded);
      msgInfo.cp(cpi);
      msgInfo.where(cpi->getTown());
      msgInfo.value1(t.getFortifications());
      campGame->sendPlayerMessage(msgInfo);

#ifdef DEBUG
      toLog.printf("------ Fort Built");
#endif
   }
}

void TO_BuildFort::stopOrder(CampaignLogicOwner* campGame, ICommandPosition cpi, Town_COrder* order)
{
   CampaignData* campData = campGame->campaignData();
   Town& t = campData->getTown(order->d_town);
   t.setisBuildingFort(False);
   // unchecked ammended by Paul - 12/9/99
   t.setFortUpgrader(NoCommandPosition);
   // end unchecked ammended by Paul - 12/9/99
}

/*
 * Static instances of the above classes
 */

static TO_BlowBridge   s_blowBridge;
static TO_RepairBridge s_repairBridge;
static TO_BuildDepot   s_buildDepot;
static TO_BuildFort    s_buildFort;
static TO_RemoveDepot  s_removeDepot;

static RealTownOrder* s_orders[ /* Town_COrder::TO_HowMany */ ] = {
  &s_repairBridge,
  &s_blowBridge,
  &s_buildDepot,
  &s_buildFort,
  &s_removeDepot
};

StaticAssert(sizeof(s_orders) == (sizeof(RealTownOrder*) * Town_COrder::TO_HowMany));

#endif  // !defined(EDITOR)

};    // end private namespace

/*===================================================================
 * Public Functions:
 */

#if !defined(EDITOR)

/*-------------------------------------------------------------------
 * Process Functions
 */

/*
 * Process Town Resources
 *
 * TODO:
 *    nextDay should be stored as part of the game data so that
 *    a saved game will update properly.
 */

void TownProcess::weeklyResources(CampaignLogicOwner* campGame, CampaignData* campData)
{
#ifdef DEBUG
   rLog.printf("+Processing Town Resources at: %s", campData->asciiTime());
#endif

   TownProcessUtil util(campGame, campData);

   ProvinceList& provinces = campData->getProvinces();

   //  campData->resetVictory();

   {
      Writer lock(&provinces);

      ArrayIndex nProvs = provinces.entries();

      for(IProvince pIndex = 0; pIndex < nProvs; pIndex++)
         util.processProvince(pIndex);
   }

   if(util.havingCallup())
      util.getNextCallup();

#ifdef DEBUG
   rLog.printf("Total Victory Points for %s is %d, for %s is %d",
      scenario->getSideName(0), campData->getVictoryLevel(0),
      scenario->getSideName(1), campData->getVictoryLevel(1));
#endif

#ifdef DEBUG
   rLog.printf("-Processed Town Resources");
#endif

   NationDefectProc::process(campGame);

#ifdef DEBUG
   rLog.close();
#endif
}


void TownProcess::dailyTownOrders(CampaignLogicOwner* campGame)
{
   // RWLock lock;
   // lock.startWrite();

   CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
   toLog.printf("Processing Town Orders at: %s", campData->asciiTime());
#endif

   TimeTick theTime = campData->getTick();
   CPIter iter(&campData->getArmies());
   while(iter.sister())
   {
      ICommandPosition cpi = iter.current();
      if(cpi->townOrders().entries())
      {
         Town_COrderListIter tIter(&cpi->townOrders());
         while(++tIter)
         {
            if(processTownOrder(campGame, cpi, tIter.current(), theTime))
               tIter.remove();
         }
      }
   }

#if 0
   for(ITown i = 0; i < campData->getTowns().entries(); i++)
   {
      Town& t = campData->getTown(i);
      Town_COrderList& ol = t.townOrders();

      SListIter<Town_COrder> iter(&ol);
      while(++iter)
      {
         if(processTownOrder(campGame, i, iter.current(), theTime))
            iter.remove();
      }
   }
#endif
   // lock.endWrite();
}

Boolean TownProcess::processTownOrder(CampaignLogicOwner* campGame, ICommandPosition cpi,
   Town_COrder* to, TimeTick theTime)
{
   ASSERT(to);

   // end order if no longer at town
   if(!cpi->atTown() || cpi->getTown() != to->d_town)
   {
      s_orders[to->d_type]->stopOrder(campGame, cpi, to);
      return True;
   }

#ifdef DEBUG
   Town& t = campGame->campaignData()->getTown(to->d_town);
   toLog.printf("Running Order for %s at %s", cpi->getName(), t.getNameNotNull());
   toLog.printf("----------------------------------");
#endif

   if(theTime >= to->d_tillWhen)
   {
      ASSERT(to->d_type < Town_COrder::TO_HowMany);
      s_orders[to->d_type]->runOrder(campGame, cpi, to);
   }

   return (theTime >= to->d_tillWhen);
}

#endif  // !defined(EDITOR)

/*-------------------------------------------------------------------
 * Information Functions
 */

void TownProcess::resourcesWeeklyRate(const CampaignData* campData, IProvince iProv, ResourceSet& totalResources)
{
   countResources(campData, iProv, totalResources);
}

void TownProcess::resourcesWeeklyNeed(const CampaignData* campData, IProvince iProv, ResourceSet& needResources)
{
   const Province& p = campData->getProvince(iProv);
   resourcesWeeklyNeed(campData, p.builds(), needResources);
}

void TownProcess::resourcesWeeklyNeed(const CampaignData* campData, const BuildItem* builds, ResourceSet& result)
{
   // ConstTownProcessUtil util(campData);

   // util.needResources(builds);
   // needResources.resource = util.getResources().resource;
   // needResources.manPower = util.getResources().manPower;

   needResources(campData, builds, result);
}

void TownProcess::resourcesAvailable(const CampaignData* campData, IProvince iProv, ResourceSet& totalResources, ResourceSet& needResources)
{
   resourcesWeeklyRate(campData, iProv, totalResources);
   resourcesWeeklyNeed(campData, iProv, needResources);
}

void TownProcess::getTotalResources(const CampaignData* campData, IProvince iprov, ResourceSet& result)
{
   const Province& prov = campData->getProvince(iprov);

   result.manPower = 0;
   result.resource = 0;

   ITown stop = static_cast<ITown>(prov.getTown() + prov.getNTowns());
   for(ITown tIndex = prov.getTown(); tIndex < stop; tIndex++)
   {
      const Town& t = campData->getTown(tIndex);

      result.manPower += t.getManPowerRate();
      result.resource += t.getResourceRate();
   }
}

void TownProcess::getAvailableResources(const CampaignData* campData, IProvince iprov, ResourceSet& result)
{
   resourcesWeeklyRate(campData, iprov, result);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
