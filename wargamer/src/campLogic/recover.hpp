/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef RECOVER_HPP
#define RECOVER_HPP

#include "gamedefs.hpp"
#include "cpdef.hpp"

/*
 *  Class for accessing Fatigue loss\recovery
 */

class CampaignData;
class CampaignLogicOwner;
struct CampUnitMoveData;

class FatigueProc {
  public:
	 static void procHourlyFatigue(ICommandPosition cpi, CampaignData* campData);
	 static void procDailyFatigue(CampaignData* campData, ICommandPosition cpi);
	 static void procDailyFatigueLimits(CampaignLogicOwner* ci, ICommandPosition cpi);
		// see if units should go init auto-rest mode. called once a day
	 static void procUnitFatigueLimit(CampaignLogicOwner* ci, ICommandPosition cpi);
		// called by procDailyFatigueLimits or after a winning battle
};

class RecoverMoraleProc {
  public:
	 static void recoverDailyMorale(CampUnitMoveData* campData, ICommandPosition cpi);
	 static void recoverHourlyMorale(CampaignData* campData, ICommandPosition cpi);
};




#endif
