/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Campaign Game Implementation (Mostly contents of old campaign.cpp)
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campimp.hpp"
#include "campdimp.hpp"         // Campaign Data Implementation
#include "campwind.hpp"         // Campaign Windows
#include "campmsg.hpp"
#include "gthread.hpp"
#include "thread.hpp"

#include "scenario.hpp"
#include "gamectrl.hpp"
#include "filename.hpp"
#include "msgbox.hpp"
#include "todolog.hpp"
#include "clogic.hpp"
#include "wldfile.hpp"
#include "filecnk.hpp"

#include "simpstr.hpp"
#include "savegame.hpp"
#include "control.hpp"
#include "campbat.hpp"          // Campaign Battle Game

#include "splash.hpp"

#include "camp_snd.hpp"

#include "s_result.hpp"
#include "siegeint.hpp"
#include "cbatmsg.hpp"
#include "cbatdial.hpp"
#include "cbr_wind.hpp"

#include "registry.hpp"
#include "armies.hpp"
#include "cu_order.hpp"
#include "resstr.hpp"

#include "MultiplayerConnection.hpp"
#include "DirectPlay.hpp"
#include "MultiPlayerMsg.hpp"

#if !defined(EDITOR)
#include "ds_unit.hpp"
#include "ds_town.hpp"
#include "ds_repoSP.hpp"
#include "Ds_reorg.hpp"

#include "despatch.hpp"

#if !defined(NOLOG)
#include "clog.hpp"
LogFile campImpLog("CampImp.log");
#endif

#endif  // !EDITOR

#ifdef DEBUG
#include "wg_msg.hpp"
#include "chckdata.hpp"
#include "logwin.hpp"
#endif   // DEBUG


#ifdef MP_CRC_CHECK
// unsigned int g_masterCRC = 0;
#endif


using Greenius_System::Thread;

/*
 * Campaign Implementation
 */

CampaignImp::CampaignImp(GameOwner* owner) :
    #if !defined(EDITOR)
        CampaignLogicOwner(),
        StartTacticalInterface(),
    #endif
        CampaignInterface(),
        d_owner(owner),
        d_campData(0),
        d_mainWindow(owner->hwnd()),
        d_windows(0),
#if !defined(EDITOR)
        d_logicThread(0),
        d_battle(0),
        d_campaignOver(false),
      // multiplayer time-syncing
      d_slaveReady(false),
      d_lastSyncAcknowledged(true),
      d_lastSyncTime(0),
      d_slaveWantTicks(0),
#endif
        d_needRedraw(False)
{
        ASSERT(d_mainWindow);
}

CampaignImp::~CampaignImp()
{
#if !defined(EDITOR)
        delete d_battle;
        d_battle = 0;

        if(d_logicThread != 0)
        {
                delete d_logicThread;
                d_logicThread = 0;
        }
#endif


        if(d_windows != 0)
        {
                delete d_windows;
                d_windows = 0;
        }

        if(d_campData)
        {
                delete d_campData;
                d_campData = 0;
        }

        Scenario::kill();
}

void CampaignImp::init(const char* fileName, SaveGame::FileFormat mode)
{
//   static const char loadingCampData[] = "Loading Campaign Data...";
//   static const char creatingWindows[] = "Creating Campaign Windows...";
//   static const char drawingMap[]      = "Drawing Map...";

   d_campData = new RealCampaignData(this);
   ASSERT(d_campData != 0);

   ASSERT(fileName != 0);

#if !defined(EDITOR)
   ASSERT(d_logicThread == 0);
   stopLogic();

   d_logicThread = new CampaignLogic(this);
#endif

   SplashScreen splashScreen(d_mainWindow);

   splashScreen.setText(InGameText::get(IDS_LoadingCampData));

   {
          readWorld(fileName, mode);                                      // This creates realCampaignData
          d_campData->clearChanged();
   }

   ASSERT(scenario != 0);

   /*
    * Set up the windows
    */

   splashScreen.setText(InGameText::get(IDS_CreatingCampWind));

   ASSERT(d_windows == 0);
   if(d_windows == 0)
   {
          d_windows = new CampaignWindows(this, d_mainWindow);
          ASSERT(d_windows);
   }

   d_windows->showWindows();

   splashScreen.destroy();

#ifdef CUSTOMIZE
   runDataCheck();
#endif

#if !defined(EDITOR)


   if(CMultiPlayerConnection::connectionType() == ConnectionMaster) {
      #ifdef DEBUG
      campImpLog.printf(">> Starting CampImp multiPlayerRun() loop for Master machine\n\n");
      #endif
   }
   else if(CMultiPlayerConnection::connectionType() == ConnectionSlave) {
      /*
      Tell Master machine that Slave is ready to go
      */
      sendSlaveReady();

      #ifdef DEBUG
      campImpLog.printf(">> Starting CampImp multiPlayerRun() loop for Slave machine\n\n");
      #endif
   }


   startLogic();

   if(d_battle)
   {
   d_windows->suspend(false);
   d_battle->resume();
   }
#endif  // !EDITOR



}



CampaignData* CampaignImp::campaignData()
{
    return d_campData;
}

#ifdef CUSTOMIZE   // Run a data sanity check
void CampaignImp::runDataCheck()
{
  d_windows->runDataSanityCheck();
}
#endif

#if !defined(EDITOR)

// Add SysTicks to the campaign time

void CampaignImp::addTime(SysTick::Value ticks)
{

      /*
      If we are playing multiplayer, we MUST limit the time-step to the max sync interval
      This is to ensure that orders will be properly synchronized on both machines. (d_maxsyncInterval is
      added onto the current_time, to give the activation time for the message on both machines
      */
      if(CMultiPlayerConnection::connectionType() == ConnectionMaster) {
         if(ticks > Despatcher::instance()->maxSyncInterval ) ticks = Despatcher::instance()->maxSyncInterval;
      }

        GameTick gTicks;

        if(d_campData->soundSystem() )
            d_campData->soundSystem()->ProcessSound();

      /*
      If this is the slave machine in a multiplayer game
      Then wantTime will be the last Synctime received from the Master
      */
      if(CMultiPlayerConnection::connectionType() == ConnectionSlave && d_logicThread && d_logicThread->isTimeRunning()) {

         d_syncLock.startRead();

         if(d_slaveWantTicks > 0) {

            CampaignTime wantTime;
            wantTime = d_slaveWantTicks;

            #ifdef DEBUG
            campImpLog.printf("Setting CampaignTime to %i\n", wantTime.toULONG() );
            #endif

            d_logicThread->setTime(wantTime);

         }

         d_syncLock.endRead();

      }


      /*
      If this is the Master machine, then only proceed if we've received a SyncAcknowledgement from the Slave
      If there is no multiplayer connection, then we proceed as normal
      */

      else {

         /*
         No ticks if a) logic isn't creted, b) time isn't running or
         c) this is the Master machine & the last sync wasn't acknowledge yet or the Slave hasn't signalled as being ready yet
         */
         d_syncLock.startRead();
         bool ackn = d_lastSyncAcknowledged;
         d_syncLock.endRead();

         if(!d_logicThread || !d_logicThread->isTimeRunning() ||
            (CMultiPlayerConnection::connectionType() == ConnectionMaster && (!ackn || !d_slaveReady))) {
            ticks = 0;
         }

         else {

            CampaignTime wantTime;

            if(d_battle != 0) {
               /*
                * Update the campaign Time with the battletime
                */

               wantTime = d_campData->getCTime();
               GameDay day = wantTime.getDate();

               #ifdef DEBUG
               campImpLog.printf("Adding %i ticks Campaign-Battle\n", ticks);
               #endif

               GameTick timeOfDay = d_battle->addTime(ticks);          // GameTicks so far today

               if(timeOfDay < wantTime.getTime())
                 ++day;
               wantTime.set(day, timeOfDay);

               #ifdef DEBUG
               campImpLog.printf("Setting CampaignTime to %i\n", wantTime.toULONG() );
               #endif

               d_logicThread->setTime(wantTime);
            }


            else {

               wantTime = d_logicThread->getWantTime();
               if(wantTime == 0) wantTime = d_campData->getCTime();

               // If Logic is more than a tick ahead then skip this
               TimeTick logicTick = gameTicksToTicks(wantTime);
               TimeTick gameTick = d_campData->getTick();

               ASSERT(logicTick >= gameTick);

               if(d_timeControl.isTimeJumping() || ((logicTick - gameTick) < 1)) {

                  wantTime = d_timeControl.addTicks(wantTime, d_campData->getCTime(), ticks);

                  #ifdef DEBUG
                  campImpLog.printf("Setting CampaignTime to %i\n", wantTime.toULONG() );
                  #endif

                  d_logicThread->setTime(wantTime);
               }
            }

            /*
             * Must check that d_logic and d_windows are not 0
             * as it is possible for a WM_TIMER event to occur
             * before these values are set.
             */


            /*
             * (for Master) : send timeSync message to Slave machine
             *
             * NOTE : it may be the case that this sync rate is too frequent for
             * some connections (eg. internet) in which case we'll have to do something
             * else, such as accumulating several wantTimes and then sending a combined
             * sync message for the total time interval...
             */

            if(CMultiPlayerConnection::connectionType() == ConnectionMaster) {
               d_lastSyncTime = wantTime.toULONG();

               d_syncLock.startWrite();
               d_lastSyncAcknowledged = false;
               d_syncLock.endWrite();

#ifdef DEBUG
               campImpLog.printf("[sending sync command to run until %i]\n", wantTime);
#endif

#ifdef MP_CRC_CHECK
               /*
               Send time sync, & include a CRC checksum on game-state
               */
               d_owner->sendTimeSync(
                  wantTime.toULONG(),
                  d_campData->getArmies().calculateChecksum()
               );
#else
               /*
               Send time sync normally
               */
               d_owner->sendTimeSync(
                  wantTime.toULONG(),
                  0
               );
#endif


            }

         }

      }

        /*
         * Redraw Campaign Map
         */

        if(d_windows != 0)
        {
                if(d_needRedraw)
                {
                        d_windows->rebuildMap();
                        d_needRedraw = False;
                }

                d_windows->timeUpdated();
                d_windows->updateInfo();
        }
}





void CampaignImp::sendTimeSync(SysTick::Value ticks, unsigned int checksum) {

   ASSERT(d_owner);
   d_owner->sendTimeSync(ticks, checksum);
}


void CampaignImp::syncSlave(SysTick::Value ticks) {

#ifdef DEBUG
   campImpLog.printf("[received sync command to run until %i]\n", ticks);
#endif

   d_syncLock.startWrite();
   d_slaveWantTicks = ticks;
   d_syncLock.endWrite();
}

void CampaignImp::timeSyncAcknowledged(SysTick::Value ticks) {

   // let it be known that Mr.Cock-Up isn't coming round for tea
   if(ticks != d_lastSyncTime) {
      FORCEASSERT("Major Bad-Mojo Error :\nTime Sync Acknowledgement recieved which mismatches with the last time-sync sent\n");
   }

#ifdef DEBUG
   campImpLog.printf("[received sync acknowledgement for %i]\n", ticks);
#endif

   d_syncLock.startWrite();
   d_lastSyncAcknowledged = true;
   d_syncLock.endWrite();

}

void CampaignImp::slaveReady(void) {

#ifdef DEBUG
   campImpLog.printf("[received SlaveReady message]\n");
#endif

   d_slaveReady = true;
}


void CampaignImp::sendSlaveReady(void) {

#ifdef DEBUG
   campImpLog.printf("[sending SlaveReady message]\n");
#endif

   ASSERT(d_owner);
   d_owner->sendSlaveReady();
}


void CampaignImp::processBattleOrder(MP_MSG_BATTLEORDER * msg)
{
   if(d_battle)
      d_battle->processBattleOrder(msg);
   else
      FORCEASSERT("Multiplayer Battle Message sent to Campaign Game!!!");
}

void CampaignImp::processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg)
{
   switch(msg->order_type) {

      case CMP_MSG_UNIT : {

         DS_UnitOrder * order = new DS_UnitOrder;
         order->unpack(msg, d_campData);

         Despatcher::slaveSend(order, msg->activation_time);
         break;
      }

      case CMP_MSG_TOWN : {

         DS_TownOrder * order = new DS_TownOrder;
         order->unpack(msg, d_campData);

         Despatcher::slaveSend(order, msg->activation_time);
         break;
      }

      case CMP_MSG_REORGANIZE : {

         DS_Reorg::DS_Reorganize * order = new DS_Reorg::DS_Reorganize;
         order->unpack(msg, d_campData);

         Despatcher::slaveSend(order, msg->activation_time);
         break;
      }

      case CMP_MSG_REPOSP : {

         RepoDespatch * order = new RepoDespatch;
         order->unpack(msg, d_campData);

         Despatcher::slaveSend(order, msg->activation_time);
         break;
      }

      default : {
         FORCEASSERT("ERROR - invalid campaign order type from remote machine\n");
         break;
      }
   }

}


#endif          // EDITOR

void CampaignImp::repaintMap()
{
  d_windows->redrawMap();
}

// Save the Campaign Game to a file

bool CampaignImp::save(SaveGame::FileFormat ff, bool prompt) const
{
         GameControl::Pauser pause;    // pauses game until destructed

       // pause
       if(CMultiPlayerConnection::connectionType() != ConnectionNone) CMultiPlayerConnection::dialogPause(true);

#ifdef CUSTOMIZE
        d_windows->runDataSanityCheck();
#endif

        SimpleString fileName;

        bool result = SaveGame::askSaveName(fileName, ff);

        if(result)
        {
         /*
         If we're the Master in a multiplayer game, then send the SAVEGAME prompt to the Slave
         */
         if(CMultiPlayerConnection::connectionType() == ConnectionMaster) {

            MP_MSG_SAVEGAME savegame_msg;
            savegame_msg.msg_type = MP_MSG_SaveGame;

            // copy the filename - without the path
            char * fname = const_cast<char *>(fileName.toStr());
            char * strptr = &fname[strlen(fname)-1];
            while(*strptr != '\\') strptr--;
            strptr++;
            // add "SavedGames\" path
            strcpy(savegame_msg.savefilename, "SavedGames\\");
            // add filename
            strcat(savegame_msg.savefilename, strptr);

            if(g_directPlay) {

               g_directPlay->sendMessage(
                  (LPVOID) &savegame_msg,
                  sizeof(MP_MSG_SAVEGAME)
               );
            }
         }

         result = writeWorld(fileName.toStr(), ff);
        }

      // unpause
      if(CMultiPlayerConnection::connectionType() != ConnectionNone) CMultiPlayerConnection::dialogPause(false);

        return result;
}

Boolean CampaignImp::saveGame(bool prompt) const
{
        return save(SaveGame::SaveGame, prompt);
}

Boolean CampaignImp::saveWorld() const
{
        return save(SaveGame::Campaign, true);
}

void CampaignImp::saveGame(const char* fileName) const
{
   writeWorld(fileName, SaveGame::SaveGame);
}

#if !defined(EDITOR)

void CampaignImp::startLogic()
{
   ASSERT(d_logicThread);
   d_logicThread->init();
}

void CampaignImp::stopLogic()
{
        if(d_logicThread != 0)
        {
            d_logicThread->reset();
        }
}


/*
 * AI Interface
 */

void CampaignImp::removeAI(Side s)
{
    if(d_logicThread)
        d_logicThread->removeAI(s);
}

void CampaignImp::addAI(Side s)
{
    if(d_logicThread)
        d_logicThread->addAI(s);
}

void CampaignImp::sendPlayerMessage(const CampaignMessageInfo& msg)
{
    if(msg.side() != SIDE_Neutral)
    {
        switch(GamePlayerControl::getControl(msg.side()))
        {
            case GamePlayerControl::Player:
                if(d_windows)
                {
                  d_windows->addMessage(msg);
                }
                break;

            case GamePlayerControl::AI:
                if(d_logicThread != 0)
                    d_logicThread->sendAIMessage(msg);
                break;
        }
    }
}

#endif  // !EDITOR


const CampaignTime& CampaignImp::getCTime()
{
        ASSERT(d_campData != 0);
        return d_campData->getCTime();
}

const Date& CampaignImp::getDate() const
{
        ASSERT(d_campData != 0);
        return d_campData->getDate();
}

const Time& CampaignImp::getTime() const
{
        ASSERT(d_campData != 0);
        return d_campData->getTime();
}

const char* CampaignImp::asciiTime(char* buffer) const
{
        ASSERT(d_campData != 0);
        return d_campData->asciiTime(buffer);
}

#if !defined(EDITOR)
TimeTick CampaignImp::getTick() const
{
        ASSERT(d_campData != 0);
        if(d_campData != 0)
                return d_campData->getTick();
        else
                return 0;
}
#endif  // !EDITOR

#if defined(CUSTOMIZE)
void CampaignImp::removeAllTowns()
{
         GameControl::Pauser pause;    // pauses game until destructed

        int result = messageBox(
                "Warning",

                "This operation will remove\r"
                "All towns, Provinces, Connections\r"
                "and units\r\r"
                "Are you sure you want to do this?",

                MB_ICONEXCLAMATION | MB_YESNO);

        if(result == IDYES)
        {
                if(d_windows != 0)
                        d_windows->invalidateWindows();

                d_campData->removeAllTowns();
        }
}

void CampaignImp::removeAllUnits()
{
         GameControl::Pauser pause;    // pauses game until destructed

        int result = messageBox(
                "Warning",

                "This operation will remove\r"
                "all Units, Leaders and\r"
                "Strength Points\r\r"
                "Are you sure you want to do this?",

                MB_ICONEXCLAMATION | MB_YESNO);

        if(result == IDYES)
        {
                if(d_windows != 0)
                        d_windows->invalidateWindows();

                d_campData->removeAllUnits();
        }
}
#endif

#if !defined(EDITOR)

#ifdef DEBUG

void CampaignImp::showAI(DrawDIBDC* dib, ITown itown, const PixelPoint& p)
{
        if(d_logicThread != 0)
                d_logicThread->showAI(dib, itown, p);
}

void CampaignImp::drawAI(const IMapWindow& mw)
{
   if(d_logicThread != 0)
       d_logicThread->drawAI(mw);
}

#endif



void CampaignImp::setController(Side side, GamePlayerControl::Controller control)
{
    GamePlayerControl::Controller oldControl = GamePlayerControl::getControl(side);

    if(oldControl != control)
    {
        if(oldControl == GamePlayerControl::AI)
            removeAI(side);

        if(control == GamePlayerControl::AI)
            addAI(side);

    }
}



void CampaignImp::startTactical(CampaignBattle* battle)
{
#ifdef DEBUG
        messageBox("CampImp", "Starting Tactical battle", MB_OK | MB_ICONINFORMATION);
#endif
        d_windows->suspend(false);
        d_battle = new CampaignBattleGame;
        d_battle->start(battle, this);
}

void CampaignImp::startReinforcement()
{
   if(d_battle)
      d_battle->reinforce();
}

class ReinforceTacticalMsg : public WargameMessage::WaitableMessage
{
      StartTacticalInterface* d_game;
   public:
      ReinforceTacticalMsg(StartTacticalInterface* game) :  d_game(game) { }

      void run()
      {
         d_game->startReinforcement();
      }

      void clear() { d_event.set(); }
};

void CampaignImp::reinforceTactical()
{
   if(d_battle)
      d_battle->reinforce();
}

void CampaignImp::tacticalOver()
{
#ifdef DEBUG
        messageBox("CampImp", "Stopping Tactical battle", MB_OK | MB_ICONINFORMATION);
#endif
        pause(true);
        delete d_battle;
        d_battle = 0;
        d_windows->suspend(true);
        startTime();
        pause(false);
}

void CampaignImp::stopTime()
{
   ASSERT(d_logicThread);
   if(d_logicThread)
      d_logicThread->stopTime();

   if(CMultiPlayerConnection::connectionType() != ConnectionNone) CMultiPlayerConnection::dialogPause(true);
}

void CampaignImp::startTime()
{
   ASSERT(d_logicThread);
   if(d_logicThread)
      d_logicThread->startTime();

   if(CMultiPlayerConnection::connectionType() != ConnectionNone) CMultiPlayerConnection::dialogPause(false);
}

/*
 * Logic thread asks for tactical battle to begin
 */

class StartTacticalMsg : public WargameMessage::WaitableMessage
{
      StartTacticalInterface* d_game;
      CampaignBattle* d_battle;
   public:
      StartTacticalMsg(CampaignBattle* battle, StartTacticalInterface* game) : d_battle(battle), d_game(game) { }

        void run()
        {
            d_game->startTactical(d_battle);
        }

      void clear() { d_event.set(); }
};

void CampaignImp::requestTactical(CampaignBattle* battle)
{
   StartTacticalMsg dial(battle, this);

   GameControl::Pauser pause;    // pauses game until destructed

   WargameMessage::postMessage(&dial);
   int result = dial.wait();
   ASSERT(result == WAIT_OBJECT_0);

}

/*
 * get response from user (or AI or remote?)
 * Return TRUE if value OK
 */

bool CampaignImp::getUserResponse(WargameMessage::WaitableMessage* msg)
{
    ASSERT(msg != 0);
    ASSERT(d_logicThread != 0);

#ifdef DEBUG
   g_logWindow.printf("getUserResponse... sending");
#endif

    msg->send();

    if(d_logicThread)
    {
        Thread::WaitValue value = d_logicThread->wait(msg->syncObject(), INFINITE);
#ifdef DEBUG
        g_logWindow.printf("getUserResponse... finished, value=%d", (int) value);
#endif
        return (value == Thread::TWV_Event);
    }
    else
    {
        int result = msg->wait();
#ifdef DEBUG
        g_logWindow.printf("getUserResponse... finished, result=%d", (int) result);
#endif
       return (result == WAIT_OBJECT_0);
    }

}

// Tell Game about siege results

void CampaignImp::siegeResults(Side side, const WG_CampaignSiege::SiegeInfoData& sd)
{
   if(GamePlayerControl::getControl(side) == GamePlayerControl::Player)
    {
      SiegeResultMsg dial(sd);
        stopTime();
        getUserResponse(&dial);
        startTime();
    }
}

// Ask Game what to do at end of Day

EndOfDayMode::Mode CampaignImp::endBattleDay(Side side, const BattleInfoData& bd)
{
   if(CMultiPlayerConnection::connectionType() == ConnectionNone) {

      if(GamePlayerControl::getControl(side) == GamePlayerControl::Player)
      {
         stopTime();
         BattleDayRequest dial(bd);
         getUserResponse(&dial);
         startTime();
         return dial.result();
      }
      else
         return EndOfDayMode::Withdraw;
   }

   /*
   See comments below for "askBattleMode()"
   Hopefully we will later be able to sort out these dialog requests, but for
   now we'll have to force a result
   */
   else {
      return EndOfDayMode::Continue;
   }

}

// tell game about Battle Results

void CampaignImp::battleResults(Side side, const BattleInfoData& bd)
{
  if(GamePlayerControl::getControl(side) == GamePlayerControl::Player)
  {
     stopTime();
    BattleResultMsg brWindow(bd);
     getUserResponse(&brWindow);
     startTime();
  }
}

// Ask what mode to do a battle

BattleMode::Mode CampaignImp::askBattleMode(const CampaignBattle* battle)
{


   if(CMultiPlayerConnection::connectionType() == ConnectionNone) {

      stopTime();
      ChooseBattleModeRequest dial(battle);
      getUserResponse(&dial);
      ASSERT( (dial.mode() == BattleMode::Calculated) || (dial.mode() == BattleMode::Tactical) );
      if(dial.mode() != BattleMode::Tactical)
         startTime();
      return dial.mode();
   }

   /*
   This is a real shame, but we're going to have to force Calculated battle in muiltiplayer
   If we can decouple the decision over Calculated / Tactical to AFTER the simulation of the current logic pass,
   then both Master & Slave can be guaranteed to be in the same state, and will bothe be waiting for the decision
   */
   else {
      stopTime();
      ChooseBattleModeRequest dial(battle);
      dial.mode(BattleMode::Calculated);
       startTime();
      return dial.mode();
   }

}

/*
 * Update nation Morale Bar
 * Thread safe method, sends message so always executed by UI thread
 */

void CampaignImp::moraleUpdated()        // Redraw national morale bars
{
    class MoraleUpdateMessage : public WargameMessage::MessageBase
    {
        public:
            MoraleUpdateMessage(CampaignWindows* wind) : d_windows(wind) { }

            virtual void run()
            {
                d_windows->moraleBarUpdated();
            }

        private:
            CampaignWindows* d_windows;
    };

    WargameMessage::postMessage(new MoraleUpdateMessage(d_windows));

}

void CampaignImp::armisticeBegun()
{
   const Armies& army = d_campData->getArmies();
   for (Side s = 0; s < scenario->getNumSides(); ++s)
   {
      CampaignMessageInfo msg(s, CampaignMessages::ArmisticeStart);
      msg.cp(army.getFirstUnit(s));
      sendPlayerMessage(msg);
   }

   moraleUpdated();
}

void CampaignImp::armisticeEnded()
{
   const Armies& army = d_campData->getArmies();
   for (Side s = 0; s < scenario->getNumSides(); ++s)
   {
      CampaignMessageInfo msg(s, CampaignMessages::ArmisticeEnd);
      msg.cp(army.getFirstUnit(s));
      sendPlayerMessage(msg);
   }

   moraleUpdated();
}


/*
 * Update Weather Window
 * Thread safe method, sends message so always executed by UI thread
 */

void CampaignImp::updateWeather()        // weather has changed
{
    class WeatherUpdateMessage : public WargameMessage::MessageBase
    {
        public:
            WeatherUpdateMessage(CampaignWindows* wind) : d_windows(wind) { }

            virtual void run()
            {
                d_windows->updateWeather();
            }

        private:
            CampaignWindows* d_windows;
    };

    WargameMessage::postMessage(new WeatherUpdateMessage(d_windows));
}


#endif  // !EDITOR

/*============================================================
 * File Handling
 */


/*
 * Read world data
 */

Boolean CampaignImp::readAsciiWorld(const char* fileName, SaveGame::FileFormat mode)
{
        try
        {
                char fName[MAX_PATH];

                addExtension(fName, fileName, "ASC");

                FileWithMode<WinFileAsciiChunkReader> fr(fName, mode);

                return readData(fr);
        }
        catch(FileError)
        {
                return False;
        }

}

Boolean CampaignImp::readBinaryWorld(const char* fileName, SaveGame::FileFormat mode)
{
        try
        {
                char fName[MAX_PATH];

                if(mode == SaveGame::Campaign)
                        addExtension(fName, fileName, CAMPAIGN_EXT);
                else if(mode == SaveGame::SaveGame)
                        addExtension(fName, fileName, SAVE_EXT);
                else
                        FORCEASSERT("Illegal mode");

                FileWithMode<WinFileBinaryChunkReader> fr(fName, mode);

                return readData(fr);
        }
        catch(FileError)
        {
                return False;
        }
}

Boolean CampaignImp::readWorld(const char* fileName, SaveGame::FileFormat mode)
{
        Boolean flag = False;

        flag = readBinaryWorld(fileName, mode);
        if(!flag)
                flag = readAsciiWorld(fileName, mode);

        return flag;

}




Boolean CampaignImp::writeBinaryWorld(const char* fileName, SaveGame::FileFormat mode) const
{
        try
        {
                char fName[MAX_PATH];

                if(mode == SaveGame::Campaign)
                        addExtension(fName, fileName, CAMPAIGN_EXT);
                else if(mode == SaveGame::SaveGame)
                        addExtension(fName, fileName, SAVE_EXT);
                else
                        FORCEASSERT("Illegal mode");

                FileWithMode<WinFileBinaryChunkWriter> fr(fName, mode);

                return writeData(fr);
        }
        catch(FileError)
        {
                return False;
        }
}

Boolean CampaignImp::writeAsciiWorld(const char* fileName, SaveGame::FileFormat mode) const
{
        try
        {
                char fName[MAX_PATH];

                addExtension(fName, fileName, "ASC");

                FileWithMode<WinFileAsciiChunkWriter> fr(fName, mode);

                return writeData(fr);
        }
        catch(FileError)
        {
                return False;
        }
}


Boolean CampaignImp::writeWorld(const char* fileName, SaveGame::FileFormat mode) const
{
   GameControl::Pauser pause;    // pauses game until destructed
   ASSERT(fileName != 0);
        writeBinaryWorld(fileName, mode);
   return True;
}

Boolean CampaignImp::readData(FileReader& f)
{
   ASSERT(d_campData != 0);

   bool retVal = d_campData->readData(f);

#if !defined(EDITOR)
   readTacticalBattle(f);

   d_logicThread->readData(f);

   /*
    * Setup starting orders
    */

    /*
     * Initialize starting orders
     * Maybe should be elsewhere
     */

    if(f.getMode() == SaveGame::Campaign)
    {
        Armies* army = &d_campData->getArmies();


#if 1
        /*
         * Logic Owner that does nothing except return campData.
         */

        class DummyLogicOwner : public CampaignLogicOwner
        {
            public:
                DummyLogicOwner(CampaignData* campData) : d_campData(campData) { }
                virtual ~DummyLogicOwner() { }

               virtual void sendPlayerMessage(const CampaignMessageInfo& msg) { }
               virtual void redrawMap() { }
               virtual void repaintMap() { }
                virtual void moraleUpdated() { }
                virtual void updateWeather() { }
               virtual CampaignData* campaignData() { return d_campData; }
                virtual void requestTactical(CampaignBattle* battle) { }
                virtual void siegeResults(Side s, const SiegeInfoData& sd) { }
                virtual EndOfDayMode::Mode endBattleDay(Side s, const BattleInfoData& bd) { return EndOfDayMode::Withdraw; }
                virtual void battleResults(Side s, const BattleInfoData& bd) { }
                virtual BattleMode::Mode askBattleMode(const CampaignBattle* battle) { return BattleMode::Calculated; }
                virtual void reinforceTactical() {}
            private:
                CampaignData* d_campData;
        };

        DummyLogicOwner owner(d_campData);
#endif

        /*
         * Pass 1 : sets up the Order On Arrival, which is how the
         *          campaign editor stores info about garrisons.
         */

        UnitIter uiter(army, army->getTop());
        while(uiter.next())
        {
            CampaignOrderUtil::initCampaignOrder(&owner, uiter.current());
        }

        /*
         * Pass 2 : Ensures sieges and raids are started properly
         */

        uiter.reset();
        while(uiter.next())
        {
            CampaignOrderUtil::orderUpdated(&owner, uiter.current());
        }
    }
#endif

        return retVal;
}

Boolean CampaignImp::writeData(FileWriter& f) const
{
        FileTypeChunk ftype(SaveGame::Campaign);
        f << ftype;

        if(!d_campData->writeData(f))
            return false;

#if !defined(EDITOR)
        writeTacticalBattle(f);
         d_logicThread->writeData(f);
#endif

        return f.isOK();
}

bool CampaignImp::readTacticalBattle(FileReader& f)
{
    // flow control is a bit dodgy here because
    // FileChunkReader must be destructed before battle is read
    // but whether it exists is also the means of knowing if there is a battle

#if !defined(EDITOR)

    ASSERT(d_battle == 0);

    int id;

    {
        FileChunkReader fr(f, campaignTacticalChunkName);

        if(fr.isOK())
        {
            d_battle = new CampaignBattleGame;

            f >> id;

        }
    }

    // Read Battle Data

    if(d_battle)
    {
        CampaignBattle* campBat = d_campData->getBattleList().getFromID(id);
        d_battle->readData(f, campBat, this);
    }
#endif

    return f.isOK();
}

bool CampaignImp::writeTacticalBattle(FileWriter& f) const
{
#if !defined(EDITOR)
    if(d_battle)
    {
        {
            FileChunkWriter fr(f, campaignTacticalChunkName);

            // Write the CampaignBattle index

            int id = d_campData->getBattleList().getID(d_battle->campaignBattle());
            f << id;
        }

        // Write the battle data

        d_battle->writeData(f);
    }
#endif
    return f.isOK();
}

void CampaignImp::pause(Boolean pauseMode)
{
        GameThreads::pause(pauseMode);
}

#if !defined(EDITOR)

/*
 * Stop the Campaign... stop the campaign...
 * sing along to the same tune as "Stop the Pigeon"
 *
 * Dastardly and Muley hatch a cunning plan to get
 * the camapign to stop, and to let the player know
 * all about their scheming plan.
 */

class VictoryMessage : public WargameMessage::MessageBase
{
   public:
      VictoryMessage(CampaignImp* campGame) : d_game(campGame)
      {
      }

      ~VictoryMessage()
      {
         d_game = 0;
      }

      void run()
      {
         d_game->runVictoryScreen();
      }

   private:
      CampaignImp* d_game;
};

void CampaignImp::endCampaign(const GameVictory& victory)
{
   // Send a message to ourselves because this is called
   // from the logic thread

   d_campData->setVictory(victory);

   WargameMessage::postMessage(new VictoryMessage(this));
}

void CampaignImp::runVictoryScreen()
{
   if (!d_campaignOver)
   {
      d_campaignOver = true;

      /*
       * Pause / Stop the game
       */

      pause(true);

      /*
       * Initiate the Results Screen
       */

      d_windows->runVictoryScreen();

      /*
       * The results screen will notify the owner
       * when it is finished by calling
       * endVictoryScreen()
       */
   }
}

void CampaignImp::endVictoryScreen()
{
   // Notify user that we are done

   WargameMessage::postAbandon();
}

#endif
/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
