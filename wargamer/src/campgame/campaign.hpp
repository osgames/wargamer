/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPAIGN_HPP
#define CAMPAIGN_HPP

#ifndef __cplusplus
#error campaign.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Top Level CampaignGame that a client can use
 *
 * This is just a wrapper on top of campimp (Campaign Implementation)
 *
 *----------------------------------------------------------------------
 */

#if !defined(MYTYPES_H)
#include "mytypes.h"
#endif
#include "systick.hpp"
#include "savegame.hpp"
#include "gamebase.hpp"

#if defined(NO_WG_DLL)
    #define CAMPGAME_DLL
#elif defined(EXPORT_CAMPGAME_DLL)
    #define CAMPGAME_DLL __declspec(dllexport)
#else
    #define CAMPGAME_DLL __declspec(dllimport)
#endif



class CampaignImp;		// Campaign Implementation



class CAMPGAME_DLL CampaignGame : public GameBase
{
	CampaignImp* d_campGame;		// Pointer to Implementation

	// Unimplemented Functions declared Private to prevent copying

	CampaignGame(const CampaignGame& campGame);
	const CampaignGame& operator = (const CampaignGame& campGame);

 public:
 	// CampaignGame(HWND mainWind, const char* fileName, SaveGame::FileFormat mode);
 	CampaignGame(GameOwner* owner, const char* fileName, SaveGame::FileFormat mode);
	~CampaignGame();

#if !defined(EDITOR)
   void showFrontEnd(const char* fileName);
#endif

#ifdef CUSTOMIZE
	void runDataCheck();
#endif

    /*
     * Implement GameBase
     */

	virtual void pauseGame(Boolean pauseMode);

	virtual void addTime(SysTick::Value ticks);
		// Add SysTicks to the campaign time

	// virtual void finishBattle();

    virtual bool saveGame(bool prompt) const;
    virtual void saveGame(const char* fileName) const;

	virtual void doNewGame(const char * current, SaveGame::FileFormat format);
	virtual void syncSlave(SysTick::Value ticks);
	virtual void timeSyncAcknowledged(SysTick::Value ticks);
	virtual void slaveReady(void);

	virtual void processBattleOrder(MP_MSG_BATTLEORDER * msg);
	virtual void processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg);
};


#endif /* CAMPAIGN_HPP */

