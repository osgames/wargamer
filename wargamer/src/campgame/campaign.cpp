/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Top Level Campaign Game that client uses
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campaign.hpp"
#include "campimp.hpp"
#include "myassert.hpp"

CampaignGame::CampaignGame(GameOwner* owner, const char* fileName, SaveGame::FileFormat mode)
{
   d_campGame = new CampaignImp(owner);
   d_campGame->init(fileName, mode);
   ASSERT(d_campGame != 0);
}

CampaignGame::~CampaignGame()
{
   ASSERT(d_campGame != 0);
   delete d_campGame;
}

void CampaignGame::pauseGame(Boolean pauseMode)
{
   ASSERT(d_campGame != 0);
   d_campGame->pause(pauseMode);
}

#ifdef CUSTOMIZE
void CampaignGame::runDataCheck()
{
  ASSERT(d_campGame != 0);
  d_campGame->runDataCheck();
}
#endif


// Add SysTicks to the campaign time

void CampaignGame::addTime(SysTick::Value ticks)
{
#if !defined(EDITOR)
   ASSERT(d_campGame != 0);
   d_campGame->addTime(ticks);
#endif
}

#if 0
void CampaignGame::finishBattle()
{
#if !defined(EDITOR)
   ASSERT(d_campGame != 0);
   d_campGame->finishBattle();
#endif
}
#endif


bool CampaignGame::saveGame(bool prompt) const
{
   ASSERT(d_campGame != 0);
    return d_campGame->saveGame(prompt);
}

void CampaignGame::saveGame(const char* fileName) const
{
   ASSERT(d_campGame != 0);
   d_campGame->saveGame(fileName);
}


void CampaignGame::doNewGame(const char * current, SaveGame::FileFormat format) {
   // do nothing

}

/*
   Notify CampaignLogic that we've been instructed by the Master machine
   to advance time to 'ticks' time

*/
void CampaignGame::syncSlave(SysTick::Value ticks) {

   ASSERT(d_campGame != 0);
   d_campGame->syncSlave(ticks);
}


/*

  Notify CampaignLogic than the Slave has acknowledged the time-sync

*/

void CampaignGame::timeSyncAcknowledged(SysTick::Value ticks) {

   ASSERT(d_campGame != 0);
   d_campGame->timeSyncAcknowledged(ticks);
}


/*

  Notify the CampaignLogic that the Slave machine is ready to do business (ie. it's started the game logic & windows up Ok) !

*/

void CampaignGame::slaveReady(void) {

   ASSERT(d_campGame != 0);
   d_campGame->slaveReady();
}




void CampaignGame::processBattleOrder(MP_MSG_BATTLEORDER * msg)
{
   d_campGame->processBattleOrder(msg);
}

void CampaignGame::processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg)
{
   d_campGame->processCampaignOrder(msg);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
