/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CBAT_OB_HPP
#define CBAT_OB_HPP

#ifndef __cplusplus
#error cbat_ob.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign To battle OB Conversions
 *
 *----------------------------------------------------------------------
 */

class CampaignBattle;
class Armies;
class BattleOB;


namespace CampaignBattleUtility
{

// Transfer Campaign Units into BattleOB
BattleOB* makeBattleOB(CampaignBattle* campBat, Armies* campArmy);

void addReinforcements(CampaignBattle* campBat, Armies* campArmy, BattleOB* ob);
// add reinforcements to the battle

// Import BattleOB back into Campaign
void restoreOB(CampaignBattle* campBat, Armies* campArmy, BattleOB* bob);

// Delete the Battle OB
// void removeOB(CampaignBattle* campBat, Armies* campArmy, BattleData* batData);

};

#endif /* CBAT_OB_HPP */

