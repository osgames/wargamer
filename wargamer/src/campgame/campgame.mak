# Microsoft Developer Studio Generated NMAKE File, Based on campgame.dsp
!IF "$(CFG)" == ""
CFG=campgame - Win32 Debug
!MESSAGE No configuration specified. Defaulting to campgame - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "campgame - Win32 Release" && "$(CFG)" != "campgame - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campgame.mak" CFG="campgame - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campgame - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campgame - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campgame - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campgame.dll"

!ELSE 

ALL : "batgame - Win32 Release" "batdata - Win32 Release" "ob - Win32 Release" "clogic - Win32 Release" "gwind - Win32 Release" "gamesup - Win32 Release" "system - Win32 Release" "campdata - Win32 Release" "camp_ai - Win32 Release" "campwind - Win32 Release" "$(OUTDIR)\campgame.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"campwind - Win32 ReleaseCLEAN" "camp_ai - Win32 ReleaseCLEAN" "campdata - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" "clogic - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "batdata - Win32 ReleaseCLEAN" "batgame - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\batentry.obj"
	-@erase "$(INTDIR)\campaign.obj"
	-@erase "$(INTDIR)\campbat.obj"
	-@erase "$(INTDIR)\campimp.obj"
	-@erase "$(INTDIR)\cbat_ob.obj"
	-@erase "$(INTDIR)\clogic.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\campgame.dll"
	-@erase "$(OUTDIR)\campgame.exp"
	-@erase "$(OUTDIR)\campgame.lib"
	-@erase "$(OUTDIR)\campgame.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /Fp"$(INTDIR)\campgame.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campgame.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\campgame.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campgame.dll" /implib:"$(OUTDIR)\campgame.lib" 
LINK32_OBJS= \
	"$(INTDIR)\batentry.obj" \
	"$(INTDIR)\campaign.obj" \
	"$(INTDIR)\campbat.obj" \
	"$(INTDIR)\campimp.obj" \
	"$(INTDIR)\cbat_ob.obj" \
	"$(INTDIR)\clogic.obj" \
	"$(OUTDIR)\campwind.lib" \
	"$(OUTDIR)\camp_ai.lib" \
	"$(OUTDIR)\campdata.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\gwind.lib" \
	"$(OUTDIR)\clogic.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\batdata.lib" \
	"$(OUTDIR)\batgame.lib"

"$(OUTDIR)\campgame.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campgameDB.dll"

!ELSE 

ALL : "batgame - Win32 Debug" "batdata - Win32 Debug" "ob - Win32 Debug" "clogic - Win32 Debug" "gwind - Win32 Debug" "gamesup - Win32 Debug" "system - Win32 Debug" "campdata - Win32 Debug" "camp_ai - Win32 Debug" "campwind - Win32 Debug" "$(OUTDIR)\campgameDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"campwind - Win32 DebugCLEAN" "camp_ai - Win32 DebugCLEAN" "campdata - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" "clogic - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "batdata - Win32 DebugCLEAN" "batgame - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\batentry.obj"
	-@erase "$(INTDIR)\campaign.obj"
	-@erase "$(INTDIR)\campbat.obj"
	-@erase "$(INTDIR)\campimp.obj"
	-@erase "$(INTDIR)\cbat_ob.obj"
	-@erase "$(INTDIR)\clogic.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\campgameDB.dll"
	-@erase "$(OUTDIR)\campgameDB.exp"
	-@erase "$(OUTDIR)\campgameDB.ilk"
	-@erase "$(OUTDIR)\campgameDB.lib"
	-@erase "$(OUTDIR)\campgameDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /Fp"$(INTDIR)\campgame.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campgame.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campgameDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campgameDB.dll" /implib:"$(OUTDIR)\campgameDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\batentry.obj" \
	"$(INTDIR)\campaign.obj" \
	"$(INTDIR)\campbat.obj" \
	"$(INTDIR)\campimp.obj" \
	"$(INTDIR)\cbat_ob.obj" \
	"$(INTDIR)\clogic.obj" \
	"$(OUTDIR)\campwindDB.lib" \
	"$(OUTDIR)\camp_aiDB.lib" \
	"$(OUTDIR)\campdataDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\gwindDB.lib" \
	"$(OUTDIR)\clogicDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\batdataDB.lib" \
	"$(OUTDIR)\batgameDB.lib"

"$(OUTDIR)\campgameDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("campgame.dep")
!INCLUDE "campgame.dep"
!ELSE 
!MESSAGE Warning: cannot find "campgame.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "campgame - Win32 Release" || "$(CFG)" == "campgame - Win32 Debug"
SOURCE=.\batentry.cpp

"$(INTDIR)\batentry.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campaign.cpp

"$(INTDIR)\campaign.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campbat.cpp

"$(INTDIR)\campbat.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campimp.cpp

"$(INTDIR)\campimp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cbat_ob.cpp

"$(INTDIR)\cbat_ob.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\clogic.cpp

"$(INTDIR)\clogic.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "campgame - Win32 Release"

"campwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Release" 
   cd "..\campgame"

"campwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"campwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Debug" 
   cd "..\campgame"

"campwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgame - Win32 Release"

"camp_ai - Win32 Release" : 
   cd "\src\sf\wargamer\src\camp_ai"
   $(MAKE) /$(MAKEFLAGS) /F .\camp_ai.mak CFG="camp_ai - Win32 Release" 
   cd "..\campgame"

"camp_ai - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\camp_ai"
   $(MAKE) /$(MAKEFLAGS) /F .\camp_ai.mak CFG="camp_ai - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"camp_ai - Win32 Debug" : 
   cd "\src\sf\wargamer\src\camp_ai"
   $(MAKE) /$(MAKEFLAGS) /F .\camp_ai.mak CFG="camp_ai - Win32 Debug" 
   cd "..\campgame"

"camp_ai - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\camp_ai"
   $(MAKE) /$(MAKEFLAGS) /F .\camp_ai.mak CFG="camp_ai - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgame - Win32 Release"

"campdata - Win32 Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" 
   cd "..\campgame"

"campdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"campdata - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" 
   cd "..\campgame"

"campdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgame - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\campgame"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\campgame"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgame - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\campgame"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\campgame"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgame - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\campgame"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\campgame"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgame - Win32 Release"

"clogic - Win32 Release" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Release" 
   cd "..\campgame"

"clogic - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"clogic - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Debug" 
   cd "..\campgame"

"clogic - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgame - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\campgame"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\campgame"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgame - Win32 Release"

"batdata - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" 
   cd "..\campgame"

"batdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"batdata - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" 
   cd "..\campgame"

"batdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgame - Win32 Release"

"batgame - Win32 Release" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" 
   cd "..\campgame"

"batgame - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgame - Win32 Debug"

"batgame - Win32 Debug" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" 
   cd "..\campgame"

"batgame - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 


!ENDIF 

