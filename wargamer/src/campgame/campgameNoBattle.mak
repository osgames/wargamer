# Microsoft Developer Studio Generated NMAKE File, Based on campgameNoBattle.dsp
!IF "$(CFG)" == ""
CFG=campgameNoBattle - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to campgameNoBattle - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "campgameNoBattle - Win32 Release" && "$(CFG)" != "campgameNoBattle - Win32 Debug" && "$(CFG)" != "campgameNoBattle - Win32 Editor Debug" && "$(CFG)" != "campgameNoBattle - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campgameNoBattle.mak" CFG="campgameNoBattle - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campgameNoBattle - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campgameNoBattle - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campgameNoBattle - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campgameNoBattle - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campgameNoBattle.dll"

!ELSE 

ALL : "clogic - Win32 Release" "campwind - Win32 Release" "campdata - Win32 Release" "ob - Win32 Release" "camp_ai - Win32 Release" "system - Win32 Release" "gamesup - Win32 Release" "$(OUTDIR)\campgameNoBattle.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "camp_ai - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "campdata - Win32 ReleaseCLEAN" "campwind - Win32 ReleaseCLEAN" "clogic - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campaign.obj"
	-@erase "$(INTDIR)\campbatn.obj"
	-@erase "$(INTDIR)\campimp.obj"
	-@erase "$(INTDIR)\clogic.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\campgameNoBattle.dll"
	-@erase "$(OUTDIR)\campgameNoBattle.exp"
	-@erase "$(OUTDIR)\campgameNoBattle.lib"
	-@erase "$(OUTDIR)\campgameNoBattle.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /Fp"$(INTDIR)\campgameNoBattle.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campgameNoBattle.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\campgameNoBattle.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campgameNoBattle.dll" /implib:"$(OUTDIR)\campgameNoBattle.lib" 
LINK32_OBJS= \
	"$(INTDIR)\campaign.obj" \
	"$(INTDIR)\campbatn.obj" \
	"$(INTDIR)\campimp.obj" \
	"$(INTDIR)\clogic.obj" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\camp_ai.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\campdata.lib" \
	"$(OUTDIR)\campwind.lib" \
	"$(OUTDIR)\clogic.lib"

"$(OUTDIR)\campgameNoBattle.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campgameNoBattleDB.dll"

!ELSE 

ALL : "clogic - Win32 Debug" "campwind - Win32 Debug" "campdata - Win32 Debug" "ob - Win32 Debug" "camp_ai - Win32 Debug" "system - Win32 Debug" "gamesup - Win32 Debug" "$(OUTDIR)\campgameNoBattleDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "camp_ai - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "campdata - Win32 DebugCLEAN" "campwind - Win32 DebugCLEAN" "clogic - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campaign.obj"
	-@erase "$(INTDIR)\campbatn.obj"
	-@erase "$(INTDIR)\campimp.obj"
	-@erase "$(INTDIR)\clogic.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\campgameNoBattleDB.dll"
	-@erase "$(OUTDIR)\campgameNoBattleDB.exp"
	-@erase "$(OUTDIR)\campgameNoBattleDB.ilk"
	-@erase "$(OUTDIR)\campgameNoBattleDB.lib"
	-@erase "$(OUTDIR)\campgameNoBattleDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /Fp"$(INTDIR)\campgameNoBattle.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campgameNoBattle.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campgameNoBattleDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campgameNoBattleDB.dll" /implib:"$(OUTDIR)\campgameNoBattleDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\campaign.obj" \
	"$(INTDIR)\campbatn.obj" \
	"$(INTDIR)\campimp.obj" \
	"$(INTDIR)\clogic.obj" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\camp_aiDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\campdataDB.lib" \
	"$(OUTDIR)\campwindDB.lib" \
	"$(OUTDIR)\clogicDB.lib"

"$(OUTDIR)\campgameNoBattleDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campgameNoBattleEDDB.dll"

!ELSE 

ALL : "campwind - Win32 Editor Debug" "campdata - Win32 Editor Debug" "ob - Win32 Editor Debug" "system - Win32 Editor Debug" "gamesup - Win32 Editor Debug" "$(OUTDIR)\campgameNoBattleEDDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 Editor DebugCLEAN" "system - Win32 Editor DebugCLEAN" "ob - Win32 Editor DebugCLEAN" "campdata - Win32 Editor DebugCLEAN" "campwind - Win32 Editor DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campaign.obj"
	-@erase "$(INTDIR)\campimp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\campgameNoBattleEDDB.dll"
	-@erase "$(OUTDIR)\campgameNoBattleEDDB.exp"
	-@erase "$(OUTDIR)\campgameNoBattleEDDB.ilk"
	-@erase "$(OUTDIR)\campgameNoBattleEDDB.lib"
	-@erase "$(OUTDIR)\campgameNoBattleEDDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /D "EDITOR" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\campgameNoBattle.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campgameNoBattle.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\campgameNoBattleEDDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campgameNoBattleEDDB.dll" /implib:"$(OUTDIR)\campgameNoBattleEDDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\campaign.obj" \
	"$(INTDIR)\campimp.obj" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\obEDDB.lib" \
	"$(OUTDIR)\campdataEDDB.lib" \
	"$(OUTDIR)\campwindEDDB.lib"

"$(OUTDIR)\campgameNoBattleEDDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Editor\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\campgameNoBattleED.dll"

!ELSE 

ALL : "campwind - Win32 Editor Release" "campdata - Win32 Editor Release" "ob - Win32 Editor Release" "system - Win32 Editor Release" "gamesup - Win32 Editor Release" "$(OUTDIR)\campgameNoBattleED.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"gamesup - Win32 Editor ReleaseCLEAN" "system - Win32 Editor ReleaseCLEAN" "ob - Win32 Editor ReleaseCLEAN" "campdata - Win32 Editor ReleaseCLEAN" "campwind - Win32 Editor ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\campaign.obj"
	-@erase "$(INTDIR)\campimp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\campgameNoBattleED.dll"
	-@erase "$(OUTDIR)\campgameNoBattleED.exp"
	-@erase "$(OUTDIR)\campgameNoBattleED.lib"
	-@erase "$(OUTDIR)\campgameNoBattleED.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\batdata" /I "..\batgame" /I "..\campwind" /I "..\mapwind" /I "..\gwind" /I "..\campLogic" /I "..\camp_ai" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_CAMPGAME_DLL" /D "EDITOR" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Fp"$(INTDIR)\campgameNoBattle.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\campgameNoBattle.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\campgameNoBattleED.pdb" /debug /machine:I386 /out:"$(OUTDIR)\campgameNoBattleED.dll" /implib:"$(OUTDIR)\campgameNoBattleED.lib" 
LINK32_OBJS= \
	"$(INTDIR)\campaign.obj" \
	"$(INTDIR)\campimp.obj" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\obED.lib" \
	"$(OUTDIR)\campdataED.lib" \
	"$(OUTDIR)\campwindED.lib"

"$(OUTDIR)\campgameNoBattleED.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("campgameNoBattle.dep")
!INCLUDE "campgameNoBattle.dep"
!ELSE 
!MESSAGE Warning: cannot find "campgameNoBattle.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "campgameNoBattle - Win32 Release" || "$(CFG)" == "campgameNoBattle - Win32 Debug" || "$(CFG)" == "campgameNoBattle - Win32 Editor Debug" || "$(CFG)" == "campgameNoBattle - Win32 Editor Release"
SOURCE=.\campaign.cpp

"$(INTDIR)\campaign.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\campbatn.cpp

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"


"$(INTDIR)\campbatn.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"


"$(INTDIR)\campbatn.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

!ENDIF 

SOURCE=.\campimp.cpp

"$(INTDIR)\campimp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\clogic.cpp

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"


"$(INTDIR)\clogic.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"


"$(INTDIR)\clogic.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

!ENDIF 

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\campgame"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\campgame"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

"gamesup - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" 
   cd "..\campgame"

"gamesup - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

"gamesup - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" 
   cd "..\campgame"

"gamesup - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\campgame"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\campgame"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

"system - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" 
   cd "..\campgame"

"system - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

"system - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" 
   cd "..\campgame"

"system - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

"camp_ai - Win32 Release" : 
   cd "\src\sf\wargamer\src\camp_ai"
   $(MAKE) /$(MAKEFLAGS) /F .\camp_ai.mak CFG="camp_ai - Win32 Release" 
   cd "..\campgame"

"camp_ai - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\camp_ai"
   $(MAKE) /$(MAKEFLAGS) /F .\camp_ai.mak CFG="camp_ai - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

"camp_ai - Win32 Debug" : 
   cd "\src\sf\wargamer\src\camp_ai"
   $(MAKE) /$(MAKEFLAGS) /F .\camp_ai.mak CFG="camp_ai - Win32 Debug" 
   cd "..\campgame"

"camp_ai - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\camp_ai"
   $(MAKE) /$(MAKEFLAGS) /F .\camp_ai.mak CFG="camp_ai - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

!ENDIF 

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\campgame"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\campgame"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

"ob - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" 
   cd "..\campgame"

"ob - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

"ob - Win32 Editor Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" 
   cd "..\campgame"

"ob - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

"campdata - Win32 Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" 
   cd "..\campgame"

"campdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

"campdata - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" 
   cd "..\campgame"

"campdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

"campdata - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Debug" 
   cd "..\campgame"

"campdata - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

"campdata - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Release" 
   cd "..\campgame"

"campdata - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

"campwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Release" 
   cd "..\campgame"

"campwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

"campwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Debug" 
   cd "..\campgame"

"campwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

"campwind - Win32 Editor Debug" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Editor Debug" 
   cd "..\campgame"

"campwind - Win32 Editor DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Editor Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

"campwind - Win32 Editor Release" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Editor Release" 
   cd "..\campgame"

"campwind - Win32 Editor ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campwind"
   $(MAKE) /$(MAKEFLAGS) /F .\campwind.mak CFG="campwind - Win32 Editor Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ENDIF 

!IF  "$(CFG)" == "campgameNoBattle - Win32 Release"

"clogic - Win32 Release" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Release" 
   cd "..\campgame"

"clogic - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Release" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Debug"

"clogic - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Debug" 
   cd "..\campgame"

"clogic - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\campgame"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campgameNoBattle - Win32 Editor Release"

!ENDIF 


!ENDIF 

