/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BATENTRY_HPP
#define BATENTRY_HPP

class CampaignData;
class CampaignBattle;
class CampaignBattleUnit;

namespace CBat_EntryPoint {
void setEntryPoint(CampaignData* campData, CampaignBattle* battle, CampaignBattleUnit* bu);
};
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
