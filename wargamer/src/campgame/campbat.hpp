/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPBAT_HPP
#define CAMPBAT_HPP

#ifndef __cplusplus
#error campbat.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Tactical Battle Game
 *
 *----------------------------------------------------------------------
 */

#include "systick.hpp"
#include "camptime.hpp"


class CampaignBattleGameImp;
class CampaignBattle;
class CampaignTacticalOwner;
struct MP_MSG_BATTLEORDER;

class CampaignBattleGame
{
	public:
		CampaignBattleGame();
		~CampaignBattleGame();

		GameTick addTime(SysTick::Value ticks);
		// void pauseGame(bool state);

		void start(CampaignBattle* battle, CampaignTacticalOwner* game);
		void reinforce();
		// void finish();
      void newDay();

      bool readData(FileReader& f, CampaignBattle* battle, CampaignTacticalOwner* game);
      bool writeData(FileWriter& f);
      void resume();  // continue after loading saved game

      CampaignBattle* campaignBattle();

      void processBattleOrder(MP_MSG_BATTLEORDER * msg);

	private:
		CampaignBattleGameImp* d_imp;
};


#endif /* CAMPBAT_HPP */

