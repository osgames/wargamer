##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#####################################
# Makefile Include for Campaign Map Window
#
# This is included from wargamer.mk
#
# It might be better to have a stand-alone makefile for each
# library instead of including like this.  That would make
# compiling individual libraries easier and faster, and also
# encourage each part of the program to be more independant
#-------------------------------------------------------

ROOT=..\..

############ Comment the MAKEDLL=1 to not make a DLL version
!ifndef MAKEDLL
!ifndef %NO_WG_DLL
MAKEDLL=1
!endif
!endif

!ifdef MAKEDLL
FNAME = campbat
!else
NAME = campbat
!endif

lnk_dependencies += campbat.mk

!ifdef %PCHDIR
GNAME=$NAME
!endif

!include $(ROOT)\config\wgpaths.mif

OBJS =	campbat.obj &
	cbat_ob.obj &
	batentry.obj

CFLAGS += -i=$(ROOT)\src\system
CFLAGS += -i=$(ROOT)\src\campdata
CFLAGS += -i=$(ROOT)\src\batgame
CFLAGS += -i=$(ROOT)\src\batdata
# CFLAGS += -i=$(ROOT)\src\campwind
# CFLAGS += -i=$(ROOT)\src\camp_ai
CFLAGS += -i=$(ROOT)\src\gamesup
CFLAGS += -i=$(ROOT)\src\ob
# CFLAGS += -i=$(ROOT)\src\mapwind
CFLAGS += -i=$(ROOT)\res

all :: $(TARGETS) .SYMBOLIC
	@%null

!ifdef MAKEDLL
###########
#Make DLL
###########

CFLAGS += -DEXPORT_CAMPBAT_DLL
SYSLIBS += COMCTL32.LIB VFW32.LIB DSOUND.LIB
LIBS += system.lib
LIBS += gamesup.lib
LIBS += ob.lib
LIBS += batdata.lib
LIBS += batlogic.lib
LIBS += batai.lib
LIBS += campdata.lib
LIBS += batdisp.lib
LIBS += batwind.lib
LIBS += batgame.lib

!include $(ROOT)\config\dll95.mif

!else

!include $(ROOT)\config\lib95.mif

!endif




