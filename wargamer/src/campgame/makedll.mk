##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

###########################################
# Battle Game Control Library Makefile
###########################################

ROOT=..\..

NAME = campGame
lnk_dependencies += makedll.mk

!include $(ROOT)\config\wgpaths.mif

CFLAGS += -DEXPORT_CAMPGAME_DLL


OBJS =	campimp.obj  &
	campaign.obj

!ifndef EDITOR
OBJS +=	clogic.obj
!endif

CFLAGS += -i=$(ROOT)\src\system
CFLAGS += -i=$(ROOT)\src\campdata
CFLAGS += -i=$(ROOT)\src\campwind
CFLAGS += -i=$(ROOT)\src\camp_ai
CFLAGS += -i=$(ROOT)\src\gamesup
CFLAGS += -i=$(ROOT)\src\ob
CFLAGS += -i=$(ROOT)\src\mapwind
CFLAGS += -i=$(ROOT)\res

SYSLIBS += COMCTL32.LIB VFW32.LIB DSOUND.LIB
# SYSLIBS += USER32.LIB KERNEL32.LIB GDI32.LIB WINMM.LIB SHELL32.LIB

LIBS += system.lib
LIBS += campdata.lib
LIBS += clogic.lib
LIBS += campwind.lib
LIBS += camp_ai.lib
LIBS += gamesup.lib
LIBS += ob.lib
LIBS += mapwind.lib
LIBS += campedit.lib
LIBS += campbat.lib
LIBS += batdata.lib
LIBS += batgame.lib
LIBS += batwind.lib
LIBS += batdisp.lib
LIBS += batlogic.lib
LIBS += batai.lib

!include $(ROOT)\config\dll95.mif


.before:
        @echo Making $(TARGETS)

#####################################################################
# $Log$
#####################################################################
