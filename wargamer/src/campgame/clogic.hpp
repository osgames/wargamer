/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CLOGIC_HPP
#define CLOGIC_HPP

#ifndef __cplusplus
#error clogic.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Logic
 *
 * Lots of stuff that used to be member functions of RealCampaignData
 * have been moved here.
 *
 *----------------------------------------------------------------------
 */

#include "thread.hpp"
#include "critical.hpp"
#include "measure.hpp"
#include "campevt.hpp"
#include "sched.hpp"
#include "aic.hpp"

class CampaignLogicOwner;
class CampaignData;


class UnitMoveSchedule;
class TownWeeklySchedule;
class SupplySchedule;
class TownDailySchedule;
class ArmyCleanupSchedule;

/*
 * Campaign Logic Thread
 */

class CampaignLogic :
  public Greenius_System::Thread
{
   CampaignLogicOwner*  d_campGame;
   CampaignData*  d_campData;
   AutoEvent      d_timePulse;      // Signal from campaign that time has changed
   SharedData     d_tickLock;
//   volatile TimeTick d_timeTick;
   CampaignTime d_wantTime;
   bool          d_pauseCount;  // Time is paused

   EventList         d_timeEvents;        // List of events to signal when time changes
   GameScheduleList  d_processes;         // List of Processes to run at particular times
   CampaignAI        d_ai;                // Campaign AI

   UnitMoveSchedule*    d_unitMoveSchedule;
   TownWeeklySchedule*  d_townWeeklySchedule;
   SupplySchedule*      d_supplySchedule;
   TownDailySchedule*   d_townDailySchedule;
   ArmyCleanupSchedule* d_armyCleanupSchedule;

   static const char s_fileChunkName[];
   static const UWORD s_fileVersion;

   CampaignLogic(const CampaignLogic& logic);
   const CampaignLogic& operator = (const CampaignLogic& logic);
 public:
   CampaignLogic(CampaignLogicOwner* campGame);
   ~CampaignLogic();

   void init();
   void reset();

    /*
     * functions called from CampaignGame
     */

   bool isTimeRunning() const { return !d_pauseCount; }
   void stopTime();
   void startTime();
//   void setTime(TimeTick timeTick);
   void setTime(const CampaignTime& wantTime);
      // Run the game until timeTick
   CampaignTime getWantTime() const;
      // Get the last value sent to setTime()

   void removeAI(Side s) { d_ai.removeAI(s); }
   void addAI(Side s);
      // Setup or Remove an AI side.
   void pauseAI(bool paused) { d_ai.pause(paused); }

   void sendAIMessage(const CampaignMessageInfo& msg) { d_ai.sendMessage(msg); }
      // Send an information from the field to the AI

#ifdef DEBUG
   void showAI(DrawDIBDC* dib, ITown itown, const PixelPoint& p) const { d_ai.showAI(dib, itown, p); }
   void drawAI(const IMapWindow& mw) { d_ai.drawAI(mw); }
#endif

      bool readData(FileReader& f);
      bool writeData(FileWriter& f) const;


 private:
   void run();
      // Implements virtual Thread Function to run the thread
};



#endif /* CLOGIC_HPP */

