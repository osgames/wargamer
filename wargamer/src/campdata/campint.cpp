/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Interface
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campint.hpp"

#if defined(GLOBAL_CAMPAIGN)

CampaignInterface::CampaignInterface()
{
   // ASSERT(campaignControl == 0);
   ASSERT(campaign == 0);
   // campaignControl = this;
   campaign = this;
}

CampaignInterface::~CampaignInterface()
{
   // ASSERT(campaignControl != 0);
   ASSERT(campaign != 0);
   // campaignControl = 0;
   campaign = 0;
}


// CampaignInterface* campaignControl = 0;   // Obsolete
CampaignInterface* campaign = 0; // Obsolete


#endif   // GLOBAL_CAMPAIGN

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
