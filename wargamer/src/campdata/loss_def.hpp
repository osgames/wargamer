/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef LOSS_DEF_HPP
#define LOSS_DEF_HPP

#ifndef __cplusplus
#error loss_def.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Strength Point Losses Deinition
 *
 *----------------------------------------------------------------------
 */

#include "fixpoint.hpp"

/*
 * Strength Point losses
 * Losses are represented as 65536th of a Strength Point,
 * e.g. SPLoss of 0x010000 means lose one SP
 *      SPLoss of 0x004000 means 25% of an SP to be lost.
 *		  SPLoss of 0x028000 means lose 2 and a half strength points
 */

typedef UFixed16_16 SPLoss;

#endif /* LOSS_DEF_HPP */

