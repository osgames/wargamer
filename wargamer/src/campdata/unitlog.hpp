/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef UNITLOG_HPP
#define UNITLOG_HPP

#ifndef __cplusplus
#error UNITLOG.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Unit Log File
 *
 *----------------------------------------------------------------------
 */


#ifdef DEBUG

#include "cdatadll.h"
#include "clog.hpp"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "measure.hpp"

// class RealCampaignData;
class CampaignData;


class UnitLogFile : public LogFile {
	// const RealCampaignData* campData;
	const CampaignData* campData;
	ICommandPosition oldCommand;
	TimeTick lastTime; 	
public:
	CAMPDATA_DLL UnitLogFile(const char* name);
	CAMPDATA_DLL ~UnitLogFile();

	CAMPDATA_DLL void printf(const char* fmt, ...);
	CAMPDATA_DLL void printf(ICommandPosition cpi, const char* fmt, ...);

	// void setCampaignData(const RealCampaignData* data) { campData = data; }
	void setCampaignData(const CampaignData* data) { campData = data; }
private:
	void checkTime();
};

CAMPDATA_DLL extern UnitLogFile cuLog;

#endif

#endif /* UNITLOG_HPP */

