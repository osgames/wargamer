/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Battles
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cbattle.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "scenario.hpp"
#include "cu_mode.hpp"
#include "gamectrl.hpp"
#include "fsalloc.hpp"
#include "c_obutil.hpp"
#include "filebase.hpp"
#include "savegame.hpp"
#include "armyutil.hpp"

/*---------------------------------------------------
 *
 */

class CBattleUtil {
public:
  static Side getOtherSide(Side s)
  {
    ASSERT(s < scenario->getNumSides());
    return (s == 0) ? 1 : 0;
  }
};

/*-----------------------------------------------------------------------
 * Hit Leader
 */

/*
 * File interface
 */

inline bool operator >> (FileReader& f, HitLeader::HitHow& h)
{
   UBYTE b;
   f >> b;

   h = static_cast<HitLeader::HitHow>(b);
   return true;
}

inline bool operator << (FileWriter& f, const HitLeader::HitHow& h)
{
   return f.putUByte(h);
}

#ifdef DEBUG
static FixedSizeAlloc<HitLeader> hitLeaderAlloc("HitLeader");
#else
static FixedSizeAlloc<HitLeader> hitLeaderAlloc;
#endif

void* HitLeader::operator new(size_t size)
{
  return hitLeaderAlloc.alloc(size);
}

#ifdef _MSC_VER
void HitLeader::operator delete(void* deadObject)
{
   hitLeaderAlloc.release(deadObject);
}
#else
void HitLeader::operator delete(void* deadObject, size_t size)
{
  hitLeaderAlloc.release(deadObject, size);
}
#endif

const UWORD s_hitLeaderVersion = 0x0000;
bool HitLeader::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_hitLeaderVersion);

  LeaderIndex iLeader;
  f >> iLeader;
  d_leader = CampaignOBUtil::leaderIndexToLeader(iLeader, ob);

  CPIndex cpi;
  f >> cpi;
  d_cpi = CampaignOBUtil::cpIndexToCPI(cpi, ob);

  f >> d_hitHow;

  return f.isOK();
}

bool HitLeader::write(FileWriter& f, const OrderBattle& ob) const
{
  f << s_hitLeaderVersion;

  LeaderIndex iLeader = CampaignOBUtil::leaderToLeaderIndex(d_leader, ob);
  f << iLeader;
  CPIndex cpi = CampaignOBUtil::cpiToCPIndex(d_cpi, ob);
  f << cpi;
  f << d_hitHow;

  return f.isOK();
}

const UWORD s_hitLeaderListVersion = 0x0000;
bool HitLeaderList::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_hitLeaderListVersion);

  int nItems;
  f >> nItems;
  while(nItems--)
  {
    HitLeader* hl = new HitLeader();
    ASSERT(hl);

    hl->read(f, ob);
    append(hl);
  }

  return f.isOK();
}

bool HitLeaderList::write(FileWriter& f, const OrderBattle& ob) const
{
  f << s_hitLeaderListVersion;

  int nItems = entries();

  f << nItems;

  SListIterR<HitLeader> iter(this);
  while(++iter)
  {
    iter.current()->write(f, ob);
  }

  return f.isOK();
}

/*-----------------------------------------------------------------------
 * Battle Unit
 */

#ifdef DEBUG
static FixedSizeAlloc<CampaignBattleUnit> CampaignBattleUnitItemAlloc("CampaignBattleUnit");
#else
static FixedSizeAlloc<CampaignBattleUnit> CampaignBattleUnitItemAlloc;
#endif

void* CampaignBattleUnit::operator new(size_t size)
{
   return CampaignBattleUnitItemAlloc.alloc(size);
}


#ifdef _MSC_VER
void CampaignBattleUnit::operator delete(void* deadObject)
{
   CampaignBattleUnitItemAlloc.release(deadObject);
}
#else
void CampaignBattleUnit::operator delete(void* deadObject, size_t size)
{
  CampaignBattleUnitItemAlloc.release(deadObject, size);
}
#endif



const UWORD s_CampaignBattleUnitVersion = 0x0000;
bool CampaignBattleUnit::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_CampaignBattleUnitVersion);

  CPIndex cpi;
  f >> cpi;
  d_cpi = CampaignOBUtil::cpIndexToCPI(cpi, ob);
  f >> d_flags;

  return f.isOK();
}

bool CampaignBattleUnit::write(FileWriter& f, const OrderBattle& ob) const
{
  f << s_CampaignBattleUnitVersion;

  CPIndex cpi = CampaignOBUtil::cpiToCPIndex(d_cpi, ob);
  f << cpi;
  f << d_flags;

  return f.isOK();
}

/*==================================================================
 *  CampaignBattleUnitList functions
 */

/*
 * File interface
 */

inline bool operator >> (FileReader& f, EndOfDayMode::Mode& m)
{
   UBYTE b;
   f >> b;

   m = static_cast<EndOfDayMode::Mode>(b);
   return true;
}

inline bool operator << (FileWriter& f, const EndOfDayMode::Mode& m)
{
   return f.putUByte(m);
}

inline bool operator >> (FileReader& f, Orders::Posture::Type& p)
{
   UBYTE b;
   f >> b;

   p = static_cast<Orders::Posture::Type>(b);
   return true;
}

inline bool operator << (FileWriter& f, const Orders::Posture::Type& p)
{
   return f.putUByte(p);
}

inline bool operator >> (FileReader& f, PreBattlePosture::Posture& p)
{
   UBYTE b;
   f >> b;

   p = static_cast<PreBattlePosture::Posture>(b);
   return true;
}

inline bool operator << (FileWriter& f, const PreBattlePosture::Posture& p)
{
   return f.putUByte(p);
}

const UWORD s_buListVersion = 0x0004;
bool CampaignBattleUnitList::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_buListVersion);


  /*
   * Read units
   */

  int nUnits;
  f >> nUnits;

  if(nUnits > 0)
  {
    while(nUnits--)
    {
      CampaignBattleUnit* bu = new CampaignBattleUnit;
      ASSERT(bu);

      bu->read(f, ob);
      append(bu);
    }

    // leader
    LeaderIndex li;
    f >> li;
    d_cinc = CampaignOBUtil::leaderIndexToLeader(li, ob);

    // attributes
    f >> d_startMorale;
    f >> d_currentMorale;
    f >> d_negativeMorale;
    f >> d_supplyLevel;
    f >> d_fatigue;
    f >> d_spValue;

    // sp / loss counts
    if(version >= 0x0001)
    {
      f >> d_startNInfantry;
      f >> d_startNCavalry;
      f >> d_startNArtillery;
      f >> d_startNOther;
    }

    f >> d_nInfantry;
    f >> d_nCavalry;
    f >> d_nArtillery;
    f >> d_nOther;
    f >> d_lossCount;
    ULONG lossValue;
    f >> lossValue;
    d_loss.setValue(lossValue);

    if(version == 0x0000)
    {
      ULONG lRubish;
      f >> lRubish;
      UWORD wRubish;
      f >> wRubish;
    }

    // misc
    f >> d_dieRoll;
    f >> d_statusCount;
    f >> d_bombValue;
    if(version < 0x0004)
    {
      UBYTE b;
      f >> b;
      d_flags = b;
    }
    else
      f >> d_flags;

    f >> d_town;
    f >> d_posture;
    if(version >= 0x0003)
    {
      f >> d_nation;
      f >> d_artilleryNation;
    }
    f >> d_endOfDayMode;
    if(version >= 0x0001)
    {
      f >> d_preBattlePosture;
    }

    if(version >= 0x0002)
      d_hitLeaders.read(f, ob);
  }

  return f.isOK();
}

bool CampaignBattleUnitList::write(FileWriter& f, const OrderBattle& ob) const
{
  f << s_buListVersion;

  /*
   * Write unit list
   */

  int nUnits = entries();
  f << nUnits;

  if(nUnits > 0)
  {
    SListIterR<CampaignBattleUnit> iter(this);
    while(++iter)
    {
      iter.current()->write(f, ob);
    }

    // leader
    LeaderIndex li = CampaignOBUtil::leaderToLeaderIndex(d_cinc, ob);
    f << li;

    // attributes
    f << d_startMorale;
    f << d_currentMorale;
    f << d_negativeMorale;
    f << d_supplyLevel;
    f << d_fatigue;

    // loss counts
    f << d_spValue;
    f << d_startNInfantry;
    f << d_startNCavalry;
    f << d_startNArtillery;
    f << d_startNOther;
    f << d_nInfantry;
    f << d_nCavalry;
    f << d_nArtillery;
    f << d_nOther;
    f << d_lossCount;
    ULONG lossValue = d_loss.getValue();
    f << lossValue;

    // misc
    f << d_dieRoll;
    f << d_statusCount;
    f << d_bombValue;
    f << d_flags;
    f << d_town;
    f << d_posture;
    f << d_nation;
    f << d_artilleryNation;
    f << d_endOfDayMode;
    f << d_preBattlePosture;

    d_hitLeaders.write(f, ob);
  }

  return f.isOK();
}

/*==================================================================
 * CampaignBattle Functions
 */

/*
 * Constructor
 */

CampaignBattle::CampaignBattle(const CampaignPosition* pos, CampaignData* campData) :
   d_campData(campData),
   d_turnMode(BattleTurnMode::Starting),
   d_mode(BattleMode::Setup),
   d_flags(0),
   d_ratio(Ratio_Equal),
   d_victor(SIDE_Neutral),
   d_siegeTown(NoTown),
   d_pursuitTarget(NoCommandPosition),
   d_tillWhen(0),
   d_nextRoundAt(0),
   d_battleRound(0),
   d_daysOfBattle(0)
{
#ifdef DEBUG
   counter = 12;     // 2 days (4 hours per count)
#endif
   setPosition(*pos);
}

CampaignBattle::CampaignBattle(CampaignData* campData) :
   d_campData(campData),
   d_turnMode(BattleTurnMode::Starting),
   d_mode(BattleMode::Setup),
   d_flags(0),
   d_ratio(Ratio_Equal),
   d_victor(SIDE_Neutral),
   d_siegeTown(NoTown),
   d_pursuitTarget(NoCommandPosition),
   d_tillWhen(0),
   d_battleRound(0),
   d_daysOfBattle(0)
{
#ifdef DEBUG
   counter = 12;     // 2 days (4 hours per count)
#endif
}

/*
 * Destructor
 */

CampaignBattle::~CampaignBattle()
{
}

/*
 * Add a unit to battle
 */

CampaignBattleUnit* CampaignBattle::addUnit(ICommandPosition cpi, bool quickBattle)
{
#if defined(DEBUG)
      if(!quickBattle)
   {
     ASSERT(!cpi->isInBattle());
   }
   ASSERT(cpi->isRealUnit());
#endif

   if(cpi->isRealUnit())
   {

     cpi->setTarget(NoCommandPosition);
     cpi->hasPursuitTarget(False);

     /*
      * We need to lock this in from setting mode to appending
      * otherwise battle display may crash
      * not sure if this is the proper way
      */

     // don't change mode if this is a quick battle
     if(!quickBattle)
     {
       isMoving(cpi->getSide(), cpi->shouldMove());
       CP_ModeUtil::setMode(d_campData, cpi, CampaignMovement::CPM_InBattle);
     }

     CampaignBattleUnit* bu = new CampaignBattleUnit(cpi);
     ASSERT(bu != 0);

     Side side = cpi->getSide();

     CampaignBattleUnitList* bl = &d_units[side];

     ASSERT(side < NumberSides);
     bl->append(bu);

     /*
      * Add unit counts to start counts
      */

     SPCount nInfantry = d_campData->getArmies().getNType(cpi, BasicUnitType::Infantry, False);
     SPCount nCavalry = d_campData->getArmies().getNType(cpi, BasicUnitType::Cavalry, False);
     SPCount nArtillery = d_campData->getArmies().getNType(cpi, BasicUnitType::Artillery, False);
     SPCount nOther = d_campData->getArmies().getNType(cpi, BasicUnitType::Special, False);

     bl->nStartInfantry(bl->nStartInfantry() + nInfantry);
     bl->nStartCavalry(bl->nStartCavalry() + nCavalry);
     bl->nStartArtillery(bl->nStartArtillery() + nArtillery);
     bl->nStartOther(bl->nStartOther() + nOther);


     /*
      * If we have no Cinc then add leader as CinC,
      * otherwise, unless SHQ, leader with highest rankrating(nobility) is  CinC
      */

     setCinC(side);
     return bu;
   }

   return 0;
}

void CampaignBattle::removeUnit(ICommandPosition cpi)
{
  ASSERT(cpi != NoCommandPosition);
  CampaignBattleUnitListIterW iter(&d_units[cpi->getSide()]);
  while(++iter)
  {
    CampaignBattleUnit* bu = iter.current();
    if(bu->getUnit() == cpi)
    {
      bu->outOfIt(True);
      break;
    }
  }
}

/*
 * Set sides commander in chief
 *
 * If we have no Cinc then add leader as CinC,
 * otherwise, unless SHQ, leader with highest rankrating(nobility) is  CinC
 *
// Bugfix: SWG 16Jul99 :
//         If outOfIt() then should not continue
//         onto the other sections because leader may be
//         NULL and leader-> crashes.
//
//          Function simplified...
//          always pick best leader and replace existing
//
//          It may be that the logic is supposed to keep the
//          same cinc if he is still alive.
//          i.e. if(oldCinC == NoLeader && (...)
 */

void CampaignBattle::setCinC(Side s)
{
   CampaignBattleUnitList* bl = &d_units[s];

   // ILeader cinc = bl->cinc();
   ILeader cinc = NoLeader;      // start without a leader

   CampaignBattleUnitListIterR iter(bl, true);  // only live units
   // CampaignBattleUnitListIterR iter(bl, False);
   while(++iter)
   {
      const CampaignBattleUnit* bu = iter.current();
      if(!bu->outOfIt())
      {
         ILeader leader = bu->getUnit()->getLeader();

         if( (leader != NoLeader) &&
             ((cinc == NoLeader) ||
              (leader->isSupremeLeader()) ||
              (d_campData->getArmies().whoShouldCommand(leader, cinc) != cinc)) )
         {
            cinc = leader;
         }
      }
   }

   bl->cinc(cinc);
}

void CampaignBattle::resetLosses()
{
  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    d_units[s].resetLosses();
  }
}

/*
 * Is side defending a chokepoint?
 */


/*
 * Is side moving?
 */

bool CampaignBattle::sideIsMoving(Side s) const
{
  CampaignBattleUnitListIterR iter(&d_units[s]);
  while(++iter)
  {
    const CampaignBattleUnit* bu = iter.current();

    CommandPosition* cp = d_campData->getCommand(bu->getUnit());

    if(cp->shouldMove())
      return True;
  }

  return False;
}

/*
 * Find out if a unit is involved in a battle
 */

bool CampaignBattle::containsUnit(ICommandPosition cpi)  const
{
   ASSERT(cpi != NoCommandPosition);

   CommandPosition* cp = d_campData->getCommand(cpi);

   CampaignBattleUnitListIterR iter(&d_units[cp->getSide()]);
   while(++iter)
   {
      const CampaignBattleUnit* bu = iter.current();

      if(bu->getUnit() == cpi && !bu->outOfIt())
         return True;
   }

   return False;
}


void CampaignBattle::setRatio()
{
  const Side side0 = 0;
  const Side side1 = 1;

  int side0Value = d_units[side0].combatManpower();//spValue();
  int side1Value = d_units[side1].combatManpower();//spValue();

  int greater = (side0Value > side1Value) ?  side0Value : side1Value;
  int lesser = (side0Value > side1Value) ?  side1Value : side0Value;

  int ratio = (lesser > 0) ? greater/lesser : greater;
  d_ratio =  static_cast<Ratio>(minimum<int>(ratio, Ratio_8To1));
}

/*
 * file interface
 */

inline bool operator >> (FileReader& f, CampaignBattle::BattleTurnMode::Value& m)
{
   UBYTE b;
   f >> b;

   m = static_cast<CampaignBattle::BattleTurnMode::Value>(b);
   return true;
}

inline bool operator << (FileWriter& f, const CampaignBattle::BattleTurnMode::Value& m)
{
   return f.putUByte(m);
}

inline bool operator >> (FileReader& f, BattleMode::Mode& m)
{
   UBYTE b;
   f >> b;

   m = static_cast<BattleMode::Mode>(b);
   return true;
}

inline bool operator << (FileWriter& f, const BattleMode::Mode& m)
{
   return f.putUByte(m);
}

inline bool operator >> (FileReader& f, CampaignBattle::Ratio& r)
{
   UBYTE b;
   f >> b;

   r = static_cast<CampaignBattle::Ratio>(b);
   return true;
}

inline bool operator << (FileWriter& f, const CampaignBattle::Ratio& r)
{
   return f.putUByte(r);
}

const UWORD s_campBattleVersion = 0x0002;
bool CampaignBattle::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_campBattleVersion);

  // read unit list
  for(Side s = 0; s < NumberSides; s++)
  {
    d_units[s].read(f, ob);
  }

  // modes
  f >> d_turnMode;
  f >> d_mode;

  //flags
  f >> d_flags;

  // misc
  f >> d_ratio;
  f >> d_victor;

  if(version >= 0x0002)
    f >> d_siegeTown;

  CPIndex cpi;
  f >> cpi;
  d_pursuitTarget = CampaignOBUtil::cpIndexToCPI(cpi, ob);
  f >> d_tillWhen;
  if(version >= 0x0001)
    f >> d_nextRoundAt;
  f >> d_battleRound;
  f >> d_daysOfBattle;

  // position
  CampaignPosition::readData(f);

  return f.isOK();
}

bool CampaignBattle::write(FileWriter& f, const OrderBattle& ob) const
{
  f << s_campBattleVersion;

  // write unit list
  for(Side s = 0; s < NumberSides; s++)
  {
    d_units[s].write(f, ob);
  }

  // modes
  f << d_turnMode;
  f << d_mode;

  //flags
  f << d_flags;

  // misc
  f << d_ratio;
  f << d_victor;
  f << d_siegeTown;
  CPIndex cpi = CampaignOBUtil::cpiToCPIndex(d_pursuitTarget, ob);
  f << cpi;
  f << d_tillWhen;
  f << d_nextRoundAt;
  f << d_battleRound;
  f << d_daysOfBattle;

  // position
  CampaignPosition::writeData(f);

  return f.isOK();
}

/*==================================================================
 *  Battle unit list iterator
 */

bool CampaignBattleUnitListIter::operator ++()
{
   /*
    * Version using iterator
    */

   while(++d_iter)
   {
      if(!d_inBattleOnly)
         return true;

      CampaignBattleUnit* bu = d_iter.current();

      if(!bu->outOfIt())
         return true;
   }
   return false;
}

/*==================================================================
 * Campaign Battle List functions
 */

CampaignBattleList::CampaignBattleList(CampaignData* campData) :
  d_playersBattle(0),
  d_campData(campData)
{
}

/*
 * Create new battle
 */


CampaignBattle* CampaignBattleList::createBattle(const CampaignPosition* pos)
{
   CampaignBattle* battle = new CampaignBattle(pos, d_campData);
   {
      Writer lock(this);
      append(battle);
   }
   return battle;
}


/*
 * Find a battle containing a particular unit
 */

CampaignBattle* CampaignBattleList::findBattle(ICommandPosition cpi)
{
   CampaignBattleIter iter(this);

   while(++iter)
   {
      if(iter->containsUnit(cpi))
         return iter.current();
   }

   FORCEASSERT("Unit Not in any Campaign Battle");

   return 0;
}

/*
 * See if a particular unit is in a battle
 */

bool CampaignBattleList::isInABattle(ICommandPosition cpi)
{
   CampaignBattleIter iter(this);

   while(++iter)
   {
      if(iter->containsUnit(cpi))
         return True;
   }

   return False;
}

/*
 * Remove a particular unit from a battle
 */

void CampaignBattleList::removeFromBattle(ICommandPosition cpi)
{
   CampaignBattleIter iter = this;
   while(++iter)
   {
      if(iter->containsUnit(cpi))
         iter.current()->removeUnit(cpi);
   }
}

void CampaignBattleList::finishBattle(CampaignBattle* battle)
{
   ASSERT(!battle->isFinished());

   if(battle == d_playersBattle)
      d_playersBattle = 0;
   battle->setMode(BattleMode::Finished);
}

/*
 * Remove given battle from list
 */

void CampaignBattleList::removeBattle(CampaignBattle* battle)
{
   Writer lock(this);

   finishBattle(battle);

   // Use SList's built in iterator

   rewind();
   while(++(*this))
   {
      if(getCurrent() == battle)
      {
         remove();
         return;
      }
   }

   // Should not get here

   FORCEASSERT("CampaignBattle not in CampaignBattleList");
}

int CampaignBattleList::getID(const CampaignBattle* battle) const
{
    CampaignBattleIterR iter(this);
    int i = 0;
    while(++iter)
    {
       if(iter.current() == battle)
       {
            return i;
       }

       i++;
    }

    return -1;
}

CampaignBattle* CampaignBattleList::getFromID(int id)
{
    CampaignBattleIter iter(this);
    int i = 0;
    while(++iter)
    {
       if(i == id)
       {
            return iter.current();
       }

       i++;
    }
    return 0;
}


const UWORD s_campBattleListVersion = 0x0000;
static const char chunkName[] = "BattleChunkName";
bool CampaignBattleList::read(FileReader& f, OrderBattle& ob, CampaignData* campData)
{
  if(f.getMode() == SaveGame::SaveGame)
  {
    FileChunkReader fr(f, chunkName);
    if(!fr.isOK())
      return False;

    UWORD version;
    f >> version;
    ASSERT(version <= s_campBattleListVersion);

    // read battle list
    int nBattles;
    f >> nBattles;

    if(nBattles > 0)
    {
      while(nBattles--)
      {
        CampaignBattle* battle = new CampaignBattle(campData);
        ASSERT(battle);

        battle->read(f, ob);
        append(battle);
      }

      // read players battle. convert from index
      bool hasPlayerBattle;
      f >> hasPlayerBattle;

      if(hasPlayerBattle)
      {
        int index;
        f >> index;

        CampaignBattleIter iter(this);
        int i = 0;
        while(++iter)
        {
          if(i == index)
          {
            d_playersBattle = iter.current();
            break;
          }

          i++;
        }

        ASSERT(d_playersBattle);
      }
    }
  }

  return f.isOK();
}

bool CampaignBattleList::write(FileWriter& f, const OrderBattle& ob) const
{
  if(f.getMode() == SaveGame::SaveGame)
  {
    FileChunkWriter fc(f, chunkName);

    f << s_campBattleListVersion;

    // write battle list
    int nBattles = entries();
    f << nBattles;

    if(nBattles > 0)
    {
      CampaignBattleIterR iter = this;
      while(++iter)
      {
        iter.current()->write(f, ob);
      }

      // write players battle. Convert to an index
      bool hasPlayerBattle = (d_playersBattle != 0);
      f << hasPlayerBattle;

      if(hasPlayerBattle)
      {
        int index = 0;
        CampaignBattleIterR iter = this;
        while(++iter)
        {
          if(iter.current() == d_playersBattle)
            break;

          index++;
        }

        f << index;
      }
    }
  }
  return f.isOK();
}

/*==================================================================
 * Campaign Battle List Iterator functions
 */

/*
 * Remove current battle
 */


void CampaignBattleIter::removeBattle()
{
    Writer lock(d_container);

   d_iter.remove();
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
