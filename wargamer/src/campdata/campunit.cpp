/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Campaign Unit Movement
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campunit.hpp"
// #include "town.hpp"
#include "fsalloc.hpp"
#include "res_str.h"
#include "resstr.hpp"
#include "c_obutil.hpp"
#include "filebase.hpp"
#include "misc.hpp"
#include "CRC.hpp"

#if !defined(EDITOR)

// strings
static int s_modeStringIDs[CampaignMovement::CPM_HowMany] = {
   IDS_None,
   IDS_CU_HOLDING,
   IDS_CU_RESTING,
   IDS_CU_RALLYING,
   IDS_CU_MOVING,
   IDS_CU_SIEGING,
   IDS_ORDER_GARRISON,
   IDS_CU_RAIDING,
   IDS_CU_WAITBATTLE,
   IDS_CU_MOVEINTOBATTLE,
   IDS_CU_PURSUING,
   IDS_CU_WITHINRANGE,
   IDS_CU_HALTNEARENEMY,
   IDS_CU_RETREATFROMENEMY,
   IDS_CU_DISTANTENEMYRANGE,
   IDS_CU_HALTAWAYFROMENEMY,
   IDS_CU_AVOIDING,
   IDS_CU_STARTWITHDRAW,
   IDS_CU_WITHDRAWING,
   IDS_CU_INBATTLE,
   IDS_CU_STARTRETREAT,
   IDS_CU_RETREATING,
   IDS_CU_REGROUPING,
   IDS_CU_INSTALLSUPPLY,
   IDS_CU_INSTALLFORT,
   IDS_CU_SURRENDERING,
};

static ResourceStrings s_modeStrings(s_modeStringIDs, CampaignMovement::CPM_HowMany);

/*
 * Table of properties for each CampaignMovement mode
 *
 *  willDoBattle, isAvoiding, shouldMove, outOfIt, retreating, finishedBattle, normalActivity, description
 */

const CampaignMovement::ModeTable CampaignMovement::movementModeTable[CPM_HowMany] = {
   { False, False, False, False, False, False,  True, s_modeStrings.get(CPM_None)                      },   // CPM_None
   { False, False, False, False, False, False,  True, s_modeStrings.get(CPM_Holding)                   },   // CPM_Holding
   { False, False, False, False, False, False,  True, s_modeStrings.get(CPM_Resting)                   },   // CPM_Resting
   { False, False, False, False, False, False,  True, s_modeStrings.get(CPM_Rallying)                  },   // CPM_Rallying
   { False, False,  True, False, False, False,  True, s_modeStrings.get(CPM_Moving)                 },   // CPM_Moving
   { False, False, False, False, False, False,  True, s_modeStrings.get(CPM_Sieging)                   },   // CPM_Sieging
   { False, False, False,  True, False, False,  True, s_modeStrings.get(CPM_Garrison)               },   // CPM_Garrison
   { False, False, False, False, False, False,  True, s_modeStrings.get(CPM_Raiding)                   },   // CPM_Raiding
   {  True, False, False, False, False, False, False, s_modeStrings.get(CPM_AcceptBattle)           },   // CPM_AcceptBattle
   {  True, False,  True, False, False, False, False, s_modeStrings.get(CPM_IntoBattle)                },   // CPM_IntoBattle
   { False, False,  True, False, False, False, False, s_modeStrings.get(CPM_Pursuing)                  },  // CPM_Pursue
   { False,  True,  True, False, False, False,  True, s_modeStrings.get(CPM_AvoidContact)            },  // CPM_AvoidContact
   { False,  True, False, False, False, False,  True, s_modeStrings.get(CPM_AvoidContactHold)        },  // CPM_AvoidContactHold
   { False,  True,  True, False,  True, False,  True, s_modeStrings.get(CPM_AvoidContactRetreat)       },   // CPM_AvoidContactRetreat
   { False,  True,  True, False, False, False,  True, s_modeStrings.get(CPM_AvoidContactFast)          },   // CPM_AvoidContactFast
   { False,  True, False, False, False, False,  True, s_modeStrings.get(CPM_AvoidContactFastHold)   },   // CPM_AvoidContactFastHold
   { False,  True,  True, False,  True, False,  True, s_modeStrings.get(CPM_AvoidContactFastRetreat) },  // CPM_AvoidContactFastRetreat
   { False,  True,  True,  True,  True, False,  True, s_modeStrings.get(CPM_StartingWithdraw)          },   // CPM_StartingWithdraw
   { False,  True,  True,  True,  True, False,  True, s_modeStrings.get(CPM_Withdrawing)            },   // CPM_Withdrawing
   { False, False, False,  True, False, False,  True, s_modeStrings.get(CPM_InBattle)               },   // CPM_InBattle
   { False,  True,  True,  True,  True, False,  True, s_modeStrings.get(CPM_StartingRetreat)        },   // CPM_StartingRetreat
   { False,  True,  True,  True,  True, False,  True, s_modeStrings.get(CPM_Retreating)                },   // CPM_Retreating
   { False,  True, False,  True, False, True,   True, s_modeStrings.get(CPM_AfterWinningBattle)     },   // CPM_AfterWinningBattle
   { False, False, False, False, False, False,  True, s_modeStrings.get(CPM_InstallSupply)             },   // CPM_InstallingSupply
   { False, False, False, False, False, False,  True, s_modeStrings.get(CPM_InstallFort)            }, // CPM_InstallingSupply
   { False, False, False,  True, False, False,  True, s_modeStrings.get(CPM_Surrendering)            }  // CPM_Surrendering
};
#endif

/*
 * CampaignMovement Class functions
 */

CampaignMovement::CampaignMovement() :
   d_mode(CPM_None),
// d_modeTime(0),
// d_modeStartTime(0),
   d_nextDest(NoTown),
   d_nextTarget(NoCommandPosition),
   d_enemyRatio(FR_None),
   d_flags(0),
   d_retreatTown(NoTown),
   d_activityCount(0),
   d_takingThisTown(NoTown),
   d_hoursResting(0)
{
}

Boolean CampaignMovement::setMode(CP_Mode newMode)
{
   Boolean result = (newMode != d_mode);
   if(result)
   {
     d_mode = newMode;
//   d_modeTime = 0;
//   d_modeStartTime = 0;
   }

   return result;
}

#if !defined(EDITOR)
Boolean CampaignMovement::shouldMove() const
{
   ASSERT(d_mode >= 0);
   ASSERT(d_mode < CPM_HowMany);
   return movementModeTable[d_mode].shouldMove;
};

Boolean CampaignMovement::willDoBattle() const
{
   ASSERT(d_mode >= 0);
   ASSERT(d_mode < CPM_HowMany);
   return movementModeTable[d_mode].willDoBattle;
}

Boolean CampaignMovement::isAvoiding() const
{
   ASSERT(d_mode >= 0);
   ASSERT(d_mode < CPM_HowMany);
   return movementModeTable[d_mode].isAvoiding;
}

Boolean CampaignMovement::shouldCheckEnemy() const
{
   ASSERT(d_mode >= 0);
   ASSERT(d_mode < CPM_HowMany);
   return (!movementModeTable[d_mode].outOfIt);// && !movementModeTable[mode].finishedBattle);
};

Boolean CampaignMovement::isRetreating() const
{
   ASSERT(d_mode >= 0);
   ASSERT(d_mode < CPM_HowMany);
   return movementModeTable[d_mode].retreating;
}

Boolean CampaignMovement::isAfterBattle() const
{
   ASSERT(d_mode >= 0);
   ASSERT(d_mode < CPM_HowMany);
   return movementModeTable[d_mode].finishedBattle;
}

Boolean CampaignMovement::isNormalActivity(Side s) const
{
   ASSERT(d_mode >= 0);
   ASSERT(d_mode < CPM_HowMany);
   return (movementModeTable[d_mode].normalActivity && (d_nextTarget == NoCommandPosition || d_nextTarget->getSide() == s));
}

void CampaignMovement::adjustActivityCount(TimeTick theTime, Side s)
{
   ASSERT(d_mode >= 0);
   ASSERT(d_mode < CPM_HowMany);


   enum AdjustBy {
     LessThan3Hours,
     LessThan5Hours,
     LessThan7Hours,
     LessThan9Hours,
     Over9Hours,

     AdjustBy_HowMany
   } adjustBy;


   int hoursSinceStarted = (theTime - d_activityTime)/TicksPerHour;

   if(hoursSinceStarted < 3)
     adjustBy = LessThan3Hours;
   else if(hoursSinceStarted < 5)
     adjustBy = LessThan5Hours;
   else if(hoursSinceStarted < 7)
     adjustBy = LessThan7Hours;
   else if(hoursSinceStarted < 9)
     adjustBy = LessThan9Hours;
   else
     adjustBy = Over9Hours;

   static const int table[AdjustBy_HowMany] = {
     0, 5, 10, 15, 20
   };

   /*
    * if a normal mode decrement counter
    */

   if(isNormalActivity(s))
     d_activityCount = clipValue<int>(d_activityCount-table[adjustBy], 0, Attribute_Range);

   /*
    * else increment
    */

   else
     d_activityCount = clipValue<int>(d_activityCount+table[adjustBy], 0, Attribute_Range);

}
#endif // !defined(EDITOR)

/*
 * CampaignMovement file interface
 */

inline bool operator >> (FileReader& f, CampaignMovement::CP_Mode& m)
{
   UBYTE b;
   f >> b;

   m = static_cast<CampaignMovement::CP_Mode>(b);
   return true;
}

inline bool operator << (FileWriter& f, const CampaignMovement::CP_Mode& m)
{
   return f.putUByte(m);
}

inline bool operator >> (FileReader& f, ForceRatio& fr)
{
   UBYTE b;
   f >> b;

   fr = static_cast<ForceRatio>(b);
   return true;
}

inline bool operator << (FileWriter& f, const ForceRatio& fr)
{
   return f.putUByte(fr);
}

const UWORD s_campMoveVersion = 0x0003;
Boolean CampaignMovement::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;

  f >> version;
  ASSERT(version <= s_campMoveVersion);

  f >> d_mode;

  d_aggressionValues.read(f);
  if(version >= 0x0001)
    d_currentRoute.read(f);
  d_previousRoute.read(f);
  if(version >= 0x0002)
    d_supplyRoute.read(f);

  f >> d_enemyRatio;
  f >> d_flags;
  f >> d_nextDest;
  f >> d_takingThisTown;
  f >> d_retreatTown;

  CPIndex cpi;
  f >> cpi;
  d_nextTarget = CampaignOBUtil::cpIndexToCPI(cpi, ob);

  f >> d_activityCount;
  f >> d_activityTime;
  if (version < 0x0003)
  {
      TimeTick modeStartTime;
      TimeTick modeTime;
      f >> modeStartTime;
      f >> modeTime;
  }
  f >> d_hoursResting;

  return f.isOK();
}

Boolean CampaignMovement::write(FileWriter& f, OrderBattle& ob) const
{
  f << s_campMoveVersion;

  f << d_mode;

  d_aggressionValues.write(f);
  d_currentRoute.write(f);
  d_previousRoute.write(f);
  d_supplyRoute.write(f);

  f << d_enemyRatio;
  f << d_flags;
  f << d_nextDest;
  f << d_takingThisTown;
  f << d_retreatTown;

  CPIndex cpi = CampaignOBUtil::cpiToCPIndex(d_nextTarget, ob);
  f << cpi;

  f << d_activityCount;
  f << d_activityTime;
//  f << d_modeStartTime;
//  f << d_modeTime;
  f << d_hoursResting;

  return f.isOK();
}




unsigned short
CampaignMovement::calculateCRC(void) {

   unsigned short crc = 0;

   // aggression values
   crc = d_aggressionValues.calculateCRC();

   // d_currentRoute
   RouteListIterR currentRouteIter(&d_currentRoute);
   if(currentRouteIter.next()) {

      do {

         crc = addIntCRC(crc, (unsigned int) currentRouteIter.getCurrent()->d_town);

      } while(currentRouteIter.next());
   }

   // d_previousRoute
   RouteListIterR previousRouteIter(&d_previousRoute);
   if(previousRouteIter.next()) {

      do {

         crc = addIntCRC(crc, (unsigned int) previousRouteIter.getCurrent()->d_town);

      } while(previousRouteIter.next());
   }

   // d_supplyRoute
   RouteListIterR supplyRouteIter(&d_supplyRoute);
   if(supplyRouteIter.next()) {

      do {

         crc = addIntCRC(crc, (unsigned int) supplyRouteIter.getCurrent()->d_town);

      } while(supplyRouteIter.next());
   }

   // other values
   crc = addByteCRC(crc, (unsigned char) d_flags);
   crc = addIntCRC(crc, (unsigned int) d_nextDest);
   crc = addIntCRC(crc, (unsigned int) d_takingThisTown);
   crc = addIntCRC(crc, (unsigned int) d_retreatTown);
   if(d_nextTarget != NoCommandPosition) crc = addIntCRC(crc, (unsigned int) d_nextTarget->getSelf());
   crc = addByteCRC(crc, (unsigned char) d_activityCount);
   crc = addIntCRC(crc, (unsigned int) d_activityTime);
   crc = addByteCRC(crc, (unsigned char) d_hoursResting);


   return crc;
}



/*
 * Command Position Action:
 */

enum {
  Completed,

  StringID_HowMany
};

static int s_stringIDs[StringID_HowMany] = {
  IDS_CU_MISC_COMPLETED
};

static ResourceStrings s_strings(s_stringIDs, StringID_HowMany);

#if !defined(EDITOR)
const char* CampaignMovement::getActionDescription(OD_TYPE how, TimeTick theTime) const
{
   static SimpleString s;

   CP_Mode mode = getMode();
   ASSERT(mode < CPM_HowMany);

   s = movementModeTable[mode].modeName;

//    if(d_modeTime != 0)
//    {
//       char buf[100];
//       wsprintf(buf, " %d%% %s",
//         static_cast<int>(modeTimeCompletion(theTime, 100)),
//         s_strings.get(Completed));
//
//       s += buf;
//    }

   ASSERT(s.toStr());
   return s.toStr();
}
#endif // !defined(EDITOR)

#if 0
static void CampaignMovement::clearActionDescription()
{
   actionText.release();
}
#endif

#if 0
/*
 * Set mode time to the current time + t
 *
 * e.g. To set modeTime for 2 days time:
 *    setModeTime(DaysToTicks(2));
 */

void CampaignMovement::setModeTime(TimeTick theTime, TimeTick tilWhen)
{
   d_modeStartTime = theTime;
   d_modeTime = d_modeStartTime + tilWhen;
}

Boolean CampaignMovement::modeTimeFinished(TimeTick theTime)
{
   if(theTime >= d_modeTime)
   {
      d_modeTime = 0;
      d_modeStartTime = 0;
      return True;
   }
   else
      return False;
}

/*
 * Return how far through completion of task unit is
 *  0 = Just started
 *  range = Finished
 *
 * e.g. specify range as 255 (MAX_UBYTE) returns 0..255
 */

unsigned int CampaignMovement::modeTimeCompletion(TimeTick theTime, unsigned int range) const
{
   ASSERT(d_modeTime != d_modeStartTime);

// TimeTick cTick = campaignData->getTick();
   TimeTick cTick = theTime;

   ASSERT(cTick >= d_modeStartTime);

   if( (d_modeTime == d_modeStartTime) || (cTick >= d_modeTime) )
      return range;
   else
      return ( (cTick - d_modeStartTime) * range) / (d_modeTime - d_modeStartTime);
}
#endif

/*-------------------------------------------------------------------
 * AggressionValues
 */

inline bool operator >> (FileReader& f, Orders::Aggression::Value& q)
{
   UBYTE b;
   f >> b;

   q = static_cast<Orders::Aggression::Value>(b);
   return true;
}

inline bool operator << (FileWriter& f, const Orders::Aggression::Value& q)
{
   return f.putUByte(q);
}

inline bool operator >> (FileReader& f, Orders::Posture::Type& p)
{
   UBYTE b;
   f >> b;

   p = static_cast<Orders::Posture::Type>(b);
   return true;
}

inline bool operator << (FileWriter& f, const Orders::Posture::Type& p)
{
   return f.putUByte(p);
}

const UWORD s_avFileVersion = 0x0000;
Boolean CampaignMovement::AggressionValues::read(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_avFileVersion);

  f >> d_activeAggression;
  f >> d_posture;
  f >> d_lockedUntil;
  f >> d_isLocked;

  return f.isOK();
}

Boolean CampaignMovement::AggressionValues::write(FileWriter& f) const
{
  f << s_avFileVersion;

  f << d_activeAggression;
  f << d_posture;
  f << d_lockedUntil;
  f << d_isLocked;

  return f.isOK();
}



unsigned short CampaignMovement::AggressionValues::calculateCRC(void) {

   unsigned short crc = 0;

   crc = addIntCRC(crc, (unsigned int) d_activeAggression);
   crc = addIntCRC(crc, (unsigned int) d_posture);
   crc = addIntCRC(crc, (unsigned int) d_lockedUntil);
   crc = addByteCRC(crc, (unsigned char) d_isLocked);

   return crc;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
