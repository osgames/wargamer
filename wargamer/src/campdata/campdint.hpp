/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPDINT_HPP
#define CAMPDINT_HPP

#ifndef __cplusplus
#error campdint.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Campaign Data Interface (equivalent to old campdata.hpp)
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "measure.hpp"
#include "cpdef.hpp"
#include "simpstr.hpp"

#ifdef DEBUG
#include "clog.hpp"
#endif

#define GLOBAL_CAMPDATA

// Allow getCommand() functiont
#define BACKWARD_COMPATIBLE

/*
 * Forward References
 */

class TownList;
class Town;
class ProvinceList;
class Province;
class ConnectionList;
struct Connection;
class Armies;
class CommandPosition;
class Leader;
class UnitTypeTable;
class UnitTypeItem;
class TerrainTypeTable;
class TerrainTypeItem;
class FileReader;
class FileWriter;
class CampaignBattleList;
class CampaignPosition;
// class String;
class CampaignTimeData;
class CampaignInfo;
namespace WG_CampaignConditions { class ConditionList; };
class NationsList;
class GameVictory;
class CampaignWeather;
//class TotalLosses;
class CampaignSides;
class IndependentLeaderList;
class CapturedLeaderList;
class CampaignBattle;
class CampaignInterface;
class CampaignSoundSystem;

/*
 * Virtual Class Definition
 */

class CampaignData
{
   private:
        // Disable compiler generated copying

        CampaignData(const CampaignData&);
        CampaignData& operator = (const CampaignData&);

public:
        CampaignData() { }
        virtual ~CampaignData() { }

        // virtual void create() = 0;

        /*
         * Towns
         */

        virtual TownList& getTowns() = 0;
        virtual const TownList& getTowns() const = 0;
        virtual const Town& getTown(ITown i) const = 0;
        virtual Town& getTown(ITown i) = 0;
#if defined(CUSTOMIZE)
        virtual void removeTown(ITown ti) = 0;
        virtual void swapTowns(ITown t1, ITown t2) = 0;
        virtual void sortTowns() = 0;
        virtual void setCapital(IProvince iProv, ITown iTown) = 0;
        virtual void moveTownToProvince(IProvince iProv, ITown iTown) = 0;
#endif
        virtual SPCount getTownStrength(ITown itown, Side s) const = 0;

        virtual IProvince getTownProvince(ITown t) const = 0;

        virtual const char* getTownName(ITown i) const = 0;
        virtual const char* getTownName(ITown i, const char* defName) const = 0;

        /*
         * Provinces
         */

        virtual ProvinceList& getProvinces() = 0;
        virtual const ProvinceList& getProvinces() const = 0;
        virtual Province& getProvince(IProvince i) = 0;
        virtual const Province& getProvince(IProvince i) const = 0;
#if defined(CUSTOMIZE)
        virtual void removeProvince(IProvince pi) = 0;
#endif

        /*
         * Connections
         */

        virtual const ConnectionList& getConnections() const = 0;
        virtual ConnectionList& getConnections() = 0;
        virtual const Connection& getConnection(IConnection i) const = 0;
        virtual Connection& getConnection(IConnection i) = 0;
#if defined(CUSTOMIZE)
        virtual void removeConnection(IConnection ti) = 0;
#endif

        virtual Distance getConnectionLength(IConnection i) const = 0;
        virtual void calculateConnectionLength(IConnection i) = 0;
        virtual void getConnectionLocation(IConnection connection, Distance distAlong, Location& l) const = 0;

        /*
         * CommandPositions
         */

        virtual Armies& getArmies() = 0;
        virtual const Armies& getArmies() const = 0;
        virtual int getSideCount() const = 0;
#ifdef BACKWARD_COMPATIBLE
        CommandPosition* getCommand(const ICommandPosition& cpi) { return cpi; }
        const CommandPosition* getCommand(const ConstICommandPosition& cpi) const { return cpi; }
        const CommandPosition* getCommand(const ICommandPosition& cpi) const { return cpi; }
        Leader* getLeader(ILeader li) { return li; }
        const Leader* getLeader(ILeader li) const { return li; }
#endif
        virtual SimpleString getUnitName(ConstICommandPosition cpi) const = 0;

        /*
         * Unattached leaders
         */

//        virtual void addIndependentLeader(ILeader l) = 0;
        virtual const IndependentLeaderList& independentLeaders() const = 0;
        virtual IndependentLeaderList& independentLeaders() = 0;

   virtual void addCapturedLeader(ILeader l) = 0;
        virtual const CapturedLeaderList& capturedLeaders() const = 0;
        virtual CapturedLeaderList& capturedLeaders() = 0;

        /*
         * Unit Types
         */

        virtual const UnitTypeTable& getUnitTypes() const = 0;
        virtual int getMaxUnitType() const = 0;
        virtual const UnitTypeItem& getUnitType(UnitType n) const = 0;

        /*
         * TerrainTypes
         */

        virtual const TerrainTypeTable& getTerrainTypes() const = 0;
        virtual int getMaxTerrainType() const = 0;
        virtual const TerrainTypeItem& getTerrainType(TerrainType n) const = 0;
        virtual Boolean isChokePoint(ITown t) const = 0;
        virtual Boolean isSameSideOfChokePoint(IConnection con1, IConnection con2) const = 0;

#if !defined(EDITOR)
        /*
         * Battles
         */

        virtual CampaignBattleList& getBattleList() = 0;
        virtual const CampaignBattleList& getBattleList() const = 0;

        /*
         * Victory level functions
         */

        virtual void resetVictory() = 0;
				virtual void addVictoryLevel(ULONG points, Side side) = 0;
				virtual void removeVictoryLevel(LONG points, Side side) = 0;
        virtual LONG getVictoryLevel(Side side) const = 0;

        /*
         * Weather functions
         */

        virtual CampaignWeather& getWeather() = 0;
        virtual const CampaignWeather& getWeather() const = 0;

        /*
         *  Unit Losses
         */

        virtual CampaignSides& getSideData() = 0;
        virtual const CampaignSides& getSideData() const = 0;

        /*
         * Armistice
         */

   virtual Boolean armisticeInEffect() const = 0;
        virtual void startArmistice(TimeTick tillWhen) = 0;
        virtual void updateArmistice(TimeTick theTime) = 0;

        /*
         * GameVictory
         */

        virtual Boolean hasVictor() const = 0;
        virtual void setVictory(const GameVictory& v) = 0;
        virtual const GameVictory& getGameVictory() const = 0;
        // virtual GameVictory& getGameVictory() = 0;

        /*
         * Side Random Events;
         */

        virtual void setSHQInChaos(Side side, TimeTick tilWhen) = 0;
        virtual Boolean isSHQInChaos(Side side, TimeTick theTime) const = 0;
        virtual void setEnemyPlansCaptured(Side side, TimeTick tilWhen) = 0;
   virtual Boolean isEnemyPlansCaptured(Side side, TimeTick theTime) const = 0;

#endif

        /*
         * Changeable Nations data
         */

        virtual NationsList& getNations() = 0;
        virtual const NationsList& getNations() const = 0;
        virtual Side getNationAllegiance(Nationality n) const = 0;
        virtual Boolean isNationActive(Nationality n) const = 0;
        virtual Boolean shouldNationEnter(Nationality n) = 0;
        virtual Attribute getNationMorale(Nationality n) const = 0;

        virtual void nationNeverEnters(Nationality n) = 0;
        virtual void setNationAllegiance(Nationality n, Side s) = 0;
        virtual void setNationMorale(Nationality n, Attribute m) = 0;

        /*
         *  Conditions
         */

        virtual WG_CampaignConditions::ConditionList& getConditions() = 0;


  /*
        * Campaign Info for startscreens
        */

  virtual CampaignInfo& getCampaignInfo() = 0;
  virtual void setCampaignInfo(const CampaignInfo& info) = 0;

        /*
         * File Interface
         */

        virtual Boolean readData(FileReader& f) = 0;
        virtual Boolean writeData(FileWriter& f) const = 0;

        virtual CampaignTimeData* getTimeData() = 0;
        virtual const CampaignTimeData* getTimeData() const = 0;

        /*
         * time and date
         */

        virtual const CampaignTime& getCTime() const = 0;
        virtual const Date& getDate() const = 0;
#if 0   // Part of CampaignTimeData class
        virtual const Time& getTime() const = 0;
        // virtual void initTime(const Date& startDate, ULONG timeValue) = 0;
        virtual void initTime(const Date& date) = 0;
#endif
        virtual void setTimeTick(TimeTick t) = 0;
        virtual const char* asciiTime(char* buffer = 0) const = 0;
#if !defined(EDITOR)
        virtual TimeTick getTick() const = 0;
#endif

        /*
         * Support functions
         */

        virtual void setChanged() = 0;
        virtual void clearChanged() = 0;
        virtual Boolean hasChanged() const = 0;

#if !defined(EDITOR)
        virtual void setLastSeenLocation(ICommandPosition cpi) = 0;


        virtual Boolean isTownOrderable(ITown iTown) const = 0;

        virtual Boolean isPlayerSide(Side side) const = 0;

        virtual void killLeader(ICommandPosition cpi) = 0;

#ifdef DEBUG
        virtual void getCampaignPositionString(const CampaignPosition* pos, String& s) const = 0;
#endif


#endif  // !EDITOR

        virtual CampaignInterface* game() = 0;

        /*
        Sound functions
        */

        virtual CampaignSoundSystem * soundSystem() const = 0;

// #if !defined(EDITOR)
//         virtual bool isTimeRunning() const = 0;
//         virtual void stopTime() = 0;
//         virtual void startTime() = 0;
// #endif

};

#if defined(GLOBAL_CAMPDATA)
/*
 * Global Reference to Campaign
 */

CAMPDATA_DLL extern CampaignData* campaignData;              // Should be obsolete!
#endif  // GLOBAL_CAMPDATA

#endif /* CAMPDINT_HPP */

