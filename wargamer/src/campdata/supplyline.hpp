/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef SupplyLine_HPP
#define SupplyLine_HPP

/*
 * Calculate Supply Lines
 */

#include "cdatadll.h"
#include "gamedefs.hpp"

class CampaignData;


class CAMPDATA_DLL CampaignSupplyLine
{
   public:
      enum
      {
         FullSupply = 0xff,
         Empty = 0
      };

      typedef UBYTE Level;

      struct Node
      {
         Level d_supplyLevel;    // 0 = unsupplied, 255=fullysupplied
      };


      CampaignSupplyLine();
      ~CampaignSupplyLine();

      void calculate(const CampaignData* campData, Side side, bool includeConstruction);
         // Side: IF Neutral then all supply depots/sources can be used
         //       ELSE only count source/depot of given side
         // construction: depots under construction are counted as being completed
         // allowSieged: towns under siege are included in the supply chain.

      Level operator[](ITown itown) const;


   private:
      Node* d_nodes;
#ifdef DEBUG
      const CampaignData* d_campData;
#endif
};















#endif   // SupplyLine_HPP

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
