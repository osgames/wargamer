/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Morale Utilities
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "morale.hpp"
#include "armies.hpp"

Attribute MoraleUtility::calcMorale(const Armies* army, ConstICommandPosition cp)
{
   /*
    * Add up directly attached SPs
    */

   long total = 0;
   int nSP = 0;

   ConstStrengthPointIter spIter(army, cp);
   while(++spIter)
   {
      ConstISP sp = spIter.current();
      const UnitTypeItem& uti = army->getUnitType(sp->getUnitType());
      total += uti.getBaseMorale();
      nSP++;
   }

   /*
    * Add up children
    */

   ConstUnitIter cpIter(army, cp->getChild(), true);
   while(cpIter.sister())
   {
      ConstICommandPosition child = cpIter.current();

      int count = army->getUnitSPCount(child, true);

      total += child->getMorale() * count;
      nSP += count;
   }

   if(nSP != 0)
   {
      total = (total + nSP/2) / nSP;
      ASSERT(total <= Attribute_Range);
      return static_cast<Attribute>(total);
   }
   else
      return 0;
}

#if defined(CUSTOMIZE)

namespace {
   
struct MoraleTally
{
   long d_total;
   int d_nSP;

   MoraleTally() : d_total(0), d_nSP(0) { }

   Attribute get()
   {
      if(d_nSP != 0)
      {
         long total = (d_total + d_nSP/2) / d_nSP;
         ASSERT(total <= Attribute_Range);
         return static_cast<Attribute>(total);
      }
      else
         return 0;
   }

   void operator += (const MoraleTally& tally)
   {
      d_total += tally.d_total;
      d_nSP += tally.d_nSP;
   }

};


void setDefaultMorale(Armies* army, ICommandPosition cp, MoraleTally* r_tally)
{
   MoraleTally tally;

   ICommandPosition child = cp->getChild();

   while(child != NoCommandPosition)
   {
      MoraleTally childTally;

      setDefaultMorale(army, child, &childTally);
      tally += childTally;

      child = child->getSister();
   }

   if(cp->getMorale() == 0)
   {
      ConstStrengthPointIter spIter(army, cp);
      while(++spIter)
      {
         ConstISP sp = spIter.current();
         const UnitTypeItem& uti = army->getUnitType(sp->getUnitType());
         tally.d_total += uti.getBaseMorale();
         tally.d_nSP++;
      }

      cp->setMorale(tally.get());
   }
   else
   {
      tally.d_total = cp->getMorale();
      tally.d_nSP = army->getUnitSPCount(cp, true);
   }

   if(r_tally)
   {
      *r_tally = tally;
   }
}

};


void MoraleUtility::setDefaultMorales(Armies* army, ICommandPosition cp)
{
   setDefaultMorale(army, cp, 0);
}

#endif   // CUSTOMIZE

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
