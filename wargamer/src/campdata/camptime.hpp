/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPTIME_H
#define CAMPTIME_H

#ifndef __cplusplus
#error camptime.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Campaign Time
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "mytypes.h"
#include "systick.hpp"
#include "datetime.hpp"

using Greenius_System::DateDefinitions;
using Greenius_System::TimeDefinitions;

#define USE_SNIPPETS_DATE

typedef UWORD GameDay;

// typedef Greenius_System::MonthEnum MonthEnum;

// Backwards Compatibility

typedef DateDefinitions::Year Year;
typedef DateDefinitions::Month Month;
typedef DateDefinitions::Day Day;


#define GameDay_MAX UWORD_MAX


struct Date;
struct Time;

/*
 * A GameTick is 1/10 of a second since the start Date
 */

class GameTick :
	public SysTick 		// So that it can have access to the namespace
{
	ULONG value;
public:
	GameTick() { }
	GameTick(ULONG v) { value = v; }
public:
	GameTick& operator = (GameTick t) { value = t.value; return *this; }
	ULONG toULONG() const { return value; }

	// void set(ULONG v) { operator = (v); }
	void set(GameTick t) { operator = (t); }

	GameTick getTime() const { return value % TicksPerDay; }
	GameDay getDate()  const { return GameDay(value / TicksPerDay); }

	CAMPDATA_DLL void getDate(Date* pdate) const;
	CAMPDATA_DLL void getTime(Time* ptime) const;

	void set(GameDay date, GameTick time)
	{
		value = date * TicksPerDay + time.toULONG();
	}

	CAMPDATA_DLL void setDate(GameDay day);
	CAMPDATA_DLL void setDate(const Date* pdate);

	GameTick operator += (GameTick t) { value += t.value; return *this; }

	CAMPDATA_DLL char* toAscii(char* buffer) const;

	friend Boolean operator < (const GameTick& t1, const GameTick& t2);
	friend Boolean operator > (const GameTick& t1, const GameTick& t2);
	friend Boolean operator == (const GameTick& t1, const GameTick& t2);
	friend GameTick operator - (const GameTick& lhs, const GameTick& rhs);
};

inline Boolean operator < (const GameTick& t1, const GameTick& t2)
{
	return t1.value < t2.value;
}

inline Boolean operator == (const GameTick& t1, const GameTick& t2)
{
	return t1.value == t2.value;
}

inline Boolean operator > (const GameTick& t1, const GameTick& t2)
{
	return t1.value > t2.value;
}

inline GameTick operator - (const GameTick& lhs, const GameTick& rhs)
{
	return GameTick(lhs.value - rhs.value);
}

inline Boolean operator <= (const GameTick& t1, const GameTick& t2)
{
	return !(t1 > t2);
}

inline Boolean operator >= (const GameTick& t1, const GameTick& t2)
{
	return !(t1 < t2);
}


class StartYear : public DateDefinitions {
	CAMPDATA_DLL static Year startYear;
protected:
	CAMPDATA_DLL static ULONG startDayNumber;
public:
	CAMPDATA_DLL static void setStartYear(Year y);
	CAMPDATA_DLL static Year getStartYear() { return startYear; }
};

class FileReader;
class FileWriter;

struct Date : public StartYear   //, public DateDefinitions 
{
	Day day;
	Month month;
	Year year;

	Date() { }
	Date(Day d, Month m, Year y) { set(d, m, y); }
	void set(Day d, Month m, Year y) { day = d; month = m; year = y; }


	// void set(GameDay value, const Date& startDate);
	// GameDay get(const Date& startDate) const;
	CAMPDATA_DLL void set(GameDay value);
	CAMPDATA_DLL GameDay get() const;

	CAMPDATA_DLL const char* toAscii(char* buffer);

	CAMPDATA_DLL Boolean operator < (const Date& date);
	CAMPDATA_DLL Boolean operator > (const Date& date);
	Boolean sameMonth(const Date& date) const { return ( (month == date.month) && (year == date.year) ); }

	CAMPDATA_DLL Boolean read(FileReader& f);
	CAMPDATA_DLL Boolean write(FileWriter& f) const;
};

struct Time : public TimeDefinitions {
	Hour hour;
	Minute minute;
	Second second;
	TSecond tenth;

	Time() { }
	Time(Hour h, Minute m, Second s = 0, TSecond t = 0) { set(h, m, s, t); }
	void set(Hour h, Minute m, Second s = 0, TSecond t = 0)
	{
		hour = h;
		minute = m;
		second = s;
		tenth = t;
	}

	CAMPDATA_DLL void set(GameTick value);
	CAMPDATA_DLL GameTick get() const;
};


inline void GameTick::getDate(Date* pdate) const
{
	pdate->set(getDate());
}

inline void GameTick::getTime(Time* ptime) const
{
	ptime->set(getTime());
}

inline void GameTick::setDate(const Date* pdate)
{
	setDate(pdate->get());
}

/*
 * value is stored as a GameTick (1/10 second since start Date)
 */


/*
 * CampaignTime class is unneccessary, but is left in so as not to confuse
 * other code that uses it.
 */

class CampaignTime : public GameTick, public StartYear {
public:
	CampaignTime() : GameTick(0) { }

	CampaignTime& operator = (GameTick ticks)
	{
	   GameTick::operator = (ticks);
	   return *this;
	}

   CampaignTime& operator = (const CampaignTime& t)
   {
      GameTick::operator=(t);
      return *this;
   }
};


CAMPDATA_DLL void timeToAscii(char* buffer, const Date& date, const Time& time);
inline const char* getMonthName(DateDefinitions::Month m, Boolean abrev) { return Greenius_System::Date::getMonthName(m, abrev); }

CAMPDATA_DLL GameTick getSunRise(const Date& date, int latitude);
CAMPDATA_DLL GameTick getSunSet(const Date& date, int latitude);

#endif /* CAMPTIME_H */

