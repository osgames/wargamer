/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPEVT_HPP
#define CAMPEVT_HPP

#ifndef __cplusplus
#error campevt.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign Timer Events
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "sllist.hpp"			// Singly Linked List
#include "sync.hpp"
#include "measure.hpp"

/*
 * Event List, for example for a list of all things that need
 * triggering when time changes
 */

class CampaignEvent : public SLink {
	AutoEvent event;
	TimeTick tick;
public:
	CAMPDATA_DLL CampaignEvent();

	CAMPDATA_DLL void wait();
		// Wait until at least one timeTick has occured

	CAMPDATA_DLL void waitUntil(TimeTick tick);
		// Wait until specified time


	Boolean isSet() { return event.isSet(); }
		// Return True if event has been signalled

	HANDLE handle() { return event.handle(); }

 private:
	void procTick(TimeTick t);
		// Process Tick

	void set();
		// Signal event

	friend class EventList;

};

class EventList : public SListS<CampaignEvent>
{
public:
	CAMPDATA_DLL void process(TimeTick tick);
};


#endif /* CAMPEVT_HPP */

