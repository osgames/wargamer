/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Campaign Orders
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.4  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1996/02/29 11:01:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/23 14:08:35  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/19 16:16:55  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include <windows.h>
#include "campord.hpp"
#include "options.hpp"
#include "fsalloc.hpp"
#include "simpstr.hpp"
#include "wmisc.hpp"
// #include "town.hpp"
#include "compos.hpp"
#include "campdint.hpp"
#include "resstr.hpp"
#include "res_str.h"
#include "filebase.hpp"
#include "c_obutil.hpp"
//#include "ob.hpp"
//#include "cp.hpp"

#ifdef DEBUG
#include "unitlog.hpp"
#endif


struct OrderData {
   Boolean hasDestTown;
   Boolean hasDestUnit;
   Boolean hasTargetSP;
   Boolean isMoveOrder;
   Boolean requiresDetach;
   Boolean isAttach;
};

static const OrderData s_orderData[Orders::Type::HowMany] = {
   { True,  True,  False, True,  True,  False  },               // move
   { False, False, False, False, True,  False  },               // hold
   { False, False, False, False, True,  False  },               // r & r
   { False, True,  False, False, True,  True   },               // attach
   { False, False, True,  False, False, False  },               // attach sp
   { False, True,  False, False, False, True   },               // detach leader
   { False, False, False, False, True,  False  },               // garrison
   { False, False, False, False, False,  False  },               // install supply
   { False, False, False, False, False,  False  },               // upgrade fort
   { False, False, False, False, False,  False  },               // Blow bridge
   { False, False, False, False, False,  False  },               // Repair bridge
   { False, False, False, False, False,  False  },               // remove supply
};

#if 0
// string id's for order types
static const int s_orderTextIDs[Orders::Type::HowMany] = {
  IDS_ORDER_MOVE,
  IDS_ORDER_HOLD,
  IDS_ORDER_RESTRALLY,
  IDS_ORDER_ATTACH,
  IDS_ORDER_ATTACHSP,
  IDS_ORDER_DETACHLEADER,
  IDS_ORDER_GARRISON,
  IDS_ORDER_INSTALLSUPPLY,
  IDS_ORDER_UPGRADEFORT,
  IDS_ORDER_BlowBridge,
  IDS_ORDER_RepairBridge,
  IDS_ORDER_RemoveDepot
};

static ResourceStrings s_strings(s_orderTextIDs, Orders::Type::HowMany);
#endif

const char* Orders::Type::text(Orders::Type::Value v)
{
  ASSERT(v < Orders::Type::HowMany);

#if 0
  // TODO: move to resource strings
  // temporary
  if(v == Orders::Type::BlowBridge || v == Orders::Type::RepairBridge)
    return (v == Orders::Type::BlowBridge) ? "Blow Bridge" : "Repair Bridge";

  if(v == Orders::Type::RemoveDepot)
    return "Remove Depot";
#endif

//  return s_strings.get(v);
   return InGameText::get(v + IDS_ORDER_FIRST);
}

/*
 * OrderBase Functions  -- new version
 */


// We could probably let the default binary copy work here...

CampaignOrder& CampaignOrder::operator = (const CampaignOrder& rhs)
{
   // ASSERT(d_type == rhs.d_type);
   d_type = rhs.d_type;

   d_aggression = rhs.d_aggression;
   d_vias = rhs.d_vias;
   d_advancedOrders = rhs.d_advancedOrders;
   d_posture = rhs.d_posture;
   d_destTown = rhs.d_destTown;
   d_destUnit = rhs.d_destUnit;
   d_destSP = rhs.d_destSP;
   d_flags = rhs.d_flags;
   d_date = rhs.d_date;

   return *this;
}


/*
 * Set values to sensible default
 * Based on userint/CompleteOrder::clearValues
 * Actually CompleteOrder used to copy the static Hold order
 */

void CampaignOrder::clearValues()
{
   d_type = Orders::Type::Hold;
   d_aggression = Orders::Aggression::DefendSmall;
   d_vias.clear();
   d_advancedOrders.clear();
   d_posture = Orders::Posture::Defensive;
   d_destTown = NoTown;
   d_destUnit = NoCommandPosition;
   d_destSP = NoStrengthPoint;
   d_flags = 0;
   d_date.set(0,0,0);
}

const char* CampaignOrder::getAggressDescription() const
{
   ASSERT(d_aggression >= 0);
   ASSERT(d_aggression < Orders::Aggression::HowMany);

   return Orders::Aggression::getName(d_aggression);
}

ITown CampaignOrder::getNextDest() const
{
   if(d_vias.get(0) != NoTown)
      return d_vias.get(0);
   else
      return d_destTown;
}

void CampaignOrder::copyVia(const Orders::Vias& src)
{
   Orders::Vias* via = getVias();
   if(via)
      *via = src;
}

/*
 * advanceNextDest removes the current destination, shifting via's
 * appropriately.
 *
 * It is intended to be called when a unit arrives at a town
 */


ITown CampaignOrder::advanceNextDest()
{

   ITown nextDest = d_vias.get();
   if(nextDest == NoTown)
   {
      nextDest = d_destTown;
      d_destTown = NoTown;
   }
   return nextDest;
}

Boolean CampaignOrder::isMoveOrder() const
{
  ASSERT(d_type < Orders::Type::HowMany);

  return (s_orderData[d_type].isMoveOrder || hasTargetUnit());
}

Boolean CampaignOrder::requiresDetach() const
{
  ASSERT(d_type < Orders::Type::HowMany);
  return (s_orderData[d_type].requiresDetach);
}

#if 0
Boolean CampaignOrder::hasDestTown() const
{
  ASSERT(d_type < Orders::Type::HowMany);

  return s_orderData[d_type].hasDestTown;
}

Boolean CampaignOrder::hasDestUnit() const
{
  ASSERT(d_type < Orders::Type::HowMany);

  return s_orderData[d_type].hasDestUnit;
}
#endif

Boolean CampaignOrder::hasTargetSP() const
{
  ASSERT(d_type < Orders::Type::HowMany);

  return s_orderData[d_type].hasTargetSP;
}

Boolean CampaignOrder::isAttach() const
{
  ASSERT(d_type < Orders::Type::HowMany);

  return s_orderData[d_type].isAttach;
}

Boolean CampaignOrder::shouldCarryOut(const Date& date)
{
  Boolean carryOut = False;

  if(d_date.year < date.year)
    carryOut = True;

  else if(d_date.year == date.year)
  {
    if(d_date.month < date.month)
      carryOut = True;

    else if(d_date.month == date.month)
    {
      if(d_date.day <= date.day)
        carryOut = True;
    }
  }

  if(carryOut)
  {
    d_date.set(0, 0, 0);
  }

  return carryOut;
}


// helper functions for read/write
#if 0
inline void operator >> (FileReader& f, Orders::AdvancedOrders::OrderOnArrival& q)
{
   UBYTE b;
   f >> b;

   q = static_cast<Orders::AdvancedOrders::OrderOnArrival>(b);
}

inline void operator << (FileWriter& f, const Orders::AdvancedOrders::OrderOnArrival& q)
{
   f.putUByte(q);
}
#endif

inline bool operator >> (FileReader& f, Orders::AdvancedOrders::MoveHow& mh)
{
   UBYTE b;
   f >> b;

   mh = static_cast<Orders::AdvancedOrders::MoveHow>(b);
   return true;
}

inline bool operator << (FileWriter& f, const Orders::AdvancedOrders::MoveHow& mh)
{
   f.putUByte(mh);
   return true;
}

inline bool operator >> (FileReader& f, Orders::Aggression::Value& q)
{
   UBYTE b;
   f >> b;

   q = static_cast<Orders::Aggression::Value>(b);
   return true;
}

inline bool operator << (FileWriter& f, const Orders::Aggression::Value& q)
{
   return f.putUByte(q);
}

inline bool operator >> (FileReader& f, Orders::Type::Value& t)
{
   UBYTE b;
   f >> b;

   t = static_cast<Orders::Type::Value>(b);
   return true;
}

inline bool operator << (FileWriter& f, const Orders::Type::Value& t)
{
   return f.putUByte(t);
}

inline bool operator >> (FileReader& f, Orders::Posture::Type& p)
{
   UBYTE b;
   f >> b;

   p = static_cast<Orders::Posture::Type>(b);
   return true;
}

inline bool operator << (FileWriter& f, const Orders::Posture::Type& p)
{
   return f.putUByte(p);
}

static UWORD s_fileVersion = 0x0001;

Boolean CampaignOrder::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;

  f >> version;

  f >> d_type;
  f >> d_aggression;

  d_vias.read(f);
  d_advancedOrders.read(f);

  f >> d_posture;
  f >> d_destTown;

  CPIndex cpi;
  f >> cpi;
  d_destUnit = CampaignOBUtil::cpIndexToCPI(cpi, ob);

  SPIndex spi;
  f >> spi;
  d_destSP = CampaignOBUtil::spIndexToISP(spi, ob);

  if(version >= 0x0001)
    f >> d_flags;
  else
    d_flags = 0;

  d_date.read(f);

  return f.isOK();
}

Boolean CampaignOrder::write(FileWriter& f, OrderBattle& ob) const
{
  f << s_fileVersion;

  f << d_type;
  f << d_aggression;

  d_vias.write(f);
  d_advancedOrders.write(f);

  f << d_posture;
  f << d_destTown;

  CPIndex cpi = CampaignOBUtil::cpiToCPIndex(d_destUnit, ob);
  f << cpi;

  SPIndex spi = CampaignOBUtil::spiToSPIndex(d_destSP, ob); //ob.getIndex(d_destSP);
  f << spi;

  f << d_flags;

  d_date.write(f);

  return f.isOK();
}


int 
CampaignOrder::pack(void * buffer, OrderBattle * ob) {

   /*
   For the moment I'm not going to worry about packing tightly
   I'll go back and do this later...
   Also, all this "dataptr=n, dataptr++" stuff is messy - but I've never been good
   with pre/post increment notation...
   */

   int bytes_written = 0;
   int * dataptr = (int *) buffer;

   *dataptr = d_type;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_aggression;
   dataptr++;
   bytes_written+=4;

   for(int v=0; v<Orders::Vias::MaxVia; v++) {
      *dataptr = d_vias.get(v);
      dataptr++;
      bytes_written+=4;
   }

   *dataptr = d_advancedOrders.flags();
   dataptr++;
   bytes_written+=4;

   *dataptr = (int) d_advancedOrders.moveHow();
   dataptr++;
   bytes_written+=4;

   *dataptr = (int) d_advancedOrders.orderOnArrival();
   dataptr++;
   bytes_written+=4;

   *dataptr = d_posture;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_destTown;
   dataptr++;
   bytes_written+=4;

   *dataptr = CampaignOBUtil::cpiToCPIndex(d_destUnit, *ob);
   dataptr++;
   bytes_written+=4;

   *dataptr = CampaignOBUtil::spiToSPIndex(d_destSP, *ob);
   dataptr++;
   bytes_written+=4;

   *dataptr = d_flags;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_date.day;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_date.month;
   dataptr++;
   bytes_written+=4;

   *dataptr = d_date.year;
   dataptr++;
   bytes_written+=4;

   return bytes_written;
}


void
CampaignOrder::unpack(void * buffer,  OrderBattle * ob) {

   int * dataptr = (int *) buffer;

   d_type = (Orders::Type::Value) *dataptr;
   dataptr++;

   d_aggression = (Orders::Aggression::Value) *dataptr;
   dataptr++;

   for(int v=0; v<Orders::Vias::MaxVia; v++) {
      d_vias.set(v, *dataptr);
      dataptr++;
   }

   d_advancedOrders.flags(*dataptr);
   dataptr++;

   Orders::AdvancedOrders::MoveHow mh = (Orders::AdvancedOrders::MoveHow) *dataptr;
   dataptr++;
   d_advancedOrders.moveHow(mh);

   Orders::Type::Value type = (Orders::Type::Value) *dataptr;
   dataptr++;
   d_advancedOrders.orderOnArrival(type);

   Orders::Posture::Type posture = (Orders::Posture::Type) *dataptr;
   dataptr++;
   d_posture = posture;

   d_destTown = *dataptr;
   dataptr++;

   int cpi = *dataptr;
   dataptr++;
   d_destUnit = CampaignOBUtil::cpIndexToCPI(cpi, *ob);

   int spi = *dataptr;
   dataptr++;
   d_destSP = CampaignOBUtil::spIndexToISP(spi, *ob);

   d_flags = *dataptr;
   dataptr++;

   d_date.day = *dataptr;
   dataptr++;

   d_date.month = *dataptr;
   dataptr++;

   d_date.year = *dataptr;
   dataptr++;
}


/*-----------------------------------------------------------
 *  CampaignOrderNode
 */

CampaignOrderNode::CampaignOrderNode(const CampaignOrder* order, Mode _mode, TimeTick _arrival, TimeTick _tryAgainAt) :
   d_order(*order),
   d_mode(_mode),
   d_arrival(_arrival),
   d_tryAgainAt(_tryAgainAt)
{
}

inline bool operator >> (FileReader& f, CampaignOrderNode::Mode& m)
{
   UBYTE b;
   f >> b;

   m = static_cast<CampaignOrderNode::Mode>(b);
   return true;
}

inline bool operator << (FileWriter& f, const CampaignOrderNode::Mode& m)
{
   return f.putUByte(m);
}

static UWORD s_nodeFileVersion = 0x0000;

Boolean CampaignOrderNode::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;

  f >> version;

  d_order.read(f, ob);
  f >> d_arrival;
  f >> d_tryAgainAt;
  f >> d_mode;

  return f.isOK();
}

Boolean CampaignOrderNode::write(FileWriter& f, OrderBattle& ob) const
{
  f << s_nodeFileVersion;

  d_order.write(f, ob);
  f << d_arrival;
  f << d_tryAgainAt;
  f << d_mode;

  return f.isOK();
}

/*-----------------------------------------------------------
 *  CampaignOrderList
 */

CampaignOrderList::CampaignOrderList() :
   d_currentOrder(),    // Orders::Type::makeStaticOrder(Orders::Type::Hold)),
   d_actioningOrder(),  // 0),
   d_acting(0)
{
}

CampaignOrderList::~CampaignOrderList()
{
   // delete currentOrder;
   // delete d_actioningOrder;      // deletion is handled by ref counting
   // d_actioningOrder = 0;
}

/*
 * Add order to list
 */

void CampaignOrderList::addOrder(const OrderBase* newOrder, TimeTick arrival, TimeTick tryAgainAt)
{
   CampaignOrderNode* node = new CampaignOrderNode(newOrder, CampaignOrderNode::Messenger, arrival, tryAgainAt);
   d_orderQueue.append(node);
}

/*
 * Immediately set order
 */

void CampaignOrderList::setCurrentOrder(const OrderBase* newOrder)
{
   d_currentOrder = *newOrder;
}

void CampaignOrderList::setActioningOrder(const OrderBase* newOrder, TimeTick when)
{
   resetActing();
   d_actioningOrder = *newOrder;
   d_acting = when;
}

void CampaignOrderList::resetActing()
{
   d_acting = 0;
}

const CampaignOrder* CampaignOrderList::getLastOrder() const
{
   CampaignOrderNode* node = d_orderQueue.getLast();

   if(node)
   {
      // ASSERT(node->order != 0);
      return node->order();
   }
   else
      return &d_currentOrder;
}

Boolean CampaignOrderList::hasIntray() const
{
   // CampaignOrderNode* node = d_orderQueue.first();
   CampaignOrderNode* node = d_orderQueue.head();

   return (node && (node->mode() == CampaignOrderNode::InTray));
}

Boolean CampaignOrderList::hasMessenger() const
{
   ConstIter iter(*this);
   while(++iter)
   {
      const CampaignOrderNode* node = iter.current();

   // for(CampaignOrderNode* node = d_orderQueue.first(); node != 0; node = d_orderQueue.next())
   // {
      if(node->mode() == CampaignOrderNode::Messenger)
         return True;
   // }
   }

   return False;
}

/*
 * Clear out the orderlist when a unit dies.
 * This removes references to units if the unit had an attach order
 */


void CampaignOrderList::reset()
{
    d_orderQueue.reset();               // Remove items in orderQueue
    d_currentOrder.clearValues();       // Set current order to Hold
    d_actioningOrder.clearValues();     // Set actioning order to Hold

}

static UWORD s_listFileVersion = 0x0000;

Boolean CampaignOrderList::read(FileReader& f, OrderBattle& ob)
{
  UWORD version;

  f >> version;

  f >> d_acting;

  d_currentOrder.read(f, ob);
  d_actioningOrder.read(f, ob);

  int nNodes;
  f >> nNodes;

  while(nNodes--)
  {
    CampaignOrderNode* node = new CampaignOrderNode;
    node->read(f, ob);
    d_orderQueue.append(node);
  }

  return f.isOK();
}

Boolean CampaignOrderList::write(FileWriter& f, OrderBattle& ob) const
{
  f << s_listFileVersion;

  f << d_acting;
  d_currentOrder.write(f, ob);
  d_actioningOrder.write(f, ob);

  int nNodes = d_orderQueue.entries();
  f << nNodes;

  SListIterR<CampaignOrderNode> iter(&d_orderQueue);
  while(++iter)
    iter.current()->write(f, ob);

  return f.isOK();
}

void CampaignOrderList::copy(const CampaignOrderList& list)
{
   d_acting = list.d_acting;
   d_currentOrder = list.d_currentOrder;
   d_actioningOrder = list.d_actioningOrder;
   // d_orderQueue = list.d_orderQueue;

   ConstIter iter(list);
   while(++iter)
   {
      d_orderQueue.append(new CampaignOrderNode(*iter.current()));
   }
}

/*
 * Advanced Orders
 */

void Orders::AdvancedOrders::clear()
{
   d_flags = 0;
   d_moveHow = Normal;
   d_orderOnArrival = Orders::Type::Hold;
}

static const UWORD s_aoFileVersion = 0x0000;

Boolean Orders::AdvancedOrders::read(FileReader& f)
{
   UWORD version;
   f >> version;

   f >> d_flags;
   f >> d_moveHow;
   f >> d_orderOnArrival;

   // Force orderOnArrival to a sensible value

   if(!Orders::Type::canGiveOnArrival(d_orderOnArrival))
   {
#ifdef DEBUG
      cuLog.printf("WARNING: invalid orderOnArrival %s changed to Hold",
         (const char*) Orders::Type::text(d_orderOnArrival));
#endif
      d_orderOnArrival = Orders::Type::Hold;
   }


   return f.isOK();
}

Boolean Orders::AdvancedOrders::write(FileWriter& f) const
{
   f << s_aoFileVersion;

   f << d_flags;
   f << d_moveHow;
   f << d_orderOnArrival;

   return f.isOK();
}


// string id's for order types
#if 0
static const int s_onArrivalTextIDs[Orders::AdvancedOrders::OrderOnArrival_HowMany] = {
  IDS_ONARRIVAL_HOLD,
  IDS_ONARRIVAL_REST,
  IDS_ONARRIVAL_GARRISON,
  IDS_ONARRIVAL_ATTACH,
};

static ResourceStrings s_onArrivalStrings(s_onArrivalTextIDs, Orders::AdvancedOrders::OrderOnArrival_HowMany);
#endif

const char* Orders::AdvancedOrders::getOrderOnArrivalName(Orders::Type::Value v)
{
  ASSERT(v < Orders::Type::HowMany);

  static char buf[200];
  lstrcpy(buf, Orders::Type::text(v));

  // append "on arrival text"
  // TODO: get this from resource
  lstrcat(buf, " ");
  lstrcat(buf, InGameText::get(IDS_OnArrival));  // " on Arrival");
  return buf;
}

#if 0
static Orders::Type::Value arrivalToType[Orders::AdvancedOrders::OrderOnArrival_HowMany] = {
   Orders::Type::Hold,
   Orders::Type::RestRally,
   Orders::Type::Garrison,
   Orders::Type::Attach
};

static Orders::Type::Value Orders::AdvancedOrders::orderOnArrivalToType(Orders::AdvancedOrders::OrderOnArrival v)
{
  ASSERT(v >= 0);
  ASSERT(v < OrderOnArrival_HowMany);

  return arrivalToType[v];
}
#endif

// string id's for movehow types
static const int s_moveHowTextIDs[Orders::AdvancedOrders::MoveHow_HowMany] = {
  IDS_MOVEHOW_NORMAL,
  IDS_MOVEHOW_EASY,
  IDS_MOVEHOW_FORCE,
};

static ResourceStrings s_moveHowStrings(s_moveHowTextIDs, Orders::AdvancedOrders::MoveHow_HowMany);

const char* Orders::AdvancedOrders::getMoveHowName(MoveHow v)
{
  ASSERT(v < MoveHow_HowMany);
  return s_moveHowStrings.get(v);
}


// string ids for advanced options text
  enum {
    SoundGunID,
    PursueID,
    SiegeActiveID,

    AdvancedOrderID_HowMany
  };

static const int s_aoStringIDs[AdvancedOrderID_HowMany] = {
  IDS_AO_SOUNDGUNS,
  IDS_AO_PURSUE,
  IDS_AO_SIEGEACTIVE
};

static ResourceStrings s_aoStrings(s_aoStringIDs, Orders::AdvancedOrders::MoveHow_HowMany);

const char* Orders::AdvancedOrders::soundGunsText()
{
  return s_aoStrings.get(SoundGunID);
}

const char* Orders::AdvancedOrders::pursueText()
{
  return s_aoStrings.get(PursueID);
}

const char* Orders::AdvancedOrders::siegeActiveText()
{
  return s_aoStrings.get(SiegeActiveID);
}

const char* Orders::AdvancedOrders::dropOffGarrisonText()
{
   return InGameText::get(IDS_AO_DropOff);
}

const char* Orders::AdvancedOrders::autoStormText()
{
   return InGameText::get(IDS_AO_AutoStorm);
}


// string id's for order types
static const int s_aggressTextIDs[Orders::Aggression::HowMany] = {
  IDS_AGGRESSION_LEVEL1,
  IDS_AGGRESSION_LEVEL2,
  IDS_AGGRESSION_LEVEL3,
  IDS_AGGRESSION_LEVEL4,
};

static ResourceStrings s_aggressStrings(s_aggressTextIDs, Orders::Aggression::HowMany);

const char* Orders::Aggression::getName(Value v)
{
  ASSERT(v < HowMany);
  return s_aggressStrings.get(v);
}

/*==============================================================
 * Vias
 */

Orders::Vias::Vias()
{
   clear();
}

void Orders::Vias::clear()
{
   for(int i = 0; i < MaxVia; i++)
      viaTown[i] = NoTown;
}

bool Orders::operator != (const Orders::Vias& v1, const Orders::Vias& v2)
{
   for(int i = 0; i < Orders::Vias::MaxVia; i++)
   {
      if(v1.viaTown[i] != v2.viaTown[i])
         return True;
   }
   return False;
}


ITown Orders::Vias::get(int i) const
{
   ASSERT(i >= 0);
   ASSERT(i < MaxVia);

   return viaTown[i];
}

/*
 * Insert Via at locatiom
 */

void Orders::Vias::set(int n, ITown t)
{
   ASSERT(n >= 0);
   ASSERT(n < MaxVia);

   viaTown[n] = t;
}

void Orders::Vias::insert(int n, ITown t)
{
   ASSERT(n >= 0);
   ASSERT(n < MaxVia);
   ASSERT(t != NoTown);

   if(viaTown[n] != NoTown)
   {
      for(int i = MaxVia - 1; i > n; i--)
         viaTown[i] = viaTown[i-1];
   }
   viaTown[n] = t;
}


void Orders::Vias::remove(int n)
{
   ASSERT(n >= 0);
   ASSERT(n < MaxVia);

   for(int i = n; i < (MaxVia - 1); i++)
      viaTown[i] = viaTown[i+1];
   viaTown[i] = NoTown;
}

ITown Orders::Vias::get()
{
   ITown t = viaTown[0];
   remove(0);
   return t;
}

int Orders::Vias::getNVias() const
{
  int nVias = 0;
  for(int i = 0; i < MaxVia; i++)
  {
    if(viaTown[i] != NoTown)
      nVias++;
  }
  return nVias;
}

Orders::Vias& Orders::Vias::operator = (const Vias& v)
{
   for(int i = 0; i < MaxVia; i++)
      viaTown[i] = v.viaTown[i];

   return *this;
}

static UWORD s_viaFileVersion = 0x0000;

Boolean Orders::Vias::read(FileReader& f)
{
  UWORD version;
  f >> version;

  int nVias;
  f >> nVias;

  for(int i = 0; i < nVias; i++)
    f >> viaTown[i];

  return f.isOK();
}

Boolean Orders::Vias::write(FileWriter& f) const
{
  f << s_viaFileVersion;

  int nVias = getNVias();
  f << nVias;

  for(int i = 0; i < nVias; i++)
    f << viaTown[i];

  return f.isOK();
}

//============================= Posture ==============================

static int s_postureNameIDs[Orders::Posture::HowMany] = {
  IDS_POSTURE_OFFENSIVE,
  IDS_POSTURE_DEFENSIVE
};

static ResourceStrings s_postureStrings(s_postureNameIDs, Orders::Posture::HowMany);

const char* Orders::Posture::postureName(Orders::Posture::Type t)
{
  ASSERT(t < HowMany);
  return s_postureStrings.get(t);
}


#ifdef DEBUG
FixedSize_Allocator CampaignOrderNode::allocator(sizeof(CampaignOrderNode), CampaignOrderNode::ChunkSize, "CampaignOrderNode");
#else
FixedSize_Allocator CampaignOrderNode::allocator(sizeof(CampaignOrderNode), CampaignOrderNode::ChunkSize);
#endif

void* CampaignOrderNode::operator new(size_t size)
{
  ASSERT(size == sizeof(CampaignOrderNode));
  return allocator.alloc(size);
}

#ifdef _MSC_VER
void CampaignOrderNode::operator delete(void* deadObject)
{
   allocator.free(deadObject, sizeof(CampaignOrderNode));
}
#else
void CampaignOrderNode::operator delete(void* deadObject, size_t size)
{
  ASSERT(size == sizeof(CampaignOrderNode));
  allocator.free(deadObject, size);
}
#endif

static struct {
   Boolean canGiveToUnit;
   Boolean canGiveOnArrival;
} const orderTable[Orders::Type::HowMany] = {
   {  // MOVETO
      True, False
   },
   {  // HOLD
      True, True
   },
   {  // REST_RALLY
      True, True
   },
   {  // ATTACH
      False, True
   },
   {  // ATTACHSP
      False, False
   },
   {  // LEADER
      True, False
   },
   {  // GARRISON
      True, True
   },
   {  // INSTALLSUPPLY
      True, True
   },
   {  // UPGRADEFORT
      True, True
   },
   {  // Blow Bridge
      True, True
   },
   {  // Repair Bridge
      True, True
   },
   {  // Remove Depot
      True, True
   },
};

Boolean Orders::Type::canGiveToUnit(Value t)
{
   ASSERT(t >= First);
   ASSERT(t < HowMany);

   if( (t >= First) && (t < HowMany) )
      return orderTable[t].canGiveToUnit;
   else
      return False;
}

Boolean Orders::Type::canGiveOnArrival(Value t)
{
   ASSERT(t >= First);
   ASSERT(t < HowMany);

   if( (t >= First) && (t < HowMany) )
      return orderTable[t].canGiveOnArrival;
   else
      return False;
}

#ifdef DEBUG   // For backwards compatibility
const CampaignOrder* Orders::Type::makeStaticOrder(Orders::Type::Value t)
{
   static CampaignOrder order;
   order.setType(t);
   return &order;
}
#endif



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
