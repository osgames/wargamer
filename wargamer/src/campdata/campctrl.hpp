/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPCTRL_HPP
#define CAMPCTRL_HPP

#ifndef __cplusplus
#error campctrl.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Logic Controller
 *
 * Campaign Logic uses this to interact with rest of game
 *
 *----------------------------------------------------------------------
 */

#if !defined(EDITOR)

#include "cbatmode.hpp"

// Forward references

class CampaignMessageInfo;
class CampaignData;
class CampaignBattle;

namespace WG_CampaignSiege { struct SiegeInfoData; };
using namespace WG_CampaignSiege;

struct BattleInfoData;

// namespace WargameMessage {
//     class WaitableMessage;
// }   // namespace WargameMessage

/*
 * Abstract Class
 */

class CampaignLogicOwner
{
    private:
        CampaignLogicOwner(const CampaignLogicOwner&);
        CampaignLogicOwner& operator = (const CampaignLogicOwner&);

    public:
        CampaignLogicOwner() { }
        virtual ~CampaignLogicOwner() = 0;

	virtual void sendPlayerMessage(const CampaignMessageInfo& msg) = 0;
		// Send Message to Player via message Window
	virtual void redrawMap() = 0;
		// Redraw the Campaign Map, static and dynamic
	virtual void repaintMap() = 0;
      // Redraw the Campaign Map, dynamic only
    virtual void moraleUpdated() = 0;
        // Redraw national morale bars
    virtual void updateWeather() = 0;
        // Weather has changed

	virtual CampaignData* campaignData() = 0;
        // Retrieve Campaign Data

    virtual void requestTactical(CampaignBattle* battle) = 0;
        // Really start tactical game...
        // called from UI thread
    virtual void reinforceTactical() = 0;
    // virtual bool getUserResponse(WargameMessage::WaitableMessage* msg) = 0;
        // Wait for a response from user...

    virtual void siegeResults(Side s, const SiegeInfoData& sd) = 0;
        // Tell Game about siege results

    virtual EndOfDayMode::Mode endBattleDay(Side side, const BattleInfoData& bd) = 0;
        // Ask Game what to do at end of Day

    virtual void battleResults(Side side, const BattleInfoData& bd) = 0;
        // tell game about Battle Results

    virtual BattleMode::Mode askBattleMode(const CampaignBattle* battle) = 0;
        // Ask what mode to do a battle

};


inline CampaignLogicOwner::~CampaignLogicOwner()
{
}

#endif	// !EDITOR



#endif /* CAMPCTRL_HPP */

