/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "routelst.hpp"
#include "filebase.hpp"
#include "fsalloc.hpp"

/*-------------------------------------------------------------------
 * Previous route container classes
 */

#ifdef DEBUG
static FixedSizeAlloc<RouteNode> routeNodeAlloc("RouteNode");
#else
static FixedSizeAlloc<RouteNode> routeNodeAlloc;
#endif

void* RouteNode::operator new(size_t size)
{
  return routeNodeAlloc.alloc(size);
}

#ifdef _MSC_VER
void RouteNode::operator delete(void* deadObject)
{
  routeNodeAlloc.release(deadObject);
}
#else
void RouteNode::operator delete(void* deadObject, size_t size)
{
  routeNodeAlloc.release(deadObject, size);
}
#endif

const UWORD s_rnFileVersion = 0x0000;

Boolean RouteNode::read(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_rnFileVersion);

  f >> d_town;

  return f.isOK();
}

Boolean RouteNode::write(FileWriter& f) const
{
  f << s_rnFileVersion;
  f << d_town;

  return f.isOK();
}

void RouteList::addNode(ITown town)
{
  ASSERT(town != NoTown);

  RouteNode* node = new RouteNode(town);
  ASSERT(node != 0);

  append(node);
}

void RouteList::addPreviousNode(ITown town)
{
  ASSERT(town != NoTown);

  RouteNode* node = new RouteNode(town);
  ASSERT(node != 0);

  insert(node);

  /*
   * If more than max nodes remove last item
   */

  while(entries() > MaxPreviousNodes)
    remove(getLast());
}

void RouteList::removeNode(ITown town)
{
  ASSERT(town != NoTown);

  SListIter<RouteNode> iter(this);
  while(++iter)
  {
    if(iter.current()->d_town == town)
    {
      remove(iter.current());
      break;
    }
  }
}

RouteList& RouteList::operator = (const RouteList& l)
{
  reset();

  RouteListIterR iter(&l);
  while(++iter)
  {
    RouteNode* node = new RouteNode(iter.current()->d_town);
    ASSERT(node);

    append(node);
  }

  return *this;
}

const UWORD s_prFileVersion = 0x0000;

Boolean RouteList::read(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_prFileVersion);

  int nNodes;
  f >> nNodes;

  while(nNodes--)
  {
    RouteNode* rn = new RouteNode;
    ASSERT(rn);

    rn->read(f);
    append(rn);
  }

  return f.isOK();
}

Boolean RouteList::write(FileWriter& f) const
{
  f << s_prFileVersion;

  int nNodes = entries();
  f << nNodes;

  SListIterR<RouteNode> iter(this);
  while(++iter)
    iter.current()->write(f);

  return f.isOK();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
