/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "randevnt.hpp"
#include "campctrl.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "armies.hpp"
#include "myassert.hpp"
#include "fsalloc.hpp"
#include "measure.hpp"
#include "campmsg.hpp"
#include "scenario.hpp"
#include "sllist.hpp"
#include "wg_rand.hpp"

#ifdef DEBUG
#include "clog.hpp"
LogFileFlush rLog("RandEvent.log");
#endif


/*
 * Wargamer Random Events
 */


/*================== Some utility classes ============================
 *
 */

struct RandomLeader : public SLink {
  enum { ChunkSize = 50 };
  ILeader d_leader;

  static FixedSizeAlloc<RandomLeader> randomLeaderAlloc;

  RandomLeader(ILeader leader) : d_leader(leader) {}

  static void free() { randomLeaderAlloc.dryUp(); }  // free allocated block

  void* operator new(size_t size);
#ifdef _MSC_VER
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif
};

class RandomLeaderList : public SList<RandomLeader> {
  public:
    void free() { reset(); RandomLeader::free(); }
};

#ifdef DEBUG
FixedSizeAlloc<RandomLeader> RandomLeader::randomLeaderAlloc("RandomLeader");
#else
FixedSizeAlloc<RandomLeader> RandomLeader::randomLeaderAlloc;
#endif

void* RandomLeader::operator new(size_t size)
{
  return randomLeaderAlloc.alloc(size);
}
#ifdef _MSC_VER
void RandomLeader::operator delete(void* deadObject)
{
  randomLeaderAlloc.release(deadObject);
}
#else
void RandomLeader::operator delete(void* deadObject, size_t size)
{
  randomLeaderAlloc.release(deadObject, size);
}
#endif

struct RandomUnit : public SLink {
  enum { ChunkSize = 50 };
  ICommandPosition d_unit;

  static FixedSizeAlloc<RandomUnit> randomUnitAlloc;

  RandomUnit(ICommandPosition unit) : d_unit(unit) {}

  static void free() { randomUnitAlloc.dryUp(); }  // free allocated block

  void* operator new(size_t size);
#ifdef _MSC_VER
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif
};

class RandomUnitList : public SList<RandomUnit> {
  public:
    void free() { reset(); RandomUnit::free(); }
};

#ifdef DEBUG
FixedSizeAlloc<RandomUnit> RandomUnit::randomUnitAlloc("RandomUnit");
#else
FixedSizeAlloc<RandomUnit> RandomUnit::randomUnitAlloc;
#endif

void* RandomUnit::operator new(size_t size)
{
  return randomUnitAlloc.alloc(size);
}

#ifdef _MSC_VER
void RandomUnit::operator delete(void* deadObject)
{
  randomUnitAlloc.release(deadObject);
}
#else
void RandomUnit::operator delete(void* deadObject, size_t size)
{
  randomUnitAlloc.release(deadObject, size);
}
#endif

/*================= RandomEvent derived classes ======================
 *
 */

class TownCatastrophe : public RandomEvent {
  public:
    TownCatastrophe() : RandomEvent(RandomEvent::Events::TownCatastrophe) {}
    ~TownCatastrophe() {}

    void runEvent(CampaignLogicOwner* campGame, Side side);
    void reset() { }
};

class LeaderSick : public RandomEvent {
    RandomLeaderList d_leaders;
  public:
    LeaderSick() : RandomEvent(RandomEvent::Events::LeaderSick) {}
    ~LeaderSick() { /*randomLeaderAlloc.dryUp();*/ }

    void runEvent(CampaignLogicOwner* campGame, Side side);
    void reset() { d_leaders.reset(); }
};

class AlliedDisillusion : public RandomEvent {
  public:
    AlliedDisillusion() : RandomEvent(RandomEvent::Events::AlliedDisillusion) {}
    ~AlliedDisillusion() {}

    void runEvent(CampaignLogicOwner* campGame, Side side);
    void reset() { }
};

class SHQInChaos : public RandomEvent {
  public:
    SHQInChaos() : RandomEvent(RandomEvent::Events::SHQInChaos) {}
    ~SHQInChaos() {}

    void runEvent(CampaignLogicOwner* campGame, Side side);
    void reset() { }
};

class EnemyPlansCaptured : public RandomEvent {
  public:
    EnemyPlansCaptured() : RandomEvent(RandomEvent::Events::EnemyPlansCaptured) {}
    ~EnemyPlansCaptured() {}

    void runEvent(CampaignLogicOwner* campGame, Side side);
    void reset() { }
};

class SquabblingLeaders : public RandomEvent {
    RandomUnitList d_units;
  public:
    SquabblingLeaders() : RandomEvent(RandomEvent::Events::SquabblingLeaders) {}
    ~SquabblingLeaders() {}

    void runEvent(CampaignLogicOwner* campGame, Side side);
    void reset() { d_units.reset(); }
};

class SquabblingAlliedLeaders : public RandomEvent {
    RandomUnitList d_units;
  public:
    SquabblingAlliedLeaders() : RandomEvent(RandomEvent::Events::SquabblingAlliedLeaders) {}
    ~SquabblingAlliedLeaders() {}

    void runEvent(CampaignLogicOwner* campGame, Side side);
    void reset() { d_units.reset(); }
};

class PopularEnthusiasm : public RandomEvent {
  public:
    PopularEnthusiasm() : RandomEvent(RandomEvent::Events::PopularEnthusiasm) {}
    ~PopularEnthusiasm() {}

    void runEvent(CampaignLogicOwner* campGame, Side side);
    void reset() { }
};


/*=========== Derived classes implementation ====================
 *
 */

void TownCatastrophe::runEvent(CampaignLogicOwner* campGame, Side side)
{
#if !defined(EDITOR)
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  rLog.printf("%s is chosen Random Event", Events::getEventText(getType()));
#endif

  /*
   * Select a random town (regardless of side) for catastrophe
   *
   */

  TownList& tl = campData->getTowns();

  ITown iTown = static_cast<ITown>(CRandom::get(tl.entries()));
  ASSERT(iTown < tl.entries());

  Town& town = campData->getTown(iTown);

  /*
   * set it up. duration is 2 days
   */

  town.setCatastrophe(DaysToTicks(2)+campData->getTick());

#ifdef DEBUG
  rLog.printf("%s has occurred at %s", Events::getEventText(getType()), town.getNameNotNull());
#endif

  /*
   * Go through all top-level units. Any unit currently in this town set
   * to forcedHold for 1 day
   */

  CPIter iter(&campData->getArmies());
  while(iter.sister())
  {
    CommandPosition* cp = iter.currentCommand();

    if(cp->atTown() && cp->getTown() == iTown)
      cp->forceHold(DaysToTicks(1)+campData->getTick());
  }

  /*
   * Let the player know
   *
   * For this lets pick a random number of 0 or 1.
   * If 0 then send a bridge collapses messages
   * if 1 then send a town fire message
   */

  CampaignMessages::CMSG_ID id = (CRandom::get(2) == 0) ? CampaignMessages::BridgeCollapse : CampaignMessages::TownFire;
  CampaignMessageInfo msgInfo(side, id);
  msgInfo.where(iTown);
  campGame->sendPlayerMessage(msgInfo);
#endif
}

void LeaderSick::runEvent(CampaignLogicOwner* campGame, Side side)
{
#if !defined(EDITOR)
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  rLog.printf("%s is chosen Random Event", Events::getEventText(getType()));
#endif

  /*
   * Select a random leader who is a corp commander or above
   *
   * First, go through every leader and add qualifying leaders to list
   */

  UnitIter iter(&campData->getArmies(), campData->getArmies().getPresident(side));
  while(iter.next())
  {
    CommandPosition* cp = iter.currentCommand();
    if(cp->isLower(Rank_President))       // skip past president
    {

      ASSERT(cp->getLeader() != NoLeader);
      Leader* leader = campData->getLeader(cp->getLeader());

      ILeader iLeader = NoLeader;

      /*
       * If this is a real unit add leader if unit is corp or above
       */

      if(cp->isRealUnit())
      {
        if(cp->getRank().getRankEnum() <= Rank_Corps) // less than is actually greater than
        {
          iLeader = cp->getLeader();
        }
      }

      /*
       * Other wise add leader if his rank level is corp or above
       */

      else
      {
        if(leader->getRankLevel().getRankEnum() <= Rank_Corps)
        {
          iLeader = cp->getLeader();
        }
      }

      /*
       * If we found an appropriate leader, we need to bias
       * possible selections toward those who are less healthy
       *
       */

      if(iLeader != NoLeader)
      {

        const Table1D<UBYTE>& table = scenario->getLeaderHealthCutoffTable();

        /*
         * If leaders health is less than table value, add him to the list
         */

        if(leader->getHealth() <= table.getValue(CRandom::get(10)))
        {
          RandomLeader* randomLeader = new RandomLeader(iLeader);
          ASSERT(randomLeader != 0);

          d_leaders.append(randomLeader);
        }
      }
    }
  }

  /*
   * Now select one at random
   */

  if(d_leaders.entries() > 0)
  {

    ILeader sickLeader = NoLeader;
    int pick = CRandom::get(d_leaders.entries());

    SListIter<RandomLeader> sIter(&d_leaders);
    for(int i = 0; ++sIter; i++)
    {
      if(i == pick)
      {
        RandomLeader* rl = sIter.current();
        sickLeader = rl->d_leader;

        break;
      }
    }

    ASSERT(sickLeader != NoLeader);

    if(sickLeader != NoLeader)
    {
      /*
       * We found our sick leader
       */

      Leader* leader = campData->getLeader(sickLeader);

#ifdef DEBUG
      rLog.printf("SickLeader is %s %s", scenario->getSideName(side), leader->getNameNotNull());
#endif

      /*
       * implement it
       */

      leader->isSick(True);

      /*
       * Let the player know
       */

      CampaignMessageInfo msgInfo(side, CampaignMessages::LeaderSick);
      msgInfo.cp(leader->getCommand());
      msgInfo.leader(sickLeader);
      campGame->sendPlayerMessage(msgInfo);
    }
  }
#ifdef DEBUG
  else
    rLog.printf("No Leader Found");
#endif

  /*
   * Free up allocated memory block
   */

  d_leaders.free();
#endif
}

void AlliedDisillusion::runEvent(CampaignLogicOwner* campGame, Side side)
{
#if !defined(EDITOR)
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  rLog.printf("%s is chosen Random Event", Events::getEventText(getType()));
#endif

  /*
   * What we are doing here is:
   *
   * 1. Search for the lowest morale unit that has allied SP
   * 2. Remove SP's until morale X SP removed >= 255
   *
   * First, get our sides default nation
   */

  Nationality sideNation = scenario->getDefaultNation(side);


  /*
   * Now iterate through units and find one with lowest morale that has
   * allied SP
   */

  ICommandPosition worstCPI = NoCommandPosition;
  Attribute worstMorale = Attribute_Range;

  UnitIter iter(&campData->getArmies(), campData->getArmies().getPresident(side));

  while(iter.next())
  {
     CommandPosition* cp = iter.currentCommand();

     if(cp->isLower(Rank_President))       // skip past president
     {
       if(cp->getNation() != sideNation)
       {
         SPCount count = campData->getArmies().getUnitSPCount(iter.current(), False);

         if(count > 0)
         {
           if(worstCPI == NoCommandPosition || cp->getMorale() < worstMorale)
           {
             worstCPI = iter.current();
             worstMorale = cp->getMorale();
           }
         }
       }
     }
  }

  /*
   * If we found a qualifying unit
   */

  if(worstCPI != NoCommandPosition)
  {
#ifdef DEBUG
    rLog.printf("%s has SP's that will be removed", (const char*)campData->getUnitName(worstCPI).toStr());
#endif

    CommandPosition* cp = campData->getCommand(worstCPI);
    ASSERT(cp->getSPEntry() != NoStrengthPoint);

    int totalMorale = 0;

    ISP isp = cp->getSPEntry();
    ASSERT(isp != NoStrengthPoint);

    int count = 0;
    while(isp != NoStrengthPoint)
    {
      count++;
      campData->getArmies().detachStrengthPoint(worstCPI, isp);
      campData->getArmies().deleteStrengthPoint(isp);

      totalMorale += cp->getMorale();

      if( (totalMorale + cp->getMorale()) > Attribute_Range )
        break;

      isp = cp->getSPEntry();
    }

    ICommandPosition topCPI = campData->getArmies().getTopParent(worstCPI);

//  campData->getArmies().clearDestroyedUnits(topCPI);

    /*
     * Let the player know
     */

    CampaignMessageInfo msgInfo(side, CampaignMessages::AlliedDisillusion);

    msgInfo.cp(topCPI);
    msgInfo.cpTarget(worstCPI);
    msgInfo.value1(count);

    campGame->sendPlayerMessage(msgInfo);

  }
#endif
}

void SHQInChaos::runEvent(CampaignLogicOwner* campGame, Side side)
{
#if !defined(EDITOR)
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  rLog.printf("%s is chosen Random Event", Events::getEventText(getType()));
#endif

  /*
   * implement it. Duration is 1 day
   */

  campData->setSHQInChaos(side, DaysToTicks(1)+campData->getTick());

  /*
   * Let the player know
   */

  ILeader iSLeader = campData->getArmies().getSupremeLeaderID(side);
  const Leader* sLeader = campData->getArmies().getSupremeLeader(side);

  CampaignMessageInfo msgInfo(side, CampaignMessages::SHQInChaos);

  msgInfo.cp( (sLeader->getCommand() != NoCommandPosition) ? sLeader->getCommand() : campData->getArmies().getFirstUnit(side));
  msgInfo.leader(iSLeader);

  campGame->sendPlayerMessage(msgInfo);
#endif
}

void EnemyPlansCaptured::runEvent(CampaignLogicOwner* campGame, Side side)
{
#if !defined(EDITOR)
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  rLog.printf("%s is chosen Random Event", Events::getEventText(getType()));
#endif

  /*
   * implement it. Duration is 2 days
   */

  campData->setEnemyPlansCaptured(side, DaysToTicks(2)+campData->getTick());

  /*
   * Let the player know
   */

  CampaignMessageInfo msgInfo(side, CampaignMessages::EnemyPlansCaptured);
  const Leader* leader = campData->getArmies().getSupremeLeader(side);
  msgInfo.cp( (leader->getCommand() != NoCommandPosition) ? leader->getCommand() : campData->getArmies().getFirstUnit(side));
  campGame->sendPlayerMessage(msgInfo);
#endif
}

void SquabblingLeaders::runEvent(CampaignLogicOwner* campGame, Side side)
{
#if !defined(EDITOR)
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  rLog.printf("%s is chosen Random Event", Events::getEventText(getType()));
#endif

  /*
   * An army whose leader has an Initiative of less than 200 has
   * a chance of being picked.
   */

  UnitIter iter(&campData->getArmies(), campData->getArmies().getFirstUnit(side));
  while(iter.sister())
  {
    CommandPosition* cp = iter.currentCommand();

    /*
     * Only Armies go in the list ?
     */

    if(cp->getRank().getRankEnum() == Rank_Army)
    {
      ASSERT(cp->getLeader() != NoLeader);

      /*
       * If leaders initiative is less than 200, add
       */

      Leader* leader = campData->getLeader(cp->getLeader());

      if(leader->getInitiative() < 200)
      {
        RandomUnit* ru = new RandomUnit(iter.current());
        ASSERT(ru != 0);

        d_units.append(ru);
      }
    }
  }

  /*
   * Now select one at random
   */

  if(d_units.entries() > 0)
  {
    ICommandPosition cpi = NoCommandPosition;
    int pick = CRandom::get(d_units.entries());

    SListIter<RandomUnit> sIter(&d_units);
    for(int i = 0; ++sIter; i++)
    {
      if(i == pick)
      {
        RandomUnit* rl = sIter.current();
        cpi = rl->d_unit;

        break;
      }
    }

    ASSERT(cpi != NoCommandPosition);

    if(cpi != NoCommandPosition)
    {
      /*
       * We found our squabbling unit
       */

      CommandPosition* cp = campData->getCommand(cpi);

#ifdef DEBUG
      rLog.printf("Squabbling unit is %s", cp->getNameNotNull());
#endif

      /*
       * implement it
       *
       * Duration is 1 day
       */

      cp->unitParalized(DaysToTicks(1)+campData->getTick());

      /*
       * Let the player know
       */

      CampaignMessageInfo msgInfo(side, CampaignMessages::SquabblingLeaders);
      msgInfo.cp(cpi);
      campGame->sendPlayerMessage(msgInfo);
    }
  }
#ifdef DEBUG
  else
    rLog.printf("No Unit Found");
#endif

  /*
   * Free up allocated memory block
   */

  d_units.free();
#endif
}

void SquabblingAlliedLeaders::runEvent(CampaignLogicOwner* campGame, Side side)
{
#if !defined(EDITOR)
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  rLog.printf("%s is chosen Random Event", Events::getEventText(getType()));
#endif

  /*
   *  Any army which has more than one major nationality present
   *  has a chance of being picked.
   */

  UnitIter iter(&campData->getArmies(), campData->getArmies().getFirstUnit(side));
  while(iter.sister())
  {
    CommandPosition* cp = iter.currentCommand();

    /*
     * Only Armies go in the list
     */

    if(cp->getRank().getRankEnum() == Rank_Army)
    {
      ASSERT(cp->getLeader() != NoLeader);

      /*
       * If Unit has mixed major nationalities, add
       */

      Nationality n = cp->getNation();

      UnitIter uiter(&campData->getArmies(), iter.current());
      while(uiter.next())
      {
        CommandPosition* subCP = uiter.currentCommand();

        if(scenario->getNationType(subCP->getNation()) == MajorNation &&
           subCP->getNation() != n)
        {
          RandomUnit* ru = new RandomUnit(uiter.current());
          ASSERT(ru != 0);

          d_units.append(ru);
        }
      }
    }
  }

  /*
   * Now select one at random
   */

  if(d_units.entries() > 0)
  {
    ICommandPosition cpi = NoCommandPosition;
    int pick = CRandom::get(d_units.entries());

    SListIter<RandomUnit> sIter(&d_units);
    for(int i = 0; ++sIter; i++)
    {
      if(i == pick)
      {
        RandomUnit* rl = sIter.current();
        cpi = rl->d_unit;

        break;
      }
    }

    ASSERT(cpi != NoCommandPosition);

    if(cpi != NoCommandPosition)
    {
      /*
       * We found our squabbling unit
       */

      CommandPosition* cp = campData->getCommand(cpi);

#ifdef DEBUG
      rLog.printf("Squabbling unit is %s", cp->getNameNotNull());
#endif

      /*
       * implement it
       *
       * Duration is 1 day
       */

      cp->unitParalized(DaysToTicks(1)+campData->getTick());

      /*
       * Let the player know
       */

      CampaignMessageInfo msgInfo(side, CampaignMessages::SquabblingLeaders);
      msgInfo.cp(cpi);
      campGame->sendPlayerMessage(msgInfo);
    }
  }
#ifdef DEBUG
  else
    rLog.printf("No Unit Found");
#endif

  /*
   * Free up allocated memory block
   */

  d_units.free();
#endif

}

void PopularEnthusiasm::runEvent(CampaignLogicOwner* campGame, Side side)
{
#if !defined(EDITOR)
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();

#ifdef DEBUG
  rLog.printf("%s is chosen Random Event", Events::getEventText(getType()));
#endif

  /*
   * TODO: implement this
   */
#endif
}

/*=========== Static Instances of Random events =====================
 *   These are the only instances allowed
 */

static TownCatastrophe          townCatastrophe;
static LeaderSick               leaderSick;
static AlliedDisillusion        alliedDisillusion;
static SHQInChaos               shqInChaos;
static EnemyPlansCaptured       enemyPlansCaptured;
static SquabblingLeaders        squabblingLeaders;
static SquabblingAlliedLeaders  squabblingAlliedLeaders;
static PopularEnthusiasm        popularEnthusiasm;

static RandomEvent* randomEvents[RandomEvent::Events::HowMany] = {
  &townCatastrophe,
  &leaderSick,
  &alliedDisillusion,
  &shqInChaos,
  &enemyPlansCaptured,
  &squabblingLeaders,
  &squabblingAlliedLeaders,
  &popularEnthusiasm
};


RandomEvent& RandomEventAllocator::newEvent(RandomEvent::Events::Event type)
{
  ASSERT(type < RandomEvent::Events::HowMany);

  return *randomEvents[type];
}

void RandomEventAllocator::resetEvents()
{
   for(int i = 0; i < RandomEvent::Events::HowMany; ++i)
   {
      randomEvents[i]->reset();
   }
}


#ifdef DEBUG

static const char* eventText[RandomEvent::Events::HowMany] = {
  "Town Catastrophe",
  "Leader Sick",
  "Allied Disillusion",
  "SHQ in Chaos",
  "Enemy Plans Captured",
  "Squabbling Leaders",
  "Squabbling Allied Leaders",
  "Popular Enthusiasm",
};

const char* RandomEvent::Events::getEventText(Event event)
{
  ASSERT(event < HowMany);
  return eventText[event];
}

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
