/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef RANDEVNT_HPP
#define RANDEVNT_HPP

#include "cdatadll.h"
#include "gamedefs.hpp"
#ifdef DEBUG
#include "clog.hpp"
#endif

class CampaignLogicOwner;

class RandomEvent {
  public:
    class Events {
       public:
         enum Event {
            First = 0,

            TownCatastrophe = First,      // fire, bridge collapses, etc.
            LeaderSick,                   // Leader falls ill
            AlliedDisillusion,            // disillusion amongst our allies rank and file
            SHQInChaos,                   // our SHQ is a madhouse
            EnemyPlansCaptured,           // enemy got careless
            SquabblingLeaders,            // no one can get along
            SquabblingAlliedLeaders,      // no one can get along with our allies
            PopularEnthusiasm,            // we have aroused the masses

            HowMany
         };
#ifdef DEBUG
         static const char* getEventText(Event event);
#endif
    };

  private:
    Events::Event d_type;

    RandomEvent(const RandomEvent&);                   // disallow copy contructor
    RandomEvent& operator=(const RandomEvent&);        // disallow copy

  public:
    RandomEvent(Events::Event type) : d_type(type) {}
    virtual ~RandomEvent() {}

    virtual void runEvent(CampaignLogicOwner* campGame, Side side) = 0;
    virtual void reset() = 0;
      // Remove any references to CP/Leader/SP

    Events::Event getType() const { return d_type; }
};


class RandomEventAllocator {
  public:
    CAMPDATA_DLL static RandomEvent& newEvent(RandomEvent::Events::Event type);
    CAMPDATA_DLL static void resetEvents();
      // Clear all references to CP/Leader/SP
};


#ifdef DEBUG
CAMPDATA_DLL extern LogFileFlush rLog;
#endif

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
