/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Campaign Position Implementation
 *
 * Assumes existance of a global campaign.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "camppos.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"
#include "options.hpp"
#ifdef DEBUG
#include "todolog.hpp"
#endif
#include "CRC.hpp"
/*
 * Constructor
 */

CampaignPosition::CampaignPosition()
{
   town = NoTown;
   headAlong = 0;
   tailAlong = 0;
   direct = Forwards;
   d_lastConnection = NoConnection;
}

/*
 * Set Position to be in a town
 */

void CampaignPosition::setPosition(ITown t, IConnection last)
{
   town = t;
   headAlong = 0;
   tailAlong = 0;
   direct = Forwards;

   // Added by Steven

   d_lastConnection = last;
}

/*
 * Set position to be just outside town along connection
 */

void CampaignPosition::startMoving(IConnection c, ITown t)
{
// if(!atTown())
//     d_lastConnection = connection;

   connection = c;
   headAlong = 1;
   tailAlong = 0;

   const Connection& rConnection = campaignData->getConnection(c);

   Distance length = campaignData->getConnectionLength(c);

   if(rConnection.node1 == t)
   {
      direct = Forwards;
      headAlong = 1;
      tailAlong = 0;
   }
   else if(rConnection.node2 == t)
   {
      direct = Backwards;
      headAlong = length - 1;
      tailAlong = length;
   }
   else
      throw GeneralError("CampaignPosition::= town %d is not part of connection %d",
         (int) t, (int) c);
}

/*
 * Private Function to convert point within connection to a Physical Location
 */


void CampaignPosition::getLocation(Distance distAlong, Location& l) const
{
   if(atTown())
   {
      const Town& t = campaignData->getTown(town);
      l = t.getLocation();
   }
   else
   {
      campaignData->getConnectionLocation(connection, distAlong, l);
   }
}

/*
 * Convert Head Position into a physical location
 */

void CampaignPosition::getHeadLocation(Location& l) const
{
   getLocation(headAlong, l);
}

/*
 * Convert Tail Position into a physical location
 */

void CampaignPosition::getTailLocation(Location& l) const
{
   getLocation(tailAlong, l);
}

/*
 * Get closest town to column head
 */

ITown CampaignPosition::getCloseTown() const
{
   if(atTown())
      return town;
   else
   {
      Connection& rConnection = campaignData->getConnection(connection);
      const Town& t1 = campaignData->getTown(rConnection.node1);
      const Town& t2 = campaignData->getTown(rConnection.node2);
      Distance d = distanceLocation(t1.getLocation(), t2.getLocation());
      if(headAlong < (d / 2))
         return rConnection.node1;
      else
         return rConnection.node2;
   }
}

/*
 * Move Position along it's connection
 *
 * return True if arrived at end of connection.
 */

Boolean CampaignPosition::movePosition(Distance d, Distance columnLength)
{
   ASSERT(!atTown());

   Connection& con = campaignData->getConnection(connection);

   Distance head = headAlong;
   Distance tail = tailAlong;

   Distance length = campaignData->getConnectionLength(connection);

   /*
    * Adjust so always moving from 0..length
    */

   if(direct == Backwards)
   {
      head = length - head;
      tail = length - tail;
   }

   /*
    * Move head
    */

   head += d;

   if(head >= length)
      head = length;

   /*
    * Move Tail
    */

   /*
    * The order of the next 3 if..else is important
    * IF head==length comes after if(head < columnLength)
    * unit's get stuck if(length < columnLength) because tail
    * never leaves previous town.
    */

   Distance wantTail;
   if(head == length)
      wantTail = length;
   else if(head < columnLength)
      wantTail = 0;
   else
      wantTail = head - columnLength;

   if(tail < wantTail)     // Doubling back
   {
      tail += d;
      if(tail > wantTail)
         tail = wantTail;
   }
   else
   {
      tail -= d;
      if(tail < wantTail)
         tail = wantTail;
   }

   /*
    * Have we arrived?
    */

#ifdef DEBUG
   if( ((head == length) && (tail == length)) ||
      Options::get(OPT_QuickMove) )
#else
   if( (head == length) && (tail == length) )
#endif
   {
      ASSERT(!atTown());

      // d_lastConnection = connection;      // this must go BEFORE setPosition

      if(direct == Forwards)
         setPosition(con.node2, connection);
      else
         setPosition(con.node1, connection);


      return True;
   }
   else
   {
      if(direct == Backwards)
      {
         head = length - head;
         tail = length - tail;
      }

      headAlong = head;
      tailAlong = tail;

      return False;
   }
}

/*
 * Get the town that unit is moving towards
 */

ITown CampaignPosition::getDestTown() const
{
   if(atTown())
      return town;
   else
   {
      Connection& con = campaignData->getConnection(connection);
      if(direct == Forwards)
         return con.node2;
      else
         return con.node1;
   }
}

/*
 * Get town that unit is moving away from
 */

ITown CampaignPosition::getLastTown() const
{
   if(atTown())
      return town;
   else
   {
      Connection& con = campaignData->getConnection(connection);
      if(direct == Forwards)
         return con.node1;
      else
         return con.node2;
   }
}

/*
 * Is the position on a connection joined to a town?
 */

Boolean CampaignPosition::isConnected(ITown itown) const
{
   if(atTown())
      return (town == itown);
   else
   {
      Connection& con = campaignData->getConnection(connection);

      return ((itown == con.node1) || (itown == con.node2));
   }
}

/*
 * Is position's head within another position's column?
 */

Boolean CampaignPosition::isIntersecting(const CampaignPosition* target) const
{
   ASSERT(!atTown());
   ASSERT(!target->atTown());
   ASSERT(connection == target->connection);

   return (Boolean) ((headAlong > target->headAlong) ^ (headAlong > target->tailAlong));
}

/*
 * Get the distance from the head to the next town
 */

Distance CampaignPosition::getRemainingDistance() const
{
   if(atTown())
      return 0;

   if(direct == Forwards)
      return campaignData->getConnectionLength(connection) - headAlong;
   else
      return headAlong;
}

Distance CampaignPosition::getDistanceAlong() const
{
   if(atTown())
      return 0;

   if(direct == Forwards)
     return headAlong;
   else
     return campaignData->getConnectionLength(connection) - headAlong;
}


void CampaignPosition::reverseDirection()
{
   ASSERT(!atTown());

   if(direct == Forwards)
      direct = Backwards;
   else
      direct = Forwards;
}


/*
 * Move towards another position that is on the same connection
 */

void CampaignPosition::moveTowards(const CampaignPosition* target)
{
   ASSERT(!atTown());
   ASSERT(!target->atTown());
   ASSERT(connection == target->connection);

// d_lastConnection = connection;

   if(headAlong < target->headAlong)
      direct = Forwards;
   else
      direct = Backwards;

   // Now that wasn't too difficult was it!
}

/*
 * Move towards a town that is on our connection
 */

void CampaignPosition::moveTowards(ITown town)
{
   ASSERT(!atTown());

   const Connection& rConnection = campaignData->getConnection(connection);
// d_lastConnection = connection;

   ASSERT((rConnection.node1 == town) || (rConnection.node2 == town));

   if(rConnection.node1 == town)
      direct = Backwards;
   else
      direct = Forwards;

   // That was easy too...
}

/*
 * Return TRUE If the head is in a town, and tail is moving up
 */

Boolean CampaignPosition::comingToTown() const
{
   if(!atTown())
   {
      if(direct == Forwards)
      {
         Distance length = campaignData->getConnectionLength(connection);
         return headAlong == length;
      }
      else
         return headAlong == 0;
   }

   return False;
}


/*
 * File Handling
 */

const UWORD CampaignPosition::fileVersion = 0x0002;

Boolean CampaignPosition::readData(FileReader& f)
{
   if(f.isAscii())
   {
#ifdef DEBUG
      todoLog.printf("TODO: CampaignPosition::readData().ascii unwritten\n");
#endif
      return False;
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      f.getSLong(headAlong);
      if(version < 0x0001)
         tailAlong = headAlong;
      else
         f.getSLong(tailAlong);

      UBYTE b;
      f.getUByte(b);
      direct = (PosDirection) b;

      if( (headAlong == 0) && (tailAlong == 0) )
         f.getUWord(town);
      else
         f.getUWord(connection);

      if(version >= 0x0002)
        f >> d_lastConnection;

#if defined(EDITOR)
      d_lastConnection = NoConnection;
#endif

   }

   return f.isOK();
}

Boolean CampaignPosition::writeData(FileWriter& f) const
{
   if(f.isAscii())
   {
      f.printf(fmt_sddddn,
         positionToken,
         (int) headAlong,
         (int) tailAlong,
         (int) direct,
         atTown() ? (int) town : (int) connection);
   }
   else
   {
      f.putUWord(fileVersion);
      f.putSLong(headAlong);
      f.putSLong(tailAlong);
      f.putUByte(direct);
      if( (headAlong == 0) && (tailAlong == 0) )
         f.putUWord(town);
      else
         f.putUWord(connection);
      f << d_lastConnection;
   }

   return f.isOK();
}



unsigned short
CampaignPosition::calculateCRC(void) {

   unsigned short crc = 0;

   crc = addIntCRC( crc, (unsigned int) headAlong);
   crc = addIntCRC( crc, (unsigned int) tailAlong);
   crc = addByteCRC( crc, (unsigned char) direct);

   if((headAlong == 0) && (tailAlong == 0)) crc = addIntCRC( crc, (unsigned int) town);
   else crc = addIntCRC( crc, (unsigned int) connection);

   crc = addIntCRC( crc, (unsigned int) d_lastConnection);

   return crc;
}



bool operator == (const CampaignPosition& lhs, const CampaignPosition& rhs)
{
   if(&lhs == &rhs)
      return true;

   // Are they both either at a town or mid-way?

   bool atTown = lhs.atTown();
   if(atTown != rhs.atTown())
      return false;

   // If both at a town then is it the same town?

   if(atTown)
      return lhs.town == rhs.town;

   // if in a connection then is it the same connection?

   if(lhs.connection != rhs.connection)
      return false;

   // do they overlap in the connection?

   Distance l1 = lhs.headAlong;
   Distance l2 = lhs.tailAlong;
   Distance r1 = rhs.headAlong;
   Distance r2 = rhs.tailAlong;
   if(l1 > l2)
      std::swap(l1, l2);
   if(r1 > r2)
      std::swap(r1, r2);
   if(l1 > r1)
   {
      std::swap(l1, r1);
      std::swap(l2, r2);
   }
   return l2 >= r1;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
