/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ARMYBASE_HPP
#define ARMYBASE_HPP

#ifndef __cplusplus
#error armybase.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Base class for Armies to reduce cyclic dependencies
 *
 *----------------------------------------------------------------------
 */

#include "cpidef.hpp"

class ArmyBase
{
	public:
		virtual ~ArmyBase() = 0;
		virtual ICommandPosition command(CPIndex cpi) = 0;
		virtual ConstICommandPosition command(CPIndex cpi) const = 0;
		virtual CPIndex getIndex(const ConstICommandPosition& cpi) const = 0;
};

inline ArmyBase::~ArmyBase() 
{
}

#endif /* ARMYBASE_HPP */

