/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MORALE_HPP
#define MORALE_HPP

#ifndef __cplusplus
#error morale.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Morale calculation functions
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "cpdef.hpp"

class Armies;

class MoraleUtility
{
	public:
		CAMPDATA_DLL static Attribute calcMorale(const Armies* army, ConstICommandPosition cp);
			// Calculates Morale for unit
			// averages up any direct attached SPs and child CPs.
			// The child CPs are not recalculated

#if defined(CUSTOMIZE)
		CAMPDATA_DLL static void setDefaultMorales(Armies* army, ICommandPosition cp);
			// Set morales for any units with zero morale
#endif
};


#endif /* MORALE_HPP */

