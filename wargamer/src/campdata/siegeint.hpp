/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SIEGEINT_HPP
#define SIEGEINT_HPP

#include "wg_msg.hpp"
#include "gamedefs.hpp"
#include "sllist.hpp"
#include "cpidef.hpp"
#include "sync.hpp"

class Armies;

namespace WG_CampaignSiege
{

/*--------------------------------------------------------
 * List of CommandPositions involved in siege
 */

struct CPListItem : public SLink {
	enum { ChunkSize = 5 };

	ICommandPosition d_cpi;
	CPListItem(ICommandPosition cpi) :
	  d_cpi(cpi) {}

	/*
	 * Allocators
	 */

	void* operator new(size_t size);
#ifdef _MSC_VER
	void operator delete(void* deadObject);
#else
	void operator delete(void* deadObject, size_t size);
#endif
};


enum ActionType {
  SA_Sortie,
  SA_MinorOperations,
  SA_NoEffect,
  SA_Breach,
  SA_BreachAndDB,
  SA_Storm,

  ActionType_HowMany
};

class CPList : public SList<CPListItem> {
	SPCount d_spStartCount;        // Total start SP
	SPCount d_spCount;             // Total SP
	SPCount d_infSP;               // Total 'Effective' Infantry SP
	ActionType d_actionType;
	ULONG d_startManpower;
	ULONG d_endManpower;
	Side d_side;
	UWORD d_flags;


	enum {
		IsAttacker     = 0x0001,
		HasEngineer    = 0x0002,
		HasSiegeTrain  = 0x0004,
		HasSurrendered = 0x0008,
		SiegeActive    = 0x0010,
		AutoStorm      = 0x0020,
		BreachBlown    = 0x0040,
		WonSiege       = 0x0080,
		Retreating     = 0x0100,
		Raiding        = 0x0200
	};

	void setFlags(UWORD mask, Boolean f)
	{
		if(f)
			d_flags |= mask;
		else
			d_flags &= ~mask;
	}

public:
	CPList() :
	  d_spStartCount(0),
	  d_spCount(0),
	  d_infSP(0),
	  d_flags(0),
	  d_actionType(SA_NoEffect),
	  d_startManpower(0),
	  d_endManpower(0),
	  d_side(SIDE_Neutral) {}

	~CPList()
	{
		reset();
	}

	void startManpower(ULONG mp)     { d_startManpower = mp; }
	void endManpower(ULONG mp)       { d_endManpower = mp; }
	void startSPCount(SPCount sc)    { d_spStartCount = sc; }
	void spCount(SPCount count)      { d_spCount = count; }
	void infSP(SPCount value)        { d_infSP = value; }
	void isAttacker(Boolean f)       { setFlags(IsAttacker, f); }
	void hasEngineer(Boolean f)      { setFlags(HasEngineer, f); }
	void hasSiegeTrain(Boolean f)    { setFlags(HasSiegeTrain, f); }
	void hasSurrendered(Boolean f)   { setFlags(HasSurrendered, f); }
	void siegeActive(Boolean f)      { setFlags(SiegeActive, f); }
	void autoStorm(Boolean f)        { setFlags(AutoStorm, f); }
	void breachBlown(Boolean f)      { setFlags(BreachBlown, f); }
	void wonSiege(Boolean f)         { setFlags(WonSiege, f); }
	void retreating(Boolean f)       { setFlags(Retreating, f); }
	void raiding(Boolean f)          { setFlags(Raiding, f); }
	void actionType(ActionType at)   { d_actionType = at; }
	void side(Side s)                { d_side = s; }

	ULONG startManpower()    const { return d_startManpower; }
	ULONG endManpower()      const { return d_endManpower; }
	SPCount startSPCount()   const { return d_spStartCount; }
	SPCount spCount()        const { return d_spCount; }
	SPCount infSP()          const { return d_infSP; }
	Boolean isAttacker()     const { return (d_flags & IsAttacker) != 0; }
	Boolean hasEngineer()    const { return (d_flags & HasEngineer) != 0; }
	Boolean hasSiegeTrain()  const { return (d_flags & HasSiegeTrain) != 0; }
	Boolean hasSurrendered() const { return (d_flags & HasSurrendered) != 0; }
	Boolean siegeActive()    const { return (d_flags & SiegeActive) != 0; }
	Boolean autoStorm()      const { return (d_flags & AutoStorm) != 0; }
	Boolean breachBlown()    const { return (d_flags & BreachBlown) != 0; }
	Boolean wonSiege()       const { return (d_flags & WonSiege) != 0; }
	Boolean retreating()     const { return (d_flags & Retreating) != 0; }
	Boolean raiding()        const { return (d_flags & Raiding) != 0; }
	ActionType actionType()  const { return d_actionType; }
	Side side()              const { return d_side; }

#ifdef DEBUG
	void CPList::print(const Armies* armies, const char* title);
#endif
};



struct SiegeInfoData {
  ITown d_town;
  const CPList& d_ourList;
  const CPList& d_theirList;

  SiegeInfoData(ITown town, const CPList& ourList, const CPList& theirList) :
	 d_town(town),
	 d_ourList(ourList),
	 d_theirList(theirList) {}
};


};  // namespace WG_CampaignSiege


#endif
