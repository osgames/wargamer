/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MEASURE_H
#define MEASURE_H

#ifndef __cplusplus
#error measure.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Coordinate System and calculations
 *
 *----------------------------------------------------------------------
 */

#include "trig.hpp"
#include "myassert.hpp"
#include "camptime.hpp"

/*---------------------------------------------------------------------
 * Forward references
 */

class FileReader;
class FileWriter;

/*---------------------------------------------------------------------
 * Our Units:
 *		1 Unit of Time			= TimeTick = 1 Hour (60 seconds)
 *		1 Unit of Distance	= Distance = 110 Yards (1/16 mile)
 *		1 Unit of Speed		= Speed	  = 110 Yards Per Hour (1/16 MPH)	
 */

const int YardsPerDistance = 110;
// enum { TicksPerHour = 1 };
const int TicksPerHour = 1;

/*---------------------------------------------------------------------
 * Some useful constants
 */

const int YardsPerMile	= 1760;
const int FeetPerYard	=  3;
const int InchesPerFoot	= 12;

/*---------------------------------------------------------------------
 * Typedefs for fundamental units
 */

typedef SLONG Distance;
const Distance Distance_MAX = SLONG_MAX;
// #define Distance_MAX SLONG_MAX

typedef ULONG TimeTick;			// Time always positive
const TimeTick TimeTick_MAX = ULONG_MAX;
// #define TimeTick_MAX ULONG_MAX

typedef UWORD Speed;				// Speed always positive
const Speed Speed_MAX = UWORD_MAX;
// #define Speed_MAX UWORD_MAX

/*---------------------------------------------------------------------
 * Some useful Conversions
 */

inline Distance YardsToDistance(int m)
{
	return m / YardsPerDistance;
}

inline Distance MilesToDistance(int m)
{
	return YardsToDistance(m * YardsPerMile);
}

inline int DistanceToYard(Distance d)
{
	return d * YardsPerDistance;
}

inline int DistanceToMile(Distance d)
{
	return DistanceToYard(d) / YardsPerMile;
}

inline TimeTick HoursToTicks(int h)
{
	return h * TicksPerHour;
}

inline TimeTick DaysToTicks(int d)
{
	return HoursToTicks(d * SysTick::HoursPerDay);
}

inline TimeTick WeeksToTicks(int d)
{
	return HoursToTicks(d * SysTick::HoursPerDay * SysTick::DaysPerWeek);
}


inline GameTick ticksToGameTicks(TimeTick t)
{
	return t * SysTick::TicksPerHour / TicksPerHour;
}

inline TimeTick gameTicksToTicks(GameTick g)
{
	return (g.toULONG() * TicksPerHour) / SysTick::TicksPerHour;
}

/*
 * Speed Conversions
 */

// Miles Per Hour

inline Speed MPH(int miles)
{
	return Speed(MilesToDistance(miles) / TicksPerHour);
}

// Yards Per Hour

inline Speed YPH(int yards)
{
	return Speed(YardsToDistance(yards) / TicksPerHour);
}

/*---------------------------------------------------------------------
 * Class for storing a Location
 */

class Location {
	Distance x;
	Distance y;
public:
  	Location() { }
	Location(Distance x, Distance y) { Location::x = x; Location::y = y; }

	Distance getX() const { return x; }
	Distance getY() const { return y; }

	void setX(Distance d) { x = d; }
	void setY(Distance d) { y = d; }

	int operator != (const Location& l) const
	{
		return ( (x != l.x) || (y != l.y) );
	}

	int operator == (const Location& l) const
	{
		return ( (x == l.x) && (y == l.y) );
	}

	Location operator / (unsigned int n)
	{
		ASSERT(n != 0);

		Location l;
		l.x = x / n;
		l.y = y / n;
		return l;
	}

	Location operator + (const Location& l)
	{
		Location l1;
		l1.x = x + l.x;
		l1.y = y + l.y;
		return l1;
	}

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;
};

/*-------------------------------------------------------------------
 * Calculate distance and direction from l1 to l2
 */

inline Distance distanceLocation(const Location& l1, const Location& l2)
{
	return aproxDistance(l2.getX() - l1.getX(), l2.getY() - l1.getY());
}

inline Wangle directionLocation(const Location& l1, const Location& l2)
{
	return direction(l2.getX() - l1.getX(), l2.getY() - l1.getY());
}

/*
 * Useful functions
 */

inline TimeTick timeToTravel(Distance d, Speed s)
{
	ASSERT(s != 0);
	if(s == 0)
		return TimeTick_MAX;
	else
		return d / s;
}

inline Speed speedTravelled(Distance d, TimeTick t)
{
	ASSERT(t != 0);
	if(t == 0)
		return Speed_MAX;
	else
		return Speed(d / t);
}

inline Distance distanceTravelled(Speed s, TimeTick t)
{
	return s * t;
}

#endif /* MEASURE_H */

