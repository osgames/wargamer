/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Campaign Time Implementation
 *
 * It can also be used for battlefield, because it allows any time
 * within an 11 year period at tenth of a second detail.
 *
 *----------------------------------------------------------------------
 *
 * Note that the Date calculations use Branislav L. Slantchev's zDate
 * class from the SNIPPETS colletion at "http://www.brokersys.com/snippets/"
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "camptime.hpp"
#include "myassert.hpp"
#include "misc.hpp"
#include "date.hpp"
#include "filebase.hpp"
#include <stdio.h>

/*
 * Static instance of startDate
 */

// static Date CampaignTime::startDate;
Year StartYear::startYear = 1800;

#if defined(USE_SNIPPETS_DATE)

ULONG StartYear::startDayNumber = 0;

void StartYear::setStartYear(Year y)
{
   startYear = y;
   zDate start(zDate::jan, 1, y);
   startDayNumber = start.DayNumber();
}


void Date::set(GameDay value)
{
   zDate zDate(value + startDayNumber);

   day = (Day) zDate.Day();
   month = (Month) (zDate.Month() - 1);
   year = (Year) zDate.Year();
}

GameDay Date::get() const
{
   zDate zDate((zDate::month) (month + 1), day, year);
   return GameDay(zDate.DayNumber() - startDayNumber);
}

Boolean Date::operator < (const Date& date)
{
  if(year <= date.year)
  {
    if(year < date.year)
      return True;
    else
    {
      if(month <= date.month)
      {
        if(month < date.month)
          return True;
        else
          return (day < date.day);
      }
    }
  }

  return False;
}

Boolean Date::operator > (const Date& date)
{
  if(year >= date.year)
  {
    if(year > date.year)
      return True;
    else
    {
      if(month >= date.month)
      {
        if(month > date.month)
          return True;
        else
          return (day > date.day);
      }
    }
  }

  return False;
}

static UWORD s_fileVersion = 0x0000;

Boolean Date::read(FileReader& f)
{
  UWORD fileVersion;
  f >> fileVersion;

  f >> year;
  f >> month;
  f >> day;

  return True;
}

Boolean Date::write(FileWriter& f) const
{
  f << s_fileVersion;

  f << year;
  f << month;
  f << day;

  return True;
}

#else
// Here's my old code that didn't work too well with leap years

/*
 * Days since start of year to start of month
 */

static UWORD monthDays[] = {
   0,                                  // January
   31,                                 // February
   31+28,                              // March
   31+28+31,                           // April
   31+28+31+30,                        // May
   31+28+31+30+31,                     // June
   31+28+31+30+31+30,                  // July
   31+28+31+30+31+30+31,               // August
   31+28+31+30+31+30+31+31,            // September
   31+28+31+30+31+30+31+31+30,         // October
   31+28+31+30+31+30+31+31+30+31,      // November
   31+28+31+30+31+30+31+31+30+31+30    // December
};

void Date::set(GameDay value)
{
   day   = 1;
   month = January;
   year  = getStartYear();

   int daysPerLeapYear = 365*4 + 1;

   value += 365*3;      // Round up so leap year is at end of 4 years
   int leapYears = value / daysPerLeapYear;

   value -= leapYears * daysPerLeapYear;

   year = Year(value / 365);
   if(year >= 4)
      year = 3;
   value -= year * 365;
   year = Year(year + leapYears * 4 + getStartYear() - 3);

   Boolean isLeapYear = ((year & 3) == 0);
   Boolean afterLeap = (isLeapYear && (value > monthDays[March]));

   if(isLeapYear && (value == monthDays[March]))
   {
      day = 29;
      month = February;
   }
   else
   {
      if(afterLeap)
         value--;

      month = January;
      while((month < December) && (value >= monthDays[month+1]))
         month = Month(month + 1);

      day = Day(value - monthDays[month] + 1);
   }

}

GameDay Date::get() const
{
   ULONG v;

   ASSERT(year >= getStartYear());
   ASSERT(day > 0);
   ASSERT(day <= 31);
   ASSERT(month < 12);

   Year y = Year(year - getStartYear());
   Day d = Day(day - 1);
   Month m = month;

   /*
    * Adjust day for leap years
    */

   d += 1 + (y / 4);

   if(((y & 3) == 0) && (m <= February))
      --d;


   v = y * DaysPerYear + (monthDays[m] + d);

   ASSERT(v <= GameDay_MAX);

   return GameDay(v);
}

#endif   // SNIPPETS

#if defined(__WATCOM_CPLUSPLUS__)
void mulRem(ULONG v, ULONG n, ULONG* div, ULONG* rem);
#pragma aux mulRem = \
   "xor ebx,ebx"        \
   "mov bx,dx"          \
   "xor edx,edx"        \
   "div ebx"            \
   "mov [ecx],eax"      \
   "mov [edi],edx"      \
   parm [eax] [edx] [ecx] [edi] \
   modify [eax ebx edx]
#else
void mulRem(ULONG val, UWORD d, ULONG* div, ULONG* r)
{
   *div = val / d;
   *r = val % d;
}
#endif

void Time::set(GameTick tick)
{
   ULONG r;
   ULONG t = tick.toULONG();

   mulRem(t, SysTick::TicksPerSecond, &t, &r);
   tenth = TSecond(r);
   mulRem(t, 60, &t, &r);
   second = Second(r);
   mulRem(t, 60, &t, &r);
   minute = Minute(r);
   mulRem(t, 24, &t, &r);
   hour = Hour(r);
   ASSERT(t == 0);
}


GameTick Time::get() const
{
   ULONG val;

   val = tenth;
   val += second * SysTick::TicksPerSecond;
   val += minute * SysTick::TicksPerMinute;
   val += hour   * SysTick::TicksPerHour;

   return val;
}

void GameTick::setDate(GameDay day)
{
   GameTick theTime = getTime();
   value = day * TicksPerDay + theTime.toULONG();
}

#if 0
static void CampaignTime::setStartDate(const Date* pdate)
{
   startDate = *pdate;
}
#endif


char* GameTick::toAscii(char* buffer) const
{
   Date date;
   Time time;

   getDate(&date);
   getTime(&time);

   timeToAscii(buffer, date, time);

   return buffer;
}

const char* Date::toAscii(char* buffer)
{
   sprintf(buffer, "%d%s %s %d",
      (int) day,
      getNths(day),
      getMonthName(month, True),
      (int) year);

   return buffer;
}


void timeToAscii(char* buffer, const Date& date, const Time& time)
{
   sprintf(buffer, "%d%s %s %d, %d:%02d:%02d.%d",
      (int) date.day,
      getNths(date.day),
      getMonthName(date.month, True),
      (int) date.year,
      (int) time.hour,
      (int) time.minute,
      (int) time.second,
      (int) time.tenth);
}

#if 0       // REWRITE
/*
 * Subtract 2 times, returning result in gameticks
 */


GameTick operator - (const CampaignTime& ct1, const CampaignTime& ct2)
{
   return
      TicksPerDay * (ct1.getDate() - ct2.getDate()) +
                         ct1.getTime() - ct2.getTime();
}

#endif

/*
 * Find out what time sunrise and sunset are
 * Date is the current date
 * latitude = ???  Could just be Y coordinate or could be angle or ignored?
 *
 * TODO: Calculate correct times based on date and latitude
 */

GameTick getSunRise(const Date& date, int latitude)
{
   Time t = Time(8, 0);    // 8am

   return t.get();
}

GameTick getSunSet(const Date& date, int latitude)
{
   Time t = Time(19, 0);      // 7pm

   return t.get();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
