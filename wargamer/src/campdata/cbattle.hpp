/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef CBATTLE_HPP
#define CBATTLE_HPP

#ifndef __cplusplus
#error cbattle.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Battle Class: public header file
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "cbatmode.hpp"
#include "camppos.hpp"
#include "misc.hpp"
#include "sync.hpp"
#include "compos.hpp"
#include "cleader.hpp"

class CampaignBattle;

class BattleStatus {
public:
   enum Status {
      WinningBig,
      WinningSmall,
      LosingBig,
      LosingSmall,
      Undecided,

      HowMany
   };
};

class PreBattlePosture {
public:
   enum Posture {
      DefendingAt,
      DefendingNear,
      AttackingAt,
      AttackingNear,
      Unknown,
      HowMany
   };
};

class CAMPDATA_DLL  HitLeader : public SLink {
  public:
    enum HitHow {
       Missed,
       HorseKilled,
       SlightWound,
       SeriousWound,
       Captured,
       Killed,

       HitHow_HowMany
    };

    enum { ChunkSize = 10 };
  private:
    ILeader d_leader;
    ICommandPosition d_cpi;
    HitHow d_hitHow;

  public:
    HitLeader() :
       d_leader(NoLeader),
       d_cpi(NoCommandPosition),
       d_hitHow(Missed) {}

    HitLeader(ILeader leader, ICommandPosition cpi, HitHow hitHow) :
       d_leader(leader),
       d_cpi(cpi),
       d_hitHow(hitHow) {}
    ~HitLeader() {}

    ILeader leader() const { return d_leader; }
    ICommandPosition command() const { return d_cpi; }
    HitHow hitHow() const { return d_hitHow; }

    /*
     * File interface
     */

    Boolean read(FileReader& f, OrderBattle& ob);
    Boolean write(FileWriter& f, const OrderBattle& ob) const;

    /*
     * Allocators
     */

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif
};


class HitLeaderList : public SList<HitLeader> {
public:

   /*
    * File interface
    */

   Boolean read(FileReader& f, OrderBattle& ob);
   Boolean write(FileWriter& f, const OrderBattle& ob) const;
};

/*
 * List of units in battle
 */

class CAMPDATA_DLL CampaignBattleUnit : public SLink {
public:
   enum Flags {
     AgainstRetreating = 0x01,       // we're engaged against retreating enemy
     OutOfIt           = 0x02,       // no longer in this battle
     Reinforcement     = 0x04
   };
private:

   ICommandPosition d_cpi;        // What unit is it
   UBYTE d_flags;   // And what is it doing

   void setFlags(UBYTE mask, Boolean f)
   {
      if(f)
         d_flags |= mask;
      else
         d_flags &= ~mask;
   }

public:
   enum { ChunkSize = 10 };
   CampaignBattleUnit() :
     d_cpi(NoCommandPosition),
     d_flags(0) {}
   CampaignBattleUnit(ICommandPosition cpi) : //, BattleUnitMode::Mode mode) :
     d_cpi(cpi),
     d_flags(0) {}

   void setFlag(Flags flag, Boolean f) { setFlags(flag, f); }
   void againstRetreating(Boolean f) { setFlags(AgainstRetreating, f); }
   void outOfIt(Boolean f) { setFlags(OutOfIt, f); }
   void reinforcement(Boolean f) { setFlags(Reinforcement, f); }

   ICommandPosition getUnit() const { return d_cpi; }
   Boolean getFlag(Flags fl) const { return (d_flags & fl) != 0; }
   Boolean againstRetreating() const { return (d_flags & AgainstRetreating) != 0; }
   Boolean outOfIt() const { return (d_flags & OutOfIt) != 0; }
   Boolean reinforcement() const { return (d_flags & Reinforcement) != 0; }

   /*
    * File interface
    */

   bool read(FileReader& f, OrderBattle& ob);
   bool write(FileWriter& f, const OrderBattle& ob) const;

   /*
    * Allocators
    */

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif
};

// List of units in battle. Each side has its own

class CampaignBattleUnitList : public SList<CampaignBattleUnit>
{
public:
    enum Flags {
      DefendingVP             = 0x0001,   // are we defending a victory location?
      Reinforced              = 0x0002,   // have we been reinforced?
      LostContact             = 0x0004,   // we have been defeated but have broken contact
      BreakingOut             = 0x0008,   // we're routed, trapped, and trying to break out
      DefendingChokePoint     = 0x0010,
      DefendingBlownBridge    = 0x0020,
      HasEngineers            = 0x0040,
      HasBridgeTrain          = 0x0080,
      IsMoving                = 0x0100
    };

    CampaignBattleUnitList() :
      d_cinc(NoLeader),
      d_startMorale(0),
      d_currentMorale(0),
      d_negativeMorale(0),
      d_supplyLevel(0),
      d_fatigue(0),
      d_nInfantry(0),
      d_nCavalry(0),
      d_nArtillery(0),
      d_nOther(0),
      d_startNInfantry(0),
      d_startNCavalry(0),
      d_startNArtillery(0),
      d_startNOther(0),
      d_endOfDayMode(EndOfDayMode::Continue),
      d_preBattlePosture(PreBattlePosture::Unknown),
      d_nation(Nationality_MAX),
      d_artilleryNation(Nationality_MAX),
      d_lossCount(0),
      d_loss(0),
      d_spValue(0),
      d_bombValue(0),
      d_dieRoll(0),
      d_statusCount(0),
      d_flags(0),
      d_town(NoTown),
      d_posture(Orders::Posture::Defensive),
      d_nArtillerySpecialist(0),
      d_nCavalrySpecialist(0) {}

    void cinc(ILeader l) { d_cinc = l; }
    void startMorale(Attribute v) { d_startMorale = v; }
    void currentMorale(Attribute v) { d_currentMorale = v; }
    void negativeMorale(Attribute v) { d_negativeMorale = v; }
    void spValue(SPCount v) { d_spValue = v; }
    void nInfantry(SPCount v) { d_nInfantry = v; }
    void nCavalry(SPCount v) { d_nCavalry = v; }
    void nArtillery(SPCount v) { d_nArtillery = v; }
    void nOther(SPCount v) { d_nOther = v; }
    void nStartInfantry(SPCount v) { d_startNInfantry = v; }
    void nStartCavalry(SPCount v) { d_startNCavalry = v; }
    void nStartArtillery(SPCount v) { d_startNArtillery = v; }
    void nStartOther(SPCount v) { d_startNOther = v; }
    void preBattlePosture(PreBattlePosture::Posture p) { d_preBattlePosture = p; }
    void sideNation(Nationality n) { d_nation = n; }
    void sideArtilleryNation(Nationality n) { d_artilleryNation = n; }
    void bombValue(BombValue v) { d_bombValue = v; }
    void incMoraleLoss(UBYTE loss)
    {
      //--- Too many negatives, simplified below
      // if((d_currentMorale-loss) < 0)
      //   d_negativeMorale = -(d_currentMorale-loss);
       if(loss > d_currentMorale)
          d_negativeMorale = loss - d_currentMorale;

      d_currentMorale = clipValue<int>(d_currentMorale-loss, 0, Attribute_Range);
    }
    void lossCount(SPCount v) { d_lossCount += v; }
    void resetLosses() { d_loss = 0; }
    void loss(SPLoss v) { d_loss += v; }
    void dieRoll(UBYTE v) { d_dieRoll = v; }
    void incStatusCount(int v) { d_statusCount += v; }
    void statusCount(int v) { d_statusCount = v; }
    void defendingVP(Boolean f) { setFlags(DefendingVP, f); }
    void reinforced(Boolean f) { setFlags(Reinforced, f); }
    void nArtillerySpecialist(UWORD n) { d_nArtillerySpecialist = n; }
    void nCavalrySpecialist(UWORD n) { d_nCavalrySpecialist = n; }
    void lostContact(Boolean f) { setFlags(LostContact, f); }
    void breakingOut(Boolean f) { setFlags(BreakingOut, f); }
    void defendingChokePoint(Boolean f) { setFlags(DefendingChokePoint, f); }
    void defendingBlownBridge(Boolean f) { setFlags(DefendingBlownBridge, f); }
    void hasEngineers(Boolean f) { setFlags(HasEngineers, f); }
    void hasBridgeTrain(Boolean f) { setFlags(HasBridgeTrain, f); }
    void isMoving(Boolean f) { setFlags(IsMoving, f); }
    void town(ITown t) { d_town = t; }
    void posture(Orders::Posture::Type posture) { d_posture = posture; }
    void supply(Attribute v) { d_supplyLevel = v; }
    void fatigue(Attribute v) { d_fatigue = v; }
    void endOfDayMode(EndOfDayMode::Mode mode) { d_endOfDayMode = mode; }
    void addHitLeader(ILeader iLeader, ICommandPosition cpi, HitLeader::HitHow hitHow)
    {
      HitLeader* hl = new HitLeader(iLeader, cpi, hitHow);
      ASSERT(hl);
      d_hitLeaders.append(hl);
    }

    ILeader cinc() const { return d_cinc; }
    Attribute startMorale() const { return d_startMorale; }
    Attribute currentMorale() const { return d_currentMorale; }
    Attribute negativeMorale() const { return d_negativeMorale; }
    SPCount spValue() const { return d_spValue; }
    SPCount spCount() const { return d_nInfantry + d_nCavalry + d_nArtillery + d_nOther; }
    SPCount nInfantry() const { return d_nInfantry; }
    SPCount nCavalry() const { return d_nCavalry; }
    SPCount nArtillery() const { return d_nArtillery; }
    SPCount nOther() const { return d_nOther; }
    SPCount nStartInfantry() const { return d_startNInfantry; }
    SPCount nStartCavalry() const { return d_startNCavalry; }
    SPCount nStartArtillery() const { return d_startNArtillery; }
    SPCount nStartOther() const { return d_startNOther; }
    int combatManpower() const
    {
      return (d_nInfantry * 1000) + (d_nCavalry * 500) + (d_nArtillery * 250);
    }
    PreBattlePosture::Posture preBattlePosture() const { return d_preBattlePosture; }
    Nationality sideNation() const { return d_nation; }
    Nationality sideArtilleryNation() const { return d_artilleryNation; }
    BombValue bombValue() const { return d_bombValue; }
    SPCount nonArtySPCount() const { return d_nInfantry + d_nCavalry + d_nOther; }
    SPCount lossCount() const { return d_lossCount; }
    SPLoss loss() const { return d_loss; }
    UBYTE dieRoll() const { return d_dieRoll; }
    int statusCount() const { return d_statusCount; }
    Boolean defendingVP() const { return (d_flags & DefendingVP) != 0; }
    Boolean reinforced() const { return (d_flags & Reinforced) != 0; }
    Boolean hasArtillerySpecialist() const { return (d_nArtillerySpecialist > 0); }
    Boolean has3ArtillerySpecialist() const { return (d_nArtillerySpecialist >= 3); }
    Boolean hasCavalrySpecialist() const { return (d_nCavalrySpecialist > 0); }
    UWORD nArtillerySpecialist() const { return d_nArtillerySpecialist; }
    UWORD nCavalrySpecialist() const { return d_nCavalrySpecialist; }
    Boolean lostContact() const { return (d_flags & LostContact) != 0; }
    Boolean breakingOut() const { return (d_flags & BreakingOut) != 0; }
    Boolean defendingChokePoint() const { return (d_flags & DefendingChokePoint) != 0; }
    Boolean hasEngineers() const { return (d_flags & HasEngineers) != 0; }
    Boolean hasBridgeTrain() const { return (d_flags & HasBridgeTrain) != 0; }
    Boolean defendingBlownBridge() const { return (d_flags & DefendingBlownBridge) != 0; }
    Boolean isMoving() const { return (d_flags & IsMoving) != 0; }
    ITown town() const { return d_town; }
    Orders::Posture::Type posture() const { return d_posture; }
    Attribute supply() const { return d_supplyLevel; }
    Attribute fatigue() const { return d_fatigue; }
    EndOfDayMode::Mode endOfDayMode() const { return d_endOfDayMode; }
    HitLeaderList& hitLeaders() { return d_hitLeaders; }
    const HitLeaderList& hitLeaders() const { return d_hitLeaders; }

    /*
     * File interface
     */

    Boolean read(FileReader& f, OrderBattle& ob);
    Boolean write(FileWriter& f, const OrderBattle& ob) const;
private:

    void setFlags(UWORD mask, Boolean f)
    {
      if(f)
         d_flags |= mask;
      else
         d_flags &= ~mask;
    }

    ILeader d_cinc;                  // ranking leader in the battle
    Attribute d_startMorale;         // average starting morale
    Attribute d_currentMorale;       // current morale
    Attribute d_negativeMorale;      // morale below 0
    Attribute d_supplyLevel;
    Attribute d_fatigue;
    SPCount d_startNInfantry;
    SPCount d_startNCavalry;
    SPCount d_startNArtillery;
    SPCount d_startNOther;
    SPCount d_nInfantry;
    SPCount d_nCavalry;
    SPCount d_nArtillery;
    SPCount d_nOther;
    EndOfDayMode::Mode d_endOfDayMode; // continue or withdraw
    PreBattlePosture::Posture d_preBattlePosture;

    Nationality d_nation;            // majority of sp is from this nation
    Nationality d_artilleryNation;   // majority of arty sp is from this nation

    SPCount d_lossCount;             // actual losses in SP's
    SPLoss d_loss;                   // SP losses in Fixed Point
    SPCount d_spValue;               // combined SP Value (Effective SP count)
    BombValue d_bombValue;           // combined Bombardment Value
    UBYTE d_dieRoll;                 // This turns casualty dieRoll. Used for determining round victor
    int d_statusCount;               // running count used to determine if we're winning or losing
    UWORD d_flags;
    ITown d_town;                     // what town are we at?
    Orders::Posture::Type d_posture;  // what is our posture? (offensive or defensive)
    UWORD d_nArtillerySpecialist;
    UWORD d_nCavalrySpecialist;

    HitLeaderList d_hitLeaders;

};

/*===============================================================
 * Unit List Iterators
 */

/*
 * Read Only Iterator
 */

class CAMPDATA_DLL CampaignBattleUnitListIter
{
  // Boolean d_started;
  Boolean d_inBattleOnly;
   SListIter<CampaignBattleUnit> d_iter;
  // CampaignBattleUnitList* d_list;
public:
   CampaignBattleUnitListIter(CampaignBattleUnitList* list, bool inBattleOnly) :
      // d_started(False),
      d_inBattleOnly(inBattleOnly),
      d_iter(list)
      // d_list(list)
   {
   }

   bool operator ++();
   void rewind() { d_iter.rewind(); }

   CampaignBattleUnit* current() const { return d_iter.current(); }

// protected:
   // CampaignBattleUnitList* list() const { return d_list; }
};


/*
 * Read only iterator
 */

class CampaignBattleUnitListIterR : public CampaignBattleUnitListIter {
public:
   CampaignBattleUnitListIterR(const CampaignBattleUnitList* list, bool inBattleOnly = True) :
     CampaignBattleUnitListIter(const_cast<CampaignBattleUnitList*>(list), inBattleOnly) {}

   const CampaignBattleUnit* current() const { return CampaignBattleUnitListIter::current(); }
   // const CampaignBattleUnit* current() { ASSERT(list() && list()->getCurrent()); return list()->getCurrent(); }
};

/*
 * Writeable iterator
 */

class CampaignBattleUnitListIterW : public CampaignBattleUnitListIter {
public:
   CampaignBattleUnitListIterW(CampaignBattleUnitList* list, bool inBattleOnly = True) :
     CampaignBattleUnitListIter(list, inBattleOnly) {}

   // CampaignBattleUnit* current() const { return CampaignBattleUnitListIter::current(); }
   // CampaignBattleUnit* current() { ASSERT(list() && list()->getCurrent()); return list()->getCurrent(); }
};


/*
 * Campaign Battle
 *
 */

class CAMPDATA_DLL CampaignBattle :
   public SLink,
   public CampaignPosition
{
public:

   /*
    * Useful enumerations
    */

   enum Ratio {
     Ratio_Equal = 1,
     Ratio_2To1,
     Ratio_3To1,
     Ratio_4To1,
     Ratio_5To1,
     Ratio_6To1,
     Ratio_7To1,
     Ratio_8To1
   };

   enum { NumberSides = 2 };

   class BattleTurnMode {  // what is happening in the battle
   public:
     enum Value {
       Starting,            // starting the battle
       Deploying,           // forces are deploying
       Fighting,            // forces are fighting
       Reconcentrating,     // fighting is over, winner is reconcentrating
       BattleOver,          // winner has reconcentrated

       HowMany
     };
   };

private:

   CampaignBattleUnitList d_units[NumberSides];    // one for each side
   CampaignData* d_campData;

   /*
    * Modes
    */

   BattleTurnMode::Value d_turnMode;  // what is happening in the battle
   BattleMode::Mode d_mode;           // is it calculated or whatever

   /*
    * flags
    */

public:
   enum Flags {
      BrushAside      = 0x01,   // Brush aside insignificant force
      StoppedForNight = 0x02,   // is battle stopped for the night?
   };

private:
   UBYTE d_flags;

   /*
    * misc. values
    */

   Ratio d_ratio;                     // force ratio. i.e. 2:1, 3:1, etc.
   Side d_victor;                     // which side won
   ICommandPosition d_pursuitTarget;  // if victor has purue option, this is who he chases
   ITown d_siegeTown;                 // a relief attempt at this sieged town
   TimeTick d_nextRoundAt;            // when is the next round to be fought
   TimeTick d_tillWhen;               // time value used for deploy and reconcentration time
   UBYTE d_battleRound;               // which round of battle is it
   UBYTE d_daysOfBattle;              // how many days of battle

#ifdef DEBUG
   int counter;      // This is for debugging...
#endif

   void setFlags(UBYTE mask, bool f)
   {
      if(f)
         d_flags |= mask;
      else
         d_flags &= ~mask;
   }

public:
   CampaignBattle(const CampaignPosition* pos, CampaignData* campData);
   CampaignBattle(CampaignData* campData);
   ~CampaignBattle();

   const CampaignPosition& getPosition() const { return *this; }

   /*
    * Container functions
    */

   CampaignBattleUnit* addUnit(ICommandPosition cpi, bool quickBattle = False);
   void resetUnits() { d_units[0].reset(); d_units[1].reset(); }
   void removeUnit(ICommandPosition cpi);

   const CampaignBattleUnitList* units() const { return d_units; }
   CampaignBattleUnitList& units(Side s) {  return d_units[s]; }
   const CampaignBattleUnitList& units(Side s) const {  return d_units[s]; }
   bool containsUnit(ICommandPosition cpi) const;

   /*
    * Battle mode functions
    */

   bool isCalculated()  const { return d_mode == BattleMode::Calculated;   }
   // bool isInteractive() const { return d_mode == BattleMode::Interactive;  }
   bool isTactical()    const { return d_mode == BattleMode::Tactical;     }
   bool isSettingUp()   const { return d_mode == BattleMode::Setup;        }
   bool isFinished()    const { return d_mode == BattleMode::Finished;     }

   // BattleMode::Mode chooseBattleMode();
    // void initBattleMode(BattleMode::Mode mode);

   // EndOfDayMode::Mode askEndOfDayMode(Side s) const;

   void setMode(BattleMode::Mode mode) { d_mode = mode; }
   BattleMode::Mode mode() const { return d_mode; }
    // void showBattleResult(Side s) const;

   /*
    * Battle-Turn mode functions
    */

   bool starting()        const { return (d_turnMode == BattleTurnMode::Starting); }
   bool deploying()       const { return (d_turnMode == BattleTurnMode::Deploying); }
   bool fighting()        const { return (d_turnMode == BattleTurnMode::Fighting); }
   bool reconcentrating() const { return (d_turnMode == BattleTurnMode::Reconcentrating); }
   bool battleOver()      const { return (d_turnMode == BattleTurnMode::BattleOver); }

   // for backward compatibility
   bool hasBattleBegun() const { return fighting(); }

   void turnMode(BattleTurnMode::Value mode) { d_turnMode = mode; }
   BattleTurnMode::Value turnMode() const { return d_turnMode; }

   /*
    * flags
    */

   void setStoppedForNight(bool f) { setFlags(StoppedForNight, f);}
   void brushAside(bool f) { setFlags(BrushAside, f); }
   bool isStoppedForNight() const { return (d_flags & StoppedForNight) != 0; }
   bool brushAside() const { return (d_flags & BrushAside) != 0; }

   /*
    * Misc.
    */

   void nextRoundAt(TimeTick t) { d_nextRoundAt = t; }
   void tillWhen(TimeTick t) { d_tillWhen = t; }
   void victor(Side side) { d_victor = side; }
   void siegeTown(ITown t) { d_siegeTown = t; }
   void incrementDaysOfBattle() { d_daysOfBattle++; }
   void daysOfBattle(UBYTE days) { d_daysOfBattle = days; }
   void pursuitTarget(ICommandPosition cpi) { d_pursuitTarget = cpi; }
   void clearBattleRound() { d_battleRound = 0; }
   void incBattleRound() { d_battleRound++; }
   void setRatio();
   void setCinC(Side s);

   bool playThisRound(TimeTick t) const { return (t >= d_nextRoundAt); }
   bool timeElapsed(TimeTick t) const { return  (t >= d_tillWhen); }
   Side victor() const { return d_victor; }
   bool hasVictor() const { return (d_victor != SIDE_Neutral); }
   ITown siegeTown() const { return d_siegeTown; }
   bool testForSortieBattle() const { return (d_siegeTown != NoTown); }
   UBYTE daysOfBattle() const { return d_daysOfBattle; }
   ICommandPosition pursuitTarget() const { return d_pursuitTarget; }
   bool hasPursuitTarget() const { return d_pursuitTarget != NoCommandPosition; }
   UBYTE battleRound() const { return d_battleRound; }
   Ratio ratio() const { return d_ratio; }

   /*
    * Side specific functions
    */

   void reinforced(Side s, bool f) { d_units[s].reinforced(f); }
   void nArtillerySpecialist(Side s, UWORD n) { d_units[s].nArtillerySpecialist(n); }
   void nCavalrySpecialist(Side s, UWORD n) { d_units[s].nCavalrySpecialist(n); }
   void startMorale(Side s, Attribute a) { d_units[s].startMorale(a); }
   void currentMorale(Side s, Attribute a) { d_units[s].currentMorale(a); }
   void negativeMorale(Side s, Attribute a) { d_units[s].negativeMorale(a); }
   void moraleLoss(Side s, UBYTE b) { d_units[s].incMoraleLoss(b); }
   void infantryCount(Side s, SPCount value) {  d_units[s].nInfantry(value); }
   void cavalryCount(Side s, SPCount value) {  d_units[s].nCavalry(value); }
   void artilleryCount(Side s, SPCount value) {  d_units[s].nArtillery(value); }
   void otherCount(Side s, SPCount value) {  d_units[s].nOther(value); }
   void startInfantryCount(Side s, SPCount value) {  d_units[s].nStartInfantry(value); }
   void startCavalryCount(Side s, SPCount value) {  d_units[s].nStartCavalry(value); }
   void startArtilleryCount(Side s, SPCount value) {  d_units[s].nStartArtillery(value); }
   void startOtherCount(Side s, SPCount value) {  d_units[s].nStartOther(value); }
   void preBattlePosture(Side s, PreBattlePosture::Posture p) { d_units[s].preBattlePosture(p); }
   void spValue(Side s, SPCount value) {  d_units[s].spValue(value); }
   void addLoss(Side s, SPCount loss) {  d_units[s].lossCount(loss); }
   void resetLosses();
   void loss(Side s, SPLoss loss) { d_units[s].loss(loss); }
   void dieRoll(Side s, UBYTE v) { d_units[s].dieRoll(v); }
   void incStatusCount(Side s, int v) { d_units[s].incStatusCount(v); }
   void bombValue(Side s, BombValue v) { d_units[s].bombValue(v); }
   void posture(Side s, Orders::Posture::Type p) { d_units[s].posture(p); }
   void sideNation(Side s, Nationality n) { d_units[s].sideNation(n); }
   void sideArtilleryNation(Side s, Nationality n) { d_units[s].sideArtilleryNation(n); }
   void supply(Side s, Attribute v) { d_units[s].supply(v); }
   void fatigue(Side s, Attribute v) { d_units[s].fatigue(v); }
   void defendingVictoryLocation(Side s, bool f) { d_units[s].defendingVP(f); }
   void hasEngineers(Side s, bool f) { d_units[s].hasEngineers(f); }
   void hasBridgeTrain(Side s, bool f) { d_units[s].hasBridgeTrain(f); }
   void isMoving(Side s, bool f) { d_units[s].isMoving(f); }
   void endOfDayMode(Side s, EndOfDayMode::Mode m) { d_units[s].endOfDayMode(m); }
   void lostContact(Side s, bool f) { d_units[s].lostContact(f); }
   void breakingOut(Side s, bool f) { d_units[s].breakingOut(f); }
   void defendingChokePoint(Side s, bool f) { d_units[s].defendingChokePoint(f); }
   void defendingBlownBridge(Side s, bool f) { d_units[s].defendingBlownBridge(f); }
   void addHitLeader(Side s, ILeader iLeader, ICommandPosition cpi, HitLeader::HitHow hitHow) { d_units[s].addHitLeader(iLeader, cpi, hitHow); }

   bool reinforced(Side s) const {  return d_units[s].reinforced(); }
   bool hasArtillerySpecialist(Side s) const {  return d_units[s].hasArtillerySpecialist(); }
   bool has3ArtillerySpecialist(Side s) const {  return d_units[s].has3ArtillerySpecialist(); }
   bool hasCavalrySpecialist(Side s) const {  return d_units[s].hasCavalrySpecialist(); }
   UWORD nArtillerySpecialist(Side s) const {  return d_units[s].nArtillerySpecialist(); }
   UWORD nCavalrySpecialist(Side s) const {  return d_units[s].nCavalrySpecialist(); }
   Attribute startMorale(Side s) const {  return d_units[s].startMorale(); }
   Attribute currentMorale(Side s) const {  return d_units[s].currentMorale(); }
   Attribute negativeMorale(Side s) const {  return d_units[s].negativeMorale(); }
   ILeader cinc(Side s) const {  return d_units[s].cinc(); }
   SPCount infantryCount(Side s) const {  return d_units[s].nInfantry(); }
   SPCount cavalryCount(Side s) const {  return d_units[s].nCavalry(); }
   SPCount artilleryCount(Side s) const {  return d_units[s].nArtillery(); }
   SPCount otherCount(Side s) const {  return d_units[s].nOther(); }
   SPCount spCount(Side s) const {  return d_units[s].spCount(); }
   SPCount nonArtySPCount(Side s) const {  return d_units[s].nonArtySPCount(); }
   SPCount spValue(Side s) const {  return d_units[s].spValue(); }
   SPCount getLosses(Side s) const {   return d_units[s].lossCount(); }
   SPLoss loss(Side s) const {  return d_units[s].loss(); }
   UBYTE dieRoll(Side s) const {  return d_units[s].dieRoll(); }
   BombValue bombValue(Side s) const {  return d_units[s].bombValue(); }
   Orders::Posture::Type posture(Side s) const {  return d_units[s].posture(); }
   Nationality sideNation(Side s) const {  return d_units[s].sideNation(); }
   Nationality sideArtilleryNation(Side s) const {  return d_units[s].sideArtilleryNation(); }
   Attribute supply(Side s) const {  return d_units[s].supply(); }
   Attribute fatigue(Side s) const {  return d_units[s].fatigue(); }
   bool defendingVictoryLocation(Side s) const {  return d_units[s].defendingVP(); }
   bool lostContact(Side s) const {  return d_units[s].lostContact(); }
   bool breakingOut(Side s) const {  return d_units[s].breakingOut(); }
   bool defendingChokePoint(Side s) const {  return d_units[s].defendingChokePoint(); }
   bool defendingBlownBridge(Side s) const {  return d_units[s].defendingBlownBridge(); }
   bool hasEngineers(Side s) const {  return d_units[s].hasEngineers(); }
   bool hasBridgeTrain(Side s) const {  return d_units[s].hasBridgeTrain(); }
   bool isMoving(Side s) const {  return d_units[s].isMoving(); }
   EndOfDayMode::Mode endOfDayMode(Side s) const {  return d_units[s].endOfDayMode(); }
   HitLeaderList& hitLeaders(Side s) {  return d_units[s].hitLeaders(); }
   const HitLeaderList& hitLeaders(Side s) const {  return d_units[s].hitLeaders(); }


   bool sideIsMoving(Side s) const;

#ifdef DEBUG
   bool isOver() { return counter-- <= 0; }
#endif

   /*
    * File interface
    */

   bool read(FileReader& f, OrderBattle& ob);
   bool write(FileWriter& f, const OrderBattle& ob) const;

};



class CAMPDATA_DLL CampaignBattleList : public SList<CampaignBattle>, public RWLock
{
   friend class CampaignBattleIterW;
   friend class CampaignBattleIterR;
   friend class CampaignBattleIter;

   CampaignBattle* d_playersBattle;
   CampaignData* d_campData;

public:
   CampaignBattleList(CampaignData* campData);
   ~CampaignBattleList() { startWrite(); reset(); endWrite(); }

   CampaignBattle* createBattle(const CampaignPosition* pos);
   // void startBattle(CampaignBattle* battle);
   void finishBattle(CampaignBattle* battle);
   void removeBattle(CampaignBattle* battle);
   CampaignBattle* findBattle(ICommandPosition cpi);
   const CampaignBattle* findBattle(ICommandPosition cpi) const;
   bool isInABattle(ICommandPosition cpi);
   void removeFromBattle(ICommandPosition cpi);
   // void doEndOfDay();
   int entries() const { return SList<CampaignBattle>::entries(); }

    bool hasPlayerBattle() const { return (d_playersBattle != 0); }
    void playersBattle(CampaignBattle* battle)
    {
        ASSERT(battle != 0);
        ASSERT(d_playersBattle == 0);
        d_playersBattle = battle;
    }

    // void initBattleMode(CampaignBattle* battle, BattleMode::Mode mode);

    int getID(const CampaignBattle* battle) const;
    CampaignBattle* getFromID(int id);


   /*
    * File interface
    */

   bool read(FileReader& f, OrderBattle& ob, CampaignData* campData);
   bool write(FileWriter& f, const OrderBattle& ob) const;
};

/*
 * Read only iterator
 */

class CampaignBattleIterR : public SListIterR<CampaignBattle>
{
   const CampaignBattleList* container;
public:
   CampaignBattleIterR(const CampaignBattleList* list) :
      container(list),
      SListIterR<CampaignBattle>()
   {
      list->startRead();
      SListIterR<CampaignBattle>::init(list);
   }

   ~CampaignBattleIterR()
   {
      container->endRead();
   }

};

/*
 * Hybrid iterator..
 * Data is only locked for writing when a non-constant value is required
 */

class CampaignBattlePtr
{
    public:
        CampaignBattlePtr() : d_list(0), d_battle(0) { }
        CampaignBattlePtr(CampaignBattleList* list, CampaignBattle* battle) :
            d_list(list),
            d_battle(battle)
        {
            if(d_battle && d_list)
                d_list->startWrite();
        }

        ~CampaignBattlePtr()
        {
            if(d_battle && d_list)
                d_list->endWrite();
        }

        CampaignBattlePtr(const CampaignBattlePtr& ptr) :
            d_list(ptr.d_list),
            d_battle(ptr.d_battle)
        {
            if(d_battle && d_list)
                d_list->startWrite();
        }

        CampaignBattlePtr& operator = (const CampaignBattlePtr& ptr)
        {
            d_list = ptr.d_list;
            d_battle = ptr.d_battle;
            if(d_battle && d_list)
                d_list->startWrite();
            return *this;
        }

        CampaignBattle* operator -> () { return d_battle; }

        operator CampaignBattle* () { return d_battle; }

    private:
        CampaignBattleList* d_list;
        CampaignBattle* d_battle;
};

typedef const CampaignBattle* ConstCampaignBattlePtr;


class CAMPDATA_DLL CampaignBattleIter
{
    public:
        CampaignBattleIter() :
            d_container(0),
            d_iter()
        {
        }

        CampaignBattleIter(CampaignBattleList* list) :
            d_container(list),
            d_iter()
        {
            d_container->startRead();
            d_iter.init(list);
        }


        CampaignBattleIter(CampaignBattleList* list, CampaignBattle* battle) :
            d_container(list),
            d_iter()
        {
            d_container->startRead();
            d_iter.init(list, battle);
        }

        ~CampaignBattleIter()
        {
            if(d_container)
                d_container->endRead();
        }

        CampaignBattleIter(const CampaignBattleIter& iter) :
            d_container(iter.d_container),
            d_iter(iter.d_iter)
        {
            if(d_container)
                d_container->startRead();
        }

        CampaignBattleIter& operator = (const CampaignBattleIter& iter)
        {
            if(d_container)
                d_container->endRead();
            d_container = iter.d_container;
            d_iter = iter.d_iter;
            if(d_container)
                d_container->startRead();
            return *this;
        }

        bool next() { return d_iter.next(); }
        void rewind() { d_iter.rewind(); }
        bool operator ++() { return next(); }

        // ConstCampaignBattlePtr constCurrent() const { return ConstCampaignBattlePtr(d_container, d_iter.current()); }
        const CampaignBattle* constCurrent() const { return d_iter.current(); }
        CampaignBattlePtr current() { return CampaignBattlePtr(d_container, d_iter.current()); }

        // ConstCampaignBattlePtr operator -> () const { return constCurrent(); }

        const CampaignBattle* operator -> () const { return d_iter.current(); }
        // CampaignBattlePtr operator -> () { return current(); }

      void removeBattle();

    private:
        CampaignBattleList* d_container;
        SListIter<CampaignBattle> d_iter;
};


#endif /* CBATTLE_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
