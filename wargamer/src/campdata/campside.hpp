/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPSIDE_HPP
#define CAMPSIDE_HPP

/*
 *  Side data needed by campaign game
 */

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "myassert.hpp"
#include "unittype.hpp"

// useful side-loss structure
struct SideLoss {
  static UWORD s_fileVersion;

  SPCount d_infantryLosses;
  SPCount d_cavalryLosses;
  SPCount d_artilleryLosses;
  SPCount d_otherLosses;

  SideLoss() :
	 d_infantryLosses(0),
	 d_cavalryLosses(0),
	 d_artilleryLosses(0),
	 d_otherLosses(0) {}

  SPCount getTotal() const { return d_infantryLosses + d_cavalryLosses +
												d_artilleryLosses + d_otherLosses; }

  /*
	* file interface
	*/

  Boolean read(FileReader& f);
  Boolean write(FileWriter& f) const;
};

struct CampSide {
  static UWORD s_fileVersion;

  SideLoss d_sideLoss;      // Loss data
	LONG     d_vp;            // Victory points
	UWORD    d_nDepots;       // Number of depots currently in service
	UWORD    d_nDepotsUC;     // Number of depots under construction

	CampSide() :
	 d_sideLoss(),
	 d_vp(-1),
	 d_nDepots(0),
	 d_nDepotsUC(0) {}

	/*
	* file interface
	*/

	Boolean read(FileReader& f);
	Boolean write(FileWriter& f) const;
};

class CampaignSides {
	 static UWORD s_fileVersion;
	 enum { NumberSides = 2 };

	 CampSide d_sideData[NumberSides];
	public:
	 CampaignSides() {}
	 ~CampaignSides() {}

	 void resetVictory()
	 {
		for(Side s = 0; s < NumberSides; s++)
		  d_sideData[s].d_vp = 0;
	 }

	 void addSideVictory(ULONG points, Side s)
	 {
		ASSERT(s < NumberSides);
		d_sideData[s].d_vp += points;
	 }

	 void removeSideVictory(LONG points, Side s)
	 {
		 ASSERT(s < NumberSides);
     d_sideData[s].d_vp = maximum(0, d_sideData[s].d_vp - points);
	 }

	 LONG getSideVictory(Side s) const
	 {
		ASSERT(s < NumberSides);
		return d_sideData[s].d_vp;
	 }

	 void incSideDepotBuilt(Side s)
	 {
		ASSERT(s < NumberSides);
		d_sideData[s].d_nDepots++;
	 }

	 void decSideDepotsBuilt(Side s)
	 {
		ASSERT(s < NumberSides);
		d_sideData[s].d_nDepots = maximum(0, d_sideData[s].d_nDepots - 1);
	 }

	 ULONG getSideDepotsBuilt(Side s) const
	 {
		ASSERT(s < NumberSides);
		return d_sideData[s].d_nDepots;
	 }

	 void incSideDepotsUnderConstruction(Side s)
	 {
		ASSERT(s < NumberSides);
		d_sideData[s].d_nDepotsUC++;
	 }

	 void decSideDepotsUnderConstruction(Side s)
	 {
		ASSERT(s < NumberSides);
		d_sideData[s].d_nDepotsUC = maximum(0, d_sideData[s].d_nDepotsUC - 1);
	 }

	 ULONG getSideDepotsUnderConstruction(Side s) const
	 {
		ASSERT(s < NumberSides);
		return d_sideData[s].d_nDepotsUC;
	 }

	 CAMPDATA_DLL void addSideLosses(Side s, SPCount loss, BasicUnitType::value type);

	 CAMPDATA_DLL SPCount getSideLosses(Side s) const;
	 CAMPDATA_DLL SPCount getTypeLosses(Side s, BasicUnitType::value type) const;

	 SPCount getTotal() const
	 {
		return d_sideData[0].d_sideLoss.getTotal() + d_sideData[1].d_sideLoss.getTotal();
	 }

	 /*
	  * file interface
	  */

	 CAMPDATA_DLL Boolean read(FileReader& f);
	 CAMPDATA_DLL Boolean write(FileWriter& f) const;

  private:
	 const char* getChunkName() const;

};

#if 0
class VictoryPoints {
	 static UWORD fileVersion;

	 ULONG d_side0Victory;
	 ULONG d_side1Victory;
  public:

	 VictoryPoints() { reset(); }

	 void reset()
	 {
		d_side0Victory = 0;
		d_side1Victory = 0;
	 }

	 void addSideVictory(ULONG points, Side s)
	 {
		if(s == 0)
		  d_side0Victory += points;
		if(s == 1)
		  d_side1Victory += points;
	 }

	 ULONG getSideVictory(Side s)
	 {
		if(s == 0)
		  return d_side0Victory;
		if(s == 1)
		  return d_side1Victory;

		return 0;
	 }


	 /*
	  * file interface
	  */

	 Boolean read(FileReader& f);
	 Boolean write(FileWriter& f) const;

	 const char* getChunkName() const;
};


class TotalLosses {
	 static UWORD fileVersion;

	 enum {
		Side0,
		Side1,
		Sides_HowMany
	 };

	 SideLoss d_sideLosses[Sides_HowMany];

  public:
	 TotalLosses() {}

	 void addSideLosses(Side s, SPCount loss, BasicUnitType::value type);

	 SPCount getSideLosses(Side s) const;
	 SPCount getTypeLosses(Side s, BasicUnitType::value type) const;

	 SPCount getTotal() const { return d_sideLosses[Side0].getTotal() + d_sideLosses[Side1].getTotal(); }

	 /*
	  * file interface
	  */

	 Boolean read(FileReader& f);
	 Boolean write(FileWriter& f) const;

	 const char* getChunkName() const;
};
#endif

#endif

