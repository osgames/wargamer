/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ROUTELST_HPP
#define ROUTELST_HPP

#include "cdatadll.h"
#include "sllist.hpp"
#include "sync.hpp"
#include "gamedefs.hpp"

class FileReader;
class FileWriter;

// Disable locking on RouteLists
// Otherwise game is incredibly slow, especially when
// processing supply lines, because RouteLists are created
// on the stack in recursive functions.

#define NO_ROUTELIST_LOCK

struct RouteNode : public SLink {

	  ITown d_town;

	  RouteNode() : d_town(NoTown) {}
	  RouteNode(ITown town) : d_town(town) {}

	  /*
		* File interface
		*/

	  CAMPDATA_DLL Boolean read(FileReader& f);
	  CAMPDATA_DLL Boolean write(FileWriter& f) const;

	  CAMPDATA_DLL void* operator new(size_t size);
#ifdef _MSC_VER
	  CAMPDATA_DLL void operator delete(void* deadObject);
#else
	  CAMPDATA_DLL void operator delete(void* deadObject, size_t size);
#endif
	  enum { ChunkSize = 50 };
};

class RouteList :
#ifndef NO_ROUTELIST_LOCK
   public RWLock,
#endif
   public SList<RouteNode>
{
		 enum { MaxPreviousNodes = 5 };
	  public:
		 CAMPDATA_DLL void addNode(ITown town);
		 CAMPDATA_DLL void addPreviousNode(ITown town);
		 CAMPDATA_DLL void removeNode(ITown town);

	     CAMPDATA_DLL RouteList& operator = (const RouteList& n);

		 /*
		  * File interface
		  */

		 CAMPDATA_DLL Boolean read(FileReader& f);
		 CAMPDATA_DLL Boolean write(FileWriter& f) const;

};

/*
 * Locking Read only iterator
 */

class RouteListIterR : public SListIterR<RouteNode>
{
	const RouteList* d_list;
public:
	RouteListIterR(const RouteList* list) :
		SListIterR<RouteNode>(list),
		d_list(list)
	{
#ifndef NO_ROUTELIST_LOCK
		d_list->startRead();
#endif
	}

	~RouteListIterR()
	{
#ifndef NO_ROUTELIST_LOCK
		d_list->endRead();
#endif
	}

};

/*
 * Locking Writeable iterator
 */

class RouteListIterW : public SListIter<RouteNode>
{
	RouteList* d_list;
public:
	RouteListIterW(RouteList* list) :
		SListIter<RouteNode>(list),
		d_list(list)
	{
#ifndef NO_ROUTELIST_LOCK
		d_list->startWrite();
#endif
	}

	~RouteListIterW()
	{
#ifndef NO_ROUTELIST_LOCK
		d_list->endWrite();
#endif
	}
};

/*
 * Non-locking Read-only iterator
 */

class RouteListIterNLR : public SListIterR<RouteNode> {
public:
	RouteListIterNLR(const RouteList* list) :
		SListIterR<RouteNode>(list) {}
};

/*
 * Non-locking Writable iterator
 */

class RouteListIterNLW : public SListIter<RouteNode> {
public:
	RouteListIterNLW(RouteList* list) :
		SListIter<RouteNode>(list) {}
};

#endif
