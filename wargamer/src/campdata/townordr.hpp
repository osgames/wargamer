/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOWNORDR_HPP
#define TOWNORDR_HPP

#include "cdatadll.h"
#include "sllist.hpp"
#include "gamedefs.hpp"
#include "measure.hpp"

class FileReader;
class FileWriter;
class ArmyBase;

struct CAMPDATA_DLL Town_COrder : public SLink {
   enum Type {
      RepairBridge,
      BlowBridge,
      BuildDepot,
      BuildFort,
      RemoveDepot,

      TO_HowMany
   };

   ITown    d_town;
   Type     d_type;
   TimeTick d_tillWhen; // order takes till this time to complete

   Town_COrder() :
      d_town(NoTown),
		d_type(TO_HowMany),
		d_tillWhen(ULONG_MAX) {}

   Town_COrder(ITown town, Type type, TimeTick tillWhen) :
      d_town(town),
		d_type(type),
		d_tillWhen(tillWhen) {}

   /*
    * File interface
    */

   Boolean read(FileReader& f);
   Boolean write(FileWriter& f) const;

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif
   enum { ChunkSize = 50 };

};


// list class
class Town_COrderList : public SList<Town_COrder> {
		// Disable copy constructors

	public:
      Town_COrderList() : SList<Town_COrder>() { }
      Town_COrderList(const Town_COrderList& list) { copy(list); }
      Town_COrderList& operator = (const Town_COrderList& list) { copy(list); return *this; }

      // unchecked ammended by Paul - 12/9/99
      const Town_COrder* find(ITown town)
      {
			SListIterR<Town_COrder> iter(this);
			while(++iter)
			{
            if(iter.current()->d_town == town)
               return iter.current();
         }

         return 0;
      }

      const Town_COrder* find(ITown town, Town_COrder::Type type)
      {
			SListIterR<Town_COrder> iter(this);
			while(++iter)
			{
            if(iter.current()->d_town == town && iter.current()->d_type == type)
               return iter.current();
         }

         return 0;
      }
      // end unchecked ammended by Paul - 12/9/99

      /*
       * File interface
       */

      CAMPDATA_DLL Boolean read(FileReader& f);
      CAMPDATA_DLL Boolean write(FileWriter& f) const;

	private:
		void copy(const Town_COrderList& list)
		{
			SListIterR<Town_COrder> iter(&list);
			while(++iter)
			{
				append(new Town_COrder(*iter.current()));
			}
		}
};

class Town_COrderListIterR : public SListIterR<Town_COrder> {
   public:
      Town_COrderListIterR(const Town_COrderList* list) : SListIterR<Town_COrder>(list) { }
};

class Town_COrderListIter : public SListIter<Town_COrder> {
   public:
      Town_COrderListIter(Town_COrderList* list) : SListIter<Town_COrder>(list) { }
};

#endif
