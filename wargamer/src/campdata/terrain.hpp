/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef _TERRAIN_HPP
#define _TERRAIN_HPP

#ifndef __cplusplus
#error terrain.hpp is for use with C++
#endif

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "array.hpp"
#include "StringPtr.hpp"
//#include "measure.hpp"		// For speed


class WinFileAsciiChunkReader;
class FileChunkReader;

/*
 * Terrain
 */

class Terrain {
  public:
	 enum GroundType {
		Open,
		Rough,
		Wooded,
		Hilly,
		WoodedHilly,
		MountainPass,
		Marsh,

		GroundType_HowMany
	 };

	 enum RiverType {
		NoRiver,
		MinorRiver,
		MajorRiver,

		RiverType_HowMany
	 };

	 CAMPDATA_DLL static const char* groundTypeName(GroundType type);
	 CAMPDATA_DLL static const char* riverTypeName(RiverType type);
	 CAMPDATA_DLL static Boolean isChokePoint(GroundType groundType, RiverType riverType);
};

/*
 *  An entry in TerrainTypeTable
 */

class TerrainTypeItem {
	friend class TerrainTypeTable;
//	CString description;				      // What is it called? e.g. "Hilly"
	StringPtr d_description;			      // What is it called? e.g. "Hilly"

	Terrain::GroundType d_groundType;    // Open, Wooded, Hilly, etc.
	Terrain::RiverType d_riverType;      // No River, Minor River, or Major River

//	Boolean d_chokePoint;               // Is it a chokepoint? e.g. a mountain pass

public:
	 TerrainTypeItem() :
		d_groundType(Terrain::Open),
		d_riverType(Terrain::NoRiver) {}
//		d_chokePoint(False) {}

	 ~TerrainTypeItem() {}

	/*
	 * Access Functions
	 */

	const char* getName() const  { return d_description; }
	Boolean isChokePoint() const { return (isBridge() || isPass()); }
	Boolean isBridge() const     { return (d_riverType == Terrain::MajorRiver); }
	Boolean isPass() const       { return (d_groundType == Terrain::MountainPass); }

	Terrain::GroundType getGroundType() const { return d_groundType; }
	Terrain::RiverType getRiverType() const { return d_riverType; }

	/*
	 * File Interface
	 */

	Boolean readData(FileChunkReader& f);
};

/*
 *  TerrainType container class
 */

class TerrainTypeTable
{
	Array<TerrainTypeItem> items;

public:
	TerrainTypeTable();
	~TerrainTypeTable();

	Boolean init(const char* fileName);

	TerrainType entries() const
	{
		return (TerrainType) items.entries();
	}

	const TerrainTypeItem& getData(TerrainType n)
	{
		return items[n];
	}

	const TerrainTypeItem& operator[](TerrainType n) const
	{
		return items[n];
	}

	/*
	 * File Interface
	 */

	Boolean readData(WinFileAsciiChunkReader& f);
};



#endif
