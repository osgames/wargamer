##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#####################################
# Makefile Include for Campaign Data
#
# This is included from wargamer.mk
#
# It might be better to have a stand-alone makefile for each
# library instead of including like this.  That would make
# compiling individual libraries easier and faster, and also
# encourage each part of the program to be more independant
#-------------------------------------------------------

ROOT=..\..

############ Comment the MAKEDLL=1 to not make a DLL version
!ifndef MAKEDLL
!ifndef %NO_WG_DLL
MAKEDLL=1
!endif
!endif

!ifdef MAKEDLL
FNAME = campdata
!else
NAME = campdata
!endif

lnk_dependencies += campdata.mk

!include $(ROOT)\config\wgpaths.mif

OBJS =	armies.obj   &
	compos.obj   &
    campunit.obj &
	camppos.obj  &
	campord.obj  &
	campdint.obj &
	campdimp.obj &
	campint.obj  &
	cwin_int.obj &
	camptime.obj &
	measure.obj  &
	gameob.obj   &
	town.obj     &
	townbld.obj  &
	unitlist.obj &
	terrain.obj  &
	c_cond.obj   &
	weather.obj  &
	randevnt.obj &
	campinfo.obj &
	campevt.obj  &
	armistic.obj &
	ctimdata.obj &
	c_obutil.obj &
	campmsg.obj &
	ileader.obj &
	armyutil.obj &
	townordr.obj &
	repopool.obj &
	camp_snd.obj &
	c_sounds.obj &
	morale.obj   &
	campside.obj &
    c_rand.obj   &
    cu_mode.obj  &
    routelst.obj &
    supplyLine.obj

!ifndef EDITOR
OBJS +=	route.obj    &
	cbattle.obj
!endif

!ifndef NODEBUG
OBJS +=	unitlog.obj
!endif

CFLAGS += -i=$(ROOT)\src\system;$(ROOT)\src\gamesup
# CFLAGS += -i=$(ROOT)\src\camp_ai
CFLAGS += -i=$(ROOT)\res
CFLAGS += -i=$(ROOT)\src\ob

all :: $(TARGETS) .SYMBOLIC
	@%null

!ifdef MAKEDLL
###########
#Make DLL
###########

CFLAGS += -DEXPORT_CAMPDATA_DLL
SYSLIBS += COMCTL32.LIB VFW32.LIB DSOUND.LIB
LIBS += system.lib
LIBS += gamesup.lib
LIBS += ob.lib
# !include $(ROOT)\src\gamesup\gamesup.mif
# !include $(ROOT)\src\system\system.mif

!include $(ROOT)\config\dll95.mif

!else

###########
# Make Library
###########

!include $(ROOT)\config\lib95.mif

!endif

.before:
	@if exist nation.* del /z nation.*
	@if exist nations.* del /z nations.*
	@if exist victory.* del /z victory.*
	@if exist aic_udat.* del /z /s aic_udat.*
	@if exist unitinfo.* del /z /s unitinfo.*
	@if exist cbatint.hpp del /z /s cbatint.hpp


#####################################################################
# $Log$
#####################################################################
