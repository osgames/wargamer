/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef C_COND_HPP
#define C_COND_HPP

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "camptime.hpp"
#include "sllist.hpp"
#include "measure.hpp"

#if defined(CUSTOMIZE)
#include "StringPtr.hpp"
// #include "string.hpp"
#endif

class FileReader;
class FileWriter;
class CampaignData;
class CampaignLogicOwner;
class Armies;


namespace WG_CampaignConditions
{

class Conditions;

/*
 *  Campaign Conditions
 *
 *  Each condition has:
 *  1. A set of Sub-Conditions(any number allowed) that must be met.
 *  2. A flag specifying whether all or just one Sub-Conditions need to be met
 *  3. A set of Actions that will occur if condition is met
 *
 */


/*
 * utility struct to hold data that can be passed to conditions
 */

struct ConditionTypeData {
  ICommandPosition d_cpi;
  ILeader d_leader;
  ITown d_town;
  Nationality d_nation;
  Side d_side;

  ConditionTypeData() :
    d_cpi(NoCommandPosition),
    d_leader(NoLeader),
    d_town(NoTown),
		d_nation(NATION_Neutral),
    d_side(SIDE_Neutral) {}
};

/*================ ConditionTypes ======================================
 *  ConditionTypes are a set of Sub-Conditions that must be met
 *  for the condition to be met.
 *
 *  i.e. French enter Hamburg, Napolean dies, Allies have x number of Victory Points, etc
 *
 */

class ConditionType : public SLink {
  public:
	 enum Type {
      ID_First = 0,
      ID_TownChangesSide = ID_First,
      ID_CasualtyLevelReached,
      ID_MovesIntoNeutral,
      ID_LeaderIsDead,
      ID_DateReached,
      ID_NationAtWar,
		ID_SideVPTotal,
      ID_SideOwnsTown,
      ID_ArmisticeEnds,
      ID_ArmisticeStarts,

      Type_HowMany
    };

  protected:
    Boolean d_conditionMet;
  private:
    Type d_type;

  public:
    ConditionType(Type t) :
      d_conditionMet(False),
      d_type(t)
    {
      ASSERT(d_type < Type_HowMany);
    }

    void conditionMet(Boolean f) { d_conditionMet = f; }
    Boolean conditionMet() { return d_conditionMet; }

    Type getType() const { return d_type; }

#if !defined(EDITOR)
	 virtual void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data) = 0;
    virtual void dailyUpdate(CampaignLogicOwner* campGame) = 0;
#endif

#if defined(CUSTOMIZE)
    virtual bool checkData() = 0;
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
	 virtual String describe(CampaignData* campData) = 0;
#endif

		/*
     * file interface
     */

    virtual Boolean read(FileReader& f, Armies* army) = 0;
    virtual Boolean write(FileWriter& f, const Armies* army) const = 0;


};

/*=============== ConditionType derived classes ==================
 *
 */

class TownChangesSide : public ConditionType {

    ITown d_town;                       // If specified town changes sides
    Side d_side;

    static UWORD fileVersion;

  public:

    TownChangesSide() :
      ConditionType(ID_TownChangesSide),
      d_town(NoTown),
      d_side(SIDE_Neutral) {}

    /*
		 * virtual functions from ConditionType
     */

#if !defined(EDITOR)
	 void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data);
    void dailyUpdate(CampaignLogicOwner* campGame) {}
#endif

#if defined(CUSTOMIZE)

    /*
     * functions used by editor
     */

    ITown getTown() const { return d_town; }
    Side getSide() const { return d_side; }

    void setTown(ITown t) { d_town = t; }
    void setSide(Side s) { d_side = s; }
#endif
#if defined(CUSTOMIZE)
    bool checkData();
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif

	 /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;

};

class CasualtyLevelReached : public ConditionType {

    SPCount d_side0Casualties;          // If Casualty levels reach this
    SPCount d_side1Casualties;

    static UWORD fileVersion;

  public:

    CasualtyLevelReached() :
			ConditionType(ID_CasualtyLevelReached),
      d_side0Casualties(0),
      d_side1Casualties(0) {}

#if !defined(EDITOR)
	 void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data) {}
	 void dailyUpdate(CampaignLogicOwner* campGame);
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)
	 virtual String describe(CampaignData* campData);
#endif
#if defined(CUSTOMIZE)
	 bool checkData();

    /*
     * functions used by editor
     */

    SPCount getSide0Casualties() const { return d_side0Casualties; }
    SPCount getSide1Casualties() const { return d_side1Casualties; }

    void setSide0Casualties(SPCount value) { d_side0Casualties = value; }
    void setSide1Casualties(SPCount value) { d_side1Casualties = value; }

#endif // (CUSTOMIZE)

    /*
     * file interface
		 */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;

};

class MovesIntoNeutral : public ConditionType {

    Nationality d_nationality;     // If side moves into specified neutrals town
    Side d_side;

    static UWORD fileVersion;

  public:

    MovesIntoNeutral() :
      ConditionType(ID_MovesIntoNeutral),
		d_nationality(NATION_Neutral),
      d_side(SIDE_Neutral) {}

#if !defined(EDITOR)
	 void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data);
	 void dailyUpdate(CampaignLogicOwner* campGame) {}
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)
	 virtual String describe(CampaignData* campData);
#endif

#ifdef CUSTOMIZE
	 bool checkData();
    /*
     * functions used by editor
     */

    Nationality getNation() const { return d_nationality; }
    Side getSide() const { return d_side; }

    void setNation(Nationality n) { d_nationality = n; }
    void setSide(Side s) { d_side = s; }

#endif //(CUSTOMIZE)

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;

};

class LeaderIsDead : public ConditionType {

    ILeader d_leader;                   // If this leader dies

    static UWORD fileVersion;

  public:

    LeaderIsDead() :
      ConditionType(ID_LeaderIsDead),
      d_leader(NoLeader) {}

#if !defined(EDITOR)
	 void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data);
	 void dailyUpdate(CampaignLogicOwner* campGame) {}
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)
		virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
    /*
     * functions used by editor
     */

     ILeader getLeader() const { return d_leader; }
     void setLeader(ILeader l) { d_leader = l; }

#endif  // (CUSTOMIZE)

    /*
     * file interface
		 */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;

};

class DateReached : public ConditionType {

    Date d_date;
    Boolean d_reached;         // if true check if date has been reached
                               // if not true check if date hasn't been reached

		static UWORD fileVersion;

  public:

    DateReached() :
      ConditionType(ID_DateReached),
      d_reached(False)
    {
      d_date.set(0, 0, 0);
    }

#if !defined(EDITOR)
	 void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data) {}
	 void dailyUpdate(CampaignLogicOwner* campGame);
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
	 virtual String describe(CampaignData* campData);
#endif

#if defined(CUSTOMIZE)
		bool checkData();

    /*
     * functions used by editor
     */

    Date& getDate() { return d_date; }
    Boolean mustReach() const { return d_reached; }

		void setDate(Day d, Month m, Year y) { d_date.set(d, m, y); }
    void mustReach(Boolean f) { d_reached = f; }

#endif  // (CUSTOMIZE)

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;

};


class NationAtWar : public ConditionType {

    Nationality d_nation;
    Boolean d_atWar;

    static UWORD fileVersion;
  public:

    NationAtWar() :
      ConditionType(ID_NationAtWar),
			d_nation(NATION_Neutral),
      d_atWar(False) {}

#if !defined(EDITOR)
		void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data) {}
    void dailyUpdate(CampaignLogicOwner* campGame);
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
    /*
     * functions used by editor
     */

    Nationality getNation() const { return d_nation; }
    Boolean atWar() const { return d_atWar; }

    void setNation(Nationality n) { d_nation = n; }
    void atWar(Boolean f) { d_atWar = f; }

#endif //(CUSTOMIZE)

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;

};

class SideVPTotal : public ConditionType {

    Side d_side;
    UWORD d_vpTotal;

    static UWORD fileVersion;
  public:

    SideVPTotal() :
      ConditionType(ID_SideVPTotal),
      d_side(SIDE_Neutral),
      d_vpTotal(0) {}

#if !defined(EDITOR)
		void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data) {}
    void dailyUpdate(CampaignLogicOwner* campGame);
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
    /*
     * functions used by editor
     */

    void setSide(Side s) { d_side = s; }
    void setVPTotal(UWORD v) { d_vpTotal = v; }

    Side getSide() const { return d_side; }
    UWORD getVPTotal() const { return d_vpTotal; }

#endif //(CUSTOMIZE)

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;
};

class SideOwnsTown : public ConditionType {

    Side d_side;
    ITown d_town;

    static UWORD fileVersion;
  public:

		SideOwnsTown() :
      ConditionType(ID_SideOwnsTown),
      d_side(SIDE_Neutral),
      d_town(NoTown) {}

#if !defined(EDITOR)
	 void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data) {}
    void dailyUpdate(CampaignLogicOwner* campGame);
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
    /*
     * functions used by editor
     */

		void setSide(Side s) { d_side = s; }
    void setTown(ITown t) { d_town = t; }

    Side getSide() const { return d_side; }
    ITown getTown() const { return d_town; }

#endif //(CUSTOMIZE)

    /*
		 * file interface
     */

    Boolean read(FileReader& f, Armies* army);
		Boolean write(FileWriter& f, const Armies* army) const;
};

class ArmisticeEnds : public ConditionType {
    static UWORD fileVersion;
  public:

    ArmisticeEnds() :
		ConditionType(ID_ArmisticeEnds) {}

#if !defined(EDITOR)
	 void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data);
    void dailyUpdate(CampaignLogicOwner* campGame) {}
#endif

#if defined(CUSTOMIZE)
    bool checkData();
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
		virtual String describe(CampaignData* campData);

    /*
     * functions used by editor
     */

#endif //(CUSTOMIZE)

    /*
     * file interface
		 */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;
};

class ArmisticeStarts : public ConditionType {
    static UWORD fileVersion;
  public:

    ArmisticeStarts() :
			ConditionType(ID_ArmisticeStarts) {}

#if !defined(EDITOR)
		void instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data) {}
		void dailyUpdate(CampaignLogicOwner* campGame);
#endif


		/*
		 * functions used by editor
		 */
#if defined(CUSTOMIZE)
		bool checkData();
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
		virtual String describe(CampaignData* campData);
#endif

		/*
		 * file interface
		 */

		Boolean read(FileReader& f, Armies* army);
		Boolean write(FileWriter& f, const Armies* army) const;
};

class ConditionTypeAllocator {
	public:
		CAMPDATA_DLL static ConditionType* newConditionType(ConditionType::Type type);
};

class ConditionTypeList : public SList<ConditionType> {
		static UWORD  fileVersion;

	public:
		ConditionTypeList() { }

#if defined(CUSTOMIZE) || defined(DEBUG)
		CAMPDATA_DLL bool checkData();
#endif

		/*
		 *  File interface
     */

    Boolean read(FileReader& f, Armies* army);
		Boolean write(FileWriter& f, const Armies* army) const;

};

/*===================== ActionType class ===========================
 *  An 'Action' is what occurs when a condition has been met
 *
 *  i.e French Victory, Armistice Occurs, Austria Enters War, etc.
 *
 */

class ActionType : public SLink {
	public:
		enum Type {
			 ID_First = 0,
			 ID_Victory = ID_First,
			 ID_Armistice,
			 ID_NationEntersWar,
			 ID_Reinforcements,
			 ID_LeaderLeaves,
			 ID_AllyDefects,
			 ID_AdjustResources,
			 ID_LeaderEnters,

			 Type_HowMany
		};

	private:
		Type d_type;

	public:
		ActionType(Type type) : d_type(type) {}
		virtual ~ActionType() {}

#if !defined(EDITOR)
	 virtual Boolean run(CampaignLogicOwner* campGame) = 0;
#endif
#if defined(CUSTOMIZE)
		virtual bool checkData() = 0;
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
		virtual String describe(CampaignData* campData) = 0;
#endif
		Type getType() const { return d_type; }

		/*
		 * file interface
		 */

		virtual Boolean read(FileReader& f, Armies* army) = 0;
		virtual Boolean write(FileWriter& f, const Armies* army) const = 0;
};

//=========== ActionType derived classes ====================

class SideHasVictory : public ActionType {
		Side d_whoWins;
    TimeTick d_victoryDate;
		static UWORD fileVersion;
	public:
		SideHasVictory() :
			ActionType(ActionType::ID_Victory),
			d_whoWins(SIDE_Neutral),
			d_victoryDate(0) {}

		~SideHasVictory() {}

#if !defined(EDITOR)
		Boolean run(CampaignLogicOwner* campGame);
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)
		virtual String describe(CampaignData* campData);
#endif
#if defined(CUSTOMIZE)
		bool checkData();
		void whoWins(Side side) { d_whoWins = side; }
		Side whoWins() const { return d_whoWins; }
#endif

		/*
		 * file interface
		 */

		Boolean read(FileReader& f, Armies* army);
		Boolean write(FileWriter& f, const Armies* army) const;
};

class ArmisticeReached : public ActionType {
		TimeTick d_armisticeTime;

    static UWORD fileVersion;
  public:
    ArmisticeReached() :
      ActionType(ActionType::ID_Armistice),
      d_armisticeTime(0) {}

    ~ArmisticeReached() {}

#if !defined(EDITOR)
    Boolean run(CampaignLogicOwner* campGame);
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
    void armisticeTime(TimeTick time) { d_armisticeTime = time; }
    TimeTick armisticeTime() const { return d_armisticeTime; }
#endif

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;
};

class NationEntersWar : public ActionType {
    Nationality d_nation;
    Side d_whichSide;

    Boolean d_entersWar;          // if true nation enters, if false it never enters

    static UWORD fileVersion;
  public:
    NationEntersWar() :
      ActionType(ID_NationEntersWar),
      d_nation(NATION_Neutral),
      d_whichSide(SIDE_Neutral),
      d_entersWar(True) {}

    ~NationEntersWar() {}

#if !defined(EDITOR)
    Boolean run(CampaignLogicOwner* campGame);
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
	 void whichNation(Nationality n) { d_nation = n; }
    void whichSide(Side s) { d_whichSide = s; }
    void entersWar(Boolean f) { d_entersWar = f; }

		Nationality whichNation() const { return d_nation; }
    Side whichSide() const { return d_whichSide; }
    Boolean entersWar() const { return d_entersWar; }
#endif

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;
};

class ReinforcementsArrive : public ActionType {
    ICommandPosition d_cpi;

		static UWORD fileVersion;
  public:
    ReinforcementsArrive() :
      ActionType(ActionType::ID_Reinforcements),
      d_cpi(NoCommandPosition) {}

    ~ReinforcementsArrive() {}

#if !defined(EDITOR)
		Boolean run(CampaignLogicOwner* campGame);
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
		virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
    void whichUnit(ICommandPosition cpi) { d_cpi = cpi; }
    ICommandPosition whichUnit() const { return d_cpi; }
#endif

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;
};

class LeaderLeaves : public ActionType {
		ILeader d_iLeader;

    static UWORD fileVersion;
  public:
    LeaderLeaves() :
      ActionType(ActionType::ID_LeaderLeaves),
      d_iLeader(NoLeader) {}

    ~LeaderLeaves() {}

#if !defined(EDITOR)
    Boolean run(CampaignLogicOwner* campGame);
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
    void whichLeader(ILeader iLeader) { d_iLeader = iLeader; }
    ILeader whichLeader() const { return d_iLeader; }
#endif

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;
};

class LeaderEnters : public ActionType {
    ILeader d_iLeader;

    static UWORD fileVersion;
  public:
    LeaderEnters() :
      ActionType(ActionType::ID_LeaderEnters),
      d_iLeader(NoLeader) {}

    ~LeaderEnters() {}

#if !defined(EDITOR)
		Boolean run(CampaignLogicOwner* campGame);
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
    void whichLeader(ILeader iLeader) { d_iLeader = iLeader; }
    ILeader whichLeader() const { return d_iLeader; }
#endif

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
		Boolean write(FileWriter& f, const Armies* army) const;
};

class AllyDefects : public ActionType {
//  Nationality d_nation;

    struct NationItem : public SLink {
      Nationality d_nation;
      NationItem(Nationality n) { d_nation = n; }
    };

    class NationItemList : public SList<NationItem> {
    } d_nations;

    static UWORD fileVersion;
  public:
    AllyDefects() :
      ActionType(ActionType::ID_AllyDefects) {}

    ~AllyDefects() {}

#if !defined(EDITOR)
    Boolean run(CampaignLogicOwner* campGame);
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
		void addNation(Nationality n) { d_nations.append(new NationItem(n)); }

    Nationality whichNation(int index)
    {
      ASSERT(index < d_nations.entries());

      d_nations.first();

      int i = 0;
      do
      {
        if(i == index)
          break;

        i++;
      } while(d_nations.next());

      return d_nations.getCurrent()->d_nation;
    }

    int nationEntries() const { return d_nations.entries(); }
#endif

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;
};

class AdjustResources : public ActionType {
    Nationality d_who;           // for what nation
    SWORD d_whatPercent;         // at what percent (can be + or -)

    static UWORD fileVersion;
  public:
    AdjustResources() :
      ActionType(ActionType::ID_AdjustResources),
      d_who(NATION_Neutral),
      d_whatPercent(0) {}

		~AdjustResources() {}

#if !defined(EDITOR)
    Boolean run(CampaignLogicOwner* campGame);
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
    virtual String describe(CampaignData* campData);
#endif
#ifdef CUSTOMIZE
    bool checkData();
    void addNation(Nationality n) { d_who = n; }
    Nationality whichNation() const { return d_who; }

    void whatPercent(SWORD p) { d_whatPercent = p; }
    SWORD whatPercent() const { return d_whatPercent; }

#endif

    /*
     * file interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;
};

class ActionTypeAllocator {
  public:
    CAMPDATA_DLL static ActionType* newActionType(ActionType::Type type);
};

class ActionTypeList : public SList<ActionType> {
    static UWORD  fileVersion;
  public:
    ActionTypeList() { }
    ~ActionTypeList() {}

#ifdef CUSTOMIZE
    CAMPDATA_DLL bool checkData();
#endif

    /*
     *  File interface
     */

		Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;

};

#ifdef CUSTOMIZE
CAMPDATA_DLL const char* getError();          // for use in checkdata routine
#endif

class Conditions : public SLink {
    ConditionTypeList d_conditions;
    ActionTypeList d_actions;

		enum {
      All          = 0x01,      // do all conditions have to be met
      ConditionMet = 0x02,      // is condition met?
		InstantOnly  = 0x04       // update on InstantUpdate only? i.e. not on dailyUpdate
    };

    UBYTE d_flags;

    void setFlag(UBYTE mask, Boolean f)
    {
      if(f)
        d_flags |= mask;
      else
        d_flags &= ~mask;
    }

#if defined(CUSTOMIZE)
    StringPtr d_conditionName;          // for use in the editor
#endif

    static UWORD fileVersion;

  public:
    Conditions() : d_flags(0) { }
    ~Conditions() {}

#if !defined(EDITOR)
	 void update(CampaignLogicOwner* campGame, ConditionType::Type type, ConditionTypeData* data);
	 void dailyUpdate(CampaignLogicOwner* campGame);
		void doAction(CampaignLogicOwner* campGame);
#endif
#if defined(CUSTOMIZE)
	 CAMPDATA_DLL bool checkData();
#endif

	 Boolean allConditions() const { return (d_flags & All); }
	 void allConditions(Boolean f) { setFlag(All, f); }

    void conditionMet(Boolean f) { setFlag(ConditionMet, f); }
    Boolean conditionMet() const { return (d_flags & ConditionMet) != 0; }

    void instantOnly(Boolean f) { setFlag(InstantOnly, f); }
    Boolean instantOnly() const { return (d_flags & InstantOnly) != 0; }

    ConditionTypeList& getTypes() { return d_conditions; }
		ActionTypeList& getActions() { return d_actions; }
//  void setWinner(Side s);

#if defined(CUSTOMIZE)
    void conditionName(const char* name) { d_conditionName = name; }
    const char* conditionName() const { return d_conditionName; }
#endif

    /*
     *  File interface
     */

    CAMPDATA_DLL Boolean read(FileReader& f, Armies* army);
    CAMPDATA_DLL Boolean write(FileWriter& f, const Armies* army) const;


  private:
    void checkForConditionsMet();
};

class ConditionList : public SList<Conditions> {
    static UWORD fileVersion;
  public:

#ifdef CUSTOMIZE
    bool checkData();
#endif

#if !defined(EDITOR)
    CAMPDATA_DLL void update(CampaignLogicOwner* campGame, ConditionType::Type type, ConditionTypeData* data);
	 CAMPDATA_DLL void dailyUpdate(CampaignLogicOwner* campGame);
#endif

    /*
     *  File interface
     */

    Boolean read(FileReader& f, Armies* army);
    Boolean write(FileWriter& f, const Armies* army) const;

    const char* getChunkName() const;

};

};  // namespace WG_CampaignConditions


#endif
