/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Create a random OB
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "armies.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "realord.hpp"
#include "scenario.hpp"

#ifdef DEBUG
/*
 * Temporary function to create a random OB
 */

LogFile obLog("ob.log");

void Armies::create()
{
   obLog.printf("Create()");

   // For each Side

   create(2);

   topCommand = createCommand();
   CommandPosition* god = getCommand(topCommand);
   god->setRank(Rank_God);
   // god->setSelf(topCommand);

   CommandPosition* pPresident = 0;

   NationIter niter = this;
   while(++niter)
   {
      Side side = niter.current();

      ICommandPosition iPresident = createCommand();
      CommandPosition* president = getCommand(iPresident);
      president->setSide(side);
      president->setRank(Rank_President);
      president->setParent(topCommand);
      president->setNation(scenario->getDefaultNation(side));
      // president->setSelf(iPresident);
      makeUnitName(iPresident, Rank_President);

      if(pPresident)
         pPresident->setSister(iPresident);
      else
         god->setChild(iPresident);

      setPresident(side, iPresident);

      int nArmies = random(3,6);    // Between 3 and 6 armies
      obLog.printf("%d Armies", nArmies);

      CommandPosition* pArmy = 0;

      while(nArmies--)
      {
         ICommandPosition iArmy = createCommand();
         CommandPosition* army = getCommand(iArmy);

         army->setSide(side);
         army->setMorale(Attribute(random(0, Attribute_Range)));
         army->setFatigue(255); //Attribute(random(0, Attribute_Range)));
         army->setSupply(Attribute(random(0, Attribute_Range)));
         army->setRank(Rank_Army);

         // army->setSelf(iArmy);

         army->setParent(iPresident);

         if(pArmy)
            pArmy->setSister(iArmy);
         else
         {
            president->setChild(iArmy);
         }

         /*
          * Set up a position as being in a random town
          */

         const TownList &tl = campaignData->getTowns();
         ITown town;
         const Town* t;

         do {
            town = (ITown) random(0, tl.entries());
            t = &(campaignData->getTown(town));
         } while(t->getSide() != side);

         army->setPosition(town);

#if 0
         CampaignOrder order;
         order.clear();
         // order.type = ORDER_NONE;
         order.type = ORDER_REST_RALLY;
         order.aggressLevel = AGGRESS_DEFEND_SMALL;
         order.destTown = town;
#else
         Order_RestRally order;
         order.setAggressLevel(Orders::Aggression::DefendSmall);
#endif

         army->setOrder(&order);

         makeUnitName(iArmy, Rank_Army);

         int nCorps = random(3, 6);
         obLog.printf("%d Corps", nCorps);

         CommandPosition* pCorps = 0;
         while(nCorps--)
         {
            ICommandPosition iCorps = createCommand();
            CommandPosition* corps = getCommand(iCorps);

            corps->setParent(iArmy);

            // corps->setSelf(iCorps);

            if(pCorps)
               pCorps->setSister(iCorps);
            else
               army->setChild(iCorps);

            corps->setSide(side);
            corps->setMorale(Attribute(random(0, Attribute_Range)));
            corps->setFatigue(255); //Attribute(random(0, Attribute_Range)));
            corps->setSupply(Attribute(random(0, Attribute_Range)));
            corps->setRank(Rank_Corps);

            corps->setPosition(town);
            corps->setOrder(&order);
            makeUnitName(iCorps, Rank_Corps);

            int nDivisions = random(3, 6);
            obLog.printf("%d Divisions", nDivisions);
            CommandPosition* pDivision = 0;
            while(nDivisions--)
            {
               ICommandPosition iDivision = createCommand();
               CommandPosition* division = getCommand(iDivision);

               division->setParent(iCorps);

               // division->setSelf(iDivision);

               if(pDivision)
                  pDivision->setSister(iDivision);
               else
                  corps->setChild(iDivision);

               division->setSide(side);
               division->setMorale(Attribute(random(0, Attribute_Range)));
               division->setFatigue(255); //Attribute(random(0, Attribute_Range)));
               division->setSupply(Attribute(random(0, Attribute_Range)));
               division->setRank(Rank_Division);

               division->setPosition(town);
               division->setOrder(&order);
               makeUnitName(iDivision, Rank_Division);

               int spCount = random(1, 16);     // SP_PER_COMMAND);

               obLog.printf("%d Strength Points", spCount);
               while(spCount--)
               {
                  ISP isp = createStrengthPoint();

                  StrengthPoint* sp = getStrengthPoint(isp);
                  sp->setUnitType(UnitType(random(0, campaignData->getMaxUnitType())));
#if 0    // SPs do not have experience or strength
                  sp->setExperience(Attribute(random(0, Attribute_Range)));
                  sp->setStrength(Attribute(random(0, Attribute_Range)));
#endif


                  attachStrengthPoint(iDivision, isp);
               }

               ILeader g = createLeader(side, scenario->getDefaultNation(side), Rank_Division, True);
               division->setNation(scenario->getDefaultNation(side));
               attachLeader(iDivision, g);

               pDivision = division;
            }

            ILeader g = createLeader(side, scenario->getDefaultNation(side), Rank_Corps, True);
            corps->setNation(scenario->getDefaultNation(side));
            attachLeader(iCorps, g);

            pCorps = corps;
         }

         ILeader g = createLeader(side, scenario->getDefaultNation(side), Rank_Army, True);
         army->setNation(scenario->getDefaultNation(side));
         attachLeader(iArmy, g);

         pArmy = army;
      }

      ICommandPosition iTop = getFirstUnit(side);

      CommandPosition* top = getCommand(iTop);
      ASSERT(top != 0);

      setSupremeLeader(side, top->getLeader());

      pPresident = president;
   }

   initStartingMorale();

   log();
}


#endif



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
