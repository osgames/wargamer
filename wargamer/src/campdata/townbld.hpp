/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOWNBLD_HPP
#define TOWNBLD_HPP

#ifndef __cplusplus
#error townbld.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Things that can be built in towns
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "unittype.hpp"

/*
 * The following are defined in gamedefs.hpp:
 * 	IBuildItem
 * 	IBuildList
 */

/*
 * Priority for building
 */

typedef UBYTE BuildPriority;
#define BuildPriorityMax 100			// 0=no priority, 100=maximum priority
#define BP_Normal 50


typedef UBYTE BuildQuantity;
#define BuildQuantityMax Attribute_Range

typedef UnitType BuildID;

//-------- Watcom C++ 11.0 bug doesn't like typedef's of nested items with -d2 option
// typedef BasicUnitType::value BuildWhat;
#define BuildWhat BasicUnitType::value

class FileReader;
class FileWriter;

class BuildItem {
	enum Flags {
	  AutoBuild = 0x01,
	  Changed   = 0x02,
	  RepoPool  = 0x04
	};


#if 0   // Silly compiler optimiser bug
    /*
     * When it is alligned like this, the compiler
     * doesn't bother setting priority and quantity in the reset() function
     *
     * The new method has better packing anyway, using 8 bytes instead of 10
     *
     * The thing that makes the compiler go wrong is when priority/quantity
     * follow the attributes.  It seems to think that writing a DWORD to
     * manPower clears priority and quantity as well.
     */

	BuildID d_what;						      // What is being built here?
	AttributePoints d_manpowerNeeded;		// How much more is needed
	AttributePoints d_resourceNeeded;
	BuildPriority d_priority;					// Not used as of now. How much to allocate if not enough spare resources
	BuildQuantity d_quantity;              // how many to build
	UBYTE d_flags;
#else

	BuildID d_what;						      // What is being built here?
	UBYTE d_flags;
	BuildPriority d_priority;					// Not used as of now. How much to allocate if not enough spare resources
	BuildQuantity d_quantity;              // how many to build
	AttributePoints d_manpowerNeeded;		// How much more is needed
	AttributePoints d_resourceNeeded;

#endif

	void setFlag(UBYTE mask, Boolean f)
	{
	  if(f)
		 d_flags |= mask;
	  else
		 d_flags &= ~mask;
	}

public:
	BuildItem() :	// Set values to sensible defaults
	  d_what(NoUnitType),
	  d_manpowerNeeded(0),
	  d_resourceNeeded(0),
	  d_priority(0),
	  d_quantity(0),
	  d_flags(0) {}

	BuildItem& operator = (const BuildItem& item)
	{
	  d_what = item.getWhat();
	  d_manpowerNeeded = item.getManpower();
	  d_resourceNeeded = item.getResource();
	  d_priority = item.getPriority();
	  d_quantity = item.getQuantity();
	  d_flags = item.flags();

	  return *this;
	}

	Boolean isEnabled() const { return (d_quantity > 0); }
	BuildID getWhat() const { return d_what; }
	AttributePoints getManpower() const { return d_manpowerNeeded; }
	AttributePoints getResource() const { return d_resourceNeeded; }
	BuildPriority getPriority() const { return d_priority; }
	BuildQuantity getQuantity() const { return d_quantity; }
   UBYTE flags() const { return d_flags; }
	Boolean getAuto()  const { return (d_flags & AutoBuild) != 0; }
	Boolean changed()  const { return (d_flags & Changed) != 0; }
	Boolean repoPool() const { return (d_flags & RepoPool) != 0; }

	CAMPDATA_DLL Boolean reduceNeeded(AttributePoints man, AttributePoints res);

	/*
	 * Set functions
	 */

	void setWhat(BuildID w) { d_what = w; }
	void setPriority(BuildPriority p) { d_priority = p; }
	void setQuantity(int n) { d_quantity = static_cast<BuildQuantity>(minimum<int>(n, BuildQuantityMax)); }
	void toggleAutoBuild() { setAutoBuild(!getAuto()); }
	void setAutoBuild(Boolean b) { setFlag(AutoBuild, b); }
	void setChanged() { d_flags |= Changed; }
	void changed(Boolean f) { setFlag(Changed, f); }
	void repoPool(Boolean f) { setFlag(RepoPool, f); }

	void reset()
	{
	  d_what = NoUnitType;
	  d_manpowerNeeded = 0;
	  d_resourceNeeded = 0;
	  d_priority = 0;
	  d_quantity = 0;
	  d_flags = 0;
	}

	void setResources(AttributePoints man, AttributePoints res)
	{
		d_manpowerNeeded = man;
		d_resourceNeeded = res;
	}

	CAMPDATA_DLL int pack(void * buffer);
	CAMPDATA_DLL int unpack(void * buffer);

	/*
	 * File interface
	 */

	CAMPDATA_DLL Boolean read(FileReader& f);
	CAMPDATA_DLL Boolean write(FileWriter& f) const;

};


#endif /* TOWNBLD_HPP */

