/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef C_SOUNDS_HPP
#define C_SOUNDS_HPP

#include "random.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : C_SOUNDS
        Description : Sound library for campaign game
        
-------------------------------------------------------------------------------------------------------------------*/



typedef struct CampaignSoundTableEntry {

    char * filename;
    // random factor by which frequency is altered each time played
    int frequency_variation;
    // how many buffers are defined for this sound
    int num_buffers;
    // buffers for this sound

        CampaignSoundTableEntry(char * fname, int freq_var, int num_buff) {
            filename = fname;
            frequency_variation = freq_var;
            num_buffers = num_buff;
        }

} CampaignSoundTableEntry;



enum CampaignSoundTypeEnum {

    SOUNDTYPE_MESSAGEBELL,
    SOUNDTYPE_DRUMROLL,
    SOUNDTYPE_MARCHING,
    SOUNDTYPE_HEARBATTLE,
    SOUNDTYPE_TOWNFIRE,
    SOUNDTYPE_MORTAR,
    SOUNDTYPE_BUGLE,
    SOUNDTYPE_CHEERING,

    SOUNDTYPE_MENUPOPUP,
    SOUNDTYPE_MENUSELECT,
    SOUNDTYPE_WINDOWOPEN,
    SOUNDTYPE_WINDOWCLOSE
    
};



extern const CampaignSoundTableEntry * CampaignSoundTable[];




#endif
