/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ROUTE_H
#define ROUTE_H

#ifndef __cplusplus
#error route.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Campaign Route Finder
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  1996/03/01 17:29:50  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/29 18:09:32  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/29 11:02:26  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "measure.hpp"
#include "cpidef.hpp"

/*
 * Forward references
 */

class CampaignPosition;
class CampaignData;
class RouteList;
class CampaignOrder;
class Town;

/*
 * Use of CampaignRouteNode.flags
 */

// #define RN_InQueue 0x0001

typedef ULONG RouteCost;
#define MaxRNcost RouteCost(ULONG_MAX)			// Maximum cost value
// #define ROUTE_NoEnemy MaxRNcost
// #define ROUTE_SameSide (MaxRNcost - 1)

struct CampaignRouteNode {
      friend class CampaignRoute;

   // Public Functions

	   Boolean isConnected() const { return cost != MaxRNcost; }

   // Public Data

	   RouteCost cost;			// Cost of getting to this node
	   Distance distance;		// Distance from destination
	   ITown next;					// Maintained as a linked list!
	   IConnection connection;	// Connection that got us here
	   // UWORD flags;			// Flags
   private:
      bool d_inQueue;         // Set if node is in the queue
};

class CampaignRoute {


   public:
      /*
       * Public Types
       */

	   /*
	    * Description of flags
	    *
	    * The caller can set these directly, or use one of the predefined
	    * setup functions.
	    *
	    *  useCosts:
	    *		When True, the time taken to move along connections and time taken
	    *		to change between transport methods is added up.  This means that
	    *		the fastest route will be found.
	    *
	    *		When False, only the number of connections is totalled up, so the
	    *		route with fewest connections will be found.  This will be faster
	    *		but not neccessarily come up with the best route.  It is intended
	    *		to be used when you just want to know if there is any connection
	    *		such as may be needed for supply lines.
	    *
	    *  findBest:
	    *		When True, an exhaustive search takes place.  This will guarantee
	    *		the absolutely best route (subject to other flags)
	    *
	    *		When False, the search stops as soon as a route is found.  This
	    *		may not be the best route, but will speed up the search and should
	    *		be a fairly good route.
	    *
	    *  travelSide:
	    *		Set to the side of the unit looking for a route.  Works in
	    *		conjunction with enemyDelay.
	    *
	    *  enemyDelay:
	    *		If non zero, then travelling through any enemy location will add
	    *		on a delay.  Units are same as CampaignTime (i.e. GameTicks) if
	    *    useCosts is enabled, or number of connections if useCosts disabled.
	    *
	    *		If set to ROUTE_NoEnemy, then no movement through enemy locations
	    *		is allowed.
	    *
	    *  costTable:
	    *    If NULL then all connections are treated as equal speed.
	    *    Otherwise points to structure detailing speeds and costs.
	    *		Must point to an array of 9 values containing speed moving along:
	    *				Poor Road,  Average Road,  Good Road
	    *				Poor Rail,  Average Rail,  Good Rail
	    *				Poor River, Average River, Good River
	    *		Time taken to change transport type:
	    *				Road_embark Road_Disembark
	    *				Rail_embark Rail_Disembark
	    *				River_embark River_Disembark
	    */

	   enum {
		   UseCosts 	= 0x0001,  	// Set if lengths of connections to be considered
		   FindBest 	= 0x0002,  	// Set for exhaustive search
		   FillTable 	= 0x0004,  	// Get best cost to every node in table
		   DoingSupply = 0x0008,	// Stop looking at supply depot/source
		   SameSide 	= 0x0010,  	// Only allow towns on same side
		   NoEnemy 		= 0x0020,  	// Disallow enemy towns (but allow neutral)
         AllowOffMap = 0x0040    // Allow offMap locations / connections
	   };

	   typedef UBYTE RouteFlag;      //lint -strong(AXJ, RouteFlag)

   public:
      /*
       * Public Functions
       */

	   CAMPDATA_DLL CampaignRoute(const CampaignData* campData);
	   CAMPDATA_DLL ~CampaignRoute();

	   CAMPDATA_DLL void setup(RouteFlag f, Side side, RouteCost delay);

	   const CampaignRouteNode* getNodes() const { return d_nodes; }
	   const CampaignRouteNode& getNode(ITown town) const
	   {
	      ASSERT(d_nodes);
         ASSERT(town < d_nodeCount);
	      return d_nodes[town];
	   }

	   CAMPDATA_DLL IConnection planRoute(ITown from, ITown dest);
	   CAMPDATA_DLL Boolean fillRoute(ITown from);
	   CAMPDATA_DLL Boolean getRouteDistance(const CampaignPosition* pos1, const CampaignPosition* pos2, Distance maxDistance, Distance& dist, Boolean actual = False);
	   CAMPDATA_DLL Boolean planSupply(ITown from);

      // Information functions

      RouteCost cost(ITown town) const
      {
         return getNode(town).cost;
      }

      bool isConnected(ITown town) const
      {
         return getNode(town).isConnected();
      }

   private:
      /*
       * Private Functions
       */

	   void addNode(ITown t, RouteCost cost, IConnection con);
	   ITown getNode();
	   IConnection searchRoute(ITown from, ITown dest);
      bool isTownAllowed(const Town& town) const;

   private:
      /*
       * Private Data
       */

      // Options set with setup()
	   RouteFlag d_flags;        // Various Flags
	   Side d_travelSide;			// Side of traveller (Or SIDE_Neutral if doesn't matter)
	   RouteCost d_enemyDelay;  	// Cost to add for moving through enemy towns

	   const CampaignData* d_campData;

	   CampaignRouteNode* d_nodes;	// One node for each entry in townlist
	   int d_nodeCount;					// Number of Nodes

	   /*
	    * Queue of items
	    */

	   ITown d_usedList;			// Entry point to used towns
	   RouteCost d_bestCost;

 #ifdef DEBUG
	   // int d_iterations;
	   int d_aStarCount;
 #endif

};

class CampaignRouteUtil {
public:
  CAMPDATA_DLL static void plotRoute(const CampaignData* campData, RouteList& list, const CampaignPosition& from, const CampaignPosition& dest, Side startSide);
   // 2 campaign positions and a side
  CAMPDATA_DLL static void plotRoute(const CampaignData* campData, RouteList& list, Side startSide, ITown startTown, ITown destTown, IConnection iCon);
   // town to town for side.
  CAMPDATA_DLL static void plotRoute(const CampaignData* campData, RouteList& list, ITown startTown, ITown endTown, IConnection iCon);
   // town to town, neutral side
  CAMPDATA_DLL static void plotUnitRoute(const CampaignData* campData, const ICommandPosition& cpi);
   // cp with current order
  CAMPDATA_DLL static void plotRoute(const CampaignData* campData, RouteList& list, const CampaignPosition& startPos, const CampaignOrder& order, Side startSide);
   // campaignPosition and order

  CAMPDATA_DLL static	Boolean areTownsAdjacent(const CampaignData* campData, ITown town1, ITown town2);
  CAMPDATA_DLL static Boolean isAdjacent(const CampaignData* campData, CampaignPosition& pos1, CampaignPosition& pos2);
  CAMPDATA_DLL static	IConnection commonConnection(const CampaignData* campData, ITown town1, ITown town2);
};


#if 0    // Not implemented yet, as I realized I don't need it...
/*
 * Handy Iterator to use after calling fillRoute()
 *
 * example usage:
 *    for(RouteNodeIter it(route, destTown); iter(); ++iter)
 *    {
 *       if(iter->isConnected())
 *       {
 *          doSomething(iter->cost);
 *       }
 *    }
 */

class RouteNodeIter
{
   public:
      RouteNodeIter(const CampaignRoute& route, ITown town);
      ~RouteNodeIter() { }

      bool isValid() const;
      const CampaignRouteNode& operator *() const;
      const CampaignRouteNode* operator ->() const;

      RouteNodeIter& operator++();
      RouteNodeIter operator++(int);

   private:
      const CampaignRoute& d_route;
      const CampaignRouteNode* d_currentNode;
};
#endif

#endif /* ROUTE_H */

