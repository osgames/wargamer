/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "terrain.hpp"
#include "filecnk.hpp"
#include "idstr.hpp"
#include "misc.hpp"
#include "resstr.hpp"

#if 0
static Boolean chokePoints[Terrain::GroundType_HowMany][Terrain::RiverType_HowMany] = {
//  NoRiver, MinorRiver, MajorRiver
  { False,   False,      True },     // open
  { False,   False,      True },     // rough
  { False,   False,      True },     // wooded
  { False,   False,      True },     // hilly
  { False,   False,      True },     // wooded-hilly
  { True,    True,       True },     // mountain-pass
  { False,   False,      True }      // marsh
};
#endif

#if 0
// TODO: get from string resource
static const char* groundTypeNames[Terrain::GroundType_HowMany] = {
  "Open",
  "Rough",
  "Wooded",
  "Hilly",
  "Wooded-Hilly",
  "Mountain-Pass",
  "Marsh"
};

static const char* riverTypeNames[Terrain::RiverType_HowMany] = {
  "No River",
  "Minor River",
  "Major River"
};
#endif

const char* Terrain::groundTypeName(Terrain::GroundType type)
{
  ASSERT(type < Terrain::GroundType_HowMany);

//  return groundTypeNames[type];
   return InGameText::get(type + IDS_CAMP_TERRAIN);
}

const char* Terrain::riverTypeName(Terrain::RiverType type)
{
  ASSERT(type < Terrain::RiverType_HowMany);

//  return riverTypeNames[type];
   return InGameText::get(type + IDS_CAMP_RIVER);
}

Boolean Terrain::isChokePoint(Terrain::GroundType groundType, Terrain::RiverType riverType)
{
  ASSERT(groundType < Terrain::GroundType_HowMany);
  ASSERT(riverType < Terrain::RiverType_HowMany);

  return ((groundType == MountainPass) || (riverType == MajorRiver));
#if 0
  return chokePoints[groundType][riverType];
#endif
}

/*
 *  Initialize TerrainTypeItem
 */
#if 0
TerrainTypeItem::TerrainTypeItem()
{
  chokePoint = False;
}

TerrainTypeItem::~TerrainTypeItem()
{
}

#endif


/*
 *  Initializw TerrainTypeTable
 */

TerrainTypeTable::TerrainTypeTable()
{

}

TerrainTypeTable::~TerrainTypeTable()
{

}

#define DEBUG_TERRAINTYPE

static const char terrainTypeChunkName[] = "TerrainTypeTable";

enum {
   TTT_GroundType,
   TTT_RiverType,

   TTT_HowMany
};

MessagePair terrainTypeTokens[] = {
   {TTT_GroundType,    "GroundType" },
   {TTT_RiverType,     "RiverType"  },
   MP_End

};

MessagePair groundTypeNameTokens[] = {
   {Terrain::Open,         "Open"},
   {Terrain::Rough,        "Rough" },
   {Terrain::Wooded,       "Wooded" },
   {Terrain::Hilly,        "Hilly" },
   {Terrain::WoodedHilly,  "WoodedHilly" },
   {Terrain::MountainPass, "MountainPass" },
   {Terrain::Marsh,        "Marsh" },
   MP_End
};

MessagePair riverTypeNameTokens[] = {
   {Terrain::NoRiver, "NoRiver"},
   {Terrain::MinorRiver, "MinorRiver"},
   {Terrain::MajorRiver, "MajorRiver"},
   MP_End
};

Boolean TerrainTypeTable::init(const char* fileName)
{
#ifdef DEBUG_TERRAINTYPE
   debugLog("+Starting to Read TerrainType Table %s\n", fileName);
#endif

   try
   {
      WinFileAsciiChunkReader f(fileName);
      readData(f);
   }
   catch(const FileError& f)
   {
      throw GeneralError("Can not read TerrainTypeTable %s", fileName);
      // return False;
   }

#ifdef DEBUG_TERRAINTYPE
   debugLog("-Finished Reading TerrainTypeTable %s\n", fileName);
#endif

   return True;
}

Boolean TerrainTypeTable::readData(WinFileAsciiChunkReader& f)
{
   ASSERT(f.isAscii());

   FileChunkReader fr(f, terrainTypeChunkName);
   if(!fr.isOK())
      throw FileError();

#ifdef DEBUG_TERRAINTYPE
   debugLog("Found %s chunk\n", terrainTypeChunkName);
#endif

   int count = fr.countEntries();
   if(!fr.isOK())
      throw FileError();

#ifdef DEBUG_TERRAINTYPE
   debugLog("%d entries\n", count);
#endif

   if(count > 0)
   {
      ASSERT(count < ArrayIndex_MAX);

      items.init((ArrayIndex) count);

      for(ArrayIndex i = 0; i < count; i++)
         items[i].readData(fr);
   }

   return fr.isOK();
}

Boolean TerrainTypeItem::readData(FileChunkReader& f)
{
#ifdef DEBUG_TERRAINTYPE
   debugLog("+TerrainTypeItem::readData()\n");
#endif

#ifdef DEBUG
   static int lineCount = 0;
#endif

   if(f.startItem())
   {
      int token;
      StringParse parameters;

      while(f.getItem(token, parameters, terrainTypeTokens))
      {
#ifdef DEBUG_TERRAINTYPE
         const char* parameterString = (const char*) parameters;
         const char* tokenName = "Unknown";
         mpGetName(terrainTypeTokens, tokenName, token);

         debugLog("token=%d (%s), parameters=%s\n",
            token,
            tokenName,
            (const char*) parameterString);
#endif
         switch(token)
         {
            case TTT_GroundType:
            {
               const char* tokenName = parameters.getToken();
               if(tokenName)
               {
                  int num;

                  if(mpGetID(groundTypeNameTokens, tokenName, num))
                  {
                     ASSERT(num < Terrain::GroundType_HowMany);

                     d_groundType = static_cast<Terrain::GroundType>(num);
                  }
                  else
                  {
                    goto badFormat;
                  }
               }
               else
                  goto badFormat;

               break;
            }


            case TTT_RiverType:
            {
               const char* tokenName = parameters.getToken();
               if(tokenName)
               {
                  int num;

                  if(mpGetID(riverTypeNameTokens, tokenName, num))
                  {
                     ASSERT(num < Terrain::RiverType_HowMany);

                     d_riverType = static_cast<Terrain::RiverType>(num);
                  }
                  else
                  {
                    goto badFormat;
                  }
               }
               else
                  goto badFormat;

               break;
            }



         badFormat:
            {
#ifdef DEBUG
               const char* tokenName = "Unknown";
               mpGetName(terrainTypeTokens, tokenName, token);

               debugMessage("Bad format in TerrainType Table\r\nLine %d,\r\ntoken=%d (%s)\r\nparameters=%s",
                  lineCount,
                  (int) token,
                  (const char*) tokenName,
                  (const char*) parameterString);
#else
               throw GeneralError("Error in Terrain Type Table");
#endif
            }
            break;

         default:
            throw GeneralError("Illegal Token (%d) in TerrainTypeItem::readData()", token);


         }
      }

      /*
       * Set description
       */

      char buf[200];

      lstrcpy(buf, Terrain::groundTypeName(d_groundType));

      if(d_riverType != Terrain::NoRiver)
      {
        lstrcat(buf, " with ");
        lstrcat(buf, Terrain::riverTypeName(d_riverType));
      }

      d_description = buf;
   }
   else
      throw GeneralError("Missing '{' in TerrainType Data");

#ifdef DEBUG_TERRAINTYPE
   debugLog("-TerrainTypeItem::readData()\n");
#endif

   return f.isOK();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
