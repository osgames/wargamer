/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef WEATHER_HPP
#define WEATHER_HPP

#include "cdatadll.h"
#include "mytypes.h"

#ifdef DEBUG
#include "clog.hpp"
#endif

class FileReader;
class FileWriter;

class CampaignWeather {
  public:
    enum Weather {
      Sunny,
      LightRain,
      HeavyRain,
      Weather_HowMany
    };

    enum GroundConditions {
      Dry,
      Muddy,
      VeryMuddy,
      GroundConditions_HowMany
    };

  private:
    Weather d_weather;
    GroundConditions d_groundConditions;
    UBYTE d_conditionCounter;
    static UWORD fileVersion;
  public:
    CAMPDATA_DLL CampaignWeather();
    CAMPDATA_DLL ~CampaignWeather() {}

    CAMPDATA_DLL void setWeather(Weather w); // { d_weather = w; }
    Weather getWeather() const { return d_weather; }

    GroundConditions getGroundConditions() const { return d_groundConditions; }

    CAMPDATA_DLL static const char* getWeatherText(Weather w);
     CAMPDATA_DLL static const char* getConditionsText(GroundConditions c);

    /*
     * file interface
     */

    CAMPDATA_DLL Boolean read(FileReader& f);
    CAMPDATA_DLL Boolean write(FileWriter& f) const;

     CAMPDATA_DLL const char* getChunkName() const;

  private:
    void adjustCurrentConditions(int by);

};

#ifdef DEBUG
CAMPDATA_DLL extern LogFileFlush weatherLog;
#endif

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */

