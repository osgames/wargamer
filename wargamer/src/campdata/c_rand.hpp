/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef C_RAND_HPP
#define C_RAND_HPP

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "myassert.hpp"
#include "measure.hpp"

/*
 * Random events that can happen to a side
 */

class FileReader;
class FileWriter;

struct SideRandomEvent {
  static UWORD fileVersion;

  TimeTick d_shqInChaos;
  TimeTick d_enemyPlansCaptured;

  SideRandomEvent() : d_shqInChaos(0), d_enemyPlansCaptured(0) {}

  /*
	* File Interface
	*/

  Boolean read(FileReader& f, TimeTick theTime);
  Boolean write(FileWriter& f, TimeTick theTime) const;
};

class CampaignSideRandomEvents {
	 enum { CampaignSides = 2 };
	 static UWORD fileVersion;

	 SideRandomEvent d_events[CampaignSides];   // one per side
  public:
	 CampaignSideRandomEvents() {}
	 ~CampaignSideRandomEvents() {}

	 void setSHQInChaos(Side side, TimeTick tilWhen)
	 {
		ASSERT(side < CampaignSides);
		d_events[side].d_shqInChaos = tilWhen;
	 }

	 Boolean isSHQInChaos(Side side, TimeTick theTime) const
	 {
		ASSERT(side < CampaignSides);
		return theTime < d_events[side].d_shqInChaos;
	 }

	 void setEnemyPlansCaptured(Side side, TimeTick tilWhen)
	 {
		ASSERT(side < CampaignSides);
		d_events[side].d_enemyPlansCaptured = tilWhen;
	 }

	 Boolean isEnemyPlansCaptured(Side side, TimeTick theTime) const
	 {
		ASSERT(side < CampaignSides);
		return theTime < d_events[side].d_enemyPlansCaptured;
	 }

	 /*
	  * File Interface
	  */

	 Boolean read(FileReader& f, TimeTick theTime);
	 Boolean write(FileWriter& f, TimeTick theTime) const;

    const char* getChunkName() const;
};








#endif
