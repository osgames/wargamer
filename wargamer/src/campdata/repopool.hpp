/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef REPOPOOL_HPP
#define REPOPOOL_HPP

#include "sllist.hpp"
#include "gamedefs.hpp"
#include "cpidef.hpp"
#include "measure.hpp"
#include "refptr.hpp"
#include "cdatadll.h"
#include "c_obutil.hpp"

class FileReader;
class FileWriter;
class OrderBattle;

/*
 * Independent and Captured Leaders
 */


/*-----------------------------------------------------------
 * Independent leaders and lists
 *
 * Reference counted so that if user interface
 * has an SPWindow up it will not point to
 * deleted objects.
 */

class CAMPDATA_DLL RepoSP : public SLink, public RefBaseCount {

public:

	 ISP d_sp;                             // who
	 ITown d_town;                         // current location (if not traveling or at SHQ)
	 ICommandPosition d_attachTo;          // attach to this unit
	 TimeTick d_travelTime;                // travel time
    bool d_active;


  public:
	 RepoSP() :
		d_sp(NoStrengthPoint),
		d_town(NoTown),
		d_attachTo(NoCommandPosition),
		d_travelTime(0),
		d_active(true) {}

	 RepoSP(const ISP& sp) :
		d_sp(sp),
		d_town(0),
		d_attachTo(NoCommandPosition),
		d_travelTime(0),
		d_active(true) {}

	 ~RepoSP() {}

	 void town(ITown t)                  { d_town = t; }
	 void attachTo(ParamCP cpi) { d_attachTo = cpi; }
	 void travelTime(TimeTick tick)      { d_travelTime = tick; }
    void setCompleted() { d_active = false; }
    bool isCompleted() const { return !d_active; }

	 ISP sp()                    const { return d_sp; }
	 ITown town()                const { return d_town; }
	 ICommandPosition attachTo() const { return d_attachTo; }
	 TimeTick travelTime()       const { return d_travelTime; }


	 /*
	  * File Interface
	  */

	 Boolean read(FileReader& f, OrderBattle& ob);
	 Boolean write(FileWriter& f, OrderBattle& ob) const;

	 /*
	  * Allocators
	  */

	 static void* operator new(size_t size);
#ifdef _MSC_VER
	 static void operator delete(void* deadObject);
#else
	 static void operator delete(void* deadObject, size_t size);
#endif
	 enum { ChunkSize = 10 };
};

class RepoSPList : public SList<RepoSP> {
public:
	 RepoSPList() {}
	 ~RepoSPList() { reset(); }

	 /*
	  * File Interface
	  */

	 Boolean read(FileReader& f, OrderBattle& ob);
	 Boolean write(FileWriter& f, OrderBattle& ob) const;
};

#endif
