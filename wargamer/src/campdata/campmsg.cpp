/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Campaign Messages
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campmsg.hpp"
#include "resdef.h"
#include "wmisc.hpp"

#ifdef DEBUG
#include "todolog.hpp"
#endif

namespace CampaignMessages
{

static struct {
        MessageGroup group;                             // Which group of messages does it belong?
        int resID;
        int descriptionID;
        CampaignSoundTypeEnum sound;
        const char* text;                                       // For messages without an ID
} messageData[CMSG_HowMany] = {
        {       Group0, IDS_CMSG_ArrivedAtTown, IDS_CMSG_DESCRIPTION + IDS_CMSG_ArrivedAtTown, SOUNDTYPE_MESSAGEBELL},
        {       Group0, IDS_CMSG_NewUnitBuilt, IDS_CMSG_DESCRIPTION + IDS_CMSG_NewUnitBuilt, SOUNDTYPE_BUGLE},
        {       Group0, IDS_CMSG_MetAtAndAwait, IDS_CMSG_DESCRIPTION + IDS_CMSG_MetAtAndAwait, SOUNDTYPE_MESSAGEBELL},
        {       Group0, IDS_CMSG_TakenCommand, IDS_CMSG_DESCRIPTION + IDS_CMSG_TakenCommand, SOUNDTYPE_DRUMROLL},
        {       Group0, IDS_CMSG_MetAndAwait, IDS_CMSG_DESCRIPTION + IDS_CMSG_MetAndAwait, SOUNDTYPE_MESSAGEBELL},
        {       Group0, IDS_CMSG_Stranded, IDS_CMSG_DESCRIPTION + IDS_CMSG_Stranded, SOUNDTYPE_MESSAGEBELL},
        {       Group0, IDS_CMSG_MovingInForBattle, IDS_CMSG_DESCRIPTION + IDS_CMSG_MovingInForBattle, SOUNDTYPE_MARCHING},
        {       Group0, IDS_CMSG_HoldingBackAt8, IDS_CMSG_DESCRIPTION + IDS_CMSG_HoldingBackAt8, SOUNDTYPE_DRUMROLL},
        {       Group0, IDS_CMSG_AvoidingContact, IDS_CMSG_DESCRIPTION + IDS_CMSG_AvoidingContact, SOUNDTYPE_DRUMROLL},
        {       Group0, IDS_CMSG_PreparingForBattle, IDS_CMSG_DESCRIPTION + IDS_CMSG_PreparingForBattle, SOUNDTYPE_MARCHING},
        {       Group0, IDS_CMSG_JoiningBattle, IDS_CMSG_DESCRIPTION + IDS_CMSG_JoiningBattle, SOUNDTYPE_MARCHING},
        {       Group0, IDS_CMSG_EngagingInBattle, IDS_CMSG_DESCRIPTION + IDS_CMSG_EngagingInBattle, SOUNDTYPE_MARCHING},
        {  Group0, IDS_CMSG_GeneralKilled, IDS_CMSG_DESCRIPTION + IDS_CMSG_GeneralKilled, SOUNDTYPE_DRUMROLL},
        {  Group0, IDS_CMSG_UnitDestroyed, IDS_CMSG_DESCRIPTION + IDS_CMSG_UnitDestroyed, SOUNDTYPE_DRUMROLL},
        {  Group0, IDS_CMSG_CannotCommand, IDS_CMSG_DESCRIPTION + IDS_CMSG_CannotCommand, SOUNDTYPE_MESSAGEBELL},
        {  Group1, IDS_CMSG_NotNoble, IDS_CMSG_DESCRIPTION + IDS_CMSG_NotNoble, SOUNDTYPE_MESSAGEBELL},
        {  Group1, IDS_CMSG_TownTaken, IDS_CMSG_DESCRIPTION + IDS_CMSG_TownTaken, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_TownLost, IDS_CMSG_DESCRIPTION + IDS_CMSG_TownLost, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_HearBattle, IDS_CMSG_DESCRIPTION + IDS_CMSG_HearBattle, SOUNDTYPE_HEARBATTLE},
        {  Group1, IDS_CMSG_EncounteredEnemy, IDS_CMSG_DESCRIPTION + IDS_CMSG_EncounteredEnemy, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_ChangedAggression, IDS_CMSG_DESCRIPTION + IDS_CMSG_ChangedAggression, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_MortarShellExploded, IDS_CMSG_DESCRIPTION + IDS_CMSG_MortarShellExploded, SOUNDTYPE_MORTAR},
        {  Group1, IDS_CMSG_PoorConstruction, IDS_CMSG_DESCRIPTION + IDS_CMSG_PoorConstruction, SOUNDTYPE_MESSAGEBELL},
        {  Group1, IDS_CMSG_BreachedFort, IDS_CMSG_DESCRIPTION + IDS_CMSG_BreachedFort, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_FortBreached, IDS_CMSG_DESCRIPTION + IDS_CMSG_FortBreached, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_TownSurrender, IDS_CMSG_DESCRIPTION + IDS_CMSG_TownSurrender, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_FortDestroyed, IDS_CMSG_DESCRIPTION + IDS_CMSG_FortDestroyed, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_BrokenSiege, IDS_CMSG_DESCRIPTION + IDS_CMSG_BrokenSiege, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_SiegeEnded, IDS_CMSG_DESCRIPTION + IDS_CMSG_SiegeEnded, SOUNDTYPE_DRUMROLL},
        {  Group1, IDS_CMSG_StartSiege, IDS_CMSG_DESCRIPTION + IDS_CMSG_StartSiege, SOUNDTYPE_DRUMROLL},
        {  Group2, IDS_CMSG_SiegeStarted, IDS_CMSG_DESCRIPTION + IDS_CMSG_SiegeStarted, SOUNDTYPE_DRUMROLL},
        {  Group2, IDS_CMSG_AttemptedSiege, IDS_CMSG_DESCRIPTION + IDS_CMSG_AttemptedSiege, SOUNDTYPE_DRUMROLL},
        {  Group2, IDS_CMSG_SiegeAttempted, IDS_CMSG_DESCRIPTION + IDS_CMSG_SiegeAttempted, SOUNDTYPE_DRUMROLL},
        {  Group2, IDS_CMSG_RaidingTown, IDS_CMSG_DESCRIPTION + IDS_CMSG_RaidingTown, SOUNDTYPE_DRUMROLL},
        {  Group2, IDS_CMSG_TownRaided, IDS_CMSG_DESCRIPTION + IDS_CMSG_TownRaided, SOUNDTYPE_DRUMROLL},
        {  Group2, IDS_CMSG_NotInstallSupply, IDS_CMSG_DESCRIPTION + IDS_CMSG_NotInstallSupply, SOUNDTYPE_MESSAGEBELL},
        {  Group2, IDS_CMSG_NotInstallSupplyDead, IDS_CMSG_DESCRIPTION + IDS_CMSG_NotInstallSupplyDead, SOUNDTYPE_MESSAGEBELL},
        {  Group2, IDS_CMSG_SupplyDepotInstalled, IDS_CMSG_DESCRIPTION + IDS_CMSG_SupplyDepotInstalled, SOUNDTYPE_BUGLE},
        {  Group2, IDS_CMSG_NotInstallFort, IDS_CMSG_DESCRIPTION + IDS_CMSG_NotInstallFort, SOUNDTYPE_MESSAGEBELL},
        {  Group2, IDS_CMSG_NotInstallFortDead, IDS_CMSG_DESCRIPTION + IDS_CMSG_NotInstallFortDead, SOUNDTYPE_MESSAGEBELL},
        {  Group2, IDS_CMSG_FortUpgraded, IDS_CMSG_DESCRIPTION + IDS_CMSG_FortUpgraded, SOUNDTYPE_BUGLE},
        {  Group2, IDS_CMSG_EngineerNotFieldWork, IDS_CMSG_DESCRIPTION + IDS_CMSG_EngineerNotFieldWork, SOUNDTYPE_MESSAGEBELL},
        {  Group2, IDS_CMSG_FieldWorkUpgraded, IDS_CMSG_DESCRIPTION + IDS_CMSG_FieldWorkUpgraded, SOUNDTYPE_BUGLE},
        {  Group2, IDS_CMSG_FieldWorkComplete, IDS_CMSG_DESCRIPTION + IDS_CMSG_FieldWorkComplete, SOUNDTYPE_BUGLE},

        {  Group3, IDS_CMSG_BridgeCollapse, IDS_CMSG_DESCRIPTION + IDS_CMSG_BridgeCollapse, SOUNDTYPE_DRUMROLL},
        {  Group3, IDS_CMSG_TownFire, IDS_CMSG_DESCRIPTION + IDS_CMSG_TownFire, SOUNDTYPE_TOWNFIRE},
        {  Group3, IDS_CMSG_LeaderSick, IDS_CMSG_DESCRIPTION + IDS_CMSG_LeaderSick, SOUNDTYPE_DRUMROLL},
        {  Group3, IDS_CMSG_LeaderRecovered, IDS_CMSG_DESCRIPTION + IDS_CMSG_LeaderRecovered, SOUNDTYPE_DRUMROLL},
        {  Group3, IDS_CMSG_AlliedDisillusion, IDS_CMSG_DESCRIPTION + IDS_CMSG_AlliedDisillusion, SOUNDTYPE_DRUMROLL},
        {  Group3, IDS_CMSG_SHQInChaos, IDS_CMSG_DESCRIPTION + IDS_CMSG_SHQInChaos, SOUNDTYPE_DRUMROLL},
        {  Group3, IDS_CMSG_EnemyPlansCaptured, IDS_CMSG_DESCRIPTION + IDS_CMSG_EnemyPlansCaptured, SOUNDTYPE_DRUMROLL},
        {  Group3, IDS_CMSG_SquabblingLeaders, IDS_CMSG_DESCRIPTION + IDS_CMSG_SquabblingLeaders, SOUNDTYPE_DRUMROLL},
        {  Group3, IDS_CMSG_PopularEnthusiasm, IDS_CMSG_DESCRIPTION + IDS_CMSG_PopularEnthusiasm, SOUNDTYPE_CHEERING},
        {  Group3, IDS_CMSG_ArmisticeBegun, IDS_CMSG_DESCRIPTION + IDS_CMSG_ArmisticeBegun, SOUNDTYPE_CHEERING },
        {  Group3, IDS_CMSG_ArmisticeEnded, IDS_CMSG_DESCRIPTION + IDS_CMSG_ArmisticeEnded, SOUNDTYPE_CHEERING },

};

static UINT groupNames[Group_HowMany] = {
        IDS_MSG_GROUP0,
        IDS_MSG_GROUP1,
        IDS_MSG_GROUP2,
        IDS_MSG_GROUP3
};


int getResID(CMSG_ID id)
{
        ASSERT(id < CMSG_HowMany);
        return messageData[id].resID;
}

int getDescriptionID(CMSG_ID id)
{
        ASSERT(id < CMSG_HowMany);
        return messageData[id].descriptionID;
}

const char* getText(CMSG_ID id)
{
#ifdef DEBUG
        ToDo("Put all campaign Messages in resource file");
#endif

        ASSERT(id < CMSG_HowMany);
        return messageData[id].text;
}

MessageGroup getGroup(CMSG_ID id)
{
        ASSERT(id < CMSG_HowMany);
        return messageData[id].group;
}

int getGroupID(MessageGroup group)
{
        ASSERT(group < Group_HowMany);
        return groupNames[group];
}


void getDescription(SimpleString& s, CMSG_ID id)
{
        idsToString(s, getDescriptionID(id));
}

void getGroupName(SimpleString& s, MessageGroup group)
{
        idsToString(s, getGroupID(group));
}


CampaignSoundTypeEnum getSound(CMSG_ID id)
{
        ASSERT(id < CMSG_HowMany);
        return messageData[id].sound;
}


};      // namespace CampaignMessages

/*==================================================================
 * CampaignMessageInfo Implementation
 */

CampaignMessageInfo::CampaignMessageInfo(Side s, CMSG_ID id)
{
        d_id = id;
        d_side = s;
        d_cp = NoCommandPosition;
        d_cpTarget = NoCommandPosition;
        d_where = NoTown;
        d_what = NoUnitType;
        d_leader = NoLeader;
        d_s1 = 0;
        d_s2 = 0;
        d_value1 = 0;
        d_sound = CampaignMessages::getSound(id);
}

/*
 * default Constuctor
 */

CampaignMessageInfo::CampaignMessageInfo() :
        d_id(CampaignMessages::NoMessage),
        d_side(SIDE_Neutral),
        d_cp(NoCommandPosition),
        d_cpTarget(NoCommandPosition),
        d_where(NoTown),
        d_what(NoUnitType),
        d_leader(NoLeader),
        d_s1(0),
        d_s2(0),
        d_value1(0),
        d_sound(SOUNDTYPE_MESSAGEBELL)
{
}





/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
