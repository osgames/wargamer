# Microsoft Developer Studio Project File - Name="campdata" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=campdata - Win32 Editor Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "campdata.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campdata.mak" CFG="campdata - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campdata - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campdata - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campdata - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campdata - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campdata - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CAMPDATA_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 User32.lib /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CAMPDATA_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 User32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campdataDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "campdata___Win32_Editor_Debug"
# PROP BASE Intermediate_Dir "campdata___Win32_Editor_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 User32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campdataDB.dll" /pdbtype:sept
# ADD LINK32 User32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campdataEDDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "campdata___Win32_Editor_Release"
# PROP BASE Intermediate_Dir "campdata___Win32_Editor_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /YX"stdinc.hpp" /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_CAMPDATA_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 User32.lib /nologo /dll /debug /machine:I386
# ADD LINK32 User32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campdataED.dll"

!ENDIF 

# Begin Target

# Name "campdata - Win32 Release"
# Name "campdata - Win32 Debug"
# Name "campdata - Win32 Editor Debug"
# Name "campdata - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\armies.cpp
# End Source File
# Begin Source File

SOURCE=.\armistic.cpp
# End Source File
# Begin Source File

SOURCE=.\armyutil.cpp
# End Source File
# Begin Source File

SOURCE=.\c_cond.cpp
# End Source File
# Begin Source File

SOURCE=.\c_obutil.cpp
# End Source File
# Begin Source File

SOURCE=.\c_rand.cpp
# End Source File
# Begin Source File

SOURCE=.\c_sounds.cpp
# End Source File
# Begin Source File

SOURCE=.\camp_snd.cpp
# End Source File
# Begin Source File

SOURCE=.\campdimp.cpp
# End Source File
# Begin Source File

SOURCE=.\campdint.cpp
# End Source File
# Begin Source File

SOURCE=.\campevt.cpp
# End Source File
# Begin Source File

SOURCE=.\campinfo.cpp
# End Source File
# Begin Source File

SOURCE=.\campint.cpp
# End Source File
# Begin Source File

SOURCE=.\campmsg.cpp
# End Source File
# Begin Source File

SOURCE=.\campord.cpp
# End Source File
# Begin Source File

SOURCE=.\camppos.cpp
# End Source File
# Begin Source File

SOURCE=.\campside.cpp
# End Source File
# Begin Source File

SOURCE=.\camptime.cpp
# End Source File
# Begin Source File

SOURCE=.\campunit.cpp
# End Source File
# Begin Source File

SOURCE=.\cbattle.cpp

!IF  "$(CFG)" == "campdata - Win32 Release"

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\compos.cpp
# End Source File
# Begin Source File

SOURCE=.\ctimdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cu_mode.cpp
# End Source File
# Begin Source File

SOURCE=.\cwin_int.cpp
# End Source File
# Begin Source File

SOURCE=.\gameob.cpp
# End Source File
# Begin Source File

SOURCE=.\ileader.cpp
# End Source File
# Begin Source File

SOURCE=.\makearmy.cpp

!IF  "$(CFG)" == "campdata - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\measure.cpp
# End Source File
# Begin Source File

SOURCE=.\morale.cpp
# End Source File
# Begin Source File

SOURCE=.\randevnt.cpp
# End Source File
# Begin Source File

SOURCE=.\repopool.cpp
# End Source File
# Begin Source File

SOURCE=.\route.cpp

!IF  "$(CFG)" == "campdata - Win32 Release"

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\routelst.cpp
# End Source File
# Begin Source File

SOURCE=.\supplyline.cpp
# End Source File
# Begin Source File

SOURCE=.\terrain.cpp
# End Source File
# Begin Source File

SOURCE=.\town.cpp
# End Source File
# Begin Source File

SOURCE=.\townbld.cpp
# End Source File
# Begin Source File

SOURCE=.\townordr.cpp
# End Source File
# Begin Source File

SOURCE=.\unitlist.cpp
# End Source File
# Begin Source File

SOURCE=.\unitlog.cpp

!IF  "$(CFG)" == "campdata - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "campdata - Win32 Debug"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "campdata - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\weather.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\armies.hpp
# End Source File
# Begin Source File

SOURCE=.\armistic.hpp
# End Source File
# Begin Source File

SOURCE=.\armybase.hpp
# End Source File
# Begin Source File

SOURCE=.\armyutil.hpp
# End Source File
# Begin Source File

SOURCE=.\bfiledef.hpp
# End Source File
# Begin Source File

SOURCE=.\c_cond.hpp
# End Source File
# Begin Source File

SOURCE=.\c_obutil.hpp
# End Source File
# Begin Source File

SOURCE=.\c_rand.hpp
# End Source File
# Begin Source File

SOURCE=.\c_sounds.hpp
# End Source File
# Begin Source File

SOURCE=.\camp_snd.hpp
# End Source File
# Begin Source File

SOURCE=.\campctrl.hpp
# End Source File
# Begin Source File

SOURCE=.\campdimp.hpp
# End Source File
# Begin Source File

SOURCE=.\campdint.hpp
# End Source File
# Begin Source File

SOURCE=.\campevt.hpp
# End Source File
# Begin Source File

SOURCE=.\campinfo.hpp
# End Source File
# Begin Source File

SOURCE=.\campint.hpp
# End Source File
# Begin Source File

SOURCE=.\campmsg.hpp
# End Source File
# Begin Source File

SOURCE=.\campord.hpp
# End Source File
# Begin Source File

SOURCE=.\camppos.hpp
# End Source File
# Begin Source File

SOURCE=.\campside.hpp
# End Source File
# Begin Source File

SOURCE=.\camptime.hpp
# End Source File
# Begin Source File

SOURCE=.\campunit.hpp
# End Source File
# Begin Source File

SOURCE=.\cbatmode.hpp
# End Source File
# Begin Source File

SOURCE=.\cbattle.hpp
# End Source File
# Begin Source File

SOURCE=.\cdatadll.h
# End Source File
# Begin Source File

SOURCE=.\cleader.hpp
# End Source File
# Begin Source File

SOURCE=.\compos.hpp
# End Source File
# Begin Source File

SOURCE=.\cpdef.hpp
# End Source File
# Begin Source File

SOURCE=.\cpidef.hpp
# End Source File
# Begin Source File

SOURCE=.\ctimdata.hpp
# End Source File
# Begin Source File

SOURCE=.\cu_mode.hpp
# End Source File
# Begin Source File

SOURCE=.\cwin_int.hpp
# End Source File
# Begin Source File

SOURCE=.\gameob.hpp
# End Source File
# Begin Source File

SOURCE=.\ileader.hpp
# End Source File
# Begin Source File

SOURCE=.\imapwindow.hpp
# End Source File
# Begin Source File

SOURCE=.\loss_def.hpp
# End Source File
# Begin Source File

SOURCE=.\measure.hpp
# End Source File
# Begin Source File

SOURCE=.\morale.hpp
# End Source File
# Begin Source File

SOURCE=.\randevnt.hpp
# End Source File
# Begin Source File

SOURCE=.\repopool.hpp
# End Source File
# Begin Source File

SOURCE=.\route.hpp
# End Source File
# Begin Source File

SOURCE=.\routelst.hpp
# End Source File
# Begin Source File

SOURCE=.\siegeint.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=.\supplyline.hpp
# End Source File
# Begin Source File

SOURCE=.\terrain.hpp
# End Source File
# Begin Source File

SOURCE=.\town.hpp
# End Source File
# Begin Source File

SOURCE=.\townbld.hpp
# End Source File
# Begin Source File

SOURCE=.\townordr.hpp
# End Source File
# Begin Source File

SOURCE=.\unitlist.hpp
# End Source File
# Begin Source File

SOURCE=.\unitlog.hpp
# End Source File
# Begin Source File

SOURCE=.\victint.hpp
# End Source File
# Begin Source File

SOURCE=.\weather.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
