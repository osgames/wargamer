/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "c_cond.hpp"
#include "campdint.hpp"
#include "compos.hpp"
#include "myassert.hpp"
#include "town.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"
#include "savegame.hpp"
#include "campside.hpp"
#include "wg_msg.hpp"
#include "armies.hpp"
#include "victint.hpp"
#include "wg_rand.hpp"
#include "armyutil.hpp"
#include "nations.hpp"
#include "route.hpp"
#include "victory.hpp"
#include "campint.hpp"
//#include "defect.hpp"
#include "campctrl.hpp"
#include "scenario.hpp"
#ifdef DEBUG
#include "clog.hpp"

static LogFileFlush cLog("Condition.log");
#endif

namespace WG_CampaignConditions
{

static StringPtr s_errorText;
#ifdef CUSTOMIZE
const char* getError()
{
   return s_errorText.toStr();
}
#endif

//============  Implementation of  TownChangesSide ===============

UWORD TownChangesSide::fileVersion = 0x0001;
#ifdef CUSTOMIZE
bool TownChangesSide::checkData()
{
   if(d_side != SIDE_Neutral && d_town != NoTown)
      return True;

   s_errorText = "Improper Value in the Condition -- Town Changes Side";
   return False;
}

#endif

#if defined(CUSTOMIZE) || defined(DEBUG)

String TownChangesSide::describe(CampaignData* campData)
{
    String result = "Town (";
    result += campData->getTownName(d_town);
    result += ") changes Side to ";
    result += scenario->getSideName(d_side);
    return result;
}


#endif

#if !defined(EDITOR)
void TownChangesSide::instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data)
{
  ASSERT(d_side != SIDE_Neutral);
  ASSERT(d_town != NoTown);
  ASSERT(campGame != 0);
  ASSERT(data->d_town != NoTown);

  CampaignData* campData = campGame->campaignData();
#ifdef DEBUG
  Town& t1 = campData->getTown(data->d_town);
  Town& t2 = campData->getTown(d_town);
  cLog.printf("%s changed sides to %s. Checking for %s changing to %s",
     t1.getName(),
     scenario->getSideName(t1.getSide()),
     t2.getName(),
     scenario->getSideName(d_side));

#endif

  if(!d_conditionMet)
  {
    if(data->d_town == d_town)
    {
      Town& t = campData->getTown(d_town);

      d_conditionMet = static_cast<Boolean>((t.getSide() == d_side));
    }
  }
}
#endif

Boolean TownChangesSide::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // Todo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);
      f.getUByte(d_side);
      f.getUWord(d_town);

      if(f.getMode() == SaveGame::SaveGame)
      {
         UBYTE b;
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
      else
      {
         if(version < 0x0001)
         {
            UBYTE rubbish;
            f.getUByte(rubbish);
         }

         d_conditionMet = False;
      }
   }

  return f.isOK();
}

Boolean TownChangesSide::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // Todo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUByte(d_side);
      f.putUWord(d_town);

      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }
   }

  return f.isOK();
}

//=========== implementation of CasualtyLevelReached ==============

UWORD CasualtyLevelReached::fileVersion = 0x0003;
#ifdef CUSTOMIZE
bool CasualtyLevelReached::checkData()
{
   return True;
}
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)

String CasualtyLevelReached::describe(CampaignData* campData)
{
    char buffer[20];

    String result = "CasualtyLevelreached (";
    wsprintf(buffer, "%ld", (long)d_side0Casualties);
    result += buffer;
    result += ", ";
    wsprintf(buffer, "%ld", (long)d_side1Casualties);
    result += buffer;
    return result;
}

#endif

#if !defined(EDITOR)
void CasualtyLevelReached::dailyUpdate(CampaignLogicOwner* campGame)
{
  ASSERT(!d_conditionMet);
  ASSERT(campGame != 0);

  CampaignData* campData = campGame->campaignData();
  SPCount side0Loss = campData->getSideData().getSideLosses(0);
  SPCount side1Loss = campData->getSideData().getSideLosses(1);

  d_conditionMet = static_cast<Boolean>( ((side0Loss >= d_side0Casualties) &&
                                          (side1Loss >= d_side1Casualties)) );

}
#endif

Boolean CasualtyLevelReached::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);
      f.getUWord(d_side0Casualties);
      f.getUWord(d_side1Casualties);

      if(version < 0x0003)
      {
         if(version >= 0x0001)
         {
            UWORD rubbish;
            f.getUWord(rubbish);
         }
      }

      if(f.getMode() == SaveGame::SaveGame)
      {
         UBYTE b;
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
      else
      {
         if(version < 0x0002)
         {
            UBYTE rubbish;
            f.getUByte(rubbish);
         }

         d_conditionMet = False;
      }
   }

  return f.isOK();
}

Boolean CasualtyLevelReached::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // Todo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUWord(d_side0Casualties);
      f.putUWord(d_side1Casualties);
      //  f.putUWord(d_totalCasualties);

      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }
   }

  return f.isOK();
}

//=========== implementation of NationAtWar ==============

UWORD NationAtWar::fileVersion = 0x0000;

#ifdef CUSTOMIZE
bool NationAtWar::checkData()
{
   if(d_nation != NATION_Neutral)
     return True;

   s_errorText = "Improper Value in the Condition -- Nation at War";
   return False;
}
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)

String NationAtWar::describe(CampaignData* campData)
{
    String result = "NationAtWar (";
    result += scenario->getNationName(d_nation);
    if(d_atWar)
        result += ", True";
    else
        result += ", False";
    result += ")";
    return result;
}

#endif

#if !defined(EDITOR)
void NationAtWar::dailyUpdate(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);
  ASSERT(d_nation != NATION_Neutral);

  Boolean nationActive = campGame->campaignData()->isNationActive(d_nation);

  d_conditionMet = (d_atWar) ? nationActive : !nationActive;
}
#endif

Boolean NationAtWar::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      f.getUByte(d_nation);

      UBYTE b;
      f.getUByte(b);
      d_atWar = (b  != 0);

      if(f.getMode() == SaveGame::SaveGame)
      {
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
   }

  return f.isOK();
}

Boolean NationAtWar::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUByte(d_nation);
      f.putUByte(d_atWar);

      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }
   }

  return f.isOK();
}

//=========== implementation of SideVPTotal ==============

UWORD SideVPTotal::fileVersion = 0x0000;

#ifdef CUSTOMIZE

bool SideVPTotal::checkData()
{
   if(d_side != SIDE_Neutral)
      return True;

   s_errorText = "Improper Value in the Condition -- SideVPTotal";
   return False;
}
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)

String SideVPTotal::describe(CampaignData* campData)
{
    String result = "SideVPTotal";
    return result;
}

#endif

#if !defined(EDITOR)
void SideVPTotal::dailyUpdate(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);
  ASSERT(d_side != SIDE_Neutral);

  if(!d_conditionMet)
    d_conditionMet = static_cast<Boolean>(campGame->campaignData()->getVictoryLevel(d_side) >= d_vpTotal);
}
#endif

Boolean SideVPTotal::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      f.getUByte(d_side);

      f.getUWord(d_vpTotal);

      if(f.getMode() == SaveGame::SaveGame)
      {
         UBYTE b;
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
   }

  return f.isOK();
}

Boolean SideVPTotal::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUByte(d_side);
      f.putUWord(d_vpTotal);

      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }
   }

  return f.isOK();
}

//============= implemetation of TakesOverNeutral ================

UWORD MovesIntoNeutral::fileVersion = 0x0001;

#ifdef CUSTOMIZE

bool MovesIntoNeutral::checkData()
{
   if(d_nationality != NATION_Neutral && d_side != SIDE_Neutral)
   {
      return True;
   }

   s_errorText = "Improper Value in the Condition -- Moves Into Neutral";
   return False;

}
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)

String MovesIntoNeutral::describe(CampaignData* campData)
{
    String result = "MovesIntoNeutral";
    return result;
}

#endif

#if !defined(EDITOR)
void MovesIntoNeutral::instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data)
{
  ASSERT(campGame != 0);
  ASSERT(data != 0);

  ASSERT(d_nationality != NATION_Neutral);
  ASSERT(d_side != SIDE_Neutral);

  ASSERT(data->d_nation != NATION_Neutral);
  ASSERT(data->d_side != NATION_Neutral);

#ifdef DEBUG
  cLog.printf("%s is moving into %s, Checking for %s moving into %s",
      scenario->getSideName(data->d_side),
      scenario->getNationName(data->d_nation),
      scenario->getSideName(d_side),
      scenario->getNationName(d_nationality));
#endif

  if(!d_conditionMet)
  {
    d_conditionMet = static_cast<Boolean>( (d_nationality == data->d_nation) &&
                                           (d_side == data->d_side) );
  }

}
#endif

Boolean MovesIntoNeutral::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);
      f.getUByte(d_nationality);
      f.getUByte(d_side);

      if(f.getMode() == SaveGame::SaveGame)
      {
         UBYTE b;
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
      else
      {
         if(version < 0x0001)
         {
            UBYTE rubbish;
            f.getUByte(rubbish);
         }

         d_conditionMet = False;
      }
   }

  return f.isOK();
}

Boolean MovesIntoNeutral::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUByte(d_nationality);
      f.putUByte(d_side);

      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }
   }

  return f.isOK();
}

//============= implementation of LeaderIsDead ===========================

UWORD LeaderIsDead::fileVersion = 0x0001;
#ifdef CUSTOMIZE

bool LeaderIsDead::checkData()
{
   if(d_leader != NoLeader)
      return True;

   s_errorText = "Improper Value in the Condition -- Leader is Dead";
   return False;

}
#endif

#if defined(CUSTOMIZE) || defined(DEBUG)
String LeaderIsDead::describe(CampaignData* campData)
{
    String result = "LeaderIsDead";
    return result;
}

#endif
#if !defined(EDITOR)
void LeaderIsDead::instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data)
{
  ASSERT(campGame != 0);
  ASSERT(data != 0);
  ASSERT(d_leader != NoLeader);
  ASSERT(data->d_leader != NoLeader);

  if(!d_conditionMet)
  {
    d_conditionMet = static_cast<Boolean>(d_leader == data->d_leader);
  }
}
#endif

Boolean LeaderIsDead::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);
      // f.getUWord(d_leader);

      LeaderIndex leader;
      f >> leader;
      d_leader = army->leader(leader);

      if(f.getMode() == SaveGame::SaveGame)
      {
         UBYTE b;
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
      else
      {
         if(version < 0x0001)
         {
            UBYTE rubbish;
            f.getUByte(rubbish);
         }

         d_conditionMet = False;
      }
   }

  return f.isOK();
}

Boolean LeaderIsDead::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      // f.putUWord(d_leader);
      f << army->getIndex(d_leader);

      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }
   }

  return f.isOK();
}

//============= implementation of DateReached ============================

UWORD DateReached::fileVersion = 0x0002;

#ifdef CUSTOMIZE

bool DateReached::checkData()
{
   const CampaignData* campData = campaignData;    // naught access to global
   const Date& cDate = campData->getDate();


//  if( (d_date.year == d_date.getStartYear()) &&
//      (d_date.month < Date::April) )
    if(d_date < cDate)
    {
        s_errorText = "Invalid date in condition";
        return false;
    }

   return True;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String DateReached::describe(CampaignData* campData)
{
    char buffer[80];

    String result = "Date ";
    if(!d_reached)
        result += "Not ";
    result += "Reached (";
    result += d_date.toAscii(buffer);
    result += ")";
    return result;
}

#endif

#if !defined(EDITOR)
void DateReached::dailyUpdate(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);
  ASSERT(!d_conditionMet);

  const Date& date = campGame->campaignData()->getDate();

  if(d_reached)
  {
    if(date.year > d_date.year)
      d_conditionMet = True;
    else if(date.year == d_date.year)
    {
      if(date.month > d_date.month)
        d_conditionMet = True;
      else if(date.month == d_date.month)
      {
        if(date.day >= d_date.day)
          d_conditionMet = True;
      }
    }
  }
  else
  {
    if(date.year < d_date.year)
      d_conditionMet = True;
    else if(date.year == d_date.year)
    {
      if(date.month < d_date.month)
        d_conditionMet = True;
      else if(date.month == d_date.month)
      {
        if(date.day < d_date.day)
          d_conditionMet = True;
      }
    }
  }

}
#endif

Boolean DateReached::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);
      f.getUByte(d_date.day);
      f.getUByte(d_date.month);
      f.getUWord(d_date.year);

      if(f.getMode() == SaveGame::SaveGame)
      {
         UBYTE b;
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
      else
      {
         if(version < 0x0001)
         {
            UBYTE rubbish;
            f.getUByte(rubbish);
         }

         d_conditionMet = False;
      }

      if(version >= 0x0002)
      {
         UBYTE b;
         f.getUByte(b);
         d_reached = (b != 0);
      }
      else
         d_reached = True;
   }

  return f.isOK();
}

Boolean DateReached::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUByte(d_date.day);
      f.putUByte(d_date.month);
      f.putUWord(d_date.year);

      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }

      f.putUByte(d_reached);
   }

  return f.isOK();
}

//============= implementation of SideOwnsTown ============================

UWORD SideOwnsTown::fileVersion = 0x0000;

#ifdef CUSTOMIZE

bool SideOwnsTown::checkData()
{
   if(d_side != SIDE_Neutral && d_town != NoTown)
      return True;

   s_errorText = "Improper Value in the Condition -- Side Owns Town";
   return False;

}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String SideOwnsTown::describe(CampaignData* campData)
{
    String result = "SideOwnsTown (";
    result += campData->getTownName(d_town);
    result += ", ";
    result += scenario->getSideName(d_side);
    result += ")";
    return result;
}

#endif

#if !defined(EDITOR)
void SideOwnsTown::dailyUpdate(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);
  ASSERT(d_side != SIDE_Neutral);
  ASSERT(d_town != NoTown);

  Town& town = campGame->campaignData()->getTown(d_town);

  d_conditionMet = static_cast<Boolean>(town.getSide() == d_side);
}
#endif

Boolean SideOwnsTown::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      f.getUByte(d_side);
      f.getUWord(d_town);

      if(f.getMode() == SaveGame::SaveGame)
      {
         UBYTE b;
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
      else
      {
         d_conditionMet = False;
      }
   }

  return f.isOK();
}

Boolean SideOwnsTown::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUByte(d_side);
      f.putUWord(d_town);

      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }
   }

  return f.isOK();
}

//============= implementation of ArmisticeEnds ============================

UWORD ArmisticeEnds::fileVersion = 0x0000;

#ifdef CUSTOMIZE

bool ArmisticeEnds::checkData()
{
   return True;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String ArmisticeEnds::describe(CampaignData* campData)
{
    String result = "ArmisticeEnds";
    return result;
}

#endif

#if !defined(EDITOR)
void ArmisticeEnds::instantUpdate(CampaignLogicOwner* campGame, ConditionTypeData* data)
{
  ASSERT(campGame != 0);

  if(!d_conditionMet)
    d_conditionMet = True;
}
#endif

Boolean ArmisticeEnds::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      if(f.getMode() == SaveGame::SaveGame)
      {
         UBYTE b;
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
      else
      {
         d_conditionMet = False;
      }
   }

  return f.isOK();
}

Boolean ArmisticeEnds::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }
   }

  return f.isOK();
}

//============= implementation of ArmisticeStarts ============================

UWORD ArmisticeStarts::fileVersion = 0x0000;

#ifdef CUSTOMIZE

bool ArmisticeStarts::checkData()
{
   return True;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String ArmisticeStarts::describe(CampaignData* campData)
{
    String result = "ArmisticeStarts";
    return result;
}

#endif

#if !defined(EDITOR)
void ArmisticeStarts::dailyUpdate(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);

  if(!d_conditionMet)
    d_conditionMet = campGame->campaignData()->armisticeInEffect();

}
#endif

Boolean ArmisticeStarts::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      if(f.getMode() == SaveGame::SaveGame)
      {
         UBYTE b;
         f.getUByte(b);
         d_conditionMet = (b != 0);
      }
      else
      {
         d_conditionMet = False;
      }
   }

   return f.isOK();
}

Boolean ArmisticeStarts::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      if(f.getMode() == SaveGame::SaveGame)
      {
         f.putUByte(d_conditionMet);
      }
   }

  return f.isOK();
}

//============= implementation of ConditionTypeAllocator==================

ConditionType* ConditionTypeAllocator::newConditionType(ConditionType::Type type)
{
  if(type == ConditionType::ID_TownChangesSide)
    return new TownChangesSide;

  else if(type == ConditionType::ID_CasualtyLevelReached)
    return new CasualtyLevelReached;

  else if(type == ConditionType::ID_MovesIntoNeutral)
    return new MovesIntoNeutral;

  else if(type == ConditionType::ID_LeaderIsDead)
    return new LeaderIsDead;

  else if(type == ConditionType::ID_DateReached)
    return new DateReached;

  else if(type == ConditionType::ID_SideVPTotal)
    return new SideVPTotal;

  else if(type == ConditionType::ID_NationAtWar)
    return new NationAtWar;

  else if(type == ConditionType::ID_SideOwnsTown)
    return new SideOwnsTown;

  else if(type == ConditionType::ID_ArmisticeEnds)
    return new ArmisticeEnds;

  else if(type == ConditionType::ID_ArmisticeStarts)
    return new ArmisticeStarts;

#ifdef DEBUG
  else
    FORCEASSERT("Improper type in ConditionTypeAllocator");
#endif

  return 0;
}

//============= implementation of ConditionTypeList ======================

UWORD ConditionTypeList::fileVersion = 0x0000;

Boolean ConditionTypeList::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);
      UWORD nTypes;
      f.getUWord(nTypes);

      while(nTypes--)
      {
         UBYTE b;
         f.getUByte(b);
         ConditionType::Type type = static_cast<ConditionType::Type>(b);

         ConditionType* item = ConditionTypeAllocator::newConditionType(type);
         ASSERT(item != 0);

         if(!item->read(f, army))
            return False;

         append(item);
      }
   }

  return f.isOK();
}

Boolean ConditionTypeList::write(FileWriter& f, const Armies* army) const
{

   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);

      UWORD nTypes = static_cast<UWORD>(entries());
      f.putUWord(nTypes);

      SListIterR<ConditionType> iter(this);
      while(++iter)
      {
         const ConditionType* item = iter.current();
         f.putUByte(item->getType());

         if(!item->write(f, army))
            return False;

      }
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool ConditionTypeList::checkData()
{
   bool ok = True;
   SListIter<ConditionType> iter(this);
   while(++iter)
   {
      if(!iter.current()->checkData())
      {
         ok = False;
         break;
      }
   }
   return ok;
}


#endif

//============= implementation of ActionTypeAllocator==================

ActionType* ActionTypeAllocator::newActionType(ActionType::Type type)
{
  if(type == ActionType::ID_Victory)
    return new SideHasVictory;

  else if(type == ActionType::ID_Armistice)
    return new ArmisticeReached;

  else if(type == ActionType::ID_NationEntersWar)
    return new NationEntersWar;

  else if(type == ActionType::ID_Reinforcements)
    return new ReinforcementsArrive;

  else if(type == ActionType::ID_LeaderLeaves)
    return new LeaderLeaves;

  else if(type == ActionType::ID_LeaderEnters)
    return new LeaderEnters;

  else if(type == ActionType::ID_AllyDefects)
    return new AllyDefects;

  else if(type == ActionType::ID_AdjustResources)
    return new AdjustResources;

#ifdef DEBUG
  else
    FORCEASSERT("Improper type in ActionTypeAllocator");
#endif

  return 0;
}

//============= implementation of ActionTypeList ======================

UWORD ActionTypeList::fileVersion = 0x0000;

Boolean ActionTypeList::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);
      UWORD nTypes;
      f.getUWord(nTypes);

      while(nTypes--)
      {
         UBYTE b;
         f.getUByte(b);
         ActionType::Type type = static_cast<ActionType::Type>(b);

         ActionType* item = ActionTypeAllocator::newActionType(type);
         ASSERT(item != 0);

         if(!item->read(f, army))
            return False;

         append(item);
      }
   }

  return f.isOK();
}

Boolean ActionTypeList::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);

      UWORD nTypes = static_cast<UWORD>(entries());
      f.putUWord(nTypes);

      SListIterR<ActionType> iter(this);
      while(++iter)
      {
         const ActionType* item = iter.current();
         f.putUByte(item->getType());

         if(!item->write(f, army))
            return False;

      }
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool ActionTypeList::checkData()
{
   bool ok = True;
   SListIter<ActionType> iter(this);
   while(++iter)
   {
      if(!iter.current()->checkData())
      {
         ok = False;
         break;
      }
   }
   return ok;
}



#endif

/*============= implementation of ActionType derived classes =============================
 *
 * =========== SideHasVictory ============================================
 */

UWORD SideHasVictory::fileVersion = 0x0001;
Boolean SideHasVictory::read(FileReader& f, Armies* army)
{
    if(f.isAscii())
    {
         // ToDo
    }
    else
    {
         UWORD version;
         f.getUWord(version);
         ASSERT(version <= fileVersion);

         f.getUByte(d_whoWins);

         if(version >= 0x0001)
            f.getULong(d_victoryDate);
    }
   return f.isOK();
}

Boolean SideHasVictory::write(FileWriter& f, const Armies* army) const
{
    if(f.isAscii())
    {
         // ToDo
    }
    else
    {
       f.putUWord(fileVersion);
       f.putUByte(d_whoWins);
     f.putULong(d_victoryDate);
    }
  return f.isOK();
}

#ifdef CUSTOMIZE

bool SideHasVictory::checkData()
{
   if(d_whoWins < scenario->getNumSides())
      return true;
   if(d_whoWins == SIDE_Neutral)
      return true;

   s_errorText = "Improper Value in the Action -- Side Has Victory";
   return false;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String SideHasVictory::describe(CampaignData* campData)
{
    String result = "SideHasVictory";
      return result;
}

#endif

#if !defined(EDITOR)

Boolean SideHasVictory::run(CampaignLogicOwner* campGame)
{
   CampaignData* campData = campGame->campaignData();
   if(d_whoWins != SIDE_Neutral && campData->getTick() >= d_victoryDate)
   {
      ASSERT(d_whoWins != SIDE_Neutral);
      ASSERT(d_whoWins < scenario->getNumSides());

      // calculate victory level
      GameVictory::VictoryLevel vl;
      if(d_victoryDate == 0)
         vl = GameVictory::Crushing;
      else
      {
         LONG ourVictory = campData->getVictoryLevel(d_whoWins);
         LONG theirVictory = campData->getVictoryLevel((d_whoWins == 0) ? 1 : 0);

         if(ourVictory >= theirVictory * 4)
            vl = GameVictory::Crushing;
         else if(ourVictory >= theirVictory * 2)
            vl = GameVictory::Major;
         else if(ourVictory >= theirVictory * 1.33)
            vl = GameVictory::Minor;
         else
         {
            vl = GameVictory::NoVictor;
            d_whoWins = SIDE_Neutral;
         }
      }

      // GameVictory* gv = &campData->getGameVictory();
      // GameVictory* gv = const_cast<GameVictory*>(&campData->getGameVictory());
      // gv->setWinner(d_whoWins, vl);

#ifdef DEBUG
      cLog.printf("%s is Victorious", (d_whoWins != SIDE_Neutral) ? scenario->getSideName(d_whoWins) : "No one");
#endif

      // FORCEASSERT("Victory Window must be rewritten");
      campData->game()->endCampaign(GameVictory(d_whoWins, vl));

#if 0
      WargameMessage::postMessage(new WargameMessage::Victory(campData));
#endif
      return True;
   }

   else if(d_whoWins == SIDE_Neutral)
   {
      LONG side0Victory = campData->getVictoryLevel(0);
      LONG side1Victory = campData->getVictoryLevel(1);

      d_whoWins = (side0Victory >= side1Victory) ? 0 : 1;

      int weeks = 0;
      int loop = 4;
      int incChance = 20;
      int chance = 40;
      while(loop--)
      {
         weeks++;
         if(CRandom::get(100) <= chance)
            break;

         chance += incChance;
      }

      d_victoryDate = campData->getTick() + WeeksToTicks(weeks);
   }

   return False;
}

#if 0
//---------------------------------------
// This used to be in campwind\victwind
// Need a better way of communicating to campgame

void WargameMessage::Victory::run()
{
}
#endif

#endif

//================ ArmisticedReached ==========================

UWORD ArmisticeReached::fileVersion = 0x0000;

Boolean ArmisticeReached::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      f.getULong(d_armisticeTime);
   }

  return f.isOK();
}

Boolean ArmisticeReached::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);

      f.putULong(d_armisticeTime);
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool ArmisticeReached::checkData()
{
   if(d_armisticeTime > 0)
      return True;

   s_errorText = "Improper Value in the Action -- Armistice Reached";
   return False;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String ArmisticeReached::describe(CampaignData* campData)
{
    String result = "ArmisticeReached";
    return result;
}

#endif

#if !defined(EDITOR)
Boolean ArmisticeReached::run(CampaignLogicOwner* campGame)
{
  ASSERT(d_armisticeTime > 0);
  CampaignData* campData = campGame->campaignData();

  campData->startArmistice(d_armisticeTime + campData->getTick());

#ifdef DEBUG
  cLog.printf("%d day Armistice starts on %s",
     d_armisticeTime/(TicksPerHour*SysTick::HoursPerDay),
     campData->asciiTime());
#endif

  return True;
}
#endif   // EDITOR

//================= NationEntersWar ==============================

UWORD NationEntersWar::fileVersion = 0x0000;

Boolean NationEntersWar::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      f.getUByte(d_nation);
      f.getUByte(d_whichSide);

      UBYTE b;
      f.getUByte(b);
      d_entersWar = (b != 0);
   }

  return f.isOK();
}

Boolean NationEntersWar::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);

      f.putUByte(d_nation);
      f.putUByte(d_whichSide);
      f.putUByte(d_entersWar);
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool NationEntersWar::checkData()
{
   if(d_nation != NATION_Neutral)
      return True;

   s_errorText = "Improper Value in the Action -- Nation Enters War";
   return False;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String NationEntersWar::describe(CampaignData* campData)
{
    String result = "NationEntersWar (";
    result += scenario->getNationName(d_nation);
    result += ")";
    return result;
}

#endif

#if !defined(EDITOR)
Boolean NationEntersWar::run(CampaignLogicOwner* campGame)
{
  ASSERT(d_nation != NATION_Neutral);
  ASSERT(d_nation < scenario->getNumNations());

  CampaignData* campData = campGame->campaignData();

  /*
   * If nation is entering war
   */

  if (campData->getNationAllegiance(d_nation) == SIDE_Neutral)      // SWG: 01dec99: Only bring in nations that are not already active
  {
     if(d_entersWar)
     {
       ASSERT(d_whichSide != SIDE_Neutral);
       ASSERT(d_whichSide < scenario->getNumSides());

       campData->setNationAllegiance(d_nation, d_whichSide);

   #ifdef DEBUG
       cLog.printf("%s is entering the war for the %s",
          scenario->getNationName(d_nation),
          scenario->getSideName(d_whichSide));
   #endif

       /*
        * Set province and towns not belonging to the other side to whichside
        */

       ProvinceList& provinces = campData->getProvinces();

       for(IProvince pIndex = 0; pIndex < provinces.entries(); pIndex++)
       {
         Province& p = provinces[pIndex];

         if(p.getNationality() == d_nation)
         {
           ITown stop = ITown(p.getTown() + p.getNTowns());

           for(ITown tIndex = p.getTown(); tIndex < stop; tIndex++)
           {
             Town& town = campData->getTown(tIndex);

             if(town.getSide() == SIDE_Neutral)
               town.setSide(d_whichSide);
           }

           p.setSide(d_whichSide);
           p.setStartSide(d_whichSide);
         }
       }

       /*
        * Set Nation unit status
        */

       UnitIter iter(&campData->getArmies());
       while(iter.next())
       {
         ICommandPosition cpi = iter.current();

         /*
          *  No need to do President or God
          */

         if(cpi->isLower(Rank_President))
         {
            if(!cpi->isActive())    // SWG: 01dec99: prevent already active units from disapearing
               cpi->isActive(campData->getNations().isActive(cpi->getNation()));
         }
       }

     }


     /*
      * if nation never enters war
      */

     else
     {
   #ifdef DEBUG
       cLog.printf("%s will not enter the war", scenario->getNationName(d_nation));
   #endif
       campData->nationNeverEnters(d_nation);
     }
  }

  return True;
}
#endif   // EDITOR
//================== ReinforcementsArrive ========================

UWORD ReinforcementsArrive::fileVersion = 0x0000;

Boolean ReinforcementsArrive::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      CPIndex cpi;
      f >> cpi;
      d_cpi = army->command(cpi);
      // f.getUWord(d_cpi);
   }

  return f.isOK();
}

Boolean ReinforcementsArrive::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUWord(army->getIndex(d_cpi));
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool ReinforcementsArrive::checkData()
{
   if(d_cpi != NoCommandPosition)
      return True;

   s_errorText = "Improper Value in the Action -- Reinforcements Arrive";
   return False;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String ReinforcementsArrive::describe(CampaignData* campData)
{
    String result = "ReinforcementsArrive (";
    result += d_cpi->getNameNotNull();
    result += ")";
    return result;
}

#endif

#if !defined(EDITOR)


struct TownItem : public SLink {
   ITown d_town;

   TownItem() : d_town(NoTown) {}
   TownItem(ITown t) : d_town(t) {}
};

class TownItemList : public SList<TownItem> {
   public:
      TownItemList() {}

      TownItem* add(ITown t)
      {
         TownItem* ti = new TownItem(t);
         ASSERT(ti);

         append(ti);
         return ti;
      }

      bool inList(ITown t)
      {
         SListIterR<TownItem> iter(this);
         while(++iter)
         {
            if(iter.current()->d_town == t)
              return True;
         }
         return False;
      }
 };


ITown findCloseTown(CampaignData* campData, Side s, ITown fromTown, TownItemList& list)
{
   /*
    * Copy the new flags to the towns
    */

   ITown bestTown = NoTown;
   Distance bestDist = Distance_MAX;
   CampaignRoute route(campData);
   CampaignPosition fromPos = fromTown;
   TownList& towns = campData->getTowns();

   for(ITown t = 0; t < towns.entries(); t++)
   {
      if(t == fromTown || list.inList(t))
        continue;

      Town* cTown = &towns[t];
      if(cTown->getSide() != s)
        continue;

      Distance routeDistance = 0;
      CampaignPosition toPos = t;
      Boolean result = route.getRouteDistance(&fromPos, &toPos, Distance_MAX, routeDistance);
      if(result)
      {
        if(bestTown == NoTown || routeDistance < bestDist)
        {
           bestTown = t;
           bestDist = routeDistance;
        }
      }
   }

   list.add(fromTown);
   return bestTown;
}

bool enemyNearTown(CampaignData* campData, Side ourSide, ITown town)
{
   CampaignRoute route(campData);
   Armies* armies = &campData->getArmies();
   Side otherSide = (ourSide == 0) ? 1 : 0;
   UnitIter iter(armies, armies->getPresident(otherSide));
   while(iter.next())
   {
         if(iter.current() == armies->getPresident(otherSide))
            continue;

         Distance routeDistance = 0;
         CampaignPosition pos = town;
         Boolean result = route.getRouteDistance(iter.current()->location(), &pos, MilesToDistance(10), routeDistance);
         if(result)
         {
           return True;
         }
   }

   return False;
}

Boolean ReinforcementsArrive::run(CampaignLogicOwner* campGame)
{
   ASSERT(campGame != 0);
   ASSERT(d_cpi != NoCommandPosition);

   if(!d_cpi->isActive())       // Added SWG: 8Sep99.
   {
      CampaignData* campData = campGame->campaignData();

     // First check to make sure town is enemy is not within say 10 miles
     // if it is then we need to find another town
   //  Side otherSide = (side == 0) ? 1 : 0;
      Armies* armies = &campData->getArmies();

      TownItemList tlist;
      bool shouldActivate = True;

      ITown town = d_cpi->getTown();
      ASSERT(town != NoTown);

      TownList& tl = campData->getTowns();
      bool loop;
      do
      {
         loop = False;
         Town& t = tl[town];
         if(t.getSide() != d_cpi->getSide() || t.isSieged() || enemyNearTown(campData, d_cpi->getSide(), town))
         {
            loop = True;
            town = findCloseTown(campData, d_cpi->getSide(), town, tlist);
            if(town == NoTown)
               return False;
         }
      } while(loop);

      if(shouldActivate)
      {
         d_cpi->isActive(True);
         d_cpi->setPosition(town);
         // activate all subs
         UnitIter iter(armies, d_cpi, True);
         while(iter.next())
         {
            iter.current()->isActive(True);
            iter.current()->setPosition(town);
         }
      }
   #ifdef DEBUG
      {
         ASSERT(d_cpi->atTown());
         Town& town = campData->getTown(d_cpi->getTown());
         Province& prov = campData->getProvince(town.getProvince());
         cLog.printf("%s has been activated at %s, %s",
            (const char*)campData->getUnitName(d_cpi).toStr(),
            town.getName(),
            prov.getName());
      }
   #endif
   }

   return True;
}
#endif   // EDITOR
//================ Leader Leaves ======================

UWORD LeaderLeaves::fileVersion = 0x0000;

Boolean LeaderLeaves::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      LeaderIndex iLeader;
      f >> iLeader;
      // f.getUWord(d_iLeader);
      d_iLeader = army->leader(iLeader);
   }

  return f.isOK();
}

Boolean LeaderLeaves::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUWord(army->getIndex(d_iLeader));
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool LeaderLeaves::checkData()
{
   if(d_iLeader != NoLeader)
      return True;

   s_errorText = "Improper Value in the Action -- Leader Leaves";
   return False;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String LeaderLeaves::describe(CampaignData* campData)
{
    String result = "LeaderLeaves";
    return result;
}

#endif

#if !defined(EDITOR)
Boolean LeaderLeaves::run(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);
  ASSERT(d_iLeader != NoLeader);

#ifdef DEBUG
  cLog.printf("Action is Leader (%s) Leaves Game", d_iLeader->getName());
#endif

  campGame->campaignData()->getArmies().killAndReplaceLeader(d_iLeader);
  return True;
}
#endif   // EDITOR

//================ Leader Leaves ======================

UWORD LeaderEnters::fileVersion = 0x0000;

Boolean LeaderEnters::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      LeaderIndex iLeader;
      f >> iLeader;
      // f.getUWord(d_iLeader);
      d_iLeader = army->leader(iLeader);
   }

  return f.isOK();
}

Boolean LeaderEnters::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUWord(army->getIndex(d_iLeader));
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool LeaderEnters::checkData()
{
   if(d_iLeader != NoLeader)
      return True;

   s_errorText = "Improper Value in the Action -- Leader Enters";
   return False;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String LeaderEnters::describe(CampaignData* campData)
{
    String result = "LeaderEnters";
    return result;
}

#endif

#if !defined(EDITOR)
Boolean LeaderEnters::run(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);
  ASSERT(d_iLeader != NoLeader);

  CampaignData* campData = campGame->campaignData();
#ifdef DEBUG
  cLog.printf("Action is Leader Enters Game");
#endif

  // go through Indy leaders and find leader
  SListIter<IndependentLeader> iter(&campData->getArmies().independentLeaders());
  while(++iter)
  {
     if(iter.current()->leader() == d_iLeader)
     {
        // ASSERT(!iter.current()->active());
        iter.current()->active(True);
        break;
     }
  }

  return True;
}
#endif   // EDITOR
//====================== Ally Defects =======================


UWORD AllyDefects::fileVersion = 0x0000;

Boolean AllyDefects::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      UWORD nEntries;
      f.getUWord(nEntries);


      while(nEntries--)
      {
         UBYTE b;
         f.getUByte(b);
         ASSERT(b < scenario->getNumNations());

         NationItem* newItem = new NationItem(static_cast<Nationality>(b));
         ASSERT(newItem != 0);

         d_nations.append(newItem);
      }
   }

  return f.isOK();
}

Boolean AllyDefects::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);
      f.putUWord(d_nations.entries());

      SListIterR<NationItem> iter(&d_nations);
      while(++iter)
      {
         const NationItem* n = iter.current();

         f.putUByte(n->d_nation);
      }
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool AllyDefects::checkData()
{
   return True;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String AllyDefects::describe(CampaignData* campData)
{
    String result = "AllyDefects";
    return result;
}

#endif

#if !defined(EDITOR)
Boolean AllyDefects::run(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);

#ifdef DEBUG
  cLog.printf("Action is Ally Defects");
#endif

  if(d_nations.entries() > 0)
  {
    SListIterR<NationItem> iter(&d_nations);
    while(++iter)
    {
#ifdef DEBUG
      cLog.printf("%s is defecting", scenario->getNationName(iter.current()->d_nation));
#endif
      NationsList& nations = campGame->campaignData()->getNations();
      nations.wantsToDefect(iter.current()->d_nation, True);
    }
  }

  return True;
}
#endif

//====================== AdjustResources =======================

UWORD AdjustResources::fileVersion = 0x0000;

Boolean AdjustResources::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f >> version;
      ASSERT(version <= fileVersion);

      f >> d_who;
      f >> d_whatPercent;
   }

  return f.isOK();
}

Boolean AdjustResources::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f << fileVersion;

      f << d_who;
      f << d_whatPercent;
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool AdjustResources::checkData()
{
   if(d_who != NATION_Neutral)
      return True;

   s_errorText = "Improper Value in the Action -- Adjust Resources";
   return False;
}
#endif
#if defined(CUSTOMIZE) || defined(DEBUG)
String AdjustResources::describe(CampaignData* campData)
{
    String result = "AdjustResources (";
    result += scenario->getNationName(d_who);
    result += ") by ";
    char buffer[20];
    wsprintf(buffer, "%ld", (long)d_whatPercent);
    result += buffer;
    result += "%";
    return result;
}

#endif

#if !defined(EDITOR)
Boolean AdjustResources::run(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);
  ASSERT(d_who < scenario->getNumNations());

  CampaignData* campData = campGame->campaignData();

//  Armies& armies = campData->getArmies();

#ifdef DEBUG
  cLog.printf("AdjustResources for %s", scenario->getNationName(d_who));
#endif

  // go through provinces and adjust resources for nations provinces
  for(IProvince pi = 0; pi < campData->getProvinces().entries(); pi++)
  {
    const Province& p = campData->getProvince(pi);
    if(p.getNationality() == d_who)
    {
      // go through each town in province and adjust resource rate
      ITown stop = static_cast<ITown>(p.getTown() + p.getNTowns());
      for(ITown tIndex = p.getTown(); tIndex < stop; tIndex++)
      {
        Town& t = campData->getTown(tIndex);
        Attribute rate = t.getResourceRate();

// unchecked - added by Paul

        t.setResourceRate((rate * d_whatPercent) / 100);
#if 0
            rate += MulDiv(rate, d_whatPercent, 100);
            t.setResourceRate(rate);
#endif
// -------------------------
      }
    }
  }

  return False;
}

#endif   // EDITOR

//============= implementation of Conditions class =======================

#ifdef CUSTOMIZE

bool Conditions::checkData()
{
   return (d_conditions.checkData() && d_actions.checkData());
}


#endif

#if !defined(EDITOR)
void Conditions::update(CampaignLogicOwner* campGame, ConditionType::Type type, ConditionTypeData* data)
{
  ASSERT(campGame != 0);
  ASSERT(data != 0);
  ASSERT(!conditionMet());

  SListIter<ConditionType> iter(&d_conditions);

  while(++iter)
  {
    ConditionType* item = iter.current();

    if(item->conditionMet() && instantOnly())
      item->conditionMet(False);

    if(item->getType() == type && !item->conditionMet())
    {
      item->instantUpdate(campGame, data);

#ifdef DEBUG
        if(item->conditionMet())
        {
            cLog.printf("Condition Met: %s", (const char*)item->describe(campGame->campaignData()).c_str());
        }
#endif

    }
  }

  checkForConditionsMet();
}

void Conditions::dailyUpdate(CampaignLogicOwner* campGame)
{
  ASSERT(campGame != 0);
  ASSERT(!conditionMet());

  SListIter<ConditionType> iter(&d_conditions);

  while(++iter)
  {
    ConditionType* item = iter.current();
    if(!item->conditionMet())
    {
      item->dailyUpdate(campGame);
#ifdef DEBUG
        if(item->conditionMet())
        {
            cLog.printf("Condition Met: %s", (const char*)item->describe(campGame->campaignData()).c_str());
        }
#endif
    }
  }

  checkForConditionsMet();
}

void Conditions::checkForConditionsMet()
{
  SListIter<ConditionType> iter(&d_conditions);

  while(++iter)
  {
    ConditionType* item = iter.current();

    conditionMet(item->conditionMet());

    if(!allConditions())
    {
      if(conditionMet())
        break;
    }
    else
    {
      if(!conditionMet())
        break;
    }
  }
}

void Conditions::doAction(CampaignLogicOwner* campGame)
{
#ifdef DEBUG
    cLog.printf("----- Condition Activated -----");
    cLog.printf("All Conditions = %s", allConditions() ? "True" : "False");
    cLog.printf("Instant = %s", instantOnly() ? "True" : "False");

    {
        SListIter<ConditionType> iter(&d_conditions);

        cLog.printf("----- Conditions ----");
        while(++iter)
        {
            ConditionType* item = iter.current();
            cLog.printf("Condition %s: %s",
                (item->conditionMet() ? "met" : "not met"),
                item->describe(campGame->campaignData()).c_str());
        }
    }

    cLog.printf("----- Actions ----");
#endif

  // ActionTypeList& actions = condition->getActions();

  SListIter<ActionType> iter(&d_actions);
  while(++iter)
  {
    ActionType* item = iter.current();

#ifdef DEBUG
    cLog.printf("Running Action: %s", (const char*)(item->describe(campGame->campaignData()).c_str()));
#endif

    if(!item->run(campGame))
        conditionMet(false);
  }

#ifdef DEBUG
    cLog.printf("-------------------");
#endif
}

#endif

#if 0
void Conditions::setWinner(Side s)
{
#ifdef DEBUG
  ASSERT(s < scenario->getNumSides() || s == SIDE_Neutral);
#endif
  d_actions.whoWins = s;
}
#endif

/*
 * Conditions file interface
 */

UWORD Conditions::fileVersion = 0x0001;

Boolean Conditions::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      if(version < 0x0001)
      {
        UBYTE b;
        f.getUByte(b);
        allConditions(b != 0);

        if(f.getMode() == SaveGame::SaveGame)
        {
          f.getUByte(b);
          conditionMet(b != 0);
        }
      }

      else
      {
        f >> d_flags;
      }

      if(!d_conditions.read(f, army))
         return False;

      if(!d_actions.read(f, army))
         return False;
   }

  return f.isOK();
}

Boolean Conditions::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      f.putUWord(fileVersion);

      f << d_flags;

      if(!d_conditions.write(f, army))
         return False;

      if(!d_actions.write(f, army))
         return False;
   }

  return f.isOK();
}

//===================== implementation of ConditionList =================

UWORD ConditionList::fileVersion = 0x0000;

Boolean ConditionList::read(FileReader& f, Armies* army)
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      FileChunkReader fr(f, getChunkName());
      if(!fr.isOK())
         return False;

      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      UWORD nEntries;
      f.getUWord(nEntries);

      while(nEntries--)
      {
         Conditions* item = new Conditions;
         ASSERT(item != 0);

         if(!item->read(f, army))
            return False;

         append(item);
      }
   }

  return f.isOK();
}

Boolean ConditionList::write(FileWriter& f, const Armies* army) const
{
   if(f.isAscii())
   {
      // ToDo
   }
   else
   {
      FileChunkWriter fc(f, getChunkName());

      f.putUWord(fileVersion);
      f.putUWord(entries());

      SListIterR<Conditions> iter(this);
      while(++iter)
      {
         const Conditions* item = iter.current();

         if(!item->write(f, army))
            return False;
      }
   }

  return f.isOK();
}

#ifdef CUSTOMIZE

bool ConditionList::checkData()
{
   bool ok = True;
   SListIter<Conditions> iter(this);
   while(++iter)
   {
      if(!iter.current()->checkData())
      {
         ok = False;
         break;
      }
   }
   return ok;
}

#endif
const char* ConditionList::getChunkName() const
{
  return conditionsChunkName;
}

#if !defined(EDITOR)

void ConditionList::update(CampaignLogicOwner* campGame, ConditionType::Type typeID, ConditionTypeData* data)
{
   SListIter<Conditions> iter(this);

   while(++iter)
   {
    Conditions* item = iter.current();

    if(!item->conditionMet())
    {
      item->update(campGame, typeID, data);
      if(item->conditionMet())
                  item->doAction(campGame);
         // beginAction(campGame, item);
    }
   }
}

void ConditionList::dailyUpdate(CampaignLogicOwner* campGame)
{
   SListIter<Conditions> iter(this);

   while(++iter)
   {
    Conditions* item = iter.current();

    if(!item->conditionMet() && !item->instantOnly())
    {
      item->dailyUpdate(campGame);
      if(item->conditionMet())
                  item->doAction(campGame);
         // beginAction(campGame, item);
    }

   }
}
#endif

};  // namespace WG_CampaignConditions


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
