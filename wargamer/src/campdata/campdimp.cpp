/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Campaign Data Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "campdimp.hpp"
// #include "campctrl.hpp"
// #include "imp_acw.hpp"
#include "campint.hpp"
#include "misc.hpp"
#include "scenario.hpp"
#include "route.hpp"
#include "wldfile.hpp"
#include "filecnk.hpp"
#include "campmsg.hpp"
#include "options.hpp"
#include "savegame.hpp"
#include "control.hpp"
#include "cu_order.hpp"  // From campLogic!!! for starting orders
#include "randevnt.hpp"
#include "camp_snd.hpp"
// #include <string.hpp>
#include <stdio.h>

#ifdef DEBUG
#include "unitlog.hpp"
#endif
#include "todolog.hpp"

/*
 * Constructor
 */

RealCampaignData::RealCampaignData(CampaignInterface* campGame) :
    CampaignTimeData(),
    d_campGame(campGame),
    d_campaignInfo(),
    d_towns(),
    d_provinces(),
    d_connections(),
    d_armies(new Armies),
    d_terrainTypes(),
    d_conditionList(new WG_CampaignConditions::ConditionList),
    d_changed(false),
#if !defined(EDITOR)
        d_battleList(0),
        d_sideData(new CampaignSides),
        d_weather(),
        d_armistice(new Armistice),
        d_gameVictory(),
        d_sideRandomEvents(new CampaignSideRandomEvents),
//        d_pauseCount(0),
#endif
    d_campaignSoundSystem(0)
{
   // ASSERT(campGame != 0);
   // ASSERT(d_owner != 0);
#if !defined(EDITOR)
   d_battleList = new CampaignBattleList(this);
#endif

   d_campaignSoundSystem = new CampaignSoundSystem(&GlobalSoundSystemObj);

   int num = 2;
   GlobalSoundSystemObj.StopCDPlaying();
   GlobalSoundSystemObj.SetPlayList(num, 1+1, 2+1);
   GlobalSoundSystemObj.StartPlayList(true);

#if defined(GLOBAL_CAMPDATA)
   ToDo("Remove campaignData global variable");
   ASSERT(::campaignData == 0);
   ::campaignData = this;
#endif

#ifdef DEBUG
   cuLog.setCampaignData(this);
#endif
}

/*
 * Destructor
 */

RealCampaignData::~RealCampaignData()
{
#ifdef DEBUG
   cuLog.setCampaignData(0);
#endif

   /*
    * Remove Town Orders from units and towns
    */

   for (int i = 0; i < d_towns.entries(); ++i)
   {
      Town& town = d_towns[i];
      town.setFortUpgrader(NoCommandPosition);
      town.setSupplyInstaller(NoCommandPosition);
   }




   RandomEventAllocator::resetEvents();

   // Delete stuff in order
   // Armies must be deleted after everything else that references units

#if !defined(EDITOR)
   delete d_battleList; d_battleList = 0;
   delete d_sideData; d_sideData = 0;
   delete d_armistice; d_armistice = 0;
   delete d_sideRandomEvents; d_sideRandomEvents = 0;
#endif   // EDITOR
   delete d_conditionList; d_conditionList = 0;
   delete d_armies; d_armies = 0;

#if defined(GLOBAL_CAMPDATA)
   ToDo("Remove campaignData global variable");
   ASSERT(::campaignData == this);
   ::campaignData = 0;
#endif

   delete d_campaignSoundSystem;
   d_campaignSoundSystem = NULL;
}


#if 0

void RealCampaignData::create()
{
   changed = False;

   // initTime(Date(1, January, 1815), 0);
   initTime(1815, 0);

   // Boolean result = unitTypes.init(scenario->getUnitTypeFileName());
   // ASSERT(result);

   result = terrainTypes.init(scenario->getTerrainTypeFileName());
   ASSERT(result);

   importACWtowns(this);

   if(towns.entries() == 0)
   {
      /*
      * For testing, we'll make a few test towns
      */

      towns.init(5);

      struct TownSetup
      {
         const char* name;
         int mileX;
         int mileY;
      };

      static TownSetup townSetup[5] = {
                        { "Washington", 850, 290 },
                        { "New Orleans",        350, 650 },
                        { "Orlando",            810, 760 },
                        { "New York",           970, 150 },
                        { "Raleigh",            830, 400 }
                };

      for(ArrayIndex i = 0; i < 5; i++)
      {
         towns[i].setName(copyString(townSetup[i].name));
         towns[i].setLocation(Location(MilesToDistance(townSetup[i].mileX),
            MilesToDistance(townSetup[i].mileY)));
      }
   }

   if(d_provinces.entries() == 0)
   {
      d_provinces.init(5);

      struct ProvinceSetup
      {
         const char* fullName;
         const char* shortName;
         int mileX;
         int mileY;
      };

      static ProvinceSetup provinceSetup[5] = {
                        { "Maryland",           "MD", 900, 260 },
                        { "N. Carolina",        "NC", 820, 400 },
                        { "Florida",            "FL", 760, 730 },
                        { "Virginia",           "VA", 780, 300 },
                        { "Texas",                      "TX", 100, 600 }
                };

      for(ArrayIndex i = 0; i < 5; i++)
      {
         d_provinces[i].setName(copyString(provinceSetup[i].fullName));
         d_provinces[i].setShortName(copyString(provinceSetup[i].shortName));
         d_provinces[i].setLocation(Location(MilesToDistance(provinceSetup[i].mileX),
            MilesToDistance(provinceSetup[i].mileY)));
      }
   }

   /*
   * Make some more roads...
   */

   /*
   * Make roads to ensure every location is connected
   */

   Boolean connected = False;
   CampaignRoute route;

   while(!connected)
   {
      /*
      * Make a map of all routes from an arbitary node
      * exhaustive search ignoring lengths and sides
      */

      route.setup(CampaignRoute::FindBest | CampaignRoute::FillTable, SIDE_Neutral, 0);
      // route.fillTable = True;
      // route.planRoute(0, 1);
      route.fillRoute(0);

      const Town& t = towns[0];

      // Scan for any unconnected nodes

      const CampaignRouteNode* nodes = route.getNodes();
      ITown closeTown1 = NoTown;
      Distance closeDist1 = 0;

      for(ITown i = 0; i < towns.entries(); i++)
      {
         if(!nodes[i].isConnected())
         {
            const Town& t1 = towns[i];
            Distance d = distanceLocation(t.getLocation(), t1.getLocation());
            if((closeTown1 == NoTown) || (d < closeDist1))
            {
               closeTown1 = i;
               closeDist1 = d;
            }
         }
      }

      if(closeTown1 == NoTown)
         connected = True;
      else
      {
         // Find closest connected town to unconnected town

         Town& t2 = towns[closeTown1];

         ITown closeTown2 = NoTown;
         Distance closeDist2 = 0;

         for(ITown i = 0; i < towns.entries(); i++)
         {
            if(nodes[i].isConnected())
            {
               const Town& t3 = towns[i];
               Distance d = distanceLocation(t2.getLocation(), t3.getLocation());
               if((closeTown2 == NoTown) || (d < closeDist2))
               {
                  closeTown2 = i;
                  closeDist2 = d;
               }
            }
         }

         /*
         * Make a connection from closeTown1 to closeTown2
         */

         if(closeTown2 != NoTown)
         {
            IConnection ci = d_connections.addNew();
            ASSERT(ci != NoConnection);
            Connection* newCon = &d_connections[ci];

            newCon->node1 = closeTown1;
            newCon->node2 = closeTown2;
            newCon->how = CT_Road;
            newCon->quality = ConnectQuality(CRandom::get(CQ_Poor, CQ_Max-1));
            newCon->capacity = ConnectCapacity(CRandom::get(0, MaxConnectCapacity ));

            Town& t3 = towns[closeTown2];
            t2.addConnection(ci);
            t3.addConnection(ci);
         }
      }
   }

   calculateAllConnections();


   /*
   * Set up the Order of Battle
   */

#ifdef DEBUG
   d_armies->create();
#endif
}

#endif


#if defined(CUSTOMIZE)
/*
 * Delete a town
 *
 * This will be fairly complex because it must adjust all
 * references to town id's.
 */

void RealCampaignData::removeTown(ITown ti)
{
   d_towns.remove(ti);

   /*
   * Adjust connections
   */

   int delCount = 0;               // Number of connections to delete

   ConnectionIter ci = d_connections;
   while(++ci)
   {
      Connection& con = ci.current();

      if((con.node1 == ti) || (con.node2 == ti))
      {       // Delete this connection
         delCount++;
         con.node1 = NoTown;
         con.node2 = NoTown;
      }
      else
      {
         if(con.node1 > ti)
            con.node1--;
         if(con.node2 > ti)
            con.node2--;
      }
   }

   /*
   * Remove unused links
   */

   debugLog("%d connections to be deleted\n", delCount);

   if(delCount)
   {
      ci.rewind();

      while(++ci)
      {
         Connection& con = ci.current();
         if((con.node1 == NoTown) || (con.node2 == NoTown))
         {
            removeConnection(d_connections.getID(&con));
            ci.backup();
         }
      }
   }

   ProvinceIterWrite pi = d_provinces;
   while(++pi)
   {
      Province& prov = pi.current();
      if(prov.getCapital() == ti)
         prov.setCapital(NoTown);
      else if((prov.getCapital() != NoTown) && (prov.getCapital() > ti))
         prov.setCapital(ITown(prov.getCapital() - 1));
   }

   setChanged();
}

/*
 * Remove Connection from connectionlist and update any references to it
 */

void RealCampaignData::removeConnection(IConnection ci)
{
   d_connections.remove(ci);

   /*
   * Adjust all references in town list
   */

   TownIterWrite ti = d_towns;
   while(++ti)
   {
      Town& town = ti.current();
      town.adjustConnections(ci);
   }

   setChanged();
}

void RealCampaignData::swapTowns(ITown t1, ITown t2)
{
#ifdef DEBUG_SWAP
   const char* s1 = d_towns[t1].getName();
   const char* s2 = d_towns[t2].getName();
   debugLog("Swapping %d %s and %d %s\n",
      (int) t1, s1 ? s1 : "?",
      (int) t2, s2 ? s2 : "?");
#endif


   std::swap(d_towns[t1], d_towns[t2]);

   /*
   * Adjust connections
   */

   ConnectionIter ci = d_connections;
   while(++ci)
   {
      Connection& con = ci.current();

      if(con.node1 == t1)
         con.node1 = t2;
      else if(con.node1 == t2)
         con.node1 = t1;

      if(con.node2 == t1)
         con.node2 = t2;
      else if(con.node2 == t2)
         con.node2 = t1;
   }

   ProvinceIterWrite pi = d_provinces;
   while(++pi)
   {
      Province& prov = pi.current();
      if(prov.getCapital() == t1)
         prov.setCapital(t2);
      else if(prov.getCapital() == t2)
         prov.setCapital(t1);
   }

   /*
   * Adjust Unit's locations
   */

   UnitIter iter(d_armies, d_armies->getTop(), true);      // SWG: 12Jul99: Added true so all units are changed
   while(iter.next())
   {
      CommandPosition* cp = iter.currentCommand();
      if(cp->atTown())
      {
         ITown cpTown = cp->getTown();
         if(cpTown == t1)
            cp->setPosition(t2);
         else if(cpTown == t2)
            cp->setPosition(t1);
      }
   }


   setChanged();
}

/*
 * Sort the towns into provinces
 */

void RealCampaignData::sortTowns()
{
   d_towns.startWrite();
   d_provinces.startWrite();

   ITown* townMap = new ITown[d_towns.entries()];

   for(ITown i = 0; i < d_towns.entries(); i++)
      townMap[i] = NoTown;

   ITown* whereIs = new ITown[d_towns.entries()];
   ITown* whatIs = new ITown[d_towns.entries()];

   for(i = 0; i < d_towns.entries(); i++)
   {
      whereIs[i] = i;
      whatIs[i] = i;
   }

   UWORD nextTown = 0;

   for(IProvince p = 0; p < d_provinces.entries(); p++)
   {
      Province& prov = d_provinces[p];
      prov.setTown(nextTown);
      prov.setNTowns(0);

      for(ITown t = 0; t < d_towns.entries(); t++)
      {
         const Town& town = d_towns[t];

         if(town.getProvince() == p)
         {
            prov.setNTowns(ITown(prov.getNTowns() + 1));
            townMap[nextTown++] = t;
         }
      }
   }

   for(ITown t = 0; t < d_towns.entries(); t++)
   {
      const Town& town = d_towns[t];

      if(town.getProvince() == NoProvince)
         townMap[nextTown++] = t;
   }

   ASSERT(nextTown == d_towns.entries());

   /*
   * We've got a table showing the relationship of old towns to new ones
   * We now have to physically move them about
   */

#ifdef DEBUG_SORT_TOWN
   debugLog("townMap:\n");
   for(i = 0; i < d_towns.entries(); i++)
   {
      const char* s = d_towns[townMap[i]].getName();
      debugLog("%d: %3d %s\n", (int) i, (int) townMap[i], s ? s : "Un-Named");
   }
#endif

   for(i = 0; i < d_towns.entries(); i++)
   {
      ITown t = townMap[i];                   // Original position
      ITown where = whereIs[t];               // Where it is now
      ITown what = whatIs[i];                 // What's in our slot?

#ifdef DEBUG_SORT_TOWN
      debugLog("%3d: %3d %3d %3d\n", (int) i, (int) t, (int) where, (int) what);
#endif

      if(i != where)
      {
         ASSERT(i < where);

         swapTowns(i, where);

         whereIs[what] = where;
         whereIs[whatIs[where]] = i;

         whatIs[i] = whatIs[where];
         whatIs[where] = what;

         setChanged();
      }
   }

   d_provinces.endWrite();
   d_towns.endWrite();

}

#if 0
/*
 * Calculate connection lengths for all towns
 */

void RealCampaignData::calculateAllConnections()
{
   ConnectionList& cl = getConnections();
   for(IConnection ci = 0; ci < cl.entries(); ci++)
   {
      Connection& con = getConnection(ci);
      if(con.distance == 0 && !con.offScreen)
         calculateConnectionLength(ci);
   }

}
#endif

void RealCampaignData::removeProvince(IProvince pi)
{
   d_provinces.startWrite();
   d_towns.startWrite();

   d_provinces.remove(pi);

   /*
   * Adjust towns
   */

   TownIterWrite ti = d_towns;
   while(++ti)
   {
      Town& town = ti.current();

      IProvince p = town.getProvince();

      if(p == pi)
         town.setProvince(NoProvince);
      else if((p > pi) && (p != NoProvince))
         town.setProvince(IProvince(p - 1));
   }

   d_towns.endWrite();
   d_provinces.endWrite();
}


void RealCampaignData::removeAllTowns()
{
   d_towns.startWrite();
   d_provinces.startWrite();

   d_conditionList->reset();
   d_armies->removeAllUnits();
   d_towns.clear();
   d_provinces.clear();
   d_connections.clear();
   d_changed = True;

   d_provinces.endWrite();
   d_towns.endWrite();

   setChanged();
}

/*
 * Delete ALL units, leaders and strength points
 *
 * It should leave behind the nations with the top level commanders
 */

void RealCampaignData::removeAllUnits()
{
   d_conditionList->reset();
   d_armies->removeAllUnits();
   d_changed = True;
}

void RealCampaignData::setCapital(IProvince iProv, ITown iTown)
{
   ASSERT(iProv != NoProvince);

   if(iProv != NoProvince)
   {
      Province& prov = getProvince(iProv);

      /*
      * Remove old Capital
      */

      ITown oldCapital = prov.getCapital();
      if((oldCapital != NoTown) && (oldCapital != iTown))
      {
         Town& oldTown = getTown(oldCapital);

         oldTown.setSize(TOWN_City);
      }

      if(iTown != NoTown)
      {
         Town& town = getTown(iTown);

         /*
         * Remove from old Province
         */

         if(town.getIsCapital())
         {
            IProvince iOldProvince = town.getProvince();

            if((iOldProvince != NoProvince) && (iOldProvince != iProv))
            {
               Province& oldProvince = getProvince(iOldProvince);

               if(oldProvince.getCapital() == iTown)
                  oldProvince.setCapital(NoTown);
            }
         }
         town.setSize(TOWN_Capital);
         town.setProvince(iProv);
      }

      prov.setCapital(iTown);

      setChanged();
   }
}

void RealCampaignData::moveTownToProvince(IProvince iProv, ITown iTown)
{
   /*
   * Remove from old province:
   */

   ASSERT(iProv != NoProvince);
   ASSERT(iTown != NoTown);

   Province& prov = getProvince(iProv);
   Town& town = getTown(iTown);

   IProvince iOldProvince = town.getProvince();

   /*
   * Add to new province
   */

   town.setProvince(iProv);

   /*
   * Check if it was the old province's capital
   * If it is a capital then overide existing capital?
   */

   if(town.getIsCapital())
   {
      if(iOldProvince != NoProvince)
      {
         Province& oldProvince = getProvince(iOldProvince);

         if(oldProvince.getCapital() == iTown)
            oldProvince.setCapital(NoTown);
      }

      setCapital(iProv, iTown);
   }

   setChanged();
}

#endif  // CUSTOMIZE

/*
 * Return the strength points needed to take over an unoccupied town
 *
 * Friendly side, halves the SP needed
 */

SPCount RealCampaignData::getTownStrength(ITown itown, Side s) const
{
   const Town& town = d_towns[itown];
   SPCount sp = town.getStrength();

   /*
   * Must be an exact match...
   * i.e. neutral towns do not help
   *      nor do towns of a friendly nation.
   */

   IProvince iprov = town.getProvince();

   if(iprov != NoProvince)
   {
      const Province& prov = d_provinces[iprov];
      Side startSide = prov.getStartSide();

      if(s == startSide)
         sp = SPCount(sp / 2);
   }

   return sp;
}

Distance RealCampaignData::getConnectionLength(IConnection i) const
{
   const Connection& con = getConnection(i);

   return con.distance;
#if 0
      const Town& t1 = getTown(con.node1);
   const Town& t2 = getTown(con.node2);

   return distanceLocation(t1.getLocation(), t2.getLocation());
#endif
}

void RealCampaignData::calculateConnectionLength(IConnection i)
{
   Connection& con = getConnection(i);


   const Town& t1 = getTown(con.node1);
   const Town& t2 = getTown(con.node2);

   con.distance = distanceLocation(t1.getLocation(), t2.getLocation());
}

/*
 * Convert a distance along a connection into a physical location
 */

void RealCampaignData::getConnectionLocation(IConnection connection, Distance distAlong, Location& l) const
{
   Connection& rConnection = campaignData->getConnection(connection);
   Town& t1 = campaignData->getTown(rConnection.node1);
   Town& t2 = campaignData->getTown(rConnection.node2);

   const Location& l1 = t1.getLocation();
   const Location& l2 = t2.getLocation();

   Distance dx = l2.getX() - l1.getX();
   Distance dy = l2.getY() - l1.getY();

#if 0           // Always use stored value
      Distance d;
   if(rConnection.offScreen)
      d = rConnection.distance;
   else
      d = aproxDistance(dx, dy);
#else
   Distance d = rConnection.distance;
#endif

   l.setX(l1.getX() + (dx * distAlong) / d);
   l.setY(l1.getY() + (dy * distAlong) / d);
}

#if !defined(EDITOR)

void RealCampaignData::setLastSeenLocation(ICommandPosition cpi)
{
   CommandPosition* cp = getCommand(cpi);
   cp->setSeen(getTick());
}

Boolean RealCampaignData::isPlayerSide(Side side) const
{
//    if(side != SIDE_Neutral)
//    {
//       return (GamePlayerControl::getControl(side) == GamePlayerControl::Player);
//    }
//
//    return False;
   return GamePlayerControl::canControl(side);
}

Boolean RealCampaignData::isTownOrderable(ITown iTown) const
{
   ASSERT(iTown != NoTown);

   const Town& town = getTown(iTown);
#ifdef DEBUG
   return (isPlayerSide(town.getSide()) || Options::get(OPT_OrderEnemy));
#else
   return (isPlayerSide(town.getSide()));
#endif
}

#endif  // !EDITOR

/*
 * Return a more meaningful unit name
 */

SimpleString RealCampaignData::getUnitName(ConstICommandPosition cp)    const
{
#if 0
      static const int BufferCount = 8;
   static String strings[BufferCount];
   static int nextBuffer = 0;

   String& s = strings[nextBuffer];

   if(++nextBuffer >= BufferCount)
      nextBuffer = 0;
#else
   SimpleString s;
#endif

   s = scenario->getSideName(cp->getSide());
   s += " ";
   if(cp->getLeader() != NoLeader)
   {
      const Leader* l = getLeader(cp->getLeader());
      s += l->getName();
      s += " ";
   }

   s += cp->getName();

   return s;
}


#if !defined(EDITOR)
#ifdef DEBUG
void RealCampaignData::getCampaignPositionString(const CampaignPosition* pos, String& s) const
{
   if(pos->atTown())
   {
      ITown itown = pos->getTown();

      s = "At ";
      s += getTownName(itown);
   }
   else
   {
      ITown dest = pos->getDestTown();
      ITown from = pos->getLastTown();
      Distance dist = pos->getRemainingDistance();

      s = "Moving from ";
      s += getTownName(from);
      s += " to ";
      s += getTownName(dest);
      s += ", distance=";

      char buffer[10];
      sprintf(buffer, "%ld", (long) dist);

      s += buffer;

      sprintf(buffer, "%ld", (long) pos->getHeadAlong());
      s += ", head=";
      s += buffer;

      sprintf(buffer, "%ld", (long) pos->getTailAlong());
      s += ", tail=";
      s += buffer;
   }
}

#endif
#endif  // !EDITOR

const char* RealCampaignData::getTownName(ITown i, const char* defName) const
{
   if(i == NoTown)
      return defName;
   else
      return d_towns[i].getNameNotNull(defName);
}

const char* RealCampaignData::getTownName(ITown i) const
{
   return getTownName(i, "Nowhere");
}

#if !defined(EDITOR)
void RealCampaignData::killLeader(ICommandPosition cpi)
{
#if 0
      ILeader deadLeader = cpi->getLeader();
   ASSERT(deadLeader != NoLeader);
   Leader* leader = getLeader(deadLeader);

   // d_armies->unassignLeader(cpi);
   d_armies->detachLeader(deadLeader);
   d_armies->autoPromote(cpi, cp->getSide());

#if 0
      //---- sendPlayerMessage decides who to send message to!
      // if(isPlayerSide(cp->getSide()))
      d_campaignGame->sendPlayerMessage(cpi, NoTown, "Gen. %s has been killed by a sniper", leader->getName());
#endif
   CampaignMessageInfo msg(cp->getSide(), CampaignMessages::GeneralKilled);
   msg.cp(cpi);
   msg.leader(deadLeader);
   d_campaignGame->sendPlayerMessage(msg);


   d_armies->removeLeader(deadLeader);
#endif
}

#if 0
void RealCampaignData::startTactical(CampaignBattle* battle)
{
   d_campaignGame->startTactical(battle);
}
#endif


#if 0
/*
 *  Victory level functions
 */

void RealCampaignData::addVictoryLevel(ULONG points, Side side)
{
   ASSERT(side != SIDE_Neutral);
   ASSERT(side < scenario->getNumSides());
   victoryPoints.addSideVictory(points, side);
}

ULONG RealCampaignData::getVictoryLevel(Side side)
{
   ASSERT(side != SIDE_Neutral);
   ASSERT(side < scenario->getNumSides());
   return victoryPoints.getSideVictory(side);
}
#endif

#endif  // !EDITOR

/*
 * Is town a choke point
 */

Boolean RealCampaignData::isChokePoint(ITown itown) const
{
   ASSERT(itown != NoTown);

   const Town& town = getTown(itown);

   return d_terrainTypes[town.getTerrain()].isChokePoint();

}


Boolean RealCampaignData::isSameSideOfChokePoint(IConnection con1, IConnection con2) const
{
   if(con1 == con2)
      return True;

   ASSERT(con1 != NoConnection);
   ASSERT(con2 != NoConnection);

   const Connection& c1 = getConnection(con1);
   const Connection& c2 = getConnection(con2);

   ASSERT(c1.node1 == c2.node1 ||
      c1.node1 == c2.node2 ||
      c1.node2 == c2.node1 ||
      c1.node2 == c2.node2);

   /*
   * Get common town
   */

   ITown town = ( (c1.node1 == c2.node1) || (c1.node1 == c2.node2) ) ?  c1.node1 : c1.node2;

   return (c1.whichSideChokePoint(town) == c2.whichSideChokePoint(town));
}

#if !defined(EDITOR)
Boolean RealCampaignData::armisticeInEffect() const
{
   return d_armistice->inEffect();
}

void RealCampaignData::startArmistice(TimeTick tillWhen)
{
   d_armistice->startArmistice(tillWhen);
   d_campGame->armisticeBegun();
}

void RealCampaignData::updateArmistice(TimeTick theTime)
{
   if (d_armistice->inEffect())
   {
      d_armistice->update(theTime);
      if(!d_armistice->inEffect())
         d_campGame->armisticeEnded();
   }
}
#endif // !defined(EDITOR)

/*=======================================================================
 * File Handling
 */

/*
 * Load in the actual information
 */

const UWORD RealCampaignData::s_fileVersion = 0x0004;

Boolean RealCampaignData::readData(FileReader& f)
{

   /*
   * Read file header
   */

   FileChunkReader fr(f, campaignChunkName);
   if(!fr.isOK())
      return False;

   UWORD version;

   if(f.isAscii())
   {
#ifdef DEBUG
      todoLog.printf("TODO: RealCampaignData::readData().ascii... unwritten");
#endif
      return False;
   }
   else
   {
      f >> version;
      ASSERT(version <= s_fileVersion);

      if (version < 0x0004)
      {
         // Scenario is initialised during FileType

         const char* scenName = f.getString();
         // Scenario::create(scenName);
      }

      ASSERT(scenario != 0);

      /*
      * read player settings
      */

      if(f.getMode() == SaveGame::SaveGame)
      {
         GamePlayerControl::readData(f);
         CampaignOptions::readData(f);
      }

      /*
      * Read startDate
      */

      Year startYear;
      f.getUWord(startYear);

      ULONG timeValue;
      f.getULong(timeValue);

      initTime(startYear, timeValue);

      Boolean result = d_terrainTypes.init(scenario->getTerrainTypeFileName());
      ASSERT(result);

      /*
      * Nation list must be read before armies
      */

      if(!d_towns.readData(f))
         return False;
      if(!d_provinces.readData(f))
         return False;
      if(!d_connections.readData(f))
         return False;
      if(!d_armies->readData(f))
         return False;
      if(!d_campaignInfo.read(f))
         return False;
#if !defined(EDITOR)
      if(!d_weather.read(f))
         return False;

      if(version >= 0x0003)
      {
         if(!d_sideData->read(f))
            return False;
      }

      if(!d_sideRandomEvents->read(f, getTick()))
         return False;
      if(version >= 0x0001)
      {
         if(!d_battleList->read(f, *d_armies->ob(), this))
            return False;
      }
      if(version >= 0x0002)
      {
         if(!d_armistice->read(f))
            return False;
      }
#endif
      if(!d_conditionList->read(f, d_armies))
         return False;
   }

   /*
   * Bodge for nation status
   */

   UnitIter iter(d_armies, d_armies->getTop());
   while(iter.next())
   {
      ICommandPosition cpi = iter.current();

      /*
      *  No need to do President or God
      */

      if(cpi->isLower(Rank_President))
      {
         cpi->isActive(d_armies->isNationActive(cpi->getNation()));
      }

      // set flags
      if(cpi->getParent() != NoCommandPosition && cpi->getParent()->sameRank(Rank_President))
         d_armies->setCommandSpecialistFlags(cpi);
   }

#if !defined(EDITOR)
   /*
   * Initialise victory Points if it was a new campaign
   */

   if(f.getMode() != SaveGame::SaveGame)
   {
      d_sideData->resetVictory();

      TownIter it(d_towns);
      while(++it)
      {
         const Town& town = it.current();
         Side s = town.getSide();
         if(s != SIDE_Neutral)
            d_sideData->addSideVictory(town.getVictory(s), s);
      }
   }
#endif


   return f.isOK();
}

Boolean RealCampaignData::writeData(FileWriter& f) const
{
   /*
   * Write a file header
   */

   {
      FileChunkWriter fc(f, campaignChunkName);
      if(f.isAscii())
      {
         f.printf(fmt_ssn, scenarioToken, scenario->getFileName());
      }
      else
      {
         f << s_fileVersion;
         // f.putString(scenario->getFileName());

         /*
         * If its a saved game (rather than a Mission)
         * Then save various player settings
         */

         if(f.getMode() == SaveGame::SaveGame)
         {
            GamePlayerControl::writeData(f);

            /*
            * Also... save:
            *               game options
            *               window positions
            *               any other game settings
            */

            CampaignOptions::writeData(f);
         }

         /*
         * Write start Date
         */

         f.putUWord(CampaignTime::getStartYear());

         /*
         * Write Date/Time and set up variables
         */

         f.putULong(getCTime().toULONG());             // Hours sinces startTime

      }
   }

   //      if(!nationsList.write(f))
   //              return False;
   if(!d_towns.writeData(f))
      return False;
   if(!d_provinces.writeData(f))
      return False;
   if(!d_connections.writeData(f))
      return False;
   if(!d_armies->writeData(f))
      return False;
   if(!d_campaignInfo.write(f))
      return False;
   if(!d_conditionList->write(f, d_armies))
      return False;
#if !defined(EDITOR)
   if(!d_weather.write(f))
      return False;
   if(!d_sideData->write(f))
      return False;
#if 0
      if(!victoryPoints.write(f))
      return False;
   if(!d_losses.write(f))
      return False;
#endif
   if(!d_sideRandomEvents->write(f, getTick()))
      return False;
   // if(!battleList.write(f, const_cast<OrderBattle&>(*d_armies->ob())))
   if(!d_battleList->write(f, *d_armies->ob()))
      return False;
   if(!d_armistice->write(f))
      return False;
#endif


   return f.isOK();
}

#if 0
/*
 * Synchronisation
 */

void RealCampaignData::startWriteLock()
{
   d_lock.startWrite();
   towns.startWrite();
   provinces.startWrite();
   d_connections.startWrite();
   d_armies->startWrite();
}

void RealCampaignData::endWriteLock()
{
   towns.endWrite();
   provinces.endWrite();
   d_connections.endWrite();
   d_armies->endWrite();
   d_lock.endWrite();
}

void RealCampaignData::startReadLock() const
{
   d_lock.startRead();
   towns.startRead();
   provinces.startRead();
   d_connections.startRead();
   d_armies->startRead();
}

void RealCampaignData::endReadLock() const
{
   towns.endRead();
   provinces.endRead();
   d_connections.endRead();
   d_armies->endRead();
   d_lock.endRead();
}
#endif




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
