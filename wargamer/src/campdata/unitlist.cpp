/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Stacked Unit list
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.3  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/22 10:28:36  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/16 17:33:39  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "unitlist.hpp"

const int StackedUnitList::BlockSize = 8;      // How many to allocate at a time

StackedUnitList::StackedUnitList()
{
   units.setBlockSize(BlockSize);
   generals.setBlockSize(BlockSize);
   changed = True;
}

StackedUnitList::~StackedUnitList()
{
}

void StackedUnitList::reset()
{
   units.reset();
   generals.reset();
   changed = True;
}

void StackedUnitList::addUnit(ConstParamCP cpi)
{
   for(UnitListID id = 0; id < units.getCount(); id++)
   {
      if(units[id].unit == cpi)
      {
         units[id].added = True;
         return;
      }
   }

   UnitIdentify ui = cpi;

   units.add(ui);
   changed = True;
}

void StackedUnitList::addGeneral(const ConstILeader& g)
{
   for(UnitListID id = 0; id < generals.getCount(); id++)
   {
      if(generals[id].general == g)
      {
         generals[id].added = True;
         return;
      }
   }

   GeneralIdentify gi = g;
   generals.add(gi);
   changed = True;
}

#if 0
const UnitIdentify* StackedUnitList::getUnit(unsigned int i) const
{
   ASSERT(i != NoUnitListID);
   return units.get(i);
}
#endif

ConstICommandPosition StackedUnitList::getUnit(UnitListID i) const
{
   ASSERT(i != NoUnitListID);
   return units.get(i)->unit;
}

void StackedUnitList::removeUnit(UnitListID i)
{
  ASSERT(i != NoUnitListID);
  units.remove(i);
}

ConstILeader StackedUnitList::getGeneral(UnitListID i) const
{
   ASSERT(i != NoUnitListID);
   return generals.get(i)->general;
}

void StackedUnitList::clearFlags()
{
   changed = False;
   for(UnitListID id = 0; id < units.getCount(); id++)
      units[id].added = False;
   for(id = 0; id < generals.getCount(); id++)
      generals[id].added = False;
}

Boolean StackedUnitList::checkFlags()
{
   for(UnitListID id = 0; id < units.getCount();)
   {
      if(!units[id].added)
      {
         units.remove(id);
         changed = True;
      }
      else
         id++;
   }

   for(id = 0; id < generals.getCount();)
   {
      if(!generals[id].added)
      {
         generals.remove(id);
         changed = True;
      }
      else
         id++;
   }

   return changed;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
