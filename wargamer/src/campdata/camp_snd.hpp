/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMP_SND_HPP
#define CAMP_SND_HPP

#include "cdatadll.h"
#include "soundsys.hpp"
#include "c_sounds.hpp"
#include "wavefile.hpp"
#include "myassert.hpp"
#include "random.hpp"
#include <windows.h>
#include <dsound.h>
#include <stack>
#include <vector>
#include <list>


/*-------------------------------------------------------------------------------------------------------------------

        File : CAMP_SND
        Description : Sound system for campaign game
        
-------------------------------------------------------------------------------------------------------------------*/

#define MAX_CAMPAIGN_SOUND_BUFFERS 4

struct CampaignSoundInfo {

    unsigned long base_frequency;
    int frequency_variation;
    int num_buffers;
    int buffers_used;
    LPDIRECTSOUNDBUFFER lpDSBuffer[MAX_CAMPAIGN_SOUND_BUFFERS];

    CampaignSoundInfo() {
        frequency_variation = 0;
        num_buffers = 0;
        buffers_used = 0;
        for(int f=0;f<MAX_CAMPAIGN_SOUND_BUFFERS;f++) lpDSBuffer[f] = NULL;
    }

};

enum CampaignSoundPriorityEnum {
    CAMPAIGNSOUNDPRIORITY_LOW = 1000,
    CAMPAIGNSOUNDPRIORITY_MEDIUM = 2000,
    CAMPAIGNSOUNDPRIORITY_HIGH = 3000,
    CAMPAIGNSOUNDPRIORITY_INSTANT = 10000
};



struct CampaignSoundEvent {
    // index into SoundsTable
    CampaignSoundTypeEnum sound_type;
    // priority assigned to this sound
    CampaignSoundPriorityEnum sound_priority;

    CampaignSoundEvent(void) { }

    CampaignSoundEvent(CampaignSoundTypeEnum type, CampaignSoundPriorityEnum priority) {
        sound_type = type;
        sound_priority = priority;
    }

    bool operator < (const CampaignSoundEvent& i) const { return (sound_priority) < (i.sound_priority); }
    bool operator > (const CampaignSoundEvent& i) const { return (sound_priority) > (i.sound_priority); }
    bool operator == (const CampaignSoundEvent& i) const { return ((sound_priority == i.sound_priority) && (sound_type == i.sound_type)); }

};


struct CampaignPlayingSound {
    
    CampaignSoundTypeEnum sound_type;
    CampaignSoundPriorityEnum sound_priority;
    LPDIRECTSOUNDBUFFER lpDSbuffer;

    bool operator < (const CampaignPlayingSound & i) const { FORCEASSERT("This shouldn't happen"); return FALSE; }
    bool operator > (const CampaignPlayingSound & i) const { FORCEASSERT("This shouldn't happen"); return FALSE; }
    bool operator == (const CampaignPlayingSound & i) const { return ((sound_type == i.sound_type) && (sound_priority == i.sound_priority)); }

};



class CampaignSoundSystem {

private:
    BOOL SetupOK;
    BOOL InUse;
    HWND app_hwnd;
    SoundSystem * SoundSys;
    LPDIRECTSOUND lpDirectSound;
    LPDIRECTSOUNDBUFFER lpPrimaryBuffer;
    LPDIRECTSOUNDBUFFER lpFormatBuffer;
    std::vector <CampaignSoundInfo> SoundsArray;
    std::deque <CampaignSoundEvent> WaitingQueue;
    unsigned int WaitingQueueMaxSize;
    std::vector <CampaignPlayingSound> PlayingList;
    unsigned int PlayingListMaxSize;

public:
    CAMPDATA_DLL CampaignSoundSystem(SoundSystem * sound_sys);
    CAMPDATA_DLL ~CampaignSoundSystem(void);

    CAMPDATA_DLL BOOL TriggerSound(CampaignSoundTypeEnum type, CampaignSoundPriorityEnum priority);
    CAMPDATA_DLL void ProcessSound(void);

private:
    void DirectSoundError(HRESULT err);
    BOOL InitialiseBuffers(void);
    BOOL CreatePrimaryBuffer(void);
    BOOL DestroyPrimaryBuffer(void);
    BOOL DestroyBuffers(void);
    BOOL RestoreBuffers(void);
    BOOL SetSpatialProperties(CampaignPlayingSound * snd);
    BOOL CreateBuffers(char * filename, CampaignSoundInfo & snd_inf);
    BOOL CopySoundToBuffer(WAVEFILE WaveFile, LPVOID lpWaveData, LPDIRECTSOUNDBUFFER lpDSbuffer);
    BOOL ProcessWaitingQueue(void);
    BOOL ProcessPlayingList(void);
};



#endif
