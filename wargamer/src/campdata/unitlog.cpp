/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Unit Log
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "unitlog.hpp"
#include "campdint.hpp"

UnitLogFile cuLog("campunit.log");

/*
 * Implementation of UnitLogFile
 */

UnitLogFile::UnitLogFile(const char* name) : LogFile(name)
{
   oldCommand = NoCommandPosition;
   lastTime = 0;
   campData = 0;
}

UnitLogFile::~UnitLogFile()
{
}

void UnitLogFile::printf(const char* fmt, ...)
{
   checkTime();
   write("  : ");

   va_list vaList;
   va_start(vaList, fmt);
   vprintf(fmt, vaList);
   va_end(vaList);
}

void UnitLogFile::printf(ICommandPosition cpi, const char* fmt, ...)
{
   checkTime();
   if(cpi != oldCommand)
   {
      oldCommand = cpi;

      if( (campData != 0) && (cpi != NoCommandPosition) )
         LogFile::printf("%s", (const char*)campData->getUnitName(cpi).toStr());
   }
   write("  ");

   va_list vaList;
   va_start(vaList, fmt);
   LogFile::vprintf(fmt, vaList);
   va_end(vaList);
}

void UnitLogFile::checkTime()
{
#if !defined(EDITOR)
   if(campData != 0)
   {
      TimeTick ticks = campData->getTick();
      if(ticks != lastTime)
      {
         lastTime = ticks;
         LogFile::printf("----- %s", campData->asciiTime());
      }
   }
#endif
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
