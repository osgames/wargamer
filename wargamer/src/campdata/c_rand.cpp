/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "c_rand.hpp"
#include "campdint.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"
#include "wldfile.hpp"
#include "savegame.hpp"


/*================ SideRandomEvent file interface ===================
 *
 */

UWORD SideRandomEvent::fileVersion = 0x0000;

Boolean SideRandomEvent::read(FileReader& f, TimeTick theTime)
{
   if(f.isAscii())
   {
      // Todo
   }
   else
   {
      UWORD version;
      f.getUWord(version);
      ASSERT(version <= fileVersion);

      ULONG time;
      f.getULong(time);
      d_shqInChaos = time+theTime;

      f.getULong(time);
      d_enemyPlansCaptured = time+theTime;
   }

  return f.isOK();
}

Boolean SideRandomEvent::write(FileWriter& f, TimeTick theTime) const
{
   if(f.isAscii())
   {
      // Todo
   }
   else
   {
      f.putUWord(fileVersion);

      // UBYTE b = static_cast<Boolean>(d_shqInChaos);
      ULONG time = ( (d_shqInChaos - theTime) > 0) ? d_shqInChaos - theTime : 0;
      f.putULong(time);

      time = ( (d_enemyPlansCaptured - theTime) > 0) ? d_enemyPlansCaptured - theTime : 0;
      f.putULong(time);
   }

  return f.isOK();
}

/*================== CampaignRandomEvents file interface ===========
 *
 */

UWORD CampaignSideRandomEvents::fileVersion = 0x0000;

Boolean CampaignSideRandomEvents::read(FileReader& f, TimeTick theTime)
{
   if(f.isAscii())
   {
      // Todo
   }
   else
   {
      if(f.getMode() == SaveGame::SaveGame)
      {
         FileChunkReader fc(f, getChunkName());
         if(!fc.isOK())
            return False;

         UWORD version;
         f.getUWord(version);
         ASSERT(version <= fileVersion);

         for(Side s = 0; s < CampaignSides; s++)
            d_events[s].read(f, theTime);
      }
   }

  return f.isOK();
}

Boolean CampaignSideRandomEvents::write(FileWriter& f, TimeTick theTime) const
{
   if(f.isAscii())
   {
      // Todo
   }
   else
   {
      if(f.getMode() == SaveGame::SaveGame)
      {
         FileChunkWriter fc(f, getChunkName());

         f.putUWord(fileVersion);

         for(Side s = 0; s < CampaignSides; s++)
            d_events[s].write(f, theTime);
      }
   }

  return f.isOK();
}

const char* CampaignSideRandomEvents::getChunkName() const
{
  return sideRandomEventsChunkName;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
