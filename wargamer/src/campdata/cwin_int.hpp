/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CWIN_INT_HPP
#define CWIN_INT_HPP

#ifndef __cplusplus
#error cwin_int.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign Window Interface
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"
#include "gamedefs.hpp"
#include "cpdef.hpp"
#ifdef CUSTOMIZE
#include "c_cond.hpp"
#endif
#include <windef.h>

class Location;
struct Date;
class CampaignMessageInfo;
class PixelPoint;

class CampaignWindowsInterface {
   public:
      virtual ~CampaignWindowsInterface() = 0;

      virtual bool windowDestroyed(HWND hwnd, int id) = 0;
      // virtual void windowDestroyed(HWND hwnd) = 0;
      // virtual void windowClosed(HWND hwnd) = 0;
      // Let CampaignWindows know that a window has had its close box pressed

      virtual bool processCommand(HWND hWnd, int id) = 0;
      // Process a Menu Item Command
      // return TRUE if processed

      virtual void showHint(int id) = 0;
      // Display Hint Line about menu item
      // virtual void clearHint() = 0;
      // Clear Hint Line
      virtual void msgWinUpdated() = 0;
      // Update Status Window's MessageWindow section

      virtual void positionWindows() = 0;
      // Position all windows within the main window

      //   virtual Boolean onClose() = 0;
      virtual bool onClose(HWND hwnd) = 0;
      // Do actions on closing window
      // Return True if it was dealt with, false for default processing

      virtual void editOptions(int page) = 0;
      // Bring up Player Options Dialog

#if !defined(EDITOR)
      virtual void orderTown(ITown itown) = 0;
      virtual void orderUnit(ConstParamCP cpi) = 0;
      // Set up Order Window for specified Town or Unit

      //   virtual void runOrderMenu(const PixelPoint& p) = 0;

      virtual int getNumCampaignMessages() = 0;
      virtual int getCampaignMessagesRead() = 0;
      // #ifdef DEBUG
      //    virtual void runVictoryScreen() = 0;
      //       // Set up Victory Window
      // #endif
#endif

      virtual void checkMenu(int id, bool flag) = 0;
      virtual void enableMenu(int id, bool flag) = 0;
      // Update Pull Down Menu and Tool bar states

#ifdef CUSTOMIZE   // used by Data Sanity Check routines
      virtual void runOBEdit(ICommandPosition cpi) = 0;
      virtual void runTownEdit(ITown iTown) = 0;
      virtual void runProvinceEdit(IProvince iProv) = 0;
      virtual void runConnectionEdit(ITown iTown) = 0;
      virtual void runConditionEdit(WG_CampaignConditions::Conditions* c) = 0;
#endif

      virtual void centerOnMap(const Location& l, UBYTE magLevel) = 0;
      // virtual void centerMapOnUnit(ConstICommandPosition cpi, const Location& l, UBYTE magLevel) = 0;
      // virtual void centerMapOnTown(ITown iTown, const Location& l, UBYTE magLevel) = 0;
      virtual void centerMapOnUnit(ConstICommandPosition cpi) = 0;
      virtual void centerMapOnTown(ITown iTown) = 0;
      virtual void redrawMap() = 0;

      /*
      * Update menu / toolbar functions
      */

      virtual void tinyMapUpdated() = 0;
      virtual void updateAll() = 0;

#if !defined(EDITOR)
      //   virtual void campaignModeUpdated() = 0;
         virtual void weatherWindowUpdated() = 0;
      virtual void messageWindowUpdated() = 0;
      virtual void reorgLeaderUpdated() = 0;
      virtual void showOrdersUpdated() = 0;
      virtual void repoSPUpdated() = 0;
      virtual void moraleBarUpdated() = 0;
#endif   // !EDITOR
      virtual void findItemUpdated() = 0;
      virtual void obWindowUpdated() = 0;


#if !defined(EDITOR)
      virtual void updateWeather() = 0;
#endif
};

inline CampaignWindowsInterface::~CampaignWindowsInterface()
{
}

#endif /* CWIN_INT_HPP */

