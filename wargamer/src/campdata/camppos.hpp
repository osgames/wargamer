/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPPOS_H
#define CAMPPOS_H

#ifndef __cplusplus
#error camppos.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Unit's Position on the Campaign
 *
 * If distanceAlong is 0 then unit is in a town
 * otherwise unit is on a connection between locations
 *
 * If direct is forwards then unit is moving from node1 to node2
 * If direct is backwards then unit is moving from node2 to node1
 *
 * The distanceAlong is always measured from node1.
 *
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.4  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1996/02/29 18:09:32  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/29 11:02:26  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/08 17:34:11  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "cdatadll.h"
#include "gamedefs.hpp"
#include "measure.hpp"

/*
 * forward references
 */

class FileReader;
class FileWriter;

/*
 * Position class
 */

class CampaignPosition {
	static const UWORD fileVersion;

	union {
		IConnection connection;		// if distanceAlong != 0
		ITown town;						// if distanceAlong == 0
	};

	IConnection d_lastConnection;   // For use if at a choke point

	Distance headAlong;
	Distance tailAlong;
	enum PosDirection { Forwards, Backwards } direct;
public:
	CAMPDATA_DLL CampaignPosition();

	// Alternate constructors and assignments

	CampaignPosition(ITown t) { setPosition(t, NoConnection); }

   friend CAMPDATA_DLL bool operator == (const CampaignPosition& lhs, const CampaignPosition& rhs);

	CAMPDATA_DLL void setPosition(ITown t, IConnection last);
	void setPosition(const CampaignPosition& pos) { *this = pos; }
	CAMPDATA_DLL void startMoving(IConnection c, ITown t);
	CAMPDATA_DLL void reverseDirection();

	Boolean atTown() const { return ( (headAlong == 0) && (tailAlong == 0) ); }
	ITown getTown() const { ASSERT(atTown()); return town; }
	IConnection getConnection() const { ASSERT(!atTown()); return connection; }

	IConnection lastConnection() const { return d_lastConnection; }
    void clearLastConnection() { d_lastConnection = NoConnection; }
	CAMPDATA_DLL Boolean movePosition(Distance d, Distance columnLength);		// Move towards destination

	CAMPDATA_DLL ITown getDestTown() const;
	CAMPDATA_DLL ITown getLastTown() const;
	CAMPDATA_DLL Distance getRemainingDistance() const;
    CAMPDATA_DLL Distance getDistanceAlong() const;

	CAMPDATA_DLL void getHeadLocation(Location& l) const;
	CAMPDATA_DLL void getTailLocation(Location& l) const;
	Distance getHeadDistance() const { return headAlong; }
	Distance getTailDistance() const { return tailAlong; }
	void setHeadDistance(Distance d) { headAlong = d; }
	void setTailDistance(Distance d) { tailAlong = d; }
	void getLocation(Location& l) const { getHeadLocation(l); }
	CAMPDATA_DLL ITown getCloseTown() const;

	CAMPDATA_DLL Boolean isConnected(ITown town) const;
	CAMPDATA_DLL Boolean isIntersecting(const CampaignPosition* target) const;

	CAMPDATA_DLL void moveTowards(const CampaignPosition* target);
	CAMPDATA_DLL void moveTowards(ITown town);

	CAMPDATA_DLL Boolean comingToTown() const;

	/*
	 * File Interface
	 */

	CAMPDATA_DLL Boolean readData(FileReader& f);
	CAMPDATA_DLL Boolean writeData(FileWriter& f) const;

	unsigned short calculateCRC(void);

#ifdef DEBUG
	Distance getHeadAlong() const { return headAlong; }
	Distance getTailAlong() const { return tailAlong; }
#endif

private:
	void getLocation(Distance distAlong, Location& l) const;

};

CAMPDATA_DLL bool operator == (const CampaignPosition& lhs, const CampaignPosition& rhs);

#endif /* CAMPPOS_H */

