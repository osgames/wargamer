/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "armistic.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"
#include "savegame.hpp"

void Armistice::startArmistice(TimeTick end)
{
   d_endArmistice = end;
//   CampaignConditionUtil::instantUpdate(
}

static const char chunkName[] = "ArmisticeChunkName";
static UWORD s_fileVersion = 0x0000;

Boolean Armistice::read(FileReader& f)
{
  if(f.getMode() == SaveGame::SaveGame)
  {
    FileChunkReader fr(f, chunkName);
    if(!fr.isOK())
      return False;

    UWORD version;
    f >> version;
    ASSERT(version <= s_fileVersion);

    f >> d_endArmistice;
  }

  return f.isOK();
}

Boolean Armistice::write(FileWriter& f) const
{
  if(f.getMode() == SaveGame::SaveGame)
  {
    FileChunkWriter fr(f, chunkName);

    f << s_fileVersion;
    f << d_endArmistice;
  }

  return f.isOK();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
