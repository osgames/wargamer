/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "camp_snd.hpp"
#include "resstr.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : CAMP_SND
        Description : Sound system for campaign game

-------------------------------------------------------------------------------------------------------------------*/

#if defined(DEBUG) && !defined(NOLOG)
#include "clog.hpp"
LogFileFlush CampSoundLog("CampSound.log");
#endif



CampaignSoundSystem::CampaignSoundSystem(SoundSystem * sound_sys) {

    if( ! sound_sys->IsSetupOK() ) { SetupOK = FALSE; return; }

    SoundSys = sound_sys;

    lpDirectSound = sound_sys->GetDirectSound();
    app_hwnd = sound_sys->GetHWND();

    lpPrimaryBuffer = NULL;
    lpFormatBuffer = NULL;

    if( ! CreatePrimaryBuffer() ) { SetupOK = FALSE; return; }

    InitialiseBuffers();

    WaitingQueueMaxSize = 8;
    PlayingListMaxSize = 8;

    InUse = FALSE;
    SetupOK = TRUE;
}





CampaignSoundSystem::~CampaignSoundSystem(void) {
if(!SetupOK) return;

    DestroyBuffers();
    DestroyPrimaryBuffer();

}


BOOL
CampaignSoundSystem::CreatePrimaryBuffer(void) {
if(!SetupOK) return FALSE;

    return TRUE;

}




BOOL
CampaignSoundSystem::DestroyPrimaryBuffer(void) {
if(!SetupOK) return FALSE;

    return TRUE;

}




BOOL
CampaignSoundSystem::CreateBuffers(char * filename, CampaignSoundInfo & snd_inf) {
if(!SetupOK) return FALSE;

    WAVEFILE WaveFile;
    LPVOID lpWaveData;
    HRESULT dsrval;
//    DSBUFFERDESC dsbd;
    DSBUFFERDESC1 dsbd;

    // load WAV file
    memset(&WaveFile, 0, sizeof(WAVEFILE) );
    lpWaveData = WAVE_LoadFile (filename, &WaveFile );
    if(lpWaveData == NULL) return FALSE;

    // fill out buffer description
//    memset(&dsbd, 0, sizeof(DSBUFFERDESC) );
    memset(&dsbd, 0, sizeof(dsbd) );
//    dsbd.dwSize = sizeof(DSBUFFERDESC);
    dsbd.dwSize = sizeof(dsbd);
//    dsbd.dwFlags = DSBCAPS_CTRLDEFAULT | DSBCAPS_STATIC ;
    dsbd.dwFlags = DSBCAPS_CTRLFREQUENCY |
                   DSBCAPS_CTRLPAN |
                   DSBCAPS_CTRLVOLUME |
//                   DSBCAPS_LOCSOFTWARE |
                   DSBCAPS_STATIC ;
    dsbd.dwBufferBytes = WaveFile.cbSize;
    dsbd.lpwfxFormat = WaveFile.pwfxInfo;

    // create initial buffer
  //  dsrval = lpDirectSound->CreateSoundBuffer(&dsbd, &snd_inf.lpDSBuffer[0], NULL);
    dsrval = lpDirectSound->CreateSoundBuffer(reinterpret_cast<DSBUFFERDESC*>(&dsbd), &snd_inf.lpDSBuffer[0], NULL);
    if(dsrval != DS_OK) { DirectSoundError(dsrval); return FALSE; }

    // copy sound data to buffer
    CopySoundToBuffer(WaveFile, lpWaveData, snd_inf.lpDSBuffer[0]);

    // copy initial buffer to duplicate buffers
    for(int f=1;f<snd_inf.num_buffers;f++) {
        dsrval = lpDirectSound->DuplicateSoundBuffer(snd_inf.lpDSBuffer[0], &snd_inf.lpDSBuffer[f]);
        if(dsrval != DS_OK) { DirectSoundError(dsrval); return FALSE; }
    }

return TRUE;
}




BOOL
CampaignSoundSystem::CopySoundToBuffer(WAVEFILE WaveFile, LPVOID lpWaveData, LPDIRECTSOUNDBUFFER lpDSbuffer) {
if(!SetupOK) return FALSE;

    HRESULT dsrval;
    LPVOID pbData = NULL;
    LPVOID pbData2 = NULL;
    DWORD dwLength;
    DWORD dwLength2;

    // Lock down the DirectSound buffer
    dsrval = lpDSbuffer->Lock(
    0,
    WaveFile.cbSize,
    &pbData,
    &dwLength,
    &pbData2,
    &dwLength2,
    NULL);

    if(dsrval != DS_OK) { DirectSoundError(dsrval); return FALSE; }

    // Copy first chunk
    memcpy(pbData, WaveFile.pbData, dwLength);

    // Copy second chunk
    if (dwLength2) memcpy(pbData2, WaveFile.pbData+dwLength , dwLength2);

    // free the WAV file data
    free(lpWaveData);

    // Unlock the buffer
    dsrval = lpDSbuffer->Unlock(pbData, dwLength, pbData2, dwLength2);
    if(dsrval != DS_OK) { DirectSoundError(dsrval); return FALSE; }

    return TRUE;
}





BOOL
CampaignSoundSystem::InitialiseBuffers(void) {
if(!SetupOK) return FALSE;

    const CampaignSoundTableEntry ** ptr = CampaignSoundTable;

    while(*ptr) {
        const CampaignSoundTableEntry * item_ptr = *ptr;
        CampaignSoundInfo snd_inf;

        // prepare the sound_info structure
        snd_inf.frequency_variation = item_ptr->frequency_variation;
        snd_inf.num_buffers = item_ptr->num_buffers;
        snd_inf.buffers_used = 0;
        for(int f=0; f<MAX_CAMPAIGN_SOUND_BUFFERS; f++) snd_inf.lpDSBuffer[f] = NULL;

        // load WAV & create buffers from it
        CreateBuffers(item_ptr->filename, snd_inf);

        // store the base frequency of this buffer
        snd_inf.lpDSBuffer[0]->SetFrequency(DSBFREQUENCY_ORIGINAL);
        snd_inf.lpDSBuffer[0]->GetFrequency(&snd_inf.base_frequency);

        // place this sound_info structure in our array (vector)
        SoundsArray.push_back(snd_inf);

        ptr++;
    }

    std::vector<CampaignSoundInfo>::iterator iter;
    int count=0;
    for(iter = SoundsArray.begin(); iter != SoundsArray.end(); iter++) {
        count++;
    }

return TRUE;
}



BOOL
CampaignSoundSystem::DestroyBuffers(void) {
if(!SetupOK) return FALSE;

    std::vector<CampaignSoundInfo>::iterator iter;
    CampaignSoundInfo * snd_inf;

    for(iter = SoundsArray.begin(); iter != SoundsArray.end(); iter++) {

        snd_inf = &*iter;

        for(int f=0; f<MAX_CAMPAIGN_SOUND_BUFFERS; f++) {

            if(snd_inf->lpDSBuffer[f] != NULL) {
                snd_inf->lpDSBuffer[f]->Stop();
                snd_inf->lpDSBuffer[f]->Release();
            }

        }


    }
return TRUE;
}





BOOL
CampaignSoundSystem::RestoreBuffers(void) {
if(!SetupOK) return FALSE;

    if(! lpPrimaryBuffer) CreatePrimaryBuffer();

    const CampaignSoundTableEntry ** ptr = CampaignSoundTable;
    int index = 0;

    while(*ptr) {
        const CampaignSoundTableEntry * item_ptr = *ptr;
        CampaignSoundInfo * snd_inf = &SoundsArray[index];

        // prepeare the structure
        snd_inf->frequency_variation = item_ptr->frequency_variation;
        snd_inf->num_buffers = item_ptr->num_buffers;
        for(int f=0; f<MAX_CAMPAIGN_SOUND_BUFFERS; f++) {
            // release any buffers that aren't destroyed already
            if(snd_inf->lpDSBuffer[f] != NULL) snd_inf->lpDSBuffer[f]->Release();
            // null out
            snd_inf->lpDSBuffer[f] = NULL;
        }

        // load WAV & create buffers from it
        CreateBuffers(item_ptr->filename, *snd_inf);

        index++;
        ptr++;
    }

return TRUE;
}




void
CampaignSoundSystem::ProcessSound(void) {
if(!SetupOK) return;

    if(InUse) return;

    InUse = TRUE;

    SoundSys->ProcessPlayList();

    ProcessPlayingList();
    ProcessWaitingQueue();

    InUse = FALSE;

}




BOOL
CampaignSoundSystem::TriggerSound(CampaignSoundTypeEnum type, CampaignSoundPriorityEnum priority) {
if(!SetupOK) return FALSE;

    if(InUse) return FALSE;

    InUse = TRUE;

    // Ensure this sound is not already in queue
    std::deque<CampaignSoundEvent>::iterator iter;

    if(! WaitingQueue.empty() ) {
        for(iter = WaitingQueue.begin(); iter != WaitingQueue.end(); iter++) {
            if( (*iter).sound_priority == priority && (*iter).sound_type == type) { InUse = FALSE; return FALSE; }
        }
    }

    // Build up a SoundEvent structure for this sound
    CampaignSoundEvent event;
    event.sound_type = type;
    event.sound_priority = priority;

    // Insert SoundEvent into WaitingQueue (sorted by priority)
    for(iter=WaitingQueue.begin(); iter != WaitingQueue.end(); iter++) {
        // insert sorted
        if(event > *iter) {
            WaitingQueue.insert(iter, event);
            // if queue has overflowed, remove the final element
            if(WaitingQueue.size() > WaitingQueueMaxSize) WaitingQueue.pop_back();
            InUse = FALSE;
            return TRUE;
        }
    }

    // If queue is full & we are trying to insert at back, then return FALSE
    if(WaitingQueue.size() == WaitingQueueMaxSize) { InUse = FALSE; return FALSE; }

    // Insert sound at end of queue
    WaitingQueue.push_back(event);
    InUse = FALSE;
    return TRUE;

}




BOOL
CampaignSoundSystem::SetSpatialProperties(CampaignPlayingSound * snd) {
if(!SetupOK) return FALSE;

    return TRUE;

}




BOOL
CampaignSoundSystem::ProcessWaitingQueue(void) {
if(!SetupOK) return FALSE;

    int i;
    BOOL sound_already_playing;
    std::vector<CampaignPlayingSound>::iterator iter;

    /*
    Remove items from front of queue which are already playing
    */

    do {

        // if queue is empty, return FALSE
        if(WaitingQueue.empty() ) return FALSE;
        // get member at front
        CampaignSoundEvent & tmpevent = WaitingQueue.front();
        sound_already_playing = FALSE;

        // Check PlayingList to see wether this instance of this sound is already playing
        for(iter=PlayingList.begin(); iter != PlayingList.end(); iter++) {
            if(
                (*iter).sound_type == tmpevent.sound_type &&
                (*iter).sound_priority == tmpevent.sound_priority) {

                    sound_already_playing = TRUE;
                    WaitingQueue.pop_front();
                    break; }
        }

    } while(sound_already_playing == TRUE);

    CampaignSoundEvent const & event = WaitingQueue.front();

    /*
    If there is space in PlayingList, fill out PlayingSound structure & add to list
    */

    if(PlayingList.size() < PlayingListMaxSize) {

        // if no buffers are free for playing this sound event, return FALSE
        if(SoundsArray[event.sound_type].buffers_used == SoundsArray[event.sound_type].num_buffers) return FALSE;

        // fill out a new PlayingSound structure
        CampaignPlayingSound sound;
        sound.sound_type = event.sound_type;
        sound.sound_priority = event.sound_priority;

        // find a free buffer
        const CampaignSoundInfo& array_entry = SoundsArray[event.sound_type];

        DWORD status;
        for(i=0; i<array_entry.num_buffers; i++) {
            SoundsArray[event.sound_type].lpDSBuffer[i]->GetStatus(&status);
            if(status != DSBSTATUS_PLAYING) break;
        }

        // have to check that this wasn't a lost buffer
        if(status == DSBSTATUS_BUFFERLOST) { RestoreBuffers(); return FALSE; }
        // otherwise increase the counter for buffers in use
        else SoundsArray[event.sound_type].buffers_used++;

        // set this as the buffer
        sound.lpDSbuffer = array_entry.lpDSBuffer[i];
        // sets the spatial properties of the sound
        SetSpatialProperties(&sound);
        // set the frequency of the sound, with a random Hz variation if defined
        int freq_variation;
        if(SoundsArray[event.sound_type].frequency_variation != 0) freq_variation =  random(0, SoundsArray[event.sound_type].frequency_variation);
        else freq_variation = 0;
        sound.lpDSbuffer->SetFrequency(SoundsArray[event.sound_type].base_frequency + freq_variation);
        // start playing the sound
        sound.lpDSbuffer->Play(0,0,0);
        // add this sound to the list
        PlayingList.push_back(sound);

        // remove this entry form the WaitingQueue
        WaitingQueue.pop_front();

        return TRUE;
    }

    /*
    If there is no space in PlayingList, overwrite the lowest priority sound with this one (if there is one ! )
    */

    else {
        // if no buffers are free for playing this sound event, return FALSE
        if(SoundsArray[event.sound_type].buffers_used == SoundsArray[event.sound_type].num_buffers) return FALSE;

        // set this at the highest initially
        int lowest_priority = CAMPAIGNSOUNDPRIORITY_INSTANT;
        CampaignPlayingSound * snd = NULL;
        std::vector<CampaignPlayingSound>::iterator index;

        // go through all entires in playing list looking for the lowest priority below the event's priority
        for(iter=PlayingList.begin(); iter != PlayingList.end(); iter++) {
            if( (*iter).sound_priority < lowest_priority) {
                lowest_priority = (*iter).sound_priority;
                index = iter;
                snd = &(*iter); }
        }

        // if index is still -1, then no entries of a lower priority were found
        if(snd == NULL) return FALSE;

        // check wether the low priority sound is still playing
        DWORD status;
        snd->lpDSbuffer->GetStatus(&status);

        if(status == DSBSTATUS_PLAYING) {
            // if still playing, stop the sound, decrease the buffer count & reset the play position
            snd->lpDSbuffer->Stop();
            snd->lpDSbuffer->SetCurrentPosition(0);
            SoundsArray[snd->sound_type].buffers_used--;
        }
        // if we've lost this buffer, restore all & return FALSE
        else if(status == DSBSTATUS_BUFFERLOST) { RestoreBuffers(); return FALSE; }

        // otherwise this buffer must have finished after the last check of the PlayingQueue !
        else {
            // decrease buffer count
            SoundsArray[snd->sound_type].buffers_used--;
            // reset the playing position justin case
            snd->lpDSbuffer->SetCurrentPosition(0);
        }

        // remove the low priority sound
        PlayingList.erase(index);

        // fill out a new PlayingSound structure
        CampaignPlayingSound sound;
        sound.sound_type = event.sound_type;
        sound.sound_priority = event.sound_priority;

        // find a free buffer
        for(i=0; i<SoundsArray[event.sound_type].num_buffers; i++) {
            SoundsArray[event.sound_type].lpDSBuffer[i]->GetStatus(&status);
            if(status != DSBSTATUS_PLAYING) break;
        }

        // have to check that this wasn't a lost buffer
        if(status == DSBSTATUS_BUFFERLOST) { RestoreBuffers(); return FALSE; }
        // otherwise increase the counter for buffers in use
        else SoundsArray[event.sound_type].buffers_used++;

        // set this as the buffer
        sound.lpDSbuffer = SoundsArray[event.sound_type].lpDSBuffer[i];
        // sets the spatial properties of the sound
        SetSpatialProperties(&sound);
        // set the frequency of the sound, with a random Hz variation if defined
        int freq_variation;
        if(SoundsArray[event.sound_type].frequency_variation != 0) freq_variation=  random(0, SoundsArray[event.sound_type].frequency_variation);
        else freq_variation = 0;
        sound.lpDSbuffer->SetFrequency(SoundsArray[event.sound_type].base_frequency + freq_variation);
        // start playing the sound
        sound.lpDSbuffer->Play(0,0,0);

        // remove this entry form the WaitingQueue
        WaitingQueue.pop_front();
        // add this sound to the playing list
        PlayingList.push_back(sound);

        return TRUE;
    }

}




BOOL
CampaignSoundSystem::ProcessPlayingList(void) {
if(!SetupOK) return FALSE;

    std::vector<CampaignPlayingSound>::iterator iter;

    for(iter=PlayingList.begin(); iter != PlayingList.end(); iter++) {

        // check sound has finished playing
        DWORD status;
        iter->lpDSbuffer->GetStatus(&status);

        if(status != DSBSTATUS_PLAYING) {
            // if we've lost this buffer, restore all & return FALSE
            if(status == DSBSTATUS_BUFFERLOST) { RestoreBuffers(); return FALSE; }
            // otherwise it has stopped playing, therefore decrease buffer count
            SoundsArray[iter->sound_type].buffers_used--;
            // reset the playing position justin case
            iter->lpDSbuffer->SetCurrentPosition(0);
            // remove the finished sound
            PlayingList.erase(iter);
            // make sure we're still indexing the correct element in the array
            iter--;
        }

        // if sound is still playing, set the spatial properties if the listening position has changed
        else {
            if(SetSpatialProperties(&*iter) == FALSE) {
                // a return of FALSE indicates this sound is now out of range & must be stopped
                iter->lpDSbuffer->Stop();
                iter->lpDSbuffer->SetCurrentPosition(0);
                SoundsArray[iter->sound_type].buffers_used--;
                PlayingList.erase(iter);
                iter--;
            }
        }

    }
return TRUE;
}



void
CampaignSoundSystem::DirectSoundError(HRESULT err) {

    const char* err_msg = NULL;

    switch(err) {
        case DSERR_ALLOCATED : err_msg = InGameText::get(IDS_SoundInUse); break;
        case DSERR_INVALIDPARAM : err_msg = InGameText::get(IDS_InvalidParameter); break;
        case DSERR_NOAGGREGATION : err_msg = InGameText::get(IDS_ObjectNoAggregation); break;
        case DSERR_NODRIVER : err_msg = InGameText::get(IDS_NoSoundDriver); break;
        case DSERR_OUTOFMEMORY : err_msg = InGameText::get(IDS_NoMemory); break;

        case DSERR_UNINITIALIZED : err_msg = InGameText::get(IDS_DSoundNotInitialized); break;
        case DSERR_UNSUPPORTED : err_msg = InGameText::get(IDS_DSound_FunctionUnSupported); break;
        default: err_msg = InGameText::get(IDS_DSoundInternalError); break;
    }

    if(err_msg != NULL)
      { MessageBox(app_hwnd, err_msg, InGameText::get(IDS_DSoundError), MB_OK); }

}















/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
