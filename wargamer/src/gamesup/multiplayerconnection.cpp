/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "MultiplayerConnection.hpp"

/*

  Filename : MultiplayerConnection.cpp

  Description : Class to hold the connection type of the game - members are static
				Connection is either None, Master or Slave
				The values are used to determine how time should be advanced in CLOGIC.CPP and other places

*/


ConnectionTypeEnum s_connectionType = ConnectionNone;	

bool s_dialogPause = false;