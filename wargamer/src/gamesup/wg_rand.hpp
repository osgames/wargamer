/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef WG_RAND_HPP
#define WG_RAND_HPP

#include "gamesup.h"

/*
 * Instance of random number generator
 */

class RandomNumber;

class GAMESUP_DLL_EXPORT CRandom {
public:
  static void seed(ULONG start);
  static ULONG getSeed();
  static int get(int range);              // get(5) can return 0,1,2,3,4
  static int get(int from, int to);       // Inclusive range, e.g. get(1,5) can return 1,2,3,4,5
  RandomNumber& object();
};



#endif
