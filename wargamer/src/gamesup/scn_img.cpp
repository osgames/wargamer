/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "scn_img.hpp"
#include "scn_res.h"
// #include "bscn_res.h"
#include "imglib.hpp"
#include "wmisc.hpp"
#include "scenario.hpp"

/*
 * One massive library of ImageLibrary
 */

class ScenarioImages {
  Boolean d_initialized;
  ImageLibrary** d_images;

  Boolean d_brInitialized;
  ImageLibrary** d_brImages;

  struct ImageInfo {
	 LPTSTR d_bmpStr;    // resource id to bmp file
	 UWORD  d_posID;     // resource id to image position data
     //---- Updated SWG: jan99, calculate size instead of relying on header file defines
	 // UWORD  d_nImages;   // number of images
	 Boolean d_needsDC;  // do we need a DrawDIBDC ?
  };


public:

  ScenarioImages() :
	 d_initialized(False),
	 d_images(0),
	 d_brInitialized(False),
	 d_brImages(0) {}

  ~ScenarioImages();

  void initImages();
  void initBRImages();

#if 0
  const ImageLibrary* getBRImages(ScenarioImageLibrary::BR_Type type)
  {
	 if(!d_brInitialized)
		initBRImages();

	 ASSERT(type < ScenarioImageLibrary::BR_HowMany);
	 return d_brImages[type];
  }
#endif

  const ImageLibrary* getImages(ScenarioImageLibrary::Type type)
  {
	 if(!d_initialized)
		initImages();

	 ASSERT(type < ScenarioImageLibrary::HowMany);
	 return d_images[type];
  }
};

ScenarioImages::~ScenarioImages()
{
  if(d_images)
  {
	 for(int i = ScenarioImageLibrary::First; i < ScenarioImageLibrary::HowMany; i++)
	 {
		if(d_images[i])
		{
		  delete d_images[i];
		}
	 }

	 delete[] d_images;
  }

  if(d_brImages)
  {
#if 0
	 for(int i = ScenarioImageLibrary::BR_First; i < ScenarioImageLibrary::BR_HowMany; i++)
	 {
		if(d_brImages[i])
		{
		  delete d_brImages[i];
		}
	 }
#endif

	 delete[] d_brImages;
  }
}

void ScenarioImages::initImages()
{
  d_images = new ImageLibrary*[ScenarioImageLibrary::HowMany];
  ASSERT(d_images);

  for(int i = ScenarioImageLibrary::First; i < ScenarioImageLibrary::HowMany; i++)
  {
	 d_images[i] = 0;
  }

#if 0
  static const ImageInfo info[ScenarioImageLibrary::HowMany] = {
	 { CheckButtonImages,                  PR_CHECKBUTTONS,     NumCheckButtonImages,   False },
	 { BuildPageImages,                    PR_BUILDPAGEICONS,   NumBuildPageImages,     False },
	 { TownInfoImages,                     PR_TOWNINFOICONS,    NumTownInfoImages,      False },
	 { SmallTownImages,                    PR_SMALLTOWNICONS,   NumSmallTownImages,     False },
	 { ToolBoxButtonImages,                PR_TOOLBOXBUTTONS,   NumToolBoxButtonImages, True  },
	 { MAKEINTRESOURCE(BM_WEATHERICONS),   PR_WEATHERICONS,     6,                      False },
	 { SpecialistImages,                   PR_SPECIALISTIMAGES, NumSpecialistImages,    False },
	 { MAKEINTRESOURCE(BM_MAPSCROLLBTNS),  PR_MAPSCROLLBTNS,    4,                      False },
	 { MAKEINTRESOURCE(BM_HMAPSCROLLBTNS), PR_MAPSCROLLBTNS,    4,                      False },
	 { MAKEINTRESOURCE(BM_CCLOCKICONS),    PR_CCLOCKICONS,      8,                      True  },
	 { CustomTabImages,                    PR_TABIMAGES,        NumTabImages,           False },
	 { ZoomImages,                         PR_ZOOMICONS,        NumZoomImages,          False },
	 { MenuImages,                         PR_MENUICONS,        NumMenuImages,          False },
	 { ToolBarImages,                      PR_TOOLBARICONS,     NumToolBarImages,       False },
	 { MAKEINTRESOURCE(BM_COMBOBUTTON),    PR_COMBOBUTTON,      1, /*NumToolBarImages,*/       False },
	 { BToolBarImages,                     PR_BTOOLBARICONS,    NumBToolBarImages,       False },
	 { BFaceImages,                        PR_BFACEIMAGES,      NumBFaceImages,       False },
  };
#else
  static const ImageInfo info[ScenarioImageLibrary::HowMany] = {
	 { CheckButtonImages,                  PR_CHECKBUTTONS,     False },
	 { BuildPageImages,                    PR_BUILDPAGEICONS,   False },
	 { TownInfoImages,                     PR_TOWNINFOICONS,    False },
	 { SmallTownImages,                    PR_SMALLTOWNICONS,   False },
	 { ToolBoxButtonImages,                PR_TOOLBOXBUTTONS,   True  },
	 { MAKEINTRESOURCE(BM_WEATHERICONS),   PR_WEATHERICONS,     False },
	 { SpecialistImages,                   PR_SPECIALISTIMAGES, False },
	 { MAKEINTRESOURCE(BM_MAPSCROLLBTNS),  PR_MAPSCROLLBTNS,    False },
	 { MAKEINTRESOURCE(BM_HMAPSCROLLBTNS), PR_MAPSCROLLBTNS,    False },
	 { MAKEINTRESOURCE(BM_CCLOCKICONS),    PR_CCLOCKICONS,      True  },
	 { CustomTabImages,                    PR_TABIMAGES,        False },
	 { ZoomImages,                         PR_ZOOMICONS,        False },
	 { MenuImages,                         PR_MENUICONS,        False },
	 { ToolBarImages,                      PR_TOOLBARICONS,     False },
	 { MAKEINTRESOURCE(BM_COMBOBUTTON),    PR_COMBOBUTTON,      False },
	 { BToolBarImages,                     PR_BTOOLBARICONS,    False },
	 { BFaceImages,                        PR_BFACEIMAGES,      False },
  };
#endif

  for(i = ScenarioImageLibrary::First; i < ScenarioImageLibrary::HowMany; i++)
  {
        HRSRC hRes = FindResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(info[i].d_posID), RT_IMAGEPOS);
        ASSERT(hRes != 0);

        HGLOBAL hGlob = LoadResource(scenario->getScenarioResDLL(), hRes);
        ASSERT(hGlob != 0);

        LPVOID data = LockResource(hGlob);
        ASSERT(data != NULL);

	    ImagePos* pos = reinterpret_cast<ImagePos*>(data);
	    ASSERT(pos != NULL);

        DWORD resourceSize = SizeofResource(scenario->getScenarioResDLL(), hRes);
        int nImages = resourceSize / sizeof(ImagePos);

        ASSERT(nImages > 0);

    	d_images[i] = new ImageLibrary(scenario->getScenarioResDLL(), info[i].d_bmpStr, nImages, pos, info[i].d_needsDC);
	    ASSERT(d_images[i] != 0);
  }

  d_initialized = True;
}

#if 0
void ScenarioImages::initBRImages()
{
  d_brImages = new ImageLibrary*[ScenarioImageLibrary::BR_HowMany];
  ASSERT(d_brImages);

  for(int i = ScenarioImageLibrary::BR_First; i < ScenarioImageLibrary::BR_HowMany; i++)
  {
	 d_brImages[i] = 0;
  }

  static const ImageInfo info[ScenarioImageLibrary::BR_HowMany] = {
	 { MAKEINTRESOURCE(BM_BR_CHECKBUTTONS),   PR_BR_CHECKBUTTONS,     NumCheckButtonImages, /*NumToolBarImages,*/       False },
	 { MAKEINTRESOURCE(BM_BR_COMBOBUTTON),    PR_BR_COMBOBUTTON,      1, /*NumToolBarImages,*/       False }
  };

  for(i = ScenarioImageLibrary::BR_First; i < ScenarioImageLibrary::BR_HowMany; i++)
  {
	 ImagePos* pos = reinterpret_cast<ImagePos*>(loadResource(scenario->getBattleResDLL(), MAKEINTRESOURCE(info[i].d_posID), RT_IMAGEPOS));
	 ASSERT(pos != NULL);

	 d_brImages[i] = new ImageLibrary(scenario->getBattleResDLL(), info[i].d_bmpStr, info[i].d_nImages, pos, info[i].d_needsDC);
	 ASSERT(d_brImages[i] != 0);

	 d_brInitialized = True;
  }
}
#endif

// path to the outside world
static ScenarioImages s_images;
const ImageLibrary* ScenarioImageLibrary::get(ScenarioImageLibrary::Type type)
{
  return s_images.getImages(type);
}

#if 0
const ImageLibrary* ScenarioImageLibrary::getBR(ScenarioImageLibrary::BR_Type type)
{
  return s_images.getBRImages(type);
}
#endif
