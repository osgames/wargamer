/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef VICTORY_HPP
#define VICTORY_HPP

#include "gamedefs.hpp"
#include "gamesup.h"

class DIB;

class GAMESUP_DLL_EXPORT GameVictory {
  public:
	 enum WhoWon {
		 Side0,
		 Side1,
		 Draw,
		 GameInProgress
	 };

	 enum VictoryLevel {
		 NoVictor,
		 Minor,
		 Major,
     Crushing

	 };

  private:
	 WhoWon d_whoWon;
	 VictoryLevel d_level;
	public:
	 GameVictory() :
		 d_whoWon(GameInProgress), d_level(NoVictor) {}

   GameVictory(Side s, VictoryLevel l) :
      d_whoWon(),
      d_level(l)
   {
      setWinner(s);
   }

   GameVictory& operator = (const GameVictory& gv)
   {
      d_whoWon = gv.d_whoWon;
      d_level = gv.d_level;
      return *this;
   }

   GameVictory(const GameVictory& gv) :
      d_whoWon(gv.d_whoWon),
      d_level(gv.d_level)
   {
   }

	void setWinner(Side side);
   void setWinner(Side s, VictoryLevel lv);

	 WhoWon whoWon() const { return d_whoWon; }
	 VictoryLevel victoryLevel() const  { return d_level; }
	 bool gameOver() const { return d_whoWon != GameInProgress; }


    static DIB* getPicture(Side winner);
      // get an appropriate picture to display

};


#endif
