/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Generic Despatcher
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "gdespat.hpp"
#include "critical.hpp"
#include "myassert.hpp"
#include <vector>

#if 0
GDespatchMessage* GDespatcher::get()
{
   LockData lock(this);
   return list.getCurrent();
}
#endif

/*
 * Called by game logic each tick
 */

void GDespatcher::process(ULONG currentTime)
{
   //swg:28July2001, Locking during entire process caused deadlock if AI and UI tried to send messages at same time.
   // Change it to lock only while pulling current message from front of queue
   // Also use stl queue and list instead of hand-rolled intrusive list

   // Make a list of items to process to avoid having to lock while processing

   if(!d_list.empty())  // quick check to avoid locking if no messages
   {
      typedef std::vector<GDespatchMessage*> ProcList;
      ProcList itemsToProcess;

      {  // scope of lock
         LockData lock(this);

         GDespatchList::iterator it(d_list.begin());
         while(it != d_list.end())
         {
            GDespatchMessage* msg = *it;
            if(currentTime >= msg->activationTime) 
            {
               // Remove from list, making sure iterator still valid
               it = d_list.erase(it);
               itemsToProcess.push_back(msg);
            }
            else
            {
               ++it;
            }
         }
      }  // Unlock

      for(ProcList::iterator it(itemsToProcess.begin());
         it != itemsToProcess.end();
         ++it)
      {
            GDespatchMessage* msg = *it;
           
            #ifdef DEBUG
            debugLog("GDespatcher::actionMessage(%s)\n", static_cast<const char*>(msg->getName().c_str()));
            #endif

            actionMessage(msg);
            *it = 0;
            delete msg;
      }
   }

#if 0 // Old version
   LockData lock(this);

   GDespatchMessage* msg;

   if( (msg = list.first()) != 0) {

      do {

         /*
         If message is due for activation
         */
         if(currentTime >= msg->activationTime) {

            #ifdef DEBUG
            debugLog("GDespatcher::actionMessage(%s)\n", static_cast<const char*>(msg->getName().c_str()));
            #endif

            actionMessage(msg);

            // NB : does removing this have any effect on our list traversal ?
            list.remove();

            if(list.getNItems() == 0) return;
         }

      } while( (msg = list.next()) != 0);
   }
#endif
}


/*
 * Send a message to despatcher
 *
 * The contents of data depends on id.
 */

void GDespatcher::sendMessage(GDespatchMessage* msg, ULONG activationTime)
{
   // ASSERT(id < DSID_Max);

   msg->activationTime = activationTime;

#ifdef DEBUG
   debugLog("GDespatcher::sendMessage(%s)\n", static_cast<const char*>(msg->getName().c_str()));
#endif

   // actionMessage(id, data);

   // GDespatchMessage* msg = new GDespatchMessage;
   // msg->id = id;
   // msg->data = data;

   LockData lock(this);
   // list.append(msg);
   d_list.push_back(msg);
}




/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
