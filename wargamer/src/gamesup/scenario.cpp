/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *      Interface to scenario specific data
 *
 * TODO:
 *              - background patterns need a neutral.
 *              - Lots of this information that is hardcoded, should be in scenario file
 *    - Nations need sorting out (Nations belong to a side)
 *
 *
 * Note that many functions are declared const even though they alter
 * members of the Scenario class.  This is because they are conceptually
 * constant, but some values are only obtained when needed.
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "scenario.hpp"
#include "config.hpp"
#include "misc.hpp"
#include "scn_res.h"
#include "bmp.hpp"
#include "dib.hpp"
#include "idstr.hpp"
#include "imglib.hpp"
#include "wmisc.hpp"
#include "app.hpp"
#include "resdef.h"
#include "options.hpp"
#include "fonts.hpp"
#include "memptr.hpp"
#include "winfile.hpp"
#include "registry.hpp"
#include "crossref.hpp"
#include "palette.hpp"

#include <stdio.h>

/*
 * Implementation Classs
 */


class /* GAMESUP_DLL_EXPORT */ Scenario_Imp : public Scenario
{
      struct CampaignSide {
         UBYTE d_value;

         CampaignSide() : d_value(0) { }
         CampaignSide(UBYTE n) : d_value(n) { }
         CampaignSide(const CampaignSide& s) : d_value(s.d_value) { }
         CampaignSide& operator = (const CampaignSide& s)
         {
            if(this != &s)
               d_value = s.d_value;
            return *this;
         }

         operator UBYTE() const { return d_value; }
         CampaignSide& operator ++ () { ++d_value; return *this; }
         void operator ++ (int) { ++d_value; }
      };

      typedef ArrayIndex IScenarioColour;

    /*
     * Public Types
     */

    public:

    /*
     * Public Functions
     */

    // private:
        Scenario_Imp(const char* name); // Can only be created using create function
        ~Scenario_Imp();
    public:
        // static Scenario_Imp* create(const char* name);      // create scenario
        // static void kill();                            // delete scenario

        int getInt(const char* keyWord);

        void lockConfig() { openConfigFile(); }
        void unlockConfig() { closeConfigFile(); }

        int getNumSides() const;
        const char* getSideName(Side side) const;
        COLORREF getSideColour(Side side) const;

        COLORREF getBorderColour(IBorderColour i) const { return borderColors.colours[i]; }
        const CustomBorderInfo& getBorderColors() const { return borderColors; }

        const char* getFolderName() const { return d_folderName; }
//        const char* getAnimationFolderName() const { return d_animationFolderName; }
//        const char* getFTIconFileName() const { return ftIconName; }
        const char* getTinyMapFileName() const { return d_tinyMapName; }
        const char* getPaletteFileName() const { return d_paletteName; }
        const char* getUnitTypeFileName() const { return d_unitTypeName; }
        const char* getTerrainTypeFileName() const { return d_terrainTypeName; }
        const char* getHelpFileName() const { return d_helpFileName; }
        const char* getHistoryFileName() const { return d_historyFileName; }
        char* makeScenarioFileName(const char* fileName, bool mustExist = true) const;

		  StringPtr getPortraitPath() const;
		  StringPtr getProvincePicturePath() const;

        HINSTANCE getScenarioResDLL() const;
//        HINSTANCE getBattleResDLL() const;

        const DrawDIB* getSideBkDIB(Side side);
        const DrawDIB* getScenarioBkDIB(BackPatterns::Value v);

        const char* getFileName() const { return d_configName; }

        /*
         *  Scenario combat tables
         */

        const Table1D<UBYTE>& getCombatDivisorTable();
        const Table1D<UWORD>& getUnitCountModifierTable();
        const Table1D<UWORD>& getBreakOutBattleTable();
        const Table1D<UWORD>& getSortieTestTable();
        const Table1D<UWORD>& getStartMoraleModifierTable();
        const Table1D<SWORD>& getBombardmentNationModifierTable();
        const Table1D<SWORD>& getBombardmentModifierTable();
        const Table2D<SWORD>& getBombardmentWeatherTable();
        const Table3D<SWORD>& getCasualtyCavalryModifierTable();
        const Table2D<SWORD>& getCasualtyNationTable();
        const Table3D<UWORD>& getLossPostureModifierTable();
        const Table1D<UWORD>& getLossModifierTable();
        const Table2D<SWORD>& getMoraleNationModifierTable();
        const Table3D<SWORD>& getMoraleDieRollModifierTable();
        const Table3D<UWORD>& getMoraleLossModifierTable();
        const Table1D<SWORD>& getLeaderHitNationModifierTable();
        const Table1D<SWORD>& getLeaderHitRankModifierTable();
        const Table1D<int>& getForceSizeTable();
        const Table1D<int>& getSizeModifierTable();
        const Table1D<UBYTE>& getDeploymentTimeTable();
        const Table1D<UBYTE>& getSupplyModifierTable();
        const Table1D<SWORD>& getCombatDieRollModifier();
        const Table1D<int>& getCasualityEffectTable();
        const Table1D<int>& getBombardmentEffectTable();
        const Table2D<Attribute>& getMoraleEffectTable();
        const Table1D<UWORD>& getAlterLeaderTable();
        const Table1D<int>& getTacticalPursuitTable();
        const Table3D<int>& getCasualtyTerrainModifierTable();
        const Table3D<int>& getCasualtyRiverTypeModifierTable();
        const Table2D<int>& getBombardmentTerrainModifierTable();
        const Table2D<UBYTE>& getLeaderHitTable();

        /*
         * Scenario Siege tables
         */
        const Table1D<UBYTE>* getActiveSiegeTableNoTrain();
        const Table1D<UBYTE>* getActiveSiegeTableTrain();
        const Table1D<UBYTE>* getSortieTableAttacker();
        const Table1D<UBYTE>* getSortieTableDefender();
        const Table1D<UBYTE>* getStormResultTable();
        const Table1D<int>* getStormLossTableAttacker();
        const Table1D<int>* getStormLossTableDefender();
        const Table1D<UBYTE>* getSallyResultTable();
        const Table1D<int>* getSallyLossTableAttacker();
        const Table1D<int>* getSallyLossTableDefender();
        const Table1D<SWORD>* getSiegeDieRollModifier();

        /*
         *  Scenario Aggression tables
         */

        const Table3D<UBYTE>& getAggressionResultTable();
        const Table2D<SWORD>& getAggressionChangeTable();

        /*
         * Attrition tables
         */

        const Table2D<int>& getAttritionTable();
        const Table2D<SWORD>& getAttritionModifierForConnectionTable();
        const Table2D<SWORD>& getAttritionModifierForTownTable();
        const Table1D<SWORD>& getAttritionModifiersTable();

        /*
         *  Delayed order tables
         */

        const Table2D<int>& getCourierSpeedAdjustmentTable();
        const Table1D<UBYTE>& getActionTimeTable();
        const Table2D<int>& getActionTimeAdjustmentTable();

        /*
         * Movement-Speed Tables
         */

        const Table2D<int>& getBasicSpeedTable();
        const Table1D<int>& getConnectQualityTable();

        /*
         * Weather Tables
         */

        const Table2D<UBYTE>& getWeatherTable();
        const Table1D<SWORD>& getConditionsAdjustmentTable();
        const Table2D<int>& getConditionToSpeedTable();

        /*
         * Nations Defecting tables
         */

        const Table1D<UBYTE>& getNationDefectsTable();
        const Table1D<UBYTE>& getNationDefectsSideTable();
        const Table1D<SWORD>& getNationDefectsModifierTable();
        const Table1D<UBYTE>& getNationDefectsEnemyNationsTable();
        const Table1D<UBYTE>& getSideDepotLimitTable();

        /*
         * Fog Of War Tables
         */

        const Table2D<UBYTE>& getSpottingEnemyTable();
        const Table1D<SWORD>&  getSpottingEnemyModifierTable();
        const Table2D<UBYTE>& getInformationQualityTable();
        const Table1D<SWORD>& getInfoQualityModifierTable();

        /*
         * Random Event Tables
         */

        const Table2D<UBYTE>& getRandomEventTable();
        const Table1D<UBYTE>& getLeaderHealthCutoffTable();

        /*
         * Town-Delay table
         */

        const Table2D<UBYTE>& getTownDelayTable();
        const Table1D<UBYTE>& getTownSizeMultiplierTable();

        /*
         * Random Leader tables
         */

        const Table2D<UBYTE>& getLeaderBaseValueTable();
        const Table1D<UBYTE>& getLeaderDecreaseAbilityTable();
        const StringTable& getSideLeaderNameTable(Side side);

        /*
         * Recover Morale tables
         */

        const Table2D<UBYTE>& getRecoverMoraleFromRestingTable();
        const Table1D<UBYTE>& getRecoverMoraleRetreatingTable();
        const Table2D<int>& getRecoverMoraleRestingNationTable();
        const Table2D<int>& getRecoverMoraleRetreatingNationTable();

        /*
         * Supply tables
         */

        const Table1D<UWORD>& getSupplyMiscTable();
        const Table1D<UWORD>& getSupplyMonthlyTable();
        const Table1D<UWORD>& getSupplyWeatherTable();

        int getNumNations() const;
        Side nationToSide(Nationality n) const;
        const char* getNationName(Nationality n) const;
        const char* getNationAdjective(Nationality n) const;
        COLORREF getNationColour(Side side) const;

        NationType getNationType(Nationality n) const;

        Nationality getGenericNation(Side s) const;
        Nationality getDefaultNation(Side s) const;
   void setAlliance(Nationality n, Side s);
        ImageLibrary* getNationFlag(Nationality n, Boolean actual);
        unsigned int getNationFlagSpriteIndex(Nationality n);

        Boolean isEnemy(Side s1, Side s2);

        unsigned int getNumCampaignMaps() const { return numMaps; }
        const char* getCampaignMapName(unsigned int i) const { ASSERT(i < numMaps); return bigMapName[i]; }

        ImageLibrary* getImageLibrary(LPCTSTR bmpResName, UWORD n, const ImagePos* pos, Boolean dc = False);

        HCURSOR getZoomCursor() const;
   HCURSOR getGloveCursor() const;
        HCURSOR getDragCursor() const;

        const MapScaleInfo* getMapScaleInfo();
                // Get Information about Map Scale size

        const char* fontName(FontStyle i) const { return fonts[i].faceName; }

        bool getColour(const char* name, COLORREF& cref) const;
                // Get Scenario Colour associated with "name"

        COLORREF getColour(const char* name) const;

		  const char* defaultPortrait(Side s) const;


    /*
     * Private Functions
     */

    private:
        void reset();           // Close everything down

        void openConfigFile() const;
        void closeConfigFile() const;

        char* getConfigFileName(const char* id, bool mustexist) const;

        void initSideBackgrounds();
        void initScenarioBackgrounds();
        void initFlagImages();
        // const char* getSideBkBitmap(Side side);

        void readColours();
        void readSides();
        void readNationTypes();
        void readNations();
        void readBorderColours();
        void readFonts();

        void addFonts();

        void findHelpFile();
        void findHistoryFile();

        IScenarioColour findColour(const char* name);
        CampaignSide findSide(const char* name);
        Nationality findNation(const char* name);
        NationType findNationType(const char* name);
        unsigned int countKeys(const char* key);

        int getRealNumSides() const;
        CampaignSide toCampaignSide(Side s) const;
        Side fromCampaignSide(CampaignSide s) const;

        void readMapInfo();

        void getLanguage();

    /*
     * Data
     */

    private:

        StringPtr d_configName;
        CString d_folderName;
        CString d_langName;
        CString d_langFolder;
//        CString d_animationFolderName;
        CString d_paletteName;
        CString d_tinyMapName;
        CString d_unitTypeName;
        CString d_terrainTypeName;
        CString d_tableDataName;
        CString d_helpFileName;
        CString d_historyFileName;
        mutable HINSTANCE d_scenarioResDLL;

        unsigned int numMaps;
        CString bigMapName[MaxCampaignMaps];

        /*
         * Side dib fillers
         */

        DrawDIB** sideBackPatterns;
        ImageLibrary** flagImages;

        DrawDIB** scnBackPatterns;

        struct ScenarioColour {
                StringPtr name;
                COLORREF colour;
        };

        struct ScenarioSide {
                StringPtr name;
                IScenarioColour colour;
					 StringPtr defPortrait;		// Default Portrait for User Interface
        };

        struct ScenarioNationType {
                StringPtr name;
        };

        struct ScenarioNation {
                StringPtr name;
                StringPtr adjective;
                IScenarioColour colour;
                CampaignSide alliance;
                NationType type;
                StringPtr bitmap;
                unsigned int SpriteIndex;
        };

        struct FontType {
                StringPtr fileName;
                StringPtr faceName;
        };

        Array<ScenarioColour> colours;
        Array<ScenarioSide> sides;
        Array<ScenarioNationType> nationTypes;
        Array<ScenarioNation> nations;
        Array<FontType> fonts;
        CustomBorderInfo borderColors;

        /*
         *  Scenario combat tables
         */

        Table1D<UBYTE> combatDivisorTable;
        Table1D<UWORD> unitCountModifierTable;
        Table1D<UWORD> breakOutBattleTable;
        Table1D<UWORD> sortieTestTable;
        Table1D<UWORD> startMoraleModifierTable;
        Table1D<SWORD> bombardmentNationModifierTable;
        Table1D<SWORD> bombardmentModifierTable;
        Table2D<SWORD> bombardmentWeatherTable;
        Table3D<SWORD> casualtyCavalryModifierTable;
        Table2D<SWORD> casualtyNationTable;
        Table3D<UWORD> lossPostureModifierTable;
        Table1D<UWORD> lossModifierTable;
        Table2D<SWORD> moraleNationModifierTable;
        Table3D<SWORD> moraleDieRollModifierTable;
        Table3D<UWORD> moraleLossModifierTable;
        Table1D<SWORD> leaderHitNationModifierTable;
        Table1D<SWORD> leaderHitRankModifierTable;
        Table1D<int> forceSizeTable;
        Table1D<int> sizeModifierTable;
        Table1D<UBYTE> deploymentTimeTable;
        Table1D<UBYTE> supplyModifierTable;
        Table1D<SWORD> combatDieRollModifier;
        Table1D<int> casualityEffectTable;
        Table1D<int> bombardmentEffectTable;
        Table2D<Attribute> moraleEffectTable;
        Table1D<UWORD> alterLeaderTable;
        Table1D<int> tacticalPursuitTable;
        Table3D<int> casualtyTerrainModifierTable;
        Table3D<int> casualtyRiverTypeModifierTable;
        Table2D<int> bombardmentTerrainModifierTable;
        Table2D<UBYTE> leaderHitTable;

        /*
         *  Scenario siege tables
         */

        Table1D<UBYTE> activeSiegeTableNoTrain;
        Table1D<UBYTE> activeSiegeTableTrain;
        Table1D<UBYTE> sortieTableAttacker;
        Table1D<UBYTE> sortieTableDefender;
        Table1D<UBYTE> stormResultTable;
        Table1D<int> stormLossTableAttacker;
        Table1D<int> stormLossTableDefender;
        Table1D<UBYTE> sallyResultTable;
        Table1D<int> sallyLossTableAttacker;
        Table1D<int> sallyLossTableDefender;
        Table1D<SWORD> siegeDieRollModifier;

        /*
         *  Scenario Aggression tables
         */

        Table3D<UBYTE> aggressionResultTable;
        Table2D<SWORD> aggressionChangeTable;

        /*
         *  Attrition tables
         */

        Table2D<int> attritionTable;
        Table2D<SWORD> attritionModifierForConnectionTable;
        Table2D<SWORD> attritionModifierForTownTable;
        Table1D<SWORD> attritionModifiersTable;

        /*
         * Delayed Order Tables
         */

        Table2D<int> courierSpeedAdjustmentTable;
        Table1D<UBYTE> actionTimeTable;
        Table2D<int> actionTimeAdjustmentTable;

        /*
         * Movement-Speed Tables
         */

        Table2D<int> basicSpeedTable;
        Table1D<int> connectQualityTable;

        /*
         * Weather Tables
         */

        Table2D<UBYTE> weatherTable;
        Table1D<SWORD>  conditionsAdjustmentTable;
        Table2D<int>   conditionToSpeedTable;

        /*
         * Fog Of War Tables
         */

        Table2D<UBYTE> spottingEnemyTable;
        Table1D<SWORD>  spottingEnemyModifierTable;
        Table2D<UBYTE> informationQualityTable;
        Table1D<SWORD> infoQualityModifierTable;

        /*
         * Random Event Tables
         */

        Table2D<UBYTE> randomEventTable;
        Table1D<UBYTE> leaderHealthCutoffTable;

        /*
         * Town-Delay tables
         */

        Table2D<UBYTE> townDelayTable;
        Table1D<UBYTE> townSizeMultiplierTable;

        /*
         * Random Leader tables
         */

        Table2D<UBYTE> leaderBaseValueTable;
        Table1D<UBYTE> leaderDecreaseAbilityTable;
        StringTable side0LeaderNameTable;
        StringTable side1LeaderNameTable;

        /*
         * Recover Morale tables
         */

        Table2D<UBYTE> recoverMoraleFromRestingTable;
        Table1D<UBYTE> recoverMoraleRetreatingTable;
        Table2D<int> recoverMoraleRestingNationTable;
        Table2D<int> recoverMoraleRetreatingNationTable;

        /*
         * Supply tables
         */

        Table1D<UWORD> supplyMiscTable;
        Table1D<UWORD> supplyMonthlyTable;
        Table1D<UWORD> supplyWeatherTable;

        /*
         * Nations Defecting tables
         */

        Table1D<UBYTE> nationDefectsTable;
        Table1D<UBYTE> nationDefectsSideTable;
        Table1D<SWORD> nationDefectsModifierTable;
        Table1D<UBYTE> nationDefectsEnemyNationsTable;
        Table1D<UBYTE> sideDepotLimitTable;

        mutable ConfigFile*     d_configFile;
        mutable int                     d_fileRefCount;

        mutable HCURSOR chZoomM;
        mutable HCURSOR chZoomC;
        mutable HCURSOR chGloveM;
        mutable HCURSOR chGloveC;
        mutable HCURSOR d_chDragM;
        mutable HCURSOR d_chDragC;

        MapScaleInfo d_mapInfo;
};


//#include "resdef.h"

/*
 * Keywords in Scenario File
 */

static const char s_CFGS_Folder[]                         = "folder";
// static const char s_CFGS_AnimationFolder[]  = "animationfolder";
static const char s_CFGS_Language[]                         = "language";
// static const char s_CFGS_FTIcon[]                         = "fticon";
static const char s_CFGS_Palette[]                        = "palette";
static const char s_CFGS_TinyMap[]                        = "tinymap";

#if defined(EDITOR)
static const char s_CFGS_CampaignMap[]            = "campaignEditMap";
#else
static const char s_CFGS_CampaignMap[]            = "campaignMap";
#endif

// static const char s_CFGS_StartScreenBMP[]   = "startScreen";
static const char s_CFGS_ScenarioResDLL[]         = "scenarioResDLL";
// static const char s_CFGS_BattleResDLL[]           = "battleResDLL";
static const char s_CFGS_HelpFile[]         = "helpFile";
static const char s_CFGS_HistoryFile[]      = "historyFile";
static const char s_CFGS_Side[]                           = "Side";
static const char s_CFGS_Colour[]                                 = "Colour";
static const char s_CFGS_Nation[]                                 = "Nation";
static const char s_CFGS_BorderColour[]     = "BorderColour";
static const char s_CFGS_NationType[]       = "NationType";
static const char s_CFGS_FontType[]         = "FontType";
static const char s_CFGS_Portrait[]                       = "Portrait";

static const char s_CFGS_MapSize[]                                = "MapSize";
static const char s_CFGS_MapScale[]                       = "MapScale";
static const char s_CFGS_MapPoint[]                       = "MapPoint";
static const char s_CFGS_MapAngle[]                       = "MapAngle";

static const char s_unitTypeFileName[] = "unittype.dat";
static const char s_terrainTypeFileName[] = "terrain.dat";
static const char s_tableDataFileName[] = "tables.dat";

/*
 *  Table chunk names
 */

static const char s_combatDivisorTableChunkName[] =        "CombatDivisorTable";
static const char s_unitCountModifierTableChunkName[] =     "UnitCountModifierTable";
static const char s_breakOutBattleTableChunkName[] =     "BreakOutBattleTable";
static const char s_sortieTestTableChunkName[] =     "SortieTestTable";
static const char s_startMoraleModifierChunkName[] =     "StartMoraleModifierTable";
static const char s_bombardmentNationModifierChunkName[] = "BombardmentNationModifierTable";
static const char s_bombardmentModifierChunkName[] = "BombardmentModifierTable";
static const char s_bombardmentWeatherChunkName[] = "BombardmentWeatherTable";
static const char s_casualtyCavalryModifierChunkName[] = "CasualtyCavalryModifierTable";
static const char s_moraleTableChunkName[] =           "MoraleEffectTable";
static const char s_moraleNationModifierChunkName[] = "MoraleNationModifierTable";
static const char s_moraleDieRollModifierChunkName[] = "MoraleDieRollModifierTable";
static const char s_moraleLossModifierChunkName[] = "MoraleLossModifierTable";
static const char s_leaderHitNationModifierChunkName[] = "LeaderHitNationModifierTable";
static const char s_leaderHitRankModifierChunkName[] = "LeaderHitRankModifierTable";
static const char s_bombardmentTableChunkName[] =      "BombardmentEffectTable";
static const char s_casualityTableChunkName[] =        "CasualityEffectTable";
static const char s_casualtyNationChunkName[] =        "CasualtyNationTable";
static const char s_lossPostureModifierChunkName[] =   "LossPostureModifierTable";
static const char s_lossModifierChunkName[] =   "LossModifierTable";
static const char s_tacticalPursuitTableChunkName[] =  "TacticalPursuitTable";
static const char s_alterLeaderChunkName[] =           "AlterLeaderTable";
static const char s_forceSizeTableChunkName[] =        "ForceSizeTable";
static const char s_casualtyTerrainModifierChunkName[] = "CasualtyTerrainModifierTable";
static const char s_casualtyRiverTypeModifierChunkName[] = "CasualtyRiverModifierTable";
static const char s_bombardmentTerrainModifierChunkName[] = "BombardmentTerrainModifierTable";
static const char s_activeSiegeNoTrainChunkName[] =    "ActiveSiegeTableNoTrain";
static const char s_activeSiegeTrainChunkName[] =      "ActiveSiegeTableTrain";
static const char s_sortieAttackerChunkName[] =        "SortieTableAttacker";
static const char s_sortieDefenderChunkName[] =        "SortieTableDefender";
static const char s_stormResultChunkName[] =           "StormResultTable";
static const char s_stormLossAttackerChunkName[] =     "StormLossTableAttacker";
static const char s_stormLossDefenderChunkName[] =     "StormLossTableDefender";
static const char s_sallyResultChunkName[] =           "SallyResultTable";
static const char s_sallyLossAttackerChunkName[] =     "SallyLossTableAttacker";
static const char s_sallyLossDefenderChunkName[] =     "SallyLossTableDefender";
static const char s_siegeDieRollModifierChunkName[] =  "SiegeDieRollModifierTable";
static const char s_sizeModifierChunkName[] =          "SizeModifierTable";
static const char s_deploymentTimeChunkName[] =        "DeploymentTimeTable";
static const char s_supplyModifierChunkName[] =        "SupplyModifierTable";
static const char s_combatDieRollChunkName[] =         "CombatDieRollModifierTable";
static const char s_aggressionResultChunkName[] =      "AggressionResultTable";
static const char s_aggressionChangeChunkName[] =      "AggressionChangeTable";
static const char s_attritionChunkName[] =             "AttritionTable";
static const char s_courierSpeedChunkName[] =          "CourierSpeedAdjustmentTable";
static const char s_actionTimeChunkName[] =            "ActionTimeTable";
static const char s_actionTimeAdjustmentChunkName[] =  "ActionTimeAdjustmentTable";
static const char s_basicSpeedChunkName[] =            "BasicSpeedTable";
static const char s_connectQualityChunkName[] =        "ConnectionQualityTable";
static const char s_weatherTableChunkName[] =          "WeatherTable";
static const char s_conditionsAdjustmentChunkName[] =  "ConditionsAdjustmentTable";
static const char s_conditionToSpeedChunkName[] =      "ConditionToSpeedTable";
static const char s_spottingEnemyChunkName[] =         "SpottingEnemyTable";
static const char s_spottingEnemyModifierChunkName[] = "SpottingEnemyModifierTable";
static const char s_informationQualityChunkName[] =    "InformationQualityTable";
static const char s_infoQualityModifierChunkName[] =   "InfoQualityModifierTable";
static const char s_randomEventChunkName[] =           "RandomEventTable";
static const char s_townDelayChunkName[] =             "TownDelayTable";
static const char s_townSizeMultiplierChunkName[] =    "TownSizeMultiplierTable";
static const char s_leaderBaseValueChunkName[] =       "LeaderBaseValueTable";
static const char s_leaderHealthCutoffChunkName[] =    "LeaderHealthCutoffTable";
static const char s_side0LeaderNameChunkName[] =       "Side0LeaderNameTable";
static const char s_side1LeaderNameChunkName[] =       "Side1LeaderNameTable";
static const char s_attritionModifierForConnectionChunkName[] = "AttritionModifierForConnectionTable";
static const char s_attritionModifierForTownChunkName[] =       "AttritionModifierForTownTable";
static const char s_attritionModifiersChunkName[] =             "AttritionModifiersTable";
static const char s_leaderHitTableChunkName[] =                 "LeaderHitTable";
static const char s_recoverMoraleFromRestingChunkName[] =       "RecoverMoraleFromRestingTable";
static const char s_recoverMoraleRetreatingChunkName[] =        "RecoverMoraleRetreatingTable";
static const char s_recoverMoraleRestingNationChunkName[] =     "RecoverMoraleRestingNationTable";
static const char s_recoverMoraleRetreatingNationChunkName[] =  "RecoverMoraleRetreatingNationTable";
static const char s_supplyMiscTableChunkName[] =  "SupplyMiscModifierTable";
static const char s_supplyMonthlyTableChunkName[] =  "SupplyMonthlyModifierTable";
static const char s_supplyWeatherTableChunkName[] =  "SupplyWeatherModifierTable";
static const char s_leaderDecreaseAbilityChunkName[] = "LeaderDecreaseAbilityTable";
static const char s_nationDefectsChunkName[] = "NationDefectsTable";
static const char s_nationDefectsSideChunkName[] = "NationDefectsSideTable";
static const char s_nationDefectsModifierChunkName[] = "NationDefectsModifierTable";
static const char s_nationDefectsEnemyNationsChunkName[] = "NationDefectsEnemyNationsTable";
static const char s_sideDepotLimitChunkName[] = "SideDepotLimitTable";

static const char s_cdRegistryPath[] = "";
static const char s_cdRegistryValue[] = "CD_Path";


static Palette::PaletteID s_sideBackPaletteID = 0;
static Palette::PaletteID s_flagPaletteID = 0;
static Palette::PaletteID s_scnBackPaletteID = 0;


#if 0
static Scenario_Imp* Scenario_Imp::create(const char* name)
{
   kill();

   ASSERT(scenario == 0);
   scenario = new Scenario(name);
   ASSERT(scenario != 0);

   return scenario;
}

static void Scenario_Imp::kill()
{
   delete scenario;
   scenario = 0;
}
#endif

/*
 * Constructor
 *
 * We'll probably want to pass this a name
 */

Scenario_Imp::Scenario_Imp(const char* name) :
        d_configName(),
        d_folderName(),
        d_langName(),
        d_langFolder(),
//        d_animationFolderName(),
//        ftIconName(),
        d_paletteName(),
        d_tinyMapName(),
        d_unitTypeName(),
        d_terrainTypeName(),
        d_tableDataName(),
//        startScreenBMP(),
        d_helpFileName(),
        d_historyFileName(),
        d_scenarioResDLL(NULL),
//        batResDLL(NULL),
        numMaps(0),
        sideBackPatterns(0),
        flagImages(0),
        // d_sideBackPaletteID(0),
        // d_flagPaletteID(0),
        scnBackPatterns(0),
        // d_scnBackPaletteID(0),
        d_configFile(0),
        d_fileRefCount(0),
        chZoomM(NULL),
        chZoomC(NULL),
        chGloveM(NULL),
        chGloveC(NULL),
        d_chDragM(NULL),
        d_chDragC(NULL)
{
   ASSERT(name != 0);

#ifdef DEBUG
   debugLog("Initialising Scenario: %s\n", name);
#endif

   d_configName = name;

   /*
    * Read info from Config File
    */

   openConfigFile();

   try
   {

      ASSERT(d_configFile->isOK());

      d_folderName = d_configFile->get(s_CFGS_Folder);
      if(d_folderName == 0)
         throw ScenarioFileException("Scenario Folder Name not defined in %s", (const char*)d_configName);

      if(!FileSystem::dirExists(d_folderName))
         throw ScenarioFileException("Scenario Folder %s is missing", (const char*)d_folderName);

      // get language must be done AFTER the scenario folder

      getLanguage();

//      d_animationFolderName = d_configFile->get(s_CFGS_AnimationFolder);
//      if(d_animationFolderName == 0)
//         throw ScenarioFileException("Scenario Animation Folder Name not defined in %s", (const char*)d_configName);

//      ftIconName = getConfigFileName(s_CFGS_FTIcon);
      d_paletteName = getConfigFileName(s_CFGS_Palette, true);
      d_tinyMapName = getConfigFileName(s_CFGS_TinyMap, true);
//      startScreenBMP = getConfigFileName(s_CFGS_StartScreenBMP);

      d_unitTypeName = makeScenarioFileName(s_unitTypeFileName);
      d_terrainTypeName = makeScenarioFileName(s_terrainTypeFileName);
      d_tableDataName = makeScenarioFileName(s_tableDataFileName);

      // scenarioResDLLName = getConfigFileName(s_CFGS_ScenarioResDLL);
      // battleResDLLName = getConfigFileName(s_CFGS_BattleResDLL);

      findHelpFile();
      findHistoryFile();

      d_configFile->startScan(s_CFGS_CampaignMap);
      while(numMaps < MaxCampaignMaps)
      {
         char* fname = d_configFile->nextScan();
         if(fname)
         {
            bigMapName[numMaps++] = makeScenarioFileName(fname);
            delete[] fname;
         }
         else
            break;
      }
      d_configFile->endScan();

      readColours();
      readSides();
      readNationTypes();
      readNations();
      readBorderColours();
      readFonts();
      addFonts();

      readMapInfo();
   }
   catch(...)
   {
      reset();
      throw;
   }

   closeConfigFile();
}

/*
 * Destructor
 */

Scenario_Imp::~Scenario_Imp()
{
   reset();
}

void Scenario_Imp::reset()
{
   ASSERT(d_fileRefCount == 0);
   ASSERT(d_configFile == 0);

   if(d_configFile != 0)
   {
      delete d_configFile;
      d_fileRefCount = 0;
   }

   // Note all strings get deleted by their own destructors

   if(sideBackPatterns != 0)
   {
      for(int s = 0; s < getRealNumSides(); s++)
      {
         if(sideBackPatterns[s] != 0)
         {
            delete sideBackPatterns[s];
            sideBackPatterns[s] = 0;
         }
      }
      delete[] sideBackPatterns;
   }

   if(scnBackPatterns != 0)
   {
      for(int i = 0; i < BackPatterns::HowMany; i++)
      {
         if(scnBackPatterns[i] != 0)
         {
            delete scnBackPatterns[i];
            scnBackPatterns[i] = 0;
         }
      }
      delete[] scnBackPatterns;
   }

   if(flagImages != 0)
   {
      for(int n = 0; n < getNumNations(); n++)
      {
         if(flagImages[n] != 0)
         {
            delete flagImages[n];
            flagImages[n] = 0;
         }
      }
      delete[] flagImages;
   }
}

void Scenario_Imp::openConfigFile() const
{
   ASSERT(d_fileRefCount >= 0);

   if(d_fileRefCount++ == 0)
   {
      ASSERT(d_configFile == 0);
      d_configFile = new ConfigFile(d_configName, d_langName);
   }

   ASSERT(d_configFile != 0);

   if(!d_configFile->isOK())
      throw ScenarioFileException("Error opening Scenario File %s", (const char*) d_configName);
}


void Scenario_Imp::closeConfigFile() const
{
   ASSERT(d_fileRefCount > 0);

   if(--d_fileRefCount == 0)
   {
      ASSERT(d_configFile != 0);
      if(d_configFile != 0)
      {
         delete d_configFile;
         d_configFile = 0;
      }
   }

   ASSERT(d_fileRefCount >= 0);
}

int Scenario_Imp::getInt(const char* keyword)
{
   openConfigFile();

   int value;
   bool result = d_configFile->getNum(keyword, value);

   closeConfigFile();

   if(!result)
      throw KeywordNotFound(d_configName, keyword);

   return value;
}


/*
 * Get information about the sides
 */

int Scenario_Imp::getNumSides() const
{
   ASSERT(sides.entries() > 0);

   return sides.entries() - 1;
}

int Scenario_Imp::getRealNumSides() const
{
   ASSERT(sides.entries() > 0);

   return sides.entries();
}


const char* Scenario_Imp::getSideName(Side side) const
{
   ASSERT( (side == SIDE_Neutral) || (side < getNumSides()));

   CampaignSide cSide = toCampaignSide(side);

   if(cSide < getRealNumSides())
      return sides[cSide].name;
   else
      return "Illegal";

}

// void Scenario_Imp::getSideColour(Side side, RGBTRIPLE& rgb) const
COLORREF Scenario_Imp::getSideColour(Side side) const
{
   ASSERT( (side == SIDE_Neutral) || (side < getNumSides()));

   CampaignSide cSide = toCampaignSide(side);

   return colours[sides[cSide].colour].colour;
}


const char* Scenario_Imp::defaultPortrait(Side side) const
{
   ASSERT(side < getNumSides());

   CampaignSide cSide = toCampaignSide(side);
   return sides[cSide].defPortrait;
}



void Scenario_Imp::initSideBackgrounds()
{
   static const int sideBkBitmap[] = {
                BM_BK_NEUTRAL,
                BM_BK_SIDE1,
                BM_BK_SIDE2
        };

   ASSERT(getRealNumSides() == 3);

   delete[] sideBackPatterns;

   sideBackPatterns = new DrawDIB*[getRealNumSides()];
   {
      for(Side s = 0; s < getRealNumSides(); s++)
         sideBackPatterns[s] = 0;
   }

   ASSERT(sideBackPatterns != 0);
   {
      for(Side s = 0; s < getRealNumSides(); s++)
      {
         sideBackPatterns[s] = BMP::newDrawDIB(getScenarioResDLL(), MAKEINTRESOURCE(sideBkBitmap[s]), BMP::RBMP_Normal);
         ASSERT(sideBackPatterns[s] != 0);
      }
   }

   s_sideBackPaletteID = Palette::paletteID();
}

void Scenario_Imp::initScenarioBackgrounds()
{
   static const int scenarioBkBitmap[BackPatterns::HowMany] = {
                BM_TABBACKGROUND,               // PaperBackground
                BM_MENUBARBK,                   // MenuBarBackground
                BM_HSCROLLBK,                   // HScrollBackground
                BM_VSCROLLBK,                   // VScrollBackground
                BM_HTHUMBBK,                    // HScrollThumbBackground
                BM_VTHUMBBK,                    // VScrollThumbBackground
                BM_BARCHARTBK,                  // BarChartBackground
                BM_BARCHARTFILLER,      // BarChartForeground
                BM_CHERRYWOOD,                  // CherryWoodBackground
                BM_TABBACKGROUND,               // ButtonBackground
        };

   delete[] scnBackPatterns;

   scnBackPatterns = new DrawDIB*[BackPatterns::HowMany];
   {
      for(int i = 0; i < BackPatterns::HowMany; i++)
         scnBackPatterns[i] = 0;
   }

   ASSERT(scnBackPatterns != 0);
   {
      for(int i = 0; i < BackPatterns::HowMany; i++)
      {
         scnBackPatterns[i] = BMP::newDrawDIB(getScenarioResDLL(), MAKEINTRESOURCE(scenarioBkBitmap[i]), BMP::RBMP_Normal);
         ASSERT(scnBackPatterns[i] != 0);
      }
   }

   s_scnBackPaletteID = Palette::paletteID();
}

void Scenario_Imp::initFlagImages()
{
   const int nUnitImages = 7;

   static ImagePos* unitPos = 0;
   if(!unitPos)
   {
      unitPos = (ImagePos*)loadResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(PR_UNITICONS), RT_IMAGEPOS);
      ASSERT(unitPos != NULL);
   }

   delete[] flagImages;

   flagImages = new ImageLibrary*[getNumNations()];
   // flagImages = getImageLibrary*[getNumNations()];
   ASSERT(flagImages != 0);


   for(Nationality i = 0; i < scenario->getNumNations(); i++)
   {
      // flagImages[i] = new ImageLibrary(nations[i].bitmap, nUnitImages, unitPos, True);
      flagImages[i] = getImageLibrary(nations[i].bitmap, nUnitImages, unitPos, True);
      ASSERT(flagImages[i] != 0);
   }

   s_flagPaletteID = Palette::paletteID();
}

const DrawDIB* Scenario_Imp::getSideBkDIB(Side side)
{
   ASSERT( (side == SIDE_Neutral) || (side < getNumSides()));

   CampaignSide cSide = toCampaignSide(side);

   if( (sideBackPatterns == 0) || (s_sideBackPaletteID != Palette::paletteID()))
      initSideBackgrounds();
   ASSERT(sideBackPatterns != 0);

   return sideBackPatterns[cSide];
}

const DrawDIB* Scenario_Imp::getScenarioBkDIB(Scenario_Imp::BackPatterns::Value v)
{
   ASSERT(v < BackPatterns::HowMany);

   if( (scnBackPatterns == 0) || (s_scnBackPaletteID != Palette::paletteID()))
      initScenarioBackgrounds();
   ASSERT(scnBackPatterns != 0);

   return scnBackPatterns[v];
}

/*
 * Extract a filename from the Scenario File
 * and append it to the scenario path
 *
 * Updated: March 14th to check existence of file
 * and automatically search in CD path as well.
 *
 * Really this should be even cleverer
 * If path starts with '\' then it should not append folderName
 * Then this can be used for any filename that is asked for.
 */

char* Scenario_Imp::getConfigFileName(const char* id, bool mustExist) const
{
   ASSERT(id != 0);
   ASSERT(d_configName != 0);
   ASSERT(d_folderName != 0);

   openConfigFile();
   CString name = d_configFile->get(id);
   closeConfigFile();

   if(name == 0)
      throw ScenarioFileException("Missing %s in Scenario file %s", (const char*) id, (const char*) d_configName);

   return makeScenarioFileName(name, mustExist);
}


/*
 * Make a filename by appending folderName
 * search for file on hard disk and CD
 * If fileName begins with a '\\' then it is relative to current directory
 * otherwise it is scenario directory.
 */

char* Scenario_Imp::makeScenarioFileName(const char* name, bool mustExist) const
{
   char fullName[MAX_PATH];

   // Skip '\'

   if(name[0] == '\\')
      ++name;

//   if(mustExist)
//   {
      // First try language file

      if(d_langFolder != 0)
      {
         wsprintf(fullName, "%s\\%s\\%s",
               (const char*) d_folderName,
               (const char*) d_langFolder,
               (const char*) name);

         if(FileSystem::fileExists(fullName))
            return copyString(fullName);
      }

      // Try scenario folder

      wsprintf(fullName, "%s\\%s",
            (const char*) d_folderName,
            (const char*) name);

      if(FileSystem::fileExists(fullName))
         return copyString(fullName);

      // try default folder

      if(FileSystem::fileExists(name))
         return copyString(name);

      /*
       * If it hasn't been found, and it doesn't need to exist
       * then return the language folder
       */

      if(!mustExist)
      {
         if (d_folderName != 0)
         {
            wsprintf(fullName, "%s\\%s\\%s",
                  (const char*) d_folderName,
                  (const char*) d_langFolder,
                  (const char*) name);
         }
         else
         {
            wsprintf(fullName, "%s\\%s",
                  (const char*) d_folderName,
                  (const char*) name);
         }


         return copyString(fullName);
      }



//   }
//   else
//   {
//      wsprintf(fullName, "%s\\%s",
//            (const char*) d_folderName,
//            (const char*) name);
//
//      return copyString(fullName);
//   }

   throw ScenarioFileException("%s can not found on hard disc or CD", name);
}

#if 0
// Old stuff to search CD

   if(name[0] != '\\')
      wsprintf(fullName, "%s\\%s", (const char*) d_folderName, (const char*) name);
   else
      lstrcpy(fullName, &name[1]);

   if(mustExist && !FileSystem::fileExists(fullName))
   {
      char cdPath[MAX_PATH];

      if( (!getRegistryString(cdRegistryValue, cdPath, cdRegistryPath)) ||
         (cdPath[0] == '\0') )
      {
         throw ScenarioFileException("%s not found, and CD path not in registry", fullName);
      }

      char* s = cdPath;
      char c;
      do
      {
         c = *s++;
      } while(*s != '\0');

      if(c != '\\')
         *s++ = '\\';

      lstrcpy(s, fullName);

      if(!FileSystem::fileExists(cdPath))
         throw ScenarioFileException("%s can not found on hard disc or CD", fullName);

      return copyString(cdPath);
   }

   return copyString(fullName);
}
#endif



StringPtr Scenario_Imp::getPortraitPath() const
{
    char fullName[MAX_PATH];
    wsprintf(fullName, "%s\\Portraits", (const char*)d_folderName);
    return fullName;
//   return makeScenarioFileName("Portraits", false);
}

StringPtr Scenario_Imp::getProvincePicturePath() const
{
    char fullName[MAX_PATH];
    wsprintf(fullName, "%s\\Provinces", (const char*)d_folderName);
    return fullName;
//   return makeScenarioFileName("Provinces", false);
}



/*
 * Interface for Nationality
 */

int Scenario_Imp::getNumNations() const
{
   return nations.entries();
}


Side Scenario_Imp::nationToSide(Nationality n) const
{
   ASSERT(n < getNumNations());

   return fromCampaignSide(nations[n].alliance);
}

void Scenario_Imp::setAlliance(Nationality n, Side s)
{
   ASSERT(n < getNumNations());
   ASSERT((s == SIDE_Neutral) || (s < getNumSides()));

   nations[n].alliance = toCampaignSide(s);
}

const char* Scenario_Imp::getNationName(Nationality n) const
{
   ASSERT(n < getNumNations());

   // return nationNames[n];
   return nations[n].name;
}

const char* Scenario_Imp::getNationAdjective(Nationality n) const
{
   ASSERT(n < getNumNations());

   // return nationNames[n];
   return nations[n].adjective;
}


COLORREF Scenario_Imp::getNationColour(Nationality n) const
{
   ASSERT(n < getNumNations());

   // return nationColours[n];
   return colours[nations[n].colour].colour;
}

NationType Scenario_Imp::getNationType(Nationality n) const
{
   ASSERT(n < getNumNations());

   return nations[n].type;
}

/*
 * Get ImageList for unit bitmap. If actual is false
 * return default nations bitmap.
 */

ImageLibrary* Scenario_Imp::getNationFlag(Nationality n, Boolean actual)
{
   ASSERT(n < getNumNations());

   if( (flagImages == 0) || (s_flagPaletteID != Palette::paletteID()))
      initFlagImages();

   ASSERT(flagImages != 0);

   if(!actual)
   {
      Side s = fromCampaignSide(nations[n].alliance);
      Nationality dNation = getDefaultNation(s);
      if(dNation < getNumNations())
         return flagImages[dNation];
   }

   return flagImages[n];
}


/*
Get offset into battle-game TROOPS.SPR file of nation's animated flag
*/

unsigned int Scenario_Imp::getNationFlagSpriteIndex(Nationality n)
{
   ASSERT(n < getNumNations());

   return nations[n].SpriteIndex;
}


/*
 * Find out if 2 sides are enemies.
 * In future scenarios with more than 2 sides, this may be more complex!
 */

Boolean Scenario_Imp::isEnemy(Side s1, Side s2)
{
   if(s1 == SIDE_Neutral)
      return False;

   if(s2 == SIDE_Neutral)
      return False;

   return s1 != s2;
}

Nationality Scenario_Imp::getGenericNation(Side s) const
{
   if(s == SIDE_Neutral)
      return NATION_Neutral;

   CampaignSide side = toCampaignSide(s);

   for(Nationality i = 0; i < getNumNations(); i++)
   {
      if(side == nations[i].alliance && nations[i].type == GenericNation)
         return i;
   }

   FORCEASSERT("No Generic Nation found for side!");

   // Bodge... if no nations left then pick any nation on our side

   for(i = 0; i < getNumNations(); i++)
   {
      if(side == nations[i].alliance)
         return i;
   }

   FORCEASSERT("No Nation at all found for side!");

   return NATION_Neutral;

//   throw ScenarioFileException("Undefined side %d", (int)s);
}

Nationality Scenario_Imp::getDefaultNation(Side s) const
{
   if(s == SIDE_Neutral)
      return NATION_Neutral;

   CampaignSide side = toCampaignSide(s);

   for(Nationality i = 0; i < getNumNations(); i++)
   {
      if(side == nations[i].alliance && nations[i].type == MajorNation)
         return i;
   }

   FORCEASSERT("No Major Nation found for side!");

   // Bodge... if no nations left then pick any nation on our side

   for(i = 0; i < getNumNations(); i++)
   {
      if(side == nations[i].alliance)
         return i;
   }

   FORCEASSERT("No Nation at all found for side!");

   return NATION_Neutral;

//   throw ScenarioFileException("Undefined side %d", (int)s);
}

Scenario_Imp::IScenarioColour Scenario_Imp::findColour(const char* name)
{
   ASSERT(name != 0);

   for(IScenarioColour i = 0; i < colours.entries(); i++)
   {
      if(strcmp(name, colours[i].name) == 0)
         return i;
   }

   throw ScenarioFileException("Undefined Colour %s", (const char*) name);
}

bool Scenario_Imp::getColour(const char* name, COLORREF& color) const
{
   ASSERT(name != 0);

   for(IScenarioColour i = 0; i < colours.entries(); i++)
   {
      if(stricmp(name, colours[i].name) == 0)
      {
         color = colours[i].colour;
         return true;
      }
   }

   return false;
}

COLORREF Scenario_Imp::getColour(const char* name) const
{
   COLORREF color;
   if(getColour(name, color))
      return color;

   throw MissingColor(name);
}

Scenario_Imp::CampaignSide Scenario_Imp::findSide(const char* name)
{
   ASSERT(name != 0);

   for(CampaignSide i = 0; i < sides.entries(); i++)
   {
      if(strcmp(name, sides[i].name) == 0)
         return i;
   }

   throw ScenarioFileException("Undefined Side %s", (const char*) name);
}

NationType Scenario_Imp::findNationType(const char* name)
{
   ASSERT(name != 0);
   for(NationType i = 0; i < nationTypes.entries(); i++)
   {
      if(strcmp(name, nationTypes[i].name) == 0)
         return i;
   }

   throw ScenarioFileException("Undefined NationType %s", (const char*) name);
}

Nationality Scenario_Imp::findNation(const char* name)
{
   ASSERT(name != 0);

   for(Nationality i = 0; i < nations.entries(); i++)
   {
      if(strcmp(name, nations[i].name) == 0)
         return i;
   }

   throw ScenarioFileException("Undefined Nation %s", (const char*) name);
}


/*
 * Count how many entries of a particular key there are
 */

unsigned int Scenario_Imp::countKeys(const char* key)
{
   unsigned int count = 0;

   d_configFile->startScan(key);
   for(;;)
   {
      char* fname = d_configFile->nextScan();
      if(fname == 0)
         break;
      else
      {
         delete[] fname;
         count++;
      }
   }
   d_configFile->endScan();

   return count;
}

class ScenarioParse : public StringParse {
   char* buffer;
   public:
   ScenarioParse(char* s) : StringParse(s) { buffer = s;
   }
   ~ScenarioParse() { delete[] buffer;
   }
};

/*
 * Read in the sides
 */

void Scenario_Imp::readSides()
{
   unsigned int nSides = countKeys(s_CFGS_Side);

   ASSERT(nSides != 0);
   ASSERT(nSides < Side_MAX);

   sides.init(CampaignSide(static_cast<UBYTE>(nSides)));

   d_configFile->startScan(s_CFGS_Side);
   for(CampaignSide i = 0; i < nSides; i++)
   {
      ScenarioParse parse = d_configFile->nextScan();

      ScenarioSide& sd = sides[i];

      sd.name = parse.getToken();
      sd.colour = findColour(parse.getToken());

#ifdef DEBUG
      debugLog("Side %d = %s %d\n", (int) i, (const char*) sd.name, (int) sd.colour);
#endif
   }

   ASSERT(d_configFile->nextScan() == 0);

   d_configFile->endScan();

   /*
   * Now scan for portraits
   */

   d_configFile->startScan(s_CFGS_Portrait);


   for(;;)
   {
      char* key = d_configFile->nextScan();
      if(key)
      {
         ScenarioParse parse = key;

         // Read side

         CampaignSide cSide = findSide(parse.getToken());

         // read portrait name

         ASSERT(sides[cSide].defPortrait.toStr() == 0);
         sides[cSide].defPortrait = parse.getToken();

      }
      else
         break;
   }
   d_configFile->endScan();

}

void Scenario_Imp::readNationTypes()
{
   unsigned int nNationType = countKeys(s_CFGS_NationType);

   ASSERT(nNationType != 0);
   ASSERT(nNationType <= NationType_MAX);

   nationTypes.init(NationType(nNationType));

   d_configFile->startScan(s_CFGS_NationType);
   for(Nationality i = 0; i < nNationType; i++)
   {
      ScenarioParse parse = d_configFile->nextScan();

      ScenarioNationType& sd = nationTypes[i];

      sd.name = parse.getToken();

#ifdef DEBUG
      debugLog("NationType %d= %s\n", (int) i, (const char*) sd.name);
#endif
   }

   ASSERT(d_configFile->nextScan() == 0);

   d_configFile->endScan();
}

void Scenario_Imp::readNations()
{
   CrossReferencer d_TroopFileCrossRef("nap1813\\g_troops.dat");
   d_TroopFileCrossRef.DefineSeperator(" = ");

   unsigned int nNation = countKeys(s_CFGS_Nation);

   ASSERT(nNation != 0);
   ASSERT(nNation <= Nationality_MAX);

   nations.init(Nationality(nNation));

   d_configFile->startScan(s_CFGS_Nation);
   for(Nationality i = 0; i < nNation; i++)
   {
      ScenarioParse parse = d_configFile->nextScan();

      ScenarioNation& sd = nations[i];

      sd.name = parse.getToken();
      sd.adjective = parse.getToken();
      sd.alliance = findSide(parse.getToken());
      sd.colour = findColour(parse.getToken());
      sd.type = findNationType(parse.getToken());
      sd.bitmap = parse.getToken();

      char * FlagGraphicName;
      FlagGraphicName = parse.getToken();

      char * ref_found;
      // find entry corresponding to name
      int value;
      ref_found = d_TroopFileCrossRef.GetIntegerEntry(FlagGraphicName, &value, 0);
      if(ref_found) sd.SpriteIndex = value;
      else {
         FORCEASSERT("Oops - Can't cross-reference nation's flag name with sprite file");
         sd.SpriteIndex = 0;
      }


#ifdef DEBUG
      debugLog("Nation %d= %s %d %d\n", (int) i, (const char*) sd.name, (int) sd.alliance, (int) sd.colour);
#endif
   }

   ASSERT(d_configFile->nextScan() == 0);

   d_configFile->endScan();
}

void Scenario_Imp::readColours()
{
   unsigned int nCols = countKeys(s_CFGS_Colour);

   ASSERT(nCols != 0);
   ASSERT(nCols < ArrayIndex_MAX);

   colours.init(IScenarioColour(nCols));

   d_configFile->startScan(s_CFGS_Colour);
   for(IScenarioColour i = 0; i < nCols; i++)
   {
      ScenarioParse parse = d_configFile->nextScan();

      ScenarioColour& sd = colours[i];

      sd.name = parse.getToken();

      unsigned int r;
      unsigned int g;
      unsigned int b;

      parse.getInt(r);
      parse.getInt(g);
      parse.getInt(b);

      sd.colour = PALETTERGB(r, g, b);

#ifdef DEBUG
      debugLog("Colour %d = %s, %d,%d,%d\n",
         (int) i,
         (const char*) sd.name,
         (int) r,
         (int) g,
         (int) b);
#endif
   }

   ASSERT(d_configFile->nextScan() == 0);

   d_configFile->endScan();
}

void Scenario_Imp::readBorderColours()
{
   unsigned int nBorderColours = countKeys(s_CFGS_BorderColour);

   ASSERT(nBorderColours != 0);
   ASSERT(nBorderColours == CustomBorderInfo::HowMany);
   // ASSERT(nBorderColours < Side_MAX);

   // borderColours.init(IBorderColour(nBorderColours));

   d_configFile->startScan(s_CFGS_BorderColour);
   for(IBorderColour i = 0; i < nBorderColours; i++)
   {
      ScenarioParse parse = d_configFile->nextScan();

#if 0
         BorderColour& bc = borderColours[i];
      bc.colour = findColour(parse.getToken());
#else

      IScenarioColour sc = findColour(parse.getToken());
      COLORREF color = colours[sc].colour;

      borderColors.colours[i] = color;

#endif

#ifdef DEBUG
      // debugLog("Type %d = %d\n", (int) i, (int) bc.colour);
#endif
   }

   ASSERT(d_configFile->nextScan() == 0);

   d_configFile->endScan();
}

void Scenario_Imp::readFonts()
{
   unsigned int nFonts = countKeys(s_CFGS_FontType);
   ASSERT(nFonts > 0);

   ASSERT(nFonts >= Font_HowMany);

   fonts.init(nFonts);

   d_configFile->startScan(s_CFGS_FontType);
   for(unsigned int i = 0; i < nFonts; i++)
   {
      ScenarioParse parse = d_configFile->nextScan();

      FontType& f = fonts[i];

      // f.faceName = parse.getToken();
      f.fileName = makeScenarioFileName(parse.getToken());

#ifdef DEBUG
      // debugLog("readFont() facename = %s filename = %s", f.faceName, f.fileName);
      debugLog("readFont() filename = %s\n", f.fileName);
#endif
   }

   ASSERT(d_configFile->nextScan() == 0);

   d_configFile->endScan();

}

void Scenario_Imp::addFonts()
{
   for(int i = 0; i < fonts.entries(); i++)
   {
      FontType& ft = fonts[i];

      CString faceName;
      Boolean result = GlobalFonts::addFontToResource(ft.fileName, faceName);
      ft.faceName = faceName;

#ifdef DEBUG
      debugLog("Added font %s (%s)\n", ft.fileName, ft.faceName);
#endif

      if(!result)
         throw GeneralError("%s not added to System Fonts!", ft.fileName);
      // throw GeneralError("%s - %s not added to System Fonts!", ft.fileName, ft.faceName);
   }
}

const Table1D<UBYTE>& Scenario_Imp::getCombatDivisorTable()
{
   if(!combatDivisorTable.isInitiated())
   {
      combatDivisorTable.init(d_tableDataName, s_combatDivisorTableChunkName);
   }

   return combatDivisorTable;
}

const Table1D<UWORD>& Scenario_Imp::getUnitCountModifierTable()
{
   if(!unitCountModifierTable.isInitiated())
   {
      unitCountModifierTable.init(d_tableDataName, s_unitCountModifierTableChunkName, 100);
   }

   return unitCountModifierTable;
}

const Table1D<UWORD>& Scenario_Imp::getBreakOutBattleTable()
{
   if(!breakOutBattleTable.isInitiated())
   {
      breakOutBattleTable.init(d_tableDataName, s_breakOutBattleTableChunkName, 10);
   }

   return breakOutBattleTable;
}

const Table1D<UWORD>& Scenario_Imp::getSortieTestTable()
{
   if(!sortieTestTable.isInitiated())
   {
      sortieTestTable.init(d_tableDataName, s_sortieTestTableChunkName, 100);
   }

   return sortieTestTable;
}

const Table1D<UWORD>& Scenario_Imp::getStartMoraleModifierTable()
{
   if(!startMoraleModifierTable.isInitiated())
   {
      startMoraleModifierTable.init(d_tableDataName, s_startMoraleModifierChunkName, 100);
   }

   return startMoraleModifierTable;
}

const Table1D<SWORD>& Scenario_Imp::getBombardmentNationModifierTable()
{
   if(!bombardmentNationModifierTable.isInitiated())
   {
      bombardmentNationModifierTable.init(d_tableDataName, s_bombardmentNationModifierChunkName);
   }

   return bombardmentNationModifierTable;
}

const Table1D<SWORD>& Scenario_Imp::getLeaderHitNationModifierTable()
{
   if(!leaderHitNationModifierTable.isInitiated())
   {
      leaderHitNationModifierTable.init(d_tableDataName, s_leaderHitNationModifierChunkName, 100);
   }

   return leaderHitNationModifierTable;
}

const Table1D<SWORD>& Scenario_Imp::getLeaderHitRankModifierTable()
{
   if(!leaderHitRankModifierTable.isInitiated())
   {
      leaderHitRankModifierTable.init(d_tableDataName, s_leaderHitRankModifierChunkName, 100);
   }

   return leaderHitRankModifierTable;
}

const Table1D<SWORD>& Scenario_Imp::getBombardmentModifierTable()
{
   if(!bombardmentModifierTable.isInitiated())
   {
      bombardmentModifierTable.init(d_tableDataName, s_bombardmentModifierChunkName);
   }

   return bombardmentModifierTable;
}

const Table1D<int>& Scenario_Imp::getForceSizeTable()
{
   if(!forceSizeTable.isInitiated())
   {
      forceSizeTable.init(d_tableDataName, s_forceSizeTableChunkName);
   }

   return forceSizeTable;
}

const Table1D<int>& Scenario_Imp::getSizeModifierTable()
{
   if(!sizeModifierTable.isInitiated())
   {
      sizeModifierTable.init(d_tableDataName, s_sizeModifierChunkName, 10);
   }

   return sizeModifierTable;
}

const Table1D<UBYTE>& Scenario_Imp::getDeploymentTimeTable()
{
   if(!deploymentTimeTable.isInitiated())
   {
      deploymentTimeTable.init(d_tableDataName, s_deploymentTimeChunkName);
   }

   return deploymentTimeTable;
}

const Table1D<UBYTE>& Scenario_Imp::getSupplyModifierTable()
{
   if(!supplyModifierTable.isInitiated())
   {
      supplyModifierTable.init(d_tableDataName, s_supplyModifierChunkName);
   }

   return supplyModifierTable;
}

const Table1D<SWORD>& Scenario_Imp::getCombatDieRollModifier()
{
   if(!combatDieRollModifier.isInitiated())
   {
      combatDieRollModifier.init(d_tableDataName, s_combatDieRollChunkName);
   }

   return combatDieRollModifier;
}

const Table1D<int>& Scenario_Imp::getCasualityEffectTable()
{
   if(!casualityEffectTable.isInitiated())
   {
      casualityEffectTable.init(d_tableDataName, s_casualityTableChunkName, 10);
   }

   return casualityEffectTable;
}

const Table1D<int>& Scenario_Imp::getTacticalPursuitTable()
{
   if(!tacticalPursuitTable.isInitiated())
   {
      tacticalPursuitTable.init(d_tableDataName, s_tacticalPursuitTableChunkName);
   }

   return tacticalPursuitTable;
}

const Table3D<int>& Scenario_Imp::getCasualtyTerrainModifierTable()
{
   if(!casualtyTerrainModifierTable.isInitiated())
   {
      casualtyTerrainModifierTable.init(d_tableDataName, s_casualtyTerrainModifierChunkName);
   }

   return casualtyTerrainModifierTable;
}

const Table3D<UWORD>& Scenario_Imp::getLossPostureModifierTable()
{
   if(!lossPostureModifierTable.isInitiated())
   {
      lossPostureModifierTable.init(d_tableDataName, s_lossPostureModifierChunkName, 100);
   }

   return lossPostureModifierTable;
}

const Table1D<UWORD>& Scenario_Imp::getLossModifierTable()
{
   if(!lossModifierTable.isInitiated())
   {
      lossModifierTable.init(d_tableDataName, s_lossModifierChunkName, 100);
   }

   return lossModifierTable;
}

const Table2D<SWORD>& Scenario_Imp::getMoraleNationModifierTable()
{
   if(!moraleNationModifierTable.isInitiated())
   {
      moraleNationModifierTable.init(d_tableDataName, s_moraleNationModifierChunkName);
   }

   return moraleNationModifierTable;
}

const Table3D<SWORD>& Scenario_Imp::getCasualtyCavalryModifierTable()
{
   if(!casualtyCavalryModifierTable.isInitiated())
   {
      casualtyCavalryModifierTable.init(d_tableDataName, s_casualtyCavalryModifierChunkName);
   }

   return casualtyCavalryModifierTable;
}

const Table2D<SWORD>& Scenario_Imp::getCasualtyNationTable()
{
   if(!casualtyNationTable.isInitiated())
   {
      casualtyNationTable.init(d_tableDataName, s_casualtyNationChunkName);
   }

   return casualtyNationTable;
}

const Table3D<int>& Scenario_Imp::getCasualtyRiverTypeModifierTable()
{
   if(!casualtyRiverTypeModifierTable.isInitiated())
   {
      casualtyRiverTypeModifierTable.init(d_tableDataName, s_casualtyRiverTypeModifierChunkName);
   }

   return casualtyRiverTypeModifierTable;
}

const Table2D<int>& Scenario_Imp::getBombardmentTerrainModifierTable()
{
   if(!bombardmentTerrainModifierTable.isInitiated())
   {
      bombardmentTerrainModifierTable.init(d_tableDataName, s_bombardmentTerrainModifierChunkName);
   }

   return bombardmentTerrainModifierTable;
}

const Table2D<UBYTE>& Scenario_Imp::getLeaderHitTable()
{
   if(!leaderHitTable.isInitiated())
   {
      leaderHitTable.init(d_tableDataName, s_leaderHitTableChunkName);
   }

   return leaderHitTable;
}

const Table1D<int>& Scenario_Imp::getBombardmentEffectTable()
{
   if(!bombardmentEffectTable.isInitiated())
   {
      bombardmentEffectTable.init(d_tableDataName, s_bombardmentTableChunkName, 10);
   }

   return bombardmentEffectTable;
}

const Table2D<SWORD>& Scenario_Imp::getBombardmentWeatherTable()
{
   if(!bombardmentWeatherTable.isInitiated())
   {
      bombardmentWeatherTable.init(d_tableDataName, s_bombardmentWeatherChunkName);
   }

   return bombardmentWeatherTable;
}

const Table2D<Attribute>& Scenario_Imp::getMoraleEffectTable()
{
   if(!moraleEffectTable.isInitiated())
   {
      moraleEffectTable.init(d_tableDataName, s_moraleTableChunkName);
   }

   return moraleEffectTable;
}

const Table3D<SWORD>& Scenario_Imp::getMoraleDieRollModifierTable()
{
   if(!moraleDieRollModifierTable.isInitiated())
   {
      moraleDieRollModifierTable.init(d_tableDataName, s_moraleDieRollModifierChunkName);
   }

   return moraleDieRollModifierTable;
}

const Table3D<UWORD>& Scenario_Imp::getMoraleLossModifierTable()
{
   if(!moraleLossModifierTable.isInitiated())
   {
      moraleLossModifierTable.init(d_tableDataName, s_moraleLossModifierChunkName, 100);
   }

   return moraleLossModifierTable;
}

const Table1D<UWORD>& Scenario_Imp::getAlterLeaderTable()
{
   if(!alterLeaderTable.isInitiated())
   {
      alterLeaderTable.init(d_tableDataName, s_alterLeaderChunkName, 10);
   }

   return alterLeaderTable;
}

const Table1D<UBYTE>& Scenario_Imp::getLeaderDecreaseAbilityTable()
{
   if(!leaderDecreaseAbilityTable.isInitiated())
   {
      leaderDecreaseAbilityTable.init(d_tableDataName, s_leaderDecreaseAbilityChunkName);
   }

   return leaderDecreaseAbilityTable;
}

const Table1D<UBYTE>& Scenario_Imp::getNationDefectsTable()
{
   if(!nationDefectsTable.isInitiated())
   {
      nationDefectsTable.init(d_tableDataName, s_nationDefectsChunkName);
   }

   return nationDefectsTable;
}

const Table1D<UBYTE>& Scenario_Imp::getNationDefectsSideTable()
{
   if(!nationDefectsSideTable.isInitiated())
   {
      nationDefectsSideTable.init(d_tableDataName, s_nationDefectsSideChunkName);
   }

   return nationDefectsSideTable;
}

const Table1D<SWORD>& Scenario_Imp::getNationDefectsModifierTable()
{
   if(!nationDefectsModifierTable.isInitiated())
   {
      nationDefectsModifierTable.init(d_tableDataName, s_nationDefectsModifierChunkName);
   }

   return nationDefectsModifierTable;
}

const Table1D<UBYTE>& Scenario_Imp::getNationDefectsEnemyNationsTable()
{
   if(!nationDefectsEnemyNationsTable.isInitiated())
   {
      nationDefectsEnemyNationsTable.init(d_tableDataName, s_nationDefectsEnemyNationsChunkName);
   }

   return nationDefectsEnemyNationsTable;
}

const Table1D<UBYTE>& Scenario_Imp::getSideDepotLimitTable()
{
   if(!sideDepotLimitTable.isInitiated())
   {
      sideDepotLimitTable.init(d_tableDataName, s_sideDepotLimitChunkName);
   }

   return sideDepotLimitTable;
}

const Table1D<UBYTE>* Scenario_Imp::getActiveSiegeTableNoTrain()
{
   if(!activeSiegeTableNoTrain.isInitiated())
   {
      activeSiegeTableNoTrain.init(d_tableDataName, s_activeSiegeNoTrainChunkName);
   }

   return &activeSiegeTableNoTrain;
}

const Table1D<UBYTE>* Scenario_Imp::getActiveSiegeTableTrain()
{
   if(!activeSiegeTableTrain.isInitiated())
   {
      activeSiegeTableTrain.init(d_tableDataName, s_activeSiegeTrainChunkName);
   }

   return &activeSiegeTableTrain;
}

const Table1D<UBYTE>* Scenario_Imp::getSortieTableAttacker()
{
   if(!sortieTableAttacker.isInitiated())
   {
      sortieTableAttacker.init(d_tableDataName, s_sortieAttackerChunkName);
   }

   return &sortieTableAttacker;
}

const Table1D<UBYTE>* Scenario_Imp::getSortieTableDefender()
{
   if(!sortieTableDefender.isInitiated())
   {
      sortieTableDefender.init(d_tableDataName, s_sortieDefenderChunkName);
   }

   return &sortieTableDefender;
}

const Table1D<UBYTE>* Scenario_Imp::getStormResultTable()
{
   if(!stormResultTable.isInitiated())
   {
      stormResultTable.init(d_tableDataName, s_stormResultChunkName);
   }

   return &stormResultTable;
}

const Table1D<int>* Scenario_Imp::getStormLossTableAttacker()
{
   if(!stormLossTableAttacker.isInitiated())
   {
      stormLossTableAttacker.init(d_tableDataName, s_stormLossAttackerChunkName, 10);
   }

   return &stormLossTableAttacker;
}

const Table1D<int>* Scenario_Imp::getStormLossTableDefender()
{
   // static Boolean initiated = False;

   if(!stormLossTableDefender.isInitiated())
   {
      stormLossTableDefender.init(d_tableDataName, s_stormLossDefenderChunkName, 10);
      // stormLossTableDefender.readData(d_tableDataName);
      // initiated = True;
   }

   return &stormLossTableDefender;
}

const Table1D<UBYTE>* Scenario_Imp::getSallyResultTable()
{
   // static Boolean initiated = False;

   if(!sallyResultTable.isInitiated())
   {
      sallyResultTable.init(d_tableDataName, s_sallyResultChunkName);
      // sallyResultTable.readData(d_tableDataName);
      // initiated = True;
   }

   return &sallyResultTable;
}

const Table1D<int>* Scenario_Imp::getSallyLossTableAttacker()
{
   // static Boolean initiated = False;

   if(!sallyLossTableAttacker.isInitiated())
   {
      sallyLossTableAttacker.init(d_tableDataName, s_sallyLossAttackerChunkName, 10);
      // sallyLossTableAttacker.readData(d_tableDataName);
      // initiated = True;
   }

   return &sallyLossTableAttacker;
}

const Table1D<int>* Scenario_Imp::getSallyLossTableDefender()
{
   // static Boolean initiated = False;

   if(!sallyLossTableDefender.isInitiated())
   {
      sallyLossTableDefender.init(d_tableDataName, s_sallyLossDefenderChunkName, 10);
      // sallyLossTableDefender.readData(d_tableDataName);
      // initiated = True;
   }

   return &sallyLossTableDefender;
}

const Table1D<SWORD>* Scenario_Imp::getSiegeDieRollModifier()
{
   // static Boolean initiated = False;

   if(!siegeDieRollModifier.isInitiated())
   {
      siegeDieRollModifier.init(d_tableDataName, s_siegeDieRollModifierChunkName);
      // siegeDieRollModifier.readData(d_tableDataName);
      // initiated = True;
   }

   return &siegeDieRollModifier;
}

const Table3D<UBYTE>& Scenario_Imp::getAggressionResultTable()
{
   if(!aggressionResultTable.isInitiated())
   {
      aggressionResultTable.init(d_tableDataName, s_aggressionResultChunkName);
   }

   return aggressionResultTable;
}

const Table2D<SWORD>& Scenario_Imp::getAggressionChangeTable()
{
   if(!aggressionChangeTable.isInitiated())
   {
      aggressionChangeTable.init(d_tableDataName, s_aggressionChangeChunkName, 10);
   }

   return aggressionChangeTable;
}

const Table2D<int>& Scenario_Imp::getAttritionTable()
{
   if(!attritionTable.isInitiated())
   {
      attritionTable.init(d_tableDataName, s_attritionChunkName, 10);
   }

   return attritionTable;
}

const Table2D<SWORD>& Scenario_Imp::getAttritionModifierForConnectionTable()
{
   if(!attritionModifierForConnectionTable.isInitiated())
   {
      attritionModifierForConnectionTable.init(d_tableDataName, s_attritionModifierForConnectionChunkName);
   }

   return attritionModifierForConnectionTable;
}

const Table2D<SWORD>& Scenario_Imp::getAttritionModifierForTownTable()
{
   if(!attritionModifierForTownTable.isInitiated())
   {
      attritionModifierForTownTable.init(d_tableDataName, s_attritionModifierForTownChunkName);
   }

   return attritionModifierForTownTable;
}

const Table1D<SWORD>& Scenario_Imp::getAttritionModifiersTable()
{
   if(!attritionModifiersTable.isInitiated())
   {
      attritionModifiersTable.init(d_tableDataName, s_attritionModifiersChunkName);
   }

   return attritionModifiersTable;
}

const Table2D<int>& Scenario_Imp::getCourierSpeedAdjustmentTable()
{
   // static Boolean initiated = False;

   if(!courierSpeedAdjustmentTable.isInitiated())
   {
      courierSpeedAdjustmentTable.init(d_tableDataName, s_courierSpeedChunkName);
      // courierSpeedAdjustmentTable.readData(d_tableDataName);
      // initiated = True;
   }

   return courierSpeedAdjustmentTable;
}

const Table1D<UBYTE>& Scenario_Imp::getActionTimeTable()
{
   // static Boolean initiated = False;

   if(!actionTimeTable.isInitiated())
   {
      actionTimeTable.init(d_tableDataName, s_actionTimeChunkName);
      // actionTimeTable.readData(d_tableDataName);
      // initiated = True;
   }

   return actionTimeTable;
}

const Table2D<int>& Scenario_Imp::getActionTimeAdjustmentTable()
{
   // static Boolean initiated = False;

   if(!actionTimeAdjustmentTable.isInitiated())
   {
      actionTimeAdjustmentTable.init(d_tableDataName, s_actionTimeAdjustmentChunkName, 100);
      // actionTimeAdjustmentTable.readData(d_tableDataName);
      // initiated = True;
   }

   return actionTimeAdjustmentTable;
}

const Table2D<int>& Scenario_Imp::getBasicSpeedTable()
{
   // static Boolean initiated = False;

   if(!basicSpeedTable.isInitiated())
   {
      basicSpeedTable.init(d_tableDataName, s_basicSpeedChunkName, 10);
      // basicSpeedTable.readData(d_tableDataName);
      // initiated = True;
   }

   return basicSpeedTable;
}

const Table1D<int>& Scenario_Imp::getConnectQualityTable()
{
   // static Boolean initiated = False;

   if(!connectQualityTable.isInitiated())
   {
      connectQualityTable.init(d_tableDataName, s_connectQualityChunkName, 100);
      // connectQualityTable.readData(d_tableDataName);
      // initiated = True;
   }

   return connectQualityTable;
}

const Table2D<UBYTE>& Scenario_Imp::getWeatherTable()
{
   if(!weatherTable.isInitiated())
   {
      weatherTable.init(d_tableDataName, s_weatherTableChunkName);
   }

   return weatherTable;
}

const Table1D<SWORD>& Scenario_Imp::getConditionsAdjustmentTable()
{
   if(!conditionsAdjustmentTable.isInitiated())
   {
      conditionsAdjustmentTable.init(d_tableDataName, s_conditionsAdjustmentChunkName);
   }

   return conditionsAdjustmentTable;
}

const Table2D<int>& Scenario_Imp::getConditionToSpeedTable()
{
   if(!conditionToSpeedTable.isInitiated())
   {
      conditionToSpeedTable.init(d_tableDataName, s_conditionToSpeedChunkName, 100);
   }

   return conditionToSpeedTable;
}

const Table2D<UBYTE>& Scenario_Imp::getSpottingEnemyTable()
{
   if(!spottingEnemyTable.isInitiated())
   {
      spottingEnemyTable.init(d_tableDataName, s_spottingEnemyChunkName);
   }

   return spottingEnemyTable;
}

const Table1D<SWORD>& Scenario_Imp::getSpottingEnemyModifierTable()
{
   if(!spottingEnemyModifierTable.isInitiated())
   {
      spottingEnemyModifierTable.init(d_tableDataName, s_spottingEnemyModifierChunkName);
   }

   return spottingEnemyModifierTable;
}

const Table2D<UBYTE>& Scenario_Imp::getInformationQualityTable()
{
   if(!informationQualityTable.isInitiated())
   {
      informationQualityTable.init(d_tableDataName, s_informationQualityChunkName);
   }

   return informationQualityTable;
}

const Table1D<SWORD>& Scenario_Imp::getInfoQualityModifierTable()
{
   if(!infoQualityModifierTable.isInitiated())
   {
      infoQualityModifierTable.init(d_tableDataName, s_infoQualityModifierChunkName);
   }

   return infoQualityModifierTable;
}

const Table2D<UBYTE>& Scenario_Imp::getRandomEventTable()
{
   if(!randomEventTable.isInitiated())
   {
      randomEventTable.init(d_tableDataName, s_randomEventChunkName);
   }

   return randomEventTable;
}

const Table2D<UBYTE>& Scenario_Imp::getTownDelayTable()
{
   if(!townDelayTable.isInitiated())
   {
      townDelayTable.init(d_tableDataName, s_townDelayChunkName);
   }

   return townDelayTable;
}

const Table2D<UBYTE>& Scenario_Imp::getLeaderBaseValueTable()
{
   if(!leaderBaseValueTable.isInitiated())
   {
      leaderBaseValueTable.init(d_tableDataName, s_leaderBaseValueChunkName);
   }

   return leaderBaseValueTable;
}

const Table1D<UBYTE>& Scenario_Imp::getTownSizeMultiplierTable()
{
   if(!townSizeMultiplierTable.isInitiated())
   {
      townSizeMultiplierTable.init(d_tableDataName, s_townSizeMultiplierChunkName);
   }

   return townSizeMultiplierTable;
}

const Table1D<UBYTE>& Scenario_Imp::getLeaderHealthCutoffTable()
{
   if(!leaderHealthCutoffTable.isInitiated())
   {
      leaderHealthCutoffTable.init(d_tableDataName, s_leaderHealthCutoffChunkName);
   }

   return leaderHealthCutoffTable;
}

const StringTable& Scenario_Imp::getSideLeaderNameTable(Side side)
{
   ASSERT(side != SIDE_Neutral);
   ASSERT(side < getNumSides());

   if(side == 0)
   {
      if(!side0LeaderNameTable.isInitiated())
      {
         side0LeaderNameTable.init(d_tableDataName, s_side0LeaderNameChunkName);
      }

      return side0LeaderNameTable;
   }
   else
   {
      if(!side1LeaderNameTable.isInitiated())
      {
         side1LeaderNameTable.init(d_tableDataName, s_side1LeaderNameChunkName);
      }

      return side1LeaderNameTable;
   }
}

const Table2D<UBYTE>& Scenario_Imp::getRecoverMoraleFromRestingTable()
{
   if(!recoverMoraleFromRestingTable.isInitiated())
   {
      recoverMoraleFromRestingTable.init(d_tableDataName, s_recoverMoraleFromRestingChunkName);
   }

   return recoverMoraleFromRestingTable;
}

const Table1D<UBYTE>& Scenario_Imp::getRecoverMoraleRetreatingTable()
{
   if(!recoverMoraleRetreatingTable.isInitiated())
   {
      recoverMoraleRetreatingTable.init(d_tableDataName, s_recoverMoraleRetreatingChunkName);
   }

   return recoverMoraleRetreatingTable;
}

const Table2D<int>& Scenario_Imp::getRecoverMoraleRestingNationTable()
{
   if(!recoverMoraleRestingNationTable.isInitiated())
   {
      recoverMoraleRestingNationTable.init(d_tableDataName, s_recoverMoraleRestingNationChunkName);
   }

   return recoverMoraleRestingNationTable;
}

const Table2D<int>& Scenario_Imp::getRecoverMoraleRetreatingNationTable()
{
   if(!recoverMoraleRetreatingNationTable.isInitiated())
   {
      recoverMoraleRetreatingNationTable.init(d_tableDataName, s_recoverMoraleRetreatingNationChunkName);
   }

   return recoverMoraleRetreatingNationTable;
}

const Table1D<UWORD>& Scenario_Imp::getSupplyMiscTable()
{
   if(!supplyMiscTable.isInitiated())
   {
      supplyMiscTable.init(d_tableDataName, s_supplyMiscTableChunkName, 100);
   }

   return supplyMiscTable;
}

const Table1D<UWORD>& Scenario_Imp::getSupplyMonthlyTable()
{
   if(!supplyMonthlyTable.isInitiated())
   {
      supplyMonthlyTable.init(d_tableDataName, s_supplyMonthlyTableChunkName, 100);
   }

   return supplyMonthlyTable;
}

const Table1D<UWORD>& Scenario_Imp::getSupplyWeatherTable()
{
   if(!supplyWeatherTable.isInitiated())
   {
      supplyWeatherTable.init(d_tableDataName, s_supplyWeatherTableChunkName, 100);
   }

   return supplyWeatherTable;
}

Scenario_Imp::ScenarioFileException::ScenarioFileException(const char* fmt, ...)
{
   char buffer[500];

   strcpy(buffer, "Error Reading Scenario File:\n");
   size_t l = strlen(buffer);
   char* bPtr = buffer + l;

   va_list vaList;
   va_start(vaList, fmt);
   vbprintf(bPtr, 500-l, fmt, vaList);
   va_end(vaList);

   setMessage(buffer);
}

Scenario_Imp::CampaignSide Scenario_Imp::toCampaignSide(Side s) const
{
   if(s == SIDE_Neutral)
      return 0;
   else
   {
      ASSERT(s < getNumSides());

      if(s < getNumSides())
         return CampaignSide(s + 1);
      else
         throw GeneralError("Illegal Side %d in Scenario_Imp::toCampaignSide", (int) s);
   }
}

Side Scenario_Imp::fromCampaignSide(CampaignSide s) const
{
   ASSERT(s < getRealNumSides());

   if(s == 0)
      return SIDE_Neutral;
   else
   {
      return Side(s - 1);
   }
}


ImageLibrary* Scenario_Imp::getImageLibrary(LPCTSTR bmpResName, UWORD n, const ImagePos* pos, Boolean dc)
{
   return new ImageLibrary(getScenarioResDLL(), bmpResName, n, pos, dc);
}


HCURSOR Scenario_Imp::getZoomCursor() const
{
   if(Options::get(OPT_ColorCursor))
   {
      if(chZoomC == NULL)
         chZoomC = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_ZOOM_COLOR));
      ASSERT(chZoomC != NULL);
      return chZoomC;
   }
   else
   {

      if(chZoomM == NULL)
         chZoomM = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_ZOOM_MONO));
      ASSERT(chZoomM != NULL);
      return chZoomM;
   }
}

HCURSOR Scenario_Imp::getGloveCursor() const
{
   if(Options::get(OPT_ColorCursor))
   {

      if(chGloveC == NULL)
         chGloveC = LoadCursor(getScenarioResDLL(), MAKEINTRESOURCE(CURS_GLOVE_COLOR));
      ASSERT(chGloveC != 0);
      return chGloveC;
   }
   else
   {
      if(chGloveM == NULL)
         chGloveM = LoadCursor(getScenarioResDLL(), MAKEINTRESOURCE(CURS_GLOVE_MONO));
      ASSERT(chGloveM != 0);
      return chGloveM;
   }
}

HCURSOR Scenario_Imp::getDragCursor() const
{
   if(Options::get(OPT_ColorCursor))
   {

      if(d_chDragC == NULL)
         d_chDragC = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_DRAG_COLOR));
      ASSERT(d_chDragC != 0);
      return d_chDragC;
   }
   else
   {
      if(d_chDragM == NULL)
         d_chDragM = LoadCursor(APP::instance(), MAKEINTRESOURCE(CURS_DRAG_MONO));
      ASSERT(d_chDragM != 0);
      return d_chDragM;
   }
}

void Scenario_Imp::findHelpFile()
{
   try
   {
      d_helpFileName = getConfigFileName(s_CFGS_HelpFile, true);
   }
   catch(const ScenarioFileException& e)
   {
      d_helpFileName = 0;        // not available
   }
}


/*
 * Find the Historical Reference File
 * We could make up some shared function to find a file
 * which would take a keyword and:
 *              - Find keyword in scenario file
 *              - Search for file on hard disk (in current dir and scenario dir)
 *              - Search for file on CD
 *              - Return char*
 *
 * The function getConfigFileName() should be clever enough to do
 * what it says in the .swg file, which is:
 *              - If prefixed with ~\ use the current directory
 *              - Otherwise append the scenario directory to it.
 */

void Scenario_Imp::findHistoryFile()
{
   try
   {
      d_historyFileName = getConfigFileName(s_CFGS_HistoryFile, true);
   }
   catch(const ScenarioFileException& e)
   {
      d_historyFileName = 0;        // not available
   }
}

const MapScaleInfo* Scenario_Imp::getMapScaleInfo()
{
   return &d_mapInfo;
}


void Scenario_Imp::readMapInfo()
{
#if 0
      d_mapInfo.s_widthMiles = 700;
   d_mapInfo.s_heightMiles = 700;
   d_mapInfo.s_total = MapPoint(4096, 3072);
   d_mapInfo.s_p[0] = MapPoint(596, 346);
   d_mapInfo.s_p[1] = MapPoint(3473, 323);
   d_mapInfo.s_p[2] = MapPoint(256, 2732);
   d_mapInfo.s_p[3] = MapPoint(3845, 2709);
#endif

   ScenarioParse parse = d_configFile->get(s_CFGS_MapSize);

   parse.get(d_mapInfo.s_widthMiles);
   parse.get(d_mapInfo.s_heightMiles);

   parse.init(d_configFile->get(s_CFGS_MapScale));
   MapCord x;
   MapCord y;
   parse.get(x);
   parse.get(y);
   d_mapInfo.s_total = MapPoint(x, y);

   d_configFile->startScan(s_CFGS_MapPoint);
   for(int i = 0; i < 4; i++)
   {
      parse.init(d_configFile->nextScan());
      MapCord x;
      MapCord y;
      parse.get(x);
      parse.get(y);
      d_mapInfo.s_p[i] = MapPoint(x, y);
   }
   d_configFile->endScan();

   parse.init(d_configFile->get(s_CFGS_MapAngle));
   parse.get(d_mapInfo.s_viewAngle);
}

HINSTANCE Scenario_Imp::getScenarioResDLL() const
{
   if(d_scenarioResDLL == NULL)
   {
      CString scenarioResDLLName = getConfigFileName(s_CFGS_ScenarioResDLL, true);
      d_scenarioResDLL = LoadLibrary(scenarioResDLLName);
   }

   ASSERT(d_scenarioResDLL != NULL);

   return d_scenarioResDLL;
}

/*
 * Initialise Language Folder Name
 *
 * First check registry:
 * If not found, check config file
 * else look at codepage
 */

void Scenario_Imp::getLanguage()
{
   char buffer[200];
   if(getRegistryString(s_CFGS_Language, buffer, 0))
   {
      d_langName = copyString(buffer);
   }
   else
   {
      openConfigFile();
      d_langName = d_configFile->get(s_CFGS_Language);
      closeConfigFile();
      if(d_langName == 0)
      {
         // work out a default name

//         LANGID userID = GetUserDefaultLangID();

          const int LocaleType = LOCALE_SENGLANGUAGE; // SNATIVELANGNAME;
         int cSize = GetLocaleInfo(LOCALE_USER_DEFAULT, LocaleType, 0, 0);
         ASSERT(cSize);
         char* locale = new char[cSize + 1];
         locale[cSize] = 0;
         cSize = GetLocaleInfo(LOCALE_USER_DEFAULT, LocaleType, locale, cSize);
         ASSERT(cSize);


//         if(PRIMARYLANGID(userID) == LANG_GERMAN)
//         {
//            d_langName = copyString("german");
//         }

         d_langName = locale;
      }
   }

   if(d_langName != 0)
   {
      SimpleString path = d_folderName;
      path += "\\";
      path += d_langName;

      if(FileSystem::dirExists(path.toStr()))
         d_langFolder = copyString(d_langName);

      if(d_configFile)
         d_configFile->setLanguage(d_langName);
//         throw ScenarioFileException("Language folder %s is missing", (const char*)path.toStr());
   }
}


/*
 * Global instance of scenario
 */

Scenario* scenario = 0;

/*
 * Class to ensure Scenario is deleted at end of game
 */

class ScenarioKiller
{
   public:
      ~ScenarioKiller() { Scenario::kill(); }
};

static ScenarioKiller s_scenarioKiller;

/*
 * Static functions to create and destroy the scenario
 */


void Scenario::create(const char* name)
{
   if (!scenario || (strcmp(name, scenario->getFileName()) != 0))
   {
      delete scenario;
      scenario = new Scenario_Imp(name);
      FORWARD_WM_NCPAINT(APP::getMainHWND(), 1, PostMessage);
   }
}

void Scenario::kill()
{
   delete scenario;
   scenario = 0;
}

char* Scenario::defaultFileName()
{
   RegistryFile registry("\\Scenarios");
   char buf[200];
   if(registry.getFileName(buf))
      return copyString(buf);
   else
      return copyString("nap1813.swg");
}


