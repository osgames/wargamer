/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCN_IMG_HPP
#define SCN_IMG_HPP

#include "gamesup.h"

/*
 * One massive library of ImageLibrary
 */

class ImageLibrary;

class GAMESUP_DLL_EXPORT ScenarioImageLibrary {
public:
  enum Type {
	 First = 0,
	 CheckButtons = First,
	 TownBuildPageImages,
	 LargeTownImages,
	 TownImages,
    ToolBoxButtons,
    WeatherWindowImages,
	 SpecialistIcons,
	 VScrollButtons,
    HScrollButtons,
	 CClockIcons,
	 CTabImages,
	 ZoomButtonImages,
	 CustomMenuImages,
	 CToolBarImages,
    CComboButtonImages,
	 B_ToolBarImages,
    B_FaceImages,
	 HowMany
  };

#if 0
  enum BR_Type {
	 BR_First = 0,
	 BR_CheckButtons = First,
	 BR_CComboButtonImages,

	 BR_HowMany
  };
#endif

  static const ImageLibrary* get(Type type);
//  static const ImageLibrary* getBR(BR_Type type);
};

#endif
