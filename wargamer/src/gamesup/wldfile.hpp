/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef WLDFILE_H
#define WLDFILE_H

#ifndef __cplusplus
#error wldfile.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	World File reading and writing: Use by objects that read/write data
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"
#include "gamesup.h"

class FileReader;
class FileWriter;

/*
 * Virtual Function for reading/writing to a file
 * All objects should implement this.
 * However, some simple objects do not derive from it, so as to avoid
 * having a vTable.
 */
      
class GAMESUP_DLL_EXPORT FileObject {
public:
	virtual ~FileObject() { }
	virtual Boolean readData(FileReader& f) = 0;
	virtual Boolean writeData(FileWriter& f) const = 0;
};

/*
 * Miscelaneous Functions
 */

void GAMESUP_DLL_EXPORT addExtension(char* buf, const char* src, const char* ext);

/*
 * Useful functions to check for valid values
 */


// #define CHECKUBYTE(b) ASSERT((b >= 0) && (b <= UBYTE_MAX))
// #define CHECKUWORD(w) ASSERT((w >= 0) && (w <= UWORD_MAX))

#ifdef DEBUG

	#define CHECKUBYTE(b) ASSERT( b == static_cast<UBYTE>(b) )
	#define CHECKUWORD(w) ASSERT( w == static_cast<UWORD>(w) )

#else
	#define CHECKUBYTE(b) { }
	#define CHECKUWORD(w) { }
#endif

/*
 * Strings, to avoid duplicating them in different object files
 */

/*
 * Chunk Names
 */

extern GAMESUP_DLL_EXPORT const char campaignChunkName[];
extern GAMESUP_DLL_EXPORT const char townChunkName[];
extern GAMESUP_DLL_EXPORT const char connectionChunkName[];
extern GAMESUP_DLL_EXPORT const char provinceChunkName[];
extern GAMESUP_DLL_EXPORT const char armyChunkName[];
extern GAMESUP_DLL_EXPORT const char nationChunkName[];
extern GAMESUP_DLL_EXPORT const char campaignCPChunkName[];
extern GAMESUP_DLL_EXPORT const char commandChunkName[];
extern GAMESUP_DLL_EXPORT const char leaderChunkName[];
extern GAMESUP_DLL_EXPORT const char spChunkName[];
extern GAMESUP_DLL_EXPORT const char iLeaderChunkName[];
extern GAMESUP_DLL_EXPORT const char conditionsChunkName[];
extern GAMESUP_DLL_EXPORT const char allegianceChunkName[];
extern GAMESUP_DLL_EXPORT const char weatherChunkName[];
extern GAMESUP_DLL_EXPORT const char victoryLevelChunkName[];
extern GAMESUP_DLL_EXPORT const char totalLossesChunkName[];
extern GAMESUP_DLL_EXPORT const char sideRandomEventsChunkName[];
extern GAMESUP_DLL_EXPORT const char campaignTacticalChunkName[];

/*
 * Keywords
 */

extern GAMESUP_DLL_EXPORT const char scenarioToken[];
extern GAMESUP_DLL_EXPORT const char entriesToken[];
extern GAMESUP_DLL_EXPORT const char nameToken[];
extern GAMESUP_DLL_EXPORT const char sideToken[];
extern GAMESUP_DLL_EXPORT const char locationToken[];
extern GAMESUP_DLL_EXPORT const char connectionToken[];
extern GAMESUP_DLL_EXPORT const char provinceToken[];
extern GAMESUP_DLL_EXPORT const char nameAlignToken[];
extern GAMESUP_DLL_EXPORT const char sizeToken[];
extern GAMESUP_DLL_EXPORT const char victoryToken[];
extern GAMESUP_DLL_EXPORT const char politicalToken[];
extern GAMESUP_DLL_EXPORT const char manPowerToken[];
extern GAMESUP_DLL_EXPORT const char resourceToken[];
extern GAMESUP_DLL_EXPORT const char supplyToken[];
extern GAMESUP_DLL_EXPORT const char forageToken[];
extern GAMESUP_DLL_EXPORT const char stackingToken[];
extern GAMESUP_DLL_EXPORT const char terrainToken[];
extern GAMESUP_DLL_EXPORT const char flagsToken[];
extern GAMESUP_DLL_EXPORT const char fortificationsToken[];
extern GAMESUP_DLL_EXPORT const char capitalToken[];
extern GAMESUP_DLL_EXPORT const char abrevToken[];
extern GAMESUP_DLL_EXPORT const char nodeToken[];
extern GAMESUP_DLL_EXPORT const char howToken[];
extern GAMESUP_DLL_EXPORT const char qualityToken[];
extern GAMESUP_DLL_EXPORT const char capacityToken[];
extern GAMESUP_DLL_EXPORT const char townsToken[];
extern GAMESUP_DLL_EXPORT const char presidentToken[];
extern GAMESUP_DLL_EXPORT const char controlToken[];
extern GAMESUP_DLL_EXPORT const char rankCountToken[];
extern GAMESUP_DLL_EXPORT const char unusedToken[];
extern GAMESUP_DLL_EXPORT const char leaderToken[];
extern GAMESUP_DLL_EXPORT const char moraleToken[];
extern GAMESUP_DLL_EXPORT const char fatigueToken[];
extern GAMESUP_DLL_EXPORT const char initiativeToken[];
extern GAMESUP_DLL_EXPORT const char staffToken[];
extern GAMESUP_DLL_EXPORT const char subordinationToken[];
extern GAMESUP_DLL_EXPORT const char aggressionToken[];
extern GAMESUP_DLL_EXPORT const char charismaToken[];
extern GAMESUP_DLL_EXPORT const char tacticalToken[];
extern GAMESUP_DLL_EXPORT const char commandRadiusToken[];
extern GAMESUP_DLL_EXPORT const char rankRatingToken[];
extern GAMESUP_DLL_EXPORT const char healthToken[];
extern GAMESUP_DLL_EXPORT const char nationToken[];
extern GAMESUP_DLL_EXPORT const char commandToken[];
extern GAMESUP_DLL_EXPORT const char positionToken[];
extern GAMESUP_DLL_EXPORT const char rankToken[];
extern GAMESUP_DLL_EXPORT const char linkToken[];
extern GAMESUP_DLL_EXPORT const char nextToken[];
extern GAMESUP_DLL_EXPORT const char strengthPointToken[];
extern GAMESUP_DLL_EXPORT const char offScreenToken[];
extern GAMESUP_DLL_EXPORT const char distanceToken[];

extern GAMESUP_DLL_EXPORT const char startItemString[];
extern GAMESUP_DLL_EXPORT const char endItemString[];
extern GAMESUP_DLL_EXPORT const char eolString[];

extern GAMESUP_DLL_EXPORT const char fmt_sddddn[];
extern GAMESUP_DLL_EXPORT const char fmt_sdddn[];
extern GAMESUP_DLL_EXPORT const char fmt_sddn[];
extern GAMESUP_DLL_EXPORT const char fmt_sdn[];
extern GAMESUP_DLL_EXPORT const char fmt_ssn[];
extern GAMESUP_DLL_EXPORT const char fmt_slln[];
extern GAMESUP_DLL_EXPORT const char fmt_sln[];
extern GAMESUP_DLL_EXPORT const char fmt_sn[];


#endif /* WLDFILE_H */

