/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "victory.hpp"
#include "myassert.hpp"
#include "scenario.hpp"
#include "simpstr.hpp"
#include "bmp.hpp"
#include "random.hpp"
#include <stdio.h>

//=========================== GameVictory ==============================

void GameVictory::setWinner(Side side)
{
  ASSERT(side < scenario->getNumSides() || side == SIDE_Neutral);

  if(side == 0)
	 d_whoWon = Side0;
  else if(side == 1)
	 d_whoWon = Side1;
  else if(side == SIDE_Neutral)
	 d_whoWon = Draw;
#ifdef DEBUG
  else
	 FORCEASSERT("Improper side in GameVictory::setWinner()");
#endif
}

void GameVictory::setWinner(Side s, GameVictory::VictoryLevel vl)
{
	d_level = vl;
  setWinner(s);
}


/*
 * Get background picture for Victory screen
 *
 * TODO: Read filenames from scenario file
 */

DIB* GameVictory::getPicture(Side winner)
{
   /*
    * Choose a picture
    *       <scenario>\Victory\drawVicResults?.bmp
    *       <scenario>\Victory\FrenchVicResults?.bmp
    *       <scenario>\Victory\AlliedVicResults?.bmp
    */

   SimpleString baseName = "Victory\\";
   int choice = 0;

   if (winner == SIDE_Neutral)
   {
       baseName += "draw";
       choice = 5;
   }
   else if (winner == 0)
   {
      baseName += "French";
      choice = 4;
   }
   else
   {
      baseName += "Allied";
      choice = 4;
   }

   int choose = random(1, choice + 1);
   char strN[5];
   sprintf(strN, "%d", choose);

   baseName += "VicResults";
   baseName += strN;
   baseName += ".bmp";

   CString picName = scenario->makeScenarioFileName(baseName.toStr());

   return  BMP::newDIB(picName.toStr(), BMP::RBMP_ForcePalette);
}
