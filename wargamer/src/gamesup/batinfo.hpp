/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATINFO_HPP
#define BATINFO_HPP

#include "scenario.hpp"
#include "piclib.hpp"
#include "control.hpp"
#include "resstr.hpp"


/*

	Filename : Batinfo.hpp
	Description : Information about a battle which Campaign passes to battlegame

*/




/*
This enumeration is used as an index into the table below when generating height fields
*/

enum TerrainHeightFieldType {
    HEIGHT_FLAT,
    HEIGHT_ROLLING,
    HEIGHT_HILLY,
    HEIGHT_STEEP
};






/*
This is the basic interface with the terrain generator
A script file can be read into these parameters, or they may be set explicitly
*/

typedef struct TerrainGenerationParameters {

    /*
    Map dimensions
    */

    // width & height in hexes of map
    int MapWidth;
    int MapHeight;

    /*
    HeightField Generation
    */

    // description of the type of heightfield to be created
    TerrainHeightFieldType HeightType;

    /*
    BaseTerrain Generation
    */


    int FirstTerrainType;

    int SecondTerrainType;
    int PercentageSecondTerrain;
    int ThirdTerrainType;
    int PercentageThirdTerrain;
    int FourthTerrainType;
    int PercentageFourthTerrain;
    int FifthTerrainType;
    int PercentageFifthTerrain;

    /*
    Coasts & Lakes
    */

    int NumberOfLakes;
    int MinimumLakeSize;
    int MaximumLakeSize;

    /*
    Rivers & Streams
    */

    // how many rivers on map
    int NumberOfRivers;
    // how many streams on map
    int NumberOfStreams;
    // percentage chance that roads will form a bridge (otherwise will create a ford)
    int ChanceCreateBridge;

	//
	int ChanceRiversSunken;
	int ChanceStreamsSunken;

    /*
    Roads & Tracks
    */

    // how many roads on map
    int NumberOfRoads;
    // how many tracks on map
    int NumberOfTracks;
    // percentage chance that roads will join (otherwise will cross)
    int ChanceRoadsJoin;

	int ChanceRoadsSunken;
	int ChanceTracksSunken;

    /*
    Seeded Settlements
    */

    // percentage chances for creating settlements at various terrain features
    int ChanceSettlementAtCrossroads;
    int ChanceSettlementAtJunction;
    int ChanceSettlementAtBridge;
    int ChanceSettlementAtFord;

    // the smallest settlement size that can be created
    int MinimumSettlementSize;
    // the largest settlement size that can be created
    int MaximumSettlementSize;
    // maximum size before a village becomes a town
    int MaximumVillageSize;
    // maximum size a town may grow to
    int MaximumTownSize;
    // how closely packed a village settlement is
    int VillageDensity;
    // how closely packed a town settlement it
    int TownDensity;
    // how close two settlements can be before joining into one larger settlement
    int CombineSettlementsDistance;

    int MinimumFarmSize;
    int MaximumFarmSize;

    /*
    Random Settlements
    */

    // how many random settlements are created
    int NumberOfRandomSettlements;
    // percentage chance for random settlement types (chosen in this order)
    int ChanceRandomSettlementIsSpecial;
    int ChanceRandomSettlementIsSolitary;
    int ChanceRandomSettlementIsFarm;

    /*
    Further Terrain Features
    */

    // percentage chance that a farm will have fields surrounding
    int ChanceFarmsCreateFields;
    // average size in hexes that a farm's fields will cover
    int AverageFarmFieldsSize;

    // number of wooded ares to create
    int NumberOfLightWoods;
    int NumberOfDenseWoods;
    // min & max size in hexes that a wooded area will cover
    int MinimumWoodSize;
    int MaximumWoodSize;
    // min & max density of woods that are created
    int MinimumWoodDensity;
    int MaximumWoodDensity;

	int d_numVillagesCreated;
	int d_numTownsCreated;
	int d_numFarmsCreated;



    TerrainGenerationParameters(void) {

        HeightType = static_cast<TerrainHeightFieldType> (0);

        FirstTerrainType = 1; // GT_Grass
        PercentageSecondTerrain = 0;
		PercentageThirdTerrain = 0;
		PercentageFourthTerrain = 0;
		PercentageFifthTerrain = 0;

        NumberOfRivers = 0;
        NumberOfStreams = 0;
        NumberOfRoads = 0;
        NumberOfTracks = 0;

		ChanceRiversSunken = 0;
		ChanceStreamsSunken = 0;
		ChanceRoadsSunken = 0;
		ChanceTracksSunken = 0;

        ChanceRoadsJoin = 50;
        ChanceCreateBridge = 100;

        MinimumSettlementSize = 4;
        MaximumSettlementSize = 10;
        MaximumVillageSize = 6;
        MaximumTownSize = 10;
        VillageDensity = 3;
        TownDensity = 12;
        CombineSettlementsDistance = 8;

        MinimumFarmSize = 2;
        MaximumFarmSize = 4;

        ChanceSettlementAtCrossroads = 50;
        ChanceSettlementAtJunction = 25;
        ChanceSettlementAtBridge = 50;
        ChanceSettlementAtFord = 25;

        NumberOfRandomSettlements = 5;
        ChanceRandomSettlementIsSpecial = 10;
        ChanceRandomSettlementIsSolitary = 10;
        ChanceRandomSettlementIsFarm = 100;

        ChanceFarmsCreateFields = 50;
        AverageFarmFieldsSize = 3;

        NumberOfLightWoods = 5;
        NumberOfDenseWoods = 5;
        MinimumWoodSize = 5;
        MaximumWoodSize = 10;
        MinimumWoodDensity = 4;
        MaximumWoodDensity = 8;

        NumberOfLakes = 0;
        MinimumLakeSize = 2;
        MaximumLakeSize = 6;

		d_numVillagesCreated = 0;
		d_numTownsCreated = 0;
		d_numFarmsCreated = 0;
    }


} TerrainGenerationParameters;









/*
This is the structure which the Campaign fills-out and passes to the Battle
*/

struct BattleInfo {

public:

	enum BattleWeatherEnum {

		SunnyDry,
		SunnyMuddy,
		RainyDry,
		RainyMuddy,
		StormyDry,
		StormyMuddy,

		BattleWeatherEnum_HowMany
	};

	enum BattleTerrainEnum {

		Open,
		Rough,
		Wooded,
		Hilly,
		WoodedHilly,
		MountainPass,
		Marsh,
		OpenWithMajorRiver,
		OpenWithMinorRiver,
		WoodedWithMajorRiver,
		WoodedWithMinorRiver,
		RoughWithMajorRiver,
		RoughWithMinorRiver,

		BattleTerrainEnum_HowMany
	};

	enum BattlePopulationEnum {

		Sparse,
		Moderate,
		High,
		Dense,

		BattlePopulationEnum_HowMany
	};


	/*
	Functions
	*/

    BattleInfo() : d_title() {
#ifdef DEBUG
		/*
		For the moment when debugging, have a constant constructor
		*/
        d_deploymentWidth = 128;
        d_deploymentHeight = 128;
        d_battleWidth = 64;
        d_battleHeight = 64;

        d_playerSide = GamePlayerControl::getSide(GamePlayerControl::Player);

		d_provincePicture = NULL;
		d_provinceDescription = 0;

		d_weather = SunnyDry;
		d_terrain = Open;
		d_population = Sparse;
	    d_heightType = HEIGHT_FLAT;

#else
		/*
		Otherwise use random values
		*/
        d_deploymentWidth = 128;
        d_deploymentHeight = 128;
        d_battleWidth = 64;
        d_battleHeight = 64;

        d_playerSide = GamePlayerControl::getSide(GamePlayerControl::Player);

		d_provincePicture = NULL;
		d_provinceDescription = 0;

		d_weather = SunnyDry;
		d_terrain = Open;
		d_population = Sparse;
	    d_heightType = HEIGHT_FLAT;

#endif
	}

    ~BattleInfo(void) { }

    const StringPtr& title(void) const { return d_title; }
    void title(const char* s) { d_title = s; }

	const int deploymentWidth(void) const { return d_deploymentWidth; }
	void deploymentWidth(int w) { d_deploymentWidth = w; }

	const int deploymentHeight(void) const { return d_deploymentHeight; }
	void deploymentHeight(int h) { d_deploymentHeight = h; }

	const int battleWidth(void) const { return d_battleWidth; }
	void battleWidth(int w) { d_battleWidth = w; }

	const int battleHeight(void) const { return d_battleHeight; }
	void battleHeight(int h) { d_battleHeight = h; }

	const Side playerSide(void) const { return d_playerSide; }
	void playerSide(Side s) { d_playerSide = s; }

	const Side initiativeSide(void) const { return d_initiativeSide; }
	void initiativeSide(Side s) { d_initiativeSide = s; }

	const DIB * provincePicture(void) const { return d_provincePicture; }
	void provincePicture(DIB * dib) { d_provincePicture = dib; }

	const char * provinceDescription(void) const { return d_provinceDescription; }
	void provinceDescription(char * d) { d_provinceDescription = d; }

	const BattleWeatherEnum weather(void) const { return d_weather; }
	void weather(BattleWeatherEnum w) { d_weather = w; }

	const BattleTerrainEnum terrain(void) const { return d_terrain; }
	void terrain(BattleTerrainEnum t) { d_terrain = t; }

	const BattlePopulationEnum population(void) const { return d_population; }
	void population(BattlePopulationEnum p) { d_population = p; }

	const TerrainHeightFieldType heightType(void) const { return d_heightType; }
	void heightType(TerrainHeightFieldType h) { d_heightType = h; }


	const char * getTerrainTypeText(void) const {
#if 0
		switch(d_terrain) {
			case Open : return "Open";
			case Rough : return "Rough";
			case MountainPass : return "Mountain Pass";
			case Hilly : return "Hilly";
			case Wooded : return "Wooded";
			case WoodedHilly : return "Wooded and Hilly";
			case Marsh : return "Marsh";
			case OpenWithMajorRiver : return "Open with Major River";
			case OpenWithMinorRiver : return "Open with Minor River";
			case WoodedWithMajorRiver : return "Wooded with Major River";
			case WoodedWithMinorRiver : return "Wooded with Minor River";
			case RoughWithMajorRiver : return "Rough with Major River";
			case RoughWithMinorRiver : return "Rough with Minor River";
			default : {
				FORCEASSERT("ERROR - bad terrain type in GetTerrainTypeText");
				return("Unknown Terrain");
			}
		}
#else
      ASSERT(d_terrain < BattleTerrainEnum_HowMany);
      return(InGameText::get(IDS_CB_TERRAIN_TYPE + d_terrain));
#endif
	}

	const char * getGroundTypeText(void) const {
#if 0
		switch(d_terrain) {
			case Open : return "Clear";
			case Rough : return "Rough";
			case MountainPass : return "Rough";
			case Hilly : return "Clear";
			case Wooded : return "Wooded";
			case WoodedHilly : return "Wooded";
			case Marsh : return "Marsh";
			case OpenWithMajorRiver : return "Clear";
			case OpenWithMinorRiver : return "Clear";
			case WoodedWithMajorRiver : return "Wooded";
			case WoodedWithMinorRiver : return "Wooded";
			case RoughWithMajorRiver : return "Rough";
			case RoughWithMinorRiver : return "Rough";
			default : {
				FORCEASSERT("ERROR - bad terrain type in GetTerrainTypeText");
				return("Unknown Terrain");
			}
		}
#else
      ASSERT(d_terrain < BattleTerrainEnum_HowMany);
      return(InGameText::get(IDS_CB_GROUND_TYPE + d_terrain));
#endif
	}

	/*
	Data Members
	*/


private:

	// size of initial battlefield
	int d_deploymentWidth;
	int d_deploymentHeight;

	// size of area to fight on
	int d_battleWidth;
	int d_battleHeight;

	// which side player is
	Side d_playerSide;
	// which side has battle-selection initiative
	Side d_initiativeSide;

	// province
	DIB * d_provincePicture;
	char * d_provinceDescription;

	// weather
	BattleWeatherEnum d_weather;

	// terrain
	BattleTerrainEnum d_terrain;

	// population
	BattlePopulationEnum d_population;

	// height type (NOTE: set automatically by BCREATE)
	TerrainHeightFieldType d_heightType;

	// title
	StringPtr d_title;

};



#endif
