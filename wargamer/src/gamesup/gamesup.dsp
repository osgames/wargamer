# Microsoft Developer Studio Project File - Name="gamesup" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=gamesup - Win32 Editor Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "gamesup.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "gamesup.mak" CFG="gamesup - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "gamesup - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gamesup - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gamesup - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gamesup - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "gamesup - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "GAMESUP_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 ComDlg32.lib user32.lib /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "gamesup - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "GAMESUP_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ComDlg32.lib user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/gamesupDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "gamesup - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "gamesup___Win32_Editor_Debug"
# PROP BASE Intermediate_Dir "gamesup___Win32_Editor_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ComDlg32.lib user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/gamesupDB.dll" /pdbtype:sept
# ADD LINK32 ComDlg32.lib user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/gamesupDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "gamesup - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "gamesup___Win32_Editor_Release"
# PROP BASE Intermediate_Dir "gamesup___Win32_Editor_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /YX"stdinc.hpp" /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_GAMESUP_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ComDlg32.lib user32.lib /nologo /dll /debug /machine:I386
# ADD LINK32 ComDlg32.lib user32.lib /nologo /dll /debug /machine:I386

!ENDIF 

# Begin Target

# Name "gamesup - Win32 Release"
# Name "gamesup - Win32 Debug"
# Name "gamesup - Win32 Editor Debug"
# Name "gamesup - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\control.cpp
# End Source File
# Begin Source File

SOURCE=.\gameinfo.cpp
# End Source File
# Begin Source File

SOURCE=.\gdespat.cpp
# End Source File
# Begin Source File

SOURCE=.\gthread.cpp
# End Source File
# Begin Source File

SOURCE=.\multiplayerconnection.cpp
# End Source File
# Begin Source File

SOURCE=.\multiplayermsg.cpp
# End Source File
# Begin Source File

SOURCE=.\name.cpp
# End Source File
# Begin Source File

SOURCE=.\options.cpp
# End Source File
# Begin Source File

SOURCE=.\print.cpp
# End Source File
# Begin Source File

SOURCE=.\savegame.cpp
# End Source File
# Begin Source File

SOURCE=.\scenario.cpp
# End Source File
# Begin Source File

SOURCE=.\scn_img.cpp
# End Source File
# Begin Source File

SOURCE=.\victory.cpp
# End Source File
# Begin Source File

SOURCE=.\wg_msg.cpp
# End Source File
# Begin Source File

SOURCE=.\wg_rand.cpp
# End Source File
# Begin Source File

SOURCE=.\wldfile.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\batinfo.hpp
# End Source File
# Begin Source File

SOURCE=.\cmsg_id.hpp
# End Source File
# Begin Source File

SOURCE=.\control.hpp
# End Source File
# Begin Source File

SOURCE=.\dplyuser.hpp
# End Source File
# Begin Source File

SOURCE=.\filename.hpp
# End Source File
# Begin Source File

SOURCE=.\gamebase.hpp
# End Source File
# Begin Source File

SOURCE=.\gamedefs.hpp
# End Source File
# Begin Source File

SOURCE=.\gameinfo.hpp
# End Source File
# Begin Source File

SOURCE=.\gamesup.h
# End Source File
# Begin Source File

SOURCE=.\gdespat.hpp
# End Source File
# Begin Source File

SOURCE=.\gpmsg.hpp
# End Source File
# Begin Source File

SOURCE=.\gthread.hpp
# End Source File
# Begin Source File

SOURCE=.\help.h
# End Source File
# Begin Source File

SOURCE=.\mappoint.hpp
# End Source File
# Begin Source File

SOURCE=.\multiplayerconnection.hpp
# End Source File
# Begin Source File

SOURCE=.\multiplayermsg.hpp
# End Source File
# Begin Source File

SOURCE=.\name.hpp
# End Source File
# Begin Source File

SOURCE=.\options.hpp
# End Source File
# Begin Source File

SOURCE=.\plyruser.hpp
# End Source File
# Begin Source File

SOURCE=.\print.hpp
# End Source File
# Begin Source File

SOURCE=.\refcp.hpp
# End Source File
# Begin Source File

SOURCE=.\savegame.hpp
# End Source File
# Begin Source File

SOURCE=.\scenario.hpp
# End Source File
# Begin Source File

SOURCE=.\scn_img.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=.\victory.hpp
# End Source File
# Begin Source File

SOURCE=.\wg_msg.hpp
# End Source File
# Begin Source File

SOURCE=.\wg_rand.hpp
# End Source File
# Begin Source File

SOURCE=.\wldfile.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
