/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GAMESUP_DLL_H
#define GAMESUP_DLL_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Included by all files in gamesup DLL
 *
 * Defines 
 *
 *
 *----------------------------------------------------------------------
 */


#if defined(NO_WG_DLL)
    #define GAMESUP_DLL_EXPORT
#elif defined(EXPORT_GAMESUP_DLL)
    #define GAMESUP_DLL_EXPORT __declspec(dllexport)
#else
    #define GAMESUP_DLL_EXPORT __declspec(dllimport)
#endif

#endif /* GAMESUP_DLL_H */


