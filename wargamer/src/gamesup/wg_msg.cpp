/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "wg_msg.hpp"

namespace WargameMessage 
{

// static Abandon s_abandonMessage;

LoadGame* LoadGame::getStaticInstance(const char* fname, LoadGame::Format mode)
{
   static LoadGame s_loadGameMessage;

	s_loadGameMessage.d_fileName = fname ? copyString(fname) : 0;
	s_loadGameMessage.d_format = mode;
	return &s_loadGameMessage;
}


// static Abandon Abandon::s_instance;
// 
// static Abandon* Abandon::getStaticInstance()
// {
// 	return &s_instance; // abandonMessage;
// }

};	// namespace WargameMessage 

