/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MULTIPLAYER_CONNECTION_HPP
#define MULTIPLAYER_CONNECTION_HPP

/*

  Filename : MultiplayerConnection.hpp

  Description : Class to hold the connection type of the game - members are static
				Connection is either None, Master or Slave
				The values are used to determine how time should be advanced in CLOGIC.CPP and other places

*/

#include "gamedefs.hpp"
#include "systick.hpp"
#include "gamesup.h"


enum ConnectionTypeEnum {
	ConnectionNone,
	ConnectionMaster,
	ConnectionSlave
};

GAMESUP_DLL_EXPORT extern ConnectionTypeEnum s_connectionType;	


// set when a dialog is up, therefore preventing time to be advanced
GAMESUP_DLL_EXPORT extern bool s_dialogPause;


class CMultiPlayerConnection {

public:

	CMultiPlayerConnection(void) { }
	~CMultiPlayerConnection(void) { }

	GAMESUP_DLL_EXPORT static ConnectionTypeEnum connectionType(void) { return s_connectionType; }
	GAMESUP_DLL_EXPORT static void connectionType(ConnectionTypeEnum type) { s_connectionType = type; }

	GAMESUP_DLL_EXPORT static bool dialogPause(void) { return s_dialogPause; }
	GAMESUP_DLL_EXPORT static void dialogPause(bool p) { s_dialogPause = p; }

};





#endif
