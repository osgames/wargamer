/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GAMEBASE_HPP
#define GAMEBASE_HPP

#ifndef __cplusplus
#error gamebase.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Base for Campaign or Battle Game
 *
 *----------------------------------------------------------------------
 */

#include "systick.hpp"
#include "savegame.hpp"

// Forward References

class GameOwner;
struct MP_MSG_CAMPAIGNORDER;
struct MP_MSG_BATTLEORDER;

/*
 * A GameBase is something that the outer level of the game has control of.
 * For example, the frontend, campaign game and battle game are all derived
 * from GameBase.
 */

class GameBase
{
	public:
        GameBase() { }

		virtual ~GameBase() = 0;
		virtual void pauseGame(bool pauseMode) = 0;
		virtual void addTime(SysTick::Value ticks) = 0;
		// virtual void finishBattle() = 0;

      virtual bool saveGame(bool prompt) const = 0;
      virtual void saveGame(const char* fileName) const = 0;

	  virtual void doNewGame(const char * current, SaveGame::FileFormat format) = 0;

	  // (for Slave) : tell logic that we've been told to advance the game to this time
	  virtual void syncSlave(SysTick::Value ticks) = 0;

	  // (for Master) : signals that the Slave has acknowledged the specified time-sync message & therefore we can continue with game-time
	  virtual void timeSyncAcknowledged(SysTick::Value ticks) = 0;

	  // (for Master) : signals that the slave is ready to start syncing
	  virtual void slaveReady(void) = 0;


	  virtual void processBattleOrder(MP_MSG_BATTLEORDER * msg) = 0;
	  virtual void processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg) = 0;

    private:
};

inline GameBase::~GameBase() { }

/*
 * GameOwner is someone that holds a pointer to a game
 * In Wargamer, this is the outer Application and the Campaign Game
 */


class GameOwner
{
    public:
        GameOwner() { }
        virtual ~GameOwner() = 0;

        virtual HWND hwnd() const = 0;
        virtual void requestSaveGame(bool prompt) = 0;

		// tells app that connection has been established Ok
		virtual void multiplayerConnectionComplete(bool isMaster) = 0;

		// (for Master) : sends time advancement message to Slave machine
		virtual void sendTimeSync(SysTick::Value ticks, unsigned int checksum) = 0;

		// (for Slave) : signals that the slave is ready to start syncing
		virtual void sendSlaveReady(void) = 0;
};

inline GameOwner::~GameOwner()
{
}

class GameBasePtr : virtual public GameOwner
{
    public:
        GameBasePtr(GameBase* game) : d_game(game) { }
        virtual ~GameBasePtr() = 0;

        void setGame(GameBase* game);

        virtual void pauseGame(bool paused);
		  virtual void addTime(SysTick::Value ticks);

        virtual void requestSaveGame(bool prompt);
        virtual void saveGame(const char* fileName) const;

		virtual void doNewGame(const char * current, SaveGame::FileFormat format);
		virtual void syncSlave(SysTick::Value ticks);
		virtual void timeSyncAcknowledged(SysTick::Value ticks);
		virtual void slaveReady(void);

		virtual void processBattleOrder(MP_MSG_BATTLEORDER * msg);
		virtual void processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg);

    private:
        GameBase* d_game;
};

inline GameBasePtr::~GameBasePtr()
{
    delete d_game;
}

inline void GameBasePtr::setGame(GameBase* game)
{
    delete d_game;
    d_game = game;
}

inline void GameBasePtr::pauseGame(bool paused)
{
    if(d_game)
        d_game->pauseGame(paused);
}

inline void GameBasePtr::addTime(SysTick::Value ticks)
{
    if(d_game)
        d_game->addTime(ticks);
}

inline void GameBasePtr::requestSaveGame(bool prompt)
{
    if(d_game)
        d_game->saveGame(prompt);
}

inline void GameBasePtr::saveGame(const char* fileName) const
{
   if(d_game)
      d_game->saveGame(fileName);
}

inline void GameBasePtr::doNewGame(const char * current, SaveGame::FileFormat format) {

	if(d_game)
		d_game->doNewGame(current, format);
}

inline void GameBasePtr::syncSlave(SysTick::Value ticks) {

	if(d_game)
		d_game->syncSlave(ticks);
}

inline void GameBasePtr::timeSyncAcknowledged(SysTick::Value ticks) {

	if(d_game)
		d_game->timeSyncAcknowledged(ticks);
}

inline void GameBasePtr::slaveReady(void) {

	// wait until game it created
	// this one's important !
	while(!d_game) { }
	d_game->slaveReady();
}

inline void GameBasePtr::processBattleOrder(MP_MSG_BATTLEORDER * msg)
{
   if(d_game)
      d_game->processBattleOrder(msg);
}

inline void GameBasePtr::processCampaignOrder(MP_MSG_CAMPAIGNORDER *msg)
{
   if(d_game)
      d_game->processCampaignOrder(msg);
}

#endif /* GAMEBASE_HPP */

