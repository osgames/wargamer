/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CONTROL_HPP
#define CONTROL_HPP

#ifndef __cplusplus
#error control.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Who controls each side
 *
 * I had to change the name of this so as not to conflict with gamectrl.hpp
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "gamesup.h"

class FileReader;
class FileWriter;

class GamePlayerControl {
public:

	/*
	 * Controller defines who can control a nation
	 */

	enum Controller {
		Player,
		AI,
		Remote,

        ControllerCount
	};

	 GAMESUP_DLL_EXPORT static void setControl(Side side, Controller value);
	 GAMESUP_DLL_EXPORT static Controller getControl(Side side);
     GAMESUP_DLL_EXPORT static Side getSide(Controller control);

	 GAMESUP_DLL_EXPORT static Boolean readData(FileReader& f);
	 GAMESUP_DLL_EXPORT static Boolean writeData(FileWriter& f);

    GAMESUP_DLL_EXPORT static bool canControl(Side side);
    GAMESUP_DLL_EXPORT static Side getDefaultSide();
};



#endif /* CONTROL_HPP */

