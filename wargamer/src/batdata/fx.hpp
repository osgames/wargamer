/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FX_HPP
#define FX_HPP

#include "bd_dll.h"
#include "batcord.hpp"
#include <map>
#include <vector>

/*-------------------------------------------------------------------------------------------------------------------

        File : FX
        Description : Classes for defining spot effects on battlefield
        
-------------------------------------------------------------------------------------------------------------------*/

/*
 * Modified: Steven
 * Moved this stuff from batdisp\animctrl so that BATDATA can be independent
 * of BATDISP.
 */

enum EffectTypeEnum {

    EFFECT_NONE,
    EFFECT_DEADUNIT,
    EFFECT_FLAG,
    EFFECT_FLAGFLAPPING,
    EFFECT_EXPLOSION_LONG,
	EFFECT_EXPLOSION_SHORT,

    MAX_EFFECTS

};



/*
Basic information about a single spot effect
*/
class EffectDisplayInfo {

    public:

        EffectTypeEnum Type;
        unsigned char Flags;
        unsigned int SpriteIndex;
        unsigned int FrameCounter;
        unsigned int FrameTimer;
        unsigned int Duration;
		unsigned int LastTime;
        float Scale;
       BattleLocation Pos;

       EffectDisplayInfo(void) {
            Type = EFFECT_NONE;
            FrameCounter = 0;
            FrameTimer = 0;
            SpriteIndex = 0; // change this to the first graphic index in effects sprites
            Duration = 1;
            Scale = 1.0;
			LastTime = 0;
        }

       EffectDisplayInfo(EffectTypeEnum type, BattleLocation pos) {
            Type = type;
            FrameCounter = 0;
            FrameTimer = 0;
            SpriteIndex = 0; // change this to the first graphic index in effects sprites
            Pos = pos;
            Duration = 1;
            Scale = 1.0;
			LastTime = 0;
        }

        EffectDisplayInfo(EffectTypeEnum type, unsigned int base_index, BattleLocation pos) {
            Type = type;
            FrameCounter = 0;
            FrameTimer = 0;
            SpriteIndex = base_index;
            Pos = pos;
            Duration = 1;
            Scale = 1.0;
			LastTime = 0;
        }


        ~EffectDisplayInfo(void) { }


        /*
         * File Interface
         */

        Boolean readData(FileReader& f);
        Boolean writeData(FileWriter& f) const;
		static const UWORD s_fileVersion;
};

inline bool operator < ( const EffectDisplayInfo& l, const EffectDisplayInfo& r) { return true; }
inline bool operator == ( const EffectDisplayInfo& l, const EffectDisplayInfo& r) { return false; }


#define EFFECT_DURATION_INFINITE 1
#define EFFECT_DURATION_TIMED 2
#define EFFECT_DURATION_REPEATS 4


/*
Array of spot effects active in a particular hex on battlefield
*/
class HexEffectsInfo {

    public:
       typedef std::vector<EffectDisplayInfo> EffectsInHex;

    private:
        EffectsInHex d_Effects;

    public:
        // constructor
        HexEffectsInfo(void) {
            d_Effects.erase(d_Effects.begin(), d_Effects.end() );
        }

        // destructor
        ~HexEffectsInfo(void) { }

        // add a spot effect to effects in this hex
        void add(EffectDisplayInfo fx) {
            d_Effects.push_back(fx);
        }

        // retrieve the effects in this hex
        EffectsInHex & Effects(void) { return d_Effects; }

        /*
         * File Interface
         */

        Boolean readData(FileReader& f);
        Boolean writeData(FileWriter& f) const;
		static const UWORD s_fileVersion;

};

inline bool operator < ( const HexEffectsInfo& l, const HexEffectsInfo& r) { return true; }
inline bool operator == ( const HexEffectsInfo& l, const HexEffectsInfo& r) { return false; }



/*
List of effects on battlefield
*/
class BATDATA_DLL EffectsList {

      typedef std::map<BattleMeasure::HexCord, HexEffectsInfo, BattleMeasure::HexCordCompare> EffectsListType;
      EffectsListType d_effects;

    public:

      EffectsList(void) { }
      ~EffectsList(void) { }

      // add an effect to the list
      void add(const BattleMeasure::HexCord & hex, EffectDisplayInfo & fx);

      // get effects in a specified hex
      HexEffectsInfo * get(const BattleMeasure::HexCord& hex);

      // remove all effects from a specified hex
      void remove(const BattleMeasure::HexCord& hex);

      // clears the effects list
      void clear(void);

      /*
      * File Interface
      */

      Boolean readData(FileReader& f);
      Boolean writeData(FileWriter& f) const;
      static const UWORD s_fileVersion;
      static const char s_fileID[];
};




#endif
