/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef KEY_TERR_HPP
#define KEY_TERR_HPP

#include "sllist.hpp"
#include "batcord.hpp"
#include "hexdata.hpp"


// using namespace BattleMeasure;

/*
        List of key-points in a randomly generated battlefield
*/






class KeyTerrainPoint : public SLink {
public:

enum KeyTerrainPointEnum {
    Junction,
    CrossRoads,
    Bridge,
    Village,
    Town,
    Farm,
    HighGround,
	PathExitingMap
};

private:
    KeyTerrainPoint::KeyTerrainPointEnum d_type;
    BattleMeasure::HexCord d_hex;

public:



    void type(KeyTerrainPointEnum t) { d_type = t; }
    KeyTerrainPointEnum type(void) { return d_type; }

    void hex(const BattleMeasure::HexCord& h) { d_hex = h; }
    BattleMeasure::HexCord hex(void) { return d_hex; }

    char * getDescription(void) {
        switch(d_type) {
            case Junction : { return "Junction"; }
            case CrossRoads : { return "CrossRoads"; }
            case Bridge : { return "Bridge"; }
            case Village : { return "Village"; }
            case Town : { return "Town"; }
            case Farm : { return "Farm"; }
            case HighGround : { return "HighGround"; }
			case PathExitingMap : { return "PathExitingMap"; }
            default : { return "UnknownType !"; }
        }
    }
    
};




class KeyTerrainList {

    SList <KeyTerrainPoint> d_list;

public:

    BATDATA_DLL SList <KeyTerrainPoint> * get(void) { return &d_list; }
    bool add(KeyTerrainPoint * node);
    BATDATA_DLL void crop(const BattleMeasure::HexCord& bottom_left, const BattleMeasure::HexCord& top_right);
    BATDATA_DLL void checkEdges(BattleMap * map);
	BATDATA_DLL void labelKeyTerrain(BattleMap * map) ;
    BATDATA_DLL void check(void);
};


#endif
