/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Multimap of objects on the map
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "hexmap.hpp"
#include "batunit.hpp"
#include <utility>      // for !=

using namespace BattleMeasure;

#ifdef _MSC_VER   // otherwise != doesn't work
using namespace std::rel_ops;
#endif


BattleHexMap::BattleHexMap()
{
}

BattleHexMap::~BattleHexMap()
{
   ASSERT(d_items.size() == 0);  // Should have got deleted by something higher up
}

void BattleHexMap::clear(const BattleMeasure::HexCord & mapsize)
{
   for( int y=0; y<mapsize.y(); y++ )
   {
      for( int x=0; x<mapsize.x(); x++ )
      {
         BattleMeasure::HexCord hex(x,y);
         BattleHexMap::iterator lower_iter;
         BattleHexMap::iterator upper_iter;

         while( find(hex, lower_iter, upper_iter) ) 
            remove(lower_iter);
      }
   }
}

void BattleHexMap::clear()
{
   d_items.clear();
}

// return iterators to cover objects in a given hex

bool BattleHexMap::find(const HexCord& hex, iterator& lower, iterator& upper)
{
   std::pair<iterator,iterator> bounds = d_items.equal_range(hex);

        lower = bounds.first;   // d_items.lower_bound(hex);
        upper = bounds.second;  // d_items.upper_bound(hex);
        return lower != upper;
}

bool BattleHexMap::find(const HexCord& hex, const_iterator& lower, const_iterator& upper) const
{
   std::pair<Container::const_iterator,Container::const_iterator> bounds = d_items.equal_range(hex);

        lower = bounds.first;   // d_items.lower_bound(hex);
        upper = bounds.second;  // d_items.upper_bound(hex);

      ASSERT(d_items.lower_bound(hex) == bounds.first);
      ASSERT(d_items.upper_bound(hex) == bounds.second);

        return lower != upper;
}

// find a unit (uses virtual hex() function

BattleHexMap::iterator BattleHexMap::find(const BattleUnit* unit)
{
        iterator lower;
        iterator higher;
        find(unit->hex(), lower, higher);
        while(lower != higher)
        {
                if((*lower).second == unit)
                        return lower;
                ++lower;
        }
        return d_items.end();
}



bool
BattleHexMap::isUnitPresent(const BattleUnit* unit)
{
        iterator lower;
        iterator higher;
        find(unit->hex(), lower, higher);
        while(lower != higher)
        {
                if((*lower).second == unit)
                        return true;
                ++lower;
        }
        return false;
}


// update an object to whatever position hex() is

void BattleHexMap::move(iterator from)
{
        BattleUnit* unit = (*from).second;
        const HexCord& newHex = unit->hex();
        if(newHex != (*from).first)
        {
                Container::iterator insertPosition = from.m_it;
                ++insertPosition;

                d_items.erase(from.m_it);
                const Container::value_type value(newHex, unit);
                d_items.insert(insertPosition, value);
        }
}

// remove an object
#if 0
void BattleHexMap::remove(iterator from)
{
  d_items.erase(from);
}
#endif
/*
 * Add a new object
 */

void BattleHexMap::add(BattleUnit* unit)
{
#ifdef _MSC_VER
   ASSERT(unit != reinterpret_cast<BattleUnit*>(0xcdcdcdcd));
   ASSERT(unit != reinterpret_cast<BattleUnit*>(0xdddddddd));
#endif
   ASSERT(unit != 0);
#ifdef DEBUG
   iterator it = find(unit);
   ASSERT(it == d_items.end());
#endif
   d_items.insert(d_items.begin(), Container::value_type(unit->hex(), unit));
}

BattleHexMap::iterator::iterator(const iterator& it) : 
   m_it(it.m_it) 
{ 
}

BattleHexMap::iterator::iterator(const Container::iterator it) :
   m_it(it)
{
}

BattleHexMap::iterator& BattleHexMap::iterator::operator=(const iterator& it) 
{
   m_it = it.m_it; 
   return *this; 
}

BattleHexMap::iterator& BattleHexMap::iterator::operator=(const Container::iterator& it) 
{
   m_it = it; 
   return *this; 
}


BattleHexMap::iterator::iterator() : 
   m_it() 
{ 
}

BattleHexMap::iterator& BattleHexMap::iterator::operator++()
{
   ++m_it;
   return *this;
}

BattleHexMap::Container::reference BattleHexMap::iterator::operator*()
{
   return *m_it;
}

bool BattleHexMap::iterator::operator==(const BattleHexMap::iterator& it) const
{
   return m_it == it.m_it;
}

BattleHexMap::const_iterator::const_iterator(const const_iterator& it) : 
   m_it(it.m_it) 
{ 
}

BattleHexMap::const_iterator::const_iterator(const Container::const_iterator& it) :
   m_it(it)
{
}

BattleHexMap::const_iterator& BattleHexMap::const_iterator::operator=(const const_iterator& it) 
{
   m_it = it.m_it; 
   return *this; 
}

BattleHexMap::const_iterator& BattleHexMap::const_iterator::operator=(const Container::const_iterator& it) 
{
   m_it = it; 
   return *this; 
}


BattleHexMap::const_iterator::const_iterator() : 
   m_it() 
{ 
}

BattleHexMap::const_iterator& BattleHexMap::const_iterator::operator++()
{
   ++m_it;
   return *this;
}

BattleHexMap::Container::const_reference BattleHexMap::const_iterator::operator*()
{
   return *m_it;
}

bool BattleHexMap::const_iterator::operator==(const BattleHexMap::const_iterator& it) const
{
   return m_it == it.m_it;
}







/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
