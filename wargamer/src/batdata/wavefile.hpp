/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef WAVEFILE_HPP
#define WAVEFILE_HPP

#include <windows.h>
#include <windowsx.h>
#include <mmsystem.h>

typedef struct tagWAVEFILE
  {
  DWORD           cbSize;                // Size of file
  LPWAVEFORMATEX  pwfxInfo;              // Wave Header
  LPBYTE          pbData;                // Wave Bits
  }
WAVEFILE, *LPWAVEFILE;

// Function Prototypes  
BOOL wave_ParseWaveMemory(void *pvRes, WAVEFORMATEX **ppWaveHeader, BYTE **ppbWaveData, DWORD *pcbWaveSize);
LPVOID WAVE_LoadResource(LPSTR lpName, HINSTANCE hModule, LPWAVEFILE pWaveFile);
LPVOID WAVE_LoadFile(LPSTR szFileName, LPWAVEFILE pWaveFile);

#endif
