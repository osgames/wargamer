/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bordutil.hpp"
#include "batdata.hpp"
#include "bob_cp.hpp"
#include "batarmy.hpp"
#include "hexmap.hpp"
#include "bobutil.hpp"
#include "bobiter.hpp"

using namespace BOB_Definitions;
using namespace BattleMeasure;

//----------- Unchecked update
bool B_OrderUtil::canGiveType(RCPBattleData bd, const BattleOrderInfo::OrderMode& type,
   const RefBattleCP& cp, const BattleMeasure::HexCord* hex)
{
  ASSERT(type < BattleOrderInfo::OM_HowMany);

  // convert type to local enum
  enum {
     InfCav,
     Arty,
     HighCommand,

     UnitType_HowMany
  } unitType = (cp->getRank().sameRank(Rank_Division) && (cp->generic()->isInfantry() || cp->generic()->isCavalry())) ?
                    InfCav :
               (cp->getRank().sameRank(Rank_Division) && cp->generic()->isArtillery()) ?
                    Arty : HighCommand;

  bool attached = (cp->getRank().sameRank(Rank_Division)) ?
        (cp->parent() != bd->ob()->getTop(cp->getSide())) : cp->attached();

  switch(type)
  {
    case BattleOrderInfo::Hold:
    case BattleOrderInfo::RestRally:
    case BattleOrderInfo::Move:
    case BattleOrderInfo::Rout:
      return True;

    case BattleOrderInfo::Attach:
    {
      if(!attached)
      {
        if(hex)
        {
          const BattleHexMap* hexMap = bd->hexMap();

          BattleHexMap::const_iterator lower;
          BattleHexMap::const_iterator upper;
          if(hexMap->find(*hex, lower, upper))
          {
            const BattleUnit* unit = hexMap->unit(lower);
            CRefBattleCP uCP = BobUtility::getCP(unit);

            if(cp->getRank().isHigher(Rank_Division))
            {
               // a leader can only attached to a subordinate in his chain of command
              for(BattleUnitIter cpIter(cp); !cpIter.isFinished(); cpIter.next())
              {
                if(cpIter.cp() == uCP)
                  return True;
              }

            }
            else
            {
              return (uCP->getRank().isHigher(cp->getRank().getRankEnum()) &&
                 uCP->getSide() == cp->getSide());
            }
          }
        }
      }

      else
        return False;
    }

    case BattleOrderInfo::Detach:
    {
      return attached;
    }

    case BattleOrderInfo::Bombard:
    {
       if(unitType == Arty)
          return (cp->spFormation() == SP_UnlimberedFormation);
       else
          return (cp->nArtillery() > 0 && cp->holding());
    }
   }

#if 0
  // TODO: move this table to scenario tables
  bool attached = (cp->getRank().sameRank(Rank_Division)) ?
     (cp->parent() != bd->ob()->getTop(cp->getSide())) : cp->attached();

  const c_nAttach = 2;

  if(hex)
  {
    const BattleHexMap* hexMap = batData->hexMap();

    BattleHexMap::const_iterator lower;
    BattleHexMap::const_iterator upper;
    if(hexMap->find(iter.hex(), lower, upper))
    {
      const BattleUnit* unit = hexMap->unit(lower);
      CRefBattleCP uCP = BobUtility::getCP(unit);

      overFriendly = (uCP != cp && uCP->getSide() ==

  }

  static const bool s_table[c_nAttach][BattleOrderInfo::OM_HowMany][UnitType_HowMany] = {
    // not attached
    {
      { True,  True,  True  }, // Hold,
      { True,  True,  True  }, // Move,
      { True,  True,  True  }, // RestRally,
      { True,  True,  False }, // Attach,
      { False, False, False }, // Detach,
//    { False, False, True  }, // AttachLeader,
//    { False, False, False }, // DetachLeader,
      { False, True,  False }, // Bombard,
      { True,  True,  True  }  // Rout,
    },
    // attached
    {
      { True,  True,  True  }, // Hold,
      { True,  True,  True  }, // Move,
      { True,  True,  True  }, // RestRally,
      { False, False, False }, // Attach,
      { True,  True,  False }, // Detach,
//    { False, False, False }, // AttachLeader,
//    { False, False, True  }, // DetachLeader,
      { False, True,  False }, // Bombard,
      { True,  True,  True  }  // Rout,
    }
  };

  return s_table[attached][type][unitType];
#endif
  return False;
}
// ------------ end update

bool B_OrderUtil::canDeployThisWay(RCPBattleData bd, CPDeployHow dh, const RefBattleCP& cp)
{
  switch(dh)
  {
    // you can always deploy left-or right
    case CPD_DeployLeft:
    case CPD_DeployRight:
    {
      return True;
    }

    // you can deploy center if
    case CPD_DeployCenter:
    {
      // a single column cannot deploy to the center
      if(cp->columns() == 1)
        return False;

      // if rows is greater than 3 we can
      // if rows == 2 and columns is greater than 2
      if( (cp->rows() > 3) ||
          (cp->rows() == 2 && cp->columns() > 2) )
      {
        return True;
      }

      return False;
    }

#ifdef DEBUG
    default:
      FORCEASSERT("Type not found");
#endif
  }

  return False;
}
#if 0
bool B_OrderUtil::canSPDeployThisWay(RCPBattleData bd, const CRefBattleCP& cp, SPFormation sf, BasicUnitType::value t)
{
   enum LUnitType {
      Inf,
      LCav,
      HCav,
      FArt,
      HvyArt,
      HorseArt
   } ltype = (t == BasicUnitType::Infantry) ? Inf :
             (t == BasicUnitType::Cav && !cp->generic()->isHeavyCavalry()) ? LCav :
             (t == BasicUnitType::Cav && !cp->generic()->isHeavyCavalry());
   
      for(int c = 0; c < cp->columns(); c++)
      {
         int index = maximum(0, (cp->columns() * 0) + c);
         DeployItem& di = map[index];

         if(!di.active() || !di.d_sp->isMoving())
          continue;

         HexItem* nextHex = (di.d_sp->routeList().entries() > 1) ?
                  reinterpret_cast<HexItem*>(di.d_sp->nextHex()->next) : 0;

         if(nextHex)
         {
            const BattleTerrainHex& hexInfo = d_batData->getTerrain(nextHex->d_hex);
            canGoOn = static_cast<bool>(tTable.getValue(unitType, hexInfo.d_terrainType, lf));

            // if terrain type is not prohibited, check path type (i.e. Rivers)
            if(canGoOn)
            {
               const Table3D<UWORD>& pTable = BattleTables::pathModifiers();
               // get path modifiers (i.e streams, rivers)

               // If we have a water obstacle
               if( (pTable.getValue(unitType, hexInfo.d_path1.d_type, lf) == 0) ||
                     (pTable.getValue(unitType, hexInfo.d_path2.d_type, lf) == 0) )
               {
                  canGoOn = False;
               }
            }

}
#endif
bool B_OrderUtil::canSPDeployThisWay(RCPBattleData bd, SPFormation sf, BasicUnitType::value t)
{
  ASSERT(sf < SP_Formation_HowMany);
  ASSERT(t < BasicUnitType::Special);

  switch(t)
  {
    case BasicUnitType::Infantry:
    case BasicUnitType::Cavalry:
    {
      static Boolean s_table[SP_Formation_HowMany][BasicUnitType::Cavalry + 1] = {
           // Inf        Cav
          {  True,      True  }, //  CoR
          {  True,      True  }, //  AC
          {  True,      False }, //  CC
          {  True,      True  }, //  LN
          {  True,      False }  //  SQ
      };

      return s_table[sf][t];
    }

    case BasicUnitType::Artillery:
    {
      return (sf == SP_LimberedFormation || sf == SP_UnlimberedFormation);
    }
  }

  return False;
}

bool B_OrderUtil::isMoveOrder(BattleOrderInfo::OrderMode& type)
{
  ASSERT(type < BattleOrderInfo::OM_HowMany);

  // TODO: move this table to scenario tables
  static const bool s_table[BattleOrderInfo::OM_HowMany] = {
     False, // Hold,
     True,  // Move,
     False, // RestRally,
     False,  // Attach,
     False, // Detach,
//   True,  // AttachLeader,
//   True,  // DetachLeader,
     False, // Bombard,
       False   // Rout,
  };

  return s_table[type];
}

const WayPoint* B_OrderUtil::nextToLastWayPoint(const BattleOrder& order)
{
  ASSERT(const_cast<BattleOrder&>(order).nextWayPoint());
  if(!const_cast<BattleOrder&>(order).nextWayPoint())
    return 0;

  WayPoint* last = order.wayPoints().getLast();
  WayPoint* lastWay = 0;

  SListIterR<WayPoint> iter(&order.wayPoints());
  while(++iter)
  {
    if(iter.current()->next &&
       iter.current()->next == last)
    {
      return iter.current();
//    break;
    }
  }

  return 0;
}

bool B_OrderUtil::lastMoveAttribs(const BattleOrder& order, CPFormation& lastFormation, HexPosition::Facing& lastFacing)
{
  if(const_cast<BattleOrder&>(order).nextWayPoint())
  {
    if(order.wayPoints().entries() > 1)
    {
      const WayPoint* last = nextToLastWayPoint(order);
      ASSERT(last);
      if(last)
      {
        lastFacing = last->d_order.d_facing;
        lastFormation = last->d_order.d_divFormation;
        return True;
      }
    }
    else
    {
      lastFacing = order.facing();
      lastFormation = order.divFormation();
      return True;
    }
  }

  return False;
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
