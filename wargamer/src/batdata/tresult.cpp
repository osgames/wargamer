/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Tactical Results
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "tresult.hpp"
#ifdef DEBUG
#include "random.hpp"
#endif
#include "scenario.hpp"
#include "batdata.hpp"
#include "bobiter.hpp"
#include "wg_rand.hpp"


#ifdef DEBUG
#include "clog.hpp"
LogFile tlog("TacticalResults.log");
#endif

using namespace BattleMeasure;

/*
 * Tactical Results functions
 */

TacticalResults::TacticalResults(const BattleFinish& f) :
#ifdef DEBUG
    d_title("Untitled"),
#endif
    BattleResults(f)
{
}

TacticalResults::~TacticalResults()
{
}

/*
 * calculate results
 */

void TacticalResults::calculate(const BattleData* batData)
{
    const BattleOB* ob = batData->ob();

    float cvp[2];     // Combat Victory Points

    for(Side side = 0; side < scenario->getNumSides(); ++side)
    {
        BattleLosses* loss = losses(side);

        // Reset results

        loss->reset();
        cvp[side] = 0;

        // Set top Units

        d_topUnit[side] = ob->getTop(side);

        /*
         * Iterate through OB and fill in losses & startStrength
         *
         * TODO: Add cvp values for dead leaders:
         *      Div   +2
         *      Corps +4
         *      Army  +8
         */

        for(TConstBattleUnitIter<BUIV_All> obIter(ob, side);
            !obIter.isFinished();
            obIter.next())
        {
            CRefBattleCP cp = obIter.cp();

            // if(cp->hasFled() || cp->isDead())

            for(TConstBattleSPIter<SPValidateAll> spIter(ob, cp, SPValidateAll());
                !spIter.isFinished();
                spIter.next())
            {
                CRefBattleSP sp = spIter.sp();

                const UnitTypeItem& uti = ob->getUnitType(sp);
                BasicUnitType::value ut = uti.getBasicType();

                int totalMen = UnitTypeConst::menPerType(ut);
                int endMen = totalMen;

                loss->addStartStrength(ut, totalMen);

                if(sp->hasFled() || sp->isDead())
                {
                    endMen = 0;
                }
                else
                {
                    endMen = sp->strength(totalMen);
                }

                if(totalMen != endMen)
                {
                    loss->addLosses(ut, totalMen - endMen);

                    // Update cvp

                    float lossStrength = float(BattleSP::SPStrength::MaxValue - sp->strength()) / float(BattleSP::SPStrength::MaxValue);

                    if(batData->type() == BattleData::Historical)
                    {
                       cvp[side] += lossStrength * uti.combatVictoryValue();
                    }
                    else
                    {
                       cvp[side] += lossStrength;
                    }

                }
            }
        }
    }

    calcVictoryLevel(batData, cvp);
    continueTest(batData);

#ifdef DEBUG
    tlog.close();
#endif

}

/*
 * If it is the end of day then a victory level must be calculated
 * See Ben's document "Winning & Losing Non Campaign Battles"
 */

void TacticalResults::calcVictoryLevel(const BattleData* bd, float cvp[2])
{
#ifdef DEBUG
   tlog << "Calculating Victory Level:" << endl;
#endif

   if(bd->type() == BattleData::Historical)
   {
      if(why() == EndDay)
      {
         int level0 = Draw;
         int level1 = Draw;

         // TODO; Get Victory Location Points
         int victory0 = 0;
         int victory1 = 0;

         HexCord bLeft;
         HexCord mSize;
         bd->getPlayingArea(&bLeft, &mSize);
         HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

         for(int y = bLeft.y(); y < tRight.y(); y++)
         {
            for(int x = bLeft.x(); x < tRight.x(); x++)
            {
               HexCord hex(x, y);
               const BattleTerrainHex& hexInfo = bd->getTerrain(hex);
               if(hexInfo.d_victoryPoints > 0)
               {
                  if(hexInfo.d_victorySide == 0)
                     victory0 += hexInfo.d_victoryPoints;
                  else if(hexInfo.d_victorySide == 1)
                     victory1 += hexInfo.d_victoryPoints;
               }
            }
         }

#ifdef DEBUG
         tlog << "Victory = " << victory0 << "/" << victory1 << endl;
         tlog << "Combat = " << cvp[0] << "/" << cvp[1] << endl;
#endif

         if((victory0 != 0) && (victory1 == 0))
            level0 = MinorVictory;
         else if((victory1 != 0) && (victory0 == 0))
            level1 = MinorVictory;
         else if(victory0 >= (victory1 * 2))
            level0 = WinningDraw;
         else if(victory1 >= (victory0 * 2))
            level1 = WinningDraw;

         if(cvp[0] >= (cvp[1] * 2))
            ++level1;
         else if(cvp[1] >= (cvp[0] * 2))
            ++level0;

         int lossDiff = losses(0)->lossPercent() - losses(1)->lossPercent();
         if(lossDiff >= 50)
            level0 -= 2;
         else if(lossDiff >= 25)
            level0 -= 1;
         else if(lossDiff <= -50)
            level1 -= 2;
         else if(lossDiff <= -25)
            level1 -= 1;

#ifdef DEBUG
         tlog << "Results are: " << level0 << "/" << level1 << endl;
#endif

         if(level0 == level1)
         {
            level(Draw);
            winner(SIDE_Neutral);
         }
         if(level0 >= level1)
         {
            level0 -= level1;
            level0 = minimum(level0, VictoryLevel_HowMany-1);
            level(static_cast<VictoryLevel>(level0));
            winner(0);
         }
         else
         {
            level1 -= level0;
            level1 = minimum(level1, VictoryLevel_HowMany-1);
            level(static_cast<VictoryLevel>(level1));
            winner(1);
         }
      }
      else
         level(MajorVictory);
   }
   else  // Campaign Battle
   {
      int vLevel[2] = { Draw, Draw };

      if(why() != EndDay)
      {
         vLevel[winner()] = MinorVictory;
      }

      if(cvp[0] > (2 * cvp[1]))
         vLevel[1] += 2;
      else if((cvp[0] * 2) > (cvp[1] * 3))
         vLevel[1] += 1;
      else if(cvp[1] > (2 * cvp[0]))
         vLevel[0] += 2;
      else if((cvp[1] * 2) > (cvp[0] * 3))
         vLevel[0] += 1;

#ifdef DEBUG
      tlog << "Campaign Battle cvp: " << vLevel[0] << "/" << vLevel[1] << endl;
#endif

      if(why() != EndDay)
      {
         if(winner() == 0)
            vLevel[0] = minimum(vLevel[0], vLevel[1] + MinorVictory);
         else if(winner() == 1)
            vLevel[1] = minimum(vLevel[1], vLevel[0] + MinorVictory);

#ifdef DEBUG
         tlog << "Results due to retreat are: " << vLevel[0] << "/" << vLevel[1] << endl;
#endif
      }

      if(vLevel[0] == vLevel[1])
      {
         level(Draw);
         winner(SIDE_Neutral);
      }
      if(vLevel[0] >= vLevel[1])
      {
         vLevel[0] -= vLevel[1];
         vLevel[0] = minimum(vLevel[0], VictoryLevel_HowMany-1);
         level(static_cast<VictoryLevel>(vLevel[0]));
         winner(0);
      }
      else
      {
         vLevel[1] -= vLevel[0];
         vLevel[1] = minimum(vLevel[1], VictoryLevel_HowMany-1);
         level(static_cast<VictoryLevel>(vLevel[1]));
         winner(1);
      }
   }


#ifdef DEBUG

   static const char* strVictory[] = {
        "Draw",
        "Winning Draw",
        "Minor Victory",
        "Major Victory",
        "Crushing Victory",
    };

   tlog << "Result is " << scenario->getSideName(winner()) << " " << strVictory[level()] << endl;
#endif
}

/*
 * Battle can continue if it is the end of day and the victorylevel is a draw
 *
 * If side's morale is greater than 65 or it is a SHQ then t will continue
 * otherwise a test is made of the leader's aggressions and charisma and supply
 *
 * Both sides must want to continue.
 */

void TacticalResults::continueTest(const BattleData* bd)
{
    // default to withdrawing

    shouldContinue(Withdraw);

    if(bd->type() == BattleData::Historical && bd->shouldEnd())
      return;

    const BattleOB* ob = bd->ob();
    if((why() == EndDay) && (level() == Draw || level() == MinorVictory || level() == WinningDraw))
    {
#ifdef DEBUG
        tlog << "------------" << endl;
        tlog << "ContinueTest" << endl;
#endif

        for(Side side = 0; side < scenario->getNumSides(); ++side)
        {
            CRefBattleCP cp = ob->getTop(side);
            ConstRefGLeader leader = cp->leader();

            bool isSHQ = leader->isSupremeLeader();

#ifdef DEBUG
            tlog << "Testing ";
            if(isSHQ)
                tlog << "SQH ";
            tlog << leader->getName() << " of " << cp->getName() << endl;
            tlog << "Morale = " << (int) cp->morale() << endl;
#endif

            if( (cp->morale() < 65) && !isSHQ)
            {
                int value = (leader->getAggression(true) + leader->getCharisma() / 2) / 4;
                value = (value * cp->generic()->supply()) / 256;
                int r = CRandom::get(100);

#ifdef DEBUG
                tlog << "Agression = " << (int)leader->getAggression() << endl;
                tlog << "Charisma = " << (int)leader->getCharisma() << endl;
                tlog << "Supply = " << (int)cp->generic()->supply() << endl;
                tlog << "percentage = " << value << endl;
                tlog << "Random = " << r << endl;
#endif

                if(r > value)   // failed test, so battle will not continue
                {
#ifdef DEBUG
                    tlog << "Failed Test: Battle will not continue" << endl;
#endif
                    return;
                }

#ifdef DEBUG
                tlog << "Passed Test: Battle may continue" << endl;
#endif
            }
        }

        // If get to here then battle can continue (subject to player agreeing)

#ifdef DEBUG
        tlog << "Battle Can Continue" << endl;
#endif

        shouldContinue(Continue);
    }
}


ConstRefGLeader TacticalResults::leader(Side side) const
{
    CRefBattleCP cp = d_topUnit[side];

    ASSERT(cp != NoBattleCP);

    if(cp != NoBattleCP)
        return cp->leader();

    return NoGLeader;
}

ConstRefGenericCP TacticalResults::cp(Side side) const
{
    CRefBattleCP cp = d_topUnit[side];

    ASSERT(cp != NoBattleCP);

    if(cp == NoBattleCP)
        return NoGenericCP;

    return cp->generic();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
