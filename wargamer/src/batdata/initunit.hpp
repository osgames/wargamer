/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef INITUNIT_HPP
#define INITUNIT_HPP

#include "bd_dll.h"
// #include "batdata.hpp"
// #include "bobiter.hpp"
#include "batcord.hpp"
#include "unittype.hpp"

class BattleData;
class BattleOB;
class BattleCP;

/*-------------------------------------------------------------------------------------------------------------------

        File : INITUNIT
        Description : Utilities to pass through Battle OB & initialise data
        
-------------------------------------------------------------------------------------------------------------------*/

namespace BatUnitUtils {


        /*
        These constants are also declared in UNITDISP.HPP
        They shoould really reside in a BatDefs file or something
        */

        static const int InfantryPerSprite = 50;
        static const int CavalryPerSprite = 80;
        static const int ArtilleryPerSprite = 3;
        static const int SpecialPerSprite = 20;

        static const int InfantryPerSP = UnitTypeConst::InfantryPerSP;     // 1000 Men
        static const int CavalryPerSP  = UnitTypeConst::CavalryPerSP;      // 500 Horses
        static const int ArtilleryPerSP = UnitTypeConst::GunsPerSP;        // 12 Guns
        static const int SpecialPerSP = UnitTypeConst::SpecialPerSP;       // 250 Men

        static const int imagesPerSP[BasicUnitType::HowMany] = {
            InfantryPerSP / InfantryPerSprite,              // Infantry (16)
            CavalryPerSP / CavalryPerSprite,                        // Cavalry (8)
            ArtilleryPerSP / ArtilleryPerSprite,    // Artillery (4)
            SpecialPerSP / SpecialPerSprite                 // Special (16)
        };

        static const int Half_Infantry_Numbers = imagesPerSP[0] / 2;

        // Leave 10% of hex for edges
        static const BattleMeasure::Distance usedHexWidth = (BattleMeasure::OneXHex * 9) / 10;
        
        static const int InfantryFrontage  = InfantryPerSP / 2; // Number of men who can squeeze in a hex width
        static const int CavalryFrontage   = CavalryPerSP / 2;  // Number of men who can squeeze in a hex width
        static const int ArtilleryFrontage = ArtilleryPerSP;            // Number of men who can squeeze in a hex width
        static const int SpecialFrontage   = SpecialPerSP / 2;  // Number of men who can squeeze in a hex width

        static const BattleMeasure::Distance spacings[BasicUnitType::HowMany] = {
                ( (usedHexWidth * InfantryPerSprite) / InfantryFrontage) + 100,     // Infantry
                (usedHexWidth * CavalryPerSprite)   / CavalryFrontage + 600,  // Cavalry
                (usedHexWidth * ArtilleryPerSprite) / ArtilleryFrontage,        // Artillery
                (usedHexWidth * SpecialPerSprite)       / SpecialFrontage       // Special
        };


    // initialize units number of sprites, & deployment info
    BATDATA_DLL void InitUnits(BattleData * batdata);

    // initialize only units number of sprites
    BATDATA_DLL void InitUnits(BattleOB * ob);
    BATDATA_DLL void InitCommandPositions(BattleCP * cp, BattleOB * ob);
    BATDATA_DLL void InitStrengthPoints(BattleCP * cp, BattleOB * ob);

    

};



#endif
