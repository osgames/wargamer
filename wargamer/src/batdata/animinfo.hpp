/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ANIMINFO_HPP
#define ANIMINFO_HPP

#include "random.hpp"
#include "batcord.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : ANIMINFO
        Description : Class to control battle units' animations
        
-------------------------------------------------------------------------------------------------------------------*/














#define MAX_SPRITES_PER_SP 4



class AnimationInfo {
public:

    unsigned int FrameCounters[MAX_SPRITES_PER_SP];
    // what frame each sprite is on
    unsigned int FrameTimers[MAX_SPRITES_PER_SP];
    // timers for delay between frames
    unsigned int RandomVal;
    // a random value to make formation displays less rigid (from 0 to 100)
    unsigned int RandomFormationVal;
    // previous battle-tick to time frame updatews by (1/10th sec resolution)
    unsigned int LastTime;
    // previous number of sprites on display
    unsigned int LastNumSprites;
    // whether unit was firing last game cycle
    unsigned int LastFiringState;
	// flag frame no
    unsigned int FlagFrameCounter;
	// flag frame timer
    unsigned int FlagFrameTimer;

   


    AnimationInfo(void) {

        int f;
        
        for(f=0;f<MAX_SPRITES_PER_SP;f++) {

            FrameCounters[f] = 0;
            FrameTimers[f] = 0;
        }

            RandomVal = random(0,100);
            RandomFormationVal = random(0,20);
            LastTime = 0;
            LastFiringState = 0;
            LastNumSprites = 20;
            FlagFrameCounter = 0;
            FlagFrameTimer = 0;
    }

    ~AnimationInfo(void) { }

};



#endif
