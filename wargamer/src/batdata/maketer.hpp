/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MAKETER_HPP
#define MAKETER_HPP

#ifndef __cplusplus
#error maketer.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Random Battlefield creation utility
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "batcord.hpp"
#include "batinfo.hpp"
// #include "batdata.hpp"
#include "BattleDataFwd.hpp"
#include "b_terr.hpp"
#include "pathfind.hpp"
#include "building.hpp"

class BattleMap;
class TerrainTable;

namespace BattleBuildings
{
   class BuildingList;
   class BuildingTable;
};

// using namespace BattleBuildings;
// using namespace BattleMeasure;





namespace BattleCreate {

// Create a random battlefield

    void createRandomBattlefield(
        // Info parameters
        TerrainGenerationParameters * params,
        BattleData * batdata);




// Functions used by editor

   BATDATA_DLL void MakeEmptyMap(BattleMap * map, const BattleMeasure::HexCord& size);
   BATDATA_DLL void ClearWholeMap(BattleMap * map, TerrainType type);
   BATDATA_DLL void ClearAllPaths(BattleMap * map);
   BATDATA_DLL void ClearCoasts(BattleMap * map);
   BATDATA_DLL void ClearCoasts(BattleMap * map);
   BATDATA_DLL void ClearAllEdges(BattleMap *map);
   BATDATA_DLL void SetAllHeights(BattleMap * map, int height);
   BATDATA_DLL void makeCoasts(BattleMap* map, const TerrainTable& terrain);
   BATDATA_DLL void FixDodgyPaths(BattleMap * map);
   BATDATA_DLL void ConstructRoadPath(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * RoadPath, LogicalPathEnum RoadType);
   BATDATA_DLL void ConstructRiverPath(BattleMap * map, const TerrainTable & terrain, SList<PathPoint> * RiverPath, LogicalPathEnum RiverType);
   BATDATA_DLL void CreateTerrainHex(BattleMap * map, const BattleMeasure::HexCord& hex, TerrainType type);
   BATDATA_DLL void EditorCreateTerrainArea(BattleMap * map, const BattleMeasure::HexCord& centre_hex, TerrainType terrain_type, int size, int spread_chance, int mode);
   BATDATA_DLL void AlterHexHeight(BattleMap * map, const BattleMeasure::HexCord& hex, int ammount);
   BATDATA_DLL void AlterHexAreaHeight(BattleMap * map, const BattleMeasure::HexCord& centre_hex, int size, int spread_chance, int ammount);
   BATDATA_DLL void AlterHexClumpHeight(BattleMap * map, const BattleMeasure::HexCord& centre_hex, int size, int density, int ammount);
   BATDATA_DLL void CreateSingleSettlement(const BattleBuildings::BuildingTable * buildingTable, BattleBuildings::BuildingList * buildings, const BattleMeasure::HexCord& hex, BattleBuildings::BuildingListEntry entry);
   BATDATA_DLL void SetEdges(BattleMap * map, const BattleMeasure::HexCord& hex, EdgeInfo edge_info);
   BATDATA_DLL void ReplaceBridgesWithBuildings(BattleMap * map, const TerrainTable & terrain, const BattleBuildings::BuildingTable* buildingTable, BattleBuildings::BuildingList* buildings);

};


#endif /* MAKETER_HPP */

