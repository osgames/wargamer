/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*

SoundLibrary

Table of structures which are built into the sound-table for the game

*/

#include "stdinc.hpp"
#include "b_sounds.hpp"



static const BattleSoundTableEntry sound1 = BattleSoundTableEntry("sounds\\battle\\musket1.wav", 1000, 4);
static const BattleSoundTableEntry sound2 = BattleSoundTableEntry("sounds\\battle\\musket2.wav", 1000, 4);
static const BattleSoundTableEntry sound3 = BattleSoundTableEntry("sounds\\battle\\musket3.wav", 1000, 4);
static const BattleSoundTableEntry sound4 = BattleSoundTableEntry("sounds\\battle\\musket4.wav", 1000, 4);

static const BattleSoundTableEntry sound5 = BattleSoundTableEntry("sounds\\battle\\muskets1.wav", 1000, 4);
static const BattleSoundTableEntry sound6 = BattleSoundTableEntry("sounds\\battle\\muskets2.wav", 1000, 4);
static const BattleSoundTableEntry sound7 = BattleSoundTableEntry("sounds\\battle\\muskets3.wav", 1000, 4);
static const BattleSoundTableEntry sound8 = BattleSoundTableEntry("sounds\\battle\\muskets4.wav", 1000, 4);
static const BattleSoundTableEntry sound9 = BattleSoundTableEntry("sounds\\battle\\muskets5.wav", 1000, 4);
static const BattleSoundTableEntry sound10 = BattleSoundTableEntry("sounds\\battle\\muskets6.wav", 1000, 4);

static const BattleSoundTableEntry sound11 = BattleSoundTableEntry("sounds\\battle\\cannon1.wav", 1000, 4);
static const BattleSoundTableEntry sound12 = BattleSoundTableEntry("sounds\\battle\\cannon2.wav", 1000, 4);
static const BattleSoundTableEntry sound13 = BattleSoundTableEntry("sounds\\battle\\cannon3.wav", 1000, 4);
static const BattleSoundTableEntry sound14 = BattleSoundTableEntry("sounds\\battle\\cannon4.wav", 1000, 4);
static const BattleSoundTableEntry sound15 = BattleSoundTableEntry("sounds\\battle\\cannon5.wav", 1000, 4);
static const BattleSoundTableEntry sound16 = BattleSoundTableEntry("sounds\\battle\\cannon6.wav", 1000, 4);
static const BattleSoundTableEntry sound17 = BattleSoundTableEntry("sounds\\battle\\cannon7.wav", 1000, 4);
static const BattleSoundTableEntry sound18 = BattleSoundTableEntry("sounds\\battle\\cannon8.wav", 1000, 4);

static const BattleSoundTableEntry sound19 = BattleSoundTableEntry("sounds\\battle\\gallop_a.wav", 0, 4);
static const BattleSoundTableEntry sound20 = BattleSoundTableEntry("sounds\\battle\\gallop_b.wav", 0, 4);

static const BattleSoundTableEntry sound21 = BattleSoundTableEntry("sounds\\battle\\crowd1.wav", 500, 4);
static const BattleSoundTableEntry sound22 = BattleSoundTableEntry("sounds\\battle\\crowd2.wav", 500, 4);
static const BattleSoundTableEntry sound23 = BattleSoundTableEntry("sounds\\battle\\crowd3.wav", 500, 4);
static const BattleSoundTableEntry sound24 = BattleSoundTableEntry("sounds\\battle\\crowd4.wav", 500, 4);

static const BattleSoundTableEntry sound25 = BattleSoundTableEntry("sounds\\battle\\whinny_a.wav", 500, 4);
static const BattleSoundTableEntry sound26 = BattleSoundTableEntry("sounds\\battle\\whinny_b.wav", 500, 4);
static const BattleSoundTableEntry sound27 = BattleSoundTableEntry("sounds\\battle\\whinny_c.wav", 500, 4);

static const BattleSoundTableEntry sound28 = BattleSoundTableEntry("sounds\\battle\\swords_a.wav", 500, 4);
static const BattleSoundTableEntry sound29 = BattleSoundTableEntry("sounds\\battle\\swords_b.wav", 500, 4);

static const BattleSoundTableEntry sound30 = BattleSoundTableEntry("sounds\\battle\\smash.wav", 1000, 4);

static const BattleSoundTableEntry sound31 = BattleSoundTableEntry("sounds\\battle\\squeak_a.wav", 0, 4);
static const BattleSoundTableEntry sound32 = BattleSoundTableEntry("sounds\\battle\\squeak_b.wav", 0, 4);

static const BattleSoundTableEntry sound33 = BattleSoundTableEntry("sounds\\battle\\drummarch.wav", 1000, 4);

static const BattleSoundTableEntry sound34 = BattleSoundTableEntry("sounds\\battle\\troopmove_a.wav", 1000, 4);
static const BattleSoundTableEntry sound35 = BattleSoundTableEntry("sounds\\battle\\troopmove_b.wav", 1000, 4);

static const BattleSoundTableEntry sound36 = BattleSoundTableEntry("sounds\\battle\\explosion1.wav", 1000, 4);
static const BattleSoundTableEntry sound37 = BattleSoundTableEntry("sounds\\battle\\explosion2.wav", 1000, 4);

static const BattleSoundTableEntry sound38 = BattleSoundTableEntry("sounds\\battle\\death1.wav", 1000, 4);
static const BattleSoundTableEntry sound39 = BattleSoundTableEntry("sounds\\battle\\death2.wav", 1000, 4);
static const BattleSoundTableEntry sound40 = BattleSoundTableEntry("sounds\\battle\\death3.wav", 1000, 4);
static const BattleSoundTableEntry sound41 = BattleSoundTableEntry("sounds\\battle\\death4.wav", 1000, 4);

static const BattleSoundTableEntry sound42 = BattleSoundTableEntry("sounds\\battle\\fire1.wav", 1000, 4);
static const BattleSoundTableEntry sound43 = BattleSoundTableEntry("sounds\\battle\\fire2.wav", 1000, 4);

static const BattleSoundTableEntry sound44 = BattleSoundTableEntry("sounds\\common\\menu_popup.wav", 0, 4);
static const BattleSoundTableEntry sound45 = BattleSoundTableEntry("sounds\\common\\menu_select.wav", 0, 4);
static const BattleSoundTableEntry sound46 = BattleSoundTableEntry("sounds\\common\\window_open.wav", 0, 4);
static const BattleSoundTableEntry sound47 = BattleSoundTableEntry("sounds\\common\\window_close.wav", 0, 4);

static const BattleSoundTableEntry sound48 = BattleSoundTableEntry("sounds\\battle\\FacingChange.wav", 1000, 4);

const BattleSoundTableEntry * BattleSoundTable[] = {
    &sound1,
    &sound2,
    &sound3,
    &sound4,
    &sound5,
    &sound6,
    &sound7,
    &sound8,
    &sound9,
    &sound10,
    &sound11,
    &sound12,
    &sound13,
    &sound14,
    &sound15,
    &sound16,
    &sound17,
    &sound18,
    &sound19,
    &sound20,
    &sound21,
    &sound22,
    &sound23,
    &sound24,
    &sound25,
    &sound26,
    &sound27,
    &sound28,
    &sound29,
    &sound30,
    &sound31,
    &sound32,
    &sound33,
    &sound34,
    &sound35,
    &sound36,
    &sound37,
    &sound38,
    &sound39,
    &sound40,
    &sound41,
    &sound42,
    &sound43,
    &sound44,
    &sound45,
   &sound46,
   &sound47,
   &sound48,
    0
};






BattleSoundTypeEnum GetMusketSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTMUSKET, SOUNDTYPE_LASTMUSKET)); }

BattleSoundTypeEnum GetMusketsSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTMUSKETS, SOUNDTYPE_LASTMUSKETS)); }

BattleSoundTypeEnum GetCannonSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTCANNON, SOUNDTYPE_LASTCANNON)); }

BattleSoundTypeEnum GetCrowdSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTCROWD, SOUNDTYPE_LASTCROWD)); }

BattleSoundTypeEnum GetGallopSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTHORSEGALLOP, SOUNDTYPE_LASTHORSEGALLOP)); }

BattleSoundTypeEnum GetWhinnySound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTWHINNY, SOUNDTYPE_LASTWHINNY)); }

BattleSoundTypeEnum GetSwordsSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTSWORDS, SOUNDTYPE_LASTSWORDS)); }

BattleSoundTypeEnum GetSmashSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTSMASH, SOUNDTYPE_LASTSMASH)); }

BattleSoundTypeEnum GetSqueakSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTSQUEAK, SOUNDTYPE_LASTSQUEAK)); }

BattleSoundTypeEnum GetDrumSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTDRUM, SOUNDTYPE_LASTDRUM)); }

BattleSoundTypeEnum GetTroopMoveSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTTROOPMOVE, SOUNDTYPE_LASTTROOPMOVE)); }

BattleSoundTypeEnum GetExplosionSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTEXPLOSION, SOUNDTYPE_LASTEXPLOSION)); }

BattleSoundTypeEnum GetDeathSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTDEATH, SOUNDTYPE_LASTDEATH)); }

BattleSoundTypeEnum GetFireSound(void) { return static_cast<BattleSoundTypeEnum> (random(SOUNDTYPE_FIRSTFIRE, SOUNDTYPE_LASTFIRE)); }

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
