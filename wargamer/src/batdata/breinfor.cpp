/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "breinfor.hpp"
#include "fsalloc.hpp"
#include "filebase.hpp"
#include "obdefs.hpp"
#include "ob.hpp"
#include "bob_cp.hpp"

/*-------------------------------------------------------------------
 * HexList class, used for storing routes, deployment hexes, etc.
 */

const int chunkSize = 50;

#ifdef DEBUG
static FixedSize_Allocator itemAlloc(sizeof(B_ReinforceItem), chunkSize, "B_ReinforceItem");
#else
static FixedSize_Allocator itemAlloc(sizeof(B_ReinforceItem), chunkSize);
#endif

void* B_ReinforceItem::operator new(size_t size)
{
        ASSERT(size == sizeof(B_ReinforceItem));
        return itemAlloc.alloc(size);
}

#ifdef _MSC_VER
void B_ReinforceItem::operator delete(void* deadObject)
{
        itemAlloc.free(deadObject, sizeof(B_ReinforceItem));
}
#else
void B_ReinforceItem::operator delete(void* deadObject, size_t size)
{
        ASSERT(size == sizeof(B_ReinforceItem));
        itemAlloc.free(deadObject, size);
}
#endif

/*
 * File Interface
 */

const UWORD B_ReinforceItem::s_fileVersion = 0x0000;
Boolean B_ReinforceItem::readData(FileReader& f, OrderBattle* ob)
{
        UWORD version;
        f >> version;
        ASSERT(version <= s_fileVersion);

  CPIndex cpi;
  f >> cpi;

  d_cp = (cpi != NoCPIndex) ? battleCP(ob->command(cpi)) : NoBattleCP;

        f >> d_time;

        if(!d_hex.readData(f))
         return False;

        return f.isOK();
}

Boolean B_ReinforceItem::writeData(FileWriter& f, OrderBattle* ob) const
{
        f << s_fileVersion;

        CPIndex cpi = (d_cp != NoBattleCP) ? ob->getIndex(d_cp->generic()) : NoCPIndex;
        f << cpi;

        f << d_time;

        if(!d_hex.writeData(f))
         return False;

        return f.isOK();
}

//-------------------------------------------------------------------------
/*
 * File Interface
 */

const UWORD B_ReinforceList::s_fileVersion = 0x0000;
Boolean B_ReinforceList::readData(FileReader& f, OrderBattle* ob)
{
        UWORD version;
        f >> version;
        ASSERT(version <= s_fileVersion);

        int nItems;
        f >> nItems;
        ASSERT(nItems >= 0);
        while(nItems--)
        {
         B_ReinforceItem* ri = new B_ReinforceItem;
         ASSERT(ri);

         append(ri);

         if(!ri->readData(f, ob))
                return False;
        }

        return f.isOK();
}

Boolean B_ReinforceList::writeData(FileWriter& f, OrderBattle* ob) const
{
        f << s_fileVersion;

        int nItems = entries();
        f << nItems;

        SListIterR<B_ReinforceItem> iter(this);
        while(++iter)
        {
         if(!iter.current()->writeData(f, ob))
                return False;
        }

        return f.isOK();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
