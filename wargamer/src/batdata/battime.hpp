/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATTIME_HPP
#define BATTIME_HPP

#ifndef __cplusplus
#error battime.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Timer
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "datetime.hpp"

class FileReader;
class FileWriter;

// using Greenius_System::DateDefinitions;
// using Greenius_System::TimeDefinitions;

namespace BattleMeasure
{

class BattleTime : private Greenius_System::DateDefinitions, private Greenius_System::TimeDefinitions
{
        public:
                typedef ULONG Tick;

                BATDATA_DLL static UBYTE TicksPerSecond;

                BattleTime() : d_date(), d_ticks(StartOfDay()), d_ratio(1) { }
                BattleTime(const BattleTime& t) : d_ticks(t.d_ticks), d_date(t.d_date) { }
                BATDATA_DLL BattleTime(const Greenius_System::Date& d, const Greenius_System::Time& t);

                BattleTime& operator+=(Tick ticks);
                        // Add on time

                BATDATA_DLL Greenius_System::Time time() const;
                BATDATA_DLL Greenius_System::Date date() const;

                BATDATA_DLL void date(Greenius_System::Date date);
                BATDATA_DLL void time(Greenius_System::Time time);

                Tick getTick() const { return d_ticks; }
				void setTick(Tick tick) { d_ticks = tick; }

                void ratio(int n) { d_ratio = n; }
                int ratio() const { return d_ratio; }

				BATDATA_DLL void startNewDay();

                bool readData(FileReader& f);
                bool writeData(FileWriter& f) const;

            static Tick battleTime(int h, int m, int s);
            static Tick EndOfDay() { return battleTime(18,0,0); }
            static Tick StartOfDay() { return battleTime(7,0,0); }

        private:
                Tick                    d_ticks;                // BattleTicks in current day
                Greenius_System::Date   d_date;                 // ticks are relative to midnight on this date

                int                     d_ratio;

                static UWORD s_fileVersion;
};

inline BattleTime::Tick seconds(int s) { return s * BattleTime::TicksPerSecond; }
inline BattleTime::Tick minutes(int m) { return seconds(m * Greenius_System::Time::SecondsPerMinute); }
inline BattleTime::Tick hours(int h) { return minutes(h * Greenius_System::Time::MinutesPerHour); }
inline BattleTime::Tick days(int d) { return hours(d * Greenius_System::Time::HoursPerDay); }

inline int secondsPerTick(BattleTime::Tick t) { return t / BattleTime::TicksPerSecond; }
inline int minutesPerTick(BattleTime::Tick t) { return secondsPerTick(t) / Greenius_System::Time::SecondsPerMinute; }
inline int hoursPerTick(BattleTime::Tick t) { return minutesPerTick(t) / Greenius_System::Time::MinutesPerHour; }
inline int daysPerTick(BattleTime::Tick t) { return hoursPerTick(t) / Greenius_System::Time::HoursPerDay; }

inline BattleTime::Tick BattleTime::battleTime(int h, int m, int s)
{
        return hours(h) + minutes(m) + seconds(s);
}

inline BattleTime::Tick battleTime(int h, int m, int s)
{
    return BattleTime::battleTime(h,m,s);
}

};

// not in namespace
BATDATA_DLL void ticksToTime(BattleMeasure::BattleTime::Tick ticks, int * days, int * hours, int * mins, int * secs);

#endif /* BATTIME_HPP */

