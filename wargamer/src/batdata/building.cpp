/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Buildings and Bridges
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "building.hpp"
#include "b_gindex.hpp"
#include "filecnk.hpp"
#include "filebase.hpp"
#include "crossref.hpp"
#include "scenario.hpp"
#include "batcord.hpp"
#include <utility>

#ifdef _MSC_VER   // otherwise <utility> != doesn't work
using namespace std::rel_ops;
#endif


// #include "b_terdis.hpp"

namespace BattleBuildings
{

/*
Test table of buildings
*/

// static const char BuildingTable::s_BuildingSpriteFileName[] = "nap1813\\building";
//static const int NoGraphic = static_cast<SWG_Sprite::SpriteLibrary::Index>(-1);


static const EffectDisplayInfo effect_none[] = {
    EffectDisplayInfo(SWG_Sprite::SpriteLibrary::NoGraphic, 0)
};


static const EffectDisplayInfo effect_smoke[] = {

    EffectDisplayInfo(BattleGraphics::Smoke, 2),
    EffectDisplayInfo(BattleGraphics::Smoke+1, 2),
    EffectDisplayInfo(BattleGraphics::Smoke+2, 2),
    EffectDisplayInfo(BattleGraphics::Smoke+3, 2),
    EffectDisplayInfo(BattleGraphics::Smoke+4, 2),
    EffectDisplayInfo(BattleGraphics::Smoke+5, 2),    
    EffectDisplayInfo(BattleGraphics::Smoke+6, 2),
    EffectDisplayInfo(BattleGraphics::Smoke+7, 2),
   EffectDisplayInfo(0, 0),

   EffectDisplayInfo(SWG_Sprite::SpriteLibrary::NoGraphic, 0)
};


static const EffectDisplayInfo effect_fire[] = {

    EffectDisplayInfo(BattleGraphics::Fire, 2),
    EffectDisplayInfo(BattleGraphics::Fire+1, 2),
    EffectDisplayInfo(BattleGraphics::Fire+2, 2),
    EffectDisplayInfo(BattleGraphics::Fire+3, 2),
    EffectDisplayInfo(BattleGraphics::Fire+4, 2),
    EffectDisplayInfo(BattleGraphics::Fire+5, 2),
    EffectDisplayInfo(BattleGraphics::Fire+6, 2),
    EffectDisplayInfo(BattleGraphics::Fire+7, 2),
   EffectDisplayInfo(0, 0),

    EffectDisplayInfo(SWG_Sprite::SpriteLibrary::NoGraphic, 0)
};



static const EffectDisplayInfo* effectsTable[] = {
    effect_none,
    effect_smoke,
    effect_fire,
    0
};


/*
Fill out the effects table structure
*/

void EffectsTable::init() {

    const EffectDisplayInfo** infoPtr = effectsTable;
    while(*infoPtr) {
        d_info.push_back(EffectInfo());
        EffectInfo* fxInfo = &d_info.back();

        for(const EffectDisplayInfo* info = *infoPtr++; info->index != SWG_Sprite::SpriteLibrary::NoGraphic; ++info) {
            fxInfo->push_back(*info);
        }

    }
}








BuildingTable::~BuildingTable()
{
        // delete d_sprites;
}



void
BuildingTable::init(void) 
{

    // d_sprites = new SWG_Sprite::SpriteLibrary(s_BuildingSpriteFileName);

    CString clusterName = scenario->makeScenarioFileName("buildtype.dat");
    CString graphicName = scenario->makeScenarioFileName("g_build.dat");

    static char s_seperator[] = " = ";
    static char s_xMax[] = "X_COORD_MAX";
    static char s_yMax[] = "Y_COORD_MAX";
    static char s_nClusters[] = "NUM_CLUSTERS";
    static char s_cluster[] = "CLUSTER";
    static char s_nBuildings[] = "NUM_BUILDINGS";
    static char s_name[] = "NAME";
    static char s_destroyed[] = "DESTROYED";
    static char s_frame[] = "FRAME";
    static char s_x[] = "X";
    static char s_y[] = "Y";


    CrossReferencer * d_ClusterFileCrossRef = new CrossReferencer(clusterName);
    d_ClusterFileCrossRef->DefineSeperator(s_seperator);

    CrossReferencer * d_GraphicFileCrossRef = new CrossReferencer(graphicName);
    d_GraphicFileCrossRef->DefineSeperator(s_seperator);

    char* cluster_data = d_ClusterFileCrossRef->GetDataStart();

    int xcoord_max;
    cluster_data = d_ClusterFileCrossRef->GetIntegerEntry(s_xMax, &xcoord_max, cluster_data);
    if(!cluster_data) {
      throw GeneralError("Oops - couldn't read max X coordinate");
   }

    int ycoord_max;
    cluster_data = d_ClusterFileCrossRef->GetIntegerEntry(s_yMax, &ycoord_max, cluster_data);
    if(!cluster_data) {
      throw GeneralError("Oops - couldn't read max Y coordinate");
   }

    int midX = xcoord_max / 2;
    int midY = ycoord_max / 2;


    int num_clusters;
    cluster_data = d_ClusterFileCrossRef->GetIntegerEntry(s_nClusters, &num_clusters, cluster_data);
    if(!cluster_data) {
      throw GeneralError("Oops - couldn't read number of clusters");
   }

    for(int i=0; i<num_clusters; i++) {

        int num;

        cluster_data = d_ClusterFileCrossRef->GetIntegerEntry(s_cluster, &num, cluster_data);
        if(!cluster_data) {
         throw GeneralError("Oops - couldn't read cluster number");
      }
        if(num != i) {
         throw GeneralError("Oops - numbering mismatch in BUILDTYPE.DAT");
      }

        int num_buildings;
        cluster_data = d_ClusterFileCrossRef->GetIntegerEntry(s_nBuildings, &num_buildings, cluster_data);
        if(!cluster_data) {
         throw GeneralError("Oops - couldn't read number of buildings in cluster");
      }

        // add a blank cluster into the building table
        d_info.push_back(BuildingInfo() );
        BuildingInfo * buildingInfo = &(d_info.back() );
        
        for(int b=0; b<num_buildings; b++) {

            char * normal_name = NULL;
            normal_name = d_ClusterFileCrossRef->GetCharacterEntry(s_name, cluster_data);
            if(!normal_name) {
            throw GeneralError("Oops - couldn't read building name");
         }

            char * destroyed_name = NULL;
            destroyed_name = d_ClusterFileCrossRef->GetCharacterEntry(s_destroyed, cluster_data);
            if(!destroyed_name) {
            throw GeneralError("Oops - couldn't read destroyed building name");
         }

            int frame;
            cluster_data = d_ClusterFileCrossRef->GetIntegerEntry(s_frame, &frame, cluster_data);
            if(!cluster_data) {
            throw GeneralError("Oops - couldn't read building frame number");
         }

            int x;
            cluster_data = d_ClusterFileCrossRef->GetIntegerEntry(s_x, &x, cluster_data);
            if(!cluster_data) {
            throw GeneralError("Oops - couldn't read building x pos");
         }

            int y;
            cluster_data = d_ClusterFileCrossRef->GetIntegerEntry(s_y, &y, cluster_data);
            if(!cluster_data) {
            throw GeneralError("Oops - couldn't read building y pos");
         }

            // set up the display details
            char * spr_result;
            int normal_index;
            spr_result = d_GraphicFileCrossRef->GetIntegerEntry(normal_name, &normal_index, 0);
            if(!spr_result) {
            throw GeneralError("Oops - couldn't cross reference (normal) building name with G_BUILD.DAT");
         }

            int destroyed_index;
            spr_result = d_GraphicFileCrossRef->GetIntegerEntry(destroyed_name, &destroyed_index, 0);
            if(!spr_result) {
            throw GeneralError("Oops - couldn't cross reference (destroyed) building name with G_BUILD.DAT");
         }

            // convert x & y from specified coorinate system to buildings coordinate system

            // x = (x - (xcoord_max/2) ) * (10000 / (xcoord_max/2) );
            // y = (y - (ycoord_max/2) ) * (10000 / (ycoord_max/2) );

            BuildingDisplayInfo::Position pos(
                MulDiv(x - midX, BattleMeasure::OneXHex, xcoord_max),
                MulDiv(y - midY, BattleMeasure::OneYHex, ycoord_max) );

                    // float(x - midX) / float(midX),
                    // float(y - midY) / float(midY) );
                                    
            BuildingDisplayInfo disp_info(normal_index + frame, destroyed_index + frame, pos);
            
            // now add this building into the cluster
            buildingInfo->push_back(disp_info);

            // clean alloc'd stuff
            if(normal_name)
                free(normal_name);
            if(destroyed_name)
                free(destroyed_name);
        }

    }

    int size = d_info.size();

    delete d_ClusterFileCrossRef;
    delete d_GraphicFileCrossRef;
}





/*
 * File Interface
 */

/*
 * Building DisplayInfo is really a static data structure
 * But we might want to save the building's status and effects

  NOTE : in file version 0x0002 we have removed dynamic frame info from this structure altogether.
       in fact there is now no reason to read/write this structure, as it is completely static
 */

const UWORD BuildingDisplayInfo::s_fileVersion = 0x0002;

Boolean BuildingDisplayInfo::readData(FileReader& f)
{
    UWORD version;
    f >> version;
    ASSERT(version <= s_fileVersion);

    f >> d_index_normal;
    f >> d_index_destroyed;

    d_location.readData(f);

   if(version < s_fileVersion) {

      UWORD _stat;
      f >> _stat;
//       d_status = static_cast<BuildingStatus>(_stat);
//       f >> d_status_timer;

      UWORD _effect;
      f >> _effect;
//       d_effect = static_cast<EffectsEnum>(_effect);
//       f >> d_frame_no;
//       f >> d_frame_timer;
   }

   return f.isOK();
}

Boolean BuildingDisplayInfo::writeData(FileWriter& f) const
{
        f << s_fileVersion;
        f << d_index_normal;
        f << d_index_destroyed;

        // f << d_position.x();
        // f << d_position.y();
        d_location.writeData(f);

      /*
        f.putUWord(d_status);
        f << d_status_timer;

        f.putUWord(d_effect);
        f << d_frame_no;
        f << d_frame_timer;
      */
        return f.isOK();
}

const UWORD BuildingInfo::s_fileVersion = 0x0001;

Boolean BuildingInfo::readData(FileReader& f)
{
        UWORD version;
        f >> version;
        ASSERT(version <= s_fileVersion);

        int n;
        f >> n;

        ASSERT(d_displayInfo.size() == 0);

        d_displayInfo.reserve(n);

        while(n--)
        {
                BuildingDisplayInfo info;
                f >> info;
                d_displayInfo.push_back(info);
        }

        return f.isOK();
}

Boolean BuildingInfo::writeData(FileWriter& f) const
{
        f << s_fileVersion;

        int n = d_displayInfo.size();
        f << n;

        for(DisplayInfo::const_iterator it = d_displayInfo.begin(); it != d_displayInfo.end(); ++it)
        {
                it->writeData(f);
        }

        return f.isOK();
}





BuildingListEntry::BuildingListEntry(void) {
   d_Index = 0;
   d_Status = BUILDING_NORMAL;
   d_EffectCount = 0;
   d_lastTime = 0;

   for(int f=0; f<MAX_BUILDINGS_PER_CLUSTER; f++) {
      d_Frame[f] = 0;
      d_Timer[f] = random(0,6);
   }
}    



const UWORD BuildingListEntry::s_fileVersion = 0x0001;


Boolean
BuildingListEntry::readData(FileReader& f) {

   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);


   f >> d_Index;
   int status;
   f >> status;
   d_Status = static_cast<BuildingStatus>(status);
   int frame;
   f >> frame;
   int timer;
   f >> timer;
   f >> d_EffectCount;

   for(int n=0; n<MAX_BUILDINGS_PER_CLUSTER; n++) {
      d_Frame[n] = frame;
      d_Timer[n] = random(0,6);
   }

   return f.isOK();
}

/*
      f >> d_Index;
      int status;
      f >> status;
      d_Status = static_cast<BuildingStatus>(status);
      f >> d_Frame;
      f >> d_Timer;
      f >> d_EffectCount;
*/



Boolean
BuildingListEntry::writeData(FileWriter& f) const {

   f << s_fileVersion;

   f << d_Index;

   int status = static_cast<int>(d_Status);
   f << status;
   int frame = d_Frame[0];
   f << frame;
   int timer = d_Timer[0];
   f << timer;
   f << d_EffectCount;

   return f.isOK();
}








// new version 12/4/99 reads BuildingIndex

/*

  NOTE version 0x0002 (2nd June '99) does the above but stores index inside BuildingListEntry structure with anim-info

*/

const UWORD BuildingList::s_fileVersion = 0x0002;
const char BuildingList::s_fileID[] = "BattleBuildingList";

Boolean BuildingList::readData(FileReader& f, BuildingTable * buildtable)
{
        FileChunkReader fc(f, s_fileID);
        UWORD version;
        f >> version;
        ASSERT(version <= s_fileVersion);

      BuildingListEntry entry;

        clear();
        int n;
        f >> n;
        while(n--)
        {
                BattleMeasure::HexCord hex;
            int index;

                f >> hex;

                // old version reads BuildingInfo vector
                if(version < 0x0001) {
                    BuildingInfo info;
                    f >> info;
                    const BuildingInfo::DisplayInfo & disp_info = info.displayInfo();

                    // we've got to go through the building table comparing with the BuildInfos, to generate an index
                    for(int i = 0; i<buildtable->size(); i++) {

                        // get the cluster
                        const BuildingInfo * tab_info = buildtable->info(i);
                        // get the vector of buildings within it
                        const BuildingInfo::DisplayInfo & tabdisp_info = tab_info->displayInfo();

                        // are there the same number of buildings in cluster ?
                        int numbuildings = disp_info.end() - disp_info.begin();
                        int tab_numbuildings = tabdisp_info.end() - tabdisp_info.begin();

                        if(numbuildings == tab_numbuildings) {

                            // is each building the same index & in same position
                            bool same = true;
                            
                            for(int bi =0; bi<numbuildings; bi++) {

                                BuildingDisplayInfo binfo = disp_info[bi];
                                BuildingDisplayInfo tab_binfo = tabdisp_info[bi];

//                                if(binfo.normalIndex() != tab_binfo.normalIndex() ) same = false;
//                                if(binfo.destroyedIndex() != tab_binfo.destroyedIndex() ) same = false;
                                if(binfo.position() != tab_binfo.position() ) same = false;
                            }

                            // i'm gonna be naughty & use a goto here - cause i'm that kinda guy
                            if(same == true) {
                                index = i;

                        entry.index(index);
                        entry.status(BUILDING_NORMAL);
                        entry.frame(0);
                        entry.timer(0);
                        entry.effectCount(0);

                                goto break_loop;
                            }

                        } // otherwise, we go to the next building-table cluster & compare
                    } // repeat for all buildings in building-table

                    // by know we know we haven't found it - so set index as 0, and complain
                    index = 0;
                    FORCEASSERT("Oops - couldn't find matching cluster when patching up from old save version...\n");

               entry.index(index);
               entry.status(BUILDING_NORMAL);
               entry.frame(0);
               entry.timer(0);
               entry.effectCount(0);
                }



                // version 0x0001 reads BuildingTableIndex
                else if(version < 0x0002) {
                    f >> index;

               entry.index(index);
               entry.status(BUILDING_NORMAL);
               entry.frame(0);
               entry.timer(0);
               entry.effectCount(0);
                }

            /*
            Version 0x0002 saves out the whole BuildingListEntry structure
            */
            else {

               entry.readData(f);
            }

/*
Add building entry to list
*/

break_loop:    add(hex, entry);


        }

        return f.isOK();
}

Boolean BuildingList::writeData(FileWriter& f) const
{
        FileChunkWriter fc(f, s_fileID);
        f << s_fileVersion;

        int n = d_buildings.size();
        f << n;
        for(BuildingListType::const_iterator it = d_buildings.begin(); it != d_buildings.end(); ++it)
        {
                (*it).first.writeData(f);
#if 0
                f << (*it).second;
                // changed to write indexes
//                (*it).second.writeData(f);
#endif
            /*
            As of version 0x0002 we write out the BuildingListEntry structure
            */
            (*it).second.writeData(f);
        }

        return f.isOK();
}

};      // namespace BattleBuildings

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
