/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "b_tables.hpp"

static const char s_spfInfChangeTimeChunkName[] =        "SPFInfChangeTimeTable";
static const char s_spfCavChangeTimeChunkName[] =        "SPFCavChangeTimeTable";
static const char s_spfHvyArtChangeTimeChunkName[] =     "SPFHvyArtChangeTimeTable";
static const char s_spfFootArtChangeTimeChunkName[] =    "SPFFootArtChangeTimeTable";
static const char s_spfHorseArtChangeTimeChunkName[] =   "SPFHorseArtChangeTimeTable";
static const char s_spfModifierChunkName[] =             "SPFModifierTable";
static const char s_basicMovementRateChunkName[] =       "BasicMovementRateTable";
static const char s_roadMovementRateChunkName[] =        "RoadMovementRateTable";
static const char s_terrainModifiersChunkName[] =        "TerrainModifiersTable";
static const char s_pathModifiersChunkName[] =           "PathModifiersTable";
static const char s_terrainDisorderChunkName[] =         "TerrainDisorderTable";
static const char s_prohibitedTerrainChunkName[] =       "ProhibitedTerrainTable";
static const char s_fireCombatRangeChunkName[] =         "FireCombatRangeTable";
static const char s_fireCombatPointsChunkName[] =        "FireCombatPointsTable";
static const char s_fireCombatCasualtyChunkName[] =      "FireCombatCasualtyTable";
static const char s_fireCombatDieRollModifiersChunkName[] = "FireCombatDieRollModifiersTable";
static const char s_moraleTestDieRollModifiersChunkName[] = "MoraleTestDieRollModifiersTable";
static const char s_corpDeterminationDieRollModifiersChunkName[] = "CorpDeterminationDieRollModifiersTable";
static const char s_fireCombatFireValueMultipliersChunkName[] = "FireCombatFireValueMultipliersTable";
static const char s_infVsInfShockLossChunkName[] =      "InfVsInfShockLossPercentTable";
static const char s_infVsCavShockLossChunkName[] =      "InfVsCavShockLossPercentTable";
static const char s_infVsCavChanceToInflictShockLossChunkName[] = "InfVsCavChanceToInflictShockTable";
static const char s_infVsInfDieRollModifiersChunkName[] = "InfVsInfDieRollModifiersTable";
static const char s_infVsInfMoraleDieRollModifiersChunkName[] = "InfVsInfMoraleDieRollModifiersTable";
static const char s_infVsCavCavDieRollModifiersChunkName[] = "InfVsCavCavDieRollModifiersTable";
static const char s_infVsCavInfDieRollModifiersChunkName[] = "InfVsCavInfDieRollModifiersTable";
static const char s_cavVsCavDieRollModifiersChunkName[] = "CavVsCavDieRollModifiersTable";
static const char s_cavChargeTestDieRollModifiersChunkName[] = "CavChargeTestDieRollModifiersTable";
static const char s_chargeTestDieRollModifiersChunkName[] = "ChargeTestDieRollModifiersTable";
static const char s_moraleBonusChunkName[] = "MoraleBonusTable";

static const char s_noEffectCutoffChunkName[] = "NoEffectCutoffTable";
static const char s_moraleLossMultiplierChunkName[] = "MoraleLossMultiplierTable";
static const char s_criticalLossCutoffChunkName[] = "CriticalLossCutoffTable";
static const char s_recoveryTimeChunkName[] = "RecoveryTimeTable";
static const char s_recoveryChunkName[] = "RecoveryTable";

// TODO: this needs to be got from Scenario
const char* s_tableDataName = "nap1813\\b_tables.dat";

class BattleTablesImp {

    /*
     * SP Formation change tables
     */

    Table3D<SWORD> d_spfInfChangeTime;
    Table3D<SWORD> d_spfCavChangeTime;
    Table3D<SWORD> d_spfHvyArtChangeTime;
    Table3D<SWORD> d_spfFootArtChangeTime;
    Table3D<SWORD> d_spfHorseArtChangeTime;
    Table1D<UWORD> d_spfModifiers;

    /*
     * Movement tables
     */

    Table2D<UBYTE>  d_basicMovementRate;
    Table2D<UBYTE>  d_roadMovementRate;
    Table3D<UWORD>  d_terrainModifiers;
    Table3D<UWORD>  d_pathModifiers;
    Table3D<UBYTE>  d_terrainDisorder;
    Table3D<UBYTE>  d_prohibitedTerrain;

    /*
     * Fire-Combat tables
     */

    Table1D<UWORD>  d_fireCombatRange;
    Table2D<UWORD>  d_fireCombatPoints;
    Table2D<UWORD>  d_fireCombatCasualty;

    Table1D<SWORD>  d_fireCombatDieRollModifiers;
    Table1D<SWORD>  d_fireCombatFireValueMultipliers;
    Table1D<SWORD>  d_moraleTestDieRollModifiers;
    Table1D<SWORD>  d_corpDeterminationDieRollModifiers;
    Table2D<UBYTE>  d_infVsInfShockLoss;
    Table2D<UBYTE>  d_infVsCavShockLoss;
    Table2D<UBYTE>  d_infVsCavChanceToInflictShockLoss;
    Table1D<SWORD>  d_infVsInfDieRollModifiers;
    Table1D<SWORD>  d_infVsInfMoraleDieRollModifiers;
    Table1D<SWORD>  d_infVsCavCavDieRollModifiers;
    Table1D<SWORD>  d_infVsCavInfDieRollModifiers;
    Table1D<SWORD>  d_cavVsCavDieRollModifiers;
    Table1D<SWORD>  d_cavChargeTestDieRollModifiers;
    Table1D<SWORD>  d_chargeTestDieRollModifiers;
    Table1D<SWORD>  d_moraleBonus;

    Table1D<UBYTE>  d_noEffectCutoff;
    Table1D<SWORD>  d_moraleLossMultiplier;
    Table1D<UBYTE>  d_criticalLossCutoff;
    Table1D<UBYTE>  d_recoveryTime;
    Table1D<SWORD>  d_recovery;

  public:
    const Table3D<SWORD>& spfInfChangeTime()
    {
      if(!d_spfInfChangeTime.isInitiated())
      {
        d_spfInfChangeTime.init(s_tableDataName, s_spfInfChangeTimeChunkName);
      }

      return d_spfInfChangeTime;
    }

    const Table3D<SWORD>& spfCavChangeTime()
    {
      if(!d_spfCavChangeTime.isInitiated())
      {
        d_spfCavChangeTime.init(s_tableDataName, s_spfCavChangeTimeChunkName);
      }

      return d_spfCavChangeTime;
    }

    const Table3D<SWORD>& spfHvyArtChangeTime()
    {
      if(!d_spfHvyArtChangeTime.isInitiated())
      {
        d_spfHvyArtChangeTime.init(s_tableDataName, s_spfHvyArtChangeTimeChunkName);
      }

      return d_spfHvyArtChangeTime;
    }

    const Table3D<SWORD>& spfFootArtChangeTime()
    {
      if(!d_spfFootArtChangeTime.isInitiated())
      {
        d_spfFootArtChangeTime.init(s_tableDataName, s_spfFootArtChangeTimeChunkName);
      }

      return d_spfFootArtChangeTime;
    }

    const Table3D<SWORD>& spfHorseArtChangeTime()
    {
      if(!d_spfHorseArtChangeTime.isInitiated())
      {
        d_spfHorseArtChangeTime.init(s_tableDataName, s_spfHorseArtChangeTimeChunkName);
      }

      return d_spfHorseArtChangeTime;
    }

    const Table1D<UWORD>& spfModifiers()
    {
      if(!d_spfModifiers.isInitiated())
      {
        d_spfModifiers.init(s_tableDataName, s_spfModifierChunkName, 10);
      }

      return d_spfModifiers;
    }

    /*
     * Movement tables
     */

    const Table2D<UBYTE>& basicMovementRate()
    {
      if(!d_basicMovementRate.isInitiated())
      {
        d_basicMovementRate.init(s_tableDataName, s_basicMovementRateChunkName);
      }

      return d_basicMovementRate;
    }

    const Table2D<UBYTE>& roadMovementRate()
    {
      if(!d_roadMovementRate.isInitiated())
      {
        d_roadMovementRate.init(s_tableDataName, s_roadMovementRateChunkName);
      }

      return d_roadMovementRate;
    }

    const Table3D<UWORD>& terrainModifiers()
    {
      if(!d_terrainModifiers.isInitiated())
      {
        d_terrainModifiers.init(s_tableDataName, s_terrainModifiersChunkName, 100);
      }

      return d_terrainModifiers;
    }

    const Table3D<UWORD>& pathModifiers()
    {
      if(!d_pathModifiers.isInitiated())
      {
        d_pathModifiers.init(s_tableDataName, s_pathModifiersChunkName, 100);
      }

      return d_pathModifiers;
    }

    const Table3D<UBYTE>& terrainDisorder()
    {
      if(!d_terrainDisorder.isInitiated())
      {
        d_terrainDisorder.init(s_tableDataName, s_terrainDisorderChunkName);
      }

      return d_terrainDisorder;
    }

    const Table3D<UBYTE>& prohibitedTerrain()
    {
      if(!d_prohibitedTerrain.isInitiated())
      {
        d_prohibitedTerrain.init(s_tableDataName, s_prohibitedTerrainChunkName);
      }

      return d_prohibitedTerrain;
    }

    /*
     * Fire-Combat tables
     */

    const Table1D<UWORD>& fireCombatRange()
    {
      if(!d_fireCombatRange.isInitiated())
      {
        d_fireCombatRange.init(s_tableDataName, s_fireCombatRangeChunkName);
      }

      return d_fireCombatRange;
    }

    const Table2D<UWORD>& fireCombatPoints()
    {
      if(!d_fireCombatPoints.isInitiated())
      {
        d_fireCombatPoints.init(s_tableDataName, s_fireCombatPointsChunkName, 10);
      }

      return d_fireCombatPoints;
    }

    const Table2D<UWORD>& fireCombatCasualty()
    {
      if(!d_fireCombatCasualty.isInitiated())
      {
        d_fireCombatCasualty.init(s_tableDataName, s_fireCombatCasualtyChunkName, 100);
      }

      return d_fireCombatCasualty;
    }

    const Table1D<SWORD>& fireCombatDieRollModifiers()
    {
      if(!d_fireCombatDieRollModifiers.isInitiated())
      {
        d_fireCombatDieRollModifiers.init(s_tableDataName, s_fireCombatDieRollModifiersChunkName);
      }

      return d_fireCombatDieRollModifiers;
    }

    const Table1D<SWORD>& fireCombatFireValueMultipliers()
    {
      if(!d_fireCombatFireValueMultipliers.isInitiated())
      {
        d_fireCombatFireValueMultipliers.init(s_tableDataName, s_fireCombatFireValueMultipliersChunkName, 100);
      }

      return d_fireCombatFireValueMultipliers;
    }

    const Table1D<SWORD>& moraleTestDieRollModifiers()
    {
      if(!d_moraleTestDieRollModifiers.isInitiated())
      {
        d_moraleTestDieRollModifiers.init(s_tableDataName, s_moraleTestDieRollModifiersChunkName);
      }

      return d_moraleTestDieRollModifiers;
    }
    const Table1D<SWORD>& moraleBonus()
    {
      if(!d_moraleBonus.isInitiated())
      {
        d_moraleBonus.init(s_tableDataName, s_moraleBonusChunkName);
      }

      return d_moraleBonus;
    }
    const Table1D<SWORD>& cavChargeTestDieRollModifiers()
    {
      if(!d_cavChargeTestDieRollModifiers.isInitiated())
      {
        d_cavChargeTestDieRollModifiers.init(s_tableDataName, s_cavChargeTestDieRollModifiersChunkName);
      }

      return d_cavChargeTestDieRollModifiers;
    }
    const Table1D<SWORD>& chargeTestDieRollModifiers()
    {
      if(!d_chargeTestDieRollModifiers.isInitiated())
      {
        d_chargeTestDieRollModifiers.init(s_tableDataName, s_chargeTestDieRollModifiersChunkName);
      }

      return d_chargeTestDieRollModifiers;
    }
    const Table1D<SWORD>& corpDeterminationDieRollModifiers()
    {
      if(!d_corpDeterminationDieRollModifiers.isInitiated())
      {
        d_corpDeterminationDieRollModifiers.init(s_tableDataName, s_corpDeterminationDieRollModifiersChunkName);
      }

      return d_corpDeterminationDieRollModifiers;
    }
    const Table2D<UBYTE>& infVsInfShockLoss()
    {
      if(!d_infVsInfShockLoss.isInitiated())
      {
        d_infVsInfShockLoss.init(s_tableDataName, s_infVsInfShockLossChunkName, 100);
      }

      return d_infVsInfShockLoss;
    }
    const Table2D<UBYTE>& infVsCavShockLoss()
    {
      if(!d_infVsCavShockLoss.isInitiated())
      {
        d_infVsCavShockLoss.init(s_tableDataName, s_infVsCavShockLossChunkName, 100);
      }

      return d_infVsCavShockLoss;
    }
    const Table2D<UBYTE>& infVsCavChanceToInflictShockLoss()
    {
      if(!d_infVsCavChanceToInflictShockLoss.isInitiated())
      {
        d_infVsCavChanceToInflictShockLoss.init(s_tableDataName, s_infVsCavChanceToInflictShockLossChunkName);
      }

      return d_infVsCavChanceToInflictShockLoss;
    }
    const Table1D<SWORD>& infVsInfDieRollModifiers()
    {
      if(!d_infVsInfDieRollModifiers.isInitiated())
      {
        d_infVsInfDieRollModifiers.init(s_tableDataName, s_infVsInfDieRollModifiersChunkName);
      }

      return d_infVsInfDieRollModifiers;
    }
    const Table1D<SWORD>& infVsInfMoraleDieRollModifiers()
    {
      if(!d_infVsInfMoraleDieRollModifiers.isInitiated())
      {
        d_infVsInfMoraleDieRollModifiers.init(s_tableDataName, s_infVsInfMoraleDieRollModifiersChunkName);
      }

      return d_infVsInfMoraleDieRollModifiers;
    }
    const Table1D<SWORD>& infVsCavCavDieRollModifiers()
    {
      if(!d_infVsCavCavDieRollModifiers.isInitiated())
      {
        d_infVsCavCavDieRollModifiers.init(s_tableDataName, s_infVsCavCavDieRollModifiersChunkName);
      }

      return d_infVsCavCavDieRollModifiers;
    }
    const Table1D<SWORD>& infVsCavInfDieRollModifiers()
    {
      if(!d_infVsCavInfDieRollModifiers.isInitiated())
      {
        d_infVsCavInfDieRollModifiers.init(s_tableDataName, s_infVsCavInfDieRollModifiersChunkName);
      }

      return d_infVsCavInfDieRollModifiers;
    }
    const Table1D<SWORD>& cavVsCavDieRollModifiers()
    {
      if(!d_cavVsCavDieRollModifiers.isInitiated())
      {
        d_cavVsCavDieRollModifiers.init(s_tableDataName, s_cavVsCavDieRollModifiersChunkName);
      }

      return d_cavVsCavDieRollModifiers;
    }

    const Table1D<UBYTE>& noEffectCutoff()
    {
      if(!d_noEffectCutoff.isInitiated())
      {
        d_noEffectCutoff.init(s_tableDataName, s_noEffectCutoffChunkName);
      }

      return d_noEffectCutoff;
    }

    const Table1D<SWORD>& moraleLossMultiplier()
    {
      if(!d_moraleLossMultiplier.isInitiated())
      {
        d_moraleLossMultiplier.init(s_tableDataName, s_moraleLossMultiplierChunkName, 10);
      }

      return d_moraleLossMultiplier;
    }

    const Table1D<UBYTE>& criticalLossCutoff()
    {
      if(!d_criticalLossCutoff.isInitiated())
      {
        d_criticalLossCutoff.init(s_tableDataName, s_criticalLossCutoffChunkName);
      }

      return d_criticalLossCutoff;
    }
    const Table1D<UBYTE>& recoveryTime()
    {
      if(!d_recoveryTime.isInitiated())
      {
        d_recoveryTime.init(s_tableDataName, s_recoveryTimeChunkName);
      }

      return d_recoveryTime;
    }
    const Table1D<SWORD>& recovery()
    {
      if(!d_recovery.isInitiated())
      {
        d_recovery.init(s_tableDataName, s_recoveryChunkName);
      }

      return d_recovery;
    }
};

/*---------------------------------------------------------------------
 * Client access
 */

static BattleTablesImp s_tables;

/*--------------------------------------------------
 * SPFormation change tables
 */

const Table3D<SWORD>& BattleTables::spfInfChangeTime()
{
  return s_tables.spfInfChangeTime();
}

const Table3D<SWORD>& BattleTables::spfCavChangeTime()
{
  return s_tables.spfCavChangeTime();
}

const Table3D<SWORD>& BattleTables::spfHvyArtChangeTime()
{
  return s_tables.spfHvyArtChangeTime();
}

const Table3D<SWORD>& BattleTables::spfFootArtChangeTime()
{
  return s_tables.spfFootArtChangeTime();
}

const Table3D<SWORD>& BattleTables::spfHorseArtChangeTime()
{
  return s_tables.spfHorseArtChangeTime();
}

const Table1D<UWORD>& BattleTables::spfModifiers()
{
  return s_tables.spfModifiers();
}

/*--------------------------------------------------
 * Movement tables
 */

const Table2D<UBYTE>& BattleTables::basicMovementRate()
{
  return s_tables.basicMovementRate();
}

const Table2D<UBYTE>& BattleTables::roadMovementRate()
{
  return s_tables.roadMovementRate();
}

const Table3D<UWORD>& BattleTables::terrainModifiers()
{
  return s_tables.terrainModifiers();
}

const Table3D<UWORD>& BattleTables::pathModifiers()
{
  return s_tables.pathModifiers();
}

const Table3D<UBYTE>& BattleTables::terrainDisorder()
{
  return s_tables.terrainDisorder();
}

const Table3D<UBYTE>& BattleTables::prohibitedTerrain()
{
  return s_tables.prohibitedTerrain();
}

/*
 * Fire-Combat tables
 */

const Table1D<UWORD>& BattleTables::fireCombatRange()
{
  return s_tables.fireCombatRange();
}

const Table2D<UWORD>& BattleTables::fireCombatPoints()
{
  return s_tables.fireCombatPoints();
}

const Table2D<UWORD>& BattleTables::fireCombatCasualty()
{
  return s_tables.fireCombatCasualty();
}

const Table1D<SWORD>& BattleTables::fireCombatDieRollModifiers()
{
   return s_tables.fireCombatDieRollModifiers();
}
const Table1D<SWORD>& BattleTables::fireCombatFireValueMultipliers()
{
   return s_tables.fireCombatFireValueMultipliers();
}
const Table1D<SWORD>& BattleTables::moraleTestDieRollModifiers()
{
   return s_tables.moraleTestDieRollModifiers();
}
const Table1D<SWORD>& BattleTables::corpDeterminationDieRollModifiers()
{
   return s_tables.corpDeterminationDieRollModifiers();
}


/*
 * Shock Combat tables
 */
const Table2D<UBYTE>& BattleTables::infVsInfShockLoss()
{
  return s_tables.infVsInfShockLoss();
}
const Table2D<UBYTE>& BattleTables::infVsCavShockLoss()
{
  return s_tables.infVsCavShockLoss();
}
const Table2D<UBYTE>& BattleTables::infVsCavChanceToInflictShockLoss()
{
   return s_tables.infVsCavChanceToInflictShockLoss();
}
const Table1D<SWORD>& BattleTables::infVsInfDieRollModifiers()
{
   return s_tables.infVsInfDieRollModifiers();
}
const Table1D<SWORD>& BattleTables::infVsInfMoraleDieRollModifiers()
{
   return s_tables.infVsInfMoraleDieRollModifiers();
}
const Table1D<SWORD>& BattleTables::infVsCavCavDieRollModifiers()
{
   return s_tables.infVsCavCavDieRollModifiers();
}
const Table1D<SWORD>& BattleTables::infVsCavInfDieRollModifiers()
{
   return s_tables.infVsCavInfDieRollModifiers();
}
const Table1D<SWORD>& BattleTables::cavVsCavDieRollModifiers()
{
   return s_tables.cavVsCavDieRollModifiers();
}
const Table1D<SWORD>& BattleTables::moraleBonus()
{
   return s_tables.moraleBonus();
}
const Table1D<SWORD>& BattleTables::cavChargeTestDieRollModifiers()
{
   return s_tables.cavChargeTestDieRollModifiers();
}

const Table1D<UBYTE>& BattleTables::noEffectCutoff()
{
   return s_tables.noEffectCutoff();
}

const Table1D<SWORD>& BattleTables::moraleLossMultiplier()
{
   return s_tables.moraleLossMultiplier();
}

const Table1D<UBYTE>& BattleTables::criticalLossCutoff()
{
   return s_tables.criticalLossCutoff();
}

const Table1D<UBYTE>& BattleTables::recoveryTime()
{
   return s_tables.recoveryTime();
}

const Table1D<SWORD>& BattleTables::recovery()
{
   return s_tables.recovery();
}
const Table1D<SWORD>& BattleTables::chargeTestDieRollModifiers()
{
   return s_tables.chargeTestDieRollModifiers();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
