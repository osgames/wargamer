/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*

FractMap.cpp

This file creates a quick heightmap on a square grid, where edge size is a power of two
Square heightmaps tesselate perfectly.  If a different sized map is needed, the system generates a large square map & crops to dimensions

*/



#include "stdinc.hpp"
#include "fractmap.hpp"

#include "wg_rand.hpp"
#include <malloc.h>     // todo: replace with C++ new/delete

#define SAVE_RAW_HEIGHTFIELD




FractalMap::FractalMap(unsigned int w, unsigned int h, FractalMapParameters * params) {

    width = w;
    height = h;

    TempData = NULL;
    MapData = NULL;

    // increase multiple until a power of 2 is found which contains the desired width & height
    int multiple = 2;
    while(width > multiple || height > multiple) multiple *= 2;

    // store this value as the actual dimensions to generate at
    grid_width = multiple;
    grid_height = multiple;


    // set the default generation values
    default_parameters.start_height = 128;
    default_parameters.minimum_height = 0;
    default_parameters.maximum_height = 255;
    default_parameters.height_variation = 64;
    default_parameters.height_roughness = 8;

    // either point the generation parameters to the default, or to the input parameters
    if(params == NULL) parameters = &default_parameters;
    else parameters = params;


    // these are the default values from the previous fractal map version, for reference
/*
    start_height =24;
    minimum_height = 24;
    maximum_height = 56;
    height_variation = 11;
    height_roughness  = 1;
*/

    // allocate the MapData memory (standard C allocs should be Ok)
    MapData = (char *) malloc(width * height);

    // generate the heightfield into MapData
    GenerateMap();

    FindMinMax();

}



/*
Free up any resources
*/

FractalMap::~FractalMap(void) {

    if(TempData != NULL) {
        free(TempData);
        TempData = NULL; }

    if(MapData != NULL) {
        free(MapData);
        MapData = NULL; }

}





/*
Generate the fractal map to the set parameters
TempData is allocated to (grid_width * grid_height) for this process
After generation height data is copied & cropped into MapData, & TempData is freed
This might seem silly, but it saves on long-term memory usage
*/

void
FractalMap::GenerateMap(void) {



// allocate some memory to generate the ^2 grid
TempData = (char *) malloc(grid_width * grid_height);

    // initial random factor, reduced each iteration
    int random_start = parameters->height_variation;
    // current random value for each new pixel
    int random_value;
    // variables to step through array
    int xpos,ypos;
    // step values
    int x_step = grid_width;
    int y_step = grid_height;
    // size of squares / diamonds we're dealing with
    int x_gridsize = grid_width;
    int y_gridsize = grid_height;
    // midpoint coordinates for defining centre of squares / corners of diamonds
    int x_midpoint = x_step / 2;
    int y_midpoint = y_step /2;

    // initialise the map's four corner values
    TempData[MapOffset(0,0)] = parameters->start_height;
    TempData[MapOffset(x_gridsize,0)] = parameters->start_height;
    TempData[MapOffset(0,y_gridsize)] = parameters->start_height;
    TempData[MapOffset(x_gridsize,y_gridsize)] = parameters->start_height;

    // height values at each corner of square / diamond
    int height_a, height_b, height_c, height_d;
    // the average of the above four heights
    int average_height;

    // repeat the algorithm until we are dealing with adjacent pixels & array is complete
    while(x_step>1 || y_step>1) {

    // loop through height array, setting the central point of each square
    
        for(ypos=0; ypos<grid_height; ypos+=y_step) {

            for(xpos = 0; xpos<grid_width; xpos+=x_step) {
                // select a new random value
                random_value = CRandom::get(random_start) - (random_start/2);
                // get height data of points defining the current square
                height_a = TempData[MapOffset(xpos,ypos)];
                height_b = TempData[MapOffset(xpos+x_gridsize,ypos)];
                height_c = TempData[MapOffset(xpos+x_gridsize,ypos+y_gridsize)];
                height_d = TempData[MapOffset(xpos,ypos+y_gridsize)];
                // average heights, and keep within constraints
                average_height = (height_a + height_b + height_c + height_d) /4;
                average_height+=random_value;
                if(average_height < parameters->minimum_height) average_height=parameters->minimum_height;
                if(average_height > parameters->maximum_height) average_height=parameters->maximum_height;
                // set height at point in centre of square
                TempData[MapOffset(xpos+x_midpoint,ypos+y_midpoint)] = (unsigned char) average_height;
            }
        }



    // loop through height array, setting the central point of each diamond above & each diamond to left of coordinates
    
        for(ypos=0; ypos<grid_height; ypos+=y_step) {

            for(xpos = 0; xpos<grid_width; xpos+=x_step) {
                // select a new random value
                random_value = CRandom::get(random_start) - (random_start/2);
                // get height data of points defining the diamond above coordinate position
                height_a = TempData[MapOffset(xpos,ypos)];
                height_b = TempData[MapOffset(xpos+x_midpoint,ypos-y_midpoint)];
                height_c = TempData[MapOffset(xpos+x_gridsize,ypos)];
                height_d = TempData[MapOffset(xpos+x_midpoint,ypos+y_midpoint)];
                // average heights, and keep within constraints
                average_height = (height_a + height_b + height_c + height_d) /4;
                average_height+=random_value;
                if(average_height < parameters->minimum_height) average_height=parameters->minimum_height;
                if(average_height > parameters->maximum_height) average_height=parameters->maximum_height;
                // set height at point in centre of diamond
                TempData[MapOffset(xpos+x_midpoint,ypos)] = average_height;


                // select a new random value
                random_value = CRandom::get(random_start) - (random_start/2);
                // get height data of points defining the diamond left of coordinate position
                height_a = TempData[MapOffset(xpos,ypos)];
                height_b = TempData[MapOffset(xpos+x_midpoint,ypos+y_midpoint)];
                height_c = TempData[MapOffset(xpos,ypos+y_gridsize)];
                height_d = TempData[MapOffset(xpos-x_midpoint,ypos+y_midpoint)];
                // average heights, and keep within constraints
                average_height = (height_a + height_b + height_c + height_d) /4;
                average_height+=random_value;
                if(average_height < parameters->minimum_height) average_height=parameters->minimum_height;
                if(average_height > parameters->maximum_height) average_height=parameters->maximum_height;
                // set height at point in centre of diamond
                TempData[MapOffset(xpos,ypos+y_midpoint)] = (unsigned char) average_height;
            }
        }



    // halve the stepping value through the array
    x_step /= 2;
    y_step /= 2;
    // halve the size of the squares / diamonds we're dealing with
    x_gridsize /= 2;
    y_gridsize /= 2;
    // halve the midpoints for calculating diamonds & suqres centres
    x_midpoint /= 2;
    y_midpoint /= 2;
    // reduce the random value for the next iteration
    random_start -= parameters->height_roughness;
    }


// copy required dimensions to MapData
CropMap();
// free up temp memory
free(TempData);
TempData = NULL;

// save out map data if debugging
#ifdef SAVE_RAW_HEIGHTFIELD
SaveAsRaw("initial.raw");
#endif

}




/*
Crop the generated square map, to the dimensions specified by (width, height)
Copies data to the MapData array.  Uses a crap one-character-at-a-time method which really should be changed
*/

void
FractalMap::CropMap(void) {
int x,y;
int offset;

    for(y=0; y<height; y++) {
        for(x=0; x<width; x++) {
            offset = (y*width) + x;
            MapData[offset] = TempData[offset];
        }
    }

}


/*
Resamples the height data to run from (0...max)
Uses the maximum_height parameter as the original height
Intended for reducing a full range of heights to a (0...n) range as used the battle-game
*/

void
FractalMap::ResampleHeights(void) {
if(MapData == NULL) return;

int max = parameters->resample_range;

int x,y;
int offset;
float ratio = ((float) max) / ((float)parameters->maximum_height);
// using this just to clarify the casting between float & u.char
float val;

    for(y=0; y<height; y++) {

        for(x=0; x<width; x++) {

            offset = (y*width) + x;
            val = MapData[offset];
            val *= ratio;
            MapData[offset] = val;
            
        }
    }

}


/*
Resamples height data relative to the range (actual_min...actual_max)
Reduces data to run from (0...max)
*/

void
FractalMap::ResampleHeightsRelative(void) {
if(MapData == NULL) return;

int max = parameters->resample_range;

int x,y;
int offset;
float ratio = (float) max / (float) (actual_max - actual_min);
float val;

    for(y=0; y<height; y++) {

        for(x=0; x<width; x++) {

            offset = (y*width) + x;
            val = MapData[offset] - actual_min;
            val *= ratio;
            MapData[offset] = val;

        }
    }

// save out map data if debugging
#ifdef SAVE_RAW_HEIGHTFIELD
SaveAsRaw("resample.raw");
#endif

}


/*
Smooths the height sata out
*/

void
FractalMap::SmoothHeights(void) {

    if(!MapData) return;

    TempData = (char *) malloc(width * height);

    int x,y;

    for(y=0; y<height; y++) {

        for(x=0; x<width; x++) {

            float average = 0;
            int offset;

            // x,y
            offset = MapOffset(x,y);
            average+=MapData[offset];
            // x-1, y
            offset = MapOffset(x-1,y);
            average+=MapData[offset];
            // x+1, y
            offset = MapOffset(x+1,y);
            average+=MapData[offset];
            // x, y-1
            offset = MapOffset(x,y-1);
            average+=MapData[offset];
            // x, y+1
            offset = MapOffset(x,y+1);
            average+=MapData[offset];

            average /= 5;

            TempData[(y*width)+x] = (char) average;
        }
    }

    // now copy back into MapData
    memcpy(MapData, TempData, (width*height) );

    free(TempData);
    TempData = NULL;

#ifdef SAVE_RAW_HEIGHTFIELD
SaveAsRaw("smoothed.raw");
#endif

}




/*
Finds the minimum & maximum heights in the map
*/

void
FractalMap::FindMinMax(void) {
if(MapData == NULL) return;

int x,y;
// set these at opposite extremes
actual_min=255;
actual_max=0;
char h;

    for(y=0;y<height;y++) {

        for(x=0;x<width;x++) {

            h = MapData[(y*width)+x];
            if(h<actual_min) actual_min=h;
            if(h>actual_max) actual_max=h;
        }
    }

int finished=1;

}



/*
Save out the generated map as a .RAW file
This is the MapData, so when loading in the correct width  height must be specified
*/
void
FractalMap::SaveAsRaw(char * filename) {
if(MapData == NULL) return;

    FILE * file;
    // open "heights.raw" for writing
    file = fopen(filename, "wb");
    if(file == NULL) return;
    // save out data
    fwrite(MapData, 1, (width * height), file);
    // close file
    fclose(file);

}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
