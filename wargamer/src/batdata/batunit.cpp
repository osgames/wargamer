/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "batunit.hpp"
#include "filebase.hpp"
#include "ob.hpp"

/*
 * Update facing...
 * assumes that HexPosition::Facing will wrap around
 *
 * Return true if arrived
 */

bool BattleUnit::updateFacing(BattleMeasure::HexPosition::Facing change)
{
        BattleMeasure::HexPosition::Facing wantDelta = d_wantFace - d_facing;
        if(wantDelta > BattleMeasure::HexPosition::FacingResolution/2)
        {
                wantDelta = BattleMeasure::HexPosition::FacingResolution - wantDelta;
        }

        BattleMeasure::HexPosition::Facing changeDelta = change;
        if(changeDelta > BattleMeasure::HexPosition::FacingResolution/2)
        {
                changeDelta = BattleMeasure::HexPosition::FacingResolution - changeDelta;
        }

        if(changeDelta >= wantDelta)
        {
                d_facing = d_wantFace;
                return true;
        }
        else
        {
                d_facing += change;
                return false;
        }
}

/*
 * File Interface
 */

const UWORD BattleUnit::s_fileVersion = 0x0003;

Boolean BattleUnit::readData(FileReader& f, OrderBattle& ob)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  if(version >= 0x0002)
  {
    f >> d_position;
    d_routeList.readData(f, &ob);
    f >> d_rFacing;
    f >> d_facing;
    f >> d_wantFace;
    f >> d_nextFace;
    UBYTE b;
    f >> b;
    d_finalPos = static_cast<BattleMeasure::HexPosition::MoveTo>(b);
    f >> d_startSPChange;
    f >> d_endSPChange;
    f >> b;
    d_visibility = static_cast<Visibility::Value>(b);
    f >> d_speed;
    f >> d_moveFlags;
    f >> d_combatFlags;
    f >> d_miscFlags;

    if(version < 0x0003)
      INCREMENT(d_visibility);      // Added NeverSeen as first value

  }
  else
  {
    // set data to sensible values

    if(version >= 0x0001)
    {
        BattleMeasure::HexCord h;
        h.readData(f);
        hex(h);
    }

    // using BattleMeasure::HexPosition;
    // d_position.hexPosition() = HexPosition(0, HexPosition::centerPos());

  }

  return f.isOK();
}

Boolean BattleUnit::writeData(FileWriter& f, OrderBattle& ob) const
{
    f << s_fileVersion;
    f << d_position;
    d_routeList.writeData(f, &ob);
    f << d_rFacing;
    f << d_facing;
    f << d_wantFace;
    f << d_nextFace;
    f.putUByte(d_finalPos);
    f << d_startSPChange;
    f << d_endSPChange;
    f.putUByte(d_visibility);
    f << d_speed;
    f << d_moveFlags;
    f << d_combatFlags;
    f << d_miscFlags;

    return f.isOK();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
