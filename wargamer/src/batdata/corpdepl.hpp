/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CORPDEPL_HPP
#define CORPDEPL_HPP

#include "bd_dll.h"
#include "mytypes.h"
#include "myassert.hpp"
#include "batunit.hpp"
// #include "batdata.hpp"
#include "BattleDataFwd.hpp"

class RandomNumber;

// How a corps is to be layed out
// Note: using Bens's Corp Deployment doc

class CorpDeployment {
public:
  enum XXType {
    LineXX,        // a line XX, Inf or Cav(Cav only if part of a Cav XXX)
    SupportXX,     // a support Art XX or a support Cav XX (if part of Inf or Combined-Arms XXX)

    XXType_HowMany,
    XXType_Undefined = XXType_HowMany
  };

  enum Where {
    Front,
    Rear,

    Where_HowMany
  };
private:
  UBYTE   d_nFront;
  UBYTE   d_nRear;
  int     d_frontID;
  int     d_rearID;
  XXType* d_front;
  XXType* d_rear;

public:

  CorpDeployment() :
    d_nFront(0),
    d_nRear(0),
    d_frontID(-1),
    d_rearID(-1),
    d_front(0),
    d_rear(0)
  {
  }

  CorpDeployment(UBYTE nFront, UBYTE nRear) :
    d_nFront(nFront),
    d_nRear(nRear),
    d_frontID(-1),
    d_rearID(-1),
    d_front(new XXType[nFront]),
    d_rear(new XXType[nRear])
  {
    ASSERT(d_front);
    ASSERT(d_rear);
  }

  ~CorpDeployment()
  {
    if(d_front)
      delete[] d_front;

    if(d_rear)
      delete[] d_rear;
  }

  void init(UBYTE nFront, UBYTE nRear)
  {
    d_nFront = nFront;
    d_nRear = nRear;
    d_front = new XXType[nFront];
    ASSERT(d_front);
    d_rear = new XXType[nRear];
    ASSERT(d_rear);
  }

  // iterators for front and back of deployment
  bool    iterFront()   { d_frontID++; return (d_frontID < d_nFront); }
  XXType  frontItem()   { return (d_frontID < d_nFront) ? d_front[d_frontID] : XXType_Undefined; }
  void    rewindFront() { d_frontID = -1; }
  void    setFrontItem(XXType t) { ASSERT(d_frontID < d_nFront); d_front[d_frontID] = t; }
  UBYTE   nFront() const { return d_nFront; }

  bool    iterRear()    { d_rearID++; return (d_rearID < d_nRear); }
  XXType  rearItem()    { return (d_rearID < d_nRear) ? d_rear[d_rearID] : XXType_Undefined; }
  void    rewindRear()  { d_rearID = -1; }
  void    setRearItem(XXType t) { ASSERT(d_rearID < d_nRear); d_rear[d_rearID] = t; }
  UBYTE   nRear() const { return d_nRear; }

  bool   iter(Where w)   { return (w == Front) ? iterFront() : iterRear(); }
  XXType item(Where w)   { return (w == Front) ? frontItem() : rearItem(); }
  void   rewind(Where w)
  {
    if(w == Front)
      rewindFront();
    else
      rewindRear();
  }
  void   setItem(Where w, XXType t)
  {
    if(w == Front)
      setFrontItem(t);
    else
      setRearItem(t);
  }
  UBYTE nUnits(Where w) const { return (w == Front) ? d_nFront : d_nRear; }
};


BATDATA_DLL  bool corpDeployment(const CRefBattleCP& cp, UBYTE nInf, UBYTE nCav, UBYTE nArt,
  CorpDeployment& cd, RandomNumber& lRand, bool countOnly = False);

BATDATA_DLL void corpsTypes(RCPBattleData bd, const CRefBattleCP& cp, UBYTE& nInfXX,
    UBYTE& nCavXX, UBYTE& nArtXX, int& totalSP, bool deploying);
   // Get composition of corps

#endif
