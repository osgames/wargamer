/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_GINDEX_HPP
#define B_GINDEX_HPP

#ifndef __cplusplus
#error b_gindex.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Wrapper for Sprite Index, to place it inside it's own namespace
 *
 *----------------------------------------------------------------------
 */

namespace BattleGraphics
{

	#include "g_terr.hpp"
	#include "g_build.hpp"

};		// namespace Battlegraphics

#endif /* B_GINDEX_HPP */

