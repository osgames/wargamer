/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_TERR_HPP
#define B_TERR_HPP

#ifndef __cplusplus
#error b_terr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Terrain Table structures
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "mytypes.h"
#include "myassert.hpp"
#include "sprlib.hpp"
#include "StringPtr.hpp"
#include "palette.hpp"
#include "batcord.hpp"
#include <vector>             // STL container

// using namespace SWG_Sprite;

// Define this to use static preset terrain tables instead of reading from files
#define STATIC_DATA_TABLES

#ifdef STATIC_DATA_TABLES
/*
 * Logical Terrain Initialisation Tables
 *
 * If this is changed, then logicalTerrainData must also be changed
 */

enum LogicalTerrainEnum
{
   LT_None = 0,            // 0 = No Terrain

      LT_Clear,                       // Clear is same as Grass
      LT_Water,
      LT_Orchard,                     // All wood types will also have overlaid sprites
      LT_Vineyard,
      LT_LightWood,
      LT_DenseWood,
      LT_PloughedField,
      LT_CornField,                       // Corn/Wheat Field
      LT_WheatField,
      LT_Rough,
      LT_Marsh,
      LT_Town,                                // Also has overlaid sprites
      LT_Village,
      LT_Farm,
      LT_Cemetary,

      // Paths are included in logical terrain table

      LT_Road,
      LT_Track,
      LT_River,
      LT_Stream,
      LT_SunkenRoad,
      LT_SunkenTrack,
      LT_SunkenRiver,
      LT_SunkenStream,

      // bridges are treat as paths at creation time (but are later replaced by building graphics, before play)
      LT_RoadBridge,
      LT_TrackBridge,
      // etc...

      LT_HowMany
};


enum LogicalPathEnum {

   LP_None = 0,

      LP_Road,
      LP_Track,
      LP_River,
      LP_Stream,

      LP_SunkenRoad,
      LP_SunkenTrack,
      LP_SunkenRiver,
      LP_SunkenStream,

      LP_RoadBridge,
      LP_TrackBridge,

      LP_HowMany

};


enum GraphicalTerrainEnum {

   GT_None = 0,

      GT_Grass,
      GT_Water,
      GT_Orchard,
      GT_Vineyard,
      GT_LightWood,
      GT_DenseWood,
      GT_PloughedField,
      GT_CornField,
      GT_WheatField,
      GT_Rough,
      GT_Marsh,
      GT_Town,
      GT_Village,
      GT_Farm,
      GT_Cemetary,

      GT_HowMany

};



#endif

typedef UBYTE EdgeMap;                  // What edges does something use?

inline bool edgesOverlap(EdgeMap e1, EdgeMap e2)
{
   ASSERT(e1 < 64);
   ASSERT(e2 < 64);
   return (e1 & e2) != 0;
}

/*
 * Logical Terrain
 */


class LTerrainTable
{
   public:
      typedef UBYTE Index;
      enum { None = 0, First = 1, MaxIndex = UBYTE_MAX  };

      struct Info
      {
         bool    d_isWater;
         bool    d_isTown;

         // Movement modifiers, etc, go here.
         int d_movement;
      };

      LTerrainTable() { }
      ~LTerrainTable() { }

      BATDATA_DLL const Info& get(Index t) const;
      const Info& operator [](Index t) const { return get(t); }

      void init();
      // Read table from data files
      // throw exception if there is an error

      class Error {  };

   private:
      std::vector<Info> d_items;
};

// typedef LTerrainTable::Info LTerrainInfo;

class PathTable
{
   public:
      typedef UBYTE Index;
      enum { None = 0, First = 1, MaxIndex = UBYTE_MAX  };

      enum BasicType
      {
         NoType,
         Road,
         River,
         Rail,
         Bridge,
      };

      class Info
      {
         public:
            BATDATA_DLL Info();
            BATDATA_DLL ~Info();
            BATDATA_DLL Info& operator=(const Info& inf);
            BATDATA_DLL Info(const Info& inf);

            BasicType basicType() const { return d_type; }
            void basicType(BasicType basicType) { d_type = basicType; }

            LTerrainTable::Index terrain() const { return d_terrain; }
            void terrain(LTerrainTable::Index l) { d_terrain = l; }

            BATDATA_DLL SWG_Sprite::SpriteLibrary::Index graphic(EdgeMap edges) const;
            void graphic(SWG_Sprite::SpriteLibrary::Index g) { d_graphic = g; }

            const char* name() const { return d_name.toStr(); }
            void name(const char* s) { d_name = s; }
            const char* nameAbrev() const { return d_nameAbrev.toStr(); }
            void nameAbrev(const char* s) { d_nameAbrev = s; }

            Index bridge() const { return d_bridge; }
            void bridge(Index b) { d_bridge = b; }

            void graphicOffsets(UWORD* offsets) { d_graphicOffsets = offsets; }

         private:
            // Info about path
            BasicType               d_type;
            LTerrainTable::Index    d_terrain;           // Logical Terrain Type
            SWG_Sprite::SpriteLibrary::Index    d_graphic;           // Graphic for base terrain
            StringPtr               d_name;
            StringPtr               d_nameAbrev;
            Index                   d_bridge;            // Path to use for bridge
            UWORD*                  d_graphicOffsets;     // edge to graphic conversion table, 0 assumes 1..63
      };

      PathTable();
      ~PathTable();

      BATDATA_DLL const Info& operator[] (Index i) const;
      BATDATA_DLL Info& operator[] (Index i);

      void init();
      // Read table from data files
      // throw exception if there is an error

      typedef std::vector<Info>::const_iterator const_iterator;
      std::vector<Info>::const_iterator begin() const { return d_items.begin(); }
      std::vector<Info>::const_iterator end() const { return d_items.end(); }

      Index first() const { return First; }
      Index last() const { return First + count(); }
      int count() const { return d_items.size(); }

      bool isType(Index i, BasicType t) const;
      // Return true if path type i is of basic type t

      class Error { };
   private:
      std::vector<Info> d_items;
};

class EdgeTable
{
   public:
      typedef UBYTE Index;
      enum { None = 0, First = 1, MaxIndex = UBYTE_MAX  };

      struct Info
      {
         public:
         BATDATA_DLL Info();
         BATDATA_DLL ~Info();

         const char* name() const { return d_name.toStr(); }
         void name(const char* s) { d_name = s; }

         SWG_Sprite::SpriteLibrary::Index graphic() const { return d_graphic; }
         void graphic(SWG_Sprite::SpriteLibrary::Index g) { d_graphic = g; }

      private:
         // Info about path
         StringPtr d_name;
         SWG_Sprite::SpriteLibrary::Index d_graphic;
      };

      EdgeTable();
      ~EdgeTable();

      BATDATA_DLL const Info& operator[] (Index i) const;
      BATDATA_DLL Info& operator[] (Index i);

      void init();
      // Read table from data files
      // throw exception if there is an error

      typedef std::vector<Info>::const_iterator const_iterator;
      std::vector<Info>::const_iterator begin() const { return d_items.begin(); }
      std::vector<Info>::const_iterator end() const { return d_items.end(); }

      Index first() const { return First; }
      Index last() const { return First + count(); }
      int count() const { return d_items.size(); }

      class Error { };
   private:
      std::vector<Info> d_items;
};



/*
 * Main Terrain Table
 */

/*
 * Main Terrain Table
 * The hexData indexes into a table of these
 */


class TerrainTable
{
   public:
      typedef UBYTE Index;
      enum { None = 0, First = 1, MaxIndex = UBYTE_MAX  };

      class Info
      {
         public:

            LTerrainTable::Index logical() const { return d_logical; }
            void logical(LTerrainTable::Index l) { d_logical = l; }

            SWG_Sprite::SpriteLibrary::Index baseGraphic() const { return d_baseGraphic; }
            void baseGraphic(SWG_Sprite::SpriteLibrary::Index g) { d_baseGraphic = g; }

            const char* name() const { return d_name.toStr(); }
            void name(const char* s) { d_name = s; }

         private:

            LTerrainTable::Index    d_logical;              // Logical Terrain Type
            SWG_Sprite::SpriteLibrary::Index    d_baseGraphic;  // Graphic for base terrain
            StringPtr               d_name;
      };


      TerrainTable();
      ~TerrainTable();

      void init();
      // Initialise, using global scenario to get filenames of
      // data tables, etc.

      Index first() const { return First; }
      Index last() const { return First + count(); }
      int count() const { return d_items.size(); }


      BATDATA_DLL const Info& get(Index t) const;
      BATDATA_DLL Info& get(Index t);
      const Info& operator [](Index t) const { return get(t); }

      const LTerrainTable::Info& logical(Index t) const { return d_logical[get(t).logical()]; }
      const LTerrainTable& logical() const { return d_logical; }

      const PathTable::Info& path(PathTable::Index i) const { return d_pathTable[i];  }
      const EdgeTable::Info& edge(EdgeTable::Index i) const { return d_edgeTable[i];  }

      BATDATA_DLL const char* pathNameAbrev(PathTable::Index i) const;
      BATDATA_DLL const char* pathName(PathTable::Index i) const;
      BATDATA_DLL const char* edgeName(PathTable::Index i) const;

      const PathTable& pathTable() const { return d_pathTable; }
      const EdgeTable& edgeTable() const { return d_edgeTable; }

      BATDATA_DLL SWG_Sprite::SpriteLibrary::Index coastGraphic(EdgeMap edge) const;
      BATDATA_DLL SWG_Sprite::SpriteLibrary::Index estuaryGraphic(BattleMeasure::HexCord::HexDirection d) const;

      class Error { };
   private:
      std::vector<Info>            d_items;
      LTerrainTable           d_logical;
      PathTable               d_pathTable;
      EdgeTable               d_edgeTable;
      SWG_Sprite::SpriteLibrary::Index    d_coastGraphic;
      SWG_Sprite::SpriteLibrary::Index    d_estuaryGraphic;
};      // TerrainTable


/*
 * Some typedefs to simplify use of this header file
 */

typedef TerrainTable::Index TerrainType;
typedef PathTable::Index PathType;
typedef EdgeTable::Index EdgeType;

#endif /* B_TERR_HPP */


