/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATCORD_HPP
#define BATCORD_HPP

#ifndef __cplusplus
#error batcord.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Coordinate System
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "mytypes.h"
#include "trig.hpp"
#include "myassert.hpp"
#include "point.hpp"
#include "battime.hpp"
#include "sqrt.hpp"
#include <math.h>
#include <ostream>

class OrderBattle;
class FileReader;
class FileWriter;

namespace BattleMeasure
{

/*
 * Useful values
 */

/*---------------------------------------------------------------------
 * Some useful constants
 */

const int YardsPerMile  = 1760;
const int FeetPerYard   =         3;
const int InchesPerFoot =        12;

const int XYardsPerHex = 220;
const int XHexPerMile = YardsPerMile / XYardsPerHex;

const int QuantaPerInch = 4;                    // Resolution of distance 1/4 inch

/*
 * Hex Coordinates are a way of referencing which hex something
 * is in.
 *
 * Numbering is from bottom left (i.e. similar to cartesian coordinates).
 * The 2nd row (y=1) has its physical X Cordinate offset by Half a hex width
 *
 * Since each hex's width is 32 yards, a UBYTE allows distances of up
 * to 32 miles.
 */

class HexCord
{
    /*--------------------------------
     * Public Definitions
     */

    public:
        typedef UBYTE Cord;

        /*
         * Directions from centre into edges
         * They are defined in the same order as cartesian angles
         * i.e. anticlockwise from horizontal-right
         */

        enum HexDirection {
            Right,
            UpRight,
            UpLeft,
            Left,
            DownLeft,
            DownRight,

            Stationary,

            First = Right,
            End = Stationary,

            Count = End - First,
        };

    /*--------------------------------
     * Public Functions
     */

    public:
        HexCord() : d_pt(0,0) { }
        HexCord(Cord x, Cord y) : d_pt(x, y) { }
        HexCord(const HexCord& hc) : d_pt(hc.d_pt) { }
        ~HexCord() { }
        HexCord& operator = (const HexCord& hc) { d_pt = hc.d_pt; return *this; }

        /*
         * File Interface
         */

        BATDATA_DLL Boolean readData(FileReader& f);
        BATDATA_DLL Boolean writeData(FileWriter& f) const;

        BATDATA_DLL bool move(HexDirection dir, HexCord& destHex, const HexCord& maxHex) const;
                            // Get a new coordinate of hex in given direction

        Cord x() const { return d_pt.x(); }
        Cord y() const { return d_pt.y(); }

        void x(Cord x) { d_pt.x(x); }
        void y(Cord y) { d_pt.y(y); }

        BATDATA_DLL friend bool operator == (const HexCord& lhs, const HexCord& rhs);
        BATDATA_DLL friend bool operator < (const HexCord& lhs, const HexCord& rhs);

        int distanceTo(HexCord hex) { return((int)fsqrt((abs(x()-hex.x())*abs(x()-hex.x()))+(abs(y()-hex.y())*abs(y()-hex.y())))); }

        /*
         * Get opposite direction to current direction,
         * e.g. left->right
         *      upLeft -> downRight
         */

        static HexDirection oppositeDirection(HexDirection d)
        {
            return static_cast<HexDirection>( (d + 3) % 6 );
        }

        /*
         * Get edge to continue moing in same orthogonal direction
         *    right <-> right
         *     left <-> left
         *   upLeft <-> upRight
         * downLeft <-> downRight
         */

        static HexDirection verticalEdge(HexDirection d)
        {
            static HexDirection vTable[] =
            {
                Right,          // Right
                UpLeft,         // UpRight
                UpRight,        // UpLeft
                Left,           // Left
                DownRight,      // DownLeft
                DownLeft,       // DownRight
                Stationary,     // Stationary
            };

            ASSERT(d >= First);
            ASSERT(d <= End);

            return vTable[d];
        }

        static HexDirection clockwise(HexDirection d)
        {
            ASSERT(d < End);
            ASSERT(d >= First);

            if(d <= First)
                return static_cast<HexDirection>(End - 1);
            else
                return static_cast<HexDirection>(d - 1);
        }

        static HexDirection anticlockwise(HexDirection d)
        {
            ASSERT(d < End);
            ASSERT(d >= First);

            d = static_cast<HexDirection>(d + 1);
            if(d >= End)
                d = First;

            return d;
        }

        operator Point<Cord>& () { return d_pt; }
        operator const Point<Cord>& () const { return d_pt; }

    /*--------------------------------
     * Private Functions
     */

    private:

    /*--------------------------------
     * Private Data
     */

    private:
        static UWORD s_fileVersion;
        Point<Cord> d_pt;

};

inline bool operator == (const HexCord& lhs, const HexCord& rhs)
{
    return (lhs.d_pt == rhs.d_pt);
}

inline bool operator < (const HexCord& lhs, const HexCord& rhs)
{
    if(lhs.d_pt.x() != rhs.d_pt.x())
        return lhs.d_pt.x() < rhs.d_pt.x();
    else
        return lhs.d_pt.y() < rhs.d_pt.y();
}

/*
 * function class used for STL comparisons
 */

class HexCordCompare
{
    public:
        bool operator()(const HexCord& h1, const HexCord& h2) const
        {
            if(h1.y() != h2.y())
                return h1.y() < h2.y();
            else
                return h1.x() < h2.x();
        }
};

/*
 * Distance Definitions
 *
 * Locations are measured from the bottom left of hex 0,0
 * so anything within a hex will have +ve coordinates
 *
 * Of course life isn't simple.
 * The visible area is (HexWidth/2, HexHeight/4) -> (HexWidth*hexes-HexWidth/2, HexHeight*hexes)
 * The maximum coordinates within a hex are (0,0) -> (hexes*HexWidth+hexWidth/2, hexes*HexHeight+HexHeight/4)
 *
 * Certain Y coordinates (+/- HexHeight/4) are shared by hexes
 */


typedef LONG Distance;                          // Long Distance (Can store up to 16000 miles!)

// typedef Point<Distance> Location;
// #define BattleMeasure::Location Point<BattleMeasure::Distance>

/*
 * Distance Utility Functions
 *
 * NB: Some confusion about QuantaPerYHex.  Does it mean the height from
 * the top to bottom of a hex?  Or does it mean the height of the
 * non-overlapping rectangle used to display hexes?
 * the first is Y=X*2/sqr(3).  The 2nd is Y=X*sqr(3)/2.
 *
 * From now on, I am using the 2nd definition, as it more useful.
 */

static const Distance QuantaPerFoot = QuantaPerInch * InchesPerFoot;
static const Distance QuantaPerYard = QuantaPerFoot * FeetPerYard;
static const Distance QuantaPerMile = QuantaPerYard * YardsPerMile;
static const Distance QuantaPerXHex = QuantaPerYard * XYardsPerHex;
//static const Distance QuantaPerYHex = ( (float)QuantaPerXHex * (float) (sqrt(3.0) / 2.0) );        // 0.866        // sqrt(3)/2
static const Distance QuantaPerYHex = static_cast<Distance>(static_cast<float>(QuantaPerXHex) * 0.866);
// static const Distance QuantaPerYHex = QuantaPerXHex * 1.15400538;    // 2/sqrt(3)

inline Distance inches(int i)
{
        return i * QuantaPerInch;
}

inline Distance feet(int f)
{
        return f * QuantaPerFoot;
}

inline Distance yards(int y)
{
        return y * QuantaPerYard;
}

inline Distance xHexes(int h)
{
        return h * QuantaPerXHex;
}

inline Distance yHexes(int h)
{
        return h * QuantaPerYHex;
}

inline Distance miles(int m)
{
    return m * QuantaPerMile;
}

static const Distance OneXHex    = QuantaPerXHex;
static const Distance HalfXHex   = QuantaPerXHex / 2;
static const Distance QuarterXHex = QuantaPerXHex / 4;

// static const Distance OneYHex         = QuantaPerYHex;
// static const Distance HalfYHex        = QuantaPerYHex / 2;
// static const Distance QuarterYHex = QuantaPerYHex / 4;
// static const Distance HexYOffset  = QuantaPerYHex - QuarterYHex;

static const Distance OneYHex    = (QuantaPerYHex * 4) / 3;
static const Distance HalfYHex   = OneYHex / 2;
static const Distance QuarterYHex = QuantaPerYHex / 3;
static const Distance HexYOffset  = QuantaPerYHex;


/*
 * A Position within a hex
 * Uses polar coordinates relative to the hex center
 */

class HexPosition
{
    /*
     * Public Definitions
     */

    public:
        typedef UWORD HexDistance;
        typedef Bangle Facing;
        enum { FacingResolution = 256 };        // 256 units per revolution
        enum {
            East = 0,
            NorthEastPoint = (1 * FacingResolution) / 12,
            NorthEastFace = (2 * FacingResolution) / 12,
            North = 64,
            NorthWestFace = (4 * FacingResolution) / 12,
            NorthWestPoint = (5 * FacingResolution) / 12,
            West = 128,
            SouthWestPoint = (7 * FacingResolution) / 12,
            SouthWestFace = (8 * FacingResolution) / 12,
            South = 192,
            SouthEastFace = (10 * FacingResolution) / 12,
            SouthEastPoint = (11 * FacingResolution) / 12,

            Facing_Undefined = 3,
            Facing_Default = North
        };

    /*--------------------------------------------------
     * Public Functions
     */

    public:
        HexPosition() :
            d_dir(Facing_Undefined),
            d_lastDir(Facing_Undefined),
            d_dist(s_centerPos),        // Used to be 0
            d_moveFrom(Center_Center),
            d_moveTo(MoveTo_Default) { }

        HexPosition(Facing dir, HexDistance dist) :
            d_dir(dir),
            d_lastDir(Facing_Undefined),
            d_dist(dist), // { }
            d_moveFrom(Center_Center),
            d_moveTo(MoveTo_Default) { }

        HexPosition(const HexPosition& hp) :
            d_dir(hp.d_dir),
            d_lastDir(hp.d_lastDir),
            d_dist(hp.d_dist), // { }
            d_moveFrom(Center_Center),
            d_moveTo(MoveTo_Default) { }

        ~HexPosition() { }

        HexPosition& operator = (const HexPosition& hp) 
        { 
        	d_dir = hp.d_dir; 
//			d_lastDir = hp.d_lastDir;
        	d_dist = hp.d_dist; 
//			d_moveFrom = hp.d_moveFrom;
//			d_moveTo = hp.d_moveTo;
        	return *this; 
        }

        /*
         * File Interface
         */

        BATDATA_DLL Boolean readData(FileReader& f);
        BATDATA_DLL Boolean writeData(FileWriter& f) const;

        BATDATA_DLL static wangle(Facing dir);

        enum MoveTo {
            East_Face,
            NE_Face,
            NW_Face,
            West_Face,
            SW_Face,
            SE_Face,

            Center_East,
            Center_NE,
            Center_NW,
            Center_West,
            Center_SW,
            Center_SE,
            Center_Center,

            MoveTo_HowMany,
            NoWhere,
            MoveTo_Default = NoWhere,
            MoveTo_Undefined,
            OuterFace_HowMany = SE_Face + 1
        };

        enum WhereAt {
                MovingToCenter,
                CenterHex,
                MovingToEdge,
                EdgeOfHex
        };

        BATDATA_DLL void init(Facing f);
        void facing(Facing f) { d_lastDir = d_dir; d_dir = f; }
        void start() { d_dist = 0; }

        BATDATA_DLL WhereAt moveAlong(Distance d);

        bool atCenter() const { return (d_dist == s_centerPos); }
        Facing facing() const { return d_dir; }
        Facing lastFacing() const { return d_lastDir; }
        HexDistance distance() const { return d_dist; }
        bool atEdge() const { return (d_dist == 0); } //s_edgePos); }
        void moveFrom(MoveTo mf) { d_moveFrom = mf; }
        void moveTo(MoveTo mf)   { d_moveTo = mf; }
        MoveTo moveFrom() const { return d_moveFrom; }
        MoveTo moveTo()   const { return d_moveTo; }

        static HexDistance centerPos() { return s_centerPos; }
        static HexDistance edgePos()   { return s_edgePos; }

    /*--------------------------------------------------
     * Private Data
     */

    private:
        static UWORD s_fileVersion;

        Facing d_dir;                   // Direction in 256ths of a circle
        Facing d_lastDir;
        HexDistance d_dist;     // Distance from centre
        MoveTo d_moveFrom;
        MoveTo d_moveTo;
        BATDATA_DLL static const HexDistance s_centerPos;
        BATDATA_DLL static const HexDistance s_edgePos;
};


/*
 * Combination of Hex and position within Hex
 */

class BattlePosition
{
    public:
        ~BattlePosition() { }

        // Various constructors

        BattlePosition() : d_hexCord(), d_hexPosition() { }
        BattlePosition(const HexCord& h) : d_hexCord(h), d_hexPosition() { }
        BattlePosition(const HexCord& h, const HexPosition& p) : d_hexCord(h), d_hexPosition(p) { }
        BattlePosition(const BattlePosition& bp) : d_hexCord(bp.d_hexCord), d_hexPosition(bp.d_hexPosition) { }

        // assignments

        BattlePosition& operator = (const HexCord& h)
        {
            d_hexCord = h;
            d_hexPosition = HexPosition();
            return *this;
        }

        BattlePosition& operator = (const BattlePosition& bp)
        {
            d_hexCord = bp.d_hexCord;
            d_hexPosition = bp.d_hexPosition;
            return *this;
        }

        // Accessors

        const HexCord& hex() const { return d_hexCord; }
        const HexPosition& hexPosition() const { return d_hexPosition; }
        HexPosition& hexPosition() { return d_hexPosition; }

        /*
         * File Interface
         */

        BATDATA_DLL Boolean readData(FileReader& f);
        BATDATA_DLL Boolean writeData(FileWriter& f) const;

    private:
        static UWORD s_fileVersion;
        HexCord                 d_hexCord;
        HexPosition d_hexPosition;
};

/*
 * Useful Angle functions
 */

inline HexCord::HexDirection facingToHex(HexPosition::Facing facing)
{
    // Adjust so centred, e.g. hex 0 = -21..+21
    static const HexPosition::Facing rounding = HexPosition::FacingResolution / (2 * 6);

    int hex = ( (facing + rounding) * 6) / HexPosition::FacingResolution;

    ASSERT(hex >= 0);
    ASSERT(hex < 6);

    return HexCord::HexDirection(hex);
}

inline HexPosition::Facing clockWiseFacing(HexPosition::Facing f)
{
    return (f == HexPosition::NorthEastPoint) ? 
            HexPosition::SouthEastPoint :
            (f - (((2 * HexPosition::FacingResolution) + 6)  / 12));
}

inline HexPosition::Facing antiClockWiseFacing(HexPosition::Facing f)
{
  return (f == HexPosition::SouthEastPoint) ? 
        HexPosition::NorthEastPoint :
        (f + (((2 * HexPosition::FacingResolution) + 6) / 12));
}

inline HexPosition::Facing hexToFacing(HexCord::HexDirection dir)
{
    return (dir * HexPosition::FacingResolution) / 6;
}

BATDATA_DLL HexPosition::Facing moveDirection(const HexCord& srcHex, const HexCord& destHex);

/*
 *
 * Note: Unit facing must be to one of the hexpoints (NE, N, NW, SW, S, SE)
 *
 * A unit's 'front' are the 2 hexes on either side of the point
 * The 'rear' are the 2 hexes opposite the point
 * The 'flanks' are the 2 hexes at 90 degree angles to the point
 *
 * Various functions for retrieving hexes relative to facing
 */

inline HexCord::HexDirection leftFront(HexPosition::Facing facing)
{
  HexCord::HexDirection r = facingToHex(facing);
  return HexCord::anticlockwise(r);
}

inline HexCord::HexDirection rightFront(HexPosition::Facing facing)
{
  return facingToHex(facing);
}

inline bool facingHexes(HexPosition::Facing facing, HexCord::HexDirection& left, HexCord::HexDirection& right)
{
  right = facingToHex(facing);
  left = HexCord::anticlockwise(right);

  return True;
}

inline bool rearHexes(HexPosition::Facing facing, HexCord::HexDirection& left, HexCord::HexDirection& right)
{
  facingHexes(facing, left, right);

  right = HexCord::oppositeDirection(left);
  left = HexCord::oppositeDirection(right);

  return True;
}

inline HexCord::HexDirection rightFlank(HexPosition::Facing facing)
{
  return HexCord::clockwise(rightFront(facing));
}

inline HexCord::HexDirection leftFlank(HexPosition::Facing facing)
{
  return HexCord::anticlockwise(leftFront(facing));
}

inline HexCord::HexDirection rightRear(HexPosition::Facing facing)
{
  return HexCord::oppositeDirection(leftFront(facing));
}

inline HexCord::HexDirection leftRear(HexPosition::Facing facing)
{
  return HexCord::oppositeDirection(rightFront(facing));
}

BATDATA_DLL HexPosition::Facing leftTurn(HexPosition::Facing f);
BATDATA_DLL HexPosition::Facing rightTurn(HexPosition::Facing f);
BATDATA_DLL HexPosition::Facing moveRight(HexPosition::Facing f);
BATDATA_DLL HexPosition::Facing moveLeft(HexPosition::Facing f);
//HexPosition::Facing hexToFacing(const HexPosition::Facing f, const HexCord& srcHex, const HexCord& destHex);
BATDATA_DLL HexCord::HexDirection hexDirection(const HexCord& srcHex, const HexCord& destHex);

BATDATA_DLL HexPosition::Facing nextFacing(HexPosition::Facing cf, HexPosition::Facing wf);
BATDATA_DLL HexPosition::Facing oppositeFace(HexPosition::Facing f);

/*
 * Speed
 *
 * Speed is stored as Quanta Per Tick
 * i.e. speed of 1 represents 1/4 inch per 10th of a second = 250 Yards per hour!
 *
 */

typedef UWORD Speed;


inline Speed mph(int m)
{
        ASSERT(m >= 0);

        int speed = miles(m) / hours(1);

        ASSERT(speed <= UWORD_MAX);

        return static_cast<Speed>(speed);
}

inline Speed yph(int y)
{
    ASSERT(y >= 0);

    int speed = yards(y) / hours(1);

    ASSERT(speed <= UWORD_MAX);

    return static_cast<Speed>(speed);
}

inline Speed ypm(int y)
{
    ASSERT(y >= 0);

    int speed = yards(y) / minutes(1);

    ASSERT(speed <= UWORD_MAX);

    return static_cast<Speed>(speed);
}

inline BattleTime::Tick timeToTravel(Distance d, Speed s)
{
    ASSERT(s != 0);
    if(s == 0)
        return ULONG_MAX;
    else
        return d / s;
}

inline Distance distanceTraveled(BattleTime::Tick t, Speed s)
{
    return t * s;
}

};      // namespace BattleMeasure


#if !defined(NOLOG)
class LogFile;
BATDATA_DLL LogFile& operator << (LogFile& file, const BattleMeasure::HexCord& hex);

template<class T>
BATDATA_DLL LogFile& operator << (LogFile& file, const Point<T>& p);
#endif

BATDATA_DLL std::ostream& operator << (std::ostream& file, const BattleMeasure::HexCord& hex);

/*
 * This must be defined outside the namespace or else the compiler
 * locks ups when in -d2 mode.
 */

typedef Point<BattleMeasure::Distance> BattleLocation;
typedef Rect<BattleMeasure::Distance> BattleArea;
typedef Rect<BattleMeasure::HexCord::Cord> BattleHexArea;


#endif /* BATCORD_HPP */




