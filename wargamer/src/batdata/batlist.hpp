/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATLIST_HPP
#define BATLIST_HPP

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 549 9;     // Disable sizeof(Type) contains compiler information warning
#endif

#include "bd_dll.h"
#include "sllist.hpp"
#include "bobdef.hpp"

class OrderBattle;
class FileReader;
class FileWriter;

/*-------------------------------------------------------------------
 * BattleList class, used for storing enemy units we are firing at.
 */

struct BattleItem : public SLink {
  static const UWORD s_fileVersion;

  enum Position {
      Front,
      RightFront,
      LeftFront,
      RightFlank,
      LeftFlank,
      Rear,
      RightRear,
      LeftRear,

     Position_HowMany
  };

  enum Flags {
      Added = 0x01
  };

  RefBattleCP d_cp;
  UWORD d_range;
  UWORD d_fireValue;
  UWORD d_lastFireValue;
  UWORD d_spsFiring;
  Position d_position;
  Position d_lastPosition;
  UBYTE d_flags;

  BattleItem() : 
  	d_cp(NoBattleCP), 
  	d_range(UWORD_MAX), 
  	d_fireValue(0), 
  	d_lastFireValue(0), 
  	d_spsFiring(0), 
  	d_position(Position_HowMany), 
	d_lastPosition(Position_HowMany),
  	d_flags(0) {}
  BattleItem(const RefBattleCP& cp, const UWORD& r, Position pos) :
		 d_cp(cp), d_range(r), d_fireValue(0), d_lastFireValue(0), d_spsFiring(0), d_position(pos), d_lastPosition(Position_HowMany), d_flags(0) {}

	BattleItem(const RefBattleCP& cp, const UWORD& r, const UWORD& fv, Position pos) :
     d_cp(cp), d_range(r), d_fireValue(fv), d_lastFireValue(0), d_spsFiring(0), d_position(pos), d_lastPosition(Position_HowMany), d_flags(0) {}

  void* operator new(size_t size);
#ifdef _MSC_VER   // Visual C++ doesn't like delete(void*, size_t)
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif

  BattleItem(const BattleItem& hi) :
    d_cp(hi.d_cp), d_range(hi.d_range), d_fireValue(hi.d_fireValue), d_lastFireValue(hi.d_lastFireValue), d_spsFiring(hi.d_spsFiring), d_position(hi.d_position), d_lastPosition(Position_HowMany), d_flags(hi.d_flags) {}

  bool added() const { return (d_flags & Added); }

	BATDATA_DLL static const TCHAR* positionText(BattleItem::Position pos);

	/*
	* File Interface
	*/

	BATDATA_DLL Boolean readData(FileReader& f, OrderBattle* ob);
	BATDATA_DLL Boolean writeData(FileWriter& f, OrderBattle* ob) const;
};

class BattleList : public SList<BattleItem> {
   static const UWORD s_fileVersion;
   UWORD d_closeRange;
   UWORD d_closeRangeFront;
   UWORD d_closeRangeRightFront;
   UWORD d_closeRangeLeftFront;
   UWORD d_closeRangeLeftFlank;
   UWORD d_closeRangeRightFlank;
   UWORD d_closeRangeRear;
   UWORD d_closeRangeRightRear;
   UWORD d_closeRangeLeftRear;
   UBYTE d_flags;
   enum Flags {
      Changed = 0x01
   };

   void setFlag(UBYTE mask, bool f)
   {
      if(f)
       d_flags |= mask;
      else
       d_flags &= ~mask;
   }
public:

   BattleList() :
      d_closeRange(UWORD_MAX),
      d_closeRangeFront(UWORD_MAX),
      d_closeRangeRightFront(UWORD_MAX),
      d_closeRangeLeftFront(UWORD_MAX),
      d_closeRangeLeftFlank(UWORD_MAX),
      d_closeRangeRightFlank(UWORD_MAX),
		d_closeRangeRear(UWORD_MAX),
      d_closeRangeRightRear(UWORD_MAX),
      d_closeRangeLeftRear(UWORD_MAX),
      d_flags(0) {}

   BATDATA_DLL BattleList& operator = (const BattleList& hl);
   BattleItem* newItem(const RefBattleCP& cp, const UWORD& r, BattleItem::Position pos);

   void reset() { d_closeRange = UWORD_MAX; SList<BattleItem>::reset(); }
   bool inList(const RefBattleCP& cp) { return (find(cp) != 0); }
   BATDATA_DLL bool addFireValue(const RefBattleCP& cp, const UWORD& r, const UWORD& fv, BattleItem::Position pos = BattleItem::Position_HowMany, bool countSP = False);
	 UWORD closeRange()           const { return d_closeRange; }
	 UWORD closeRangeFront()      const { return d_closeRangeFront; }
	 UWORD closeRangeRightFront() const { return d_closeRangeRightFront; }
	 UWORD closeRangeLeftFront()  const { return d_closeRangeLeftFront; }
	 UWORD closeRangeLeftFlank()  const { return d_closeRangeLeftFlank; }
	 UWORD closeRangeRightFlank() const { return d_closeRangeRightFlank; }
	 UWORD closeRangeRear()       const { return d_closeRangeRear; }
	 UWORD closeRangeRightRear()  const { return d_closeRangeRightRear; }
	 UWORD closeRangeLeftRear()   const { return d_closeRangeLeftRear; }
	 BATDATA_DLL void resetItems();
	 bool changed() const { return (d_flags & Changed); }
   // unchecked added by Paul
	 void clearChanged() { d_flags &= ~Changed; }
   BATDATA_DLL void moveUpCloseUnit();
   BATDATA_DLL void cleanUpList();
   BATDATA_DLL BattleItem* find(const RefBattleCP& cp);

   /*
    * File Interface
    */

	BATDATA_DLL Boolean readData(FileReader& f, OrderBattle* ob);
	BATDATA_DLL Boolean writeData(FileWriter& f, OrderBattle* ob) const;
};

class BattleListIter : public SListIter<BattleItem> {
public:
   BattleListIter(BattleList* list) :
      SListIter<BattleItem>(list) {}

   ~BattleListIter() {}
};

class BattleListIterR : public SListIterR<BattleItem> {
public:
   BattleListIterR(const BattleList* list) :
      SListIterR<BattleItem>(list) {}

   ~BattleListIterR() {}
};

//#pragma warning 549 5;      // Disable sizeof(Type) contains compiler information warning

#endif
