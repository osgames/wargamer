/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BSHOCKL_HPP
#define BSHOCKL_HPP

#include "sllist.hpp"
#include "batunit.hpp"
//#include "batdata.hpp"

class OrderBattle;
class FileWriter;
class FileReader;

/*-------------------------------------------------------------------
 * SideShockBattleList class, used for storing units involved in shock battle.
 */

struct SideShockBattleItem : public SLink {
  static const UWORD s_fileVersion;

  RefBattleCP d_cp;

  SideShockBattleItem() : d_cp(NoBattleCP) {}
  SideShockBattleItem(const RefBattleCP& cp) : d_cp(cp) {}

  void* operator new(size_t size);
#ifdef _MSC_VER
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif

  SideShockBattleItem(const SideShockBattleItem& hi) : d_cp(hi.d_cp) {}

  /*
   * File Interface
   */

  Boolean readData(FileReader& f, OrderBattle* ob);
  Boolean writeData(FileWriter& f, OrderBattle* ob) const;
};

class SideShockBattleList : public SList<SideShockBattleItem> {
   static const UWORD s_fileVersion;
public:
   SideShockBattleList() {}

   SideShockBattleList& operator = (const SideShockBattleList& hl);
   SideShockBattleItem* addItem(const RefBattleCP& cp);

   /*
    * File Interface
    */

   Boolean readData(FileReader& f, OrderBattle* ob);
   Boolean writeData(FileWriter& f, OrderBattle* ob) const;
};

class SideShockBattleListIter : public SListIter<SideShockBattleItem> {
public:
   SideShockBattleListIter(SideShockBattleList* list) :
      SListIter<SideShockBattleItem>(list) {}

   ~SideShockBattleListIter() {}
};

class SideShockBattleListIterR : public SListIterR<SideShockBattleItem> {
public:
   SideShockBattleListIterR(const SideShockBattleList* list) :
      SListIterR<SideShockBattleItem>(list) {}

   ~SideShockBattleListIterR() {}
};

/*-------------------------------------------------------------------
 * SideShockBattleList class, used for storing units involved in shock battle.
 */

struct ShockBattleItem : public SLink {
  static const UWORD s_fileVersion;

  enum { NumberSides = 2 };
  SideShockBattleList d_units[NumberSides];
  BattleMeasure::BattleTime::Tick d_nextRound;
  UWORD d_round;
  Side d_winnerLastRound;
  enum {
    BattleOver = 0x01
  };
  UBYTE d_flags;
  ShockBattleItem() : d_nextRound(0), d_flags(0), d_round(0), d_winnerLastRound(SIDE_Neutral) {}

  void* operator new(size_t size);
#ifdef _MSC_VER
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif

//  ShockBattleItem(const ShockBattleItem& hi) : d_cp(hi.d_cp) {}

  /*
   * File Interface
   */

  Boolean readData(FileReader& f, OrderBattle* ob);
  Boolean writeData(FileWriter& f, OrderBattle* ob) const;
};

class ShockBattleList : public SList<ShockBattleItem> {
   static const UWORD s_fileVersion;
public:
   ShockBattleList() {}

   /*
    * File Interface
    */

   Boolean readData(FileReader& f, OrderBattle* ob);
   Boolean writeData(FileWriter& f, OrderBattle* ob) const;
};
//#if 0

class ShockBattleListIter : public SListIter<ShockBattleItem> {
public:
   ShockBattleListIter(ShockBattleList* list) :
      SListIter<ShockBattleItem>(list) {}

   ~ShockBattleListIter() {}
};

class ShockBattleListIterR : public SListIterR<ShockBattleItem> {
public:
   ShockBattleListIterR(const ShockBattleList* list) :
      SListIterR<ShockBattleItem>(list) {}

   ~ShockBattleListIterR() {}
};
//#endif
//#pragma warning 549 5;      // Disable sizeof(Type) contains compiler information warning


#endif
/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
