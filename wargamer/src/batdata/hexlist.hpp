/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef HEXLIST_HPP
#define HEXLIST_HPP

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 549 9;     // Disable sizeof(Type) contains compiler information warning
#endif

#include "bd_dll.h"
#include "sllist.hpp"
#include "batcord.hpp"

class OrderBattle;
class FileReader;
class FileWriter;

/*-------------------------------------------------------------------
 * HexList class, used for storing route hexes, deployment hexes, etc.
 * i.e.  Battle Units and orders maintain a hex list of destination hexes
 *
 *   Each item has a
 *   1. hex
 *   2. facing data (i.e. a unit moves into this hex, facing this way
 */

struct HexItem : public SLink {
  static const UWORD s_fileVersion;

  BattleMeasure::HexCord d_hex;
  BattleMeasure::HexPosition::Facing d_facing;

  HexItem() : d_hex(), d_facing(BattleMeasure::HexPosition::Facing_Undefined) {}
  HexItem(const BattleMeasure::HexCord& hex) : d_hex(hex), d_facing(BattleMeasure::HexPosition::Facing_Undefined) {}
  HexItem(const BattleMeasure::HexCord& hex, BattleMeasure::HexPosition::Facing f) :
    d_hex(hex), d_facing(f) {}

  BATDATA_DLL void* operator new(size_t size);
#ifdef _MSC_VER      // Visual C++ complains when using delete(void*, size_t)
  BATDATA_DLL void operator delete(void* deadObject);
#else
  BATDATA_DLL void operator delete(void* deadObject, size_t size);
#endif

  HexItem(const HexItem& hi) :
    d_hex(hi.d_hex),
    d_facing(hi.d_facing) {}

  HexItem& operator = (const HexItem& hi)
  {
    d_hex = hi.d_hex;
    d_facing = hi.d_facing;
    return *this;
  }

  /*
   * File Interface
   */

  Boolean readData(FileReader& f, OrderBattle* ob);
  Boolean writeData(FileWriter& f, OrderBattle* ob) const;
};

class HexList : public SList<HexItem> {
   static const UWORD s_fileVersion;
public:
   HexList& operator = (const HexList& hl)
   {
     // reset list
     reset();
     addList(hl);
     return *this;
   }

   BATDATA_DLL void addList(const HexList& list);
   HexItem* newItem(const BattleMeasure::HexCord& hex, const BattleMeasure::HexPosition::Facing f)
   {
     HexItem* hi = new HexItem(hex, f);
     ASSERT(hi);

     append(hi);
     return hi;
   }

   HexItem* newItem(const BattleMeasure::HexCord& hex)
   {
     HexItem* hi = new HexItem(hex);
     ASSERT(hi);

     append(hi);
     return hi;
   }

   /*
    * File Interface
    */

   Boolean readData(FileReader& f, OrderBattle* ob);
   Boolean writeData(FileWriter& f, OrderBattle* ob) const;
};

//#pragma warning 549 5;      // Disable sizeof(Type) contains compiler information warning

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
