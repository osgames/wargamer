/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BTIMCTRL_HPP
#define BTIMCTRL_HPP

#ifndef __cplusplus
#error btimctrl.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Time Control
 * 
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "battime.hpp"
#include "systick.hpp"

// using BattleMeasure::BattleTime;

class BatTimeControl
{
	public:
		BATDATA_DLL BatTimeControl();
        BATDATA_DLL ~BatTimeControl();

		BATDATA_DLL BattleMeasure::BattleTime::Tick addTicks(SysTick::Value sysTicks);
		BATDATA_DLL void setRatio(int ratio);
		int ratio() const { return d_ratio; }

        bool isFrozen() const { return d_frozen; }
        void freeze(bool p) { d_frozen = p; }

        void setJump(BattleMeasure::BattleTime::Tick jump) { d_jumping = d_tick + jump; }
        bool isJumping() const { return d_jumping > d_tick; }

	private:
		SysTick::Value d_overflow;			// Overflow from conversion to BattleTick
		int d_ratio;						// Game Seconds per Real Second
        bool d_frozen;
        BattleMeasure::BattleTime::Tick d_jumping;
        BattleMeasure::BattleTime::Tick d_tick;
};

#endif /* BTIMCTRL_HPP */

