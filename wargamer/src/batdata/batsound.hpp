/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BATSOUND_HPP
#define BATSOUND_HPP

#include "bd_dll.h"
#include "soundsys.hpp"
#include "b_sounds.hpp"
#include "wavefile.hpp"
#include "batcord.hpp"
#include <windowsx.h>
#include <dsound.h>
#include <stack>
#include <vector>
#include <list>

// using BattleMeasure::HexCord;

#define MAX_SOUND_BUFFERS 4


enum BattleSoundZoomEnum {
   Zoom_LevelOne,
   Zoom_LevelTwo,
   Zoom_LevelThree,
   Zoom_LevelFour
};


/*
Enumeration for the different sound priorities
A sound with SOUND_INSTANT priority will not be sorted into the sound queue
but will be inserted automatically at the head
*/

enum BattleSoundPriorityEnum {
    BATTLESOUNDPRIORITY_UNIQUE = 0,
    BATTLESOUNDPRIORITY_LOW = 1000,
    BATTLESOUNDPRIORITY_MEDIUM = 2000,
    BATTLESOUNDPRIORITY_HIGH = 3000,
    BATTLESOUNDPRIORITY_INSTANT = 10000
};




typedef struct BattleSoundInfo {

    unsigned long base_frequency;
    int frequency_variation;
    int num_buffers;
    int buffers_used;
    LPDIRECTSOUNDBUFFER lpDSBuffer[MAX_SOUND_BUFFERS];

    BattleSoundInfo(void) {
        frequency_variation = 0;
        num_buffers = 0;
        buffers_used = 0;
        for(int f=0;f<MAX_SOUND_BUFFERS;f++) lpDSBuffer[f] = NULL;
    }

}BattleSoundInfo;


/*
SoundEvent structures are held in the waiting queue
*/

typedef struct BattleSoundEvent {
    // index into SoundsTable
    BattleSoundTypeEnum sound_type;
    // priority assigned to this sound
    BattleSoundPriorityEnum sound_priority;
    // distance from sound to view center
    int sound_distance;
    // actual hexpos of sound origin
    BattleMeasure::HexCord sound_origin;

    BattleSoundEvent(void) { }

    BattleSoundEvent(BattleSoundTypeEnum type, BattleSoundPriorityEnum priority, int distance, BattleMeasure::HexCord origin) {
        sound_type = type;
        sound_priority = priority;
        sound_distance = distance;
        sound_origin = origin;
    }

    bool operator < (const BattleSoundEvent& i) const { return (sound_priority+sound_distance) < (i.sound_priority+i.sound_distance); }
    bool operator > (const BattleSoundEvent& i) const { return (sound_priority+sound_distance) > (i.sound_priority+i.sound_distance); }
    bool operator == (const BattleSoundEvent& i) const { return (sound_priority == i.sound_priority) && (sound_distance == i.sound_distance) && (sound_origin == i.sound_origin) && (sound_type == i.sound_type); }

}BattleSoundEvent;



/*
PlayingSound structures are held in the playing list
*/

typedef struct BattlePlayingSound {
    BattleMeasure::HexCord sound_origin;
    BattleSoundTypeEnum sound_type;
    BattleSoundPriorityEnum sound_priority;
    LPDIRECTSOUNDBUFFER lpDSbuffer;

    bool operator < (const BattlePlayingSound & i) const { FORCEASSERT("This shouldn't happen"); return FALSE; }
    bool operator > (const BattlePlayingSound & i) const { FORCEASSERT("This shouldn't happen"); return FALSE; }
    bool operator == (const BattlePlayingSound & i) const { return (sound_origin == i.sound_origin) && (sound_type == i.sound_type) && (sound_priority == i.sound_priority); }

}BattlePlayingSound;
    

/*
The main sound system class, using DirectSound API

Consists of a list of currently playing sounds & a queue of sounds waiting to be played
Entries in the queue are sorted primarily by PRIORITY, and secondarily by DISTANCE from listener

Each pass the list is cleared of completed sounds, and the queue is processed for sounds to be played
If a high priority sound is waiting in queue, and there are no list places available for playing, the sound with
the priority lowest then the waiting sound, is deleted, and the waiting sound is played
*/

class BattleSoundSystem {
   /*
    * Functions
    */

   public:
       BattleSoundSystem(SoundSystem * sound_sys);
       ~BattleSoundSystem(void);

       BATDATA_DLL void processSound(BattleMeasure::HexCord pos, BattleSoundZoomEnum zoom);
       BATDATA_DLL bool triggerSound(BattleSoundTypeEnum type, BattleSoundPriorityEnum priority, BattleMeasure::HexCord origin);
       BATDATA_DLL bool triggerSound(BattleSoundTypeEnum type, BattleSoundPriorityEnum priority);
       BATDATA_DLL bool triggerSound(BattleSoundTypeEnum type);
    
   private:
       void directSoundError(HRESULT err);
       bool initialiseBuffers();
       bool createPrimaryBuffer();
       bool destroyPrimaryBuffer();
       bool destroyBuffers();
       bool restoreBuffers();
       bool setSpatialProperties(BattlePlayingSound * snd);
       bool createBuffers(char * filename, BattleSoundInfo & snd_inf);
       bool copySoundToBuffer(WAVEFILE WaveFile, LPVOID lpWaveData, LPDIRECTSOUNDBUFFER lpDSbuffer);
       bool processWaitingQueue();
       bool processPlayingList();

       // random number generator for frequency variations
//       inline static int random(int range) 
//       {
//         if(range<=0) 
//            return 0; 
//         int mod=(RAND_MAX+1) / range; 
//         return(rand() / mod); 
//       }

   /*
    * Variables
    */

   private:
      SoundSystem * d_soundSys;
      bool d_setupOK;
      // pointer to main Soundsystem class (to get hwnd for reporting errors)
      HWND d_appHwnd;
      // the main direct sound object
      LPDIRECTSOUND d_lpDirectSound;
      // the primary sound buffer
      LPDIRECTSOUNDBUFFER d_lpPrimaryBuffer;
      // WaveFile mixing format
      LPDIRECTSOUNDBUFFER d_lpFormatBuffer;
      // array of sound information (indexed by enumeration)
      std::vector <BattleSoundInfo> d_soundsArray;
      // queue of sounds waiting to be played
      std::deque <BattleSoundEvent> d_waitingQueue;
      int d_waitingQueueMaxSize;
      // array of sounds currently playing in system
      std::vector<BattlePlayingSound> d_playingList;
      int d_playingListMaxSize;

      int d_maxAudibleDistance[4];
      int d_squaredMaxAudibleDistance[4];
      int d_panRatio[4];
      int d_volumeRatio[4];
      int d_verticalVolumeModifier[4];

      BattleMeasure::HexCord d_listenerOrigin;
      BattleSoundZoomEnum d_listenerZoom;

      bool d_inUse;
};




#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
