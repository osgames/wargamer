/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BUILDING_HPP
#define BUILDING_HPP

#ifndef __cplusplus
#error building.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Buildings and bridges
 *
 *----------------------------------------------------------------------
 */

#include <map>                        // stl map
#include <vector>             // stl vector
#include "bd_dll.h"
#include "batcord.hpp"
#include "sprlib.hpp"
#include "random.hpp"


namespace BattleBuildings
{


typedef UWORD BuildingType;
//static const BuildingType NoBuilding = static_cast<BuildingType>(-1);


struct EffectDisplayInfo {

        SWG_Sprite::SpriteLibrary::Index index;
        int frame_time;

        EffectDisplayInfo();
        // constructor specifiec the sprite index & the frame display time
        EffectDisplayInfo(SWG_Sprite::SpriteLibrary::Index i, int time) :
                index(i),
                frame_time(time) { }
};


struct EffectInfo {

        public:
           typedef std::vector<EffectDisplayInfo> DisplayInfo;

        private:
                DisplayInfo d_displayInfo;
                // whether animation will loop at end
                int loop_status;

        public:

                EffectInfo() { }
                void push_back(const EffectDisplayInfo& item) { d_displayInfo.push_back(item); }
                const DisplayInfo& displayInfo() const { return d_displayInfo; }
};





enum BuildingEffectsEnum {
    NONE = 0,
    BUILDING_SMOKE = 1,
    BUILDING_FIRE = 2
};


/*
Similar to the building table, this holds a vector of fx animations
*/

class EffectsTable {

   std::vector<EffectInfo> d_info;

    public:
        void init();
        int size() const { return d_info.size(); }
        const EffectInfo & info(BuildingEffectsEnum e) const { return d_info[e]; }

};




/*
Destruction status of a building
*/

enum BuildingStatus {

    BUILDING_NORMAL,
    BUILDING_BURNING,
    BUILDING_SMOULDERING,
    BUILDING_DESTROYED

};



#define BUILDING_BURNING_INTERVAL		72000
#define BUILDING_SMOULDERING_INTERVAL	108000



/*
 * Information about one part of a building display
 */

// to initially convert from our arbitrary coordinate system to in-game coords
// const int building_xcoord_max = 86;
// const int building_ycoord_max = 62;
// const int building_xcoord_centre = building_xcoord_max/2;
// const int building_ycoord_centre = building_ycoord_max/2;

class BuildingDisplayInfo
{
    public:
        typedef BattleLocation Position;

        /*
         * Functions
         */

        BuildingDisplayInfo() :
                                d_index_normal(SWG_Sprite::SpriteLibrary::NoGraphic),
                                d_index_destroyed(SWG_Sprite::SpriteLibrary::NoGraphic),
                                d_location()
                  {
                  }
#if 0
        // constructor specifying only normal sprite sets destroyed as the same
        BuildingDisplayInfo(SWG_Sprite::SpriteLibrary::Index i_normal, const BuildingDisplayInfo::Position& p) :
                                d_index_normal(i_normal),
                                d_index_destroyed(i_normal),
                                d_location(p),
                                status(BUILDING_NORMAL),
                                status_timer(0),
                                effect(NONE),
                                frame_no(0),
                                frame_timer(0)
        {
        }
#endif

        // constructor specifying normal & destroyed sprites
        BuildingDisplayInfo(SWG_Sprite::SpriteLibrary::Index i_normal, SWG_Sprite::SpriteLibrary::Index i_destroyed, const Position& p) :
                                d_index_normal(i_normal),
                                d_index_destroyed(i_destroyed),
                                d_location(p)
        {
        }

        const Position& position() const
        {
            return d_location;
        };

        SWG_Sprite::SpriteLibrary::Index normalIndex() const { return d_index_normal; }
        SWG_Sprite::SpriteLibrary::Index destroyedIndex() const { return d_index_destroyed; }


        /*
         * File Interface
         */

        Boolean readData(FileReader& f);
        Boolean writeData(FileWriter& f) const;

    private:

        SWG_Sprite::SpriteLibrary::Index d_index_normal;
        SWG_Sprite::SpriteLibrary::Index d_index_destroyed;
        BuildingDisplayInfo::Position d_location;


        static const UWORD s_fileVersion;
};


/*
This enumeration holds which category the BuildingDisplayInfo strucutres above belong to
The numbers are indexes into the BuildTable below
Generally each category has 8 entries, varying in size from SMALL to LARGE
*/



enum BuildingClusterType {

    TOWN_SMALL = 0,
    TOWN_LARGE = 15,
    TOWN_SPECIAL_SMALL = 16,
    TOWN_SPECIAL_LARGE = 31,

    VILLAGE_SMALL = 32,
    VILLAGE_LARGE = 47,
    VILLAGE_SPECIAL_SMALL = 48,
    VILLAGE_SPECIAL_LARGE = 63,

    FARM_SMALL = 64,
    FARM_LARGE = 79,
    FARM_SPECIAL_SMALL = 80,
    FARM_SPECIAL_LARGE = 95,

    // woods are considered as buildings

    LIGHT_WOOD_SMALL = 96,
    LIGHT_WOOD_LARGE = 111,

    DENSE_WOOD_SMALL = 112,
    DENSE_WOOD_LARGE = 127,

    TRACK_BRIDGE = 128,
    ROAD_BRIDGE = 137,

	TOWN_ROADSIDE_BANK1 = 146,
	TOWN_ROADSIDE_BANK2 = 155,

	VILLAGE_ROADSIDE_BANK1 = 164,
	VILLAGE_ROADSIDE_BANK2 = 173

};




/*
	Information about a building cluster
	Essentially just a vector of BuildingDisplayInfo
*/

class BuildingInfo
{


                // Info for display

        public:
           typedef std::vector<BuildingDisplayInfo> DisplayInfo;



        private:
                DisplayInfo             d_displayInfo;
        public:

                BuildingInfo() { }

                void push_back(const BuildingDisplayInfo& item) { d_displayInfo.push_back(item); }
                const DisplayInfo& displayInfo() const { return d_displayInfo; }
					 DisplayInfo& displayInfo() { return d_displayInfo; }

					 /*
					  * File Interface
					  */

					 Boolean readData(FileReader& f);
					 Boolean writeData(FileWriter& f) const;
					 static const UWORD s_fileVersion;
};







inline bool
operator < (const BuildingInfo& l, const BuildingInfo& r) {
	return true;
}

inline bool
operator == (const BuildingInfo& l, const BuildingInfo& r) {
	return false;
}










#define NoBuilding -1





class BuildingTable {

   std::vector<BuildingInfo> d_info;

public:

	BuildingTable() { }
	~BuildingTable();
	void init();
	int size() const { return d_info.size(); }
	const BuildingInfo * info(BuildingType t) const { return &(d_info[t]); }
	BuildingInfo * info(BuildingType t) { return &(d_info[t]); }

private:

	static const char s_BuildingSpriteFileName[];
	typedef int Index;
};


template <class T1, class T2>
inline std::pair<const T1, T2> make_pair_const(const T1& x, const T2& y)
{
	 return pair<const T1, T2>(x, y);
}



#define MAX_BUILDINGS_PER_CLUSTER 10


struct BuildingListEntry {

	// Info required for game logic, e.g. occupied, etc

private:
	int d_Index;
	BuildingStatus d_Status;
	char d_Frame[MAX_BUILDINGS_PER_CLUSTER];
	char d_Timer[MAX_BUILDINGS_PER_CLUSTER];
	int d_EffectCount;
	unsigned int d_lastTime;

public:

	BATDATA_DLL BuildingListEntry(void);

	const int index(void) const { return d_Index; }
	void index(int i) { d_Index = i; }

	const BuildingStatus status(void) const { return d_Status; }
	void status(BuildingStatus s) { d_Status = s; }

	const int frame(int n) const { return d_Frame[n]; }
	void frame(int n, int f) { d_Frame[n] = f; }

	const int timer(int n) const { return d_Timer[n]; }
	void timer(int n, int t) { d_Timer[n] = t; }

	// this is essentially so we can progress the status from burning, through smoking, to destroyed
	const int effectCount(void) const { return d_EffectCount; }
	void effectCount(int fx) { d_EffectCount = fx; }

	const unsigned int lastTime(void) const { return d_lastTime; }
	void lastTime(unsigned int t) { d_lastTime = t; }

	 /*
	  * File Interface
	  */

	 Boolean readData(FileReader& f);
	 Boolean writeData(FileWriter& f) const;
	 static const UWORD s_fileVersion;
};

inline bool
operator < (const BuildingListEntry& l, const BuildingListEntry& r) {
	return true;
}

inline bool
operator == (const BuildingListEntry& l, const BuildingListEntry& r) {
	return false;
}


class BuildingList {

   typedef std::map<BattleMeasure::HexCord, BuildingListEntry, BattleMeasure::HexCordCompare> BuildingListType;

	BuildingListType d_buildings;

public:

	BuildingList() { }

	void add(const BattleMeasure::HexCord& h, const BuildingListEntry entry) {
		d_buildings.insert(BuildingListType::value_type(h,entry));
	}

	const BuildingListEntry * get(const BattleMeasure::HexCord& h) const {

		BuildingListType::const_iterator i = d_buildings.find(h);
		if(i == d_buildings.end()) return NULL;
		const BuildingListEntry & entry = (*i).second;
		return &entry;
		//else return (*i).second;
	}

	BuildingListEntry * get(const BattleMeasure::HexCord& h) {

		BuildingListType::const_iterator i = d_buildings.find(h);
		if(i == d_buildings.end()) return NULL;
		const BuildingListEntry & entry = (*i).second;
		return const_cast<BuildingListEntry *>(&entry);
		//else return (*i).second;
	}

	void remove(const BattleMeasure::HexCord& h)  {
		d_buildings.erase(h);
	}


	// clears the buildings list
	void clear(void) {
		d_buildings.erase(d_buildings.begin(), d_buildings.end() );
	}

	// crop buildings list to given area
	void crop(BattleMeasure::HexCord & topleft, BattleMeasure::HexCord & bottomright) {

		BuildingListType::const_iterator i = d_buildings.begin();
		while(i != d_buildings.end() ) {
			BattleMeasure::HexCord hex = (*i).first;
			// remove if outside area
			if(hex.x() < topleft.x() || hex.y() < topleft.y() || hex.x() > bottomright.x() || hex.y() > bottomright.y() ) {
				remove(hex);
				i--;
			}
			i++;
		}
	}


    void adjust(BattleMeasure::HexCord & offset) {

        BuildingListType::const_iterator i = d_buildings.begin();
        while(i != d_buildings.end() ) {
            BattleMeasure::HexCord * hex = const_cast < BattleMeasure::HexCord * > (& (*i).first);
            // remove if outside area
            hex->x( hex->x() - offset.x() );
            hex->y( hex->y() - offset.y() );
            i++;
        }
    }

	/*
	* Go through buildings list & increase any effects counters
	*/
	void process(BattleMeasure::BattleTime::Tick ticks) {

		BuildingListType::const_iterator i = d_buildings.begin();
		while(i != d_buildings.end() ) {

			BuildingListEntry * entry = const_cast<BuildingListEntry *>(&(*i).second);

			if(entry->status() == BUILDING_BURNING) {

				entry->effectCount( entry->effectCount() + ticks );

				if(entry->effectCount() > BUILDING_BURNING_INTERVAL) {
					entry->effectCount(random(0,BUILDING_SMOULDERING_INTERVAL/2));
					entry->status(BUILDING_SMOULDERING);
				}
			}

			else if(entry->status() == BUILDING_SMOULDERING) {

				entry->effectCount( entry->effectCount() + ticks );

				if(entry->effectCount() > BUILDING_SMOULDERING_INTERVAL) {
					entry->effectCount(0);
					entry->status(BUILDING_DESTROYED);
				}
			}

			i++;
		}
	}


	/*
     * File Interface
     */

    BATDATA_DLL Boolean readData(FileReader& f, BuildingTable * buildtable);
    BATDATA_DLL Boolean writeData(FileWriter& f) const;

    static const UWORD s_fileVersion;
    static const char s_fileID[];

};









};      // namespace BattleBuildings

#endif /* BUILDING_HPP */

