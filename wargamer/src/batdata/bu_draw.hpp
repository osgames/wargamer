/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BU_DRAW_HPP
#define BU_DRAW_HPP

#ifndef __cplusplus
#error bu_draw.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * virtual class to allow a higher level module to draw BattleUnits
 *
 *----------------------------------------------------------------------
 */

class UnitDrawData;
class BattleSP;
class BattleCP;
class BattleUnit;

/*
 * virtual class to allow a higher level module to draw BattleUnits
 * The only other ways to do this are:
 *		- virtual draw() function, which means that draw functions are
 *		  part of batData instead of batdisp.
 *		- using typeid or dynamic_cast, which is messy.
 *		- using seperate multimaps for different types of objects
 *		- Having loads of virtual functions, e.g. getFormation(), getFacing(), getFlag(), etc...
 */


class UnitDrawer
{
	public:
		virtual ~UnitDrawer() = 0;

		virtual void draw(const BattleSP* sp) = 0;
		virtual void draw(const BattleCP* cp) = 0;
		virtual void draw(const BattleUnit* unit) = 0;		// unknown object
};

inline UnitDrawer::~UnitDrawer() { }


#endif /* BU_DRAW_HPP */

