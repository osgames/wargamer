/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BOB_VIS_H
#define BOB_VIS_H

#ifndef __cplusplus
#error bob_vis.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Is SP visible?
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "bobdef.hpp"

class BattleOB;

namespace BOB_Utility
{

bool isSPVisible(const BattleOB* ob, ParamCRefBattleSP sp);

};  // namespace BOB_Utility


#endif /* BOB_VIS_H */

