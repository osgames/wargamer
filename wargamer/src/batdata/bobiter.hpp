/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BOBITER_HPP
#define BOBITER_HPP

#ifndef __cplusplus
#error bobiter.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle OB Iterators
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "batarmy.hpp"

class UnitValidator
{
  public:
    enum ValidMode
    {
      OK,          // Unit is valid
      Invalid,     // Unit is invalid, but children maybe OK
      OrgInvalid   // Unit and children are invalid
    };

    // virtual ValidMode isValid(CRefBattleCP cp) const = 0;
};

class BUIV_All : public UnitValidator
{
  public:
    ValidMode isValid(CRefBattleCP cp) const { return OK; }
};

class BUIV_OnlyActive : public UnitValidator
{
  public:
    BATDATA_DLL ValidMode isValid(CRefBattleCP cp) const;
};

/*
 * Typical use is:
 *              for(BattleUnitIter iter = cp; !iter.finished(); iter.next())
 *              {
 *                      doSomethingWith(iter.cp());
 *              }
 *
 * Note that this differs from the way that the campaign iterators work
 * where the finished() and next() functions are both built into next()
 * and current values are not valid until next has been called at least once.
 */

class BATDATA_DLL BaseBattleUnitIter : public UnitValidator
{
    // Allow Copying!
  public:

    BaseBattleUnitIter(const BaseBattleUnitIter& iter);

    BaseBattleUnitIter& operator=(const BaseBattleUnitIter& iter);

    BaseBattleUnitIter(BattleOB* ob);
    // Iterate ALL units

    BaseBattleUnitIter(BattleOB* ob, Side side);
    // Iterate all units in a side
    BaseBattleUnitIter(ParamRefBattleCP cp);
    // iterate unit and its children

    void setTopDown();
    // iterate top down (default)
    void setBottomUp();
    // return low level units first

    void sister();
    // advance to next unit, ignoring current unit's children
    void next();
    // advance to next unit, going into current unit's children if they exist

    bool isFinished() const; 
    // report whether iterator has finished

    /*
     * Get current values
     */

    ReturnRefBattleCP cp() const;
    // Get current unit

    BattleOB* ob() const { return d_ob; }
    // Get OB being iterated

  private:
    void finish();
    // Set up for finished
    void gotoBottom();
    // Set current unit as lowest child of current unit
    void dealWithTop();
    // Advance to next side or stop
    void moveUp();
    // Move up to a parent
    void gotoNext();
    void gotoSister();
    void getValidNext();

    virtual ValidMode isValid(CRefBattleCP cp) const = 0;

  private:
    BattleOB*       d_ob;           // Order of Battle being operated on
    Side            d_side;         // Current Side if iterating all units
    // SIDE_Neutral if not iterating sides
    RefBattleCP     d_cp;           // Current unit being iterated
    RefBattleCP     d_top;          // Top unit
    bool            d_finished;     // set when iterator has completed
    bool            d_topDown;      // set if top down, false if bottom up

    // const UnitValidator& d_valid;
    mutable bool d_initialised;
};

class BaseConstBattleUnitIter : private BaseBattleUnitIter
{
    typedef BaseBattleUnitIter Super;

  public:
//    using UnitValidator::ValidMode;
    typedef UnitValidator::ValidMode ValidMode;

    BaseConstBattleUnitIter(const BaseConstBattleUnitIter& iter) :
      Super(iter)
    {
    }

    BaseConstBattleUnitIter& operator=(const BaseConstBattleUnitIter& iter)
    {
      if(&iter != this)
      {
        Super::operator=(iter);
      }
      return *this;
    }

    BaseConstBattleUnitIter(const BattleOB* ob) :
      Super(const_cast<BattleOB*>(ob))
    {
    }

    BaseConstBattleUnitIter(const BattleOB* ob, Side side) :
      Super(const_cast<BattleOB*>(ob), side)
    {
    }

    BaseConstBattleUnitIter(ParamCRefBattleCP cp) :
      Super(const_cast<ParamRefBattleCP>(cp))
    {
    }

    void setTopDown() { Super::setTopDown(); }
    void setBottomUp() { Super::setBottomUp(); }
    void sister() { Super::sister(); }
    void next() { Super::next(); }
    bool isFinished() const { return Super::isFinished(); }
    ReturnCRefBattleCP cp() const { return Super::cp(); }
    const BattleOB* ob() const { return Super::ob(); }
};


template<class V>
class TBattleUnitIter : public BaseBattleUnitIter
{
    // Copying not written yet
    TBattleUnitIter(const TBattleUnitIter<V>&);
    TBattleUnitIter& operator=(const TBattleUnitIter<V>&);

  public:
    TBattleUnitIter(BattleOB* ob, const V& v = V()) :
      d_valid(v),
      BaseBattleUnitIter(ob)
    {
    }

    TBattleUnitIter(BattleOB* ob, Side side, const V& v = V()) :
      d_valid(v),
      BaseBattleUnitIter(ob, side)
    {
    }

    TBattleUnitIter(ParamRefBattleCP cp, const V& v = V()) :
      d_valid(v),
      BaseBattleUnitIter(cp)
    {
    }

  private:
    virtual ValidMode isValid(CRefBattleCP cp) const
    {
      return d_valid.isValid(cp);
    }

    V d_valid;
};

template<class V>
class TConstBattleUnitIter : public BaseConstBattleUnitIter
{
    typedef BaseConstBattleUnitIter Super;
  public:
    TConstBattleUnitIter(const TConstBattleUnitIter<V>& iter) :
      Super(iter),
      d_valid(iter.d_valid)
    {
    }

    TConstBattleUnitIter& operator=(const TConstBattleUnitIter<V>& iter)
    {
      if(&iter != this)
      {
        Super::operator=(iter);
        d_valid = iter.d_valid;
      }
      return *this;
    }


    TConstBattleUnitIter(const BattleOB* ob, const V& v = V()) :
      d_valid(v),
      Super(ob)
    {
    }

    TConstBattleUnitIter(const BattleOB* ob, Side side, const V& v = V()) :
      d_valid(v),
      Super(ob, side)
    {
    }

    TConstBattleUnitIter(ParamCRefBattleCP cp, const V& v = V()) :
      d_valid(v),
      Super(cp)
    {
    }

  private:
    virtual ValidMode isValid(CRefBattleCP cp) const
    {
      return d_valid.isValid(cp);
    }

    V d_valid;
};


class BattleUnitIter : public TBattleUnitIter<BUIV_OnlyActive>
{
    typedef TBattleUnitIter<BUIV_OnlyActive> Super;
  public:
    BattleUnitIter(BattleOB* ob) :
      Super(ob, BUIV_OnlyActive())
    {
    }

    BattleUnitIter(BattleOB* ob, Side side) :
      Super(ob, side, BUIV_OnlyActive())
    {
    }

    BattleUnitIter(ParamRefBattleCP cp) :
      Super(cp, BUIV_OnlyActive())
    {
    }
};

class ConstBattleUnitIter : public TConstBattleUnitIter<BUIV_OnlyActive>
{
    typedef TConstBattleUnitIter<BUIV_OnlyActive> Super;
  public:
    ConstBattleUnitIter(const ConstBattleUnitIter& iter) :
      Super(iter)
    {
    }

    ConstBattleUnitIter& operator=(const ConstBattleUnitIter& iter)
    {
      if(&iter != this)
      {
        Super::operator=(iter);
      }
      return *this;
    }


    ConstBattleUnitIter(const BattleOB* ob) :
      Super(ob, BUIV_OnlyActive())
    {
    }

    ConstBattleUnitIter(const BattleOB* ob, Side side) :
      Super(ob, side, BUIV_OnlyActive())
    {
    }

    ConstBattleUnitIter(ParamCRefBattleCP cp) :
      Super(cp, BUIV_OnlyActive())
    {
    }
};




/*
 * Iterate through a chain of SPs
 *
 * The Modes used really would be better implemented as subclasses
 * so that this iterator is kept sweet and simple.
 * Currently it is too complicated and the mode names are misleading
 * in particular the default mode is currently InfCavOnly, when it
 * would be more obvious for the default to be All.  
 * InfCavOnly is complex because it really means:
 *   If Unit contains any Infantry and Cavalry then ignore artillery.
 *   but if unit contains only artillery then return it.
 */

class BattleOB;

class SP_Validator
{
  public:
    virtual bool isValid(CRefBattleSP sp) const = 0;
};

/*
 * Validator to allow all units through
 */

struct SPValidateAll : public SP_Validator
{
    virtual bool isValid(CRefBattleSP sp) const { return true; }
};

template<class V>
class TBattleSPIter
{
    // Disable Copying of iterator

    TBattleSPIter(const TBattleSPIter<V>&);
    TBattleSPIter& operator = (const TBattleSPIter<V>&);
  public:

    TBattleSPIter(const BattleOB* ob, ParamRefBattleCP cp, const V& v = V()) :
      d_ob(ob),
      d_sp(cp->sp()),
      d_valid(v),
      d_initialised(false)
    {
      ASSERT(ob);
      ASSERT(cp != NoBattleCP);
    }

    ReturnRefBattleSP sp()
    {
      ASSERT(!isFinished());
      return d_sp;
    }

    bool isFinished() const
    {
      if(!d_initialised)
      {
        d_initialised = true;
        if(!isValidSP())
        {
          TBattleSPIter<V>* ncThis = const_cast<TBattleSPIter<V>*>(this);
          ncThis->next();
        }
      }

      return d_sp == NoBattleSP;
    }

    void next()
    {
      ASSERT(!isFinished());

      if(d_sp != NoBattleSP)
      {
        do
        {
          ASSERT(d_sp != NoBattleSP);

          d_sp = d_sp->sister();
        } while(!isValidSP());
      }
    }

    // void bodgedNext(void);
    // void getNext();
  private:
    bool isValidSP() const 
    {
      if(d_sp == NoBattleSP)
        return true;
      else
        return d_valid.isValid(d_sp); 
    }
    // is current SP valid according to mode.

  private:
    const BattleOB* d_ob;
    RefBattleSP     d_sp;           // Current SP
    mutable bool d_initialised;
    V d_valid;
};


template<class V>
class TConstBattleSPIter : private TBattleSPIter<V>
{
    typedef TBattleSPIter<V> Super;
  public:
    TConstBattleSPIter(const BattleOB* ob, ParamCRefBattleCP cp, const V& v) :
      Super(ob, const_cast<ParamRefBattleCP>(cp), v)
    {
    }

    ReturnCRefBattleSP sp() { return Super::sp(); }
                
    bool isFinished() const { return Super::isFinished(); }
    void next() { Super::next(); }
};

/*
 * Meanings of Mode:
 *            All : Return ALL units
 *         InfCav : If unit contains Inf/Cav then do NOT include Artillery
 *      Artillery : Only return Artillery
 */

struct BSPI_ModeDefs
{
    enum Mode {
      All,
      InfCavOnly,
      ArtilleryOnly,
      // default used to be Inf & Cavalry
      Default = InfCavOnly,

      OnDisplay = InfCavOnly
    };
};

class BSPI_Default : public SP_Validator, public BSPI_ModeDefs
{
  public:
    BSPI_Default() : d_ob(0), d_mode(Default) { }
    BSPI_Default(const BattleOB* ob) : d_ob(ob), d_mode(Default) { }
    BSPI_Default(const BattleOB* ob, Mode m) : d_ob(ob), d_mode(m) { }

    BATDATA_DLL virtual bool isValid(CRefBattleSP sp) const;
  private:
    Mode d_mode;
    const BattleOB* d_ob;
};

class BattleSPIter : public BSPI_ModeDefs, public TBattleSPIter<BSPI_Default>
{
    typedef TBattleSPIter<BSPI_Default> Super;
    typedef BSPI_Default Valid;
  public:
    BattleSPIter(const BattleOB* ob, ParamRefBattleCP cp, Mode mode) :
      Super(ob, cp, Valid(ob, mode))
    {
    }

    BattleSPIter(const BattleOB* ob, ParamRefBattleCP cp) :
      Super(ob, cp, Valid(ob))
    {
    }

    ReturnRefBattleSP sp() { return Super::sp(); }
    bool isFinished() const { return Super::isFinished(); }
    void next() { Super::next(); }

};

class ConstBattleSPIter : public BSPI_ModeDefs, public TConstBattleSPIter<BSPI_Default>
{
    typedef TConstBattleSPIter<BSPI_Default> Super;
    typedef BSPI_Default Valid;
  public:
    ConstBattleSPIter(const BattleOB* ob, ParamCRefBattleCP cp, Mode mode) :
      Super(ob, cp, Valid(ob, mode))
    {
    }

    ConstBattleSPIter(const BattleOB* ob, ParamCRefBattleCP cp) :
      Super(ob, cp, Valid(ob))
    {
    }

    ReturnCRefBattleSP sp() { return Super::sp(); }
    bool isFinished() const { return Super::isFinished(); }
    void next() { Super::next(); }

};

/*=====================================================================
 * Iterates through sides
 * Just a wrapper for OBSideIterso that it can be passed an army
 */

class BattleSideIter : public OBSideIter
{
  public:
    BattleSideIter(const BattleOB* army) :
      OBSideIter(army->ob())
    {
      ASSERT(army != 0);
    }
};

#endif /* BOBITER_HPP */

/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */
