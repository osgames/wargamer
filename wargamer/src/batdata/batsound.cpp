/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *--------------------------------------------Sound--------------------------------*/
/*

######################################

SOUNDS.CPP

Sound processing for the battle game.

######################################

When a sound is triggered it is added to a queue which is processed each frame
Sounds are sorted primarily by PRIORITY & secondarily by DISTANCE
All availiable sounds within the battle game are held in an enumeration
Sound playback is currently done using the DirectSound2 interface

*/

#include "stdinc.hpp"
#include "batsound.hpp"
#include "b_sounds.hpp"
#include "sqrt.hpp"
#include "resstr.hpp"


#if defined(DEBUG) && !defined(NOLOG)
#include "clog.hpp"
LogFileFlush BatSoundLog("BatSound.log");
#endif


/*
Constructor to initialise the sound system
INIT_OK is set if DirectSound is setup successfully
and if sound buffers are loaded OK
*/

BattleSoundSystem::BattleSoundSystem(SoundSystem * soundSys) :
   d_soundSys(soundSys),
   d_setupOK(false),
   d_appHwnd(0),
   d_lpDirectSound(0),
   d_lpPrimaryBuffer(0),
   d_lpFormatBuffer(0),
   d_soundsArray(),
   d_waitingQueue(),
   d_waitingQueueMaxSize(8),
   d_playingList(),
   d_playingListMaxSize(16),
   d_maxAudibleDistance(),
   d_squaredMaxAudibleDistance(),
   d_panRatio(),
   d_volumeRatio(),
   d_verticalVolumeModifier(),
   d_listenerOrigin(),
   d_listenerZoom(Zoom_LevelOne),
   d_inUse(false)
{

   if(!soundSys->IsSetupOK()) 
   { 
      return; 
   }

   d_lpDirectSound = soundSys->GetDirectSound();
   d_appHwnd = soundSys->GetHWND();

   if(!createPrimaryBuffer()) 
   { 
      return; 
   }

   initialiseBuffers();

   d_maxAudibleDistance[0] = 32;//8; // half of 1 mile (8 hexes)
   d_maxAudibleDistance[1] = 32;//16; // half of 2 miles (16 hexes)
   d_maxAudibleDistance[2] = 32; // half of 4 miles (32 hexes)
   d_maxAudibleDistance[3] = 128; // half of maximum battle-size (128 hexes)

   for(int i=0; i<4; i++) 
   {
      d_squaredMaxAudibleDistance[i] = d_maxAudibleDistance[i] * d_maxAudibleDistance[i];
      d_panRatio[i] = (DSBPAN_RIGHT/2) / d_maxAudibleDistance[i];
      d_volumeRatio[i] = abs(DSBVOLUME_MIN) / d_squaredMaxAudibleDistance[i];
   }

   int modifier = abs(DSBVOLUME_MIN) / 16;

   d_verticalVolumeModifier[0] = 0;
   d_verticalVolumeModifier[1] = modifier;
   d_verticalVolumeModifier[2] = modifier*2;
   d_verticalVolumeModifier[3] = modifier*4;

   d_setupOK = true;
}
   



BattleSoundSystem::~BattleSoundSystem(void) 
{
   destroyBuffers();
   destroyPrimaryBuffer();
}



bool BattleSoundSystem::createPrimaryBuffer(void) 
{
/*
    HRESULT dsrval;
    DSBUFFERDESC dsbd;

    // specify primary buffer
    memset(&dsbd, 0, sizeof(DSBUFFERDESC) );
    dsbd.dwSize = sizeof(DSBUFFERDESC);
    dsbd.dwFlags = DSBCAPS_PRIMARYBUFFER;
    dsbd.dwBufferBytes = 0;
    dsbd.lpwfxFormat = NULL;

    // create it
    dsrval = d_lpDirectSound->CreateSoundBuffer(&dsbd, &d_lpPrimaryBuffer, NULL);
    if(dsrval != DS_OK) { DirectSoundError(dsrval); return false; }

    // fill out caps
    DSBCAPS dsbc;
    memset(&dsbc, 0, sizeof(DSBCAPS) );
    dsbc.dwSize = sizeof(DSBCAPS);

    // retrieve caps
    dsrval = d_lpPrimaryBuffer->GetCaps(&dsbc);
    if(dsrval != DS_OK) { DirectSoundError(dsrval); return false; }

    // check out the mixing overhead
    DWORD cpu_overhead = dsbc.dwPlayCpuOverhead;

    // continous-play the primary buffer to avoid DMA thrashing
    dsrval = d_lpPrimaryBuffer->Play(0, 0, DSBPLAY_LOOPING);
    if(dsrval != DS_OK) { DirectSoundError(dsrval); return false; }
*/

    return true;

}


bool BattleSoundSystem::destroyPrimaryBuffer(void) 
{
/*
    if(! d_lpPrimaryBuffer) return false;

    d_lpPrimaryBuffer->Stop();
    d_lpPrimaryBuffer->Release();
    d_lpPrimaryBuffer = NULL;
*/
   return true;
}



/*
Load WAV file into first buffer of SoundInfo structure
Duplicate initial buffer into remaining buffers
*/

bool BattleSoundSystem::createBuffers(char * filename, BattleSoundInfo & snd_inf) 
{
    WAVEFILE WaveFile;
    LPVOID lpWaveData;
    HRESULT dsrval;
    // DSBUFFERDESC dsbd;
    DSBUFFERDESC1 dsbd;

    // load WAV file
    memset(&WaveFile, 0, sizeof(WAVEFILE) );
    lpWaveData = WAVE_LoadFile (filename, &WaveFile );
    if(lpWaveData == NULL) return false;

    // fill out buffer description
//    memset(&dsbd, 0, sizeof(DSBUFFERDESC) );
    memset(&dsbd, 0, sizeof(dsbd) );
//    dsbd.dwSize = sizeof(DSBUFFERDESC);
    dsbd.dwSize = sizeof(dsbd);
//    dsbd.dwFlags = DSBCAPS_CTRLDEFAULT | DSBCAPS_STATIC ;
    dsbd.dwFlags = DSBCAPS_CTRLFREQUENCY |
                   DSBCAPS_CTRLPAN |
                   DSBCAPS_CTRLVOLUME |
//                   DSBCAPS_LOCSOFTWARE |
                   DSBCAPS_STATIC ;
    dsbd.dwBufferBytes = WaveFile.cbSize;
    dsbd.lpwfxFormat = WaveFile.pwfxInfo;

    // create initial buffer
  //  dsrval = d_lpDirectSound->CreateSoundBuffer(&dsbd, &snd_inf.lpDSBuffer[0], NULL);
    dsrval = d_lpDirectSound->CreateSoundBuffer(reinterpret_cast<DSBUFFERDESC*>(&dsbd), &snd_inf.lpDSBuffer[0], NULL);

#ifdef DEBUG
   BatSoundLog.printf("CreateSoundBuffer(%s)=%d", filename, (int) dsrval);
#endif

    if(dsrval != DS_OK) 
    { 
       directSoundError(dsrval); 
       return false; 
    }

    // copy sound data to buffer
    copySoundToBuffer(WaveFile, lpWaveData, snd_inf.lpDSBuffer[0]);

    // copy initial buffer to duplicate buffers
    for(int f=1;f<snd_inf.num_buffers;f++) {
        dsrval = d_lpDirectSound->DuplicateSoundBuffer(snd_inf.lpDSBuffer[0], &snd_inf.lpDSBuffer[f]);
#ifdef DEBUG
     BatSoundLog.printf("DuplicateSoundBuffer(%s)=%d", filename, (int) dsrval);
#endif
        if(dsrval != DS_OK) { directSoundError(dsrval); return false; }
    }

return true;
}


/*
Copies a loaded wavefile into a direct sound buffer
Deletes the wavefile memory is successful
*/

bool BattleSoundSystem::copySoundToBuffer(WAVEFILE WaveFile, LPVOID lpWaveData, LPDIRECTSOUNDBUFFER lpDSbuffer) 
{
    HRESULT dsrval;
    LPVOID pbData = NULL;
    LPVOID pbData2 = NULL;
    DWORD dwLength;
    DWORD dwLength2;

    // Lock down the DirectSound buffer
    dsrval = lpDSbuffer->Lock(
    0,
    WaveFile.cbSize,
    &pbData,
    &dwLength,
    &pbData2,
    &dwLength2,
    NULL);

#ifdef DEBUG
   BatSoundLog.printf("CopySoundToBuffer::Lock=%d", (int) dsrval);
#endif

    if(dsrval != DS_OK) { directSoundError(dsrval); return false; }

    // Copy first chunk
    memcpy(pbData, WaveFile.pbData, dwLength);

    // Copy second chunk
    if (dwLength2) memcpy(pbData2, WaveFile.pbData+dwLength , dwLength2);

    // free the WAV file data
    free(lpWaveData);

    // Unlock the buffer
    dsrval = lpDSbuffer->Unlock(pbData, dwLength, pbData2, dwLength2);
#ifdef DEBUG
   BatSoundLog.printf("CopySoundToBuffer::Unlock=%d", (int) dsrval);
#endif
    if(dsrval != DS_OK) 
    { 
       directSoundError(dsrval);
       return false; 
    }

    return true;
}



/*
Go through SoundTable, constructing the SoundsArray used by
system for holding DirectSound buffers and related info
*/

bool BattleSoundSystem::initialiseBuffers(void) 
{
    const BattleSoundTableEntry ** ptr = BattleSoundTable;

    while(*ptr) {
        const BattleSoundTableEntry * item_ptr = *ptr;
        BattleSoundInfo snd_inf;

        // prepare the sound_info structure
        snd_inf.frequency_variation = item_ptr->frequency_variation;
        snd_inf.num_buffers = item_ptr->num_buffers;
        snd_inf.buffers_used = 0;
        for(int f=0; f<MAX_SOUND_BUFFERS; f++) snd_inf.lpDSBuffer[f] = NULL;

        // load WAV & create buffers from it
        createBuffers(item_ptr->filename, snd_inf);

        // store the base frequency of this buffer
        snd_inf.lpDSBuffer[0]->SetFrequency(DSBFREQUENCY_ORIGINAL);
        snd_inf.lpDSBuffer[0]->GetFrequency(&snd_inf.base_frequency);

        // place this sound_info structure in our array (vector)
        d_soundsArray.push_back(snd_inf);

        ptr++;
    }

    std::vector<BattleSoundInfo>::iterator iter;
    int count=0;
    for(iter = d_soundsArray.begin(); iter != d_soundsArray.end(); iter++) {
        count++;
    }

   return true;
}



bool BattleSoundSystem::destroyBuffers(void) 
{
   std::vector<BattleSoundInfo>::iterator iter;
    BattleSoundInfo * snd_inf;

    for(iter = d_soundsArray.begin(); iter != d_soundsArray.end(); iter++) {

        snd_inf = &*iter;

        for(int f=0; f<MAX_SOUND_BUFFERS; f++) {

            if(snd_inf->lpDSBuffer[f] != NULL) {
                snd_inf->lpDSBuffer[f]->Stop();
                snd_inf->lpDSBuffer[f]->Release();
            }

        }


    }
   return true;
}





/*
Restores all the DirectSound buffers after any buffer is lost
*/

bool BattleSoundSystem::restoreBuffers(void) 
{
#if defined(DEBUG) && !defined(NOLOG)
   BatSoundLog.printf("--------------------------------------------------------");
   BatSoundLog.printf("Restoring buffers, after a probable DSBSTATUS_BUFFERLOST");
   BatSoundLog.printf("--------------------------------------------------------");
#endif

    if(! d_lpPrimaryBuffer) 
       createPrimaryBuffer();

    const BattleSoundTableEntry ** ptr = BattleSoundTable;
    int index = 0;

    while(*ptr) {
        const BattleSoundTableEntry * item_ptr = *ptr;
        BattleSoundInfo * snd_inf = &d_soundsArray[index];

        // prepeare the structure
        snd_inf->frequency_variation = item_ptr->frequency_variation;
        snd_inf->num_buffers = item_ptr->num_buffers;
        for(int f=0; f<MAX_SOUND_BUFFERS; f++) {
            // release any buffers that aren't destroyed already
            if(snd_inf->lpDSBuffer[f] != NULL) snd_inf->lpDSBuffer[f]->Release();
            // null out
            snd_inf->lpDSBuffer[f] = NULL;
        }

        // load WAV & create buffers from it
        createBuffers(item_ptr->filename, *snd_inf);

        index++;
        ptr++;
    }

   return true;
}


/*
Sound polling function called from BattleGame to process sounds
POS is the current view-center in map coords
*/

void BattleSoundSystem::processSound(BattleMeasure::HexCord pos, BattleSoundZoomEnum zoom) 
{
   if(!d_setupOK || d_inUse) 
      return;

    d_inUse = true;

    // process the CD play list to advance tracks
    d_soundSys->ProcessPlayList();

#if defined(DEBUG) && !defined(NOLOG)
   BatSoundLog.printf("Waiting Queue Size : %i, Playing List Size : %i", d_waitingQueue.size(), d_playingList.size() );
#endif

   ASSERT(zoom >= 0 && zoom <= 3);

    d_listenerOrigin = pos;
   d_listenerZoom = zoom;

    processPlayingList();
    processWaitingQueue();

    d_inUse = false;

}



/*
EntryPoint for playing a sound immediately (ie - doesn't go through waiting list process) - always uses buffer 0 !
*/

bool BattleSoundSystem::triggerSound(BattleSoundTypeEnum type) 
{
   if(!d_setupOK) 
      return false;

    const BattleSoundInfo& array_entry = d_soundsArray[type];

    int i;
    DWORD status;
    for(i=0; i<(array_entry.num_buffers-1); i++) {
        d_soundsArray[type].lpDSBuffer[i]->GetStatus(&status);
        // use the first free buffer (or the last buffer, if none are free)
        if(status != DSBSTATUS_PLAYING) break;
    }

    // have to check that this wasn't a lost buffer
    if(status == DSBSTATUS_BUFFERLOST) 
    { 
       restoreBuffers(); 
       return false; 
    }

    // set random frequency variation
    int freq_variation;
    if(d_soundsArray[type].frequency_variation != 0) 
       freq_variation=  random(0, d_soundsArray[type].frequency_variation);
    else freq_variation = 0;
    array_entry.lpDSBuffer[i]->SetFrequency(d_soundsArray[type].base_frequency + freq_variation);

    // start playing the sound (we're going to use this buffer directly)
    array_entry.lpDSBuffer[i]->Play(0,0,0);

    return true;

}



/*
EntryPoint for playing a sound with no origin (origin is taken to be at listener origin)
*/

bool  BattleSoundSystem::triggerSound(BattleSoundTypeEnum type, BattleSoundPriorityEnum priority) 
{
   if(!d_setupOK) 
      return false;

    return triggerSound(type, priority, d_listenerOrigin);
}


/*
Main function for playing sounds from BattleGame
*/

bool BattleSoundSystem::triggerSound(BattleSoundTypeEnum type, BattleSoundPriorityEnum priority, BattleMeasure::HexCord origin) 
{
   if(!d_setupOK) 
      return false;

    if(d_inUse) return false;

    d_inUse = true;

    // Make sure that this sound is within range
    int dx = abs(d_listenerOrigin.x() - origin.x());
    int dy = abs(d_listenerOrigin.y() - origin.y());
    int sq_distance = (dx*dx)+(dy*dy);
    if(sq_distance > d_squaredMaxAudibleDistance[d_listenerZoom]) { d_inUse = false; return false; }

    // Ensure this sound is not already in queue
    std::deque<BattleSoundEvent>::iterator iter;

    // if sound is unique, make sure no similar types are already waiting
    if(priority == BATTLESOUNDPRIORITY_UNIQUE) {
        if(! d_waitingQueue.empty() ) {
            for(iter = d_waitingQueue.begin(); iter != d_waitingQueue.end(); iter++) {
                if( (*iter).sound_type == type) { d_inUse = false; return false; }
            }
        }
    }

    // otherwise ensure no sounds have same priority, location & type
    if(! d_waitingQueue.empty() ) {
        for(iter = d_waitingQueue.begin(); iter != d_waitingQueue.end(); iter++) {
            if( (*iter).sound_origin == origin && (*iter).sound_priority == priority && (*iter).sound_type == type) { d_inUse = false; return false; }
        }
    }

    // Build up a SoundEvent structure for this sound
    BattleSoundEvent event;
    event.sound_type = type;
    event.sound_priority = priority;
    event.sound_origin = origin;
    event.sound_distance = sq_distance;

    // Insert SoundEvent into WaitingQueue (sorted by priority & distance)
    for(iter=d_waitingQueue.begin(); iter != d_waitingQueue.end(); iter++) {
        // insert sorted
        if(event > *iter) {
            d_waitingQueue.insert(iter, event);
            // if queue has overflowed, remove the final element
            if(d_waitingQueue.size() > d_waitingQueueMaxSize) d_waitingQueue.pop_back();
            d_inUse = false;
            return true;
        }
    }

    // If queue is full & we are trying to insert at back, then return false
    if(d_waitingQueue.size() == d_waitingQueueMaxSize) { d_inUse = false; return false; }

    // Insert sound at end of queue
    d_waitingQueue.push_back(event);

    d_inUse = false;
    return true;

}




/*
Set the spatial properties of the sound relative to the sound origin & the listener's position
*/

bool BattleSoundSystem::setSpatialProperties(BattlePlayingSound * snd) 
{
    int hdist = snd->sound_origin.x() - d_listenerOrigin.x();
    int vdist = snd->sound_origin.y() - d_listenerOrigin.y();

    int sq_distance = abs(hdist*hdist) + abs(vdist*vdist);

    // if this sound is now out of range
    if(sq_distance > d_squaredMaxAudibleDistance[d_listenerZoom]) return false;

    int hpan = hdist * d_panRatio[d_listenerZoom];

    int vfade = -(sq_distance * d_volumeRatio[d_listenerZoom]);
   vfade -= d_verticalVolumeModifier[d_listenerZoom];
   if(vfade < DSBVOLUME_MIN) vfade = DSBVOLUME_MIN;

    snd->lpDSbuffer->SetVolume(vfade);
    snd->lpDSbuffer->SetPan(hpan);

   return true;
}





bool BattleSoundSystem::processWaitingQueue(void) 
{
    // int i;
    bool sound_already_playing;
    std::vector<BattlePlayingSound>::iterator iter;

    // Remove items from front of queue which are already playing

    do {

        // if queue is empty, return false
        if(d_waitingQueue.empty() ) return false;
        // get member at front
        BattleSoundEvent & tmpevent = d_waitingQueue.front();
        sound_already_playing = false;

        // Check PlayingList to see wether this instance of this sound is already playing
        for(iter=d_playingList.begin(); iter != d_playingList.end(); iter++) {

            // if waiting sound event is unique & similar type is playing, alter remove this from queue
            if(tmpevent.sound_priority == BATTLESOUNDPRIORITY_UNIQUE) {
                if( (*iter).sound_type == tmpevent.sound_type) {
                    sound_already_playing = true;
                    d_waitingQueue.pop_front();
                    break;
                }
            }

            // otherwise make sure no sound has same location, type & priority
            else {
                if( (*iter).sound_origin == tmpevent.sound_origin && (*iter).sound_type == tmpevent.sound_type && (*iter).sound_priority == tmpevent.sound_priority) {
                    sound_already_playing = true;
                    d_waitingQueue.pop_front();
                    break;
                }
            }

        } // go through all elements in playihng list

    } while(sound_already_playing == true);

    BattleSoundEvent const & event = d_waitingQueue.front();

    // If there is space in PlayingList, fill out PlayingSound structure & add to list

    if(d_playingList.size() < d_playingListMaxSize) {

        // if no buffers are free for playing this sound event, return false
        if(d_soundsArray[event.sound_type].buffers_used == d_soundsArray[event.sound_type].num_buffers) return false;

        // fill out a new PlayingSound structure
        BattlePlayingSound sound;
        sound.sound_origin = event.sound_origin;
        sound.sound_type = event.sound_type;
        sound.sound_priority = event.sound_priority;

        // find a free buffer
        const BattleSoundInfo& array_entry = d_soundsArray[event.sound_type];

        DWORD status;
        for(int i=0; i<array_entry.num_buffers; i++) {
            d_soundsArray[event.sound_type].lpDSBuffer[i]->GetStatus(&status);
            if(status != DSBSTATUS_PLAYING) break;
        }

        // have to check that this wasn't a lost buffer
        if(status == DSBSTATUS_BUFFERLOST)
        { 
           restoreBuffers();
           return false; 
        }
        // otherwise increase the counter for buffers in use
        else 
           d_soundsArray[event.sound_type].buffers_used++;

        // set this as the buffer
        sound.lpDSbuffer = array_entry.lpDSBuffer[i];
        // sets the spatial properties of the sound
        setSpatialProperties(&sound);
        // set the frequency of the sound, with a random Hz variation if defined
        int freq_variation;
        if(d_soundsArray[event.sound_type].frequency_variation != 0) freq_variation=  random(0, d_soundsArray[event.sound_type].frequency_variation);
        else freq_variation = 0;
        sound.lpDSbuffer->SetFrequency(d_soundsArray[event.sound_type].base_frequency + freq_variation);
        // start playing the sound
        sound.lpDSbuffer->Play(0,0,0);
        // add this sound to the list
        d_playingList.push_back(sound);

        // remove this entry form the WaitingQueue
        d_waitingQueue.pop_front();

        return true;
    }

    // If there is no space in PlayingList, overwrite the lowest priority sound with this one (if there is one ! )

    else {
        // if no buffers are free for playing this sound event, return false
        if(d_soundsArray[event.sound_type].buffers_used == d_soundsArray[event.sound_type].num_buffers) return false;

        // set this at the highest initially
        int lowest_priority = BATTLESOUNDPRIORITY_INSTANT;
        BattlePlayingSound * snd = NULL;
        std::vector<BattlePlayingSound>::iterator index;

        // go through all entires in playing list looking for the lowest priority below the event's priority
        for(iter=d_playingList.begin(); iter != d_playingList.end(); iter++) {
            if( ( (*iter).sound_priority != BATTLESOUNDPRIORITY_UNIQUE) && ( (*iter).sound_priority < lowest_priority) ) {
                lowest_priority = (*iter).sound_priority;
                index = iter;
                snd = &(*iter); }
        }

        // if index is still -1, then no entries of a lower priority were found
        if(snd == NULL) return false;

        // check wether the low priority sound is still playing
        DWORD status;
        snd->lpDSbuffer->GetStatus(&status);

        if(status == DSBSTATUS_PLAYING) {
            // if still playing, stop the sound, decrease the buffer count & reset the play position
            snd->lpDSbuffer->Stop();
            snd->lpDSbuffer->SetCurrentPosition(0);
            d_soundsArray[snd->sound_type].buffers_used--;
        }
        // if we've lost this buffer, restore all & return false
        else if(status == DSBSTATUS_BUFFERLOST) 
        {
           restoreBuffers();
           return false; 
        }

        // otherwise this buffer must have finished after the last check of the PlayingQueue !
        else 
        {
            // decrease buffer count
            d_soundsArray[snd->sound_type].buffers_used--;
            // reset the playing position justin case
            snd->lpDSbuffer->SetCurrentPosition(0);
        }

        // remove the low priority sound
        d_playingList.erase(index);

        // fill out a new PlayingSound structure
        BattlePlayingSound sound;
        sound.sound_origin = event.sound_origin;
        sound.sound_type = event.sound_type;
        sound.sound_priority = event.sound_priority;

        // find a free buffer
        for(int i=0; i<d_soundsArray[event.sound_type].num_buffers; i++) {
            d_soundsArray[event.sound_type].lpDSBuffer[i]->GetStatus(&status);
            if(status != DSBSTATUS_PLAYING) break;
        }

        // have to check that this wasn't a lost buffer
        if(status == DSBSTATUS_BUFFERLOST)
        {
           restoreBuffers();
           return false; 
        }
        // otherwise increase the counter for buffers in use
        else
           d_soundsArray[event.sound_type].buffers_used++;

        // set this as the buffer
        sound.lpDSbuffer = d_soundsArray[event.sound_type].lpDSBuffer[i];
        // sets the spatial properties of the sound
        setSpatialProperties(&sound);
        // set the frequency of the sound, with a random Hz variation if defined
        int freq_variation;
        if(d_soundsArray[event.sound_type].frequency_variation != 0)
           freq_variation=  random(0, d_soundsArray[event.sound_type].frequency_variation);
        else 
           freq_variation = 0;
        sound.lpDSbuffer->SetFrequency(d_soundsArray[event.sound_type].base_frequency + freq_variation);
        // start playing the sound
        sound.lpDSbuffer->Play(0,0,0);

        // remove this entry form the WaitingQueue
        d_waitingQueue.pop_front();
        // add this sound to the playing list
        d_playingList.push_back(sound);

        return true;
    }

}


/*
Remove all the finished sounds from the PlayingList
and set spatial properties if listening origin has changed
*/

bool BattleSoundSystem::processPlayingList(void) 
{
    // int i;
    std::vector<BattlePlayingSound>::iterator iter;

    for(iter=d_playingList.begin(); iter != d_playingList.end(); iter++) {

        // check sound has finished playing
        DWORD status;
        iter->lpDSbuffer->GetStatus(&status);

        if(status != DSBSTATUS_PLAYING)
        {
            // if we've lost this buffer, restore all & return false
            if(status == DSBSTATUS_BUFFERLOST) 
            {
               restoreBuffers();
               return false; 
            }
            // otherwise it has stopped playing, therefore decrease buffer count
            d_soundsArray[iter->sound_type].buffers_used--;
            // reset the playing position justin case
            iter->lpDSbuffer->SetCurrentPosition(0);
            // remove the finished sound
            d_playingList.erase(iter);
            // make sure we're still indexing the correct element in the array
            iter--;
        }
        // if sound is still playing, set the spatial properties if the listening position has changed
        else 
        {
            if(!setSpatialProperties(&*iter)) 
            {
                // a return of false indicates this sound is now out of range & must be stopped
                iter->lpDSbuffer->Stop();
                iter->lpDSbuffer->SetCurrentPosition(0);
                d_soundsArray[iter->sound_type].buffers_used--;
                d_playingList.erase(iter);
                iter--;
            }
        }

    }
   return true;
}





void  BattleSoundSystem::directSoundError(HRESULT err) 
{

    const char* err_msg = NULL;

    switch(err) {
        case DSERR_ALLOCATED : err_msg = InGameText::get(IDS_SoundInUse); break;
        case DSERR_INVALIDPARAM : err_msg = InGameText::get(IDS_InvalidParameter); break;
        case DSERR_NOAGGREGATION : err_msg = InGameText::get(IDS_ObjectNoAggregation); break;
        case DSERR_NODRIVER : err_msg = InGameText::get(IDS_NoSoundDriver); break;
        case DSERR_OUTOFMEMORY : err_msg = InGameText::get(IDS_NoMemory); break;

        case DSERR_UNINITIALIZED : err_msg = InGameText::get(IDS_DSoundNotInitialized); break;
        case DSERR_UNSUPPORTED : err_msg = InGameText::get(IDS_DSound_FunctionUnSupported); break;
        default: err_msg = InGameText::get(IDS_DSoundInternalError); break;
    }

    if(err_msg != NULL)
      { MessageBox(d_appHwnd, err_msg, InGameText::get(IDS_DSoundError), MB_OK); }

#if defined(DEBUG) && !defined(NOLOG)
   if(err_msg != NULL) 
   {
      BatSoundLog.printf("----------------------------------------------------------------------------------------------------");
      BatSoundLog.printf("Reporting DirectSound error message : %s", err_msg);
      BatSoundLog.printf("----------------------------------------------------------------------------------------------------");
   }
#endif

}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
