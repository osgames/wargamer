/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Morale Utilities
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "b_morale.hpp"
#include "bobiter.hpp"


Attribute BattleMoraleUtility::CalcMorale(BattleOB * ob, BattleCP * cp) {

        /*
        * Add up directly attached SPs
        */

        long total = 0;
        int nSP = 0;

        BattleSPIter spIter(ob, cp, BattleSPIter::All);
        while(!spIter.isFinished() )
        {
                BattleSP * sp = spIter.sp();
                const UnitTypeItem& uti = ob->getUnitType(sp->getUnitType());
                total += uti.getBaseMorale();
                nSP++;

                spIter.next();
        }

        /*
        * Add up children
        */

        if(cp->child() ) {
            ConstBattleUnitIter cpIter(cp->child());
            while(!cpIter.isFinished() )
            {
                    const BattleCP * child = cpIter.cp();

                    int count = cp->spCount(true);

                    total += child->morale() * count;
                    nSP += count;

                    cpIter.next();
            }
        }

        if(nSP != 0)
        {
                total = (total + nSP/2) / nSP;
                ASSERT(total <= Attribute_Range);
                return static_cast<Attribute>(total);
        }

        // a CP with no SPs has no morale value
        return 0;
}

#if defined(CUSTOMIZE)

namespace {
        
struct BattleMoraleTally
{
        long d_total;
        int d_nSP;

        BattleMoraleTally() : d_total(0), d_nSP(0) { }

        Attribute get()
        {
                if(d_nSP != 0)
                {
                        long total = (d_total + d_nSP/2) / d_nSP;
                        ASSERT(total <= Attribute_Range);
                        return static_cast<Attribute>(total);
                }
                else
                        return 0;
        }

        void operator += (const BattleMoraleTally& tally)
        {
                d_total += tally.d_total;
                d_nSP += tally.d_nSP;
        }

};



void
SetDefaultMorale(BattleOB * ob, BattleCP * cp, BattleMoraleTally* r_tally) {

        BattleMoraleTally tally;

        BattleCP * child = cp->child();

        while(child != 0)
        {
                BattleMoraleTally childTally;

                SetDefaultMorale(ob, child, &childTally);
                tally += childTally;

                child = child->sister();
        }

        if(cp->morale() == 0)
        {
                BattleSPIter spIter(ob, cp);
                while(!spIter.isFinished() )
                {
                        BattleSP * sp = spIter.sp();
                        const UnitTypeItem& uti = ob->getUnitType(sp->getUnitType());
                        tally.d_total += uti.getBaseMorale();
                        tally.d_nSP++;

                        spIter.next();
                }

                cp->morale(tally.get());
        }
        else
        {
                tally.d_total = cp->morale();
                tally.d_nSP = cp->spCount(true);
        }

        if(r_tally)
        {
                *r_tally = tally;
        }
}

};


void BattleMoraleUtility::SetDefaultMorales(BattleOB * ob, BattleCP * cp) {

        SetDefaultMorale(ob, cp, 0);
}

#endif  // CUSTOMIZE

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
