/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*

PathFind.cpp

A general class for finding a path through the hex-map
Pathing mode is given as an enumerations, which selects the different parameters

The algorithm is based on the A* search
Some optimisations have been added for speed :
  As well as maintaining a tree of OPEN (unvisited) & CLOSED (visited) nodes, an array is
  kept so that a quick lookup may tell the open/closed status of a hex

The algorithm has also been changed from my original version, to specifically support hex pathing,
and uses the HexMap strucures from the battle game.

Apart from this, it is more or less the standard algorithm

Provision is included for multiple attributes to effect the pathing :

  1) terrain height
  2) terrain type
  3) negative / positive influences (ie. influence mapping - such as avoiding enemy units, or moving towards prime objectives)

The basic procedure to find a path is

  1) construct a PathFinder instance specifying the battlemap
  2) call FindPath with the start & end coords, and a SList of <PathPoint> to hold the resultant path
  3) specify the path-finding mode in the above call

*/


#include "stdinc.hpp"
#include "pathfind.hpp"
#include "batdata.hpp"
#include "mmtimer.hpp"
#include "myassert.hpp"

// #include "..\batlogic\moveutil.hpp"

#ifdef DEBUG
#include "clog.hpp"
LogFile pathfinderLog("pathfinder.log");
#endif

using namespace BattleMeasure;

/*

  A global class instance, simply to ensuere array gets deleted
  v. messy I know...

*/

StaticArrayDestructor::StaticArrayDestructor(void) { }

StaticArrayDestructor::~StaticArrayDestructor(void) {

   if(PathFinder::ClosedArray) {
      delete[] PathFinder::ClosedArray;
      PathFinder::ClosedArray = 0;
   }
}

StaticArrayDestructor s_staticArrayDestructor;





int PathFinder::ArraySize = 0;
int PathFinder::MaxArraySize = 0;
PathFinder::ClosedFlag* PathFinder::ClosedArray = 0;

PathFinder::PathFinder(BattleData * batdata) {

    battle_data = batdata;
    battle_map = batdata->map();

    HexCord MapSize = battle_map->getSize();
    MapWidth = MapSize.x();
    MapHeight = MapSize.y();
    ArraySize = MapWidth * MapHeight;

   if(ArraySize > MaxArraySize) {
      MaxArraySize = ArraySize;
      DestroyArray();
      SetupArray();
   }

   ClearArray();

}




PathFinder::~PathFinder(void) {

//    DestroyArray();

}




void
PathFinder::FindPath(int start_x, int start_y, int end_x, int end_y, int path_flags, SList<PathPoint> * PathList) {

    start_hex.x(start_x);
    start_hex.y(start_y);

    end_hex.x(end_x);
    end_hex.y(end_y);

    FindPath(NULL, start_hex, end_hex, path_flags, PathList, NULL);

}

void
PathFinder::FindPath(const HexCord& start, const HexCord& end, int path_flags, SList<PathPoint> * PathList) {

    FindPath(NULL, start, end, path_flags, PathList, NULL);
}


void
PathFinder::FindPath(BattleCP * cp, int start_x, int start_y, int end_x, int end_y, int path_flags, HexList * UnitHexList) {

    start_hex.x(start_x);
    start_hex.y(start_y);

    end_hex.x(end_x);
    end_hex.y(end_y);

    FindPath(cp, start_hex, end_hex, path_flags, NULL, UnitHexList);

}


void
PathFinder::FindPathExcludingHexes(BattleCP * cp, const HexCord& start, const HexCord& end, int path_flags, HexList * exclude_hexes, HexList * hexlist) {

   if(exclude_hexes->first()) {

      do {

         AstarNode * node = new AstarNode;
         node->parent = 0;
         node->hex = exclude_hexes->getCurrent()->d_hex;
         node->cost = 0;
         node->factor = 0;

         PutHexInOpen(node);
         PutHexInOpenArray(node);

      } while(exclude_hexes->next());

   }

   FindPath(cp, start, end, path_flags, NULL, hexlist);

}



void
PathFinder::FindPath(BattleCP * cp, const HexCord& start, const HexCord& end, int path_flags, SList<PathPoint> * PathList, HexList * UnitHexList) {

    if((start.x() == end.x()) && (start.y() == end.y())) return;

    int start_time = Greenius_System::MMTimer::getTime();

    start_hex = start;
    end_hex = end;

    battle_cp = cp;

    // method used to find path
    PathFlags = path_flags;

    PathLength=0;
    PathFound=0;
    PathCost=0;

    // clear the open & closed array
//    ClearArray();
    // set up the open & closed lists
    SetupLists();

    // create the starting node
    AstarNode * start_node = new(AstarNode);
    // set up initial values
    ClearNode(start_node);
    start_node->parent = NULL;
    start_node->hex = start;
    start_node->cost = 0;
    start_node->factor = 0;

    // put this Hex in the open list, to be examined
    PutHexInOpen(start_node);
    PutHexInOpenArray(start_node);

    // retrieve first Hex from open list
    AstarNode * current_node;
    current_node = GetHexFromOpen();


    // repeat until a) destination is found, or b) no more Hexes are left in open
    for(;;) {

        // if we are at destination, break the loop (should we colse this node first ??)
        if(current_node->hex == end_hex) {
            PathFound=1;
            PathCost=current_node->cost;
            // clean up
            RemoveHexFromOpen();
            PutHexInClosedArray(current_node);
            PutHexInClosed(current_node);
            break;
        }

        // add the hexes surrounding our current hex, to the open list
        AddSurroundingHexes(current_node, &current_node->hex);

        // remove first Hex from open list, since we have now checked it
        RemoveHexFromOpen();

        // put current hex in closed list, to mark it as visited
        PutHexInClosedArray(current_node);
        PutHexInClosed(current_node);

        // get next Hex off the open list, for examining
        current_node = GetHexFromOpen();

        // if current node is the last in the open list, then all hexes have been visited & destination not found
        if(current_node==NULL) {
            PathFound=0;
            break; }

    }


    // if a path was found, trace it back to the start node via the connections in the closed list
    // using which ever path-type was passed in for notmal or units' paths
    if(PathFound) {

        if(PathList != NULL) {
            TraceBackPath(current_node, PathList);
            PathList->rewind();
        }
        else if(UnitHexList != NULL) {
            TraceBackUnitPath(current_node, UnitHexList);
            UnitHexList->rewind();
        }
    }


    int end_time = Greenius_System::MMTimer::getTime();

    time_taken = ( (float)end_time - (float)start_time) / 1000;

#ifdef DEBUG
    DebugReport();
#endif

    DestroyLists();

// int finished = 1;
#ifdef DEBUG
SaveAsRaw();
#endif
}



/*
Initialise the Open & Closed lists
*/

void
PathFinder::SetupLists(void) {

    open_list_size=0;
    closed_list_size=0;

    OpenList = new(AstarNodeList);
    OpenList->node=NULL;
    OpenList->next=NULL;

    ClosedList = new(AstarNodeList);
    ClosedList->node=NULL;
    ClosedList->next=NULL;

}



/*
Delete the Open & Closed lists
This ammounts to deleting all nodes visited & unvisited
The closed list is maintained solely for this purpose
*/

void
PathFinder::DestroyLists(void) {

    AstarNodeList * ptr;

    // destroy the open list
    ptr = OpenList->next;

    if(ptr != NULL) {

        delete ptr->node;
        AstarNodeList * tmpptr = ptr;
        ptr = ptr->next;
        delete tmpptr;

        open_list_size--;
    }

    delete OpenList;
    OpenList = NULL;

    // destroy the closed list
    ptr = ClosedList->next;

    if(ptr != NULL) {

        delete ptr->node;
        AstarNodeList * tmpptr = ptr;
        ptr = ptr->next;
        delete tmpptr;

        closed_list_size--;
    }

    delete ClosedList;
    ClosedList = NULL;

}




/*
Initalise the Open/Closed array to map dimensions
*/

void
PathFinder::SetupArray(void)
{

   ASSERT(ClosedArray == 0);
   ASSERT(ArraySize != 0);
    // ClosedArray=(char*)malloc(ArraySize);
    ClosedArray = new ClosedFlag[ArraySize];
}

/*
Clear the Open/Closed array to zero
*/

void PathFinder::ClearArray(void) 
{
   std::fill(&ClosedArray[0], &ClosedArray[ArraySize], INIT_FLAG);

//    if(ClosedArray) memset(ClosedArray,0,ArraySize);
}

/*
Destroy the Open/Closed array
*/

void
PathFinder::DestroyArray(void) {

    if(ClosedArray) {
      // free(ClosedArray);
      delete[] ClosedArray;
      ClosedArray = NULL;
   }

}


/*
Clear the specified node, ie. NULL out all it's child pointer
*/

void
PathFinder::ClearNode(AstarNode * node) {
    node->child[0]=NULL;
    node->child[1]=NULL;
    node->child[2]=NULL;
    node->child[3]=NULL;
    node->child[4]=NULL;
    node->child[5]=NULL;
}


/*
Put a Hex in the Open list
Sorts nodes with respect to their 'cost' factor so that the first retrieved, is always :
a) closer to the destination & b) least in cost
*/

void
PathFinder::PutHexInOpen(AstarNode * node) {

    open_list_size++;

    AstarNodeList * ptr = OpenList;

    // insert Hex in list
    while(ptr->next != NULL) {

        if(node->factor < ptr->next->node->factor) {  // sort with respect to 'cost' factor
            AstarNodeList * newnode = new(AstarNodeList);
            newnode->node = node;
            newnode->next = ptr->next;
            ptr->next = newnode;
            return;
        }

        ptr=ptr->next;
    }

    // put Hex at end of list
    AstarNodeList * newnode = new AstarNodeList;
    newnode->node = node;
    newnode->next = NULL;

    ptr->next = newnode;

}

/*
Removes the first Hex from the Open list
*/

void
PathFinder::RemoveHexFromOpen(void) {

    open_list_size--;

    AstarNodeList * ptr;

#ifdef DEBUG
    if(OpenList->next == NULL)
      FORCEASSERT("Oops - trying to remove node from the empty openlist");
#endif

    ptr = OpenList->next->next;

    delete OpenList->next;

    OpenList->next = ptr;
}




/*
Returns the first node in the Open list
*/
AstarNode *
PathFinder::GetHexFromOpen(void) {

    if(OpenList->next == NULL) return NULL;

    return(OpenList->next->node);
}



void
PathFinder::PutHexInClosed(AstarNode * node) {

    closed_list_size++;

    AstarNodeList * newnode = new AstarNodeList;

    newnode->node = node;

    newnode->next = ClosedList->next;

    ClosedList->next = newnode;
}

/*
Sets the Open/Closed array flag to indicate that this coordinate is Closed (ie.visited)
*/

void
PathFinder::PutHexInClosedArray(AstarNode * node) {
    int address=(node->hex.y() * MapWidth) + node->hex.x();
    ClosedArray[address] = CLOSED_FLAG;
}


/*
Checks wether the coordinate in the Open/Closed array is marked as Closed (ie.visited)
*/

int
PathFinder::IsHexInClosedArray(HexCord * hexpos) {
    int address=(hexpos->y() * MapWidth) + hexpos->x();
    if(ClosedArray[address] == CLOSED_FLAG) return 1;
    else return 0;
}


/*
Sets the Open/Closed array flag to indicate that this coordinate is Open (ie.unvisited)
*/

void
PathFinder::PutHexInOpenArray(AstarNode * node) {
    int address=(node->hex.y() * MapWidth) + node->hex.x();
    ClosedArray[address] = OPEN_FLAG;
}


/*
Checks wether the coordinate in the Open/Closed array is marked as Open (ie.unvisited)
*/

int
PathFinder::IsHexInOpenArray(HexCord * hexpos) {
    int address=(hexpos->y() * MapWidth) + hexpos->x();
    if(ClosedArray[address] == OPEN_FLAG) return 1;
    return 0;
}




/*
Checks wether the given coordinates are valid for the map dimensions
*/

int
PathFinder::IsHexValid(HexCord * hexpos) {
    if(hexpos->x() >= 0 && hexpos->x() < MapWidth && hexpos->y() >= 0 && hexpos->y() < MapHeight) return 1;
    return 0;
}


/*
Checks wether the given coordinates are passable or impassable
This must be changed to interface with the HexMap system
*/

int
PathFinder::IsHexPassable(const HexCord& hex) {

  // convert unit type flags to local enum
  // convert spformation to local enum
  LocalUnitType unitType;
  LocalSPFormation formation;
  getTypeAndFormation(battle_cp, formation, unitType);


  // get terrain
  const BattleTerrainHex& hexInfo = battle_data->getTerrain(hex);
  if(BattleTables::prohibitedTerrain().getValue(unitType, hexInfo.d_terrainType, March) != 0)
  {
         // Modified by Steven so BattleData not dependent on logic
         // if(!B_Logic::roadHex(battle_data, hex))
         if(!battle_data->roadHex(hex))
         {
                // if terrain is passable, check for river
                for(int i = 0; i < 2; i++)
                {
                  PathType pt = (i == 0) ? hexInfo.d_path1.d_type : hexInfo.d_path2.d_type;

                  if(pt != LP_None)
                  {
                         if(BattleTables::pathModifiers().getValue(unitType, pt, formation) == 0)
                                return False;
                  }
                }
         }

         return True;
  }

  return False;
}



UWORD
PathFinder::terrainCost(RPBattleData bd, const RefBattleCP& cp, const HexCord& hex)
{
  // convert unit type flags to local enum
  // convert spformation to local enum
  LocalUnitType unitType;
  LocalSPFormation formation;

  getTypeAndFormation(cp, formation, unitType);

  // get terrain
  const BattleTerrainHex& hexInfo = bd->getTerrain(hex);

  return static_cast<UWORD>(
         100 + maximum(0, 100 - BattleTables::terrainModifiers().getValue(unitType, hexInfo.d_terrainType, formation)));
}




UWORD
PathFinder::pathCost(RPBattleData bd, const RefBattleCP& cp, PathType pathtype) {

  // convert unit type flags to local enum
  // convert spformation to local enum
  LocalUnitType unitType;
  LocalSPFormation formation;

  getTypeAndFormation(cp, formation, unitType);


  return static_cast<UWORD>(
         100 + maximum(0, 100 - BattleTables::pathModifiers().getValue(unitType, pathtype, formation)));
}

/*
Adds the surrounding hexes to the Open list
*/

void
PathFinder::AddSurroundingHexes(AstarNode * current, HexCord * CurrentHexPos) {

    AstarNode * newnode;

    // MaxHexPos is equal to the dimensions of the map
    HexCord MaxHexPos;
    MaxHexPos.x(MapWidth);
    MaxHexPos.y(MapHeight);

    // NewHexPos is the surrounding hex being looked at
    HexCord NewHexPos;
    // MoveDirection is the direction from current to neighbouring hex
    HexCord::HexDirection MoveDirection;

/*
Visit all the surrounding nodes
Add to the open list ONLY if not already Open or Closes
*/

    // HexDirection = Right
    MoveDirection = HexCord::Right;
    if(CurrentHexPos->move(MoveDirection, NewHexPos, MaxHexPos))
//    if(IsHexPassable(NewHexPos)) // is Hex passable
    if(! IsHexInOpenArray(&NewHexPos)) // Hex not in open
    if(! IsHexInClosedArray(&NewHexPos)) { // Hex not in closed
        newnode=CreateNewNode(current,&NewHexPos, MoveDirection);
        PutHexInOpen(newnode);
        PutHexInOpenArray(newnode); }

    // HexDirection = Up Right
    MoveDirection = HexCord::UpRight;
    if(CurrentHexPos->move(MoveDirection, NewHexPos, MaxHexPos))
//    if(IsHexPassable(NewHexPos)) // is Hex passable
    if(! IsHexInOpenArray(&NewHexPos)) // Hex not in open
    if(! IsHexInClosedArray(&NewHexPos)) { // Hex not in closed
        newnode=CreateNewNode(current,&NewHexPos, MoveDirection);
        PutHexInOpen(newnode);
        PutHexInOpenArray(newnode); }

    // HexDirection = Up Left
    MoveDirection = HexCord::UpLeft;
    if(CurrentHexPos->move(MoveDirection, NewHexPos, MaxHexPos))
//    if(IsHexPassable(NewHexPos)) // is Hex passable
    if(! IsHexInOpenArray(&NewHexPos)) // Hex not in open
    if(! IsHexInClosedArray(&NewHexPos)) { // Hex not in closed
        newnode=CreateNewNode(current,&NewHexPos, MoveDirection);
        PutHexInOpen(newnode);
        PutHexInOpenArray(newnode); }

    // HexDirection = Left
    MoveDirection = HexCord::Left;
    if(CurrentHexPos->move(MoveDirection, NewHexPos, MaxHexPos))
//    if(IsHexPassable(NewHexPos)) // is Hex passable
    if(! IsHexInOpenArray(&NewHexPos)) // Hex not in open
    if(! IsHexInClosedArray(&NewHexPos)) { // Hex not in closed
        newnode=CreateNewNode(current,&NewHexPos, MoveDirection);
        PutHexInOpen(newnode);
        PutHexInOpenArray(newnode); }

    // HexDirection = Down Left
    MoveDirection = HexCord::DownLeft;
    if(CurrentHexPos->move(MoveDirection, NewHexPos, MaxHexPos))
//    if(IsHexPassable(NewHexPos)) // is Hex passable
    if(! IsHexInOpenArray(&NewHexPos)) // Hex not in open
    if(! IsHexInClosedArray(&NewHexPos)) { // Hex not in closed
        newnode=CreateNewNode(current,&NewHexPos, MoveDirection);
        PutHexInOpen(newnode);
        PutHexInOpenArray(newnode); }

    // HexDirection = Down Right
    MoveDirection = HexCord::DownRight;
    if(CurrentHexPos->move(MoveDirection, NewHexPos, MaxHexPos))
//    if(IsHexPassable(NewHexPos)) // is Hex passable
    if(! IsHexInOpenArray(&NewHexPos)) // Hex not in open
    if(! IsHexInClosedArray(&NewHexPos)) { // Hex not in closed
        newnode=CreateNewNode(current,&NewHexPos, MoveDirection);
        PutHexInOpen(newnode);
        PutHexInOpenArray(newnode); }

}




AstarNode *
PathFinder::CreateNewNode(AstarNode * last, HexCord * NewHexPos, BattleMeasure::HexCord::HexDirection direction) {
    // take the last node, & valid coords of the next node
    // calculate the offset of these Hexes into the terrain maps (heights & types)
    int last_address = (last->hex.y() * MapWidth) + last->hex.x();
    int new_address = (NewHexPos->y() * MapWidth) + NewHexPos->x();

    // put the new & old terrain heights & types into variables for quick access
    BattleTerrainHex last_hex = battle_map->get(last->hex);
    BattleTerrainHex current_hex = battle_map->get(*NewHexPos);

    int last_height = last_hex.d_height;
    int last_type = last_hex.d_terrainType;
    int new_height = current_hex.d_height;
    int new_type = last_hex.d_terrainType;

    // create the new node
    AstarNode * newnode = new(AstarNode);
    // initialise it (clear all child fields to zero)
    ClearNode(newnode);

    // the distance is exactly calculated - probably not necessary to be so accurate
    //int distance = DistanceBetweenHexes(NewHexPos,&end_hex);

   int dx = abs(NewHexPos->x() - end_hex.x() );
   int dy = abs(NewHexPos->y() - end_hex.y() );
   int distance;
   if(dx>dy) {
      distance = dx + (dy>>1);
   }
   else distance = dy + (dx>>1);

   //int distance = sqrt((float)(dx*dx + dy*dy));

    // overall cost of moving from last hex to this one
    int movement_cost = 1;
    bool on_road = false;

    // this test for roads is a bit expensive.. is there a better way ?
    if(PathFlags & PATHFLAG_USEROADS) {
        UWORD path1_cost = 0;
        UWORD path2_cost = 0;
        if(
            current_hex.d_path1.d_type == LP_Road ||
            current_hex.d_path1.d_type == LP_Track ||
            current_hex.d_path1.d_type == LP_SunkenRoad ||
            current_hex.d_path1.d_type == LP_SunkenTrack ||
            current_hex.d_path1.d_type == LP_RoadBridge ||
            current_hex.d_path1.d_type == LP_TrackBridge) {
                on_road = true;
                // cost is in yards / minute.  max is 100, min currently 25.  divide by 4 to keep reasonable
                path1_cost = pathCost(battle_data, battle_cp, current_hex.d_path1.d_type) >> 1;
            }
        if(
            current_hex.d_path2.d_type == LP_Road ||
            current_hex.d_path2.d_type == LP_Track ||
            current_hex.d_path2.d_type == LP_SunkenRoad ||
            current_hex.d_path2.d_type == LP_SunkenTrack ||
            current_hex.d_path2.d_type == LP_RoadBridge ||
            current_hex.d_path2.d_type == LP_TrackBridge) {
                on_road = true;
                // cost is in yards / minute.  max is 100, min currently 25.  divide by 4 to keep reasonable
                path2_cost = pathCost(battle_data, battle_cp, current_hex.d_path2.d_type) >> 1;
            }

        // if not on road, path cost will be 100.  otherwise will vary from about 75 to 95
        UWORD p1 = 100 - path1_cost;
        UWORD p2 = 100 - path2_cost;
        // choose the minimum path cost here
        movement_cost += minimum(p1, p2);
    }

    // calculate cost for unit
    if(PathFlags & PATHFLAG_TERRAINCOST) {
        // movement values are all above 100
        if(!on_road) if(battle_cp) movement_cost += (terrainCost(battle_data, battle_cp, *NewHexPos) - 100) *3;
    }


    if( (! (PathFlags & PATHFLAG_AVOIDUPHILL)) && (! (PathFlags & PATHFLAG_AVOIDDOWNHILL)) ) {
        movement_cost += 1;
    }

    else if( (PathFlags & PATHFLAG_AVOIDUPHILL) && (PathFlags & PATHFLAG_AVOIDDOWNHILL) ) {
        if(new_height > last_height) movement_cost += 10+((new_height - last_height)*2);
        else if(new_height < last_height) movement_cost += 10+((last_height - new_height)*2);
        else movement_cost += 1;
    }

    else if(PathFlags & PATHFLAG_AVOIDUPHILL) {
        if(new_height > last_height) movement_cost += 10+((new_height - last_height)*2);
        else movement_cost += 1;
    }

    else if(PathFlags & PATHFLAG_AVOIDDOWNHILL) {
        if(new_height < last_height) movement_cost += 10+((last_height - new_height)*2);
        else movement_cost += 1;
    }


    if(PathFlags & PATHFLAG_AVOIDPROHIBITED) {
        // these modes only applicable if we are pathfinding for a CP
        if(battle_cp) if(! IsHexPassable(*NewHexPos) ) movement_cost += 10000;
    }

    // increase the distsance factor
    distance *=2;


    // loop through the parent node's children, until we find a pointer
    int i;
    for(i=0;i<6;i++) if(last->child[i] == NULL) break;

    // connect last node to new node
    last->child[i] = newnode;

    newnode->parent = last;
    newnode->hex = *NewHexPos;
    newnode->dir_from_parent = direction;
    // cost of getting to this node
    newnode->cost = last->cost + movement_cost;
    // estimated distance to end
    newnode->factor = newnode->cost + distance;

    return(newnode);
}





/*
Trace the path back from the end node to the start node, by traversing through each parent node
Store path in Slist
*/

void
PathFinder::TraceBackPath(AstarNode * current_node, SList<PathPoint> * path_list) {

    // set up linked list to store path in
    path_list->rewind();
    PathPoint * path_node;

    HexCord::HexDirection last_dir = HexCord::Stationary;
    HexCord::HexDirection current_dir;

    AstarNode * trace_node = current_node;

    // trace back from end hex to start hex
    while(trace_node->parent != NULL) {

        int address=(trace_node->hex.y() * MapWidth) + trace_node->hex.x();
        ClosedArray[address] = MARK_FLAG;

        PathLength++;
        PathCost += trace_node->factor;

        path_node = new PathPoint;
        path_node->hex = trace_node->hex;
        current_dir = trace_node->dir_from_parent;
        path_node->dir_prev = HexCord::oppositeDirection(current_dir);
        path_node->dir_next = last_dir;
        last_dir = current_dir;

        // add this path point to list
        path_list->insert(path_node);

        trace_node = trace_node->parent;
    }

    // add the start hex to the list
    path_node = new PathPoint;
    path_node->hex = trace_node->hex;
    path_node->dir_prev = HexCord::Stationary;
    path_node->dir_next = last_dir;
    path_list->insert(path_node);

    PathLength++;
    PathCost += trace_node->factor;
}







/*
Trace the path back from the end node to the start node, by traversing through each parent node
Store path in Slist
*/

void
PathFinder::TraceBackUnitPath(AstarNode * current_node, HexList * hex_list) {

    // set up linked list to store path in
    hex_list->rewind();
    HexItem * hex_node;

    HexCord::HexDirection last_dir = FacingToDirection(battle_cp->facing() );
    HexCord::HexDirection current_dir;

    AstarNode * trace_node = current_node;

    // trace back from end hex to start hex
    while(trace_node->parent != NULL) {

        int address=(trace_node->hex.y() * MapWidth) + trace_node->hex.x();
        ClosedArray[address] = MARK_FLAG;
        PathLength++;
        PathCost += trace_node->factor;

        hex_node = new HexItem(trace_node->hex, DirectionToFacing(last_dir) );

        current_dir = trace_node->dir_from_parent;
        last_dir = current_dir;

        // add this path point to list
        hex_list->insert(hex_node);

        trace_node = trace_node->parent;
    }

    // add the start hex to the list
    hex_node = new HexItem(trace_node->hex, DirectionToFacing(last_dir) );

//    path_node->dir_next = last_dir;
    hex_list->insert(hex_node);

    PathLength++;
    PathCost += trace_node->factor;
}




BattleMeasure::HexPosition::Facing
PathFinder::DirectionToFacing(HexCord::HexDirection direction) {

    switch(direction) {

        case HexCord::Right : return BattleMeasure::HexPosition::East;
        case HexCord::UpRight : return BattleMeasure::HexPosition::NorthEastFace;
        case HexCord::UpLeft : return BattleMeasure::HexPosition::NorthWestFace;
        case HexCord::Left : return BattleMeasure::HexPosition::West;
        case HexCord::DownLeft : return BattleMeasure::HexPosition::SouthWestFace;
        case HexCord::DownRight : return BattleMeasure::HexPosition::SouthEastFace;
        case HexCord::Stationary : return BattleMeasure::HexPosition::Facing_Undefined;

        default : { FORCEASSERT("Oops - invalid hex-direction to convert to facing in unit path-generator"); return BattleMeasure::HexPosition::Facing_Undefined; }
    }
}



HexCord::HexDirection
PathFinder::FacingToDirection(BattleMeasure::HexPosition::Facing facing) {

    switch(facing) {

        case BattleMeasure::HexPosition::East : return HexCord::Right;
        case BattleMeasure::HexPosition::NorthEastPoint : return HexCord::Right;
        case BattleMeasure::HexPosition::NorthEastFace : return HexCord::UpRight;
        case BattleMeasure::HexPosition::North : return HexCord::UpRight;
        case BattleMeasure::HexPosition::NorthWestFace : return HexCord::UpLeft;
        case BattleMeasure::HexPosition::NorthWestPoint : return HexCord::UpLeft;
        case BattleMeasure::HexPosition::West : return HexCord::Left;
        case BattleMeasure::HexPosition::SouthWestPoint : return HexCord::Left;
        case BattleMeasure::HexPosition::SouthWestFace : return HexCord::DownLeft;
        case BattleMeasure::HexPosition::South : return HexCord::DownLeft;
        case BattleMeasure::HexPosition::SouthEastFace : return HexCord::DownRight;
        case BattleMeasure::HexPosition::SouthEastPoint : return HexCord::DownRight;

        case BattleMeasure::HexPosition::Facing_Undefined : return HexCord::Stationary;

        default : { FORCEASSERT("Oops - invalid facing to convert to hex-direction in unit path-generator"); return HexCord::Stationary; }
    }
}


/*
Save out the closed array as a .RAW file
mostly to see if discrepancies with the pathfinding are due to
more complicated distance measurements in the hexmap
*/
void
PathFinder::SaveAsRaw(void)
{
#ifdef DEBUG
   if(ClosedArray == NULL) return;

    FILE * file;
    // open "path.raw" for writing
    file = fopen("path.raw", "wb");
    if(file == NULL) return;
    // save out data
    fwrite(ClosedArray, 1, ArraySize, file);
    // close file
    fclose(file);
#endif
}


void
PathFinder::DebugReport(void) {
#ifdef DEBUG
    pathfinderLog.printf("Pathfinder invoked...\n");
    pathfinderLog.printf("_____________________\n\n");

    if(battle_cp) pathfinderLog.printf("BattleCP : %i\n",battle_cp);
    else pathfinderLog.printf("BattleCP : NONE\n");

    if(PathFound) pathfinderLog.printf("Path status : FOUND\n");
    else pathfinderLog.printf("Path status : NOT FOUND\n");

    pathfinderLog.printf("Path Flags : ");
    if(PathFlags & PATHFLAG_AVOIDUPHILL) pathfinderLog.printf("PATHFLAG_AVOIDUPHILL ");
    if(PathFlags & PATHFLAG_AVOIDDOWNHILL) pathfinderLog.printf("PATHFLAG_AVOIDDOWNHILL ");
    if(PathFlags & PATHFLAG_AVOIDPROHIBITED) pathfinderLog.printf("PATHFLAG_AVOIDPROHIBITED ");
    if(PathFlags & PATHFLAG_TERRAINCOST) pathfinderLog.printf("PATHFLAG_TERRAINCOST ");
    if(PathFlags & PATHFLAG_USEROADS) pathfinderLog.printf("PATHFLAG_USEROADS ");

    pathfinderLog.printf("\n");

    pathfinderLog.printf("Path Length : %i hexes\n", PathLength);
    pathfinderLog.printf("Path Cost : %i\n", PathCost);

    pathfinderLog.printf("Nodes Closed : %i\n", closed_list_size);
    pathfinderLog.printf("Nodes Open : %i\n", open_list_size);

    int val = (sizeof(AstarNode) + sizeof(AstarNodeList) ) * (open_list_size + closed_list_size);

    pathfinderLog.printf("Memory Alloc'd for Nodes : %i bytes\n", val);
    pathfinderLog.printf("Memory Alloc'd for BitArray : %i bytes\n", ArraySize);
    pathfinderLog.printf("Total Memory Alloc'd : %i bytes\n", (val + ArraySize) );

    pathfinderLog.printf("TimeTaken : %f seconds\n", time_taken);

    pathfinderLog.printf("\n\n");
#endif
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
