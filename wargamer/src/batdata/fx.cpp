/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "fx.hpp"
#include "filecnk.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : FX
        Description : Classes for defining spot effects on battlefield
        
-------------------------------------------------------------------------------------------------------------------*/



const UWORD EffectDisplayInfo::s_fileVersion = 0x0000;


Boolean
EffectDisplayInfo::readData(FileReader& f) {

   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   int t;
   f >> t;
   Type = static_cast<EffectTypeEnum>(t);

   f >> Flags;
   f >> SpriteIndex;
   f >> FrameCounter;
   f >> FrameTimer;
   f >> Duration;
   f >> LastTime;
   f >> Scale;
   Pos.readData(f);

   return f.isOK();
}

Boolean
EffectDisplayInfo::writeData(FileWriter& f) const {

   f << s_fileVersion;

   int t = static_cast<EffectTypeEnum>(Type);
   f << t;

   f << Flags;
   f << SpriteIndex;
   f << FrameCounter;
   f << FrameTimer;
   f << Duration;
   f << LastTime;
   f << Scale;
   Pos.writeData(f);

   return f.isOK();
}






const UWORD HexEffectsInfo::s_fileVersion = 0x0000;


Boolean
HexEffectsInfo::readData(FileReader& f) {

   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   int num;
   f >> num;

   for(int i=0; i<num; i++) {

      EffectDisplayInfo info;
      info.readData(f);

      d_Effects.push_back(info);
   }

   return f.isOK();
}

Boolean
HexEffectsInfo::writeData(FileWriter& f) const {

   f << s_fileVersion;

   int num = d_Effects.size();
   f << num;

   for(int i=0; i<num; i++) {

      EffectDisplayInfo info = d_Effects[i];
      info.writeData(f);
   }

   return f.isOK();
}








const UWORD EffectsList::s_fileVersion = 0x0000;
const char EffectsList::s_fileID[] = "BattleEffectsList";

Boolean
EffectsList::readData(FileReader& f) {

   FileChunkReader fc(f, s_fileID);

   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   int num;
   f >> num;

   for(int i=0; i<num; i++) {

      BattleMeasure::HexCord hex;
      hex.readData(f);

      HexEffectsInfo info;
      info.readData(f);

      d_effects.insert(EffectsListType::value_type(hex, info));
   }

   return f.isOK();
}




Boolean
EffectsList::writeData(FileWriter& f) const {

   FileChunkWriter fc(f, s_fileID);

   f << s_fileVersion;

   int num = d_effects.size();
   f << num;

   EffectsListType::const_iterator i;

   for(i = d_effects.begin(); i != d_effects.end(); i++) {

      BattleMeasure::HexCord hex = (*i).first;
      hex.writeData(f);

      HexEffectsInfo info = (*i).second;
      info.writeData(f);
   }

   return f.isOK();
}

void EffectsList::add(const BattleMeasure::HexCord & hex, EffectDisplayInfo & fx) 
{
   HexEffectsInfo * hex_fx = get(hex);
   if(hex_fx != NULL) hex_fx->add(fx);
   else {
       HexEffectsInfo new_hex_fx;
       d_effects.insert(EffectsListType::value_type(hex, new_hex_fx) );
       hex_fx = get(hex);
       hex_fx->add(fx);
   }
}

HexEffectsInfo * EffectsList::get(const BattleMeasure::HexCord& hex) {
   EffectsListType::iterator i = d_effects.find(hex);
   if(i == d_effects.end() ) return NULL;
   else return & (*i).second;
}

// remove all effects from a specified hex
void EffectsList::remove(const BattleMeasure::HexCord& hex)  {
   d_effects.erase(hex);
}

// clears the effects list
void EffectsList::clear(void) {
   d_effects.erase(d_effects.begin(), d_effects.end() );
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */

