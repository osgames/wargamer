/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Terrain Information as stored in map
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "b_terr.hpp"
#include "resstr.hpp"



#ifdef STATIC_DATA_TABLES
#include "g_terr.hpp"
#include "palette.hpp"          // to convert COLORREF into ColourIndex
#endif

using namespace SWG_Sprite;

/*
 * This must match up exactly with the LT_Enumerations, but excluding LT_None
 */

#ifdef STATIC_DATA_TABLES

LTerrainTable::Info logicalTerrainInitData[] = {
        { false, false, 0 }, // LT_Clear
        {  true, false, 0 }, // LT_Water
        { false, false, 0 }, // LT_Orchard
        { false, false, 0 }, // LT_Vineyard
        { false, false, 0 }, // LT_LightWood
        { false, false, 0 }, // LT_DenseWood
        { false, false, 0 }, // LT_PloughedField
        { false, false, 0 }, // LT_CornField
        { false, false, 0 }, // LT_WheatField
        { false, false, 0 }, // LT_Rough
        { false, false, 0 }, // LT_Marsh
        { false,  true, 0 }, // LT_Town
        { false,  true, 0 }, // LT_Village
        { false,  true, 0 }, // LT_Farm
        { false,  true, 0 }, // LT_Cemetary

        { false, false, 0 }, // LT_Road
        { false, false, 0 }, // LT_Track
        { false, false, 0 }, // LT_River
        { false, false, 0 }, // LT_Stream

        { false, false, 0 }, // LT_SunkenRoad
        { false, false, 0 }, // LT_SunkenTrack
        { false, false, 0 }, // LT_SunkenRiver
        { false, false, 0 }, // LT_SunkenStream

        { false, false, 0 }, // LT_RoadBridge
        { false, false, 0 }, // LT_TrackBridge
};

#endif

/*------------------------------------------------
 * Path Table::Info
 */

PathTable::Info::Info() :
   d_type(NoType),
   d_terrain(LT_None),
   d_graphic(),
   d_name(0),
   d_nameAbrev(0),
   d_bridge(LP_None),
   d_graphicOffsets(0)
{

}

PathTable::Info::~Info()
{
   delete[] d_graphicOffsets;
}

PathTable::Info& PathTable::Info::operator=(const Info& inf)
{
   d_type = inf.d_type;
   d_terrain = inf.d_terrain;
   d_graphic = inf.d_graphic;
   d_name = inf.d_name;
   d_nameAbrev = inf.d_nameAbrev;
   d_bridge = inf.d_bridge;
   d_graphicOffsets = 0;

   if(inf.d_graphicOffsets)
   {
      d_graphicOffsets = new UWORD[63];
      for (int i = 0; i < 63; ++i)
      {
         d_graphicOffsets[i] = inf.d_graphicOffsets[i];
      }
   }
   return *this;
}

PathTable::Info::Info(const Info& inf) :
   d_type(inf.d_type),
   d_terrain(inf.d_terrain),
   d_graphic(inf.d_graphic),
   d_name(inf.d_name),
   d_nameAbrev(inf.d_nameAbrev),
   d_bridge(inf.d_bridge),
   d_graphicOffsets(0)
{
   if(inf.d_graphicOffsets)
   {
      d_graphicOffsets = new UWORD[63];
      for (int i = 0; i < 63; ++i)
      {
         d_graphicOffsets[i] = inf.d_graphicOffsets[i];
      }
   }
}

SpriteLibrary::Index PathTable::Info::graphic(EdgeMap edges) const
{
        ASSERT(edges != 0);

        UWORD offset = edges - 1;

        if(d_graphicOffsets != 0)
                offset = d_graphicOffsets[offset];

        return d_graphic + offset;
}

/*------------------------------------------------
 * Path Table
 */

PathTable::PathTable()
{
}

PathTable::~PathTable()
{
}

const PathTable::Info& PathTable::operator [] (Index t) const
{
        ASSERT(t != None);
        ASSERT(t <= MaxIndex);
        ASSERT(t >= First);
        ASSERT( (t - First) < d_items.size() );

        if( (t == None) || (t > MaxIndex) )
                throw Error();

        return d_items[t - 1];
}

PathTable::Info& PathTable::operator [] (Index t)
{
        ASSERT(t != None);
        ASSERT(t <= MaxIndex);
        ASSERT(t >= First);
        ASSERT( (t - First) < d_items.size() );

        if( (t == None) || (t > MaxIndex) )
                throw Error();

        return d_items[t - 1];
}

bool PathTable::isType(Index i, BasicType t) const
{
        if(i == None)
                return (t == NoType);
        else
                return (operator[](i).basicType() == t);
}

#ifdef STATIC_DATA_TABLES


struct PathInfoInitData
{
#if 0
        PathInfoInitData(PathTable::BasicType baseType, LogicalTerrainEnum terrain, SpriteLibrary::Index graphic, const char* name, COLORREF col, const char* bridge, const UWORD* offsets) :
                d_type(baseType),
                d_terrain(terrain),
                d_graphic(graphic),
                d_name(name),
                d_baseColor(col),
                d_bridge(bridge),
                d_graphicOffsets(offsets)
        {
                // ASSERT( (graphic + 63) < TerrainSpriteIndex_HowMany);
                ASSERT(graphic < TerrainSpriteIndex_HowMany);
                ASSERT(terrain < LT_HowMany);
        }

        PathInfoInitData() :
                d_type(PathTable::NoType),
                d_terrain(LT_None),
                d_graphic(SpriteLibrary::NoGraphic),
                d_name(),
                d_baseColor(PALETTERGB(255,0,255)),
                d_bridge(),
                d_graphicOffsets()
        {
        }
#endif

        PathTable::BasicType d_type;
        LogicalTerrainEnum d_terrain;
        SpriteLibrary::Index d_graphic;
        InGameText::ID d_name;
        InGameText::ID d_nameAbrev;
        LogicalPathEnum d_bridge;
        const UWORD* d_graphicOffsets;
};

static UWORD bridgeOffsets[] = {
        0x05,0x09,0x0a,0x11,0x12,0x14,0x22,0x24,0x28,
        0
};

static PathInfoInitData pathInitData[] =
{
        // normal paths
        { PathTable::Road,  LT_Road,   Roads,  IDS_PATH1, IDS_PATH1_ABREV, LP_RoadBridge,  0 },
        { PathTable::Road,  LT_Track,  Tracks, IDS_PATH2, IDS_PATH2_ABREV, LP_TrackBridge, 0 },
        { PathTable::River, LT_River,  Rivers, IDS_PATH3, IDS_PATH3_ABREV, LP_None,        0 },
        { PathTable::River, LT_Stream, Streams,IDS_PATH4, IDS_PATH4_ABREV, LP_None,        0 },

        // sunken variants
        { PathTable::Road,  LT_SunkenRoad,   SunkenRoads,   IDS_PATH5, IDS_PATH5_ABREV, LP_RoadBridge,  0 },
        { PathTable::Road,  LT_SunkenTrack,  SunkenTracks,  IDS_PATH6, IDS_PATH6_ABREV, LP_TrackBridge, 0 },
        { PathTable::River, LT_SunkenRiver,  SunkenRivers,  IDS_PATH7, IDS_PATH7_ABREV, LP_None,        0 },
        { PathTable::River, LT_SunkenStream, SunkenStreams, IDS_PATH8, IDS_PATH8_ABREV, LP_None,        0 },

        // bridge paths
        { PathTable::Bridge, LT_RoadBridge,  0, IDS_PATH9,  IDS_PATH9_ABREV,  LP_None, bridgeOffsets },
        { PathTable::Bridge, LT_TrackBridge, 0, IDS_PATH10, IDS_PATH10_ABREV, LP_None, bridgeOffsets },

        // End of Table
        { PathTable::NoType, LT_None, 0, 0, 0, LP_None, 0 },
};

#endif

void PathTable::init()
{
        int i = First;

        for(const PathInfoInitData* initData = pathInitData;
                initData->d_terrain != LT_None;
                ++initData, ++i)
        {
                ASSERT(initData->d_terrain != LT_None);
                ASSERT(initData->d_terrain < LT_HowMany);
                ASSERT(initData->d_name != 0);


                PathTable::Info info;
                info.basicType(initData->d_type);
                info.terrain(initData->d_terrain);
                info.graphic(initData->d_graphic);
                info.name(InGameText::get(initData->d_name));
                info.nameAbrev(InGameText::get(initData->d_nameAbrev));
//ColorRef                 info.color(initData->d_baseColor);

                info.bridge(initData->d_bridge);
#if 0
                info.bridge(i);

                if(initData->d_bridge != 0)
                {
                        int i1 = First;
                        for(const PathInfoInitData* iData = pathInitData;
                                iData->d_terrain != LT_None;
                                ++iData, ++i1)
                        {
                                if(strcmp(iData->d_name, initData->d_bridge) == 0)
                                {
                                        info.bridge(i1);
                                        break;
                                }
                        }
                        ASSERT(iData->d_terrain != LT_None);
                }
#endif

                if(initData->d_graphicOffsets != 0)
                {
                        UWORD* offsets = new UWORD[63];

                        ASSERT(offsets != 0);
                        if(offsets == 0)
                                throw Error();

                        int i;
                        for(i = 0; i < 63; ++i)
                        {
                                offsets[i] = 0;
                        }

                        for(i = 0; initData->d_graphicOffsets[i] != 0; ++i)
                        {
                                ASSERT(initData->d_graphicOffsets[i] != 0);
                                ASSERT(initData->d_graphicOffsets[i] <= 0x3f);

                                offsets[initData->d_graphicOffsets[i] - 1] = i;

                        }

                        ASSERT((initData->d_graphic + i) <= TerrainSpriteIndex_HowMany);

                        info.graphicOffsets(offsets);
                }
                else
                {
                        ASSERT( (initData->d_graphic + 63) < TerrainSpriteIndex_HowMany);
                        info.graphicOffsets(0);
                }

                d_items.push_back(info);
        }
}


/*------------------------------------------------
 * Edge Table::Info
 */

EdgeTable::Info::Info()
{
}

EdgeTable::Info::~Info()
{
}

/*------------------------------------------------
 * Edge Table
 */

EdgeTable::EdgeTable()
{
}

EdgeTable::~EdgeTable()
{
}

const EdgeTable::Info& EdgeTable::operator [] (Index t) const
{
        ASSERT(t != None);
        ASSERT(t <= MaxIndex);
        ASSERT(t >= First);
        ASSERT( (t - First) < d_items.size() );

        if( (t == None) || (t > MaxIndex) )
                throw Error();

        return d_items[t - 1];
}

EdgeTable::Info& EdgeTable::operator [] (Index t)
{
        ASSERT(t != None);
        ASSERT(t <= MaxIndex);
        ASSERT(t >= First);
        ASSERT( (t - First) < d_items.size() );

        if( (t == None) || (t > MaxIndex) )
                throw Error();

        return d_items[t - 1];
}

#ifdef STATIC_DATA_TABLES

struct EdgeInfoInitData
{
#if 0
        EdgeInfoInitData(SpriteLibrary::Index graphic, const char* name) :
                d_graphic(graphic),
                d_name(name)
        {
                ASSERT( (graphic + 63) < TerrainSpriteIndex_HowMany);
        }

        EdgeInfoInitData() :
                d_graphic(SpriteLibrary::NoGraphic),
                d_name()
        {
        }
#endif
        SpriteLibrary::Index d_graphic;
//        const char* d_name;
        InGameText::ID d_name;
};


static EdgeInfoInitData edgeInitData[] =
{
#if 0
        EdgeInfoInitData(Hedge, "Hedge"),
        EdgeInfoInitData(Fence, "Fence"),
        EdgeInfoInitData(Wall, "Stone Wall"),

        EdgeInfoInitData()
#endif

   { Hedge, IDS_EDGE_1 },
   { Hedge, IDS_EDGE_2 },
   { Hedge, IDS_EDGE_3 },
   { SpriteLibrary::NoGraphic, InGameText::Null }

};

#endif  // STATIC_DATA_TABLES


void EdgeTable::init()
{
        for(const EdgeInfoInitData* initData = edgeInitData;
                initData->d_graphic != SpriteLibrary::NoGraphic;
                ++initData)
        {
                ASSERT( (initData->d_graphic + 63) < TerrainSpriteIndex_HowMany);
                ASSERT(initData->d_name != 0);

                EdgeTable::Info info;
                info.graphic(initData->d_graphic);
                info.name(InGameText::get(initData->d_name));
                d_items.push_back(info);
        }
}


/*
 * Logical Terrain Table functions
 */

const LTerrainTable::Info& LTerrainTable::get(Index t) const
{
        ASSERT(t != None);
        ASSERT(t <= MaxIndex);
        ASSERT(t >= First);
        ASSERT( (t - First) < d_items.size() );

        if( (t == None) || (t > MaxIndex) )
                throw Error();

        return d_items[t - 1];
}


void LTerrainTable::init()
{
        d_items.erase(d_items.begin(), d_items.end());
        d_items.reserve(LT_HowMany);

        for(int i = 0; i < LT_HowMany; ++i)
        {
                d_items.push_back(logicalTerrainInitData[i]);
        }
}

/*
 * Main Terrain Table functions
 */

TerrainTable::TerrainTable()
{
}

TerrainTable::~TerrainTable()
{
}


const TerrainTable::Info& TerrainTable::get(Index t) const
{
        ASSERT(t != None);
        ASSERT(t <= MaxIndex);
        ASSERT(t >= First);
        ASSERT( (t - First) < d_items.size() );

        if( (t == None) || (t > MaxIndex) )
                throw Error();

        return d_items[t - First];
}


TerrainTable::Info& TerrainTable::get(Index t){

        ASSERT(t != None);
        ASSERT(t <= MaxIndex);
        ASSERT(t >= First);
        ASSERT( (t - First) < d_items.size() );

        if( (t == None) || (t > MaxIndex) )
                throw Error();

        return d_items[t - First];
}



SpriteLibrary::Index TerrainTable::coastGraphic(EdgeMap edge) const
{
        ASSERT( (edge > 0) && (edge <= 63) );

        if( (edge > 0) && (edge <= 63) )
                return d_coastGraphic + edge - 1;
        else
                return SpriteLibrary::NoGraphic;
}

SpriteLibrary::Index TerrainTable::estuaryGraphic(BattleMeasure::HexCord::HexDirection d) const
{
        if( (d >= BattleMeasure::HexCord::First) && (d < BattleMeasure::HexCord::End) )
                return d_estuaryGraphic + (d - BattleMeasure::HexCord::First);
        else
                return SpriteLibrary::NoGraphic;
}


#ifdef STATIC_DATA_TABLES


/*
 * Base Terrain Initialisation
 */

struct TerrainInfoInitData
{
#if 0
        TerrainInfoInitData(
                        LogicalTerrainEnum logical,
                        SpriteLibrary::Index baseGraphic,
                        const char* name,
                        COLORREF color) :
                d_logical(logical),
                d_baseGraphic(baseGraphic),
                d_name(name),
                d_baseColor(color)
        {
                ASSERT(baseGraphic < TerrainSpriteIndex_HowMany);

//                const SWG_Sprite::SpriteBlock * sprBlock;
//                SWG_Sprite::SpriteBlockPtr sprite(sprBlock);
        }

        /*
         * End of Table Constructor
         */

        TerrainInfoInitData() :
                d_logical(LT_None),
                d_baseGraphic(SpriteLibrary::NoGraphic),
                d_name(),
                d_baseColor(PALETTERGB(255,0,255)),
                d_isWater(false)
        {
        }
#endif


        LogicalTerrainEnum    d_logical;
        SpriteLibrary::Index  d_baseGraphic;
        InGameText::ID        d_name;
        // const char* d_name;
        // ColourIndex d_baseColor;
//        COLORREF d_baseColor;
//        bool d_isWater;
};

static const TerrainInfoInitData terrainInitData[] =
{
#if 0
        TerrainInfoInitData(
                LT_Clear,
                Grass,
                "Grass",
                PALETTERGB(41,24,66)),
                // false),
        TerrainInfoInitData(
                LT_Water,
                Water,
                "Water",
                PALETTERGB(80,80,180)),
                // true),
        TerrainInfoInitData(
                LT_Orchard,
                Orchard,
                "Orchard",
                PALETTERGB(255,0,255)),
                // false),
        TerrainInfoInitData(
                LT_Orchard,
                Vineyard,
                "Vineyard",
                PALETTERGB(255,0,255)),
                // false),
        TerrainInfoInitData(
                LT_LightWood,
                LightWood,
                "Light Wood",
                PALETTERGB(33,33,8)),
                // false),
        TerrainInfoInitData(
                LT_DenseWood,
                DenseWood,
                "Dense Wood",
                PALETTERGB(33,57,24)),
                // false),
        TerrainInfoInitData(
                LT_PloughedField,
                PloughedField,
                "Ploughed Field",
                PALETTERGB(107,82,66)),
                // false),
        TerrainInfoInitData(                                            // NB I have left Corn and Wheat in
                LT_CornField,
                CornField,
                "Corn Field",
                PALETTERGB(115,99,24)),
                // false),
        TerrainInfoInitData(
                LT_WheatField,
                WheatField,
                "Wheat Field",
                PALETTERGB(140,99,24)),
                // false),
        TerrainInfoInitData(
                LT_Rough,
                Rough,
                "Rough",
                PALETTERGB(99,82,57)),
                // false),
        TerrainInfoInitData(
                LT_Marsh,
                Marsh,
                "Marsh",
                PALETTERGB(255,0,255)),
                // false),
        TerrainInfoInitData(
                LT_Town,
                Town,
                "Town",
                PALETTERGB(255,0,255)),
                // false),
        TerrainInfoInitData(
                LT_Village,
                Village,
                "Village",
                PALETTERGB(255,0,255)),
                // false),
        TerrainInfoInitData(
                LT_Farm,
                Farm,
                "Farm",
                PALETTERGB(255,0,255)),
                // false),
        TerrainInfoInitData(
                LT_Cemetary,
                Cemetary,
                "Cemetary",
                PALETTERGB(255,0,255)),
                // false),


        TerrainInfoInitData()           // End of Table
#else
        { LT_Clear,           Grass,         IDS_TERRAIN_1  },
        { LT_Water,           Water,         IDS_TERRAIN_2  },
        { LT_Orchard,         Orchard,       IDS_TERRAIN_3  },
        { LT_Orchard,         Vineyard,      IDS_TERRAIN_4  },
        { LT_LightWood,       LightWood,     IDS_TERRAIN_5  },
        { LT_DenseWood,       DenseWood,     IDS_TERRAIN_6  },
        { LT_PloughedField,   PloughedField, IDS_TERRAIN_7  },
        { LT_CornField,       CornField,     IDS_TERRAIN_8  },
        { LT_WheatField,      WheatField,    IDS_TERRAIN_9  },
        { LT_Rough,           Rough,         IDS_TERRAIN_10 },
        { LT_Marsh,           Marsh,         IDS_TERRAIN_11 },
        { LT_Town,            Town,          IDS_TERRAIN_12 },
        { LT_Village,         Village,       IDS_TERRAIN_13 },
        { LT_Farm,            Farm,          IDS_TERRAIN_14 },
        { LT_Cemetary,        Cemetary,      IDS_TERRAIN_15 },
        { LT_None, SpriteLibrary::NoGraphic, InGameText::Null }
#endif
};


#endif

/*
 * Read Terrain Tables from scenario specific data tables
 *
 * For now, they are hard coded as static data tables
 *
 * A global palette must already be initialised
 */

void TerrainTable::init()
{
        // Read Logical Terrain Table

        d_logical.init();

        // Read Path Table

        d_pathTable.init();

        // Read Edge Table

        d_edgeTable.init();

        // Read Main Table

        ASSERT(d_items.empty());

        for(const TerrainInfoInitData* initData = terrainInitData;
                initData->d_logical != LT_None;
                ++initData)
        {
                ASSERT(initData->d_logical != LT_None);
                ASSERT(initData->d_logical < LT_HowMany);
                ASSERT(initData->d_baseGraphic < TerrainSpriteIndex_HowMany);
                ASSERT(initData->d_name != 0);

                Info item;
                item.logical(initData->d_logical);
                item.baseGraphic(initData->d_baseGraphic);
                item.name(InGameText::get(initData->d_name));
                d_items.push_back(item);
        }

        d_coastGraphic = Coasts;
//        ASSERT(Coasts_Count == 63);

        d_estuaryGraphic = Estuaries;
//        ASSERT(Estuaries_Count == (BattleMeasure::HexCord::End - BattleMeasure::HexCord::First));
}

const char* TerrainTable::pathName(PathTable::Index i) const
{
        if(i == PathTable::None)
                return "";    // "No Path";
        else
                return d_pathTable[i].name();
}

const char* TerrainTable::pathNameAbrev(PathTable::Index i) const
{
   if(i == PathTable::None)
          return "";
   else
          return d_pathTable[i].nameAbrev();
}

const char* TerrainTable::edgeName(PathTable::Index i) const
{
        if(i == EdgeTable::None)
                return "";    // "No Path";
        else
                return d_edgeTable[i].name();
}

// };   // namespace WG_BattleData


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
