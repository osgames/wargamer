/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Coordinate system
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "batcord.hpp"
#include "misc.hpp"
#include "filebase.hpp"
#if !defined(NOLOG)
#include "clog.hpp"
#endif
#include <iostream>

#ifdef DEBUG
#include "options.hpp"
#endif
namespace BattleMeasure
{



/*
 * File Interface for HexCord
 */

UWORD HexCord::s_fileVersion = 0x0000;

Boolean HexCord::readData(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  Cord x;
  f >> x;
  d_pt.x(x);

  Cord y;
  f >> y;
  d_pt.y(y);

  return f.isOK();
}

Boolean HexCord::writeData(FileWriter& f) const
{
  f << s_fileVersion;

  f << d_pt.x();
  f << d_pt.y();

  return f.isOK();
}

/*
 * File Interface for HexPosition
 */

UWORD HexPosition::s_fileVersion = 0x0001; // changed from 0 to 1

Boolean HexPosition::readData(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  f >> d_dir;                   // Direction in 256ths of a circle
  f >> d_lastDir;               // Last Direction we were traveling
  f >> d_dist;                  // Distance from centre

  if(version >= 0x0001)
  {
         UBYTE b;
         f >> b;
         d_moveTo = static_cast<MoveTo>(b);
         f >> b;
         d_moveFrom = static_cast<MoveTo>(b);
  }
  return f.isOK();
}

Boolean HexPosition::writeData(FileWriter& f) const
{
  f << s_fileVersion;

  f << d_dir;
  f << d_lastDir;
  f << d_dist;
  UBYTE b = d_moveTo;
  f << b;
  b = d_moveFrom;
  f << b;

  return f.isOK();
}


HexPosition::WhereAt HexPosition::moveAlong(Distance d)
{
  if(d_dist < s_centerPos)
  {
         d_dist = (d_dist + d >= s_centerPos) ?  static_cast<HexDistance>(s_centerPos) :
                static_cast<HexDistance>(d + d_dist);

         return (d_dist == s_centerPos) ? CenterHex : MovingToCenter;
  }

  else
  {
         d_dist = (d_dist + d < s_edgePos) ? static_cast<HexDistance>(d_dist + d) : 0;
         return (d_dist == 0) ? EdgeOfHex : MovingToEdge;
  }
}

// initialize starting positions. used only at start of new game
void HexPosition::init(Facing f)
{
  d_dir = d_lastDir = moveRight(oppositeFace(f));
}

/*
 * Return Hex coordinate when moving in a given hex direction.
 *
 * Directions are using cartesian style coordinates
 * 0,0 is in bottom left
 * 0,1 is above and to the right of 0,0
 */

bool HexCord::move(HexDirection dir, HexCord& destHex, const HexCord& maxHex) const
{
        static SBYTE xOffset[12] = { +1,+1, 0,+1, -1,0, -1,-1, -1,0, 0,+1 };
        static SBYTE yOffset[6] = { 0, +1, +1, 0, -1, -1 };

        int xOff = (dir * 2) + (y() & 1);

        int newX = x() + xOffset[xOff];
        int newY = y() + yOffset[dir];

        if( (newX >= 0)
          && (newX < maxHex.x())
          && (newY >= 0)
          && (newY < maxHex.y()) )
        {
                        //destHex = HexCord(newX, newY);
                        destHex.x(newX);
                destHex.y(newY);
                return true;
        }

        return false;
}

HexPosition::wangle(Facing dir)
{
        return bangleToWangle(dir);
}

inline int squared(int v)
{
  return (v * v);
}

inline double getDist(const HexCord& srcHex, const HexCord& destHex)
{
  return sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y()));
}

HexCord::HexDirection hexDirection(const HexCord& srcHex, const HexCord& destHex)
{
#if 0
  // get distances
  SWORD x = destHex.x() - srcHex.x();
  SWORD y = destHex.y() - srcHex.y();

  // get byte angle
  Bangle bFace = wangleToBangle(direction(x, y));

  if(bFace >= HexPosition::SouthEastPoint || bFace < HexPosition::NorthEastPoint)
         return HexCord::Right;
   else if(bFace < HexPosition::North)
         return HexCord::UpRight;
  else if(bFace < HexPosition::NorthWestPoint)
         return HexCord::UpLeft;
  else if(bFace < HexPosition::SouthWestPoint)
         return HexCord::Left;
  else if(bFace < HexPosition::South)
         return HexCord::DownLeft;
  else
         return HexCord::DownRight;
#endif
  return HexCord::Stationary;
}

const HexPosition::HexDistance HexPosition::s_centerPos = HalfXHex;  // yards(110);
const HexPosition::HexDistance HexPosition::s_edgePos = OneXHex;     // yards(220);

HexPosition::Facing leftTurn(HexPosition::Facing f)
{
  ASSERT(f == HexPosition::NorthEastPoint || f == HexPosition::North ||
         f == HexPosition::NorthWestPoint || f == HexPosition::SouthWestPoint ||
         f == HexPosition::South || f == HexPosition::SouthEastPoint);

  const int addTo = (f == HexPosition::North || f == HexPosition::South) ?
         (2 * HexPosition::FacingResolution) / 12 : ((2 * HexPosition::FacingResolution) / 12) + 1;

   // wrap around
  return ((f + addTo) < HexPosition::FacingResolution) ?
         f + addTo : (f + addTo) - HexPosition::FacingResolution;
}

HexPosition::Facing rightTurn(HexPosition::Facing f)
{
  ASSERT(f == HexPosition::NorthEastPoint || f == HexPosition::North ||
         f == HexPosition::NorthWestPoint || f == HexPosition::SouthWestPoint ||
         f == HexPosition::South || f == HexPosition::SouthEastPoint);

  const int addTo = (f == HexPosition::NorthWestPoint || f == HexPosition::SouthEastPoint) ?
         (2 * HexPosition::FacingResolution) / 12 : ((2 * HexPosition::FacingResolution) / 12) + 1;

  // wrap around
  return ((f - addTo) <= 0) ? f - addTo : (f - addTo) + HexPosition::FacingResolution;
}

HexPosition::Facing moveRight(HexPosition::Facing f)
{
  ASSERT(f == HexPosition::NorthEastPoint || f == HexPosition::North ||
         f == HexPosition::NorthWestPoint || f == HexPosition::SouthWestPoint ||
         f == HexPosition::South || f == HexPosition::SouthEastPoint ||
         f == HexPosition::NorthEastFace || f == HexPosition::NorthWestFace ||
         f == HexPosition::West || f == HexPosition::SouthWestFace ||
             f == HexPosition::SouthEastFace || f == HexPosition::East);

  const int addTo = (f == HexPosition::East || f == HexPosition::South || f == HexPosition::West || f == HexPosition::North) ?
         (1 * HexPosition::FacingResolution) / 12 + 1 : ((1 * HexPosition::FacingResolution) / 12);

  // wrap around
#ifdef DEBUG
  HexPosition::Facing newFace = ((f - addTo) <= 0) ? f - addTo : (f - addTo) + HexPosition::FacingResolution;
  ASSERT(newFace == HexPosition::NorthEastPoint || newFace == HexPosition::North ||
         newFace == HexPosition::NorthWestPoint || newFace == HexPosition::SouthWestPoint ||
         newFace == HexPosition::South || newFace == HexPosition::SouthEastPoint ||
         newFace == HexPosition::NorthEastFace || newFace == HexPosition::NorthWestFace ||
         newFace == HexPosition::West || newFace == HexPosition::SouthWestFace ||
         newFace == HexPosition::SouthEastFace || newFace == HexPosition::East);
  return newFace;
#else
  return ((f - addTo) <= 0) ? f - addTo : (f - addTo) + HexPosition::FacingResolution;
#endif
}

HexPosition::Facing moveLeft(HexPosition::Facing f)
{
  ASSERT(f == HexPosition::NorthEastPoint || f == HexPosition::North ||
         f == HexPosition::NorthWestPoint || f == HexPosition::SouthWestPoint ||
         f == HexPosition::South || f == HexPosition::SouthEastPoint ||
         f == HexPosition::NorthEastFace || f == HexPosition::NorthWestFace ||
         f == HexPosition::West || f == HexPosition::SouthWestFace ||
             f == HexPosition::SouthEastFace || f == HexPosition::East);

  const int addTo = (f == HexPosition::NorthEastFace || f == HexPosition::NorthWestPoint ||
       f == HexPosition::SouthWestFace || f == HexPosition::SouthEastPoint) ?
         (1 * HexPosition::FacingResolution) / 12 + 1 : ((1 * HexPosition::FacingResolution) / 12);

  // wrap around
#ifdef DEBUG
  HexPosition::Facing newFace = ((f + addTo) < HexPosition::FacingResolution) ?
         f + addTo : (f + addTo) - HexPosition::FacingResolution;

  ASSERT(newFace == HexPosition::NorthEastPoint || newFace == HexPosition::North ||
         newFace == HexPosition::NorthWestPoint || newFace == HexPosition::SouthWestPoint ||
         newFace == HexPosition::South || newFace == HexPosition::SouthEastPoint ||
         newFace == HexPosition::NorthEastFace || newFace == HexPosition::NorthWestFace ||
             newFace == HexPosition::West || newFace == HexPosition::SouthWestFace ||
             newFace == HexPosition::SouthEastFace || newFace == HexPosition::East);
   return newFace;
#else
   return ((f + addTo) < HexPosition::FacingResolution) ?
             f + addTo : (f + addTo) - HexPosition::FacingResolution;
#endif
}

HexPosition::Facing moveDirection(const HexCord& srcHex, const HexCord& destHex)
{
   int xDif = (destHex.x() - srcHex.x()) + 1;
   int yDif = (destHex.y() - srcHex.y()) + 1;
   ASSERT(xDif >= 0 && xDif < 3);
   ASSERT(yDif >= 0 && yDif < 3);

   if(xDif < 0 || xDif >= 3 || yDif < 0 || yDif >= 3)
   {
      return HexPosition::Facing_Undefined;
   }

   if(srcHex.y() % 2 == 0)
   {
      static const HexPosition::Facing s_table[3][3] = {
         //       -1,                      0,                               1
         { HexPosition::SouthWestFace,    HexPosition::West,             HexPosition::NorthWestFace    }, //-1
         { HexPosition::SouthEastFace,    HexPosition::Facing_Undefined, HexPosition::NorthEastFace    }, // 0
         { HexPosition::Facing_Undefined, HexPosition::East,             HexPosition::Facing_Undefined }  // 1
      };

#ifdef DEBUG
      HexPosition::Facing f = s_table[xDif][yDif];
      ASSERT(f != HexPosition::Facing_Undefined);
      return f;
#else
      return s_table[xDif][yDif];
#endif
   }
   else
   {
      static const HexPosition::Facing s_table[3][3] = {
         //       -1,                      0,                               1
         { HexPosition::Facing_Undefined, HexPosition::West,             HexPosition::Facing_Undefined }, //-1
         { HexPosition::SouthWestFace,    HexPosition::Facing_Undefined, HexPosition::NorthWestFace    }, // 0
         { HexPosition::SouthEastFace,    HexPosition::East,             HexPosition::NorthEastFace }  // 1
      };

#ifdef DEBUG
      HexPosition::Facing f = s_table[xDif][yDif];
      ASSERT(f != HexPosition::Facing_Undefined);
      return f;
#else
      return s_table[xDif][yDif];
#endif
   }
}

HexPosition::Facing nextFacing(HexPosition::Facing cf, HexPosition::Facing wf)
{
  ASSERT(wf != cf);

  int loop = 0;

  HexPosition::Facing f = cf;
  while(f != wf)
  {
         f = moveRight(f);
         loop++;

#ifdef DEBUG
         if(loop > 11)
         {
                FORCEASSERT("Infinite loop in nextFacing()");
                break;
         }
#else
         if(loop > 6)
                break;
#endif

  }

  return (loop <= 6)  ? moveRight(cf) : moveLeft(cf);
}

HexPosition::Facing oppositeFace(HexPosition::Facing f)
{
  const int addTo = HexPosition::FacingResolution / 2;
  return (addTo > f) ? f + addTo : f - addTo;
}


/*
 * File Interface for BattlePosition
 */

UWORD BattlePosition::s_fileVersion = 0x0000;

Boolean BattlePosition::readData(FileReader& f)
{
  UWORD version;
  f >> version;
  ASSERT(version <= s_fileVersion);

  if(!d_hexCord.readData(f))
         return False;

  if(!d_hexPosition.readData(f))
         return False;

  return True;
}

Boolean BattlePosition::writeData(FileWriter& f) const
{
  f << s_fileVersion;

  if(!d_hexCord.writeData(f))
         return False;

  if(!d_hexPosition.writeData(f))
         return False;

  return True;
}
//------------------------------------------------

};      // namespace BattleMeasure


/*
 * Must be defined outside namespace or else compiler doesn't seem
 * to consider it for overloading.
 */

#if !defined(NOLOG)

BATDATA_DLL LogFile& operator << (LogFile& file, const BattleMeasure::HexCord& hex)
{
        file << '[' << static_cast<int>(hex.x()) << ',' << static_cast<int>(hex.y()) << ']';
        return file;
}

// template<class T>
BATDATA_DLL LogFile& operator << (LogFile& file, const Point<BattleMeasure::Distance>& p)
{
        file << '[' << p.x() << ',' << p.y() << ']';
        return file;
}

#endif


std::ostream& operator << (std::ostream& file, const BattleMeasure::HexCord& hex)
{
        file << '[' << static_cast<int>(hex.x()) << ',' << static_cast<int>(hex.y()) << ']';
        return file;
}

// template<class T>
std::ostream& operator << (std::ostream& file, const Point<BattleMeasure::Distance>& p)
{
        file << '[' << p.x() << ',' << p.y() << ']';
        return file;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
