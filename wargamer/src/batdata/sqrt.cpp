/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/* fsqrt.cpp
 *
 * A fast square root program adapted from the code of
 * Paul Lalonde and Robert Dawson in Graphics Gems I.
 * The format of IEEE double precision floating point numbers is:
 *
 * SEEEEEEEEEEEMMMM MMMMMMMMMMMMMMMM MMMMMMMMMMMMMMMM MMMMMMMMMMMMMMMM
 *
 * S = Sign bit for whole number
 * E = Exponent bit (exponent in excess 1023 form)
 * M = Mantissa bit
 */
#include "stdinc.hpp"
#include "sqrt.hpp"

int sqrt_tab[SQRT_TAB_SIZE];


void
init_sqrt_tab(void) {
int i;
double f;
unsigned int  *fi = (unsigned int *) &f + MOST_SIG_OFFSET;
        
for (i = 0; i < SQRT_TAB_SIZE/2; i++) {
    f = 0; /* Clears least sig part */
    *fi = (i << MANT_SHIFTS) | (EXP_BIAS << EXP_SHIFTS);
    f = sqrt(f);
    sqrt_tab[i] = *fi & MANT_MASK;

    f = 0; /* Clears least sig part */
    *fi = (i << MANT_SHIFTS) | ((EXP_BIAS + 1) << EXP_SHIFTS);
    f = sqrt(f);
    sqrt_tab[i + SQRT_TAB_SIZE/2] = *fi & MANT_MASK;
}
}


double
fsqrt(double f) {
unsigned int e;
unsigned int   *fi = (unsigned int *) &f + MOST_SIG_OFFSET;

if (f == 0.0) return(0.0);

e = (*fi >> EXP_SHIFTS) - EXP_BIAS;
*fi &= MANT_MASK;

if (e & 1) *fi |= EXP_LSB;

e >>= 1;
*fi = (sqrt_tab[*fi >> MANT_SHIFTS]) |
((e + EXP_BIAS) << EXP_SHIFTS);
return(f);
}


void
dump_sqrt_tab(void) {
int        i, nl = 0;

printf("unsigned int sqrt_tab[] = {\n");
for (i = 0; i < SQRT_TAB_SIZE-1; i++) {
    printf("0x%x,", sqrt_tab[i]);
    nl++;
    if (nl > 8) { nl = 0; putchar('\n'); }
}
printf("0x%x\n", sqrt_tab[SQRT_TAB_SIZE-1]);
printf("};\n");
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
