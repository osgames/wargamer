/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/

#include "stdinc.hpp"
#include "initunit.hpp"
#include "myassert.hpp"
#include "batdata.hpp"
#include "bobiter.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : INITUNIT
        Description : Utilities to pass through Battle OB & initialise data
        
-------------------------------------------------------------------------------------------------------------------*/

namespace BatUnitUtils {




void
InitUnits(BattleData * batdata) {

    unsigned int nSprites;

         // get OB
         BattleOB * ob = batdata->ob();

         // go through all CPs in battle
         BattleUnitIter CPiter(ob);

         while(!CPiter.isFinished() ) {

                  // get the current cp
                  RefBattleCP cp = CPiter.cp();


            switch(cp->getSide() ) {

                  /*
                  French
                  */
                  case 0 : {

                     // Napoleon
                     if(cp->leader()->isSupremeLeader()) {
                        nSprites = 4;
                        break;
                     }
                     // Marshall (
                     else if(cp->getRank().isHigher(Rank_Corps)) {
                        nSprites = 3;
                        break;
                     }
                     // Cavalry
                     else if(cp->generic()->isCavalry()) {
                        nSprites = 2;
                        break;
                     }
                     // Infantry (type One or Two ?)
                     else {
                        nSprites = 2;
                        break;
                     }
                  }

                  /*
                  Allied
                  */
                  case 1 : {

                     // Army Group
                     if(cp->getRank().sameRank(Rank_ArmyGroup)) {
                        nSprites = 4;
                        break;
                     }
                     // Army
                     if(cp->getRank().sameRank(Rank_Army)) {
                        nSprites = 3;
                        break;
                     }
                     //Cavalry
                     else if(cp->generic()->isCavalry()) {
                        nSprites = 2;
                        break;
                     }
                     // Infantry (type One or Two ?)
                     else {
                        nSprites = 2;
                        break;
                     }
                  }

                  default : {
                     FORCEASSERT("ERROR - CP has invalid side (not 0 or 1)\n");
                     nSprites = 4;
                     break;
                  }

               }


                  // set number of CP sprites (always 4)
                  AnimationInfo & animinfo = cp->GetAnimInfo();
                  animinfo.LastNumSprites = nSprites;

                  for(std::vector<DeployItem>::iterator di = cp->mapBegin();
                                di != cp->mapEnd();
                                di++)
                  {
                         if(di->active())
                         {

                                const RefBattleSP& sp = di->d_sp;

                                // get basic type
                                const UnitTypeItem & unitType = ob->getUnitType(sp);
                                BasicUnitType::value basicType = unitType.getBasicType();

                                // get anim info
                                AnimationInfo & animinfo = sp->GetAnimInfo();

                                // get number of sprites to display in formation
                                nSprites = imagesPerSP[basicType];
                                // adjust by the strength of this SP
                                nSprites = (sp->strength() * nSprites + BattleSP::SPStrength::MaxValue - 1) / BattleSP::SPStrength::MaxValue;

                                // set anim info num sprites
                                animinfo.LastNumSprites = nSprites;
                         }
//                              SPiter.next();

                  } // repeat for all SPs in chain

                  CPiter.next();

         } // repeat for all CPs in OB

}







/*
Simply go through OB setting number of sprites
*/

void
InitUnits(BattleOB * ob) {

    unsigned int nSprites;

    // go through all CPs in battle
    BattleCP * cp;

    for(Side side=0; side<2; side++) {

        cp = ob->getTop(side);
        while(cp != NoBattleCP) {

            // set number of CP sprites (always 4)
            AnimationInfo & animinfo = cp->GetAnimInfo();
            nSprites = 4;
            animinfo.LastNumSprites = nSprites;

            if(cp->child() ) InitCommandPositions(cp->child(), ob);

            cp = cp->sister();
        }
    }
}
        





void
InitCommandPositions(BattleCP * cp, BattleOB * ob) {

    // set number of CP sprites (always 4)
    AnimationInfo & animinfo = cp->GetAnimInfo();
    int nSprites = 4;
    animinfo.LastNumSprites = nSprites;

    if(cp->sp() ) InitStrengthPoints(cp,ob);

    if(cp->child() ) InitCommandPositions(cp->child(), ob);

    if(cp->sister() ) InitCommandPositions(cp->sister(), ob);

}






void
InitStrengthPoints(BattleCP * cp, BattleOB * ob) {

    BattleSP * sp = cp->sp();

    while(sp) {
        // get basic type
        const UnitTypeItem & unitType = ob->getUnitType(sp);
        BasicUnitType::value basicType = unitType.getBasicType();

        // get anim info
        AnimationInfo & animinfo = sp->GetAnimInfo();

        // get number of sprites to display in formation
        int nSprites = imagesPerSP[basicType];
        // adjust by the strength of this SP
        nSprites = (sp->strength() * nSprites + BattleSP::SPStrength::MaxValue - 1) / BattleSP::SPStrength::MaxValue;

        // set anim info num sprites
        animinfo.LastNumSprites = nSprites;

        sp = sp->sister();
    }
    
}


}; // end of namespace BatUnitUtils

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
