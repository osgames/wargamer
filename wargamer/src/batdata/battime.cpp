/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Time Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "battime.hpp"
#include "filebase.hpp"
#include "myassert.hpp"

using Greenius_System::Date;
using Greenius_System::Time;

UBYTE BattleMeasure::BattleTime::TicksPerSecond = 10;

BattleMeasure::BattleTime::BattleTime(const Date& d, const Time& t) :
        d_date(d)
{
        d_ticks = (t.ticks() * TicksPerSecond) / Time::TicksPerSecond;
}

BattleMeasure::BattleTime& BattleMeasure::BattleTime::operator+=(Tick ticks)
{
        const Tick TicksPerDay = hours(24);

        d_ticks += ticks;
        while(d_ticks >= TicksPerDay)
        {
                d_ticks -= TicksPerDay;
                ++d_date;
        }

        return *this;
}

void BattleMeasure::BattleTime::startNewDay()
{
   if(d_ticks >= EndOfDay())
   {
      ++d_date;
      d_ticks = StartOfDay();
   }
   else if(d_ticks < StartOfDay())
   {
      d_ticks = StartOfDay();
   }
}


Greenius_System::Time BattleMeasure::BattleTime::time() const
{
        Greenius_System::Time::Ticks sysTicks = (d_ticks * Time::TicksPerSecond) / TicksPerSecond;
        return Greenius_System::Time(sysTicks);
}

Greenius_System::Date BattleMeasure::BattleTime::date() const
{
        return d_date;
}

void BattleMeasure::BattleTime::date(Greenius_System::Date date) {

    d_date = date;
}

void BattleMeasure::BattleTime::time(Greenius_System::Time time) {

    d_ticks = battleTime(time.hours(), time.minutes(), time.seconds() );
}



UWORD BattleMeasure::BattleTime::s_fileVersion = 0x0001;

bool BattleMeasure::BattleTime::readData(FileReader& f)
{
        UWORD version;
        f >> version;
        ASSERT(version <= s_fileVersion);
        f >> d_ticks;

        UBYTE month;
        int day;
        int year;

        f >> day;
        f >> month;
        f >> year;

        if(version < 0x0001)
            --month;

        d_date = Greenius_System::Date(
            static_cast<MonthEnum>(month),
            day,
            year);

        return f.isOK();
}

bool BattleMeasure::BattleTime::writeData(FileWriter& f) const
{
        f << s_fileVersion;
        f << d_ticks;
        int day = d_date.day();
        UBYTE month = d_date.month();
        int year = d_date.year();
        f << day;
        f << month;
        f << year;
        return f.isOK();
}









// return by parameter
void
ticksToTime(BattleMeasure::BattleTime::Tick ticks, int * days, int * hours, int * mins, int * secs) {

   BattleMeasure::BattleTime::Tick tmp_ticks = ticks;

   *days = BattleMeasure::daysPerTick(tmp_ticks);
   tmp_ticks -= BattleMeasure::days(*days);

   *hours = BattleMeasure::hoursPerTick(tmp_ticks);
   tmp_ticks -= BattleMeasure::hours(*hours);

   *mins = BattleMeasure::minutesPerTick(tmp_ticks);
   tmp_ticks -= BattleMeasure::minutes(*mins);

   *secs = BattleMeasure::secondsPerTick(tmp_ticks);
   tmp_ticks -= BattleMeasure::seconds(*secs);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
