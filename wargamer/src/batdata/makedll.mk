##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

###########################################
# Battle Datae Control Library Makefile
###########################################

NAME = campGame
lnk_dependencies += makedll.mk

!include ..\wgpaths.mif

CFLAGS += -DEXPORT_BATDATA_DLL

OBJS =  batdata.obj     &
        battime.obj     &
        btimctrl.obj    &
        building.obj    &
        batcord.obj     &
        b_terr.obj      &
        hexdata.obj     &
        bcreate.obj     &
        batarmy.obj     &
        bob_cp.obj      &
        bob_sp.obj      &
        batord.obj      &
        bobiter.obj     &
        bobutil.obj     &
        hexmap.obj      &
        b_ob_ran.obj    &
        maketer.obj     &
        fractmap.obj    &
        pathfind.obj    &
        sqrt.obj        &
	batsound.obj    &
	b_sounds.obj    &
	batunit.obj     &
	hexlist.obj  	&
	batlist.obj  	&
	bordutil.obj	&
	batmsg.obj	&
	initunit.obj	&
	bshockl.obj	&
	b_morale.obj &
	breinfor.obj


!ifndef NODEBUG
OBJS += boblog.obj
!endif


CFLAGS += -i=$(ROOT)\system
CFLAGS += -i=$(ROOT)\campdata
CFLAGS += -i=$(ROOT)\campwind
CFLAGS += -i=$(ROOT)\camp_ai
CFLAGS += -i=$(ROOT)\gamesup
CFLAGS += -i=$(ROOT)\ob
CFLAGS += -i=$(ROOT)\mapwind
CFLAGS += -i=$(ROOT)\res

SYSLIBS += COMCTL32.LIB VFW32.LIB DSOUND.LIB
# SYSLIBS += USER32.LIB KERNEL32.LIB GDI32.LIB WINMM.LIB SHELL32.LIB

LIBS += system.lib
LIBS += gamesup.lib
LIBS += ob.lib

TARGETS += $(LIBNAME)

!include ..\dll95.mif

.before:
        @echo Making $(TARGETS)

$(LIBNAME) : $(TARGETNAME)
        wlib -n -b $(LIBNAME) $(TARGETNAME)

