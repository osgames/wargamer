/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATORD_HPP
#define BATORD_HPP

#ifndef __cplusplus
#error batord.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Orders
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "bobdef.hpp"
#include "batcord.hpp"
#include "unittype.hpp"
#include "battime.hpp"
#include "hexlist.hpp"
#include "sllist.hpp"
#include <queue>         // STL: Priority Queue
#include <vector>        // STL: Vector
//#include "batunit.hpp"

// using namespace BOB_Definitions;
// using namespace BattleMeasure;


class FileReader;
class FileWriter;
class OrderBattle;

// what in what order do we exucute an order
struct WhatOrder : public SLink {
  static const UWORD s_fileVersion;

  enum What {
    SPFormation,
    CPFormation,
    CPLineHow,
    Manuever,
    Facing,

    HowMany,
    Undefined = HowMany,
    Default = CPFormation
  } d_whatOrder;

  WhatOrder() : d_whatOrder(Default) {}
  WhatOrder(What w) : d_whatOrder(w) {}
  WhatOrder(const WhatOrder& w) : d_whatOrder(w.d_whatOrder) {}

  BATDATA_DLL void* operator new(size_t size);
#ifdef _MSC_VER      // Visual C++ complains when using delete(void*, size_t)
  BATDATA_DLL void operator delete(void* deadObject);
#else
  BATDATA_DLL void operator delete(void* deadObject, size_t size);
#endif

  /*
   * File Interface
   */

  Boolean readData(FileReader& f);
  Boolean writeData(FileWriter& f) const;
};

class WhatOrderList : public SList<WhatOrder> {
   static const UWORD s_fileVersion;
public:
   WhatOrderList() {}

   WhatOrderList& operator = (const WhatOrderList& hl);

   bool inList(WhatOrder::What w);
   BATDATA_DLL void removeWhat(WhatOrder::What w);
   WhatOrder* add(WhatOrder::What w)
   {
     removeWhat(w);
     WhatOrder* wi = new WhatOrder(w);
     ASSERT(wi);
     append(wi);

     return wi;
   }

   WhatOrder* insert(WhatOrder::What w)
   {
     removeWhat(w);
     WhatOrder* wi = new WhatOrder(w);
     ASSERT(wi);
     SList<WhatOrder>::insert(wi);

     return wi;
   }

   /*
    * File Interface
    */

   Boolean readData(FileReader& f);
   Boolean writeData(FileWriter& f) const;

   BATDATA_DLL int pack(void * buffer, OrderBattle * ob);
   BATDATA_DLL int unpack(void * buffer, OrderBattle * ob);
};




struct BattleOrderInfo {
  static const UWORD s_fileVersion;

  // orders
  enum OrderMode {
    Hold,
    Move,
    RestRally,
    Attach,
    Detach,
//  AttachLeader,
//  DetachLeader,
    Bombard,
    Rout,

    OM_HowMany,

    OM_First = Hold,
    OM_Default = Hold
  };

  // Aggression. How unit is to react to nearby enemy
  enum Aggression {
    AvoidContact,      // Avoid all enemy contact
    ProbeDelay,        // Probing attack or defensive delaying action
    LimitedEngagement, // Limited Attack-Defense
    EngageAtAllCost,   // Attack / Defend at all cost

    Aggress_HowMany,
    Aggress_Default = LimitedEngagement,
    Aggress_First = AvoidContact
  };

  // Posture. Either offensive or defensive
  enum Posture {
    Defensive,
    Offensive,

    Posture_HowMany,
    Posture_Default = Defensive,
    Posture_First = Defensive
  };

  OrderMode           d_mode;
  Aggression          d_aggression;
  Posture             d_posture;
  BOB_Definitions::SPFormation         d_spFormation;
  BOB_Definitions::CPFormation         d_divFormation;
  BOB_Definitions::CPDeployHow         d_deployHow;
  BOB_Definitions::CPLineHow           d_lineHow;
  BOB_Definitions::CPManuever          d_manuever;
  BattleMeasure::HexPosition::Facing d_facing;
  BOB_Definitions::CPFaceHow           d_faceHow;

  WhatOrderList       d_whatOrder;

  BattleOrderInfo() :
    d_mode(OM_Default),
    d_aggression(Aggress_Default),
    d_posture(Posture_Default),
    d_spFormation(BOB_Definitions::SP_Default),
    d_divFormation(BOB_Definitions::CPF_Default),
    d_deployHow(BOB_Definitions::CPD_Default),
    d_lineHow(BOB_Definitions::CPL_Default),
    d_manuever(BOB_Definitions::CPM_Default),
    d_facing(BattleMeasure::HexPosition::Facing_Default),
    d_faceHow(BOB_Definitions::CPFaceHow_Default) {}

  BattleOrderInfo(const BattleOrderInfo& o) { copy(o); }

  BATDATA_DLL static const char* orderName(OrderMode om);
  BATDATA_DLL static const char* aggressionName(Aggression a);
  BATDATA_DLL static const char* postureName(Posture p);
  BATDATA_DLL static const char* spFormationName(BOB_Definitions::SPFormation f, bool arty = False);
  BATDATA_DLL static const char* divFormationName(BOB_Definitions::CPFormation f);
  BATDATA_DLL static const char* deployHowName(BOB_Definitions::CPDeployHow dh);
  BATDATA_DLL static const char* lineHowName(BOB_Definitions::CPLineHow lh);
  BATDATA_DLL static const char* manueverName(BOB_Definitions::CPManuever m);

  BATDATA_DLL void copy(const BattleOrderInfo& o);
  BattleOrderInfo& operator = (const BattleOrderInfo& o) { copy(o); return *this; }

  /*
   * File Interface
   */

  Boolean readData(FileReader& f);
  Boolean writeData(FileWriter& f) const;


  BATDATA_DLL int pack(void * buffer, OrderBattle * ob);
  BATDATA_DLL int unpack(void * buffer, OrderBattle * ob);
};

/*-------------------------------------------------------------------
 * WayPoint class,
 *
 *   Each Waypoint has a
 *   1. hex
 *   2. battle-order data for order to be executed upon arrival
 *      at the waypoint
 */

struct WayPoint : public SLink {
  static const UWORD s_fileVersion;

  BattleMeasure::HexCord d_hex;
  BattleOrderInfo d_order;

  WayPoint() : d_hex(), d_order() {}
  WayPoint(const BattleMeasure::HexCord& hex) : d_hex(hex), d_order() {}
  WayPoint(const BattleMeasure::HexCord& hex, const BattleOrderInfo& order) :
     d_hex(hex), d_order(order) {}

  BATDATA_DLL void* operator new(size_t size);
#ifdef _MSC_VER
  BATDATA_DLL void operator delete(void* deadObject);
#else
  BATDATA_DLL void operator delete(void* deadObject, size_t size);
#endif

  WayPoint(const WayPoint& wp) :
    d_hex(wp.d_hex),
    d_order(wp.d_order) {}

  WayPoint& operator = (const WayPoint& wp)
  {
    d_hex = wp.d_hex;
    d_order = wp.d_order;
    return *this;
  }

  /*
   * File Interface
   */

  Boolean readData(FileReader& f);
  Boolean writeData(FileWriter& f) const;
};

class WayPointList : public SList<WayPoint> {
    static const UWORD s_fileVersion;
  public:
    WayPointList& operator = (const WayPointList& wl);

    /*
     * File Interface
     */

    Boolean readData(FileReader& f);
    Boolean writeData(FileWriter& f) const;

	BATDATA_DLL int pack(void * buffer, OrderBattle * ob);
	BATDATA_DLL int unpack(void * buffer, OrderBattle * ob);
};

class BattleOrder
{
  public:
      typedef BattleOrderInfo::OrderMode OrderMode;
      typedef BattleOrderInfo::Aggression Aggression;
      typedef BattleOrderInfo::Posture Posture;

      BATDATA_DLL BattleOrder();                               // Default Constructor
      BattleOrder(const BattleOrder& order) { copy(order); }         // Copy constructor
      BattleOrder& operator = (const BattleOrder& order) { copy(order); return *this; }   // copy

      friend bool operator == (const BattleOrder& ord1, const BattleOrder& ord2);
      friend bool operator != (const BattleOrder& ord1, const BattleOrder& ord2);

      /*
       * Accessors
       */

      OrderMode mode() const { return d_order.d_mode; }
      Aggression aggression() const { return d_order.d_aggression; }
      Posture posture() const { return d_order.d_posture; }
      BOB_Definitions::SPFormation spFormation() const { return d_order.d_spFormation; }
      BOB_Definitions::CPFormation divFormation() const { return d_order.d_divFormation; }
      BOB_Definitions::CPDeployHow deployHow() const { return d_order.d_deployHow; }
      BOB_Definitions::CPLineHow lineHow() const { return d_order.d_lineHow; }
      BOB_Definitions::CPManuever manuever() const { return d_order.d_manuever; }
      BattleMeasure::HexPosition::Facing facing() const { return d_order.d_facing; }
      WhatOrderList& whatOrder() { return d_order.d_whatOrder; }
      const WhatOrderList& whatOrder() const { return d_order.d_whatOrder; }

      void mode(OrderMode mode) { d_order.d_mode = mode; }
      void aggression(Aggression a) { d_order.d_aggression = a; }
      void posture(Posture p) { d_order.d_posture = p; }
      void spFormation(BOB_Definitions::SPFormation spFormation) { d_order.d_spFormation = spFormation; }
      void divFormation(BOB_Definitions::CPFormation divFormation) { d_order.d_divFormation = divFormation; }
      void deployHow(BOB_Definitions::CPDeployHow dh) { d_order.d_deployHow = dh; }
      void lineHow(BOB_Definitions::CPLineHow lh) { d_order.d_lineHow = lh; }
      void manuever(BOB_Definitions::CPManuever m) { d_order.d_manuever = m; }
      void facing(BattleMeasure::HexPosition::Facing facing) { d_order.d_facing = facing; }

      WayPointList& wayPoints() { return d_wayPoints; }
      const WayPointList& wayPoints() const { return d_wayPoints; }
      WayPoint* nextWayPoint() { return (d_wayPoints.entries() > 0) ? d_wayPoints.first() : 0; }
      WayPoint* lastWayPoint() { return (d_wayPoints.entries() > 0) ? d_wayPoints.getLast() : 0; }
      void removeWayPoint() { d_wayPoints.remove(d_wayPoints.first()); }
      void removeLastWayPoint() { d_wayPoints.remove(d_wayPoints.getLast()); }

      BattleOrderInfo& order() { return d_order; }
      const BattleOrderInfo order() const { return d_order; }

      void attachTo(CRefBattleCP thisUnit)  { d_attachTo = thisUnit; }
      ReturnCRefBattleCP attachTo() const { return d_attachTo; }

      void targetCP(CRefBattleCP thisUnit)  { d_targetCP = thisUnit; }
      ReturnCRefBattleCP targetCP() const { return d_targetCP; }

      void startOrderIn(BattleMeasure::BattleTime::Tick t)
      {
         d_startOrderIn = t; // start order in
      }

      BattleMeasure::BattleTime::Tick startOrderIn() const
      {
         return d_startOrderIn;
      }


		void startOrder()  { d_flags |= OrderStarted; }
	   void finishOrder() { d_flags |= OrderFinished; }
		void resetOrder() { d_flags = 0; }
		bool started() const  { return (d_flags & OrderStarted) != 0; }
	   bool finished() const { return (d_flags & OrderFinished) != 0; }

		/*
		 * File Interface
		 */

		BATDATA_DLL Boolean readData(FileReader& f, OrderBattle* ob);
		BATDATA_DLL Boolean writeData(FileWriter& f, OrderBattle* ob) const;

		BATDATA_DLL int pack(void * buffer, OrderBattle * ob);
		BATDATA_DLL void unpack(void * buffer, OrderBattle * ob);

	private:
		BATDATA_DLL void copy(const BattleOrder& order);

	private:
		static const UWORD s_fileVersion;

		BattleOrderInfo d_order;       // ordertype, deployment, sp-formation etc.
		WayPointList    d_wayPoints;   // list of movment waypoints
		CRefBattleCP     d_attachTo;    // attach to this unit
		CRefBattleCP     d_targetCP;    // bombard this target
		BattleMeasure::BattleTime::Tick d_startOrderIn; // start order in

      enum Flags {
        OrderStarted  = 0x01,
        OrderFinished = 0x02
      };

      UBYTE d_flags;
};

/*
 * An order with an arrival time
 */

class BattleSentOrder : public BattleOrder
{
      
   public:

	  enum { Invalid = 0 }; // Jim : changed this to public for BattleCP to use

      BATDATA_DLL BattleSentOrder();
      BattleSentOrder(BattleMeasure::BattleTime::Tick ticks, const BattleOrder& order) { set(ticks, order); }
      BattleSentOrder(const BattleSentOrder& order) { copy(order); }
      BattleSentOrder& operator = (const BattleSentOrder& order) { copy(order); return *this; }

      void set(BattleMeasure::BattleTime::Tick ticks, const BattleOrder& order);
      bool isTime(BattleMeasure::BattleTime::Tick t) const { return (d_ticks != Invalid) && (t >= d_ticks); }
      bool isValid() const { return d_ticks != Invalid; }
      void reset() { d_ticks = Invalid; }
      BattleMeasure::BattleTime::Tick ticks() const { return d_ticks; }
      void ticks(BattleMeasure::BattleTime::Tick t) { d_ticks = t; }

      /*
       * File Interface
       */

		Boolean readData(FileReader& f, OrderBattle* ob);
		Boolean writeData(FileWriter& f, OrderBattle* ob) const;

	private:
		BATDATA_DLL void copy(const BattleSentOrder& order);
	private:
		static const UWORD s_fileVersion;

		BattleMeasure::BattleTime::Tick d_ticks;

   friend bool operator < (const BattleSentOrder& ord1, const BattleSentOrder& ord2);

};

inline bool operator < (const BattleSentOrder& ord1, const BattleSentOrder& ord2)
{
   return ord1.d_ticks < ord2.d_ticks;
}


class BattleOrderList
{
   public:
      BattleOrderList();
      ~BattleOrderList() { }

      typedef std::priority_queue<BattleSentOrder, std::vector<BattleSentOrder>, std::less<BattleSentOrder> > Container;

      BATDATA_DLL void addOrder(BattleMeasure::BattleTime::Tick ticks, const BattleOrder& order);

	   BATDATA_DLL bool getOrder(void) const;
		 // If there are any orders in the queue then return true

      BATDATA_DLL bool getOrder(BattleMeasure::BattleTime::Tick ticks, BattleOrder* order);
         // If there are any orders in the queue then return true and copy to order

      BATDATA_DLL bool getOrder(BattleOrder* order);
         // If there are any orders in the queue then return true and copy to order
      BATDATA_DLL BattleMeasure::BattleTime::Tick ticks() const;
      BATDATA_DLL bool ticks(BattleMeasure::BattleTime::Tick ticks);

      bool getLastOrder(BattleOrder* order) const;
         // Get the last order that was sent

      /*
       * File Interface
       */

		Boolean readData(FileReader& f, OrderBattle* ob);
		Boolean writeData(FileWriter& f, OrderBattle* ob) const;

   private:
      static const UWORD s_fileVersion;
      Container d_items;
};


inline bool operator != (const BattleOrder& ord1, const BattleOrder& ord2)
{
   return !(ord1 == ord2);
}

#endif /* BATORD_HPP */

