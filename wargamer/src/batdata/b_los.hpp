/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef B_LOS_HPP
#define B_LOS_HPP

#define MAX_DIMENSION_OF_BATTLEFIELD   128
#include "hexdata.hpp"
// #include "batdata.hpp"
#include "BattleDataFwd.hpp"
#include "batcord.hpp"


class B_LineOfSight {
public:
   
   BATDATA_DLL B_LineOfSight(RCPBattleData bd);
   BATDATA_DLL Visibility::Value FastLOS(const BattleMap * map, const BattleMeasure::HexCord & from_hex, const BattleMeasure::HexCord & to_hex);
   BATDATA_DLL Visibility::Value FastLOS(const BattleMap * map, const BattleMeasure::HexCord & from_hex, const BattleMeasure::HexCord & to_hex, Side side);
private:
   // lookup of 1/n values.  multiply by [n] instead of doing 39cycle divide
   // dependent on cache consistency for any advantage !
   // therefore do big runs of execution with low thread usage, rather than many small calls with high thread activity
   CPBattleData d_batData;
   float d_distanceRecips[MAX_DIMENSION_OF_BATTLEFIELD];

   // this could be cleared better !
   int losVals[GT_HowMany];

   bool unitsUnderHex(BattleMeasure::HexCord& hex, Side s);

};

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
