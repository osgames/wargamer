/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BOBDEF_HPP
#define BOBDEF_HPP

#ifndef __cplusplus
#error bobdef.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Low Level Battle Order of Battle Definitions
 *
 *----------------------------------------------------------------------
 */

#include "refptr.hpp"


/*
 * Forward References and RefPtr<> definitions
 */

#if 0
class BattleSP;
typedef RefPtr<BattleSP> RefBattleSP;
typedef CRefPtr<BattleSP> CRefBattleSP;
typedef const RefPtr<BattleSP>& ParamRefBattleSP;
typedef const CRefPtr<BattleSP>& ParamCRefBattleSP;
typedef RefPtr<BattleSP> ReturnRefBattleSP;
typedef CRefPtr<BattleSP> ReturnCRefBattleSP;
static const RefBattleSP NoBattleSP = 0;

class BattleCP;
typedef RefPtr<BattleCP> RefBattleCP;
typedef CRefPtr<BattleCP> CRefBattleCP;
typedef const RefPtr<BattleCP>& ParamRefBattleCP;
typedef const CRefPtr<BattleCP>& ParamCRefBattleCP;
typedef RefPtr<BattleCP> ReturnRefBattleCP;
typedef CRefPtr<BattleCP> ReturnCRefBattleCP;
static const RefBattleCP NoBattleCP = 0;
#else

class BattleSP;
typedef BattleSP* RefBattleSP;
typedef const BattleSP* CRefBattleSP;
typedef BattleSP* ParamRefBattleSP;
typedef const BattleSP* ParamCRefBattleSP;
typedef BattleSP* ReturnRefBattleSP;
typedef const BattleSP* ReturnCRefBattleSP;
static const RefBattleSP NoBattleSP = 0;

class BattleCP;
typedef BattleCP* RefBattleCP;
typedef const BattleCP* CRefBattleCP;
typedef BattleCP* ParamRefBattleCP;
typedef const BattleCP* ParamCRefBattleCP;
typedef BattleCP* ReturnRefBattleCP;
typedef const BattleCP* ReturnCRefBattleCP;
static const RefBattleCP NoBattleCP = 0;


#endif


namespace BOB_Definitions
{

/*
 * Enumerations and data structures
 */

/*
 * Divisional Deployment
 *
 * How a CP's sp's are deployed
 * Note: There is never a gap between SP's, they always occupy adjacent hexes
 */

// center on first unit, or to the left or right
enum CPDeployHow
{
	CPD_DeployCenter,
	CPD_DeployRight,
	CPD_DeployLeft,

	CPD_Last,

	CPD_First = CPD_DeployCenter,
	CPD_Default = CPD_DeployCenter
};

// lines and columns
enum CPFormation
{
	CPF_March,               // in column
	CPF_Massed,              // in 2 parallel columns
	CPF_Deployed,            // on line, 2 hexes in depth
	CPF_Extended,            // on line, 1 hex in depth

	CPF_Last,

//   CPF_Readjust = CPF_Last,            // adjust due to loss
	CPF_First = CPF_March,
	CPF_Default = CPF_March
};

// how to form line
enum CPLineHow {
	CPL_Line,
	CPL_RefuseR,          // refuse right flank
	CPL_RefuseL,          // refuse left flank
	CPL_EchelonR,         // an envelopment manuever
	CPL_EchelonL,

	CPL_Last,
	CPL_First = CPL_Line,
	CPL_Default = CPL_Line
};

enum CPManuever {
	CPM_Forward,
	CPM_WheelR,       // wheel
	CPM_WheelL,
	CPM_RWheelR,
	CPM_RWheelL,
	CPM_SideStepR,
	CPM_SideStepL,
	CPM_AboutFace,
	CPM_Hold,

	CPM_Last,
	CPM_First = CPM_Forward,
	CPM_Default = CPM_Hold
};

/*
 * Enumeration of SP Formations
 */

enum SPFormation
{
	// Basic types
	SP_MarchFormation,
	SP_ColumnFormation,
	SP_ClosedColumnFormation,
	SP_LineFormation,
	SP_SquareFormation,

	SP_Formation_HowMany,
	SP_Formation_ArtilleryHowMany = SP_LineFormation,

	SP_Charge = SP_Formation_HowMany,
	SP_Rout,
	// Special value for changing

	SP_ChangingFormation,
	SP_UnknownFormation,

	// Synonyms for different unit types

	SP_LimberedFormation = SP_MarchFormation,			// Might redo this later on as a seperate flag
	SP_UnlimberedFormation = SP_ColumnFormation,		// also need mounted/unmounted for cavalry
	SP_Prolong = SP_ClosedColumnFormation,
	SP_Default = SP_MarchFormation
};

enum CPFaceHow {
	CPH_Front,
	CPH_Back,

	CPFaceHow_HowMany,
	CPFaceHow_Default = CPH_Front

};
};	// namespace BOB_Definitions

#endif /* BOBDEF_HPP */

