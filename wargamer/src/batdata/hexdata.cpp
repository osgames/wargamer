/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Data stored per hex
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "hexdata.hpp"
#include "random.hpp"
#include "filecnk.hpp"
#include "filebase.hpp"
#include "scenario.hpp"

using BattleMeasure::HexCord;

// namespace WG_BattleData
// {

BattleMap::BattleMap()
{
}

BattleMap::~BattleMap()
{
   delete s_nameTable;
   s_nameTable = 0;
}


const BattleTerrainHex& BattleMap::get(const HexCord& h) const
{
#ifdef DEBUG
        checkHex(h);
#endif

        size_t index = h.x() + h.y() * d_hexSize.x();
        return d_hexes[index];

}

BattleTerrainHex& BattleMap::get(const HexCord& h)
{
#ifdef DEBUG
        checkHex(h);
#endif

        size_t index = h.x() + h.y() * d_hexSize.x();
        return d_hexes[index];
}

#ifdef DEBUG
/*
 * Assert hex codes are valid
 */

void BattleMap::checkHex(const HexCord& h) const
{
        ASSERT(h.x() < d_hexSize.x());
        ASSERT(h.y() < d_hexSize.y());
}

#endif


bool BattleMap::moveHex(const HexCord& srcHex, HexCord::HexDirection dir, HexCord& destHex) const
{
        return srcHex.move(dir, destHex, getSize());
}




void BattleMap::create(const HexCord& hexSize)
{
        ASSERT(hexSize.x() > 0);
        ASSERT(hexSize.y() > 0);
        ASSERT(d_hexes.size() == 0);

        d_hexSize = hexSize;
        size_t totalSize = d_hexSize.x() * d_hexSize.y();

        /*
         * Set up a Terrain Hex to use as a default
         */

        TerrainType baseTerrain = TerrainTable::First;
        BattleTerrainHex initHex;
        initHex.d_terrainType = baseTerrain;
        initHex.d_height = 0;
        initHex.d_coast = 0;
        initHex.d_highlightFlags = 0;

        initHex.d_label = 0;
        initHex.d_victorySide = 0;
        initHex.d_victoryPoints = 0;

      initHex.d_visibility = Visibility::Full;
      initHex.d_lastFullVisUnit = NULL;

        /*
         * Allocate hexes, and initialise to default
         */

        d_hexes.erase(d_hexes.begin(), d_hexes.end());
        d_hexes.reserve(totalSize);
        d_hexes.insert(d_hexes.begin(), totalSize, initHex);

        ASSERT(d_hexes.size() == totalSize);
}


/*
This just clears all hexes in map
Basically only used in battle editor when a battlefield
needs to be regenerated on the fly
*/

void
BattleMap::clear(void) {

    d_hexes.erase(d_hexes.begin(), d_hexes.end());

}



/*
Crop map to a given size - coordinates are inclusive
This will have undefined results if other systems are using the outside area of the map
*/

void
BattleMap::crop(HexCord & topleft, HexCord & bottomright) {

    HexCord size = HexCord(bottomright.x() - topleft.x(), bottomright.y() - topleft.y() );
    BattleMap temp_map;
    temp_map.create(size);

    int x,y;

    // copy crop area into temporary map
    for(y=0; y<size.y(); y++) {
        for(x=0; x<size.x(); x++) {

            BattleTerrainHex & src_hex = get( HexCord(x+topleft.x(), y+topleft.y() ) );
            BattleTerrainHex & dest_hex = temp_map.get( HexCord(x,y) );

            dest_hex = src_hex;
        }
    }

    // create a new map at the new dimensions
    clear();
    create(size);

    // copy temp map back into this
    for(y=0; y<size.y(); y++) {
        for(x=0; x<size.x(); x++) {

            BattleTerrainHex & src_hex = temp_map.get( HexCord(x,y) );
            BattleTerrainHex & dest_hex = get( HexCord(x,y) );

            dest_hex = src_hex;
        }
    }

}



/*
 * File Interface
 */

const char BattleMap::s_fileID[] = "BattleMap";
const UWORD BattleMap::s_fileVersion = 0x0002;

StringFileTable* BattleMap::s_nameTable = 0;
const char BattleMap::s_nameFileName[] = "BattleLabels.dat";

Boolean BattleMap::readData(FileReader& file)
{
        FileChunkReader fc(file, s_fileID);

        UWORD version;
        file >> version;
        ASSERT(version <= s_fileVersion);

        file >> d_hexSize;

   create(d_hexSize);

   if (s_nameTable == 0)
   {
      CString fName = scenario->makeScenarioFileName(s_nameFileName, false);
      s_nameTable = new StringFileTable(fName);
   }


   for(int y=0; y < d_hexSize.y(); y++)
        {
        for(int x=0; x < d_hexSize.x(); x++)
                  {

            EdgeMap tmp_edges;

            BattleTerrainHex& maphex =  get(HexCord(x,y));

            file >> maphex.d_terrainType;

            file >> maphex.d_path1.d_type;
            file >> tmp_edges;
            maphex.d_path1.d_edges = tmp_edges;

            file >> maphex.d_path2.d_type;
            file >> tmp_edges;
            maphex.d_path2.d_edges = tmp_edges;

            file >> maphex.d_height;
            file >> maphex.d_coast;
            file >> maphex.d_edge.d_type;
            file >> tmp_edges;
            maphex.d_edge.d_edges = tmp_edges;

            if(version >= 0x0001) {
               if(version < 0x0002)
               {
                  maphex.d_label = file.getString();
               }
               else
               {
                  StringFileTable::ID id;
                  file >> id;
                  if (id != StringFileTable::Null)
                  {
                     maphex.d_label = copyString(s_nameTable->get(id));
                  }
               }

                file >> maphex.d_victorySide;
                file >> maphex.d_victoryPoints;
            }

        }
    }
        return file.isOK();
}

Boolean BattleMap::writeData(FileWriter& f) const
{
        FileChunkWriter fc(f, s_fileID);
        f << s_fileVersion;

        f << d_hexSize;

   if (s_nameTable == 0)
   {
      CString fName = scenario->makeScenarioFileName(s_nameFileName, false);
      s_nameTable = new StringFileTable(fName);
   }

    for(int y=0; y < d_hexSize.y(); y++)
         {

        for(int x=0; x < d_hexSize.x(); x++)
                  {

            /*
             *Write out data in each hex ( in the same order as the structure is defined in HEXDATA.HPP )
             */

            HexCord hex(x,y);
            const BattleTerrainHex& maphex =  get(hex);

            f << maphex.d_terrainType;
            f << maphex.d_path1.d_type;
            f << maphex.d_path1.d_edges;
            f << maphex.d_path2.d_type;
            f << maphex.d_path2.d_edges;
            f << maphex.d_height;
            f << maphex.d_coast;
            f << maphex.d_edge.d_type;
            f << maphex.d_edge.d_edges;

//            f.putString(maphex.d_label);
            StringFileTable::ID id = StringFileTable::Null;
            if (maphex.d_label)
            {
               id = s_nameTable->add(maphex.d_label);
            }
            f << id;


            f << maphex.d_victorySide;
            f << maphex.d_victoryPoints;

        }
    }
        return f.isOK();
}

void BattleMap::writeNameTable()
{
   if(s_nameTable)
      s_nameTable->writeFile();
}

// };   // namespace WG_BattleData


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
