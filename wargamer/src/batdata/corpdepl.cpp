/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "corpdepl.hpp"
#include "batdata.hpp"
#include "random.hpp"
#include "bobiter.hpp"

bool shouldBeVisible(RCPBattleData bd, const CRefBattleCP& cp, const CRefBattleSP& sp)
{
   const UnitTypeItem& uti = bd->ob()->getUnitType(sp);

   switch(uti.getBasicType())
   {
      case BasicUnitType::Infantry:
             return (cp->generic()->isInfantry());

      case BasicUnitType::Artillery:
         return (cp->generic()->isArtillery());

      case BasicUnitType::Cavalry:
         return (cp->generic()->isCavalry());
   }

   return False;
}

int countSP(RCPBattleData bd, const CRefBattleCP& cp, bool deploying)
{
   if(deploying)
   {
      int count = 0;
      CRefBattleSP sp = cp->sp();
      while(sp != NoBattleSP)
      {
         if(shouldBeVisible(bd, cp, sp))
            count++;

         sp = sp->sister();
      }

      return count;
   }
   else
         return cp->spCount();
}

bool corpDeployment(const CRefBattleCP& cp, UBYTE nInf, UBYTE nCav, UBYTE nArt,
  CorpDeployment& cd, RandomNumber& lRand, bool countOnly)
{
  int nLineXXFront = 0;
  int nSupportXXFront = 0;
  int nLineXXRear = 0;
  int nSupportXXRear = 0;

  // Inf / Combined Arms XXX
  if(nInf > 0)
  {
    int nTotalSupport = nCav + nArt;

    nLineXXFront = (nInf > 2) ? (nInf + 1) / 2 : nInf;
    nLineXXRear = nInf - nLineXXFront;

    nSupportXXFront = (nTotalSupport > 0 && lRand(100) < lRand(65, 85)) ?
         maximum(1, lRand(((nTotalSupport + 1) / 2) + 1)) : 0;
    nSupportXXRear = nTotalSupport - nSupportXXFront;
  }

  // Cav XXX
  else if(nCav > 0)
  {
    nLineXXFront = (nCav + 1) / 2;
    nLineXXRear = nCav - nLineXXFront;
    nSupportXXFront = (nArt > 0 && lRand(100) < lRand(65, 85)) ?
         maximum(1, lRand(((nArt + 1) / 2) + 1)) : 0;
    nSupportXXRear = nArt - nSupportXXFront;
  }

  // Probably wouldn't be an artillery XXX
  else if(nArt > 0)
  {
    nLineXXFront = (nArt + 1) / 2;
    nLineXXRear = nArt - nLineXXFront;
  }

  // initialize Corp deployment structure
  cd.init(nLineXXFront + nSupportXXFront, nLineXXRear + nSupportXXRear);

  if(countOnly)
    return True;

  // set front
  int nLine = nLineXXFront;
  int nSupport = nSupportXXFront;
  while(cd.iterFront())
  {
    CorpDeployment::XXType type;
    if(nSupport == 0)
      type = CorpDeployment::LineXX;
    else if(nLine == 0)
      type = CorpDeployment::SupportXX;

    else if(nLine < nLineXXFront)
    {
      // a 80% chance we add support
      type = (lRand(100) < lRand(97, 100)) ? CorpDeployment::SupportXX : CorpDeployment::LineXX;
    }

    else
    {
      // a 80% chance we add support
      type = (lRand(100) < lRand(97, 100)) ? CorpDeployment::LineXX : CorpDeployment::SupportXX;
    }

    if(type == CorpDeployment::LineXX)
      nLine--;
    else
      nSupport--;

    ASSERT(nLine >= 0);
    ASSERT(nSupport >= 0);
    cd.setFrontItem(type);
  }

  // set rear
  nLine = nLineXXRear;
  nSupport = nSupportXXRear;
  while(cd.iterRear())
  {
    CorpDeployment::XXType type;
    if(nSupport == 0)
      type = CorpDeployment::LineXX;
    else if(nLine == 0)
      type = CorpDeployment::SupportXX;

    else if(nLine < nLineXXRear)
    {
      // a 80% chance we add support
      type = (lRand(100) < lRand(70, 90)) ? CorpDeployment::SupportXX : CorpDeployment::LineXX;
    }

    else
    {
      // a 80% chance we add support
      type = (lRand(100) < lRand(70, 90)) ? CorpDeployment::LineXX : CorpDeployment::SupportXX;
    }

    if(type == CorpDeployment::LineXX)
      nLine--;
    else
      nSupport--;

    ASSERT(nLine >= 0);
    ASSERT(nSupport >= 0);
    cd.setRearItem(type);
  }

  return True;
}

void corpsTypes(RCPBattleData bd, const CRefBattleCP& cp, UBYTE& nInfXX, UBYTE& nCavXX, UBYTE& nArtXX, int& totalSP, bool deploying)
   // Get composition of corps
{
   if(cp->getRank().sameRank(Rank_Corps))
   {
      if(cp->child() == NoBattleCP)
         return;

      for(ConstBattleUnitIter iter(cp); !iter.isFinished(); iter.next())
      {
         if(!iter.cp()->getRank().sameRank(Rank_Division))
            continue;

         totalSP += countSP(bd, iter.cp(), deploying);

         if(iter.cp()->generic()->isInfantry())
            nInfXX++;
         else if(iter.cp()->generic()->isCavalry())
            nCavXX++;
         else if(iter.cp()->generic()->isArtillery())
            nArtXX++;
#ifdef DEBUG
         else
            FORCEASSERT("Improper unittype in corpTypes()");
#endif
      }
   }

   else if(cp->getRank().sameRank(Rank_Division))
   {
      totalSP += countSP(bd, cp, deploying);

      if(cp->generic()->isInfantry())
         nInfXX++;
      else if(cp->generic()->isCavalry())
         nCavXX++;
         else if(cp->generic()->isArtillery())
         nArtXX++;
#ifdef DEBUG
      else
         FORCEASSERT("Improper unittype in corpTypes()");
#endif
   }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
