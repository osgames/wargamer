/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATDATA_HPP
#define BATDATA_HPP

#ifndef __cplusplus
#error batdata.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Game Data
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "refptr.hpp"
#include "batcord.hpp"
#include "battime.hpp"
#include "batsound.hpp"
#include "batunit.hpp"
#include "batarmy.hpp"
#include "BattleDataFwd.hpp"

class   BattleDataImp;
struct BattleTerrainHex;
class TerrainTable;
class BattleMap;
class BattleOB;
class BattleHexMap;
class EffectsList;
class FileReader;
class FileWriter;
class ShockBattleList;
class KeyTerrainList;

// namespace BattleMeasure { class HexCord; }

namespace BattleBuildings
{
        class BuildingTable;
        class BuildingList;
        class EffectsTable;
};


class BATDATA_DLL BattleData 
#ifdef DEBUG
    : public RefBaseCount
#endif
{
//   using BattleMeasure::HexCord;           // Save some typing

        BattleDataImp* d_imp;

        // Unimplemented copy

        BattleData(const BattleData&);
        BattleData& operator = (const BattleData&);

    public:
        enum Type { Random, Historical };
        enum Mode { Deploying, Running, Overnight, Finished };

         BattleData();
         ~BattleData();

        /*
         * thread Locking
         */

        void startRead() const { ob()->startRead(); }
        void endRead() const { ob()->endRead(); }
        void startWrite() { ob()->startWrite(); }
        void endWrite() { ob()->endWrite(); }

        class ReadLock : public BattleOB::ReadLock
        {
            public:
                ReadLock(const BattleData* bd) : BattleOB::ReadLock(bd->ob()) { }
        };

        class WriteLock : public BattleOB::WriteLock
        {
            public:
                WriteLock(BattleData* bd) : BattleOB::WriteLock(bd->ob()) { }
        };

        /*
         * Miscellaneous functions
         */

         void type(Type t);
         Type type() const;

         void setChanged();
        // Let data know that it has changed in someway.

        /*                                                                                                                                
         * Hex locations and size functions
         */


        enum GetHexMode {
            Top,
            Mid,
            Low
        };

         BattleMeasure::HexCord getHex(const BattleLocation& l, GetHexMode mode = Mid) const;
        // Convert Physical location into the nearest hex.

         BattleLocation getLocation(const BattleMeasure::HexCord& h) const;
        // Get coordinate of hex center


         BattleArea getViewableArea() const;
        // Return the viewable area

		/*
		Functions to alter the battle playing-area
		*/
		 void getPlayingArea(BattleMeasure::HexCord * bottomleft, BattleMeasure::HexCord * size) const;
		 void setPlayingArea(BattleMeasure::HexCord bottomleft, BattleMeasure::HexCord size);
		 bool expandPlayingArea(BattleMeasure::HexCord newsize);
		 void maximizePlayingArea(void);


		 void setSideLeftRight(Side s, BattleMeasure::HexCord leftHex, BattleMeasure::HexCord rightHex);
		 void getSideLeftRight(Side s, BattleMeasure::HexCord& leftHex, BattleMeasure::HexCord& rightHex);

         bool moveHex(const BattleMeasure::HexCord& srcHex, BattleMeasure::HexCord::HexDirection dir, BattleMeasure::HexCord& destHex) const;
        // Get coordinate of hex in given direction
        // returns false if off edge of battlefield

         const BattleMeasure::HexCord& hexSize() const;
        // Get the size of the battlefield in hex units
        // Get hex size of the map

         void addToShockCombatList(const RefBattleCP& cp);
         ShockBattleList& shockBattles();
         const ShockBattleList& shockBattles() const;

        #ifdef DEBUG
         void assertHexOnMap(const BattleMeasure::HexCord& hex) const;
        // return true if hex is on the map
        #endif

        /*
         * Terrain Functions
         */

         const BattleTerrainHex& getTerrain(const BattleMeasure::HexCord& h) const;
        // Get raw Terrain Hex Information

         const TerrainTable& terrainTable() const;
         TerrainTable& terrainTable();
        // Get the main Terrain Table

         const BattleBuildings::BuildingTable* buildingTable() const;
         BattleBuildings::BuildingTable* buildingTable();
         const BattleBuildings::BuildingList* buildingList() const;
         BattleBuildings::BuildingList* buildingList();

         const BattleBuildings::EffectsTable* effectsTable() const;
         BattleBuildings::EffectsTable* effectsTable();

         EffectsList* effectsList() const;

         const BattleMap* map() const;
         BattleMap* map();

         BattleOB* ob();
         const BattleOB* ob() const;
         void ob(BattleOB* ob);
        void deleteOB();

         BattleHexMap* hexMap();
         const BattleHexMap* hexMap() const;

         bool roadHex(const BattleMeasure::HexCord& hex) const;

         BattleSoundSystem * soundSystem() const;

         const KeyTerrainList * keyTerrainList(void) const;
         KeyTerrainList * keyTerrainList(void);

		 void BattleData::cropMap(BattleMeasure::HexCord center, BattleMeasure::HexCord size);

		 void processBuildings(BattleMeasure::BattleTime::Tick ticks);

        /*
         * Time/Date Functions: Note that these must be thread-safe
         */

         void setDateAndTime(const BattleMeasure::BattleTime& t);
        // Sets the current Date and Time
         BattleMeasure::BattleTime getDateAndTime() const;
        // Makes a copy of the current date/time
         void addTime(BattleMeasure::BattleTime::Tick ticks);
        // Adds on BattleTicks to the time
         BattleMeasure::BattleTime::Tick getTick() const;
         void timeRatio(int n);       // ratio of real time to game time
         int timeRatio() const;

         void mode(Mode m);
         Mode mode() const;
         void endGameIn(BattleMeasure::BattleTime::Tick time);
		 BattleMeasure::BattleTime::Tick endGameIn(void);
         void addStartTimeToEndTime();
         bool shouldEnd() const;

         void setupUnits(bool initDeploy);

        /*
         * File Interface
         */

         Boolean readData(FileReader& f, OrderBattle* gob, bool initDeployMap);
         Boolean writeData(FileWriter& f, bool standAlone) const;
         void clear();
};


// };   // namespace WG_BattleData


#endif /* BATDATA_HPP */


/*----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:36  greenius
 * Initial Import
 *
 *----------------------------------------------------------------------
 */