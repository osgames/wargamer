/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "b_los.hpp"
#include "ftol.hpp"
#include "hexmap.hpp"
#include "bobutil.hpp"
#include "batdata.hpp"

using BattleMeasure::HexCord;

// define to do full terrain-aware LOS
#define TERRAIN_LOS
// define to use precalculated distance recips
#define PRECALC_DISTANCE

B_LineOfSight::B_LineOfSight(RCPBattleData bd) : d_batData(bd)
{
   for(int f=0; f<MAX_DIMENSION_OF_BATTLEFIELD; f++) {
      float val = f;
      d_distanceRecips[f] = 1.0f / val;
   }
}


/*
Presquared distance calculations to eliminate sqrt()s
*/
#define MAX_VIS_DIST_SQUARED  36*36
#define FAIR_VIS_DIST_SQUARED 18*18
#define GOOD_VIS_DIST_SQUARED 12*12
#define FULL_VIS_DIST_SQUARED 8*8


#define VISIBILITY_DELTA      -4
#define VISIBILITY_CUTOFF     VISIBILITY_DELTA - 3


/*

  This routine takes care of the LineOfSight calculations for the hex-visibility checks.
  Initially it worked on an on-off basis, but has now been adapted for graded visibility.
  Obscuring terrain is calculated correctly, as is the proper height gradient.
  Lots of optimizes have been made, including pre-squared distance comparisons to avoid sqrt()s,
  and a 1/z lookup to avoid FDIVs

*/


Visibility::Value
B_LineOfSight::FastLOS(const BattleMap * map, const HexCord & from_hex, const HexCord & to_hex) {

#ifdef TERRAIN_LOS

   struct LOSStruct {
      UBYTE d_seeThrough; // how many hexes of this type of terrain can we see through?
      UBYTE d_height;     // height in meters of this terrain.
   };

   static const LOSStruct s_losTable[GT_HowMany] = {
      { 0,      0  }, // GT_None  shouldn't be used
      { UBYTE_MAX, 0  }, // GT_Grass
      { UBYTE_MAX, 0  }, // GT_Water
      { 4,      5  }, // GT_Orchard
      { 4,      3  }, // GT_Vineyard
      { 2,      10 }, // GT_LightWood
      { 1,      30 }, // GT_DenseWood
      { UBYTE_MAX, 0  }, // GT_PloughedField
      { 4,         2  }, // GT_CornField
      { 4,         2  }, // GT_WheatField
      { 3,         1  }, // GT_Rough
      { UBYTE_MAX, 0  }, // GT_Marsh
      { 1,         20 }, // GT_Town
      { 2,         15 }, // GT_Village
      { UBYTE_MAX, 0  }, // GT_Farm
      { UBYTE_MAX, 0  }  // GT_Cemetary
   };

   // this *could* be cleared better in ASM, but it should always be in L1 cache
   for(int f=GT_HowMany-1; f>=0; f--) losVals[f] = 0;

#endif

   // both are +2 to account for unit heights
   int start_height = d_batData->map()->get(from_hex).d_height;
   int end_height = d_batData->map()->get(to_hex).d_height;

    int d, x, y, ax, ay, sx, sy, dx, dy;

   int x1 = from_hex.x();
   int y1 = from_hex.y();
   int x2 = to_hex.x();
   int y2 = to_hex.y();

    dx = to_hex.x()-from_hex.x();
   ax = abs(dx);
    dy = to_hex.y()-from_hex.y();
   ay = abs(dy);

   // sx = sgn(dx);
   if(dx>0) sx = 1;
   else if(dx<0) sx = -1;
   else sx = 0;

   // sy = sgn(dy);
   if(dy>0) sy = 1;
   else if(dy<0) sy = -1;
   else sy = 0;

    x = from_hex.x();
    y = from_hex.y();

   int squared_dist = (ax*ax)+(ay*ay);
   if(squared_dist > MAX_VIS_DIST_SQUARED) return Visibility::NotSeen;

   // NB : why are these doubled ??
   ax = ax<<1;
   ay = ay<<1;


   /*
   x dominant
   */
    if(ax>ay) {

#ifdef PRECALC_DISTANCE
      float height_inc = ((float)(end_height - start_height)) * d_distanceRecips[ax>>1];
#else
      float height_inc = ((float)(end_height - start_height)) / ((float)(ax>>1));
#endif
      float current_height = start_height;

      d = ay-(ax>>1);
      for (;;) {

         // we reached the end without being obscured
         if (x==to_hex.x()) {
            if(squared_dist > FAIR_VIS_DIST_SQUARED) return Visibility::Poor;
            else if(squared_dist > GOOD_VIS_DIST_SQUARED) return Visibility::Fair;
            else if(squared_dist > FULL_VIS_DIST_SQUARED) return Visibility::Good;
            else return Visibility::Full;
         }
         // step along line
         if (d>=0) {
            y += sy;
            d -= ax;
         }
         x += sx;
         d += ay;

         // check height on hex
         const BattleTerrainHex & maphex = d_batData->map()->get(HexCord(x,y));
         float height_diff = (current_height - maphex.d_height);
         if(height_diff < VISIBILITY_DELTA) {
            if(height_diff < VISIBILITY_CUTOFF) height_diff = VISIBILITY_CUTOFF;
            long vis_val;
            FloatToLong(&vis_val, height_diff);

            return clipValue(static_cast<Visibility::Value>(vis_val), Visibility::NotSeen, Visibility::Full);
            // return static_cast<Visibility::Value>(clipValue(vis_val, Visibility::NotSeen, Visibility::Full));
         }


#ifdef TERRAIN_LOS
         // check if we are looking through terrain
         if(height_diff < s_losTable[maphex.d_terrainType].d_height) {

            losVals[maphex.d_terrainType]++;
            // if we've reached the maximum allowed ammount
            if(losVals[maphex.d_terrainType] > s_losTable[maphex.d_terrainType].d_seeThrough) return Visibility::NotSeen;
         }
#endif

         current_height += height_inc;
      }
    }

   /*
   y dominant
   */
    else {

#ifdef PRECALC_DISTANCE
      float height_inc = ((float)(end_height - start_height)) * d_distanceRecips[ay>>1];
#else
      float height_inc = ((float)(end_height - start_height)) / ((float)(ay>>1));
#endif
      float current_height = start_height;

      d = ax-(ay>>1);
      for (;;) {

         // we reached the end without being obscured
         if(y==to_hex.y()) {
            if(squared_dist > FAIR_VIS_DIST_SQUARED) return Visibility::Poor;
            else if(squared_dist > GOOD_VIS_DIST_SQUARED) return Visibility::Fair;
            else if(squared_dist > FULL_VIS_DIST_SQUARED) return Visibility::Good;
            else return Visibility::Full;
         }
         // step along line
         if(d>=0) {
            x += sx;
            d -= ay;
         }
         y += sy;
         d += ax;

         // check height on hex
         const BattleTerrainHex & maphex = d_batData->map()->get(HexCord(x,y));
         float height_diff = (current_height - maphex.d_height);
         if(height_diff < VISIBILITY_DELTA) {
            if(height_diff < VISIBILITY_CUTOFF) height_diff = VISIBILITY_CUTOFF;
            long vis_val;
            FloatToLong(&vis_val, height_diff);
            return clipValue(static_cast<Visibility::Value>(vis_val), Visibility::NotSeen, Visibility::Full);
            // return static_cast<Visibility::Value>(clipValue(vis_val, Visibility::NotSeen, Visibility::Full));
         }


#ifdef TERRAIN_LOS
         // check if we are looking through terrain
         if(height_diff < s_losTable[maphex.d_terrainType].d_height) {

            losVals[maphex.d_terrainType]++;
            // if we've reached the maximum allowed ammount
            if(losVals[maphex.d_terrainType] > s_losTable[maphex.d_terrainType].d_seeThrough) return Visibility::NotSeen;
         }
#endif

         current_height += height_inc;
      }
    }


}


Visibility::Value
B_LineOfSight::FastLOS(const BattleMap * map, const HexCord & from_hex, const HexCord & to_hex, Side side) {

#ifdef TERRAIN_LOS

   struct LOSStruct {
      UBYTE d_seeThrough; // how many hexes of this type of terrain can we see through?
      UBYTE d_height;     // height in meters of this terrain.
   };

   static const LOSStruct s_losTable[GT_HowMany] = {
      { 0,      0  }, // GT_None  shouldn't be used
      { UBYTE_MAX, 0  }, // GT_Grass
      { UBYTE_MAX, 0  }, // GT_Water
      { 4,      5  }, // GT_Orchard
      { 4,      3  }, // GT_Vineyard
      { 2,      10 }, // GT_LightWood
      { 1,      30 }, // GT_DenseWood
      { UBYTE_MAX, 0  }, // GT_PloughedField
      { 4,         2  }, // GT_CornField
      { 4,         2  }, // GT_WheatField
      { 3,         1  }, // GT_Rough
      { UBYTE_MAX, 0  }, // GT_Marsh
      { 1,         20 }, // GT_Town
      { 2,         15 }, // GT_Village
      { UBYTE_MAX, 0  }, // GT_Farm
      { UBYTE_MAX, 0  }  // GT_Cemetary
   };

   // this *could* be cleared better in ASM, but it should always be in L1 cache
   for(int f=GT_HowMany-1; f>=0; f--) losVals[f] = 0;

#endif

   // both are +2 to account for unit heights
   int start_height = d_batData->map()->get(from_hex).d_height;
   int end_height = d_batData->map()->get(to_hex).d_height;

    int d, x, y, ax, ay, sx, sy, dx, dy;

   int x1 = from_hex.x();
   int y1 = from_hex.y();
   int x2 = to_hex.x();
   int y2 = to_hex.y();

    dx = to_hex.x()-from_hex.x();
   ax = abs(dx);
    dy = to_hex.y()-from_hex.y();
   ay = abs(dy);

   // sx = sgn(dx);
   if(dx>0) sx = 1;
   else if(dx<0) sx = -1;
   else sx = 0;

   // sy = sgn(dy);
   if(dy>0) sy = 1;
   else if(dy<0) sy = -1;
   else sy = 0;

    x = from_hex.x();
    y = from_hex.y();

   int squared_dist = (ax*ax)+(ay*ay);
   if(squared_dist > MAX_VIS_DIST_SQUARED) return Visibility::NotSeen;

   // NB : why are these doubled ??
   ax = ax<<1;
   ay = ay<<1;


   /*
   x dominant
   */
    if(ax>ay) {

#ifdef PRECALC_DISTANCE
      float height_inc = ((float)(end_height - start_height)) * d_distanceRecips[ax>>1];
#else
      float height_inc = ((float)(end_height - start_height)) / ((float)(ax>>1));
#endif
      float current_height = start_height;

      d = ay-(ax>>1);
      for (;;) {

         // we reached the end without being obscured
         if (x==to_hex.x()) {
            if(squared_dist > FAIR_VIS_DIST_SQUARED) return Visibility::Poor;
            else if(squared_dist > GOOD_VIS_DIST_SQUARED) return Visibility::Fair;
            else if(squared_dist > FULL_VIS_DIST_SQUARED) return Visibility::Good;
            else return Visibility::Full;
         }
         // step along line
         if (d>=0) {
            y += sy;
            d -= ax;
         }
         x += sx;
         d += ay;

         // check height on hex
            HexCord hex(x, y);
         const BattleTerrainHex & maphex = d_batData->map()->get(hex);
            int thisHeight = (unitsUnderHex(hex, side)) ? maphex.d_height + 8 : maphex.d_height;
         float height_diff = (current_height - thisHeight);
         if(height_diff < VISIBILITY_DELTA) {
            if(height_diff < VISIBILITY_CUTOFF) height_diff = VISIBILITY_CUTOFF;
            long vis_val;
            FloatToLong(&vis_val, height_diff);
            return clipValue(static_cast<Visibility::Value>(vis_val), Visibility::NotSeen, Visibility::Full);
         }


#ifdef TERRAIN_LOS
         // check if we are looking through terrain
         if(height_diff < s_losTable[maphex.d_terrainType].d_height) {

            losVals[maphex.d_terrainType]++;
            // if we've reached the maximum allowed ammount
            if(losVals[maphex.d_terrainType] > s_losTable[maphex.d_terrainType].d_seeThrough) return Visibility::NotSeen;
         }
#endif

         current_height += height_inc;
      }
    }

   /*
   y dominant
   */
    else {

#ifdef PRECALC_DISTANCE
      float height_inc = ((float)(end_height - start_height)) * d_distanceRecips[ay>>1];
#else
      float height_inc = ((float)(end_height - start_height)) / ((float)(ay>>1));
#endif
      float current_height = start_height;

      d = ax-(ay>>1);
      for (;;) {

         // we reached the end without being obscured
         if(y==to_hex.y()) {
            if(squared_dist > FAIR_VIS_DIST_SQUARED) return Visibility::Poor;
            else if(squared_dist > GOOD_VIS_DIST_SQUARED) return Visibility::Fair;
            else if(squared_dist > FULL_VIS_DIST_SQUARED) return Visibility::Good;
            else return Visibility::Full;
         }
         // step along line
         if(d>=0) {
            x += sx;
            d -= ay;
         }
         y += sy;
         d += ax;

         // check height on hex
            HexCord hex(x, y);
         const BattleTerrainHex & maphex = d_batData->map()->get(hex);
            int thisHeight = (unitsUnderHex(hex, side)) ? maphex.d_height + 8 : maphex.d_height;
         float height_diff = (current_height - thisHeight);//maphex.d_height);
         if(height_diff < VISIBILITY_DELTA) {
            if(height_diff < VISIBILITY_CUTOFF) height_diff = VISIBILITY_CUTOFF;
            long vis_val;
            FloatToLong(&vis_val, height_diff);
            return clipValue(static_cast<Visibility::Value>(vis_val), Visibility::NotSeen, Visibility::Full);
         }


#ifdef TERRAIN_LOS
         // check if we are looking through terrain
         if(height_diff < s_losTable[maphex.d_terrainType].d_height) {

            losVals[maphex.d_terrainType]++;
            // if we've reached the maximum allowed ammount
            if(losVals[maphex.d_terrainType] > s_losTable[maphex.d_terrainType].d_seeThrough) return Visibility::NotSeen;
         }
#endif

         current_height += height_inc;
      }
    }


}

bool B_LineOfSight::unitsUnderHex(HexCord& hex, Side side)
{
   const BattleHexMap* bm = d_batData->hexMap();
   BattleHexMap::const_iterator lower;
   BattleHexMap::const_iterator upper;

   bm->find(hex, lower, upper);
   while(lower != upper)
   {
         const BattleUnit* bu = bm->unit(lower);
         RefBattleCP hq = const_cast<RefBattleCP>(BobUtility::getCP(bu));

         if(hq->getRank().sameRank(Rank_Division) && hq->getSide() == side)
            return True;

         ++lower;
   }

   return False;
   
}





/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
