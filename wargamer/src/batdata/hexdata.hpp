/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef HEXDATA_HPP
#define HEXDATA_HPP

#ifndef __cplusplus
#error hexdata.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Information stored for a battle hex
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "b_terr.hpp"
#include "batcord.hpp"
#include "gamedefs.hpp"
#include "nameTable.hpp"
#include <vector>     // "autoptr.hpp"

class BattleUnit;

// using namespace BattleMeasure;

// number of hexes along each edge of map to act as buffer (ie. unplayable in)
#define BATTLEMAP_BUFFERHEXES	8
// max width & height (not taking in to account buffer zones)
#define BATTLEMAP_MAXWIDTH		128
#define BATTLEMAP_MAXHEIGHT		128


// enumeration for FOW routines
class Visibility {
public:
   enum Value {
      NeverSeen,        // Added SWG: 11Nov99
      NotSeen,
      Poor,
      Fair,
      Good,
      Full,

      HowMany,
      Default = NotSeen
   };
};


struct PathInfo
{
        PathInfo() : d_type(PathTable::None), d_edges(0) { }

        PathType        d_type;                         // Index into PathTable
        EdgeMap d_edges:6;                      // Which edges does the path go through

        bool isRoad() const
        {
        	return (    d_type == LP_Road        ||
				        d_type == LP_SunkenRoad  ||
				        d_type == LP_SunkenTrack ||
				        d_type == LP_Track       ||
				        d_type == LP_RoadBridge  ||
				        d_type == LP_TrackBridge    );
        }
};

struct EdgeInfo
{
        EdgeInfo() : d_type(EdgeTable::None), d_edges(0) { }

        EdgeType        d_type;                         // Index into EdgeTable
        EdgeMap d_edges:6;                      // Which edges does the path go through
};


/*
 * Information stored in a hex
 */

#define HIGHLIGHT_SET		4
#define HIGHLIGHT_DARKENED	8
#define HIGHLIGHT_DARKEST	16
#define HIGHLIGHT_OUTLINE	32

struct BattleTerrainHex
{

        typedef UBYTE Height;
        enum { MaxHeight = 127 };

        TerrainType     d_terrainType;
        PathInfo                d_path1;
        PathInfo                d_path2;
        Height          d_height;
        EdgeMap         d_coast;                        // Coast line edges
        EdgeInfo                d_edge;                 // Wall/Hedge/etc

		unsigned char d_highlightFlags;

      char * d_label;
      StringFileTable::ID d_labelID;

        Side d_victorySide;
        unsigned char d_victoryPoints;

		Visibility::Value d_visibility;
		BattleUnit * d_lastFullVisUnit;


    bool isRoad() const { return d_path1.isRoad() || d_path2.isRoad(); }
};


class BattleMap
{
            static StringFileTable* s_nameTable;
            static const char s_nameFileName[];

        public:
                BATDATA_DLL BattleMap();
                BATDATA_DLL ~BattleMap();

                BATDATA_DLL const BattleTerrainHex& get(const BattleMeasure::HexCord& h) const;
                BATDATA_DLL BattleTerrainHex& get(const BattleMeasure::HexCord& h);

                const BattleMeasure::HexCord& getSize() const {        return d_hexSize; }

                bool moveHex(const BattleMeasure::HexCord& srcHex, BattleMeasure::HexCord::HexDirection dir, BattleMeasure::HexCord& destHex) const;
                        // Get coordinate of hex in given direction
                        // returns false if off edge of battlefield

                // void createRandom(const TerrainTable& terrain, const HexCord& hexSize);

                BATDATA_DLL void create(const BattleMeasure::HexCord& hexSize);
                        // Create a blank map of given size

                // clear whole map
                BATDATA_DLL void clear(void);

                BATDATA_DLL void crop(BattleMeasure::HexCord & topleft, BattleMeasure::HexCord & bottomright);

                inline void highlightHex(const BattleMeasure::HexCord& hex, unsigned char flags) {
                    BattleTerrainHex &maphex = get(hex);
                    maphex.d_highlightFlags = flags;
				}

                inline void unhighlightHex(const BattleMeasure::HexCord& hex) {
					BattleTerrainHex &maphex = get(hex);
					maphex.d_highlightFlags = 0;
				}

                inline unsigned char isHighlighted(const BattleMeasure::HexCord& hex) {
					BattleTerrainHex &maphex = get(hex);
					return maphex.d_highlightFlags;
				}
/*
                inline void toggleHexHighlight(const BattleMeasure::HexCord& hex) {
                    BattleTerrainHex &maphex = get(hex);
                    if(maphex.d_highlight == 1) maphex.d_highlight = 0;
                    else maphex.d_highlight = 1; }
*/
                inline void removeAllHighlights(void) {
                    for(int y=0; y<d_hexSize.y(); y++)
                        for(int x=0; x<d_hexSize.x(); x++) {
                            BattleTerrainHex &maphex = get(BattleMeasure::HexCord(x,y));
                            maphex.d_highlightFlags = 0;
                        }
                }


                static BATDATA_DLL void writeNameTable();

                /*
                 * File Interface
                 */

                BATDATA_DLL Boolean readData(FileReader& f);
                BATDATA_DLL Boolean writeData(FileWriter& f) const;

#ifdef DEBUG
                void checkHex(const BattleMeasure::HexCord& h) const;  // Assert hex cords are valid
#endif

        private:
                //--- Creation functions moved to bcreate
                // void createRoad(const TerrainTable& terrain, PathTable::Index type);
                // void createRiver(const TerrainTable& terrain, PathTable::Index type);
                // void createPath(const TerrainTable& terrain, PathTable::Index type, int bendiness, int loopiness);
                // BattleMeasure::HexCord getStartCord(int edge, const TerrainTable& terrain) const;
                // bool edgeAcceptable(const TerrainTable& terrain, const BattleMeasure::HexCord& hex) const;
                // void setPath(const TerrainTable& table, BattleTerrainHex* ter, PathTable::Index type, UBYTE bits);
                // void createWall(const TerrainTable& terrain, EdgeType type);

        private:
                BattleMeasure::HexCord d_hexSize;              // Size of map
                std::vector<BattleTerrainHex> d_hexes;
                // auto_ptr<BattleTerrainHex> d_hexes;

                                         static const char s_fileID[];
                                         static const UWORD s_fileVersion;
};

// };   // namespace WG_BattleData


#endif /* HEXDATA_HPP */

