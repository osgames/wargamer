/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef HEXMAP_HPP
#define HEXMAP_HPP

#ifndef __cplusplus
#error hexmap.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      HexMap: MultiMap of objects on the map, indexed by hex
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "batcord.hpp"
#include "refptr.hpp"
#include "batunit.hpp"
#include <map>

class BATDATA_DLL BattleHexMap
{
    typedef std::multimap<BattleMeasure::HexCord, RefPtr<BattleUnit>, std::less<const BattleMeasure::HexCord> > Container;

  public:
    class BATDATA_DLL iterator
    {
      public:
        iterator();
        iterator(const Container::iterator it);
        iterator(const iterator& it);
        iterator& operator=(const iterator& it);
        iterator& operator=(const Container::iterator& it);

        iterator& operator++();
        Container::reference operator*();
        bool operator==(const iterator& it) const;
        bool operator!=(const iterator& it) const { return !(*this == it); }

      private:
        Container::iterator m_it;

        friend class BattleHexMap;
    };

    class BATDATA_DLL const_iterator
    {
      public:
        const_iterator();
        const_iterator(const const_iterator& it);
        const_iterator(const Container::const_iterator& it);
        const_iterator& operator=(const const_iterator& it);
        const_iterator& operator=(const Container::const_iterator& it);

        const_iterator& operator++();
        Container::const_reference operator*();
        bool operator==(const const_iterator& it) const;
        bool operator!=(const const_iterator& it) const { return !(*this == it); }

      private:
        Container::const_iterator m_it;

        friend class BattleHexMap;
    };




    BattleHexMap();
    ~BattleHexMap();

    bool find(const BattleMeasure::HexCord& hex, const_iterator& lower, const_iterator& upper) const;
    bool find(const BattleMeasure::HexCord& hex, iterator& lower, iterator& upper);
    // return iterators to cover objects in a given hex

    iterator find(const BattleUnit* unit);
    // find a unit (uses virtual hex() function

    bool isUnitPresent(const BattleUnit* unit);

    void move(iterator from);
    // update an object to whatever position hex() is

    void remove(iterator from)
    { 
      d_items.erase(from.m_it); 
    }
    // remove item from map

    void add(BattleUnit* unit);
    // Add a new object

    static BattleUnit* unit(iterator i)
    {
      return (*i).second; 
    }

    static const BattleUnit* unit(const_iterator i)
    {
      return (*i).second; 
    }

    void clear(const BattleMeasure::HexCord & mapsize);
    void clear();

  private:
    Container d_items;
};


#endif /* HEXMAP_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.4  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 *----------------------------------------------------------------------
 */
