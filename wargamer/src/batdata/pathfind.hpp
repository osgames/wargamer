/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PATHFIND_HPP
#define PATHFIND_HPP

#include "bd_dll.h"
// #include "batdata.hpp"
#include "BattleDataFwd.hpp"
#include "bob_cp.hpp"
#include "hexdata.hpp"
#include "sqrt.hpp"
#include "sllist.hpp"
#include "ftol.hpp"
#include "hexlist.hpp"
#include "batunit.hpp"
#include "bobdef.hpp"
#include "b_tables.hpp"
#include "..\batlogic\batlogic.hpp"

/*
Exact distance between points.
A less accurate approximation would do just as well, since we are only comparing integers
*/


inline float
DistanceBetweenHexes(BattleMeasure::HexCord * A, BattleMeasure::HexCord * B) {
    float dx=abs(B->x() - A->x());
    float dy=abs(B->y() - A->y());

    return sqrt((dx*dx)+(dy*dy));
}


inline float
RelativeDistanceBetweenHexes(BattleMeasure::HexCord * A, BattleMeasure::HexCord * B) {
    int dx=abs(B->x() - A->x());
    int dy=abs(B->y() - A->y());

    return (dx*dx)+(dy*dy);
}






/*
Structures
*/

    typedef struct AstarNode {
        BattleMeasure::HexCord hex;
        float cost;
        float factor;
        AstarNode * parent;
        BattleMeasure::HexCord::HexDirection dir_from_parent;  // direction to this hex from parent
        AstarNode * child[6];  // six directions of movement from a hex
    } AstarNode;

    typedef struct AstarNodeList {
        AstarNode * node;
        AstarNodeList * next;
    } AstarNodeList;

    typedef struct PathPoint : public SLink {
        BattleMeasure::HexCord hex;
        BattleMeasure::HexCord::HexDirection dir_prev;
        BattleMeasure::HexCord::HexDirection dir_next;
    } PathPoint;



#define PATHFLAG_AVOIDUPHILL    0x01
#define PATHFLAG_AVOIDDOWNHILL  0x02
#define PATHFLAG_AVOIDPROHIBITED        0x04
#define PATHFLAG_TERRAINCOST     0x08
#define PATHFLAG_USEROADS       0x10

#define PATHTYPE_DIRECTPATH     0
#define PATHTYPE_RIVERPATH      PATHFLAG_AVOIDUPHILL
#define PATHTYPE_ROADPATH       PATHFLAG_AVOIDUPHILL | PATHFLAG_AVOIDDOWNHILL


class PathFinder {

private:

    BattleData * battle_data;
    BattleMap * battle_map;

    BattleCP * battle_cp;

    int MapWidth;
    int MapHeight;

    BattleMeasure::HexCord start_hex;
    BattleMeasure::HexCord end_hex;

    int PathFound;
    int PathLength;
    float PathCost;


    AstarNodeList * OpenList;
    AstarNodeList * ClosedList;

    int DEBUG_plot_Hexes;
    int open_list_size;
    int closed_list_size;
    float time_taken;

    int PathFlags;

	// these public cause a controlling class destructs them
public:
    static int ArraySize;
	static int MaxArraySize;
    static enum ClosedFlag {
      INIT_FLAG = 0,
      OPEN_FLAG = 0x01,
      CLOSED_FLAG = 0x02,
      MARK_FLAG = 0xff
    }* ClosedArray;

public:

    BATDATA_DLL PathFinder(BattleData * batdata);
    BATDATA_DLL ~PathFinder(void);
    BATDATA_DLL void FindPath(const BattleMeasure::HexCord& start, const BattleMeasure::HexCord& end, int path_mode, SList<PathPoint> * PathList);
    void FindPath(int start_x, int start_y, int end_x, int end_y, int path_mode, SList<PathPoint> * PathList);
    void FindPath(BattleCP * cp, int start_x, int start_y, int end_x, int end_y, int path_flags, HexList * UnitHexList);

	BATDATA_DLL void FindPathExcludingHexes(BattleCP * cp, const BattleMeasure::HexCord& start, const BattleMeasure::HexCord& end, int path_flags, HexList * exclude_hexes, HexList * hexlist);
	// find a path, excluding given hexes
	// return path in hexlist (this should be reset() before calling)

    BATDATA_DLL void FindPath(BattleCP * cp, const BattleMeasure::HexCord& start, const BattleMeasure::HexCord& end, int path_flags, SList<PathPoint> * PathList, HexList * UnitHexList);

private:

    void ShowDebugInfo(void);
    void SetupLists(void);
    void DestroyLists(void);
    void SetupArray(void);
    void ClearArray(void);
    void DestroyArray(void);
    void ClearNode(AstarNode * node);
    void PutHexInOpen(AstarNode * node);
    void RemoveHexFromOpen(void);
    AstarNode * GetHexFromOpen(void);
    void PutHexInClosedArray(AstarNode * node);
    int IsHexInClosedArray(BattleMeasure::HexCord * hexpos);
    void PutHexInOpenArray(AstarNode * node);
    int IsHexInOpenArray(BattleMeasure::HexCord * hexpos);
    void PutHexInClosed(AstarNode * node);
    int IsHexInOpen(BattleMeasure::HexCord * hexpos);
    int IsHexValid(BattleMeasure::HexCord * hexpos);
    int IsHexPassable(const BattleMeasure::HexCord& hexpos);
    UWORD terrainCost(RPBattleData bd, const RefBattleCP& cp, const BattleMeasure::HexCord& hex);
    UWORD pathCost(RPBattleData bd, const RefBattleCP& cp, PathType pathtype);
    void AddSurroundingHexes(AstarNode * current, BattleMeasure::HexCord * CurrentHexPos);
    AstarNode * CreateNewNode(AstarNode * last, BattleMeasure::HexCord * NewHexPos, BattleMeasure::HexCord::HexDirection direction);

    void TraceBackPath(AstarNode * current_node, SList<PathPoint> * path_list);
    void TraceBackUnitPath(AstarNode * current_node, HexList * List);

    BattleMeasure::HexPosition::Facing DirectionToFacing(BattleMeasure::HexCord::HexDirection direction);
    BattleMeasure::HexCord::HexDirection FacingToDirection(BattleMeasure::HexPosition::Facing facing);

    void SaveAsRaw(void);
    void DebugReport(void);




enum LocalSPFormation {
                March,
                Column,
                Other
};

enum LocalUnitType {
         Inf,
         LCav,
         HCav,
         FootArt,
         HvyArt,
         HorseArt,

         End,
         Undefined = End
};

void getTypeAndFormation(const RefBattleCP& cp, LocalSPFormation& spf, LocalUnitType& ut)
{
  // convert unit type flags to local enum
  ut = (cp->getRank().isHigher(Rank_Division) || cp->generic()->isInfantry()) ? Inf :
                 (cp->generic()->isHeavyCavalry())   ? HCav :
                 (cp->generic()->isCavalry())        ? LCav :
                 (cp->generic()->isHeavyArtillery()) ? HvyArt :
                 (cp->generic()->isHorseArtillery()) ? HorseArt :
                 (cp->generic()->isArtillery())      ? FootArt : Undefined;

  ASSERT(ut != Undefined);

  // convert spformation to local enum
  spf = March;
//  spf = (cp->getRank().isHigher(Rank_Division) || cp->spFormation() == SP_MarchFormation) ? March :
//                (cp->spFormation() == SP_ColumnFormation) ? Column : Other;
}


};



class StaticArrayDestructor {

public:

	BATDATA_DLL StaticArrayDestructor(void);
	BATDATA_DLL ~StaticArrayDestructor(void);

};










#endif
