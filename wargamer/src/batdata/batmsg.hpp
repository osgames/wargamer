/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATMSG_HPP
#define BATMSG_HPP

#ifndef __cplusplus
#error batmsg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Player Messages
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "gpmsg.hpp"		// GenericPlayerMessage
#include "gamedefs.hpp"
#include "bobdef.hpp"
#include "obdefs.hpp"
#include "sync.hpp"
#include "batcord.hpp"
// #include <string.hpp>

class BattleData;

class BattleMessageID
{
	public:
		enum BMSG_ID
		{
         BMSG_First = 0,
			TestMessage = BMSG_First,

			// combat related messages
			BMSG_EngagingWithArtillery,
			BMSG_EngagingWithMuskets,
			BMSG_TakingArtilleryFire,
			BMSG_TakingMusketFire,
			BMSG_IntoCloseCombat,
			BMSG_TakingLosses,
			BMSG_InflictingLosses,
			BMSG_CapturedTrophy,
			BMSG_EnemyCapturedTrophy,
			BMSG_Routing,
			BMSG_EnemyRouting,
			BMSG_EnemySighted,
			BMSG_CarriedOutOrder,
			BMSG_AvoidingEnemy,
			BMSG_EnemyAvoiding,
			BMSG_Retreating,
			BMSG_EnemyRetreating,
			BMSG_FleeingTheField,
			BMSG_EnemyFleeing,
			BMSG_CriticalLossReached,
      BMSG_Wavering,
      BMSG_EnemyWavering,

			// leader messages
			BMSG_LeaderHit,
			BMSG_HitEnemyLeader,
			BMSG_TakenCommand,

			// misc
			BMSG_OutOfAmmo,
			BMSG_AmmoRemaingin,

			// move related
			BMSG_UnableToDeploy,
			BMSG_UnableToFindRoute,
			BMSG_RefusingFlank,
			BMSG_ChangingSPFormation,
			BMSG_ChangingXXDeployment,

         // Unchecked
         // Victory location
         BMSG_EnemyTookVP,
         // End
			BMSG_HowMany,
			NoMessage = -1
		};

		enum MessageGroup {
			Group0,
			Group1,
			Group2,
			Group3,

			Group_HowMany
		};

		BattleMessageID() : d_id(NoMessage) { }
		BattleMessageID(BMSG_ID id) : d_id(id) { }
		BattleMessageID(const BattleMessageID& id) : d_id(id.d_id) { }
		BattleMessageID& operator = (const BattleMessageID& id) { d_id = id.d_id; return *this; }

		String text() const;


		// these may be used by the Battle settings dialog

		int getDescriptionID();
		MessageGroup getGroup();
		int getGroupID();
		BATDATA_DLL String getDescription();
		BATDATA_DLL BMSG_ID id() const { return d_id; } // For easy use by AI

		static int getGroupID(MessageGroup group);

	private:
		BMSG_ID d_id;
};


class BattleMessageInfo : public GenericPlayerMessage // , public RWLock
{
	public:

		/*
		 * Functions used by creators
		 */

		BATDATA_DLL BattleMessageInfo();
		BattleMessageInfo(const BattleMessageInfo& bmi) { copyMessage(bmi); }
		BATDATA_DLL BattleMessageInfo(Side s, BattleMessageID id, CRefBattleCP cp);
		~BattleMessageInfo() { }

		BattleMessageInfo& operator = (const BattleMessageInfo& bmi)
		{
			copyMessage(bmi);
			return *this;
		}
		BATDATA_DLL void copyMessage(const BattleMessageInfo& bmi);
		void id(BattleMessageID id) { d_id = id; }
		void side(Side side) { d_side = side; }
		void cp(CRefBattleCP cp) { d_cp = cp; }
		void leader(ConstRefGLeader l) { d_leader = l; }
		void target(CRefBattleCP cp) { d_cpTarget = cp; }
		void s1(const char* s) { d_s1 = s; }
		void s2(const char* s) { d_s2 = s; }
		void s1(const String s) { d_s1 = s; }
		void s2(const String s) { d_s2 = s; }
		void n1(int n) { d_n1 = n; }
      // Unchecked
      void hex(const BattleMeasure::HexCord& h) { d_hex = h; }
      // End
		BattleMessageID id() const { return d_id; }
		Side side() const { return d_side; }
		CRefBattleCP cp() const { return d_cp; }
		ConstRefGLeader leader() const { return d_leader; }
		CRefBattleCP target() const { return d_cpTarget; }
      // Unchecked
      const BattleMeasure::HexCord& hex() const { return d_hex; }
      // End
		const char* s1() const { return d_s1.c_str(); }
		const char* s2() const { return d_s2.c_str(); }
		int n1() const { return d_n1; }

#if 0 // unused
      class ReadLock
		{
				const BattleMessageInfo* d_bmi;
			public:
				ReadLock(const BattleMessageInfo* bmi) : d_bmi(bmi) { d_bmi->startRead(); }
				~ReadLock() { d_bmi->endRead(); }
		};

		class WriteLock
		{
				BattleMessageInfo* d_bmi;
			public:
				WriteLock(BattleMessageInfo* bmi) : d_bmi(bmi) { d_bmi->startWrite(); }
				~WriteLock() { d_bmi->endWrite(); }
		};
#endif

	private:
		BattleMessageID	d_id;
		Side 					d_side;			// who is this message for?
		CRefBattleCP 		d_cp;
		ConstRefGLeader 	d_leader;
		CRefBattleCP 		d_cpTarget;
		String				d_s1;
		String				d_s2;
		int					d_n1;
      BattleMeasure::HexCord           d_hex;

		// also need:
		//		Location?
};


class BattleMessageFormat
{
	public:
		BATDATA_DLL static String messageToText(const BattleMessageInfo& msg, const BattleData* batData);
};


class BATDATA_DLL BattleMessageOptions {
 public:
	static Boolean get(BattleMessageID id);
	static void set(BattleMessageID id, Boolean flag);
	static void toggle(BattleMessageID id);
   static void setDefault();
};


#endif /* BATMSG_HPP */

