/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TRESULT_HPP
#define TRESULT_HPP

#ifndef __cplusplus
#error tresult.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Tactical Results
 *
 *----------------------------------------------------------------------
 */

#include "bd_dll.h"
#include "b_result.hpp"
#include "bobdef.hpp"

class BattleData;
class BattleOB;

class TacticalResults : public BattleResults
{
    public:
        BATDATA_DLL TacticalResults(const BattleFinish& f);
        BATDATA_DLL ~TacticalResults();
        BATDATA_DLL void calculate(const BattleData* batData);


        void setTitle(const String title) { d_title = title; }
        void setTopUnit(Side s, ParamCRefBattleCP cp) { d_topUnit[s] = cp; }


        // BattleResults Implementation

        virtual String battleName() const { return d_title; }
        virtual ConstRefGLeader leader(Side s) const;
        virtual ConstRefGenericCP cp(Side s) const;

    private:
        void calcVictoryLevel(const BattleData* batData, float cvp[2]);
        void continueTest(const BattleData* batData);
    private:

        CRefBattleCP d_topUnit[2];
        String d_title;
};



#endif /* TRESULT_HPP */

