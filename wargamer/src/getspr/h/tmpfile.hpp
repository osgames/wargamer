/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TMPFILE_HPP
#define TMPFILE_HPP

#ifndef __cplusplus
#error tmpfile.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Temporary Files
 *
 *----------------------------------------------------------------------
 */

#include <stdio.h>

class TmpFile
{
		const char* d_name;
		FILE* d_file;
		enum { None, Read, Write } d_mode;
	public:
		TmpFile(const char* name);
		~TmpFile();

		void openRead();
		void openWrite();
		void close();
		void read(void* buffer, size_t len);
		void write(const void* buffer, size_t len);

		operator FILE* () { return d_file; }

		class Error
		{
		};
};

#endif /* TMPFILE_HPP */

