/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AUTOPTR_HPP
#define AUTOPTR_HPP

#ifndef __cplusplus
#error autoptr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Auto Pointer
 *
 * A Pointer that deletes itself when destructed
 * Taken from Scott Meyers' More Effective C++
 *
 *----------------------------------------------------------------------
 */

// Use stl auto_ptr instead

#include <memory>

#ifdef _MSC_VER
#define __BEGIN_STL_NAMESPACE 
#define __END_STL_NAMESPACE 
#define __STL_THROWS(t) 
#endif

// Modified version of auto_ptr to make auto_array_ptr

/*
 * Copyright (c) 1997
 * Moscow Center for SPARC Technology
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.  Moscow Center for SPARC Technology makes no
 * representations about the suitability of this software for any
 * purpose.  It is provided "as is" without express or implied warranty.
 *
 */

__BEGIN_STL_NAMESPACE

// 20.4.5 Template class auto_ptr

// Warning : to be sure, you should only use new() to initialize auto_ptr contents,
// since it uses explicit delete() to destroy its contents.

template <class X>
class auto_array_ptr {
public:
    typedef X element_type;
    explicit auto_array_ptr(X* p=0) __STL_THROWS(()) : ptr_(p), owner_(p!=0)  {}
#  if defined __STL_MEMBER_TEMPLATES
    template<class Y>
#  else
   typedef X Y;
#  endif
    auto_array_ptr(const auto_array_ptr<Y>& r) __STL_THROWS(()) : ptr_(r.get()), owner_(r.owner_) {
        r.release();
    }
#  if defined __STL_MEMBER_TEMPLATES
    template<class Y>
#  endif
    auto_array_ptr<X>& operator=(const auto_array_ptr<Y>& r)  __STL_THROWS(()) {
        if ((void*)&r != (void*)this) {
            reset();
            owner_ = r.owner_; 
            ptr_ = r.release();
        }
        return *this;
    }
    ~auto_array_ptr()                         { reset(); }
    X& operator*()  const  __STL_THROWS(()) { 
#  ifdef __AUTO_PTR_NON_OWNER_ASSERT
		assert(owner_);
#  endif
        return *ptr_; }
    X* operator->() const  __STL_THROWS(()) { 
#  ifdef __AUTO_PTR_NON_OWNER_ASSERT
		assert(owner_);
#  endif
        return ptr_; 
    }
    X* get()        const  __STL_THROWS(()) { return ptr_; }
    X* release()    const  __STL_THROWS(()) { 
#  if defined __STL_MUTABLE
        owner_ = false; 
#  else
        __CONST_CAST(auto_array_ptr<X>*,this)->owner_ = false;
#  endif
        return ptr_; 
    }

private:
    void reset() { if (owner_) delete[] ptr_; owner_=0; }
    typedef element_type* pointer_type;
    pointer_type ptr_;
    mutable bool owner_;
#  if defined __STL_FRIEND_TEMPLATES
    template<class Y> friend class auto_array_ptr;
#  endif
};

__END_STL_NAMESPACE


#if 0
 
#if defined(__WATCOM_CPLUSPLUS__) && (__WATCOM_CPLUSPLUS__ <= 1100)
#define NO_MEMBER_TEMPLATES
#endif

/*
 * This version was downloaded from http://www.aw.com/cp/mec++-app-update.html
 * It was written by Greg Colvin
 */

template<class X>
class auto_ptr {
		mutable bool owner;
		X* px;
#if !defined(NO_MEMBER_TEMPLATES)
		template<class Y> friend class auto_ptr;
#endif
	public:
		explicit auto_ptr(X* p=0) 
			: owner(p != 0), px(p) {}

#if !defined(NO_MEMBER_TEMPLATES)
		template<class Y>
		auto_ptr(const auto_ptr<Y>& r) 
			: owner(r.owner), px(r.release()) {}

		template<class Y>
		auto_ptr& operator=(const auto_ptr<Y>& r) {
			if ((void*)&r != (void*)this) {
				if (owner) 
					delete px;
				owner = r.owner; 
				px = r.release();
			}
			return *this;
		}

		template<class Y>
		auto_ptr<X>& operator=(Y* r)
		{
			if(owner && ((void*)px != (void*) r))
				delete px;
			owner = r;
			px = r;
			return *this;
		}
#else
		auto_ptr(const auto_ptr<X>& r) 
			: owner(r.owner), px(r.release()) {}


		auto_ptr<X>& operator=(const auto_ptr<X>& r) 
		{
			if (&r != this) 
			{
				if (owner) 
					delete px;
				owner = r.owner; 
				px = r.release();
			}
			return *this;
		}
		
		auto_ptr<X>& operator=(X* r)
		{
			if(owner)
				delete px;
			owner = (r != 0);
			px = r;
			return *this;
		}
#endif

		~auto_ptr()           { if (owner) delete px; }
		X& operator*()  const { return *px; }
		X* operator->() const { return px; }
		X* get()        const { return px; }
		X* release()    const { owner = 0; return px; }
};


#if 0	// New version replaces this
// Watcom Compiler doesn't support explicit keyword yet

#if defined(__WATCOM_CPLUSPLUS__) && (__WATCOM_CPLUSPLUS__ <= 1060)
#define explicit
#endif

#if defined(__WATCOM_CPLUSPLUS__) && (__WATCOM_CPLUSPLUS__ <= 1100)
#define NO_MEMBER_TEMPLATES
#endif


template<class T>
class auto_ptr {
 public:
	explicit auto_ptr(T* p = 0) : pointee(p) { }

#if !defined(NO_MEMBER_TEMPLATES)
	template<class U>
	auto_ptr(auto_ptr<U>& rhs) : pointee(ths.release()) { }
#endif

	~auto_ptr() { delete pointee; }

#if !defined(NO_MEMBER_TEMPLATES)
	template<class U>
	auto_ptr<T>& operator =(auto_ptr<U>& rhs)
	{
		if(this != &rhs)
			reset(rhs.release());
		return *this;
	}
#endif

	auto_ptr<T>& operator = (T* p) { reset(p); return *this; }

	auto_ptr<T>& operator =(auto_ptr<T>& rhs)
	{
		if(this != &rhs)
			reset(rhs.release());
		return *this;
	}

	T& operator*() const { return *pointee; }
	T* operator->() const { return pointee; }
	T* get() const { return pointee; }

	T* release()
	{
		T* oldPointee = pointee;
		pointee = 0;
		return oldPointee;
	}

	void reset(T* p = 0)
	{
		if(p != pointee)
			delete pointee;
		pointee = p;
	}

	int operator ! () const { return pointee == 0; }

 private:
 	T* pointee;
};


template<class T>
bool operator == (const auto_ptr<T> lhs, const T* rhs)
{
	return lhs.pointee == rhs;
}

template<class T>
bool operator == (const auto_ptr<T> lhs, const auto_ptr<T> rhs)
{
	return lhs.pointee == rhs.pointee;
}

template<class T>
bool operator != (const auto_ptr<T> lhs, const T* rhs)
{
	return lhs.pointee != rhs;
}

template<class T>
bool operator != (const auto_ptr<T> lhs, const auto_ptr<T> rhs)
{
	return lhs.pointee != rhs.pointee;
}
#endif
#endif

#endif /* AUTOPTR_HPP */

