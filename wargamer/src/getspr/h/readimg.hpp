/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef READIMG_HPP
#define READIMG_HPP

#ifndef __cplusplus
#error readimg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Read Image from File
 *
 *----------------------------------------------------------------------
 */

#include "types.h"
#include "assert.h"
#include "except.hpp"
#include "palette.hpp"
#include "imgread.hpp"


class Image : private ImageReaderInterface
{
		UBYTE* d_bits;
		UWORD	 d_storageWidth;
		UWORD  d_width;
		UWORD  d_height;
		Color  d_transparent;
		Palette d_palette;
	public:
		Image(const char* fname);
		~Image();

		const PaletteEntry& getColor(Color c) const { return d_palette[c]; }

		Color& getPixel(int x, int y);
		Color getPixel(int x, int y) const;

		int width() const { return d_width; }
		int height() const { return d_height; }

		const Palette& getPalette() const { return d_palette; }
		Palette& getPalette() { return d_palette; }

		Color transparent() const { return d_transparent; }

		class ImageReadError : public GeneralError
		{
			public:
				ImageReadError() : GeneralError("Error Reading Image") { }
		};

	private:
		bool read(const char* fname);
		void destroy();

		/*
		 * Image Reader functions
		 */

		UBYTE* init(UWORD width, UWORD height);
		void setTransparent(Color c) { d_transparent = c; }
		UWORD getStorageWidth() const { return d_storageWidth; }

};

#endif /* READIMG_HPP */

