/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FONT_H
#define FONT_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Font File header
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	IBYTE height;
	IWORD firstChar;
	IWORD howMany;
	IWORD streamSize;
} FontHeaderD;

/*
 * This is followed on disk by:
 *   IBYTE widths[howMany];
 *   IBYTE bitStream[streamSize];
 */



#ifdef __cplusplus
};
#endif

#endif /* FONT_H */

