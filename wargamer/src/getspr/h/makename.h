/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef H_MAKENAME
#define H_MAKENAME
/*
 * $Id$
 *
 * Filename handling function header prototype
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 * 
 *    Rev 1.1   25 Jun 1993 15:44:18   sgreen
 * No change.
 * 
 *    Rev 1.0   28 Apr 1993 16:33:56   sgreen
 * Initial revision.
 * Revision 1.1  1992/10/17  00:12:23  sgreen
 * Initial revision
 *
 */

#ifndef H_TYPES
#include "types.h"
#endif

String makeFilename(const String& src, const String& ext, bool force);

#endif
