/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *-------------------------------------------------------------
 * $Id$
 *-------------------------------------------------------------
 *
 * Utility program to grab sprites from LBM screens
 * Originally written for Napoleon
 *
 * It started life as Mark Mccubbin's tile grabber program
 * but I wanted to make so many changes that it was difficult
 * to maintain backwards compatibility, so decided to give
 * it a new name and continue development unhindered by the
 * past.
 *
 * (C) 1993,1994 Steven Green and Dagger Interactive Technologies Ltd
 * (C) 1997 Steven Green
 *
 * Updated to read BMPs, new options for transparency and anchors
 * Partly converted to use C++ and less global variables
 *
 * Portions of this code (c)1992 MICROPROSE UK
 * Written by Steven Green
 * Based on code by Mark McCubbins
 *
 *-------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 * Revision 1.2  19 Dec 1994 Steven Green
 * Anchor spots can be anywhere within the sprite.
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 * 
 *    Rev 1.2   25 Jun 1993 15:44:26   sgreen
 * No change.
 * 
 *    Rev 1.1   01 Jun 1993 19:24:48   sgreen
 * Shadow + Anchor sets fullHeight to top of shadow
 * 
 *    Rev 1.0   28 Apr 1993 16:33:48   sgreen
 * Initial revision.
 * Revision 1.15  1992/12/17  11:08:10  sgreen
 * Nybble packing works line by line so that each line
 * is aligned.
 *
 * Revision 1.14  1992/12/11  12:12:42  sgreen
 * Remap flag is set in header correctly.
 *
 * Revision 1.13  1992/12/10  10:40:46  sgreen
 * Debugged color remapping
 *
 * Revision 1.12  1992/12/09  16:54:56  sgreen
 * Colour remap table written for every nybble packed sprite.
 *
 * Revision 1.11  1992/11/03  10:46:36  sgreen
 * Anchor Color ignored in nybble colour remapping
 *
 * Revision 1.10  1992/10/28  02:27:38  sgreen
 * anchors are printed as signed values in def file
 *
 * Revision 1.9  1992/10/27  23:17:16  sgreen
 * Writes extra information to DEF file:
 *   x y fullWidth fullHeight xOffset yOffset width height xAnchor yAnchor
 * The information is for user information, and only the 1st 4 fields are
 * used when reading a def file.
 *
 * Revision 1.8  1992/10/27  00:39:50  sgreen
 * An anchor point has been added to the sprite structure, replacing the
 * old baseY.  The anchor point defaults to being the bottom centre of the
 * sprite, but can be set by placing a dot in the top border line (X) and
 * left border (Y).
 *
 * Revision 1.7  1992/10/23  16:23:06  sgreen
 * Background is taken from ILBM file if not specified with /b option
 *
 * Revision 1.6  1992/10/23  14:41:47  sgreen
 * Fixed bug in Squeeze function which stopped it working when squeezed
 * width was 1.
 *
 * Revision 1.5  1992/10/21  23:06:57  sgreen
 * Anchor Option added as a way of positioning sprites
 * temporary file removed after writing final files
 *
 * Revision 1.4  1992/10/20  21:39:41  sgreen
 * Fixed problem with vertically overlapping rectangles.
 * Added a few dots to see what is happening
 * Fixed error with saving of flags
 *
 * Revision 1.3  1992/10/19  20:45:26  sgreen
 * Def File problems fixed
 * Output file created in MCC/SQZ/SQS and my new SPR formats
 * Squeeze function added to remove white space
 * Shadow function added for baseY
 * Nybble versions added
 *
 * Revision 1.2  1992/10/17  00:11:31  sgreen
 * Most of the higher level stuff is done (command line, file opening, etc)
 * The basic tile grabbing works including the use of DEF files.
 *
 * Revision 1.1  1992/10/15  23:46:00  sgreen
 * Initial revision
 *
 *-------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <conio.h>
#include <sys/stat.h>
#include <string>

#include "types.h"
#include "swgspr.hpp"
#include "font.h"
// #include "ilbm.h"
#include "readimg.hpp"
#include "makename.h"
#include "verbose.h"
#include "lzhuf.h"
#include "autoptr.hpp"
#include "tmpfile.hpp"

using namespace SWG_Sprite;

/*
 * Error values that may be returned by functions
 */

enum MainError {
	ERROR_OK,
	ERROR_BADPARAMETER,
	ERROR_MISSING_LIST_FILE,
	ERROR_INTERNAL,
	ERROR_CANT_CREATE_TMPFILE,
	ERROR_WRITING_TMPFILE,
	ERROR_READING_TMPFILE,
	ERROR_BAD_CLOSE_TMPFILE,
	ERROR_CANT_CREATE_PALFILE,
	// ERROR_WRITING_PALFILE,
	// ERROR_READING_PALFILE,
	ERROR_BAD_CLOSE_PALFILE,
	ERROR_CANT_LOAD_ILBM,
	ERROR_CLOSING_DEFFILE,
	ERROR_OPENING_DEFFILE,
	ERROR_NOMEMORY,
	ERROR_WRITING_OUTPUT,
	ERROR_MORE_THAN_16_COLORS,
	ERROR_WRITING_RMP,

	ERROR_UNKNOWN_LANGUAGE,

	ERROR_LAST_ERROR
};

class SPR_Error
{
		MainError d_err;
	public:
		SPR_Error(MainError err) : d_err(err) { }
		~SPR_Error() { }

		MainError error() const { return d_err; }
		virtual const char* errorString() const { return "No Description"; }
};

class LanguageError : public SPR_Error
{
	public:
		LanguageError() : SPR_Error(ERROR_UNKNOWN_LANGUAGE) { }
		virtual const char* errorString() const { return "Unknown language"; }
};
/*
 * Options and settings structure
 */

struct OPTIONS {
	bool useDefFile;
	bool squeeze;
	bool shadow; 						/* Work out shadow offset */
	bool useAnchor;	  				/* Work out anchorpoint with anchorColor */
	bool nybble;						/* Output should be nybble packed */
	bool remap;							/* Nybble screens should be remapped */
	bool rle;							/* Image is RLE encoded */
	bool lzHuf;							/* Attempt to Apply lzHuf compression */
	bool givenBackground;			/* Background colour was given by user */
	bool colorKey;					 	/* 1st few pixels of image are used for background, border, anchor, shadow */
	bool anchorBorder;				// Use anchor color to specify fullWidth/Height
	UBYTE backGroundColor;			// Transparent color
	UBYTE borderColor;				// Border color
	UBYTE shadowColor;
	UBYTE anchorColor;	 			/* Color of pixel to indicate Y position */
	UWORD fontStart;
	enum {
		OUT_SWG,
#ifdef MCC_FORMAT
		OUT_MCC,
#endif
		OUT_FONT
	} outputFormat;

	enum Language
	{
		LANG_C,
		LANG_CPP,
		LANG_ASM	 ,

		LANG_HowMany
	} language;

	bool wantIncludeFile;
};

static OPTIONS globalOptions;	/* Global options restored for each file */
static OPTIONS options;			/* Temporary options changeable for each file */

static std::auto_ptr<Image> image;
static int paletteCount = 0;			// Palette Index (0=none)

/*
 * Variables and constants
 */

// char sprite_id[] = SPRITE_ID;
#define sprite_id SWG_Sprite::SPRITE_ID

static const char whiteSpace[] = " \t\r\n";




static TmpFile tmpFile("getspr.tmp");
static TmpFile palFile("getspr.pal");

#if 0
FILE *outFD = NULL;						/* File handle for temporary outfile */
const char *tmpFileName = "getspr.tmp";	/* Filename used for temporary files */
const char* tmpPalName = "getspr.pal";
FILE* palFD = NULL;
#endif

FILE *defFD = NULL;						/* Def File */
String defFileName;				/* Name of DEF file */

char *includeFile = NULL;
FILE *hFD = NULL;

enum { READING, WRITING } defFileMode = WRITING;

int tileCount = 0;
UBYTE fontHeight = 0;

typedef struct {
	SpriteHeader header;
	ULONG dataSize;
	REMAP_TABLE remapTable;
} TmpSpriteHeader;

ULONG uncompressedSize = 0;
ULONG compressedSize = 0;

/*
 * Replacement for strdup that uses new/delete
 */

char* strdup(const char* s)
{
	size_t l = strlen(s) + 1;
	char* buffer = new char[l];
	memcpy(buffer, s, l);
	return buffer;
}


/* Close output file if it is open and then delete it */

void cleanup()
{
#if 0	// done automatically in static destructors
	if(outFD)
	{
		fclose(outFD);
		outFD = NULL;
	}

	if(palFD)
	{
		fclose(palFD);
		palFD = NULL;
	}

	remove(tmpFileName);
	remove(tmpPalName);
#endif
}

/*-------------------------------------------
 * Find what version of the program this is!
 */

char *getVersion(void)
{
	static char rcsRevision[] = "$Revision$";
	char *start;
	char *s;

	/* Find first digit */

	s = rcsRevision;
	while(*s && !isdigit(*s))
		s++;
	start = s;

	/* Find end of version */

	while(*s && (*s != ' ') && (*s != '$'))
		s++;
	*s = 0;

	return start;
}

/*
 * Introduce ourselves
 */

#define bannerWidth 76

void cPrintf(char *fmt, ...)
{
	va_list vaList;
	char buffer[120];
	int count;
	int spaces;

	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

	spaces = bannerWidth - strlen(buffer);
	if(spaces < 0)
		spaces = 0;
	printf("|");
	count = spaces / 2;
	spaces -= count;
	while(count--)
		printf(" ");
	printf(buffer);
	while(spaces--)
		printf(" ");
	printf("|\n");
}

void bannerLine(void)
{
	int count = bannerWidth;
	printf("+");
	while(count--)
		printf("-");
	printf("+\n");
}

void banner(void)
{
	printf("\n");
	bannerLine();
	cPrintf("Sprite Extraction Utility version %s", getVersion());
	cPrintf("Copyright (c) 1992-1997 Steven Morle-Green");
	cPrintf("Compiled on " __DATE__ " at " __TIME__);
#if defined(__BCPLUSPLUS__)
	cPrintf("Compiled with Borland C++ version %d.%d",
		(__BCPLUSPLUS__ >> 8) & 0xff, __BCPLUSPLUS__ & 0xff);
#elif defined(__BORLANDC__)
	cPrintf("Compiled with Borland C version %d.%d",
		(__BORLANDC__ >> 8) & 0xff, __BORLANDC__ & 0xff);
#elif defined(__WATCOMC__)
	cPrintf("Compiled with Watcom C version %d.%02d",
		__WATCOMC__  / 100, __WATCOMC__ % 100);
#endif
	bannerLine();
	printf("\n");
}

/*
 * Ask the user to press a key
 */

void waitKey(void)
{
	cPrintf("Press a key to continue");
	if(getch() == 0)	/* Extended key if 0 */
		getch();
}


/*
 * Create string from a filename, with the path stripped off
 */

void stripFileName(const char* src, String& dest, String& destBase)
{
	const char* s1 = strrchr(src, '\\');
	if(s1 == 0)
		s1 = strrchr(src, ':');
	if(s1 == 0)
		s1 = src;
	else
		++s1;
	dest = s1;

	while( (*s1 != 0) && (*s1 != '.') )
		destBase += *s1++;
}

struct LanguageStrings
{
	const char* ext;
	const char* extH;
	const char* commentStart;
	const char* commentMid;
	const char* commentEnd;
	const char* ifndef;
	const char* define;
	const char* endif;
};

static const LanguageStrings languageStrings[OPTIONS::LANG_HowMany] =
{
	{ 	// C
		".H",
		"_H",
		"/*", 
		" *", 
		" */",
		"#ifndef %s",
		"#define %s",
		"#endif\t/* %s */",
	},
	{ 	// C++
		".HPP",
		"_HPP",
		0,
		"//",
		0 ,
		"#ifndef %s",		//	"#if !defined(%s)",
		"#define %s",
		"#endif\t// %s",
	},
	{ 	// ASM
		".S",
		"_S",
		0,
		";",
		0,
		"ifndef %s",
		"%s equ 1",
		"endif",
	}
};

/*
 * Frequently used strings
 */

// made from header filename

static String includeDefineName;		// NAME_HPP
static String includeFileName;		// name.ext  (path is stripped)

// made from graphics filename

static String graphicFileName;		// name (extension is stripped)
static String graphicBaseName;		// name (extension is stripped)
static String graphicEnumName;		// NameSpriteIndex


void startHFile(char* s, char* outName)
{
	if(options.language >= OPTIONS::LANG_HowMany)
		throw LanguageError();
	const LanguageStrings* lStr = &languageStrings[options.language];

	// char fileName[FILENAME_MAX];
	String fileName = makeFilename(s, lStr->ext, false);

	report(V_VERBOSE, "Writing Header File %s\n", fileName);

	/*
	 * Strip path name
	 */

	String includeBaseName;
	stripFileName(fileName, includeFileName, includeBaseName);

#ifdef _WATCOM_C
	includeDefineName = includeBaseName.upper();
#else
        includeDefineName = includeBaseName;
        for(String::iterator it(includeDefineName.begin()); it != includeDefineName.end(); ++it)
        {
            *it = toupper(*it);
        }
                                              
#endif
	includeDefineName += lStr->extH;

	/*
	 * Strings made from outName
	 */

	stripFileName(outName, graphicFileName, graphicBaseName);
	graphicBaseName[0] = toupper(graphicBaseName[0]);
	graphicEnumName = graphicBaseName + "SpriteIndex";

	report(V_DEBUG, "includeDefineName = %s\n", includeDefineName.c_str());
	report(V_DEBUG, "includeFileName = %s\n", includeFileName.c_str());
	report(V_DEBUG, "graphicFileName = %s\n", graphicFileName.c_str());
	report(V_DEBUG, "graphicBaseName = %s\n", graphicBaseName.c_str());
	report(V_DEBUG, "graphicEnumName = %s\n", graphicEnumName.c_str());

	hFD = fopen(fileName, "w");

	if(hFD == NULL)
	{
		throw GeneralError("Can't create %s", fileName);
	}

	if(lStr->commentStart)
		fprintf(hFD, "%s\n", lStr->commentStart);
	fprintf(hFD, "%s====================================================\n", lStr->commentMid);
	fprintf(hFD, "%s $ID: %s $\n", lStr->commentMid, includeFileName.c_str());
	fprintf(hFD, "%s====================================================\n", lStr->commentMid);
	fprintf(hFD, "%s\n", lStr->commentMid);
	fprintf(hFD, "%s Sprite Definitions for %s\n", lStr->commentMid, graphicFileName.c_str());
	fprintf(hFD, "%s\n", lStr->commentMid);
	fprintf(hFD, "%s Warning: Do NOT EDIT this file\n", lStr->commentMid);
	fprintf(hFD, "%s It was generated automatically by GetSpr version %s\n", lStr->commentMid, getVersion());
	fprintf(hFD, "%s\n", lStr->commentMid);
	fprintf(hFD, "%s====================================================\n", lStr->commentMid);
	if(lStr->commentEnd)
		fprintf(hFD, "%s\n", lStr->commentEnd);
	fputs("\n", hFD);

	fprintf(hFD, lStr->ifndef, includeDefineName.c_str());
	fputs("\n", hFD);
	fprintf(hFD, lStr->define, includeDefineName.c_str());
	fputs("\n\n", hFD);

	if(options.language == OPTIONS::LANG_CPP)
	{
		fputs("#ifndef __cplusplus\n", hFD);
		fprintf(hFD, "#error %s is for use with C++\n", includeFileName.c_str());
		fputs("#endif\n\n", hFD);
	}
	else if(options.language == OPTIONS::LANG_C)
	{
		fputs("#ifdef __cplusplus\n", hFD);
		fputs("extern \"C\" {\n", hFD);
		fputs("#endif\n\n", hFD);
	}

	/*
	 * Put some explanatory comments
	 */

	if(lStr->commentStart)
		fprintf(hFD, "%s\n", lStr->commentStart);
	fprintf(hFD, "%s======================================================================\n", lStr->commentMid);
	fprintf(hFD, "%s Each processed graphic file generates 3 values:\n", lStr->commentMid);
	fprintf(hFD, "%s   Name       : Index of the 1st image from this graphic\n", lStr->commentMid);
	fprintf(hFD, "%s   Name_Last  : Index of the next index following this graphic\n", lStr->commentMid);
	fprintf(hFD, "%s   Name_Count : Number of images in this graphic\n", lStr->commentMid);
	fprintf(hFD, "%s\n", lStr->commentMid);
	fprintf(hFD, "%s Important: Name_Last is not the last image, but the one following it\n", lStr->commentMid);
	fprintf(hFD, "%s\n", lStr->commentMid);
	fprintf(hFD, "%s There is also a EnumName_HowMany value containing the total number\n", lStr->commentMid);
	fprintf(hFD, "%s of images in the graphic\n", lStr->commentMid);
	fprintf(hFD, "%s======================================================================\n", lStr->commentMid);
	if(lStr->commentEnd)
		fprintf(hFD, "%s\n", lStr->commentEnd);
	fputs("\n", hFD);

	/*
	 * start enumeration for C++
	 */

	if(options.language == OPTIONS::LANG_CPP)
	{
		fprintf(hFD, "enum %s\n", graphicEnumName.c_str());
		fputs("{\n", hFD);
	}
}

void endHFile()
{
	if(hFD)
	{
		if(options.language >= OPTIONS::LANG_HowMany)
			throw LanguageError();

		const LanguageStrings* lStr = &languageStrings[options.language];

		if(options.language == OPTIONS::LANG_CPP)
		{
			fprintf(hFD, "\t%s_HowMany = %d\n", graphicEnumName.c_str(),  tileCount);
			fprintf(hFD, "}; // enum %s\n\n", graphicEnumName.c_str());
		}
		else
		{
			if(lStr->commentStart)
				fputs(lStr->commentStart, hFD);
			else
				fputs(lStr->commentMid, hFD);
			fprintf(hFD, " Total number of sprites = %d ", tileCount);
			if(lStr->commentEnd)
				fputs(lStr->commentEnd, hFD);
			fputs("\n\n", hFD);
		}

		if(options.language == OPTIONS::LANG_C)
		{
			fputs("#ifdef __cplusplus\n", hFD);
			fputs("};\n", hFD);
			fputs("#endif\n", hFD);
			fputs("\n\n", hFD);
		}

		fprintf(hFD, lStr->endif, includeDefineName.c_str());
		fputs("\n", hFD);

		fclose(hFD);
	}
}


/*
 * DEF file handling
 */

/*
 * Make up a name for def file
 * if chop is set then any extension should be ignored
 * otherwise it has been provided by the user
 */

void makeDefFileName(char *s, bool chop)
{
	// char buffer[FILENAME_MAX];

	// String buffer 
        defFileName = makeFilename(s, ".def", chop);

	// defFileName = buffer;
}


MainError closeDefFile(void)
{
	MainError errorNum = ERROR_OK;

	if(defFD != NULL)
	{
		if(fclose(defFD) != 0)
			errorNum = ERROR_CLOSING_DEFFILE;
		defFD = NULL;
	}
	return errorNum;
}

MainError openDefFile(void)
{
	MainError errorNum = ERROR_OK;

	if(options.useDefFile && (defFD == NULL))		/* If not already open */
	{
		/* See if file exists */

		// char fileName[FILENAME_MAX];
		struct stat statBuf;

                String fileName = makeFilename(defFileName, "*.DEF", true);
		// makeFilename(fileName, defFileName, ".DEF", true);

		if(stat(fileName, &statBuf) == 0)		/* It exists */
		{
			report(V_QUIET, "Reading Def File %s\n", fileName);
			defFileMode = READING;
			defFD = fopen(fileName, "r");
		}
		else
		{
			report(V_QUIET, "Writing Def File %s\n", fileName);
			defFileMode = WRITING;
			defFD = fopen(fileName, "w");
		}
		if(defFD == NULL)
		{
			error(0, fileName, NULL, "Couldn't open Def File\n");
			errorNum = ERROR_OPENING_DEFFILE;
		}
	}

	return errorNum;
}

/*
 * Write the actual data to disk
 * The buffer always has the data moved to the top left occupying
 * width,height
 */

void writeData(TmpSpriteHeader *sprite, UBYTE *data)
{
	tmpFile.write(data, sprite->dataSize);
}

/*
 * Pixel manipulation functions... defined as macros for speed
 */


inline Color getPixel(int x, int y)
{
	assert(image.get() != 0);
	return image->getPixel(x, y);
}

inline void putPixel(int x, int y, Color c)
{
	assert(image.get() != 0);
	image->getPixel(x, y) = c;
}

inline bool isBackground(int x, int y)
{
	return getPixel(x, y) == options.backGroundColor;
}

/*
 * Remove white space from a sprite
 */

void squeeze(TmpSpriteHeader *sprite, UBYTE *data)
{
	UWORD top, bottom, left, right;

	/* Remove from top */

	UWORD x,y;
	UBYTE *line = data;
	bool found = false;
	y = 0;
	while(!found && (y < sprite->header.height))
	{
		x = 0;
		while(x < sprite->header.width)
		{
			if(*line++ != options.backGroundColor)		//--- was 0
			{
				found = true;
				break;
			}
			x++;
		}
		if(!found)
			y++;
	}

	top = y;

	/* Remove from bottom */

	y = sprite->header.height;
	found = false;

	line = data + y * sprite->header.width;
	while(!found && (--y > top))
	{
		x = sprite->header.width;

		while(x--)
		{
			if(*--line != options.backGroundColor)		//--- was 0
			{
				found = true;
				break;
			}
		}
	}
	bottom = y;

	/* Remove left */

	found = false;
	x = 0;
	while(!found && (x < sprite->header.width))
	{
		y = top;
		for(y = top; y <= bottom; y++)
		{
			if(data[y * sprite->header.width + x] != options.backGroundColor)		//--- was 0
			{
				found = true;
				break;
			}
		}
		if(!found)
			x++;
	}
	left = x;

	/* Remove right */

	x = sprite->header.width;
	found = false;
	while(!found && (--x > left))
	{
		y = top;
		while(y < bottom)
		{
			if(data[y * sprite->header.width + x] != options.backGroundColor)		//--- was 0
			{
				found = true;
				break;
			}
			y++;
		}
	}			  
	right = x;

	/* Shift data to top left */

	line = data;		/* Where data must go to */
	y = top;
	while(y <= bottom)
	{
		x = left;
		while(x <= right)
		{
			*line++ = data[y * sprite->header.width + x];
			x++;
		}
		y++;
	}

	/* Update header */

	sprite->header.xOffset = left;
	sprite->header.yOffset = top;
	sprite->header.width = right - left + 1;
	sprite->header.height = bottom - top + 1;
	sprite->header.anchorX -= left;
	sprite->header.anchorY -= top;

	if(options.useAnchor && options.shadow)
		sprite->header.fullHeight -= top;

	sprite->dataSize = sprite->header.width * sprite->header.height;

	report(V_VERBOSE, "Squeezed from 0,0,%d,%d to %d,%d,%d,%d\n",
				sprite->header.fullWidth, sprite->header.fullHeight,
				sprite->header.xOffset, sprite->header.yOffset,
				sprite->header.width, sprite->header.height);
}

/*
 * Calculate baseY based on shadow
 */

void shadow(TmpSpriteHeader *sprite, UBYTE *data)
{
	/* Remove from bottom */

	UWORD y = sprite->header.height;
	bool found = false;

	UBYTE *line = data + y * sprite->header.width;
	while(!found && --y)
	{
		UWORD x = sprite->header.width;

		while(x--)
		{
			UBYTE val = *--line;
			if( (val != 0) && (val != options.shadowColor) )
			{
				found = true;
				break;
			}
		}
	}
	sprite->header.anchorY = y;

	report(V_VERBOSE, "shadowed bottom anchor is %d\n", sprite->header.anchorY);
}

void anchor(TmpSpriteHeader *sprite, UBYTE *data)
{
	bool xAnchorFound = false;
	bool yAnchorFound = false;
	UWORD y = 0;

	int minX = 0;
	int maxX = 0;
	int minY = 0;
	int maxY = 0;


	while(y < sprite->header.height)
	{
		UWORD x = 0;

		while(x < sprite->header.width)
		{
			UBYTE val = *data;
			if(val == options.anchorColor)
			{
				*data = options.backGroundColor;		/* Replace pixel with 0 */

				/*
				 * If it is not at the side then it can be used as an xAnchor
				 */

				if( (y != 0) && (y != (sprite->header.height - 1)) )
				{
					if(!yAnchorFound)
					{
						minY = y;
						maxY = y;
						yAnchorFound = true;
					}
					else if(y < minY)
						minY = y;
					else if(y > maxY)
						maxY = y;

					// sprite->header.anchorY = y;
					report(V_DEBUG, "Y Anchor is at %d\n", sprite->header.anchorY);
					// yAnchorFound = true;
				}

				if( (x != 0) && (x != (sprite->header.width - 1)) )
				{
					if(!xAnchorFound)
					{
						minX = x;
						maxX = x;
						xAnchorFound = true;
					}
					else if(x < minX)
						minX = x;
					else if(x > maxX)
						maxX = x;

					// sprite->header.anchorX = x;
					report(V_DEBUG, "X Anchor is at %d\n", sprite->header.anchorX);
					// xAnchorFound = true;
				}
#if 0
				if(y == 0)		/* if on top line then it is the X anchor */
				{
					sprite->header.anchorX = x;
					report(V_VERBOSE, "X Anchor is at %d\n", sprite->header.anchorX);
					anchorFound = true;
				}
				else
				{
					sprite->header.anchorY = y;
					report(V_VERBOSE, "Y Anchor is at %d\n", sprite->header.anchorY);
					return;
				}
#endif

			}
			data++;
			x++;
		}
		y++;
	}

	if(yAnchorFound)
	{
		sprite->header.anchorY = (minY + maxY + 1) / 2;
		report(V_VERBOSE, "Y Anchor set to (%d,%d) => %d\n",
			(int) minY, (int) maxY, (int) sprite->header.anchorY);
	}
	else
		report(V_VERBOSE, "No Y Anchor pixel was found\n");

	if(xAnchorFound)
	{
		sprite->header.anchorX = (minX + maxX + 1) / 2;
		report(V_VERBOSE, "X Anchor set to (%d,%d) => %d\n",
			(int) minX, (int) maxX, (int) sprite->header.anchorX);
	}
	else
		report(V_VERBOSE, "No X Anchor pixel was found\n");

	if(xAnchorFound && yAnchorFound && options.anchorBorder)
	{
		int fullWidth = maxX - minX - 1;

		if(fullWidth > 0)
			sprite->header.fullWidth = fullWidth;
		else
			throw GeneralError("X anchor size is zero");

		int fullHeight = maxY - minY - 1;

		if(fullHeight)
			sprite->header.fullHeight = fullHeight;
		else
			throw GeneralError("Y anchor size is zero");

		report(V_VERBOSE, "fullwidth/height set top anchor values of %d,%d\n", (int)sprite->header.fullWidth, (int)sprite->header.fullHeight);
	}
}

/*
 * Nybble pack a sprite
 *
 * the remap table will already have been created when the
 * lbm was loaded
 */

static UBYTE remapColors[256];	/* Space for all the colors */
static int maxRemapColor = 0;



void initRemap(void)
{
	maxRemapColor = 1;
	remapColors[0] = 0;

	report(V_DEBUG, "---------- Colour Remap Table ------------\n");
	report(V_DEBUG, "Color %2d: %3d ($%02x) => r=%3d, g=%3d, b=%3d [Transparent]\n",
		(int) 0, (int) 0, (int) 0,
		(int) image->getColor(0).s_red,
		(int) image->getColor(0).s_green,
		(int) image->getColor(0).s_blue);
		// ilbm_palette[0][0],
		// ilbm_palette[0][1],
		// ilbm_palette[0][2]);
}

MainError checkRemap(void)
{
	report(V_DEBUG, "------------------------------------------\n");
	if(maxRemapColor > RemapColorCount)
	{
		error(0, NULL, NULL,
			"Sprite has more than %d colours (%d) so can not be nybble packed\n",
			RemapColorCount, maxRemapColor);
		return ERROR_MORE_THAN_16_COLORS;
	}

 	return ERROR_OK;
}

UBYTE findRemap(UBYTE color)
{
	int i;

	/*
	 * Disable background and anchor colours from being
	 * remapped
	 */

	if(color == options.backGroundColor)
		color = 0;
	if(options.useAnchor && (color == options.anchorColor))
		color = 0;
	if(color == 0)
		return 0;

	i = 0;
	while(i < maxRemapColor)
	{
		if(remapColors[i] == color)
			return i;
		i++;
	}

	/*
	 * If it wasn't in the table then add it to the end
	 */


	if(maxRemapColor == RemapColorCount)
		report(V_DEBUG, "----------- Too Many Colours! ------------\n");
	report(V_DEBUG, "Color %2d: %3d ($%02x) => r=%3d, g=%3d, b=%3d",
		(int) maxRemapColor, (int) color, (int) color,
		(int) image->getColor(color).s_red,
		(int) image->getColor(color).s_green,
		(int) image->getColor(color).s_blue);
		// ilbm_palette[color][0],
		// ilbm_palette[color][1],
		// ilbm_palette[color][2]);

	remapColors[maxRemapColor++] = color;

	if(options.shadow && (color == options.shadowColor))
		report(V_DEBUG, " [Shadow]");

	report(V_DEBUG, "\n");

	return maxRemapColor - 1;
}

MainError nybblePack(TmpSpriteHeader *sprite, UBYTE *data)
{
	UBYTE *dest = data;
	UBYTE *src = data;
	int lineCount = sprite->header.height;
	int columns = sprite->header.width / 2;
	MainError error = ERROR_OK;

	report(V_VERBOSE, "Nybble Packing\n");

	if(options.remap)
		initRemap();

	sprite->dataSize = lineCount * columns;
	if(sprite->header.width & 1)
		sprite->dataSize += lineCount;

	while(lineCount--)
	{
		int columnCount = columns;

		while(columnCount--)
		{
			UBYTE col1, col2;

			col1 = *src++;
			col2 = *src++;
	
			if(options.remap)
			{
				col1 = findRemap(col1);
				col2 = findRemap(col2);
			}
			else
			{
				col1 &= 0xf;
				col2 &= 0xf;
			}

			*dest++ = ((col1 << 4) & 0xf0) | col2;
		}

		/* Handle remaining nybble */

		if(sprite->header.width & 1)
		{
			UBYTE col = *src++;

			if(options.remap)
				col = findRemap(col);
			else
				col &= 0xf;
			*dest++ = (col << 4);
		}
	}
	if(options.remap)
	{
		error = checkRemap();
		memcpy(sprite->remapTable, remapColors, sizeof(REMAP_TABLE));
	}

	return error;
}

#if 0
/*
 * Create the remap table
 */

MainError makeRemap(char *name)		/* Name to use as base for RMP */
{
	UBYTE *pic = ilbm_ptr;
	ULONG count = (long)bmhd.h * bmhd.w;

	maxRemapColor = 0;
	
	while(count--)
	{
		UBYTE color = *pic++;
		int colCount;
		bool found;

		/* See if its already in the table */

		if(color == options.backGroundColor)
			color = 0;
		if(options.useAnchor && (color == options.anchorColor))
			color = 0;

		colCount = 0;
		found = false;
		while(colCount < maxRemapColor)
		{
			if(remapColors[colCount] == color)
			{	
				found = true;
				break;
			}
			colCount++;
		}

		if(!found)	/* Its a new color */
		{
			if(maxRemapColor >= 16)
			{
				error(0, name, NULL, "Picture has more than 16 colours so can not be nybble packed\n");
				return ERROR_MORE_THAN_16_COLORS;
			}			
			remapColors[maxRemapColor++] = color;
			report(V_DEBUG, "Color %d: %d\n", maxRemapColor, color);
		}
	}

	/*
 	 * Write an RMP file
	 */

	{
		char rmpName[FILENAME_MAX];
		FILE *rmpFD;

		makeFilename(rmpName, name, ".RMP", true);

		report(V_QUIET, "Writing Color remap table %s\n", rmpName);

		rmpFD = fopen(rmpName, "w");
		if(rmpFD != NULL)
		{
			int i;
			int xCount;

			if( (options.language == LANG_C) ||
				 (options.language == LANG_CPP ))
			{
				fprintf(rmpFD, "/* Color remap table for %s */\n", name);
				fprintf(rmpFD, "{");
			}
			else if(options.language == LANG_ASM)
			{
				fprintf(rmpFD, "; Color remap table for %s\n", name);
			}
			i = 0;
			xCount = 0;
			while(i < 16)
			{
				if(xCount == 0)
				{
					if((options.language == LANG_C) ||
					 	(options.language == LANG_CPP))
						fprintf(rmpFD, "\n\t");
					else if(options.language == LANG_ASM)
						fprintf(rmpFD, "\n\t\tdb\t");
					xCount = 7;
				}
				else
				{
					fprintf(rmpFD, ", ");
					xCount--;
				}
				if((options.language == LANG_C) ||
				 	(options.language == LANG_CPP))
					fprintf(rmpFD, "0x");
				else if(options.language == LANG_ASM)
					fprintf(rmpFD, "0");
				fprintf(rmpFD, "%02x", remapColors[i]);
				if(options.language == LANG_ASM)
					fprintf(rmpFD, "h");
				i++;
			}
			if((options.language == LANG_C) ||
			 	(options.language == LANG_CPP))
				fprintf(rmpFD, "\n}\n");
	 
			fclose(rmpFD);
		}
		else
		{
			error(0, rmpName, NULL, "Can't create color remap file\n");
			return ERROR_WRITING_RMP;
		}
	}

	return ERROR_OK;
}
#endif


/*
 * Actually process a tile
 */

MainError takeTile(int x, int y, int w, int h)
{
	assert(w != 0);
	assert(h != 0);

	MainError errorNum = ERROR_OK;

	tileCount++;
	report(V_VERBOSE, "Tile %3d: %3d,%3d -> %3d,%3d\n", tileCount, x, y, w, h);
	if(verbosity < V_VERBOSE)
		report(V_QUIET, ".");

	auto_array_ptr<UBYTE> tileBuffer(new UBYTE[w * h]);
	// tileBuffer = new UBYTE[w * h];	// malloc(w * h);
	if(tileBuffer.get() != 0)	// NULL)
	{
		/* Copy it to buffer and rub it off main screen */

		int y1 = y;
		int yCount = h;
		UBYTE *ptr = tileBuffer.get();

		TmpSpriteHeader header;

		while(yCount--)
		{
			int x1 = x;
			int xCount = w;

			while(xCount--)
			{
				UBYTE pixel = getPixel(x1, y1);

				// if(pixel == options.backGroundColor)
				if(pixel == options.borderColor)
					pixel = options.backGroundColor;
				*ptr++ = pixel;
				putPixel(x1, y1, options.backGroundColor);

				x1++;
			}

			y1++;
		}

		/*
		 * Initialise header
		 */

		header.dataSize = w * h;
		header.header.width = w;
		header.header.height = h;
		header.header.fullHeight = h;
		header.header.fullWidth = w;
		header.header.xOffset = 0;
		header.header.yOffset = 0;
		header.header.anchorY = h/2;
		header.header.anchorX = w/2;
		header.header.shadowColor = options.shadowColor;
#if 1
		header.header.transparentColor = options.backGroundColor;
#else
		header.header.transparentColor = 0;
#endif
		header.header.flags.lzHuf = false;
		header.header.flags.rle = options.rle;
		header.header.flags.nybble = options.nybble;
		header.header.flags.squeeze = options.squeeze;
		header.header.flags.shadow = options.shadow;
		header.header.flags.remap = options.remap;
		header.header.paletteIndex = paletteCount;

		/*
 		 * Do whatever processing is needed!
		 */

		if(options.shadow)
			shadow(&header, tileBuffer.get());
		if(options.useAnchor)
		{
			/*
			 *	Bodge so that if shadow and anchor is enabled
			 * full height is the height to the top of the shadow
			 */

			if(options.shadow)
				header.header.fullHeight = header.header.anchorY;

			anchor(&header, tileBuffer.get());
		}
		if(options.squeeze)
			squeeze(&header, tileBuffer.get());

		if(options.nybble)
		{
			errorNum = nybblePack(&header, tileBuffer.get());
			if(errorNum != ERROR_OK)
				goto processError;
		}

		/*
		 * Add info to def file
		 *
		 * x y w h xoffset yoffset w1 h1 xanchor yanchor
		 */

		if(options.useDefFile && (defFileMode == WRITING) && (defFD != NULL))
			fprintf(defFD, "%-3u %-3u %-3u %-3u %-3u %-3u %-3u %-3u %-3d %-3d\n",
				x, y, w, h,
				header.header.xOffset, header.header.yOffset,
				header.header.width, header.header.height,
				header.header.anchorX, header.header.anchorY);

		if(options.outputFormat == OPTIONS::OUT_FONT)
		{
			if(header.header.height > fontHeight)
			{
				assert(header.header.height <= UCHAR_MAX);
				fontHeight = static_cast<UBYTE>(header.header.height);
			}
		}

		/*
		 * Attempt to compress Image if in LZHuf mode
		 */

		if(options.lzHuf)
		{
			long compSize;
			
			/*
			 * Compress to a buffer
			 * Make buffer twice the size of the data to make sure it
			 * will not overflow
			 */

			report(V_VERBOSE, "LZHuffing data, %ld bytes", header.dataSize);

			auto_array_ptr<UBYTE> compBuffer(new UBYTE[header.dataSize * 2]);
			// compBuffer = malloc(header.dataSize * 2);
			compSize = encode_lzhuf_mm(tileBuffer.get(), compBuffer.get(), header.dataSize);

			report(V_VERBOSE, " -> %ld bytes (%d%%)",
				compSize,
				(int)(compSize * 100 / header.dataSize));

			/*
			 * If the compressed data is smaller then adjust header
			 */

			uncompressedSize += header.dataSize;

			if(compSize < header.dataSize)
			{
				report(V_VERBOSE, ", compressed\n");
				// free(tileBuffer);
				tileBuffer = compBuffer;
				header.dataSize = compSize;
				header.header.flags.lzHuf = true;
			}
			else
				report(V_VERBOSE, ", Store\n");

			compressedSize += header.dataSize;
		}

		/*
		 * Write out the sprite to temporary file
		 * This can be in machine dependant format!
		 */

		tmpFile.write(&header, sizeof(header));
#if 0
		{
			error(0, tmpFileName, NULL, "Error writing to temporary file\n");
			errorNum = ERROR_WRITING_TMPFILE;
		}
		else
			errorNum = writeData(&header, tileBuffer.get());
#endif
		writeData(&header, tileBuffer.get());

	processError:
		// free(tileBuffer);
		;
	}
	else
	{
		error(0, NULL, NULL, "Couldn't get memory for tile buffer\n");
		errorNum = ERROR_NOMEMORY;
	}

	return errorNum;
}



/*
 * Grab sprites using existing DEF file, which is already open
 */

MainError grabUsingDef(void)
{
	char buffer[120];
	int x,y,w,h;
	bool haveRead = false;
	MainError errorNum = ERROR_OK;

	while(fgets(buffer, 120, defFD) != NULL)
	{
		if(isdigit(buffer[0]))
		{
			sscanf(buffer, "%d%d%d%d", &x, &y, &w, &h);

			errorNum = takeTile(x, y, w, h);
			if(errorNum != ERROR_OK)
				return errorNum;

			haveRead = true;
		}
		else
		{
			if(haveRead)
				return errorNum;		/* Stop this file when we reach a file name */
		}
	}
	return errorNum;
}

/*
 * Scan the raw picture for tiles
 */

MainError grabTiles(void)
{
	int y;
	MainError errorNum = ERROR_OK;

	/* Scan from top to bottom for non-background pixel */

	// for(y = 0; y < bmhd.h; y++)
	for(y = 0; y < image->height(); y++)
	{
		int x;

		// for(x = 0; x < bmhd.w; x++)
		for(x = 0; x < image->width(); x++)
		{
			if(!isBackground(x, y))
			{
				/*
				 * A non-background pixel has been found
				 * Try to work out a rectangle containing connected pixels
				 */

				int minX, maxX, minY, maxY;
				int y1;

				minX = x;
				maxX = x;
				minY = y;
				maxY = y;

				// for(y1 = y + 1; y1 < bmhd.h; y1++)
				// for(y1 = y + 1; y1 < image->height(); y1++)
				for(y1 = y; y1 < image->height(); y1++)
				{
					int leftX, rightX;
					bool finished = false;

					rightX = 0;

					do
					{
						leftX = rightX;
						while((leftX <= maxX) && isBackground(leftX,y1))
							leftX++;

						if(leftX > maxX)
						{
							finished = true;
							break;
						}

						rightX = leftX;
						// while((rightX < bmhd.w) && !isBackground(rightX,y1))
						while((rightX < image->width()) && !isBackground(rightX,y1))
							rightX++;
					}
					// while(!finished && (rightX < bmhd.w) && (rightX < minX));
					while(!finished && (rightX < image->width()) && (rightX < minX));

					if(finished)
						break;

					if(rightX < minX)		/* Off bottom */
						break;

					// if(rightX >= bmhd.w)
					//	break;

					if(leftX < minX)
						minX = leftX;
					if(rightX > maxX)
						maxX = rightX;
				}			  
				maxY = y1;	

				/*
				 * We have a tile at (minX,minY) -> (maxX,maxY)
				 */

				errorNum = takeTile(minX, minY, maxX-minX, maxY-minY);
				if(errorNum != ERROR_OK)
					return errorNum;
			}
		}
	}
	return errorNum;
}


/*===================================================================
 * Process a file, sending a processed tile to the temporary output file
 */

MainError processFile(char *inName)
{
	MainError errorNum = ERROR_OK;

	/* Make sure temporary file is open */

	tmpFile.openWrite();
	palFile.openWrite();

#if 0
	if(outFD == NULL)
	{
		errorNum = openTmpFile();
  		if(errorNum != ERROR_OK)
	  		return errorNum;
	}

	if(palFD == NULL)
	{
		errorNum = openPalFile();
  		if(errorNum != ERROR_OK)
	  		return errorNum;
	}
#endif
  

 	report(V_QUIET, "Processing %s\n", inName);

	// if(ilbm_to_raw(inName) == 0)

	try
	{
		image.reset(new Image(inName));		// auto_ptr automatically deletes old one
		if(image.get() != 0)
		{
			++paletteCount;
			report(V_DEBUG, "Palette Index = %d\n", (int) paletteCount);

			const Palette& palette = image->getPalette();
			PaletteHeader palHead;
			palHead.nColors = palette.nColors();

			palFile.write(&palHead, sizeof(palHead));
			palFile.write(&palette[0], sizeof(PaletteEntry) * palette.nColors());

			char idName[FILENAME_MAX];
			int tileStart = tileCount;

			if(options.wantIncludeFile)
			{
				char* s = strrchr(inName, '\\');
				if(!s)
					s = strrchr(inName, ':');
				if(s)
					s++;
				else
					s = inName;

				char* s1 = idName;
				if(!isalpha(*s))
					*s1++ = '_';

				bool capNext = true;

				while(*s && (*s != '.'))
				{
					char c = *s++;

					if(isspace(c))
					{
						capNext = true;
					}
					else if(isalnum(c) || (c == '_'))
					{
						if(capNext)
						{
							c = toupper(c);
							capNext = false;
						}
						*s1++ = c;
					}
					else
					{
						capNext = true;
					}
				}
				*s1 = 0;

				if(options.language == OPTIONS::LANG_C)
					fprintf(hFD, "#define SPR_%s %d\n", idName, tileCount);
				if(options.language == OPTIONS::LANG_CPP)
					fprintf(hFD, "\t%s = %d,\n", idName, tileCount);
				if(options.language == OPTIONS::LANG_ASM)
					fprintf(hFD, "SPR_%s equ %d\n", idName, tileCount);

			}

			if(options.colorKey)
			{
				if(!options.givenBackground)
					options.backGroundColor = image->getPixel(0,0);

				options.borderColor = image->getPixel(1,0);
				image->getPixel(1,0) = options.backGroundColor;

				options.anchorColor = image->getPixel(2,0);
				image->getPixel(2,0) = options.backGroundColor;
				options.useAnchor = 
					(options.anchorColor != options.backGroundColor) &&
					(options.anchorColor != options.borderColor);

				options.shadowColor = image->getPixel(3,0);
				image->getPixel(3,0) = options.backGroundColor;
				options.shadow = 
					(options.shadowColor != options.backGroundColor) &&
					(options.shadowColor != options.borderColor);
			}
			else
			if(!options.givenBackground)
			{
				options.backGroundColor = image->transparent();	// bmhd.transparentColor;
				report(V_VERBOSE, "Taken background color as %d from %s\n",
					options.backGroundColor, inName);
			}

			if(options.useDefFile && (defFD == NULL))
				openDefFile();

	#if 0
			if(options.remap)
			{
				errorNum = makeRemap(inName);
				if(errorNum != ERROR_OK)
					return errorNum;
			}
	#endif

			if(options.useDefFile && (defFileMode == READING) && (defFD != NULL))
				errorNum = grabUsingDef();
			else
			{
				if(options.useDefFile && (defFileMode == WRITING) && (defFD != NULL))
					fprintf(defFD, "<%s\n", inName);
				errorNum = grabTiles();
			}
			if(verbosity < V_VERBOSE)
				report(V_QUIET, "\n");

			/*
			 * Report on number of images in this image
			 */

			if(options.wantIncludeFile)
			{
				int tileDiff = tileCount - tileStart;

				if(options.language == OPTIONS::LANG_C)
				{
					fprintf(hFD, "#define SPR_%s_Last %d\n", idName, tileCount);
					fprintf(hFD, "#define SPR_%s_Count %d\n\n", idName, tileDiff);
				}
				if(options.language == OPTIONS::LANG_CPP)
				{
					fprintf(hFD, "\t%s_Last = %d,\n", idName, tileCount);
					fprintf(hFD, "\t%s_Count = %d,\n\n", idName, tileDiff);
				}
				if(options.language == OPTIONS::LANG_ASM)
				{
					fprintf(hFD, "SPR_%s_LAST equ %d\n", idName, tileCount);
					fprintf(hFD, "SPR_%s_COUNT equ %d\n\n", idName, tileDiff);
				}
			}
		}
		else
			throw Image::ImageReadError();
	}
	catch(Image::ImageReadError e)
	{
		error(0, inName, NULL, "Could not load Image\n%s\n", e.get());
		errorNum = ERROR_CANT_LOAD_ILBM;
	}

	return errorNum;
}

/*
 * Copy data from one file to another
 */

MainError copyData(FILE *dest, FILE *src, size_t howMuch)
{
	while(howMuch--)
	{
		int c = fgetc(src);
		fputc(c, dest);
	}
	if(ferror(dest) || ferror(src))
		return ERROR_WRITING_OUTPUT;

	return ERROR_OK;
}

/*
 * Convert temporary file to SWG format
 */

MainError outputSWG(char *name)
{
	MainError errorNum = ERROR_OK;
	int err;
	// FILE *tmpFD;
	FILE *outFD;
	// char outName[FILENAME_MAX];

	String outName = makeFilename(name, ".SPR", true);

	report(V_QUIET, "Writing final file to %s\n", outName);

	/* Open output file */
	
	outFD = fopen(outName, "wb");
	if(outFD != NULL)
	{
		fpos_t offsetPos;
		fpos_t palOffsetPos;
		ILONG ilong;
		int count;

		/* Create the file header */

		SpriteFileHeaderD fileHead;

		memcpy(fileHead.id, sprite_id, sizeof(fileHead.id));
		putWord(&fileHead.headerSize, sizeof(fileHead));
		putByte(&fileHead.versionOrder, IsIntel | SPRITE_VERSION);
		putWord(&fileHead.spriteCount, tileCount);
		putWord(&fileHead.paletteCount, paletteCount);
		if(fwrite(&fileHead, sizeof(fileHead), 1, outFD) != 1)
		{
		outErr1:
			error(0, outName, NULL, "Error writing file header\n");
			goto outErr;
		}

		/* Write a dummy offset table */

		fgetpos(outFD, &offsetPos);		/* Position of 1st offset */
		count = tileCount;
		putLong(&ilong, 0);
		while(count--)
		{
			if(fwrite(&ilong, sizeof(ilong), 1, outFD) != 1)
				goto outErr1;
		}

		/*
		 * Write a dummy Palette Offset table
		 */

		fgetpos(outFD, &palOffsetPos);
		count = paletteCount;
		while(count--)
		{
			if(fwrite(&ilong, sizeof(ilong), 1, outFD) != 1)
				goto outErr1;
		}

		/* Open temporary file */

		tmpFile.openRead();	// tmpFD = fopen(tmpFileName, "rb");
		// if(tmpFD != NULL)
		{
			int count = tileCount;
			while(count--)
			{
				TmpSpriteHeader head;
				SpriteHeaderD dHead;
				fpos_t pos;
				ULONG dataSize;

				report(V_QUIET, ".");	/* Show progress */

				/* Update the offset table */

				fgetpos(outFD, &pos);
				fsetpos(outFD, &offsetPos);
				putLong(&ilong, pos);
				if(fwrite(&ilong, sizeof(ilong), 1, outFD) != 1)
				{
					error(0, outName, NULL, "Error writing output file\n");
					errorNum = ERROR_WRITING_OUTPUT;
					break;
				}
				offsetPos += sizeof(ilong);
				fsetpos(outFD, &pos);

				/* Read header */

				tmpFile.read(&head, sizeof(head));
#if 0
				{
				 	error(0, tmpFileName, NULL, "Error reading temporary file\n");
				 	errorNum = ERROR_READING_TMPFILE;
				 	break;
				}
#endif

				/* make new header */
	
				// assert(head.dataSize <= USHRT_MAX);
				// dataSize = static_cast<UWORD>(head.dataSize);
				dataSize = head.dataSize;
				if(head.header.flags.remap)
					dataSize += sizeof(REMAP_TABLE);

				UBYTE flags = 0;
				if(head.header.flags.lzHuf)
					flags |= 1 << LZHUF_B;
				if(head.header.flags.rle)
					flags |= 1 << RLE_B;
				if(head.header.flags.squeeze)
					flags |= 1 << SQUEEZE_B;
				if(head.header.flags.shadow)
					flags |= 1 << SHADOW_B;
				if(head.header.flags.nybble)
					flags |= 1 << NYBBLE_B;
				if(head.header.flags.remap)
					flags |= 1 << REMAP_B;

				putByte(&dHead.flags, flags);
				putByte(&dHead.headerSize,       sizeof(dHead));
				putLong(&dHead.dataSize,         dataSize);
				putWord(&dHead.width,            head.header.width);
				putWord(&dHead.height,           head.header.height);
				putWord(&dHead.fullWidth,        head.header.fullWidth);
				putWord(&dHead.fullHeight,       head.header.fullHeight);
				putWord(&dHead.xOffset,          head.header.xOffset);
				putWord(&dHead.yOffset,          head.header.yOffset);
				putWord(&dHead.anchorX,          head.header.anchorX);
				putWord(&dHead.anchorY,          head.header.anchorY);
				putByte(&dHead.shadowColor,      head.header.shadowColor);
				putByte(&dHead.transparentColor, head.header.transparentColor);
				putWord(&dHead.paletteIndex,		head.header.paletteIndex);
				
				if(fwrite(&dHead, sizeof(dHead), 1, outFD) != 1)
				{
				 	error(0, outName, NULL, "Error writing output file\n");
				 	errorNum = ERROR_WRITING_OUTPUT;
				 	break;
				}

				/* Copy remap table if present */

				if(head.header.flags.remap)
					if(fwrite(head.remapTable, sizeof(REMAP_TABLE), 1, outFD) != 1)
					{
						error(0, outName, NULL, "Error writing remap table\n");
			 			errorNum = ERROR_WRITING_OUTPUT;
			 			break;
					}

				/* Copy the data */

				if(copyData(outFD, tmpFile, head.dataSize) != ERROR_OK)
		 		{
		 			error(0, outName, NULL, "Error copying data\n");
		 			errorNum = ERROR_WRITING_OUTPUT;
		 			break;
		 		}
		 	}
			tmpFile.close();
			// fclose(tmpFD);
		}
#if 0
		else
		{
			error(0, tmpFileName, NULL, "Couldn't reopen temp file for reading\n");
			errorNum = ERROR_READING_TMPFILE;
		}
#endif

		palFile.openRead();
		count = paletteCount;
		while(count--)
		{
			PaletteHeader head;
			PaletteHeaderD dHead;

			fpos_t pos;
			ULONG dataSize;

			report(V_QUIET, "c");

			fgetpos(outFD, &pos);
			fsetpos(outFD, &palOffsetPos);
			putLong(&ilong, pos);
			if(fwrite(&ilong, sizeof(ilong), 1, outFD) != 1)
 			{
 				error(0, outName, NULL, "Error writing output file\n");
 				errorNum = ERROR_WRITING_OUTPUT;
 				break;
 			}
 
			palOffsetPos += sizeof(ilong);
			fsetpos(outFD, &pos);

			/* Read header */

			palFile.read(&head, sizeof(head));

			dataSize = sizeof(PaletteEntry) * head.nColors;
			putWord(&dHead.nColors, head.nColors);
			if(fwrite(&dHead, sizeof(dHead), 1, outFD) != 1)
			{
			 	error(0, outName, NULL, "Error writing palette to file\n");
			 	errorNum = ERROR_WRITING_OUTPUT;
			 	break;
			}

			/* Copy the data */

			if(copyData(outFD, palFile, dataSize) != ERROR_OK)
		 	{
		 		error(0, outName, NULL, "Error copying palette\n");
		 		errorNum = ERROR_WRITING_OUTPUT;
		 		break;
		 	}
		}
		palFile.close();


	outErr:
		err = fclose(outFD);
		if(err && (errorNum == ERROR_OK))
		{
			error(0, outName, NULL, "Writing output file\n");
			errorNum = ERROR_WRITING_OUTPUT;
		}
	}
	else
	{
		error(0, outName, NULL, "Can't create output file\n");
		errorNum = ERROR_WRITING_OUTPUT;
	} 	

	report(V_QUIET, "\n");	/* Show progress */
	return errorNum;		
}


#ifdef MCC_FORMAT
/*
 * Convert temporary file to MCC format
 */

MainError outputMCC(char *name)
{
	MainError errorNum = ERROR_OK;
	int err;
	char mccName[FILENAME_MAX];
	char offName[FILENAME_MAX];
	char *ext = NULL;
	FILE *tmpFD;
	FILE *mccFD;
	FILE *offFD;

	/* work out extension based on options */

	if(options.shadow)
		ext = ".SQS";
	else if(options.squeeze)
		ext = ".SQZ";
	else
		ext = ".MCC";

	makeFilename(mccName, name, ext, true);
	makeFilename(offName, name, ".OFF", true);

	report(V_QUIET, "Writing final files to %s and %s\n", mccName, offName);

	mccFD = fopen(mccName, "wb");
	if(mccFD != NULL)
	{
		offFD = fopen(offName, "wb");
		if(offFD != NULL)
		{
			tmpFD = fopen(tmpFileName, "rb");
			if(tmpFD != NULL)
			{
				int count = tileCount;
				while(count--)
				{
					TmpSpriteHeader header;
					fpos_t pos;
					ILONG ilong;
					struct {
						UBYTE width_h;
						UBYTE width_l;
						UBYTE height_h;
						UBYTE height_l;
						UBYTE xOffset_l;
						UBYTE xOffset_h;
						UBYTE yOffset_l;
						UBYTE yOffset_h;
						UBYTE shadowOffset;
						UBYTE shadowColor;
						UBYTE spare1;
						UBYTE spare2;
					} mccHead;

					if(fread(&header, sizeof(header), 1, tmpFD) != 1)
					{
						error(0, tmpFileName, NULL, "Error reading temporary file\n");
						errorNum = ERROR_READING_TMPFILE;
						break;
					}

					/* Write the position to the OFF file */

					fgetpos(mccFD, &pos);
					putLong(&ilong, pos);
					if(fwrite(&ilong, sizeof(ilong), 1, offFD) != 1)
					{
						error(0, offName, NULL, "Error writing offset file\n");
						errorNum = ERROR_WRITING_OUTPUT;
						break;
					}

					/* Write the sprite header */

					mccHead.width_l = header.header.width & 0xff;
					mccHead.width_h = (header.header.width >> 8) & 0xff;
					mccHead.height_l = header.header.height & 0xff;
					mccHead.height_h = (header.header.height >> 8) & 0xff;
					mccHead.xOffset_l = header.header.xOffset & 0xff;
					mccHead.xOffset_h = (header.header.xOffset >> 8) & 0xff;
					mccHead.yOffset_l = header.header.yOffset & 0xff;
					mccHead.yOffset_h = (header.header.yOffset >> 8) & 0xff;
					mccHead.shadowColor = header.header.shadowColor;
					mccHead.shadowOffset = header.header.anchorY;
					mccHead.spare1 = 0;
					mccHead.spare2 = 0;

					if(fwrite(&mccHead, sizeof(mccHead), 1, mccFD) != 1)
					{
						error(0, mccName, NULL, "Error writing output file\n");
						errorNum = ERROR_WRITING_OUTPUT;
						break;
					}
					
					/* Copy data */

					if(copyData(mccFD, tmpFD, header.dataSize) != ERROR_OK)
					{
						error(0, mccName, NULL, "Error copying data\n");
						errorNum = ERROR_WRITING_OUTPUT;
						break;
					}
				}

				fclose(tmpFD);
			}
			else
			{
				error(0, tmpFileName, NULL, "Couldn't reopen temp file for reading\n");
				errorNum = ERROR_READING_TMPFILE;
			}

			err = fclose(offFD);
			if(err && (errorNum == ERROR_OK))
			{
				error(0, mccName, NULL, "Error Writing output file\n");
				errorNum = ERROR_WRITING_OUTPUT;
			}										
		}
		else
		{
			error(0, offName, NULL, "Can't create output file\n");
			errorNum = ERROR_WRITING_OUTPUT;
		}

		err = fclose(mccFD);
		if(err && (errorNum == ERROR_OK))
		{
			error(0, mccName, NULL, "Writing output file\n");
			errorNum = ERROR_WRITING_OUTPUT;
		}
	}
	else
	{
		error(0, mccName, NULL, "Can't create output file\n");
		errorNum = ERROR_WRITING_OUTPUT;
	} 	
	return errorNum;		
}
#endif	/* MCC_FORMAT */

MainError outputFont(char *name)
{
	MainError errorNum = ERROR_OK;
	int err;
	// FILE *tmpFD;
	FILE *outFD;
	// char outName[FILENAME_MAX];

	String outName = makeFilename(name, ".FNT", true);

	report(V_QUIET, "Writing font file to %s\n", outName);

	outFD = fopen(outName, "wb");
	if(outFD != NULL)
	{
		fpos_t widthPos;
		IBYTE iByte;
		int count;

		UBYTE currentByte = 0;
		UBYTE bitMask = 0x80;

		/*
		 * Make Header
		 */

		FontHeaderD header;
		UWORD streamSize = 0;

		putByte(&header.height, fontHeight);
		putWord(&header.firstChar, options.fontStart);
		putWord(&header.howMany, tileCount);
		putWord(&header.streamSize, streamSize);

		if(fwrite(&header, sizeof(header), 1, outFD) != 1)
		{
		outErr1:
			error(0, outName, NULL, "Error writing file header\n");
			goto outErr;
		}

		/*
		 * Make dummy width table
		 */

		fgetpos(outFD, &widthPos);
		count = tileCount;
		putByte(&iByte, 0);
		while(count--)
		{
			if(fwrite(&iByte, sizeof(iByte), 1, outFD) != 1)
				goto outErr1;
		}

		/* Open temporary file */

		tmpFile.openRead();	// tmpFD = fopen(tmpFileName, "rb");
		// if(tmpFD != NULL)
		{
			int count = tileCount;
			while(count--)
			{
				TmpSpriteHeader head;
				fpos_t pos;

				report(V_QUIET, ".");	/* Show progress */

				/* Read header */

#if 0
				if(fread(&head, sizeof(head), 1, tmpFD) != 1)
				{
				 	error(0, tmpFileName, NULL, "Error reading temporary file\n");
				 	errorNum = ERROR_READING_TMPFILE;
				 	break;
				}
#else
				tmpFile.read(&head, sizeof(head));
#endif

				/* Update the width table */

				fgetpos(outFD, &pos);
				fsetpos(outFD, &widthPos);

				assert(head.header.width <= UCHAR_MAX);
				putByte(&iByte, static_cast<UBYTE>(head.header.width));
				if(fwrite(&iByte, sizeof(iByte), 1, outFD) != 1)
				{
					error(0, outName, NULL, "Error writing output file\n");
					errorNum = ERROR_WRITING_OUTPUT;
					break;
				}
				widthPos += sizeof(iByte);
				fsetpos(outFD, &pos);

				/* Copy the data */

				{
					int dataCount = head.dataSize;
					int copyCount = head.header.width * fontHeight;

					while(dataCount--)
					{
						// int c = fgetc(tmpFD);
						int c = fgetc(tmpFile);

						if(copyCount-- > 0)
						{
							/*
							 * Add bit to bitstream
							 */

							if(c)
								currentByte |= bitMask;
							bitMask >>= 1;
							if(bitMask == 0)
							{
								fputc(currentByte, outFD);
								streamSize++;

								bitMask = 0x80;
								currentByte = 0;
							}
						}
					}

					while(copyCount-- > 0)
					{
						/*
						 * Add 0 to bitstream
						 */

						bitMask >>= 1;
						if(bitMask == 0)
						{
							fputc(currentByte, outFD);
							streamSize++;

							bitMask = 0x80;
							currentByte = 0;
						}
					}
				}
		 	}
			// fclose(tmpFD);
			tmpFile.close();

			/*
			 * Add any remaining bits of bitstream
			 */

			if(bitMask != 0x80)
			{
				fputc(currentByte, outFD);
				streamSize++;
			}

			/*
			 * Go back and rewrite header
			 */

			putWord(&header.streamSize, streamSize);
			fseek(outFD, 0, SEEK_SET);
			if(fwrite(&header, sizeof(header), 1, outFD) != 1)
				goto outErr1;
		}
#if 0
		else
		{
			error(0, tmpFileName, NULL, "Couldn't reopen temp file for reading\n");
			errorNum = ERROR_READING_TMPFILE;
		}
#endif
	outErr:
		err = fclose(outFD);
		if(err && (errorNum == ERROR_OK))
		{
			error(0, outName, NULL, "Writing output file\n");
			errorNum = ERROR_WRITING_OUTPUT;
		}
	}
	else
	{
		error(0, outName, NULL, "Can't create output file\n");
		errorNum = ERROR_WRITING_OUTPUT;
	} 	

	report(V_QUIET, "\n");	/* Show progress */
	return errorNum;		

}

/*
 * Convert temporary file to final file
 */

MainError makeFinalFile(char *name)
{
	MainError errorNum = ERROR_OK;

	report(V_QUIET, "%d Sprites grabbed\n", tileCount);

#if 0
	/* Close the temporary file if it is open */

	if(outFD != NULL)
	{
		errorNum = closeTmpFile();
		if(errorNum != ERROR_OK)
			return errorNum;
	}

	if(palFD != NULL)
	{
		errorNum = closePalFile();
		if(errorNum != ERROR_OK)
			return errorNum;
	}
#else
	tmpFile.close();
	palFile.close();
#endif

	switch(options.outputFormat)
	{
	case OPTIONS::OUT_SWG:
		errorNum = outputSWG(name);
		break;
#ifdef MCC_FORMAT
	case OPTIONS::OUT_MCC:
		errorNum = outputMCC(name);
		break;
#endif /* MCC_FORMAT */
	case OPTIONS::OUT_FONT:
		errorNum = outputFont(name);
		break;
	}

	/* Get rid of the temporary file */

	// remove(tmpFileName);
	// remove(tmpPalName);

	return errorNum;
}



/*
 * Tell the user how silly s/he is and tell them how it should
 * be used
 */

void usage(void)
{
	printf(
			"\nUsage:\n"
			"\tGETSPR [@listfile] [[[source] destination] defFile] [options]\n\n"
			"Options are:\n"
			"\t-h\n"
			"\t-? : Help (displays this!)\n"
			"\t-d[filename] : Use Def file for coordinates\n"
			"\t-b<n> : Use colour n as background colour [default taken from picture]\n"
			"\t-k  : First few pixels in image are used for background, border, anchor and shadow\n"
			"\t-o<format> : Define output format, format is:\n"
#ifdef MCC_FORMAT
			"\t\tM : MCC compatible format with .OFF file\n"
#endif
			"\t\tS : SWG format [DEFAULT]\n"
			"\t\tF : Font format\n"
			"\t-v<level> : Define verbosity of text output from:\n"
			"\t\tS : Silent\n"
			"\t\tQ : Quiet [Default]\n"
			"\t\tV : Verbose\n"
			"\t\tD : Debug\n"
			"\t-s : Remove white space and store animation offsets\n"
			"\t-sh<n> : Calculate base of sprite using n as a shadow color\n"
			"\t-a<n> : Define Y anchor point using a pixel of color n\n"
			"\t-a : Use Anchor to set the bounding rectangle size rather than border\n"
			"\t-n : Nybble packing (16 colours per sprite) with no remapping\n"
			"\t-nM : Nybble pack with remapped colours\n"
			"\t-l<language> : Select language for output files:\n"
			"\t\tC : the C programming language [default]\n"
			"\t\tS : Assembler\n"
			"\t\tH : Hacker (raw data)\n"
			"\tF<n> : Create FONT file and starting character [default=32]\n"
			"\tC : Compress Images with LZhuf\n"
			"\tIfilename : Create C style header file\n"
			"\n"
	);
}

/*------------------------------------
 * Process an argument
 *
 * Return 0 if it was a valid argument
 */

enum ProcessError { ARG_PROCESSED, ARG_UNKNOWN, ARG_ERROR } ;

ProcessError processArg(char *s)
{
	char c = *s++;
	ProcessError retVal = ARG_UNKNOWN;
	
	if((c == '-') || (c == '/'))
	{
		retVal = ARG_PROCESSED;

		c = *s++;

		switch(toupper(c))
		{
		case 'A':		/* Anchor point */
			if(isdigit(*s))
			{
				options.anchorColor = atoi(s);
				options.useAnchor = true;
				report(V_VERBOSE, "Anchor color is %d\n", options.anchorColor);
			}
			else if(*s == 0)
			{
				options.anchorBorder = true;
				report(V_VERBOSE, "Anchor color is used to define fullWidth/Height");
			}
			else
			{
				error(0, NULL, NULL, "/A must be followed by nothing or a color index\n");
				return ARG_ERROR;
			}
			break;

		case 'B':		/* Background color */
			if(isdigit(*s))
			{
				options.backGroundColor = atoi(s);
				options.givenBackground = true;
				report(V_VERBOSE, "Background Color is %d\n", options.backGroundColor);
			}
			else
			{
				error(0,NULL,NULL, "/B must be followed by a number, e.g. /b3\n");
				return ARG_ERROR;
			}
			break;

		case 'C':		/* Use LZHuf compression if it makes the image smaller */
			options.lzHuf = true;
				report(V_VERBOSE, "Using color key\n", options.backGroundColor);
			break;

		case 'D':		/* use Def File */
			closeDefFile();			/* Make sure any existing def file is closed */
			options.useDefFile = true;

			if(*s)
				makeDefFileName(s, false);
			else
				defFileName.clear(); // = 0;
#if 0
				if(defFileName != NULL)
				{
					free(defFileName);
					defFileName = NULL;
				}
#endif

			break;

		case 'F':				/* Make a Font File and set the start character */
			if(isdigit(*s))
				options.fontStart = atoi(s);
			else
				options.fontStart = ' ';

			options.outputFormat = OPTIONS::OUT_FONT;
			report(V_VERBOSE, "FONT format enabled, start character is %d\n", options.fontStart);
			break;

		case 'I':
			options.wantIncludeFile = true;
			if(*s)
				includeFile = s;
			report(V_VERBOSE, "Include file will be made\n");
			break;

		case 'K':
			options.colorKey = true;
			break;

		case 'L':			/* Programming language */
			c = *s++;
			switch(toupper(c))
			{
			case 'C':
				c = *s++;
				switch(toupper(c))
				{
				case 'P':
				case '+':
					options.language = OPTIONS::LANG_CPP;
					report(V_VERBOSE, "Output language is 'C++'\n");
					break;
				case 0:
					options.language = OPTIONS::LANG_C;
					report(V_VERBOSE, "Output language is 'C'\n");
					break;
				default:
					error(0, NULL, NULL, "-L language option must be followed by C, CPP, C++ or S\n");
					return ARG_ERROR;
				}
				break;
			case 'S':
				options.language = OPTIONS::LANG_ASM;
				report(V_VERBOSE, "Output language is 'ASM'\n");
				break;
			default:
				error(0, NULL, NULL, "-L language option must be followed by C, CPP, C++ or S\n");
				return ARG_ERROR;
			}
			break;

		case 'O':		/* Output format */
			c = *s++;
			switch(toupper(c))
			{
			case 'S':
				options.outputFormat = OPTIONS::OUT_SWG;
				report(V_VERBOSE, "SWG sprite format enabled\n");
				break;
#ifdef MCC_FORMAT
			case 'M':
				options.outputFormat = OPTIONS::OUT_MCC;
				report(V_VERBOSE, "MCC tile format enabled\n");
				break;
#endif
			case 'F':
				options.outputFormat = OPTIONS::OUT_FONT;
				report(V_VERBOSE, "FONT format enabled\n");
				break;
			default:
				error(0,NULL,NULL, "/O must be followed by a format type\n");
				return ARG_ERROR;
			}
			break;

		case 'S': 	/* Squeeze... remove white space from edges */
			c = *s++;
			switch(toupper(c))
			{
			case 0:
				options.squeeze = true;
				report(V_VERBOSE, "Squeeze option enabled\n");
				break;
			case 'H':	/* shadow */
				if(isdigit(*s))
				{
					options.shadow = true;
					options.shadowColor = atoi(s);
					report(V_VERBOSE, "Shadow Color is %d\n", options.shadowColor);
				}
				else
				{
					error(0, NULL, NULL, "-SH must be followed by the shadow color\n");
					return ARG_ERROR;
				}
			 	break;
			default:
				retVal = ARG_UNKNOWN;
			}
			break;

		case 'N':			/* Nybble pack */
			options.nybble = true;
			report(V_VERBOSE, "Nybble packing enabled\n");
			c = *s++;
			switch(toupper(c))
			{
			case 0:
				break;
			case 'M':
				options.remap = true;
				report(V_VERBOSE, "Colour remapping enabled\n");
				break;
			default:
				error(0, NULL, NULL, "Unknown option following -N (nybble pack)\n");
				return ARG_ERROR;
			}
			break;

		case 'V':
			c = *s++;
			switch(toupper(c))
			{
			case 'S':
				verbosity = V_SILENT;
				break;
			case 'Q':
				verbosity = V_QUIET;
				break;
			case 'V':
				verbosity = V_VERBOSE;
				report(V_VERBOSE, "Verbose mode\n");
				break;
			case 'D':
				verbosity = V_DEBUG;
				report(V_DEBUG, "Debug mode\n");
				break;
			default:
				error(0,NULL,NULL,
						"/V must be followed by a verbosity level of\n"
						"Silent, Quiet, Verbose or Debug\n");
				return ARG_ERROR;

			}
		 	break;

		default:
			retVal = ARG_UNKNOWN;
			break;
		}
	}

	return retVal;
}

MainError processListFile(char *fileName)
{
	// char fName[FILENAME_MAX];
	FILE *fd;
	MainError errorNum = ERROR_OK;

	String fName = makeFilename(fileName, ".LST", false);
 	report(V_QUIET, "Processing list file %s\n", fName);
	
	fd = fopen(fName, "r");		/* ascii file */
	if(fd != NULL)
	{
		char buffer[120];
		char copyBuffer[120];

		int lineCount = 1;	/* Line counter used for reporting errors */

		while(fgets(buffer, 119, fd) != NULL)
		{
			char *s;

#ifdef DEBUG
			report(V_DEBUG, "%3d: %s", lineCount, buffer);
#endif
			strcpy(copyBuffer, buffer);
			strtok(copyBuffer, "\n\r");	/* Remove CR/LF */

			s = buffer;
			while(*s && isspace(*s))
				s++;

			if( (*s != '\0') && (*s != ';') && (*s != '#') )	/* Comment/blank line */
			{
				char *picName = NULL;
				options = globalOptions;

				char* s1 = NULL;

				if(*s == '\"')
				{
					for(s1 = ++s; *s1; ++s1)
					{
						if(*s1 == '\"')
						{
							*s1++ = 0;
							*s1++ = 0;
							break;
						}
					}
				}
				else
					s = strtok(s, whiteSpace);

				while(s)
				{
#ifdef DEBUG
					report(V_DEBUG, "Token: %s\n", s);
#endif
					if((*s == '-') || (*s == '/'))
					{
						int err = processArg(s);
						if(err == ARG_UNKNOWN)
						{
							error(lineCount, fName, copyBuffer,
								"Unknown option '%s'\n", s);
							return ERROR_BADPARAMETER;
						}
						else if(err == ARG_ERROR)
						{
							error(lineCount, fName, copyBuffer,
								"Error in opt '%s' of line", s);
							return ERROR_BADPARAMETER;
						}
					}
					else	/* Assume its a filename */
					{
						if(picName == NULL)
							picName = s;
						else
						{
							error(lineCount, fName, copyBuffer,
								"You have put more than one filename on the line\n"
								"'%s' and '%s'\n", picName, s);
							return ERROR_BADPARAMETER;
						}
					}
					s = strtok(s1, whiteSpace);
					s1 = NULL;
				}

				if(picName)
				{
					/*
					 * Make a def file name if a new one was requested
					 * and no name was given
					 */

					if(options.useDefFile && (defFileName.length() == 0))
						makeDefFileName(picName, true);

					errorNum = processFile(picName);
					if(errorNum != ERROR_OK)
						return errorNum;
				}
			}

			lineCount++;
		}
		fclose(fd);
	}
	else
	{
		error(0, fName, NULL, "Couldn't open list file\n");
		errorNum = ERROR_MISSING_LIST_FILE;
	}

	return errorNum;
}

/*-------------------------------------------------------------------
 * main function
 *
 * Returns:
 *  0 = OK
 *  1 = Bad parameters
 */

int main(int argc, char *argv[])
{
	int i;
	char *listFile = NULL;		/* Name of list file being processed */
	char *inFile = NULL;
	char *outFile = NULL;
	MainError errorNum = ERROR_OK;

	banner();

	if(argc < 2)
	{
		usage();
		return ERROR_BADPARAMETER;
	}

	/*
 	 * Initialise options
	 */

	options.useDefFile = false;
	options.squeeze = false;
	options.shadow = false;
	options.nybble = false;
	options.remap = false;
	options.rle = false;
	options.lzHuf = false;
	options.backGroundColor = 0;
	options.borderColor = 0;
	options.givenBackground = false;
	options.colorKey = false;
	options.anchorColor = false;
	options.shadowColor = 0;
	options.outputFormat = OPTIONS::OUT_SWG;
	options.language = OPTIONS::LANG_C;
	verbosity = V_QUIET;
	options.fontStart = ' ';
	options.wantIncludeFile = false;
	includeFile = NULL;

	fontHeight = 0;

	/*
	 * Process command line
	 */

	i = 0;
	while(++i < argc)
	{
		char *currentArg = argv[i];
		char *arg = currentArg;

		if((*arg == '-') || (*arg == '/'))	/* Option */
		{	
			int err = processArg(arg);
			if(err == ARG_ERROR)
			{
				error(0, NULL, NULL, "Bad Command Line arguments\n");
				return ERROR_BADPARAMETER;
			}
			else if(err == ARG_UNKNOWN)	/* Not a general option */
			{
				char c;			
				arg++;
				c = *arg++;

				switch(toupper(c))
				{
				default:
					error(0, NULL, NULL, "Unknown option '%s'\n", currentArg);
					waitKey();
				case 'H':
				case '?':
					usage();
					return ERROR_BADPARAMETER;
				}
			}
		}
		else if(*arg == '@')	/* List file */
		{
			arg++;

			if( (listFile == NULL) && (inFile == NULL) )
				listFile = arg;
			else
			{
				error(0, NULL, NULL,
					"You have tried to define more than one listFile\n%s and %s\n",
					listFile, arg);
				return ERROR_BADPARAMETER;
			}
		}
		else	/* Must be a filename */
		{
			if( (inFile == NULL) && (listFile == NULL) )
				inFile = arg;
			else if(outFile == NULL)
				outFile = arg;
			else if(defFileName.length() == 0)
				makeDefFileName(arg, false);
			else
			{
				error(0, NULL, NULL, "There are two many filenames on the command line '%s'\n", arg);
				return ERROR_BADPARAMETER;
			}
		}
	}

	globalOptions = options;

	try
	{

		if(listFile)
		{
			if(outFile == NULL)				/* Set the output file name */
				outFile = listFile;

			if(defFileName.length() == 0)
				makeDefFileName(listFile, true);

			if(options.wantIncludeFile)
			{
				if(includeFile == NULL)
					includeFile = outFile;
				startHFile(includeFile, outFile);
			}

			errorNum =  processListFile(listFile);
			if(errorNum == ERROR_OK)
				errorNum = makeFinalFile(outFile);
			closeDefFile();
		} 
		else		/* Process the filename given on the command line */
		{
			if(inFile == NULL)
			{
				error(0, NULL, NULL, "You did not specify any filenames on the command line\n");
				return ERROR_BADPARAMETER;
			}
			if(outFile == NULL)
				outFile = inFile;
			if(defFileName.length() == 0)
				makeDefFileName(inFile, true);

			if(options.wantIncludeFile)
			{
				if(includeFile == NULL)
					includeFile = outFile;
				startHFile(includeFile, outFile);
			}

			errorNum = processFile(inFile);
			if(errorNum != ERROR_OK)
				return errorNum;
			closeDefFile();

			options = globalOptions;
 
			makeFinalFile(outFile);
			if(errorNum != ERROR_OK)
				return errorNum;
		}

 		if(options.wantIncludeFile)
 			endHFile();

		if(options.lzHuf)
			report(V_QUIET, "Overall Compression: %ld bytes -> %ld bytes (%d%%)\n",
				uncompressedSize,
				compressedSize,
				(int)(compressedSize * 100 / uncompressedSize));
	}
	catch(SPR_Error e)
	{
		report(V_QUIET, "Error %d (%s)\n", e.error(), e.errorString());
	}
	catch(GeneralError e)
	{
		report(V_QUIET, "Error: %s\n", e.get());
	}
	catch(...)
	{
		report(V_QUIET, "Abnormal exit");
	}

	return errorNum;
}
