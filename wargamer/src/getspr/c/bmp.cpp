/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Load in .BMP file
 *
 *----------------------------------------------------------------------
 */

#include "bmp.hpp"
#include "imgread.hpp"
#include "cppfile.hpp"
#include <assert.h>

#define WIN32_LEAN_AND_MEAN
#define NOGDICAPMASKS     
#define NOVIRTUALKEYCODES 
#define NOWINMESSAGES     
#define NOWINSTYLES       
#define NOSYSMETRICS      
#define NOMENUS           
#define NOICONS           
#define NOKEYSTATES       
#define NOSYSCOMMANDS     
#define NORASTEROPS       
#define NOSHOWWINDOW      
#define OEMRESOURCE       
#define NOATOM            
#define NOCLIPBOARD       
#define NOCOLOR           
#define NOCTLMGR          
#define NODRAWTEXT        
// #define NOGDI             
#define NOKERNEL          
#define NOUSER            
#define NONLS             
#define NOMB              
#define NOMEMMGR          
#define NOMETAFILE        
#define NOMINMAX          
#define NOMSG             
#define NOOPENFILE        
#define NOSCROLL          
#define NOSERVICE         
#define NOSOUND           
#define NOTEXTMETRIC      
#define NOWH              
#define NOWINOFFSETS      
#define NOCOMM            
#define NOKANJI           
#define NOHELP            
#define NOPROFILER        
#define NODEFERWINDOWPOS  
#define NOMCX             
#include <windows.h>

template<class T>
inline T abs(T t)
{
	return (t < 0) ? -t : +t;
}

namespace BMPReader
{



BMPError::BMPError(const char* fmt, ...)
{
	va_list vaList;
	va_start(vaList, fmt);
	setMessage(fmt, vaList);
	va_end(vaList);
}

bool read(const char* fname, ImageReaderInterface* imgReader)
{
	try
	{
		STDIOReader fr(fname);

		/*
		 * Read BITMAPFILEHEADER
		 */

		BITMAPFILEHEADER header;
		fr.read(&header, sizeof(header));

		/*
		 * Read BITMAPINFO
		 */

		BITMAPINFOHEADER bmh;
		fr.read(&bmh, sizeof(bmh));

		/*
		 * Check for valid type
		 */

		if((bmh.biPlanes != 1) ||
		   (bmh.biBitCount != 8) ||
			(bmh.biCompression != BI_RGB))
		{
			throw BMPError("%s is not 256 colour RGB Windows BMP", fname);
		}

		/*
	 	 * Read and Copy Colour Table
	 	 */

		int nColors = 256;
		if(bmh.biClrUsed != 0)
			nColors = bmh.biClrUsed;

		RGBQUAD pal[256];
		// fr.read(pal, sizeof(pal));
		fr.read(pal, sizeof(RGBQUAD) * nColors);

		Palette& destPal = imgReader->getPalette();
		destPal.init(256);
		for(int i = 0; i < 256; i++)
		{
			destPal[i] = PaletteEntry(
				pal[i].rgbRed,
				pal[i].rgbGreen,
				pal[i].rgbBlue);
		}

		UWORD width = static_cast<UWORD>(bmh.biWidth);
		UWORD height = static_cast<UWORD>(abs(bmh.biHeight));

		UBYTE* bits = imgReader->init(width, height);
		if(bits == 0)
			throw BMPError("Could not allocate memory for %s", fname);

		UWORD sWidth = imgReader->getStorageWidth();
		UWORD fileWidth = (width + 3) & ~3;
		UWORD skipSize = fileWidth - width;		// BMPs round up to 4 bytes

		// Seek to bit start

		if(header.bfOffBits != 0)
			fr.seekTo(header.bfOffBits);


		UBYTE* dest = bits + height * sWidth;
		int y = height;
		while(y--)
		{
			dest -= sWidth;

			fr.read(dest, width);

			if(skipSize != 0)
			{
				fr.seekTo(fr.getPos()+skipSize);
			}

		}

		return true;
	}
	catch(FileError e)
	{
		exceptionHandler->alert("File Error", e.get());
		return false;
	}
	catch(BMPError e)
	{
		exceptionHandler->alert("BMP Reader", e.get());
		return false;
	}
}

};

#if 0
/*
 * Internal BMP Reading Functions
 */

class BMPReader : public BMP {
 public:
 	static void readDIB(RawDIB* dib, const char* fileName, ReadMode mode);
 	static void readDIB(RawDIB* dib, HINSTANCE hinst, const char* resName, ReadMode mode);
};

/*
 * Read BMP from File
 */

static void BMPReader::readDIB(RawDIB* dib, const char* fileName, ReadMode mode)
{
	ASSERT(fileName != 0);
	ASSERT(mode < RBMP_HowMany);

	/*
	 * Open the file (Use a class so automatically closed)
	 */

	WinFileBinaryReader fr(fileName);

	if(fr.getError())
	{
#ifdef DEBUG
		debugMessage("Can't open %s", fileName);
#endif
		throw BMPError();
	}

	/*
	 * Read BITMAPFILEHEADER
	 */

	BITMAPFILEHEADER header;
	if(!fr.read(&header, sizeof(header)))
		throw BMPError();

	/*
	 * Read BITMAPINFO
	 */

	BITMAPINFOHEADER bmh;
	if(!fr.read(&bmh, sizeof(bmh)))
		throw BMPError();

	/*
	 * Check for valid type
	 */

	if((bmh.biPlanes != 1) ||
	   (bmh.biBitCount != 8) ||
		(bmh.biCompression != BI_RGB))
	{
#ifdef DEBUG
		debugMessage("%s is not 256 colour RGB Windows BMP", fileName);
#endif
		throw BMPError();
	}

	/*
	 * Read and Copy Colour Table
	 */

	RGBQUAD pal[256];

	if(!fr.read(pal, sizeof(pal)))
		throw BMPError();

	if((Palette::get() == NULL) || (mode == RBMP_ForcePalette) || (mode == RBMP_OnlyPalette))
		Palette::set(Palette::makeIdentityPalette(pal, 256));

	if(mode == RBMP_OnlyPalette)
	{
		return;
	}
	else
	{
		ASSERT(dib != 0);

		/*
	 	 * Create DIB
	 	 */

		dib->makeBitmapInfo(bmh.biWidth, bmh.biHeight);

		/*
	 	 * Read and Copy Bit Table
		 *
		 * Do it line by line so that it is right way up
	 	 */

		PUCHAR dest = dib->getBits() + dib->getHeight() * dib->getStorageWidth();
		int y = dib->getHeight();
		while(y--)
		{
			dest -= dib->getStorageWidth();

			if(!fr.read(dest, dib->getStorageWidth()))
			{
				// delete dib;
				throw BMPError();
			}
		}

		/*
	 	 * Remap it
	 	 */

		dib->remap(pal);
	}
}

/*
 * Read BMP From Resource
 */

static void BMPReader::readDIB(RawDIB* dib, HINSTANCE hinst, const char* resName, ReadMode mode)
{
	ASSERT(resName != 0);
	ASSERT(mode < RBMP_HowMany);

	/*
	 * Load in the bitmap
	 */

	HRSRC hRes = FindResource(hinst, resName, RT_BITMAP);
	HGLOBAL hGlobal = LoadResource(hinst, hRes);
	LPBITMAPINFO bm = (LPBITMAPINFO) LockResource(hGlobal);

	/*
	 * Check for valid type
	 */

	if((bm->bmiHeader.biPlanes != 1) ||
	   (bm->bmiHeader.biBitCount != 8) ||
		(bm->bmiHeader.biCompression != BI_RGB))
	{
#ifdef DEBUG
		debugMessage("Resource %s is not 256 colour RGB Windows BMP", resName);
#endif
		throw BMPError();
	}

	if((Palette::get() == NULL) || (mode == RBMP_ForcePalette) || (mode == RBMP_OnlyPalette))
		Palette::set(Palette::makeIdentityPalette(bm->bmiColors, 256));

	if(mode == RBMP_OnlyPalette)
		return;

	dib->makeBitmapInfo(bm->bmiHeader.biWidth, bm->bmiHeader.biHeight);

	/*
	 * Copy the image into bits
	 * Image is stored upside down!!!  
	 * So we have to copy line by line to flip it
	 */

	const UBYTE* src = (const UBYTE*) ( ((const UBYTE*)bm->bmiColors) + dib->getColours() * sizeof(RGBQUAD));

#if 0		// Simplify, to avoid use of buffer
	UBYTE* dest = dib->getBits();

	memcpy(dest, src, dib->getStorageWidth() * dib->getHeight());

	dest = dib->getBits() + dib->getHeight() * dib->getStorageWidth();

	UBYTE* copySrc = new unsigned char[getStorageWidth() * getHeight()];
	ASSERT(copySrc != 0);
	memcpy(copySrc, src, getStorageWidth() * getHeight());

	src = copySrc;

	int y = getHeight();
	while(y--)
	{
	  dest -= getStorageWidth();
	  memcpy(dest, src, getStorageWidth()); //* getHeight());
	  src += getStorageWidth();
	}

	delete[] copySrc;
#endif
	
	UBYTE* dest = dib->getBits() + dib->getHeight() * dib->getStorageWidth();
	int y = dib->getHeight();
	while(y--)
	{
	  dest -= dib->getStorageWidth();
	  memcpy(dest, src, dib->getStorageWidth());
	  src += dib->getStorageWidth();
	}

	/*
	 * Remap the images colours
	 */

	dib->remap(bm->bmiColors);

	/*
	 * Don't care what the SDK says, I'm going to unlock and free them
	 * otherwise how can system know whether I've finished with it so
	 * it can be freed from memory?
	 */

	// UnlockResource(bm);
	FreeResource(hGlobal);
}


/*
 * External Functions
 */

static void BMP::getPalette(const char* fileName)
{
	BMPReader::readDIB(0, fileName, RBMP_OnlyPalette);
}

static void BMP::getPalette(HINSTANCE hinst, const char* resName)
{
	BMPReader::readDIB(0, hinst, resName, RBMP_OnlyPalette);
}


static DIB* BMP::newDIB(const char* fileName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, fileName, mode);

	DIB* dib = new DIB(rDib);
	return dib;
}

static DrawDIB* BMP::newDrawDIB(const char* fileName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, fileName, mode);

	DrawDIB* dib = new DrawDIB(rDib);
	return dib;
}

static DIBDC* BMP::newDIBDC(const char* fileName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, fileName, mode);

	DIBDC* dib = new DIBDC(rDib);
	return dib;
}

static DrawDIBDC* BMP::newDrawDIBDC(const char* fileName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, fileName, mode);

	DrawDIBDC* dib = new DrawDIBDC(rDib);
	return dib;
}


static DIB* BMP::newDIB(HINSTANCE hinst, const char* resName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, hinst, resName, mode);

	DIB* dib = new DIB(rDib);
	return dib;
}

static DrawDIB* BMP::newDrawDIB(HINSTANCE hinst, const char* resName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, hinst, resName, mode);

	DrawDIB* dib = new DrawDIB(rDib);
	return dib;
}

static DIBDC* BMP::newDIBDC(HINSTANCE hinst, const char* resName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, hinst, resName, mode);

	DIBDC* dib = new DIBDC(rDib);
	return dib;
}

static DrawDIBDC* BMP::newDrawDIBDC(HINSTANCE hinst, const char* resName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, hinst, resName, mode);

	DrawDIBDC* dib = new DrawDIBDC(rDib);
	return dib;
}

#endif
