/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Generic Exception Class Implementation
 *
 *----------------------------------------------------------------------
 */

#include "except.hpp"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>


/*
 * Exception Handling
 */

bool GeneralError::doingError = false;
bool GeneralError::firstError = false;

GeneralError::GeneralError()
{
	describe = 0;
}

GeneralError::GeneralError(const char* fmt, ...)
{
	// char buffer[500];

	va_list vaList;
	va_start(vaList, fmt);
	setMessage(fmt, vaList);
	// _vbprintf(buffer, 500, fmt, vaList);
	va_end(vaList);

#if 0
	/*
	 * Convert any \r's into \r\n
	 */

	char* s = buffer;
	while(*s)
	{
		if(*s++ == '\r')
		{
			char* s1 = s;
			char last = '\n';

			do
			{
				char c = *s1;
				*s1 = last;
				last = c;

				s1++;
			}  while(last);
			*s1 = 0;
		}
	}

	setMessage(buffer);
	// messageBox("Fatal Error!", buffer, MB_ICONSTOP);
	exceptionHandler->alert("Fatal Error!", buffer);
#else
	exceptionHandler->alert("Fatal Error!", describe);
#endif
}

void GeneralError::setMessage(const char* fmt, va_list& vaList)
{
	char buffer[500];

#ifdef _WATCOM_C
        _vbprintf(buffer, 500, fmt, vaList);
#else
        vsprintf(buffer, 500, fmt, vaList);
#endif

	/*
	 * Convert any \r's into \r\n
	 */

	char* s = buffer;
	while(*s)
	{
		if(*s++ == '\r')
		{
			char* s1 = s;
			char last = '\n';

			do
			{
				char c = *s1;
				*s1 = last;
				last = c;

				s1++;
			}  while(last);
			*s1 = 0;
		}
	}

	setMessage(buffer);
}

void GeneralError::setMessage(const char* s)
{
	firstError = doingError;
	doingError = true;

	describe = strdup(s);

#if !defined(NOLOG)
	if(!firstError)
		exceptionHandler->logError("General Error Report", s);
#endif
}

const char* GeneralError::get()
{
	return describe;
}


