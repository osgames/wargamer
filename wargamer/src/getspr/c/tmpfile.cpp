/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Temporary files
 *
 *----------------------------------------------------------------------
 */

#include "tmpfile.hpp"
#include "verbose.h"
#include <stdio.h>


TmpFile::TmpFile(const char* name) : d_name(name), d_file(NULL), d_mode(None)
{
}

TmpFile::~TmpFile()
{
	if(d_file)
		fclose(d_file);
	remove(d_name);
	report(V_DEBUG, "deleted %s\n", d_name);
}

void TmpFile::openRead()
{
	if(d_mode != Read)
		close();

	if(d_file == NULL)
	{
		d_file = fopen(d_name, "rb");
		if(d_file == NULL)
		{
			error(0, NULL, NULL, "Can't open temporary file '%s'\n", d_name);
			// return ERROR_CANT_CREATE_TMPFILE;
			throw TmpFile::Error();
		}
		else
			d_mode = Read;

		report(V_DEBUG, "%s opened for reading\n", d_name);
	}
#if 0
	else
	{
		error(0, NULL, NULL, "tmpFile '%s' is already open!\n", d_name);
		throw TmpFile::Error();
		// return ERROR_INTERNAL;
	}
#endif
}

void TmpFile::openWrite()
{
	if(d_mode != Write)
		close();

	if(d_file == NULL)
	{
		d_file = fopen(d_name, "wb");
		if(d_file == NULL)
		{
			error(0, NULL, NULL, "Can't create temporary file '%s'\n", d_name);
			// return ERROR_CANT_CREATE_TMPFILE;
			throw TmpFile::Error();
		}
		else
			d_mode = Write;
		report(V_DEBUG, "%s opened for writing\n", d_name);
	}
#if 0
	else
	{
		error(0, NULL, NULL, "tmpFile '%s' is already open!\n", d_name);
		throw TmpFile::Error();
		// return ERROR_INTERNAL;
	}
#endif
}

void TmpFile::close()
{
	if(d_file != NULL)
	{
		int err = fclose(d_file);
		d_file = NULL;
		d_mode = None;
		if(err != 0)
		{
			error(0, NULL, NULL, "error %d closing temporary file %s\n", err, d_name);
			// return ERROR_BAD_CLOSE_TMPFILE;
			throw TmpFile::Error();
		}
		report(V_DEBUG, "%s closed\n", d_name);
	}
#if 0
	else
	{
		error(0, NULL, NULL, "tmpFile %s was not opened!\n", d_name);
		throw TmpFile::Error();
		// return ERROR_INTERNAL;
	}
#endif
}

void TmpFile::read(void* buffer, size_t len)
{
	if((d_file == NULL) || (d_mode != Read))
	{
		error(0, NULL, NULL, "Trying to read from %s, but it is not open!", d_name);
		throw TmpFile::Error();
	}

	if(fread(buffer, len, 1, d_file) != 1)
	{
		error(0, NULL, NULL, "Error reading from %s", d_name);
		throw TmpFile::Error();
	}

	report(V_DEBUG, "read from %s\n", d_name);
}

void TmpFile::write(const void* buffer, size_t len)
{
	if((d_file == NULL) || (d_mode != Write))
	{
		error(0, NULL, NULL, "Trying to write to %s, but it is not open!", d_name);
		throw TmpFile::Error();
	}

	if(fwrite(buffer, len, 1, d_file) != 1)
	{
		error(0, NULL, NULL, "Error writing to %s", d_name);
		throw TmpFile::Error();
	}
	report(V_DEBUG, "written to %s\n", d_name);
}


