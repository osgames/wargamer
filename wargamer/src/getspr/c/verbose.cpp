/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Verbosity level and error reporting header file
 *
 *----------------------------------------------------------------------
 */

#include "verbose.h"
#include <stdarg.h>
#include <stdio.h>

/*
 * Global Instance of verbosity
 */

Verbose verbosity = V_QUIET;

/*
 * Display a message to the user depending on verbosity level
 */

void report(Verbose level, const char *fmt, ...)
{
	va_list vaList;

	if(level <= verbosity)
	{
		va_start(vaList, fmt);
		vprintf(fmt, vaList);
		va_end(vaList);
	}

	fflush(stdout);
}


/*-------------------------------
 * Give the user an error message
 */

void error(int line, const char *fileName, const char *lineBuf, const char *fmt, ...)
{
	va_list vaList;

	printf("\n\aError");
	if(line != 0)
		printf(" in line %d", line);
	if(fileName != NULL)
	{
		if(line == 0)
			printf(" in");
		else
			printf(" of");
		printf(" file %s", fileName);
	}
	if(lineBuf != NULL)
		printf("\n: '%s'", lineBuf);
	printf("\n");

	va_start(vaList, fmt);
	vprintf(fmt, vaList);
	va_end(vaList);
	printf("\n");

	cleanup();
}

