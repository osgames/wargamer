/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
//====================================================
// $ID: g_terr.hpp $
//====================================================
//
// Sprite Definitions for \terrain.lst
//
// Warning: Do NOT EDIT this file
// It was generated automatically by GetSpr version 3.2
//
//====================================================

#ifndef G_TERR_HPP
#define G_TERR_HPP

#ifndef __cplusplus
#error g_terr.hpp is for use with C++
#endif

//======================================================================
// Each processed graphic file generates 3 values:
//   Name       : Index of the 1st image from this graphic
//   Name_Last  : Index of the next index following this graphic
//   Name_Count : Number of images in this graphic
//
// Important: Name_Last is not the last image, but the one following it
//
// There is also a EnumName_HowMany value containing the total number
// of images in the graphic
//======================================================================

enum \terrainSpriteIndex
{
	3wayriv = 0,
	3wayriv_Last = 14,
	3wayriv_Count = 14,

	Rvr&cst = 14,
	Rvr&cst_Last = 46,
	Rvr&cst_Count = 32,

	BSmthBlndGrassHexesWithOverlaps = 46,
	BSmthBlndGrassHexesWithOverlaps_Last = 62,
	BSmthBlndGrassHexesWithOverlaps_Count = 16,

	SmoothGrassTest = 62,
	SmoothGrassTest_Last = 78,
	SmoothGrassTest_Count = 16,

	RiverTest = 78,
	RiverTest_Last = 93,
	RiverTest_Count = 15,

	\terrainSpriteIndex_HowMany = 93
}; // enum \terrainSpriteIndex

#endif	// G_TERR_HPP
