/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TEMPDIB_HPP
#define TEMPDIB_HPP

#ifndef __cplusplus
#error tempdib.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Temporary DIB generator
 *
 *----------------------------------------------------------------------
 */

// #include <memory>
#include <map>
#include "dib.hpp"

class DrawDIBDC;
class DrawDIB;

class TempDIB
{
		DrawDIBDC* d_dib;
	public:
		TempDIB() : d_dib(0) { }
		~TempDIB();

		DrawDIBDC* get(PixelDimension w, PixelDimension h);
};

/*
 * Overlapped Mask DIB generator
 */

struct MaskDIBKey
{
	MaskDIBKey() :		// Empty constructor needed for STL
		d_dx(0),
		d_dy(0),
		d_w(0),
		d_h(0)
	{
	}

	MaskDIBKey(int dx, int dy, LONG w, LONG h) :
		d_dx(dx),
		d_dy(dy),
		d_w(w),
		d_h(h)
	{
	}

	int d_dx;
	int d_dy;
	LONG d_w;
	LONG d_h;

	friend bool operator < (const MaskDIBKey& k1, const MaskDIBKey& k2);
	friend bool operator == (const MaskDIBKey& k1, const MaskDIBKey& k2);
};


class MaskDIB
{
	public:
		MaskDIB() : d_dib(0) { }	// default constructor for STL
		MaskDIB(const MaskDIBKey& params) : d_dib(0) { create(params); }

		bool create(const MaskDIBKey& params);
      void release();   // swg: 8jun2001: to replace auto_ptr<>

		// const DrawDIB* get() const { return d_dib.get(); }
		const DrawDIB* get() const { return d_dib; }

		enum {
			NonMaskColor = 1,
			MaskColor = 0
		};

		friend bool operator < (const MaskDIB& lhs, const MaskDIB& rhs);
		friend bool operator == (const MaskDIB& lhs, const MaskDIB& rhs);

	private:
      //SWG: 8jun2001: Can not store auto_ptr in an STL container!
		// auto_ptr<DrawDIB> d_dib;
      DrawDIB* d_dib;
};

class OverlapMaskList
{
      typedef std::map< MaskDIBKey,MaskDIB,std::less<MaskDIBKey> > ItemType;
	public:
      // SWG: 8jun2001: Must manually destruct because not auto_ptr any more!
      ~OverlapMaskList();

		const DrawDIB* get(const MaskDIBKey& params);

	private:
		ItemType d_items;
};

#endif /* TEMPDIB_HPP */

