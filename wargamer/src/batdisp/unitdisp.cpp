/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Display Units on Battlefield
 *
 *----------------------------------------------------------------------
 */

#ifdef __WATCOM_CPLUSPLUS__
#pragma inline_depth 0      // Prevents compiler from locking up during optimization
#endif

#include "stdinc.hpp"
#include "unitdisp.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "hexmap.hpp"
#include "hexiter.hpp"
#include "bu_draw.hpp"
#include "batunit.hpp"
#include "unittype.hpp"
#include "bres.hpp"
#include "bdispob.hpp"
// #include "zlist.hpp"
#include "refptr.hpp"
#include "sprlib.hpp"
#include "bdispute.hpp"
#include "g_troops.hpp"
#include "random.hpp"

#include "animctrl.hpp"
#include "fx.hpp"
#include "scenario.hpp"
#include "control.hpp"

#include "mmtimer.hpp"
#include "options.hpp"

#if defined(DEBUG) && !defined(NOLOG)
#include "clog.hpp"
LogFileFlush UnitDispLog("UnitDisp.log");
#endif



/*

  NOTE : define this to enable full ASSERTs on artillery formations
  however - this makes the game unplayable !

*/

//#define DEBUG_ARTILLERY





namespace BattleDisplay {
   // precalc points for a SP formations
   UnitFormationPointsType UnitFormationPoints;
   void PrecalcFormations(const BattleData * d_batData);
   bool bFormationsPrecalced;
}

namespace               // Private Namespace
{

using BattleDisplay::DisplayObject;
using BattleDisplay::Displayer;
using BattleDisplay::DIBDisplayer;


class TroopSpriteManager
{
        public:
                TroopSpriteManager(SWG_Sprite::SpriteLibrary* sprLib);
                ~TroopSpriteManager();

                // Return sprite frame to display for a SP with facing & start frame details
                inline const SWG_Sprite::SpriteBlock* TroopSpriteManager::getGraphic(
                        SpriteTypeEnum sprite_type,
                        const UnitTypeItem& unitType,
                        HexPosition::Facing facing,
                        int graphic_index,
                        unsigned int frame_no);

                // Return sprite graphic index
                inline unsigned int TroopSpriteManager::getGraphicIndex(
                        SpriteTypeEnum sprite_type,
                        const UnitTypeItem& unitType,
                        HexPosition::Facing facing,
                        int graphic_index,
                        unsigned int frame_no);

                inline const SWG_Sprite::SpriteBlock* getGraphic(const BattleData* batData, const CRefBattleCP& cp);
                        // Return sprite to display for given Leader
//                const SWG_Sprite::SpriteBlock* getFlag(const BattleData* batData, const CRefBattleCP& cp);
                        // Return sprite to display for given Leader

                inline const SWG_Sprite::SpriteBlock * getFlagGraphic(int graphic_index, unsigned int frame_number);

                inline const SWG_Sprite::SpriteBlock * getGraphic(int graphic_index) {
                    return d_sprites->read(graphic_index);
                }


                inline int FacingToGraphicOffset(HexPosition::Facing facing, int graphics_per_facing);

        private:
                SWG_Sprite::SpriteLibrary* d_sprites;

                // static const char s_SpriteFileName[];
};

/*
 * Warning... hard wired filename.... should be in scenario file
 * and use the scenario folder.
 */

// static const char TroopSpriteManager::s_SpriteFileName[] = "nap1813\\troops";
// static const UnitPixelScale = ((float)BattleDisplayUtility::S_HexGraphicSize.x() * 1.6);           // scale at which graphics were drawn (1.2)
// static const float UnitPixelScaleRecip = (float) 1.0 / (float) UnitPixelScale;






TroopSpriteManager::TroopSpriteManager(SWG_Sprite::SpriteLibrary* sprLib) :
    d_sprites(sprLib)
{
    // StringPtr fileName = scenario->makeScenarioFileName("troops");
    // d_sprites = new SWG_Sprite::SpriteLibrary(fileName.toStr());
}

TroopSpriteManager::~TroopSpriteManager()
{
        // delete d_sprites;
}











inline int
TroopSpriteManager::FacingToGraphicOffset(HexPosition::Facing facing, int graphics_per_facing) {

int facing_offset;

    switch(facing) {
        case HexPosition::South : { facing_offset = 0; break; }
        case HexPosition::SouthWestFace : { facing_offset = 1 * graphics_per_facing; break; }
        case HexPosition::SouthWestPoint : { facing_offset = 1 * graphics_per_facing; break; }
        case HexPosition::West : { facing_offset = 1 * graphics_per_facing; break; }
        case HexPosition::NorthWestPoint : { facing_offset = 2 * graphics_per_facing; break; }
        case HexPosition::NorthWestFace : { facing_offset = 2 * graphics_per_facing; break; }
        case HexPosition::North : { facing_offset = 3 * graphics_per_facing; break; }
        case HexPosition::NorthEastFace : { facing_offset = 4 * graphics_per_facing; break; }
        case HexPosition::NorthEastPoint : { facing_offset = 4 * graphics_per_facing; break; }
        case HexPosition::East : { facing_offset = 5 * graphics_per_facing; break; }
        case HexPosition::SouthEastPoint : { facing_offset = 5 * graphics_per_facing; break; }
        case HexPosition::SouthEastFace : { facing_offset = 5 * graphics_per_facing; break; }

        default : { FORCEASSERT("Oops - Invalid unit facing direction during sprite draw.\n"); facing_offset = 0; break; }
    }

return facing_offset;

}


/*
inline int
TroopSpriteManager::FacingToGraphicOffset(HexPosition::Facing facing, int graphics_per_facing) {

int facing_offset;

    switch(facing) {
        case HexPosition::South : { facing_offset = 0; break; }
        case HexPosition::SouthWestFace : { facing_offset = 0; }
        case HexPosition::SouthWestPoint : { facing_offset = 1 * graphics_per_facing; break; }
        case HexPosition::West : { facing_offset = 1 * graphics_per_facing; break; }
        case HexPosition::NorthWestPoint : { facing_offset = 2 * graphics_per_facing; break; }
        case HexPosition::NorthWestFace : { facing_offset = 2 * graphics_per_facing; break; }
        case HexPosition::North : { facing_offset = 3 * graphics_per_facing; break; }
        case HexPosition::NorthEastFace : { facing_offset = 3 * graphics_per_facing; break; }
        case HexPosition::NorthEastPoint : { facing_offset = 4 * graphics_per_facing; break; }
        case HexPosition::East : { facing_offset = 4 * graphics_per_facing; break; }
        case HexPosition::SouthEastPoint : { facing_offset = 5 * graphics_per_facing; break; }
        case HexPosition::SouthEastFace : { facing_offset = 5 * graphics_per_facing; break; }

        default : { FORCEASSERT("Oops - Invalid unit facing direction during sprite draw.\n"); facing_offset = 0; break; }
    }

return facing_offset;

}
*/






/*
New GetGraphic function to handle multiple infantry types & varying frames of animation
*/

inline const SWG_Sprite::SpriteBlock *
TroopSpriteManager::getGraphic(
    SpriteTypeEnum sprite_type,
    const UnitTypeItem& unitType,
    HexPosition::Facing facing,
    int graphic_index,
    unsigned int frame_no) {

    ASSERT(d_sprites != 0);

    // sprites are all grouped by facing
    int graphics_per_facing;


    switch(sprite_type) {

        case SPRITE_NONE : { graphics_per_facing = 0; break; }
        // set parameters for infantry
        case SPRITE_INFANTRY : { graphics_per_facing = INFANTRY_TOTALFRAMES; break; }
        // set parameters for cavalry
        case SPRITE_CAVALRY : { graphics_per_facing = CAVALRY_TOTALFRAMES; break; }
        // set parameters for foot artillery
        case SPRITE_FOOT_ARTILLERY : { graphics_per_facing = FOOTARTILLERY_TOTALFRAMES; break; }
        // set parameters for horse artillery
        case SPRITE_HORSE_ARTILLERY : { graphics_per_facing = HORSEARTILLERY_TOTALFRAMES; break; }
        // set parameters for foot officer
        case SPRITE_OFFICER : { graphics_per_facing = OFFICER_TOTALFRAMES; break; }
        // set parameters for foot man
        case SPRITE_FOOTMAN : { graphics_per_facing = FOOTMAN_TOTALFRAMES; break; }
        // set parameters for horse man
        case SPRITE_HORSEMAN : { graphics_per_facing = HORSEMAN_TOTALFRAMES; break; }

        default : { FORCEASSERT("Oops - Invalid BasicUnitType when attempting SP GetGraphic.\n"); }

    }

        int facing_offset = FacingToGraphicOffset(facing, graphics_per_facing);

        SWG_Sprite::SpriteLibrary::Index gIndex;
        // get the appropriate bank of graphics
        gIndex = unitType.getSpriteIndex(graphic_index);
        // offset to get the correctly facing row of sprites
        gIndex+=facing_offset;
        // offset to get the correct frame of animation
        gIndex+=frame_no;

        const SWG_Sprite::SpriteBlock* sprite = d_sprites->read(gIndex);
        return sprite;
}






inline unsigned int
TroopSpriteManager::getGraphicIndex(
    SpriteTypeEnum sprite_type,
    const UnitTypeItem& unitType,
    HexPosition::Facing facing,
    int graphic_index,
    unsigned int frame_no) {

    ASSERT(d_sprites != 0);

    // sprites are all grouped by facing
    int graphics_per_facing;


    switch(sprite_type) {

        case SPRITE_NONE : { graphics_per_facing = 0; break; }
        // set parameters for infantry
        case SPRITE_INFANTRY : { graphics_per_facing = INFANTRY_TOTALFRAMES; break; }
        // set parameters for cavalry
        case SPRITE_CAVALRY : { graphics_per_facing = CAVALRY_TOTALFRAMES; break; }
        // set parameters for foot artillery
        case SPRITE_FOOT_ARTILLERY : { graphics_per_facing = FOOTARTILLERY_TOTALFRAMES; break; }
        // set parameters for horse artillery
        case SPRITE_HORSE_ARTILLERY : { graphics_per_facing = HORSEARTILLERY_TOTALFRAMES; break; }
        // set parameters for foot officer
        case SPRITE_OFFICER : { graphics_per_facing = OFFICER_TOTALFRAMES; break; }
        // set parameters for foot man
        case SPRITE_FOOTMAN : { graphics_per_facing = FOOTMAN_TOTALFRAMES; break; }
        // set parameters for horse man
        case SPRITE_HORSEMAN : { graphics_per_facing = HORSEMAN_TOTALFRAMES; break; }

        default : { FORCEASSERT("Oops - Invalid BasicUnitType when attempting SP GetGraphic.\n"); }

    }

        // get facing offset for graphics type
        int facing_offset = FacingToGraphicOffset(facing, graphics_per_facing);

        SWG_Sprite::SpriteLibrary::Index gIndex;

        // get the appropriate bank of graphics
        gIndex = unitType.getSpriteIndex(graphic_index);
        // offset to get the correctly facing row of sprites
        gIndex+=facing_offset;
        // offset to get the correct frame of animation
        gIndex+=frame_no;

        return gIndex;
}







inline const SWG_Sprite::SpriteBlock* TroopSpriteManager::getGraphic(const BattleData* batData, const CRefBattleCP& cp)
{
    FORCEASSERT("Oops - This is an old routine, which shouldn't be being called");
/*
        ASSERT(d_sprites != 0);

        // Hard coded results for now...

        const BattleOB* ob = batData->ob();

        static SWG_Sprite::SpriteLibrary::Index cpGraphicTable[2] =
                { Cavalry+0, Cavalry+48*3+0 };          // big Cavalry


        SWG_Sprite::SpriteLibrary::Index gIndex = cpGraphicTable[cp->getSide()];
*/
        const SWG_Sprite::SpriteBlock* sprite = d_sprites->read(0);
        return sprite;

}
/*
const SWG_Sprite::SpriteBlock* TroopSpriteManager::getFlag(const BattleData* batData, const CRefBattleCP& cp)
{
        ASSERT(d_sprites != 0);

        // Hard coded results for now...

        const BattleOB* ob = batData->ob();

        static SWG_Sprite::SpriteLibrary::Index cpGraphicTable[2] =
                { Flags+0, Flags+1 };           // big Cavalry


        SWG_Sprite::SpriteLibrary::Index gIndex = cpGraphicTable[cp->getSide()];

        const SWG_Sprite::SpriteBlock* sprite = d_sprites->read(gIndex);
        return sprite;
}
*/


inline const
SWG_Sprite::SpriteBlock* TroopSpriteManager::getFlagGraphic(int graphic_index, unsigned int frame_number) {

    SWG_Sprite::SpriteLibrary::Index gIndex;

    gIndex = graphic_index + frame_number;

    const SWG_Sprite::SpriteBlock* sprite = d_sprites->read(gIndex);

    return sprite;

}


/*
 * Simple StrengthPoint Drawer to get us going
 * just draws a solid rectangle
 */

class BattleSPDisplayObject : public DisplayObject
{

        public:
            typedef HexIterator::Scale Scale;


                BattleSPDisplayObject(const BattleSPDisplayObject& ob);
                BattleSPDisplayObject& operator = (const BattleSPDisplayObject& ob);

                BattleSPDisplayObject(const SWG_Sprite::SpriteBlock* sprite, const Scale& scale) :
                        d_sprite(sprite),
                        d_scale(scale)
                  { }

                virtual ~BattleSPDisplayObject() {  }
                virtual void draw(DrawDIB* dib, const PixelPoint& p) const;

        private:

                Scale d_scale;
                const SWG_Sprite::SpriteBlock* d_sprite;
};


// Bodge because we don't have template member functions

class RefPtrBattleSPDisplayObject : public RefPtr<BattleSPDisplayObject>
{
                typedef RefPtr<BattleSPDisplayObject> Super;

                // Don't allow any constructors except for creator
                RefPtrBattleSPDisplayObject();

        public:
                RefPtrBattleSPDisplayObject(const RefPtrBattleSPDisplayObject& ptr) : Super(ptr) { }

                // Creator constructor


                // RefPtrBattleSPDisplayObject(const SWG_Sprite::SpriteBlock* sprite, PixelDimension height) :
                //         Super(new BattleSPDisplayObject(sprite, height))
                // {
                // }

                RefPtrBattleSPDisplayObject(const SWG_Sprite::SpriteBlock* sprite, const BattleSPDisplayObject::Scale& scale) :
                        Super(new BattleSPDisplayObject(sprite, scale))
                {
                }


                operator RefPtr<DisplayObject> () const
                {
                        return RefPtr<DisplayObject>(value());
                }
};

void
BattleSPDisplayObject::draw(DrawDIB* dib, const PixelPoint& p) const
{

       BattleDisplayUtility::drawSquashedGraphic(d_sprite, dib, p, d_scale);

}



class BattleRectDisplayObject : public DisplayObject
{

        public:
                BattleRectDisplayObject(const BattleRectDisplayObject& ob);
                BattleRectDisplayObject& operator = (const BattleRectDisplayObject& ob);

                BattleRectDisplayObject(COLORREF col, const PixelPoint& size) :
                    d_colour(col),
                    d_size(size)
                {
                }

                virtual ~BattleRectDisplayObject() { }
                virtual void draw(DrawDIB* dib, const PixelPoint& p) const;

        private:
                COLORREF d_colour;
                PixelPoint d_size;
};

void BattleRectDisplayObject::draw(DrawDIB* dib, const PixelPoint& p) const
{
    dib->rect(p.x(), p.y(), d_size.x(), d_size.y(), dib->getColour(d_colour));
}

// Bodge because we don't have template member functions

class RefPtrBattleRectDisplayObject : public RefPtr<BattleRectDisplayObject>
{
                typedef RefPtr<BattleRectDisplayObject> Super;

                // Don't allow any constructors except for creator
                RefPtrBattleRectDisplayObject();

        public:
                RefPtrBattleRectDisplayObject(const RefPtrBattleRectDisplayObject& ptr) : Super(ptr) { }

                RefPtrBattleRectDisplayObject(COLORREF col, const PixelPoint& size) :
                        Super(new BattleRectDisplayObject(col, size))
                {
                }

                operator RefPtr<DisplayObject> () const
                {
                        return RefPtr<DisplayObject>(value());
                }
};


class BattleDrawUnit : public UnitDrawer {

                BattleDrawUnit(const BattleDrawUnit&);
                BattleDrawUnit& operator = (const BattleDrawUnit&);

        public:
                BattleDrawUnit(Displayer* dib, const HexIterator& iter, const BattleData* batData, TroopSpriteManager* sprLib);
                ~BattleDrawUnit() { }

                // Implement UnitDrawer
                virtual void draw(const BattleSP* sp);
                virtual void draw(const BattleCP* cp);
                virtual void draw(const BattleUnit* unit);              // unknown object

        private:
                // void DrawFormation(const BattleSP* sp, int num, const HexIterator& iter, const UnitTypeItem& unitType, const UBYTE* remapTable);
                // void DrawTweenFormation(const BattleSP* sp, int num, const HexIterator& iter, const UnitTypeItem& unitType, const UBYTE* remapTable);
                // void DrawFlag(const BattleCP * cp, const HexIterator& iter, BattleLocation pos);

                void DrawFormation(const BattleSP* sp, int num, const UnitTypeItem& unitType);
                void DrawTweenFormation(const BattleSP* sp, int num, const UnitTypeItem& unitType);
                void DrawFlag(const BattleCP * cp, BattleLocation pos, int UpdateAnim);

        private:
                const HexIterator& d_iter;
                const BattleData* d_batData;

                // how many frames to skip from last anim, due to passed time
                unsigned int d_FrameMultiplier;

            unsigned int d_UpdateAnimModifier;

                Displayer * d_display;

                // retrieve appropriate sprites
                TroopSpriteManager* d_spriteManager;

};


// static TroopSpriteManager BattleDrawUnit::s_spriteManager;


BattleDrawUnit::BattleDrawUnit(Displayer* display, const HexIterator& iter, const BattleData* batData, TroopSpriteManager* sprLib) :
        d_display(display),
        d_iter(iter),
        d_batData(batData),
        d_spriteManager(sprLib)
{

   d_UpdateAnimModifier = 1;


}


/*
Draw a Strength Point

If too small on screen, draw as a single pixel

Otherwise draw according to
    a) sp->basicType
    b) sp->formation

Each SP type has a separate function to draw a normal formation & an SP between formations

Drawing Infantry has a slight overhead above other SP types due to the possibility of multiple facings within the SP

*/

void BattleDrawUnit::draw(const BattleSP* sp) {

   Visibility::Value visibility;

   /*
   If this is a player-unit, or is an enemy with Visibility::Full set, or we are not using FogOfWar, then we display fully
   */
   if((GamePlayerControl::getControl(sp->parent()->getSide()) == GamePlayerControl::Player)
      || (sp->visibility() == Visibility::Full)
      || ((sp->visibility() != Visibility::NeverSeen) && !CampaignOptions::get(OPT_FogOfWar)))
   {
      visibility = Visibility::Full;
   }
   /*
   Otherwise use the visibility value held in its maphex
   */
   else
   {
      visibility = d_batData->map()->get(sp->hex()).d_visibility;
      // if hex is Visibility::NotSeen, we return immediately
      if(visibility <= Visibility::NotSeen)
         return;
   }

    const BattleOB* ob = d_batData->ob();

    // Get Basic Type
    const UnitTypeItem& unitType = ob->getUnitType(sp);
    BasicUnitType::value basicType = unitType.getBasicType();
   ASSERT(basicType < BasicUnitType::HowMany);

   // if we're at zero in any dimension, quit out
    if(d_iter.width() == 0 || d_iter.height() == 0) return;

   // if we're less than 3 width, just draw in side colour
    if(d_iter.width() <= 3) {

        Side unitside = sp->parent()->getSide();
        COLORREF sidecol = scenario->getSideColour(unitside);

        Rect<LONG> hexrect = d_iter.squareRect();

        RefPtrBattleRectDisplayObject ob(sidecol, PixelPoint(hexrect.width(), hexrect.height()));
        d_display->draw(ob, PixelPoint(hexrect.left(), hexrect.top()));

        return;
    }

   // otherwise, we're drawing the full sprite
    else {

       // get number of sprites to display
        int nSprites = imagesPerSP[basicType];
        nSprites = (sp->strength() * nSprites + BattleSP::SPStrength::MaxValue - 1) / BattleSP::SPStrength::MaxValue;

        // if this unit is dead
        if((nSprites == 0) && (sp->GetAnimInfo().LastNumSprites == 0)) return;

        // in-between formations
        if(sp->formation() == SP_ChangingFormation) {
            DrawTweenFormation(sp, nSprites, unitType);
        }
        // normal formations
        else {
            DrawFormation(sp, nSprites, unitType);
        }

    }

}





bool
IsOpposite(HexPosition::Facing dir_1, HexPosition::Facing dir_2) {

    int opposite_dir = dir_1 + (HexPosition::FacingResolution/2);
    if(opposite_dir >= HexPosition::FacingResolution) opposite_dir -= HexPosition::FacingResolution;

    if(opposite_dir == dir_2) return True;
    else return False;
}




int
AngleBetweenFacings(HexPosition::Facing src_dir, HexPosition::Facing dest_dir) {

    int difference = (int) src_dir - (int) dest_dir;

    if(difference > (HexPosition::FacingResolution/2)) difference-=(HexPosition::FacingResolution);
    if(difference < -(HexPosition::FacingResolution/2)) difference+=(HexPosition::FacingResolution);

    return difference;

}




// this is gonna have to be timed properly...
// cause on fast computers we'll see loads more explosions

// these chances are out fo 1000 (because 1 in 100 was too frequent)
#define EXPLOSION_CHANCE 1
//#define EXPLOSION_CHANCE 5
#define INFANTRY_SHOUT_CHANCE 250

#define INFANTRY_DEATHSCREAM_CHANCE 500
#define CAVALRY_DEATHSCREAM_CHANCE 500
#define ARTILLERY_DEATHSCREAM_CHANCE 500

void
GetExplosionGraphic(unsigned int * graphic_index, EffectTypeEnum * fx_type) {

    int r = random(0,8);
    switch(r) {

      case 0 : {
         *graphic_index = Expl1;
         *fx_type = EFFECT_EXPLOSION_SHORT;
         return;
      }
      case 1 : {
         *graphic_index = Expl2;
         *fx_type = EFFECT_EXPLOSION_SHORT;
         return;
      }
      case 2 : {
         *graphic_index = Expl3;
         *fx_type = EFFECT_EXPLOSION_SHORT;
         return;
      }
      case 3 : {
         *graphic_index = Expl4;
         *fx_type = EFFECT_EXPLOSION_SHORT;
         return;
      }
      case 4 : {
         *graphic_index = Explo16frame;
         *fx_type = EFFECT_EXPLOSION_LONG;
         return;
      }
      case 5 : {
         *graphic_index = Expl2_Explosion;
         *fx_type = EFFECT_EXPLOSION_LONG;
         return;
      }
      case 6 : {
         *graphic_index = Expl2_Air;
         *fx_type = EFFECT_EXPLOSION_LONG;
         return;
      }
      case 7 : {
         *graphic_index = Expl2_Smoke;
         *fx_type = EFFECT_EXPLOSION_LONG;
         return;
      }
        default : {
            FORCEASSERT("Oops - Invalid explosion number in get random explosion graphic");
            return; }
    }
}


#define AnimNextFrame   1000

void
BattleDrawUnit::DrawFormation(const BattleSP* sp, int num, const UnitTypeItem& unitType) {

   BattleCP * cp = const_cast<BattleCP *>(sp->parent());
    /*
    Get animation info
    */
    AnimationInfo & anim_info = sp->GetAnimInfo();
    int rand_start_index = anim_info.RandomVal;

    /*
    Check wether to update frame (should check unit movement speed also)
    */
    unsigned int DeltaFrames = (BattleDisplay::FrameCounter - anim_info.LastTime);
    anim_info.LastTime = BattleDisplay::FrameCounter;
   unsigned int cpSpeed = cp->speed();
   int UpdateAnim = cpSpeed + 1;
    UpdateAnim = UpdateAnim * DeltaFrames;
   UpdateAnim *= d_batData->timeRatio();

    /*
    Get facing information
    */
    int facing = sp->facing();
    SPFormation formation_type = sp->formation();

    /*
    Calculate display offset due to in-hex movement
    */
    int distance = sp->hexPosition().distance();
    if(sp->hexPosition().moveFrom() >= HexPosition::Center_East && sp->hexPosition().moveFrom() <= HexPosition::Center_Center) distance -= yards(110);

    BattleLocation origin = BattleDisplay::UnitFormationPoints.GetInHexPosition(sp->hexPosition().moveFrom());
    BattleLocation stepval = BattleDisplay::UnitFormationPoints.GetInHexUnitStepVal(sp->hexPosition().moveFrom(), sp->hexPosition().moveTo() );
    float scale = BattleDisplay::UnitFormationPoints.GetInHexUnitDistances(sp->hexPosition().moveFrom(), sp->hexPosition().moveTo() );

    int xoffset = origin.x() + (stepval.x() * (scale * distance) );
    int yoffset = origin.y() + (stepval.y() * (scale * distance) );

    /*
    Get the appropriate facing info
    */
    SPTypesEnum SPtype;
    switch(unitType.getBasicType() ) {
        case BasicUnitType::Infantry : { SPtype = SPTYPE_INFANTRY; break; }
        case BasicUnitType::Cavalry : { SPtype = SPTYPE_CAVALRY; break; }
        case BasicUnitType::Artillery : {
            if(! unitType.isMounted() ) SPtype = SPTYPE_FOOT_ARTILLERY;
            else SPtype = SPTYPE_HORSE_ARTILLERY;
            break;
        }
        default : FORCEASSERT("Oops - invalid SP unit type on battlefield (may only be Infantry, Cavalry, Foot-Artillery or Horse-Artillery)");
    }

    UnitFacingInfo * facing_info = BattleDisplay::UnitFormationPoints.GetUnitFacing(formation_type, facing, SPtype, NORMAL);

    /*
    Declare sprite-block pointers
    */
    const SWG_Sprite::SpriteBlock* sprBlock[MAX_GRAPHICSPERFORMATION];


   unsigned int canDeathScream = 0;
   unsigned int chanceDeathScream = 0;

    /*
    Play appropriate sounds dependent upon SP type and action
    */
   if(sp->takingMusketHits() || sp->inCloseCombat()) 
   {
      canDeathScream = 1;
      if(unitType.getBasicType() == BasicUnitType::Infantry) 
         d_batData->soundSystem()->triggerSound(GetCrowdSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
   }

    /*
    If being hit by cannon fire - put random explosions in hex
    */
    if(cp->takingGunHits() ) {
        if(UpdateAnim && (random(0,10000) < (EXPLOSION_CHANCE * d_batData->timeRatio())) ) {
            EffectsList * fxlist = const_cast<EffectsList *>(d_batData->effectsList() );
         unsigned int explosion_index;
         EffectTypeEnum fx_type;
         GetExplosionGraphic(&explosion_index, &fx_type);
            EffectDisplayInfo fx = EffectDisplayInfo(fx_type, explosion_index, BattleLocation(random(0,OneXHex)-HalfXHex, random(0,OneYHex)-HalfYHex));
            fx.Flags = EFFECT_DURATION_REPEATS;
            fx.Duration = 1;
            fxlist->add(d_iter.hex(), fx);

         canDeathScream = 1;
         chanceDeathScream = 75;
        }
    }

    /*
    Get graphics for each unit type
    */
    switch(SPtype) {

        /*
        SPTYPE_INFANTRY
        */

        case SPTYPE_INFANTRY : {

            /*
            Infantry firing whilst standing
            */
            if((sp->shootingMuskets() || cp->inCloseCombat()) && ( ! sp->isMoving())  ) {
                /*
            Setup frame timers for staggered firing
            */
                if(!anim_info.LastFiringState) {
                    anim_info.FrameCounters[0] = 7;
               anim_info.FrameCounters[1] = 8;
               anim_info.FrameCounters[2] = 11;
               anim_info.FrameCounters[3] = 13;
                    anim_info.LastFiringState = True;
                }
                for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) {
                    if(UpdateAnim) {
                        unsigned char flags = s_AnimationControl.UpdateInfantryFrame(SHOOTING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim, TRUE);
                        if(flags & ANIMCUE_MUSKETFIRE) 
                        {
                            if(num >= Half_Infantry_Numbers) 
                               d_batData->soundSystem()->triggerSound(GetMusketsSound(), BATTLESOUNDPRIORITY_MEDIUM, d_iter.hex() );
                            else 
                               d_batData->soundSystem()->triggerSound(GetMusketSound(), BATTLESOUNDPRIORITY_MEDIUM, d_iter.hex() );
                        }
                    }
                    sprBlock[f] = d_spriteManager->getGraphic(SPRITE_INFANTRY, unitType, facing_info->dir[f], f, s_AnimationControl.GetInfantryFrame(SHOOTING, anim_info.FrameCounters[f]));
                }
            break;
            }


            else {
                if(anim_info.LastFiringState) { anim_info.FrameCounters[0] = 0; anim_info.FrameCounters[1] = 0; anim_info.FrameCounters[2] = 0; anim_info.FrameCounters[3] = 0; anim_info.LastFiringState = False; }
                /*
                Infantry Moving
                */
                if(sp->isMoving() ) {

               /*
               Fleeing
               */
               if(cp->fleeingTheField() ) {
                  for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) {
                     if(UpdateAnim) { s_AnimationControl.UpdateInfantryFrame(RUNNING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim, FALSE); }
                     sprBlock[f] = d_spriteManager->getGraphic(SPRITE_INFANTRY, unitType, facing_info->dir[f], f, s_AnimationControl.GetInfantryFrame(RUNNING, anim_info.FrameCounters[f])) ;
                  }
               }

               /*
               Walking
               */
               else {
                  for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) {
                     if(UpdateAnim) { s_AnimationControl.UpdateInfantryFrame(WALKING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim, FALSE); }
                     sprBlock[f] = d_spriteManager->getGraphic(SPRITE_INFANTRY, unitType, facing_info->dir[f], f, s_AnimationControl.GetInfantryFrame(WALKING, anim_info.FrameCounters[f])) ;
                  }
               }
                }

                /*
                Infantry Standing
                */
                else {
                    for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
                    {
                        if(UpdateAnim) 
                        { 
                           s_AnimationControl.UpdateInfantryFrame(STANDING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim, FALSE); 
                        }
                        sprBlock[f] = d_spriteManager->getGraphic(SPRITE_INFANTRY, unitType, facing_info->dir[f], f, s_AnimationControl.GetInfantryFrame(STANDING, anim_info.FrameCounters[f]));
                    }
                }
            }
        break;
        }

        /*
        SPTYPE_CAVALRY
        */

        case SPTYPE_CAVALRY : {

         /*
         Melee
         */
            if((sp->shootingMuskets() || cp->inCloseCombat()) && ( ! sp->isMoving())  ) {

            if(UpdateAnim) {
                    anim_info.FrameCounters[0] = random(0, s_AnimationControl.GetNumCavalryFrames(MELEE) );
               anim_info.FrameCounters[1] = random(0, s_AnimationControl.GetNumCavalryFrames(MELEE) );
               anim_info.FrameCounters[2] = random(0, s_AnimationControl.GetNumCavalryFrames(MELEE) );
               anim_info.FrameCounters[3] = random(0, s_AnimationControl.GetNumCavalryFrames(MELEE) );
                    anim_info.LastFiringState = True;
                }
                d_batData->soundSystem()->triggerSound(GetSwordsSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
                for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
                {
                    if(UpdateAnim) s_AnimationControl.UpdateCavalryFrame(MELEE, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                    sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], f, s_AnimationControl.GetCavalryFrame(MELEE, anim_info.FrameCounters[f]));
                }
            break;
            }

         /*
         Charging
         */
         else if(cp->charging() && (!sp->shootingMuskets()) ) 
         {
                d_batData->soundSystem()->triggerSound(GetGallopSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
                for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
                {
                    if(UpdateAnim) 
                       s_AnimationControl.UpdateCavalryFrame(CHARGING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                    sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], 0, s_AnimationControl.GetCavalryFrame(CHARGING, anim_info.FrameCounters[f]));
                }
            break;
            }

         /*
         Cavalry moving
         */
            else if(sp->isMoving() ) {

            /*
            Walking
            */
            if(cpSpeed<19) 
            {
               d_batData->soundSystem()->triggerSound(GetGallopSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
               for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
               {
                  if(UpdateAnim)
                     s_AnimationControl.UpdateCavalryFrame(WALKING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                  sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], 0, s_AnimationControl.GetCavalryFrame(WALKING, anim_info.FrameCounters[f]));
               }
            }
            /*
            Trotting
            */
            else if(cpSpeed<38) 
            {
               d_batData->soundSystem()->triggerSound(GetGallopSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
               for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
               {
                  if(UpdateAnim)
                     s_AnimationControl.UpdateCavalryFrame(TROTTING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                  sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], 0, s_AnimationControl.GetCavalryFrame(TROTTING, anim_info.FrameCounters[f]));
               }
            }
            /*
            Galloping
            */
            else 
            {
               d_batData->soundSystem()->triggerSound(GetGallopSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
               for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
               {
                  if(UpdateAnim)
                     s_AnimationControl.UpdateCavalryFrame(GALLOPING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                  sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], 0, s_AnimationControl.GetCavalryFrame(GALLOPING, anim_info.FrameCounters[f]));
               }
            }
         break;
            }

         /*
         Cavalry Standing
         */
            else {
                for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) {
                    if(UpdateAnim) s_AnimationControl.UpdateCavalryFrame(STANDING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                    sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], 0, s_AnimationControl.GetCavalryFrame(STANDING, anim_info.FrameCounters[f]));
                }
            break;
            }
        }

        /*
        SPTYPE_FOOT_ARTILLERY
        */

        case SPTYPE_FOOT_ARTILLERY : {


         /*
         Foot-Artillery firing
         */
            if(sp->shootingGuns()) {

                if(UpdateAnim) {

                    unsigned char flags = s_AnimationControl.UpdateFootArtilleryFrame(SHOOTING, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], UpdateAnim);
                    if(flags & ANIMCUE_CANNONFIRE) 
                       d_batData->soundSystem()->triggerSound(GetCannonSound(), BATTLESOUNDPRIORITY_MEDIUM, d_iter.hex() );
                    s_AnimationControl.UpdateHorseManFrame(STANDING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], UpdateAnim);
                    s_AnimationControl.UpdateCavalryFrame(STANDING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], UpdateAnim);
                    s_AnimationControl.UpdateFootManFrame(STANDING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], UpdateAnim);
                }

                sprBlock[Index_0] = d_spriteManager->getGraphic(SPRITE_FOOT_ARTILLERY, unitType, facing_info->dir[Index_0], Index_0, s_AnimationControl.GetFootArtilleryFrame(SHOOTING, anim_info.FrameCounters[Index_0]));
                sprBlock[Index_1] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_1], Index_1, s_AnimationControl.GetHorseManFrame(SHOOTING, anim_info.FrameCounters[Index_1]));
                sprBlock[Index_2] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[Index_2], Index_2, s_AnimationControl.GetCavalryFrame(SHOOTING, anim_info.FrameCounters[Index_2]));
                sprBlock[Index_3] = d_spriteManager->getGraphic(SPRITE_FOOTMAN, unitType, facing_info->dir[Index_3], Index_3, s_AnimationControl.GetFootManFrame(STANDING, anim_info.FrameCounters[Index_3]));
            }

         /*
         Foot-Artillery moving
         */
            else if(sp->isMoving() ) {

                if(UpdateAnim) {
                    s_AnimationControl.UpdateFootArtilleryFrame(WALKING, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], UpdateAnim);
                    s_AnimationControl.UpdateHorseManFrame(WALKING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], UpdateAnim);
                    s_AnimationControl.UpdateCavalryFrame(WALKING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], UpdateAnim);
                    s_AnimationControl.UpdateFootManFrame(WALKING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], UpdateAnim);
                }

                sprBlock[Index_0] = d_spriteManager->getGraphic(SPRITE_FOOT_ARTILLERY, unitType, facing_info->dir[Index_0], Index_0, s_AnimationControl.GetFootArtilleryFrame(WALKING, anim_info.FrameCounters[Index_0]));
                sprBlock[Index_1] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_1], Index_1, s_AnimationControl.GetHorseManFrame(WALKING, anim_info.FrameCounters[Index_1]));
                sprBlock[Index_2] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[Index_2], Index_2, s_AnimationControl.GetCavalryFrame(WALKING, anim_info.FrameCounters[Index_2]));
                sprBlock[Index_3] = d_spriteManager->getGraphic(SPRITE_FOOTMAN, unitType, facing_info->dir[Index_3], Index_3, s_AnimationControl.GetFootManFrame(WALKING, anim_info.FrameCounters[Index_3]));

            }

         /*
         Foot-Artillery stading Limbered or Unlimbered
         */
            else {

                SpriteActionEnum artillery_action;
                if(formation_type == SP_LimberedFormation) artillery_action = STANDING_LIMBERED;
                else artillery_action = STANDING_UNLIMBERED;

                if(UpdateAnim) {
                    s_AnimationControl.UpdateFootArtilleryFrame(artillery_action, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], UpdateAnim);
                    s_AnimationControl.UpdateHorseManFrame(STANDING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], UpdateAnim);
                    s_AnimationControl.UpdateCavalryFrame(STANDING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], UpdateAnim);
                    s_AnimationControl.UpdateFootManFrame(STANDING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], UpdateAnim);
                }

                sprBlock[Index_0] = d_spriteManager->getGraphic(SPRITE_FOOT_ARTILLERY, unitType, facing_info->dir[Index_0], Index_0, s_AnimationControl.GetFootArtilleryFrame(artillery_action, anim_info.FrameCounters[Index_0]));
                sprBlock[Index_1] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_1], Index_1, s_AnimationControl.GetHorseManFrame(STANDING, anim_info.FrameCounters[Index_1]));
                sprBlock[Index_2] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[Index_2], Index_2, s_AnimationControl.GetCavalryFrame(STANDING, anim_info.FrameCounters[Index_2]));
                sprBlock[Index_3] = d_spriteManager->getGraphic(SPRITE_FOOTMAN, unitType, facing_info->dir[Index_3], Index_3, s_AnimationControl.GetFootManFrame(STANDING, anim_info.FrameCounters[Index_3]));
            }
        break;
        }

        /*
        SPTYPE_HORSE_ARTILLERY
        */
        case SPTYPE_HORSE_ARTILLERY : {

         /*
         Horse-Artillery firing
         */
            if(sp->shootingGuns()) {

                if(UpdateAnim) {
                    unsigned char flags = s_AnimationControl.UpdateHorseArtilleryFrame(SHOOTING, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], UpdateAnim);
                    if(flags & ANIMCUE_CANNONFIRE) 
                       d_batData->soundSystem()->triggerSound(GetCannonSound(), BATTLESOUNDPRIORITY_MEDIUM, d_iter.hex() );
                    s_AnimationControl.UpdateHorseManFrame(STANDING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], UpdateAnim);
                    s_AnimationControl.UpdateHorseManFrame(STANDING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], UpdateAnim);
                    s_AnimationControl.UpdateCavalryFrame(STANDING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], UpdateAnim);
                }

                sprBlock[Index_0] = d_spriteManager->getGraphic(SPRITE_HORSE_ARTILLERY, unitType, facing_info->dir[Index_0], Index_0, s_AnimationControl.GetHorseArtilleryFrame(SHOOTING, anim_info.FrameCounters[Index_0]));
                sprBlock[Index_1] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_1], Index_1, s_AnimationControl.GetHorseManFrame(SHOOTING, anim_info.FrameCounters[Index_1]));
                sprBlock[Index_2] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_2], Index_2, s_AnimationControl.GetHorseManFrame(STANDING, anim_info.FrameCounters[Index_2]));
                sprBlock[Index_3] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[Index_3], Index_3, s_AnimationControl.GetCavalryFrame(STANDING, anim_info.FrameCounters[Index_3]));
            }

         /*
         Horse-Artillery moving
         */
            else if(sp->isMoving() ) {

                if(UpdateAnim) {
                    s_AnimationControl.UpdateHorseArtilleryFrame(WALKING, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], UpdateAnim);
                    s_AnimationControl.UpdateHorseManFrame(WALKING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], UpdateAnim);
                    s_AnimationControl.UpdateHorseManFrame(WALKING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], UpdateAnim);
                    s_AnimationControl.UpdateCavalryFrame(WALKING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], UpdateAnim);
                }

                sprBlock[Index_0] = d_spriteManager->getGraphic(SPRITE_HORSE_ARTILLERY, unitType, facing_info->dir[Index_0], Index_0, s_AnimationControl.GetHorseArtilleryFrame(WALKING, anim_info.FrameCounters[Index_0]));
                sprBlock[Index_1] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_1], Index_1, s_AnimationControl.GetHorseManFrame(WALKING, anim_info.FrameCounters[Index_1]));
                sprBlock[Index_2] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_2], Index_2, s_AnimationControl.GetHorseManFrame(WALKING, anim_info.FrameCounters[Index_2]));
                sprBlock[Index_3] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[Index_3], Index_3, s_AnimationControl.GetCavalryFrame(WALKING, anim_info.FrameCounters[Index_3]));
            }

         /*
         Horse-Artillert standing Limbered or Unlimbered
         */
            else {

                SpriteActionEnum artillery_action;
                if(formation_type == SP_LimberedFormation) artillery_action = STANDING_LIMBERED;
                else artillery_action = STANDING_UNLIMBERED;

                if(UpdateAnim) {
                    s_AnimationControl.UpdateHorseArtilleryFrame(artillery_action, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], 1);
                    s_AnimationControl.UpdateHorseManFrame(STANDING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], 1);
                    s_AnimationControl.UpdateHorseManFrame(STANDING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], 1);
                    s_AnimationControl.UpdateCavalryFrame(STANDING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], 1);
                }

                sprBlock[Index_0] = d_spriteManager->getGraphic(SPRITE_HORSE_ARTILLERY, unitType, facing_info->dir[Index_0], Index_0, s_AnimationControl.GetHorseArtilleryFrame(artillery_action, anim_info.FrameCounters[Index_0]));
                sprBlock[Index_1] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_1], Index_1, s_AnimationControl.GetHorseManFrame(STANDING, anim_info.FrameCounters[Index_1]));
                sprBlock[Index_2] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_2], Index_2, s_AnimationControl.GetHorseManFrame(STANDING, anim_info.FrameCounters[Index_2]));
                sprBlock[Index_3] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[Index_3], Index_3, s_AnimationControl.GetCavalryFrame(STANDING, anim_info.FrameCounters[Index_3]));
            }
        break;
        }
    }

    /*
    Get sprites
    */
    SWG_Sprite::SpriteBlockPtr sprite1(sprBlock[0]);
    SWG_Sprite::SpriteBlockPtr sprite2(sprBlock[1]);
    SWG_Sprite::SpriteBlockPtr sprite3(sprBlock[2]);
    SWG_Sprite::SpriteBlockPtr sprite4(sprBlock[3]);

    /*
    Release sprite block pointers
    */
    for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) sprBlock[f]->release();

    /*
    Ensure sprite heights are > 0
    */
    HexIterator::Scale drawScale = d_iter.troopScale();
    RefPtrBattleSPDisplayObject spDraw1(sprite1, drawScale);
    RefPtrBattleSPDisplayObject spDraw2(sprite2, drawScale);
    RefPtrBattleSPDisplayObject spDraw3(sprite3, drawScale);
    RefPtrBattleSPDisplayObject spDraw4(sprite4, drawScale);


    /*
    Main loop to draw units
    */
    int xpos, ypos;

    int index;
    unsigned int old_num = anim_info.LastNumSprites;
    for(int i=0; i<old_num; i++) {

        /*
        Get random unit index
        */
        index = BattleDisplay::UnitFormationPoints.GetFormationIndex(anim_info.RandomFormationVal, SPtype, i);
        /*
        Get unit's position
        */
        UnitPointInfo * pos_info = BattleDisplay::UnitFormationPoints.GetUnitPoint(formation_type, facing, SPtype, index, NORMAL);

        /*
        Get random unit's offsets
        */
        xpos = ( pos_info->pos.x() + xoffset + BattleDisplay::UnitFormationPoints.GetUnitXPositionOffset(SPtype, rand_start_index+index) );
        ypos = ( pos_info->pos.y() + yoffset + BattleDisplay::UnitFormationPoints.GetUnitYPositionOffset(SPtype, rand_start_index+index) );

        /*
        Draw a dead unit if necessary
        */

        if((i >= num) && Options::get(OPT_DeadBodies)) {

            unsigned int fx_index;

            switch(SPtype) {

         case SPTYPE_INFANTRY : {
                    fx_index = d_spriteManager->getGraphicIndex(SPRITE_INFANTRY, unitType, RandomFacing(), 0, s_AnimationControl.GetInfantryFrame(DEAD, 0) );
               if(canDeathScream) chanceDeathScream += 10;
                    break;
                }

            case SPTYPE_CAVALRY : {
                fx_index = d_spriteManager->getGraphicIndex(SPRITE_CAVALRY, unitType, RandomFacing(), 0, s_AnimationControl.GetCavalryFrame(DEAD, 0) );
                if(canDeathScream) chanceDeathScream += 10;
                break;
            }

            case SPTYPE_FOOT_ARTILLERY : {
                switch(i) {
                    case 0 : {
                        fx_index = d_spriteManager->getGraphicIndex(SPRITE_FOOT_ARTILLERY, unitType, RandomFacing(), Index_0, s_AnimationControl.GetFootArtilleryFrame(DEAD, 0));
                  if(canDeathScream) chanceDeathScream += 10;
                        break;
                    }
                    case 1 : {
                        fx_index = d_spriteManager->getGraphicIndex(SPRITE_HORSEMAN, unitType, RandomFacing(), Index_1, s_AnimationControl.GetHorseManFrame(DEAD, 0));
                        break;
                    }
                    case 2 : {
                        fx_index = d_spriteManager->getGraphicIndex(SPRITE_CAVALRY, unitType, RandomFacing(), Index_2, s_AnimationControl.GetCavalryFrame(DEAD, 0));
                        break;
                    }
                    case 3 : {
                        fx_index = d_spriteManager->getGraphicIndex(SPRITE_FOOTMAN, unitType, RandomFacing(), Index_3, s_AnimationControl.GetFootManFrame(DEAD, 0));
                        break;
                    }
                    default : {
                        FORCEASSERT("Oops - Invalid index when drawing dead foot-artillery (may only be 0...3)");
                        return;
                    }
                }
            break;
            }

            case SPTYPE_HORSE_ARTILLERY : {
                switch(i) {
                    case 0 : {
                        fx_index = d_spriteManager->getGraphicIndex(SPRITE_FOOT_ARTILLERY, unitType, RandomFacing(), Index_0, s_AnimationControl.GetFootArtilleryFrame(DEAD, 0));
                        if(canDeathScream) chanceDeathScream += 10;
                        break;
                    }
                    case 1 : {
                        fx_index = d_spriteManager->getGraphicIndex(SPRITE_HORSEMAN, unitType, RandomFacing(), Index_1, s_AnimationControl.GetHorseManFrame(DEAD, 0));
                        break;
                    }
                    case 2 : {
                        fx_index = d_spriteManager->getGraphicIndex(SPRITE_HORSEMAN, unitType, RandomFacing(), Index_2, s_AnimationControl.GetHorseManFrame(DEAD, 0));
                        break;
                    }
                    case 3 : {
                        fx_index = d_spriteManager->getGraphicIndex(SPRITE_CAVALRY, unitType, RandomFacing(), Index_3, s_AnimationControl.GetCavalryFrame(DEAD, 0));
                        break;
                    }
                    default : {
                        FORCEASSERT("Oops - Invalid index when drawing dead horse-artillery (may only be 0...3)");
                        return;
                    }
                }
            break;
         }

            default : FORCEASSERT("Oops - invalid SP unit type when drawing dead unit");
        }

            /*
            Send dead unit graphic to FX list
            */
            EffectDisplayInfo fx(EFFECT_DEADUNIT, fx_index, BattleLocation(xpos, ypos));
            fx.Flags = EFFECT_DURATION_INFINITE;
            EffectsList * fxlist = const_cast<EffectsList *>(d_batData->effectsList() );
            fxlist->add(d_iter.hex(), fx);
        }

        /*
        Draw unit
        */

        else {

            PixelPoint pos(d_iter.position(BattleLocation(xpos, ypos )));

            switch(pos_info->graphic_number) {
                case 0 : { d_display->draw(spDraw1, pos); break; }
                case 1 : { d_display->draw(spDraw2, pos); break; }
                case 2 : { d_display->draw(spDraw3, pos); break; }
                case 3 : { d_display->draw(spDraw4, pos); break; }
                default : FORCEASSERT("Oops - invalid graphic index in unit pos table\n");
            }
        }

    }  // step through whole formation

   /*
   Trigger death sounds
   */
   if(canDeathScream && Options::get(OPT_DeadBodies)) {

      chanceDeathScream *= canDeathScream;

      switch(SPtype) {
         case SPTYPE_INFANTRY : {
            int r = random(0,INFANTRY_DEATHSCREAM_CHANCE);
            if(r < chanceDeathScream) 
            {
               d_batData->soundSystem()->triggerSound(GetDeathSound(), BATTLESOUNDPRIORITY_LOW, d_iter.hex() );
            }
            break;
         }
         case SPTYPE_CAVALRY : {
            int r = random(0,CAVALRY_DEATHSCREAM_CHANCE);
            if(r < chanceDeathScream) 
            {
               d_batData->soundSystem()->triggerSound(GetWhinnySound(), BATTLESOUNDPRIORITY_LOW, d_iter.hex() );
            }
            break;
         }
         case SPTYPE_FOOT_ARTILLERY : {
            int r = random(0,ARTILLERY_DEATHSCREAM_CHANCE);
            if(r < chanceDeathScream) 
            {
               d_batData->soundSystem()->triggerSound(GetSmashSound(), BATTLESOUNDPRIORITY_LOW, d_iter.hex() );
            }
            break;
         }
         case SPTYPE_HORSE_ARTILLERY : {
            int r = random(0,ARTILLERY_DEATHSCREAM_CHANCE);
            if(r < chanceDeathScream) 
            {
               d_batData->soundSystem()->triggerSound(GetSmashSound(), BATTLESOUNDPRIORITY_LOW, d_iter.hex() );
            }
            break;
         }
      }
   }

   // set new number of sprites
   anim_info.LastNumSprites = num;


   HexCord centerHex;
   if(cp->centerHexIfActive(&centerHex)) {
      if(d_iter.hex() == centerHex) DrawFlag(cp, BattleLocation(xoffset, yoffset), UpdateAnim );
   }

}









void
BattleDrawUnit::DrawTweenFormation(const BattleSP* sp, int num, const UnitTypeItem& unitType) {

   BattleCP * cp = const_cast<BattleCP *>(sp->parent());

    /*
    Get animation info
    */
    AnimationInfo & anim_info = sp->GetAnimInfo();
    int rand_start_index = anim_info.RandomVal;

    /*
    Check wether to update frame (should check unit movement speed also)
    */

    unsigned int DeltaFrames = (BattleDisplay::FrameCounter - anim_info.LastTime);
    anim_info.LastTime = BattleDisplay::FrameCounter;
   unsigned int cpSpeed = cp->speed();
   int UpdateAnim = cpSpeed + 1;
    UpdateAnim = UpdateAnim * DeltaFrames;
   UpdateAnim *= d_batData->timeRatio();

    /*
    Get facing information
    */
    int facing = sp->facing();
    SPFormation formation_type = sp->formation();

    int dx,dy;
    float tween = sp->formationTween().normalise();

    /*
    Work out which formations / facings to change between
    */
    HexPosition::Facing src_facing = sp->facing();
    HexPosition::Facing dest_facing = sp->wantFacing();
    SPFormation src_formation = sp->fromFormation();
    SPFormation dest_formation = sp->destFormation();

    bool FacingChange;
    if(src_facing == dest_facing) FacingChange=0;
    else FacingChange = 1;

    bool FormationChange;
    if(src_formation == dest_formation) FormationChange=0;
    else FormationChange=1;

    ASSERT(FacingChange || FormationChange);
    ASSERT( ! (FacingChange && FormationChange) );

    /*
    Calculate display offset due to in-hex movement
    */

    int distance = sp->hexPosition().distance();
    if(sp->hexPosition().moveFrom() >= HexPosition::Center_East && sp->hexPosition().moveFrom() <= HexPosition::Center_Center) distance -= yards(110);
    BattleLocation origin = BattleDisplay::UnitFormationPoints.GetInHexPosition(sp->hexPosition().moveFrom());
    BattleLocation stepval = BattleDisplay::UnitFormationPoints.GetInHexUnitStepVal(sp->hexPosition().moveFrom(), sp->hexPosition().moveTo() );
    float scale = BattleDisplay::UnitFormationPoints.GetInHexUnitDistances(sp->hexPosition().moveFrom(), sp->hexPosition().moveTo() );
    int xoffset = origin.x() + (stepval.x() * (scale * distance) );
    int yoffset = origin.y() + (stepval.y() * (scale * distance) );

    BattleLocation unit_location(
        (QuantaPerXHex *d_iter.hex().x() ) + xoffset,
        (QuantaPerYHex * d_iter.hex().y() ) + yoffset
    );

    /*
    Get the appropriate facing info
    */

    SPTypesEnum SPtype;
    switch(unitType.getBasicType() ) {
        case BasicUnitType::Infantry : { SPtype = SPTYPE_INFANTRY; break; }
        case BasicUnitType::Cavalry : { SPtype = SPTYPE_CAVALRY; break; }
        case BasicUnitType::Artillery : {
            if(! unitType.isMounted() ) SPtype = SPTYPE_FOOT_ARTILLERY;
            else SPtype = SPTYPE_HORSE_ARTILLERY;
            break;
        }
        default : FORCEASSERT("Oops - invalid SP unit type on battlefield (may only be Infantry, Cavalry, Foot-Artillery or Horse-Artillery)");
    }

    UnitFacingInfo * src_facing_info = BattleDisplay::UnitFormationPoints.GetUnitFacing(src_formation, src_facing, SPtype, NORMAL);
    UnitFacingInfo * dest_facing_info = BattleDisplay::UnitFormationPoints.GetUnitFacing(dest_formation, dest_facing, SPtype, NORMAL);
    /*
   switch to destination facing half-way
   */
    UnitFacingInfo * facing_info;
    if(tween < 0.5) facing_info = src_facing_info;
    else facing_info = dest_facing_info;

    /*
    Declare sprite-block pointers
    */
    const SWG_Sprite::SpriteBlock* sprBlock[MAX_GRAPHICSPERFORMATION];


   unsigned int canDeathScream = 0;
   unsigned int chanceDeathScream = 0;

    /*
    Play appropriate sounds dependent upon SP type and action
    */
   if(sp->takingMusketHits() || sp->inCloseCombat()) 
   {
      canDeathScream = 1;
      if(unitType.getBasicType() == BasicUnitType::Infantry) 
         d_batData->soundSystem()->triggerSound(GetCrowdSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
   }

    /*
    If being hit by cannon fire - put random explosions in hex
    */
    if(cp->takingGunHits() ) {
      if(UpdateAnim && (random(0,10000) < (EXPLOSION_CHANCE * d_batData->timeRatio())) ) {
            EffectsList * fxlist = const_cast<EffectsList *>(d_batData->effectsList() );
         unsigned int explosion_index;
         EffectTypeEnum fx_type;
         GetExplosionGraphic(&explosion_index, &fx_type);
            EffectDisplayInfo fx = EffectDisplayInfo(fx_type, explosion_index, BattleLocation(random(0,OneXHex)-HalfXHex, random(0,OneYHex)-HalfYHex));
            fx.Flags = EFFECT_DURATION_REPEATS;
            fx.Duration = 1;
            fxlist->add(d_iter.hex(), fx);

         canDeathScream = 1;
         chanceDeathScream = 75;
        }
    }

    /*
    Get graphics for each unit type
    */
    switch(SPtype) {

        /*
        SPTYPE_INFANTRY
        */
        case SPTYPE_INFANTRY : {

         /*
         Reset numbers set up for staggered firing
         */
            if(anim_info.LastFiringState) {
            anim_info.FrameCounters[0] = 0;
            anim_info.FrameCounters[1] = 0;
            anim_info.FrameCounters[2] = 0;
            anim_info.FrameCounters[3] = 0;
            anim_info.LastFiringState = False; }
            /*
            Infantry walking
            */
            for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) {
                if(UpdateAnim) { s_AnimationControl.UpdateInfantryFrame(WALKING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim, FALSE); }
                sprBlock[f] = d_spriteManager->getGraphic(SPRITE_INFANTRY, unitType, facing_info->dir[f], f, s_AnimationControl.GetInfantryFrame(WALKING, anim_info.FrameCounters[f]));
            }
        break;
        }

        /*
        SPTYPE_CAVALRY
        */
        case SPTYPE_CAVALRY : {

         /*
         Cavalry charging
         */
         if(cp->charging() && (!sp->shootingMuskets()) ) 
         {
                d_batData->soundSystem()->triggerSound(GetGallopSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
                for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
                {
                    if(UpdateAnim)
                       s_AnimationControl.UpdateCavalryFrame(CHARGING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                    sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], 0, s_AnimationControl.GetCavalryFrame(CHARGING, anim_info.FrameCounters[f]));
                }
            break;
            }

         /*
         Cavalry moving
         */
         else {

            /*
            Walking
            */
            if(cpSpeed<19) 
            {
               d_batData->soundSystem()->triggerSound(GetGallopSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
               for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
               {
                  if(UpdateAnim)
                     s_AnimationControl.UpdateCavalryFrame(WALKING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                  sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], 0, s_AnimationControl.GetCavalryFrame(WALKING, anim_info.FrameCounters[f]));
               }
            }
            /*
            Trotting
            */
            else if(cpSpeed<38) 
            {
               d_batData->soundSystem()->triggerSound(GetGallopSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
               for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
               {
                  if(UpdateAnim)
                     s_AnimationControl.UpdateCavalryFrame(TROTTING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                  sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], 0, s_AnimationControl.GetCavalryFrame(TROTTING, anim_info.FrameCounters[f]));
               }
            }
            /*
            Galloping
            */
            else 
            {
               d_batData->soundSystem()->triggerSound(GetGallopSound(), BATTLESOUNDPRIORITY_UNIQUE, d_iter.hex() );
               for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) 
               {
                  if(UpdateAnim)
                     s_AnimationControl.UpdateCavalryFrame(GALLOPING, anim_info.FrameCounters[f], anim_info.FrameTimers[f], UpdateAnim);
                  sprBlock[f] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[f], 0, s_AnimationControl.GetCavalryFrame(GALLOPING, anim_info.FrameCounters[f]));
               }
            }
         break;
         }
      }

        /*
        SPTYPE_FOOT_ARTILLERY
        */

        case SPTYPE_FOOT_ARTILLERY : {

         ASSERT(!sp->shootingGuns());

         /*
         Find out whether we're Moving or Limbering / Unlimbering
         */
            SpriteActionEnum artillery_action;
            if(FacingChange) artillery_action = WALKING;
         else {
            if(dest_formation == SP_LimberedFormation) artillery_action = LIMBERING;
                else artillery_action = UNLIMBERING;
            }

            if(UpdateAnim) {
                s_AnimationControl.UpdateFootArtilleryFrame(artillery_action, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], UpdateAnim);
                s_AnimationControl.UpdateHorseManFrame(WALKING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], UpdateAnim);
                s_AnimationControl.UpdateCavalryFrame(WALKING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], UpdateAnim);
                s_AnimationControl.UpdateFootManFrame(WALKING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], UpdateAnim);
            }

            sprBlock[Index_0] = d_spriteManager->getGraphic(SPRITE_FOOT_ARTILLERY, unitType, facing_info->dir[Index_0], Index_0, s_AnimationControl.GetFootArtilleryFrame(artillery_action, anim_info.FrameCounters[Index_0]));
            sprBlock[Index_1] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_1], Index_1, s_AnimationControl.GetHorseManFrame(WALKING, anim_info.FrameCounters[Index_1]));
            sprBlock[Index_2] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[Index_2], Index_2, s_AnimationControl.GetCavalryFrame(WALKING, anim_info.FrameCounters[Index_2]));
            sprBlock[Index_3] = d_spriteManager->getGraphic(SPRITE_FOOTMAN, unitType, facing_info->dir[Index_3], Index_3, s_AnimationControl.GetFootManFrame(WALKING, anim_info.FrameCounters[Index_3]));

            break;
        }

        /*
        SPTYPE_HORSE_ARTILLERY
        */

        case SPTYPE_HORSE_ARTILLERY : {

         ASSERT(!sp->shootingGuns());

         /*
         Find out whether we're Moving or Limbering / Unlimbering
         */
            SpriteActionEnum artillery_action;
            if(FacingChange) { artillery_action = WALKING; }
            else {
              if(dest_formation == SP_LimberedFormation) artillery_action = LIMBERING;
                else artillery_action = UNLIMBERING;
            }

            if(UpdateAnim) {
                s_AnimationControl.UpdateHorseArtilleryFrame(artillery_action, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], UpdateAnim);
                s_AnimationControl.UpdateHorseManFrame(WALKING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], UpdateAnim);
                s_AnimationControl.UpdateHorseManFrame(WALKING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], UpdateAnim);
                s_AnimationControl.UpdateCavalryFrame(WALKING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], UpdateAnim);
            }

         sprBlock[Index_0] = d_spriteManager->getGraphic(SPRITE_HORSE_ARTILLERY, unitType, facing_info->dir[Index_0], Index_0, s_AnimationControl.GetHorseArtilleryFrame(artillery_action, anim_info.FrameCounters[Index_0]));
            sprBlock[Index_1] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_1], Index_1, s_AnimationControl.GetHorseManFrame(WALKING, anim_info.FrameCounters[Index_1]));
            sprBlock[Index_2] = d_spriteManager->getGraphic(SPRITE_HORSEMAN, unitType, facing_info->dir[Index_2], Index_2, s_AnimationControl.GetHorseManFrame(WALKING, anim_info.FrameCounters[Index_2]));
            sprBlock[Index_3] = d_spriteManager->getGraphic(SPRITE_CAVALRY, unitType, facing_info->dir[Index_3], Index_3, s_AnimationControl.GetCavalryFrame(WALKING, anim_info.FrameCounters[Index_3]));
        }

    }

    /*
    Get sprites
    */
    SWG_Sprite::SpriteBlockPtr sprite1(sprBlock[0]);
    SWG_Sprite::SpriteBlockPtr sprite2(sprBlock[1]);
    SWG_Sprite::SpriteBlockPtr sprite3(sprBlock[2]);
    SWG_Sprite::SpriteBlockPtr sprite4(sprBlock[3]);

    /*
    Releasae sprite block pointers
    */
    for(int f=0; f<MAX_GRAPHICSPERFORMATION; f++) sprBlock[f]->release();

    HexIterator::Scale drawScale = d_iter.troopScale();
    RefPtrBattleSPDisplayObject spDraw1(sprite1, drawScale);
    RefPtrBattleSPDisplayObject spDraw2(sprite2, drawScale);
    RefPtrBattleSPDisplayObject spDraw3(sprite3, drawScale);
    RefPtrBattleSPDisplayObject spDraw4(sprite4, drawScale);

    /*
    Main loop to draw units
    */
    UnitPointInfo * src_pos_info;
    UnitPointInfo * dest_pos_info;
    int xpos, ypos;

    int index;
    unsigned int old_num = anim_info.LastNumSprites;
    for(int i=0; i<old_num; i++) {

        /*
        Get random unit index
        */
        index = BattleDisplay::UnitFormationPoints.GetFormationIndex(anim_info.RandomFormationVal, SPtype, i);
        /*
        Get unit's position (source is from 'index', dest is from 'i' - to achieve a random look)
        */
        src_pos_info = BattleDisplay::UnitFormationPoints.GetUnitPoint(src_formation, src_facing, SPtype, index, NORMAL);
        dest_pos_info = BattleDisplay::UnitFormationPoints.GetUnitPoint(dest_formation, dest_facing, SPtype, i, NORMAL);

        /*
        Changing formation - dx & xy are linear
        */
        if(FormationChange) {
            // get distance between dest & src points
            dx = dest_pos_info->pos.x() - src_pos_info->pos.x();
            dy = dest_pos_info->pos.y() - src_pos_info->pos.y();
            dx*=tween;
            dy*=tween;

            // set initial positions
            xpos = (src_pos_info->pos.x() + dx + xoffset + BattleDisplay::UnitFormationPoints.GetUnitXPositionOffset(SPtype, rand_start_index+index) );
            ypos = (src_pos_info->pos.y() + dy + yoffset + BattleDisplay::UnitFormationPoints.GetUnitYPositionOffset(SPtype, rand_start_index+index) );
        }

        /*
        Changing facing - dx & dy are rotated around formation origin
        */
        if(FacingChange) {

            int int_angle;
            int_angle = AngleBetweenFacings(src_facing, dest_facing);
            int_angle *= tween;
            if(int_angle < 0) int_angle += HexPosition::FacingResolution;
            if(int_angle >= HexPosition::FacingResolution) int_angle -= HexPosition::FacingResolution;

            HexPosition::Facing rot_val = int_angle;
            dx = mcos(src_pos_info->pos.x(), HexPosition::wangle(rot_val) ) + -msin(src_pos_info->pos.y(), HexPosition::wangle(rot_val) );
            dy = msin(src_pos_info->pos.x(), HexPosition::wangle(rot_val) ) + mcos(src_pos_info->pos.y(), HexPosition::wangle(rot_val) );

            // set initial positions
            xpos = (dx + xoffset + BattleDisplay::UnitFormationPoints.GetUnitXPositionOffset(SPtype, rand_start_index+index) );
            ypos = (dy + yoffset + BattleDisplay::UnitFormationPoints.GetUnitYPositionOffset(SPtype, rand_start_index+index) );
        }



        /*
        Draw a dead unit if necessary
        */
        if((i > num) && Options::get(OPT_DeadBodies)) {

            unsigned int fx_index;

            switch(SPtype) {

                case SPTYPE_INFANTRY : {
                    fx_index = d_spriteManager->getGraphicIndex(SPRITE_INFANTRY, unitType, RandomFacing(), 0, s_AnimationControl.GetInfantryFrame(DEAD, 0) );
                    if(canDeathScream) chanceDeathScream += 10;
                    break;
                }

                case SPTYPE_CAVALRY : {
                    fx_index = d_spriteManager->getGraphicIndex(SPRITE_CAVALRY, unitType, RandomFacing(), 0, s_AnimationControl.GetCavalryFrame(DEAD, 0) );
                    if(canDeathScream) chanceDeathScream += 10;
                    break;
                }

                case SPTYPE_FOOT_ARTILLERY : {
                    switch(i) {
                        case 0 : {
                            fx_index = d_spriteManager->getGraphicIndex(SPRITE_FOOT_ARTILLERY, unitType, RandomFacing(), Index_0, s_AnimationControl.GetFootArtilleryFrame(DEAD, 0));
                            if(canDeathScream) chanceDeathScream += 10;
                            break;
                        }
                        case 1 : {
                            fx_index = d_spriteManager->getGraphicIndex(SPRITE_HORSEMAN, unitType, RandomFacing(), Index_1, s_AnimationControl.GetHorseManFrame(DEAD, 0));
                            break;
                        }
                        case 2 : {
                            fx_index = d_spriteManager->getGraphicIndex(SPRITE_CAVALRY, unitType, RandomFacing(), Index_2, s_AnimationControl.GetCavalryFrame(DEAD, 0));
                            break;
                        }
                        case 3 : {
                            fx_index = d_spriteManager->getGraphicIndex(SPRITE_FOOTMAN, unitType, RandomFacing(), Index_3, s_AnimationControl.GetFootManFrame(DEAD, 0));
                            break;
                        }
                        default : {
                            FORCEASSERT("Oops - Invalid index when drawing dead foot-artillery (may only be 0...3)");
                            return;
                        }
                    }
                break;
                }

                case SPTYPE_HORSE_ARTILLERY : {
                    switch(i) {
                        case 0 : {
                            fx_index = d_spriteManager->getGraphicIndex(SPRITE_HORSE_ARTILLERY, unitType, RandomFacing(), Index_0, s_AnimationControl.GetHorseArtilleryFrame(DEAD, 0));
                            if(canDeathScream) chanceDeathScream += 10;
                            break;
                        }
                        case 1 : {
                            fx_index = d_spriteManager->getGraphicIndex(SPRITE_HORSEMAN, unitType, RandomFacing(), Index_1, s_AnimationControl.GetHorseManFrame(DEAD, 0));
                            break;
                        }
                        case 2 : {
                            fx_index = d_spriteManager->getGraphicIndex(SPRITE_HORSEMAN, unitType, RandomFacing(), Index_2, s_AnimationControl.GetHorseManFrame(DEAD, 0));
                            break;
                        }
                        case 3 : {
                            fx_index = d_spriteManager->getGraphicIndex(SPRITE_CAVALRY, unitType, RandomFacing(), Index_3, s_AnimationControl.GetCavalryFrame(DEAD, 0));
                            break;
                        }
                        default : {
                            FORCEASSERT("Oops - Invalid index when drawing dead horse-artillery (may only be 0...3)");
                            return;
                        }
                    }
                break;
                }

                default : FORCEASSERT("Oops - invalid SP unit type when drawing dead unit");
            }

            /*
            Send dead unit graphic to FX list
            */
            EffectDisplayInfo fx(EFFECT_DEADUNIT, fx_index, BattleLocation(xpos, ypos));
            fx.Flags = EFFECT_DURATION_INFINITE;
            EffectsList * fxlist = const_cast<EffectsList *>(d_batData->effectsList() );
            fxlist->add(d_iter.hex(), fx);
        }

        /*
        Draw unit
        */

        else {

            PixelPoint pos(d_iter.position(BattleLocation(xpos, ypos)));

            switch(src_pos_info->graphic_number) {
                case 0 : { d_display->draw(spDraw1, pos); break; }
                case 1 : { d_display->draw(spDraw2, pos); break; }
                case 2 : { d_display->draw(spDraw3, pos); break; }
                case 3 : { d_display->draw(spDraw4, pos); break; }
                default : FORCEASSERT("Oops - invalid graphic index in unit pos table\n");
            }
        }

    }  // step through whole formation

   /*
   Trigger death sounds
   */
   if(canDeathScream && Options::get(OPT_DeadBodies)) {

      chanceDeathScream *= canDeathScream;

      switch(SPtype) {
         case SPTYPE_INFANTRY : {
            int r = random(0,INFANTRY_DEATHSCREAM_CHANCE);
            if(r < chanceDeathScream) 
            {
               d_batData->soundSystem()->triggerSound(GetDeathSound(), BATTLESOUNDPRIORITY_LOW, d_iter.hex() );
            }
            break;
         }
         case SPTYPE_CAVALRY : {
            int r = random(0,CAVALRY_DEATHSCREAM_CHANCE);
            if(r < chanceDeathScream) 
            {
               d_batData->soundSystem()->triggerSound(GetWhinnySound(), BATTLESOUNDPRIORITY_LOW, d_iter.hex() );
            }
            break;
         }
         case SPTYPE_FOOT_ARTILLERY : {
            int r = random(0,ARTILLERY_DEATHSCREAM_CHANCE);
            if(r < chanceDeathScream) 
            {
               d_batData->soundSystem()->triggerSound(GetSmashSound(), BATTLESOUNDPRIORITY_LOW, d_iter.hex() );
            }
            break;
         }
         case SPTYPE_HORSE_ARTILLERY : {
            int r = random(0,ARTILLERY_DEATHSCREAM_CHANCE);
            if(r < chanceDeathScream) 
            {
               d_batData->soundSystem()->triggerSound(GetSmashSound(), BATTLESOUNDPRIORITY_LOW, d_iter.hex() );
            }
            break;
         }
      }
   }

   // set new number of sprites
   anim_info.LastNumSprites = num;

   HexCord centerHex;
   if(cp->centerHexIfActive(&centerHex)) {
      if(d_iter.hex() == centerHex) DrawFlag(cp, BattleLocation(xoffset, yoffset), UpdateAnim );
   }

}















void
BattleDrawUnit::DrawFlag(const BattleCP* cp, BattleLocation offset, int UpdateAnim) {

    AnimationInfo & anim_info = cp->GetAnimInfo();

    unsigned int flag_index = scenario->getNationFlagSpriteIndex(cp->getNation() );
    unsigned int flag_frame = anim_info.FlagFrameCounter;

    EffectDisplayInfo fx(EFFECT_FLAGFLAPPING, flag_index+flag_frame, offset);
    fx.Flags = EFFECT_DURATION_TIMED;
    fx.Duration = 1;
    fx.Scale = 2.0;
    EffectsList * fxlist = const_cast<EffectsList *>(d_batData->effectsList() );

    fxlist->add(d_iter.hex(), fx);

   /*
   * Set flag flapping if UpdateAnim indicates time-update
   * and unit is awaiting or acting on an order
   * enemy units always have the flag set to prevent players knowing whether the enemy have orders
   * unless "OrderEnemy" option is set
   */

   bool updateFlag;

   if(GamePlayerControl::getControl(cp->getSide()) != GamePlayerControl::Player) updateFlag = true;
   else updateFlag = cp->isActingOnOrder() || (cp->getCurrentOrder().started() && (!cp->getCurrentOrder().finished()));

   if(UpdateAnim && updateFlag) {
      /*
      adjust frame counters
      if we want we can have the flag speed alter with the unit's morale, or whatever..
      */
      anim_info.FlagFrameTimer++;
      if(anim_info.FlagFrameTimer >= 2) {
         anim_info.FlagFrameTimer = 0;
         anim_info.FlagFrameCounter++;
         if(anim_info.FlagFrameCounter >= FLAG_TOTALFRAMES) anim_info.FlagFrameCounter = 0;
      }
   }
}




/*



CP


*/










void BattleDrawUnit::draw(const BattleCP* cp) {

   if (GamePlayerControl::getControl(cp->getSide()) != GamePlayerControl::Player)
   {
      if(cp->visibility() == Visibility::NeverSeen)
         return;

      if( (cp->visibility() == Visibility::NotSeen)
         && CampaignOptions::get(OPT_FogOfWar) )
      {
         return;
      }
   }

   // if we're at zero in any dimension, quit out
    if(d_iter.width() == 0 || d_iter.height() == 0) return;

   // if we're less than 3 width, just draw in side colour
    if(d_iter.width() <= 3) {

        Side unitside = cp->getSide();
        COLORREF sidecol = scenario->getSideColour(unitside);

        Rect<LONG> hexrect = d_iter.squareRect();

        RefPtrBattleRectDisplayObject ob(sidecol, PixelPoint(hexrect.width(), hexrect.height()));
        d_display->draw(ob, PixelPoint(hexrect.left(), hexrect.top()));

        return;
    }


    // get animation info
    AnimationInfo & anim_info = cp->GetAnimInfo();
    int rand_start_index = anim_info.RandomVal;

    /*
    Check wether to update frame (should check unit movement speed also)
    */
    unsigned int DeltaFrames = (BattleDisplay::FrameCounter - anim_info.LastTime);
    anim_info.LastTime = BattleDisplay::FrameCounter;
   int UpdateAnim = cp->speed() + 1;
    UpdateAnim = UpdateAnim * DeltaFrames;
   UpdateAnim *= d_batData->timeRatio();


   /*
   Get correct CP graphics [ NOTE : HARDWIRED GRAPHIC TYPES ]
   */
   int OfficerGraphicIndex1;
   int OfficerGraphicIndex2;
   int OfficerGraphicIndex3;
   int OfficerGraphicIndex4;
   int nSprites;

   switch(cp->getSide() ) {

      /*
      French
      */
      case 0 : {

         // Napoleon
         if(cp->leader()->isSupremeLeader()) {
            nSprites = 4;
            OfficerGraphicIndex1 = OfficerNapoleon;
            OfficerGraphicIndex2 = OfficerMarshalFrance;
            OfficerGraphicIndex3 = OfficerCavaFrance;
            OfficerGraphicIndex4 = OfficerInfantryFranceOne;
            break;
         }
         // Marshall (
         else if(cp->getRank().isHigher(Rank_Corps)) {
            nSprites = 3;
            OfficerGraphicIndex1 = OfficerMarshalFrance;
            OfficerGraphicIndex2 = OfficerCavaFrance;
            OfficerGraphicIndex3 = OfficerInfantryFranceTwo;
            OfficerGraphicIndex4 = 0;
            break;
         }
         // Cavalry
         else if(cp->generic()->isCavalry()) {
            nSprites = 2;
            OfficerGraphicIndex1 = OfficerCavaFrance;
            OfficerGraphicIndex2 = OfficerCavaFrance;
            OfficerGraphicIndex3= 0;
            OfficerGraphicIndex4 = 0;
            break;
         }
         // Infantry (type One or Two ?)
         else {
            nSprites = 2;
            OfficerGraphicIndex1 = OfficerInfantryFranceOne;
            OfficerGraphicIndex2 = OfficerInfantryFranceTwo;
            OfficerGraphicIndex3 = 0;
            OfficerGraphicIndex4 = 0;
            break;
         }
      }

      /*
      Allied
      */
      case 1 : {

         // Army Group
         if(cp->getRank().sameRank(Rank_ArmyGroup)) {
            nSprites = 4;
            OfficerGraphicIndex1 = OfficerCommanderAllied;
            OfficerGraphicIndex2 = OfficerCavaAllied;
            OfficerGraphicIndex3 = OfficerInfantryAlliedOne;
            OfficerGraphicIndex4 = OfficerInfantryAlliedTwo;
            break;
         }
         // Army
         if(cp->getRank().sameRank(Rank_Army)) {
            nSprites = 3;
            OfficerGraphicIndex1 = OfficerCommanderAllied;
            OfficerGraphicIndex2 = OfficerCavaAllied;
            OfficerGraphicIndex3 = OfficerInfantryAlliedTwo;
            OfficerGraphicIndex4 = 0;
            break;
         }
         //Cavalry
         else if(cp->generic()->isCavalry()) {
            nSprites = 2;
            OfficerGraphicIndex1 = OfficerCavaAllied;
            OfficerGraphicIndex2 = OfficerInfantryAlliedOne;
            OfficerGraphicIndex3 = 0;
            OfficerGraphicIndex4 = 0;
            break;
         }
         // Infantry (type One or Two ?)
         else {
            nSprites = 2;
            OfficerGraphicIndex1 = OfficerInfantryAlliedOne;
            OfficerGraphicIndex2 = OfficerInfantryAlliedTwo;
            OfficerGraphicIndex3 = 0;
            OfficerGraphicIndex4 = 0;
            break;
         }
      }

   }


    int xpos, ypos, f;

    int facing = cp->facing();
    SPFormation formation_type = SP_MarchFormation;

    /*
   Calculate display offset due to in-hex movement
   */
    int distance = cp->hexPosition().distance();
    if(cp->hexPosition().moveFrom() >= HexPosition::Center_East && cp->hexPosition().moveFrom() <= HexPosition::Center_Center) distance -= yards(110);
    BattleLocation origin = BattleDisplay::UnitFormationPoints.GetInHexPosition(cp->hexPosition().moveFrom());
    BattleLocation stepval = BattleDisplay::UnitFormationPoints.GetInHexUnitStepVal(cp->hexPosition().moveFrom(), cp->hexPosition().moveTo() );
    float scale = BattleDisplay::UnitFormationPoints.GetInHexUnitDistances(cp->hexPosition().moveFrom(), cp->hexPosition().moveTo() );
    int xoffset = origin.x() + (stepval.x() * (scale * distance) );
    int yoffset = origin.y() + (stepval.y() * (scale * distance) );

    // get facings info for each graphic
    UnitFacingInfo * facing_info = BattleDisplay::UnitFormationPoints.GetUnitFacing(formation_type, facing, SPTYPE_COMMAND_POSITION, NORMAL);

    // sprite block ptr
    const SWG_Sprite::SpriteBlock* sprBlock[MAX_GRAPHICSPERFORMATION];

    /*
   Command position walking
   */
    if(cp->isMoving() ) {

        if(UpdateAnim) {
            s_AnimationControl.UpdateOfficerFrame(WALKING, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], UpdateAnim );
            s_AnimationControl.UpdateOfficerFrame(WALKING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], UpdateAnim );
            s_AnimationControl.UpdateOfficerFrame(WALKING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], UpdateAnim );
            s_AnimationControl.UpdateOfficerFrame(WALKING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], UpdateAnim );
        }

        sprBlock[Index_0] = d_spriteManager->getGraphic(OfficerGraphicIndex1 + d_spriteManager->FacingToGraphicOffset(facing, OFFICER_TOTALFRAMES) + s_AnimationControl.GetOfficerFrame(WALKING, anim_info.FrameCounters[Index_0]));
        sprBlock[Index_1] = d_spriteManager->getGraphic(OfficerGraphicIndex2 + d_spriteManager->FacingToGraphicOffset(facing, OFFICER_TOTALFRAMES) + s_AnimationControl.GetOfficerFrame(WALKING, anim_info.FrameCounters[Index_1]));
        sprBlock[Index_2] = d_spriteManager->getGraphic(OfficerGraphicIndex3 + d_spriteManager->FacingToGraphicOffset(facing, OFFICER_TOTALFRAMES) + s_AnimationControl.GetOfficerFrame(WALKING, anim_info.FrameCounters[Index_2]));
        sprBlock[Index_3] = d_spriteManager->getGraphic(OfficerGraphicIndex4 + d_spriteManager->FacingToGraphicOffset(facing, OFFICER_TOTALFRAMES) + s_AnimationControl.GetOfficerFrame(WALKING, anim_info.FrameCounters[Index_3]));

   /*
   Command position standing
   */
    }
   else {

        if(UpdateAnim) {
            s_AnimationControl.UpdateOfficerFrame(STANDING, anim_info.FrameCounters[Index_0], anim_info.FrameTimers[Index_0], UpdateAnim );
            s_AnimationControl.UpdateOfficerFrame(STANDING, anim_info.FrameCounters[Index_1], anim_info.FrameTimers[Index_1], UpdateAnim );
            s_AnimationControl.UpdateOfficerFrame(STANDING, anim_info.FrameCounters[Index_2], anim_info.FrameTimers[Index_2], UpdateAnim );
            s_AnimationControl.UpdateOfficerFrame(STANDING, anim_info.FrameCounters[Index_3], anim_info.FrameTimers[Index_3], UpdateAnim );
        }

        sprBlock[Index_0] = d_spriteManager->getGraphic(OfficerGraphicIndex1 + d_spriteManager->FacingToGraphicOffset(facing, OFFICER_TOTALFRAMES) + s_AnimationControl.GetOfficerFrame(STANDING, anim_info.FrameCounters[Index_0]));
        sprBlock[Index_1] = d_spriteManager->getGraphic(OfficerGraphicIndex2 + d_spriteManager->FacingToGraphicOffset(facing, OFFICER_TOTALFRAMES) + s_AnimationControl.GetOfficerFrame(STANDING, anim_info.FrameCounters[Index_1]));
        sprBlock[Index_2] = d_spriteManager->getGraphic(OfficerGraphicIndex3 + d_spriteManager->FacingToGraphicOffset(facing, OFFICER_TOTALFRAMES) + s_AnimationControl.GetOfficerFrame(STANDING, anim_info.FrameCounters[Index_2]));
        sprBlock[Index_3] = d_spriteManager->getGraphic(OfficerGraphicIndex4 + d_spriteManager->FacingToGraphicOffset(facing, OFFICER_TOTALFRAMES) + s_AnimationControl.GetOfficerFrame(STANDING, anim_info.FrameCounters[Index_3]));

    }

    SWG_Sprite::SpriteBlockPtr sprite1(sprBlock[0]);
    SWG_Sprite::SpriteBlockPtr sprite2(sprBlock[1]);
    SWG_Sprite::SpriteBlockPtr sprite3(sprBlock[2]);
    SWG_Sprite::SpriteBlockPtr sprite4(sprBlock[3]);

    for(f=0; f<MAX_GRAPHICSPERFORMATION; f++) sprBlock[f]->release();

    HexIterator::Scale drawScale = d_iter.troopScale();
    RefPtrBattleSPDisplayObject spDraw1(sprite1, drawScale);
    RefPtrBattleSPDisplayObject spDraw2(sprite2, drawScale);
    RefPtrBattleSPDisplayObject spDraw3(sprite3, drawScale);
    RefPtrBattleSPDisplayObject spDraw4(sprite4, drawScale);

//    int index;


    for(int i=0; i<4; i++) {

        UnitPointInfo * pos_info = BattleDisplay::UnitFormationPoints.GetUnitPoint(formation_type, facing, SPTYPE_COMMAND_POSITION, i, NORMAL);

        // scale formation position from in-hex coords to pixel coords
        xpos = ( pos_info->pos.x() + xoffset + BattleDisplay::UnitFormationPoints.GetInfantryXPositionOffset(rand_start_index+i) );
        ypos = ( pos_info->pos.y() + yoffset + BattleDisplay::UnitFormationPoints.GetInfantryYPositionOffset(rand_start_index+i) );

        PixelPoint pos(d_iter.position(BattleLocation(xpos, ypos)));

        //  draw appropriate sprite, dependent upon graphic number for this formation entry
        switch(pos_info->graphic_number) {
            case 0 : { d_display->draw(spDraw1, pos); break; }
            case 1 : { d_display->draw(spDraw2, pos); break; }
            case 2 : { if(nSprites>2) d_display->draw(spDraw3, pos); break; }
            case 3 : { if(nSprites>3) d_display->draw(spDraw4, pos); break; }
            default : FORCEASSERT("Oops - invalid graphic index in unit pos table\n");
        }

    } // draw all troops

   DrawFlag(cp, BattleLocation(xoffset, yoffset), UpdateAnim );
}






/*
 * Draw unknown unit
 */

void BattleDrawUnit::draw(const BattleUnit* unit)
{
        FORCEASSERT("Drawing unknown unit");
}


};      // Private namespace
















void BattleDisplay::drawUnits(Displayer* display, const HexIterator& iter, const BattleData* batData, SWG_Sprite::SpriteLibrary* sprLib)
{

    TroopSpriteManager troopSprites(sprLib);

        const BattleHexMap* hexMap = batData->hexMap();

        BattleHexMap::const_iterator lower;
        BattleHexMap::const_iterator upper;
        if(hexMap->find(iter.hex(), lower, upper))
        {
            BattleDrawUnit drawer(display, iter, batData, &troopSprites);

            while(lower != upper)
            {
                const BattleUnit* unit = hexMap->unit(lower);
                if(unit)
                    unit->draw(&drawer);
                ++lower;
            }
        }

}





void
BattleDisplay::PrecalcFormations(const BattleData * d_batData) {

        /*
        firstly, lets initialize the BattleDisplay namespace animation variables to something sensible
        */

        BattleDisplay::FrameCounter = 0;
        BattleDisplay::LastTime = Greenius_System::MMTimer::getTime(); // NB: first frame is gonna jump !


        int facing;
        int nSprites = imagesPerSP[0];

        ASSERT(nSprites < MAX_TROOPS);

        BattleLocation midPoint(0,0);
        BattleMeasure::Distance hSpace;
        BattleMeasure::Distance vSpace;


        /*
        INFANTRY
        Precalculate all infantry formations in all 6 facings
        Each formation may have 4 graphic types associated with it
        */


        // SP_MarchFormation

        hSpace = spacings[0] + 800;
        vSpace = hSpace;

        UnitFormationPoints.SetNumUnits(SP_MarchFormation, SPTYPE_INFANTRY, nSprites);

        for(facing=0; facing<12; facing++) {

            PrecalcLines(
                SP_MarchFormation,
                IndexToFacing(facing),
                SPTYPE_INFANTRY,
                0,
                midPoint,
                hSpace,
                (vSpace*3)/4,
                IndexToFacing(facing) + HexPosition::FacingResolution/4,
                Index_Random,
                nSprites,
                3, false);

                UnitFormationPoints.SetUnitFacing(SP_MarchFormation, IndexToFacing(facing), SPTYPE_INFANTRY, IndexToFacing(facing));
        }


        // SP_ColumnFormation

        hSpace = spacings[0] + 800;
        vSpace = hSpace;

        UnitFormationPoints.SetNumUnits(SP_ColumnFormation, SPTYPE_INFANTRY, nSprites);

        for(facing=0; facing<12; facing++) {

            PrecalcLines(
                SP_ColumnFormation,
                IndexToFacing(facing),
                SPTYPE_INFANTRY,
                0,
                midPoint,
                hSpace,
                vSpace,
                IndexToFacing(facing),
                Index_Random,
                nSprites,
                6, false);

            UnitFormationPoints.SetUnitFacing(SP_ColumnFormation, IndexToFacing(facing), SPTYPE_INFANTRY, IndexToFacing(facing));
        }



        // SP_ClosedColumn

        hSpace = spacings[0] + 800;
        vSpace = hSpace;

        UnitFormationPoints.SetNumUnits(SP_ClosedColumnFormation, SPTYPE_INFANTRY, nSprites);

        for(facing=0; facing<12; facing++) {

            PrecalcLines(
                SP_ClosedColumnFormation,
                IndexToFacing(facing),
                SPTYPE_INFANTRY,
                0,
                midPoint,
                hSpace,
                (vSpace*3)/4,
                IndexToFacing(facing),
                Index_Random,
                nSprites,
                6, false);

            UnitFormationPoints.SetUnitFacing(SP_ClosedColumnFormation, HexPosition::North, SPTYPE_INFANTRY, HexPosition::North);
        }



        // SP_LineFormation

        hSpace = spacings[0] + 300;
        vSpace = hSpace;

        UnitFormationPoints.SetNumUnits(SP_LineFormation, SPTYPE_INFANTRY, nSprites);

        for(facing=0; facing<12; facing++) {

            PrecalcLines(
                SP_LineFormation,
                IndexToFacing(facing),
                SPTYPE_INFANTRY,
                0,
                midPoint,
                hSpace,
                vSpace,
                IndexToFacing(facing),
                Index_Random,
                nSprites,
                2, false);

            UnitFormationPoints.SetUnitFacing(SP_LineFormation, IndexToFacing(facing), SPTYPE_INFANTRY, IndexToFacing(facing));
        }


        // SP_SquareFormation

        UnitFormationPoints.SetNumUnits(SP_SquareFormation, SPTYPE_INFANTRY, nSprites);

        int quarter = nSprites/4;
        int last_quarter;
        if((quarter*4) < nSprites) last_quarter = (nSprites - (quarter*3));
        else last_quarter = quarter;

        hSpace = spacings[0];
        vSpace = hSpace;

        BattleMeasure::Distance rowSize = (quarter * hSpace) / 2;
        BattleMeasure::Distance dx;
        BattleMeasure::Distance dy;

        for(facing=0; facing<12; facing++) {

            dx = mcos(rowSize, HexPosition::wangle(IndexToFacing(facing)));
            dy = msin(rowSize, HexPosition::wangle(IndexToFacing(facing)));

            PrecalcLines(
                SP_SquareFormation, IndexToFacing(facing),
                SPTYPE_INFANTRY,
                (quarter*0),
                midPoint + BattleLocation(dx,dy),
                hSpace,
                vSpace,
                IndexToFacing(facing),
                Index_0,
                quarter,
                1, false);

            PrecalcLines(
                SP_SquareFormation,
                IndexToFacing(facing),
                SPTYPE_INFANTRY,
                (quarter*1),
                midPoint + BattleLocation(-dx,-dy),
                hSpace,
                vSpace,
                IndexToFacing(facing) + HexPosition::FacingResolution/2,
                Index_1,
                quarter,
                1, false);

            PrecalcLines(
                SP_SquareFormation,
                IndexToFacing(facing),
                SPTYPE_INFANTRY,
                (quarter*2),
                midPoint + BattleLocation(-dy,+dx),
                hSpace,
                vSpace,
                IndexToFacing(facing) + HexPosition::FacingResolution/4,
                Index_2,
                quarter,
                1, false);

            PrecalcLines(
                SP_SquareFormation,
                IndexToFacing(facing),
                SPTYPE_INFANTRY,
                (quarter*3),
                midPoint + BattleLocation(+dy,-dx),
                hSpace,
                vSpace,
                IndexToFacing(facing) - HexPosition::FacingResolution/4,
                Index_3,
                last_quarter,
                1, false);

            UnitFormationPoints.SetUnitFacing(
                SP_SquareFormation,
                IndexToFacing(facing),
                SPTYPE_INFANTRY,
                IndexToFacing(facing),
                IndexToFacing(facing+6),
                IndexToFacing(facing+3),
                IndexToFacing(facing+9)
                );

        }



        /*
        CAVALRY
        Precalculate all cavalry formations in all 6 facings
        Each formation may have 4 graphic types associated with it
        */
        nSprites = imagesPerSP[1];

        ASSERT(nSprites < MAX_TROOPS);

        midPoint = BattleLocation(0, -(QuantaPerYHex/8));
        hSpace = spacings[1];
        vSpace = hSpace;


        // SP_MarchFormation
        UnitFormationPoints.SetNumUnits(SP_MarchFormation, SPTYPE_CAVALRY, nSprites);

        for(facing=0; facing<12; facing++) {
// March & Colum were reversed

            PrecalcLines(
                SP_MarchFormation,
                IndexToFacing(facing),
                SPTYPE_CAVALRY,
                0,
                midPoint,
                (hSpace*4)/3,
                vSpace,
                IndexToFacing(facing) + HexPosition::FacingResolution/4,
                Index_0,
                nSprites,
                2, false);

            UnitFormationPoints.SetUnitFacing(SP_MarchFormation, IndexToFacing(facing), SPTYPE_CAVALRY, IndexToFacing(facing));
        }


        // SP_ColumnFormation
        midPoint = BattleLocation(0,0);

        UnitFormationPoints.SetNumUnits(SP_ColumnFormation, SPTYPE_CAVALRY, nSprites);

        for(facing=0; facing<12; facing++) {
// March & Colum were reversed

            PrecalcLines(
                SP_ColumnFormation,
                IndexToFacing(facing),
                SPTYPE_CAVALRY,
                0,
                midPoint,
                (hSpace*4)/3,
                (vSpace*3)/4,
                IndexToFacing(facing) + HexPosition::FacingResolution/4,
                Index_0,
                nSprites,
                3, false);

            UnitFormationPoints.SetUnitFacing(SP_ColumnFormation, IndexToFacing(facing), SPTYPE_CAVALRY, IndexToFacing(facing));
        }


        // SP_LineFormation
        midPoint = BattleLocation(0,-(QuantaPerYHex/8));

        UnitFormationPoints.SetNumUnits(SP_LineFormation, SPTYPE_CAVALRY, nSprites);

        for(facing=0; facing<12; facing++) {

            PrecalcLines(
                SP_LineFormation,
                IndexToFacing(facing),
                SPTYPE_CAVALRY,
                0,
                midPoint,
                (hSpace*3)/5,
                vSpace,
                IndexToFacing(facing),
                Index_0,
                nSprites,
                1, false);

            UnitFormationPoints.SetUnitFacing(SP_LineFormation, IndexToFacing(facing), SPTYPE_CAVALRY, IndexToFacing(facing));
        }






        /*
        FOOT ARTILLERY
        Precalculate all foot artillery formations in all 6 facings
        Each formation may have 4 graphic types associated with it
        */

        nSprites = 4;

        midPoint = BattleLocation(0,0);
        hSpace = spacings[2];
        vSpace = hSpace;

        BattleLocation pos0, pos1, pos2, pos3;

        pos0 = BattleLocation(-10000,0); // cannon
        pos1 = BattleLocation(8000,4000); // riderless
        pos2 = BattleLocation(8000,-4000); // riderless
        pos3 = BattleLocation(0,15000); // footman

        // SP_LimberedFormation (SP_MarchFormation)
        UnitFormationPoints.SetNumUnits(SP_LimberedFormation, SPTYPE_FOOT_ARTILLERY, nSprites);

        for(facing=0; facing<12; facing++) {

            UnitFormationPoints.SetUnitFacing(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                IndexToFacing(facing+6), // facings of all 4 graphics
                IndexToFacing(facing),
                IndexToFacing(facing),
                IndexToFacing(facing));

            // FootArtillery Graphic

            PrecalcPoints(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                0, // index in array
                pos0,
                Index_0); // graphic no

            // RiderlessHorse Graphic 1

            PrecalcPoints(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                1, // index in array
                pos1,
                Index_1); // graphic no

            // RiderlessHorse Graphic 2

            PrecalcPoints(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                2, // index in array
                pos2,
                Index_2); // graphic no

            // FootArtiMan Graphic

            PrecalcPoints(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                3, // index in array
                pos3,
                Index_3); // graphic no

        }



        pos0 = BattleLocation(10000,0); // cannon
        pos1 = BattleLocation(0,10000); // riderless
        pos2 = BattleLocation(0,-10000); // riderless
        pos3 = BattleLocation(-10000, 0); // footman

        // SP_UnlimberedFormation (SP_ColumnFormation)
        UnitFormationPoints.SetNumUnits(SP_UnlimberedFormation, SPTYPE_FOOT_ARTILLERY, nSprites);

        for(facing=0; facing<12; facing++) {

            UnitFormationPoints.SetUnitFacing(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                IndexToFacing(facing+6), // facings of all 4 graphics
                IndexToFacing(facing),
                IndexToFacing(facing),
                IndexToFacing(facing));

            // FootArtillery Graphic

            PrecalcPoints(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                0, // index in array
                pos0,
                Index_0); // graphic no

            // RiderlessHorse Graphic 1

            PrecalcPoints(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                1, // index in array
                pos1,
                Index_1); // graphic no

            // RiderlessHorse Graphic 2

            PrecalcPoints(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                2, // index in array
                pos2,
                Index_2); // graphic no

            // FootArtiMan Graphic

            PrecalcPoints(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_FOOT_ARTILLERY,
                3, // index in array
                pos3,
                Index_3); // graphic no

        }









        /*
        HORSE ARTILLERY
        Precalculate all horse artillery formations in all 6 facings
        Each formation may have 4 graphic types associated with it
        */

        nSprites = 4;

        midPoint = BattleLocation(0,0);
        hSpace = spacings[2];
        vSpace = hSpace;


        pos0 = BattleLocation(-10000,0); // cannon
        pos1 = BattleLocation(8000, 4000); // mounted
      pos2 = BattleLocation(0, 15000); // mounted
      pos3 = BattleLocation(8000, -4000); // riderless

        // SP_LimberedFormation (SP_MarchFormation)
        UnitFormationPoints.SetNumUnits(SP_LimberedFormation, SPTYPE_HORSE_ARTILLERY, nSprites);

        for(facing=0; facing<12; facing++) {

            UnitFormationPoints.SetUnitFacing(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                IndexToFacing(facing+6), // facings of all 4 graphics
                IndexToFacing(facing),
                IndexToFacing(facing),
                IndexToFacing(facing));

            // HorseArtillery Graphic

            PrecalcPoints(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                0, // index in array
                pos0,
                Index_0); // graphic no

            // RiderlessHorse Graphic 1

            PrecalcPoints(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                1, // index in array
                pos1,
                Index_1); // graphic no

            // RiderlessHorse Graphic 2

            PrecalcPoints(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                2, // index in array
                pos2,
                Index_2); // graphic no

            // RiderlessHorse Graphic 3

            PrecalcPoints(
                SP_LimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                3, // index in array
                pos3,
                Index_3); // graphic no

        }



        pos0 = BattleLocation(10000,0); // cannon
        pos1 = BattleLocation(0,10000); // mounted
        pos2 = BattleLocation(-10000,0); // mounted
        pos3 = BattleLocation(0,-10000); // riderless

        // SP_UnlimberedFormation (SP_ColumnFormation)
        UnitFormationPoints.SetNumUnits(SP_UnlimberedFormation, SPTYPE_HORSE_ARTILLERY, nSprites);

        for(facing=0; facing<12; facing++) {

            UnitFormationPoints.SetUnitFacing(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                IndexToFacing(facing+6), // facings of all 4 graphics
                IndexToFacing(facing),
                IndexToFacing(facing),
                IndexToFacing(facing));

            // HorseArtillery Graphic

            PrecalcPoints(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                0, // index in array
                pos0,
                Index_0); // graphic no

            // RiderlessHorse Graphic 1

            PrecalcPoints(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                1, // index in array
                pos1,
                Index_1); // graphic no

            // RiderlessHorse Graphic 2

            PrecalcPoints(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                2, // index in array
                pos2,
                Index_2); // graphic no

            // RiderlessHorse Graphic 3

            PrecalcPoints(
                SP_UnlimberedFormation,
                IndexToFacing(facing),
                SPTYPE_HORSE_ARTILLERY,
                3, // index in array
                pos3,
                Index_3); // graphic no

        }


        /*
        COMMAND POSITION
        Precalculate all foot artillery formations in all 6 facings
        Each formation may have 4 graphic types associated with it
        */

        pos0 = BattleLocation(0,-5000); // office #1
        pos1 = BattleLocation(5000,0); // officer #2
        pos2 = BattleLocation(-6000,1000); // officer #3
        pos3 = BattleLocation(-1000,6000); // officer #4


        UnitFormationPoints.SetNumUnits(SP_MarchFormation, SPTYPE_COMMAND_POSITION, 4);

        for(facing=0; facing<12; facing++) {

            UnitFormationPoints.SetUnitFacing(
                SP_MarchFormation,
                IndexToFacing(facing),
                SPTYPE_COMMAND_POSITION,
                IndexToFacing(facing+6), // facings of all 4 graphics
                IndexToFacing(facing),
                IndexToFacing(facing),
                IndexToFacing(facing));

            // FlagGraphic

            PrecalcPoints(
                SP_MarchFormation,
                IndexToFacing(facing),
                SPTYPE_COMMAND_POSITION,
                0, // index in array
                pos0,
                Index_0); // graphic no

            PrecalcPoints(
                SP_MarchFormation,
                IndexToFacing(facing),
                SPTYPE_COMMAND_POSITION,
                1, // index in array
                pos1,
                Index_1); // graphic no

            PrecalcPoints(
                SP_MarchFormation,
                IndexToFacing(facing),
                SPTYPE_COMMAND_POSITION,
                2, // index in array
                pos2,
                Index_2); // graphic no

            PrecalcPoints(
                SP_MarchFormation,
                IndexToFacing(facing),
                SPTYPE_COMMAND_POSITION,
                3, // index in array
                pos3,
                Index_3); // graphic no
        }



        int n;
        /*
        Setup table of random offsets for infantry units' positions
        */
        for(n=0; n<200; n++) {
            BattleLocation pos;
            pos.x( random(0,500) - 250 );
            pos.y( random(0,500) - 250 );
            UnitFormationPoints.SetInfantryRandomOffset(n, pos);
        }

        /*
        Setup table of random offsets for cavalry units' positions
        */
        for(n=0; n<200; n++) {
            BattleLocation pos;
            pos.x( random(0,1000) - 500 );
            pos.y( random(0,1000) - 500 );
            UnitFormationPoints.SetCavalryRandomOffset(n, pos);
        }

        /*
        Setup table of random offsets for atrillery units' positions
        */
        for(n=0; n<200; n++) {
            BattleLocation pos;
            pos.x( random(0,2000) - 1000 );
            pos.y( random(0,2000) - 1000 );
            UnitFormationPoints.SetArtilleryRandomOffset(n, pos);
        }


        /*
        Setup arrays of random formation indexes for jumbled movement
        */

        PrecalcFormationIndexes();

        /*
        Setup arrays of movement offsets between all in hex posints
        */

        PrecalcInHexPositions();


}






void
BattleDisplay::PrecalcPoints(
                        SPFormation formation_type, // formation type
                        HexPosition::Facing formation_facing, // formation facing
                        SPTypesEnum SPType, // basic type of unit
                        int index_start, // start index to put data in [0...MAXUNITS]
                        BattleLocation& point, // point to rotate about origin
                        unsigned int sprite_number) { // graphic index for units in this line (helps when drawing sprites) [0...MAX_GRAPHICSPERFORMATION]


        Wangle wangle = HexPosition::wangle(formation_facing);

        BattleLocation newpoint;

        newpoint.x( mcos(point.x(), wangle) + -msin(point.y(), wangle) );
        newpoint.y( - (msin(point.x(), wangle) + mcos(point.y(), wangle) ) );

        UnitFormationPoints.SetUnitPoint(formation_type, formation_facing, SPType, index_start, newpoint, sprite_number);

}



/*
Precalculates a single line (or stacked line) of troops
Basically the same as the old DrawLines routine
*/

void
BattleDisplay::PrecalcLines(
                        SPFormation formation_type, // formation type
                        HexPosition::Facing formation_facing, // formation facing
                        SPTypesEnum SPType, // basic type of unit
                        int index_start, // start index to put data in [0...MAXUNITS]
                        const BattleLocation& centre, // centre of line
                        BattleMeasure::Distance hSpace, // horizontal spacing
                        BattleMeasure::Distance vSpace,  // vertical spacing
                        HexPosition::Facing line_facing, // facing normal to line
                        GraphicIndexEnum sprite_number, // graphic index for units in this line (helps when drawing sprites) [0...MAX_GRAPHICSPERFORMATION]
                        int nSprites, // total number of sprites
                        int nRows,
                        bool Offset) { // number of rows to create

        ASSERT(nRows > 0);

        Wangle wangle = HexPosition::wangle(line_facing);

        BattleMeasure::Distance vSize = (nRows - 1) * vSpace;

        BattleLocation startV( centre.x() + mcos(vSize/2, wangle), centre.y() + msin(vSize/2, wangle));
        BattleLocation endV( centre.x() - mcos(vSize/2, wangle), centre.y() - msin(vSize/2, wangle));


        // Set up a Bresenham iterator to step along rows
        BresenhamIterator<BattleMeasure::Distance> vxIter(startV.x(), endV.x(), nRows);
        BresenhamIterator<BattleMeasure::Distance> vyIter(startV.y(), endV.y(), nRows);

        // count which aray index is being done
        int unit_index=index_start;

        int initial_colCount = (nSprites + nRows - 1) / nRows;

        // go through all rows in formation
        for(; nRows && nSprites>1; --nRows) {

                int colCount = initial_colCount;

                // Calculate end points of row
                BattleMeasure::Distance rowLength = colCount * hSpace - hSpace/2;
                BattleMeasure::Distance x1 = -rowLength / 2;

                // Offset each other row
                if(Offset) if(nRows & 1) { x1 += hSpace / 2; }

                BattleMeasure::Distance x2 = x1 + rowLength;

                BattleLocation midRow(vxIter++, vyIter++);

                // Rotate

                BattleLocation end1( midRow.x() + msin(x1, wangle), midRow.y() - mcos(x1, wangle));
                BattleLocation end2( midRow.x() + msin(x2, wangle), midRow.y() - mcos(x2, wangle));

                BattleLocation p1 = end1;
                BattleLocation p2 = end2;

                // Set up a Bresenham iterator to step along each row
                BresenhamIterator<PixelDimension> xIter(p1.x(), p2.x(), colCount);
                BresenhamIterator<PixelDimension> yIter(p1.y(), p2.y(), colCount);




                while(colCount--) {

                        // Fill out the position data
                        if(sprite_number == Index_Random) UnitFormationPoints.SetUnitPoint(formation_type, formation_facing, SPType, unit_index, BattleLocation(*xIter, -(*yIter) ), random(0,4) );
                        else UnitFormationPoints.SetUnitPoint(formation_type, formation_facing, SPType, unit_index, BattleLocation(*xIter, -(*yIter) ), sprite_number );

                        unit_index++;
                        xIter++;
                        yIter++;

                }
                nSprites-=initial_colCount;

        }

}










int
FacingToIndex(HexPosition::Facing facing) {

    switch(facing) {

        case HexPosition::East : return 0;
        case HexPosition::NorthEastPoint : return 1;
        case HexPosition::NorthEastFace : return 2;
        case HexPosition::North : return 3;
        case HexPosition::NorthWestFace : return 4;
        case HexPosition::NorthWestPoint : return 5;
        case HexPosition::West : return 6;
        case HexPosition::SouthWestPoint : return 7;
        case HexPosition::SouthWestFace : return 8;
        case HexPosition::South : return 9;
        case HexPosition::SouthEastFace : return 10;
        case HexPosition::SouthEastPoint : return 11;

        default : { FORCEASSERT("Oops - Invalid facing for converting to index."); return 0; }
    }
}


HexPosition::Facing
IndexToFacing(int index) {

while(index >= 12) index-=12;

    switch(index) {

        case 0 : return HexPosition::East;
        case 1 : return HexPosition::NorthEastPoint;
        case 2 : return HexPosition::NorthEastFace;
        case 3 : return HexPosition::North;
        case 4 : return HexPosition::NorthWestFace;
        case 5 : return HexPosition::NorthWestPoint;
        case 6 : return HexPosition::West;
        case 7 : return HexPosition::SouthWestPoint;
        case 8 : return HexPosition::SouthWestFace;
        case 9 : return HexPosition::South;
        case 10 : return HexPosition::SouthEastFace;
        case 11 : return HexPosition::SouthEastPoint;

        default : { FORCEASSERT("Oops - Invalid index for converting to facing."); return HexPosition::East; }
    }
}




HexPosition::Facing
RandomFacing(void) {

    return IndexToFacing(random(0,12));

}





void
BattleDisplay::PrecalcFormationIndexes(void) {

    int array;
    int index;
    int n;
    int i1,i2;
    int val1;
    int val2;

    int num_swaps = 40;


    // INFANTRY

    int nSprites = imagesPerSP[0];
    // set all arrays to straight 0...nSprites values
    for(array=0; array<20; array++) { for(index=0; index<nSprites; index++) UnitFormationPoints.SetFormationIndex(array, SPTYPE_INFANTRY, index, index); }
    // jumble up formations by swapping random values
    for(array=0; array<20; array++) {
        // repeat 100 times to randomize (this could increment with array val to make increasing disorderment)
        for(n=0; n<num_swaps; n++) {
            i1 = random(0,nSprites);
            i2 = random(0,nSprites);
            // take two random values
            val1 = UnitFormationPoints.GetFormationIndex(array, SPTYPE_INFANTRY, i1);
            val2 = UnitFormationPoints.GetFormationIndex(array, SPTYPE_INFANTRY, i2);
            // swap values
            UnitFormationPoints.SetFormationIndex(array, SPTYPE_INFANTRY, i1, val2);
            UnitFormationPoints.SetFormationIndex(array, SPTYPE_INFANTRY, i2, val1);
        }
    }


    // CAVALRY

    nSprites = imagesPerSP[1];
    // set all arrays to straight 0...nSprites values
    for(array=0; array<20; array++) { for(index=0; index<nSprites; index++) UnitFormationPoints.SetFormationIndex(array, SPTYPE_CAVALRY, index, index); }
    // jumble up formations by swapping random values
    for(array=0; array<20; array++) {
        // repeat 100 times to randomize (this could increment with array val to make increasing disorderment)
        for(n=0; n<num_swaps; n++) {
            i1 = random(0,nSprites);
            i2 = random(0,nSprites);
            // take two random values
            val1 = UnitFormationPoints.GetFormationIndex(array, SPTYPE_CAVALRY, i1);
            val2 = UnitFormationPoints.GetFormationIndex(array, SPTYPE_CAVALRY, i2);
            // swap values
            UnitFormationPoints.SetFormationIndex(array, SPTYPE_CAVALRY, i1, val2);
            UnitFormationPoints.SetFormationIndex(array, SPTYPE_CAVALRY, i2, val1);
        }
    }


    // FOOT ARTILLERY

    nSprites = imagesPerSP[2];
    // set all arrays to straight 0...nSprites values
    for(array=0; array<20; array++) {
        for(index=0; index<nSprites; index++) UnitFormationPoints.SetFormationIndex(array, SPTYPE_FOOT_ARTILLERY, index, index);
    }


    // HORSE ARTILLERY

    nSprites = imagesPerSP[2];
    // set all arrays to straight 0...nSprites values
    for(array=0; array<20; array++) {
        for(index=0; index<nSprites; index++) UnitFormationPoints.SetFormationIndex(array, SPTYPE_HORSE_ARTILLERY, index, index);
    }


}









void
BattleDisplay::PrecalcInHexPositions(void) {

    /*
    Calculate in-hex positions
    */

    int f;
    HexPosition::MoveTo pos_enum1;
    HexPosition::MoveTo pos_enum2;
    BattleLocation inhexpos;
    float xpos, ypos;

    static const float y_scalar = static_cast<float>(1.134);

    pos_enum1 = HexPosition::East_Face;
    pos_enum2 = HexPosition::Center_East;



    // do rotations at edge & center
    for(f=0; f<12; f+=2) {

        xpos = mcos(HalfXHex, HexPosition::wangle(IndexToFacing(f)));
        ypos = -msin( (float)HalfYHex * y_scalar, HexPosition::wangle(IndexToFacing(f)));
        inhexpos = BattleLocation(xpos, ypos);
        UnitFormationPoints.SetInHexPosition(pos_enum1, inhexpos);

        xpos = mcos(HalfXHex/2, HexPosition::wangle(IndexToFacing(f)));
        ypos = -msin(( (float)HalfYHex/2) * y_scalar, HexPosition::wangle(IndexToFacing(f)));
        inhexpos = BattleLocation(xpos, ypos);
        UnitFormationPoints.SetInHexPosition(pos_enum2, inhexpos);

        INCREMENT(pos_enum1);
        INCREMENT(pos_enum2);
    }

    // do the dead center
    inhexpos = BattleLocation(0,0);
    UnitFormationPoints.SetInHexPosition(HexPosition::Center_Center, inhexpos);



    /*
    Calculate in-hex angles (or step vals)
    */

    BattleLocation src_pos,dest_pos;
    int dx, dy;

    for(pos_enum1 = HexPosition::East_Face; pos_enum1 <= HexPosition::Center_Center;INCREMENT(pos_enum1)) {

        for(pos_enum2 = HexPosition::East_Face; pos_enum2 <= HexPosition::Center_Center; INCREMENT(pos_enum2)) {

            src_pos = UnitFormationPoints.GetInHexPosition(pos_enum1);
            dest_pos = UnitFormationPoints.GetInHexPosition(pos_enum2);

            dx = dest_pos.x() - src_pos.x();
            dy = dest_pos.y() - src_pos.y();

            UnitFormationPoints.SetInHexUnitStepVal(pos_enum1, pos_enum2, BattleLocation(dx,dy) );

        }

    }


    /*
    Calculate normalized scalars for each MoveTo combination
    */
    HexPosition::MoveTo from;
    HexPosition::MoveTo to;

    int start_distance;
    int end_distance;

    for(from = HexPosition::East_Face; from <= HexPosition::Center_Center; INCREMENT(from)) {

        for(to = HexPosition::East_Face; to <= HexPosition::Center_Center; INCREMENT(to)) {

            if(from >= HexPosition::East_Face && from <= HexPosition::SE_Face) start_distance = 0;
            if(from >= HexPosition::Center_East && from <= HexPosition::Center_Center) start_distance = yards(110);

            if(to >= HexPosition::East_Face && to <= HexPosition::SE_Face) end_distance = yards(220);
            if(to >= HexPosition::Center_East && to <= HexPosition::Center_Center) end_distance = yards(110);

            float recip = 1.0 / ( (float) end_distance - (float) start_distance);

             UnitFormationPoints.SetInHexUnitDistances(from, to, recip);

        }

    }




}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
