/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
       * This Software is subject to the GNU General Public License.          *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef B_VIS_HPP
#define B_VIS_HPP

/*

  Filename : b_vis.hpp

  Description : Unit->Hex visibility checks, for realtime FOW
  
*/

#include "gthread.hpp"
#include "thread.hpp"
//#include "cmbtutil.hpp"
#include "hexdata.hpp"
#include "batdata.hpp"
#include "batarmy.hpp"
#include "batcord.hpp"
#include "bobiter.hpp"
#include "batunit.hpp"
#include "batdisp.hpp"
#include "b_los.hpp"



class BattleHexVisibility : public Greenius_System::Thread {

public:

   BattleHexVisibility(BattleData * batdata, BattleMapDisplay * display, Side side);
   ~BattleHexVisibility(void);

   void Process(int n_hexes);

private:

   BattleData * d_batData;
   BattleMapDisplay * d_display;
   Side d_side;
   B_LineOfSight d_los;

   virtual void run();

   void Initialize(void);
// Visibility::Value FastLOS(BattleMap * map, const HexCord & from_hex, const HexCord & to_hex);

   int d_minX;
   int d_minY;
   int d_maxX;
   int d_maxY;
   // last hex processed
   BattleMeasure::HexCord d_position;

   // lookup of 1/n values.  multiply by [n] instead of doing 39cycle divide
   // dependent on cache consistency for any advantage !
   // therefore do big runs of execution with low thread usage, rather than many small calls with high thread activity
// float d_distanceRecips[MAX_DIMENSION_OF_BATTLEFIELD];

   // this could be cleared better !
// int losVals[GT_HowMany];
};


#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
