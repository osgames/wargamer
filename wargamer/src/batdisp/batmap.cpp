/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Main Viewing Window
 * It is called a MapWindow as it is analagous to the CampaignMapWindow
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "batmap.hpp"
#include "batdisp.hpp"
// #include "generic.hpp"
#include "palwind.hpp"
#include "palette.hpp"
#include "cscroll.hpp"
#include "app.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "wmisc.hpp"
#include "registry.hpp"
#include "scenario.hpp"
#include "scn_img.hpp"

#include "control.hpp"
#include "b_vis.hpp"

#ifdef DEBUG
#include "clog.hpp"
LogFileFlush batmapLog("batmap.log");
// LogFile batmapLog("batdisp.log");
#endif

class BattleMapWindImp :
    public PaletteWindow,
    public WindowBaseND
{
    public:
        BattleMapWindImp(const PBattleWindows& batWind, RCPBattleData battleData);
        ~BattleMapWindImp();

        BattleMapDisplay * getDisplay(void) { return &d_display; }

        void create();

        // void ShowWindows(void);
        // void HideWindows(void);
        void show(bool visible);
        void enable(bool visible);

        BattleMapInfo::Mode mode() const { return d_display.mode(); }
        void mode(BattleMapInfo::Mode mode);

        void reset();

        void startZoom();
                // Initiate Zoom Mode

        bool isZooming() const { return d_isZooming; }
        bool canZoom() const { return d_display.mode() != 0; }
                // return true if zoom button should be enabled

        bool toggleHexOutline();
        bool hexOutline() const { return d_display.hexOutline(); }
    #ifdef DEBUG
        bool toggleHexDebug();
        bool hexDebug() const { return d_display.hexDebug(); }
    #endif

        bool setLocation(const BattleLocation& loc);

      void centerLocation(void);
                                                        // Set desired centre location
        BattleArea area() const { return d_display.area(); }

        void redraw(bool all);

      void updateScrollBars(void);

      bool isVisibilityRunning(void) { return d_visibilityRunning; }

      // start the visibility thread running
      void startVisibilityChecks(void) {

         if(d_visibilityRunning) return;

         Side player_side = GamePlayerControl::getSide(GamePlayerControl::Player);
         if(player_side != SIDE_Neutral) {
            const BattleData * bd = d_battleData;
            d_hexVisibility = new BattleHexVisibility(const_cast<BattleData *>(bd), &d_display, player_side);
            d_hexVisibility->start();
            d_visibilityRunning = true;
         }
         else d_hexVisibility = NULL;
      }

      // stop the visibility thread running
      void stopVisibilityChecks(void) {

         if(!d_visibilityRunning) return;

         if(d_hexVisibility) {
            d_hexVisibility->stop();
            delete d_hexVisibility;
            d_hexVisibility = NULL;
            d_visibilityRunning = false;
         }
      }

    private:
        LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
        BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
        void onDestroy(HWND hWnd);
        void onPaint(HWND hWnd);
        void onSize(HWND hwnd, UINT state, int cx, int cy);
        void onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
        void onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
        void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
        void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
        void onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
        void onRButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
        void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
        BOOL onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg);

        void setXScroll();
        void setYScroll();
        void setScrollRange();


        bool updateLocation(const BattleLocation& loc);

        static const int BattleMapWindImp::SB_Width;
        static const int BattleMapWindImp::SB_Height;
        static const char s_registryName[];
        // static const char s_regMode[];
        // static const char s_regLocation[];
        static const char s_regHexoutline[];
        static const char s_regHexdebug[];

        HWND d_hwndVScroll;                                             // Scroll Bar Handle
        HWND d_hwndHScroll;

        BattleMapDisplay d_display;
        BattleLocation          d_wantCentre;           // Desired centre location

        CPBattleData             d_battleData;
        PBattleWindows           d_batWind;             // Way of communicating with other windows

        bool d_isZooming;
        bool d_needsRedraw;

      BattleHexVisibility * d_hexVisibility;
      bool d_visibilityRunning;

      bool d_displayFPS;
      unsigned int d_frames;
      unsigned int d_lastTime;
      char d_framesString[64];
                                                                                                                                // Set if click should zoom
};

/*
 * Scroll Bar size
 */

const int BattleMapWindImp::SB_Width = 15;
const int BattleMapWindImp::SB_Height = 15;

// Key in system registry

const char BattleMapWindImp::s_registryName[] = "BattleMap";
// static const char BattleMapWindImp::s_regMode[] = "Mode";
// static const char BattleMapWindImp::s_regLocation[] = "Location";
const char BattleMapWindImp::s_regHexoutline[] = "Hexes";
const char BattleMapWindImp::s_regHexdebug[] = "Debug";

/*
 * Border Colors until we sort out scenario data for battles
 */

static CustomBorderInfo borderColors = {
        PALETTERGB(189, 165, 123),
        PALETTERGB(255, 247, 215),
        PALETTERGB(230, 214, 123),
        PALETTERGB(115, 90, 66)
};


BattleMapWindImp::BattleMapWindImp(const PBattleWindows& batWind, RCPBattleData battleData) :
        PaletteWindow(),
        WindowBaseND(),
        d_hwndVScroll(NULL),
        d_hwndHScroll(NULL),
        d_battleData(battleData),
        d_batWind(batWind),
        d_display(battleData),
        d_isZooming(false),
        d_needsRedraw(true),
      d_hexVisibility(NULL),
      d_visibilityRunning(false),
      d_displayFPS(false),
      d_frames(0),
      d_lastTime(timeGetTime())
{

   d_framesString[0] = 0;

    initCustomScrollBar(APP::instance());


}


BattleMapWindImp::~BattleMapWindImp() {

   stopVisibilityChecks();

   selfDestruct();
}


void BattleMapWindImp::create()
{
                  ASSERT(getHWND() == NULL);

        ASSERT(d_batWind() != 0);
        HWND hParent = d_batWind->hwnd();
        ASSERT(hParent != NULL);

        RECT r;
        GetClientRect(hParent, &r);
        // getInitialPosition(r);

        createWindow(0,
                // className(),
                NULL,
                WS_CHILD |
                WS_CLIPSIBLINGS,
                r.left, r.top, r.right, r.bottom,
                hParent,                /* parent window */
                NULL                           /* menu handle (or child ID) */
                // APP::instance()                         /* program handle */
        );

//                  ShowWindow(getHWND(), SW_SHOW);
}

void BattleMapWindImp::show(bool visible)
{
    WindowBaseND::show(visible);
    if(isVisible())
    {
        d_needsRedraw = true;
        InvalidateRect(getHWND(), NULL, FALSE);
    }
}

void BattleMapWindImp::enable(bool visible)
{
    WindowBaseND::enable(visible);
    if(isVisible())
    {
        d_needsRedraw = true;
        InvalidateRect(getHWND(), NULL, FALSE);
    }
}

#if 0
void BattleMapWindImp::ShowWindows(void) {
    
    ShowWindow(getHWND(), SW_SHOW);
    ShowWindow(d_hwndVScroll, SW_SHOW);
    ShowWindow(d_hwndHScroll, SW_SHOW);

    d_needsRedraw = true;
    InvalidateRect(getHWND(), NULL, FALSE);
}

void BattleMapWindImp::HideWindows(void) {

    ShowWindow(getHWND(), SW_HIDE);
    ShowWindow(d_hwndVScroll, SW_HIDE);
    ShowWindow(d_hwndHScroll, SW_HIDE);
}
#endif

void BattleMapWindImp::mode(BattleMapInfo::Mode mode) 
{
        d_display.mode(mode); 
        d_display.setPosition(d_wantCentre);
        d_batWind->mapAreaChanged(d_display.area());
      setScrollRange();
        setXScroll();
        setYScroll();
        redraw(true);
}


void BattleMapWindImp::updateScrollBars(void) {

   setScrollRange();
    setXScroll();
    setYScroll();

}

void BattleMapWindImp::startZoom()
{
                  if(canZoom())
                d_isZooming = !d_isZooming;
        else
                d_isZooming = false;
}

bool BattleMapWindImp::toggleHexOutline()
{
        bool flag = d_display.toggleHexOutline(); 
        redraw(true);
        return flag;
}

#ifdef DEBUG
bool BattleMapWindImp::toggleHexDebug()
{
                  bool flag = d_display.toggleHexDebug();
        redraw(true);
        return flag;
}
#endif

void BattleMapWindImp::reset()
{
    d_display.reset();
}

/*
 * Set desired centre location
 * This is used so that when toggling between overview and detail,
 * you end up zoomed to the same location
 *
 * Return true if display needed updating
 */

bool BattleMapWindImp::setLocation(const BattleLocation& loc)
{
        d_wantCentre = loc;
        bool flag = d_display.setPosition(d_wantCentre);

        if(flag)
        {
                d_batWind->mapAreaChanged(d_display.area());
                setXScroll();
                setYScroll();
                InvalidateRect(getHWND(), NULL, TRUE);
        }

        return flag;
}


void
BattleMapWindImp::centerLocation(void) {

   BattleMeasure::HexCord bottomleft_hex, size;
   d_battleData->getPlayingArea(&bottomleft_hex, &size);

   BattleMeasure::HexCord center_hex(
      bottomleft_hex.x() + (size.x() /2),
      bottomleft_hex.y() + (size.y() /2)
   );

   BattleLocation center_location = d_battleData->getLocation(center_hex);

   setLocation(center_location);
}

/*
 * Local function to set desired location
 * this indirectly calls the setLocation function above, but
 * also allows the tiny map window to be updated
 */


bool BattleMapWindImp::updateLocation(const BattleLocation& loc)
{
        return d_batWind->setMapLocation(loc);
}

LRESULT BattleMapWindImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
        LRESULT l;
        if(PaletteWindow::handlePalette(hWnd, msg, wParam, lParam, l))
                return l;

                  switch(msg)
        {
                                         HANDLE_MSG(hWnd, WM_CREATE,                     onCreate);
                HANDLE_MSG(hWnd, WM_DESTROY,                    onDestroy);
           HANDLE_MSG(hWnd, WM_PAINT,                           onPaint);
                                         // HANDLE_MSG(hWnd, WM_ERASEBKGND,              onEraseBk);
                // HANDLE_MSG(hWnd, WM_COMMAND,                 onCommand);
                HANDLE_MSG(hWnd, WM_SIZE,                               onSize);
                // HANDLE_MSG(hWnd, WM_GETMINMAXINFO,   onGetMinMaxInfo);
                // HANDLE_MSG(hWnd, WM_NOTIFY,                  onNotify);
                // HANDLE_MSG(hWnd, WM_CLOSE,                           onClose);
                                         HANDLE_MSG(hWnd, WM_SETCURSOR,    onSetCursor);
                                         HANDLE_MSG(hWnd, WM_HSCROLL,    onHScroll);
                                         HANDLE_MSG(hWnd, WM_VSCROLL,    onVScroll);
                                         HANDLE_MSG(hWnd, WM_LBUTTONDOWN,                onLButtonDown);
                                         HANDLE_MSG(hWnd, WM_LBUTTONUP,          onLButtonUp);
                                         HANDLE_MSG(hWnd, WM_RBUTTONDOWN,                onRButtonDown);
                                         HANDLE_MSG(hWnd, WM_RBUTTONUP,          onRButtonUp);
                                         HANDLE_MSG(hWnd, WM_MOUSEMOVE,          onMouseMove);

                  default:
                                         // return SimpleChildWindow::procMessage(hWnd, msg, wParam, lParam);
                                         return defProc(hWnd, msg, wParam, lParam);
                  }

}

BOOL BattleMapWindImp::onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg)
{
        if(d_isZooming)
        {
                ASSERT(scenario->getZoomCursor() != NULL);
                SetCursor(scenario->getZoomCursor());
                return TRUE;
        }
        else if(d_batWind->setCursor())
                return TRUE;
        else
                return FORWARD_WM_SETCURSOR(hwnd, hwndCursor, codeHitTest, msg, defProc);
}

BOOL BattleMapWindImp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#ifdef DEBUG
                  batmapLog.printf(">onCreate()");
#endif

                  /*
         * Create scroll bars
         */

        RECT r;
        GetClientRect(hWnd, &r);
        d_hwndVScroll = csb_create(
                0, WS_CHILD | SBS_VERT,
                r.right-SB_Width,       /* horizontal position       */
                0,                      /* vertical position         */
                SB_Width,               /* width of the scroll bar   */
                r.bottom-SB_Height,     /* default height            */
                hWnd,                   /* handle of main window          */
                APP::instance()         /* instance owning this window    */
        );

        ASSERT(d_hwndVScroll != NULL);

        /*
         * Get imagelist for scroll buttons
         * set imagelist, scrollbk and thumbbk
         */

        ImagePos* pos = static_cast<ImagePos*>(loadResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(PR_MAPSCROLLBTNS), RT_IMAGEPOS));
        ASSERT(pos != 0);

        csb_setButtonIcons(d_hwndVScroll, ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons));
        csb_setThumbBk(d_hwndVScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground));
        csb_setScrollBk(d_hwndVScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground));
        csb_setBorder(d_hwndVScroll, &scenario->getBorderColors());

        d_hwndHScroll = csb_create(
                0, WS_CHILD | SBS_HORZ,
                0,                      /* horizontal position       */
                r.bottom-SB_Height,     /* vertical position         */
                r.right, // -SB_Width,  /* width of the scroll bar   */
                SB_Height,              /* default height            */
                hWnd,                   /* handle of main window          */
                APP::instance()         /* instance owning this window    */
        );

        ASSERT(d_hwndHScroll != NULL);

        csb_setButtonIcons(d_hwndHScroll, ScenarioImageLibrary::get(ScenarioImageLibrary::HScrollButtons));
        csb_setThumbBk(d_hwndHScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollThumbBackground));
        csb_setScrollBk(d_hwndHScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::HScrollBackground));
        csb_setBorder(d_hwndHScroll, &scenario->getBorderColors());

        ShowWindow(d_hwndVScroll, SW_SHOW);
        ShowWindow(d_hwndHScroll, SW_SHOW);

        /*
         * Restore values from registry
         */

        bool hex_outlines;
        if(getRegistry(s_regHexoutline, &hex_outlines, sizeof(hex_outlines), s_registryName))
                d_display.hexOutline(hex_outlines);


#ifdef DEBUG
        bool hexDebug;
        if(getRegistry(s_regHexdebug, &hexDebug, sizeof(hexDebug), s_registryName))
                d_display.hexDebug(hexDebug);
#endif


#ifdef DEBUG
        batmapLog.printf("<onCreate()");
#endif

        return TRUE;
}

void BattleMapWindImp::onDestroy(HWND hWnd)
{
        /*
         * Save values to registry
         */

        bool hex_outlines = d_display.hexOutline();
        setRegistry(s_regHexoutline, &hex_outlines, sizeof(hex_outlines), s_registryName);

#ifdef DEBUG
        bool hexDebug = d_display.hexDebug();
        setRegistry(s_regHexdebug, &hexDebug, sizeof(hexDebug), s_registryName);
#endif
}


void BattleMapWindImp::onSize(HWND hwnd, UINT state, int cx, int cy)
{
#ifdef DEBUG
        batmapLog.printf(">onSize(%d,%d)", cx, cy);
#endif

                  /*
         * Work out where we go in relation to clientWindow
         */

        if(state == SIZE_MAXIMIZED)
        {
#ifdef DEBUG
                batmapLog.printf("BattleWindow is maximized");
#endif
        }
        else if(state == SIZE_RESTORED)
        {
#ifdef DEBUG
                batmapLog.printf("BattleMap is restored");
#endif
        }

        MoveWindow(d_hwndVScroll, cx-SB_Width, 0, SB_Width, cy-SB_Height, TRUE);
        // MoveWindow(d_hwndHScroll, 0, cy-SB_Height, cx-SB_Width, SB_Height, TRUE);
        MoveWindow(d_hwndHScroll, 0, cy-SB_Height, cx, SB_Height, TRUE);

        if(state != SIZE_MINIMIZED)
        {
                int w = cx - SB_Width;
                int h = cy - SB_Height;

                if(d_display.setSize(w, h))
                                                                d_batWind->mapAreaChanged(d_display.area());

                setScrollRange();
                setXScroll();
                setYScroll();
        }

#ifdef DEBUG
        batmapLog.printf("<onSize");
#endif
}

void BattleMapWindImp::onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
#ifdef DEBUG
        batmapLog.printf("onHScroll(%d,%d)", code, pos);
#endif

        int minP;
        int maxP;

        pos = csb_getPosition(hwndCtl);
        csb_getRange(hwndCtl, &minP, &maxP);

        BattleLocation centre(d_display.getCentre());
        BattleLocation size(d_display.getSize());

        BattleMeasure::Distance newX = centre.x();

        switch(code)
        {
        case SB_PAGELEFT:
                newX -= size.x();
                break;
        case SB_PAGERIGHT:
                newX += size.x();
                break;
        case SB_LINELEFT:
                newX -= size.x() / 16;
                break;
        case SB_LINERIGHT:
                newX += size.x() / 16;
                break;
        case SB_THUMBPOSITION:
                                         newX = pos + size.x() / 2;
                break;
        default:
                return;
        }

        centre.x(newX);

        if(updateLocation(centre))
        {
                setXScroll();
                redraw(true);
        }
}

void BattleMapWindImp::onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
#ifdef DEBUG
        batmapLog.printf("onVScroll(%d,%d)", code, pos);
#endif

        // BattleLocation mapSize(d_battleData->getMapSize());
        BattleLocation centre(d_display.getCentre());
        BattleLocation size(d_display.getSize());

        BattleMeasure::Distance newY = centre.y();

                  switch(code)
        {
        case SB_PAGELEFT:
                newY += size.y();
                break;
        case SB_PAGERIGHT:
                newY -= size.y();
                break;
        case SB_LINELEFT:
                newY += size.y() / 16;
                break;
        case SB_LINERIGHT:
                newY -= size.y() / 16;
                break;
        case SB_THUMBPOSITION:
                {
                        int minP;
                        int maxP;

                        pos = csb_getPosition(hwndCtl);
                        csb_getRange(hwndCtl, &minP, &maxP);

                        newY = maxP + minP - (pos + size.y()/2);
                }
                break;
        default:
                return;
                  }

        centre.y(newY);

        if(updateLocation(centre))
        {
                setYScroll();
                redraw(true);
        }
}

void BattleMapWindImp::onPaint(HWND hWnd)
{
#ifdef DEBUG
        // batmapLog.printf(">onPaint()");
        batmapLog << ">onPaint started at " << sysTime << endl;
#endif

        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);

#ifdef DEBUG
        batmapLog.printf("PAINTSTRUCT x : %i, y : %i, width : %i, height : %i",
         ps.rcPaint.left,
         ps.rcPaint.top,
         ps.rcPaint.right - ps.rcPaint.left,
         ps.rcPaint.bottom - ps.rcPaint.top
      );
#endif

        HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
        RealizePalette(hdc);


        if(d_needsRedraw) d_display.draw();
        d_needsRedraw = true;
        
      // debug info
      unsigned int blit_time = timeGetTime();

        d_display.blit(&ps);

      // debug info
      blit_time = timeGetTime() - blit_time;
      

      if(d_displayFPS) {
         d_frames++;
         int time = timeGetTime();
         if((time - d_lastTime) >= 1000) {
            d_lastTime = time;
            sprintf(d_framesString, "%i fps", d_frames);
            d_frames = 0;
         }
         TextOut(hdc, 0, 0, d_framesString, strlen(d_framesString));
         char tmp[256];
         sprintf(tmp, "blit_time %i", blit_time);
         TextOut(hdc, 64, 0, tmp, strlen(tmp));
      }
         

        SelectPalette(hdc, oldPal, FALSE);

        EndPaint(hWnd, &ps);

#ifdef DEBUG
        batmapLog << "<onPaint finished at " << sysTime << endl;
                  // batmapLog.printf("<onPaint()");
#endif
}

/*
 * Scroll Bar setup
 */

void BattleMapWindImp::setXScroll()
{
#ifdef DEBUG
        batmapLog.printf("setXScroll()");
#endif

        BattleLocation corner(d_display.getCorner());
        BattleLocation size(d_display.getSize());

        csb_setPosition(d_hwndHScroll, corner.x());
        csb_setPage(d_hwndHScroll, size.x());
}

void BattleMapWindImp::setYScroll()
{
#ifdef DEBUG
        batmapLog.printf("setYScroll()");
#endif

                  BattleArea mapArea(d_battleData->getViewableArea());
        BattleLocation corner(d_display.getCorner());
        BattleLocation size(d_display.getSize());

        // Y Coordinates are bottom to top

        csb_setPosition(d_hwndVScroll, 
                mapArea.bottom() - size.y() - (corner.y() - mapArea.top()));

        csb_setPage(d_hwndVScroll, size.y());

}

void BattleMapWindImp::setScrollRange()
{
#ifdef DEBUG
        batmapLog.printf("setScrollRange()");
#endif
        BattleArea mapArea(d_battleData->getViewableArea());

        csb_setRange(d_hwndHScroll, mapArea.left(), mapArea.right());
        csb_setRange(d_hwndVScroll, mapArea.top(), mapArea.bottom());
}

inline PixelPoint screenPoint(HWND hwnd, int x, int y)
{
        POINT pt = PixelPoint(x, y);
                  ClientToScreen(hwnd, &pt);
        return pt;
}

void BattleMapWindImp::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{

        if(d_isZooming)
        {
                BattleLocation loc = d_display.pixelToLocation(PixelPoint(x,y));
                d_wantCentre = loc;
                BattleMapInfo::Mode mode = d_display.mode();
                ASSERT(mode != 0);
                DECREMENT(mode);
                d_display.mode(mode);
                d_display.setPosition(loc);
                d_batWind->mapAreaChanged(d_display.area());
                d_batWind->mapZoomChanged();
                setXScroll();
                setYScroll();
                redraw(true);
            startZoom();
        }
        else
        {
                                         d_batWind->onLButtonDown(
                                                                BattleMapSelect(
                                                                                  screenPoint(hwnd,x,y),
                                                                                  d_display.pixelToLocation(PixelPoint(x,y)),
                                                                                  BattleMapSelect::LeftButton ) );
                  }
}

void BattleMapWindImp::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{

    if(d_isZooming)
        d_isZooming = false;
    else
        d_batWind->onLButtonUp(
            BattleMapSelect(
                screenPoint(hwnd,x,y),
                d_display.pixelToLocation(PixelPoint(x,y)),
                BattleMapSelect::LeftButton ) );
}

/*
 * Right button...
 */

void BattleMapWindImp::onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{

    if(d_isZooming)
    {
        // cancel zoom

        d_isZooming = false;
    }
    else
    {
        d_batWind->onRButtonDown(
            BattleMapSelect(
                screenPoint(hwnd,x,y),
                d_display.pixelToLocation(PixelPoint(x,y)),
                BattleMapSelect::RightButton ) );
    }
}

void BattleMapWindImp::onRButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{

                  if(d_isZooming)
                                         d_isZooming = false;
                  else
                                         d_batWind->onRButtonUp(
                                                                BattleMapSelect(
                                                                                  screenPoint(hwnd,x,y),
                                                                                  d_display.pixelToLocation(PixelPoint(x,y)),
                                                                                  BattleMapSelect::RightButton ) );
}

void BattleMapWindImp::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{

                  PixelPoint p(x, y);

                  BattleLocation bLoc = d_display.pixelToLocation(p);

                  // Convert p to screen coordinates

                  POINT pt = p;
        ClientToScreen(hwnd, &pt);
        PixelPoint screenPoint = pt;

        d_batWind->onMove(BattleMapSelect(screenPoint, bLoc, BattleMapSelect::NoButton));
}

void BattleMapWindImp::redraw(bool all)
{
  d_display.redraw(all);
  InvalidateRect(getHWND(), NULL, TRUE);
}

/*
 * Interface Functions
 */

BattleMapWind::BattleMapWind(PBattleWindows batWind, RCPBattleData battleData)
{
        d_imp = new BattleMapWindImp(batWind, battleData);
}


BattleMapWind::~BattleMapWind() 
{
        delete d_imp;
}

void BattleMapWind::create()
{
        d_imp->create();
}

HWND BattleMapWind::getHWND() const
{
        return d_imp->getHWND();
}

void BattleMapWind::enable(bool visible)
{
    d_imp->enable(visible);
}

void BattleMapWind::show(bool visible)
{
    d_imp->show(visible);
}

bool BattleMapWind::isVisible() const
{
    return d_imp->isVisible();
}

bool BattleMapWind::isEnabled() const
{
    return d_imp->isEnabled();
}

// void BattleMapWind::ShowWindows(void) {
// 
//     d_imp->ShowWindows();
// }
// 
// void BattleMapWind::HideWindows(void) {
// 
//     d_imp->HideWindows();
// }

void BattleMapWind::reset()
{
    d_imp->reset();
}

BattleMapDisplay *
BattleMapWind::getDisplay(void) {

    return d_imp->getDisplay();

}

BattleMapInfo::Mode BattleMapWind::mode() const
{
        return d_imp->mode();
}

void BattleMapWind::mode(BattleMapInfo::Mode mode)
{
        d_imp->mode(mode);
}

void BattleMapWind::startZoom()
{
        d_imp->startZoom();
}

bool BattleMapWind::isZooming() const
{
        return d_imp->isZooming();
}

bool BattleMapWind::canZoom() const
{
        return d_imp->canZoom();
}


bool BattleMapWind::toggleHexOutline()
{
        return d_imp->toggleHexOutline();
}


bool BattleMapWind::hexOutline() const
{
        return d_imp->hexOutline();
}

#ifdef DEBUG
bool BattleMapWind::toggleHexDebug()
{
        return d_imp->toggleHexDebug();
}

bool BattleMapWind::hexDebug() const
{
        return d_imp->hexDebug();
}
#endif

/*
 * set location
 *      return true if it requires a redraw
 */

bool BattleMapWind::setLocation(const BattleLocation& loc)
{
        return d_imp->setLocation(loc);
}

// center map
void BattleMapWind::centerLocation(void) {

   d_imp->centerLocation();
}

/*
 * return the area being displayed
 */

BattleArea BattleMapWind::area() const
{
        return d_imp->area();
}

void BattleMapWind::update(bool all)
{
        d_imp->redraw(all);
}


void BattleMapWind::updateScrollBars(void) {

   d_imp->updateScrollBars();

}

bool
BattleMapWind::isVisibilityRunning(void) {

   return d_imp->isVisibilityRunning();
}



void
BattleMapWind::startVisibilityChecks(void) {

   d_imp->startVisibilityChecks();
}


void
BattleMapWind::stopVisibilityChecks(void) {

   d_imp->stopVisibilityChecks();
}






/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
