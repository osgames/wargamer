/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BDISPUTE_HPP
#define BDISPUTE_HPP

#ifndef __cplusplus
#error bdispute.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Display Utility
 *
 *----------------------------------------------------------------------
 */

#include "sprlib.hpp"
#include "point.hpp"
#include "hexiter.hpp"


namespace BattleDisplay {

	class Displayer;

	extern BattleMeasure::BattleTime::Tick LastTime;
	extern BattleMeasure::BattleTime::Tick NewTime;
	extern unsigned int FrameCounter;
	extern BattleMeasure::BattleTime::Tick LastTick;
	extern BattleMeasure::BattleTime::Tick DeltaTicks;
	extern unsigned int BuildingsLastTime;
}


class PixelPoint;
class DrawDIBDC;

namespace BattleDisplayUtility
{
        // static const Point<int> S_HexGraphicSize(79,42);
        // static const Point<int> S_HexGraphicSize(80,32);
        // static const Point<int> S_HexGraphicSize(82,44);

        // typedef HexIterator::Scale Scale;

        // Scale makeScale(LONG hexWidth, LONG hexHeight)
        // {
        //     return Scale(hexWidth / S_HexGraphicSize.x(),
        //                  hexHeight / S_HexGraphicSize.y());
        // }

        // void drawSquashedGraphic(const SWG_Sprite::SpriteBlock* sprite, DrawDIB* destDIB, const PixelRect& rect, const PixelPoint& offset);
                // Put simple graphic onto DIB
                // rect is in destination coordinates
                // offset is in source coordinates

        void drawSquashedGraphic(const SWG_Sprite::SpriteBlock* sprite, DrawDIB* destDIB, const PixelPoint& dest, const HexIterator::Scale& scale);
            // Draw sprite at position in dib
            // scaling by the ratio in scale
            // e.g. scale = (0.5,0.5) is half size.

        void drawShadowedGraphic(const DIB* srcDIB, DrawDIB* destDIB, const PixelPoint& destP, const SWG_Sprite::SpriteBlock* sprite, LONG wantW, LONG wantH);


};      // namespace BattleDisplayUtility

#endif /* BDISPUTE_HPP */

