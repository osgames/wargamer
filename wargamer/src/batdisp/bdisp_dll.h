/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BDISP_DL_H
#define BDISP_DL_H

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * BATDISP DLL header
 *
 *----------------------------------------------------------------------
 */

#if defined(NO_WG_DLL)
    #define BATDISP_DLL
#elif defined(EXPORT_BATDISP_DLL)
    #define BATDISP_DLL __declspec(dllexport)
#else
    #define BATDISP_DLL __declspec(dllimport)
#endif


#endif /* BDISP_DL_H */

