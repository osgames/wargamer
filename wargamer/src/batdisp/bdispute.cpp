/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Display Utility
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bdispute.hpp"
#include "grtypes.hpp"
#include "dib_blt.hpp"
#include "sprlib.hpp"
#include "palette.hpp"
// #include <auto_ptr.h>

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

namespace BattleDisplay {

    BattleMeasure::BattleTime::Tick LastTime = 0;
    BattleMeasure::BattleTime::Tick NewTime = 0;
    unsigned int FrameCounter = 0;
   BattleMeasure::BattleTime::Tick LastTick = 0;
   BattleMeasure::BattleTime::Tick DeltaTicks = 0;
   unsigned int BattleDisplay::BuildingsLastTime = 0;
};


template<class T>
class SimplePtr
{
        public:
                SimplePtr() : d_ptr(0) { }
                SimplePtr(T* t) : d_ptr(t) { }
                SimplePtr(SimplePtr<T>& p) : d_ptr(p.d_ptr) { p.d_ptr = 0; }

                SimplePtr<T>& operator = (T* p)
                {
                        delete d_ptr;
                        d_ptr = p;
                        return *this;
                }

                SimplePtr<T>& operator = (SimplePtr<T>& p)
                {
                        delete d_ptr;
                        d_ptr = p.d_ptr;
                        p.d_ptr = 0;
                        return *this;
                }

                operator T* () { return d_ptr; }
                operator const T* () const { return d_ptr; }
                T* operator ->() { return d_ptr; }
                const T* operator ->() const { return d_ptr; }

        private:
                T* d_ptr;
};

namespace BattleDisplayUtility
{


class ShadowLookUp
{
            // Visual C++ can't cope with this    
            // typedef ColourIndex Table[256][256];            // [src][dest]
            typedef ColourIndex TableRow[256];
            typedef TableRow Table[256];

        public:
                ShadowLookUp();
                ~ShadowLookUp() { delete[] d_table; }

                void invalidate();
                void setShadow(ColourIndex col, int percent);
                void clearShadow();
                void setMask(ColourIndex col);
                void clearMask();

                void remap(ColourIndex* dest, const ColourIndex* src, int width);

        private:
                void resetRow(ColourIndex row);

        private:
                // Table* d_table;
                TableRow* d_table;

                bool d_hasShadow;
                bool d_hasMask;
                ColourIndex d_shadow;
                ColourIndex d_mask;
                int d_shadowPercent;
};

ShadowLookUp::ShadowLookUp() :
        // d_table(new Table),
        d_table(new TableRow[256]),
        d_hasShadow(false),
        d_hasMask(false)
{
//        ColourIndex* ptr = (*d_table)[0];
        ColourIndex* ptr = d_table[0];

        for(int row = 0; row < 256; ++row)
        {
                for(int col = 0; col < 256; ++col)
                {
                        *ptr++ = row;
                }
        }

}

void ShadowLookUp::resetRow(ColourIndex row)
{
        // ColourIndex* ptr = (*d_table)[row];
        ColourIndex* ptr = d_table[row];
        for(int col = 0; col < 256; ++col)
                *ptr++ = row;
}


void ShadowLookUp::invalidate()
{
        clearShadow();
        clearMask();
}


void ShadowLookUp::setShadow(ColourIndex shadow, int percent)
{
        // if there is an old shadow defined
        if(d_hasShadow)
        {
                // clear the old shadow if a different index
                if(shadow != d_shadow) clearShadow();
                // if the same shadow percent, return
                else if(percent == d_shadowPercent) return;
        }

        d_shadow = shadow;
        d_shadowPercent = percent;
        d_hasShadow = true;

        ColourIndex* ptr = d_table[shadow];
        // ColourIndex* ptr = (*d_table)[shadow];
//        const ColourIndex* darkTable = Palette::getDarkRemapTable(percent);
//        Palette::RemapTable darkTable(Palette::getDarkRemapTable(percent) );

        CPalette::RemapTablePtr darkTable(CPalette::defaultPalette()->getDarkRemapTable(percent));

        // const ColourIndex * coltab = darkTable;

        for(int col = 0; col < 256; ++col)
            *ptr++ = (*darkTable)[col];
        //        *ptr++ = *coltab++;


//                *ptr++ = *darkTable++;
//        Palette::releaseTable(darkTable);
}

void ShadowLookUp::clearShadow()
{
    if(d_hasShadow) {
        // reset shadow row to initial
        if(!d_hasMask || (d_mask != d_shadow) )  resetRow(d_shadow);
        d_hasShadow = false;
    }
}

void ShadowLookUp::setMask(ColourIndex mask)
{
        // if there is an old mask defined
        if(d_hasMask)
        {
                // clear the old mask if a different index
                if(mask != d_mask) clearMask();
                // if same index, return
                else return;
        }

        d_mask = mask;
        d_hasMask = true;


        // ColourIndex* ptr = (*d_table)[mask];
        ColourIndex* ptr = d_table[mask];
        for(int col = 0; col < 256; ++col)
                *ptr++ = col;
}

void ShadowLookUp::clearMask()
{
    if(d_hasMask) {
        if(!d_hasShadow || (d_shadow != d_mask) ) resetRow(d_mask);
        d_hasMask = false;
    }
}


inline void ShadowLookUp::remap(ColourIndex* dest, const ColourIndex* src, int width)
{
        ASSERT(src != 0);
        ASSERT(dest != 0);
        ASSERT(width > 0);
        ASSERT(d_table);

        while(width--)
        {
                // *dest = (*d_table)[*src][*dest];
                *dest = d_table[*src][*dest];
                ++src;
                ++dest;
        }
}


/*
 * Squash a sprite into a destination DIB
 *
 * This can be optimised significantly by either:
 * - Maintaining a cache of resized sprites
 * - Let the higher level functions resize the sprites.  This would make
 *   troop display significantly quicker since a whole SP would only need
 *   to do one squash.
 */


// void drawSquashedGraphic(const SWG_Sprite::SpriteBlock* sprite, DrawDIB* destDIB, const PixelRect& rect, const PixelPoint& offset)

void drawSquashedGraphic(const SWG_Sprite::SpriteBlock* sprite, DrawDIB* destDIB, const PixelPoint& dest, const HexIterator::Scale& scale)
{
    PixelPoint destP = sprite->topLeft(PixelPoint(0,0));

    destP.setX( (destP.getX() + 0.5) * scale.x() );
    destP.setY( (destP.getY() + 0.5) * scale.y() );

    destP += dest;


    /*
     * Shadowed version
     *
     * 2 Steps:
     *
     * 1. Stretch into a temporary DIB
     * 2. Do a masked/shadowed blit
     */

    LONG wantW = (sprite->width() * scale.x());
    LONG wantH = (sprite->height() * scale.y());

    if(! wantW || !wantH) return;

    static SimplePtr<DrawDIB> s_bufDIB = 0;
    static Palette::PaletteID s_palID = 0;

    bool palChanged = (s_palID != Palette::paletteID());
    s_palID = Palette::paletteID();

    class OldSettings{
        public:
            OldSettings() : d_sprite(0), d_scale(0,0) { }
            void invalidate() { d_sprite = 0; }

            bool testAndUpdate(const SWG_Sprite::SpriteBlock* sprite, const HexIterator::Scale& scale)
            {
                if( (d_sprite != sprite) || (d_scale != scale))
                {
                    d_sprite = sprite;
                    d_scale = scale;
                    return true;
                }
                return false;
            }


        private:
            const SWG_Sprite::SpriteBlock* d_sprite;
            HexIterator::Scale d_scale;
    };


    static OldSettings s_previousSettings;

    /*
     * Make sure bufDIB is created and big enough
     * also take into account possible system palette change
     */

    if( palChanged || (s_bufDIB == 0) )
    {
        // round width up multiple of 4
        LONG w = (wantW + 3) & ~3;

        s_bufDIB = new DrawDIB(w, wantH);       // auto_ptr auto deletes old one
        s_palID = Palette::paletteID();
        s_previousSettings.invalidate();
    }
    else
    {
        if( (s_bufDIB->getWidth() < wantW) ||
            (s_bufDIB->getHeight() < wantH) )
      {
            LONG w = (wantW + 3) & ~3;

            s_bufDIB->resize(w, wantH);
            s_previousSettings.invalidate();
      }
    }

    /*
     * Stretch into bufDIB with no masking
     */

    if(s_previousSettings.testAndUpdate(sprite, scale))
        DIB_Utility::stretchDIBNoMask(s_bufDIB, 0, 0, wantW, wantH, sprite, 0, 0, sprite->width(), sprite->height());

    drawShadowedGraphic(s_bufDIB, destDIB, destP, sprite, wantW, wantH);
}

/*
 * Blit to destination with masking and shadowing
 *
 * srcDIB contains alrady sized graphic in its top left.
 * sprite is used to obtain mask and shadow colours
 */

void drawShadowedGraphic(const DIB* srcDIB, DrawDIB* destDIB, const PixelPoint& destP, const SWG_Sprite::SpriteBlock* sprite, LONG wantW, LONG wantH)
{
    /*
     * Make colour look up table... note this only needs doing if the palette
     * changes, or the shadowing value is altered
     *
     * This table is 256 by 256 entries to convert (src, dest) --> final
     * in most cases final=src
     * but mask = dest
     *      and shadow = shadow
     */

    static ShadowLookUp s_lookup;
    static Palette::PaletteID s_palID = 0;

    bool palChanged = (s_palID != Palette::paletteID());
    s_palID = Palette::paletteID();

    if(palChanged)
        s_lookup.invalidate();

    if(sprite->hasShadow())
        s_lookup.setShadow(sprite->shadowColor(), 30);
    else
        s_lookup.clearShadow();

    if(sprite->hasMask())
        s_lookup.setMask(sprite->getMask());
    else
        s_lookup.clearMask();

    /*
     * Clip coordinates
     */

    LONG sx = 0;
    LONG sy = 0;
    LONG dx = destP.getX();
    LONG dy = destP.getY();
    if(destDIB->clip(dx, dy, wantW, wantH))
    {
        sx += dx - destP.getX();
        sy += dy - destP.getY();

        const UBYTE* srcLine = srcDIB->getAddress(sx, sy);
        UBYTE* destLine = destDIB->getAddress(dx, dy);

        while(wantH--)
        {
            s_lookup.remap(destLine, srcLine, wantW);
            srcLine += srcDIB->getStorageWidth();
            destLine += destDIB->getStorageWidth();
        }
    }

}


};      // namespace BattleDisplayUtility


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
