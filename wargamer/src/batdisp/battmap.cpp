/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Tiny Battle Map
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "battmap.hpp"
#include "batdisp.hpp"
// #include "generic.hpp"
#include "palwind.hpp"
#include "palette.hpp"
#include "app.hpp"
#include "dib.hpp"


class TinyBattleMapWindImp :
        public PaletteWindow,
        public WindowBaseND,
        public CustomBorderWindow
{
   typedef DWORD Tick;
   enum
   { TicksPerSecond = 1000
   };

   public:
   TinyBattleMapWindImp(const PBattleWindows& batWind, RCPBattleData battleData);
   ~TinyBattleMapWindImp();
   void create(HWNDbase* parent);
   void reset();

   LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   void setLocation(const BattleArea& area);

   void update(bool all);
   // Note that data may have changed

   private:
   BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
   void onSize(HWND hwnd, UINT state, int cx, int cy);
   void onPaint(HWND hWnd);
   void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
   void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
   void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);

   void updateZoomArea(const PixelPoint& p);
   void redraw(bool all);
   void updateSize();

   Tick getCurrentTick()
   { return timeGetTime();
   } // System MM function

   private:
   BattleMapDisplay        d_display;
   PBattleWindows          d_batWind;
   PixelRect               d_zoomRect;             // Area shown by main map
   bool                    d_dragging;
   Tick                    d_lastUpdateTick;       // When was it last drawn?
   CPBattleData            d_battleData;
   PixelPoint              d_pixelSize;
};

/*
 * Border Colors until we sort out scenario data for battles
 */

static CustomBorderInfo borderColors = {
        PALETTERGB(189, 165, 123),
        PALETTERGB(255, 247, 215),
        PALETTERGB(230, 214, 123),
        PALETTERGB(115, 90, 66)
};


TinyBattleMapWindImp::TinyBattleMapWindImp(const PBattleWindows& batWind, RCPBattleData battleData) :
        PaletteWindow(),
        WindowBaseND(),
        CustomBorderWindow(borderColors),
        d_display(battleData, BattleMapInfo::Tiny),
        d_batWind(batWind),
        d_zoomRect(),                   // Empty Rectangle
        d_dragging(false),
        d_lastUpdateTick(0),
        d_pixelSize(128,110),
        d_battleData(battleData)
{
}

TinyBattleMapWindImp::~TinyBattleMapWindImp()
{
   selfDestruct();
}

void TinyBattleMapWindImp::create(HWNDbase* hParent)
{

   // BattleArea l = battleData->getViewableArea();
   // d_display.setPosition(l.topLeft() + l.size() / 2);
   d_display.setPosition(BattleLocation(0,0)); // let display clip it

   ASSERT(getHWND() == NULL);

   ASSERT(d_batWind() != 0);
   // HWND hParent = d_batWind->hwnd();
   ASSERT(hParent != NULL);


   createWindow(0,
      // className(),
      "Overview",             // NULL,
      WS_CHILD | WS_CLIPSIBLINGS,     // WS_POPUP | WS_CAPTION,
      0,0, d_pixelSize.x(), d_pixelSize.y(),
      hParent->getHWND(),                     /* parent window */
      NULL                           /* menu handle (or child ID) */
      // APP::instance()                         /* program handle */
      );

   // ShowWindow(getHWND(), SW_SHOW);
}

void TinyBattleMapWindImp::updateSize()
{
   // static const PixelPoint defaultSize(128, 110);
   BattleArea area(d_battleData->getViewableArea());

   if (area.width())
   {
      PixelPoint newSize;
      newSize.x(128);
      newSize.y(MulDiv(newSize.x(), area.height(), area.width()));

      if((newSize != d_pixelSize) && (newSize.y() > 0))
      {
         d_pixelSize = newSize;
         SetWindowPos(getHWND(), HWND_TOP, 0, 0, newSize.x(), newSize.y(), SWP_NOMOVE | SWP_NOZORDER);

         FORWARD_WM_COMMAND(GetParent(getHWND()), 0, getHWND(), WM_SIZE, SendMessage);
      }
   }
}

LRESULT TinyBattleMapWindImp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   LRESULT l;
   if(PaletteWindow::handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
   HANDLE_MSG(hWnd, WM_CREATE,                     onCreate);
   // HANDLE_MSG(hWnd, WM_DESTROY,                 onDestroy);
   HANDLE_MSG(hWnd, WM_PAINT,                           onPaint);
   // HANDLE_MSG(hWnd, WM_ERASEBKGND,              onEraseBk);
   // HANDLE_MSG(hWnd, WM_COMMAND,                 onCommand);
   HANDLE_MSG(hWnd, WM_SIZE,                               onSize);
   // HANDLE_MSG(hWnd, WM_GETMINMAXINFO,   onGetMinMaxInfo);
   // HANDLE_MSG(hWnd, WM_NOTIFY,                  onNotify);
   // HANDLE_MSG(hWnd, WM_CLOSE,                           onClose);
   // HANDLE_MSG(hWnd, WM_HSCROLL, onHScroll);
   // HANDLE_MSG(hWnd, WM_VSCROLL, onVScroll);
   HANDLE_MSG(hWnd, WM_LBUTTONDOWN,                onLButtonDown);
   HANDLE_MSG(hWnd, WM_LBUTTONUP,          onLButtonUp);
   HANDLE_MSG(hWnd, WM_MOUSEMOVE,          onMouseMove);

   default:
      // return SimpleChildWindow::procMessage(hWnd, msg, wParam, lParam);
      return defProc(hWnd, msg, wParam, lParam);
   }

}

BOOL TinyBattleMapWindImp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#ifdef DEBUG
   bdLog.printf(">TMAP::onCreate()");
#endif


#ifdef DEBUG
   bdLog.printf("<TMAP::onCreate()");
#endif

   return TRUE;
}


void TinyBattleMapWindImp::onSize(HWND hwnd, UINT state, int cx, int cy)
{
#ifdef DEBUG
   bdLog.printf(">TMAP::onSize(%d,%d)", cx, cy);
#endif

   if(state != SIZE_MINIMIZED)
   {
      PixelRect r(CustomBorderWindow::ThinBorder,
         CustomBorderWindow::ThinBorder,
         cx - CustomBorderWindow::ThinBorder,
         cy - CustomBorderWindow::ThinBorder);

      d_display.setSize(r);
   }

#ifdef DEBUG
   bdLog.printf("<TMAP::onSize");
#endif
}

void TinyBattleMapWindImp::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   if(!d_dragging)
   {
      d_dragging = true;
      SetCapture(hwnd);
   }

   updateZoomArea(PixelPoint(x, y));
   // updateMapPos(x,y);
}

void TinyBattleMapWindImp::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
   if(d_dragging)
   {
      d_dragging = false;
      ReleaseCapture();
   }

   // updateMapPos(x,y);

   BattleLocation bloc = d_display.pixelToLocation(PixelPoint(x,y));
   d_batWind->setMapLocation(bloc);
}

void TinyBattleMapWindImp::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
   //      using BattleWindows_Internal::BattleMapSelect;

   PixelPoint p(x, y);

   if(d_dragging)
      updateZoomArea(p);

   BattleLocation bLoc = d_display.pixelToLocation(p);
   // d_batWind->overLocation(bLoc);
   // Convert p to screen coordinates

   POINT pt = p;
   ClientToScreen(hwnd, &pt);
   PixelPoint screenPoint = pt;

   d_batWind->onMove(BattleMapSelect(screenPoint, bLoc, BattleMapSelect::NoButton));

}

void TinyBattleMapWindImp::updateZoomArea(const PixelPoint& p)
{
   PixelPoint wSize = d_display.getPixelSize();

   LONG width = d_zoomRect.width();
   LONG height = d_zoomRect.height();

   LONG wantX = p.x() - width / 2;
   LONG wantY = p.y() - height / 2;

   LONG maxX = wSize.x() - width;
   LONG maxY = wSize.y() - height;

   if(wantX < 0)
      wantX = 0;
   else if(wantX > maxX)
      wantX = maxX;
   if(wantY < 0)
      wantY = 0;
   else if(wantY > maxY)
      wantY = maxY;

   d_zoomRect = PixelRect(wantX, wantY, wantX + width, wantY + height);

   redraw(false);
}

void TinyBattleMapWindImp::onPaint(HWND hWnd)
{
#ifdef DEBUG
   // bdLog.printf(">TMAP::onPaint()");
   bdLog << ">TMAP::onPaint started at " << sysTime << endl;
#endif

   PAINTSTRUCT ps;
   HDC hdc = BeginPaint(hWnd, &ps);

   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);


   bool redraw = d_display.draw();         // Draw stuff into the DIBs without blitting

   if(redraw)
   {
      d_lastUpdateTick = getCurrentTick();
   }

   if(redraw && (d_zoomRect.width() != 0) && (d_zoomRect.height() != 0))
   {
      DrawDIBDC* dib = d_display.mainDIB();

      ColorIndex black = dib->getColour(PALETTERGB(0,0,0));
      ColorIndex white = dib->getColour(PALETTERGB(255,255,255));

      LONG x1 = d_zoomRect.left();
      LONG x2 = d_zoomRect.right();
      LONG y1 = d_zoomRect.top();
      LONG y2 = d_zoomRect.bottom();

      // dib->frame(x1+1, y1+1, d_zoomRect.width()-2, d_zoomRect.height()-2, white);

      /*
      * Lighten area not in view
      */

      // Palette::RemapTable remapTable = Palette::getDarkRemapTable(10);
      CPalette::RemapTablePtr remapTable = dib->getPalette()->getDarkRemapTable(10);

      if(y1 != 0)
         dib->remapRectangle(remapTable, 0,0, dib->getWidth(), y1);
      if(y2 != dib->getHeight())
         dib->remapRectangle(remapTable, 0,y2, dib->getWidth(), dib->getHeight()-y2);
      if(x1 != 0)
         dib->remapRectangle(remapTable, 0,y1, x1,y2-y1);
      if(x2 != dib->getWidth())
         dib->remapRectangle(remapTable, x2,y1, dib->getWidth()-x2, y2-y1);

      // Palette::releaseTable(remapTable);

      dib->frame(x1, y1, d_zoomRect.width(), d_zoomRect.height(), white);
      dib->frame(x1+1, y1+1, d_zoomRect.width()-2, d_zoomRect.height()-2, black);

      // dib->lightenRectangle(d_zoomRect.left(), d_zoomRect.top(), d_zoomRect.width(), d_zoomRect.height(), 50);
      // dib->line(x1, y1, x2, y1, black);
      // dib->line(x2, y1, x2, y2, black);
      // dib->line(x2, y2, x1, y2, black);
      // dib->line(x1, y2, x1, y1, black);

      // dib->line(x1, y1, x2, y1, black);
      // dib->line(x2, y1, x2, y2, black);
      // dib->line(x2, y2, x1, y2, black);
      // dib->line(x1, y2, x1, y1, black);

   }

   d_display.blit(&ps);

   RECT r;
   GetClientRect(hWnd, &r);
   drawThinBorder(hdc, r);

   SelectPalette(hdc, oldPal, FALSE);

   EndPaint(hWnd, &ps);

#ifdef DEBUG
   // bdLog.printf("<TMAP::onPaint()");
   bdLog << "<TMAP::onPaint finished at " << sysTime << endl;
#endif
}

/*
 * Ask for tiny map to be redrawn
 *
 * to speed things up until we get around to optimizing the display routines
 * we will only up date it if more than 10 seconds have elapsed since the
 * last update.
 */

void TinyBattleMapWindImp::update(bool all)
{
#ifdef DEBUG
   bdLog << "<TMAP::update(" << all << "), " << sysTime << endl;
#endif

   updateSize();

   /*
   * Drop out if only doing a dynamic update and less than
   * 10 seconds have passed since the last draw
   *
   * A better method might be to put the display on a seperate thread
   * which redraws every 10 seconds IF redraw has been called.
   */

   Tick tick = getCurrentTick();

   if(!all)
   {
      const Tick period = TicksPerSecond * 10;

      Tick diffTick = tick - d_lastUpdateTick;
      if(diffTick < period)
         return;
   }

   d_lastUpdateTick = tick;

   redraw(all);
}

/*
 * Internal redraw
 *
 * This is not subject to the 10 second delay, so that changing the
 * zoomed area is immediate.
 */

void TinyBattleMapWindImp::redraw(bool all)
{
#ifdef DEBUG
   bdLog << "<TMAP::redraw(" << all << ", " << sysTime << endl;
#endif

   d_display.redraw(all);
   InvalidateRect(getHWND(), NULL, TRUE);
}


void TinyBattleMapWindImp::reset()
{
   // Bodge to force display to note if map has changed in size

   updateSize();

#if 0
      RECT r;
   GetClientRect(getHWND(), &r);
   r.left += CustomBorderWindow::ThinBorder;
   r.top += CustomBorderWindow::ThinBorder;
   r.right -= CustomBorderWindow::ThinBorder;
   r.bottom -= CustomBorderWindow::ThinBorder;
   d_display.setSize(r);
#else
   d_display.reset();
#endif
}

/*
 * Set location of the rectangle
 */

void TinyBattleMapWindImp::setLocation(const BattleArea& area)
{
   /*
   * Convert area into pixels
   */

   PixelPoint top = d_display.locationToPixel(area.topLeft());
   PixelPoint bottom = d_display.locationToPixel(area.bottomRight());

   // order of points is odd because physical coordinates are bottom up
   // whereas pixel coordinates are top down

   d_zoomRect = PixelRect(top.x(), bottom.y(), bottom.x(), top.y());
   redraw(false);
}





/*
 * Interface Functions
 */

TinyBattleMapWind::TinyBattleMapWind(HWNDbase* parent, RPBattleWindows batWind, RCPBattleData battleData)
{
   d_imp = new TinyBattleMapWindImp(batWind, battleData);
   d_imp->create(parent);
}


TinyBattleMapWind::~TinyBattleMapWind()
{
   delete d_imp;
}


HWND TinyBattleMapWind::getHWND() const
{
   return d_imp->getHWND();
}

bool TinyBattleMapWind::isVisible() const
{
   return d_imp->isVisible();
}

bool TinyBattleMapWind::isEnabled() const
{
   return d_imp->isEnabled();
}

void TinyBattleMapWind::show(bool visible)
{
   d_imp->show(visible);
   if(visible)
      d_imp->update(true);
}

void TinyBattleMapWind::enable(bool enable)
{
   d_imp->enable(enable);
   if(enable)
      d_imp->update(true);
}



void TinyBattleMapWind::setLocation(const BattleArea& area)
{
   d_imp->setLocation(area);
}

void TinyBattleMapWind::update(bool all)
{
   d_imp->update(all);
}

void TinyBattleMapWind::reset()
{
   d_imp->reset();
   d_imp->update(true);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
