/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Building Display
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "buildisp.hpp"
#include "bdispute.hpp"
#include "hexiter.hpp"
#include "building.hpp"
#include "sprlib.hpp"
#include "bdispob.hpp"
#include "zlist.hpp"
// #include "refptrim.hpp"


using namespace SWG_Sprite;

using namespace BattleDisplay;


// static const BuildingPixelScale = ((float)BattleDisplayUtility::S_HexGraphicSize.x() * 1.0);           // scale at which graphics were drawn (1.6)
// static const float BuildingPixelScaleRecip = (float) 1.0 / (float) BuildingPixelScale;


/*
class for drawing buildings by inserting into the Y-sorted sprite list
*/
class BattleBuildingDisplayObject : public DisplayObject  {

public:
   
   typedef HexIterator::Scale Scale;

private:

    Scale d_scale;

    const SWG_Sprite::SpriteBlock* d_sprite_building;
    const SWG_Sprite::SpriteBlock* d_sprite_effect;

public:

    // constructor
   BattleBuildingDisplayObject(
    const SWG_Sprite::SpriteBlockPtr & sprite_building,
    const SWG_Sprite::SpriteBlockPtr & sprite_effect,
    const Scale& scale) :
        d_sprite_building(sprite_building),
        d_sprite_effect(sprite_effect),
        d_scale(scale)
    {
    }

   // constructor for creating without an effect sprite
   BattleBuildingDisplayObject(
    const SWG_Sprite::SpriteBlockPtr & sprite_building,
    const Scale& scale) :
        d_sprite_building(sprite_building),
        d_sprite_effect(0),
        d_scale(scale)
    {
    }


    // draw function
    virtual void draw(DrawDIB* dib, const PixelPoint& p) const;

};





class RefPtrBattleBuildingDisplayObject : public RefPtr<BattleBuildingDisplayObject> {

   typedef RefPtr<BattleBuildingDisplayObject> Super;

   // Don't allow any constructors except for creator
   RefPtrBattleBuildingDisplayObject();
                
public:

   RefPtrBattleBuildingDisplayObject(const RefPtrBattleBuildingDisplayObject& ptr) : Super(ptr) { }

   // Creator constructor
   RefPtrBattleBuildingDisplayObject(
      const SWG_Sprite::SpriteBlock* sprite_building,
      const SWG_Sprite::SpriteBlock* sprite_effect,
      const HexIterator::Scale& scale) :

   Super(new BattleBuildingDisplayObject(sprite_building, sprite_effect, scale)) { }


   // Constructor for creating without an 'effect' sprite
   RefPtrBattleBuildingDisplayObject(
      const SWG_Sprite::SpriteBlock* sprite_building,
      const HexIterator::Scale& scale) :

   Super(new BattleBuildingDisplayObject(sprite_building, scale)) { }


   operator RefPtr<DisplayObject> () const {
      return RefPtr<DisplayObject>(value());
   }

};





void BattleBuildingDisplayObject::draw(DrawDIB* dib, const PixelPoint& p) const {

    PixelPoint fullSize;
    // int width;
    PixelRect rect;

   // draw building
    BattleDisplayUtility::drawSquashedGraphic(
        d_sprite_building,
        dib,
        p,
        d_scale
   );

   // draw effect
   if(d_sprite_effect) {
      BattleDisplayUtility::drawSquashedGraphic(
         d_sprite_effect,
         dib,
         p,
         d_scale
      );
   }
}




void
BuildingDisplay::drawBuildings(Displayer * d_display, const HexIterator& iter, const BattleData * batData, SWG_Sprite::SpriteLibrary* sprLib) {

   const BuildingList * list = batData->buildingList();
   const BuildingTable * table = batData->buildingTable();
   const EffectsTable * effectsTable = batData->effectsTable();

   BuildingListEntry * entry = const_cast<BuildingListEntry *>(list->get(iter.hex()));


   if(entry != NULL) {

        BuildingInfo const * building_info = table->info(entry->index() );

        if(building_info != NULL) {

      /*
      * Decide wether to update animation
      */
      unsigned int DeltaFrames = (BattleDisplay::FrameCounter - entry->lastTime());
      entry->lastTime(BattleDisplay::FrameCounter);
      // modify by gametime to realtime ratio
      int UpdateAnim = DeltaFrames * batData->timeRatio();


            /*
            Go through all buildings in cluster
            */
         int b_num = 0;

            const BuildingInfo::DisplayInfo& dispInfo = building_info->displayInfo();

            for(BuildingInfo::DisplayInfo::const_iterator buildingIter = dispInfo.begin(); buildingIter != dispInfo.end(); ++buildingIter) {

            ASSERT(b_num < MAX_BUILDINGS_PER_CLUSTER);

               const BuildingDisplayInfo* bdInfo = &*buildingIter;

            /*
             * Get sprites from sprite library
             */

            SpriteLibrary::Index gIndexNormal = bdInfo->normalIndex();
            ASSERT(gIndexNormal != SpriteLibrary::NoGraphic);
            SWG_Sprite::SpriteBlockPtr sprite_normal(sprLib, gIndexNormal);

            SpriteLibrary::Index gIndexDestroyed = bdInfo->destroyedIndex();
            ASSERT(gIndexDestroyed != SpriteLibrary::NoGraphic);
            SWG_Sprite::SpriteBlockPtr sprite_destroyed(sprLib, gIndexDestroyed);

            /*
             * Calculate height of graphics
             */

             PixelPoint position(iter.position(bdInfo->position()));

            /*
             * Get graphic indexes dependent on building cluster state
             */
 
             

             switch(entry->status()) {


               case BUILDING_NORMAL : {

                  RefPtrBattleBuildingDisplayObject building_obj(sprite_normal, iter.buildingScale() );
                  d_display->draw(building_obj, position);
                  break;
               }


               case BUILDING_BURNING : {

                  const EffectInfo::DisplayInfo & fxinfo = effectsTable->info(BUILDING_FIRE).displayInfo();

                  int frame_index = fxinfo[entry->frame(b_num)].index;
                  SWG_Sprite::SpriteBlockPtr sprite_effect(sprLib, frame_index);
                  RefPtrBattleBuildingDisplayObject building_obj(sprite_normal, sprite_effect, iter.buildingScale() );
                  d_display->draw(building_obj, position);

                  batData->soundSystem()->triggerSound(GetFireSound(), BATTLESOUNDPRIORITY_UNIQUE, iter.hex() );

                  /*
                  * Update animation frames
                  */
                  if(UpdateAnim) {

                     entry->timer(b_num, entry->timer(b_num)+1 );
                     if(entry->timer(b_num) > fxinfo[entry->frame(b_num)].frame_time) {
                        entry->timer(b_num, 0);
                        entry->frame(b_num, entry->frame(b_num)+1 );

                        if(fxinfo[entry->frame(b_num)].frame_time == 0) {
                           entry->frame(b_num, 0);
                        }
                     }
                  }

                  break;
               }

               case BUILDING_SMOULDERING : {

                  const EffectInfo::DisplayInfo & fxinfo = effectsTable->info(BUILDING_SMOKE).displayInfo();

                  int frame_index = fxinfo[entry->frame(b_num)].index;
                  SWG_Sprite::SpriteBlockPtr sprite_effect(sprLib, frame_index);
                  RefPtrBattleBuildingDisplayObject building_obj(sprite_destroyed, sprite_effect, iter.buildingScale() );
                  d_display->draw(building_obj, position);

                  batData->soundSystem()->triggerSound(GetFireSound(), BATTLESOUNDPRIORITY_UNIQUE, iter.hex() );

                  /*
                  * Update animation frames
                  */
                  if(UpdateAnim) {

                     entry->timer(b_num, entry->timer(b_num)+1 );
                     if(entry->timer(b_num) > fxinfo[entry->frame(b_num)].frame_time) {
                        entry->timer(b_num, 0);
                        entry->frame(b_num, entry->frame(b_num)+1 );

                        if(fxinfo[entry->frame(b_num)].frame_time == 0) {
                           entry->frame(b_num, 0);
                        }
                     }
                  }

                  break;
               }

               case BUILDING_DESTROYED : {

                  RefPtrBattleBuildingDisplayObject building_obj(sprite_destroyed, iter.buildingScale() );
                  d_display->draw(building_obj, position);
                  break;
               }

               default : {
                  FORCEASSERT("ERROR - building in BuildingsList has an invalid building-status");
                  break;
               }

             } // end of switch statement

             b_num++;

         } // repeat for all buildings in cluster

      } // if BuildingInfo != NULL

   } // if BuildingEntry for this hex != NULL

}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
