/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATDISP_HPP
#define BATDISP_HPP

#ifndef __cplusplus
#error batdisp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Display
 *
 *----------------------------------------------------------------------
 */

#include "bdisp_dll.h"
#include "batdata.hpp"
#include "batmap_i.hpp"
#include "grtypes.hpp"



#ifdef DEBUG
#include "clog.hpp"
extern LogFileFlush bdLog;
#endif

// using namespace WG_BattleData;

class PixelPoint;
class BattleMapDisplayImp;
class DrawDIBDC;

class BattleMapDisplay
{
                BattleMapDisplayImp* d_imp;
        public:
                BATDISP_DLL BattleMapDisplay(RCPBattleData batData, BattleMapInfo::Mode mode = BattleMapInfo::OneMile);
                BATDISP_DLL ~BattleMapDisplay();

                void reset();  // Note that map may have changed

                BATDISP_DLL bool draw();
                        // Draw information into the DIB
                BATDISP_DLL void blit(PAINTSTRUCT * ps);
                        // Copy DIBs to the screen

                BATDISP_DLL bool setPosition(const BattleLocation& location);
                        // Set window position
                        // Clips to battleSize
                        // returns true if position really changed

                BATDISP_DLL bool setSize(int width, int height) { return setSize(PixelRect(0,0,width,height)); }
                BATDISP_DLL bool setSize(const PixelRect& r);
                        // set display size
                        // return true if changed

                BATDISP_DLL PixelPoint getPixelSize() const;
                BATDISP_DLL const BattleLocation& getCorner() const;
                BATDISP_DLL const BattleLocation& getSize() const;
                BATDISP_DLL const BattleLocation& getCentre() const;
                                         BattleArea area() const;
                BATDISP_DLL PixelPoint hexPixelSize() const; 

                BATDISP_DLL void redraw(bool all);

                BATDISP_DLL BattleMapInfo::Mode mode() const;
                BATDISP_DLL void mode(BattleMapInfo::Mode mode);

                BATDISP_DLL DrawDIBDC* mainDIB();
                BATDISP_DLL DrawDIBDC* staticDIB();

                BATDISP_DLL bool toggleHexOutline();
                BATDISP_DLL bool hexOutline() const;
                BATDISP_DLL bool hexOutline(bool flag);
#ifdef DEBUG
                BATDISP_DLL bool toggleHexDebug();
                BATDISP_DLL bool hexDebug() const;
                BATDISP_DLL bool hexDebug(bool flag);
#endif

                BATDISP_DLL BattleLocation pixelToLocation(const PixelPoint& p);
                BATDISP_DLL PixelPoint locationToPixel(const BattleLocation& b);
				BATDISP_DLL void getHexCorners(BattleMeasure::HexCord & bottom_left, BattleMeasure::HexCord & top_right);
				BATDISP_DLL BattleMeasure::HexCord getBottomLeft(void);
				BATDISP_DLL BattleMeasure::HexCord getTopRight(void);


                BATDISP_DLL void ShowSelection(bool state);
                BATDISP_DLL void SetSelectionColour(ColourIndex col);
                BATDISP_DLL void SetSelectionArea(BattleMeasure::HexCord bottomleft, BattleMeasure::HexCord size);

                BATDISP_DLL void ShowDarkenedArea(bool state);
                BATDISP_DLL void SetDarkenedArea(int tophex, int bottomhex);

                BATDISP_DLL void RequestTerrainAveraging(void);

				BATDISP_DLL void showHexVisibility(bool state);
				BATDISP_DLL bool isHexVisibilityOn(void);


				BATDISP_DLL bool isDisplayingTimes(void);
				BATDISP_DLL void displayTimes(bool state);

				BATDISP_DLL bool isDrawingTerrain(void);
				BATDISP_DLL void drawTerrain(bool state);
				BATDISP_DLL bool isDrawingOverlaps(void);
				BATDISP_DLL void drawOverlaps(bool state);
				BATDISP_DLL bool isDrawingPathways(void);
				BATDISP_DLL void drawPathways(bool state);
				BATDISP_DLL bool isDrawingCoasts(void);
				BATDISP_DLL void drawCoasts(bool state);
				BATDISP_DLL bool isDrawingHills(void);
				BATDISP_DLL void drawHills(bool state);
				BATDISP_DLL bool isDrawingEdges(void);
				BATDISP_DLL void drawEdges(bool state);
				BATDISP_DLL bool isDrawingTroops(void);
				BATDISP_DLL void drawTroops(bool state);
				BATDISP_DLL bool isDrawingBuildings(void);
				BATDISP_DLL void drawBuildings(bool state);
				BATDISP_DLL bool isDrawingEffects(void);
				BATDISP_DLL void drawEffects(bool state);


};

#endif /* BATDISP_HPP */

