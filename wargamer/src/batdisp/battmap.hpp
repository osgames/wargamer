/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/

#ifndef BATTMAP_HPP
#define BATTMAP_HPP

#ifndef __cplusplus
#error battmap.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Tiny Battle Map
 *
 *----------------------------------------------------------------------
 */

#include "bdisp_dll.h"
#include "refptr.hpp"
#include "batdata.hpp"
#include "btwin_i.hpp"
#include "wind.hpp"

// using namespace WG_BattleData;

class HWNDbase;
class TinyBattleMapWindImp;

class TinyBattleMapWind :
        public RefBaseDel,
        public Window
{
                TinyBattleMapWindImp* d_imp;

        public:
                BATDISP_DLL TinyBattleMapWind(HWNDbase* parent, RPBattleWindows batWind, RCPBattleData battleData);
                BATDISP_DLL ~TinyBattleMapWind();

                BATDISP_DLL virtual HWND getHWND() const;
                BATDISP_DLL virtual bool isVisible() const;
                BATDISP_DLL virtual bool isEnabled() const;
                BATDISP_DLL virtual void show(bool visible);
                BATDISP_DLL virtual void enable(bool enable);

                BATDISP_DLL void setLocation(const BattleArea& area);

                BATDISP_DLL void update(bool all);
                        // redraw dynamic map

                BATDISP_DLL void reset();
                        // note that battlemap may have changed
};


#endif /* BATTMAP_HPP */

