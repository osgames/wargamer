/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Battle Display
 *
 * Currently this is all done as part of the user interface thread
 * in response to WM_PAINT messages.
 *
 * I have considered making this run in a seperate thread
 * and to use double buffering.  I've also considered calling this
 * directly from the logic thread.
 *
 * The way this would work is that the parent's onPaint would
 * simply copy a DIB to the screen.
 *
 * Scrolling and other movement changes would just set an event
 * for the thread to wait for.
 *
 * Various flags would be used to indicate whether the static or dynamic
 * parts need to be redrawn.
 *
 * We could organize the static display into levels of details so that
 * for scrolling a fast aproximation is drawn, and then after the scrolling
 * stops it is rendered properly.
 *
 * We could have "dirty " rectangles so that only areas where units are
 * moving are redrawn on the dynamic DIB.
 *
 * We could pre-render the entire battlefield and just blit the apropriate
 * section, making scrolling very fast.  This would require about 44 Megs
 * thus is not really feasible!
 *
 * We could pre-render or cache any of the graphics that need resizing, such
 * as the river hexes.  This should speed up the static display and scrolling.
 *
 * We could cache some of the working information used to display units
 * instead of rebuilding it every time.
 *
 * Tiny Map only needs to be redrawn if units have moved to different hexes
 * or been destroyed.  If they have changed formation or facing then there
 * is no difference on the display.
 *
 * Advantages of threading are:
 *  - User Interface should be very responsive.  This applies whether this
 *    is a seperate thread, or if it is part of the logic thread.
 * Disadvantages are:
 *  - Slower execution, because thread is running at the same time as
 *    other threads.  If part of Logic Thread then this would be
 *    insignificant.
 *  - Screen may be out of sync with real-world data, causing anomalies
 *    such as clicking on a unit and another one's information coming up.
 *    This would be true if not part of logic thread.
 *  - If part of Logic thread, then logic thread must be more complex
 *    to ensure that screen updates are frequent enough.  e.g. the combat
 *    calculations may be so complex that it takes several seconds to run
 *    through all the units.
 *  - Memory for an extra DIB is needed (768K at 1024X768 resolution).
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "batdisp.hpp"

#include <math.h>

// Other BATDISP headers

#include "hexiter.hpp"
#include "bdispute.hpp"
#include "buildisp.hpp"
#include "unitdisp.hpp"
#include "fx_disp.hpp"
#include "zlist.hpp"
#include "bd_hill.hpp"
#include "tempdib.hpp"

#ifdef USE_ZBUFFER
#include "zbuffer.hpp"
#endif

// Batdata
#include "batdata.hpp"
#include "b_terr.hpp"
#include "hexdata.hpp"

// System

#include "misc.hpp"
#include "wmisc.hpp"
#include "sprlib.hpp"
#include "autoptr.hpp"
#include "palette.hpp"
#include "dib_blt.hpp"
#include "dib_util.hpp"
#include "dib_poly.hpp"
#include "random.hpp"
#include "sync.hpp"
#include "palette.hpp"
#include "ftol.hpp"
#include "fonts.hpp"

// Gamesup
#include "options.hpp"
#include "scenario.hpp"

// STL

#include <limits.h>
#include <map>                                // STL container
// #include <function.h>           // STL Functions

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

#ifdef DEBUG
#include "clog.hpp"
LogFileFlush bdLog("batdisp.log");
// LogFile bdLog("batdisp.log");
#endif


using namespace BattleDisplayUtility;
using BattleDisplay::ZDisplayObjectList;
using namespace BattleDisplay;
using namespace SWG_Sprite;

template<class X>
class SharedObject
{
      X* d_lib;
      int d_usage;
   public:
      SharedObject() : d_lib(0), d_usage(0) {  }
      ~SharedObject() { ASSERT(d_usage == 0); delete d_lib;  }

      void addRef() { ++d_usage;  }
      void delRef()
      {
         ASSERT(d_usage > 0);
         if(--d_usage == 0)
         {
            delete d_lib; d_lib = 0;
         }
      }

      SharedObject& operator = (X* lib)
      {
         ASSERT(d_lib == 0);
         d_lib = lib;
         return *this;
      }

       X& operator*()  const { return *d_lib; }
       X* operator->() const { return d_lib; }
       X* get()        const { return d_lib; }
};

class SharedSprites : public SharedObject<SWG_Sprite::SpriteLibrary>
{
      typedef SharedObject<SWG_Sprite::SpriteLibrary> Super;
   public:
      SharedSprites() {  }
      ~SharedSprites() {  }

      void init(const char* name);
   private:
};

void SharedSprites::init(const char* name)
{
   if(get() == 0)
   {
      StringPtr fileName = scenario->makeScenarioFileName(name);
      Super::operator=(new SWG_Sprite::SpriteLibrary(fileName));
      ASSERT(get() != 0);
   }
   addRef();
}

/*
 * Class for getting Average Terrain Colours
 */

class TerrainColors
{
   public:
      TerrainColors(const DIB* dib, SWG_Sprite::SpriteLibrary* sprLib) :
         // d_dib(dib),
         d_palette(dib->getPalette()),
         d_sprLib(sprLib),
         d_indexes()
      {
      }

      ColorIndex getColor(SWG_Sprite::SpriteLibrary::Index index);
      void reset(const DIB* dib);

   private:
      SWG_Sprite::SpriteLibrary* d_sprLib;
      // const DIB* d_dib;
      PalettePtr d_palette;

      typedef std::map<SWG_Sprite::SpriteLibrary::Index, ColorIndex, std::less<SWG_Sprite::SpriteLibrary::Index> > IndexContainer;
      IndexContainer d_indexes;

      typedef std::map<SWG_Sprite::SpriteLibrary::Index, COLORREF, std::less<SWG_Sprite::SpriteLibrary::Index> > ColorContainer;
      static ColorContainer s_colors;
};

TerrainColors::ColorContainer TerrainColors::s_colors;

void TerrainColors::reset(const DIB* dib)
{
   if (dib->getPalette() != d_palette)
   {
      d_palette = dib->getPalette();
      d_indexes.clear();
   }
}

ColorIndex TerrainColors::getColor(SWG_Sprite::SpriteLibrary::Index index)
{
   ColorIndex colIndex;
   IndexContainer::const_iterator indexIt = d_indexes.find(index);
   if(indexIt != d_indexes.end())
      colIndex = (*indexIt).second;
   else
   {
      COLORREF rgb;
      ColorContainer::const_iterator colorIt = s_colors.find(index);
      if (colorIt != s_colors.end())
         rgb = (*colorIt).second;
      else
      {
         // calculate average color
         SWG_Sprite::SpriteBlockPtr sprite(d_sprLib, index);
         rgb = sprite->averageColor();
         s_colors[index] = rgb;
      }

      // colIndex = d_dib->getPalette()->getIndex(rgb);
      colIndex = d_palette->getIndex(rgb);
      d_indexes[index] = colIndex;
   }

   return colIndex;
}


/*
 * Battle Map Display Implementation
 */

class BattleMapDisplayImp : public BattleMapDisplayInt
{

   public:

      BattleMapDisplayImp(RCPBattleData batData, BattleMapInfo::Mode mode = BattleMapInfo::OneMile);
      ~BattleMapDisplayImp();

      bool draw();
      void blit(PAINTSTRUCT * ps);
      DrawDIBDC* mainDIB()   { return d_mainDIB.get();    }
      DrawDIBDC* staticDIB() { return d_staticDIB.get();  }

      bool setPosition(const BattleLocation& location);
      // Set window position
      // Clips to battleSize
      // returns true if position really changed

      bool setSize(const PixelRect& r);

      PixelPoint getPixelSize() const { return PixelPoint(d_pixelArea.width(), d_pixelArea.height());  }
      const BattleLocation& getCorner() const { return d_corner;  }
      const BattleLocation& getSize() const { return d_size;  }
      const BattleLocation& getCentre() const { return d_centre;  }
      BattleArea area() const { return BattleArea(d_corner, d_size);  }
      PixelPoint hexPixelSize() const;

      RCPBattleData getBattleData() const { return d_batData;  }

      void redraw(bool all)
      {
         if(all)
            d_staticRedraw = true;
         else
            d_redraw = true;
      }

      BattleMapInfo::Mode mode() const { return d_mode;  }
      void mode(BattleMapInfo::Mode mode);

      bool toggleHexOutline() { return hexOutline(!d_bDrawHexOutlines);  }
      bool hexOutline() const { return d_bDrawHexOutlines;  }
      bool hexOutline(bool flag) { d_bDrawHexOutlines = flag; redraw(true); return d_bDrawHexOutlines;  }
#ifdef DEBUG
      bool toggleHexDebug() { return hexDebug(!d_hexDebug);  }
      bool hexDebug() const { return d_hexDebug;  }
      bool hexDebug(bool flag) { d_hexDebug = flag; redraw(true); return d_hexDebug;  }
#endif

      BattleLocation pixelToLocation(const PixelPoint& p) const;
      PixelPoint locationToPixel(const BattleLocation& b) const;

      /*
      return top-left / bottom-right hexes on display - now obsolete ?
      */
      void getHexCorners(HexCord & bottom_left, HexCord & top_right);
      HexCord getBottomLeft(void);
      HexCord getTopRight(void);

      /*
      handles the selection-area display (ie. for selecting battlefield
      */
      void ShowSelection(bool state) { d_ShowSelection = state;  }

      void SetSelectionColour(ColourIndex col) { d_SelectionColour = col;  }

      void SetSelectionArea(HexCord bottomleft, HexCord size) {
         d_SelectionBottomLeft = bottomleft;
         d_SelectionSize = size;
      }

      /*
      handles the horizontal-band darkened area of map - now obsolete ?
      */
      void ShowDarkenedArea(bool state) { d_ShowDarkenedArea = state;  }

      void SetDarkenedArea(int tophex, int bottomhex) {
         d_DarkenedAreaHighHex = tophex;
         d_DarkenedAreaLowHex = bottomhex;
      }

#if 0
      /*
      next time display is constructed, colours will be averaged
      */
      void RequestTerrainAveraging(void) {
         m_bAveragedColours = false;
      }
#endif

      /*
      handles the display of the visibility-status of hexes
      */
      bool isHexVisibilityOn(void) { return d_showHexVisibility;  }

      // set visibility state
      void showHexVisibility(bool state) {

         d_showHexVisibility = state;

         // if switched off, we have to reset all the hexdata :
         // a bit naughty to do from the display, but necessary !
         if(!d_showHexVisibility) {
            const BattleMap * cmap = d_batData->map();
            BattleMap * map = const_cast<BattleMap *>(cmap);
            HexCord mapsize = map->getSize();
            for(int y=0; y<mapsize.y(); y++) {
               for(int x=0; x<mapsize.x(); x++) {
                  BattleTerrainHex& maphex = map->get(HexCord(x,y));
                  maphex.d_visibility = Visibility::Full;
                  maphex.d_lastFullVisUnit = NULL;
               }
            }
         }

      }


      void reset();


      /*
      Debugging toggles
      */

      bool isDisplayingTimes(void) { return d_bDisplayTimes;  }
      void displayTimes(bool state) { d_bDisplayTimes = state;  }


      /*
      Drawing toggles
      */

      bool isDrawingTerrain(void) { return d_bDrawTerrain;  }
      void drawTerrain(bool state) { d_bDrawTerrain = state;  }

      bool isDrawingOverlaps(void) { return d_bDrawOverlaps;  }
      void drawOverlaps(bool state) { d_bDrawOverlaps = state;  }

      bool isDrawingPathways(void) { return d_bDrawPathways;  }
      void drawPathways(bool state) { d_bDrawPathways = state;  }

      bool isDrawingCoasts(void) { return d_bDrawCoasts;  }
      void drawCoasts(bool state) { d_bDrawCoasts = state;  }

      bool isDrawingHills(void) { return d_bDrawHills;  }
      void drawHills(bool state) { d_bDrawHills = state;  }

      bool isDrawingEdges(void) { return d_bDrawEdges;  }
      void drawEdges(bool state) { d_bDrawEdges = state;  }

      bool isDrawingTroops(void) { return d_bDrawTroops;  }
      void drawTroops(bool state) { d_bDrawTroops = state;  }

      bool isDrawingBuildings(void) { return d_bDrawBuildings;  }
      void drawBuildings(bool state) { d_bDrawBuildings = state;  }

      bool isDrawingEffects(void) { return d_bDrawEffects;  }
      void drawEffects(bool state) { d_bDrawEffects = state;  }


   private:

      void drawStatic();
      void drawStaticOverview();
      void drawStaticTiny(bool AntiAlias);
      void drawStaticDetail();
      void drawMain();

      DrawDIBDC* tempDIB(LONG w, LONG h) { return d_tempDIB.get(w,h);  }

      void drawPath(const HexIterator& iter, const PathInfo& path, const TerrainTable& terrainTable);
      // Draw path onto hex
      void drawEstuary(const HexIterator& iter, const PathInfo& path, EdgeMap edges, const TerrainTable& tTable);
      // Draw Estuaries onto hex

      void drawSquashedGraphic(const HexIterator& iter, SpriteLibrary::Index gIndex) const;

      Point<int> getDetailAspectRatio();
      Point<int> getNormalAspectRatio() { return s_normalAspectRatio;  }

      void createHighlightDIBs(unsigned int dib_w, unsigned int dib_h, unsigned int max_width, unsigned int max_height, HexIterator &iter);
      void createStippleDIBs(unsigned int dib_w, unsigned int dib_h, unsigned int max_width, unsigned int max_height, HexIterator &iter);

   private:

      std::auto_ptr<DrawDIBDC> d_staticDIB;
      std::auto_ptr<DrawDIBDC> d_mainDIB;
      TempDIB d_tempDIB;

#ifdef USE_ZBUFFER
      ZBuffer<UBYTE> d_zBuffer;
#endif

      ZDisplayObjectList d_zlist;
      bool d_redraw; // Set if time has changed so map should be redrawn
      bool d_staticRedraw; // Set if it all needs redrawing

      TerrainColors* d_terrainColors;      // Obtain a colour to use to average a terrain sprite

      /*
      Draw toggles for static display
      */
      bool d_bDrawTerrain;
      bool d_bDrawOverlaps;
      bool d_bDrawPolys;
      bool d_bDrawPathways;
      bool d_bDrawCoasts;
      bool d_bDrawHills;
      bool d_bDrawEdges;
      bool d_bDrawHexOutlines;
      bool d_bDrawTroops;
      bool d_bDrawBuildings;
      bool d_bDrawEffects;




#ifdef DEBUG
      bool d_hexDebug;                                                        // Set if debug information drawn
#endif

      CPBattleData d_batData;
      BattleLocation d_centre;        // Physical coordinate of centre of display
      BattleLocation d_size;          // viewable size
      BattleLocation d_corner;

      PixelRect d_pixelArea;            // rectangle

      Point<int>              d_aspectRatio;  // Used for aspect ratio
      BattleMapInfo::Mode     d_mode;

      // Cached Overlap masks
      // This could be made static and shared

      OverlapMaskList d_overlaps;


      int d_lastMaxWidth;
      int d_lastMaxHeight;

      // DIBs for stippling non-visible hexes
      DrawDIB * d_stippleDIBNotSeen;
      DrawDIB * d_stippleDIBPoor;
      DrawDIB * d_stippleDIBFair;
      DrawDIB * d_stippleDIBGood;

      // DIBs for highlighting hexes
      DrawDIB * d_alliedNormalHighlightDIB;
      DrawDIB * d_frenchNormalHighlightDIB;

      DrawDIB * d_alliedDarkenedHighlightDIB;
      DrawDIB * d_frenchDarkenedHighlightDIB;

      DrawDIB * d_alliedDarkestHighlightDIB;
      DrawDIB * d_frenchDarkestHighlightDIB;

      DrawDIB * d_alliedNormalOutlineHighlightDIB;
      DrawDIB * d_frenchNormalOutlineHighlightDIB;

      // friend class HexIterator;
      RWLock d_lock;

      int d_MinimumDetailViewPixelWidth;

      static Point<int> s_detailAspectRatio;
      static const Point<int> s_normalAspectRatio;
      static const char s_terrainSpriteFileName[];
      static const char s_troopSpriteFileName[];
      static const char s_buildingSpriteFileName[];

//      dib->getIndex(rgb);static bool m_bAveragedColours;
//      void AverageTerrainColours();

      bool d_ShowSelection;
      ColourIndex d_SelectionColour;
      HexCord d_SelectionBottomLeft;
      HexCord d_SelectionSize;

      bool d_ShowHexLabels;
      bool d_ShowVictoryPoints;

      bool d_ShowDarkenedArea;
      int d_DarkenedAreaHighHex;
      int d_DarkenedAreaLowHex;

      bool d_showHexVisibility;

      HillDisplay * d_hillDisplay;

      bool d_bDisplayTimes;
      unsigned int d_staticTime;
      unsigned int d_dynamicTime;

      LogFont d_captionLogFont;
      LogFont d_pointsLogFont;
      int d_lastFontHeight;
      void SetMapFonts(int max_height);


public:

     static SharedSprites * s_troopLibrary;
      static SharedSprites * s_terrainLibrary;
      static SharedSprites * s_buildingLibrary;
};


/*
 * Hex Display Aspect Ratio...  82 * 44 pixels
 *
 * Calculated as:
 *
 *              H = W * SQRT(3) / 2 * Sin(A)
 *
 * e.g. W = 82 pixels  (Desired width)
 *      A = 39 degrees (Angle looking down, 0=flat, 90=top down
 *      h1 = 70 pixels (Flat down height of hexes interlinked rectangle
 *      H = 44 pixels (Desired pixel height).
 *
 * Aspect Ratio is the ratio of displayed vertical height/width compared
 * to reality.  It is NOT the ratio of the hex height/width or the hex
 * inner rectangle's height/width.
 *
 * Given a rectangle that represents the non-overlapping rectangle of
 * a hex, then the Y size represents 3L and the X size 2.L.sqrt(3), where
 * L is half the length of a hex edge.
 *
 * then actual aspect ratio is: Px, Py.2/sqr(3)
 **
 * e.g. graphics are 82 X 44.  Aspect Ratio is 82 by 50
 */


Point<int> BattleMapDisplayImp::s_detailAspectRatio(0,0);

//         HexIterator::S_HexGraphicSize.x() * BattleMeasure::HexYOffset,
//        HexIterator::S_HexGraphicSize.y() * BattleMeasure::OneXHex);

Point<int> BattleMapDisplayImp::getDetailAspectRatio()
{
   if(s_detailAspectRatio.x() == 0)
   {
      Point<int> hexGraphicSize = HexIterator::hexGraphicSize();

      s_detailAspectRatio.x(BattleMeasure::HexYOffset * hexGraphicSize.x());
      s_detailAspectRatio.y(BattleMeasure::OneXHex * hexGraphicSize.y());
   }

   return s_detailAspectRatio;
}

const Point<int> BattleMapDisplayImp::s_normalAspectRatio(1,1);

const char BattleMapDisplayImp::s_terrainSpriteFileName[] = "terrain.spr";
const char BattleMapDisplayImp::s_troopSpriteFileName[] = "troops.spr";
const char BattleMapDisplayImp::s_buildingSpriteFileName[] = "building.spr";

SharedSprites * BattleMapDisplayImp::s_troopLibrary = 0;
SharedSprites * BattleMapDisplayImp::s_terrainLibrary = 0;
SharedSprites * BattleMapDisplayImp::s_buildingLibrary = 0;


/*
*  Make sure that these get deleted, if the ref counting fails
*/

class SpritesStaticDestructor {

public:

   SpritesStaticDestructor(void) { }

   ~SpritesStaticDestructor(void) {
      if(BattleMapDisplayImp::s_troopLibrary) {
         delete BattleMapDisplayImp::s_troopLibrary;
         BattleMapDisplayImp::s_troopLibrary = 0;
      }
      if(BattleMapDisplayImp::s_terrainLibrary) {
         delete BattleMapDisplayImp::s_terrainLibrary;
         BattleMapDisplayImp::s_terrainLibrary = 0;
      }
      if(BattleMapDisplayImp::s_buildingLibrary) {
         delete BattleMapDisplayImp::s_buildingLibrary;
         BattleMapDisplayImp::s_buildingLibrary = 0;
      }
   }
};

SpritesStaticDestructor g_GlobalSpritesStaticDestructor;



// static SharedObject<SWG_Sprite::SpriteLibrary> BattleMapDisplayImp::d_hexGraphics;


// bool BattleMapDisplayImp::m_bAveragedColours = false;

BattleMapDisplayImp::BattleMapDisplayImp(RCPBattleData batData, BattleMapInfo::Mode mode) :
   d_redraw(true),
   d_staticRedraw(true),
   d_terrainColors(0),
   d_bDrawTerrain(true),
   d_bDrawOverlaps(true),
   d_bDrawPolys(true),
   d_bDrawPathways(true),
   d_bDrawCoasts(true),
   d_bDrawHills(true),
   d_bDrawEdges(true),
   d_bDrawHexOutlines(false),
   d_bDrawTroops(true),
   d_bDrawBuildings(true),
   d_bDrawEffects(true),
   d_hillDisplay(0),
 #ifdef DEBUG
   d_hexDebug(false),
 #endif
   d_batData(batData),
   d_mode(mode),
   d_bDisplayTimes(false),
   d_staticTime(timeGetTime()),
   d_dynamicTime(timeGetTime()),
   d_lastFontHeight(0)
{
#ifdef DEBUG
   bdLog.printf("Constructed");

#endif

   if(!s_troopLibrary) {
      s_troopLibrary = new SharedSprites;
      s_troopLibrary->init(s_troopSpriteFileName);
   }
   else s_troopLibrary->addRef();

   if(!s_terrainLibrary) {
      s_terrainLibrary = new SharedSprites;
      s_terrainLibrary->init(s_terrainSpriteFileName);
   }
   else s_terrainLibrary->addRef();

   if(!s_buildingLibrary) {
      s_buildingLibrary = new SharedSprites;
      s_buildingLibrary->init(s_buildingSpriteFileName);
   }
   else s_buildingLibrary->addRef();




   // a call to precalculate troop formations on construction
   if( ! BattleDisplay::bFormationsPrecalced)
   {
      BattleDisplay::PrecalcFormations(d_batData );
      BattleDisplay::bFormationsPrecalced = true;
   }

   // set base colour of all terrain in terrain table
//    if( ! m_bAveragedColours)
//    {
//       AverageTerrainColours();
//       m_bAveragedColours = true;
//    }

   d_MinimumDetailViewPixelWidth = 12;

      d_ShowSelection = false;
   d_SelectionColour = 0;
   d_SelectionBottomLeft = HexCord(0,0);
   d_SelectionSize = HexCord(0,0);

   d_ShowHexLabels = true;
   d_ShowVictoryPoints = true;

   d_ShowDarkenedArea = false;
   d_DarkenedAreaHighHex = 0;
   d_DarkenedAreaLowHex = 0;

   d_showHexVisibility = false;

   d_lastMaxWidth = 0;
   d_lastMaxHeight = 0;

   int dib_w = 8;
   int dib_h = 8;


   d_stippleDIBNotSeen = new DrawDIB(dib_w,dib_h);
   d_stippleDIBPoor = new DrawDIB(dib_w,dib_h);
   d_stippleDIBFair = new DrawDIB(dib_w,dib_h);
   d_stippleDIBGood = new DrawDIB(dib_w,dib_h);


   d_alliedNormalHighlightDIB = new DrawDIB(dib_w,dib_h);
   d_frenchNormalHighlightDIB = new DrawDIB(dib_w,dib_h);
   d_alliedDarkenedHighlightDIB = new DrawDIB(dib_w,dib_h);
   d_frenchDarkenedHighlightDIB = new DrawDIB(dib_w,dib_h);
   d_alliedDarkestHighlightDIB = new DrawDIB(dib_w,dib_h);
   d_frenchDarkestHighlightDIB = new DrawDIB(dib_w,dib_h);

   d_alliedNormalOutlineHighlightDIB = new DrawDIB(dib_w,dib_h);
   d_frenchNormalOutlineHighlightDIB = new DrawDIB(dib_w,dib_h);

   BattleDisplay::LastTick = d_batData->getTick();

}



BattleMapDisplayImp::~BattleMapDisplayImp()
{
#ifdef DEBUG
   bdLog.printf("destructed");
#endif

   delete d_terrainColors;
   d_terrainColors = 0;

   s_troopLibrary->delRef();
   if(!s_troopLibrary->get()) {
      delete s_troopLibrary;
      s_troopLibrary = 0;
   }

   s_terrainLibrary->delRef();
   if(!s_terrainLibrary->get()) {
      delete s_terrainLibrary;
      s_terrainLibrary = 0;
   }

   s_buildingLibrary->delRef();
   if(!s_buildingLibrary->get()) {
      delete s_buildingLibrary;
      s_buildingLibrary = 0;
   }

   if(d_hillDisplay) delete d_hillDisplay;

   if(d_stippleDIBNotSeen) delete d_stippleDIBNotSeen;
   if(d_stippleDIBPoor) delete d_stippleDIBPoor;
   if(d_stippleDIBFair) delete d_stippleDIBFair;
   if(d_stippleDIBGood) delete d_stippleDIBGood;

   if(d_alliedNormalHighlightDIB) delete d_alliedNormalHighlightDIB;
   if(d_frenchNormalHighlightDIB) delete d_frenchNormalHighlightDIB;
   if(d_alliedDarkenedHighlightDIB) delete d_alliedDarkenedHighlightDIB;
   if(d_frenchDarkenedHighlightDIB) delete d_frenchDarkenedHighlightDIB;
   if(d_alliedDarkestHighlightDIB) delete d_alliedDarkestHighlightDIB;
   if(d_frenchDarkestHighlightDIB) delete d_frenchDarkestHighlightDIB;
   if(d_alliedNormalOutlineHighlightDIB) delete d_alliedNormalOutlineHighlightDIB;
   if(d_frenchNormalOutlineHighlightDIB) delete d_frenchNormalOutlineHighlightDIB;
}





#if 0    // Done as part of TerrainColors class and also in Sprlib.cpp
void
BattleMapDisplayImp::AverageTerrainColours(void) {

   const BattleData * tmpbd = d_batData;
   BattleData * batdata = const_cast <BattleData *> (tmpbd);

   TerrainTable & tTable = batdata->terrainTable();
   GraphicalTerrainEnum t = GT_None;
   INCREMENT(t);

   while(t < GT_HowMany) {

      SWG_Sprite::SpriteLibrary::Index gIndex;
      gIndex = tTable.get(t).baseGraphic();

      // const SWG_Sprite::SpriteBlock * spriteptr = s_terrainLibrary->read(gIndex);
      // SWG_Sprite::SpriteBlockPtr sprite(spriteptr);
      // spriteptr->release();
      SWG_Sprite::SpriteBlockPtr sprite(s_terrainLibrary.get(), gIndex);

      int x,y;
      int width,height;
      width = sprite->getWidth();
      height = sprite->getHeight();

      const UBYTE * col_index;
      int total_red=0;
      int total_green=0;
      int total_blue=0;

      HPALETTE pal = Palette::get();
      PALETTEENTRY pal_entry;

      RGBQUAD rgbval;

      for(y=0; y<height; y++) {
         for(x=0;x<width;x++) {

            col_index = sprite->getAddress(x,y);

            GetPaletteEntries(pal, *col_index, 1, &pal_entry);

            total_red += pal_entry.peRed;
            total_green += pal_entry.peGreen;
            total_blue += pal_entry.peBlue;

         }
      }

      total_red /= (width*height);
      total_green /= (width*height);
      total_blue /= (width*height);

      unsigned char i = t;
      TerrainTable::Info & info = tTable.get(i);

      info.color(PALETTERGB(total_red, total_green, total_blue) );

      INCREMENT(t);
   } // do for all terrain types

}
#endif











/*
 * Convert coordinates
 */

BattleLocation BattleMapDisplayImp::pixelToLocation(const PixelPoint& p) const
{
   return BattleLocation(
      d_corner.x() + MulDiv(p.getX(), d_size.x(), d_pixelArea.width()),
      d_corner.y() + MulDiv(d_pixelArea.height() - p.getY(), d_size.y(), d_pixelArea.height()) );
}

PixelPoint BattleMapDisplayImp::locationToPixel(const BattleLocation& b) const
{
   return PixelPoint(
      MulDiv( b.x() - d_corner.x(), d_pixelArea.width(), d_size.x() ),
      d_pixelArea.height() -
      MulDiv( b.y() - d_corner.y(), d_pixelArea.height(), d_size.y())
      );
}

void
BattleMapDisplayImp::getHexCorners(HexCord & bottom_left, HexCord & top_right) {

   BattleLocation bottom_left_location = pixelToLocation(PixelPoint(0, d_staticDIB->getHeight()) );
   BattleLocation top_right_location = pixelToLocation(PixelPoint(d_staticDIB->getWidth(), 0) );

   bottom_left.x( static_cast<int>(bottom_left_location.x() / QuantaPerXHex));
   bottom_left.y( static_cast<int>(bottom_left_location.y() / QuantaPerYHex));

   top_right.x( static_cast<int>(top_right_location.x() / QuantaPerXHex));
   top_right.y( static_cast<int>(top_right_location.y() / QuantaPerYHex));
}


HexCord
BattleMapDisplayImp::getBottomLeft(void) {

   if(!d_staticDIB.get()) return HexCord(0,0);

   BattleLocation bottom_left_location = pixelToLocation(PixelPoint(0, d_staticDIB->getHeight()) );

   HexCord bottom_left;
   bottom_left.x( static_cast<int>(bottom_left_location.x() / QuantaPerXHex));
   bottom_left.y( static_cast<int>(bottom_left_location.y() / QuantaPerYHex));

   return bottom_left;

}


HexCord
BattleMapDisplayImp::getTopRight(void) {

   if(!d_staticDIB.get()) return d_batData->map()->getSize();

   BattleLocation top_right_location = pixelToLocation(PixelPoint(d_staticDIB->getWidth(), 0) );

   HexCord top_right;
   top_right.x( (int)(top_right_location.x() / QuantaPerXHex));
   top_right.y( (int)(top_right_location.y() / QuantaPerYHex));

   return top_right;
}

/*
 * Reset values in case map has changed
 *
 * setSize does everything required...
 */

void BattleMapDisplayImp::reset()
{
   setSize(d_pixelArea);
}

/*
 * Set Centre of display on given location
 */

bool BattleMapDisplayImp::setPosition(const BattleLocation& location)
{
   /*
   * Clip coordinates to valid area
   */

   /*
   * Corner coordinate
   */

   BattleLocation l = location - d_size / 2;
   BattleArea batArea = d_batData->getViewableArea();
   BattleLocation maxLoc = batArea.bottomRight() + BattleLocation(1,1) - d_size;

   if(l.x() < batArea.left())
      l.x(batArea.left());
   else if(l.x() > maxLoc.x())
      l.x(maxLoc.x());

   if(l.y() < batArea.top())
      l.y(batArea.top());
   else if(l.y() > maxLoc.y())
      l.y(maxLoc.y());

   bool result = (d_corner != l);

   d_corner = l;

   l += d_size / 2;

   if(l != d_centre)
      result = true;

   d_centre = l;

   if(result)
      redraw(true);

   // d_centre = location;

   // d_corner.x(location.x() - d_size.x()/2);
   // d_corner.y(location.y() - d_size.y()/2);

#ifdef DEBUG
   bdLog.printf("setPosition(%d,%d)->(%d,%d)(%d,%d)",
      static_cast<int>(location.x()),
      static_cast<int>(location.y()),
      static_cast<int>(d_centre.x()),
      static_cast<int>(d_centre.y()),
      static_cast<int>(d_corner.x()),
      static_cast<int>(d_corner.y()));
#endif

   return result;
}

/*
 * Update values when themap has changed size
 */

bool BattleMapDisplayImp::setSize(const PixelRect& r)
{
   d_pixelArea = r;

   BattleLocation battleSize(d_batData->getViewableArea().size());

   switch(d_mode)
   {

   case BattleMapInfo::OneHex: {
         //                    d_size.x(BattleMeasure::xHexes(1));
         d_size.x(1.0*BattleMeasure::QuantaPerYHex);
         d_aspectRatio = getDetailAspectRatio();
         break;
      }

   case BattleMapInfo::OneMile: {
         d_size.x(BattleMeasure::miles(1));
         d_aspectRatio = getDetailAspectRatio();
         break;
      }

   case BattleMapInfo::TwoMile: {
         d_size.x(BattleMeasure::miles(2));
         d_aspectRatio = getDetailAspectRatio();
         break;
      }

   case BattleMapInfo::FourMile: {
         d_size.x(BattleMeasure::miles(4));
         d_aspectRatio = getDetailAspectRatio();
         break;
      }

   case BattleMapInfo::Strategic:
   case BattleMapInfo::Tiny: {
         d_size = battleSize;
         d_aspectRatio = getNormalAspectRatio();
         break;
      }

#ifdef DEBUG
   default:
      FORCEASSERT("Unknown BattleMode");
#endif
   }

   if(d_size.x() > battleSize.x())
      d_size.x(battleSize.x());

   // Set Y size to maintain correct aspect ratio

   if (d_mode != BattleMapInfo::Tiny)
   {
      d_size.y( MulDiv(d_pixelArea.height(), d_size.x(), d_pixelArea.width() ) );
      d_size.y( MulDiv(d_size.y(), d_aspectRatio.x(), d_aspectRatio.y() ) );
   }

   // If window is too high, then adjust the size so that the Y size shows
   // the complete height, and the x size is partial.

   if(d_size.y() > battleSize.y())
   {
      d_size.y(battleSize.y());
      d_size.x( MulDiv(d_pixelArea.width(), d_size.y(), d_pixelArea.height() ) );
      d_size.x( MulDiv(d_size.x(), d_aspectRatio.y(), d_aspectRatio.x() ) );
   }


   /*
   * Reset top
   */

   bool flag = setPosition(d_centre);

#ifdef DEBUG
   bdLog.printf("setSize(%d,%d)->(%d,%d),(%d%d)",
      static_cast<int>(r.width()),
      static_cast<int>(r.height()),
      static_cast<int>(d_corner.x()),
      static_cast<int>(d_corner.y()),
      static_cast<int>(d_size.x()),
      static_cast<int>(d_size.y()));
#endif

   return flag;
}


void BattleMapDisplayImp::mode(BattleMapInfo::Mode mode)
{
   d_mode = mode;
   // setAspectRatio();
   setSize(d_pixelArea);
}

#if 0
DrawDIBDC* BattleMapDisplayImp::tempDIB(LONG w, LONG h)
{
   if(d_tempDIB.get() == 0)
   {
      d_tempDIB = auto_ptr<DrawDIBDC>(new DrawDIBDC(w, h));
   }
   else if( (w > d_tempDIB->getWidth()) || (h > d_tempDIB->getHeight()) )
   {
      d_tempDIB->resize(maximum(w, d_tempDIB->getWidth()), maximum(h, d_tempDIB->getHeight()));
   }

   return d_tempDIB.get();
}
#endif

/*
 * Draw the static part of the display
 */

void
BattleMapDisplayImp::drawStatic(void) {

#ifdef DEBUG
   bdLog.printf(">drawStatic(%d,%d,%d,%d)", d_pixelArea.left(), d_pixelArea.top(), d_pixelArea.width(), d_pixelArea.height());
#endif

   d_staticRedraw = false;

   if(!d_staticDIB.get() || (d_staticDIB->getWidth() != d_pixelArea.width()) || (d_staticDIB->getHeight() != d_pixelArea.height()) )
   {

      // auto_ptr will automatically delete any old one
      // auto_ptr<DrawDIBDC> newDib(new DrawDIBDC(d_pixelArea.width(), d_pixelArea.height()));
      // d_staticDIB = newDib;
      // d_staticDIB = auto_ptr<DrawDIBDC>(new DrawDIBDC(d_pixelArea.width(), d_pixelArea.height()));
#ifdef _MSC_VER   // Not sure what standard says about this... which is correct way?
      d_staticDIB = std::auto_ptr<DrawDIBDC>(new DrawDIBDC(d_pixelArea.width(), d_pixelArea.height()));
#else
      d_staticDIB.reset(new DrawDIBDC(d_pixelArea.width(), d_pixelArea.height()));
#endif
      d_staticDIB->setTextAlign(TA_CENTER + TA_BOTTOM);
      d_staticDIB->setBkMode(OPAQUE);
      d_staticDIB->setTextColor(PALETTERGB(0,0,0));
      d_staticDIB->setBkColor(PALETTERGB(255,255,255));

      // create new hill-displayer
      //          if(!d_hillDisplay) d_hillDisplay = new HillDisplay(d_staticDIB.get(), d_batData);


#ifdef USE_ZBUFFER
      d_zBuffer.setSize(d_pixelArea.width(), d_pixelArea.height());
#endif

      if(d_terrainColors)
         d_terrainColors->reset(d_staticDIB.get());
      else
         d_terrainColors = new TerrainColors(d_staticDIB.get(), s_terrainLibrary->get());
   }

   ASSERT(d_terrainColors);


#ifdef USE_ZBUFFER
   d_zBuffer.clear();
#endif
   // d_pathMap.clear();
   // d_forestDisplay.clear();


   ASSERT(d_staticDIB.get());

#ifdef DEBUG    // should be unneccessary when finished
   // d_staticDIB->fill(d_staticDIB->getColour(PALETTERGB(64,255,128)));
   d_staticDIB->fill(d_staticDIB->getColour(PALETTERGB(255,128,255)));
#endif

   switch(d_mode)
   {
   case BattleMapInfo::OneHex:
   case BattleMapInfo::OneMile:
   case BattleMapInfo::TwoMile:
   case BattleMapInfo::FourMile:
      // drawStaticOverview();
      drawStaticDetail();
      break;
   case BattleMapInfo::Strategic:
      drawStaticOverview();
      break;
   case BattleMapInfo::Tiny:
      // drawStaticOverview();
      drawStaticTiny(false);
      break;
#ifdef DEBUG
   default:
      FORCEASSERT("Unknown map mode");
#endif
   }

   /*
   * Save some memory by clearing out the pathMap, since we don't need
   * it again until the static map is redrawn.
   */

   // d_pathMap.clear();
   // d_forestDisplay.clear();

#ifdef DEBUG
   bdLog.printf("<drawStatic");
#endif
}

void BattleMapDisplayImp::drawStaticOverview()
{
   // int pixelWidth = MulDiv(d_pixelArea.width(), BattleMeasure::QuantaPerXHex, d_size.x());
   int pixelWidth = MulDiv(d_pixelArea.width(), BattleMeasure::OneXHex, d_size.x());
   if(pixelWidth >= d_MinimumDetailViewPixelWidth)
      drawStaticDetail();
   else
      drawStaticTiny(true);
}

void BattleMapDisplayImp::drawStaticTiny(bool AntiAlias)
{
   // HexTerrainInfo hexInfo(d_batData);
   const TerrainTable& tTable = d_batData->terrainTable();

   for(HexIterator iter(this); iter(); ++iter)
   {
      const BattleTerrainHex& hexInfo = d_batData->getTerrain(iter.hex());
      const TerrainTable::Info& tInfo = tTable[hexInfo.d_terrainType];
      Rect<LONG> rect = iter.squareRect();

      // ColourIndex color = tInfo.color();
      ASSERT(d_terrainColors);
      ColorIndex color = d_terrainColors->getColor(tInfo.baseGraphic());

      // Overide colour with path if present

      const PathInfo* pathInfo = &hexInfo.d_path1;
      if(pathInfo->d_type == PathTable::None)
         pathInfo = &hexInfo.d_path2;
      if(pathInfo->d_type != PathTable::None)
      {
         const PathTable::Info& info = tTable.path(pathInfo->d_type);
         // color = pathInfo.color();
         color = d_terrainColors->getColor(info.graphic(pathInfo->d_edges));
         // Note: above could just get the base graphic
         //       but it may look nicer if each edgetype has
         //       its own subtle color
      }

      // Adjust color based on height...
      // Draw Coloured rectangle

      d_staticDIB->rect(rect.left(), rect.top(), rect.width(), rect.height(), color);

   }


   /*
   If flag set, go through & smooth out colours (not real antialiasing, but a sorta gaussian filter)
   */


   // turn of smoothing in debug, since this is very annoying !
#ifdef DEBUG
   AntiAlias = false;
#endif

   if(AntiAlias) {

#ifdef DEBUG
      int total_averaged = 0;
#endif

      int x, y;
      int width = d_staticDIB->getWidth();
      int height = d_staticDIB->getHeight();
      DrawDIB d_bufferDIB(width, height);
      d_bufferDIB.blitFrom(&(*d_staticDIB));


         HPALETTE pal = Palette::get();
      PALETTEENTRY pal_entry_0;
      PALETTEENTRY pal_entry_1;
      PALETTEENTRY pal_entry_2;
      PALETTEENTRY pal_entry_3;
      PALETTEENTRY pal_entry_4;

      const UBYTE * col_index_0;
      const UBYTE * col_index_1;
      const UBYTE * col_index_2;
      const UBYTE * col_index_3;
      const UBYTE * col_index_4;

      float total_red;
      float total_green;
      float total_blue;

      long average_red;
      long average_green;
      long average_blue;

      static const float recip = static_cast<float>(1.0 / 5.0);

      // allow for filter overlap
      int xstep = 4;
      int ystep = 4;

      int halfxstep = xstep/2;
      int halfystep = ystep/2;

         width-=xstep;
      height-=ystep;

      for(y=ystep; y<height; y+=ystep) {

         for(x=xstep; x<width; x+=xstep) {

            col_index_0 = d_staticDIB->getAddress(x, y);
            col_index_1 = d_staticDIB->getAddress(x-xstep, y);
            col_index_2 = d_staticDIB->getAddress(x+xstep, y);
            col_index_3 = d_staticDIB->getAddress(x, y-ystep);
            col_index_4 = d_staticDIB->getAddress(x, y+ystep);

            if(*col_index_0 != *col_index_1 != *col_index_2 != *col_index_3 != *col_index_4) {

               GetPaletteEntries(pal, *col_index_0, 1, &pal_entry_0);
               GetPaletteEntries(pal, *col_index_1, 1, &pal_entry_1);
               GetPaletteEntries(pal, *col_index_2, 1, &pal_entry_2);
               GetPaletteEntries(pal, *col_index_3, 1, &pal_entry_3);
               GetPaletteEntries(pal, *col_index_4, 1, &pal_entry_4);

               total_red = pal_entry_0.peRed + pal_entry_1.peRed + pal_entry_2.peRed + pal_entry_3.peRed + pal_entry_4.peRed;
               total_green = pal_entry_0.peGreen + pal_entry_1.peGreen + pal_entry_2.peGreen + pal_entry_3.peGreen + pal_entry_4.peGreen;
               total_blue = pal_entry_0.peBlue + pal_entry_1.peBlue + pal_entry_2.peBlue + pal_entry_3.peBlue + pal_entry_4.peBlue;

               FloatToLong(&average_red, (total_red*recip) );
               FloatToLong(&average_green, (total_green*recip) );
               FloatToLong(&average_blue, (total_blue*recip) );

                  COLORREF newcol = RGB(
                  average_red,
                  average_green,
                  average_blue
                  );

                  ColourIndex col = d_bufferDIB.getColour(newcol);
               d_bufferDIB.rect(x-halfxstep, y-halfystep, xstep, ystep, col);

#ifdef DEBUG
               total_averaged++;
#endif
            }
         }
      }
      d_staticDIB->blitFrom(&d_bufferDIB);
   }
}




// unchecked
PixelPoint BattleMapDisplayImp::hexPixelSize() const
{
   HexIterator iter(this);
   ASSERT(iter());
   Rect<LONG> rect = iter.squareRect();

   return PixelPoint(rect.width(), rect.height());
}





/*-------------------------------------

  drawStaticDetail(void)

  Draws the battlefield background.
  ie. the non-dynamic part

-------------------------------------*/


void
BattleMapDisplayImp::drawStaticDetail(void) {


   ASSERT(s_terrainLibrary->get() != 0);

   const TerrainTable& tTable = d_batData->terrainTable();

   HexIterator iter(this);

   /*
   * Pass 1 to fill background with basic Hex, excluding water
   */

   if(d_bDrawTerrain) {

      for(HexIterator iter(this); iter(); ++iter) {

         // HexTerrainInfo hexInfo(d_batData, iter.hex());
         const BattleTerrainHex& hexInfo = d_batData->getTerrain(iter.hex());
         const TerrainTable::Info& tInfo = tTable[hexInfo.d_terrainType];
         Rect<LONG> rect = iter.squareRect();

         /*
         * Put terrain graphic
         */

         ASSERT(s_terrainLibrary->get() != 0);

         // Get Graphic Index
         SpriteLibrary::Index gIndex = tInfo.baseGraphic();
         ASSERT(gIndex != SpriteLibrary::NoGraphic);

         // Get Graphic into memory
         SWG_Sprite::SpriteBlockPtr sprite(s_terrainLibrary->get(), gIndex);

         // Copy graphic to DIB as a texture
         ASSERT(sprite->getWidth() != 0);
         ASSERT(sprite->getHeight() != 0);

         LONG srcX = rect.left();        // % sprite->getWidth();
         LONG srcY = rect.top();         // % sprite->getHeight();

         d_staticDIB->rect(rect.left(), rect.top(), rect.width(), rect.height(), sprite, srcX, srcY);

      }
   }


   /*
   * Draw Overlap "splodges"
   *
   * For each hex
   * If neighbouring hex is not the same
   * Put a splodge over the border.
   *
   * Special cases:
   * Water does not cause splodges, this is handled seperately as coasts
   */


   if(d_bDrawOverlaps) {

      for(iter = HexIterator(this); iter(); ++iter) {

         const BattleTerrainHex& hexInfo = d_batData->getTerrain(iter.hex());
         const TerrainTable::Info& tInfo = tTable[hexInfo.d_terrainType];
         const LTerrainTable::Info& lInfo = tTable.logical(hexInfo.d_terrainType);

         // Could speed this up, by pre-calculating edges that need splodges
         if(!lInfo.d_isWater) {

            for(HexCord::HexDirection d = HexCord::First; d < HexCord::End; INCREMENT(d)) {

               HexCord edgeHex;
               if(d_batData->moveHex(iter.hex(), d, edgeHex)) {

                  const BattleTerrainHex& edgeInfo = d_batData->getTerrain(edgeHex);
                  const LTerrainTable::Info& tEdgeInfo = tTable.logical(edgeInfo.d_terrainType);

                  if(!tEdgeInfo.d_isWater) {

                     if((edgeInfo.d_terrainType != hexInfo.d_terrainType) || (edgeInfo.d_height != hexInfo.d_height)) {

                        struct OverLapPosition {
                           int d_w; // Proportion of total width
                           int d_h; // Proportion of total height
                           int d_dx;   // direction to reduce pixels
                           int d_dy;
                           int d_x; // 1/2 of width
                           int d_y; // 1/2 of height
                        };

                        static OverLapPosition positions[HexCord::End-HexCord::First] = {
                           { 4, 1,  1,  0, 2, 0 }, // Right
                           { 2, 4,  0, -1, 1, 0 }, // UpRight,
                           { 2, 4,  0, -1, 0, 0 }, // UpLeft,
                           { 4, 1, -1,  0, 0, 0 }, // Left,
                           { 2, 4,  0,  1, 0, 2 }, // DownLeft,
                           { 2, 4,  0,  1, 1, 2 }  // DownRight,
                        };

                        const OverLapPosition& pos = positions[d];

                        Rect<LONG> rect = iter.squareRect();

                        // I've replaced all "/2" by ">>1"
                        LONG x = rect.left() + ((rect.width() * pos.d_x) >>1);
                        LONG y = rect.top() + ((rect.height() * pos.d_y) >>1);
                        LONG w = (rect.width() + (pos.d_w >>1)) / pos.d_w;
                        LONG h = (rect.height() + (pos.d_h >>1)) / pos.d_h;

                        if(pos.d_dx < 0) x -= w;
                        if(pos.d_dy < 0) y -= h;

                        SpriteLibrary::Index gIndex = tInfo.baseGraphic();
                        ASSERT(gIndex != SpriteLibrary::NoGraphic);

                        SWG_Sprite::SpriteBlockPtr sprite(s_terrainLibrary->get(), gIndex);

                        // Copy sprite to buffer
                        // Create and apply mask using dx,dy
                        MaskDIBKey key(pos.d_dx, pos.d_dy, w, h);
                        const DrawDIB* maskDIB = d_overlaps.get(key);
                        ASSERT(maskDIB != 0);
                        ASSERT(maskDIB->getWidth() == w);
                        ASSERT(maskDIB->getHeight() == h);

                        // Get temporary DIB
                        DrawDIB* bufDIB = tempDIB(w, h);

                        // Copy texture to tempdib
                        bufDIB->rect(0, 0, w, h, sprite, x, y);


                        // apply mask to tempdib
                        bufDIB->blit(maskDIB, 0, 0);

                        // set mask colour of tempdib
                        bufDIB->setMaskColour(MaskDIB::NonMaskColor);

                        // blit to screen
                        d_staticDIB->blit(x, y, w, h, bufDIB, 0, 0);

                        // d_staticDIB->rect(x, y, w, h, sprite, x, y);
                        // d_staticDIB->blit(maskDIB, x, y);

#ifdef DEBUG
                        if(d_hexDebug) {
                           ColourIndex Purple = d_staticDIB->getColour(PALETTERGB(255,0,255));
                           d_staticDIB->frame(x,y,w,h, Purple);
                        }
#endif

                     } // if((edgeInfo.d_terrainType != hexInfo.d_terrainType) || (edgeInfo.d_height != hexInfo.d_height))

                  } // if(!tEdgeInfo.d_isWater)

               } // if(d_batData->moveHex(iter.hex(), d, edgeHex))

            } // for(HexCord::HexDirection d = HexCord::First; d < HexCord::End; INCREMENT(d))

         } // if(!lInfo.d_isWater)

      } // for(iter = HexIterator(this); iter(); ++iter)

   }



   /*
   * Draw Polys which are missing off top & bottom tips of the hex, in hex's terrain texture
   */


   if(d_bDrawPolys) {

      for(iter = HexIterator(this); iter(); ++iter) {

         const BattleTerrainHex& hexInfo = d_batData->getTerrain(iter.hex());
         const TerrainTable::Info& tInfo = tTable[hexInfo.d_terrainType];
         const LTerrainTable::Info& lInfo = tTable.logical(hexInfo.d_terrainType);

         // Could speed this up, by pre-calculating edges that need splodges
         if(!lInfo.d_isWater) {
            /*
            PixelPoint top_poly[3];
            PixelPoint bottom_poly[3];

            // top-middle point
            top_poly[0].x(iter.midX());
            top_poly[0].y(iter.top());
            // top-left point
            top_poly[1].x((iter.left() + iter.midX()) >>1);
            top_poly[1].y((iter.top() + iter.topCorner()) >>1);
            // top-right point
            top_poly[2].x((iter.right() + iter.midX()) >>1);
            top_poly[2].y((iter.top() + iter.topCorner()) >>1);

            // bottom-middle point
            bottom_poly[0].x(iter.midX());
            bottom_poly[0].y(iter.bottom());
            // bottom-left point
            bottom_poly[1].x((iter.left() + iter.midX()) >>1);
            bottom_poly[1].y((iter.bottom() + iter.lowCorner()) >> 1);
            // bottom-right point
            bottom_poly[2].x((iter.right() + iter.midX()) >>1);
            bottom_poly[2].y((iter.bottom() + iter.lowCorner()) >> 1);

            DIB_Utility::drawPoly(d_staticDIB.get(), top_poly, 3, d_staticDIB->getColour(RGB(255,0,0)) );
            DIB_Utility::drawPoly(d_staticDIB.get(), bottom_poly, 3, d_staticDIB->getColour(RGB(0,255,0)) );
            */
         }
      }
   }


   /*
   * Pass to draw pathways
   */

   if(d_bDrawPathways) {

      for(iter = HexIterator(this); iter(); ++iter) {

         const BattleTerrainHex& tHex = d_batData->getTerrain(iter.hex());

         if(tHex.d_path1.d_type != PathTable::None) {

            drawPath(iter, tHex.d_path1, tTable);
            // d_staticDIB->line(iter.left(), iter.top(), iter.right(), iter.bottom(), d_staticDIB->getColour(PALETTERGB(255,0,255)));
         }
         if(tHex.d_path2.d_type != PathTable::None) {

            drawPath(iter, tHex.d_path2, tTable);
            // d_staticDIB->line(iter.left(), iter.bottom(), iter.right(), iter.top(), d_staticDIB->getColour(PALETTERGB(255,0,255)));
         }
      }
   }


   /*
   * Draw Coastlines
   */

   if(d_bDrawCoasts) {

      for(iter = HexIterator(this); iter(); ++iter) {

         const BattleTerrainHex& tHex = d_batData->getTerrain(iter.hex());

         if(tHex.d_coast != 0) {

            SpriteLibrary::Index gIndex = tTable.coastGraphic(tHex.d_coast);
            ASSERT(gIndex != SpriteLibrary::NoGraphic);
            drawSquashedGraphic(iter, gIndex);

            /*
            * Draw Estuaries if needed
            */

            drawEstuary(iter, tHex.d_path1, tHex.d_coast, tTable);
            drawEstuary(iter, tHex.d_path2, tHex.d_coast, tTable);
         }
      }
   }


   /*
   * Draw Height shading overlaps
   */

   if(d_bDrawHills) {

      if(!d_hillDisplay) d_hillDisplay = new HillDisplay(d_staticDIB.get(), d_batData);
      else d_hillDisplay->Init(d_staticDIB.get());

      int total_shaded = 0;

      for(iter = HexIterator(this); iter(); ++iter) {

         total_shaded += d_hillDisplay->ShadeHeights(iter);
      }

#ifdef DEBUG

      bdLog.printf("Shaded %i hex heights.", total_shaded);

#endif

   }


   /*
   * Pass to draw Hedges/Walls
   */

   if(d_bDrawEdges) {

      for(iter = HexIterator(this); iter(); ++iter) {

         const BattleTerrainHex& tHex = d_batData->getTerrain(iter.hex());

         if(tHex.d_edge.d_type != EdgeTable::None) {

            ASSERT(tHex.d_edge.d_edges != 0);
            const EdgeTable::Info& edgeInfo = tTable.edge(tHex.d_edge.d_type);
            SpriteLibrary::Index gIndex = edgeInfo.graphic() + tHex.d_edge.d_edges - 1;
            drawSquashedGraphic(iter, gIndex);
         }
      }
   }

   /*
   * Pass to draw hexes on top of background
   */

   if(d_bDrawHexOutlines) {

      ColourIndex black = d_staticDIB->getColour(PALETTERGB(0,0,0));

      for(iter = HexIterator(this); iter(); ++iter) {

         ASSERT(iter.width() >= 4);
         iter.drawHexOutline(d_staticDIB.get(), black);
      }
   }



   /*
   * Pass to draw darkened area (this has to be tidied up later...)
   */

   if(d_ShowDarkenedArea) {

      int toppixel;
      int bottompixel;
      int lowhex;
      int highhex;

      iter = HexIterator(this);
      lowhex = iter.hex().y();
      bottompixel = iter.bottom();

      while(iter() ) {
         highhex = iter.hex().y();
         ++iter;
         toppixel = iter.top();
      }

      if(!(d_DarkenedAreaLowHex < lowhex && d_DarkenedAreaHighHex > highhex)) {
         // find pixel coords for darkened area
         for(iter = HexIterator(this); iter(); ++iter) {
            int iter_y = iter.hex().y();
            if(iter_y == d_DarkenedAreaLowHex) bottompixel = iter.bottom();
            if(iter_y == d_DarkenedAreaHighHex) toppixel = iter.top();
         }
      }

      int width = d_staticDIB.get()->getWidth();
      d_staticDIB.get()->darkenRectangle(0, toppixel, width, bottompixel, 30);
   }









#ifdef DEBUG

   if(hexDebug()) {

      int pixelWidth = MulDiv(d_pixelArea.width(), BattleMeasure::OneXHex, d_size.x());
      if(pixelWidth >= 16) {

         for(iter = HexIterator(this); iter(); ++iter) {

            const BattleTerrainHex& tHex = d_batData->getTerrain(iter.hex());
            BattleTerrainHex::Height height = tHex.d_height;
            wTextPrintf(d_staticDIB->getDC(), iter.midX(),iter.midY(), "%d", static_cast<int>(height));
         }
      }
   }

#endif




}














/*
 * Draw the dynamic part of the display
 */

void BattleMapDisplayImp::drawMain()
{
#ifdef DEBUG
   // bdLog.printf(">drawDynamic(%d,%d)", d_pixelSize.getX(), d_pixelSize.getY());
   bdLog << ">drawDynamic(" << d_pixelArea.width() << ',' << d_pixelArea.height() << ") " << sysTime << endl;
#endif

   d_redraw = false;

   if(!d_mainDIB.get() || (d_mainDIB->getWidth() != d_pixelArea.width()) || (d_mainDIB->getHeight() != d_pixelArea.height()) )
   {
      // auto_ptr will automatically delete any old one

      // auto_ptr<DrawDIBDC> newDib(new DrawDIBDC(d_pixelArea.width(), d_pixelArea.height()));
      // d_mainDIB = newDib;
#ifdef _MSC_VER
      d_mainDIB = std::auto_ptr<DrawDIBDC>(new DrawDIBDC(d_pixelArea.width(), d_pixelArea.height()));
#else
      d_mainDIB.reset(new DrawDIBDC(d_pixelArea.width(), d_pixelArea.height()));
#endif
   }

   ASSERT(d_mainDIB.get() != 0);
   ASSERT(d_staticDIB.get() != 0);

   d_mainDIB->blitFrom(d_staticDIB.get());


   /*
   * Draw Units
   */


   int max_width = 0;
   int max_height = 0;

   for(HexIterator iter = HexIterator(this); iter(); ++iter) {
      if(iter.width() > max_width) max_width = iter.width();
      if(iter.height() > max_height) max_height = iter.height();
   }

   int dib_w = max_width+8;
   int dib_h = max_height+8;

   /*
   * recreate highlight hexes if necessary
   */
   createHighlightDIBs(dib_w, dib_h, max_width, max_height, iter);

   /*
   * highlight hexes
   */

   for(iter = HexIterator(this); iter(); ++iter)
   {

      /*
      * highlight a hex in side's colours
      */
      const BattleTerrainHex& tHex = d_batData->getTerrain(iter.hex());
      if(tHex.d_highlightFlags & HIGHLIGHT_SET) {

         // draw allied highlights
         if(tHex.d_highlightFlags & 1) {

            if(tHex.d_highlightFlags & HIGHLIGHT_DARKENED) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  iter.width(), iter.height(),
                  d_alliedDarkenedHighlightDIB,
                  0,
                  0
                  );
            }
            else if(tHex.d_highlightFlags & HIGHLIGHT_DARKEST) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  iter.width(), iter.height(),
                  d_alliedDarkestHighlightDIB,
                  0,
                  0
                  );
            }
            else if(tHex.d_highlightFlags & HIGHLIGHT_OUTLINE) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  iter.width(), iter.height(),
                  d_alliedNormalOutlineHighlightDIB,
                  0,
                  0
                  );
            }
            else {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  iter.width(), iter.height(),
                  d_alliedNormalHighlightDIB,
                  0,
                  0
                  );
            }
         }

         // draw french highlights
         else {

            if(tHex.d_highlightFlags & HIGHLIGHT_DARKENED) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  iter.width(), iter.height(),
                  d_frenchDarkenedHighlightDIB,
                  0,
                  0
                  );
            }
            else if(tHex.d_highlightFlags & HIGHLIGHT_DARKEST) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  iter.width(), iter.height(),
                  d_frenchDarkestHighlightDIB,
                  0,
                  0
                  );
            }
            else if(tHex.d_highlightFlags & HIGHLIGHT_OUTLINE) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  iter.width(), iter.height(),
                  d_frenchNormalOutlineHighlightDIB,
                  0,
                  0
                  );
            }
            else {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  iter.width(), iter.height(),
                  d_frenchNormalHighlightDIB,
                  0,
                  0
                  );
            }
         }
      }

     /*
     * calculate animation speed / frame-update
     */
     static int frame_millisecs = 100;
     BattleMeasure::BattleTime::Tick new_tick = d_batData->getTick();
     // Is game is moving forwards ?
     if(new_tick != BattleDisplay::LastTick) {

        BattleDisplay::DeltaTicks = new_tick - BattleDisplay::LastTick;
        BattleDisplay::LastTick = new_tick;

        BattleDisplay::NewTime = Greenius_System::MMTimer::getTime();

        unsigned int DeltaTime = BattleDisplay::NewTime - BattleDisplay::LastTime;
        if(DeltaTime >= frame_millisecs) {
           BattleDisplay::FrameCounter += (DeltaTime / frame_millisecs);
           BattleDisplay::LastTime = BattleDisplay::NewTime;
        }
     }

      /*
      * draw building cluster
      */
      if(d_bDrawBuildings) BuildingDisplay::drawBuildings(&d_zlist, iter, d_batData, s_buildingLibrary->get());

      /*
      * draw units
      */
      if(d_bDrawTroops) BattleDisplay::drawUnits(&d_zlist, iter, d_batData, s_troopLibrary->get());

      /*
      * draw effects (explosions, dead bodies, etc)
      */
      if(d_bDrawEffects) BattleDisplay::drawEffects(&d_zlist, iter, d_batData, s_troopLibrary->get());
   }

   d_zlist.drawAndClear(d_mainDIB.get());




   /*
   * draw hex labels & victory points
   */

   if(max_width >= 16) {

      /*
      Rescale fonts (if necessary)
      */
      SetMapFonts((max_height/4)*3);
      int shadow_offset = (max_height / 16) +1;
      Fonts font;

      d_mainDIB.get()->setTextAlign(TA_CENTER + TA_BOTTOM);
      d_mainDIB.get()->setBkMode(TRANSPARENT);

      for(iter = HexIterator(this); iter(); ++iter) {

         if(d_ShowHexLabels && Options::get(OPT_Labels)) {
            const HexCord & hex = iter.hex();
            const BattleTerrainHex & maphex = d_batData->map()->get(hex);

            if(maphex.d_label) {

               // drop shadow
               d_mainDIB.get()->setTextColor(PALETTERGB(0,0,0));
               font.set(d_mainDIB.get()->getDC(), d_captionLogFont);
               wTextPrintf(d_mainDIB.get()->getDC(), iter.midX()+shadow_offset,iter.topCorner()+shadow_offset, "%s", maphex.d_label);

               // text
               d_mainDIB.get()->setTextColor(PALETTERGB(128,128,128));
               font.set(d_mainDIB.get()->getDC(), d_captionLogFont);
               wTextPrintf(d_mainDIB.get()->getDC(), iter.midX(),iter.topCorner(), "%s", maphex.d_label);
            }
         }

         if(d_ShowVictoryPoints) {
            const HexCord & hex = iter.hex();
            const BattleTerrainHex & maphex = d_batData->map()->get(hex);

            if(maphex.d_victoryPoints > 0) {

               // drop shadow
               d_mainDIB.get()->setTextColor(PALETTERGB(0,0,0));
               font.set(d_mainDIB.get()->getDC(), d_pointsLogFont);
               wTextPrintf(d_mainDIB.get()->getDC(), iter.midX()+shadow_offset,iter.lowCorner()+shadow_offset, "%d", maphex.d_victoryPoints);

               // French
               if(maphex.d_victorySide == 0) {
                  d_mainDIB.get()->setTextColor(scenario->getSideColour(0) );
               }
               // Allied
               else if(maphex.d_victorySide == 1){
                  d_mainDIB.get()->setTextColor(scenario->getSideColour(1) );
               }
               // Neutral
               else {
                  d_mainDIB.get()->setTextColor(PALETTERGB(255,255,255));
               }

               // text
               font.set(d_mainDIB.get()->getDC(), d_pointsLogFont);
               wTextPrintf(d_mainDIB.get()->getDC(), iter.midX(),iter.lowCorner(), "%d", maphex.d_victoryPoints);
            }
         }
      }

   } // end of hex labels & victory points




   /*
   * draw selection area
   */


   if(d_ShowSelection) {

      HexCord topright = HexCord(
         d_SelectionBottomLeft.x() + d_SelectionSize.x(),
         d_SelectionBottomLeft.y() + d_SelectionSize.y()
      );

      for(iter = HexIterator(this); iter(); ++iter) {

         int iter_x = iter.hex().x();
         int iter_y = iter.hex().y();

         // if within X extents, do top & bottom
         if( (iter_x >= d_SelectionBottomLeft.x() ) && (iter_x <= topright.x() ) ) {

            // top line
            if(iter_y == topright.y() ) {
               d_mainDIB.get()->line(iter.left(), iter.topCorner(), iter.midX(), iter.top(), d_SelectionColour);
               d_mainDIB.get()->line(iter.midX(), iter.top(), iter.right(), iter.topCorner(), d_SelectionColour);
            }
            // bottom line
            if(iter_y == d_SelectionBottomLeft.y() ) {
               d_mainDIB.get()->line(iter.left(), iter.lowCorner(), iter.midX(), iter.bottom(), d_SelectionColour);
               d_mainDIB.get()->line(iter.midX(), iter.bottom(), iter.right(), iter.lowCorner(), d_SelectionColour);
            }
         }

         // if within Y extents, do left & right
         if( (iter_y >= d_SelectionBottomLeft.y() ) && (iter_y <= topright.y() ) ) {

            // left side (offset every other row)
            if(iter_x == d_SelectionBottomLeft.x() ) {
               if(iter_y & 1) {
                  d_mainDIB.get()->line(iter.left(), iter.lowCorner(), iter.left(), iter.topCorner(), d_SelectionColour);
               }
               else {
                  d_mainDIB.get()->line(iter.midX(), iter.bottom(), iter.left(), iter.lowCorner(), d_SelectionColour);
                  d_mainDIB.get()->line(iter.left(), iter.lowCorner(), iter.left(), iter.topCorner(), d_SelectionColour);
                  d_mainDIB.get()->line(iter.left(), iter.topCorner(), iter.midX(), iter.top(), d_SelectionColour);
               }
            }

            // right side (offset every other row)
            if(iter_x == topright.x() ) {
               if(iter_y & 1) {
                  d_mainDIB.get()->line(iter.midX(), iter.top(), iter.right(), iter.topCorner(), d_SelectionColour);
                  d_mainDIB.get()->line(iter.right(), iter.topCorner(), iter.right(), iter.lowCorner(), d_SelectionColour);
                  d_mainDIB.get()->line(iter.right(), iter.lowCorner(), iter.midX(), iter.bottom(), d_SelectionColour);
               }
               else {
                  d_mainDIB.get()->line(iter.right(), iter.topCorner(), iter.right(), iter.lowCorner(), d_SelectionColour);
               }
            }
         }

      } // do for all hexes

   } // end of drawing selection area




   /*
     If we need to resize, then do so
     */

   createStippleDIBs(dib_w, dib_h, max_width, max_height, iter);


   /*
   Shade areas of the map which aren't visible
   */

   if(d_showHexVisibility) {


      for(iter = HexIterator(this); iter(); ++iter) {

         const HexCord & hex = iter.hex();
         const BattleTerrainHex & maphex = d_batData->map()->get(hex);

         // if hex is not visible, then shade
         if(maphex.d_visibility != Visibility::Full) {

            if(maphex.d_visibility <= Visibility::NotSeen) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  max_width, max_height,
                  //d_stippleDIBNotSeen->getWidth(),
                  //d_stippleDIBNotSeen->getHeight(),
                  d_stippleDIBNotSeen,
                  0,
                  0
                  );
            }
            else if(maphex.d_visibility == Visibility::Poor) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  max_width, max_height,
                  //d_stippleDIBPoor->getWidth(),
                  //d_stippleDIBPoor->getHeight(),
                  d_stippleDIBPoor,
                  0,
                  0
                  );
            }
            else if(maphex.d_visibility == Visibility::Fair) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  max_width, max_height,
                  //d_stippleDIBFair->getWidth(),
                  //d_stippleDIBFair->getHeight(),
                  d_stippleDIBFair,
                  0,
                  0
                  );
            }
            else if(maphex.d_visibility == Visibility::Good) {

               d_mainDIB.get()->blit(
                  iter.left(),
                  iter.top(),
                  max_width, max_height,
                  //d_stippleDIBGood->getWidth(),
                  //d_stippleDIBGood->getHeight(),
                  d_stippleDIBGood,
                  0,
                  0
                  );
            }

         } // if !Visibility::Full
      }

   } // if d_showHexVisibility


   d_lastMaxWidth = max_width;
   d_lastMaxHeight = max_height;





#ifdef DEBUG
   bdLog << "<drawDynamic, " << sysTime << endl;
#endif
}

void BattleMapDisplayImp::drawPath(const HexIterator& iter, const PathInfo& path, const TerrainTable& terrainTable)
{
   ASSERT(path.d_edges != 0);

   // this line is to stop bridges being drawn as paths (they are now drawn in the Zlist as buildings)
   if(path.d_type == LP_TrackBridge || path.d_type == LP_RoadBridge) return;

   // How big the graphics were originally drawn
   const PathTable::Info& pathInfo = terrainTable.path(path.d_type);
   // SpriteLibrary::Index gIndex = pathInfo.graphic() + path.d_edges - 1;
   SpriteLibrary::Index gIndex = pathInfo.graphic(path.d_edges);
   drawSquashedGraphic(iter, gIndex);
}

/*
 * Draw Estuaries for each edge that path.edge matches with edges
 * if path.type is a river or stream
 */

void BattleMapDisplayImp::drawEstuary(const HexIterator& iter, const PathInfo& path, EdgeMap edges, const TerrainTable& tTable)
{
   // Determine if path is a river/stream

   if(path.d_type != PathTable::None)
   {
      const PathTable::Info& pathInfo = tTable.path(path.d_type);
      if(pathInfo.basicType() == PathTable::River)
      {
         // Determine if edges match up

         if( edgesOverlap(edges, path.d_edges))
         {
            // For each matching edge

            for(HexCord::HexDirection d = HexCord::First; d < HexCord::End; INCREMENT(d))
            {
               UBYTE mask = 1 << d;

               if(mask & edges & path.d_edges)
               {
                  // Display estuary graphic

                  SpriteLibrary::Index gIndex = tTable.estuaryGraphic(d);
                  drawSquashedGraphic(iter, gIndex);
               }
            }


         }
      }
   }
}

void BattleMapDisplayImp::drawSquashedGraphic(const HexIterator& iter, SpriteLibrary::Index gIndex) const
{
   SWG_Sprite::SpriteBlockPtr sprite(s_terrainLibrary->get(), gIndex);

#if 0
      Rect<LONG> rect = iter.squareRect();
   // PixelRect has left/bottom outside of rectangle, but Rect<> is inclusive
   PixelRect pRect(rect.left(),rect.top(), rect.right()+1, rect.bottom()+1);

   PixelPoint fullSize = sprite->fullSize();
   PixelPoint offset(fullSize.x() >> 1, fullSize.y() >> 1);
   BattleDisplayUtility::drawSquashedGraphic(sprite, d_staticDIB.get(), pRect, offset);
#endif

   // Need to fiddle with the destination because the hotspot is in the middle of the hex


   BattleDisplayUtility::drawSquashedGraphic(sprite, d_staticDIB.get(), PixelPoint(iter.midX(), iter.midY()), iter.scale());
}


/*
 * Draw info into DIBs
 */

bool
BattleMapDisplayImp::draw() {

   bool redrawn = false;
   // int width = r.right - r.left;
   // int height = r.bottom - r.top;

   d_lock.startWrite();

   if(d_staticRedraw || !d_staticDIB.get() || (d_staticDIB->getWidth() != d_pixelArea.width()) || (d_staticDIB->getHeight() != d_pixelArea.height()) ) {

      if(d_bDisplayTimes) d_staticTime = timeGetTime();

      drawStatic();
      d_redraw = true;

      if(d_bDisplayTimes) d_staticTime = (timeGetTime() - d_staticTime);

   }


   if(d_redraw || !d_mainDIB.get() || (d_mainDIB->getWidth() != d_pixelArea.width()) || (d_mainDIB->getHeight() != d_pixelArea.height()) ) {

      if(d_bDisplayTimes) d_dynamicTime = timeGetTime();

      drawMain();
      redrawn = true;

      if(d_bDisplayTimes) d_dynamicTime = (timeGetTime() - d_dynamicTime);
   }

   d_lock.endWrite();


   return redrawn;
}




void
BattleMapDisplayImp::blit(PAINTSTRUCT * ps) {

   HDC hdc = ps->hdc;

   ASSERT(hdc != NULL);

   if(hdc != NULL) {

      d_lock.startWrite();

      /*
      If there's no main DIB, just fill with block col
      */
      if(!d_mainDIB.get()) {

         PixelRect r(d_pixelArea.left(), d_pixelArea.top(), d_pixelArea.right(), d_pixelArea.bottom());
         HBRUSH hBrush = static_cast<HBRUSH>(GetStockObject(GRAY_BRUSH));
         FillRect(hdc, &r, hBrush);
      }

      /*
      WindowsBlit the main DIB to the DC
      */
      else {
         /*
         BitBlt(
         hdc,
         ps->rcPaint.left,
         ps->rcPaint.top,
         ps->rcPaint.right - ps->rcPaint.left,
         ps->rcPaint.bottom - ps->rcPaint.top,
         d_mainDIB->getDC(),
         ps->rcPaint.left, // (top,left) dest coords
         ps->rcPaint.top,
         SRCCOPY
         );
         */

         BitBlt(
            hdc,
            d_pixelArea.left(),
            d_pixelArea.top(),
            d_pixelArea.width(),
            d_pixelArea.height(),
            d_mainDIB->getDC(),
            0,
            0,SRCCOPY
            );

      }

      /*
      Display some timing info
      */
      if(d_bDisplayTimes) {
         char tmp[256];
         sprintf(tmp, "static(ms): %i", d_staticTime);
         TextOut(hdc, 0, 16, tmp, strlen(tmp));
         sprintf(tmp, "dynamic(ms): %i", d_dynamicTime);
         TextOut(hdc, 0, 32, tmp, strlen(tmp));
      }

      d_lock.endWrite();
   }
}














void
BattleMapDisplayImp::createHighlightDIBs(unsigned int dib_w, unsigned int dib_h, unsigned int max_width, unsigned int max_height, HexIterator &iter) {

   if((dib_w > d_alliedNormalHighlightDIB->getWidth()) || (dib_h > d_alliedNormalHighlightDIB->getHeight())) {

#ifdef DEBUG
      bdLog.printf("recreating Highlight DIBs from (%i,%i) to (%i,%i)\n",
         d_alliedNormalHighlightDIB->getWidth(),
         d_alliedNormalHighlightDIB->getHeight(),
         dib_w, dib_h
         );
#endif

      delete d_alliedNormalHighlightDIB;
      delete d_frenchNormalHighlightDIB;
      delete d_alliedDarkenedHighlightDIB;
      delete d_frenchDarkenedHighlightDIB;
      delete d_alliedDarkestHighlightDIB;
      delete d_frenchDarkestHighlightDIB;
      delete d_alliedNormalOutlineHighlightDIB;
      delete d_frenchNormalOutlineHighlightDIB;

      d_alliedNormalHighlightDIB = new DrawDIB(dib_w,dib_h);
      d_frenchNormalHighlightDIB = new DrawDIB(dib_w,dib_h);
      d_alliedDarkenedHighlightDIB = new DrawDIB(dib_w,dib_h);
      d_frenchDarkenedHighlightDIB = new DrawDIB(dib_w,dib_h);
      d_alliedDarkestHighlightDIB = new DrawDIB(dib_w,dib_h);
      d_frenchDarkestHighlightDIB = new DrawDIB(dib_w,dib_h);
      d_alliedNormalOutlineHighlightDIB = new DrawDIB(dib_w,dib_h);
      d_frenchNormalOutlineHighlightDIB = new DrawDIB(dib_w,dib_h);

   }

   if((d_lastMaxWidth != max_width) || (d_lastMaxHeight != max_height)) {

      static ColourIndex maskCol = 0;
      static ColourIndex nonMaskCol = 1;

      // colour info for highlighting hexes
      COLORREF colref; BYTE r; BYTE g; BYTE b;
      ColourIndex side_col;
      ColourIndex side_col_darkened;
      ColourIndex side_col_darkest;

      // setup mask of hex
      int left = iter.left();
      int top = iter.top();
      int x,y;

      // setup poly
      PixelPoint polyPoints[6];
      polyPoints[0].x(iter.midX() - left);
      polyPoints[0].y(iter.top() - top);
      polyPoints[1].x(iter.right() - left);
      polyPoints[1].y(iter.topCorner() - top);
      polyPoints[2].x(iter.right() - left);
      polyPoints[2].y(iter.lowCorner() - top);
      polyPoints[3].x(iter.midX() - left);
      polyPoints[3].y((iter.bottom() - top) -1);
      polyPoints[4].x(iter.left() - left);
      polyPoints[4].y(iter.lowCorner() - top);
      polyPoints[5].x(iter.left() - left);
      polyPoints[5].y(iter.topCorner() - top);

      // setup inside poly for masking out center for outlined highlights
      PixelPoint insidePolyPoints[6];
      for(int i=0; i<6; i++) {
         float new_x = polyPoints[i].x() - (iter.midX() - left);
         new_x = new_x * 0.75f;
         float new_y = polyPoints[i].y() - (iter.midY() - top);
         new_y = new_y * 0.75f;

         insidePolyPoints[i].x( (int)new_x + (iter.midX() - left) );
         insidePolyPoints[i].y( (int)new_y + (iter.midY() - top) );
      }

      /*
       get side 0 colours
       */
      colref = scenario->getSideColour(Side(0));
      r = GetRValue(colref);
      g = GetGValue(colref);
      b = GetBValue(colref);
      // get colour indexes
      side_col = d_frenchNormalHighlightDIB->getColour(colref);
      side_col_darkened = d_frenchDarkenedHighlightDIB->getColour(RGB((r/4)*3,(g/4)*3,(b/4)*3));
      side_col_darkest = d_frenchDarkestHighlightDIB->getColour(RGB(r/2,g/2,b/2));

      // french Normal
      d_frenchNormalHighlightDIB->setMaskColour(maskCol);
      d_frenchNormalHighlightDIB->fill(maskCol);
      DIB_Utility::drawPoly(d_frenchNormalHighlightDIB, polyPoints, 6, side_col);
      // french Darkened
      d_frenchDarkenedHighlightDIB->setMaskColour(maskCol);
      d_frenchDarkenedHighlightDIB->fill(maskCol);
      DIB_Utility::drawPoly(d_frenchDarkenedHighlightDIB, polyPoints, 6, side_col_darkened);
      // french Darkest
      d_frenchDarkestHighlightDIB->setMaskColour(maskCol);
      d_frenchDarkestHighlightDIB->fill(maskCol);
      DIB_Utility::drawPoly(d_frenchDarkestHighlightDIB, polyPoints, 6, side_col_darkest);

      // setup stipple pattern
      for(y=1; y < iter.height(); y+=2) {
         for(x=1; x < iter.width(); x+=2) {
            d_frenchNormalHighlightDIB->plot(x,y,maskCol);
            d_frenchDarkenedHighlightDIB->plot(x,y,maskCol);
            d_frenchDarkestHighlightDIB->plot(x,y,maskCol);
         }
      }

      d_frenchNormalOutlineHighlightDIB->setMaskColour(maskCol);
      d_frenchNormalOutlineHighlightDIB->blitFrom(d_frenchNormalHighlightDIB);
      DIB_Utility::drawPoly(d_frenchNormalOutlineHighlightDIB, insidePolyPoints, 6, maskCol);


      /*
       get side 1 colours
       */
      colref = scenario->getSideColour(Side(1));
      r = GetRValue(colref);
      g = GetGValue(colref);
      b = GetBValue(colref);
      // get colour indexes
      side_col = d_alliedNormalHighlightDIB->getColour(colref);
      side_col_darkened = d_alliedDarkenedHighlightDIB->getColour(RGB((r/4)*3,(g/4)*3,(b/4)*3));
      side_col_darkest = d_alliedDarkestHighlightDIB->getColour(RGB(r/2,g/2,b/2));

      // allied Normal
      d_alliedNormalHighlightDIB->setMaskColour(maskCol);
      d_alliedNormalHighlightDIB->fill(maskCol);
      DIB_Utility::drawPoly(d_alliedNormalHighlightDIB, polyPoints, 6, side_col);
      // allied Darkened
      d_alliedDarkenedHighlightDIB->setMaskColour(maskCol);
      d_alliedDarkenedHighlightDIB->fill(maskCol);
      DIB_Utility::drawPoly(d_alliedDarkenedHighlightDIB, polyPoints, 6, side_col_darkened);
      // allied Darkest
      d_alliedDarkestHighlightDIB->setMaskColour(maskCol);
      d_alliedDarkestHighlightDIB->fill(maskCol);
      DIB_Utility::drawPoly(d_alliedDarkestHighlightDIB, polyPoints, 6, side_col_darkest);

      // setup stipple pattern
      for(y=1; y < max_height; y+=2) {
         for(x=1; x < max_width; x+=2) {
            d_alliedNormalHighlightDIB->plot(x,y,maskCol);
            d_alliedDarkenedHighlightDIB->plot(x,y,maskCol);
            d_alliedDarkestHighlightDIB->plot(x,y,maskCol);
         }
      }

      d_alliedNormalOutlineHighlightDIB->setMaskColour(maskCol);
      d_alliedNormalOutlineHighlightDIB->blitFrom(d_alliedNormalHighlightDIB);
      DIB_Utility::drawPoly(d_alliedNormalOutlineHighlightDIB, insidePolyPoints, 6, maskCol);
   }

}



void
BattleMapDisplayImp::createStippleDIBs(unsigned int dib_w, unsigned int dib_h, unsigned int max_width, unsigned int max_height, HexIterator &iter) {

   if((dib_w > d_stippleDIBNotSeen->getWidth()) || (dib_h > d_stippleDIBNotSeen->getHeight())) {

#ifdef DEBUG
      bdLog.printf("recreating Stipple DIBs from (%i,%i) to (%i,%i)\n",
         d_stippleDIBNotSeen->getWidth(),
         d_stippleDIBNotSeen->getHeight(),
         dib_w, dib_h
         );
#endif

      delete d_stippleDIBNotSeen;
      delete d_stippleDIBPoor;
      delete d_stippleDIBFair;
      delete d_stippleDIBGood;
      d_stippleDIBNotSeen = new DrawDIB(dib_w,dib_h);
      d_stippleDIBPoor = new DrawDIB(dib_w,dib_h);
      d_stippleDIBFair = new DrawDIB(dib_w,dib_h);
      d_stippleDIBGood = new DrawDIB(dib_w,dib_h);
   }

   if((d_lastMaxWidth != max_width) || (d_lastMaxHeight != max_height)) {

      static ColourIndex maskCol = 0;
      ColourIndex nonMaskCol = d_stippleDIBNotSeen->getColour(RGB(0,0,0));


      int x,y;


      // setup NotSeen stipple pattern
      d_stippleDIBNotSeen->setMaskColour(maskCol);
      d_stippleDIBNotSeen->fill(maskCol);
      int start=0;
      for(y=0; y < max_height; y++) {
         for(x=start; x < max_width; x+=2) {
            d_stippleDIBNotSeen->plot(x,y,nonMaskCol);
         }
         if(start==0) start=1;
         else start=0;
      }

      // setup Poor stipple pattern
      d_stippleDIBPoor->setMaskColour(maskCol);
      d_stippleDIBPoor->fill(maskCol);
      start=0;
      for(y=0; y < max_height; y++) {
         for(x=start; x < max_width; x+=3) {
            d_stippleDIBPoor->plot(x,y,nonMaskCol);
         }
         if(start==0) start=1;
         else start=0;
      }

      // setup Fair stipple pattern
      d_stippleDIBFair->setMaskColour(maskCol);
      d_stippleDIBFair->fill(maskCol);
      start=0;
      for(y=0; y < max_height; y++) {
         for(x=start; x < max_width; x+=4) {
            d_stippleDIBFair->plot(x,y,nonMaskCol);
         }
         if(start==0) start=2;
         else start=0;
      }

      // setup Good stipple pattern
      d_stippleDIBGood->setMaskColour(maskCol);
      d_stippleDIBGood->fill(maskCol);
      start = 0;
      for(y=0; y < max_height; y+=3) {
         for(x=start; x < max_width; x+=3) {
            d_stippleDIBGood->plot(x,y,nonMaskCol);
         }
         if(start == 0) start = 1;
         else start = 0;
      }



      // draw outside of hex in mask colour
      int left = iter.left();
      int top = iter.top();

      PixelPoint polyPoints[3];

      polyPoints[0].x(0);
      polyPoints[0].y(0);
      polyPoints[1].x(iter.midX() - left);
      polyPoints[1].y(0);
      polyPoints[2].x(0);
      polyPoints[2].y(iter.topCorner() - top);
      DIB_Utility::drawPoly(d_stippleDIBNotSeen, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBPoor, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBFair, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBGood, polyPoints, 3, maskCol);

      polyPoints[0].x(iter.midX() - left);
      polyPoints[0].y(0);
      polyPoints[1].x(iter.right() - left);
      polyPoints[1].y(0);
      polyPoints[2].x(iter.right() - left);
      polyPoints[2].y(iter.topCorner() - top);
      DIB_Utility::drawPoly(d_stippleDIBNotSeen, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBPoor, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBFair, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBGood, polyPoints, 3, maskCol);

      polyPoints[0].x(0);
      polyPoints[0].y((iter.bottom() - top) +1);
      polyPoints[1].x(iter.midX() - left);
      polyPoints[1].y((iter.bottom() - top) +1);
      polyPoints[2].x(0);
      polyPoints[2].y(iter.lowCorner() - top);
      DIB_Utility::drawPoly(d_stippleDIBNotSeen, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBPoor, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBFair, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBGood, polyPoints, 3, maskCol);

         polyPoints[0].x(iter.right() - left);
      polyPoints[0].y((iter.bottom() - top) +1);
      polyPoints[1].x(iter.midX() - left);
      polyPoints[1].y((iter.bottom() - top) +1);
      polyPoints[2].x(iter.right() - left);
      polyPoints[2].y(iter.lowCorner() - top);
      DIB_Utility::drawPoly(d_stippleDIBNotSeen, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBPoor, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBFair, polyPoints, 3, maskCol);
      DIB_Utility::drawPoly(d_stippleDIBGood, polyPoints, 3, maskCol);

   }



}





void
BattleMapDisplayImp::SetMapFonts(int max_height) {

   // if font height has changed by more than 4 pixels, then redefine fonts
   if(abs(d_lastFontHeight - max_height) > 4) {

      /*
      Font for Label Captions
      */
      d_captionLogFont.height(max_height);
      // titleLogFont.weight(FW_HEAVY);
      d_captionLogFont.face(scenario->fontName(Font_Bold));
      d_captionLogFont.underline(false);


      /*
      Font for Victory Points
      */
      d_pointsLogFont.height(max_height);
      d_pointsLogFont.weight(FW_HEAVY);
      d_pointsLogFont.face(scenario->fontName(Font_Bold));
      d_pointsLogFont.underline(false);
   }

}




/*
 * Interface Functions
 */


BattleMapDisplay::BattleMapDisplay(RCPBattleData batData, BattleMapInfo::Mode mode)
{
   d_imp = new BattleMapDisplayImp(batData, mode);
   ASSERT(d_imp != 0);
}

BattleMapDisplay::~BattleMapDisplay()
{
   delete d_imp;
}

bool BattleMapDisplay::draw()
{
   ASSERT(d_imp != 0);
   return d_imp->draw();
}

void BattleMapDisplay::blit(PAINTSTRUCT * ps)
{
   ASSERT(d_imp != 0);
   d_imp->blit(ps);
}

DrawDIBDC* BattleMapDisplay::mainDIB()
{
   ASSERT(d_imp != 0);
   return d_imp->mainDIB();
}

DrawDIBDC* BattleMapDisplay::staticDIB()
{
   ASSERT(d_imp != 0);
   return d_imp->staticDIB();
}

bool BattleMapDisplay::setPosition(const BattleLocation& location)
{
   ASSERT(d_imp != 0);
   return d_imp->setPosition(location);
}

bool BattleMapDisplay::setSize(const PixelRect& r)
{
   ASSERT(d_imp != 0);
   return d_imp->setSize(r);
}

PixelPoint BattleMapDisplay::getPixelSize() const
{
   ASSERT(d_imp != 0);
   return d_imp->getPixelSize();
}

const BattleLocation& BattleMapDisplay::getCorner() const
{
   ASSERT(d_imp != 0);
   return d_imp->getCorner();
}

const BattleLocation& BattleMapDisplay::getSize() const
{
   ASSERT(d_imp != 0);
   return d_imp->getSize();
}

const BattleLocation& BattleMapDisplay::getCentre() const
{
   ASSERT(d_imp != 0);
   return d_imp->getCentre();
}

BattleArea BattleMapDisplay::area() const
{
   ASSERT(d_imp != 0);
   return d_imp->area();
}


void BattleMapDisplay::redraw(bool all)
{
   ASSERT(d_imp != 0);
   d_imp->redraw(all);
}

BattleMapInfo::Mode BattleMapDisplay::mode() const
{
   ASSERT(d_imp != 0);
   return d_imp->mode();
}

void BattleMapDisplay::mode(BattleMapInfo::Mode mode)
{
   ASSERT(d_imp != 0);
   d_imp->mode(mode);
}

bool BattleMapDisplay::toggleHexOutline()
{
   return d_imp->toggleHexOutline();
}

bool BattleMapDisplay::hexOutline() const
{
   return d_imp->hexOutline();
}

bool BattleMapDisplay::hexOutline(bool flag)
{
   return d_imp->hexOutline(flag);
}


#ifdef DEBUG
bool BattleMapDisplay::toggleHexDebug()
{
   return d_imp->toggleHexDebug();
}

bool BattleMapDisplay::hexDebug() const
{
   return d_imp->hexDebug();
}

bool BattleMapDisplay::hexDebug(bool flag)
{
   return d_imp->hexDebug(flag);
}
#endif


BattleLocation BattleMapDisplay::pixelToLocation(const PixelPoint& p)
{
   return d_imp->pixelToLocation(p);
}

PixelPoint BattleMapDisplay::locationToPixel(const BattleLocation& b)
{
   return d_imp->locationToPixel(b);
}

void
BattleMapDisplay::getHexCorners(HexCord & bottom_left, HexCord & top_right) {

   d_imp->getHexCorners(bottom_left, top_right);
}

HexCord
BattleMapDisplay::getBottomLeft(void) {

   return d_imp->getBottomLeft();
}

HexCord
BattleMapDisplay::getTopRight(void) {

   return d_imp->getTopRight();
}


PixelPoint BattleMapDisplay::hexPixelSize() const
{
   return d_imp->hexPixelSize();
}


void
BattleMapDisplay::ShowSelection(bool state) {

   d_imp->ShowSelection(state);
}

void
BattleMapDisplay::SetSelectionColour(ColourIndex col) {

   d_imp->SetSelectionColour(col);
}

void
BattleMapDisplay::SetSelectionArea(HexCord bottomleft, HexCord size) {

   d_imp->SetSelectionArea(bottomleft, size);
}


void
BattleMapDisplay::ShowDarkenedArea(bool state) {

   d_imp->ShowDarkenedArea(state);
}


void
BattleMapDisplay::SetDarkenedArea(int tophex, int bottomhex) {

   d_imp->SetDarkenedArea(tophex, bottomhex);
}

void
BattleMapDisplay::RequestTerrainAveraging(void) {

//   d_imp->RequestTerrainAveraging();
}

void
BattleMapDisplay::showHexVisibility(bool state) {

   d_imp->showHexVisibility(state);
}


bool
BattleMapDisplay::isHexVisibilityOn(void) {

   return d_imp->isHexVisibilityOn();
}


void BattleMapDisplay::reset()
{
   d_imp->reset();
}


/*
Debugging toggles
*/

bool
BattleMapDisplay::isDisplayingTimes(void) {

   return d_imp->isDisplayingTimes();
}

void
BattleMapDisplay::displayTimes(bool state) {

   d_imp->displayTimes(state);
}

/*
Display toggles
*/
bool
BattleMapDisplay::isDrawingTerrain(void) {

   return d_imp->isDrawingTerrain();
}

void
BattleMapDisplay::drawTerrain(bool state) {

   d_imp->drawTerrain(state);
}

bool
BattleMapDisplay::isDrawingOverlaps(void) {

   return d_imp->isDrawingOverlaps();
}

void
BattleMapDisplay::drawOverlaps(bool state) {

   d_imp->drawOverlaps(state);
}

bool
BattleMapDisplay::isDrawingPathways(void) {

   return d_imp->isDrawingPathways();
}

void
BattleMapDisplay::drawPathways(bool state) {

   d_imp->drawPathways(state);
}

bool
BattleMapDisplay::isDrawingCoasts(void) {

   return d_imp->isDrawingCoasts();
}

void
BattleMapDisplay::drawCoasts(bool state) {

   d_imp->drawCoasts(state);
}

bool
BattleMapDisplay::isDrawingHills(void) {

   return d_imp->isDrawingHills();
}

void
BattleMapDisplay::drawHills(bool state) {

   d_imp->drawHills(state);
}

bool
BattleMapDisplay::isDrawingEdges(void) {

   return d_imp->isDrawingEdges();
}

void
BattleMapDisplay::drawEdges(bool state) {

   d_imp->drawEdges(state);
}

bool
BattleMapDisplay::isDrawingTroops(void) {

   return d_imp->isDrawingTroops();
}

void
BattleMapDisplay::drawTroops(bool state) {

   d_imp->drawTroops(state);
}

bool
BattleMapDisplay::isDrawingBuildings(void) {

   return d_imp->isDrawingBuildings();
}

void
BattleMapDisplay::drawBuildings(bool state) {

   d_imp->drawBuildings(state);
}

bool
BattleMapDisplay::isDrawingEffects(void) {

   return d_imp->isDrawingEffects();
}

void
BattleMapDisplay::drawEffects(bool state) {

   d_imp->drawEffects(state);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
