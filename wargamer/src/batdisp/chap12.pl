print "dir:"; chomp($dir = <STDIN>);
chdir($dir) || warn "Could not change to $dir: $!";
while(sort <*>)
{
	print "$_\n";
}