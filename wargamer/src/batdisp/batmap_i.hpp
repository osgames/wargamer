/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATMAP_I_HPP
#define BATMAP_I_HPP

#ifndef __cplusplus
#error batmap_i.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Interface and utilities for BattleMap
 *
 *----------------------------------------------------------------------
 */

namespace BattleMapInfo
{
        enum Mode
        {
                OneMile,                // View aproximately 1 Mile
                TwoMile,                // View aproximately 2 Miles
                FourMile,       // View aproximately 2 Miles
                Strategic,      // Strategic View
                Tiny,                   // Locator Map
                OneHex                 // Close-up zoom of one hex
        };
};

#endif /* BATMAP_I_HPP */

