/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BTWIN_I_HPP
#define BTWIN_I_HPP

#ifndef __cplusplus
#error btwin_i.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle Windows Interface
 *
 * Child windows of the main battle window use this to communicate
 * with other windows.  It prevents the cyclic dependancy if they
 * had access to BattleWindows at the expense of a virtual function call.
 *
 * Other methods would be to use Windows's SendMessage to send user defined
 * messages up to the main window.  But that would be slower and messier.
 *
 *----------------------------------------------------------------------
 */

#include "bdisp_dll.h"
#include "refptr.hpp"
#include "batcord.hpp"
#include "grtypes.hpp"
#include "bobdef.hpp"



/*
 * Information passed to user interace from the map
 */

class BattleMapSelect
{
	public:
		/*
		* Types
		*/

		enum Button
		{
			NoButton,
			LeftButton,
			RightButton
		};

		/*
		 * Constructors
		 */

		BattleMapSelect() : d_loc(), d_button(NoButton) { }

		BattleMapSelect(const PixelPoint& p, const BattleLocation& l, Button b) :
			d_pPoint(p),
			d_loc(l),
			d_button(b)
		{
		}

		/*
		 * Accessors
		 */

		const BattleLocation& location() const { return d_loc; }
		Button button() const { return d_button; }
		const PixelPoint& pixelPoint() const { return d_pPoint; }

		void location(BattleLocation l) { d_loc = l; }
		void button(Button b) { d_button = b; }
		void pixel(PixelPoint p) { d_pPoint = p; }

	private:
		/*
		 * Data
		 */

		BattleLocation d_loc;		// What hex is it?
		Button d_button;				// What buttons are pressed?
		PixelPoint d_pPoint;			// Screen coordinates

};

/*
 * BattleWindowsImp is derived from this
 */

class DrawDIBDC;
class BattleMapDisplay;
class BattleWindowsInterface
#ifdef DEBUG
 : public RefBaseCount
#endif
{
	public:
		virtual ~BattleWindowsInterface() { }

		virtual HWND hwnd() const = 0;
			// Get Window Handle

		virtual bool setMapLocation(const BattleLocation& l) = 0;
			// Set centre of main map, return true if has changed
		virtual void mapAreaChanged(const BattleArea& area) = 0;
			// Sent by mapwind when it has changed its area
		virtual void mapZoomChanged() = 0;
			// Get mapwindow zoom buttons updated

		virtual void onLButtonDown(const BattleMapSelect& info) = 0;
			// sent by mapwind when Button is pressed
		virtual void onLButtonUp(const BattleMapSelect& info) = 0;
			// sent by mapwind when Button is released while not dragging
		virtual void onRButtonDown(const BattleMapSelect& info) = 0;
			// sent by mapwind when Button is pressed
		virtual void onRButtonUp(const BattleMapSelect& info) = 0;
			// sent by mapwind when Button is released while not dragging
		virtual void onStartDrag(const BattleMapSelect& info) = 0;
			// sent by mapwind when Mouse is moved while button is held
	 	virtual void onEndDrag(const BattleMapSelect& info) = 0;
			// sent by mapwind when button is released while dragging
	 	virtual void onMove(const BattleMapSelect& info) = 0;
			// sent by mapwind when Mouse has moved
		virtual bool setCursor() = 0;
		virtual void redrawMap(bool all) = 0;

		virtual void orderCP(CRefBattleCP cp) = 0;
		virtual void messageWindowUpdated() = 0;
		// virtual void windowDestroyed(HWND hwnd) = 0;

		virtual void positionWindows() = 0;
		virtual void updateAll() = 0;

		virtual bool updateTracking(const BattleMeasure::HexCord& hex) = 0;

		virtual const DrawDIBDC* mapDib()      const = 0;
		virtual BattleMapDisplay* mapDisplay() const = 0;
};


#ifdef DEBUG
typedef RefPtr<BattleWindowsInterface> PBattleWindows;
typedef const RefPtr<BattleWindowsInterface>& RPBattleWindows;
#else
typedef BattleWindowsInterface* PBattleWindows;
typedef BattleWindowsInterface* RPBattleWindows;
#endif

#if !defined(NOLOG)
class LogFile;
BATDISP_DLL LogFile& operator << (LogFile& file, const BattleMapSelect& bms);
#endif

#endif /* BTWIN_I_HPP */

