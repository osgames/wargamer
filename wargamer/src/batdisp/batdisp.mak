# Microsoft Developer Studio Generated NMAKE File, Based on batdisp.dsp
!IF "$(CFG)" == ""
CFG=batdisp - Win32 Debug
!MESSAGE No configuration specified. Defaulting to batdisp - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "batdisp - Win32 Release" && "$(CFG)" != "batdisp - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "batdisp.mak" CFG="batdisp - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "batdisp - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "batdisp - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "batdisp - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batdisp.dll"

!ELSE 

ALL : "ob - Win32 Release" "gamesup - Win32 Release" "system - Win32 Release" "batdata - Win32 Release" "$(OUTDIR)\batdisp.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batdata - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\animctrl.obj"
	-@erase "$(INTDIR)\b_vis.obj"
	-@erase "$(INTDIR)\batdisp.obj"
	-@erase "$(INTDIR)\batmap.obj"
	-@erase "$(INTDIR)\battmap.obj"
	-@erase "$(INTDIR)\bd_hill.obj"
	-@erase "$(INTDIR)\bdispute.obj"
	-@erase "$(INTDIR)\btwin_i.obj"
	-@erase "$(INTDIR)\buildisp.obj"
	-@erase "$(INTDIR)\fx_disp.obj"
	-@erase "$(INTDIR)\hexiter.obj"
	-@erase "$(INTDIR)\tempdib.obj"
	-@erase "$(INTDIR)\unitdisp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\zlist.obj"
	-@erase "$(OUTDIR)\batdisp.dll"
	-@erase "$(OUTDIR)\batdisp.exp"
	-@erase "$(OUTDIR)\batdisp.lib"
	-@erase "$(OUTDIR)\batdisp.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATDISP_DLL" /Fp"$(INTDIR)\batdisp.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batdisp.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib Winmm.lib gdi32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\batdisp.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batdisp.dll" /implib:"$(OUTDIR)\batdisp.lib" 
LINK32_OBJS= \
	"$(INTDIR)\animctrl.obj" \
	"$(INTDIR)\b_vis.obj" \
	"$(INTDIR)\batdisp.obj" \
	"$(INTDIR)\batmap.obj" \
	"$(INTDIR)\battmap.obj" \
	"$(INTDIR)\bd_hill.obj" \
	"$(INTDIR)\bdispute.obj" \
	"$(INTDIR)\btwin_i.obj" \
	"$(INTDIR)\buildisp.obj" \
	"$(INTDIR)\fx_disp.obj" \
	"$(INTDIR)\hexiter.obj" \
	"$(INTDIR)\tempdib.obj" \
	"$(INTDIR)\unitdisp.obj" \
	"$(INTDIR)\zlist.obj" \
	"$(OUTDIR)\batdata.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\ob.lib"

"$(OUTDIR)\batdisp.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "batdisp - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\batdispDB.dll"

!ELSE 

ALL : "ob - Win32 Debug" "gamesup - Win32 Debug" "system - Win32 Debug" "batdata - Win32 Debug" "$(OUTDIR)\batdispDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batdata - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\animctrl.obj"
	-@erase "$(INTDIR)\b_vis.obj"
	-@erase "$(INTDIR)\batdisp.obj"
	-@erase "$(INTDIR)\batmap.obj"
	-@erase "$(INTDIR)\battmap.obj"
	-@erase "$(INTDIR)\bd_hill.obj"
	-@erase "$(INTDIR)\bdispute.obj"
	-@erase "$(INTDIR)\btwin_i.obj"
	-@erase "$(INTDIR)\buildisp.obj"
	-@erase "$(INTDIR)\fx_disp.obj"
	-@erase "$(INTDIR)\hexiter.obj"
	-@erase "$(INTDIR)\tempdib.obj"
	-@erase "$(INTDIR)\unitdisp.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\zlist.obj"
	-@erase "$(OUTDIR)\batdispDB.dll"
	-@erase "$(OUTDIR)\batdispDB.exp"
	-@erase "$(OUTDIR)\batdispDB.ilk"
	-@erase "$(OUTDIR)\batdispDB.lib"
	-@erase "$(OUTDIR)\batdispDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\batdata" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATDISP_DLL" /Fp"$(INTDIR)\batdisp.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\batdisp.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=User32.lib Winmm.lib gdi32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\batdispDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\batdispDB.dll" /implib:"$(OUTDIR)\batdispDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\animctrl.obj" \
	"$(INTDIR)\b_vis.obj" \
	"$(INTDIR)\batdisp.obj" \
	"$(INTDIR)\batmap.obj" \
	"$(INTDIR)\battmap.obj" \
	"$(INTDIR)\bd_hill.obj" \
	"$(INTDIR)\bdispute.obj" \
	"$(INTDIR)\btwin_i.obj" \
	"$(INTDIR)\buildisp.obj" \
	"$(INTDIR)\fx_disp.obj" \
	"$(INTDIR)\hexiter.obj" \
	"$(INTDIR)\tempdib.obj" \
	"$(INTDIR)\unitdisp.obj" \
	"$(INTDIR)\zlist.obj" \
	"$(OUTDIR)\batdataDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\obDB.lib"

"$(OUTDIR)\batdispDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("batdisp.dep")
!INCLUDE "batdisp.dep"
!ELSE 
!MESSAGE Warning: cannot find "batdisp.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "batdisp - Win32 Release" || "$(CFG)" == "batdisp - Win32 Debug"
SOURCE=.\animctrl.cpp

"$(INTDIR)\animctrl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\b_vis.cpp

"$(INTDIR)\b_vis.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batdisp.cpp

"$(INTDIR)\batdisp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\batmap.cpp

"$(INTDIR)\batmap.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\battmap.cpp

"$(INTDIR)\battmap.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bd_hill.cpp

"$(INTDIR)\bd_hill.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bdispute.cpp

"$(INTDIR)\bdispute.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\btwin_i.cpp

"$(INTDIR)\btwin_i.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\buildisp.cpp

"$(INTDIR)\buildisp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fx_disp.cpp

"$(INTDIR)\fx_disp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\hexiter.cpp

"$(INTDIR)\hexiter.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\tempdib.cpp

"$(INTDIR)\tempdib.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\unitdisp.cpp

"$(INTDIR)\unitdisp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\zlist.cpp

"$(INTDIR)\zlist.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "batdisp - Win32 Release"

"batdata - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" 
   cd "..\batdisp"

"batdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batdisp"

!ELSEIF  "$(CFG)" == "batdisp - Win32 Debug"

"batdata - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" 
   cd "..\batdisp"

"batdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\batdata"
   $(MAKE) /$(MAKEFLAGS) /F .\batdata.mak CFG="batdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batdisp"

!ENDIF 

!IF  "$(CFG)" == "batdisp - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\batdisp"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batdisp"

!ELSEIF  "$(CFG)" == "batdisp - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\batdisp"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batdisp"

!ENDIF 

!IF  "$(CFG)" == "batdisp - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\batdisp"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batdisp"

!ELSEIF  "$(CFG)" == "batdisp - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\batdisp"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batdisp"

!ENDIF 

!IF  "$(CFG)" == "batdisp - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\batdisp"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\batdisp"

!ELSEIF  "$(CFG)" == "batdisp - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\batdisp"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\batdisp"

!ENDIF 


!ENDIF 

