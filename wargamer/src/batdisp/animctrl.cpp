/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "animctrl.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : ANIMCTRL
        Description : Classes for controling animation sequences
        
-------------------------------------------------------------------------------------------------------------------*/




/*
    unsigned char AnimationControl::UpdateInfantryFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed, bool RandomizeTimer) {

        // for initial pass (probably not necessary)
        if(index >= InfantryAnimations[action].NumFrames) index = 0;

        unsigned char retval = ANIMCUE_NONE;
        timer+=speed;

        while(timer > InfantryAnimations[action].FrameDelay[index] ) {
            timer -= InfantryAnimations[action].FrameDelay[index];
            index++;
            if(index >= InfantryAnimations[action].NumFrames) {
                if(InfantryAnimations[action].LoopFlag == true) index = 0;
                else index = InfantryAnimations[action].NumFrames-1;
            }
            retval = InfantryAnimations[action].FrameFlag[index];
        }
        if(RandomizeTimer) timer = random(0, InfantryAnimations[action].FrameDelay[index]);

        return retval;
    }
*/
    unsigned char AnimationControl::UpdateInfantryFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed, bool RandomizeTimer) {
        if(index >= InfantryAnimations[action].NumFrames) index = 0;
        
        timer+=speed;
        if(timer >= InfantryAnimations[action].FrameDelay[index]) {
            timer = 0;
            index++;
            if(index == InfantryAnimations[action].NumFrames) {
                if(InfantryAnimations[action].LoopFlag == true) index = 0;
                else index = InfantryAnimations[action].NumFrames-1;
            }
            if(RandomizeTimer) timer = random(0, InfantryAnimations[action].FrameDelay[index]);
            return InfantryAnimations[action].FrameFlag[index];
        }
//        if(RandomizeTimer) timer = random(0, InfantryAnimations[action].FrameDelay[index]);
        return ANIMCUE_NONE;
    }


    unsigned char AnimationControl::UpdateCavalryFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed) {
        if(index >= CavalryAnimations[action].NumFrames) index = 0;
        
        timer+=speed;
        if(timer >= CavalryAnimations[action].FrameDelay[index]) {
            timer = 0;
            index++;
            if(index == CavalryAnimations[action].NumFrames) {
                if(CavalryAnimations[action].LoopFlag == true) index = 0;
                else index = CavalryAnimations[action].NumFrames-1;
            }
            return CavalryAnimations[action].FrameFlag[index];
        }
        return ANIMCUE_NONE;
    }

    unsigned char AnimationControl::UpdateFootArtilleryFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed) {
        if(index >= FootArtilleryAnimations[action].NumFrames) index = 0;
        
        timer+=speed;
        if(timer >= FootArtilleryAnimations[action].FrameDelay[index]) {
            timer = 0;
            index++;
            if(index == FootArtilleryAnimations[action].NumFrames) {
                if(FootArtilleryAnimations[action].LoopFlag == true) index = 0;
                else index = FootArtilleryAnimations[action].NumFrames-1;
            }
            return FootArtilleryAnimations[action].FrameFlag[index];
        }
        return ANIMCUE_NONE;
    }

    unsigned char AnimationControl::UpdateHorseArtilleryFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed) {
        if(index >= HorseArtilleryAnimations[action].NumFrames) index = 0;
        
        timer+=speed;
        if(timer >= HorseArtilleryAnimations[action].FrameDelay[index]) {
            timer = 0;
            index++;
            if(index == HorseArtilleryAnimations[action].NumFrames) {
                if(HorseArtilleryAnimations[action].LoopFlag == true) index = 0;
                else index = HorseArtilleryAnimations[action].NumFrames-1;
            }
            return HorseArtilleryAnimations[action].FrameFlag[index];
        }
        return ANIMCUE_NONE;
    }


    unsigned char AnimationControl::UpdateOfficerFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed) {
        if(index >= OfficerAnimations[action].NumFrames) index = 0;
        
        timer+=speed;
        if(timer >= OfficerAnimations[action].FrameDelay[index]) {
            timer = 0;
            index++;
            if(index == OfficerAnimations[action].NumFrames) {
                if(OfficerAnimations[action].LoopFlag == true) index = 0;
                else index = OfficerAnimations[action].NumFrames-1;
            }
            return OfficerAnimations[action].FrameFlag[index];
        }
        return ANIMCUE_NONE;
    }


    unsigned char AnimationControl::UpdateEffectFrame(EffectTypeEnum action, unsigned int &index, unsigned int &timer, int speed) {
        if(index >= EffectsAnimations[action].NumFrames) index = 0;
        
        timer+=speed;
        if(timer >= EffectsAnimations[action].FrameDelay[index]) {
            timer = 0;
            index++;
            if(index == EffectsAnimations[action].NumFrames) {
                if(EffectsAnimations[action].LoopFlag == true) index = 0;
                else index = EffectsAnimations[action].NumFrames-1;
            }
            return EffectsAnimations[action].FrameFlag[index];
        }
        return ANIMCUE_NONE;
    }


    unsigned char AnimationControl::UpdateFlagFrame(FlagTypeEnum action, unsigned int &index, unsigned int &timer, int speed) {
        if(index >= FlagAnimations[action].NumFrames) index = 0;
        
        timer+=speed;
        if(timer >= FlagAnimations[action].FrameDelay[index]) {
            timer = 0;
            index++;
            if(index == FlagAnimations[action].NumFrames) {
                if(FlagAnimations[action].LoopFlag == true) index = 0;
                else index = FlagAnimations[action].NumFrames-1;
            }
            return FlagAnimations[action].FrameFlag[index];
        }
        return ANIMCUE_NONE;
    }



    unsigned char AnimationControl::UpdateFootManFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed) {
        if(index >= FootManAnimations[action].NumFrames) index = 0;
        
        timer+=speed;
        if(timer >= FootManAnimations[action].FrameDelay[index]) {
            timer = 0;
            index++;
            if(index == FootManAnimations[action].NumFrames) {
                if(FootManAnimations[action].LoopFlag == true) index = 0;
                else index = FootManAnimations[action].NumFrames-1;
            }
            return FootManAnimations[action].FrameFlag[index];
        }
        return ANIMCUE_NONE;
    }



    unsigned char AnimationControl::UpdateHorseManFrame(SpriteActionEnum action, unsigned int &index, unsigned int &timer, int speed) {
        if(index >= HorseManAnimations[action].NumFrames) index = 0;
        
        timer+=speed;
        if(timer >= HorseManAnimations[action].FrameDelay[index]) {
            timer = 0;
            index++;
            if(index == HorseManAnimations[action].NumFrames) {
                if(HorseManAnimations[action].LoopFlag == true) index = 0;
                else index = HorseManAnimations[action].NumFrames-1;
            }
            return HorseManAnimations[action].FrameFlag[index];
        }
        return ANIMCUE_NONE;
    }












AnimationControl::AnimationControl(void) {

// load anim data from file
// for the moment this is going to be 'hard-wired'

    /*
    INFANTRY ANIMATIONS
    */

    InfantryAnimations[STANDING].NumFrames = 1;
    InfantryAnimations[STANDING].LoopFlag = false;
    InfantryAnimations[STANDING].FrameIndex[0] = 0;
    InfantryAnimations[STANDING].FrameDelay[0] = 0;

    InfantryAnimations[WALKING].NumFrames = 6;
    InfantryAnimations[WALKING].LoopFlag = true;
    InfantryAnimations[WALKING].FrameIndex[0] = 1;
    InfantryAnimations[WALKING].FrameDelay[0] = 600;
    InfantryAnimations[WALKING].FrameFlag[0] = ANIMCUE_NONE;
    InfantryAnimations[WALKING].FrameIndex[1] = 2;
    InfantryAnimations[WALKING].FrameDelay[1] = 600;
    InfantryAnimations[WALKING].FrameFlag[1] = ANIMCUE_NONE;
    InfantryAnimations[WALKING].FrameIndex[2] = 3;
    InfantryAnimations[WALKING].FrameDelay[2] = 600;
    InfantryAnimations[WALKING].FrameFlag[2] = ANIMCUE_NONE;
    InfantryAnimations[WALKING].FrameIndex[3] = 4;
    InfantryAnimations[WALKING].FrameDelay[3] = 600;
    InfantryAnimations[WALKING].FrameFlag[3] = ANIMCUE_NONE;
    InfantryAnimations[WALKING].FrameIndex[4] = 5;
    InfantryAnimations[WALKING].FrameDelay[4] = 600;
    InfantryAnimations[WALKING].FrameFlag[4] = ANIMCUE_NONE;
    InfantryAnimations[WALKING].FrameIndex[5] = 6;
    InfantryAnimations[WALKING].FrameDelay[5] = 600;
    InfantryAnimations[WALKING].FrameFlag[5] = ANIMCUE_NONE;

    InfantryAnimations[SHOOTING].NumFrames = 15;
    InfantryAnimations[SHOOTING].LoopFlag = true;
    InfantryAnimations[SHOOTING].FrameIndex[0] = 7; // aim
    InfantryAnimations[SHOOTING].FrameDelay[0] = 1600;
    InfantryAnimations[SHOOTING].FrameFlag[0] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[1] = 24; // fire
    InfantryAnimations[SHOOTING].FrameDelay[1] = 100;
    InfantryAnimations[SHOOTING].FrameFlag[1] = ANIMCUE_MUSKETFIRE;
    InfantryAnimations[SHOOTING].FrameIndex[2] = 25;
    InfantryAnimations[SHOOTING].FrameDelay[2] = 100;
    InfantryAnimations[SHOOTING].FrameFlag[2] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[3] = 26;
    InfantryAnimations[SHOOTING].FrameDelay[3] = 100;
    InfantryAnimations[SHOOTING].FrameFlag[3] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[4] = 27;
    InfantryAnimations[SHOOTING].FrameDelay[4] = 100;
    InfantryAnimations[SHOOTING].FrameFlag[4] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[5] = 28;
    InfantryAnimations[SHOOTING].FrameDelay[5] = 100;
    InfantryAnimations[SHOOTING].FrameFlag[5] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[6] = 29;
    InfantryAnimations[SHOOTING].FrameDelay[6] = 100;
    InfantryAnimations[SHOOTING].FrameFlag[6] = ANIMCUE_NONE;
    
    InfantryAnimations[SHOOTING].FrameIndex[7] = 8; // reload
    InfantryAnimations[SHOOTING].FrameDelay[7] = 1400;
    InfantryAnimations[SHOOTING].FrameFlag[7] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[8] = 9;
    InfantryAnimations[SHOOTING].FrameDelay[8] = 1400;
    InfantryAnimations[SHOOTING].FrameFlag[8] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[9] = 10;
    InfantryAnimations[SHOOTING].FrameDelay[9] = 1400;
    InfantryAnimations[SHOOTING].FrameFlag[9] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[10] = 11;
    InfantryAnimations[SHOOTING].FrameDelay[10] = 1400;
    InfantryAnimations[SHOOTING].FrameFlag[10] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[11] = 8; // reload
    InfantryAnimations[SHOOTING].FrameDelay[11] = 1400;
    InfantryAnimations[SHOOTING].FrameFlag[11] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[12] = 9;
    InfantryAnimations[SHOOTING].FrameDelay[12] = 1400;
    InfantryAnimations[SHOOTING].FrameFlag[12] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[13] = 10;
    InfantryAnimations[SHOOTING].FrameDelay[13] = 1400;
    InfantryAnimations[SHOOTING].FrameFlag[13] = ANIMCUE_NONE;
    InfantryAnimations[SHOOTING].FrameIndex[14] = 11;
    InfantryAnimations[SHOOTING].FrameDelay[14] = 1400;
    InfantryAnimations[SHOOTING].FrameFlag[14] = ANIMCUE_NONE;

    InfantryAnimations[RUNNING].NumFrames = 6;
    InfantryAnimations[RUNNING].LoopFlag = true;
    InfantryAnimations[RUNNING].FrameIndex[0] = 18;
    InfantryAnimations[RUNNING].FrameDelay[0] = 600;
    InfantryAnimations[RUNNING].FrameFlag[0] = ANIMCUE_NONE;
    InfantryAnimations[RUNNING].FrameIndex[1] = 19;
    InfantryAnimations[RUNNING].FrameDelay[1] = 600;
    InfantryAnimations[RUNNING].FrameFlag[1] = ANIMCUE_NONE;
    InfantryAnimations[RUNNING].FrameIndex[2] = 20;
    InfantryAnimations[RUNNING].FrameDelay[2] = 600;
    InfantryAnimations[RUNNING].FrameFlag[2] = ANIMCUE_NONE;
    InfantryAnimations[RUNNING].FrameIndex[3] = 21;
    InfantryAnimations[RUNNING].FrameDelay[3] = 600;
    InfantryAnimations[RUNNING].FrameFlag[3] = ANIMCUE_NONE;
    InfantryAnimations[RUNNING].FrameIndex[4] = 22;
    InfantryAnimations[RUNNING].FrameDelay[4] = 600;
    InfantryAnimations[RUNNING].FrameFlag[4] = ANIMCUE_NONE;
    InfantryAnimations[RUNNING].FrameIndex[5] = 23;
    InfantryAnimations[RUNNING].FrameDelay[5] = 600;
    InfantryAnimations[RUNNING].FrameFlag[5] = ANIMCUE_NONE;

    InfantryAnimations[CHARGING].NumFrames = 6;
    InfantryAnimations[CHARGING].LoopFlag = true;
    InfantryAnimations[CHARGING].FrameIndex[0] = 12;
    InfantryAnimations[CHARGING].FrameDelay[0] = 600;
    InfantryAnimations[CHARGING].FrameFlag[0] = ANIMCUE_NONE;
    InfantryAnimations[CHARGING].FrameIndex[1] = 13;
    InfantryAnimations[CHARGING].FrameDelay[1] = 600;
    InfantryAnimations[CHARGING].FrameFlag[1] = ANIMCUE_NONE;
    InfantryAnimations[CHARGING].FrameIndex[2] = 14;
    InfantryAnimations[CHARGING].FrameDelay[2] = 600;
    InfantryAnimations[CHARGING].FrameFlag[2] = ANIMCUE_NONE;
    InfantryAnimations[CHARGING].FrameIndex[3] = 15;
    InfantryAnimations[CHARGING].FrameDelay[3] = 600;
    InfantryAnimations[CHARGING].FrameFlag[3] = ANIMCUE_NONE;
    InfantryAnimations[CHARGING].FrameIndex[4] = 16;
    InfantryAnimations[CHARGING].FrameDelay[4] = 600;
    InfantryAnimations[CHARGING].FrameFlag[4] = ANIMCUE_NONE;
    InfantryAnimations[CHARGING].FrameIndex[5] = 17;
    InfantryAnimations[CHARGING].FrameDelay[5] = 600;
    InfantryAnimations[CHARGING].FrameFlag[5] = ANIMCUE_NONE;

    InfantryAnimations[STABBING].NumFrames = 6;
    InfantryAnimations[STABBING].LoopFlag = true;
    InfantryAnimations[STABBING].FrameIndex[0] = 30;
    InfantryAnimations[STABBING].FrameDelay[0] = 1300;
    InfantryAnimations[STABBING].FrameFlag[0] = ANIMCUE_NONE;
    InfantryAnimations[STABBING].FrameIndex[1] = 31;
    InfantryAnimations[STABBING].FrameDelay[1] = 300;
    InfantryAnimations[STABBING].FrameFlag[1] = ANIMCUE_NONE;
    InfantryAnimations[STABBING].FrameIndex[2] = 32;
    InfantryAnimations[STABBING].FrameDelay[2] = 300;
    InfantryAnimations[STABBING].FrameFlag[2] = ANIMCUE_NONE;
    InfantryAnimations[STABBING].FrameIndex[3] = 33;
    InfantryAnimations[STABBING].FrameDelay[3] = 1300;
    InfantryAnimations[STABBING].FrameFlag[3] = ANIMCUE_NONE;
    InfantryAnimations[STABBING].FrameIndex[4] = 32;
    InfantryAnimations[STABBING].FrameDelay[4] = 400;
    InfantryAnimations[STABBING].FrameFlag[4] = ANIMCUE_NONE;    
    InfantryAnimations[STABBING].FrameIndex[5] = 31;
    InfantryAnimations[STABBING].FrameDelay[5] = 400;
    InfantryAnimations[STABBING].FrameFlag[5] = ANIMCUE_NONE;

    InfantryAnimations[DEAD].NumFrames = 1;
    InfantryAnimations[DEAD].LoopFlag = false;
    InfantryAnimations[DEAD].FrameIndex[0] = 34;
    InfantryAnimations[DEAD].FrameDelay[0] = 0;
    InfantryAnimations[DEAD].FrameFlag[0] = ANIMCUE_NONE;

    /*
    CAVALRY ANIMATIONS
    */

    CavalryAnimations[STANDING].NumFrames = 1;
    CavalryAnimations[STANDING].LoopFlag = false;
    CavalryAnimations[STANDING].FrameIndex[0] = 0;
    CavalryAnimations[STANDING].FrameDelay[0] = 0;
    CavalryAnimations[STANDING].FrameFlag[0] = ANIMCUE_NONE;

    CavalryAnimations[WALKING].NumFrames = 6;
    CavalryAnimations[WALKING].LoopFlag = true;
    CavalryAnimations[WALKING].FrameIndex[0] = 1;
    CavalryAnimations[WALKING].FrameDelay[0] = 600;
    CavalryAnimations[WALKING].FrameFlag[0] = ANIMCUE_NONE;
    CavalryAnimations[WALKING].FrameIndex[1] = 2;
    CavalryAnimations[WALKING].FrameDelay[1] = 600;
    CavalryAnimations[WALKING].FrameFlag[1] = ANIMCUE_NONE;
    CavalryAnimations[WALKING].FrameIndex[2] = 3;
    CavalryAnimations[WALKING].FrameDelay[2] = 600;
    CavalryAnimations[WALKING].FrameFlag[2] = ANIMCUE_NONE;
    CavalryAnimations[WALKING].FrameIndex[3] = 4;
    CavalryAnimations[WALKING].FrameDelay[3] = 600;
    CavalryAnimations[WALKING].FrameFlag[3] = ANIMCUE_NONE;
    CavalryAnimations[WALKING].FrameIndex[4] = 5;
    CavalryAnimations[WALKING].FrameDelay[4] = 600;
    CavalryAnimations[WALKING].FrameFlag[4] = ANIMCUE_NONE;
    CavalryAnimations[WALKING].FrameIndex[5] = 6;
    CavalryAnimations[WALKING].FrameDelay[5] = 600;
    CavalryAnimations[WALKING].FrameFlag[5] = ANIMCUE_NONE;

    CavalryAnimations[GALLOPING].NumFrames = 6;
    CavalryAnimations[GALLOPING].LoopFlag = true;
    CavalryAnimations[GALLOPING].FrameIndex[0] = 7;
    CavalryAnimations[GALLOPING].FrameDelay[0] = 600;
    CavalryAnimations[GALLOPING].FrameFlag[0] = ANIMCUE_NONE;
    CavalryAnimations[GALLOPING].FrameIndex[1] = 8;
    CavalryAnimations[GALLOPING].FrameDelay[1] = 600;
    CavalryAnimations[GALLOPING].FrameFlag[1] = ANIMCUE_NONE;
    CavalryAnimations[GALLOPING].FrameIndex[2] = 9;
    CavalryAnimations[GALLOPING].FrameDelay[2] = 600;
    CavalryAnimations[GALLOPING].FrameFlag[2] = ANIMCUE_NONE;
    CavalryAnimations[GALLOPING].FrameIndex[3] = 10;
    CavalryAnimations[GALLOPING].FrameDelay[3] = 600;
    CavalryAnimations[GALLOPING].FrameFlag[3] = ANIMCUE_NONE;
    CavalryAnimations[GALLOPING].FrameIndex[4] = 11;
    CavalryAnimations[GALLOPING].FrameDelay[4] = 600;
    CavalryAnimations[GALLOPING].FrameFlag[4] = ANIMCUE_NONE;
    CavalryAnimations[GALLOPING].FrameIndex[5] = 12;
    CavalryAnimations[GALLOPING].FrameDelay[5] = 600;
    CavalryAnimations[GALLOPING].FrameFlag[5] = ANIMCUE_NONE;

    CavalryAnimations[TROTTING].NumFrames = 6;
    CavalryAnimations[TROTTING].LoopFlag = true;
    CavalryAnimations[TROTTING].FrameIndex[0] = 13;
    CavalryAnimations[TROTTING].FrameDelay[0] = 600;
    CavalryAnimations[TROTTING].FrameFlag[0] = ANIMCUE_NONE;
    CavalryAnimations[TROTTING].FrameIndex[1] = 14;
    CavalryAnimations[TROTTING].FrameDelay[1] = 600;
    CavalryAnimations[TROTTING].FrameFlag[1] = ANIMCUE_NONE;
    CavalryAnimations[TROTTING].FrameIndex[2] = 15;
    CavalryAnimations[TROTTING].FrameDelay[2] = 600;
    CavalryAnimations[TROTTING].FrameFlag[2] = ANIMCUE_NONE;
    CavalryAnimations[TROTTING].FrameIndex[3] = 16;
    CavalryAnimations[TROTTING].FrameDelay[3] = 600;
    CavalryAnimations[TROTTING].FrameFlag[3] = ANIMCUE_NONE;
    CavalryAnimations[TROTTING].FrameIndex[4] = 17;
    CavalryAnimations[TROTTING].FrameDelay[4] = 600;
    CavalryAnimations[TROTTING].FrameFlag[4] = ANIMCUE_NONE;
    CavalryAnimations[TROTTING].FrameIndex[5] = 18;
    CavalryAnimations[TROTTING].FrameDelay[5] = 600;
    CavalryAnimations[TROTTING].FrameFlag[5] = ANIMCUE_NONE;

    CavalryAnimations[CHARGING].NumFrames = 6;
    CavalryAnimations[CHARGING].LoopFlag = true;
    CavalryAnimations[CHARGING].FrameIndex[0] = 19;
    CavalryAnimations[CHARGING].FrameDelay[0] = 600;
    CavalryAnimations[CHARGING].FrameFlag[0] = ANIMCUE_NONE;
    CavalryAnimations[CHARGING].FrameIndex[1] = 20;
    CavalryAnimations[CHARGING].FrameDelay[1] = 600;
    CavalryAnimations[CHARGING].FrameFlag[1] = ANIMCUE_NONE;
    CavalryAnimations[CHARGING].FrameIndex[2] = 21;
    CavalryAnimations[CHARGING].FrameDelay[2] = 600;
    CavalryAnimations[CHARGING].FrameFlag[2] = ANIMCUE_NONE;
    CavalryAnimations[CHARGING].FrameIndex[3] = 22;
    CavalryAnimations[CHARGING].FrameDelay[3] = 600;
    CavalryAnimations[CHARGING].FrameFlag[3] = ANIMCUE_NONE;
    CavalryAnimations[CHARGING].FrameIndex[4] = 23;
    CavalryAnimations[CHARGING].FrameDelay[4] = 600;
    CavalryAnimations[CHARGING].FrameFlag[4] = ANIMCUE_NONE;
    CavalryAnimations[CHARGING].FrameIndex[5] = 24;
    CavalryAnimations[CHARGING].FrameDelay[5] = 600;
    CavalryAnimations[CHARGING].FrameFlag[5] = ANIMCUE_NONE;

    CavalryAnimations[SWORDOUT].NumFrames = 1;
    CavalryAnimations[SWORDOUT].LoopFlag = false;
    CavalryAnimations[SWORDOUT].FrameIndex[0] = 25;
    CavalryAnimations[SWORDOUT].FrameDelay[0] = 0;
    CavalryAnimations[SWORDOUT].FrameFlag[0] = ANIMCUE_NONE;

    CavalryAnimations[DEAD].NumFrames = 1;
    CavalryAnimations[DEAD].LoopFlag = false;
    CavalryAnimations[DEAD].FrameIndex[0] = 26;
    CavalryAnimations[DEAD].FrameDelay[0] = 0;
    CavalryAnimations[DEAD].FrameFlag[0] = ANIMCUE_NONE;

    CavalryAnimations[MELEE].NumFrames = 7;
    CavalryAnimations[MELEE].LoopFlag = true;
    CavalryAnimations[MELEE].FrameIndex[0] = 27;
    CavalryAnimations[MELEE].FrameDelay[0] = 300;
    CavalryAnimations[MELEE].FrameFlag[0] = ANIMCUE_NONE;
    CavalryAnimations[MELEE].FrameIndex[1] = 28;
    CavalryAnimations[MELEE].FrameDelay[1] = 300;
    CavalryAnimations[MELEE].FrameFlag[1] = ANIMCUE_NONE;
    CavalryAnimations[MELEE].FrameIndex[2] = 29;
    CavalryAnimations[MELEE].FrameDelay[2] = 300;
    CavalryAnimations[MELEE].FrameFlag[2] = ANIMCUE_NONE;
    CavalryAnimations[MELEE].FrameIndex[3] = 30;
    CavalryAnimations[MELEE].FrameDelay[3] = 300;
    CavalryAnimations[MELEE].FrameFlag[3] = ANIMCUE_NONE;
    CavalryAnimations[MELEE].FrameIndex[4] = 31;
    CavalryAnimations[MELEE].FrameDelay[4] = 300;
    CavalryAnimations[MELEE].FrameFlag[4] = ANIMCUE_NONE;
    CavalryAnimations[MELEE].FrameIndex[5] = 32;
    CavalryAnimations[MELEE].FrameDelay[5] = 300;
    CavalryAnimations[MELEE].FrameFlag[5] = ANIMCUE_NONE;
    CavalryAnimations[MELEE].FrameIndex[6] = 33;
    CavalryAnimations[MELEE].FrameDelay[6] = 300;
    CavalryAnimations[MELEE].FrameFlag[6] = ANIMCUE_NONE;
    
    /*
    FOOT ARTILLERY ANIMATIONS
    */
    
    FootArtilleryAnimations[STANDING_UNLIMBERED].NumFrames = 1;
    FootArtilleryAnimations[STANDING_UNLIMBERED].LoopFlag = false;
    FootArtilleryAnimations[STANDING_UNLIMBERED].FrameIndex[0] = 0;
    FootArtilleryAnimations[STANDING_UNLIMBERED].FrameDelay[0] = 0;
    FootArtilleryAnimations[STANDING_UNLIMBERED].FrameFlag[0] = ANIMCUE_NONE;

    FootArtilleryAnimations[STANDING_LIMBERED].NumFrames = 1;
    FootArtilleryAnimations[STANDING_LIMBERED].LoopFlag = false;
    FootArtilleryAnimations[STANDING_LIMBERED].FrameIndex[0] = 24;
    FootArtilleryAnimations[STANDING_LIMBERED].FrameDelay[0] = 0;
    FootArtilleryAnimations[STANDING_LIMBERED].FrameFlag[0] = ANIMCUE_NONE;

    FootArtilleryAnimations[WALKING].NumFrames = 6;
    FootArtilleryAnimations[WALKING].LoopFlag = true;
    FootArtilleryAnimations[WALKING].FrameIndex[0] = 24;
    FootArtilleryAnimations[WALKING].FrameDelay[0] = 600;
    FootArtilleryAnimations[WALKING].FrameFlag[0] = ANIMCUE_NONE;
    FootArtilleryAnimations[WALKING].FrameIndex[1] = 25;
    FootArtilleryAnimations[WALKING].FrameDelay[1] = 600;
    FootArtilleryAnimations[WALKING].FrameFlag[1] = ANIMCUE_NONE;
    FootArtilleryAnimations[WALKING].FrameIndex[2] = 26;
    FootArtilleryAnimations[WALKING].FrameDelay[2] = 600;
    FootArtilleryAnimations[WALKING].FrameFlag[2] = ANIMCUE_NONE;
    FootArtilleryAnimations[WALKING].FrameIndex[3] = 27;
    FootArtilleryAnimations[WALKING].FrameDelay[3] = 600;
    FootArtilleryAnimations[WALKING].FrameFlag[3] = ANIMCUE_NONE;
    FootArtilleryAnimations[WALKING].FrameIndex[4] = 28;
    FootArtilleryAnimations[WALKING].FrameDelay[4] = 600;
    FootArtilleryAnimations[WALKING].FrameFlag[4] = ANIMCUE_NONE;
    FootArtilleryAnimations[WALKING].FrameIndex[5] = 29;
    FootArtilleryAnimations[WALKING].FrameDelay[5] = 600;
    FootArtilleryAnimations[WALKING].FrameFlag[5] = ANIMCUE_NONE;

    FootArtilleryAnimations[LIMBERING].NumFrames = 6;
    FootArtilleryAnimations[LIMBERING].LoopFlag = false;
    FootArtilleryAnimations[LIMBERING].FrameIndex[0] = 35;  // turning cannon
    FootArtilleryAnimations[LIMBERING].FrameDelay[0] = 600;
    FootArtilleryAnimations[LIMBERING].FrameFlag[0] = ANIMCUE_NONE;
    FootArtilleryAnimations[LIMBERING].FrameIndex[1] = 34;
    FootArtilleryAnimations[LIMBERING].FrameDelay[1] = 600;
    FootArtilleryAnimations[LIMBERING].FrameFlag[1] = ANIMCUE_NONE;
    FootArtilleryAnimations[LIMBERING].FrameIndex[2] = 33;
    FootArtilleryAnimations[LIMBERING].FrameDelay[2] = 600;
    FootArtilleryAnimations[LIMBERING].FrameFlag[2] = ANIMCUE_NONE;
    FootArtilleryAnimations[LIMBERING].FrameIndex[3] = 32;
    FootArtilleryAnimations[LIMBERING].FrameDelay[3] = 600;
    FootArtilleryAnimations[LIMBERING].FrameFlag[3] = ANIMCUE_NONE;
    FootArtilleryAnimations[LIMBERING].FrameIndex[4] = 31;
    FootArtilleryAnimations[LIMBERING].FrameDelay[4] = 600;
    FootArtilleryAnimations[LIMBERING].FrameFlag[4] = ANIMCUE_NONE;
    FootArtilleryAnimations[LIMBERING].FrameIndex[5] = 30;
    FootArtilleryAnimations[LIMBERING].FrameDelay[5] = 600;
    FootArtilleryAnimations[LIMBERING].FrameFlag[5] = ANIMCUE_NONE;

    FootArtilleryAnimations[UNLIMBERING].NumFrames = 9;
    FootArtilleryAnimations[UNLIMBERING].LoopFlag = false;
    FootArtilleryAnimations[UNLIMBERING].FrameIndex[0] = 22;  // removing limber
    FootArtilleryAnimations[UNLIMBERING].FrameDelay[0] = 600;
    FootArtilleryAnimations[UNLIMBERING].FrameFlag[0] = ANIMCUE_NONE;
    FootArtilleryAnimations[UNLIMBERING].FrameIndex[1] = 23;
    FootArtilleryAnimations[UNLIMBERING].FrameDelay[1] = 600;
    FootArtilleryAnimations[UNLIMBERING].FrameFlag[1] = ANIMCUE_NONE;
    FootArtilleryAnimations[UNLIMBERING].FrameIndex[2] = 24;
    FootArtilleryAnimations[UNLIMBERING].FrameDelay[2] = 600;
    FootArtilleryAnimations[UNLIMBERING].FrameFlag[2] = ANIMCUE_NONE;
    FootArtilleryAnimations[UNLIMBERING].FrameIndex[3] = 30;  // turning cannon
    FootArtilleryAnimations[UNLIMBERING].FrameDelay[3] = 600;
    FootArtilleryAnimations[UNLIMBERING].FrameFlag[3] = ANIMCUE_NONE;
    FootArtilleryAnimations[UNLIMBERING].FrameIndex[4] = 31;
    FootArtilleryAnimations[UNLIMBERING].FrameDelay[4] = 600;
    FootArtilleryAnimations[UNLIMBERING].FrameFlag[4] = ANIMCUE_NONE;
    FootArtilleryAnimations[UNLIMBERING].FrameIndex[5] = 32;
    FootArtilleryAnimations[UNLIMBERING].FrameDelay[5] = 600;
    FootArtilleryAnimations[UNLIMBERING].FrameFlag[5] = ANIMCUE_NONE;
    FootArtilleryAnimations[UNLIMBERING].FrameIndex[6] = 33;
    FootArtilleryAnimations[UNLIMBERING].FrameDelay[6] = 600;
    FootArtilleryAnimations[UNLIMBERING].FrameFlag[6] = ANIMCUE_NONE;
    FootArtilleryAnimations[UNLIMBERING].FrameIndex[7] = 34;
    FootArtilleryAnimations[UNLIMBERING].FrameDelay[7] = 600;
    FootArtilleryAnimations[UNLIMBERING].FrameFlag[7] = ANIMCUE_NONE;
    FootArtilleryAnimations[UNLIMBERING].FrameIndex[8] = 35;
    FootArtilleryAnimations[UNLIMBERING].FrameDelay[8] = 600;
    FootArtilleryAnimations[UNLIMBERING].FrameFlag[8] = ANIMCUE_NONE;

    FootArtilleryAnimations[SHOOTING].NumFrames = 23;
    FootArtilleryAnimations[SHOOTING].LoopFlag = true;
    FootArtilleryAnimations[SHOOTING].FrameIndex[0] = 11; // firing sequence
    FootArtilleryAnimations[SHOOTING].FrameDelay[0] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[0] = ANIMCUE_CANNONFIRE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[1] = 12;
    FootArtilleryAnimations[SHOOTING].FrameDelay[1] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[1] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[2] = 13;
    FootArtilleryAnimations[SHOOTING].FrameDelay[2] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[2] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[3] = 14;
    FootArtilleryAnimations[SHOOTING].FrameDelay[3] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[3] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[4] = 15;
    FootArtilleryAnimations[SHOOTING].FrameDelay[4] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[4] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[5] = 16;
    FootArtilleryAnimations[SHOOTING].FrameDelay[5] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[5] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[6] = 17;
    FootArtilleryAnimations[SHOOTING].FrameDelay[6] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[6] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[7] = 18;
    FootArtilleryAnimations[SHOOTING].FrameDelay[7] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[7] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[8] = 19;
    FootArtilleryAnimations[SHOOTING].FrameDelay[8] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[8] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[9] = 20;
    FootArtilleryAnimations[SHOOTING].FrameDelay[9] = 10;
    FootArtilleryAnimations[SHOOTING].FrameFlag[9] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[10] = 1; // pushing back sequence
    FootArtilleryAnimations[SHOOTING].FrameDelay[10] = 80;
    FootArtilleryAnimations[SHOOTING].FrameFlag[10] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[11] = 2;
    FootArtilleryAnimations[SHOOTING].FrameDelay[11] = 80;
    FootArtilleryAnimations[SHOOTING].FrameFlag[11] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[12] = 3;
    FootArtilleryAnimations[SHOOTING].FrameDelay[12] = 80;
    FootArtilleryAnimations[SHOOTING].FrameFlag[12] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[13] = 4;
    FootArtilleryAnimations[SHOOTING].FrameDelay[13] = 80;
    FootArtilleryAnimations[SHOOTING].FrameFlag[13] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[14] = 5;
    FootArtilleryAnimations[SHOOTING].FrameDelay[14] = 80;
    FootArtilleryAnimations[SHOOTING].FrameFlag[14] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[15] = 6;
    FootArtilleryAnimations[SHOOTING].FrameDelay[15] = 80;
    FootArtilleryAnimations[SHOOTING].FrameFlag[15] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[16] = 7; // reloading sequence
    FootArtilleryAnimations[SHOOTING].FrameDelay[16] = 160;
    FootArtilleryAnimations[SHOOTING].FrameFlag[16] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[17] = 8;
    FootArtilleryAnimations[SHOOTING].FrameDelay[17] = 160;
    FootArtilleryAnimations[SHOOTING].FrameFlag[17] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[18] = 9;
    FootArtilleryAnimations[SHOOTING].FrameDelay[18] = 160;
    FootArtilleryAnimations[SHOOTING].FrameFlag[18] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[19] = 10;
    FootArtilleryAnimations[SHOOTING].FrameDelay[19] = 160;
    FootArtilleryAnimations[SHOOTING].FrameFlag[19] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[20] = 9;
    FootArtilleryAnimations[SHOOTING].FrameDelay[20] = 160;
    FootArtilleryAnimations[SHOOTING].FrameFlag[20] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[21] = 8;
    FootArtilleryAnimations[SHOOTING].FrameDelay[21] = 160;
    FootArtilleryAnimations[SHOOTING].FrameFlag[21] = ANIMCUE_NONE;
    FootArtilleryAnimations[SHOOTING].FrameIndex[22] = 7;
    FootArtilleryAnimations[SHOOTING].FrameDelay[22] = 160;
    FootArtilleryAnimations[SHOOTING].FrameFlag[22] = ANIMCUE_NONE;

    FootArtilleryAnimations[DEAD].NumFrames = 1;
    FootArtilleryAnimations[DEAD].LoopFlag = false;
    FootArtilleryAnimations[DEAD].FrameIndex[0] = 21;
    FootArtilleryAnimations[DEAD].FrameDelay[0] = 0;
    FootArtilleryAnimations[DEAD].FrameFlag[0] = ANIMCUE_NONE;

    /*
    HORSE ARTILLERY ANIMATIONS
    */


    HorseArtilleryAnimations[STANDING_UNLIMBERED].NumFrames = 1;
    HorseArtilleryAnimations[STANDING_UNLIMBERED].LoopFlag = false;
    HorseArtilleryAnimations[STANDING_UNLIMBERED].FrameIndex[0] = 7;
    HorseArtilleryAnimations[STANDING_UNLIMBERED].FrameDelay[0] = 0;
    HorseArtilleryAnimations[STANDING_UNLIMBERED].FrameFlag[0] = ANIMCUE_NONE;

    HorseArtilleryAnimations[STANDING_LIMBERED].NumFrames = 1;
    HorseArtilleryAnimations[STANDING_LIMBERED].LoopFlag = false;
    HorseArtilleryAnimations[STANDING_LIMBERED].FrameIndex[0] = 0;
    HorseArtilleryAnimations[STANDING_LIMBERED].FrameDelay[0] = 0;
    HorseArtilleryAnimations[STANDING_LIMBERED].FrameFlag[0] = ANIMCUE_NONE;

    HorseArtilleryAnimations[WALKING].NumFrames = 5;
    HorseArtilleryAnimations[WALKING].LoopFlag = true;
    HorseArtilleryAnimations[WALKING].FrameIndex[0] = 31;
    HorseArtilleryAnimations[WALKING].FrameDelay[0] = 600;
    HorseArtilleryAnimations[WALKING].FrameFlag[0] = ANIMCUE_NONE;
    HorseArtilleryAnimations[WALKING].FrameIndex[1] = 32;
    HorseArtilleryAnimations[WALKING].FrameDelay[1] = 600;
    HorseArtilleryAnimations[WALKING].FrameFlag[1] = ANIMCUE_NONE;
    HorseArtilleryAnimations[WALKING].FrameIndex[2] = 33;
    HorseArtilleryAnimations[WALKING].FrameDelay[2] = 600;
    HorseArtilleryAnimations[WALKING].FrameFlag[2] = ANIMCUE_NONE;
    HorseArtilleryAnimations[WALKING].FrameIndex[3] = 34;
    HorseArtilleryAnimations[WALKING].FrameDelay[3] = 600;
    HorseArtilleryAnimations[WALKING].FrameFlag[3] = ANIMCUE_NONE;
    HorseArtilleryAnimations[WALKING].FrameIndex[4] = 35;
    HorseArtilleryAnimations[WALKING].FrameDelay[4] = 600;
    HorseArtilleryAnimations[WALKING].FrameFlag[4] = ANIMCUE_NONE;

    HorseArtilleryAnimations[LIMBERING].NumFrames = 7;
    HorseArtilleryAnimations[LIMBERING].LoopFlag = false;
    HorseArtilleryAnimations[LIMBERING].FrameIndex[0] = 25;
    HorseArtilleryAnimations[LIMBERING].FrameDelay[0] = 600;
    HorseArtilleryAnimations[LIMBERING].FrameFlag[0] = ANIMCUE_NONE;
    HorseArtilleryAnimations[LIMBERING].FrameIndex[1] = 24;
    HorseArtilleryAnimations[LIMBERING].FrameDelay[1] = 600;
    HorseArtilleryAnimations[LIMBERING].FrameFlag[1] = ANIMCUE_NONE;
    HorseArtilleryAnimations[LIMBERING].FrameIndex[2] = 23;
    HorseArtilleryAnimations[LIMBERING].FrameDelay[2] = 600;
    HorseArtilleryAnimations[LIMBERING].FrameFlag[2] = ANIMCUE_NONE;
    HorseArtilleryAnimations[LIMBERING].FrameIndex[3] = 22;
    HorseArtilleryAnimations[LIMBERING].FrameDelay[3] = 600;
    HorseArtilleryAnimations[LIMBERING].FrameFlag[3] = ANIMCUE_NONE;
    HorseArtilleryAnimations[LIMBERING].FrameIndex[4] = 28;
    HorseArtilleryAnimations[LIMBERING].FrameDelay[4] = 600;
    HorseArtilleryAnimations[LIMBERING].FrameFlag[4] = ANIMCUE_NONE;
    HorseArtilleryAnimations[LIMBERING].FrameIndex[5] = 29;
    HorseArtilleryAnimations[LIMBERING].FrameDelay[5] = 600;
    HorseArtilleryAnimations[LIMBERING].FrameFlag[5] = ANIMCUE_NONE;
    HorseArtilleryAnimations[LIMBERING].FrameIndex[6] = 30;
    HorseArtilleryAnimations[LIMBERING].FrameDelay[6] = 600;
    HorseArtilleryAnimations[LIMBERING].FrameFlag[6] = ANIMCUE_NONE;

    HorseArtilleryAnimations[UNLIMBERING].NumFrames = 10;
    HorseArtilleryAnimations[UNLIMBERING].LoopFlag = false;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[0] = 1;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[0] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[0] = ANIMCUE_NONE;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[1] = 2;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[1] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[1] = ANIMCUE_NONE;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[2] = 3;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[2] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[2] = ANIMCUE_NONE;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[3] = 4;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[3] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[3] = ANIMCUE_NONE;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[4] = 5;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[4] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[4] = ANIMCUE_NONE;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[5] = 6;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[5] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[5] = ANIMCUE_NONE;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[6] = 22;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[6] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[6] = ANIMCUE_NONE;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[7] = 23;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[7] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[7] = ANIMCUE_NONE;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[8] = 24;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[8] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[8] = ANIMCUE_NONE;
    HorseArtilleryAnimations[UNLIMBERING].FrameIndex[9] = 25;
    HorseArtilleryAnimations[UNLIMBERING].FrameDelay[9] = 600;
    HorseArtilleryAnimations[UNLIMBERING].FrameFlag[9] = ANIMCUE_NONE;

    HorseArtilleryAnimations[SHOOTING].NumFrames = 17;
    HorseArtilleryAnimations[SHOOTING].LoopFlag = true;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[0] = 11;  // firing
    HorseArtilleryAnimations[SHOOTING].FrameDelay[0] = 100;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[0] = ANIMCUE_CANNONFIRE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[1] = 12;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[1] = 10;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[1] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[2] = 13;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[2] = 10;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[2] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[3] = 14;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[3] = 10;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[3] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[4] = 15;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[4] = 10;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[4] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[5] = 16; // pushing back ?
    HorseArtilleryAnimations[SHOOTING].FrameDelay[5] = 80;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[5] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[6] = 17;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[6] = 80;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[6] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[7] = 18;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[7] = 80;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[7] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[8] = 19;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[8] = 80;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[8] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[9] = 20;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[9] = 80;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[9] = ANIMCUE_NONE;

    HorseArtilleryAnimations[SHOOTING].FrameIndex[10] = 7; // reloading
    HorseArtilleryAnimations[SHOOTING].FrameDelay[10] = 160;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[10] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[11] = 8;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[11] = 160;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[11] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[12] = 9;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[12] = 160;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[12] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[13] = 10;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[13] = 160;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[13] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[14] = 9;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[14] = 160;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[14] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[15] = 8;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[15] = 160;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[15] = ANIMCUE_NONE;
    HorseArtilleryAnimations[SHOOTING].FrameIndex[16] = 7;
    HorseArtilleryAnimations[SHOOTING].FrameDelay[16] = 160;
    HorseArtilleryAnimations[SHOOTING].FrameFlag[16] = ANIMCUE_NONE;

    HorseArtilleryAnimations[DEAD].NumFrames = 1;
    HorseArtilleryAnimations[DEAD].LoopFlag = false;
    HorseArtilleryAnimations[DEAD].FrameIndex[0] = 21;
    HorseArtilleryAnimations[DEAD].FrameDelay[0] = 0;
    HorseArtilleryAnimations[DEAD].FrameFlag[0] = ANIMCUE_NONE;

    
    /*
    OFFICER ANIMATIONS
    */

    OfficerAnimations[STANDING].NumFrames = 1;
    OfficerAnimations[STANDING].LoopFlag = false;
    OfficerAnimations[STANDING].FrameIndex[0] = 0; 
    OfficerAnimations[STANDING].FrameDelay[0] = 0;
    OfficerAnimations[STANDING].FrameFlag[0] = ANIMCUE_NONE;

    OfficerAnimations[WALKING].NumFrames = 6;
    OfficerAnimations[WALKING].LoopFlag = true;
    OfficerAnimations[WALKING].FrameIndex[0] = 1; 
    OfficerAnimations[WALKING].FrameDelay[0] = 600;
    OfficerAnimations[WALKING].FrameFlag[0] = ANIMCUE_NONE;
    OfficerAnimations[WALKING].FrameIndex[1] = 2; 
    OfficerAnimations[WALKING].FrameDelay[1] = 600;
    OfficerAnimations[WALKING].FrameFlag[1] = ANIMCUE_NONE;
    OfficerAnimations[WALKING].FrameIndex[2] = 3;  
    OfficerAnimations[WALKING].FrameDelay[2] = 600;
    OfficerAnimations[WALKING].FrameFlag[2] = ANIMCUE_NONE;
    OfficerAnimations[WALKING].FrameIndex[3] = 4;
    OfficerAnimations[WALKING].FrameDelay[3] = 600;
    OfficerAnimations[WALKING].FrameFlag[3] = ANIMCUE_NONE;
    OfficerAnimations[WALKING].FrameIndex[4] = 5;
    OfficerAnimations[WALKING].FrameDelay[4] = 600;
    OfficerAnimations[WALKING].FrameFlag[4] = ANIMCUE_NONE;
    OfficerAnimations[WALKING].FrameIndex[5] = 6;
    OfficerAnimations[WALKING].FrameDelay[5] = 600;
    OfficerAnimations[WALKING].FrameFlag[5] = ANIMCUE_NONE;

    OfficerAnimations[GALLOPING].NumFrames = 6;
    OfficerAnimations[GALLOPING].LoopFlag = true;
    OfficerAnimations[GALLOPING].FrameIndex[0] = 7; 
    OfficerAnimations[GALLOPING].FrameDelay[0] = 600;
    OfficerAnimations[GALLOPING].FrameFlag[0] = ANIMCUE_NONE;
    OfficerAnimations[GALLOPING].FrameIndex[1] = 8; 
    OfficerAnimations[GALLOPING].FrameDelay[1] = 600;
    OfficerAnimations[GALLOPING].FrameFlag[1] = ANIMCUE_NONE;
    OfficerAnimations[GALLOPING].FrameIndex[2] = 9;  
    OfficerAnimations[GALLOPING].FrameDelay[2] = 600;
    OfficerAnimations[GALLOPING].FrameFlag[2] = ANIMCUE_NONE;
    OfficerAnimations[GALLOPING].FrameIndex[3] = 10;
    OfficerAnimations[GALLOPING].FrameDelay[3] = 600;
    OfficerAnimations[GALLOPING].FrameFlag[3] = ANIMCUE_NONE;
    OfficerAnimations[GALLOPING].FrameIndex[4] = 11;
    OfficerAnimations[GALLOPING].FrameDelay[4] = 600;
    OfficerAnimations[GALLOPING].FrameFlag[4] = ANIMCUE_NONE;
    OfficerAnimations[GALLOPING].FrameIndex[5] = 12;
    OfficerAnimations[GALLOPING].FrameDelay[5] = 600;
    OfficerAnimations[GALLOPING].FrameFlag[5] = ANIMCUE_NONE;

    OfficerAnimations[TROTTING].NumFrames = 6;
    OfficerAnimations[TROTTING].LoopFlag = true;
    OfficerAnimations[TROTTING].FrameIndex[0] = 13; 
    OfficerAnimations[TROTTING].FrameDelay[0] = 600;
    OfficerAnimations[TROTTING].FrameFlag[0] = ANIMCUE_NONE;
    OfficerAnimations[TROTTING].FrameIndex[1] = 14; 
    OfficerAnimations[TROTTING].FrameDelay[1] = 600;
    OfficerAnimations[TROTTING].FrameFlag[1] = ANIMCUE_NONE;
    OfficerAnimations[TROTTING].FrameIndex[2] = 15;  
    OfficerAnimations[TROTTING].FrameDelay[2] = 600;
    OfficerAnimations[TROTTING].FrameFlag[2] = ANIMCUE_NONE;
    OfficerAnimations[TROTTING].FrameIndex[3] = 16;
    OfficerAnimations[TROTTING].FrameDelay[3] = 600;
    OfficerAnimations[TROTTING].FrameFlag[3] = ANIMCUE_NONE;
    OfficerAnimations[TROTTING].FrameIndex[4] = 17;
    OfficerAnimations[TROTTING].FrameDelay[4] = 600;
    OfficerAnimations[TROTTING].FrameFlag[4] = ANIMCUE_NONE;
    OfficerAnimations[TROTTING].FrameIndex[5] = 18;
    OfficerAnimations[TROTTING].FrameDelay[5] = 600;
    OfficerAnimations[TROTTING].FrameFlag[5] = ANIMCUE_NONE;

    OfficerAnimations[CHARGING].NumFrames = 6;
    OfficerAnimations[CHARGING].LoopFlag = true;
    OfficerAnimations[CHARGING].FrameIndex[0] = 19; 
    OfficerAnimations[CHARGING].FrameDelay[0] = 600;
    OfficerAnimations[CHARGING].FrameFlag[0] = ANIMCUE_NONE;
    OfficerAnimations[CHARGING].FrameIndex[1] = 20; 
    OfficerAnimations[CHARGING].FrameDelay[1] = 600;
    OfficerAnimations[CHARGING].FrameFlag[1] = ANIMCUE_NONE;
    OfficerAnimations[CHARGING].FrameIndex[2] = 21;  
    OfficerAnimations[CHARGING].FrameDelay[2] = 600;
    OfficerAnimations[CHARGING].FrameFlag[2] = ANIMCUE_NONE;
    OfficerAnimations[CHARGING].FrameIndex[3] = 22;
    OfficerAnimations[CHARGING].FrameDelay[3] = 600;
    OfficerAnimations[CHARGING].FrameFlag[3] = ANIMCUE_NONE;
    OfficerAnimations[CHARGING].FrameIndex[4] = 23;
    OfficerAnimations[CHARGING].FrameDelay[4] = 600;
    OfficerAnimations[CHARGING].FrameFlag[4] = ANIMCUE_NONE;
    OfficerAnimations[CHARGING].FrameIndex[5] = 24;
    OfficerAnimations[CHARGING].FrameDelay[5] = 600;
    OfficerAnimations[CHARGING].FrameFlag[5] = ANIMCUE_NONE;
// 76
    OfficerAnimations[SHOOTING].NumFrames = 3;
    OfficerAnimations[SHOOTING].LoopFlag = true;
    OfficerAnimations[SHOOTING].FrameIndex[0] = 27; 
    OfficerAnimations[SHOOTING].FrameDelay[0] = 3600;
    OfficerAnimations[SHOOTING].FrameFlag[0] = ANIMCUE_NONE;
    OfficerAnimations[SHOOTING].FrameIndex[1] = 25; 
    OfficerAnimations[SHOOTING].FrameDelay[1] = 18600;
    OfficerAnimations[SHOOTING].FrameFlag[1] = ANIMCUE_NONE;
    OfficerAnimations[SHOOTING].FrameIndex[2] = 26;  
    OfficerAnimations[SHOOTING].FrameDelay[2] = 600;
    OfficerAnimations[SHOOTING].FrameFlag[2] = ANIMCUE_NONE;

    OfficerAnimations[ORDERING].NumFrames = 6;
    OfficerAnimations[ORDERING].LoopFlag = true;
    OfficerAnimations[ORDERING].FrameIndex[0] = 29; 
    OfficerAnimations[ORDERING].FrameDelay[0] = 600;
    OfficerAnimations[ORDERING].FrameFlag[0] = ANIMCUE_NONE;
    OfficerAnimations[ORDERING].FrameIndex[1] = 30; 
    OfficerAnimations[ORDERING].FrameDelay[1] = 600;
    OfficerAnimations[ORDERING].FrameFlag[1] = ANIMCUE_NONE;
    OfficerAnimations[ORDERING].FrameIndex[2] = 31;  
    OfficerAnimations[ORDERING].FrameDelay[2] = 600;
    OfficerAnimations[ORDERING].FrameFlag[2] = ANIMCUE_NONE;
    OfficerAnimations[ORDERING].FrameIndex[3] = 32;  
    OfficerAnimations[ORDERING].FrameDelay[3] = 600;
    OfficerAnimations[ORDERING].FrameFlag[3] = ANIMCUE_NONE;

    OfficerAnimations[DEAD].NumFrames = 1;
    OfficerAnimations[DEAD].LoopFlag = false;
    OfficerAnimations[DEAD].FrameIndex[0] = 28; 
    OfficerAnimations[DEAD].FrameDelay[0] = 0;
    OfficerAnimations[DEAD].FrameFlag[0] = ANIMCUE_NONE;



    /*
    EFFECTS ANIMATIONS
    */

    EffectsAnimations[EFFECT_DEADUNIT].NumFrames = 1;
    EffectsAnimations[EFFECT_DEADUNIT].LoopFlag = false;
    EffectsAnimations[EFFECT_DEADUNIT].FrameIndex[0] = 0;
    EffectsAnimations[EFFECT_DEADUNIT].FrameDelay[0] = 0;
    EffectsAnimations[EFFECT_DEADUNIT].FrameFlag[0] = ANIMCUE_NONE;

    EffectsAnimations[EFFECT_FLAG].NumFrames = 1;
    EffectsAnimations[EFFECT_FLAG].LoopFlag = false;
    EffectsAnimations[EFFECT_FLAG].FrameIndex[0] = 0;
    EffectsAnimations[EFFECT_FLAG].FrameDelay[0] = 0;
    EffectsAnimations[EFFECT_FLAG].FrameFlag[0] = ANIMCUE_NONE;

    EffectsAnimations[EFFECT_FLAGFLAPPING].NumFrames = 4;
    EffectsAnimations[EFFECT_FLAGFLAPPING].LoopFlag = true;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameIndex[0] = 0;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameDelay[0] = 200;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameFlag[0] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameIndex[1] = 1;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameDelay[1] = 200;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameFlag[1] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameIndex[2] = 2;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameDelay[2] = 200;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameFlag[2] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameIndex[3] = 3;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameDelay[3] = 200;
    EffectsAnimations[EFFECT_FLAGFLAPPING].FrameFlag[3] = ANIMCUE_NONE;

    EffectsAnimations[EFFECT_EXPLOSION_LONG].NumFrames = 16;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].LoopFlag = false;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[0] = 0;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[0] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[0] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[1] = 1;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[1] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[1] = ANIMCUE_EXPLOSION;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[2] = 2;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[2] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[2] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[3] = 3;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[3] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[3] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[4] = 4;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[4] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[4] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[5] = 5;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[5] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[5] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[6] = 6;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[6] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[6] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[7] = 7;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[7] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[7] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[8] = 8;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[8] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[8] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[9] = 9;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[9] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[9] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[10] = 10;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[10] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[10] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[11] = 11;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[11] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[11] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[12] = 12;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[12] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[12] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[13] = 13;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[13] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[13] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[14] = 14;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[14] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[14] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameIndex[15] = 15;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameDelay[15] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_LONG].FrameFlag[15] = ANIMCUE_ENDOFSEQUENCE;


    EffectsAnimations[EFFECT_EXPLOSION_SHORT].NumFrames = 9;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].LoopFlag = false;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameIndex[0] = 0;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameDelay[0] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameFlag[0] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameIndex[1] = 1;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameDelay[1] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameFlag[1] = ANIMCUE_EXPLOSION;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameIndex[2] = 2;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameDelay[2] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameFlag[2] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameIndex[3] = 3;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameDelay[3] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameFlag[3] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameIndex[4] = 4;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameDelay[4] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameFlag[4] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameIndex[5] = 5;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameDelay[5] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameFlag[5] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameIndex[6] = 6;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameDelay[6] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameFlag[6] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameIndex[7] = 7;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameDelay[7] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameFlag[7] = ANIMCUE_NONE;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameIndex[8] = 8;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameDelay[8] = 25;
    EffectsAnimations[EFFECT_EXPLOSION_SHORT].FrameFlag[8] = ANIMCUE_ENDOFSEQUENCE; 

    /*
    FLAG ANIMATIONS
    */
    
    FlagAnimations[FLAG_NORMAL].NumFrames = 1;
    FlagAnimations[FLAG_NORMAL].LoopFlag = false;
    FlagAnimations[FLAG_NORMAL].FrameIndex[0] = 0;
    FlagAnimations[FLAG_NORMAL].FrameDelay[0] = 0;
    FlagAnimations[FLAG_NORMAL].FrameFlag[0] = ANIMCUE_NONE;

    FlagAnimations[FLAG_FLAPPING].NumFrames = 4;
    FlagAnimations[FLAG_FLAPPING].LoopFlag = true;
    FlagAnimations[FLAG_FLAPPING].FrameIndex[0] = 0;
    FlagAnimations[FLAG_FLAPPING].FrameDelay[0] = 200;
    FlagAnimations[FLAG_FLAPPING].FrameFlag[0] = ANIMCUE_NONE;
    FlagAnimations[FLAG_FLAPPING].FrameIndex[1] = 1;
    FlagAnimations[FLAG_FLAPPING].FrameDelay[1] = 200;
    FlagAnimations[FLAG_FLAPPING].FrameFlag[1] = ANIMCUE_NONE;
    FlagAnimations[FLAG_FLAPPING].FrameIndex[2] = 2;
    FlagAnimations[FLAG_FLAPPING].FrameDelay[2] = 200;
    FlagAnimations[FLAG_FLAPPING].FrameFlag[2] = ANIMCUE_NONE;
    FlagAnimations[FLAG_FLAPPING].FrameIndex[3] = 3;
    FlagAnimations[FLAG_FLAPPING].FrameDelay[3] = 200;
    FlagAnimations[FLAG_FLAPPING].FrameFlag[3] = ANIMCUE_NONE;


    /*
    FOOT MAN ANIMATIONS
    */

    FootManAnimations[STANDING].NumFrames = 1;
    FootManAnimations[STANDING].LoopFlag = false;
    FootManAnimations[STANDING].FrameIndex[0] = 0;
    FootManAnimations[STANDING].FrameDelay[0] = 0;
    FootManAnimations[STANDING].FrameFlag[0] = ANIMCUE_NONE;

    FootManAnimations[WALKING].NumFrames = 6;
    FootManAnimations[WALKING].LoopFlag = true;
    FootManAnimations[WALKING].FrameIndex[0] = 1;
    FootManAnimations[WALKING].FrameDelay[0] = 600;
    FootManAnimations[WALKING].FrameFlag[0] = ANIMCUE_NONE;    
    FootManAnimations[WALKING].FrameIndex[1] = 2;
    FootManAnimations[WALKING].FrameDelay[1] = 600;
    FootManAnimations[WALKING].FrameFlag[1] = ANIMCUE_NONE;    
    FootManAnimations[WALKING].FrameIndex[2] = 3;
    FootManAnimations[WALKING].FrameDelay[2] = 600;
    FootManAnimations[WALKING].FrameFlag[2] = ANIMCUE_NONE;    
    FootManAnimations[WALKING].FrameIndex[3] = 4;
    FootManAnimations[WALKING].FrameDelay[3] = 600;
    FootManAnimations[WALKING].FrameFlag[3] = ANIMCUE_NONE;    
    FootManAnimations[WALKING].FrameIndex[4] = 5;
    FootManAnimations[WALKING].FrameDelay[4] = 600;
    FootManAnimations[WALKING].FrameFlag[4] = ANIMCUE_NONE;    
    FootManAnimations[WALKING].FrameIndex[5] = 6;
    FootManAnimations[WALKING].FrameDelay[5] = 600;
    FootManAnimations[WALKING].FrameFlag[5] = ANIMCUE_NONE;

    FootManAnimations[RUNNING].NumFrames = 6;
    FootManAnimations[RUNNING].LoopFlag = true;
    FootManAnimations[RUNNING].FrameIndex[0] = 7;
    FootManAnimations[RUNNING].FrameDelay[0] = 600;
    FootManAnimations[RUNNING].FrameFlag[0] = ANIMCUE_NONE;    
    FootManAnimations[RUNNING].FrameIndex[1] = 8;
    FootManAnimations[RUNNING].FrameDelay[1] = 600;
    FootManAnimations[RUNNING].FrameFlag[1] = ANIMCUE_NONE;    
    FootManAnimations[RUNNING].FrameIndex[2] = 9;
    FootManAnimations[RUNNING].FrameDelay[2] = 600;
    FootManAnimations[RUNNING].FrameFlag[2] = ANIMCUE_NONE;    
    FootManAnimations[RUNNING].FrameIndex[3] = 10;
    FootManAnimations[RUNNING].FrameDelay[3] = 600;
    FootManAnimations[RUNNING].FrameFlag[3] = ANIMCUE_NONE;    
    FootManAnimations[RUNNING].FrameIndex[4] = 11;
    FootManAnimations[RUNNING].FrameDelay[4] = 600;
    FootManAnimations[RUNNING].FrameFlag[4] = ANIMCUE_NONE;    
    FootManAnimations[RUNNING].FrameIndex[5] = 12;
    FootManAnimations[RUNNING].FrameDelay[5] = 600;
    FootManAnimations[RUNNING].FrameFlag[5] = ANIMCUE_NONE;

    FootManAnimations[DEAD].NumFrames = 1;
    FootManAnimations[DEAD].LoopFlag = false;
    FootManAnimations[DEAD].FrameIndex[0] = 13;
    FootManAnimations[DEAD].FrameDelay[0] = 0;
    FootManAnimations[DEAD].FrameFlag[0] = ANIMCUE_NONE;    

    /*
    HORSE MAN ANIMATIONS
    */


    HorseManAnimations[STANDING].NumFrames = 1;
    HorseManAnimations[STANDING].LoopFlag = false;
    HorseManAnimations[STANDING].FrameIndex[0] = 0;
    HorseManAnimations[STANDING].FrameDelay[0] = 0;
    HorseManAnimations[STANDING].FrameFlag[0] = ANIMCUE_NONE;

    HorseManAnimations[WALKING].NumFrames = 6;
    HorseManAnimations[WALKING].LoopFlag = true;
    HorseManAnimations[WALKING].FrameIndex[0] = 1;
    HorseManAnimations[WALKING].FrameDelay[0] = 600;
    HorseManAnimations[WALKING].FrameFlag[0] = ANIMCUE_NONE;    
    HorseManAnimations[WALKING].FrameIndex[1] = 2;
    HorseManAnimations[WALKING].FrameDelay[1] = 600;
    HorseManAnimations[WALKING].FrameFlag[1] = ANIMCUE_NONE;    
    HorseManAnimations[WALKING].FrameIndex[2] = 3;
    HorseManAnimations[WALKING].FrameDelay[2] = 600;
    HorseManAnimations[WALKING].FrameFlag[2] = ANIMCUE_NONE;    
    HorseManAnimations[WALKING].FrameIndex[3] = 4;
    HorseManAnimations[WALKING].FrameDelay[3] = 600;
    HorseManAnimations[WALKING].FrameFlag[3] = ANIMCUE_NONE;    
    HorseManAnimations[WALKING].FrameIndex[4] = 5;
    HorseManAnimations[WALKING].FrameDelay[4] = 600;
    HorseManAnimations[WALKING].FrameFlag[4] = ANIMCUE_NONE;    
    HorseManAnimations[WALKING].FrameIndex[5] = 6;
    HorseManAnimations[WALKING].FrameDelay[5] = 600;
    HorseManAnimations[WALKING].FrameFlag[5] = ANIMCUE_NONE;

    HorseManAnimations[GALLOPING].NumFrames = 6;
    HorseManAnimations[GALLOPING].LoopFlag = true;
    HorseManAnimations[GALLOPING].FrameIndex[0] = 7;
    HorseManAnimations[GALLOPING].FrameDelay[0] = 600;
    HorseManAnimations[GALLOPING].FrameFlag[0] = ANIMCUE_NONE;    
    HorseManAnimations[GALLOPING].FrameIndex[1] = 8;
    HorseManAnimations[GALLOPING].FrameDelay[1] = 600;
    HorseManAnimations[GALLOPING].FrameFlag[1] = ANIMCUE_NONE;    
    HorseManAnimations[GALLOPING].FrameIndex[2] = 9;
    HorseManAnimations[GALLOPING].FrameDelay[2] = 600;
    HorseManAnimations[GALLOPING].FrameFlag[2] = ANIMCUE_NONE;    
    HorseManAnimations[GALLOPING].FrameIndex[3] = 10;
    HorseManAnimations[GALLOPING].FrameDelay[3] = 600;
    HorseManAnimations[GALLOPING].FrameFlag[3] = ANIMCUE_NONE;    
    HorseManAnimations[GALLOPING].FrameIndex[4] = 11;
    HorseManAnimations[GALLOPING].FrameDelay[4] = 600;
    HorseManAnimations[GALLOPING].FrameFlag[4] = ANIMCUE_NONE;    
    HorseManAnimations[GALLOPING].FrameIndex[5] = 12;
    HorseManAnimations[GALLOPING].FrameDelay[5] = 600;
    HorseManAnimations[GALLOPING].FrameFlag[5] = ANIMCUE_NONE;

    HorseManAnimations[TROTTING].NumFrames = 6;
    HorseManAnimations[TROTTING].LoopFlag = true;
    HorseManAnimations[TROTTING].FrameIndex[0] = 13;
    HorseManAnimations[TROTTING].FrameDelay[0] = 600;
    HorseManAnimations[TROTTING].FrameFlag[0] = ANIMCUE_NONE;    
    HorseManAnimations[TROTTING].FrameIndex[1] = 14;
    HorseManAnimations[TROTTING].FrameDelay[1] = 600;
    HorseManAnimations[TROTTING].FrameFlag[1] = ANIMCUE_NONE;    
    HorseManAnimations[TROTTING].FrameIndex[2] = 15;
    HorseManAnimations[TROTTING].FrameDelay[2] = 600;
    HorseManAnimations[TROTTING].FrameFlag[2] = ANIMCUE_NONE;    
    HorseManAnimations[TROTTING].FrameIndex[3] = 16;
    HorseManAnimations[TROTTING].FrameDelay[3] = 600;
    HorseManAnimations[TROTTING].FrameFlag[3] = ANIMCUE_NONE;    
    HorseManAnimations[TROTTING].FrameIndex[4] = 17;
    HorseManAnimations[TROTTING].FrameDelay[4] = 600;
    HorseManAnimations[TROTTING].FrameFlag[4] = ANIMCUE_NONE;    
    HorseManAnimations[TROTTING].FrameIndex[5] = 18;
    HorseManAnimations[TROTTING].FrameDelay[5] = 600;
    HorseManAnimations[TROTTING].FrameFlag[5] = ANIMCUE_NONE;


    HorseManAnimations[DEAD].NumFrames = 1;
    HorseManAnimations[DEAD].LoopFlag = false;
    HorseManAnimations[DEAD].FrameIndex[0] = 19;
    HorseManAnimations[DEAD].FrameDelay[0] = 0;
    HorseManAnimations[DEAD].FrameFlag[0] = ANIMCUE_NONE;    






}

AnimationControl::~AnimationControl(void) {

}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
