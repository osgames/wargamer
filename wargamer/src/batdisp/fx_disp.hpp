/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FX_DISP_HPP
#define FX_DISP_HPP

#include "fx.hpp"
#include "bdispob.hpp"
#include "bdispute.hpp"
#include "hexiter.hpp"
#include "batcord.hpp"
#include "zlist.hpp"
#include "sprlib.hpp"
#include "animctrl.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : FX_DISP
        Description : Classes for displaying spot effects on battlefield
        
-------------------------------------------------------------------------------------------------------------------*/

using namespace BattleDisplay;

namespace BattleDisplay {

    void drawEffects(Displayer * d_display, const HexIterator& iter, CPBattleData batData, SWG_Sprite::SpriteLibrary* sprites);

}

// scale at which graphics are drawn
// static const EffectPixelScale = ((float)BattleDisplayUtility::S_HexGraphicSize.x() * 1.6);
// static const float EffectPixelScaleRecip = (float) 1.0 / (float)EffectPixelScale;

/*
Main effect display object class
*/

class BattleEffectDisplayObject : public DisplayObject {

    public:

        BattleEffectDisplayObject(const BattleEffectDisplayObject& ob);
        BattleEffectDisplayObject& operator = (const BattleEffectDisplayObject& ob);

        BattleEffectDisplayObject(const SWG_Sprite::SpriteBlock* sprite, HexIterator::Scale scale) :
            d_sprite(sprite),
            d_scale(scale) { }


        // Setup(const SWG_Sprite::SpriteBlock* sprite, PixelDimension height) {
        //     d_sprite = sprite;
        //     d_height = height;
        // }


        virtual ~BattleEffectDisplayObject() { }
        virtual void draw(DrawDIB* dib, const PixelPoint& p) const;


    private:

        // PixelDimension d_height;
        HexIterator::Scale d_scale;
        const SWG_Sprite::SpriteBlock* d_sprite;

        BattleMeasure::BattleTime::Tick d_LastTick;

};





/*
Reference pointer stuff
*/

class RefPtrBattleEffectDisplayObject : public RefPtr<BattleEffectDisplayObject> {

    typedef RefPtr<BattleEffectDisplayObject> Super;

    // Don't allow any constructors except for creator
    RefPtrBattleEffectDisplayObject();

    public:

        RefPtrBattleEffectDisplayObject(const RefPtrBattleEffectDisplayObject& ptr) : Super(ptr) { }

        // Creator constructor
        RefPtrBattleEffectDisplayObject(const SWG_Sprite::SpriteBlock* sprite, HexIterator::Scale scale) :
            Super(new BattleEffectDisplayObject(sprite, scale)) { }

        operator RefPtr<DisplayObject> () const {
            return RefPtr<DisplayObject>(value());
        }
        
};




#endif

