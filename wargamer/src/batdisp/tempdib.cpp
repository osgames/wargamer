/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Get a temporary DIB
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "tempdib.hpp"
#include "dib.hpp"
#include "random.hpp"
#include "misc.hpp"

TempDIB::~TempDIB()
{
   delete d_dib;
}

DrawDIBDC* TempDIB::get(PixelDimension w, PixelDimension h)
{
   if(d_dib == 0)
   {
      d_dib = new DrawDIBDC(w, h);
   }
   else if( (w > d_dib->getWidth()) || (h > d_dib->getHeight()) )
   {
      // int newWidth = maximum(static_cast<int>(w), d_dib->getWidth());
      // int newHeight = maximum(static_cast<int>(h), d_dib->getHeight());
      int newWidth = maximum(w, d_dib->getWidth());
      int newHeight = maximum(h, d_dib->getHeight());
      d_dib->resize(newWidth, newHeight);
   }

   return d_dib;
}



/*
 * Overlap Masks
 */

// Comparison function needed for STL

bool operator < (const MaskDIBKey& k1, const MaskDIBKey& k2)
{
   if(k1.d_w != k2.d_w)
      return (k1.d_w < k2.d_w);
   else if(k1.d_h != k2.d_h)
      return (k1.d_h < k2.d_h);
   else if(k1.d_dx != k2.d_dx)
      return (k1.d_dx < k2.d_dx);
   else  // if(k1.d_dy != k2.d_dy)
      return (k1.d_dy < k2.d_dy);
}

bool operator == (const MaskDIBKey& k1, const MaskDIBKey& k2)
{
   return (k1.d_w == k2.d_w) &&
          (k1.d_h == k2.d_h) &&
          (k1.d_dx == k2.d_dx) &&
          (k1.d_dy == k2.d_dy);
}

bool operator < (const MaskDIB& lhs, const MaskDIB& rhs)
{
   return lhs.get() < rhs.get();
}

bool operator == (const MaskDIB& lhs, const MaskDIB& rhs)
{
   return lhs.get() == rhs.get();
}



bool MaskDIB::create(const MaskDIBKey& params)
{
   // ASSERT(d_dib.get() == 0);
   ASSERT(d_dib == 0);

   ASSERT(params.d_w > 0);
   ASSERT(params.d_h > 0);

   // Create a DIB

   // auto_ptr<DrawDIB> ptr(new DrawDIB(params.d_w, params.d_h));
   // d_dib = ptr;

   // d_dib = auto_ptr<DrawDIB>(new DrawDIB(params.d_w, params.d_h));
   d_dib = new DrawDIB(params.d_w, params.d_h);
   // ASSERT(d_dib.get() != 0);
   ASSERT(d_dib != 0);

   // if(d_dib.get() != 0)
   if(d_dib != 0)
   {
      // Create mask inside DIB according to parameters

      // DrawDIB* dib = d_dib.get();
      DrawDIB* dib = d_dib;

      dib->setMaskColour(MaskColor);

      /*
       * Set up values
       */

      size_t offset;
      size_t lineOffset;
      int xSize;
      int ySize;
      UBYTE* destPtr;

      if(params.d_dx < 0)
      {
         ASSERT(params.d_dy == 0);
         offset = dib->getStorageWidth();
         lineOffset = -1;
         destPtr = dib->getAddress(params.d_w-1, 0);
         xSize = params.d_h;
         ySize = params.d_w;
      }
      else if(params.d_dx > 0)
      {
         ASSERT(params.d_dy == 0);
         offset = dib->getStorageWidth();
         lineOffset = +1;
         destPtr = dib->getAddress(0, 0);
         xSize = params.d_h;
         ySize = params.d_w;
      }
      else if(params.d_dy < 0)
      {
         ASSERT(params.d_dx == 0);
         offset = +1;
         lineOffset = -dib->getStorageWidth();
         destPtr = dib->getAddress(0, params.d_h-1);
         xSize = params.d_w;
         ySize = params.d_h;
      }
      else if(params.d_dy > 0)
      {
         ASSERT(params.d_dx == 0);
         offset = +1;
         lineOffset = dib->getStorageWidth();
         destPtr = dib->getAddress(0, 0);
         xSize = params.d_w;
         ySize = params.d_h;
      }

      RandomNumber rand(params.d_w + params.d_h);

      int range = ySize * 2;
      int chance = ySize;

      for(int y = 0; y < ySize; ++y, destPtr += lineOffset, --chance)
      {
         ASSERT(chance > 0);

         UBYTE* linePtr = destPtr;

         for(int x = 0; x < xSize; ++x, linePtr += offset)
         {
            if(rand(range) < chance)
            {
               *linePtr = MaskColor;
            }
            else
            {
               *linePtr = NonMaskColor;
            }
         }
      }
   }

   // return success if DIB was created

// return (d_dib.get() != 0);;
   return (d_dib != 0);;
}


void MaskDIB::release()
{
   delete d_dib;
   d_dib = 0;
}

const DrawDIB* OverlapMaskList::get(const MaskDIBKey& params)
{
// ItemType::const_iterator found = d_items.find(params);
   ItemType::iterator found = d_items.find(params);
   if(found == d_items.end())
   {
      // Not found so create a new one

      MaskDIB mask(params);

      // pair<ItemType::iterator,bool> result;
      // result = d_items.insert(make_pair(params, mask));
      // ASSERT(result.second);
      d_items[params] = mask;

      ASSERT(d_items.find(params) != d_items.end());

      return mask.get();
   }
   else
      return (*found).second.get();
}

OverlapMaskList::~OverlapMaskList()
{
   for(ItemType::iterator it(d_items.begin()); it != d_items.end(); ++it)
   {
      (*it).second.release();
   }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
