/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * In-game Message processing
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_msg.hpp"
#include "aic_util.hpp"
#include "aic_sdat.hpp"
#include "aic_town.hpp"
#include "aic_res.hpp"
#ifdef DEBUG
#include "cmsgutil.hpp"
#include "simpstr.hpp"
#endif

void AIC_MessageProcess::increasePriority(AIC_CODELET_ID id, ITown itown)
{
   if (itown != NoTown)
   {
      const AIC_TownInfo& townInfo = d_towns->find(itown);
      d_sData->addPriority(id, townInfo.importance());
   }
   else
      d_sData->addPriority(id, d_towns->totalImportance() / d_towns->entries());
}

void AIC_MessageProcess::run(unsigned int r)
{
#ifdef DEBUG
   d_sData->logWin("Processing Messages(%d)", r);
#endif

   AIC_Utility util(d_sData);

   while(d_msgList.size())
   {
      const CampaignMessageInfo& msg = d_msgList.front();

      switch(msg.id())
      {
         case CampaignMessages::ArrivedAtTown:
            increasePriority(AIC_CODE_Create, msg.where());
            break;
         case CampaignMessages::NewUnitBuilt:
            {
               const AIC_TownInfo& townInfo = d_towns->find(msg.where());
               d_sData->addPriority(AIC_CODE_Resource, townInfo.importance());
               d_sData->addPriority(AIC_CODE_Reorganize, townInfo.importance());
            }
            break;
         case CampaignMessages::MetAtAndAwait:
            increasePriority(AIC_CODE_Create, msg.where());
            break;
         case CampaignMessages::TakenCommand:
            increasePriority(AIC_CODE_Create, msg.where());
            break;
         case CampaignMessages::MetAndAwait:
            increasePriority(AIC_CODE_Create, msg.where());
            break;
         case CampaignMessages::Stranded:
            break;
         case CampaignMessages::MovingInForBattle:
            break;
         case CampaignMessages::HoldingBackAt8:
            break;
         case CampaignMessages::AvoidingContact:
            break;
         case CampaignMessages::PreparingForBattle:
            break;
         case CampaignMessages::JoiningBattle:
            break;
         case CampaignMessages::EngagingInBattle:
            break;
         case CampaignMessages::GeneralKilled:
            util.increaseCaution();
            d_sData->addPriority(AIC_CODE_Reorganize, MulDiv(10, d_sData->priorityRange(), 100));
            break;
         case CampaignMessages::UnitDestroyed:
            util.increaseCaution();
            d_sData->addPriority(AIC_CODE_Reorganize, MulDiv(10, d_sData->priorityRange(), 100));
            break;
         case CampaignMessages::CannotCommand:
            break;
         case CampaignMessages::NotNoble:
            break;
         case CampaignMessages::TownTaken:
            util.increaseAgression();
            util.decreaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::TownLost:
            util.decreaseAgression();
            util.increaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::HearBattle:
            break;
         case CampaignMessages::EncounteredEnemy:
            increasePriority(AIC_CODE_Create, msg.where());
            break;
         case CampaignMessages::ChangedAggression:
            break;
         case CampaignMessages::MortarShellExploded:
            break;
         case CampaignMessages::PoorConstruction:
            break;
         case CampaignMessages::BreachedFort:
            util.decreaseAgression();
            util.decreaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::FortBreached:
            util.increaseAgression();
            util.increaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::TownSurrender:
            util.increaseAgression();
            util.decreaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::FortDestroyed:
            util.increaseAgression();
            util.increaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::BrokenSiege:
            util.increaseAgression();
            util.increaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::SiegeEnded:
            util.increaseAgression();
            util.decreaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::StartSiege:
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::SiegeStarted:
            util.decreaseAgression();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::AttemptedSiege:
            util.increaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::SiegeAttempted:
            util.increaseAgression();
            util.decreaseCaution();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::RaidingTown:
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::TownRaided:
            util.increaseAgression();
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::NotInstallSupply:
            break;
         case CampaignMessages::NotInstallSupplyDead:
            break;
         case CampaignMessages::SupplyDepotInstalled:
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::NotInstallFort:
            break;
         case CampaignMessages::NotInstallFortDead:
            break;
         case CampaignMessages::FortUpgraded:
            increasePriority(AIC_CODE_Create, msg.where());
            increasePriority(AIC_CODE_World, msg.where());
            break;
         case CampaignMessages::EngineerNotFieldWork:
            break;
         case CampaignMessages::FieldWorkUpgraded:
            break;
         case CampaignMessages::FieldWorkComplete:
            break;
         case CampaignMessages::BridgeCollapse:
            break;
         case CampaignMessages::TownFire:
            break;
         case CampaignMessages::LeaderSick:
            break;
         case CampaignMessages::LeaderRecovered:
            break;
         case CampaignMessages::AlliedDisillusion:
            break;
         case CampaignMessages::SHQInChaos:
            break;
         case CampaignMessages::EnemyPlansCaptured:
            break;
         case CampaignMessages::SquabblingLeaders:
            break;
         case CampaignMessages::PopularEnthusiasm:
            break;
         case CampaignMessages::ArmisticeStart:
            break;
         case CampaignMessages::ArmisticeEnd:
            break;

         default:
            FORCEASSERT("AI does not understand message");
            break;
      }
      d_msgList.pop();
   }

   priority(0);
}

void AIC_MessageProcess::init()
{
}

void AIC_MessageProcess::timeUpdate(TimeTick t)
{
}

void AIC_MessageProcess::addMessage(const CampaignMessageInfo& msg)
{
   d_msgList.push(msg);

   // addPriority(1000);      // should be based on cp and town's priorities
   addPriority(d_sData->priorityRange());      // 100%

#ifdef DEBUG
   if(d_sData)    // It may not be fully initialised yet
   {
      SimpleString s;

      CampaignMessageFormat::messageToText(s, msg, d_sData->campData());
      d_sData->logWin("Report: %s", s.toStr());
   }
#endif
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
