/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ObjectiveWindow_HPP
#define ObjectiveWindow_HPP

#ifndef __cplusplus
#error ObjectiveWindow.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI ObjectiveWindows
 *
 *----------------------------------------------------------------------
 */

class ImpObjectiveDisplay;
class AIC_StrategyData;
class AIC_ObjectiveList;

class ObjectiveDisplay
{
      // Disable copying
      ObjectiveDisplay(const ObjectiveDisplay&);      //lint !e1714 !e1526... not referenced or defined
      ObjectiveDisplay& operator=(const ObjectiveDisplay&);      //lint !e1714 !e1526... not referenced or defined
   public:
      ObjectiveDisplay(AIC_ObjectiveList* objectives, const AIC_StrategyData* sData);
      ~ObjectiveDisplay();

      void update();

   private:
      ImpObjectiveDisplay* d_imp;
};



#endif /* ObjectiveWindow_HPP */
