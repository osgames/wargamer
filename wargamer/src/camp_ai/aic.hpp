/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_HPP
#define AIC_HPP

#ifndef __cplusplus
#error aic.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI main class
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"


#if defined(NO_WG_DLL)
    #define CAMP_AI_DLL
#elif defined(EXPORT_CAMP_AI_DLL)
    #define CAMP_AI_DLL __declspec(dllexport)
#else
    #define CAMP_AI_DLL __declspec(dllimport)
#endif

/*
 * Insulated AI class
 */

class CampaignData;
class CampaignAIList;
class EventList;
#ifdef DEBUG
#include "gamedefs.hpp"
class DrawDIBDC;
class PixelPoint;
class IMapWindow;
#endif
class CampaignMessageInfo;
class FileReader;
class FileWriter;

class CampaignAI
{
	CampaignAIList* d_aiList;
	// bool				 d_paused;

	// Unimplemented Copy constructor

	CampaignAI(const CampaignAI& ai);      //lint !e1714 !e1526 ... not referenced or defined
	CampaignAI& operator = (const CampaignAI& ai);      //lint !e1714 !e1526 ... not referenced or defined

 public:
 	CAMP_AI_DLL CampaignAI(const CampaignData* campData, EventList* eventList);
	CAMP_AI_DLL ~CampaignAI();

	CAMP_AI_DLL void removeAI(Side s);
	CAMP_AI_DLL void addAI(Side s);
   CAMP_AI_DLL void start();
   CAMP_AI_DLL void pause(bool paused);
	CAMP_AI_DLL void sendMessage(const CampaignMessageInfo& msg);
		// Receive a report from a general about some event.

   CAMP_AI_DLL bool readData(FileReader& f);
   CAMP_AI_DLL bool writeData(FileWriter& f) const;

#ifdef DEBUG
	CAMP_AI_DLL void showAI(DrawDIBDC* dib, ITown itown, const PixelPoint& p) const;
   CAMP_AI_DLL void drawAI(const IMapWindow& mw);
#endif
};

#endif /* AIC_HPP */

