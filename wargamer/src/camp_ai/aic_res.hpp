/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_RES_HPP
#define AIC_RES_HPP

#ifndef __cplusplus
#error aic_res.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Resources
 *
 *----------------------------------------------------------------------
 */

#include "codelet.hpp"

class AIC_StrategyData;

class AIC_ResourceProcess : public AIC_Codelet
{
	public:
		AIC_ResourceProcess(AIC_StrategyData* sDat) : d_sData(sDat), AIC_Codelet() { }
		~AIC_ResourceProcess() { d_sData = 0; }
		void run(unsigned int r);
		void init();
      void timeUpdate(TimeTick t);

	private:
         #ifdef DEBUG
         String name() const { return("ResourceProcess"); }
         #endif

      AIC_StrategyData* d_sData;

};









#endif /* AIC_RES_HPP */

