/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_UnitInfluence_H
#define AIC_UnitInfluence_H

#ifndef __cplusplus
#error AIC_UnitInfluence.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Calculate Influence of Units on a town
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "cpdef.hpp"
#include "decayAverage.hpp"
#include <vector>

// Forward References
class AIC_StrategyData;
class AIC_TownInfo;
class AIC_UnitList;
class CampaignRoute;
class AIC_ConstUnitRef;

 /*
  * Class Definition
  */

class TownInfluence
{

   public:
      /*
       * Types
       */

      typedef float Influence;

      //lint -strong(AzJo,TownInfluence::Influence)

      class Unit
      {
         public:
            Unit() : d_influence(0), d_cp(NoCommandPosition) { }

            Unit(ConstParamCP cp, Influence i) : d_cp(cp), d_influence(i)
            {
               ASSERT(i > 0);
               ASSERT(cp != NoCommandPosition);
            }

            ~Unit() { }

            Influence influence() const
            {
               return(d_influence);

            }

            const ConstICommandPosition& cp() const
            {
               return(d_cp);

            }

            // Functions to keep STL happy

            bool operator ==(const Unit& unit) const
            {
                return(d_cp == unit.d_cp);

            }

            bool operator <(const Unit& unit) const
            {
                return(d_cp < unit.d_cp);

            }
         private:
            Influence d_influence;       // Influence relative to this objective
            ConstICommandPosition d_cp;  // Which unit this is.
      };

      typedef std::vector<Unit> UnitList;

      /*
       * Functions
       */

      // TownInfluence(AIC_StrategyData* sData, AIC_UnitList* units, const AIC_TownInfo& townInfo);
      TownInfluence(AIC_StrategyData* sData, AIC_UnitList* units);
      ~TownInfluence();

      void init(const AIC_TownInfo& townInfo);
      void reset();

      bool canAchieve();      // can AI achieve this objective?
      SPCount spNeeded();     // How much SP is needed to achieve this objective?
      SPCount spAvailable() { return d_spAvailable; }

      bool pickAndRemove( /*out*/ Unit* unit);      // Choose a random unit from list and remove it.  return true if successful

      static Influence effectivePriority(const AIC_ConstUnitRef& aiUnit);

      // Influence highestPriority() const { return d_highestPriority; }

   private:
      AIC_StrategyData* d_sData;
      AIC_UnitList* d_aiUnits;
      UnitList d_units;                      // Units that can be assigned to this Town
      Influence d_totalUnitPriority;         // Influence of units assigned
      SPCount d_spNeeded;                    // SPCount needed to take or defend this Town
      SPCount d_spAvailable;                 // Total SPCount of Units available
      // Influence d_highestPriority;

      DecayAverage<float> d_threatRange;

};



#endif /* AIC_SDAT_HPP */
