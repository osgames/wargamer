/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Data stored in Command Position
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_unit.hpp"
#include "armies.hpp"
#include "ailog.hpp"
#include "filecnk.hpp"

AIC_UnitData::AIC_UnitData() :
   // d_inUse(false),
   // d_independent(false),
   d_spCount(0),
   // d_cp(NoCommandPosition),
   // d_activity(0)
   d_objective(0),
   d_priority(0)
{

}

// AIC_UnitData::AIC_UnitData(ConstParamCP cp) :
//    d_spCount(0),
//    d_cp(cp),
//    d_objective(0),
//    d_priority(0)
// {
// }
//

AIC_UnitData::~AIC_UnitData()
{
   ASSERT(d_objective == 0);
   d_objective = 0;
}

UWORD AIC_UnitData::s_fileVersion = 0x0000;

bool AIC_UnitData::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   f >> d_spCount;
   f >> d_priority;
   // Objective is filled in when objectives are read in

   return f.isOK();
}

bool AIC_UnitData::writeData(FileWriter& f) const
{
   f << s_fileVersion;

   f << d_spCount;
   f << d_priority;

   return f.isOK();
}



void AIC_UnitData::update(const Armies* army, ConstParamCP cp)
{
   d_spCount = army->getUnitSPCount(cp, true);
}

/*
 * AIC_UnitList Functions
 */

AIC_UnitList::AIC_UnitList() :
   d_items(),
   d_side(SIDE_Neutral)
{
}

AIC_UnitList::~AIC_UnitList()
{
}

UWORD AIC_UnitList::s_fileVersion = 0x0001;

bool AIC_UnitList::readData(FileReader& f, const Armies* army)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   ASSERT(d_items.size() == 0);

   f >> d_side;

   int count;
   f >> count;

   while (count--)
   {
      CPIndex cpi;
      f >> cpi;
      ConstICommandPosition cp = army->command(cpi);
      AIC_UnitRef ur = create(cp);
      ur.unitData()->readData(f);
   }


   return f.isOK();
}

bool AIC_UnitList::writeData(FileWriter& f) const
{
   f << s_fileVersion;
   f << d_side;

   int count = d_items.size();
   f << count;
   for (const_iterator it = begin();
      it != end();
      ++it)
   {
      ConstICommandPosition cp = (*it).first;
      f << cp->getSelf();
      const AIC_UnitData& ud = (*it).second;
      ud.writeData(f);
   }


   return f.isOK();
}



AIC_UnitRef AIC_UnitList::operator[](ConstParamCP cp)
{
   ASSERT(cp != NoCommandPosition);
   ASSERT(isMember(cp));
//   ASSERT(cp->getSide() == d_side);

   Container::iterator it = d_items.find(cp);

   if(it != d_items.end())
   {
      return AIC_UnitRef((*it).first, &(*it).second);
      // return *it;
      // return (*it).second; // (*it);
   }

   throw GeneralError("cp %s not in AIC_UnitList", cp->getName());
   // return d_items[cp];  // ->getSelf();
}

AIC_ConstUnitRef AIC_UnitList::operator[](ConstParamCP cp) const
{
   ASSERT(cp != NoCommandPosition);
   ASSERT(isMember(cp));
   ASSERT(cp->getSide() == d_side);

   Container::const_iterator it = d_items.find(cp);

   if(it != d_items.end())
   {
      return AIC_ConstUnitRef((*it).first, &(*it).second);
      // return *it;
      // return (*it).second;   // (*it);
   }

   throw GeneralError("cp %s not in AIC_UnitList", cp->getName());
}

bool AIC_UnitList::isMember(ConstParamCP cp) const
{
   ASSERT(cp != NoCommandPosition);

   //--- This is possible if a nation defects
   // ASSERT(cp->getSide() == d_side);

   return d_items.find(cp) != d_items.end();

}

AIC_UnitRef AIC_UnitList::getOrCreate(ConstParamCP cp)
{
   if(!isMember(cp))
      return create(cp);
   else
      return operator[](cp);
}

AIC_UnitRef AIC_UnitList::create(ConstParamCP cp)
{
   ASSERT(cp != NoCommandPosition);
   ASSERT(!isMember(cp));
   ASSERT(cp->getSide() == d_side);

#ifdef DEBUG
    aiLog.printf("UnitList::create(%s), ref=%d", (const char*) cp->getName(), (int) cp->generic()->refCount());
#endif

   if(!isMember(cp))
   {
      // pair<Container::iterator, bool> result = d_items.insert(AIC_Unit(cp, AIC_UnitData()));
      std::pair<Container::iterator, bool> result = d_items.insert(Container::value_type(cp, AIC_UnitData()));
      ASSERT(result.second);
      // return &(*result.first);
      // return &(*result.first).second;

      return AIC_UnitRef((*result.first).first, &(*result.first).second);
      // return *result.first;

   }
   else
      return operator[](cp);
}

void AIC_UnitList::remove(ConstParamCP cp)
{
   ASSERT(cp != NoCommandPosition);
   ASSERT(isMember(cp));
   ASSERT(cp->getSide() == d_side);

#ifdef DEBUG
    aiLog.printf("UnitList::remove(%s), ref=%d", (const char*) cp->getName(), (int) cp->generic()->refCount());
#endif

   Container::iterator it = d_items.find(cp);
   if(it != d_items.end())
   {
      d_items.erase(it);
   }
}

/*
 * Create the list of Units that are on our side
 */

void AIC_UnitList::init(const Armies* army, Side side)
{
   // If it has not already been initialised by
   // virtue of being loaded from a saved game

   if (d_items.size() == 0)
   {
      ASSERT(d_items.size() == 0);
      ASSERT((d_side == SIDE_Neutral) || (d_side == side));
      ASSERT(side != SIDE_Neutral);

      d_side = side;

      ConstUnitIter unitIter(army, army->getFirstUnit(side));
      while(unitIter.sister())
      {
         ConstICommandPosition cp = unitIter.current();
         AIC_UnitRef aiUnit = create(cp);
         // ASSERT(aiUnit != 0);
         // calculate Strength, etc.
         aiUnit.update(army);   //, cp);
      }
   }
}


bool AIC_UnitList::isUnitValid(ConstParamCP cp)
{
   if(!cp->isActive() || cp->isDead())
   {
      return false;
   }

   // Check if parent is president

   ConstICommandPosition parent = cp->getParent();
   if(parent == NoCommandPosition)
      return false;

   if(parent->getRank().isLower(Rank_President))
      return false;

   return true;
}

/*
 * return SPCount in list
 */

SPCount AIC_CPList::spCount() const
{
   SPCount value = 0;

   for (const_iterator it = begin(); it != end(); ++it)
   {
      value += (*it)->spCount(true);
   }
   return value;
}


UWORD AIC_CPList::s_fileVersion = 0x0000;

bool AIC_CPList::readData(FileReader& f, const Armies* army)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   ASSERT(d_items.size() == 0);

   int count;
   f >> count;
   while (count--)
   {
      CPIndex cpi;
      f >> cpi;
      ConstICommandPosition cp = army->command(cpi);
      d_items.insert(cp);
   }


   return f.isOK();
}

bool AIC_CPList::writeData(FileWriter& f) const
{
   f << s_fileVersion;

   int count = d_items.size();
   f << count;
   for (const_iterator it = begin(); it != end(); ++it)
   {
      f << (*it)->getSelf();
   }


   return f.isOK();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
