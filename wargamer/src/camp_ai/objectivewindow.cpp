/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Objectives Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "objectiveWindow.hpp"
#include "aic_objective.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "wind.hpp"
// #include "windsave.hpp"
#include "registry.hpp"
#include "winctrl.hpp"
#include "app.hpp"
#include "scenario.hpp"
#include "simpstr.hpp"
#include "aic_sdat.hpp"
#include "thread.hpp"
#include "dc_hold.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "town.hpp"
#include "grtypes.hpp"
// #include <algo.h>
#include <vector>
#include <algorithm>


using Greenius_System::Thread;

class ImpObjectiveDisplay : public WindowBaseND, public Thread
{
   public:
      ImpObjectiveDisplay(AIC_ObjectiveList* objectives, const AIC_StrategyData* sData);
      ~ImpObjectiveDisplay();
      void update();

   private:
      enum {
         ID_TextWindow = 1,
         ID_Header
      };

      enum HeaderType {
         HD_Priority,
         HD_UnitCount,
         HD_SP,
         HD_Side,
         HD_Town,

         HD_HowMany
      };


      void create();    // Having a seperate create() removes problem of virtual functions called in constructor
      virtual void run();
      virtual LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
      void onDestroy(HWND hWnd);
      BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
      // void onPaint(HWND hWnd);
      void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
      void onSize(HWND hWnd, UINT state, int cx, int cy);
      void onVScroll(HWND hWnd, HWND hCntrl, UINT code, int pos);
      LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);

      void setScrollBar();
      SimpleString name() const;

      void printItem(HDC hdc, HeaderType ht, int y, const char* fmt, ...);

      virtual const char* className() const;


      AIC_ObjectiveList* d_objectives;
      const AIC_StrategyData* d_sData;
      Font d_font;
      unsigned int d_fontHeight;
      unsigned int d_topLineNumber;
      unsigned int d_linesOnDisplay;

      WindowControl d_headerWindow;
      StaticWindow d_textWindow;

      unsigned int d_headerIndex[HD_HowMany];
      unsigned int d_headerPos[HD_HowMany];
      unsigned int d_headerWidth[HD_HowMany];

//      static char s_regKey[];
      static char s_regWidths[];
      static char s_className[];
      static ATOM s_atom;
      static WSAV_FLAGS s_saveFlags;
};

// char ImpObjectiveDisplay::s_regKey[] = "state";
char ImpObjectiveDisplay::s_regWidths[] = "widths";
char ImpObjectiveDisplay::s_className[] = "AI_ObjectiveWindow";
ATOM ImpObjectiveDisplay::s_atom = NULL;
WSAV_FLAGS ImpObjectiveDisplay::s_saveFlags = WSAV_POSITION | WSAV_SIZE | WSAV_RESTORE | WSAV_MINIMIZE;

ImpObjectiveDisplay::ImpObjectiveDisplay(AIC_ObjectiveList* objectives, const AIC_StrategyData* sData) :
   Thread(0),      // No manager for Thread
   WindowBaseND(),
   d_objectives(objectives),
   d_sData(sData),
   d_font(),
   d_fontHeight(0),
   d_topLineNumber(0),
   d_linesOnDisplay(0),
   d_headerWindow(NULL),
   d_textWindow(NULL)
{
   ASSERT(objectives != 0);
   ASSERT(sData != 0);

   for(unsigned int i = 0; i < HD_HowMany; ++i)
   {
      d_headerIndex[i] = 0;
      d_headerPos[i] = 0;
      d_headerWidth[i] = 0;
   }
}

ImpObjectiveDisplay::~ImpObjectiveDisplay()
{
   // selfDestruct();
   // selfDestruct doesn't work because it calls DestroyWindow which results in
   // the window destruction messages being sent from the wrong thread and our
   // thread's message loop doesn't terminate, and then ~Thread locks up because
   // our thread is not handling the exit/pause signals.

   // Get window to close down
   PostMessage(getHWND(), WM_DESTROY, 0, 0);

   // wait for thread to finish

   DWORD result = WaitForSingleObject(handle(), 20000);
   ASSERT(result == WAIT_OBJECT_0);

   d_sData = 0;
   d_objectives = 0;
}

void ImpObjectiveDisplay::run()
{
   create();

   // Message Loop

   MSG msg;

   while(GetMessage(&msg, NULL, 0, 0))
   {
      // if(msg.message == WM_QUIT)
      //    break;
      TranslateMessage(&msg);
      DispatchMessage(&msg);
      if(msg.message == WM_NCDESTROY)
         break;
   }
}


void ImpObjectiveDisplay::create()
{
   createWindow(
      WS_EX_LEFT,
         NULL,
         WS_POPUP
         | WS_CAPTION
         | WS_SIZEBOX
         | WS_MINIMIZEBOX
         | WS_SYSMENU
         | WS_VSCROLL
         | WS_CLIPSIBLINGS,
      50,50,100,300,
      // r.left,                    /* init. x pos */
      // r.top,                     /* init. y pos */
      // r.right,                   /* init. x size */
      // r.bottom,                  /* init. y size */
      APP::getMainHWND(),        /* parent window */
      NULL
   );

   SetWindowText(getHWND(), name().toStr());

//   restoreWindowState(s_regKey, getHWND(), WSAV_ALL, name().toStr());
   getState(name().toStr(), s_saveFlags);

   // Get widths from registry
   if(getRegistry(s_regWidths, d_headerWidth, sizeof(d_headerWidth), name().toStr()))
   {
      unsigned int x = 0;
      for(unsigned int i = 0; i < HD_HowMany; ++i)
      {
         d_headerPos[i] = x;

         HD_ITEM item;
         item.mask = HDI_WIDTH;
         item.cxy = d_headerWidth[i];
         BOOL result = Header_SetItem(d_headerWindow.getHWND(), d_headerIndex[i], &item);
         ASSERT(result);

         x += d_headerWidth[i];
      }
   }


   show(true);

}

void ImpObjectiveDisplay::update()
{
   setScrollBar();
   // InvalidateRect(getHWND(), NULL, TRUE);
   InvalidateRect(d_textWindow.getHWND(), NULL, TRUE);
}


LRESULT ImpObjectiveDisplay::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   //lint -save -e511 ... Size incompatibility
   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
      // HANDLE_MSG(hWnd, WM_PAINT, onPaint);
      HANDLE_MSG(hWnd, WM_SIZE,     onSize);
      HANDLE_MSG(hWnd, WM_VSCROLL,  onVScroll);
      HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
      HANDLE_MSG(hWnd, WM_NOTIFY,   onNotify);

      default:
         return defProc(hWnd, msg, wParam, lParam);
   }
   //lint -restore
}

void ImpObjectiveDisplay::onDestroy(HWND hWnd)
{
   // Save widths to registry
   setRegistry(s_regWidths, d_headerWidth, sizeof(d_headerWidth), name().toStr());
   // saveWindowState(s_regKey, hWnd, WSAV_ALL, name().toStr());
   saveState(name().toStr(), s_saveFlags);
   PostQuitMessage(0);
}

BOOL ImpObjectiveDisplay::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
   LONG dbu = GetDialogBaseUnits();
   d_fontHeight = HIWORD(dbu);
   unsigned int fontWidth = LOWORD(dbu);

   LogFont lf;
   lf.height(d_fontHeight);
   d_font.set(lf);

   d_textWindow.create(
      ID_TextWindow,
      hWnd,
      0,
      WS_CHILD | SS_OWNERDRAW,
      PixelRect(0,0,0,0));

   d_headerWindow = CreateWindowEx(
          0,
          WC_HEADER,
          (LPCTSTR) NULL,
          WS_CHILD | WS_BORDER | HDS_BUTTONS | HDS_HORZ,
          0, 0, 0, 0,
          hWnd,
          (HMENU) ID_Header,
          wndInstance(hWnd),
          NULL);

   // Insert items into header

   struct HeaderItem {
      const char* d_text;
      unsigned int d_width;   // in Characters
   };

   static const HeaderItem s_items[] = {
      { "Priority", 6 },
      { "Units", 6 },
      { "SP", 6 },
      { "Side", 6 },
      { "Town", 20 },
      { 0, 0 }
   };

   unsigned int i = 0;
   unsigned int x = 0;
   for(const HeaderItem* item = s_items; item->d_text; ++item, ++i)
   {
      ASSERT(i < HD_HowMany);

      HD_ITEM hdi;

      hdi.mask = HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
      hdi.pszText = const_cast<char*>(item->d_text);
      hdi.cxy = item->d_width * fontWidth;
      hdi.cchTextMax = lstrlen(hdi.pszText);
      hdi.fmt = HDF_LEFT | HDF_STRING;

      int index = Header_InsertItem(d_headerWindow.getHWND(), i, &hdi);
      ASSERT(index >= 0);

      d_headerIndex[i] = index;
      d_headerPos[i] = x;
      d_headerWidth[i] = hdi.cxy;

      x += hdi.cxy;
   }
   ASSERT(i == HD_HowMany);


   return TRUE;
}

/*
 * Display an item
 */

void ImpObjectiveDisplay::printItem(HDC hdc, HeaderType ht, int y, const char* fmt, ...)
{
   ASSERT(ht < HD_HowMany);

   RECT r;
   r.left = d_headerPos[ht];
   r.top = y;
   r.right = r.left + d_headerWidth[ht];
   r.bottom = y + d_fontHeight;

   const size_t BufSize = 100;
   char buffer[BufSize];

#ifdef DEBUG
   buffer[BufSize-1] = 0;
#endif

   va_list args;
   va_start(args,  fmt);
   vsprintf(buffer, fmt, args);
   va_end(args);

   ASSERT(buffer[BufSize-1] == 0);

   wTextOut(hdc, r, buffer, DT_LEFT | DT_SINGLELINE | DT_TOP);
}

#ifdef _MSC_VER
      struct ObjCompare
      {
         bool operator()(const AIC_Objective& ob1, const AIC_Objective& ob2)
         {
              return ob1.priority() > ob2.priority();
         }
      };
#endif


void ImpObjectiveDisplay::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{

   if( (lpDrawItem->CtlID == ID_TextWindow) && (lpDrawItem->itemAction & ODA_DRAWENTIRE))
   {
      ASSERT(lpDrawItem->CtlType == ODT_STATIC);

      HDC hdc = lpDrawItem->hDC;

      PixelRect r = lpDrawItem->rcItem;
      // GetClientRect(hWnd, &r);

      // Clear the rectangle to white

      FillRect(hdc, &r, GetSysColorBrush(COLOR_WINDOW));

      const CampaignData* campData = d_sData->campData();
      const AIC_ObjectiveList& realObList = *d_objectives;  // d_objectives->objectiveList();

      // Copy the list so we can sort it

      std::vector<AIC_Objective> obList;
      {
         Reader lock(&realObList);
         // obList.insert(obList.begin(), realObList.begin(), realObList.end());
         // copy(realObList.begin(), realObList.end(), obList.begin());
         obList.reserve(realObList.size());
         for(AIC_ObjectiveList::const_iterator it = realObList.begin();
            it != realObList.end();
            ++it)
         {
            obList.push_back(*it);
         }
      }

      unsigned int nItems = obList.size();

      // Sort the list
#ifndef _MSC_VER
      struct ObjCompare
      {
         bool operator()(const AIC_Objective& ob1, const AIC_Objective& ob2)
         {
              return ob1.priority() > ob2.priority();
         }
      };
#endif

      std::sort(obList.begin(), obList.end(), ObjCompare());

      // Ensure that topLine is valid

      unsigned int maxLine = 0;
      if(nItems > d_linesOnDisplay)
      {
         maxLine = nItems - d_linesOnDisplay;
      }
      d_topLineNumber = minimum(maxLine, d_topLineNumber);

      DCFontHolder fh(hdc, d_font);
      TextColorHolder ch(hdc, RGB(0,0,0));
      BkModeHolder bm(hdc, TRANSPARENT);

      int y = 0;

      for(unsigned int n = d_topLineNumber;
         (n < obList.size()) && (y < r.height());
         ++n, y += d_fontHeight)
      {
         const AIC_Objective* obj = &obList[n];
         const AIC_CPList& units = obj->unitList();
         unsigned int nUnits = units.size();

//         SPCount spCount = 0;
//          for(AIC_CPList::iterator it = units.begin();
//             it != units.end();
//             ++it)
//          {
//             ConstICommandPosition cp = (*it);
//             spCount += campData->getArmies().getUnitSPCount(cp, true);
//          }

         SPCount spAlloc = units.spCount();
         SPCount spNeed = obj->spNeeded();

         const Town& town = campData->getTown(obj->town());
         const Province& prov = campData->getProvince(town.getProvince());

         printItem(hdc, HD_Priority, y, "%d:", obj->priority());
         printItem(hdc, HD_UnitCount, y, "%d:", nUnits);
         // printItem(hdc, HD_SP, y, "%d", (int)spCount);
         printItem(hdc, HD_SP, y, "%d/%d", (int)spAlloc, (int)spNeed);
         printItem(hdc, HD_Side, y, "%s", (const char *)scenario->getSideName(town.getSide()));
         printItem(hdc, HD_Town, y, "%s, %s", town.getName(), prov.getName());
      }
   }
}

void ImpObjectiveDisplay::onSize(HWND hWnd, UINT state, int cx, int cy)
{
   // Setup the windows

   // Header goes at top

   PixelRect r(0,0,cx,cy);
   WINDOWPOS wp;
   HD_LAYOUT hdl;
   hdl.prc = &r;
   hdl.pwpos = &wp;

   HRESULT result = SendMessage(d_headerWindow.getHWND(), HDM_LAYOUT, 0, (LPARAM) &hdl);
   ASSERT(result != 0);
   SetWindowPos(d_headerWindow.getHWND(), wp.hwndInsertAfter, wp.x, wp.y,
            wp.cx, wp.cy, wp.flags | SWP_SHOWWINDOW);
   d_headerWindow.show(TRUE);

   // Text Window takes remaining space

   r.top(wp.y + wp.cy);
   SetWindowPos(d_textWindow.getHWND(), HWND_TOP, r.left(), r.top(), r.width(), r.height(), 0);
   d_textWindow.show(TRUE);


   if(d_fontHeight != 0)
      d_linesOnDisplay = (r.height() + d_fontHeight - 1) / d_fontHeight;

   setScrollBar();

   InvalidateRect(hWnd, NULL, TRUE);
}

void ImpObjectiveDisplay::onVScroll(HWND hWnd, HWND hCntrl, UINT code, int pos)
{
   const AIC_ObjectiveList& obList = *d_objectives;   // d_objectives->objectiveList();
   unsigned int nItems = obList.size();

   unsigned int oldTop = d_topLineNumber;
   int newLine = d_topLineNumber;

   switch(code)
   {
   case SB_TOP:
      newLine = 0;
      break;
   case SB_BOTTOM:
      newLine = nItems - d_linesOnDisplay;
      break;
   case SB_LINEUP:
      if(newLine > 0)
         newLine--;
      break;
   case SB_LINEDOWN:
      newLine++;
      break;
   case SB_PAGEUP:
      newLine -= (d_linesOnDisplay - 1);
      break;
   case SB_PAGEDOWN:
      newLine += (d_linesOnDisplay - 1);
      break;
   case SB_THUMBPOSITION:
      newLine = pos;
      break;
   default:
      break;
   }

   int maxLine = nItems - d_linesOnDisplay;
   newLine = minimum(maxLine, newLine);
   newLine = maximum(0, newLine);

   if(oldTop != newLine)
   {
      //lint -e(732) ... signed to unsigned
      d_topLineNumber = newLine;
      update();
   }
}


void ImpObjectiveDisplay::setScrollBar()
{
   const AIC_ObjectiveList& obList = *d_objectives;   // d_objectives->objectiveList();
   unsigned int nItems = obList.size();

   SCROLLINFO si;
   si.cbSize = sizeof(si);
   si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS | SIF_DISABLENOSCROLL;
   si.nMin = 0;
   si.nMax = (nItems == 0) ? 0 : (nItems - 1);
   si.nPage = d_linesOnDisplay;
   // si.nPage = d_clientHeight / d_lineHeight;    // Fiddle about because may not be exact multiple of lineHeight
   si.nPos = d_topLineNumber;
   // if(d_atBottom && (si.nPage != d_linesOnDisplay))
   //    si.nPos++;
   si.nTrackPos = 0;
   SetScrollInfo(hWnd, SB_VERT, &si, TRUE);
}

SimpleString ImpObjectiveDisplay::name() const
{
   SimpleString s = scenario->getSideName(d_sData->side());
   s += " AI Objectives";
   return s;
}

LRESULT ImpObjectiveDisplay::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
   if(lpNMHDR->idFrom == ID_Header)
   {
      HD_NOTIFY* notify = reinterpret_cast<HD_NOTIFY*>(lpNMHDR);

      switch(lpNMHDR->code)
      {
      case HDN_BEGINTRACK:
      case HDN_ITEMCHANGING:
         return FALSE;

      case HDN_ITEMCLICK:
         // Set sorting key
         break;

      case HDN_ITEMCHANGED:
      case HDN_ENDTRACK:
      case HDN_TRACK:
      {
         ASSERT(lpNMHDR->hwndFrom == d_headerWindow.getHWND());
         int item = notify->iItem;
         unsigned int width;
         if(notify->pitem->mask & HDI_WIDTH)
         {
            width = notify->pitem->cxy;
         }
         else
         {
            HD_ITEM info;
            info.mask = HDI_WIDTH;
            BOOL result = Header_GetItem(lpNMHDR->hwndFrom, item, &info);
            ASSERT(result);
            ASSERT(info.mask & HDI_WIDTH);
            width = info.cxy;
         }

         // Convert item into array index

         int i = 0;
         while(d_headerIndex[i] != item)
         {
            ++i;
            ASSERT(i < HD_HowMany);
         }

         d_headerWidth[i] = width;

         unsigned int x = d_headerPos[i];
         while(i < (HD_HowMany - 1))
         {
            x += d_headerWidth[i];
            ++i;
            d_headerPos[i] = x;
         }

         return FALSE;
      }

      default:
         break;
      }
   }

   return TRUE;

}

const char* ImpObjectiveDisplay::className() const
{
   if(!s_atom)
   {
      WNDCLASS wc;

      wc.style = CS_OWNDC | CS_NOCLOSE;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(this);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = reinterpret_cast<HBRUSH>(1 + COLOR_WINDOW);
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;
      s_atom = RegisterClass(&wc);
   }

   ASSERT(s_atom != 0);

   return s_className;
}




ObjectiveDisplay::ObjectiveDisplay(AIC_ObjectiveList* objectives, const AIC_StrategyData* sData) :
   d_imp(new ImpObjectiveDisplay(objectives, sData))
{
   ASSERT(d_imp);
   if(d_imp)
      d_imp->start();
}


ObjectiveDisplay::~ObjectiveDisplay()
{
   delete d_imp;
   d_imp = 0;
}

void ObjectiveDisplay::update()
{
   ASSERT(d_imp);
   if(d_imp)
      d_imp->update();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.4  2003/02/15 14:42:17  greenius
 * Fixed a possible deadlock situation when starting a thread.
 * Tidied up source code, and removed using namespace from header file.
 *
 * Revision 1.3  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
