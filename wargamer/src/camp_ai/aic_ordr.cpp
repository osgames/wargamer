/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Order Units in activity
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "aic_ordr.hpp"
#include "aic_sdat.hpp"
#include "aic_objective.hpp"
#include "aic_unit.hpp"
#include "aic_town.hpp"
#include "ds_unit.hpp"        // for sendUnitOrder()
#include "minmax.hpp"
#include "realord.hpp"
#include "town.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "route.hpp"
#include "routelst.hpp"
#include "c_supply.hpp"
#include "options.hpp"
// #include <algo.h>
// #include <algobase.h>

AIC_ObjectiveOrderer::AIC_ObjectiveOrderer(AIC_StrategyData* sData, AIC_ObjectiveList* objectives, AIC_UnitList* units, const AIC_TownList* towns) :
   d_sData(sData),
   d_objectives(objectives),
   d_units(units),
   d_towns(towns)
{
   ASSERT(sData != 0);
   ASSERT(objectives != 0);
   ASSERT(units != 0);
   ASSERT(towns != 0);
}

AIC_ObjectiveOrderer::~AIC_ObjectiveOrderer()
{
   d_sData = 0;
   d_objectives = 0;
   d_units = 0;
   d_towns = 0;
}

void AIC_ObjectiveOrderer::init()
{

}

/*
 * Run the order codelet:
 *
 * This used to be based on Objectives, but i am changing it to run based
 * on units instead.
 * Instead of a unit being given an order to go direct to the Objective destination
 * it will evaluate what it needs to do to get there.
 * It may need to:
 *   - Build supply depots
 *   - Leave garrisons
 *   - Leave siege units
 *
 */


void AIC_ObjectiveOrderer::run(unsigned int rand)
{
#ifdef DEBUG
   d_sData->logWin("----------------------");
   d_sData->logWin("Order Objective(%d)", rand);
#endif

   /*
    * For now, we will process all units
    * But we will probably change it to only process those with
    * a certain priority or higher.
    */

   for (AIC_UnitList::iterator it = d_units->begin();
      it != d_units->end();
      ++it)
   {
      AIC_UnitRef aiUnit((*it).first, &(*it).second);
      procUnit(aiUnit);
   }

   priority(0);
}

namespace
{

/*
 * Is a unit doing anything worthwhile?
 * He must be at a town with hold/rest-rally
 */

bool isIdle(ConstParamCP cp)
{
   if(!cp->atTown())
      return false;

   if(!cp->isHolding() && !cp->isResting())
      return false;

   const CampaignOrder* order = cp->getLastOrder();
   if ( (order->getType() != Orders::Type::Hold) &&
        (order->getType() != Orders::Type::RestRally) )
   {
      return false;
   }

   if(cp->townOrders().entries())
      return false;

   return true;
}

};    // Private namespace

/*
 * Does a unit need supplies
 *
 * A unit is happy with >75%
 * and is unhappy at <50%
 * anywhere in between he needs a random number
 */

bool AIC_ObjectiveOrderer::needsSupply(ConstParamCP cp)
{
   if(!CampaignOptions::get(OPT_Supply))
      return false;

   Attribute supply = cp->getSupply();

   // Pick probability based on:
   //   75% = 0%
   //   50% = 100%

   const Attribute minSupply = Attribute_Range / 2;
   const Attribute maxSupply = (Attribute_Range * 3) / 4;
   const int RandRange = 0x4000;

   int prob;

   if(supply <= minSupply)
      prob = RandRange;
   else if (supply >= maxSupply)
      prob = 0;
   else
      prob = RandRange - MulDiv(RandRange, (supply - minSupply), (maxSupply - minSupply));

   ASSERT(prob >= 0);
   ASSERT(prob <= RandRange);

   if(prob)
   {
      int r = d_sData->rand(RandRange);

#ifdef DEBUG
   d_sData->logWin("NeedsSupply(%s), supply=%d, prob=%d, r=%d",
         cp->getName(),
         (int) supply,
         prob,
         r);
#endif
      return r < prob;
   }
   else
      return false;

//   return ( (prob != 0) && (d_sData->rand(RandRange) < prob) );
}


void AIC_ObjectiveOrderer::procUnit(AIC_UnitRef& aiUnit)
{
#ifdef DEBUG_FULL
   d_sData->logWin("---");
   d_sData->logWin("procUnit(%s)", aiUnit.cp()->getName());
#endif

   if(aiUnit.objective())
   {
      procUnitWithObjective(aiUnit);
   }
   else
   {
      // Unit does not have an objective
      // Consider something useful he could do

      ConstICommandPosition cp = aiUnit.cp();

      if(isIdle(cp))
      {
         procIdleUnit(aiUnit);
      }
   }
}

void AIC_ObjectiveOrderer::procIdleUnit(const AIC_UnitRef& aiUnit)
{
   ConstICommandPosition cp = aiUnit.cp();
   ITown itown = cp->getTown();
   const Town& town = d_sData->campData()->getTown(itown);

   if( (town.getSide() == d_sData->side()) &&
       !town.isSieged())
   {

      if(CampaignOrderUtil::allowed(Orders::Type::InstallSupply, cp, d_sData->campData()) &&
         d_towns->find(itown).needsSupplyDepot(),
         !town.getIsSupplySource() &&
         !town.getIsDepot() &&
         !town.isBuildingDepot() &&
          needsSupply(cp))
      {
         buildSupplyDepot(aiUnit);
         return;
      }

      // Upgrade Fort
      //    If town not at maximum fort level
      //    And not too tired and enemy is not too close

      if (CampaignOrderUtil::allowed(Orders::Type::UpgradeFort, cp, d_sData->campData()) &&
         !town.isBuildingFort() &&
         (town.getFortifications() < Town::MaxFortifications))
      {
#ifdef DEBUG
         d_sData->logWin("%s upgrades fort at %s",
               cp->getName(),
               town.getName());
#endif

         CampaignOrder order = *cp->getLastOrder();
         order.setType(Orders::Type::UpgradeFort);
         sendUnitOrder(cp, &order);
         return;
      }

      // R&R
      //    If tired.

      const Attribute maxFatigue = (Attribute_Range * 3) / 4;

      if (CampaignOrderUtil::allowed(Orders::Type::RestRally, cp, d_sData->campData()) &&
           cp->getFatigue() < maxFatigue)
      {
#ifdef DEBUG
         d_sData->logWin("%s R&R's at %s",
               cp->getName(),
               town.getName());
#endif
         CampaignOrder order = *cp->getLastOrder();
         order.setType(Orders::Type::RestRally);
         sendUnitOrder(cp, &order);
         return;
      }
   }
}

/*
 * Sends Unit order for its ultimate objective
 * this needs to be changed to look at the route
 * stepping through it to see if anything needs to be done
 * at a location such as building supply depot or dropping
 * garrison
 */

void AIC_ObjectiveOrderer::procUnitWithObjective(AIC_UnitRef& aiUnit)
{
   const AIC_Objective* objective = aiUnit.objective();
   ConstICommandPosition cp = aiUnit.cp();
   const CampaignData* campData = d_sData->campData();

#ifdef DEBUG
   d_sData->logWin("%s Has Objective %s",
      cp->getName(),
      d_sData->campData()->getTownName(objective->town()));
#endif

   /*
    * If still building a depot, etc, then wait until complete
    */

   if(cp->townOrders().entries())
      return;


   /*
    * If unit is already at the objective's destination
    * then process seperately
    */

   if (cp->atTown() && (objective->town() == cp->getTown()))
   {
      procUnitAtObjective(aiUnit);
   }
   else
   {
      /*
       * Trace route to objective and issue order to first location that may need some action.
       */

      RouteList route;
      CampaignRouteUtil::plotRoute(
         d_sData->campData(),
         route,
         cp->getPosition(),
         CampaignPosition(objective->town()),
         d_sData->side());

      SListIterR<RouteNode> routeIter(&route);
      while(++routeIter)
      {
         const RouteNode* node = routeIter.current();
         ITown itown = node->d_town;
         const Town& town = campData->getTown(itown);

         Orders::Type::Value onArrival = Orders::Type::RestRally;
         if(town.getSide() != d_sData->side())
            onArrival = Orders::Type::Hold;

         if(townNeedsAction(aiUnit, itown, onArrival))
         {
            orderUnitToTown(aiUnit, itown, onArrival);
            break;
         }
      }
   }
}

/*
 * Does a town need anything doing there?
 *
 * If so return true and fill onArrival with an appropriate order
 */

bool AIC_ObjectiveOrderer::townNeedsAction(const AIC_UnitRef& aiUnit, ITown itown, Orders::Type::Value& onArrival)
{
   const CampaignData* campData = d_sData->campData();
   const Armies* army = &campData->getArmies();
   Side enemySide = d_sData->enemySide();
   const Town& town = d_sData->campData()->getTown(itown);
   ConstICommandPosition cp = aiUnit.cp();

   /*
    * Always
    */

   if(itown == aiUnit.objective()->town())
      return true;

   /*
    * Is it an enemy fort?
    * Then drop off some units for an inactive siege.
    */

   if (shouldSiegeTown(aiUnit, itown))
   {
      // The dropping off occurs when it decides to move on

#ifdef DEBUG
      d_sData->logWin("Stopping at %s for inactive siege", town.getName());
#endif
      return true;
   }

   /*
    * Friendly fortification, then consider dropping off garrison
    */

   if(shouldDropOffGarrison(aiUnit, itown))
   {
      // actual garrison is done on departure.

#ifdef DEBUG
      d_sData->logWin("Stopping at %s to drop off garrison", town.getName());
#endif

      return true;
   }

   /*
    * Out of supply
    * Stop and build a depot
    */

   if (shouldBuildSupplyDepot(aiUnit, itown))
   {
#ifdef DEBUG
      d_sData->logWin("Stopping at %s for supplies", town.getName());
#endif
      onArrival = Orders::Type::InstallSupply;
      return true;
   }

   return false;
}

bool AIC_ObjectiveOrderer::shouldSiegeTown(const AIC_UnitRef& aiUnit, ITown itown)
{
   const Town& town = d_sData->campData()->getTown(itown);
   Side enemySide = d_sData->enemySide();
   if ( (town.getSide() == enemySide) &&
        (town.getFortifications() > 0) &&
        !town.isSieged() &&
        town.getSiegeable() )
   {
      // If already have friendly units here...

      // Probability related to VP and chokepoint

      float prob = 1.0 - 0.5 / (1 + town.getVictory(d_sData->side()));
      return boolRand(prob);
   }

   return false;
}

bool AIC_ObjectiveOrderer::shouldDropOffGarrison(const AIC_UnitRef& aiUnit, ITown itown)
{
   const Town& town = d_sData->campData()->getTown(itown);
   if (( town.getSide() == d_sData->side()) && (town.getFortifications() > 0) && !town.isGarrisoned())
   {
      // Probabilty based on distance from enemy

      const AIC_TownInfo& tinfo = d_towns->find(itown);
      Distance d = tinfo.enemyDistance();
      float prob = 1.0;

      const Distance MinDistance = MilesToDistance(108);
      const Distance HalfDistance = MilesToDistance(150);
      const float K = (HalfDistance - MinDistance + 1) * 0.5;

      if(d > MinDistance)
      {
         prob = K / (d - MinDistance + 1);
      }

      return boolRand(prob);
   }

   return false;

}

bool AIC_ObjectiveOrderer::shouldBuildSupplyDepot(const AIC_UnitRef& aiUnit, ITown itown)
{
   if(!CampaignOptions::get(OPT_Supply))
      return false;

   const Town& town = d_sData->campData()->getTown(itown);
   Side enemySide = d_sData->enemySide();

   // Must be our own town
   // Can't build if already has a depot / source

   if(!town.canBuildDepot(d_sData->side()))
      return false;

   // Is it already building one?

   if(town.isBuildingDepot()) // hasSupplyInstaller())
      return false;

   if(!d_towns->find(itown).needsSupplyDepot())
      return false;

   RouteList routeList;
   int supply = CampaignSupply::findSupplyLine(
      d_sData->campData(),
      itown,
      100,
      routeList);

   // TODO:
   // Probability 50..75%

#ifdef DEBUG
   d_sData->logWin("shouldBuildSupplyDepot(%s at %s) = %d",
      aiUnit.cp()->getName(),
      town.getName(),
      supply);
#endif

   const int minSupply = 10;
   const int maxSupply = 50;

   if(supply >= maxSupply)
      return false;
   else if(supply < minSupply)
      return true;
   else
   {
      int prob = supply - minSupply;
      int probRange = maxSupply - minSupply;
      return d_sData->rand(probRange) >= prob;
   }

//    if(supply < 75)
//    {
//       return true;
//    }

//   return false;
}

void AIC_ObjectiveOrderer::buildSupplyDepot(const AIC_UnitRef& aiUnit)
{
   ConstICommandPosition cp = aiUnit.cp();

//   ASSERT(cp->atTown());

   if (cp->atTown())
   {
#ifdef DEBUG
      const Town& town = d_sData->campData()->getTown(cp->getCloseTown());
      d_sData->logWin("Building depot at %s with %s",
         town.getName(),
         cp->getName());
#endif


      CampaignOrder order = *cp->getLastOrder();
      order.setType(Orders::Type::InstallSupply);
      sendUnitOrder(cp, &order);
   }
}

/*
 * Drop off a garrison at the unit's town
 * Return true if the entire organization has been ordered
 */

bool AIC_ObjectiveOrderer::dropOffGarrison(const AIC_UnitRef& aiUnit)
{
   const CampaignData* campData = d_sData->campData();
   const Armies* army = &campData->getArmies();
   ConstICommandPosition cp = aiUnit.cp();

   if (cp->atTown())
   {

      // ASSERT(cp->atTown());

      ITown itown = cp->getCloseTown();
      const Town& town = d_sData->campData()->getTown(itown);



   #ifdef DEBUG
      d_sData->logWin("Leaving Garrison at %s", town.getName());
   #endif

      // What sort of strength do we need?

      SPCount townStrength = campData->getTownStrength(itown, d_sData->enemySide());

      // Find appropriate units to detatch.
      // Ideally, a single non-cavalry division
      // the smallest infantry division is best

      ConstICommandPosition bestCP = NoCommandPosition;
      SPCount smallestSP = 0;
      BasicUnitType::value bestType = BasicUnitType::Infantry;

      ConstUnitIter iter(army, cp);
      while (iter.next())
      {
         ConstICommandPosition iterCP = iter.current();
         if (iterCP->getRankEnum() == Rank_Division)
         {
            // No cavalry allowed to garrison

            if(army->getNType(iterCP, BasicUnitType::Cavalry, false) == 0)
            {
               SPCount spCount = army->getUnitSPValue(iterCP);
               if ( (bestCP == NoCommandPosition) ||
                   (iterCP->isInfantry() && (bestType != BasicUnitType::Infantry)) ||
                   (spCount < smallestSP))
               {
                  bestCP = iterCP;
                  smallestSP = spCount;
                  if(iterCP->isInfantry())
                     bestType = BasicUnitType::Infantry;
                  else
                     bestType = BasicUnitType::Artillery;
               }
            }
         }
      }

      /*
       * If we have a unit then issue order
       */

      if (bestCP != NoCommandPosition)
      {
   #ifdef DEBUG
         d_sData->logWin("Issuing Garrison Order to %s at %s",
               bestCP->getName(),
               town.getName());
   #endif

         CampaignOrder order = *bestCP->getLastOrder();
         order.setType(Orders::Type::Garrison);
         order.setDestTown(itown);
         sendUnitOrder(bestCP, &order);
         return(bestCP == cp);
      }
   }

   return false;
}

/*
 * Drop off an inactive-siege at the unit's town
 * Return true if the entire organization has been ordered
 */

bool AIC_ObjectiveOrderer::dropOffSiege(const AIC_UnitRef& aiUnit)
{
   const CampaignData* campData = d_sData->campData();
   const Armies* army = &campData->getArmies();
   ConstICommandPosition cp = aiUnit.cp();

   // ASSERT(cp->atTown());
   if (cp->atTown())
   {
      ITown itown = cp->getCloseTown();
      const Town& town = d_sData->campData()->getTown(itown);

   #ifdef DEBUG
      d_sData->logWin("Leaving behind an inactive siege at %s", town.getName());
   #endif

      bool siegeActive = false;
      if(town.getVictory(d_sData->side()))
         siegeActive = true;

      // What sort of strength do we need?

      SPCount townStrength = campData->getTownStrength(itown, d_sData->enemySide());

      // Find appropriate units to detatch.

      ConstICommandPosition bestCP = NoCommandPosition;
      SPCount smallestSP = 0;

      ConstUnitIter iter(army, cp);
      while (iter.next())
      {
         ConstICommandPosition iterCP = iter.current();
         if (iterCP->getRankEnum() == Rank_Division)
         {
            SPCount spCount = army->getUnitSPValue(iterCP);
            if ( (bestCP == NoCommandPosition) ||
                (spCount < smallestSP))
            {
               bestCP = iterCP;
               smallestSP = spCount;
            }
         }
      }

      /*
       * If we have a unit then issue order
       */

      if (bestCP != NoCommandPosition)
      {
   #ifdef DEBUG
         d_sData->logWin("Issuing Siege Order to %s at %s",
               bestCP->getName(),
               town.getName());
   #endif

         CampaignOrder order = *bestCP->getLastOrder();
         order.setType(Orders::Type::Hold);
         order.setSiegeActive(siegeActive);
         order.setDestTown(itown);
         sendUnitOrder(bestCP, &order);
         return(bestCP == cp);
      }
   }

   return false;
}

/*
 * Order a unit that is about to leave a town
 * return true if it can not move out
 */

bool AIC_ObjectiveOrderer::orderUnitLeavingTown(const AIC_UnitRef& aiUnit)
{
   const CampaignData* campData = d_sData->campData();
   const Armies* army = &campData->getArmies();
   Side enemySide = d_sData->enemySide();
   ConstICommandPosition cp = aiUnit.cp();
   ASSERT(cp->atTown());
   ITown itown = cp->getTown();
   const Town& town = d_sData->campData()->getTown(itown);

   /*
    * Is it an enemy fort?
    * Then drop off some units for an inactive siege.
    */

   if (shouldSiegeTown(aiUnit, itown))
   {
      return dropOffSiege(aiUnit);
   }

   /*
    * Friendly fortification, then consider dropping off garrison
    */

   if(shouldDropOffGarrison(aiUnit, itown))
   {
      return dropOffGarrison(aiUnit);
   }

   /*
    * Out of supply
    * Stop and build a depot
    */

   if (shouldBuildSupplyDepot(aiUnit, itown))
   {
      buildSupplyDepot(aiUnit);
      return true;
   }

   /*
    * If out of supply, but shouldBuildSupplyDepot didn't work
    * don't do anything.
    */

   if (cp->getSupply() < (Attribute_Range / 2))
   {
#ifdef DEBUG
      d_sData->logWin("Not moving %s because supply is %d",
            cp->getName(),
            (int) cp->getSupply() );
#endif
      return true;
   }

   /*
    * Nothing... just move on as normal
    */


   return false;
}

void AIC_ObjectiveOrderer::orderUnitToTown(AIC_UnitRef& aiUnit, ITown itown, Orders::Type::Value onArrival)
{
   ConstICommandPosition cp = aiUnit.cp();
   const CampaignData* campData = d_sData->campData();
   const AIC_Objective* objective = aiUnit.objective();

   /*
    * If unit is about to move off, then consider various things
    * to be done:
    */

   if(cp->atTown() && (cp->getTown() != objective->town()))
   {
      if (orderUnitLeavingTown(aiUnit))
      {
         // Remove unit from Objective and return from function

         d_objectives->remove(cp);

         return;
      }
   }



   /*
    * Calculate an agression rating based on:
    * - Importance of location
    * - AI_Agression
    * - AI_Caution
    * Each unit then alters this based on:
    * - Leader attributes
    * Then applies a random number, which is used to set:
    * - Posture
    * - Agression
    */

   float agressBase = calcBaseAgression(objective);
   agressBase *= d_sData->agression();

#ifdef DEBUG
   d_sData->logWin("Agression Base = %f", static_cast<float>(agressBase));
#endif

   CampaignOrder order = *cp->getLastOrder();
   order.setType(Orders::Type::MoveTo);

   // Adjust agression with leader attributes

   float agress = agressBase;

   ILeader leader = cp->getLeader();
   if (leader != NoLeader)
   {
      const float AgressionModifier = 0.2 / static_cast<float>(Attribute_Range);
      const float InitiativeModifier = 0.3 / static_cast<float>(Attribute_Range);
      agress -= AgressionModifier * agress * static_cast<float>(Attribute_Range - leader->getAggression());
      agress -= InitiativeModifier * agress * static_cast<float>(Attribute_Range - leader->getInitiative());
#ifdef DEBUG
      d_sData->logWin("Final Agression for %s (%s) = %f",
         cp->getName(), leader->getName(),
         static_cast<float>(agress));
#endif
   }

   // Fill in agression

   int agressLevel = biassedRand(agress, Orders::Aggression::HowMany, Orders::Aggression::HowMany);
   ASSERT(agressLevel >= 0);
   ASSERT(agressLevel < Orders::Aggression::HowMany);
   Orders::Aggression::Value agressValue = static_cast<Orders::Aggression::Value>(agressLevel);

   // Bodge... don't allow maximum agression level
   if(agressValue == Orders::Aggression::Attack)
      DECREMENT(agressValue);

   order.setAggressLevel(agressValue);

#ifdef DEBUG
   d_sData->logWin("Agression Level = %s",  order.getAggressDescription());
#endif

   // Posture

   bool posture = boolRand(agress);
   Orders::Posture::Type postureType = posture ? Orders::Posture::Offensive : Orders::Posture::Defensive;
   order.posture(postureType);

#ifdef DEBUG
   d_sData->logWin("Posture = %s",  posture ? "Offensive" : "Defensive");
#endif

   // Pursue

   bool pursue = boolRand(agress);
   order.setPursue(pursue);
#ifdef DEBUG
   if(pursue)
      d_sData->logWin("Pursue enemy");
#endif

   // March to sound of guns
   //   If objective contains multiple units and agression is high

   float marchChance = agress;
   if(objective->unitList().size() > 1)
      marchChance *= 2;
   bool soundGuns = boolRand(marchChance);
   order.setSoundGuns(soundGuns);
#ifdef DEBUG
   if(soundGuns)
      d_sData->logWin("March to Sound of Guns");
#endif

   // Todo:
   //    March Speed
   order.setMoveHow(Orders::AdvancedOrders::Normal);

   /*
    * Order On Arrival:
    *
    * If unit is part of larger task force and we are
    * a low level unit, then issue an attach-to
    * otherwise issue a move-to.
    *
    * Only attaches single SP divisions.
    *
    * The units can always get unattached later on during
    * the OB Reorganize codelet.
    *
    * Todo:
    *    Consider: Garrison, Hold, etc...
    */

//          bool doAttach = (cp->getRankEnum == Rank_Division) && (cp->getRank().isLower(bestRank) && (cp->getSPCount() == 1);
//
//          if(doAttach)
//             order.setOrderOnArrival(Orders::Type::Attach);
//          else

   // order.setOrderOnArrival(Orders::Type::RestRally);
   order.setOrderOnArrival(onArrival);

   //    Auto-Garrison

   order.setDropOffGarrison(calcAutoGarrison(cp));

   //    Siege Active
   order.setSiegeActive(boolRand(agress));

   //    Auto-Storm
   order.setAutoStorm(boolRand(agress));

   /*
    * Set Destination:
    */

//          if(doAttach)
//             order.setTargetUnit(topUnit);
//          else

   // order.setDestTown(objective->town());
   order.setDestTown(itown);

   /*
    * Send the order
    */

   sendUnitOrder(cp, &order);

   /*
    * Increase Unit's priority to reduce chance of it being pulled away for another objective
    */

   const float OrderedPriorityIncrease = 2.0;   // 1.5
   aiUnit.priority(aiUnit.priority() * OrderedPriorityIncrease);


#ifdef DEBUG
   SimpleString orderText = CampaignOrderUtil::getDescription(d_sData->campData(), &order, OD_LONG);
   d_sData->logWin("Order for %s is %s",
      static_cast<const char*>(cp->getName()),
      static_cast<const char*>(orderText.toStr()));
#endif
}

void AIC_ObjectiveOrderer::procUnitAtObjective(const AIC_UnitRef& aiUnit)
{
#ifdef DEBUG
   d_sData->logWin("%s is at its objective at %s",
         aiUnit.cp()->getName(),
         d_sData->campData()->getTownName(aiUnit.objective()->town()));
#endif
   procIdleUnit(aiUnit);
}


void AIC_ObjectiveOrderer::timeUpdate(TimeTick t)
{
   // addPriority(t * d_objectives->size(), DaysToTicks(1));
   addPriority(2 * d_objectives->priority());   // Double amount
}

#ifdef DEBUG
String AIC_ObjectiveOrderer::name() const
{
   return "AIC_ObjectiveOrderer";
}
#endif


/*
 * Return a value 0..1 containing a base agression level
 * for this objective
 *
 * Value is objective's priority / maximum priority
 */

float AIC_ObjectiveOrderer::calcBaseAgression(const AIC_Objective* objective) const
{
   // Get min/max objective priorities

   // MinMax<unsigned int> minMax;
   unsigned int maxPriority = 0;

   for (AIC_ObjectiveList::const_iterator it = d_objectives->begin();
         it != d_objectives->end();
         ++it)
   {
      // minMax += (*it).priority();
      maxPriority = maximum(maxPriority, (*it).priority());

   }

   unsigned int range = maxPriority;   // minMax.maxValue() - minMax.minValue();
   if (range != 0)
   {
      return static_cast<float>(objective->priority())      // - minMax.minValue())
             /
             static_cast<float>(range);
   }
   else
      return 0.5;
}


/*
 * Pick a randum value such that:
 *    result is 0..range-1
 *    base * (range - 1) has higher probability of being picked
 *    result may be +/- variance from base * (range - 1).
 */

int AIC_ObjectiveOrderer::biassedRand(float base, unsigned int range, unsigned int variance) const
{
   // First pick a gaussian random number
   // Note it is not realy gaussian, it is just the difference
   // between two random numbers so 0 has a higher proabability

   float br = base * (range - 1);
   if(br > (range - 1))
      br = range - 1;
   if(br < 0)
      br = 0;

   int loopCount = 0;      // Prevent infinite loop
   const int MaxLoop = 100;

   unsigned int result;
   do
   {
      ++loopCount;
      ASSERT(loopCount < MaxLoop);
      if(loopCount >= MaxLoop)
         result = br;

      // -1 .. +1 biassed to 0.

      const int RandRange = 0x10000;   // -0xffff .. +0xffff
      float gauss = static_cast<float>(d_sData->rand(RandRange) - d_sData->rand(RandRange))
                     /
                    static_cast<float>(RandRange);

      // base-variance .. base+variance

      result = br + gauss * static_cast<float>(variance);
   } while( (result < 0) || (result >= range) );

   ASSERT(result >= 0);
   ASSERT(result < range);

   return result;
}

/*
 * Pick random false or true
 * if base=0, then almost always pick false
 * if base=1, then slmost always pick true
 * if base=0.5 then 50% chance
 */

bool AIC_ObjectiveOrderer::boolRand(float base) const
{
   const int RandRange = 0x10000;   // -0xffff .. +0xffff
   int baseRange = base * (RandRange - 1);
   return d_sData->rand(RandRange) <= baseRange;
}

/*
 * Decide whether to do auto-garrison drop off
 *
 * If unit is at a friendly town then decide based on town's priority
 */

bool AIC_ObjectiveOrderer::calcAutoGarrison(ConstParamCP cp) const
{
   if (cp->atTown())
   {
      const AIC_TownInfo& aiTown = d_towns->find(cp->getTown());

      float agress = static_cast<float>(aiTown.priority())
                     /
                     static_cast<float>(d_towns->maxPriority());

      return boolRand(agress);
   }

   return false;
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
