/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Codelet List
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "codelist.hpp"
#ifdef DEBUG
#include "logwin.hpp"
#endif

AIC_CodeletList::ID AIC_CodeletList::add(AIC_Codelet* codelet)
{
   ASSERT(codelet != 0);

   codelet->parent(this);
   d_items.push_back(codelet);

   addPriority(codelet->priority());

   ASSERT(d_items.size() > 0);
   ASSERT(d_items.size() < UWORD_MAX);

   return static_cast<AIC_CodeletList::ID>(d_items.size() - 1);
}

AIC_Codelet* AIC_CodeletList::get(ID id)
{
   ASSERT(id < d_items.size());
   return d_items[id];
}


void AIC_CodeletList::run(unsigned int r)
{
   // ASSERT(d_priority >= 0);

   if(d_priority > 0)
   {
      ASSERT(r < d_priority);
      // unsigned int r = (*d_random)(d_priority);

      Container::iterator i;
      for(i = d_items.begin(); i != d_items.end(); ++i)
      {
         AIC_Codelet* code = *i;

         if(r < code->priority())
         {
            code->run(r);
            return;
         }
         else
         {
            r -= code->priority();
         }
      }

      FORCEASSERT("Did not choose Codelet");
   }
}

void AIC_CodeletList::timeUpdate(TimeTick t)
{
   for(Container::iterator it = d_items.begin();
      it != d_items.end();
      ++it)
   {
      (*it)->timeUpdate(t);
   }
}

void AIC_CodeletList::init()
{
   for(Container::iterator it = d_items.begin();
      it != d_items.end();
      ++it)
   {
      (*it)->init();
   }
}

void AIC_CodeletList::deleteAll()
{
   for(Container::iterator it = d_items.begin(); it != d_items.end(); ++it)
   {
      delete *it;
      *it = 0;
   }
   d_items.erase(d_items.begin(), d_items.end());
}

#ifdef DEBUG
void AIC_CodeletList::log(LogWindow* log) const
{
   AIC_Codelet::log(log);
   log->printf("> %d Children", static_cast<int>(d_items.size()));
   for(Container::const_iterator it = d_items.begin(); it != d_items.end(); ++it)
   {
      (*it)->log(log);
   }
   log->printf("<");
}
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
