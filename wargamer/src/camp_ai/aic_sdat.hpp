/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_SDAT_HPP
#define AIC_SDAT_HPP

#ifndef __cplusplus
#error aic_sdat.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Strategy Data
 *
 *----------------------------------------------------------------------
 */

#include "resinfo.hpp"
#include "random.hpp"

#ifdef DEBUG
#include "logwin.hpp"
#endif


// class AIC_Codelet;

class Armies;
class CampaignData;

enum AIC_CODELET_ID
{
   AIC_CODE_Message,          // Process incoming messages
   AIC_CODE_Resource,         // Build new units
   AIC_CODE_World,            // Recalculate world data
   AIC_CODE_Units,            // Update AI unit lists from real world data
   AIC_CODE_Create,           // Create/Update Objectives
   AIC_CODE_Order,            // Order Units to achieve objective
   AIC_CODE_Reorganize,       // Reorganize Order of Battle and assign new SPs

   AIC_CODE_HowMany
};

/*
 * Strategy Data
 *
 * A place in which all data that may be used by the various processing
 * codelets is stored.
 *
 * This is not good practice... but saves passing pointers around all over
 * the place or defining loads of little interfaces.
 */

//lint -save -e1536 -e578 ... exposing members is intentional, also overloading some functions

class AIC_StrategyData
{
 	public:
 		AIC_StrategyData(Side s, const CampaignData* campData);
		virtual ~AIC_StrategyData();

		AIC_ResourceInformation* resInfo() { return &d_resInfo; }

		Side side() const { return d_side; }
      Side enemySide() const { return otherSide(d_side); }

		const CampaignData* campData() const { return d_campData; }
		const Armies* armies() const;

		float caution() const { return d_caution; }
		void caution(float caution) { d_caution = caution; }

		float agression() const { return d_agression; }
		void agression(float agression) { d_agression = agression; }

		/*
		 * Random Number functions
		 */

		int rand(int from, int to) { return d_random(from, to); }
			// Pick random number between from.. to
			// e.g. rand(1,10) picks 1..10
		int rand(int range) { return d_random(range); }
			// Picks random number between 0..range-1
			// e.g. rand(10), picks 0..9
		// RandomNumber& random() { return d_random; }
			// Return the random number generator

//       float normalizeThreat(float threat)
//       {
//          return d_threatRange.updateAndNormalize(threat);
//       }

		/*
		 * Utility Functions
		 */

#ifdef DEBUG
		void logWin(const char* fmt, ...);
		LogWindow* logWindow() { return d_aiLogWindow.get(); }
#endif

      // Functions Implemented at a higher level

      virtual void addPriority(AIC_CODELET_ID id, unsigned int p) = 0;      // Add Priority and return new value
      virtual void subPriority(AIC_CODELET_ID id, unsigned int p) = 0;      // Add Priority and return new value
      virtual void priority(AIC_CODELET_ID id, unsigned int p) = 0;
      virtual unsigned int priorityRange() const = 0;
   private:
#ifdef DEBUG
		LogWinPtr d_aiLogWindow;
#endif

		const CampaignData* d_campData;
		Side d_side;

		RandomNumber 		d_random;		// Random Number generator
		AIC_ResourceInformation	d_resInfo;
//      DecayAverage<float> d_threatRange;

		/*
	 	 * Aggression is used to balance attack versus defensive operations
	 	 * The priority of attack objectives is multiplied by agression.
	 	 * The aggression is increased and decreased based on what has happened
	 	 * It is increased when:
	 	 *		- After certain lengths of game time while no friendly towns have
	 	 *      been attacked, sieged, raided or taken over by enemy
	 	 *    - Whenever we succesfully fight off an attempt on one of our towns
	 	 * It is decreased (i.e. more force is used for defense), when:
	 	 *		- A friendly town is succesfully attacked.
	 	 * The amount of change can depend on the size of the towns concerned, and
	 	 * how easily the enemy were beaten off.
	 	 *
	 	 * We could consider this value changing based on our own attack success,
	 	 * but it is difficult to decide which way to take it.  e.g. if we win a
	 	 * battle, does it mean that we have ample forces attacking, thus we can
	 	 * lower our aggression, or does it mean that we are doing well, therefor
	 	 * can put all our forces into attacking to finish off the enemy.
	 	 *
	 	 * Let us keep things simple, with one function for one variable.  Thus
	 	 * aggression is a value that indicates how succesful our defense is.  A
	 	 * high value means more forces can be used for attacking, a low one that
	 	 * we need more defence.
	 	 *
	 	 * Initially it will begin with a value of 1.0
	 	 * We use a floating point number, as it likely to vary betweem 0.5 -> 2.0
	 	 * We could consider using a fixed point (e.g n/65536) or a rational
	 	 * number (i.e. A/B), but if the program is to run on a machine with
	 	 * an FPU, then we may as well make use of it.
	 	 */

		float d_agression;					// Multiplier for attack activities

		/*
	 	 * Caution is a measure of how to interpret the SP needed for actions
	 	 * The SP needed is multiplied by caution to decide how many SPs to send
	 	 * on an objective.
	 	 * A high value means that we send more SPs than needed, and a low value
	 	 * that we send less.
	 	 * The value will change as a result of the game's action:
	 	 * Caution will increase when:
	 	 *		- We lose a battle
	 	 *		- We fail to take over a town
	 	 *		- We fail to win a siege
	 	 * Caution will decrease when:
	 	 *		- We win a battle
	 	 *		- We take over a town
	 	 *		- We win a siege
	 	 * The amount of change may depend on how succesfully we win or lose
	 	 * battles.  e.g. if we lose 50% of our units, then the caution will
	 	 * increase by 50%.  If the enemy lose 50% of their units instead then
	 	 * caution may decrease by 50%.
	 	 *
	 	 * Typical values will be 0.5 -> 2.0
	 	 * Starting value of 1.0 (may be changed based on experience).
	 	 * We may keep a history of this value and make the starting value
	 	 * change based on an average value.  This means the game will adapt
	 	 * to the player after a few games.
	 	 */

		float d_caution;						// Multiplier for SP needed for activity
};


//lint -restore ... exposing members re-enable

#endif /* AIC_SDAT_HPP */

