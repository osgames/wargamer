/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI Resource Information
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "resinfo.hpp"
#include "codelet.hpp"

namespace { // private namespace

struct BuildInfo
{
   int d_buildRatio;
   BasicUnitType::value d_basicType;
   SpecialUnitType::value d_specialType;
};

static BuildInfo s_buildInfo[AIC_ResourceInformation::WB_Count] =
{
   { 50, BasicUnitType::Infantry,  SpecialUnitType::NotSpecial,   },
   { 30, BasicUnitType::Cavalry,   SpecialUnitType::NotSpecial    },
   { 15, BasicUnitType::Artillery, SpecialUnitType::NotSpecial    },
   { 2,  BasicUnitType::Special,   SpecialUnitType::Engineer      },
   { 1,  BasicUnitType::Special,   SpecialUnitType::BridgeTrain   },
   { 2,  BasicUnitType::Special,   SpecialUnitType::SiegeTrain    }
};

static const char* s_whatBuildNames[AIC_ResourceInformation::WB_Count] =
{
   "Infantry",
   "Cavalry",
   "Artillery",
   "Engineer",
   "BridgeTrain",
   "SiegeTrain"
};


};    // private namespace

AIC_ResourceInformation::AIC_ResourceInformation() :
   // d_proc(0),
   d_urgency(Mid_Urgency)
{
   for(EnumType<WhatToBuild> b = WB_First; b != WB_Count; ++b)
   {
      d_buildRatios[b] = s_buildInfo[b].d_buildRatio;
   }

}

AIC_ResourceInformation::~AIC_ResourceInformation()
{
   // d_proc = 0;    // Keep lint happy
}

#if 0
void AIC_ResourceInformation::codelet(AIC_Codelet* code)
{
   ASSERT(code != 0);
   ASSERT(d_proc == 0);

   d_proc = code;
}

void AIC_ResourceInformation::addPriority(int p)
{
   ASSERT(d_proc != 0);

   if(d_proc)  //!lint !e774 ... always true
      d_proc->addPriority(p);
}
#endif


/*
 * adjust type such that ratio (0..1) of what is required
 * eg. (WB_Infantry, 0.5), means try to maintain 50% infantry.
 *
 * Normally this is only used to increase priority of a type.
 * Other types will reduce their own priorities to compensate
 */

void AIC_ResourceInformation::adjustRatio(WhatToBuild what, float ratio)
{
   ASSERT(what < WB_Count);
   ASSERT(ratio >= 0);
   ASSERT(ratio <= 1.0);

   // calculate total of all other types

   int total = 0;
   {
      for(EnumType<WhatToBuild> b = WB_First; b != WB_Count; ++b)
      {
         if(b != what)
            total += d_buildRatios[b];
      }
   }

   const int RatioRange = 65536; // arbitrary total
   int newValue = static_cast<int>(ratio * RatioRange);
   int remainder = RatioRange - newValue;

   {
      for(EnumType<WhatToBuild> b = WB_First; b != WB_Count; ++b)
      {
         if(b == what)
            d_buildRatios[b] = newValue;
         else
            d_buildRatios[b] = MulDiv(d_buildRatios[b], remainder, total);
      }
   }

}

/*
 * alter urgency by ratio
 * eg. 0.5 will increase urgency by 50%
 *    -0.5 will decrease urgency by 50%
 */

void AIC_ResourceInformation::adjustUrgency(float ratio)
{
   ASSERT(ratio <= 1.0);
   ASSERT(ratio >= -1.0);

   if(ratio >= 0)
   {
      Urgency ceiling = Max_Urgency - d_urgency;

      d_urgency += static_cast<Urgency>(ratio * ceiling);
   }
   else
   {
      Urgency floor = d_urgency - Min_Urgency;
      d_urgency -= static_cast<Urgency>(ratio * floor);
   }
}

#ifdef DEBUG
const char* AIC_ResourceInformation::whatBuildName(WhatToBuild wb)
{
   ASSERT(wb < WB_Count);

   return s_whatBuildNames[wb];
}
#endif

BasicUnitType::value AIC_ResourceInformation::basicType(WhatToBuild wb)
{
   ASSERT(wb < WB_Count);
   return s_buildInfo[wb].d_basicType;
}

SpecialUnitType::value AIC_ResourceInformation::specialType(WhatToBuild wb)
{
   ASSERT(wb < WB_Count);
   return s_buildInfo[wb].d_specialType;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
