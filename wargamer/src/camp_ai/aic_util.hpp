/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_UTIL_HPP
#define AIC_UTIL_HPP

#ifndef __cplusplus
#error aic_util.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	AI Utility functions
 *
 *----------------------------------------------------------------------
 */


#include "gamedefs.hpp"

class AIC_StrategyData;
class AIC_Activity;
class AIC_ActivityTownList;

#if 0
class AIC_ActivityUtility
{
	public:
		AIC_ActivityUtility(AIC_StrategyData* sData) : d_sData(sData) { }
#if 0
		AIC_Activity* createActivity(AIC_Activity* parent);
			// Create an activty as a child of the parent

		void killAllActivities();
		void killActivity(AIC_Activity* activity);
			// kill activity and its children

		void ageActivities();

		void moveTown(AIC_Activity* activity, ITown itown);
		SPCount addTownSP(const AIC_ActivityTownList* towns) const;
#endif
		void eventInTown(ITown itown);

		// void log();

	private:
#if 0
		AIC_Activity* topActivity();
#endif

	private:
		AIC_StrategyData* d_sData;
};
#endif


class AIC_Utility
{
	public:
		AIC_Utility(AIC_StrategyData* sData) : d_sData(sData) { }

		void increaseCaution();
		void decreaseCaution();
		void increaseAgression();
		void decreaseAgression();

		// void increaseResourcePriority(ITown town);

	private:
		AIC_StrategyData* d_sData;
};





#endif /* AIC_UTIL_HPP */

