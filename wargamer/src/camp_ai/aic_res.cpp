/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Resources
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_res.hpp"
#include "aic_sdat.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "ds_town.hpp"
#include "unittype.hpp"
#include "muldiv.hpp"
#include "minmax.hpp"

namespace   // private namespace
{

class ResUtility
{
   public:
      typedef AIC_ResourceInformation::WhatToBuild WhatToBuild;

      ResUtility(AIC_StrategyData* sdat);

      void reset();
      void makeProvinceTable();
      IProvince pickProvince();
      WhatToBuild pickType();
      UnitType pickUnitType();
      void sendOrder();
      // unsigned int getPriority(unsigned int startPriority);

   private:
      AIC_StrategyData* d_sdat;
      IProvince         d_iProvince;
      const Province*   d_province;
      ResUtility::WhatToBuild d_wb;
      UnitType          d_ut;

      struct ProvInfo
      {
         IProvince      d_prov;
         unsigned int   d_priority;
      };

      typedef std::vector<ProvInfo> ProvTable;
      typedef ProvTable::iterator ptIterator;
      typedef ProvTable::const_iterator ptConstIterator;

      ProvTable d_provTable;

};

ResUtility::ResUtility(AIC_StrategyData* sdat) :
   d_sdat(sdat),
   d_iProvince(NoProvince),
   d_province(0),
   d_wb(AIC_ResourceInformation::WB_None),
   d_ut(NoUnitType),
   d_provTable()
{
}

void ResUtility::reset()
{
   d_iProvince = NoProvince;
   d_province = 0;
   d_wb = AIC_ResourceInformation::WB_None;
   d_ut = NoUnitType;
}

/*
 * Create a table of province priorities
 *
 * Provinces must be:
 *    Controlled by AI side
 *    Have a slot that is not being built
 *
 * priority = province's resources * (sum(emptyslots * ratio of slot type))
 */

void ResUtility::makeProvinceTable()
{
   ASSERT(d_provTable.size() == 0);

   const AIC_ResourceInformation* resInfo = d_sdat->resInfo();

   /*
    * calculate a few values
    */

   unsigned int buildPriority[BasicUnitType::HowMany];
   unsigned int totalBuildPriority = 0;

// for(EnumType<BasicUnitType::value> bt = static_cast<BasicUnitType::value>(0);
//    bt != BasicUnitType::HowMany;
//    ++bt)
   for(int bt = 0; bt   != BasicUnitType::HowMany; ++bt)
   {
      buildPriority[bt] = 0;
   }

   for(EnumType<WhatToBuild> wb = AIC_ResourceInformation::WB_First; wb != AIC_ResourceInformation::WB_Count; ++wb)
   {
      BasicUnitType::value bt = AIC_ResourceInformation::basicType(wb);
      buildPriority[bt] += resInfo->buildRatio(wb);
      totalBuildPriority += resInfo->buildRatio(wb);
   }



   ProvinceIter iter(d_sdat->campData()->getProvinces());
   while(++iter)
   {
      const Province& prov = iter.current();
      IProvince iProv = iter.currentID();    //lint !e632 ... Assignment to strong type (really should update town.hpp)

      // Check side

      if(prov.getSide() == d_sdat->side())
      {
         unsigned int priority = 0;

         // Count slots

         for(EnumType<BasicUnitType::value> bt = static_cast<BasicUnitType::value>(0);
            bt != BasicUnitType::HowMany;
            ++bt)
         {
            const BuildItem& bi = prov.getBuild(bt);

            if(!bi.isEnabled())
            {
               priority += buildPriority[bt];
            }
         }

         // add to list

         if(priority != 0)
         {
            unsigned int finalPriority = UMulDiv(priority, prov.getResource() + prov.getManPower(), totalBuildPriority);

            ProvInfo pi;
            pi.d_prov = iProv;
            pi.d_priority = finalPriority;
            d_provTable.push_back(pi);

            #ifdef DEBUG
               d_sdat->logWin("considering %s, p=%u -> %u",
                  (const char*) prov.getNameNotNull(),
                  (unsigned int) priority,
                  (unsigned int) finalPriority);
            #endif
         }
      }
   }
}

IProvince ResUtility::pickProvince()
{
    d_iProvince = NoProvince;

   if(d_provTable.size() == 0)
      return NoProvince;

   const CampaignData* campData = d_sdat->campData();
   const ProvinceList& provinces = campData->getProvinces();

   unsigned int totalPriority = 0;

   {
      for(ptConstIterator i = d_provTable.begin();
         i != d_provTable.end();
         ++i)
      {
         totalPriority += i->d_priority;
      }
   }

   if(totalPriority == 0)
        return NoProvince;

   unsigned int r = d_sdat->rand(totalPriority);      //lint !e732 ... loss of sign

   ptIterator i = d_provTable.begin();
   while(r >= i->d_priority)
   {
      r -= i->d_priority;
      ++i;
      ASSERT(i != d_provTable.end());
   }

   d_iProvince =  i->d_prov;
   i->d_prov = NoProvince;    // stop it being picked again, and help get out of loop

   if(d_iProvince != NoProvince)
   {
      d_province = &provinces[d_iProvince];

      #ifdef DEBUG
         d_sdat->logWin("Building at: %s", (const char*) d_province->getNameNotNull());
      #endif

   }

   return d_iProvince;
}

ResUtility::WhatToBuild ResUtility::pickType()
{
   ASSERT(d_iProvince != NoProvince);
   ASSERT(d_province != 0);

   const AIC_ResourceInformation* resInfo = d_sdat->resInfo();

   int total = 0;

   EnumType<WhatToBuild> wb;
   for(wb = AIC_ResourceInformation::WB_First; wb != AIC_ResourceInformation::WB_Count; ++wb)
   {
      // check that type is not already being built

      total += resInfo->buildRatio(wb);
   #ifdef DEBUG
      d_sdat->logWin("ratio[%s]=%d",
         (const char*) AIC_ResourceInformation::whatBuildName(wb),
         // (const char*) d_sdat->resInfo()->whatBuildName(wb),
         resInfo->buildRatio(wb));
   #endif
   }

   int r = d_sdat->rand(total);

   #ifdef DEBUG
      d_sdat->logWin("random=%d", r);
   #endif

   wb = AIC_ResourceInformation::WB_First;
   while(r >= resInfo->buildRatio(wb))
   {
      r -= resInfo->buildRatio(wb);
      ++wb;
      ASSERT(wb < AIC_ResourceInformation::WB_Count);
   }

   #ifdef DEBUG
   // d_sdat->logWin("Building %s", (const char*) d_sdat->resInfo()->whatBuildName(wb));
   d_sdat->logWin("Building %s", (const char*) AIC_ResourceInformation::whatBuildName(wb));
   #endif

   WhatToBuild wb1 = wb;
   d_wb = wb1;
   return d_wb;
}

#ifdef _MSC_VER
   struct PickUnit
   {
      UnitType d_type;
      int d_value;
   };
#endif


UnitType ResUtility::pickUnitType()
{
   ASSERT(d_iProvince != NoProvince);
   ASSERT(d_province != 0);
   ASSERT(d_wb != AIC_ResourceInformation::WB_None);

   const UnitTypeTable& utable = d_sdat->campData()->getUnitTypes();
   Nationality nationality = d_province->getNationality();     //lint !e613 ... possible use of NULL pointer
   BasicUnitType::value basicType = AIC_ResourceInformation::basicType(d_wb);
   SpecialUnitType::value specialType = AIC_ResourceInformation::specialType(d_wb);

   /*
    * Make a list of acceptable units, along with some value
    * that can be used to pick the best one based on urgency.
    */
#ifndef _MSC_VER
   struct PickUnit
   {
      UnitType d_type;
      int d_value;
   };
#endif

   std::vector<PickUnit> unitList;
   int totalPriority = 0;

   // find units of correct type

   MinMax<AttributePoints> attrRange;

   for(UnitType t = 0; t < utable.entries(); ++t)
   {
      const UnitTypeItem& item = utable[t];

      if( (item.getBasicType() == basicType) &&
          (item.getSpecialType() == specialType) &&
          item.isNationality(nationality) )
      {
         AttributePoints tRes = 0;
         AttributePoints tMan = 0;

         item.getTotalResource(tMan, tRes);

         attrRange += tMan + tRes;

         PickUnit pu;
         pu.d_type = t;
         pu.d_value = 0;
         unitList.push_back(pu);

      }
   }

   for(std::vector<PickUnit>::iterator it = unitList.begin();
         it != unitList.end();
         ++it)
   {
      const UnitTypeItem& item = utable[it->d_type];

      AttributePoints tRes = 0;
      AttributePoints tMan = 0;
      item.getTotalResource(tMan, tRes);

      const int pMax = 1000;
      int rMax = attrRange.maxValue();    // 2 * (AttributePoint_Range + 1);
      const int uMax = AIC_ResourceInformation::Max_Urgency + 1;
      int r = tRes + tMan;
      int u = d_sdat->resInfo()->urgency();

      int priority = 1 + pMax - MulDiv(pMax, r*uMax + u*rMax - 2*r*u, rMax*uMax);

      #ifdef DEBUG
         d_sdat->logWin("Considering %s %d/%d [u=%d/%d, r=%d/%d]",
            (const char*) item.getName(),
            priority, pMax,
            u, uMax,
            r, rMax);
      #endif

      it->d_value = priority;
      totalPriority += priority;
   }

   if(unitList.size() == 0)
   {
      #ifdef DEBUG
         d_sdat->logWin("Can not build any %s at %s",
            // (const char*) d_sdat->resInfo()->whatBuildName(d_wb),
            (const char*) AIC_ResourceInformation::whatBuildName(d_wb),
            (const char*) d_province->getNameNotNull());     //lint !e613 ... possible use of NULL pointer
      #endif
      return NoUnitType;
   }

   int r = d_sdat->rand(totalPriority);

   #ifdef DEBUG
      d_sdat->logWin("Random = %d / %d", (int) r, (int) totalPriority);
   #endif


   std::vector<PickUnit>::const_iterator i = unitList.begin();

   while(r >= i->d_value)
   {
      r -= i->d_value;
      ++i;
      ASSERT(i != unitList.end());
   }

   d_ut = i->d_type;

   #ifdef DEBUG
      d_sdat->logWin("Picked %s", (const char*) utable[d_ut].getName());
   #endif

   return d_ut;
}

void ResUtility::sendOrder()
{
   ASSERT(d_iProvince != NoProvince);
   ASSERT(d_province != 0);
   ASSERT(d_ut != NoUnitType);

   #ifdef DEBUG
      const CampaignData* campData = d_sdat->campData();
      const UnitTypeTable& utable = campData->getUnitTypes();

      d_sdat->logWin("Sending order to build %s at %s",
         (const char*) utable[d_ut].getName(),
         (const char*) d_province->getName());     //lint !e613 ... possible use of NULL pointer
   #endif

   BuildItem bi;
   bi.setWhat(d_ut);
   bi.setQuantity(1);
   bi.setAutoBuild(false);
   // bi.repoPool(false);
   bi.repoPool(true);

   BasicUnitType::value basicType = AIC_ResourceInformation::basicType(d_wb);

   TownOrder::send(d_province->getCapital(), basicType, bi);     //lint !e613 ... possible use of NULL pointer
}

// unsigned int ResUtility::getPriority(unsigned int startPriority)
// {
//    ASSERT(d_iProvince != NoProvince);
//    ASSERT(d_province != 0);
//
//    return startPriority / 2;
// }

};

void AIC_ResourceProcess::run(unsigned int r)
{
   #ifdef DEBUG
      d_sData->logWin("Process Resources");
   #endif

   ASSERT(r < priority());

   ResUtility util(d_sData);

   /*
    * Make a table of province priorities based on
    * - Ownership:
    * - What is already being built there
    * - Resource rates
    */

   util.makeProvinceTable();

   /*
    * Pick a province to do a build
    */

   for(;;)
   {
      util.reset();

      IProvince ip = util.pickProvince();
      if(ip == NoProvince)
         break;

      /*
       * Pick a type based on:
       * - What is already being built
       * - Priority of type
       */

      ResUtility::WhatToBuild wb = util.pickType();
      if(wb == AIC_ResourceInformation::WB_None)
         continue;

      /*
       * Pick a specialist type based on:
       * - urgency
       */

      UnitType ut = util.pickUnitType();
      if(ut == NoUnitType)
         continue;

      /*
       * Send build order
       */

      util.sendOrder();

      /*
       * reduce codelet priority
       */

      // priority(util.getPriority(priority()));
      subPriority(priority() / 2);        // Halve priority
   }
}

void AIC_ResourceProcess::init()
{
   // we could do an initial run to get things going.
   // but is probably cheating, better to do it in the flow of things
}

void AIC_ResourceProcess::timeUpdate(TimeTick t)
{
   addPriority(t);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
