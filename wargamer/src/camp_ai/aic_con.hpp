/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_CON_HPP
#define AIC_CON_HPP

#ifndef __cplusplus
#error aic_con.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI : Connectivity of the route network
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "array.hpp"
#include "minmax.hpp"
#include "float.h"

class CampaignData;

/*--------------------------------------------------------------------
 * Array of Connectivities
 */

class AIC_TownConnectivity
{
	public:
			typedef float Type;
			// static const Type TypeMax;

	private:

		Array<Type> d_values;
		MinMax<Type> d_range;

		AIC_TownConnectivity(const AIC_TownConnectivity&);      //lint !e1714 !e1526... not referenced or defined
		AIC_TownConnectivity& operator = (const AIC_TownConnectivity&);      //lint !e1714 !e1526... not referenced or defined
 public:
	 	AIC_TownConnectivity() :
			d_values(),
			d_range()
			// d_minValue(TypeMax),
			// d_maxValue(0)
		{
		}

		~AIC_TownConnectivity() { }

		void make(const CampaignData* campData);
		Type get(ITown t) const { return d_values[t]; }
		Type getMin() const { return d_range.minValue(); }
		Type getMax() const { return d_range.maxValue(); }
};




#endif /* AIC_CON_HPP */

