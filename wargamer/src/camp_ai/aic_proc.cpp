/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI : region Processing
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_proc.hpp"
#include "aic_act.hpp"
#include "aic_sdat.hpp"
#include "aic_util.hpp"
#include "aic_grow.hpp"
#include "aic_asgn.hpp"
#include "aic_ordr.hpp"
#include "aic_org.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "armies.hpp"

namespace      // private namespace
{

/*
 * Recalculate or update an activity's priorities based on
 * all of its data
 */

bool updatePriorities(AIC_StrategyData* sdat, AIC_Activity* activity)
{
#ifdef DEBUG
   char buffer[80];

   sdat->logWin("update %s (%s)",
      activity->description(),
      activity->priorityStr(buffer));
#endif   

   AIC_ActivityUtility util(sdat);

   // Set up a table for how the update priority is to be redistributed

   /*
    * It might be better to treat these priorities as a rational number
    * i.e. a/b.  Since most of it is concerned with ratios, such as
    * the proportion of allocated SP's to needed SP's.  This may be
    * more reliable than adding the denominator to other actions.
    * Thus we may calculate that there is a 1/10 chance of splitting an
    * activity, so we add on 1/10 of the activity's importance or total 
    * priority.
    */


   int pri[AIC_Activity::TDD_HowMany];
   for(AIC_Activity::ThingsToDo i = AIC_Activity::TDD_First; i <= AIC_Activity::TDD_Last; INCREMENT(i))
   {
      pri[i] = 0;
   }

   /*
    * Split
    */

   if(activity->spAllocated() &&                // No point splitting if no SPs.
      (activity->towns()->entries() > 1) )      // Nor if there is only 1 town!
   {
      // Split gets SPneeded for all local towns

      pri[AIC_Activity::TDD_Split] += util.addTownSP(activity->towns());

      // Split also gets SPNeeded

      pri[AIC_Activity::TDD_Split] += activity->spNeeded();
   }

   /*
    * Expand
    */

   // Expand gets SPneeded for Parent's local towns

   if(activity->parent())
   {
      pri[AIC_Activity::TDD_Expand] += util.addTownSP(activity->parent()->towns());

      // Expand gets SPAlloced

      pri[AIC_Activity::TDD_Expand] += activity->spAllocated();
   }

   /*
    * Assign SP
    */

   if(activity->spNeeded() > activity->spAllocated())
      pri[AIC_Activity::TDD_AssignSP] += activity->spNeeded() - activity->spAllocated();

   /*
    * Take SP
    */


   /*
    * Send Order
    */

   pri[AIC_Activity::TDD_SendOrder] += activity->spAllocated();

   // Redistribute as per the pri table

   int total = 0;
   for(i = AIC_Activity::TDD_First; i <= AIC_Activity::TDD_Last; INCREMENT(i))
   {
      total += pri[i];
   }

   int redist = activity->priority(AIC_Activity::TDD_Update);
   activity->priority(AIC_Activity::TDD_Update, 0);

   if(total != 0)
   {
      for(i = AIC_Activity::TDD_First; i <= AIC_Activity::TDD_Last; INCREMENT(i))
      {
         if(pri[i] != 0)
         {
            ASSERT(total != 0);

            int amount = (redist * pri[i] + total/2) / total;

            activity->addPriority(i, amount);
            redist -= amount;
            total -= pri[i];

            if(total == 0)
               break;
         }
      }
   }

   return True;      // don't lower it, because we've already cleared it.
}

/*
 * Pick a town to split off from an activity
 */

AIC_TownInfo* pickTown(AIC_StrategyData* sdat, AIC_ActivityTownList* townList)
{
   // Add up total priority of towns

   int total = 0;

   AIC_WorldInfo* world = sdat->worldInfo();

   AIC_ActivityTownIter townIter(townList);
   while(++townIter)
   {
      ITown itown = townIter.current();
      const AIC_TownInfo& townInfo = world->getTown(itown);
      total += townInfo.priority;
   }

   ASSERT(total != 0);
   if(total == 0)
      return 0;

   // Pick a random number

   int r = sdat->rand(total);
   ASSERT(r < total);

   // Pick out the town

   townIter.reset();
   while(++townIter)
   {
      ITown itown = townIter.current();
      AIC_TownInfo& townInfo = world->getTown(itown);

      if(r < townInfo.priority)
      {
#ifdef DEBUG
         sdat->logWin("Picked town %s, p=%d, total=%d", 
            sdat->campData()->getTownName(townInfo.town),
            (int) townInfo.priority,
            (int) total);
#endif
         return &townInfo;
      }

      r -= townInfo.priority;
   }

   FORCEASSERT("Error picking town");

   return 0;
}

/*
 * Attempt to create a child activity
 * Return True if a new child was created
 */

bool splitActivity(AIC_StrategyData* sdat, AIC_Activity* activity)
{
#ifdef DEBUG
   sdat->logWin("split %s", activity->description());
#endif   

   AIC_ActivityUtility util(sdat);

   /*
    * Decide if we really want to split
    * This will be more likely if:
    *   1. neededSP is greater than actualSP
    *   2. actualSP is greater than the highest town's priority
    *
    * 1. means the activity does not have enough units to take all the
    *    towns, so needs to break it down into a smaller region.
    * 2. means the activity has too many towns, and needs to split up.
    *
    * This decision should probably be made higher up, and we can assume
    * that if this function is called, then the activity really should
    * be split.
    */

   // Can't split if less than 2 towns

   if(activity->towns()->entries() <= 1)
      return false;

   /*
    * Pick a town to start the child activity with
    * This can either be:
    *    - Most important town
    *    - Random biassed towards most important town
    *    - Town with greatest connectivity
    *    - Something else
    *
    * I'm going to go for the random bias.
    */

   AIC_TownInfo* townInfo = pickTown(sdat, activity->towns());
   ASSERT(townInfo != 0);

   if(townInfo == 0)
      return False;

   // Create a new activity as a child of this one

   AIC_Activity* newActivity = util.createActivity(activity);

   // Move the selected town

   util.moveTown(newActivity, townInfo->town);

   // Give new activity a higher priority to get it going...

   newActivity->addPriority(AIC_Activity::TDD_Update, activity->importance());

   // This will need updating, to be more complex

   townInfo->happiness = newActivity->priority();

   return True;
}



}; // private namespace

/*
 * Run the Region codelet
 */

void AIC_RegionProcess::run(AIC_StrategyData* sDat)
{
#ifdef DEBUG
   sDat->logWin("RegionProcess::run()");
#endif

   AIC_ActivityUtility util(sDat);

   /*
    * Pick an activity
    */

   AIC_Activity::ThingsToDo what;
   AIC_Activity* activity = sDat->activities()->pick(&what, sDat->random());

   ASSERT((what >= 0) && (what < AIC_Activity::TDD_HowMany));

#ifdef DEBUG
   sDat->logWin("Thinking about %s %s", 
      AIC_Activity::thingToDoStr(what),
      (const char*) activity->description());
#endif

   switch(what)
   {
      case AIC_Activity::TDD_Update:
         updatePriorities(sDat, activity);
         break;
      case AIC_Activity::TDD_Split:
         splitActivity(sDat, activity);
         activity->lowerPriority(what);
         activity->resetAge();
         break;
      case AIC_Activity::TDD_Expand:
         AIC_Expand::expandActivity(sDat, activity);
         activity->lowerPriority(what);
         activity->resetAge();
         break;
      case AIC_Activity::TDD_AssignSP:
         AIC_AssignUnits::assignSP(sDat, activity);
         activity->lowerPriority(what);
         break;
      case AIC_Activity::TDD_SendOrder:
         AIC_OrderUnits::sendOrders(sDat, activity);
         activity->lowerPriority(what);
         activity->resetAge();
         break;

      case AIC_Activity::TDD_OrganizeTowns:
      {
         activity->priority(what, 0);  // remove this action before deleting activity

         if( (activity->towns()->entries() == 0) &&
             (activity->childCount() == 0) )
         {
#ifdef DEBUG
            sDat->logWin("Killed %s because it has no towns or children",
               (const char*) activity->description());
#endif
            util.killActivity(activity);
         }
         break;
      }

      case AIC_Activity::TDD_Fizzle:
         util.killActivity(activity);
         break;

      default:
         FORCEASSERT("Unknown thing to do");
         break;
   }
}


void AIC_RegionProcess::init(AIC_StrategyData* sDat)
{
   AIC_ActivityUtility util(sDat);

   // Add in a "Rule the world" activity

   AIC_Activity* activity = util.createActivity(0);      // Create top activity

   // All towns, and all units to be added.

   int nTowns = sDat->campData()->getTowns().entries();
   for(ITown i = 0; i < nTowns; i++)
   {
      util.moveTown(activity, i);
   }

   const Armies* army = sDat->armies();   // &campData()->getArmies();
   ConstICommandPosition top = army->getPresident(sDat->side());
   ConstUnitIter unitIter(army, top, false);
   while(unitIter.next())
   {
      ConstICommandPosition cp = unitIter.current();
      if(cp->isLower(Rank_President))

      if(sDat->unitList()->isMember(cp))
         AIC_UnitAllocator::moveUnit(sDat->unitList(), activity, cp);
   }
}

/*
 * Update Region Priority before picking a codelet
 */

void AIC_RegionProcess::update(AIC_ActivityList* activities)
{
   priority(activities->priority());
}





/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
