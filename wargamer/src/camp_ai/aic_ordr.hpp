/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_ORDR_HPP
#define AIC_ORDR_HPP

#ifndef __cplusplus
#error aic_ordr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Send order to unit
 *
 *----------------------------------------------------------------------
 */

#include "codelet.hpp"
#include "cpdef.hpp"

class AIC_StrategyData;
class AIC_ObjectiveList;
class AIC_Objective;
class AIC_UnitList;
class AIC_TownList;
class AIC_UnitRef;
class Town;

class AIC_ObjectiveOrderer : public AIC_Codelet
{
   public:
      AIC_ObjectiveOrderer(AIC_StrategyData* sData, AIC_ObjectiveList* objectives, AIC_UnitList* units, const AIC_TownList* towns);
      ~AIC_ObjectiveOrderer();

      virtual void init();
		virtual void run(unsigned int rand);
      virtual void timeUpdate(TimeTick t);
#ifdef DEBUG
      virtual String name() const;
#endif

   private:
      void procUnit(AIC_UnitRef& aiUnit);
      void procUnitWithObjective(AIC_UnitRef& aiUnit);
      void procUnitAtObjective(const AIC_UnitRef& aiUnit);
      void procIdleUnit(const AIC_UnitRef& aiUnit);

      bool townNeedsAction(const AIC_UnitRef& aiUnit, ITown itown, Orders::Type::Value& onArrival);
      void orderUnitToTown(AIC_UnitRef& aiUnit, ITown itown, Orders::Type::Value onArrival);
      bool orderUnitLeavingTown(const AIC_UnitRef& aiUnit);

      float calcBaseAgression(const AIC_Objective* objective) const;
      int biassedRand(float base, unsigned int range, unsigned int variance) const;
      bool AIC_ObjectiveOrderer::boolRand(float base) const;
      bool calcAutoGarrison(ConstParamCP cp) const;

      bool needsSupply(ConstParamCP cp);

      bool shouldBuildSupplyDepot(const AIC_UnitRef& aiUnit, ITown itown);
      bool shouldDropOffGarrison(const AIC_UnitRef& aiUnit, ITown itown);
      bool shouldSiegeTown(const AIC_UnitRef& aiUnit, ITown itown);

      void buildSupplyDepot(const AIC_UnitRef& aiUnit);
      bool dropOffGarrison(const AIC_UnitRef& aiUnit);
      bool dropOffSiege(const AIC_UnitRef& aiUnit);

      AIC_StrategyData* d_sData;
      AIC_ObjectiveList* d_objectives;
      AIC_UnitList* d_units;
      const AIC_TownList* d_towns;
};



 #if 0         // Old Obsolete stuff
#include "mytypes.h"

class AIC_StrategyData;
class AIC_Activity;

class AIC_OrderUnits
{
		AIC_OrderUnits();	// Private constructor to prevent instantation
	public:
		static Boolean sendOrders(AIC_StrategyData* sData, AIC_Activity* activity);
			// Try to order units in activity
			// Return False if activity's priority should be reduced
};
#endif      // Old Obsolete stuff

#endif /* AIC_ORDR_HPP */

