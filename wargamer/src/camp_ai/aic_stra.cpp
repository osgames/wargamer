/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI Strategy
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "aic_stra.hpp"

// Includes for Data Objects

#include "aic_town.hpp"
#include "aic_objective.hpp"
#include "aic_unit.hpp"

// Includes for Processors

#include "aic_msg.hpp"
#include "aic_res.hpp"
#include "aic_wld.hpp"
#include "aic_makeObjective.hpp"
#include "aic_ordr.hpp"
#include "aic_org.hpp"

#include "campdint.hpp"
#include "armies.hpp"

#ifdef DEBUG
#include "scenario.hpp"
#include "gamectrl.hpp"
// #include "fonts.hpp"
// #include "dib.hpp"
// #include "wmisc.hpp"
#include "IMapWindow.hpp"
#include "dib.hpp"
#include "campint.hpp"
#include "thicklin.hpp"
#include "scenario.hpp"
#include "town.hpp"
#endif

// Set the resolution that AI is aware of time passage
// Every day LOCATOR will get called inirectly via GameSchedule::run()

TimeTick AIC_Strategy::s_realTimeInterval = DaysToTicks(1);

/*
 * Constructor
 */

AIC_Strategy::AIC_Strategy(Side s, const CampaignData* campData) :
   GameSchedule(s_realTimeInterval),
   AIC_StrategyData(s, campData),
   d_codeList(),
   d_townList(0),
   d_objectiveList(0),
   d_unitList(0),
   d_procMessages(0)
{
   for(int i = 0; i < AIC_CODE_HowMany; ++i)
      d_codeID[i] = AIC_CodeletList::Unused;
}

/*
 * Destructor
 */

AIC_Strategy::~AIC_Strategy()
{
   d_procMessages = 0;
   d_codeList.deleteAll();

   delete d_objectiveList;
   d_objectiveList = 0;
   delete d_unitList;
   d_unitList = 0;
   delete d_townList;
   d_townList = 0;
}

/*
 * Codelet Priority Management
 *
 * Note: priorities have changed so that instead of absolute values
 * they are now a probability of being picked each thought.
 * This allows priorities to be more easily controlled instead of each
 * priority using adhoc values.
 */

void AIC_Strategy::addPriority(AIC_CODELET_ID id, unsigned int p)
{
   if(d_codeID[id] != AIC_CodeletList::Unused)
   {
      d_codeList.get(id)->addPriority(p);
   }
}

void AIC_Strategy::subPriority(AIC_CODELET_ID id, unsigned int p)
{
   if(d_codeID[id] != AIC_CodeletList::Unused)
   {
      d_codeList.get(id)->subPriority(p);
   }
}

void AIC_Strategy::priority(AIC_CODELET_ID id, unsigned int p)
{
   if(d_codeID[id] != AIC_CodeletList::Unused)
   {
      d_codeList.get(id)->priority(p);
   }
}

unsigned int AIC_Strategy::priorityRange() const
{
   if(d_townList)
      return d_townList->totalImportance();
   else
      return 100;    // arbitary non-zero value
}


/*
 * Intialisation
 */

void AIC_Strategy::createData()
{
   if(!d_townList)
      d_townList = new AIC_TownList(this);
   if(!d_unitList)
      d_unitList = new AIC_UnitList;
   if(!d_objectiveList)
      d_objectiveList = new AIC_ObjectiveList(d_unitList);
}

void AIC_Strategy::init()
{
#ifdef DEBUG
   logWin("Campaign AI initialised for %s", scenario->getSideName(side()));
#endif

   /*
    * Create Data
    */

   createData();

   /*
    * Create and add Codelets
    */

   d_codeID[AIC_CODE_Message]    = d_codeList.add(d_procMessages = new AIC_MessageProcess(this, d_townList));
   d_codeID[AIC_CODE_Resource]   = d_codeList.add(new AIC_ResourceProcess(this));
   d_codeID[AIC_CODE_World]      = d_codeList.add(new AIC_WorldProcess(this, d_townList));
   d_codeID[AIC_CODE_Units]      = d_codeList.add(new AIC_UnitProcess(this, d_unitList, d_objectiveList));
   d_codeID[AIC_CODE_Create]     = d_codeList.add(new AIC_ObjectiveCreator(this, d_townList, d_objectiveList, d_unitList));
   d_codeID[AIC_CODE_Order]      = d_codeList.add(new AIC_ObjectiveOrderer(this, d_objectiveList, d_unitList, d_townList));
   d_codeID[AIC_CODE_Reorganize] = d_codeList.add(new AIC_Reorganize(this, d_unitList));

   // Initialise codelist

   d_codeList.init();

#ifdef DEBUG
   d_codeList.log(AIC_StrategyData::logWindow());
#endif
}

/*
 * Called every day... adds on priority to the analyse world codelet
 *
 * worldSpeed is initially set to 1/14...
 * It should probably vary depending on the real-time that has passed
 * and on whether much has happened in the game recently.
 * eg. if no messages received then could increase time
 * if messages received then decrease time.
 * If realtime is going fast, eg. second per day, then we dont want
 * to waste all our time processing the world.
 * But if time is paused, or very slow we could process it every day.
 */

void AIC_Strategy::process(TimeTick interval)
{
   d_codeList.timeUpdate(interval);    // CodeletList function

#ifdef DEBUG
   static TimeTick s_lastLog = 0;
   // static const TimeTick s_interval = TicksPerHour * 24 * 7;    // Once a week
   static const TimeTick s_interval = TicksPerHour * 24 * 1;    // Once a day

   AIC_StrategyData::logWin("interval=%d, lastLog=%d", (int)interval, (int)s_lastLog);

   s_lastLog += interval;
   if(s_lastLog > s_interval)
   {
      s_lastLog = 0;
      d_codeList.log(AIC_StrategyData::logWindow());
   }
#endif
}

/*
 * Think about AI:
 * this is called at the rate specified by the control panel.
 * Defaults to one thought per second.
 *
 * The AI can keep thinking until it gets pressured into issuing orders
 * when gametime has changed
 */

void AIC_Strategy::think()
{
#ifdef DEBUG_ALL
   AIC_StrategyData::logWin("Thinking about strategy");
#endif
#ifdef DEBUG
   SysTick::Value tick = GameControl::getTick();
#endif

   /*
    * Do regular maintenance first
    */

   maintain();

   if(d_codeList.priority() != 0)
      d_codeList.run(AIC_StrategyData::rand(d_codeList.priority()));

#ifdef DEBUG
   SysTick::Value newTick = GameControl::getTick();
   newTick -= tick;
   AIC_StrategyData::logWin("Thought for %d.%01d Seconds",
      (int) (newTick / SysTick::TicksPerSecond),
      (int) (((newTick % SysTick::TicksPerSecond) * 10) / SysTick::TicksPerSecond));

#endif
}

/*
 * Regular maintenance called every thought
 *
 * Updates activity's ages
 * Deletes unhappy activities
 */

void AIC_Strategy::maintain()
{
}

void AIC_Strategy::receiveGameMessage(const CampaignMessageInfo& msg)
{
   if(d_procMessages)
      d_procMessages->addMessage(msg);
}


#ifdef DEBUG
/*
 *
 */

void AIC_Strategy::showTown(DrawDIBDC* dib, ITown itown, const PixelPoint& p)
{
   if(d_townList)
      d_townList->showTown(dib, itown, p);
}


void AIC_Strategy::drawAI(const IMapWindow& mw)
{
   if(d_objectiveList)
   {
      Reader lock(d_objectiveList);

      const CampaignData* campData = AIC_StrategyData::campData();
      DrawDIBDC* dib = mw.dib();

      // Iterate through Objectives

      for(AIC_ObjectiveList::const_iterator it = d_objectiveList->begin();
            it != d_objectiveList->end();
            ++it)
      {
         // For each Objective

         const AIC_Objective& ob = *it;

         // Draw Circle Around Town

         const Town& town = campData->getTown(ob.town());
         PixelPoint townP;
         mw.locationToPixel(town.getLocation(), townP);
         ColourIndex color = dib->getColour(scenario->getSideColour(town.getSide()));

         for(int r = 6; r < 10; ++r)
         {
            dib->circle(townP.x(), townP.y(), r, color);
         }

         // Draw Line from First Unit to town
         // Draw Line from other units to First Unit

         PixelPoint unitP = townP;
         // color = dib->getColour(PALETTERGB(64,64,0));
         // color = dib->getColour(scenario->getSideColour(town.getSide()));

         ThickLineEngine thickLine(dib);
         thickLine.thickness(3);
         thickLine.color(color);

         const AIC_CPList& cpList = ob.unitList();
         for(AIC_CPList::const_iterator cpIter = cpList.begin();
            cpIter != cpList.end();
            ++cpIter)
         {
            ConstICommandPosition cp = *cpIter;

            PixelPoint p;
            Location l;
            cp->getLocation(l);
            mw.locationToPixel(l, p);

            PixelDimension length = aproxDistance(p.x()-unitP.x(), p.y()-unitP.y());

            const PixelDimension BigLength = 10;

            if(length > BigLength)
            {
               GraphicEngine::GPoint startP;
               startP.x( p.x() + (BigLength * (unitP.x() - p.x())) / length);
               startP.y( p.y() + (BigLength * (unitP.y() - p.y())) / length);

               // thickLine.addPoint(GraphicEngine::GPoint(p.x(), p.y()));
               thickLine.addPoint(startP);
               thickLine.addPoint(GraphicEngine::GPoint(unitP.x(), unitP.y()));
               thickLine.close();
            }
            else if(length != 0)
            {
               dib->line(p.x(), p.y(), unitP.x(), unitP.y(), color);
            }

            // dib->line(p.x(), p.y(), unitP.x(), unitP.y(), color);
            // if(cpIter == cpList.begin())
            // {
            //    unitP = p;
            //    color = dib->getColour(PALETTERGB(64,64,0));
            //    thickLine.color(color);
            // }
         }
      }
   }
}

#endif

UWORD AIC_Strategy::s_fileVersion = 0x0000;

bool AIC_Strategy::readData(FileReader& f)
{
   createData();

   const Armies* army = &campData()->getArmies();

   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);
   d_townList->readData(f);
   d_unitList->readData(f, army);
   d_objectiveList->readData(f, army);
   return f.isOK();
}

bool AIC_Strategy::writeData(FileWriter& f) const
{
   f << s_fileVersion;
   d_townList->writeData(f);
   d_unitList->writeData(f);
   d_objectiveList->writeData(f);
   return f.isOK();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
