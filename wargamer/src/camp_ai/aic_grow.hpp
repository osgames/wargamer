/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"


#ifndef AIC_GROW_HPP
#define AIC_GROW_HPP

#ifndef __cplusplus
#error aic_grow.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Try to expand an activity
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"

class AIC_StrategyData;
class AIC_Activity;

class AIC_Expand
{
 public:
 	static Boolean expandActivity(AIC_StrategyData* sData, AIC_Activity* activity);
		// Try to expand activity
		// Return True if expansion was succesful
};

#endif /* AIC_GROW_HPP */

