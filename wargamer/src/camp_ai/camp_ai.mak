# Microsoft Developer Studio Generated NMAKE File, Based on camp_ai.dsp
!IF "$(CFG)" == ""
CFG=camp_ai - Win32 Debug
!MESSAGE No configuration specified. Defaulting to camp_ai - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "camp_ai - Win32 Release" && "$(CFG)" != "camp_ai - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "camp_ai.mak" CFG="camp_ai - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "camp_ai - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "camp_ai - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "camp_ai - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\camp_ai.dll"

!ELSE 

ALL : "clogic - Win32 Release" "gwind - Win32 Release" "gamesup - Win32 Release" "campdata - Win32 Release" "system - Win32 Release" "ob - Win32 Release" "$(OUTDIR)\camp_ai.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" "campdata - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" "clogic - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\aic.obj"
	-@erase "$(INTDIR)\aic_con.obj"
	-@erase "$(INTDIR)\aic_makeobjective.obj"
	-@erase "$(INTDIR)\aic_msg.obj"
	-@erase "$(INTDIR)\aic_objective.obj"
	-@erase "$(INTDIR)\aic_ordr.obj"
	-@erase "$(INTDIR)\aic_org.obj"
	-@erase "$(INTDIR)\aic_res.obj"
	-@erase "$(INTDIR)\aic_sdat.obj"
	-@erase "$(INTDIR)\aic_stra.obj"
	-@erase "$(INTDIR)\aic_threat.obj"
	-@erase "$(INTDIR)\aic_town.obj"
	-@erase "$(INTDIR)\aic_unit.obj"
	-@erase "$(INTDIR)\aic_unitinfluence.obj"
	-@erase "$(INTDIR)\aic_util.obj"
	-@erase "$(INTDIR)\aic_wld.obj"
	-@erase "$(INTDIR)\codelet.obj"
	-@erase "$(INTDIR)\codelist.obj"
	-@erase "$(INTDIR)\objectivewindow.obj"
	-@erase "$(INTDIR)\resinfo.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\camp_ai.dll"
	-@erase "$(OUTDIR)\camp_ai.exp"
	-@erase "$(OUTDIR)\camp_ai.lib"
	-@erase "$(OUTDIR)\camp_ai.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /I "..\campData" /I "..\gwind" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMP_AI_DLL" /Fp"$(INTDIR)\camp_ai.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\camp_ai.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\camp_ai.pdb" /debug /machine:I386 /out:"$(OUTDIR)\camp_ai.dll" /implib:"$(OUTDIR)\camp_ai.lib" 
LINK32_OBJS= \
	"$(INTDIR)\aic.obj" \
	"$(INTDIR)\aic_con.obj" \
	"$(INTDIR)\aic_makeobjective.obj" \
	"$(INTDIR)\aic_msg.obj" \
	"$(INTDIR)\aic_objective.obj" \
	"$(INTDIR)\aic_ordr.obj" \
	"$(INTDIR)\aic_org.obj" \
	"$(INTDIR)\aic_res.obj" \
	"$(INTDIR)\aic_sdat.obj" \
	"$(INTDIR)\aic_stra.obj" \
	"$(INTDIR)\aic_threat.obj" \
	"$(INTDIR)\aic_town.obj" \
	"$(INTDIR)\aic_unit.obj" \
	"$(INTDIR)\aic_unitinfluence.obj" \
	"$(INTDIR)\aic_util.obj" \
	"$(INTDIR)\aic_wld.obj" \
	"$(INTDIR)\codelet.obj" \
	"$(INTDIR)\codelist.obj" \
	"$(INTDIR)\objectivewindow.obj" \
	"$(INTDIR)\resinfo.obj" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\system.lib" \
	"$(OUTDIR)\campdata.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\gwind.lib" \
	"$(OUTDIR)\clogic.lib"

"$(OUTDIR)\camp_ai.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\camp_aiDB.dll"

!ELSE 

ALL : "clogic - Win32 Debug" "gwind - Win32 Debug" "gamesup - Win32 Debug" "campdata - Win32 Debug" "system - Win32 Debug" "ob - Win32 Debug" "$(OUTDIR)\camp_aiDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"ob - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" "campdata - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" "clogic - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\aic.obj"
	-@erase "$(INTDIR)\aic_con.obj"
	-@erase "$(INTDIR)\aic_makeobjective.obj"
	-@erase "$(INTDIR)\aic_msg.obj"
	-@erase "$(INTDIR)\aic_objective.obj"
	-@erase "$(INTDIR)\aic_ordr.obj"
	-@erase "$(INTDIR)\aic_org.obj"
	-@erase "$(INTDIR)\aic_res.obj"
	-@erase "$(INTDIR)\aic_sdat.obj"
	-@erase "$(INTDIR)\aic_stra.obj"
	-@erase "$(INTDIR)\aic_threat.obj"
	-@erase "$(INTDIR)\aic_town.obj"
	-@erase "$(INTDIR)\aic_unit.obj"
	-@erase "$(INTDIR)\aic_unitinfluence.obj"
	-@erase "$(INTDIR)\aic_util.obj"
	-@erase "$(INTDIR)\aic_wld.obj"
	-@erase "$(INTDIR)\ailog.obj"
	-@erase "$(INTDIR)\codelet.obj"
	-@erase "$(INTDIR)\codelist.obj"
	-@erase "$(INTDIR)\objectivewindow.obj"
	-@erase "$(INTDIR)\resinfo.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\camp_aiDB.dll"
	-@erase "$(OUTDIR)\camp_aiDB.exp"
	-@erase "$(OUTDIR)\camp_aiDB.ilk"
	-@erase "$(OUTDIR)\camp_aiDB.lib"
	-@erase "$(OUTDIR)\camp_aiDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /I "..\campData" /I "..\gwind" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMP_AI_DLL" /Fp"$(INTDIR)\camp_ai.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\camp_ai.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=gdi32.lib user32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\camp_aiDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\camp_aiDB.dll" /implib:"$(OUTDIR)\camp_aiDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\aic.obj" \
	"$(INTDIR)\aic_con.obj" \
	"$(INTDIR)\aic_makeobjective.obj" \
	"$(INTDIR)\aic_msg.obj" \
	"$(INTDIR)\aic_objective.obj" \
	"$(INTDIR)\aic_ordr.obj" \
	"$(INTDIR)\aic_org.obj" \
	"$(INTDIR)\aic_res.obj" \
	"$(INTDIR)\aic_sdat.obj" \
	"$(INTDIR)\aic_stra.obj" \
	"$(INTDIR)\aic_threat.obj" \
	"$(INTDIR)\aic_town.obj" \
	"$(INTDIR)\aic_unit.obj" \
	"$(INTDIR)\aic_unitinfluence.obj" \
	"$(INTDIR)\aic_util.obj" \
	"$(INTDIR)\aic_wld.obj" \
	"$(INTDIR)\ailog.obj" \
	"$(INTDIR)\codelet.obj" \
	"$(INTDIR)\codelist.obj" \
	"$(INTDIR)\objectivewindow.obj" \
	"$(INTDIR)\resinfo.obj" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\systemDB.lib" \
	"$(OUTDIR)\campdataDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\gwindDB.lib" \
	"$(OUTDIR)\clogicDB.lib"

"$(OUTDIR)\camp_aiDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("camp_ai.dep")
!INCLUDE "camp_ai.dep"
!ELSE 
!MESSAGE Warning: cannot find "camp_ai.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "camp_ai - Win32 Release" || "$(CFG)" == "camp_ai - Win32 Debug"
SOURCE=.\aic.cpp

"$(INTDIR)\aic.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_act.cpp
SOURCE=.\aic_asgn.cpp
SOURCE=.\aic_con.cpp

"$(INTDIR)\aic_con.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_grow.cpp
SOURCE=.\aic_makeobjective.cpp

"$(INTDIR)\aic_makeobjective.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_msg.cpp

"$(INTDIR)\aic_msg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_objective.cpp

"$(INTDIR)\aic_objective.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_ordr.cpp

"$(INTDIR)\aic_ordr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_org.cpp

"$(INTDIR)\aic_org.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_proc.cpp
SOURCE=.\aic_res.cpp

"$(INTDIR)\aic_res.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_sdat.cpp

"$(INTDIR)\aic_sdat.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_stra.cpp

"$(INTDIR)\aic_stra.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_threat.cpp

"$(INTDIR)\aic_threat.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_town.cpp

"$(INTDIR)\aic_town.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_unit.cpp

"$(INTDIR)\aic_unit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_unitinfluence.cpp

"$(INTDIR)\aic_unitinfluence.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_util.cpp

"$(INTDIR)\aic_util.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\aic_wld.cpp

"$(INTDIR)\aic_wld.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ailog.cpp

!IF  "$(CFG)" == "camp_ai - Win32 Release"

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"


"$(INTDIR)\ailog.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\codelet.cpp

"$(INTDIR)\codelet.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\codelist.cpp

"$(INTDIR)\codelist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\objectivewindow.cpp

"$(INTDIR)\objectivewindow.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\resinfo.cpp

"$(INTDIR)\resinfo.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "camp_ai - Win32 Release"

"ob - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   cd "..\camp_ai"

"ob - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"

"ob - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   cd "..\camp_ai"

"ob - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ENDIF 

!IF  "$(CFG)" == "camp_ai - Win32 Release"

"system - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   cd "..\camp_ai"

"system - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"

"system - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   cd "..\camp_ai"

"system - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ENDIF 

!IF  "$(CFG)" == "camp_ai - Win32 Release"

"campdata - Win32 Release" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" 
   cd "..\camp_ai"

"campdata - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Release" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"

"campdata - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" 
   cd "..\camp_ai"

"campdata - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campdata"
   $(MAKE) /$(MAKEFLAGS) /F .\campdata.mak CFG="campdata - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ENDIF 

!IF  "$(CFG)" == "camp_ai - Win32 Release"

"gamesup - Win32 Release" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   cd "..\camp_ai"

"gamesup - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"

"gamesup - Win32 Debug" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   cd "..\camp_ai"

"gamesup - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ENDIF 

!IF  "$(CFG)" == "camp_ai - Win32 Release"

"gwind - Win32 Release" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   cd "..\camp_ai"

"gwind - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"

"gwind - Win32 Debug" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   cd "..\camp_ai"

"gwind - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ENDIF 

!IF  "$(CFG)" == "camp_ai - Win32 Release"

"clogic - Win32 Release" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Release" 
   cd "..\camp_ai"

"clogic - Win32 ReleaseCLEAN" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Release" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"

"clogic - Win32 Debug" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Debug" 
   cd "..\camp_ai"

"clogic - Win32 DebugCLEAN" : 
   cd "\src\sf\wargamer\src\campLogic"
   $(MAKE) /$(MAKEFLAGS) /F .\clogic.mak CFG="clogic - Win32 Debug" RECURSE=1 CLEAN 
   cd "..\camp_ai"

!ENDIF 


!ENDIF 

