/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Unit Organization and allocator
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_org.hpp"
#include "aic_sdat.hpp"
#include "aic_unit.hpp"
#include "armies.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "repopool.hpp"
#include "ds_repoSP.hpp"
#include "ds_reorg.hpp"
#include "armyutil.hpp"

AIC_Reorganize::AIC_Reorganize(AIC_StrategyData* sData, AIC_UnitList* units) :
   d_sData(sData),
   d_units(units)
{
}

AIC_Reorganize::~AIC_Reorganize()
{
   d_sData = 0;
   d_units = 0;
}

void AIC_Reorganize::init()
{
#ifdef DEBUG
   d_sData->logWin("Reorganize::init()");
#endif
}

void AIC_Reorganize::run(unsigned int rand)
{
#ifdef DEBUG
   d_sData->logWin("Reorganize::run()");
#endif

   // Assign new SPs

   assignNewSPs();

   // Check for badly setup organisations and reorganize them

   adjustOB();

   priority(0);
}


void AIC_Reorganize::timeUpdate(TimeTick t)
{
   addPriority(MulDiv(t, d_sData->priorityRange(), DaysToTicks(7)));
}

#ifdef DEBUG
String AIC_Reorganize::name() const
{
   return "Reorganize";
}
#endif



/*
 * Private worker functions
 */
#ifdef _MSC_VER
   struct AttachableCP
   {
      AttachableCP() :
         d_cp(NoCommandPosition),
         d_priority(0)
      {
      }

      AttachableCP(ConstParamCP cp, unsigned int p) :
         d_cp(cp),
         d_priority(p)
      {
      }

      ConstICommandPosition d_cp;
      unsigned int d_priority;
   };
#endif


ConstICommandPosition AIC_Reorganize::findSuitableOrg(const RepoSP* repo)
{
   const Armies* army = d_sData->armies();

   ISP sp = repo->sp();
   UnitType ut = sp->getUnitType();
   const UnitTypeItem& uti = army->getUnitType(ut);

   /*
    * Build a list of suitable units with a probability
    */
#ifndef _MSC_VER
   struct AttachableCP
   {
      AttachableCP() :
         d_cp(NoCommandPosition),
         d_priority(0)
      {
      }

      AttachableCP(ConstParamCP cp, unsigned int p) :
         d_cp(cp),
         d_priority(p)
      {
      }

      ConstICommandPosition d_cp;
      unsigned int d_priority;
   };
#endif

   unsigned int totalPriority = 0;

   std::vector<AttachableCP> cpList;

   ConstUnitIter iter(army, army->getPresident(d_sData->side()));
   while(iter.next())
   {
      ConstICommandPosition cp = iter.current();
      if (cp->getRankEnum() == Rank_Division)
      {
         if (CampaignArmy_Util::canAttachTo(d_sData->campData(), cp, uti))
         {
            // TODO:
            //    calculate a priority...
            //    based on distance from SP
            //    current divisional organization
            //    CP's activity and priority of activity

            unsigned int p = 10;    // arbitary value to be calculated

            cpList.push_back(AttachableCP(cp, p));
            totalPriority += p;

#ifdef DEBUG_FULL
            d_sData->logWin("%s, p=%d: added to possible attach list",
                  static_cast<const char*>(cp->getName()),
                  static_cast<int>(p));
#endif
         }
      }
   }

   /*
    * Pick a unit from the list
    */

   if (cpList.size() != 0)
   {
      ASSERT(totalPriority != 0);
      int r = d_sData->rand(totalPriority);
      for(std::vector<AttachableCP>::const_iterator it = cpList.begin();
         it != cpList.end();
         ++it)
      {
         if(r < it->d_priority)
            return it->d_cp;

         r -= it->d_priority;
      }
   }

   return NoCommandPosition;
}

void AIC_Reorganize::orderSPtoAttach(ConstParamCP cp, const RepoSP* rsp)
{
#ifdef DEBUG
   d_sData->logWin("Attaching to %s",
            static_cast<const char*>(cp->getName()));
#endif

   RepoSPOrder::send(cp, rsp);
}


/*======================================
 * Assign New SPs from the SP Pool
 *======================================
 * We have 2 ways of doing this:
 * 1. New SPs are created in SPRepoPool
 *    and assigned to divisions.
 * 2. New SPs are created in new divisions
 *    and we look for tiny units and issue
 *    move/attach on arrival
 *======================================
 */

void AIC_Reorganize::assignNewSPs()
{
#ifdef DEBUG_FULL
   d_sData->logWin("Scanning RepoSP List");
#endif

   const Armies* army = d_sData->armies();
   const RepoSPList& repoList = army->repoSPList();
   const CampaignData* campData = d_sData->campData();

   ArmyReadLocker lock(*army);
   SListIterR<RepoSP> iter(&repoList);
   while (++iter)
   {
      const RepoSP* repoSP = iter.current();

      // If the SP has not already been ordered to attach

      const Town& t = campData->getTown(repoSP->town());


      if(!repoSP->isCompleted() &&
        (repoSP->attachTo() == NoCommandPosition) &&
        (t.getSide() == d_sData->side() ))
      {
         ISP sp = repoSP->sp();
         UnitType ut = sp->getUnitType();
         const UnitTypeItem& uti = army->getUnitType(ut);

#ifdef DEBUG
         d_sData->logWin("Looking at SP %s",
            static_cast<const char*>(uti.getName()));
#endif

         // Find a suitable Organisation to attach to

         ConstICommandPosition cp = findSuitableOrg(repoSP);
         if (cp != NoCommandPosition)
         {
            orderSPtoAttach(cp, repoSP);
         }
         else
         {
            // What do we do if no suitable divisions?
            // Try to create one?
#ifdef DEBUG
            d_sData->logWin("Can't find suitable CP for %s",
               static_cast<const char*>(uti.getName()));
#endif
         }

      }
   }
}

/*
 * Reorganize OB:
 *
 * Starting at the bottom, look for divisions that can be
 * split into two.  Then work up splitting as neccessary.
 *
 * Also look at tiny divisions and attach them somewhere suitable.
 * and also tiny organizations, e.g. corps with a single division.
 */

void AIC_Reorganize::adjustOB()
{
#ifdef DEBUG_FULL
   d_sData->logWin("adjustOB()");
#endif

   const Armies* army = d_sData->armies();

   for (AIC_UnitList::const_iterator it = d_units->begin();
      it != d_units->end();
      ++it)
   {
      ConstICommandPosition topCP = (*it).first;

#ifdef DEBUG_FULL
      d_sData->logWin("Looking at Organization under %s", topCP->getName());
#endif

      ConstUnitIter cpIt(army, topCP);
      while(cpIt.next())
      {
         ConstICommandPosition cp = cpIt.current();
#ifdef DEBUG_FULL
         d_sData->logWin("Looking at %s", cp->getName());
#endif

         // If it is a division then consider splitting
         // into 2 divisions.

         if (cp->getRankEnum() == Rank_Division)
         {
            reorgDivision(cp);
         }
         else if (cp->getRankEnum() == Rank_Corps)
         {
            reorgCorps(cp);
         }
         else // if (cp->getRankEnum() == Rank_Army)
         {
            reorgArmy(cp);
         }
      }
   }
}

inline float makeProbability(int value, int minRange, int maxRange, float maxProb)
{
   if(value <= minRange)
      return 0;
   else if(value >= maxRange)
      return maxProb;
   else
      return maxProb * static_cast<float>(value - minRange) / static_cast<float>(maxRange - minRange);
}

void AIC_Reorganize::reorgDivision(ConstParamCP cp)
{
#ifdef DEBUG_FULL
   d_sData->logWin("reorgDivision(%s)", cp->getName());
#endif

   const Armies* army = d_sData->armies();

   // Depending on type of Division, work out
   // probability of splitting based on number of SPs.

   static const SPCount minSPToSplit[BasicUnitType::HowMany] =
   {
      UnitTypeConst::InfAllowedPerXX - 1,
      UnitTypeConst::CavAllowedPerXX - 1,
      UnitTypeConst::ArtAllowedPerXX - 1,
      UnitTypeConst::MaxAllowedPerXX - 1,
   };
   static const SPCount minTotalSPToSplit = (UnitTypeConst::MaxAllowedPerXX * 3) / 4;   // =9

   SPCount minSP = UnitTypeConst::MaxAllowedPerXX;
   {

      for(BasicUnitType::value t = BasicUnitType::Infantry; t < BasicUnitType::HowMany; INCREMENT(t))
      {
         minSP = minimum(minSP, minSPToSplit[t]);
      }
   }


   // Count up the SPs of each type:

   SPCount spCount = army->getUnitSPCount(cp, false);

   if (spCount >= minSP)
   {
      SPCount counts[BasicUnitType::HowMany];
      {
         for (BasicUnitType::value t = BasicUnitType::Infantry; t < BasicUnitType::HowMany; INCREMENT(t))
         {
            counts[t] = army->getNType(cp, t, false);
         }
      }

      float probability = makeProbability(spCount, UnitTypeConst::MaxAllowedPerXX / 2, UnitTypeConst::MaxAllowedPerXX, 0.5);

      if (cp->isInfantry())
      {
         float prob1 = makeProbability(counts[BasicUnitType::Infantry], minSPToSplit[BasicUnitType::Infantry], UnitTypeConst::InfAllowedPerXX, 0.25);
         probability = maximum(probability, prob1);
      }
      else if(cp->isCavalry())
      {
         float prob1 = makeProbability(counts[BasicUnitType::Cavalry], minSPToSplit[BasicUnitType::Cavalry], UnitTypeConst::CavAllowedPerXX, 0.25);
         probability = maximum(probability, prob1);
      }
      else if(cp->isArtillery())
      {
         float prob1 = makeProbability(counts[BasicUnitType::Artillery], minSPToSplit[BasicUnitType::Artillery], UnitTypeConst::ArtAllowedPerXX, 0.25);
         probability = maximum(probability, prob1);
      }

   #ifdef DEBUG_FULL
      d_sData->logWin("probability = %f", (float) probability);
   #endif

      const unsigned int randRange = 0x4000; // arbitrary value
      unsigned int range = probability * randRange;
      if (d_sData->rand(randRange) < range)
      {
         splitDivision(cp);

      }
   }
}

/*
 * Split a division in half.
 * Could be quite sophisticated, try to keep similar types together
 * keep balance of division correct.
 */

void AIC_Reorganize::splitDivision(ConstParamCP cp)
{
#ifdef DEBUG
   d_sData->logWin("Splitting %s", cp->getName());
#endif

   const Armies* army = d_sData->armies();

   /*
    * Count up the different types of units
    */

   SPCount spCount = army->getUnitSPCount(cp, false);

   SPCount nToTransfer[BasicUnitType::HowMany];

   {
      for (BasicUnitType::value t = BasicUnitType::Infantry; t < BasicUnitType::HowMany; INCREMENT(t))
      {
         nToTransfer[t] = army->getNType(cp, t, false) / 2;
      }
   }

   /*
    * Step through SP Chain and transfer appropriate number of each type
    */

   DS_Reorg::HOrganize h = DS_Reorg::start(cp->getParent());
   DS_Reorg::createCP(h, cp, cp->getNation(), Rank_Division, false);
   DS_Reorg::push(h);
   // DS_Reorg::createLeader(h);    // Create Leader under current CP
   ConstStrengthPointIter spIter(army, cp);
   while(++spIter)
   {
      ConstISP sp = spIter.currentISP();

      UnitType ut = sp->getUnitType();
      const UnitTypeItem& uti = army->getUnitType(ut);
      BasicUnitType::value t = uti.getBasicType();
      if (nToTransfer[t])
      {
         DS_Reorg::transferSP(h, cp, sp);
         --nToTransfer[t];
      }
   }
   DS_Reorg::pop(h);
   DS_Reorg::end(h);

}

/*
 * Examine a Corps to consider splitting into 2 Armies
 *
 * A standard corps should contain:
 *    2-4 Infantry XX
 *    0-1 Cavalry XX (light)
 *    0-1 Artillery XX
 * Or
 *    Several Heavy Artillery XX
 * Or
 *    2-4 Cavalry XX, at least half heavy.
 */

namespace { // private namespace

struct NewOrgList
{
      NewOrgList(ConstParamCP parent, AIC_StrategyData* sData) :
#ifdef DEBUG
         d_recurseLevel(0),
#endif
         d_sData(sData),
         d_parent(parent),
         d_infList(),
         d_cavList(),
         d_artList()
      {
      }

      ~NewOrgList() { d_sData = 0; }

      void addCP(ConstParamCP cp);
      bool isValid();

      void split();

   private:
      typedef std::vector<ConstICommandPosition> OrgList;

      void transferUnits(DS_Reorg::HOrganize h, const OrgList& list);
      void sendOrder();

      void takeCP(OrgList& list)
      {
         ASSERT(list.size() > 0);
         addCP(list.back());
         list.pop_back();
      }


      enum {
         MaxInf = 4
      };

      OrgList d_infList;
      OrgList d_cavList;
      OrgList d_artList;
      ConstICommandPosition d_parent;
      AIC_StrategyData* d_sData;
#ifdef DEBUG
      int d_recurseLevel;
#endif

};

void NewOrgList::addCP(ConstParamCP cp)
{
   if (cp->isInfantry())
   {
      d_infList.push_back(cp);
   }
   else if (cp->isCavalry())
   {
      d_cavList.push_back(cp);
      // check for light/heavy...
   }
   else if (cp->isArtillery())
   {
      d_artList.push_back(cp);
      // check for light/heavy
   }
}

bool NewOrgList::isValid()
{
   if(d_infList.size() > MaxInf)
      return false;

   if(d_cavList.size() > 1)
   {
      if(d_artList.size() || d_infList.size())
         return false;
   }

   if(d_artList.size() > 1)
   {
      if(d_cavList.size() || d_infList.size())
         return false;
   }

   return true;
}

void NewOrgList::transferUnits(DS_Reorg::HOrganize h, const OrgList& list)
{
   for (OrgList::const_iterator it = list.begin();
         it != list.end();
         ++it)
   {
      DS_Reorg::transferCP(h, *it);
#ifdef DEBUG
      d_sData->logWin("Transferring %s to new Corps",
            (*it)->getName());
#endif
   }
}

/*
 * recursive Function to split organization
 */

void NewOrgList::split()
{
#ifdef DEBUG
   ++d_recurseLevel;
   ASSERT(d_recurseLevel < 10);     // infinite recursion depth check

   int loopCount = 0;
#endif

   while(!isValid())
   {
#ifdef DEBUG
      ++loopCount;
      ASSERT(loopCount < 10);     // infinite loop check
#endif

      NewOrgList newOrg(d_parent, d_sData);

      // transfer units from org to new org

      if (d_infList.size() > MaxInf)
      {
         // Split off a new infantry Corps

         int infToTake = d_infList.size() / 2;
         if(infToTake > MaxInf)
            infToTake = MaxInf;

         while (infToTake--)
         {
            newOrg.takeCP(d_infList);
         }

         // also take a cav and art if there are enough

         if(d_cavList.size() > 1)
         {
            newOrg.takeCP(d_cavList);
         }

         if(d_artList.size() > 1)
         {
            newOrg.takeCP(d_artList);
         }
      }
      else if(d_infList.size() > 0)
      {
         // Infantry that has too much art or cav

         while(d_cavList.size() > 1)
         {
            newOrg.takeCP(d_cavList);
         }

         while(d_artList.size() > 1)
         {
            newOrg.takeCP(d_artList);
         }
      }
      else if ( (d_cavList.size() > 0) && (d_artList.size() > 0))
      {
         // We have no infantry, but have both cavalry and artillery

         // Split into seperate cav and art Corps

         while (d_artList.size() > 0)
         {
            newOrg.takeCP(d_artList);
         }
      }
#ifdef DEBUG
      else
         FORCEASSERT("Org is not valid, but did not go into condition");
#endif

      if(!newOrg.isValid())
         newOrg.split();

      // Order units in newOrg

      newOrg.sendOrder();
   }

#ifdef DEBUG
   --d_recurseLevel;
#endif
}

void NewOrgList::sendOrder()
{
   ASSERT(d_infList.size() || d_artList.size() || d_cavList.size());

   DS_Reorg::HOrganize h = DS_Reorg::start(d_parent);
   DS_Reorg::createCP(h, d_parent, d_parent->getNation(), Rank_Corps, true);  // independent Corps
   DS_Reorg::push(h);
   transferUnits(h, d_infList);
   transferUnits(h, d_cavList);
   transferUnits(h, d_artList);
   DS_Reorg::pop(h);
   DS_Reorg::end(h);
}

};    // private namespace

void AIC_Reorganize::reorgCorps(ConstParamCP cp)
{
#ifdef DEBUG_FULL
   d_sData->logWin("reorgCorps(%s)", cp->getName());
#endif

   // Count up number of divisions of each type

   // The campaign doesn't really set up heavy/light cav/art values properly...
   // SO we will fudge it to ignore the type of cav/art

//    int nInf = 0;
//    int nCav = 0;
//    int nArt = 0;
//    int nLightCav = 0;
//    int nHeavyCav = 0;
//    int nLightArt = 0;
//    int nHeavyArt = 0;

   NewOrgList list(cp, d_sData);

   const Armies* army = d_sData->armies();
   ConstUnitIter it(army, cp);
   while (it.next())
   {
      ConstICommandPosition cpDiv = it.current();
      if (cpDiv->getRankEnum() == Rank_Division)
      {
         list.addCP(cpDiv);
      }
   }

   /*
    * Split them if neccessary
    */

   if (!list.isValid())
   {
#ifdef DEBUG
      d_sData->logWin("Corps %s is going to be split",
            cp->getName());
#endif
      list.split();
   }



}

/*
 * Examine Army of Army Group to consider splitting
 *
 * Not sure about rules for this.
 * It may be best if the AI always orders corps, thus Army's are not really neccessary
 * Or just keep them as they started out.
 */


void AIC_Reorganize::reorgArmy(ConstParamCP cp)
{
#ifdef DEBUG_FULL
   d_sData->logWin("reorgArmy(%s)", cp->getName());
#endif

   // Do nothing...

}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
