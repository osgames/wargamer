/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Objectives Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_objective.hpp"
#include "aic_sdat.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "filecnk.hpp"
// #include <algo.h>


AIC_Objective::AIC_Objective() :
   d_town(NoTown),
   d_priority(0),
   d_units(),
   d_spNeeded(0)
{
}

AIC_Objective::AIC_Objective(ITown itown) :
   d_town(itown),
   d_priority(0),
   d_units(),
   d_spNeeded(0)
{
}

AIC_Objective::~AIC_Objective()
{
   //--- Can't do this because Debug Window copies Objectives and doesn't delete units
   // ASSERT(d_units.size() == 0);     // Units must have been removed before this is called
}

UWORD AIC_Objective::s_fileVersion = 0x0000;

bool AIC_Objective::readData(FileReader& f, const Armies* army)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   // town already filled in
   f >> d_priority;
   f >> d_spNeeded;
   d_units.readData(f, army);

   return f.isOK();
}

bool AIC_Objective::writeData(FileWriter& f) const
{
   f << s_fileVersion;

   f << d_priority;
   f << d_spNeeded;
   d_units.writeData(f);

   return f.isOK();
}


void AIC_Objective::addUnit(ConstParamCP cp)
{
   ASSERT(d_units.find(cp) == d_units.end());   //lint !e1023 !e1703 ... ambiguous operator
   d_units.insert(cp);
}

void AIC_Objective::removeUnit(ConstParamCP cp)
{
   d_units.erase(cp);
}

/*------------------------------------------------------------
 * ObjectiveList Functions
 */

AIC_ObjectiveList::AIC_ObjectiveList(AIC_UnitList* unitList, unsigned int nTowns) :
   d_unitList(unitList),
   d_items(),
   d_totalPriority(0)
{
   // d_items.reserve(nTowns);
}

AIC_ObjectiveList::~AIC_ObjectiveList()
{
   d_unitList = 0;
}

UWORD AIC_ObjectiveList::s_fileVersion = 0x0000;

bool AIC_ObjectiveList::readData(FileReader& f, const Armies* army)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   ASSERT(d_items.size() == 0);

   f >> d_totalPriority;
   int count;
   f >> count;
   while (count--)
   {
      ITown itown;
      f >> itown;
      iterator objIt = d_items.insert(d_items.end(), AIC_Objective(itown));
      AIC_Objective* obj = &(*objIt);
      obj->readData(f, army);

      // Patch up the unitList

      const AIC_CPList& list = (*obj).unitList();
      for (AIC_CPList::const_iterator it = list.begin();
         it != list.end();
         ++it)
      {
         ConstICommandPosition cp = *it;

         AIC_UnitRef ur = (*d_unitList)[cp];
         ur.objective(obj);
      }
   }

   return f.isOK();
}

bool AIC_ObjectiveList::writeData(FileWriter& f) const
{
   f << s_fileVersion;

   f << d_totalPriority;
   int count = d_items.size();
   f << count;
   for (const_iterator it = begin(); it != end(); ++it)
   {
      f << (*it).town();
      (*it).writeData(f);
   }



   return f.isOK();
}


AIC_Objective* AIC_ObjectiveList::addOrUpdate(ITown itown, unsigned int p) // const AIC_Town& tInfo)
{
   iterator obj = get(itown);       // tInfo.town());
   if(obj == d_items.end())
   {
      obj = add(itown);    // tInfo.town());
   }

   d_totalPriority -= (*obj).priority();
   (*obj).priority(p);     // tInfo.priority());
   d_totalPriority += (*obj).priority();
   return &(*obj);
}

AIC_Objective* AIC_ObjectiveList::pick(unsigned int value)
{
   // int value = r(d_totalPriority);

   ASSERT(value < d_totalPriority);

   for(Container::iterator it = d_items.begin();
      it != d_items.end();
      ++it)
   {
      if(value < (*it).priority())
         return &(*it);
      value -= (*it).priority();
   }

   FORCEASSERT("AIC_ObjectiveList::pick() did not pick anything");

   return 0;   //lint !e527 ... unreachable
}

AIC_ObjectiveList::iterator AIC_ObjectiveList::add(ITown itown)
{
   ASSERT(!isMember(itown));
   iterator obj = get(itown);
   if(obj == d_items.end())
   {
      // d_items.push_back(AIC_Objective(itown));
      // obj = &d_items.back();
      obj = d_items.insert(d_items.end(), AIC_Objective(itown));
   }

   return  obj;
}

// Remove Town, return true if it was a member

bool AIC_ObjectiveList::remove(ITown itown)
{
   Writer lock(this);

   ASSERT(itown != NoTown);
   iterator it = get(itown);
   if(it != d_items.end())
   {

      // Remove units

      AIC_CPList& cpList = (*it).unitList();

      for(AIC_CPList::iterator cpIt = cpList.begin();
         cpIt != cpList.end();
         ++cpIt)
      {
         ConstICommandPosition cp = *cpIt;

         AIC_UnitRef aiUnit = (*d_unitList)[cp];

         aiUnit.objective(0);
         aiUnit.priority(0);        //lint !e747 ... int to float.  If I change it to 0.0 I get double to float!

//    #ifdef DEBUG
//          d_sData->logWin("Removing %s from %s",
//             cp->getName(),
//             d_sData->campData()->getTownName(itown) );
//    #endif

      }

      // cpList.erase(cpList.begin(), cpList.end());
      cpList.clear();

      ASSERT((*it).unitList().size() == 0);     // Unitlist should have been removed

      d_totalPriority -= (*it).priority();
      d_items.erase(it);

      return(true);
   }
   return(false);
}


/*
 * Remove a unit from objective and delte objective if no units left
 *
 * This differs from AIC_ObjectiveCreator::removeUnit which
 * remove the objective if the SPallocated is below a random level
 */

void AIC_ObjectiveList::remove(ConstParamCP cp)
{
   AIC_UnitRef aiUnit = (*d_unitList)[cp];
   AIC_Objective* objective = aiUnit.objective();
   ASSERT(objective != 0);

   aiUnit.objective(0);
   aiUnit.priority(0);
   objective->removeUnit(cp);
   if(objective->unitList().size() == 0)
   {
      remove(objective->town());
   }
}

struct FindTown
{
   FindTown(ITown t) : d_town(t) { }
   bool operator()(const AIC_Objective& ob) { return d_town == ob.town(); }

   ITown d_town;
};

#if defined( __WATCOM_CPLUSPLUS__) && (__WATCOM_CPLUSPLUS__ <= 1100)
template<class T, class Pred>
T __find_if(T from, T to, Pred p, bidirectional_iterator_tag)
{
   while((from != to) && !p(*from))
   {
      ++from;
   }
   return from;
}
#endif

AIC_ObjectiveList::iterator AIC_ObjectiveList::get(ITown itown)
{
   return std::find_if(d_items.begin(), d_items.end(), FindTown(itown));
}

AIC_ObjectiveList::const_iterator AIC_ObjectiveList::get(ITown itown) const
{
   return std::find_if(d_items.begin(), d_items.end(), FindTown(itown));  //lint !e1036 ... ambiguous
}


bool AIC_ObjectiveList::isMember(ITown itown) const
{
   return get(itown) != d_items.end();
}

AIC_Objective* AIC_ObjectiveList::find(ITown itown)
{
   Container::iterator it = get(itown);
   if(it != d_items.end())
      return &(*it);
   else
      return 0;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
