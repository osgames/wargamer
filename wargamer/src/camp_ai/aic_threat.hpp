/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_Threat_HPP
#define AIC_Threat_HPP

#ifndef __cplusplus
#error AIC_Threat.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Calculate Influence of Units on a town
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"

// Forward References
class AIC_StrategyData;
class AIC_UnitList;

 /*
  * Class Definition
  */

class UnitThreat
{
   public:
      typedef float Influence;

      UnitThreat() { }
      ~UnitThreat() { }
      static Influence getInfluence(const AIC_UnitList* aiUnits, AIC_StrategyData* sData, ITown itown, Side s);


};

#endif /* AIC_Threat_HPP */

