/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_WLD_HPP
#define AIC_WLD_HPP

#ifndef __cplusplus
#error aic_wld.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI World Analysis Data and Utilities
 *
 *----------------------------------------------------------------------
 */

#include "codelet.hpp"

class AIC_StrategyData;
class AIC_TownList;
class AIC_UnitList;
class AIC_ObjectiveList;

class AIC_WorldProcess : public AIC_Codelet
{
	public:
      AIC_WorldProcess(AIC_StrategyData* sDat, AIC_TownList* towns); // , AIC_Codelet* resourceProc);
		~AIC_WorldProcess();
		void run(unsigned int r);
		void init();
      void timeUpdate(TimeTick t);

   private:
         #ifdef DEBUG
         String name() const { return("WorldProcess"); }
         #endif

      AIC_StrategyData* d_sData;
      AIC_TownList*     d_townList;
      // AIC_Codelet*      d_resProc;
};

class AIC_UnitProcess : public AIC_Codelet
{
	public:
      AIC_UnitProcess(AIC_StrategyData* sDat, AIC_UnitList* iunits, AIC_ObjectiveList* objectives);
		~AIC_UnitProcess();
		void run(unsigned int r);
		void init();
      void timeUpdate(TimeTick t);

   private:
         #ifdef DEBUG
         String name() const { return("UnitProcess"); }
         #endif

      AIC_StrategyData* d_sData;
      AIC_UnitList*     d_unitList;
      AIC_ObjectiveList* d_objectives;
};





#if 0    // Old Stuff

#include "aic_town.hpp"

/*--------------------------------------------------------------------
 * World Info
 */

class AIC_WorldInfo :
	public RWLock
{
#ifdef AIC_PROVINCES
	AIC_ProvinceInfoList d_provInfo;
#endif

	AIC_TownInfoList d_townInfo;


	AIC_WorldInfo(const AIC_WorldInfo&);      //lint !e1714 !e1526... not referenced or defined
	AIC_WorldInfo& operator = (const AIC_WorldInfo&);      //lint !e1714 !e1526... not referenced or defined
public:
	AIC_WorldInfo(const CampaignData* campData);
	~AIC_WorldInfo() { }

	void make(const CampaignData* campData, Side s);

	const AIC_TownInfo& pickTown(RandomNumber& r);
		// Pick a random town from list using priority as a random probablity

#ifdef AIC_PROVINCES
	const AIC_ProvinceInfo& pickProvince(RandomNumber& r) const;
		// Pick a random town from list using priority as a random probablity
	const AIC_ProvinceInfo& getProvince(IProvince iprov) const { return d_provInfo.find(iprov); }
	int provincePriority() const { return d_provInfo.totalPriority(); }
#endif

	AIC_TownInfo& getTown(ITown town) { return d_townInfo.find(town); }
	const AIC_TownInfo& getTown(ITown town) const { return d_townInfo.find(town); }
		// Return TownInfo for given town

   const AIC_TownInfoList& townInfo() const { return d_townInfo; }
   AIC_TownInfoList& townInfo() { return d_townInfo; }      //lint !e1536 ... exposing member


#ifdef DEBUG
	AIC_TownConnectivity::Type getConnectivity(ITown town) { return d_townInfo.getConnectivity(town); }
#endif

   unsigned int priority() const;   // get priority of things that have changed.

 private:
#ifdef DEBUG
	// void log(LogFile* log, const CampaignData* campData);
#endif
};
#endif   // Old Stuff

#endif /* AIC_WLD_HPP */

