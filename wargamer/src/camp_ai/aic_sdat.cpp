/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI: Strategy Data
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_sdat.hpp"
#include "campdint.hpp"
// #include "aic_org.hpp"
// #include "aic_unit.hpp"
#include "armies.hpp"
#include "town.hpp"

#ifdef DEBUG
#include "scenario.hpp"
#include "logw_imp.hpp"
#include "app.hpp"
#include "ailog.hpp"
#endif


static const float InitialCaution = 1.0;
// static const float InitialAgression = 1.0;
static const float InitialAgression = 1.5;

AIC_StrategyData::AIC_StrategyData(Side s, const CampaignData* campData) :
   d_side(s),
   d_campData(campData),
   #ifdef DEBUG
      d_random(s),         // Set seed for reproducible behaviour
      d_aiLogWindow(),
   #else
      d_random(),       // Random behaviour
   #endif
   // d_threatRange(campData->getTowns().entries()),
   d_agression(InitialAgression),
   d_caution(InitialCaution),
   // d_worldSpeed(InitialWorldSpeed),
   // d_worldInfo(campData),
   // d_unitInfo(&campData->getArmies()),
   d_resInfo()
   // d_activities()
{
#ifdef DEBUG

   /*
    * NOTE:
    *    Whatever thread creates this must handle Window Messages!
    */

   String title = scenario->getSideName(s);
   title += " AI";
   String logName = scenario->getSideName(s);
   logName += "_AI.log";


//   WindowsLogWindow::make(&d_aiLogWindow, APP::getMainHWND(), title.c_str(), logName.c_str());
   d_aiLogWindow.set(WindowsLogWindow::make(APP::getMainHWND(), title.c_str(), logName.c_str()));
   // if(d_aiLogWindow)
      d_aiLogWindow.printf("CampaignAI (side=%s) constructed", scenario->getSideName(d_side));
   aiLog.printf("CampaignAI (side=%s) constructed", scenario->getSideName(d_side));
#endif
}

AIC_StrategyData::~AIC_StrategyData()
{
   d_campData = 0;   // Keep Lint Happy
}


const Armies* AIC_StrategyData::armies() const
{
   return &campData()->getArmies();
}



#ifdef DEBUG
void AIC_StrategyData::logWin(const char* fmt, ...)
{
   // if(d_aiLogWindow != 0)
   {
      va_list vaList;
      va_start(vaList, fmt);
      d_aiLogWindow.vprintf(fmt, vaList);
      va_end(vaList);
   }

   /*
    * Also add it to ai.log
    */

   {
      va_list vaList;
      va_start(vaList, fmt);
      aiLog.vprintf(fmt, vaList);
      va_end(vaList);
   }

}
#endif



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:16  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
