/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_MSG_HPP
#define AIC_MSG_HPP

#ifndef __cplusplus
#error aic_msg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	In-Game Message Processing
 *
 *----------------------------------------------------------------------
 */

#include "codelet.hpp"
#include "campmsg.hpp"
#include "aic_sdat.hpp"
#include <queue>				// STL adapater
// #include <deque>				// STL container

class AIC_TownList;
class AIC_ResourceProcess;

class AIC_MessageProcess : public AIC_Codelet
{
	public:
		AIC_MessageProcess(AIC_StrategyData* sDat, AIC_TownList* towns) :    //, AIC_ResourceProcess* resProc) :
		   AIC_Codelet(),
         d_sData(sDat),
         d_towns(towns),
         // d_resProc(resProc),
		   d_msgList()
		{
		}

		~AIC_MessageProcess()
		{
		   d_sData = 0;
         d_towns = 0;
         // d_resProc = 0;
		}

		void run(unsigned int r);
		void init();
      void timeUpdate(TimeTick t);

		void addMessage(const CampaignMessageInfo& msg);
	private:
      void increasePriority(AIC_CODELET_ID id, ITown itown);

         #ifdef DEBUG
         String name() const { return("MessageProcess"); }
         #endif

		AIC_StrategyData* d_sData;
      AIC_TownList* d_towns;
      // AIC_ResourceProcess* d_resProc;
      std::queue<CampaignMessageInfo, std::deque<CampaignMessageInfo> > d_msgList;
};


#endif /* AIC_MSG_HPP */

