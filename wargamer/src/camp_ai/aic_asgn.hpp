/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"


#ifndef AIC_ASGN_HPP
#define AIC_ASGN_HPP

#ifndef __cplusplus
#error aic_asgn.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI: Assigning units to activities
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"

class AIC_StrategyData;
class AIC_Activity;

class AIC_AssignUnits
{
	AIC_AssignUnits();		// Prevent Instantiation
 public:
	static Boolean assignSP(AIC_StrategyData* sData, AIC_Activity* activity);
		// Take Strength Points from parent
		// Return False if priority should be lowered

	// static Boolean takeSP(AIC_StrategyData* sData, AIC_Activity* activity);
		// Take Strength Points from children
		// Return False if priority should be lowered
};

#endif /* AIC_ASGN_HPP */

