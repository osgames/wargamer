/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIC_TOWN_HPP
#define AIC_TOWN_HPP

#ifndef __cplusplus
#error aic_town.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI : Town priorities
 *
 *----------------------------------------------------------------------
 */

// #include "codelet.hpp"
#include "gamedefs.hpp"
#include "aic_con.hpp"			// World Connectivity
#include <vector>
#include "measure.hpp"
#include "sync.hpp"
// #include "aic_threat.hpp"

#ifdef DEBUG
class DrawDIBDC;
class PixelPoint;
// class LogFile;
#endif

class AIC_StrategyData;

// class AIC_Activity;
// class CampaignData;
// class TownList;
// class ProvinceList;
// class RandomNumber;


// class AIC_Objectives;

#undef AIC_PROVINCES


/*--------------------------------------------------------------------
 * Town Info
 */

class AIC_TownInfo {
      friend class AIC_TownList;

    public:
 	   AIC_TownInfo();

      bool readData(FileReader& f);
      bool writeData(FileWriter& f) const;

      ITown town() const { return d_town; }
      unsigned int importance() const { return d_importance; }
      unsigned int priority() const { return d_pickPriority; }
      unsigned int resValue() const { return d_resValue; }
      AIC_TownConnectivity::Type conValue() const { return d_conValue; }
      Distance enemyDistance() const { return d_enemyDistance; }
      UWORD enemyTownLinks() const { return d_enemyTownLinks; }

      bool needsSupplyDepot() const { return d_needSupplyDepot; }

   private:
	   ITown		                  d_town;					// What town is this about?
	   UWORD		                  d_enemyTownLinks;		// How many connections is it to an enemy town
	   Distance                   d_enemyDistance;		// What is the physical distance to an enemy town
	   unsigned int 		         d_resValue;				// resource/supply value
	   AIC_TownConnectivity::Type	d_conValue;	         // connection/location value
      bool                       d_needSupplyDepot;   // set if good location for depot
	   unsigned int		         d_importance;			// An overall value
      unsigned int               d_pickPriority;      // Chance of being chosen
      // UnitThreat::Influence      d_threat;            // Threat from enemy units

      static UWORD s_fileVersion;
};


class AIC_TownList :
	public RWLock
{
   public:
      typedef std::vector<AIC_TownInfo> Container;

   private:
	   AIC_TownList(const AIC_TownList&);     // !e1714 !e1526
	   AIC_TownList& operator = (const AIC_TownList&);    // !e1714 !e1526

   public:
	   AIC_TownList(AIC_StrategyData* sData); //, AIC_Objectives* objectives);
	   ~AIC_TownList();

      bool readData(FileReader& f);
      bool writeData(FileWriter& f) const;

	   void make();
      const AIC_TownInfo& pick(unsigned int r);
      void unpick(ITown itown);

	   AIC_TownInfo& find(ITown town);
	   const AIC_TownInfo& find(ITown town) const;
		   // Return info about a town

   #ifdef DEBUG
	   // AIC_TownConnectivity::Type getConnectivity(ITown town) { return d_connectivity.get(town); }
   #endif

	   unsigned int conModifier() const { return d_conModifier; }
	   void conModifier(unsigned int v) { d_conModifier = v; }
	   unsigned int resModifier() const { return d_resModifier; }
	   void resModifier(unsigned int v) { d_resModifier = v; }

      unsigned int totalImportance() const { return d_totalImportance; }
      unsigned int priority() const { return d_totalPickPriority; }
      unsigned int maxPriority() const;

      unsigned int entries() const { return d_items.size(); }

      #ifdef DEBUG
      void showTown(DrawDIBDC* dib, ITown itown, const PixelPoint& p);
      #endif


    private:
//	   void sort();

      void setPriority(AIC_TownInfo& info, unsigned int p);
      void setImportance(AIC_TownInfo& info, unsigned int p);

      void modifyNeighbours();
         // Add proportion of neighbours priorities to each town

      void calcSupplyDepots(const CampaignData* campData);

   #ifdef DEBUG
	   void logTowns();
   #endif

   private:
      Container d_items;

	   unsigned int d_conModifier;
	   unsigned int d_resModifier;

	   AIC_TownConnectivity d_connectivity;
	   unsigned int d_totalImportance;
	   unsigned int d_totalPickPriority;

      AIC_StrategyData* d_sData;
      // AIC_Objectives* d_objectives;

      static UWORD s_fileVersion;
};


#ifdef AIC_PROVINCES

/*--------------------------------------------------------------------
 * Province Info
 */

struct AIC_ProvinceInfo
{
	IProvince	d_province;		// Province ID
	unsigned int	 		d_priority;		// How important is this province
	SPCount 		d_strength;		// What SP is needed to attack/defend this province?
	// More information including
	//	- How much we own
	// - Whether we control the capital
	// - Minimum Links to enemy
	// - Minimum Connectivity

	AIC_ProvinceInfo();
};

class AIC_ProvinceInfoList : public Array<AIC_ProvinceInfo>
{
	unsigned int d_totalPriority;

	AIC_ProvinceInfoList(const AIC_ProvinceInfoList&);
	AIC_ProvinceInfoList& operator = (const AIC_ProvinceInfoList&);
 public:
 	AIC_ProvinceInfoList(const CampaignData* campData);
 	~AIC_ProvinceInfoList() { }

	void make(const CampaignData* campData, const AIC_TownList& townInfo, Side s);
	const AIC_ProvinceInfo& pick(RandomNumber& r) const;

	AIC_ProvinceInfo& find(IProvince p);
	const AIC_ProvinceInfo& find(IProvince p) const;

	unsigned int totalPriority() const { return d_totalPriority; }

 private:
#ifdef DEBUG
	void log(const ProvinceList& provList);
#endif
};

#endif	// AIC_PROVINCES



#endif /* AIC_TOWN_HPP */

