# Microsoft Developer Studio Project File - Name="camp_ai" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=camp_ai - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "camp_ai.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "camp_ai.mak" CFG="camp_ai - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "camp_ai - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "camp_ai - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "camp_ai - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CAMP_AI_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /I "..\campData" /I "..\gwind" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMP_AI_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 gdi32.lib user32.lib /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CAMP_AI_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campLogic" /I "..\campData" /I "..\gwind" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMP_AI_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 gdi32.lib user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/camp_aiDB.dll" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "camp_ai - Win32 Release"
# Name "camp_ai - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\aic.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_act.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\aic_asgn.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\aic_con.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_grow.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\aic_makeobjective.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_msg.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_objective.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_ordr.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_org.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_proc.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\aic_res.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_sdat.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_stra.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_threat.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_town.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_unit.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_unitinfluence.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_util.cpp
# End Source File
# Begin Source File

SOURCE=.\aic_wld.cpp
# End Source File
# Begin Source File

SOURCE=.\ailog.cpp

!IF  "$(CFG)" == "camp_ai - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "camp_ai - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\codelet.cpp
# End Source File
# Begin Source File

SOURCE=.\codelist.cpp
# End Source File
# Begin Source File

SOURCE=.\objectivewindow.cpp
# End Source File
# Begin Source File

SOURCE=.\resinfo.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\aic.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_act.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_asgn.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_con.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_grow.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_makeobjective.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_msg.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_objective.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_ordr.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_org.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_proc.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_res.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_sdat.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_stra.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_threat.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_town.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_unit.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_unitinfluence.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_util.hpp
# End Source File
# Begin Source File

SOURCE=.\aic_wld.hpp
# End Source File
# Begin Source File

SOURCE=.\ailog.hpp
# End Source File
# Begin Source File

SOURCE=.\codelet.hpp
# End Source File
# Begin Source File

SOURCE=.\codelist.hpp
# End Source File
# Begin Source File

SOURCE=.\decayaverage.hpp
# End Source File
# Begin Source File

SOURCE=.\objectivewindow.hpp
# End Source File
# Begin Source File

SOURCE=.\resinfo.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
