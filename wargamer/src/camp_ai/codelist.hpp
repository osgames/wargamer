/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CODELIST_HPP
#define CODELIST_HPP

#ifndef __cplusplus
#error codelist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	List of Codelets
 *
 *----------------------------------------------------------------------
 */

#include "codelet.hpp"

class RandomNumber;

class AIC_CodeletList : public AIC_Codelet
{
		typedef std::vector<AIC_Codelet*> Container;

	public:

      typedef UWORD ID;
      enum
      {
         Unused = UWORD_MAX
      };

		AIC_CodeletList() : AIC_Codelet(), d_items() { }
		~AIC_CodeletList() { }

		ID add(AIC_Codelet* codelet);
      AIC_Codelet* get(ID id);

		void run(unsigned int rand);
      void init();
      virtual void timeUpdate(TimeTick t);

      #ifdef DEBUG
      virtual void log(LogWindow* log) const;
      virtual String name() const { return("CodeletList"); }
      #endif

      void deleteAll();

	private:
		Container d_items;
};


#endif /* CODELIST_HPP */

