/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Campaign AI World Analysis Data and Utilities
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aic_wld.hpp"
#include "aic_sdat.hpp"
#include "aic_town.hpp"
#include "aic_unit.hpp"
#include "aic_objective.hpp"
#include "armies.hpp"

AIC_WorldProcess::AIC_WorldProcess(AIC_StrategyData* sDat, AIC_TownList* towns) : //, AIC_Codelet* resProc) :
   AIC_Codelet(),
   d_sData(sDat),
   d_townList(towns)
   // d_resProc(resProc)
{
}

AIC_WorldProcess::~AIC_WorldProcess()
{
   d_sData = 0;
   d_townList = 0;
   // d_resProc = 0;
}


/*-------------------------------------------------------------------
 * World Process Codelet
 * Update all world information
 */

void AIC_WorldProcess::run(unsigned int r)
{
#ifdef DEBUG
   d_sData->logWin("Analyzing World");
#endif

   /*
    * Update Town and Province Information
    */

   d_townList->make();

   /*
    * Apply Changed town information to activities
    *
    * Really need to completely update activities SPNeeded and SPAlloced
    * instead of relying on differences.
    * Priorities are not so critical, as they are only a measure of
    * how important an event is.
    */

   /*
    * Reset our priority until more game-time has occured
    */

   d_sData->priority(AIC_CODE_Resource, d_townList->totalImportance());
   priority(0);      // Reset our own priority
}

/*
 * Initialise the Process
 *
 * For now we just do an initial run
 */

void AIC_WorldProcess::init()
{
   run(0);
}

void AIC_WorldProcess::timeUpdate(TimeTick t)
{
   // Set it so that it gets picked aproximately once in 2 weeks

   unsigned int p = MulDiv(t, d_sData->priorityRange(), WeeksToTicks(1));     // 1 week
//   unsigned int p = MulDiv(t, d_townList->totalImportance(), WeeksToTicks(2));
   addPriority(p);
}


/*
 * Unit Processor
 */


AIC_UnitProcess::AIC_UnitProcess(AIC_StrategyData* sDat, AIC_UnitList* units, AIC_ObjectiveList* objectives) :
   AIC_Codelet(),
   d_sData(sDat),
   d_unitList(units),
   d_objectives(objectives)
   // d_resProc(resProc)
{
}

AIC_UnitProcess::~AIC_UnitProcess()
{
   d_sData = 0;
   d_unitList = 0;
   d_objectives = 0;
   // d_resProc = 0;
}


/*-------------------------------------------------------------------
 * Unit Process Codelet
 * Update all Unit information
 */

void AIC_UnitProcess::run(unsigned int r)
{
#ifdef DEBUG
   d_sData->logWin("Analyzing Units");
#endif

   // Remove any units that are dead or not in OB any more
   // because erase() probably invalidates iterators
   // we restart the loop every time we erase one

   // Lock the data for reading

   const Armies* army = d_sData->armies();
   ArmyReadLocker lock(*army);

   AIC_UnitList::iterator it = d_unitList->begin();
   while(it != d_unitList->end())
   {
      ConstICommandPosition cp = (*it).first;
      AIC_UnitData* unitData = &(*it).second;

      if(!d_unitList->isUnitValid(cp) || (cp->getSide() != d_sData->side()))
      {
#ifdef DEBUG
         d_sData->logWin("UnitList::removed(%s) because it is not valid", (const char*) cp->getName());
#endif
         if(unitData->objective())
            d_objectives->remove(unitData->objective()->town());

         d_unitList->erase(it);
         it = d_unitList->begin();        // restart the loop
      }
      else
         ++it;
   }

   // Add and update any units

   ConstUnitIter unitIter(army, army->getFirstUnit(d_sData->side()));
   while(unitIter.sister())
   {
      ConstICommandPosition cp = unitIter.current();
      AIC_UnitRef aiUnit = d_unitList->getOrCreate(cp);
      aiUnit.update(army);
   }

   // Get the reorganize codelet to run soon

   d_sData->addPriority(AIC_CODE_Reorganize, d_sData->priorityRange() / 4);
   priority(0);
}

/*
 * Initialise the Process
 *
 * For now we just do an initial run
 */

void AIC_UnitProcess::init()
{
   d_unitList->init(d_sData->armies(), d_sData->side());
}

void AIC_UnitProcess::timeUpdate(TimeTick t)
{
//   unsigned int p = MulDiv(t, d_sData->priorityRange(), WeeksToTicks(2));
   unsigned int p = MulDiv(t, d_sData->priorityRange(), WeeksToTicks(1));     // 1 week
   addPriority(p);
}



#if 0    // Old stuff
/*---------------------------------------------------------------------
 * World Info
 */

AIC_WorldInfo::AIC_WorldInfo(const CampaignData* campData) :
#ifdef AIC_PROVINCES
   d_provInfo(campData),
#endif
   d_townInfo(campData)
{
}



const AIC_TownInfo& AIC_WorldInfo::pickTown(RandomNumber& r)
{
   return d_townInfo.pick(r);
}

#ifdef AIC_PROVINCES

const AIC_ProvinceInfo& AIC_WorldInfo::pickProvince(RandomNumber& r) const
{
   return d_provInfo.pick(r);
}

#endif


void AIC_WorldInfo::make(const CampaignData* campData, Side s)
{
   d_townInfo.make(campData, s);
#ifdef AIC_PROVINCES
   d_provInfo.make(campData, d_townInfo, s);
#endif
}

unsigned int AIC_WorldInfo::priority() const
{
   unsigned int p = d_townInfo.totalPriority();
   return p;
}
#endif


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
