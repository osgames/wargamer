/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef RESINFO_HPP
#define RESINFO_HPP

#ifndef __cplusplus
#error resinfo.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Campaign AI ; resource Information
 *
 *----------------------------------------------------------------------
 */

#include "unittype.hpp"
#include "misc.hpp"

class AIC_Codelet;

template<class T>
class EnumType
{
	public:
		EnumType() : d_value(static_cast<T>(0)) { }
		EnumType(T v) : d_value(v) { }
		// EnumType<T>& operator = (T v) { d_value = v; return *this; }
		operator T() const { return d_value; }
		EnumType<T>& operator++() { INCREMENT(d_value); return *this; }

	private:
		T d_value;
};

class AIC_ResourceInformation
{
	public:
		AIC_ResourceInformation();
		~AIC_ResourceInformation();

		// void codelet(AIC_Codelet* code);
		// void addPriority(int p);

		enum WhatToBuild
		{
			WB_First,

			WB_Infantry = WB_First,
			WB_Cavalry,
			WB_Artillery,
			WB_Engineer,
			WB_Bridgetrain,
			WB_Siegetrain,

			WB_Count,

			WB_None = WB_Count
		};

		void adjustRatio(WhatToBuild what, float ratio);
			// adjust type such that ratio (0..1) of what is required
			// eg. (WB_Infantry, 0.5), means try to maintain 50% infantry
			// Not quite as simple as this... what it really does is
			// alter the current ratio, eg. 0.5 will lower the priority
			// by 50%, 1.5 will increase it.

		void adjustUrgency(float ratio);
			// alter urgency to (urgency * ratio)
			// eg. 0.5 will reduce urgency by 50%

		#ifdef DEBUG
			static const char* whatBuildName(WhatToBuild wb);
		#endif
		static BasicUnitType::value basicType(WhatToBuild wb);
		static SpecialUnitType::value specialType(WhatToBuild wb);

		typedef UBYTE Urgency;
		enum
		{
			Min_Urgency = 0,
			Max_Urgency = UBYTE_MAX,
			Mid_Urgency = (Max_Urgency + 1 + Min_Urgency) / 2
		};

		Urgency urgency() const { return d_urgency; }
		int buildRatio(WhatToBuild bw) const { return d_buildRatios[bw]; }

	private:
		// AIC_Codelet* d_proc;


		Urgency d_urgency;				// 0=high quality, 255=fast build/low quality
		int d_buildRatios[WB_Count];	// What ratio should OB have of each type?
};


#endif /* RESINFO_HPP */

