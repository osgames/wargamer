/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Custom Track Bar
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ctbar.hpp"
#include "dib.hpp"
//#include "scn_res.h"
//#include "scenario.hpp"
#include "bargraph.hpp"
#include "palette.hpp"
#include "palwind.hpp"



WNDPROC CustomTrackBar::lpfnTBWndProc = 0;

LRESULT CALLBACK CustomTrackBar::tbSubClassProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  ASSERT(lpfnTBWndProc != 0);
  CustomTrackBar* ctb = (CustomTrackBar*)GetWindowLong(hwnd, GWL_USERDATA);
  ASSERT(ctb != 0);

  switch(msg)
  {
	 case WM_ERASEBKGND:
		return TRUE;
	 case WM_PAINT:
		ctb->onPaint(hwnd);
		return TRUE;
#if 0
	 case WM_MOUSEMOVE:
		ctb->relayToolTip();
		break;
#endif
  }

  return CallWindowProc(lpfnTBWndProc, hwnd, msg, wParam, lParam);
}

const int ThumbIconWidth = 8;

// CustomTrackBar::CustomTrackBar(HWND p, HWND hwnd, HWND hTTip, const DrawDIB* dib)
CustomTrackBar::CustomTrackBar(HWND p, HWND hwnd, const DrawDIB* dib, const CustomBorderInfo& info)
{
  ASSERT(p != 0);
  ASSERT(hwnd != 0);
  ASSERT(dib != 0);
  parent = p;
  hWnd = hwnd;
  bkDib = dib;
  // hToolTip = hTTip;

  lightBrown = info.colours[LightBrown];
  offWhite = info.colours[OffWhite];
  tan = info.colours[Tan];
  darkBrown = info.colours[DarkBrown];

  numTics = SendMessage(hwnd, TBM_GETNUMTICS, 0, 0);

  tics = new POINT[numTics];
  ASSERT(tics != 0);

  RECT r;
  SendMessage(hwnd, TBM_GETCHANNELRECT, 0, (LPARAM) (LPRECT)&r);

  RECT cr;
  GetClientRect(hwnd, &cr);

  dDib = 0;

  dDib = new DrawDIBDC(cr.right-cr.left, cr.bottom-cr.top);
  ASSERT(dDib != 0);

//  image.loadBitmap(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_TBTHUMBICON), ThumbIconWidth, RGB(255, 255, 255));

  double nSpaces = numTics-1;
  double width = (r.right-r.left);
  double space = width / nSpaces;

  for(int i = 0; i < numTics; i++)
  {
	 tics[i].x = (int)(r.left + (i * space));
	 tics[i].y = cr.top + 8;
  }
  SetClassLong(hwnd, GCL_HBRBACKGROUND, (long)(HBRUSH)NULL);

  SetWindowLong(hwnd, GWL_USERDATA, (DWORD)this);
  lpfnTBWndProc = (WNDPROC) SetWindowLong(hwnd, GWL_WNDPROC, (DWORD) tbSubClassProc);
}


CustomTrackBar::~CustomTrackBar()
{
  if(dDib)
	 delete dDib;
}

void CustomTrackBar::onPaint(HWND hwnd)
{
  // let it run paint routine, otherwise it loops infinitly
  PAINTSTRUCT ps;
  BeginPaint(hwnd, &ps);
  EndPaint(hwnd, &ps);

  // get a DC without pre-defined clipping region
  HDC hdc = GetDC(hwnd);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  ASSERT(bkDib != 0);
  dDib->fill(bkDib);

  drawChannel(hwnd);

  drawTics();

  drawThumb(hwnd);

  RECT r;
  GetClientRect(hwnd, &r);

  BitBlt(hdc, 0, 0, r.right, r.bottom, dDib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, False);
  ReleaseDC(hwnd, hdc);
}

void CustomTrackBar::drawChannel(HWND hwnd)
{

  RECT r;
  SendMessage(hwnd, TBM_GETCHANNELRECT, 0, (LPARAM) (LPRECT)&r);

  int length = SendMessage(hwnd, TBM_GETTHUMBLENGTH, 0, 0);

  // draw channel
  dDib->hLine(r.left, r.top, r.right-r.left, dDib->getColour(lightBrown));
  dDib->vLine(r.left, r.top, 4, dDib->getColour(lightBrown));
  dDib->hLine(r.left+1, r.top+1, (r.right-r.left)-2, dDib->getColour(darkBrown));
  dDib->hLine(r.left+1, r.top+2, (r.right-r.left)-1, dDib->getColour(offWhite));
  dDib->vLine(r.right-1, r.top+1, 3, dDib->getColour(offWhite));
  dDib->hLine(r.left, r.top+3, (r.right-r.left)-1, dDib->getColour(lightBrown));

}

void CustomTrackBar::drawTics()
{
  // draw tic marks
  for(int i = 0; i < numTics; i++)
  {
	 dDib->vLine(tics[i].x, tics[i].y, 5, dDib->getColour(darkBrown));
  }
}

void CustomTrackBar::drawThumb(HWND hwnd)
{
  RECT r;
  SendMessage(hwnd, TBM_GETTHUMBRECT, 0, (LPARAM) (LPRECT)&r);

  // draw thumb
  dDib->vLine(r.left, r.top, 7, dDib->getColour(offWhite));
  dDib->vLine(r.left+1, r.top, 8, dDib->getColour(offWhite));
  dDib->vLine(r.left+2, r.top, 7, dDib->getColour(offWhite));
  dDib->plot(r.left+2, r.top+7, dDib->getColour(lightBrown));
  dDib->plot(r.left+2, r.top+8, dDib->getColour(darkBrown));
  dDib->plot(r.left+3, r.top, dDib->getColour(offWhite));
  dDib->vLine(r.left+3, r.top+1, 6, dDib->getColour(lightBrown));
  dDib->plot(r.left+3, r.top+7, dDib->getColour(darkBrown));
  dDib->vLine(r.left+4, r.top, 7, dDib->getColour(darkBrown));
}

#if 0
void CustomTrackBar::relayToolTip()
{
	if(hToolTip)
	{
	  MSG msg;
	  msg.hwnd = hWnd;
	  msg.message = WM_MOUSEMOVE;
	  SendMessage(hToolTip, TTM_RELAYEVENT, 0, (LPARAM) (LPMSG) &msg);
	}
}
#endif








