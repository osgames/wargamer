/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Implementation of Generic Application Class
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "app.hpp"
#include "threadMsg.hpp"
#include "myassert.hpp"

// #define DEBUG_MESSAGES
#undef DEBUG_MESSAGES

#ifdef DEBUG_MESSAGES
// #include "clog.hpp"
// #include "msgenum.hpp"
// LogFileFlush mLog("msg.log");
#endif

/*
 * Instances of  Static Members
 */

HINSTANCE	APP::s_hinst				 = NULL;	// Global Instance
LPSTR 		APP::s_lpCmdLine			 = 0;		// Command Line
int			APP::s_nCmdShow			 = 0;		// Initial show state
HWND			APP::s_activeDialogue	 = NULL;	// Needed for isDialogMessage()
HWND			APP::s_activePropsheet	 = NULL;	// Needed for PropSheet_IsDialogMessage
HWND			APP::s_mainHWND			 = NULL;	// Top level HWND
DWORD      APP::s_interfaceThreadID = 0;    // User-Interface Thread ID

/*
 * Constructor
 */


APP::APP()
{
	// Should probably initialise static members to something
	ASSERT(s_hinst == NULL);
}


int
APP::execute(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	int retVal = 0;

	try {
		if(!init(hInstance, hPrevInstance, lpszCmdLine, nCmdShow))
			return 0;

		retVal = messageLoop();
		endApplication();
		return retVal;
	}
	catch(...)
	{
		endApplication();
		throw;
	}
}


BOOL APP::init(HINSTANCE inst, HINSTANCE prev, LPSTR cmd, int show)
{
	ASSERT(s_hinst == NULL);		// Not already in use?

	s_hinst = inst;
	s_lpCmdLine = cmd;
	s_nCmdShow = show;
	s_activeDialogue = NULL;
	s_activePropsheet = NULL;
	s_interfaceThreadID = GetCurrentThreadId();
	// s_active = FALSE;

	if(!prev)
		if(!initApplication())
			return FALSE;

	if(!doCmdLine(cmd))
		return FALSE;

	if(!initInstance())
		return FALSE;

	return TRUE;
}

int APP::messageLoop()
{
   WargameMessage::MessageQueue* queue = WargameMessage::MessageQueue::instance();

   HANDLE handles[1];
   handles[0] = queue->syncObject().handle();

   for(;;)
	{
      DWORD result = MsgWaitForMultipleObjects(1, handles, FALSE, INFINITE, QS_ALLEVENTS);

      if (result == WAIT_TIMEOUT)
      {
         // Timeout... do nothing
      }
      if(result == WAIT_OBJECT_0)
      {
         // Custom Messages available

         while (!queue->isEmpty())
         {
            WargameMessage::MessageBase* msg = queue->get();
            msg->run();
            msg->clear();
         }
      }
      else if(result == WAIT_OBJECT_0 + 1)
      {
         // Windows messages available
         MSG msg ;

         // read all of the messages in this next loop
         // removing each message as we read it
         while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
         {
            if(msg.message == WM_QUIT)
               return msg.wParam;

		      if(!processMessage(&msg))		// Application can fiddle with message
		      {
			      if(s_activeDialogue && IsDialogMessage(s_activeDialogue, &msg))
				      continue;

			      if(s_activePropsheet)
			      {
				      if(PropSheet_IsDialogMessage(s_activePropsheet, &msg))
					      continue;

				      if(PropSheet_GetCurrentPageHwnd(s_activePropsheet) == NULL)
				      {
					      DestroyWindow(s_activePropsheet);
					      clearPropsheet(s_activePropsheet);
				      }
			      }

			      TranslateMessage(&msg);
			      DispatchMessage(&msg);
		      }
         }

      }
      else
      {
         FORCEASSERT("Illegal result from MsgWaitForMultipleObjects");
      }
	}


#if 0
#ifdef DEBUG_MESSAGES
		// mLog.printf("%s %lu %ld,%ld",
		// 	getWMdescription(msg.hwnd, msg.message, msg.wParam, msg.lParam),
		// 	msg.time,
		// 	msg.pt.x, msg.pt.y);
#endif

		if(!processMessage(&msg))		// Application can fiddle with message
		{
			if(s_activeDialogue && IsDialogMessage(s_activeDialogue, &msg))
				continue;

			if(s_activePropsheet)
			{
				if(PropSheet_IsDialogMessage(s_activePropsheet, &msg))
					continue;

				if(PropSheet_GetCurrentPageHwnd(s_activePropsheet) == NULL)
				{
					DestroyWindow(s_activePropsheet);
					clearPropsheet(s_activePropsheet);
				}
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
#endif
}




/*
 * Keep track of active dialog
 */

void APP::setActiveDial(HWND hDial)
{
	ASSERT(s_activeDialogue == NULL);

	s_activeDialogue = hDial;

	debugLog("Active Window: %08lx\n", (unsigned long) hDial);
}

void APP::clearActiveDial(HWND hDial)
{
	// ASSERT(s_activeDialogue == hDial);

	if(s_activeDialogue == hDial)
	{
		debugLog("Active Window cleared\n");
		s_activeDialogue = NULL;
	}
}

/*
 * Property Sheet Handler
 *
 * NB: There could be several active propsheets open, so we need
 * to build up a list.
 *
 * Why can't the property sheet handler pass some messages onto its
 * parent?  Or allow a handler for the main page, so we can trap
 * the WM_ACTIVATE, etc, messages.
 *
 * I think I may end up writing my own property sheet dialog handler
 * before long.
 */

void APP::setPropsheet(HWND hDial)
{
	ASSERT(s_activePropsheet == NULL);

	s_activePropsheet = hDial;

	debugLog("Active Propsheet %08lx\n", (unsigned long) hDial);
}

void APP::clearPropsheet(HWND hDial)
{
	ASSERT(s_activePropsheet == hDial);

	if(s_activePropsheet == hDial)
	{
		debugLog("Active Propsheet cleared\n");
		s_activePropsheet = NULL;
	}
}


/*
 * Return Handle of main Window
 */

HWND APP::getMainHWND()
{
	return s_mainHWND;
}

/*
 * Set the Main Window handle
 */

void APP::setMainHWND(HWND hWnd)
{
#ifdef DEBUG
	if(hWnd == NULL)
		ASSERT(s_mainHWND != NULL);
	else
		ASSERT(s_mainHWND == NULL);
#endif

	s_mainHWND = hWnd;
}


HINSTANCE APP::instance()
{
	return s_hinst;
}

DWORD APP::getInterfaceThreadID()
{
	return s_interfaceThreadID;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 * Revision 1.3  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/11/07 10:39:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/16 11:31:51  Steven_Green
 * Initial revision
 *----------------------------------------------------------------------
 */

