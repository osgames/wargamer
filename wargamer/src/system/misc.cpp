/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Miscellaneous Support Functions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "misc.hpp"
#include "myassert.hpp"
#include <string.h>

char*

copyString(const char* s)
{
	if(s == 0)
		return 0;
	else
	{
		char* s1 = new char[strlen(s)+1];
		ASSERT(s1 != 0);
		strcpy(s1, s);
		return s1;
	}
}

/*
 * See if two strings are different
 *
 * Can handle the case where either or both of the strings are 0
 */

Boolean

stringsDiffer(const char* s1, const char* s2)
{
	if(s1 == 0)
		return (s2 != 0);
	if(s2 == 0)
		return True;
	else
		return (strcmp(s1, s2) != 0);
}

const char* getNths(int number)
{
  LANGID userID = GetUserDefaultLangID();
  if(PRIMARYLANGID(userID) == LANG_GERMAN)
  {
    return ".";
  }
  else if(PRIMARYLANGID(userID) == LANG_DUTCH)
	{
		if( (number == 1) || (number == 8) || (number >= 20))
			return "ste";
		else
			return "de";
	}
  else if(PRIMARYLANGID(userID) == LANG_FRENCH)
  { // Thanks to Thierry MICHEL for this information
    if(number==1)
    {
      return "er";
    }
    else
    {
      return "\xe8me"; // e8=egrave, Could also be "e" for contracted.
    }
  }
	else	// Assume English
	{
		if((number < 20) && (number >= 10))	// 10th..19th
			number = 0;
		else
			number %= 10;

		switch(number)
		{
      case 1:
        return "st";
      case 2:
        return "nd";
      case 3:
        return "rd";
      default:
        return "th";
		}
	}
}


#ifdef DEBUG

const char*

boolToAscii(Boolean b)
{
	return b ? "True" : "False";
}

#endif

/*
 * Integer Square root
 */

int squareRoot(int n)
{
	ASSERT(n >= 0);

	int low = 0;
	int high = (n + 1) / 2;
	int value = n/4;

	while(low < high)
	{
		int diff = (value * value) - n;

		if(diff == 0)
			return value;

		if(diff > 0)
		{
			high = value - 1;
			value = (low + high + 1) / 2;
		}
		else
		{
			low = value + 1;
			value = (low + high + 1) / 2;
		}
	}

	ASSERT(value != 0);

	return value;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
