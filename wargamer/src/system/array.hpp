/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ARRAY_H
#define ARRAY_H

#ifndef __cplusplus
#error array.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Array class
 *
 * Todo:
 *		Make it handle bounded arrays, e.g. constructor can specify lower value
 *		so if you want you could have an array[1..10] instead of 0..9
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <string.h>
#include "mytypes.h"
#include "except.hpp"
#include "myassert.hpp"

class ArrayBase {
 public:
 	virtual ~ArrayBase() { }

	typedef UWORD ArrayIndex;

	enum { ArrayIndex_MAX = UWORD_MAX };

	class SYSTEM_DLL ArrayError : public GeneralError {
	  public:
	    ArrayError(const char* s);
	    ArrayError(const char* s, int i);
	};

	class SYSTEM_DLL ArrayOutOfRange : public ArrayError {
	  public:
	    ArrayOutOfRange(int i);
	};

	class SYSTEM_DLL ArrayNoMemory : public ArrayError {
	  public:
	  	ArrayNoMemory();
	};
};

typedef ArrayBase::ArrayIndex ArrayIndex;		// For backwards compatibility
enum { ArrayIndex_MAX = ArrayBase::ArrayIndex_MAX };

template<class Type>
class Array : public ArrayBase {
 private:
	ArrayIndex count;
	Type* items;

 private:
	Array(Array<Type>& t);
		// Don't allow copy constructor

 public:
	Array() { count = 0; items = 0; }		// Initialise with no items
	~Array();										// Destructor


	void init(int howMany);
		// Initialize array with howMany members

	Type& get(int n) const;
		// Return Item n
		// Throw error if n out of range!

	void set(int n, Type& f);
	void set(int n, Type* f);
		// Set Item to given value
		// Throws exception if out of range

	Type& operator[] (int n);
		// Get Item using array operator
		// Throws exception if out of range

	const Type& operator[] (int n) const;
		// Get Constant Item using array operator
		// Throws exception if out of range

	Array<Type>& operator = (Array<Type>& src);
		// Copy Array using object's == operator

	ArrayIndex entries() const { return count; }
		// Return number of items

	ArrayIndex getID(const Type* item) const;
		// Get Object's ID from pointer

	void clear();
		// Initialise to 0 items, delete any existing items

	ArrayIndex addNew();
		// Create a new item, returning the new Index value
		// Note that this causes any pointers or references to
		// existing objects to be invalid

	void remove(int i);
		// Remove item from array, shifting others down to fill space
		// Note that this causes any pointers or references to
		// existing objects to be invalid

private:
	void checkRange(int n) const;
		// Validate n for being a valid index
		// Throw exception if out of range

};

/*
 * Iterator class
 */

template<class Type>
class CArrayIter {
 private:
	const Array<Type>* d_container;
	ArrayIndex index;
 public:
	CArrayIter(const Array<Type>& list) { d_container = &list; index = -1; }

	int operator++()
	{
		index++;

		if(index < d_container->entries())
			return 1;
		else
			return 0;
	}

	const Type& current() const 
	{ 
		// ASSERT( (index >= 0) && (index < d_container->entries()));
		ASSERT(index < d_container->entries());
		return d_container->get(index); 
	}

	ArrayIndex currentID() const
	{
		// ASSERT( (index >= 0) && (index < d_container->entries()));
		ASSERT(index < d_container->entries());
		return index;
	}

	void rewind() { index = -1; }
	void backup() { index--; }

	const Array<Type>* container() const { return d_container; }
};

template<class Type>
class ArrayIter {
 private:
	Array<Type>* d_container;
	ArrayIndex index;
 public:
	ArrayIter(Array<Type>& list) { d_container = &list; index = -1; }

	int operator++()
	{
		index++;

		if(index < d_container->entries())
			return 1;
		else
			return 0;
	}

	Type& current() const 
	{ 
		// ASSERT( (index >= 0) && (index < d_container->entries()));
		ASSERT(index < d_container->entries());
		return d_container->get(index); 
	}

	ArrayIndex currentID() const
	{
		// ASSERT( (index >= 0) && (index < d_container->entries()));
		ASSERT(index < d_container->entries());
		return index;
	}

	void rewind() { index = -1; }
	void backup() { index--; }

	Array<Type>* container() { return d_container; }
};

/*
 * Implementations of Array Functions
 *
 * By putting these in a seperate header file, we may be able
 * to cut down on compile time and executable size, by instantiating
 * each usage only once and using wrapper classes.
 */

template<class Type>
inline void Array<Type>::init(int howMany)
{
	ASSERT(howMany >= 0);
	ASSERT(howMany <= ArrayIndex_MAX);

	if(items)
		delete[] items;

	if(howMany == 0)
	{
		items = 0;
		count = 0;
	}
	else
	{
		items = new Type[howMany];
		if(items)
			count = ArrayIndex(howMany);
		else
			throw ArrayNoMemory();
			// count = 0;		// What!!!  Should throw an error.
	}
}

template<class Type>
inline Array<Type>::~Array()
{
	if(items)
		delete[] items;
}

template<class Type>
inline Type& Array<Type>::get(int n) const
{
	checkRange(n);
	return items[n];
}

template<class Type>
inline Type& Array<Type>::operator[] (int n)
{
	checkRange(n);
	return items[n];
}

template<class Type>
inline const Type& Array<Type>::operator[] (int n) const
{
	checkRange(n);
	return items[n];
}

template<class Type>
inline void Array<Type>::set(int n, Type& f) 
{
	checkRange(n);
	items[n] = f; 
}

template<class Type>
inline void Array<Type>::set(int n, Type* f)
{
	checkRange(n);
	items[n] = *f; 
}

template<class Type>
inline Array<Type>& Array<Type>::operator = (Array<Type>& src)
{
	count = src.count;

	if(count)
	{
		items = new Type[count];
		if(items == 0)
			throw ArrayNoMemory();
		for(int i = 0; i < count; i++)
			items[i] = src.items[i];
	}
	else
		items = 0;
	return *this;
}


template<class Type>
inline ArrayIndex Array<Type>::getID(const Type* item) const
{
	int id = item - items;

	ASSERT( (id >= 0) && (id < count) );

	if( (id >= 0) && (id < count) )
		return ArrayIndex(id);
	else
		return ArrayIndex(-1);
}

template<class Type>
inline void Array<Type>::clear()
{
	if(items)
	{
		delete[] items;
		items = 0;
		count = 0;
	}
}

template<class Type>
inline ArrayIndex Array<Type>::addNew()
{
	Type* newItems = new Type[count + 1];
	ASSERT(newItems != 0);

	if(items)
	{
		for(ArrayIndex i = 0; i < count; i++)
			newItems[i] = items[i];
	}
	Type* oldItems = items;
	
	items = newItems;
	count++;
	delete[] oldItems;
	
	return ArrayIndex(count-1);
}

template<class Type>
inline void Array<Type>::remove(int i)
{
	checkRange(i);
	ASSERT(count > 0);

	Type* newItems;
	if(count > 1)
	{
		newItems = new Type[count-1];

		for(ArrayIndex i1 = 0; i1 < i; i1++)
			newItems[i1] = items[i1];
		for(; i1 < (count - 1); i1++)
			newItems[i1] = items[i1 + 1];
	}
	else
		newItems = 0;

	Type* oldItems = items;
	count--;
	items = newItems;

	delete[] oldItems;
}


template<class Type>
inline void Array<Type>::checkRange(int n) const
{
	if( (n >= count) || (n < 0) )
		throw ArrayOutOfRange(n);
}


#endif /* ARRAY_H */


