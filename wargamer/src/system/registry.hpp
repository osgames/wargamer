/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef REGISTRY_H
#define REGISTRY_H

#ifndef __cplusplus
#error registry.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Win95 System registry Handler
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  1996/03/04 17:12:03  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"

/*
 *  for binary data
 */

SYSTEM_DLL Boolean setRegistry(const char* key, const void* data, size_t length, const char* subDir);
SYSTEM_DLL Boolean getRegistry(const char* key, void* data, size_t length, const char* subDir);

/*
 * for ascii data
 */

SYSTEM_DLL Boolean setRegistryString(const char* key, const char* string, const char* subDir);
SYSTEM_DLL Boolean getRegistryString(const char* key, char* string, const char* subDir);


/*
 *  class for putting or getting filenames in registry
 */

class SYSTEM_DLL RegistryFile {
	 char d_key[200];
	 const char* d_registryName;
    int d_fileNumber;
  public:
	 RegistryFile(const char* registryName);

	 Boolean setFileName(const char* fileName);
	 Boolean getFileName(char* fileName);

    void reset();
	 void reset(const char* registryName);

};

// typedef struct tagRECT RECT;
// typedef struct tagPOINT POINT;
// SYSTEM_DLL void checkResolution(RECT& r, POINT& p);

#endif /* REGISTRY_H */

