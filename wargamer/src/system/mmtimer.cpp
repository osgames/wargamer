/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Multi-Media Timer wrapper class implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mmtimer.hpp"
#include "myassert.hpp"
#include "sync.hpp"
// #include <algobase.h>         // STL min/max/swap functions

namespace Greenius_System
{

int MMTimer::s_usage = 0;
unsigned int MMTimer::s_resolution = 0;
SharedData MMTimer::s_lock;

/*
 * Construct a timer
 */

MMTimer::MMTimer(unsigned int ms, Mode repeat, bool autoStart, Event* event) :
   d_timerID(NULL),
   d_tickRate(ms),
   d_repeat(repeat),
#ifdef DEBUG
   d_id(ID),
#endif
   d_event(event)
{
    LockData lock(&s_lock);

   ASSERT(!autoStart);

    if (s_usage++ == 0)
    {
       s_resolution = 1;     // (d_tickRate + 3) / 4;

       TIMECAPS tc;

       if(timeGetDevCaps(&tc, sizeof(TIMECAPS)) != TIMERR_NOERROR)
       {
          throw MMTimerError("Can't get multimedia Timer Capabilities");
       }

       if(tc.wPeriodMin > s_resolution)
          s_resolution = tc.wPeriodMin;
       if(tc.wPeriodMax < s_resolution)
          s_resolution = tc.wPeriodMax;

#ifdef DEBUG
       debugLog("initTimer: min=%u, max=%u, res=%u\n",
          tc.wPeriodMin,
          tc.wPeriodMax,
          s_resolution);
#endif

       ASSERT(s_resolution <= d_tickRate);

       MMRESULT tpResult = timeBeginPeriod(s_resolution);
       if(tpResult != TIMERR_NOERROR)
          throw MMTimerError("Can not set Multimedia Timer Resolution");
    }

    if(autoStart)
        start();
}

/*
 * Destructor
 */

MMTimer::~MMTimer()
{
    LockData lock(&s_lock);

   ASSERT(d_timerID == NULL);       // Derived class should call stop() to avoid pure virtual function call

   if(d_timerID != NULL)
   {
      stop();
   }

    ASSERT(s_usage > 0);
    if (--s_usage == 0)
    {
       MMRESULT result = timeEndPeriod(s_resolution);
       s_resolution = 0;
       ASSERT(result == TIMERR_NOERROR);
    }
}

/*
 * Start timer
 */

void MMTimer::start()
{
    LockData lock(&s_lock);

   ASSERT(d_tickRate != 0);
   ASSERT(d_timerID == NULL);

#ifdef DEBUG
   debugLog("MMTimer::start(), rate=%d\n",
      (int) d_tickRate);
#endif

   UINT flags = 0;
//    if(d_event)
//       flags = TIME_CALLBACK_EVENT_SET;
//    else
      flags = TIME_CALLBACK_FUNCTION;

   if(d_repeat)
      flags |= TIME_PERIODIC;
   else
      flags |= TIME_ONESHOT;

   d_timerID = timeSetEvent(d_tickRate,
        // (d_tickRate+3) / 4,
        1 + d_tickRate / 4,
        timerProc,
        reinterpret_cast<DWORD>(this),
        flags
      );
#if 0
   d_timerID = timeSetEvent(d_tickRate,
      (d_tickRate+3)/4,
      d_event ?  reinterpret_cast<LPTIMECALLBACK>(d_event->handle()) : timerProc,
        reinterpret_cast<DWORD>(this),
          flags
      );
#endif

   ASSERT(d_timerID != NULL);

   if(d_timerID == NULL)
      throw MMTimerError("Can not start Waitable Timer");
}

/*
 * Stop timer
 */

void MMTimer::stop()
{
   LockData lock(&s_lock);

   if (d_timerID != NULL)
   {

      ASSERT(d_timerID != NULL);

#ifdef DEBUG
      debugLog("MMTimer::stop(), rate=%d\n",
         (int) d_tickRate);
#endif

      timeKillEvent(d_timerID);
      d_timerID = NULL;
   }
}

void MMTimer::setRate(unsigned int ms)
{
   bool isRunning = (d_timerID != NULL);

   if(isRunning)
      stop();

   if(ms < 1)
      ms = 1;
   d_tickRate = ms;

   if(isRunning)
      start();
}

/*
 * Local function called on interrupt
 */

void CALLBACK MMTimer::timerProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
   MMTimer* wTimer = reinterpret_cast<MMTimer*>(dwUser);

   ASSERT(wTimer->d_id == ID);
   ASSERT(wTimer->d_timerID == uID);

   if(!wTimer->d_repeat)
   {
      wTimer->d_timerID = NULL;
   }

    if(wTimer->d_event)
        wTimer->d_event->set();

    wTimer->proc();
}

}; // namespace Greenius_System


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */

