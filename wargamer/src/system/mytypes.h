/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MYTYPES_H
#define MYTYPES_H

/*
 *-------------------------------------------------------------
 * $Id$
 *-------------------------------------------------------------
 *
 * User defined Data types
 *
 *-------------------------------------------------------------
 */

#ifdef __WATCOM_CPLUSPLUS__
#pragma off(unreferenced)
#endif

// #include "sysdll.h"
#include <limits.h>

/*
 * String type
 */

#ifdef __WATCOM_CPLUSPLUS__
#include <string.hpp>
#else
#include <string>
typedef std::string String;
#endif


// Is this neccessary?
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Replacements for built in types
 */

typedef unsigned short 	UWORD;
typedef unsigned long 	ULONG;
typedef unsigned char 	UBYTE;
typedef short 				SWORD;
typedef long 				SLONG;
typedef signed char 		SBYTE;

#define UWORD_MIN		0
#define UWORD_MAX		USHRT_MAX
#define UBYTE_MIN		0
#define UBYTE_MAX		UCHAR_MAX
#define SWORD_MIN		SHRT_MIN
#define SWORD_MAX		SHRT_MAX
#define SLONG_MIN		LONG_MIN
#define SLONG_MAX		LONG_MAX
#define SBYTE_MIN		SCHAR_MIN
#define SBYTE_MAX		SCHAR_MAX


#ifndef _SIZE_T_DEFINED_
#define _SIZE_T_DEFINED_
 typedef unsigned int	size_t;
#endif

#if 0

enum {
	False = 0,
	True = 1
};
typedef unsigned char Boolean;

#else

// C++ Standard

typedef bool Boolean;
enum {
	False = false,
	True = true
};

#endif


#ifdef __cplusplus
}
#endif
#endif	/* MYTYPES_H */

/*
 *-------------------------------------------------------------
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.5  1996/01/19 11:30:29  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/10/29 16:02:49  Steven_Green
 * Added _MIN _MAX definitions
 *
 * Revision 1.3  1995/10/25 09:52:49  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/20 11:05:41  Steven_Green
 * My Simple Type definitions
 *
 * Revision 1.1  1995/10/18 10:59:53  Steven_Green
 * Initial revision
 *
 * Revision 1.4  1993/12/03  16:52:10  Steven_Green
 * size_t only defined if not already defined
 *
 * Revision 1.3  1993/11/19  19:01:25  Steven_Green
 * Point operators use Point& wherever possible.
 * Used the const keyword wherever possible.
 *
 * Revision 1.2  1993/10/26  21:33:33  Steven_Green
 * graphics types moved to gr_types
 *
 * Revision 1.1  1993/10/26  14:54:37  Steven_Green
 * Initial revision
 *
 * Revision 1.2  1993/10/21  20:52:00  Steven_Green
 * Changed BOOL to be UWORD instead of enum
 *
 * Revision 1.1  1993/10/20  23:07:58  Steven_Green
 * Initial revision
 *
 *
 *-------------------------------------------------------------
 */
