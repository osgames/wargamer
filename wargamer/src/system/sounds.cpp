/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Sound Effect Playback
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "sounds.hpp"
#include "critical.hpp"
#include "myassert.hpp"
#include <mmsystem.h>

/*
 * Private Base class used internally
 */

class SoundImp {
 public:
	static Boolean s_initialized;
	static SharedData s_shared;

	SoundImp() { }
	~SoundImp() { cleanup(); }

	static void init();
	static void cleanup();
};

/*
 * Define a static instance, so that destructor is always
 * called when program exits
 */

static SoundImp soundImp;

Boolean SoundImp::s_initialized = False;
SharedData SoundImp::s_shared;

/*
 * Do any intialization of the sound system
 * To be Done:
 */

void SoundImp::init()
{
	ASSERT(!s_initialized);

	s_initialized = True;
}

/*
 * Do any cleanup of the sound system
 */

void SoundImp::cleanup()
{
	if(s_initialized)
	{
		s_initialized = False;
	}
}

/*
 * Public Function to play a sound
 */

void Sound::playSound(const char* fileName)
{
	ASSERT(fileName != 0);

	LockData lock(&SoundImp::s_shared);

	if(!SoundImp::s_initialized)
		SoundImp::init();

	// Boolean result = (Boolean)PlaySound((LPCSTR)fileName, NULL, SND_FILENAME | SND_ASYNC);
	HRESULT result = PlaySound((LPCSTR)fileName, NULL, SND_FILENAME | SND_ASYNC);

	ASSERT(result);
   // should used FAILED macro?
	if(!result)
		throw SoundError();
}


/*----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *----------------------------------------------------------------------
 */
