/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Picture Library
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "piclib.hpp"
#include "bmp.hpp"
#include "dib.hpp"
#include "StringPtr.hpp"

/*
 * These are only test functions so far...
 *
 * Replace manager with a cache of pictures
 */

namespace
{

class PicLibraryManager
{
    public:
       PicLibraryManager() : s_lastName(), s_lastDIB(0) { }

       ~PicLibraryManager() { delete s_lastDIB; }

       DIB* readPicture(const char* name);
       void release(DIB* dib) { }

    private:
       StringPtr s_lastName;
       DIB* s_lastDIB;
};

static PicLibraryManager manager;


DIB* PicLibraryManager::readPicture(const char* name)
{
	if(s_lastName == name)
		return s_lastDIB;
	else
	{
		delete s_lastDIB;
		s_lastDIB = 0;
	}

	s_lastDIB = BMP::newDIB(name, BMP::RBMP_Normal);
	s_lastName = name;

    s_lastDIB->setMaskColour(*s_lastDIB->getBits());

	return s_lastDIB;
}


};


PicLibrary::Picture::Picture(const char* name) :
	d_dib(0)
{
	if(name)
		d_dib = manager.readPicture(name);
}

PicLibrary::Picture::~Picture()
{
    manager.release(d_dib);
}


PicLibrary::Picture PicLibrary::get(const char* name)
{
	return Picture(name);
}

