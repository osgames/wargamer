/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef _SPLASH_HPP
#define _SPLASH_HPP

#include "sysdll.h"

class SplashWindow;

class SYSTEM_DLL SplashScreen 
{
  SplashWindow* d_splashWindow;
public:
  SplashScreen(HWND hParent);
  ~SplashScreen();

  void destroy();
  void setText(const char* text);
};


#endif
