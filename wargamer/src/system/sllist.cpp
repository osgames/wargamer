/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 * Written by Paul Sample
 *----------------------------------------------------------------------
 *
 * implementation of list class, essentially copied from Strousteps book
 * with my own remove function
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "sllist.hpp"
//#include "myassert.hpp"



/*
 * implementation of list class, essentially copied from Strousteps book
 * with my own remove function
 */

void

SList_Base::insert(SLink* sLink)
{
	if(last)
	  sLink->next = last->next;
	else
	  last = sLink;
	last->next = sLink;
	nItems++;
}

void

SList_Base::append(SLink* sLink)
{
	if(last)
	{
	  sLink->next = last->next;
	  last->next = sLink;
	  last = sLink;
	}
	else
	  last = sLink->next = sLink;
	nItems++;
}

SLink* SList_Base::get()
{
	ASSERT(last != 0);
	SLink* f = last->next;
	if(f == last)
	  last = 0;
	else
	  last->next = f->next;
	nItems--;
	return f;
}



SLink*
SList_Base::next()
{
	ASSERT(last != 0);
	ASSERT(current != 0);

	prev = current;

	if(current == last)
		current = 0;
	else
		current = current->next;

	return current;
}

void
SList_Base::remove()
{
  ASSERT(last != 0);
  ASSERT(current != 0);
  ASSERT(prev != 0);
  if(current == last && nItems == 1)
  {
  	 if(dyn)
	 	delete current;
	 last = 0;
  }
  else if(current == last)
  {
	 prev->next = last->next;
	 last = prev;
	 if(dyn)
	 	delete current;
  }
  else
  {
	 prev->next = current->next;
	 if(dyn)
	 	delete current;
  }
  current = head();
  prev = last;
  nItems--;
}

SLink* SList_Base::remove(SLink* node)
{
	ASSERT(node != 0);

	SLink* current = head();
	SLink* prev = last;

	ASSERT(current != 0);
	ASSERT(prev != 0);

	while((current != 0) && (current != node))
	{
		prev = current;
		if(current == last)
			current = 0;
		else
			current = current->next;
	}

	ASSERT(current == node);

	if(current == node)
	{
		ASSERT(prev != 0);

		prev->next = current->next;

		if(current == last)
		{
			if(current == prev)		// deleting the last item!
				prev = 0;

			last = prev;
		}

		if(dyn)
			delete current;

		nItems--;
	}

    return prev;
}

SLink*

SList_Base::previous(SLink* node)
{
	ASSERT(node != 0);

	SLink* current = head();
	SLink* prev = last;

	ASSERT(current != 0);
	ASSERT(prev != 0);

	while((current != 0) && (current != node))
	{
		prev = current;
		if(current == last)
			current = 0;
		else
			current = current->next;
	}

	ASSERT(current == node);
   return prev;
}

void

SList_Base::insertAfter(SLink* prevNode, SLink* newNode)
{
	if(prevNode == 0)
		insert(newNode);
	else if(prevNode == last)
		append(newNode);
	else
	{
		newNode->next = prevNode->next;
		prevNode->next = newNode;
		nItems++;
	}
	
}

void 

SList_Base::reset()
{
  while(last)
  {
	 SLink* ob = get();
	 if(dyn)
	 	delete ob;
  }

  ASSERT(nItems == 0);
}

bool SList_Base::isMember(SLink* node) const
{
	if(last)
	{
		SLink* current = last;

		do
		{
			current = current->next;

			if(current == node)
				return true;

		} while(current != last);
	}

	return false;
}

/*
 * Alternative Iterators
 */

void

SList_Base::rewind()
{
	current = 0;
}

bool

SList_Base::operator ++()
{
	if(current == 0)
		return (first() != 0);
	else
		return (next() != 0);
}

/*
 * Iterator Class
 */

bool

ListIter::next()
{
	ASSERT(list != 0);

  if(!started)
  {
	 if(list->getLast() == 0)
		return false;
	 else
		started = true;

	 return true;
  }
  else
  {
	 if((current == 0) || (list->getLast() == current))
		return false;

	 // prev = current;
	 current = current->next;
	 ASSERT(current != 0);
  }
  return true;
}

void

ListIter::remove()
{
	ASSERT(list != 0);
	ASSERT(current != 0);
	// ASSERT(prev != 0);

	if(current)
	{
		// SLink* node = current;
		// SLink* next = node->next;
		// SLink* prev = list->remove(node);

        bool finished = (current == list->getLast());

        current = list->remove(current);

        if(!finished && (current == list->getLast()) )
            rewind();

		//----- Bug... this next line writes over memory that has just been freed
		//----- if deleting the last object in the list
		//
		// prev->next = next;

		// if(prev == list->getLast())
		// {
			// rewind();
        //     current = 0;
		// }
		// else
		// {
		//  current = prev;
		// }

		// if(current == node)
		// 	current = 0;


	}
}

/*
 * Iterator Class
 */

bool ListIterR::next()
{
	ASSERT(list != 0);

  if(!started)
  {
	 if(list->getLast() == 0)
		return false;
	 else
		started = true;

	 return true;
  }
  else
  {
	 if((current == 0) || (list->getLast() == current))
		return false;
    // prev = current;
	 current = current->next;
	 ASSERT(current != 0);
  }
  return true;
}

