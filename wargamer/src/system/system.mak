# Microsoft Developer Studio Generated NMAKE File, Based on system.dsp
!IF "$(CFG)" == ""
CFG=system - Win32 Editor Debug
!MESSAGE No configuration specified. Defaulting to system - Win32 Editor Debug.
!ENDIF 

!IF "$(CFG)" != "system - Win32 Release" && "$(CFG)" != "system - Win32 Debug" && "$(CFG)" != "system - Win32 Editor Debug" && "$(CFG)" != "system - Win32 Editor Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "system.mak" CFG="system - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "system - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "system - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "system - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "system - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "system - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

ALL : "$(OUTDIR)\system.dll"


CLEAN :
	-@erase "$(INTDIR)\alertbox.obj"
	-@erase "$(INTDIR)\animate.obj"
	-@erase "$(INTDIR)\app.obj"
	-@erase "$(INTDIR)\array.obj"
	-@erase "$(INTDIR)\bargraph.obj"
	-@erase "$(INTDIR)\bezier.obj"
	-@erase "$(INTDIR)\bmp.obj"
	-@erase "$(INTDIR)\button.obj"
	-@erase "$(INTDIR)\c_combo.obj"
	-@erase "$(INTDIR)\capstest.obj"
	-@erase "$(INTDIR)\cbutton.obj"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\cmenu.obj"
	-@erase "$(INTDIR)\config.obj"
	-@erase "$(INTDIR)\crc.obj"
	-@erase "$(INTDIR)\critical.obj"
	-@erase "$(INTDIR)\crossref.obj"
	-@erase "$(INTDIR)\cscroll.obj"
	-@erase "$(INTDIR)\ctab.obj"
	-@erase "$(INTDIR)\ctbar.obj"
	-@erase "$(INTDIR)\cust_dlg.obj"
	-@erase "$(INTDIR)\date.obj"
	-@erase "$(INTDIR)\datetime.obj"
	-@erase "$(INTDIR)\dc_man.obj"
	-@erase "$(INTDIR)\dialog.obj"
	-@erase "$(INTDIR)\dib.obj"
	-@erase "$(INTDIR)\dib_blt.obj"
	-@erase "$(INTDIR)\dib_poly.obj"
	-@erase "$(INTDIR)\dib_util.obj"
	-@erase "$(INTDIR)\dibref.obj"
	-@erase "$(INTDIR)\directplay.obj"
	-@erase "$(INTDIR)\dlist.obj"
	-@erase "$(INTDIR)\dragwin.obj"
	-@erase "$(INTDIR)\except.obj"
	-@erase "$(INTDIR)\filebase.obj"
	-@erase "$(INTDIR)\filecnk.obj"
	-@erase "$(INTDIR)\filedata.obj"
	-@erase "$(INTDIR)\fileiter.obj"
	-@erase "$(INTDIR)\filepack.obj"
	-@erase "$(INTDIR)\fillwind.obj"
	-@erase "$(INTDIR)\fonts.obj"
	-@erase "$(INTDIR)\fractal.obj"
	-@erase "$(INTDIR)\framewin.obj"
	-@erase "$(INTDIR)\fsalloc.obj"
	-@erase "$(INTDIR)\gamectrl.obj"
	-@erase "$(INTDIR)\gdiengin.obj"
	-@erase "$(INTDIR)\grtypes.obj"
	-@erase "$(INTDIR)\gsecwind.obj"
	-@erase "$(INTDIR)\gtoolbar.obj"
	-@erase "$(INTDIR)\idstr.obj"
	-@erase "$(INTDIR)\imglib.obj"
	-@erase "$(INTDIR)\itemwind.obj"
	-@erase "$(INTDIR)\logwin.obj"
	-@erase "$(INTDIR)\lzdecode.obj"
	-@erase "$(INTDIR)\lzencode.obj"
	-@erase "$(INTDIR)\lzmisc.obj"
	-@erase "$(INTDIR)\makename.obj"
	-@erase "$(INTDIR)\mciwind.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\mmtimer.obj"
	-@erase "$(INTDIR)\msgbox.obj"
	-@erase "$(INTDIR)\msgenum.obj"
	-@erase "$(INTDIR)\myassert.obj"
	-@erase "$(INTDIR)\nametable.obj"
	-@erase "$(INTDIR)\palette.obj"
	-@erase "$(INTDIR)\palwin.obj"
	-@erase "$(INTDIR)\palwind.obj"
	-@erase "$(INTDIR)\pathdial.obj"
	-@erase "$(INTDIR)\piclib.obj"
	-@erase "$(INTDIR)\poolarry.obj"
	-@erase "$(INTDIR)\popicon.obj"
	-@erase "$(INTDIR)\ptrlist.obj"
	-@erase "$(INTDIR)\random.obj"
	-@erase "$(INTDIR)\registry.obj"
	-@erase "$(INTDIR)\resstr.obj"
	-@erase "$(INTDIR)\scrnbase.obj"
	-@erase "$(INTDIR)\simpstr.obj"
	-@erase "$(INTDIR)\sllist.obj"
	-@erase "$(INTDIR)\sounds.obj"
	-@erase "$(INTDIR)\soundsys.obj"
	-@erase "$(INTDIR)\splash.obj"
	-@erase "$(INTDIR)\sprlib.obj"
	-@erase "$(INTDIR)\sync.obj"
	-@erase "$(INTDIR)\tables.obj"
	-@erase "$(INTDIR)\tabwin.obj"
	-@erase "$(INTDIR)\thicklin.obj"
	-@erase "$(INTDIR)\thread.obj"
	-@erase "$(INTDIR)\threadmsg.obj"
	-@erase "$(INTDIR)\timer.obj"
	-@erase "$(INTDIR)\todolog.obj"
	-@erase "$(INTDIR)\tooltip.obj"
	-@erase "$(INTDIR)\trackwin.obj"
	-@erase "$(INTDIR)\transwin.obj"
	-@erase "$(INTDIR)\trig.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\w95excpt.obj"
	-@erase "$(INTDIR)\wavefile.obj"
	-@erase "$(INTDIR)\winctrl.obj"
	-@erase "$(INTDIR)\wind.obj"
	-@erase "$(INTDIR)\winerror.obj"
	-@erase "$(INTDIR)\winfile.obj"
	-@erase "$(INTDIR)\wmisc.obj"
	-@erase "$(OUTDIR)\system.dll"
	-@erase "$(OUTDIR)\system.exp"
	-@erase "$(OUTDIR)\system.lib"
	-@erase "$(OUTDIR)\system.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /Fp"$(INTDIR)\system.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\system.bsc" 
BSC32_SBRS=

LINK32=link.exe
LINK32_FLAGS=winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplay.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\system.pdb" /debug /machine:I386 /out:"$(OUTDIR)\system.dll" /implib:"$(OUTDIR)\system.lib" 
LINK32_OBJS= \
	"$(INTDIR)\alertbox.obj" \
	"$(INTDIR)\animate.obj" \
	"$(INTDIR)\app.obj" \
	"$(INTDIR)\array.obj" \
	"$(INTDIR)\bargraph.obj" \
	"$(INTDIR)\bezier.obj" \
	"$(INTDIR)\bmp.obj" \
	"$(INTDIR)\button.obj" \
	"$(INTDIR)\c_combo.obj" \
	"$(INTDIR)\capstest.obj" \
	"$(INTDIR)\cbutton.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\cmenu.obj" \
	"$(INTDIR)\config.obj" \
	"$(INTDIR)\crc.obj" \
	"$(INTDIR)\critical.obj" \
	"$(INTDIR)\crossref.obj" \
	"$(INTDIR)\cscroll.obj" \
	"$(INTDIR)\ctab.obj" \
	"$(INTDIR)\ctbar.obj" \
	"$(INTDIR)\cust_dlg.obj" \
	"$(INTDIR)\date.obj" \
	"$(INTDIR)\datetime.obj" \
	"$(INTDIR)\dc_man.obj" \
	"$(INTDIR)\dialog.obj" \
	"$(INTDIR)\dib.obj" \
	"$(INTDIR)\dib_blt.obj" \
	"$(INTDIR)\dib_poly.obj" \
	"$(INTDIR)\dib_util.obj" \
	"$(INTDIR)\dibref.obj" \
	"$(INTDIR)\directplay.obj" \
	"$(INTDIR)\dlist.obj" \
	"$(INTDIR)\dragwin.obj" \
	"$(INTDIR)\except.obj" \
	"$(INTDIR)\filebase.obj" \
	"$(INTDIR)\filecnk.obj" \
	"$(INTDIR)\filedata.obj" \
	"$(INTDIR)\fileiter.obj" \
	"$(INTDIR)\filepack.obj" \
	"$(INTDIR)\fillwind.obj" \
	"$(INTDIR)\fonts.obj" \
	"$(INTDIR)\fractal.obj" \
	"$(INTDIR)\framewin.obj" \
	"$(INTDIR)\fsalloc.obj" \
	"$(INTDIR)\gamectrl.obj" \
	"$(INTDIR)\gdiengin.obj" \
	"$(INTDIR)\grtypes.obj" \
	"$(INTDIR)\gsecwind.obj" \
	"$(INTDIR)\gtoolbar.obj" \
	"$(INTDIR)\idstr.obj" \
	"$(INTDIR)\imglib.obj" \
	"$(INTDIR)\itemwind.obj" \
	"$(INTDIR)\logwin.obj" \
	"$(INTDIR)\lzdecode.obj" \
	"$(INTDIR)\lzencode.obj" \
	"$(INTDIR)\lzmisc.obj" \
	"$(INTDIR)\makename.obj" \
	"$(INTDIR)\mciwind.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\mmtimer.obj" \
	"$(INTDIR)\msgbox.obj" \
	"$(INTDIR)\msgenum.obj" \
	"$(INTDIR)\myassert.obj" \
	"$(INTDIR)\nametable.obj" \
	"$(INTDIR)\palette.obj" \
	"$(INTDIR)\palwin.obj" \
	"$(INTDIR)\palwind.obj" \
	"$(INTDIR)\pathdial.obj" \
	"$(INTDIR)\piclib.obj" \
	"$(INTDIR)\poolarry.obj" \
	"$(INTDIR)\popicon.obj" \
	"$(INTDIR)\ptrlist.obj" \
	"$(INTDIR)\random.obj" \
	"$(INTDIR)\registry.obj" \
	"$(INTDIR)\resstr.obj" \
	"$(INTDIR)\scrnbase.obj" \
	"$(INTDIR)\simpstr.obj" \
	"$(INTDIR)\sllist.obj" \
	"$(INTDIR)\sounds.obj" \
	"$(INTDIR)\soundsys.obj" \
	"$(INTDIR)\splash.obj" \
	"$(INTDIR)\sprlib.obj" \
	"$(INTDIR)\sync.obj" \
	"$(INTDIR)\tables.obj" \
	"$(INTDIR)\tabwin.obj" \
	"$(INTDIR)\thicklin.obj" \
	"$(INTDIR)\thread.obj" \
	"$(INTDIR)\threadmsg.obj" \
	"$(INTDIR)\timer.obj" \
	"$(INTDIR)\todolog.obj" \
	"$(INTDIR)\tooltip.obj" \
	"$(INTDIR)\trackwin.obj" \
	"$(INTDIR)\transwin.obj" \
	"$(INTDIR)\trig.obj" \
	"$(INTDIR)\w95excpt.obj" \
	"$(INTDIR)\wavefile.obj" \
	"$(INTDIR)\winctrl.obj" \
	"$(INTDIR)\wind.obj" \
	"$(INTDIR)\winerror.obj" \
	"$(INTDIR)\winfile.obj" \
	"$(INTDIR)\wmisc.obj"

"$(OUTDIR)\system.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "system - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

ALL : "$(OUTDIR)\systemDB.dll"


CLEAN :
	-@erase "$(INTDIR)\alertbox.obj"
	-@erase "$(INTDIR)\animate.obj"
	-@erase "$(INTDIR)\app.obj"
	-@erase "$(INTDIR)\array.obj"
	-@erase "$(INTDIR)\bargraph.obj"
	-@erase "$(INTDIR)\bezier.obj"
	-@erase "$(INTDIR)\bmp.obj"
	-@erase "$(INTDIR)\button.obj"
	-@erase "$(INTDIR)\c_combo.obj"
	-@erase "$(INTDIR)\capstest.obj"
	-@erase "$(INTDIR)\cbutton.obj"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\clog.obj"
	-@erase "$(INTDIR)\cmenu.obj"
	-@erase "$(INTDIR)\config.obj"
	-@erase "$(INTDIR)\crc.obj"
	-@erase "$(INTDIR)\critical.obj"
	-@erase "$(INTDIR)\crossref.obj"
	-@erase "$(INTDIR)\cscroll.obj"
	-@erase "$(INTDIR)\ctab.obj"
	-@erase "$(INTDIR)\ctbar.obj"
	-@erase "$(INTDIR)\cust_dlg.obj"
	-@erase "$(INTDIR)\date.obj"
	-@erase "$(INTDIR)\datetime.obj"
	-@erase "$(INTDIR)\dc_man.obj"
	-@erase "$(INTDIR)\dialog.obj"
	-@erase "$(INTDIR)\dib.obj"
	-@erase "$(INTDIR)\dib_blt.obj"
	-@erase "$(INTDIR)\dib_poly.obj"
	-@erase "$(INTDIR)\dib_util.obj"
	-@erase "$(INTDIR)\dibref.obj"
	-@erase "$(INTDIR)\directplay.obj"
	-@erase "$(INTDIR)\dlist.obj"
	-@erase "$(INTDIR)\dragwin.obj"
	-@erase "$(INTDIR)\except.obj"
	-@erase "$(INTDIR)\filebase.obj"
	-@erase "$(INTDIR)\filecnk.obj"
	-@erase "$(INTDIR)\filedata.obj"
	-@erase "$(INTDIR)\fileiter.obj"
	-@erase "$(INTDIR)\filepack.obj"
	-@erase "$(INTDIR)\fillwind.obj"
	-@erase "$(INTDIR)\fonts.obj"
	-@erase "$(INTDIR)\fractal.obj"
	-@erase "$(INTDIR)\framewin.obj"
	-@erase "$(INTDIR)\fsalloc.obj"
	-@erase "$(INTDIR)\gamectrl.obj"
	-@erase "$(INTDIR)\gdiengin.obj"
	-@erase "$(INTDIR)\grtypes.obj"
	-@erase "$(INTDIR)\gsecwind.obj"
	-@erase "$(INTDIR)\gtoolbar.obj"
	-@erase "$(INTDIR)\idstr.obj"
	-@erase "$(INTDIR)\imglib.obj"
	-@erase "$(INTDIR)\itemwind.obj"
	-@erase "$(INTDIR)\logw_imp.obj"
	-@erase "$(INTDIR)\logwin.obj"
	-@erase "$(INTDIR)\lzdecode.obj"
	-@erase "$(INTDIR)\lzencode.obj"
	-@erase "$(INTDIR)\lzmisc.obj"
	-@erase "$(INTDIR)\makename.obj"
	-@erase "$(INTDIR)\mciwind.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\mmtimer.obj"
	-@erase "$(INTDIR)\msgbox.obj"
	-@erase "$(INTDIR)\msgenum.obj"
	-@erase "$(INTDIR)\myassert.obj"
	-@erase "$(INTDIR)\nametable.obj"
	-@erase "$(INTDIR)\palette.obj"
	-@erase "$(INTDIR)\palwin.obj"
	-@erase "$(INTDIR)\palwind.obj"
	-@erase "$(INTDIR)\pathdial.obj"
	-@erase "$(INTDIR)\piclib.obj"
	-@erase "$(INTDIR)\poolarry.obj"
	-@erase "$(INTDIR)\popicon.obj"
	-@erase "$(INTDIR)\ptrlist.obj"
	-@erase "$(INTDIR)\random.obj"
	-@erase "$(INTDIR)\registry.obj"
	-@erase "$(INTDIR)\resstr.obj"
	-@erase "$(INTDIR)\scrnbase.obj"
	-@erase "$(INTDIR)\simpstr.obj"
	-@erase "$(INTDIR)\sllist.obj"
	-@erase "$(INTDIR)\sounds.obj"
	-@erase "$(INTDIR)\soundsys.obj"
	-@erase "$(INTDIR)\splash.obj"
	-@erase "$(INTDIR)\sprlib.obj"
	-@erase "$(INTDIR)\sync.obj"
	-@erase "$(INTDIR)\tables.obj"
	-@erase "$(INTDIR)\tabwin.obj"
	-@erase "$(INTDIR)\thicklin.obj"
	-@erase "$(INTDIR)\thread.obj"
	-@erase "$(INTDIR)\threadmsg.obj"
	-@erase "$(INTDIR)\timer.obj"
	-@erase "$(INTDIR)\todolog.obj"
	-@erase "$(INTDIR)\tooltip.obj"
	-@erase "$(INTDIR)\trackwin.obj"
	-@erase "$(INTDIR)\transwin.obj"
	-@erase "$(INTDIR)\trig.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\w95excpt.obj"
	-@erase "$(INTDIR)\wavefile.obj"
	-@erase "$(INTDIR)\winctrl.obj"
	-@erase "$(INTDIR)\wind.obj"
	-@erase "$(INTDIR)\winerror.obj"
	-@erase "$(INTDIR)\winfile.obj"
	-@erase "$(INTDIR)\wmisc.obj"
	-@erase "$(OUTDIR)\systemDB.dll"
	-@erase "$(OUTDIR)\systemDB.exp"
	-@erase "$(OUTDIR)\systemDB.ilk"
	-@erase "$(OUTDIR)\systemDB.lib"
	-@erase "$(OUTDIR)\systemDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /Fp"$(INTDIR)\system.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\system.bsc" 
BSC32_SBRS=
LINK32=link.exe
LINK32_FLAGS=winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplayx.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\systemDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\systemDB.dll" /implib:"$(OUTDIR)\systemDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\alertbox.obj" \
	"$(INTDIR)\animate.obj" \
	"$(INTDIR)\app.obj" \
	"$(INTDIR)\array.obj" \
	"$(INTDIR)\bargraph.obj" \
	"$(INTDIR)\bezier.obj" \
	"$(INTDIR)\bmp.obj" \
	"$(INTDIR)\button.obj" \
	"$(INTDIR)\c_combo.obj" \
	"$(INTDIR)\capstest.obj" \
	"$(INTDIR)\cbutton.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\clog.obj" \
	"$(INTDIR)\cmenu.obj" \
	"$(INTDIR)\config.obj" \
	"$(INTDIR)\crc.obj" \
	"$(INTDIR)\critical.obj" \
	"$(INTDIR)\crossref.obj" \
	"$(INTDIR)\cscroll.obj" \
	"$(INTDIR)\ctab.obj" \
	"$(INTDIR)\ctbar.obj" \
	"$(INTDIR)\cust_dlg.obj" \
	"$(INTDIR)\date.obj" \
	"$(INTDIR)\datetime.obj" \
	"$(INTDIR)\dc_man.obj" \
	"$(INTDIR)\dialog.obj" \
	"$(INTDIR)\dib.obj" \
	"$(INTDIR)\dib_blt.obj" \
	"$(INTDIR)\dib_poly.obj" \
	"$(INTDIR)\dib_util.obj" \
	"$(INTDIR)\dibref.obj" \
	"$(INTDIR)\directplay.obj" \
	"$(INTDIR)\dlist.obj" \
	"$(INTDIR)\dragwin.obj" \
	"$(INTDIR)\except.obj" \
	"$(INTDIR)\filebase.obj" \
	"$(INTDIR)\filecnk.obj" \
	"$(INTDIR)\filedata.obj" \
	"$(INTDIR)\fileiter.obj" \
	"$(INTDIR)\filepack.obj" \
	"$(INTDIR)\fillwind.obj" \
	"$(INTDIR)\fonts.obj" \
	"$(INTDIR)\fractal.obj" \
	"$(INTDIR)\framewin.obj" \
	"$(INTDIR)\fsalloc.obj" \
	"$(INTDIR)\gamectrl.obj" \
	"$(INTDIR)\gdiengin.obj" \
	"$(INTDIR)\grtypes.obj" \
	"$(INTDIR)\gsecwind.obj" \
	"$(INTDIR)\gtoolbar.obj" \
	"$(INTDIR)\idstr.obj" \
	"$(INTDIR)\imglib.obj" \
	"$(INTDIR)\itemwind.obj" \
	"$(INTDIR)\logw_imp.obj" \
	"$(INTDIR)\logwin.obj" \
	"$(INTDIR)\lzdecode.obj" \
	"$(INTDIR)\lzencode.obj" \
	"$(INTDIR)\lzmisc.obj" \
	"$(INTDIR)\makename.obj" \
	"$(INTDIR)\mciwind.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\mmtimer.obj" \
	"$(INTDIR)\msgbox.obj" \
	"$(INTDIR)\msgenum.obj" \
	"$(INTDIR)\myassert.obj" \
	"$(INTDIR)\nametable.obj" \
	"$(INTDIR)\palette.obj" \
	"$(INTDIR)\palwin.obj" \
	"$(INTDIR)\palwind.obj" \
	"$(INTDIR)\pathdial.obj" \
	"$(INTDIR)\piclib.obj" \
	"$(INTDIR)\poolarry.obj" \
	"$(INTDIR)\popicon.obj" \
	"$(INTDIR)\ptrlist.obj" \
	"$(INTDIR)\random.obj" \
	"$(INTDIR)\registry.obj" \
	"$(INTDIR)\resstr.obj" \
	"$(INTDIR)\scrnbase.obj" \
	"$(INTDIR)\simpstr.obj" \
	"$(INTDIR)\sllist.obj" \
	"$(INTDIR)\sounds.obj" \
	"$(INTDIR)\soundsys.obj" \
	"$(INTDIR)\splash.obj" \
	"$(INTDIR)\sprlib.obj" \
	"$(INTDIR)\sync.obj" \
	"$(INTDIR)\tables.obj" \
	"$(INTDIR)\tabwin.obj" \
	"$(INTDIR)\thicklin.obj" \
	"$(INTDIR)\thread.obj" \
	"$(INTDIR)\threadmsg.obj" \
	"$(INTDIR)\timer.obj" \
	"$(INTDIR)\todolog.obj" \
	"$(INTDIR)\tooltip.obj" \
	"$(INTDIR)\trackwin.obj" \
	"$(INTDIR)\transwin.obj" \
	"$(INTDIR)\trig.obj" \
	"$(INTDIR)\w95excpt.obj" \
	"$(INTDIR)\wavefile.obj" \
	"$(INTDIR)\winctrl.obj" \
	"$(INTDIR)\wind.obj" \
	"$(INTDIR)\winerror.obj" \
	"$(INTDIR)\winfile.obj" \
	"$(INTDIR)\wmisc.obj"

"$(OUTDIR)\systemDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "system - Win32 Editor Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\Debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

ALL : "$(OUTDIR)\systemDB.dll"


CLEAN :
	-@erase "$(INTDIR)\alertbox.obj"
	-@erase "$(INTDIR)\animate.obj"
	-@erase "$(INTDIR)\app.obj"
	-@erase "$(INTDIR)\array.obj"
	-@erase "$(INTDIR)\bargraph.obj"
	-@erase "$(INTDIR)\bezier.obj"
	-@erase "$(INTDIR)\bmp.obj"
	-@erase "$(INTDIR)\button.obj"
	-@erase "$(INTDIR)\c_combo.obj"
	-@erase "$(INTDIR)\capstest.obj"
	-@erase "$(INTDIR)\cbutton.obj"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\clog.obj"
	-@erase "$(INTDIR)\cmenu.obj"
	-@erase "$(INTDIR)\config.obj"
	-@erase "$(INTDIR)\crc.obj"
	-@erase "$(INTDIR)\critical.obj"
	-@erase "$(INTDIR)\crossref.obj"
	-@erase "$(INTDIR)\cscroll.obj"
	-@erase "$(INTDIR)\ctab.obj"
	-@erase "$(INTDIR)\ctbar.obj"
	-@erase "$(INTDIR)\cust_dlg.obj"
	-@erase "$(INTDIR)\date.obj"
	-@erase "$(INTDIR)\datetime.obj"
	-@erase "$(INTDIR)\dc_man.obj"
	-@erase "$(INTDIR)\dialog.obj"
	-@erase "$(INTDIR)\dib.obj"
	-@erase "$(INTDIR)\dib_blt.obj"
	-@erase "$(INTDIR)\dib_poly.obj"
	-@erase "$(INTDIR)\dib_util.obj"
	-@erase "$(INTDIR)\dibref.obj"
	-@erase "$(INTDIR)\directplay.obj"
	-@erase "$(INTDIR)\dlist.obj"
	-@erase "$(INTDIR)\dragwin.obj"
	-@erase "$(INTDIR)\except.obj"
	-@erase "$(INTDIR)\filebase.obj"
	-@erase "$(INTDIR)\filecnk.obj"
	-@erase "$(INTDIR)\filedata.obj"
	-@erase "$(INTDIR)\fileiter.obj"
	-@erase "$(INTDIR)\filepack.obj"
	-@erase "$(INTDIR)\fillwind.obj"
	-@erase "$(INTDIR)\fonts.obj"
	-@erase "$(INTDIR)\fractal.obj"
	-@erase "$(INTDIR)\framewin.obj"
	-@erase "$(INTDIR)\fsalloc.obj"
	-@erase "$(INTDIR)\gamectrl.obj"
	-@erase "$(INTDIR)\gdiengin.obj"
	-@erase "$(INTDIR)\grtypes.obj"
	-@erase "$(INTDIR)\gsecwind.obj"
	-@erase "$(INTDIR)\gtoolbar.obj"
	-@erase "$(INTDIR)\idstr.obj"
	-@erase "$(INTDIR)\imglib.obj"
	-@erase "$(INTDIR)\itemwind.obj"
	-@erase "$(INTDIR)\logw_imp.obj"
	-@erase "$(INTDIR)\logwin.obj"
	-@erase "$(INTDIR)\lzdecode.obj"
	-@erase "$(INTDIR)\lzencode.obj"
	-@erase "$(INTDIR)\lzmisc.obj"
	-@erase "$(INTDIR)\makename.obj"
	-@erase "$(INTDIR)\mciwind.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\mmtimer.obj"
	-@erase "$(INTDIR)\msgbox.obj"
	-@erase "$(INTDIR)\msgenum.obj"
	-@erase "$(INTDIR)\myassert.obj"
	-@erase "$(INTDIR)\nametable.obj"
	-@erase "$(INTDIR)\palette.obj"
	-@erase "$(INTDIR)\palwin.obj"
	-@erase "$(INTDIR)\palwind.obj"
	-@erase "$(INTDIR)\pathdial.obj"
	-@erase "$(INTDIR)\piclib.obj"
	-@erase "$(INTDIR)\poolarry.obj"
	-@erase "$(INTDIR)\popicon.obj"
	-@erase "$(INTDIR)\ptrlist.obj"
	-@erase "$(INTDIR)\random.obj"
	-@erase "$(INTDIR)\registry.obj"
	-@erase "$(INTDIR)\resstr.obj"
	-@erase "$(INTDIR)\scrnbase.obj"
	-@erase "$(INTDIR)\simpstr.obj"
	-@erase "$(INTDIR)\sllist.obj"
	-@erase "$(INTDIR)\sounds.obj"
	-@erase "$(INTDIR)\soundsys.obj"
	-@erase "$(INTDIR)\splash.obj"
	-@erase "$(INTDIR)\sprlib.obj"
	-@erase "$(INTDIR)\sync.obj"
	-@erase "$(INTDIR)\tables.obj"
	-@erase "$(INTDIR)\tabwin.obj"
	-@erase "$(INTDIR)\thicklin.obj"
	-@erase "$(INTDIR)\thread.obj"
	-@erase "$(INTDIR)\threadmsg.obj"
	-@erase "$(INTDIR)\timer.obj"
	-@erase "$(INTDIR)\todolog.obj"
	-@erase "$(INTDIR)\tooltip.obj"
	-@erase "$(INTDIR)\trackwin.obj"
	-@erase "$(INTDIR)\transwin.obj"
	-@erase "$(INTDIR)\trig.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\w95excpt.obj"
	-@erase "$(INTDIR)\wavefile.obj"
	-@erase "$(INTDIR)\winctrl.obj"
	-@erase "$(INTDIR)\wind.obj"
	-@erase "$(INTDIR)\winerror.obj"
	-@erase "$(INTDIR)\winfile.obj"
	-@erase "$(INTDIR)\wmisc.obj"
	-@erase "$(OUTDIR)\systemDB.dll"
	-@erase "$(OUTDIR)\systemDB.exp"
	-@erase "$(OUTDIR)\systemDB.ilk"
	-@erase "$(OUTDIR)\systemDB.lib"
	-@erase "$(OUTDIR)\systemDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\system.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\system.bsc" 
BSC32_SBRS=
LINK32=link.exe
LINK32_FLAGS=winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplayx.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /incremental:yes /pdb:"$(OUTDIR)\systemDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\systemDB.dll" /implib:"$(OUTDIR)\systemDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\alertbox.obj" \
	"$(INTDIR)\animate.obj" \
	"$(INTDIR)\app.obj" \
	"$(INTDIR)\array.obj" \
	"$(INTDIR)\bargraph.obj" \
	"$(INTDIR)\bezier.obj" \
	"$(INTDIR)\bmp.obj" \
	"$(INTDIR)\button.obj" \
	"$(INTDIR)\c_combo.obj" \
	"$(INTDIR)\capstest.obj" \
	"$(INTDIR)\cbutton.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\clog.obj" \
	"$(INTDIR)\cmenu.obj" \
	"$(INTDIR)\config.obj" \
	"$(INTDIR)\crc.obj" \
	"$(INTDIR)\critical.obj" \
	"$(INTDIR)\crossref.obj" \
	"$(INTDIR)\cscroll.obj" \
	"$(INTDIR)\ctab.obj" \
	"$(INTDIR)\ctbar.obj" \
	"$(INTDIR)\cust_dlg.obj" \
	"$(INTDIR)\date.obj" \
	"$(INTDIR)\datetime.obj" \
	"$(INTDIR)\dc_man.obj" \
	"$(INTDIR)\dialog.obj" \
	"$(INTDIR)\dib.obj" \
	"$(INTDIR)\dib_blt.obj" \
	"$(INTDIR)\dib_poly.obj" \
	"$(INTDIR)\dib_util.obj" \
	"$(INTDIR)\dibref.obj" \
	"$(INTDIR)\directplay.obj" \
	"$(INTDIR)\dlist.obj" \
	"$(INTDIR)\dragwin.obj" \
	"$(INTDIR)\except.obj" \
	"$(INTDIR)\filebase.obj" \
	"$(INTDIR)\filecnk.obj" \
	"$(INTDIR)\filedata.obj" \
	"$(INTDIR)\fileiter.obj" \
	"$(INTDIR)\filepack.obj" \
	"$(INTDIR)\fillwind.obj" \
	"$(INTDIR)\fonts.obj" \
	"$(INTDIR)\fractal.obj" \
	"$(INTDIR)\framewin.obj" \
	"$(INTDIR)\fsalloc.obj" \
	"$(INTDIR)\gamectrl.obj" \
	"$(INTDIR)\gdiengin.obj" \
	"$(INTDIR)\grtypes.obj" \
	"$(INTDIR)\gsecwind.obj" \
	"$(INTDIR)\gtoolbar.obj" \
	"$(INTDIR)\idstr.obj" \
	"$(INTDIR)\imglib.obj" \
	"$(INTDIR)\itemwind.obj" \
	"$(INTDIR)\logw_imp.obj" \
	"$(INTDIR)\logwin.obj" \
	"$(INTDIR)\lzdecode.obj" \
	"$(INTDIR)\lzencode.obj" \
	"$(INTDIR)\lzmisc.obj" \
	"$(INTDIR)\makename.obj" \
	"$(INTDIR)\mciwind.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\mmtimer.obj" \
	"$(INTDIR)\msgbox.obj" \
	"$(INTDIR)\msgenum.obj" \
	"$(INTDIR)\myassert.obj" \
	"$(INTDIR)\nametable.obj" \
	"$(INTDIR)\palette.obj" \
	"$(INTDIR)\palwin.obj" \
	"$(INTDIR)\palwind.obj" \
	"$(INTDIR)\pathdial.obj" \
	"$(INTDIR)\piclib.obj" \
	"$(INTDIR)\poolarry.obj" \
	"$(INTDIR)\popicon.obj" \
	"$(INTDIR)\ptrlist.obj" \
	"$(INTDIR)\random.obj" \
	"$(INTDIR)\registry.obj" \
	"$(INTDIR)\resstr.obj" \
	"$(INTDIR)\scrnbase.obj" \
	"$(INTDIR)\simpstr.obj" \
	"$(INTDIR)\sllist.obj" \
	"$(INTDIR)\sounds.obj" \
	"$(INTDIR)\soundsys.obj" \
	"$(INTDIR)\splash.obj" \
	"$(INTDIR)\sprlib.obj" \
	"$(INTDIR)\sync.obj" \
	"$(INTDIR)\tables.obj" \
	"$(INTDIR)\tabwin.obj" \
	"$(INTDIR)\thicklin.obj" \
	"$(INTDIR)\thread.obj" \
	"$(INTDIR)\threadmsg.obj" \
	"$(INTDIR)\timer.obj" \
	"$(INTDIR)\todolog.obj" \
	"$(INTDIR)\tooltip.obj" \
	"$(INTDIR)\trackwin.obj" \
	"$(INTDIR)\transwin.obj" \
	"$(INTDIR)\trig.obj" \
	"$(INTDIR)\w95excpt.obj" \
	"$(INTDIR)\wavefile.obj" \
	"$(INTDIR)\winctrl.obj" \
	"$(INTDIR)\wind.obj" \
	"$(INTDIR)\winerror.obj" \
	"$(INTDIR)\winfile.obj" \
	"$(INTDIR)\wmisc.obj"

"$(OUTDIR)\systemDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "system - Win32 Editor Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\Release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

ALL : "$(OUTDIR)\system.dll"


CLEAN :
	-@erase "$(INTDIR)\alertbox.obj"
	-@erase "$(INTDIR)\animate.obj"
	-@erase "$(INTDIR)\app.obj"
	-@erase "$(INTDIR)\array.obj"
	-@erase "$(INTDIR)\bargraph.obj"
	-@erase "$(INTDIR)\bezier.obj"
	-@erase "$(INTDIR)\bmp.obj"
	-@erase "$(INTDIR)\button.obj"
	-@erase "$(INTDIR)\c_combo.obj"
	-@erase "$(INTDIR)\capstest.obj"
	-@erase "$(INTDIR)\cbutton.obj"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\cmenu.obj"
	-@erase "$(INTDIR)\config.obj"
	-@erase "$(INTDIR)\crc.obj"
	-@erase "$(INTDIR)\critical.obj"
	-@erase "$(INTDIR)\crossref.obj"
	-@erase "$(INTDIR)\cscroll.obj"
	-@erase "$(INTDIR)\ctab.obj"
	-@erase "$(INTDIR)\ctbar.obj"
	-@erase "$(INTDIR)\cust_dlg.obj"
	-@erase "$(INTDIR)\date.obj"
	-@erase "$(INTDIR)\datetime.obj"
	-@erase "$(INTDIR)\dc_man.obj"
	-@erase "$(INTDIR)\dialog.obj"
	-@erase "$(INTDIR)\dib.obj"
	-@erase "$(INTDIR)\dib_blt.obj"
	-@erase "$(INTDIR)\dib_poly.obj"
	-@erase "$(INTDIR)\dib_util.obj"
	-@erase "$(INTDIR)\dibref.obj"
	-@erase "$(INTDIR)\directplay.obj"
	-@erase "$(INTDIR)\dlist.obj"
	-@erase "$(INTDIR)\dragwin.obj"
	-@erase "$(INTDIR)\except.obj"
	-@erase "$(INTDIR)\filebase.obj"
	-@erase "$(INTDIR)\filecnk.obj"
	-@erase "$(INTDIR)\filedata.obj"
	-@erase "$(INTDIR)\fileiter.obj"
	-@erase "$(INTDIR)\filepack.obj"
	-@erase "$(INTDIR)\fillwind.obj"
	-@erase "$(INTDIR)\fonts.obj"
	-@erase "$(INTDIR)\fractal.obj"
	-@erase "$(INTDIR)\framewin.obj"
	-@erase "$(INTDIR)\fsalloc.obj"
	-@erase "$(INTDIR)\gamectrl.obj"
	-@erase "$(INTDIR)\gdiengin.obj"
	-@erase "$(INTDIR)\grtypes.obj"
	-@erase "$(INTDIR)\gsecwind.obj"
	-@erase "$(INTDIR)\gtoolbar.obj"
	-@erase "$(INTDIR)\idstr.obj"
	-@erase "$(INTDIR)\imglib.obj"
	-@erase "$(INTDIR)\itemwind.obj"
	-@erase "$(INTDIR)\logwin.obj"
	-@erase "$(INTDIR)\lzdecode.obj"
	-@erase "$(INTDIR)\lzencode.obj"
	-@erase "$(INTDIR)\lzmisc.obj"
	-@erase "$(INTDIR)\makename.obj"
	-@erase "$(INTDIR)\mciwind.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\mmtimer.obj"
	-@erase "$(INTDIR)\msgbox.obj"
	-@erase "$(INTDIR)\msgenum.obj"
	-@erase "$(INTDIR)\myassert.obj"
	-@erase "$(INTDIR)\nametable.obj"
	-@erase "$(INTDIR)\palette.obj"
	-@erase "$(INTDIR)\palwin.obj"
	-@erase "$(INTDIR)\palwind.obj"
	-@erase "$(INTDIR)\pathdial.obj"
	-@erase "$(INTDIR)\piclib.obj"
	-@erase "$(INTDIR)\poolarry.obj"
	-@erase "$(INTDIR)\popicon.obj"
	-@erase "$(INTDIR)\ptrlist.obj"
	-@erase "$(INTDIR)\random.obj"
	-@erase "$(INTDIR)\registry.obj"
	-@erase "$(INTDIR)\resstr.obj"
	-@erase "$(INTDIR)\scrnbase.obj"
	-@erase "$(INTDIR)\simpstr.obj"
	-@erase "$(INTDIR)\sllist.obj"
	-@erase "$(INTDIR)\sounds.obj"
	-@erase "$(INTDIR)\soundsys.obj"
	-@erase "$(INTDIR)\splash.obj"
	-@erase "$(INTDIR)\sprlib.obj"
	-@erase "$(INTDIR)\sync.obj"
	-@erase "$(INTDIR)\tables.obj"
	-@erase "$(INTDIR)\tabwin.obj"
	-@erase "$(INTDIR)\thicklin.obj"
	-@erase "$(INTDIR)\thread.obj"
	-@erase "$(INTDIR)\threadmsg.obj"
	-@erase "$(INTDIR)\timer.obj"
	-@erase "$(INTDIR)\todolog.obj"
	-@erase "$(INTDIR)\tooltip.obj"
	-@erase "$(INTDIR)\trackwin.obj"
	-@erase "$(INTDIR)\transwin.obj"
	-@erase "$(INTDIR)\trig.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\w95excpt.obj"
	-@erase "$(INTDIR)\wavefile.obj"
	-@erase "$(INTDIR)\winctrl.obj"
	-@erase "$(INTDIR)\wind.obj"
	-@erase "$(INTDIR)\winerror.obj"
	-@erase "$(INTDIR)\winfile.obj"
	-@erase "$(INTDIR)\wmisc.obj"
	-@erase "$(OUTDIR)\system.dll"
	-@erase "$(OUTDIR)\system.exp"
	-@erase "$(OUTDIR)\system.lib"
	-@erase "$(OUTDIR)\system.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_SYSTEM_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /Fp"$(INTDIR)\system.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\system.bsc" 
BSC32_SBRS=
LINK32=link.exe
LINK32_FLAGS=winmm.lib gdi32.lib user32.lib vfw32.lib ole32.lib dplay.lib comctl32.lib advapi32.lib dsound.lib shell32.lib /nologo /dll /incremental:no /pdb:"$(OUTDIR)\system.pdb" /debug /machine:I386 /out:"$(OUTDIR)\system.dll" /implib:"$(OUTDIR)\system.lib" 
LINK32_OBJS= \
	"$(INTDIR)\alertbox.obj" \
	"$(INTDIR)\animate.obj" \
	"$(INTDIR)\app.obj" \
	"$(INTDIR)\array.obj" \
	"$(INTDIR)\bargraph.obj" \
	"$(INTDIR)\bezier.obj" \
	"$(INTDIR)\bmp.obj" \
	"$(INTDIR)\button.obj" \
	"$(INTDIR)\c_combo.obj" \
	"$(INTDIR)\capstest.obj" \
	"$(INTDIR)\cbutton.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\cmenu.obj" \
	"$(INTDIR)\config.obj" \
	"$(INTDIR)\crc.obj" \
	"$(INTDIR)\critical.obj" \
	"$(INTDIR)\crossref.obj" \
	"$(INTDIR)\cscroll.obj" \
	"$(INTDIR)\ctab.obj" \
	"$(INTDIR)\ctbar.obj" \
	"$(INTDIR)\cust_dlg.obj" \
	"$(INTDIR)\date.obj" \
	"$(INTDIR)\datetime.obj" \
	"$(INTDIR)\dc_man.obj" \
	"$(INTDIR)\dialog.obj" \
	"$(INTDIR)\dib.obj" \
	"$(INTDIR)\dib_blt.obj" \
	"$(INTDIR)\dib_poly.obj" \
	"$(INTDIR)\dib_util.obj" \
	"$(INTDIR)\dibref.obj" \
	"$(INTDIR)\directplay.obj" \
	"$(INTDIR)\dlist.obj" \
	"$(INTDIR)\dragwin.obj" \
	"$(INTDIR)\except.obj" \
	"$(INTDIR)\filebase.obj" \
	"$(INTDIR)\filecnk.obj" \
	"$(INTDIR)\filedata.obj" \
	"$(INTDIR)\fileiter.obj" \
	"$(INTDIR)\filepack.obj" \
	"$(INTDIR)\fillwind.obj" \
	"$(INTDIR)\fonts.obj" \
	"$(INTDIR)\fractal.obj" \
	"$(INTDIR)\framewin.obj" \
	"$(INTDIR)\fsalloc.obj" \
	"$(INTDIR)\gamectrl.obj" \
	"$(INTDIR)\gdiengin.obj" \
	"$(INTDIR)\grtypes.obj" \
	"$(INTDIR)\gsecwind.obj" \
	"$(INTDIR)\gtoolbar.obj" \
	"$(INTDIR)\idstr.obj" \
	"$(INTDIR)\imglib.obj" \
	"$(INTDIR)\itemwind.obj" \
	"$(INTDIR)\logwin.obj" \
	"$(INTDIR)\lzdecode.obj" \
	"$(INTDIR)\lzencode.obj" \
	"$(INTDIR)\lzmisc.obj" \
	"$(INTDIR)\makename.obj" \
	"$(INTDIR)\mciwind.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\mmtimer.obj" \
	"$(INTDIR)\msgbox.obj" \
	"$(INTDIR)\msgenum.obj" \
	"$(INTDIR)\myassert.obj" \
	"$(INTDIR)\nametable.obj" \
	"$(INTDIR)\palette.obj" \
	"$(INTDIR)\palwin.obj" \
	"$(INTDIR)\palwind.obj" \
	"$(INTDIR)\pathdial.obj" \
	"$(INTDIR)\piclib.obj" \
	"$(INTDIR)\poolarry.obj" \
	"$(INTDIR)\popicon.obj" \
	"$(INTDIR)\ptrlist.obj" \
	"$(INTDIR)\random.obj" \
	"$(INTDIR)\registry.obj" \
	"$(INTDIR)\resstr.obj" \
	"$(INTDIR)\scrnbase.obj" \
	"$(INTDIR)\simpstr.obj" \
	"$(INTDIR)\sllist.obj" \
	"$(INTDIR)\sounds.obj" \
	"$(INTDIR)\soundsys.obj" \
	"$(INTDIR)\splash.obj" \
	"$(INTDIR)\sprlib.obj" \
	"$(INTDIR)\sync.obj" \
	"$(INTDIR)\tables.obj" \
	"$(INTDIR)\tabwin.obj" \
	"$(INTDIR)\thicklin.obj" \
	"$(INTDIR)\thread.obj" \
	"$(INTDIR)\threadmsg.obj" \
	"$(INTDIR)\timer.obj" \
	"$(INTDIR)\todolog.obj" \
	"$(INTDIR)\tooltip.obj" \
	"$(INTDIR)\trackwin.obj" \
	"$(INTDIR)\transwin.obj" \
	"$(INTDIR)\trig.obj" \
	"$(INTDIR)\w95excpt.obj" \
	"$(INTDIR)\wavefile.obj" \
	"$(INTDIR)\winctrl.obj" \
	"$(INTDIR)\wind.obj" \
	"$(INTDIR)\winerror.obj" \
	"$(INTDIR)\winfile.obj" \
	"$(INTDIR)\wmisc.obj"

"$(OUTDIR)\system.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("system.dep")
!INCLUDE "system.dep"
!ELSE 
!MESSAGE Warning: cannot find "system.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "system - Win32 Release" || "$(CFG)" == "system - Win32 Debug" || "$(CFG)" == "system - Win32 Editor Debug" || "$(CFG)" == "system - Win32 Editor Release"
SOURCE=.\alertbox.cpp

"$(INTDIR)\alertbox.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\animate.cpp

"$(INTDIR)\animate.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\app.cpp

"$(INTDIR)\app.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\array.cpp

"$(INTDIR)\array.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bargraph.cpp

"$(INTDIR)\bargraph.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bezier.cpp

"$(INTDIR)\bezier.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bmp.cpp

"$(INTDIR)\bmp.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\button.cpp

"$(INTDIR)\button.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\c_combo.cpp

"$(INTDIR)\c_combo.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\capstest.cpp

"$(INTDIR)\capstest.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cbutton.cpp

"$(INTDIR)\cbutton.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cedit.cpp

"$(INTDIR)\cedit.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\clog.cpp

!IF  "$(CFG)" == "system - Win32 Release"

!ELSEIF  "$(CFG)" == "system - Win32 Debug"


"$(INTDIR)\clog.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "system - Win32 Editor Debug"


"$(INTDIR)\clog.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "system - Win32 Editor Release"

!ENDIF 

SOURCE=.\cmenu.cpp

"$(INTDIR)\cmenu.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\config.cpp

"$(INTDIR)\config.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\crc.cpp

"$(INTDIR)\crc.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\critical.cpp

"$(INTDIR)\critical.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\crossref.cpp

"$(INTDIR)\crossref.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cscroll.cpp

"$(INTDIR)\cscroll.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ctab.cpp

"$(INTDIR)\ctab.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ctbar.cpp

"$(INTDIR)\ctbar.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\cust_dlg.cpp

"$(INTDIR)\cust_dlg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\date.cpp

"$(INTDIR)\date.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\datetime.cpp

"$(INTDIR)\datetime.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dc_man.cpp

"$(INTDIR)\dc_man.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dialog.cpp

"$(INTDIR)\dialog.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dib.cpp

"$(INTDIR)\dib.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dib_blt.cpp

"$(INTDIR)\dib_blt.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dib_poly.cpp

"$(INTDIR)\dib_poly.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dib_util.cpp

"$(INTDIR)\dib_util.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dibref.cpp

"$(INTDIR)\dibref.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\directplay.cpp

"$(INTDIR)\directplay.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dlist.cpp

"$(INTDIR)\dlist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\dragwin.cpp

"$(INTDIR)\dragwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\except.cpp

"$(INTDIR)\except.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\filebase.cpp

"$(INTDIR)\filebase.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\filecnk.cpp

"$(INTDIR)\filecnk.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\filedata.cpp

"$(INTDIR)\filedata.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fileiter.cpp

"$(INTDIR)\fileiter.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\filepack.cpp

"$(INTDIR)\filepack.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fillwind.cpp

"$(INTDIR)\fillwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fonts.cpp

"$(INTDIR)\fonts.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fractal.cpp

"$(INTDIR)\fractal.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\framewin.cpp

"$(INTDIR)\framewin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\fsalloc.cpp

"$(INTDIR)\fsalloc.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gamectrl.cpp

"$(INTDIR)\gamectrl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gdiengin.cpp

"$(INTDIR)\gdiengin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\grtypes.cpp

"$(INTDIR)\grtypes.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gsecwind.cpp

"$(INTDIR)\gsecwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gtoolbar.cpp

"$(INTDIR)\gtoolbar.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\idstr.cpp

"$(INTDIR)\idstr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\imglib.cpp

"$(INTDIR)\imglib.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\itemwind.cpp

"$(INTDIR)\itemwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\logw_imp.cpp

!IF  "$(CFG)" == "system - Win32 Release"

!ELSEIF  "$(CFG)" == "system - Win32 Debug"


"$(INTDIR)\logw_imp.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "system - Win32 Editor Debug"


"$(INTDIR)\logw_imp.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "system - Win32 Editor Release"

!ENDIF 

SOURCE=.\logwin.cpp

"$(INTDIR)\logwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lzdecode.cpp

"$(INTDIR)\lzdecode.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lzencode.cpp

"$(INTDIR)\lzencode.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lzmisc.cpp

"$(INTDIR)\lzmisc.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\makename.cpp

"$(INTDIR)\makename.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mciwind.cpp

"$(INTDIR)\mciwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\misc.cpp

"$(INTDIR)\misc.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mmtimer.cpp

"$(INTDIR)\mmtimer.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\msgbox.cpp

"$(INTDIR)\msgbox.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\msgenum.cpp

"$(INTDIR)\msgenum.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\myassert.cpp

"$(INTDIR)\myassert.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\nametable.cpp

"$(INTDIR)\nametable.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\palette.cpp

"$(INTDIR)\palette.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\palwin.cpp

"$(INTDIR)\palwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\palwind.cpp

"$(INTDIR)\palwind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\pathdial.cpp

"$(INTDIR)\pathdial.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\piclib.cpp

"$(INTDIR)\piclib.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\poolarry.cpp

"$(INTDIR)\poolarry.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\popicon.cpp

"$(INTDIR)\popicon.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ptrlist.cpp

"$(INTDIR)\ptrlist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\random.cpp

"$(INTDIR)\random.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\registry.cpp

"$(INTDIR)\registry.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\resstr.cpp

"$(INTDIR)\resstr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\scrnbase.cpp

"$(INTDIR)\scrnbase.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\simpstr.cpp

"$(INTDIR)\simpstr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sllist.cpp

"$(INTDIR)\sllist.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sounds.cpp

"$(INTDIR)\sounds.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\soundsys.cpp

"$(INTDIR)\soundsys.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\splash.cpp

"$(INTDIR)\splash.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sprlib.cpp

"$(INTDIR)\sprlib.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sync.cpp

"$(INTDIR)\sync.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\tables.cpp

"$(INTDIR)\tables.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\tabwin.cpp

"$(INTDIR)\tabwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\thicklin.cpp

"$(INTDIR)\thicklin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\thread.cpp

"$(INTDIR)\thread.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\threadmsg.cpp

"$(INTDIR)\threadmsg.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\timer.cpp

"$(INTDIR)\timer.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\todolog.cpp

"$(INTDIR)\todolog.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\tooltip.cpp

"$(INTDIR)\tooltip.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\trackwin.cpp

"$(INTDIR)\trackwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\transwin.cpp

"$(INTDIR)\transwin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\trig.cpp

"$(INTDIR)\trig.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\w95excpt.cpp

"$(INTDIR)\w95excpt.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wavefile.cpp

"$(INTDIR)\wavefile.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\winctrl.cpp

"$(INTDIR)\winctrl.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wind.cpp

"$(INTDIR)\wind.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\winerror.cpp

"$(INTDIR)\winerror.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\winfile.cpp

"$(INTDIR)\winfile.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wmisc.cpp

"$(INTDIR)\wmisc.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 

