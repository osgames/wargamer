/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SPACING_HPP
#define SPACING_HPP

#ifndef __cplusplus
#error spacing.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Class for evenly spacing things within a given height
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

class EvenSpacing
{
	public:
		EvenSpacing(int sections, int yStart, int height, int fontHeight) :
			d_sections(sections),
			d_y(yStart),
			d_height(height),
			d_fontHeight(fontHeight)
		{
			ASSERT(d_sections >= 1);
			ASSERT(d_height >= sections);
			ASSERT(d_fontHeight <= (height / sections));
		}

		~EvenSpacing()
		{
			ASSERT(d_sections == 0);
		}

		int nextY()
		{
			ASSERT(d_sections > 0);

			int h1 = d_height / d_sections;
			int yVal = d_y + (h1 - d_fontHeight) / 2;

			d_y += h1;
			d_height -= h1;
			--d_sections;

			return yVal;
		}


	private:
		int d_sections;
		int d_height;
		int d_y;
		int d_fontHeight;
};


#endif /* SPACING_HPP */

