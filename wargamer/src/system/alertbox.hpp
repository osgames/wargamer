/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
#ifndef _ALERTBOX
#define _ALERTBOX

#include "wind.hpp"
#include "dialog.hpp"
#include "sysdll.h"


// loword returns item selection, hiword returns flag state
SYSTEM_DLL long alertBox(HWND h, LPCTSTR lpText, LPCTSTR lpCaption,	UINT uType);

#endif

/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */
