/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cbutton.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "palette.hpp"
#include "winctrl.hpp"
#include "palwind.hpp"
#include "imglib.hpp"
#include "wmisc.hpp"
#include <algorithm>         // STL min/max/swap functions

const char CUSTOMBUTTONCLASS[] = "CUSTOMBUTTONCLASS";

static const int BorderWidth = 2;


enum MessageID {
	CBM_FIRST = WM_USER,

	CBM_SETFILLDIB = CBM_FIRST,
	CBM_SETBORDERCOLOURS,
	CBM_SETCHECK,
	CBM_GETCHECK,
	CBM_SETCHECKBOXIMAGES,
	CBM_SETFONT,
	CBM_SETIMAGES,

	CBM_LAST
};

// void onSetFillDib(HWND hwnd, const DIB* fillDib)
#define HANDLE_CBM_SETFILLDIB(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, reinterpret_cast<DIB*>(lParam)), 0L)

// void onSetBorderColours(HWND hwnd, const CustomBorderInfo* info)
#define HANDLE_CBM_SETBORDERCOLOURS(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, reinterpret_cast<const CustomBorderInfo*>(lParam)), 0L)

// void onSetCheck(HWND hwnd, bool f)
#define HANDLE_CBM_SETCHECK(hwnd, wParam, lParam, fn) \
	 ((fn)((hwnd), (bool)(wParam != 0)), 0L)

/* bool onGetCheck(HWND hwnd) */
#define HANDLE_CBM_GETCHECK(hwnd, wParam, lParam, fn) \
	MAKELRESULT((bool)(fn)(hwnd), 0L)
//	 (LRESULT)(DWORD)(bool)((fn)(hwnd), 0L)

// void onSetCheckImages(HWND hwnd, const CheckImages* il)
#define HANDLE_CBM_SETCHECKBOXIMAGES(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, reinterpret_cast<const CheckImages*>(lParam)), 0L)

// void onSetImages(HWND hwnd, UWORD imgID, const ImageLibrary* il)
#define HANDLE_CBM_SETIMAGES(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, static_cast<UWORD>(wParam), reinterpret_cast<const ImageLibrary*>(lParam)), 0L)

// void onSetFont(HWND hwnd, HFONT hFont)
#define HANDLE_CBM_SETFONT(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, reinterpret_cast<HFONT>(lParam)), 0L)

const UWORD c_noImgID = UWORD_MAX;


/*
 * Check button Info
 */

struct CheckImages
{
    const ImageLibrary* d_imgLib;
    UWORD d_uncheckImage;
    UWORD d_checkedImage;

    CheckImages() :
        d_imgLib(0),
        d_uncheckImage(0),
        d_checkedImage(1)
    {
    }

		CheckImages(const ImageLibrary* imgLib,  UWORD uncheckImage,  UWORD checkedImage) :
        d_imgLib(imgLib),
        d_uncheckImage(uncheckImage),
        d_checkedImage(checkedImage)
    {
    }

		CheckImages(const ImageLibrary* imgLib) :
        d_imgLib(imgLib),
        d_uncheckImage(0),
        d_checkedImage(1)
    {
    }


};

struct CustomButtonData {
	HWND d_hParent;
	HWND d_hwnd;
	LONG d_style;
	LONG d_state;
	HFONT d_hFont;

	DrawDIBDC* d_buttonDib;
	const DIB* d_fillDib;
	const ImageLibrary* d_buttonImages;
	UWORD d_imgID;

	CheckImages d_checkImage;

   enum Flags {
      Lightened    = 0x01,
      Checked    = 0x02,
      ButtonDown = 0x04
   };

   UBYTE d_flags;

//	bool d_lightened;
//	bool d_checked;
   void setFlag(UBYTE mask, bool f)
   {
      if(f)
         d_flags |= mask;
      else
         d_flags &= ~mask;
   }

   void lightened(bool f)    { setFlag(Lightened, f); }
   void checked(bool f)      { setFlag(Checked, f); }
   void buttonDown(bool f)   { setFlag(ButtonDown, f); }
   bool lightened()    const { return (d_flags & Lightened) != 0;    }
   bool checked()      const { return (d_flags & Checked) != 0;    }
   bool buttonDown()   const { return (d_flags & ButtonDown) != 0; }

	/*
	 * all buttons share the same DibDC to prevent excessive instances of DC's
	 */

//	static DCObject* s_dc;
//	static UWORD s_instanceCount;

	CustomBorderInfo d_borderColours;

	CustomButtonData(HWND hParent, HWND hwnd, LONG style) :
	   d_hParent(hParent),
	   d_hwnd(hwnd),
	   d_style(style),
	   d_state(0),
	   d_hFont(0),
	   d_buttonDib(0),
	   d_fillDib(0),
	   d_checkImage(),
//	   d_lightened(False),
//	   d_checked(False),
      d_flags(0),
	   d_buttonImages(0),
	   d_imgID(c_noImgID)
	{

//	   s_instanceCount++;

	   for(int i = 0; i < CustomBorderInfo::HowMany; i++)
	   {
		   // default border colours
		   d_borderColours.colours[i] = (i < CustomBorderInfo::Tan) ?
			   PALETTERGB(255, 255, 255) :
			   PALETTERGB(0, 0, 0);
	   }
	}

	~CustomButtonData()
	{
#if 0
		ASSERT((s_instanceCount - 1) >= 0);
	    if( (--s_instanceCount == 0) && s_dc) //s_buttonDib)
	    {
		    delete s_dc; //s_buttonDib;
		    s_dc = 0; //s_buttonDib = 0;
	    }
#endif
	    if(d_buttonDib)
		    delete d_buttonDib;
	}

	void allocateStaticDib(int cx, int cy);

};


//DCObject* CustomButtonData::s_dc = 0;
//UWORD CustomButtonData::s_instanceCount = 0;

static CustomButtonData* cb_getData(HWND hwnd)
{
	ASSERT(hwnd);
	CustomButtonData* cbd = reinterpret_cast<CustomButtonData*>(GetWindowLong(hwnd, GWL_USERDATA));
	ASSERT(cbd != 0);
	return cbd;
}

void CustomButtonData::allocateStaticDib(int cx, int cy)
{
	/*
	* Special case returns
	*
	* if either cx or cy are 0, return
	*/

	if(cx <= 0 || cy <= 0)
	  ;                       // do nothing
	else
	{
	  if( (!d_buttonDib) ||
			((d_buttonDib->getWidth() < cx) || (d_buttonDib->getHeight() < cy)) )
	  {
		 int oldCX = 0;
		 int oldCY = 0;

		 if(d_buttonDib)
		 {
			oldCX = d_buttonDib->getWidth();
			oldCY = d_buttonDib->getHeight();

			delete d_buttonDib;
			d_buttonDib = 0;
		 }

		 d_buttonDib = new DrawDIBDC(maximum(oldCX, cx), maximum(oldCY, cy));
		 ASSERT(d_buttonDib);
	  }
	}
}

static void cb_drawPushButton(CustomButtonData* data, const int cx, const int cy)
{
   ASSERT(data->d_buttonDib);
   if(!data->d_buttonDib)
      return;

	char text[100];
	if(GetWindowText(data->d_hwnd, text, 100))
	{
	   HFONT hFont = (data->d_hFont != 0) ? data->d_hFont :
		   reinterpret_cast<HFONT>(GetStockObject(SYSTEM_FONT));

//	   HBITMAP oldBM = (HBITMAP)data->s_dc->selectObject(data->d_buttonDib->getHandle());
      data->d_buttonDib->setBkMode(TRANSPARENT);

      HFONT oldFont = data->d_buttonDib->setFont(hFont);

	   SIZE s;
	   GetTextExtentPoint32(data->d_buttonDib->getDC(), text, lstrlen(text), &s);

	   const int x = maximum(static_cast<int>(CustomBorderWindow::ThinBorder), (cx - s.cx) / 2);
	   const int y = maximum(0, (cy - s.cy) / 2);
	   const int percent = 90;

	   ASSERT(hFont);

	   COLORREF color = PALETTERGB(0, 0, 0);//data->d_borderColours.colours[CustomBorderInfo::DarkBrown];
	   COLORREF oldColor = data->d_buttonDib->setTextColor(color);

	   wTextOut(data->d_buttonDib->getDC(), x, y, cx - (CustomBorderWindow::ThinBorder * 2), text);

	   data->d_buttonDib->setTextColor(oldColor);
	   data->d_buttonDib->setFont(oldFont);
//	   data->d_buttonDib->selectObject(oldBM);
	}

	else if(data->d_buttonImages)
	{
	   ASSERT(data->d_imgID != c_noImgID);

	   RECT r;
	   GetClientRect(data->d_hwnd, &r);

	   const int cx = (r.right - r.left) - (BorderWidth * 2);
	   const int cy = (r.bottom - r.top) - (BorderWidth * 2);

	   data->d_buttonImages->stretchBlit(data->d_buttonDib, data->d_imgID,
		   BorderWidth, BorderWidth, cx, cy);
	}

}

static void cb_drawCheckBox(CustomButtonData* data, const int cx, const int cy)
{
	/*
	 * Some useful values
	 */

	const int offsetCX = 2;
	const int buttonCX = minimum(cy -(2*offsetCX), cx - (2*offsetCX));
	const int buttonCY = cy - (2*offsetCX);

	if(data->d_checkImage.d_imgLib)
	{
		/*
		 * Blit check images
		 */

		UWORD imageID;

		if(data->d_state & ODS_CHECKED)
			imageID = data->d_checkImage.d_checkedImage;
		else
			imageID = data->d_checkImage.d_uncheckImage;

		data->d_checkImage.d_imgLib->stretchBlit(data->d_buttonDib, imageID, offsetCX, offsetCX,
			buttonCX, buttonCY);

		/*
		 * Put text
		 */

		char text[100];
		if(GetWindowText(data->d_hwnd, text, 100))
		{
			HFONT hFont = (data->d_hFont != 0) ? data->d_hFont :
				reinterpret_cast<HFONT>(GetStockObject(SYSTEM_FONT));

//			HBITMAP oldBM = (HBITMAP)data->s_dc->selectObject(data->d_buttonDib->getHandle());
			HFONT oldFont = data->d_buttonDib->setFont(hFont);

			SIZE s;
			GetTextExtentPoint32(data->d_buttonDib->getDC(), text, lstrlen(text), &s);

			const int x = buttonCX + (2*offsetCX);
			int y = (cy - s.cy) / 2;
			if(y < 0)
				y = offsetCX;

			COLORREF color = PALETTERGB(0, 0, 0);//data->d_borderColours.colours[CustomBorderInfo::DarkBrown];
			COLORREF oldColor = data->d_buttonDib->setTextColor(color);

			wTextOut(data->d_buttonDib->getDC(), x, y, text);

			data->d_buttonDib->setTextColor(oldColor);
			data->d_buttonDib->setFont(oldFont);
//			data->d_buttonDib->selectObject(oldBM);
		}
	}
}

static void cb_drawButtonDib(CustomButtonData* data)
{
	ASSERT(data);
	ASSERT(data->d_buttonDib);

	/*
	 * some useful constants
	 */


	/*
	 * if we have a fill pattern, use it
	 */


	RECT r;
	GetWindowRect(data->d_hwnd, &r);

	const int cx = r.right - r.left;
	const int cy = r.bottom - r.top;

	if(data->d_fillDib)
	{
	 data->d_buttonDib->rect(0, 0, cx, cy, data->d_fillDib);
	}

	/*
	 * Otherwise, fill in with a default background
	 */

	else
	{
	 const ColourIndex ci = data->d_buttonDib->getColour(PALETTERGB(122, 122, 122));
	 data->d_buttonDib->rect(0, 0, cx, cy, ci);
	}

	// draw borders
	SetRect(&r, 0, 0, cx, cy);

	ColourIndex c1 = ( ( (data->d_style & BS_AUTOCHECKBOX) && !(data->d_style & BS_PUSHLIKE) )  ||
							(data->d_state & (ODS_SELECTED | ODS_CHECKED)) ) ?
		data->d_buttonDib->getColour(data->d_borderColours.colours[CustomBorderInfo::DarkBrown]) :
		data->d_buttonDib->getColour(data->d_borderColours.colours[CustomBorderInfo::OffWhite]);

	ColourIndex c2 = ( ( (data->d_style & BS_AUTOCHECKBOX) && !(data->d_style & BS_PUSHLIKE) ) ||
							(data->d_state & (ODS_SELECTED | ODS_CHECKED)) ) ?
		data->d_buttonDib->getColour(data->d_borderColours.colours[CustomBorderInfo::OffWhite]) :
		data->d_buttonDib->getColour(data->d_borderColours.colours[CustomBorderInfo::DarkBrown]);

	data->d_buttonDib->frame(r.left, r.top, r.right-r.left, r.bottom-r.top, c1, c2);

	if( (data->d_style & BS_AUTOCHECKBOX) && !(data->d_style & BS_PUSHLIKE) )// | BS_PUSHLIKE)) )
	{
	 cb_drawCheckBox(data, cx, cy);
	}
	else
	{
	 SetRect(&r, r.left+1, r.top+1, r.right-1, r.bottom-1);
	 cb_drawPushButton(data, cx, cy);
	}

	/*
	 * If window is disabled
	 */

	if(!IsWindowEnabled(data->d_hwnd))
	{
	  data->d_buttonDib->lighten(30);
	  data->lightened(True);
	}
	else
	{
     data->d_buttonDib->darken(10);
	  data->lightened(False);
	}

	if(data->d_state & (ODS_SELECTED | ODS_CHECKED))
	{
		data->checked(True);
	}
    else
	{
		data->checked(False);
	}

}


BOOL cb_onCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
	/*
	 * Allocate pointer to control data
	 */

	CustomButtonData* bd = new CustomButtonData(
											lpCreateStruct->hwndParent,
											hwnd,
											lpCreateStruct->style);

	ASSERT(bd != 0);
	if(bd == 0)
	 return FALSE;


	/*
	 * Assign to window
	 */

	SetWindowLong(hwnd, GWL_USERDATA, reinterpret_cast<LONG>(bd));

	return TRUE;
}

void cb_onDestroy(HWND hwnd)
{
	CustomButtonData* bd = cb_getData(hwnd);

	if(bd)
	 delete bd;

	SetWindowLong(hwnd, GWL_USERDATA, 0);
}

void cb_onPaint(HWND hwnd)
{
	CustomButtonData* bd = cb_getData(hwnd);
	ASSERT(bd);
	ASSERT(bd->d_hwnd == hwnd);

	bool redrawDib = False;
	if(!bd->d_buttonDib)
	{
		RECT r;
		GetClientRect(hwnd, &r);
		bd->allocateStaticDib(r.right - r.left, r.bottom - r.top);
		ASSERT(bd->d_buttonDib);
		if(bd->d_buttonDib)
      {
			redrawDib = True;
         bd->d_buttonDib->setBkMode(TRANSPARENT);
      }
	}
	else
	{
		BOOL enabled = IsWindowEnabled(hwnd);
		if( (!enabled && !bd->lightened()) ||
				(enabled && bd->lightened()) )
		{
			redrawDib = True;
		}

		if(!redrawDib)
		{
			if(!bd->checked() && bd->d_state & (ODS_SELECTED | ODS_CHECKED) ||
				 bd->checked() && !(bd->d_state & (ODS_SELECTED | ODS_CHECKED)))
			{
				redrawDib = True;
			}
		}
	}
#if 0
	if(!bd->s_dc)
	{
		bd->s_dc = new DCObject;
		ASSERT(bd->s_dc);

    bd->s_dc->setBkMode(TRANSPARENT);
	}
#endif
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hwnd, &ps);

	HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
	RealizePalette(hdc);

	RECT r;
	GetWindowRect(hwnd, &r);

	const int cx = r.right-r.left;
	const int cy = r.bottom-r.top;

	if(redrawDib)
		cb_drawButtonDib(bd);

	if(bd->d_buttonDib)
	{
		// draw borders
		SetRect(&r, 0, 0, cx, cy);

		if( (bd->d_style & BS_AUTOCHECKBOX) && !(bd->d_style & BS_PUSHLIKE) )// | BS_PUSHLIKE)) )
			;
		else
		{
			ColourIndex c1 = ( ( (bd->d_style & BS_AUTOCHECKBOX) && !(bd->d_style & BS_PUSHLIKE) )  ||
							(bd->d_state & (ODS_SELECTED | ODS_CHECKED)) ) ?
				bd->d_buttonDib->getColour(bd->d_borderColours.colours[CustomBorderInfo::DarkBrown]) :
				bd->d_buttonDib->getColour(bd->d_borderColours.colours[CustomBorderInfo::OffWhite]);

			ColourIndex c2 = ( ( (bd->d_style & BS_AUTOCHECKBOX) && !(bd->d_style & BS_PUSHLIKE) ) ||
							(bd->d_state & (ODS_SELECTED | ODS_CHECKED)) ) ?
				bd->d_buttonDib->getColour(bd->d_borderColours.colours[CustomBorderInfo::OffWhite]) :
				bd->d_buttonDib->getColour(bd->d_borderColours.colours[CustomBorderInfo::DarkBrown]);

			bd->d_buttonDib->frame(r.left, r.top, r.right-r.left, r.bottom-r.top, c1, c2);
		}

//		HBITMAP old = (HBITMAP)bd->d_buttonDib->selectObject(bd->d_buttonDib->getHandle());
		BitBlt(hdc, 0, 0, cx, cy, bd->d_buttonDib->getDC(), 0, 0, SRCCOPY);
//		bd->s_dc->selectObject(old);
	}

	SelectPalette(hdc, Palette::get(), FALSE);
	EndPaint(hwnd, &ps);
}

BOOL cb_onEraseBkgnd(HWND hwnd, HDC hdc)
{
	// Don't do anything, but let system think it has been erased

	return TRUE;
}

void cb_onEnable(HWND hwnd, BOOL fEnable)
{
	ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);

  InvalidateRect(hwnd, NULL, FALSE);
  UpdateWindow(hwnd);
}

void cb_onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
	ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
	CustomButtonData* bd = cb_getData(hwnd);
	ASSERT(bd);

	SetCapture(hwnd);

	bd->d_state |= ODS_SELECTED;
//   bd->buttonDown(True);

	if( !(bd->d_style & BS_AUTOCHECKBOX) || (bd->d_style & BS_PUSHLIKE) )
	{
	   InvalidateRect(hwnd, NULL, FALSE);
	   UpdateWindow(hwnd);
   }
}

void cb_onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
	ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
	CustomButtonData* bd = cb_getData(hwnd);
	ASSERT(bd);

   RECT r;
   GetClientRect(hwnd, &r);
//   if(bd->buttonDown())
   {
	   bd->d_state &= ~ODS_SELECTED;

	   if(x >= 0 && y >= 0 && x <= r.right && y <= r.bottom)
	   {
	      if(bd->d_style & BS_AUTOCHECKBOX)
	      {
	         bd->d_state ^= ODS_CHECKED;
	         if(bd->d_buttonDib)
	            cb_drawButtonDib(bd);
	      }
	      SendMessage(bd->d_hParent, WM_COMMAND,
		      MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), BN_CLICKED),
		      reinterpret_cast<LONG>(hwnd));
	   }
//      bd->buttonDown(False);
   }

	ReleaseCapture();

	InvalidateRect(hwnd, NULL, FALSE);
	UpdateWindow(hwnd);
}

void cb_onSetFillDib(HWND hwnd, const DIB* fillDib)
{
	ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
	CustomButtonData* bd = cb_getData(hwnd);
	ASSERT(bd);

	bd->d_fillDib = fillDib;
}

void cb_onSetCheckImages(HWND hwnd, const CheckImages* img)
{
	ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
	CustomButtonData* bd = cb_getData(hwnd);
	ASSERT(bd);

	bd->d_checkImage = *img;
}

void cb_onSetImages(HWND hwnd, UWORD imgID, const ImageLibrary* il)
{
	ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
	CustomButtonData* bd = cb_getData(hwnd);
	ASSERT(bd);

	bd->d_buttonImages = il;
	bd->d_imgID = imgID;
}

void cb_onSetBorderColours(HWND hwnd, const CustomBorderInfo* info)
{
	ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
	CustomButtonData* bd = cb_getData(hwnd);
	ASSERT(bd);

	bd->d_borderColours = *info;    // structure copy
}

void cb_onSetFont(HWND hwnd, HFONT hFont)
{
	ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
	CustomButtonData* bd = cb_getData(hwnd);
	ASSERT(bd);

	bd->d_hFont = hFont;
}

void cb_onSetCheck(HWND hwnd, bool f)
{
	ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
	CustomButtonData* bd = cb_getData(hwnd);
	ASSERT(bd);

	if(f)
	 bd->d_state |= ODS_CHECKED;
	else
	 bd->d_state &= ~ODS_CHECKED;

	if(bd->d_buttonDib)
	  cb_drawButtonDib(bd);

	InvalidateRect(bd->d_hwnd, NULL, FALSE);
	UpdateWindow(bd->d_hwnd);
}

void cb_onSize(HWND hwnd, UINT state, int cx, int cy)
{
  ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
  CustomButtonData* bd = cb_getData(hwnd);
  ASSERT(bd);

  if(bd->d_buttonDib)
  {
     delete bd->d_buttonDib;
     bd->d_buttonDib = 0;
  }

  InvalidateRect(bd->d_hwnd, NULL, FALSE);
  UpdateWindow(bd->d_hwnd);
}

bool cb_onGetCheck(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
  CustomButtonData* bd = cb_getData(hwnd);
  ASSERT(bd);

  return (bd->d_state & ODS_CHECKED) != 0;
}

void cb_onKeydown(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
   ASSERT_CLASS(hwnd, CUSTOMBUTTONCLASS);
   CustomButtonData* bd = cb_getData(hwnd);
   ASSERT(bd);

   SendMessage(bd->d_hParent, WM_COMMAND,
		 MAKEWPARAM(GetWindowLong(hwnd, GWL_ID), BN_CLICKED),
		 reinterpret_cast<LONG>(hwnd));
}

LRESULT CALLBACK cb_customButtonProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	cb_onCreate);
		HANDLE_MSG(hWnd, WM_DESTROY, cb_onDestroy);
		HANDLE_MSG(hWnd, WM_PAINT, cb_onPaint);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, cb_onLButtonDown);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, cb_onLButtonUp);
		HANDLE_MSG(hWnd, WM_ERASEBKGND, cb_onEraseBkgnd);
		HANDLE_MSG(hWnd, WM_ENABLE, cb_onEnable);
      HANDLE_MSG(hWnd, WM_SIZE,   cb_onSize);
      HANDLE_MSG(hWnd, WM_KEYDOWN, cb_onKeydown);

		HANDLE_MSG(hWnd, CBM_SETFILLDIB, cb_onSetFillDib);
		HANDLE_MSG(hWnd, CBM_SETBORDERCOLOURS, cb_onSetBorderColours);
		HANDLE_MSG(hWnd, CBM_SETCHECK, cb_onSetCheck);
		HANDLE_MSG(hWnd, CBM_GETCHECK, cb_onGetCheck);
		HANDLE_MSG(hWnd, CBM_SETCHECKBOXIMAGES, cb_onSetCheckImages);
		HANDLE_MSG(hWnd, CBM_SETFONT, cb_onSetFont);
		HANDLE_MSG(hWnd, CBM_SETIMAGES, cb_onSetImages);

	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}


/* -------------------------------------------------------------------
 * Access class
 */



void CustomButton::initCustomButton(HINSTANCE instance)
{
	static bool initialized = False;

	if(!initialized)
	{
		initialized = TRUE;

		WNDCLASSEX wc;

		wc.cbSize			= sizeof(WNDCLASSEX);
		wc.style 			= CS_DBLCLKS | CS_PARENTDC;
		wc.lpfnWndProc 	= cb_customButtonProc;
		wc.cbClsExtra 		= 0;
		wc.cbWndExtra 		= sizeof(CustomButtonData*);
		wc.hInstance 		= instance;
		wc.hIcon 			= NULL;
		wc.hCursor 			= LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground 	= NULL; //(HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName 	= NULL;
		wc.lpszClassName 	= CUSTOMBUTTONCLASS;
		wc.hIconSm 			= NULL;

		RegisterClassEx(&wc);
	}
}


void CustomButton::setFillDib(const DIB* fillDib)
{
  ASSERT(hwnd);
  SendMessage(hwnd, CBM_SETFILLDIB, 0, reinterpret_cast<LPARAM>(fillDib));
}

void CustomButton::setCheckBoxImages(const ImageLibrary* il)
{
  ASSERT(hwnd);
  CheckImages img(il);
  SendMessage(hwnd, CBM_SETCHECKBOXIMAGES, 0, reinterpret_cast<LPARAM>(&img));
}

void CustomButton::setCheckBoxImages(const ImageLibrary* il, UWORD uncheckedImg, UWORD checkedImg)
{
  ASSERT(hwnd);

  CheckImages img(il, uncheckedImg, checkedImg);

  SendMessage(hwnd, CBM_SETCHECKBOXIMAGES, 0, reinterpret_cast<LPARAM>(&img));
}

void CustomButton::setFont(HFONT hFont)
{
  ASSERT(hwnd);
  SendMessage(hwnd, CBM_SETFONT, 0, reinterpret_cast<LPARAM>(hFont));
}

void CustomButton::setBorderColours(const CustomBorderInfo& info)
{
  ASSERT(hwnd);
  SendMessage(hwnd, CBM_SETBORDERCOLOURS, 0, reinterpret_cast<LPARAM>(&info));
}

void CustomButton::setCheck(bool f)
{
  ASSERT(hwnd);
  SendMessage(hwnd, CBM_SETCHECK, static_cast<WPARAM>(f), 0);
}

bool CustomButton::getCheck()
{
  ASSERT(hwnd);
  return SendMessage(hwnd, CBM_GETCHECK, 0, 0) != 0;
}

void CustomButton::setButtonImage(const ImageLibrary* il, UWORD imgID)
{
  ASSERT(hwnd);
  SendMessage(hwnd, CBM_SETIMAGES, static_cast<WPARAM>(imgID), reinterpret_cast<LPARAM>(il));
}

void CustomButton::setFocus()
{
  ASSERT(hwnd);
  SetFocus(hwnd);
}


