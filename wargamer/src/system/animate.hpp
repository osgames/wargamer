/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef ANIMATE_HPP
#define ANIMATE_HPP

#include "sysdll.h"
#include "mytypes.h"

class DrawDIB;
class DrawDIBDC;
struct ImagePos;

class SYSTEM_DLL Frames {
	 DrawDIB* d_frames;
	 DrawDIBDC* d_frame;
	 UWORD d_frameID;               // index to current frame
	 UWORD d_frameCount;
	 const ImagePos* d_data;
  public:
	 Frames() :
		d_frames(0),
		d_frame(0),
		d_frameID(0),
		d_frameCount(0),
		d_data(0) {}

	 ~Frames();

	 void init(HINSTANCE hInst, const char* bmpResName, const char* frameDataResName, const char* imagePosName, UWORD nFrames);
	 void animate(const DrawDIBDC* srcDib, HDC destDC, int x, int y);
	 void blitCurrent(const DrawDIBDC* srcDib, HDC destDC, int x, int y);
	 UWORD frameWidth() const;
    UWORD frameHeight() const;
};


#endif
