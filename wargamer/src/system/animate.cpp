/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "animate.hpp"
#include "dib.hpp"
#include "bmp.hpp"
#include "imglib.hpp"
#include "wmisc.hpp"

Frames::~Frames()
{
  if(d_frames)
	 delete d_frames;
  if(d_frame)
	 delete d_frame;
}

void Frames::init(HINSTANCE hInst, const char* bmpResName, const char* frameDataResName, const char* imagePosName, UWORD nFrames)
{
  ASSERT(hInst);
  ASSERT(bmpResName);
  ASSERT(frameDataResName);

  d_frameCount = nFrames;

  /*
	* Get our dib from the resource file
	*/

  d_frames = BMP::newDrawDIB(hInst, bmpResName, BMP::RBMP_Normal);
  ASSERT(d_frames);

  if(d_frames == 0)
	 throw GeneralError("Unable to create Frames Library");

  d_frames->setMaskColour((d_frames->getBits())[0]);

  /*
	* Get data from the resource file
	*/

  d_data = reinterpret_cast<ImagePos*>(loadResource(hInst, frameDataResName, imagePosName));
  ASSERT(d_data);

  /*
	* Allocate a working dib
	*/

  d_frame = new DrawDIBDC(d_data[0].w, d_data[0].h);
  ASSERT(d_frame);

  d_frame->setMaskColour((d_frames->getBits())[0]);
}

void Frames::animate(const DrawDIBDC* srcDib, HDC destDC, int x, int y)
{
  ASSERT(srcDib);
  ASSERT(destDC);
  // ASSERT(x >= 0);
  // ASSERT(y >= 0);
  ASSERT(d_frameID < d_frameCount);

  if(++d_frameID >= d_frameCount)
	 d_frameID = 0;

  blitCurrent(srcDib, destDC, x, y);
}

/*
 * Blit current frame to destination
 *
 * Updated SWG: 3Dec98 to clip the destination coordinates
 */

void Frames::blitCurrent(const DrawDIBDC* srcDib, HDC destDC, int x, int y)
{
  const ImagePos& pos = d_data[d_frameID];

  /*
   * Clip destination coordinates
   */

  LONG srcX = pos.x;
  LONG srcY = pos.y;
  LONG w = pos.w;
  LONG h = pos.h;

  LONG destX = x;
  LONG destY = y;

  if(srcDib->clip(destX, destY, w, h))
  {
  		srcX += (destX - x);
		srcY += (destY - y);
  		

	  /*
		* First, Blit background from source dib to our working dib
		*/

	  d_frame->blit(0, 0, w, h, srcDib, destX, destY);

	  /*
		* Now, blit foreground onto the working dib
		*/

	  d_frame->blit(0, 0, w, h, d_frames, srcX, srcY);

	  /*
		* Finally, blit our working dib to dest HDC
		*/

	  BitBlt(destDC, destX, destY, w, h, d_frame->getDC(), 0, 0, SRCCOPY);
	}
}

UWORD Frames::frameWidth() const
{
  ASSERT(d_data);
  return d_data[0].w;
}

UWORD Frames::frameHeight() const
{
  ASSERT(d_data);
  return d_data[0].h;
}

