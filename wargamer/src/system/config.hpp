/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CONFIG_H
#define CONFIG_H

#ifndef __cplusplus
#error config.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Configuration File Manager
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  1996/06/22 19:04:48  sgreen
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "winfile.hpp"

class SYSTEM_DLL ConfigFile : public WinFileAsciiReader {
   	static const int MaxLineLength;
      static const char s_defLanguage[];
      static const char s_sectionStartChar;
      static const char s_sectionEndChar;

   	const char* d_fileName;
      const char* d_scanID;
      const char* d_language;
      bool d_firstScan;

   public:
	   ConfigFile(const char* name, const char* language = 0);
	   ~ConfigFile();

      void setLanguage(const char* lang);

	   char* get(const char* id);
	   void set(const char* id, const char* setting);
	   void setBool(const char* id, Boolean flag);
	   Boolean getBool(const char* id, Boolean defVal);

	   void setNum(const char* id, int n);
	   Boolean getNum(const char* id, int& n);

	   int getEnum(const char* id, const char** enums, int defVal);
	   void setEnum(const char* id, const char** enums, int val);

	   Boolean startScan(const char* id);
	   // char* find(const char* id);
      char* nextScan();
	   void endScan();

   private:
	   char* find(const char* id);
      bool findSection(const char* lang);
};


#endif /* CONFIG_H */

