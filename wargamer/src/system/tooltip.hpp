/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOOLTIP_H
#define TOOLTIP_H

#ifndef __cplusplus
#error tooltip.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Tooltip manager
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"

/*
 * Base class for something that can display hints
 * e.g. a Status Window
 */

class SYSTEM_DLL HintLineBase {
  public:
    virtual ~HintLineBase() { }

    virtual void showHint(const String& text) = 0;
		// Display Text in hint line

    virtual void clearHint() = 0;
		// Clear the hint line
};

/*
 * Data passed to addTips
 * Last item in array has controlID set to -1 (or use EndTipData)
 *
 * toolTipID and hintID can be:
 *		: Resource ID
 *		: -1 (Callback) LPSTR_TEXTCALLBACK
 *		: 0 (Nothing).
 *
 * It might be better to make it take a pointer to a string, then
 * we have the option to give it straight text as well as a resource.
 */


struct TipData {

    enum TipID {
      None = 0,
      Callback = -1
    };

    int controlID;		// Child Control ID
    int toolTipID;		// Resource ID for tooltip
    int hintID;			// Resource ID for Hint Line
};

#define EndTipData { -1, -1, -1 }

/*
 * Complete Tool and Hint line class
 *
 * Installs a WindowsHook to intercept all messages, so that
 * each window doesn't need to fiddle about responding to
 * tool tip messages, etc...
 */

#ifndef BEFORE_DLL

class SYSTEM_DLL ToolTipManager {
  public:
    static void addTip(HWND hDial, HWND hControl, int tipID, int hintID);
		// Add a single Tip for a window.

    static void addTip(HWND hDial, int controlID, const RECT& r, int tipID, int hintID);
		// Add a single tip using a rectangle within parent window

    static void addTips(HWND hDial, const TipData* tipData);
		// Add some tips

    static void delTips(HWND hDial);
		// Remove all Tips belonging to given window

    static void setHintDisplay(HintLineBase* hintDisplay);
		// Set the hint Line Displayer

    static void showHint(const String& text);
		// Display hint... 
		// This stops tooltip from using hint until clearHint is called
    static void showHint(HINSTANCE hinst, UINT id);
		// Display Hint using a resource ID
		// Blocks toolTip until clearHinet
    static void clearHint();
		// Clears the hint line and allows tooltips to work again
};

static ToolTipManager g_toolTip;

#else	 // BEFORE_DLL

class ToolTipImp;

class SYSTEM_DLL ToolTipManager {
    ToolTipImp* d_tipData;			// Internal data for tips
  public:
    ToolTipManager();
    ~ToolTipManager();

    void addTip(HWND hDial, HWND hControl, int tipID, int hintID);
		// Add a single Tip for a window.

    void addTip(HWND hDial, int controlID, const RECT& r, int tipID, int hintID);
		// Add a single tip using a rectangle within parent window

    void addTips(HWND hDial, const TipData* tipData);
		// Add some tips

    void delTips(HWND hDial);
		// Remove all Tips belonging to given window

    void setHintDisplay(HintLineBase* hintDisplay);
		// Set the hint Line Displayer

    void showHint(const String& text);
		// Display hint... 
		// This stops tooltip from using hint until clearHint is called
    void showHint(HINSTANCE hinst, UINT id);
		// Display Hint using a resource ID
		// Blocks toolTip until clearHinet
    void clearHint();
		// Clears the hint line and allows tooltips to work again
};

/*
 * Global Instance of Tooltips
 * This might be better being added to CampaignWindows
 * For example... ToolTips functions can be made virtual
 * functions of CampaignWindows, which can then make
 * a call to a ToolTips instance that it creates.
 */

extern SYSTEM_DLL ToolTipManager g_toolTip;

#endif	// BEFORE_DLL


// Need TipTextID for realord.cpp
/*
 * TipText Structure
 */

struct TipTextID {
    int toolTipID;
    int statusID;
};


#endif /* TOOLTIP_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */

