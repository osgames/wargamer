/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cedit.hpp"
#include "dib.hpp"
#include "wind.hpp"
#include "app.hpp"
#include "palette.hpp"
#include "wmisc.hpp"
#include "scrnbase.hpp"
// #include "generic.hpp"
#include "dib_util.hpp"

#include <ctype.h> // for character functions (tolower(int c))

/*-----------------------------------------------------
 *  a user-drawn edit control
 */

class CEdit_Imp : public WindowBaseND {
    String d_text;        // text of the edit control
    UINT d_pos;          // position of caret

    static DrawDIBDC* s_dib;
    static int s_instanceCount;

    enum Flags {
      CaretOn     = 0x01,
      TimerActive = 0x02
    };

    UBYTE d_flags;

    enum {
      TextBlockSize = 25,
      TimerID = 100
    };

    CEditData d_ed;
  public:
    CEdit_Imp();
    ~CEdit_Imp();

    void create(const CEditData& ed);
    void setFont(HFONT hFont)
    {
      if(s_dib)
        s_dib->setFont(hFont);
    }

    void setText(const String& text);
    const String& getText() const { return d_text; }

  private:
    LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
    void onDestroy(HWND hwnd);
    void onSetFocus(HWND hwnd, HWND hwndOldFocus);
    void onPaint(HWND hwnd);
    BOOL onEraseBk(HWND hwnd, HDC hdc);
    void onKey(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);
    void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
    void onTimer(HWND hwnd, UINT id);
    void onKillFocus(HWND hwnd, HWND hwndNewFocus);
    BOOL onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg);

    void addChar(char c);
    void redraw();
    void allocateBuffer(size_t newSize, bool copyOld);
};

DrawDIBDC* CEdit_Imp::s_dib = 0;
int CEdit_Imp::s_instanceCount = 0;

CEdit_Imp::CEdit_Imp() :
  d_text(),
  d_pos(0),
  d_flags(0)
{
  s_instanceCount++;
}

CEdit_Imp::~CEdit_Imp()
{
  ASSERT((s_instanceCount - 1) >= 0);

  selfDestruct();

  if(--s_instanceCount == 0)
  {
    if(s_dib)
      delete s_dib;

    s_dib = 0;
  }
}

void CEdit_Imp::create(const CEditData& ed)
{
  const int dbX = ScreenBase::dbX();
  const int dbY = ScreenBase::dbY();
  const int baseX = ScreenBase::baseX();
  const int baseY = ScreenBase::baseY();

  d_ed = ed;

  HWND hWnd = createWindow(
      0,
      // GenericNoBackClass::className(),
        NULL,
      WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS,
      (ed.d_x * dbX) / baseX, (ed.d_y * dbY) / baseY,
      (ed.d_cx * dbX) / baseX, (ed.d_cy * dbY) / baseY,
      ed.d_parent, (HMENU)ed.d_id // , APP::instance()
  );

  ASSERT(hWnd);
}

void CEdit_Imp::onDestroy(HWND hwnd)
{
  if(d_flags & TimerActive)
  {
    BOOL result = KillTimer(hwnd, TimerID);
    ASSERT(result);
  }
}
LRESULT CEdit_Imp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
      HANDLE_MSG(hWnd, WM_CREATE, onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
      HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
      HANDLE_MSG(hWnd, WM_PAINT, onPaint);
      HANDLE_MSG(hWnd, WM_SETFOCUS, onSetFocus);
      HANDLE_MSG(hWnd, WM_KEYDOWN, onKey);
      HANDLE_MSG(hWnd, WM_KEYUP, onKey);
      HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
      HANDLE_MSG(hWnd, WM_TIMER, onTimer);
      HANDLE_MSG(hWnd, WM_KILLFOCUS, onKillFocus);
      HANDLE_MSG(hWnd, WM_SETCURSOR, onSetCursor);

    default:
      return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL CEdit_Imp::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  DIB_Utility::allocateDib(&s_dib, lpCreateStruct->cx, lpCreateStruct->cy);
  ASSERT(s_dib);

  s_dib->setBkMode(TRANSPARENT);

  return TRUE;
}

void CEdit_Imp::onPaint(HWND hwnd)
{
   PAINTSTRUCT ps;
   HDC hdc = BeginPaint(hwnd, &ps);

   if( s_dib )
   {
      HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
      RealizePalette(hdc);

      RECT r;
      GetWindowRect(hwnd, &r);
      const int cx = r.right - r.left;
      const int cy = r.bottom - r.top;

      // fill in dib
      if( d_ed.d_fillDib )
         s_dib->rect(0, 0, cx, cy, d_ed.d_fillDib);
      else
      {
         const ColourIndex ci = s_dib->getColour(PALETTERGB(255, 255, 255));
         s_dib->rect(0, 0, cx, cy, ci);
      }

      // draw border
      ColourIndex c1 = s_dib->getColour(d_ed.d_bc.colours[CustomBorderInfo::DarkBrown]);
      ColourIndex c2 = s_dib->getColour(d_ed.d_bc.colours[CustomBorderInfo::LightBrown]);

      s_dib->frame(0, 0, cx, cy, c1, c2);
      s_dib->frame(1, 1, cx - 2, cy - 2, c1, c2);

      SIZE s;
      if( !d_text.empty() )
      {
         // get text
         GetTextExtentPoint32(s_dib->getDC(), d_text.c_str(), d_text.size(), &s);
      }
      else
      {
         s.cx = 0;
         s.cy = 0;
      }

      // put text
      const int x = (1 * ScreenBase::dbX()) / ScreenBase::baseX();
      const int y = (s.cy < cy) ? (cy - s.cy) / 2 : 0;

      if( !d_text.empty() )
         wTextOut(s_dib->getDC(), x, y, d_text);

      // draw caret
      if( d_flags & CaretOn )
      {
         const ColourIndex ci = s_dib->getColour(PALETTERGB(0, 0, 0));
         s_dib->vLine(x + s.cx, 2, cy - 4, ci);
         s_dib->vLine(x + s.cx + 1, 2, cy - 4, ci);
      }

      // blit to control dc
      BitBlt(hdc, 0, 0, cx, cy, s_dib->getDC(), 0, 0, SRCCOPY);

      SelectPalette(hdc, oldPal, FALSE);
   }

   EndPaint(hwnd, &ps);
}

BOOL CEdit_Imp::onEraseBk(HWND hwnd, HDC hdc)
{
  return TRUE;
}

void CEdit_Imp::onSetFocus(HWND hwnd, HWND hwndOldFocus)
{
  d_flags |= TimerActive;
  d_flags |= CaretOn;

  redraw();

  int result = SetTimer(hwnd, TimerID, 600, NULL);
  ASSERT(result);
}

void CEdit_Imp::onKillFocus(HWND hwnd, HWND hwndNewFocus)
{
  d_flags &= ~CaretOn;
  d_flags &= ~TimerActive;

  BOOL result = KillTimer(hwnd, TimerID);
  //ASSERT(result);
}


void CEdit_Imp::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  SetFocus(hwnd);
}

void CEdit_Imp::onTimer(HWND hwnd, UINT id)
{
  ASSERT(id == TimerID);

  if(d_flags & CaretOn)
    d_flags &= ~CaretOn;
  else
    d_flags |= CaretOn;

  InvalidateRect(hwnd, NULL, FALSE);
  UpdateWindow(hwnd);
}

BOOL CEdit_Imp::onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg)
{
  static HCURSOR h = 0;

  if(!h)
  {
    h = LoadCursor(NULL, IDC_IBEAM);
    ASSERT(h);
  }

  SetCursor(h);

  return TRUE;
}

void CEdit_Imp::onKey(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
  if(fDown)
  {
    if(d_flags & TimerActive)
    {
      BOOL result = KillTimer(hwnd, TimerID);
      ASSERT(result);

      d_flags &= ~TimerActive;
      d_flags |= CaretOn;

      redraw();
    }

    /*
     * Special cases
     */
    if(vk == VK_RETURN)
    {
       const char ret = 13;
       addChar(ret);
    }
    else if(vk == VK_ESCAPE)
    {
       const char ret = 27;
       addChar(ret);
    }
    // if backspace
    else if(vk == VK_BACK)
    {
      if(d_pos > 0)
      {
        //d_text[--d_pos] = '\0';
        d_text.erase(--d_pos, 1);
        redraw();
      }
    }

    // if spacebar
    else if(vk == VK_SPACE)
    {
      const char space = 32;
      addChar(space);
    }

    // everything else
    else
    {
      UINT c = MapVirtualKey(vk, 2);

      if(c && isprint(c))
        addChar(static_cast<char>(c));
    }

    HWND hParent = GetParent(hwnd);
    ASSERT(hParent);

    int id = GetWindowLong(hwnd, GWL_ID);
    SendMessage(hParent, WM_COMMAND, MAKEWPARAM(id, EN_CHANGE), reinterpret_cast<LPARAM>(hwnd));
  }

  else
  {
    int result = SetTimer(hwnd, TimerID, 600, NULL);
    ASSERT(result);

    d_flags |= TimerActive;
  }
}

void CEdit_Imp::addChar(char c)
{
  const char spaceBar = 32;      // space bar

  if(c != spaceBar)
  {
    /*
     * c will be an upper case so convert
     * to lower in shift key or capslock is not down
     */

    bool shift = (GetKeyState(VK_SHIFT) & 0x8000) != 0;
    bool capLock = (GetKeyState(VK_CAPITAL) & 0x0001) != 0;

    if(!shift && !capLock)
    {
      c = tolower(c);
    }
  }

  ASSERT(d_pos <= d_text.size());

   d_text.insert(d_pos++, 1, c);

  // now redraw text
  redraw();
}

void CEdit_Imp::redraw()
{
  InvalidateRect(getHWND(), NULL, FALSE);
  UpdateWindow(getHWND());
}

void CEdit_Imp::setText(const String& text)
{
   d_text = text;
   d_pos = d_text.size();
   redraw();
}

/*---------------------------------------------------------------------------
 * Client access
 */

CEdit::CEdit() :
  d_edit(new CEdit_Imp)
{
  ASSERT(d_edit);
}

CEdit::~CEdit()
{
  destroy();
}

void CEdit::create(const CEditData& ed)
{
  d_edit->create(ed);
}


void CEdit::setFont(HFONT hFont)
{
  d_edit->setFont(hFont);
}

const String& CEdit::getText()
{
  return d_edit->getText();
}

void CEdit::setText(const String& text)
{
  d_edit->setText(text);
}

void CEdit::destroy()
{
    delete d_edit;
    d_edit = 0;

  // if(d_edit)
  // {
  //   DestroyWindow(d_edit->getHWND());
  //   d_edit = 0;
  // }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
