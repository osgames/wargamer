/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FILLWIND_HPP
#define FILLWIND_HPP

#include "sysdll.h"
#include "grtypes.hpp"
#include "palwind.hpp"
#include "palette.hpp"
#include "dibref.hpp"

/*
 * A window that is filled with a given pattern
 */

class DrawDIBDC;
class DrawDIB;

class DCObject; // Unchecked added by Paul

class SYSTEM_DLL FillWindow {
        // static int s_instanceCount;
        // DCObject d_dc;

        DrawDIBDC* d_dib;
        const DrawDIB* d_fillDib;
        CustomBorderInfo d_borderColors;
        UINT d_shadowCX;
        SIZE d_size;
        bool d_needRedraw;  // set if d_dib needs recreating

    public:

        FillWindow();
        ~FillWindow();

        void setFillDib(const DrawDIB* fillDib);
        void setShadowWidth(UINT sw)
        {
            d_shadowCX = sw;
            d_needRedraw = true;
        }

        void setBorderColors(CustomBorderInfo bc)
        {
            d_borderColors = bc;
            d_needRedraw = true;
        }

        void allocateDib(int cx, int cy);
        void paint(HDC hdc);
        void paint(HDC hdc, const RECT& r);

        const DrawDIB* getFillDib() const { return d_fillDib; }

    private:
        void makeDIB();
};

#endif
