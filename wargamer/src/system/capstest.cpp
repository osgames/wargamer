/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "capstest.hpp"

#include <vfw.h>

/*

  Filename : capstest.cpp
  
  Description : Class to test GDI capabilities

*/


#if defined(DEBUG) && !defined(NOLOG)
#include "clog.hpp"
LogFileFlush CapsTestLog("CapsTest.log");
#endif





CapsTest::CapsTest(HDC hdc, LPBITMAPINFOHEADER bminfo) {

	int val;

	val = GetDeviceCaps(hdc, DRIVERVERSION);
	d_CapsDescription.driverVersion = val;

	val = GetDeviceCaps(hdc, CLIPCAPS);
	d_CapsDescription.clipCapability = val;

	val = GetDeviceCaps(hdc, RASTERCAPS);

	if(val & RC_BANDING) d_CapsDescription.banding = true;
	else d_CapsDescription.banding = false;
	if(val & RC_BITBLT) d_CapsDescription.blit = true;
	else d_CapsDescription.blit = false;
	if(val & RC_BITMAP64) d_CapsDescription.bitmap64k = true;
	else d_CapsDescription.bitmap64k = false;
	if(val & RC_DI_BITMAP) d_CapsDescription.dibBits = true;
	else d_CapsDescription.dibBits = false;
	if(val & RC_DIBTODEV) d_CapsDescription.dibBitsToDevice = true;
	else d_CapsDescription.dibBitsToDevice = false;
	if(val & RC_FLOODFILL) d_CapsDescription.floodFill = true;
	else d_CapsDescription.floodFill = false;
	if(val & RC_GDI20_OUTPUT) d_CapsDescription.gdi20 = true;
	else d_CapsDescription.gdi20 = false;
	if(val & RC_PALETTE) d_CapsDescription.paletteBased = true;
	else d_CapsDescription.paletteBased = false;
	if(val & RC_SCALING) d_CapsDescription.scaling = true;
	else d_CapsDescription.scaling = false;
	if(val & RC_STRETCHBLT) d_CapsDescription.stretchBlit = true;
	else d_CapsDescription.stretchBlit = false;
	if(val & RC_STRETCHDIB) d_CapsDescription.stretchDibBits = true;
	else d_CapsDescription.stretchDibBits = false;

	HDRAWDIB dib = DrawDibOpen();
	val = DrawDibProfileDisplay(bminfo);
	DrawDibClose(dib);

	if(val & PD_CAN_DRAW_DIB) d_CapsDescription.canDrawDib = true;
	else d_CapsDescription.canDrawDib = false;
	if(val & PD_CAN_STRETCHDIB) d_CapsDescription.canStretchAndDraw = true;
	else d_CapsDescription.canStretchAndDraw = false;
	if(val & PD_STRETCHDIB_1_1_OK) d_CapsDescription.stretch1to1 = true;
	else d_CapsDescription.stretch1to1 = false;
	if(val & PD_STRETCHDIB_1_2_OK) d_CapsDescription.stretch1to2 = true;
	else d_CapsDescription.stretch1to2 = false;
	if(val & PD_STRETCHDIB_1_N_OK) d_CapsDescription.stretch1toN = true;
	else d_CapsDescription.stretch1toN = false;
}

void
CapsTest::DebugOut(void) {

#if defined(DEBUG) && !defined(NOLOG)

	CapsTestLog.printf("-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|\n");
	CapsTestLog.printf("    Running 'GetDeviceCaps' test on display.\n");
	CapsTestLog.printf("-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|\n\n");

	CapsTestLog.printf("DriverVersion : %i\n", d_CapsDescription.driverVersion);
	CapsTestLog.printf("DriverVersion : %i\n", d_CapsDescription.clipCapability);

	CapsTestLog.printf("RasterCaps :\n");
	CapsTestLog.printf("- requires banding support : %i\n", d_CapsDescription.banding);
	CapsTestLog.printf("- blit support : %i\n", d_CapsDescription.blit);
	CapsTestLog.printf("- bitmaps over 64k : %i\n", d_CapsDescription.bitmap64k);
	CapsTestLog.printf("- set & getDibBits support: %i\n", d_CapsDescription.dibBits);
	CapsTestLog.printf("- setDibBitsToDevice support : %i\n", d_CapsDescription.dibBitsToDevice);
	CapsTestLog.printf("- floodFill support : %i\n", d_CapsDescription.floodFill);
	CapsTestLog.printf("- Windows 2.0 GDI support : %i\n", d_CapsDescription.gdi20);
	CapsTestLog.printf("- paletteBased device : %i\n", d_CapsDescription.paletteBased);
	CapsTestLog.printf("- scaling support : %i\n", d_CapsDescription.scaling);
	CapsTestLog.printf("- stretchBlt support : %i\n", d_CapsDescription.stretchBlit);
	CapsTestLog.printf("- stretchDibBits support : %i\n", d_CapsDescription.stretchDibBits);

	CapsTestLog.printf("\n\n");


	CapsTestLog.printf("-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|\n");
	CapsTestLog.printf("    Running 'DrawDibProfileDisplay' on vidmem.\n");
	CapsTestLog.printf("-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|\n\n");

	CapsTestLog.printf("Can DrawDib : %i\n", d_CapsDescription.canDrawDib);
	CapsTestLog.printf("Can StretchDib : %i\n", d_CapsDescription.canStretchAndDraw);
	CapsTestLog.printf("Can Stretch 1:1 OK : %i\n", d_CapsDescription.stretch1to1);
	CapsTestLog.printf("Can Stretch 1:2 OK : %i\n", d_CapsDescription.stretch1to2);
	CapsTestLog.printf("Can Stretch 1:N OK : %i\n", d_CapsDescription.stretch1toN);

	CapsTestLog.printf("\n\n");

#endif
}



CapsTest::~CapsTest(void) {


}







