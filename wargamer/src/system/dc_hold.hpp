/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DC_HOLD_HPP
#define DC_HOLD_HPP

#ifndef __cplusplus
#error dc_hold.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Selection of objects into DCs with automatic restoring
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

template<class T>
class DCHolderBase
{
	public:
		DCHolderBase(HDC hdc) :
			d_dc(hdc),
			d_oldObject(0),
         d_initialised(false)
		{
			ASSERT(d_dc != NULL);
		}

		DCHolderBase(HDC hdc, T ob) :
			d_dc(hdc),
			d_oldObject(NULL),
         d_initialised(false)
		{
			ASSERT(d_dc != NULL);
			set(ob);
		}

		~DCHolderBase()
		{
			if(d_initialised)
				d_oldObject.select(d_dc);
		}

		void set(T ob)
		{
			T oldOb = ob.select(d_dc);

			if(!d_initialised)
			{
				d_initialised = true;
				d_oldObject = oldOb;
			}

			// return oldOb;
		}

	private:
		T select(T ob);

	private:
		HDC d_dc;
		T d_oldObject;
		bool d_initialised;
};

class FSelectObject
{
	public:
		FSelectObject() : d_ob() { }
		FSelectObject(HGDIOBJ ob) : d_ob(ob) { }

		HGDIOBJ select(HDC dc)
		{
			return SelectObject(dc, d_ob);
		}

	private:
		HGDIOBJ d_ob;
};


template<class T>
class DCHold : public DCHolderBase<FSelectObject>
{
	public:
		DCHold(HDC dc, T ob) : DCHolderBase<FSelectObject>(dc, ob) { }
		DCHold(HDC dc) : DCHolderBase<FSelectObject>(dc) { }

		void set(T ob)
		{
			DCHolderBase<FSelectObject>::set(ob);
		}


		// T set(T ob) { return static_cast<T>(DCHolderBase<FSelectObject>::set(ob)); }
};

typedef DCHold<HFONT> DCFontHolder;
typedef DCHold<HPEN> DCPenHolder;
typedef DCHold<HBITMAP> DCBitmapHolder;
typedef DCHold<HBRUSH> DCBrushHolder;

class FSetTextColor
{
	public:
		FSetTextColor() : d_col() { }
		FSetTextColor(COLORREF c) : d_col(c) { }
		
		COLORREF select(HDC dc)
		{
			return SetTextColor(dc, d_col);
		}
	private:
		COLORREF d_col;
};

class TextColorHolder : public DCHolderBase<FSetTextColor>
{
	public:
		TextColorHolder(HDC dc, COLORREF col) : DCHolderBase<FSetTextColor>(dc, col) { }
		TextColorHolder(HDC dc) : DCHolderBase<FSetTextColor>(dc) { }
};

class FSetBkColor
{
	public:
		FSetBkColor() : d_col() { }
		FSetBkColor(COLORREF c) : d_col(c) { }
		
		COLORREF select(HDC dc)
		{
			return SetBkColor(dc, d_col);
		}
	private:
		COLORREF d_col;
};

class BkColorHolder : public DCHolderBase<FSetBkColor>
{
	public:
		BkColorHolder(HDC dc, COLORREF col) : DCHolderBase<FSetBkColor>(dc, col) { }
		BkColorHolder(HDC dc) : DCHolderBase<FSetBkColor>(dc) { }
};

class FSetBkMode
{
	public:
		FSetBkMode() : d_mode() { }
		FSetBkMode(int m) : d_mode(m) { }
		
		int select(HDC dc)
		{
			return SetBkMode(dc, d_mode);
		}
	private:
		int d_mode;
};

class BkModeHolder : public DCHolderBase<FSetBkMode>
{
	public:
		BkModeHolder(HDC dc, int mode) : DCHolderBase<FSetBkMode>(dc, mode) { }
		BkModeHolder(HDC dc) : DCHolderBase<FSetBkMode>(dc) { }
};

#endif /* DC_HOLD_HPP */

