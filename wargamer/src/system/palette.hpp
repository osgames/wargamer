/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
#ifndef PALETTE_H
#define PALETTE_H

#ifndef __cplusplus
#error palette.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Palette Manager for Windows 95
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "grtypes.hpp"
#include "globals.hpp"
#include "refptr.hpp"
// #include <list>

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 549 9;     // Disable sizeof contains compiler generated information
#endif

/*
 * Simple class to hold palette
 */

class SYSTEM_DLL HPalette {
   HPALETTE d_hPalette;
 public:
   HPalette() : d_hPalette(NULL) { }
   ~HPalette();

   void set(HPALETTE newPal);
      // Set a new palette.  Release old palette first

   HPALETTE get() const;
      // Return handle to palette.
      // If it is NULL then returns Window's default palette
};


class PaletteDefs
{
   public:
      enum { MaxColors = 256 };
};

/*
 * Remap table
 */

class SYSTEM_DLL ColorRemapTable : public RefBaseCount, public PaletteDefs
{
   public:
      ColorRemapTable() { }
      ColorIndex& operator[](ColorIndex i)
      {
         // ASSERT(i < MaxColors);
         return d_table[i];
      }

      const ColorIndex& operator[](ColorIndex i) const
      {
         // ASSERT(i < MaxColors);
         return d_table[i];
      }

      int delRef() const
      {
         int count = RefBaseCount::delRef();
         if(count == 0)
         {
            deleteMe();
         }
         return count;
      }

   private:
      void deleteMe() const;

   private:
      ColorIndex d_table[MaxColors];
};

/*
 * Useless operators to keep STL happy
 */

inline bool operator == (const ColorRemapTable& t1, const ColorRemapTable& t2)
{
   return true;
}

inline bool operator < (const ColorRemapTable& t1, const ColorRemapTable& t2)
{
   return false;
}

// Similar to a reference Counting Pointer

class SYSTEM_DLL ColorRemapTablePtr
{
   public:
      ~ColorRemapTablePtr()
      {
         if(d_table)
            d_table->delRef();
      }

      ColorRemapTablePtr(const ColorRemapTablePtr& p)
      {
         d_table = p.d_table;
         if(d_table)
            d_table->addRef();
      }

      ColorRemapTablePtr(const ColorRemapTable* p)
      {
         d_table = p;
         if(d_table)
            p->addRef();
      }


      ColorRemapTablePtr& operator = (const ColorRemapTablePtr& p)
      {
         if(&p != this)
         {
            if(d_table)
               d_table->delRef();
            d_table = p.d_table;
            if(d_table)
               d_table->addRef();
         }
         return *this;
      }

      const ColorRemapTable* operator->() const { return d_table; }
      const ColorRemapTable& operator*() const { return *d_table; }
      operator const ColorRemapTable*() const { return d_table; }

   private:
      const ColorRemapTable* d_table;
};

/*--------------------------------------------------------------------------------
 * New Palette Handling
 *
 * Reference Counted Palette Information that is stored in a DIB
 */

class CPalette;

typedef CRefPtr<CPalette> PalettePtr;

class SYSTEM_DLL CPalette : public RefBaseCount, public PaletteDefs
{
   public:

      typedef unsigned int ID;
      typedef ColorRemapTablePtr RemapTablePtr;

      static PalettePtr create(unsigned int nColors, RGBQUAD* colors);
      static PalettePtr defaultPalette();
      static RemapTablePtr makeRemapTable(const PalettePtr& from, const PalettePtr& to);

      void makeDefault() const;
         // Make this palette the default

      unsigned int getNColors() const { return d_nColors; }
         // Return number of colors in palette

      const RGBQUAD* colors() const { return d_colors; }
         // get pointer to raw colors

      const COLORREF color(ColorIndex i) const { return RGB(d_colors[i].rgbRed, d_colors[i].rgbGreen, d_colors[i].rgbBlue); }
         // Return pointer to actual color

      HPALETTE handle() const { return d_handle.get(); }
         // get Windows handle

      ColorIndex getIndex(COLORREF rgb) const;
         // Get closest color

      ID id() const { return d_id; }

      int delRef() const;

      friend bool operator == (const CPalette& p1, const CPalette& p2);
      friend bool operator < (const CPalette& p1, const CPalette& p2);

      // Remap Tables
      RemapTablePtr getDarkRemapTable(int percent) const;
      RemapTablePtr getLightRemapTable(int percent) const;
      RemapTablePtr getBrightRemapTable(int percent) const;
      RemapTablePtr getBlackWhiteRemapTable() const;
      RemapTablePtr getColorizeTable(COLORREF rgb, int percent) const;


   private:
      /*
       * Client must use create()
       * Palettes are automoatically delete when there are no references left
       */

      CPalette(unsigned int nColors, const RGBQUAD* colors);
      CPalette(HPALETTE hPal);
      ~CPalette();

      void deleteMe();


      RGBQUAD        d_colors[MaxColors];
      HPalette       d_handle;
      unsigned int   d_nColors;
      ID             d_id;

      static ID         s_nextID;
      static PalettePtr s_default;         // Default Palette
};


inline bool operator == (const CPalette& p1, const CPalette& p2)
{
   return p1.d_id == p2.d_id;
}

inline bool operator < (const CPalette& p1, const CPalette& p2)
{
   return p1.d_id < p2.d_id;
}


inline int CPalette::delRef() const
{
   int count = RefBaseCount::delRef();
   if(count == 0)
   {
      const_cast<CPalette*>(this)->deleteMe();
   }
   return count;
}


/*--------------------------------------------------------------------------------
 * Old Palette Handling
 *--------------------------------------------------------------------------------
 */

/*
 * Old confused Palette Class
 */

namespace Palette
{
#define PALETTE_EXPORT SYSTEM_DLL

   /*
    * Compatibility with old version of Palette Manager
    */

   PALETTE_EXPORT HPALETTE get();  // { return s_globalPalette.get(); }
      // Return a handle to the global palette

   PALETTE_EXPORT ColorIndex getColorIndex(COLORREF cRef);
      // Convert color reference to index

   typedef CPalette::ID PaletteID;
   PALETTE_EXPORT PaletteID paletteID();
      // Find out if palette has changed

   PALETTE_EXPORT COLORREF brightColor(COLORREF rgb, UBYTE bright, UBYTE contrast);
   inline COLORREF brightColour(COLORREF rgb, UBYTE bright, UBYTE contrast) { return brightColor(rgb, bright, contrast); }
      // Adjust brightness and contrast of a color
      // bright sets the lowest value that a channel can have
      // contrat sets the range between dark and bright colors
      // This would be better implemented by having HSL conversions instead

   PALETTE_EXPORT COLORREF brighten(COLORREF rgb, int percent);
   PALETTE_EXPORT COLORREF darken(COLORREF rgb, int percent);

   inline bool isBlack(PALETTEENTRY& rgb)
   {
      return (rgb.peRed == 0)
         &&  (rgb.peGreen == 0)
         &&  (rgb.peBlue == 0);
   }

   inline bool isWhite(PALETTEENTRY& rgb)
   {
      return (rgb.peRed == 0xff)
         &&  (rgb.peGreen == 0xff)
         &&  (rgb.peBlue == 0xff);
   }

//   class ColorRef
//    {
//       public:
//          ColorRef() : d_converted(false), d_color(PALETTERGB(255,0,255)) { }
//          ColorRef(COLORREF ref) { setColor(ref); }
//          ColorRef& operator = (COLORREF ref) { setColor(ref); return *this; }
//
//          ColorIndex getIndex() const
//          {
//             if(!d_converted)
//             {
//                d_index = getColorIndex(d_color);
//                d_converted = true;
//             }
//             return d_index;
//          }
//
//       private:
//          void setColor(COLORREF ref)
//          {
//             d_converted = false;
//             d_color = ref;
//          }
//
//       private:
//          mutable bool d_converted;
//          union
//          {
//             COLORREF d_color;
//             mutable ColorIndex d_index;
//          };
//    };
//
};

#endif /* PALETTE_H */

/*----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */