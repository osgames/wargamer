/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Combat Log
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.2  1996/02/08 17:33:46  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1996/01/19 11:30:05  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "clog.hpp"
#include <mmsystem.h>

#ifdef DEBUG


#if !defined(NOLOG)

#include <windows.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <time.h>

#include "mytypes.h"
#include "myassert.hpp"
#include "critical.hpp"		// for SharedData
#include "StringPtr.hpp"
#include "winfile.hpp"

struct _Log : public SharedData {
	HANDLE log;
	StringPtr name;
	Boolean created;
	int indent;

	_Log(const char* s);
	~_Log();

	void vprintf(const char* fmt, va_list vaList);
	void wfprintf(LPSTR fmt, ...);
	void write(const char* s);
	void endLine();
};

_Log::_Log(const char* s)
{
	log = NULL;
	name = s;
	created = False;
	indent = 0;
}

_Log::~_Log()
{
}

LogFile::LogFile(const char* s)
{
	logFile = new _Log(s);
	ASSERT(logFile != 0);
}

LogFile::~LogFile()
{
	if(logFile->log != NULL)
	{
		CloseHandle(logFile->log);
		delete logFile;
	}
}

/*
 * Helper Function
 */

void _Log::wfprintf(LPSTR fmt, ...)
{
	char buffer[512];
   buffer[511] = 0;

	va_list vaList;
	va_start(vaList, fmt);
	vbprintf(buffer, 511, fmt, vaList);
	va_end(vaList);

	DWORD bw;

	LockData lock(this);
	WriteFile(log, buffer, strlen(buffer), &bw, NULL);
}


void _Log::write(const char* text)
{
	LockData lock(this);

	if(log == NULL)
	{
		SimpleString fullName;
		FileSystem::makeFullPath(fullName, name);


		if(created)
		{
			log = CreateFile(
				fullName.toStr(),
				GENERIC_WRITE,
				0,
				NULL,
				OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL);

				if(log != NULL)
					SetFilePointer(log, 0, (LPLONG) NULL, FILE_END);
		}
		else
		{
			log = CreateFile(
				fullName.toStr(),
				GENERIC_WRITE,
				0,
				NULL,
				CREATE_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL);

			if(log)
			{
				created = True;

				time_t t;
				time(&t);

				wfprintf("%s created at %s\r\n", (const char*) name, (const char*) ctime(&t));
			}
		}
	}

	if(log != NULL)
	{
		DWORD bw;
		WriteFile(log, text, strlen(text), &bw, NULL);
	}
}

void _Log::vprintf(const char* fmt, va_list vaList)
{
   ASSERT(fmt != 0);

	const int BufLen = 512;
	int bufSpace = BufLen - 1;

	char buffer[BufLen];

	int useIndent = indent;
	const char* leadingText = 0;

	// Get rid of any leading cr/lf's

	while( (fmt[0] == '\r') || (fmt[0] == '\n') )
		++fmt;

	// Deal with indents

	if(fmt[0] == '>')
	{
		indent++;
		fmt++;
		leadingText = "+ ";
	}
	else if(fmt[0] == '<')
	{
		ASSERT(indent > 0);
		indent--;
		useIndent = indent;
		fmt++;
		leadingText = "- ";
	}

	char* bPtr = buffer;

	if(useIndent > 0)
	{
		ASSERT(useIndent < (BufLen/2));

		for(int i  = 0; i < useIndent; i++)
		{
			*bPtr++ = '|';
			*bPtr++ = ' ';
			bufSpace -= 2;
		}
	}

	ASSERT(bufSpace > 0);

	if(leadingText != 0)
	{
		while(*leadingText)
		{
			*bPtr++ = *leadingText++;
			bufSpace--;
		}
	}

	ASSERT(bufSpace > 0);

	vbprintf(bPtr, bufSpace, fmt, vaList);

	// remove any trailing cr/lf

	char* last = buffer;
	for(bPtr = buffer; *bPtr; ++bPtr)
	{
		if((*bPtr != '\r') && (*bPtr != '\n'))
			last = bPtr;
	}
	last[1] = '\0';

	write(buffer);
}

void _Log::endLine()
{
	write("\r\n");
}

void LogFile::vprintf(const char* fmt, va_list args)
{
	logFile->vprintf(fmt, args);
	endLine();
}

void LogFile::printfNoLF(const char* fmt, ...)
{

	va_list vaList;
	va_start(vaList, fmt);
	logFile->vprintf(fmt, vaList);
	va_end(vaList);
}

void LogFile::printf(const char* fmt, ...)
{

	va_list vaList;
	va_start(vaList, fmt);
	vprintf(fmt, vaList);
	va_end(vaList);
}

void LogFile::write(const char* s)
{
	logFile->write(s);
	// endLine();
}

void LogFile::endLine()
{
	logFile->endLine();
}

/*
 * Temporarily close a log file
 */

void LogFile::close()
{
	LockData lock(logFile);
	if(logFile->log != NULL)
	{
		CloseHandle(logFile->log);
		logFile->log = NULL;
	}
}

void LogFile::lock()
{
	if(logFile)
		logFile->enter();
}

void LogFile::unLock()
{
	if(logFile)
		logFile->leave();
}

/*
 * LogFileFlush Functions
 */

void LogFileFlush::endLine()
{
	LogFile::endLine();
	close();
}


/*
 * Stream operators
 */


LogFile& sysTime(LogFile& file)
{
	DWORD ms = timeGetTime();		// Windows API Function returns ms
	file << ms;
	return file;
}

LogFile& operator << (LogFile& file, bool b)
{
	if(b)
		 file << "true";
	else
		file << "false";
	return file;
}


#endif		// NOLOG
#endif		// DEBUG

