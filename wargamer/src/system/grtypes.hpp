/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GRTYPES_H
#define GRTYPES_H

#ifndef __cplusplus
#error grtypes.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Useful types for Graphics Operations
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  1996/02/16 17:34:14  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/11/22 10:45:25  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "myassert.hpp"
#include <windef.h>

typedef UBYTE ColourIndex;
typedef ColourIndex ColorIndex;		// Be lenient towards US/UK spelling differences
typedef LONG PixelDimension;

/*
 * PixelPoint and PixelRect must be defined in the global
 * namespace, because there are frequent forward references
 * to them in other header files, which result in redefinition errors
 * if they are put in a namespace with using statements.
 */

class PixelPoint {
	POINT d_p;
public:
	PixelPoint() { set(0,0); }
	PixelPoint(PixelDimension _x, PixelDimension _y) { set(_x, _y); }
	PixelPoint(const POINT& pt) { d_p = pt; }

	void setX(PixelDimension _x) { d_p.x = _x; }
	void setY(PixelDimension _y) { d_p.y = _y; }
	void x(PixelDimension _x) { d_p.x = _x; }
	void y(PixelDimension _y) { d_p.y = _y; }
	void set(PixelDimension _x, PixelDimension _y) { d_p.x = _x; d_p.y = _y; }

	PixelDimension getX() const { return d_p.x; }
	PixelDimension getY() const { return d_p.y; }
	PixelDimension x() const { return d_p.x; }
	PixelDimension y() const { return d_p.y; }

	// operator POINT() const { return d_p; }
	operator POINT&() { return d_p; }
	operator const POINT&() const { return d_p; }
	// POINT* operator&() { return &d_p; }
	// const POINT* operator&() const { return &d_p; }

	PixelPoint& operator = (const POINT& pt) { d_p = pt; return *this; }

	Boolean operator == (const PixelPoint& p1) const
	{
		return ((d_p.x == p1.d_p.x) && (d_p.y == p1.d_p.y));
		// return (d_p == p1.d_p);
	}

	Boolean operator != (const PixelPoint& p1) const
	{
		return ((d_p.x != p1.d_p.x) || (d_p.y != p1.d_p.y));
		// return (d_p != p1.d_p);
	}

	PixelPoint& operator += (const PixelPoint& p)
	{
		d_p.x += p.d_p.x;
		d_p.y += p.d_p.y;
		return *this;
	}

	PixelPoint& operator -= (const PixelPoint& p)
	{
		d_p.x -= p.d_p.x;
		d_p.y -= p.d_p.y;
		return *this;
	}


	friend PixelPoint operator + (const PixelPoint& pl, const PixelPoint& pr);
	friend PixelPoint operator - (const PixelPoint& pl, const PixelPoint& pr);
	friend PixelPoint operator / (const PixelPoint& pl, unsigned int n);
};

inline PixelPoint operator + (const PixelPoint& pl, const PixelPoint& pr)
{
	return PixelPoint(pl.d_p.x + pr.d_p.x, pl.d_p.y + pr.d_p.y);
}

inline PixelPoint operator - (const PixelPoint& pl, const PixelPoint& pr)
{
	return PixelPoint(pl.d_p.x - pr.d_p.x, pl.d_p.y - pr.d_p.y);
}

inline PixelPoint operator / (const PixelPoint& pl, unsigned int n)
{
	ASSERT(n != 0);
	return PixelPoint(pl.d_p.x / n, pl.d_p.y / n);
}


/*
 * Wrapper for RECT structure
 */

class PixelRect
{
		RECT d_r;
	public:
		PixelRect() { SetRectEmpty(&d_r); }
		PixelRect(const PixelRect& r) { CopyRect(&d_r, &r.d_r); }
		PixelRect(int x, int y, int x1, int y1) { SetRect(&d_r, x, y, x1, y1); }
		PixelRect(const PixelPoint& p1, const PixelPoint& p2)
		{
			SetRect(&d_r, p1.getX(), p1.getY(), p2.getX(), p2.getY());
		}
		PixelRect(const RECT& r) : d_r(r) { }

		PixelRect& operator = (const RECT& r) { d_r = r; return *this; }

		PixelRect& operator = (const PixelRect& r)
		{
			CopyRect(&d_r, &r.d_r);
				return *this;
		}

		PixelRect& operator += (const PixelPoint& p)
		{
			OffsetRect(&d_r, p.getX(), p.getY());
			return *this;
		}

		RECT* operator & () { return &d_r; }
		const RECT* operator &() const { return &d_r; }
		
		operator RECT () const { return d_r; }
		operator const RECT&() const { return d_r; }
		operator RECT&() { return d_r; }
		const RECT& rect() const { return d_r; }

		int left() const { return d_r.left; }
		int right() const { return d_r.right; }
		int top() const { return d_r.top; }
		int bottom() const { return d_r.bottom; }
		int width() const { return d_r.right - d_r.left; }
		int height() const { return d_r.bottom - d_r.top; }

		void left(int p) { d_r.left = p; }
		void right(int p) { d_r.right = p; }
		void top(int p) { d_r.top = p; }
		void bottom(int p) { d_r.bottom = p; }

		static PixelRect intersect(const PixelRect& r1, const PixelRect& r2);
};

#if 0
PixelRect& operator + (PixelRect& r, const PixelPoint& p)
{
	PixelRect r1 = r;
	r1 += p;
	return r1;
}
#endif

#endif /* GRTYPES_H */

