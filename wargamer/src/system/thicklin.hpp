/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef THICKLIN_HPP
#define THICKLIN_HPP

#ifndef __cplusplus
#error thicklin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Thick Line Graphic Engine
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "gengine.hpp"
#include "grtypes.hpp"

class DrawDIB;

class SYSTEM_DLL ThickLineEngine : public GraphicEngine
{
	public:
		ThickLineEngine(DrawDIB* dib);
		~ThickLineEngine();

		void addPoint(const GPoint& p);
		void close();

		void thickness(int thick);
		// void color(COLORREF col);
		void color(ColourIndex col);

	private:
		void plot(int dx, int dy);
			// Plot point at d_lastPoint

	private:
		DrawDIB* d_dib;

		int d_thick;
		int d_circleSize;
		// COLORREF d_col;
		ColourIndex d_colIndex;
		// const UBYTE* d_remapTable;
		DrawDIB* d_circleDIB;

		// UBYTE d_lastDir;
		GPoint d_lastPoint;
		bool d_closed;
};

#endif /* THICKLIN_HPP */

