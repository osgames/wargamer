/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "crossref.hpp"
#include <stdio.h>
#include "myassert.hpp"

/*-------------------------------------------------------------------------------------------------------------------

        File : CROSSREF
        Description : Class to cross reference entries in an ascii file
        
-------------------------------------------------------------------------------------------------------------------*/



CrossReferencer::CrossReferencer(const char * filename) {

    FileBuffer = NULL;
    szSeperator[0] = ' ';
    szSeperator[1] = '\0';
    szComment[0] = '#';
    szComment[1] = '\0';
    
    ASSERT(filename);

    unsigned int filesize = 0;
    unsigned int pos;
    FILE * fileptr;

    fileptr = fopen(filename, "rt");
    if(! fileptr) { FORCEASSERT("Oops - Couldn't open cross-reference file for reading"); return; }
        
    // find filesize
    rewind(fileptr);
    pos = ftell(fileptr);
    fseek(fileptr, 0L, SEEK_END);
    filesize = ftell(fileptr);

    // allocate space & read file
    rewind(fileptr);
    FileBuffer = new char[filesize];
    fread(FileBuffer, 1, filesize, fileptr);
    fclose(fileptr);

}

CrossReferencer::~CrossReferencer(void) {

    if(FileBuffer) delete FileBuffer;

}


bool
CrossReferencer::DefineSeperator(const char * lpszStr) {

    if(! lpszStr || strlen(lpszStr) >= 64) { FORCEASSERT("Oops - Problem defining seperator"); return false; }
    strcpy(szSeperator, lpszStr);
    return true;

}


bool
CrossReferencer::DefineComment(const char * lpszStr) {

    if(! lpszStr || strlen(lpszStr) >= 64) { FORCEASSERT("Oops - Problem defining comment"); return false; }
    strcpy(szComment, lpszStr);
    return true;

}


char *
CrossReferencer::GetDataStart(void) {
    return FileBuffer;
}



char *
CrossReferencer::GetIntegerEntry(const char * lpszStr, int * retval, char * data_pos) {

if(! lpszStr) { FORCEASSERT("Oops - Bad string passed in for cross-referencing"); return 0; }

    if(!data_pos) data_pos = FileBuffer;

    bool found = false;
    // repeat until we've found the string which is not a comment
    while(! found) {

        // find occurance
        data_pos = strstr(data_pos, lpszStr);
        // if not found anywhere, then return
        if(! data_pos) {
//            FORCEASSERT("Oops - Cross-reference string not found in file");
            return false;
        }
        
        char * comment_pos = data_pos;
        // backtrack until either a comment or previous newline is found
        do {
            comment_pos--;
            // if this is a comment
            if(*comment_pos == szComment[0] ) {
                found = false;
                int stringlength = strlen(lpszStr);
                while(stringlength) {
                    stringlength--; data_pos++;
                    if(! *data_pos) { FORCEASSERT("Oops - End of file found when scanning for cross reference"); return 0; }
                }
            }
            // if this is the actual string
            if(*comment_pos == 13 || *comment_pos == 10 ) found = true;
        } while(*comment_pos != 13 && *comment_pos != 10 && *comment_pos != szComment[0] );

    }

    // skip over the input string, making sure we're not at end
    int stringlength = strlen(lpszStr);
    while(stringlength) {
        stringlength--;
        data_pos++;
        if(! *data_pos) { FORCEASSERT("Oops - End of file found when scanning for cross reference"); return 0; }
    }

    // find occurence of seperator string
    data_pos = strstr(data_pos, szSeperator);
    if(! data_pos) { FORCEASSERT("Oops - Couldn't find seperator after cross-reference item"); return 0; }

    // skip over the seperator string, making sure we're not at end
    stringlength = strlen(szSeperator);
    while(stringlength) {
        stringlength--;
        data_pos++;
        if(! *data_pos) { FORCEASSERT("Oops - End of file found when scanning past seperator"); return 0; }
    }

    // read in the value
    if(sscanf(data_pos, "%i", retval) != EOF) return data_pos;
    else return 0;

}



char *
CrossReferencer::GetCharacterEntry(const char * lpszStr, char * data_pos) {
if(! lpszStr) { FORCEASSERT("Oops - Bad string passed in for cross-referencing"); return false; }

    // find occurence of input string in datafile
    if(!data_pos) data_pos = FileBuffer;
    data_pos = strstr(data_pos, lpszStr);
    if(! data_pos) { FORCEASSERT("Oops - Cross-reference string not found in file"); return false; }


    bool found = false;
    // repeat until we've found the string which is not a comment
    while(! found) {

        // find occurance
        data_pos = strstr(data_pos, lpszStr);
        // if not found anywhere, then return
        if(! data_pos) {
//            FORCEASSERT("Oops - Cross-reference string not found in file");
            return false;
        }
        
        char * comment_pos = data_pos;
        // backtrack until either a comment or previous newline is found
        do {
            comment_pos--;
            // if this is a comment
            if(*comment_pos == szComment[0] ) {
                found = false;
                int stringlength = strlen(lpszStr);
                while(stringlength) {
                    stringlength--; data_pos++;
                    if(! *data_pos) { FORCEASSERT("Oops - End of file found when scanning for cross reference"); return 0; }
                }
            }
            // if this is the actual string
            if(*comment_pos == 13 || *comment_pos == 10 ) found = true;
        } while(*comment_pos != 13 && *comment_pos != 10 && *comment_pos != szComment[0] );

    }

    // skip over the input string, making sure we're not at end
    int stringlength = strlen(lpszStr);
    while(stringlength) {
        stringlength--;
        data_pos++;
        if(! *data_pos) { FORCEASSERT("Oops - End of file found when scanning for cross reference"); return 0; }
    }







    // find occurence of seperator string
    data_pos = strstr(data_pos, szSeperator);
    if(! data_pos) { FORCEASSERT("Oops - Couldn't find seperator after cross-reference item"); return false; }

    // skip over the seperator string, making sure we're not at end
    stringlength = strlen(szSeperator);
    while(stringlength) {
        stringlength--;
        data_pos++;
        if(! *data_pos) { FORCEASSERT("Oops - End of file found when scanning past seperator"); return false; }
    }

    // read in the value
    char temp_buffer[1024];
    if(sscanf(data_pos, "%s", temp_buffer) != EOF) {
        return strdup(temp_buffer);
    }
    
    else return NULL;

}



