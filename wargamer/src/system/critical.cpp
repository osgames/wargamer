/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Base class for shared data
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1996/06/22 19:02:35  sgreen
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "critical.hpp"
#include "myassert.hpp"

SharedData::SharedData()
{
#ifdef DEBUG_CRITICAL
	static int nextID = 0;
	id = nextID++;

	debugLog("InitialiseCriticalSection (id=%d)\n", id);
	count = 0;
#endif

	InitializeCriticalSection(&critic);
}

SharedData::~SharedData()
{
#ifdef DEBUG_CRITICAL
	debugLog("DeleteCriticalSection (id=%d, count=%d)\n", id, count);
#endif

	DeleteCriticalSection(&critic);
}

void SharedData::enter() const
{
#ifdef DEBUG_CRITICAL
	count++;
	debugLog("+SharedData::enter(%d), count=%d\n", id, count);
	debugLog("+EnterCriticalSection(%d)... start\n", id);
#endif
	EnterCriticalSection(&critic);
#ifdef DEBUG_CRITICAL
	debugLog("-EnterCriticalSection(%d)... end\n", id);
#endif
}

void SharedData::leave() const
{
#ifdef DEBUG_CRITICAL
	debugLog("+LeaveCriticalSection(%d)... start (count=%d)\n", id, count);
#endif
	LeaveCriticalSection(&critic);
#ifdef DEBUG_CRITICAL
	debugLog("-LeaveCriticalSection(%d)... end\n", id);
	debugLog("-SharedData::leave(%d)\n", id);
	--count;
#endif
}

