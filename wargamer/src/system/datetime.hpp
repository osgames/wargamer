/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DATETIME_HPP
#define DATETIME_HPP

#ifndef __cplusplus
#error datetime.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Date and Time Definitions
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "date.hpp"		// Snippets Date

namespace Greenius_System
{

class DateDefinitions
{
    public:
        typedef UBYTE Day;			// 1..31
        typedef UBYTE Month;		// 0..11 = Jan..Dec
        typedef UWORD Year;			// 1800, 1815, 1865, etc

        enum MonthEnum {
	        January,
	        February,
	        March,
	        April,
	        May,
	        June,
	        July,
	        August,
	        September,
	        October,
	        November,
	        December
        };
};

class TimeDefinitions
{
    public:
        typedef UBYTE Hour;			// 0..23
        typedef UBYTE Minute;		// 0..59
        typedef UBYTE Second;		// 0..59
        typedef UBYTE TSecond;		// 0..9
};

//-----------------------------------------------------
// this typedef locks up the watcom compiler 
// when Greenius_System::Date is used from within other namespaces
//-----------------------------------------------------
// typedef zDate Date;

class SYSTEM_DLL Date : public DateDefinitions
{
	public:
		// duplicate zDate's constructors and assignments that we will use
		Date() : d_date() { }
		Date(MonthEnum month, Day day, Year year);
		// Date(const Date &aDate) : zDate(aDate) { }
		// Date&            operator=(const Date &aDate);

        Date& operator ++() { ++d_date; return *this; }

        MonthEnum month() const { return convertMonth(d_date.Month()); }
        Day day() const { return d_date.Day(); }
        Year year() const { return d_date.Year(); }
        const char* monthName(Boolean abrev) { return getMonthName(month(), abrev); }

        static const char* getMonthName(Month m, Boolean abrev);
        const char* Date::toAscii(char* buffer);

    private:
        zDate::month convertMonth(MonthEnum m) const;
        MonthEnum convertMonth(zDate::month m) const;

        zDate d_date;


};

class SYSTEM_DLL Time : public TimeDefinitions
{
	public:
		typedef UBYTE Hour;			// 0..23
		typedef UBYTE Minute;		// 0..59
		typedef UBYTE Second;		// 0..59

		typedef ULONG Ticks;
		enum 
		{
			TicksPerSecond = 1,
			SecondsPerMinute = 60,
			MinutesPerHour = 60,
			HoursPerDay = 24,
		};

		struct OutOfRange { };		// Thrown if a value is out of range

		/*
		 * Contructors
		 */

		Time() : d_hours(), d_minutes(), d_seconds() { }
		
		Time(Ticks ticks) { set(ticks); }

		Time(const Time& time) : 
			d_hours(time.d_hours),
			d_minutes(time.d_minutes),
			d_seconds(time.d_seconds)
		{
		}
		
		Time(Hour h, Minute m, Second s=0) : d_hours(h), d_minutes(m), d_seconds(s) { }

		/*
		 * Assignment
		 */

		Time& operator = (const Time& t)
		{
			d_hours = t.d_hours;
			d_minutes = t.d_minutes;
			d_seconds = t.d_seconds;
			return *this;
		}

		Time& operator = (Ticks t) { set(t); return *this; }

		Hour hours() const { return d_hours; }
		Minute minutes() const { return d_minutes; }
		Second seconds() const { return d_seconds; }

		Ticks ticks() const;

	private:
		void set(Ticks t);

	private:
		Hour d_hours;
		Minute d_minutes;
		Second d_seconds;
};


};	// namespace Greenius_System


#endif /* DATETIME_HPP */

