/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
#ifndef THREADMSG_HPP
#define THREADMSG_HPP
/*
 * Interthread Messages
 */


#include "sync.hpp"
#include "sysdll.h"
// #include <list>
// #include <stack>
#include <queue>

namespace WargameMessage
{

  	class MessageBase
	{
		public:
			MessageBase() { }
			virtual ~MessageBase() { }

			virtual void run() = 0;
			virtual void clear() { delete this; }

         SYSTEM_DLL void send();;     // { return postMessage(this); }
  	};

   inline void postMessage(MessageBase* msg) { msg->send(); }


	class WaitableMessage : public MessageBase
	{
		protected:
			Event d_event;						// for caller to wait for
		public:
#ifdef DEBUG
			int wait() { return d_event.wait(100000); }
#else
			int wait() { return d_event.wait(); }
#endif
         void signal() { d_event.set(); }

         SyncObject& syncObject() { return d_event; }

			virtual void clear() { }
	};

   class MessageQueue
   {
      public:
         SYSTEM_DLL static MessageQueue* instance();

         SYSTEM_DLL bool isEmpty() const;
         SYSTEM_DLL MessageBase* get();
         SYSTEM_DLL void put(MessageBase* msg);

         SyncObject& syncObject() { return d_msgInserted; }

         SYSTEM_DLL void clear();

      private:
         static MessageQueue* s_instance;

//         struct MessageItem
//         {
//            AutoEvent d_received;
//            MessageBase* d_msg;
//         };

         AutoEvent d_msgInserted;
         // simple_queue<MessageItem> d_messages;
         // queue<MessageBase*,deque<MessageBase*> > d_messages;
         std::queue<MessageBase*, std::deque<MessageBase*> > d_messages;
   };

}  // namespace

#endif   // THREADMSG_HPP


/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */