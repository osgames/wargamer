/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Miscellaneous functions for LZHuf compression/Decompression
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1994/07/19  19:53:03  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "lzmisc.h"

/* Initialize tree */

void LzHuf::StartHuff ()
{
        register SWORD i, j;

        for (i = 0; i < N_CHAR; i++) {
                freq[i] = 1;
                son[i] = i + T;
                prnt[i + T] = i;
        }
        i = 0; j = N_CHAR;
        while (j <= R) {
                freq[j] = freq[i] + freq[i + 1];
                son[j] = i;
                prnt[i] = prnt[i + 1] = j;
                i += 2; j++;
        }
        freq[T] = 0xffff;
        prnt[R] = 0;
}

/* reconstruct tree */
void LzHuf::reconst ()
{
        register SWORD i, j, k;
        register UWORD f;

        /* correct leaf node into of first half,
           and set these freqency to (freq+1)/2       */
        j = 0;
        for (i = 0; i < T; i++) {
                if (son[i] >= T) {
                        freq[j] = (freq[i] + 1) / 2;
                        son[j] = son[i];
                        j++;
                }
        }
        /* build tree.  Link sons first */
        for (i = 0, j = N_CHAR; j < T; i += 2, j++) {
                k = i + 1;
                f = freq[j] = freq[i] + freq[k];
                for (k = j - 1; f < freq[k]; k--);
                k++;
                {       register UWORD *p, *e;
                        for (p = &freq[j], e = &freq[k]; p > e; p--)
                                p[0] = p[-1];
                        freq[k] = f;
                }
                {       register SWORD *p, *e;
                        for (p = &son[j], e = &son[k]; p > e; p--)
                                p[0] = p[-1];
                        son[k] = i;
                }
        }
        /* link parents */
        for (i = 0; i < T; i++) {
                if ((k = son[i]) >= T) {
                        prnt[k] = i;
                } else {
                        prnt[k] = prnt[k + 1] = i;
                }
        }
}


/* update given code's frequency, and update tree */

void LzHuf::update (UWORD c)
{
        register UWORD *p;
        register SWORD i, j, k, l;

        if (freq[R] == MAX_FREQ) {
                reconst();
        }
        c = prnt[c + T];
        do {
                k = ++freq[c];

                /* swap nodes when become wrong frequency order. */
                if (k > freq[l = c + 1]) {
                        for (p = freq+l+1; k > *p++; ) ;
                        l = p - freq - 2;
                        freq[c] = p[-2];
                        p[-2] = k;

                        i = son[c];
                        prnt[i] = l;
                        if (i < T) prnt[i + 1] = l;

                        j = son[l];
                        son[l] = i;

                        prnt[j] = c;
                        if (j < T) prnt[j + 1] = c;
                        son[c] = j;

                        c = l;
                }
        } while ((c = prnt[c]) != 0);   /* loop until reach to root */
}

