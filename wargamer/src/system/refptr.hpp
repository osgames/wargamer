/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef REFPTR_HPP
#define REFPTR_HPP

#ifndef __cplusplus
#error refptr.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Reference Counting Pointers
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "myassert.hpp"

/*
 * Empty RefBase to allow RefPtr<> to be used for debugging.
 */

class RefBaseNull
{
 public:
	void addRef() const { }
	void delRef() const { }
};

/*
 * Simple Reference counting base
 *
 * Don't need any virtual functions, bacause all access is
 * done via RefPtr<>, which can be more efficient if it
 * can inline the addRef/delRef functions instead of having
 * to go through virtual function calls.
 *
 * We are never going to pass RefBaseCounts around polymorphically
 */

class RefBaseCount
{
	mutable int d_refCount;
 public:
 	RefBaseCount() { d_refCount = 0; }

	virtual ~RefBaseCount()
	{
		ASSERT(d_refCount == 0);
	}

	int addRef() const
	{
		d_refCount++; 
		return d_refCount;
	}

	int delRef() const 
	{
		ASSERT(d_refCount > 0);
		d_refCount--; 
		return d_refCount;
	}

	int refCount() const { return d_refCount; }
};


/*
 * Version that deletes itself when reference gets to 0.
 *
 * Destructor MUST be virtual!!!! for "delete this" to work.
 */

class RefBaseDel : public RefBaseCount 
{
 public:
 	RefBaseDel() : RefBaseCount() { }

	virtual ~RefBaseDel()
	{
	}

	//---- Use RefBaseCount's implementation
	// int addRef() const { return ++d_refCount; }

	int delRef() const
	{
		int count = RefBaseCount::delRef();

		if(count == 0)
		{
			RefBaseDel* ncThis = const_cast<RefBaseDel*>(this);
			delete ncThis; 
		}

		return count;
	}

	//---- Use RefBaseCount's implementation
	// /* virtual */ int refCount() const { return d_refCount; }
};

/*
 * Class T must include the member functions:
 *		addRef()
 *		delRef()
 * Deriving from RefBaseCount will give a very basic Reference Pointer
 * that simply checks that there are no references when it is deleted.
 *
 * A real class is more likely to want to do things when the last reference
 * is removed, such as delete itself.
 *
 * constness applies to the pointer rather than to RefPtr
 * e.g. const RefPtr<T> ptr; ptr = ptr2;
 * is a valid instruction even thought it changes ptr.d_value
 */


template<class T>
class CRefPtr
{
	public:
 		CRefPtr();									// defined in refptrim.hpp
 		CRefPtr(const T* value);							// defined in refptrim.hpp
		CRefPtr(const CRefPtr<T>& ptr);			// defined in refptrim.hpp
		~CRefPtr();									// defined in refptrim.hpp

 		const T* operator ->() const 
		{
			ASSERT(d_const != 0);
			return d_const; 
		}

		const T& operator *() const 
		{
			ASSERT(d_const != 0);
			return *d_const; 
		}

		CRefPtr<T>& operator = (const T* value);				// defined in refptrim.hpp
		CRefPtr<T>& operator = (const CRefPtr<T>& ptr);		// defined in refptrim.hpp

		const T* value() const { return d_const; }

		const T* operator()() const
		{
			return d_const;
		}

		operator const T* () const { return d_const; }

		bool operator == (const T* t) const { return d_const == t; }
		bool operator != (const T* t) const { return d_const != t; }
		bool operator == (const CRefPtr<T>& t) const { return d_const == t.d_const; }
		bool operator != (const CRefPtr<T>& t) const { return d_const != t.d_const; }

		bool operator !() const { return d_const == 0; }

      // Simple Pointer comparison
      bool operator<(const CRefPtr<T>& t) const { return d_const < t.d_const; }

	protected:

		union		// See Scott Myers, More Effective C++, Item 28
		{
			T* d_nonConst;
			const T* d_const;
		};
};



template<class T>
class RefPtr : public CRefPtr<T>
{
 public:
 	RefPtr();									// defined in refptrim.hpp
 	RefPtr(T* value);							// defined in refptrim.hpp
	RefPtr(const RefPtr<T>& ptr);					// defined in refptrim.hpp
	~RefPtr();									// defined in refptrim.hpp

 	T* operator ->() const
	{
		ASSERT(d_nonConst != 0);
		return d_nonConst; 
	}

	T& operator *() const 
	{
		ASSERT(d_nonConst != 0);
		return *d_nonConst; 
	}

	RefPtr<T>& operator = (T* value);				// defined in refptrim.hpp
	RefPtr<T>& operator = (const RefPtr<T>& ptr);		// defined in refptrim.hpp

	T* value() const { return d_nonConst; }

	T* operator()() const
	{
		return d_nonConst;
	}

	operator T* () const { return d_nonConst; }

};

template<class T>
inline CRefPtr<T>::CRefPtr() :
	d_const(0)
{
}

/*
 * Non-Constant version
 */

template<class T>
inline RefPtr<T>::RefPtr() : CRefPtr<T>()
{
}

template<class T>
inline RefPtr<T>::RefPtr(T* value)  : CRefPtr<T>(value)
{
}

template<class T>
inline RefPtr<T>::RefPtr(const RefPtr<T>& ptr) : CRefPtr<T>(ptr)
{
}

template<class T>
inline RefPtr<T>::~RefPtr()
{
}

template<class T>
inline RefPtr<T>& RefPtr<T>::operator = (T* value)
{
	CRefPtr<T>::operator = (value);
	return *this;
}

template<class T>
inline RefPtr<T>& RefPtr<T>::operator = (const RefPtr<T>& ptr)
{
	CRefPtr<T>::operator = (ptr);
	return *this;
}

// #if !defined(NO_REFPTR_IMP)

template<class T>
inline CRefPtr<T>::CRefPtr(const T* value) 
{
	if(value)
		value->addRef();
	d_const = value; 
}

template<class T>
inline CRefPtr<T>::CRefPtr(const CRefPtr<T>& ptr) 
{
	if(ptr.d_const)
		ptr.d_const->addRef();
	d_const = ptr.d_const; 
}

template<class T>
inline CRefPtr<T>::~CRefPtr()
{
	if(d_const)
		d_const->delRef();
}

template<class T>
inline CRefPtr<T>& CRefPtr<T>::operator = (const T* value)
{
	if(d_const)
		d_const->delRef();
	if(value)
		value->addRef();
	d_const = value; 
	return *this;
}

template<class T>
inline CRefPtr<T>& CRefPtr<T>::operator = (const CRefPtr<T>& ptr)
{
	if(d_const)
		d_const->delRef();
	if(ptr.d_const)
		ptr.d_const->addRef();
	d_const = ptr.d_const; 
	return *this;
}

// #endif



#endif /* REFPTR_HPP */

