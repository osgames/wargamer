/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 * Name Table implementation
 */

#include "stdinc.hpp"
#include "nameTable.hpp"
#include "winfile.hpp"
#include "idstr.hpp"

/*
 * Statics
 */

const char StringFileTable::s_id[] = "GreeniusNameTable";
const UWORD StringFileTable::s_version = 0x0001;


/*
 * Public Functions
 */

StringFileTable::StringFileTable(const char* fileName) :
   d_items(),
   d_changed(false),
   d_fileName(fileName)
{
   /*
    * If the file exists open it up and initialise values
    */

   if(fileName)
      readFile();



}

StringFileTable::~StringFileTable()
{
   /*
    * If the file has changed then save it
    */

//   if(d_changed)
//      writeFile();

   for(Container::iterator it = d_items.begin();
      it != d_items.end();
      ++it)
   {
#ifdef DEBUG
      if(it->d_new)
         debugLog("StringFileTable::~ %s is a new name in %s\n", it->d_name, (const char*)d_fileName);
#endif


      delete it->d_name;
      it->d_name = 0;
   }
}

const char* StringFileTable::get(ID id)
{
   ASSERT((id == Null) || (id < d_items.size()));
   if(id == Null)
      return "";
   else
   {
      const char* s = d_items[id].d_name;
      ASSERT(s != 0);
      return s;
   }
}

bool StringFileTable::isNew(ID id)
{
   ASSERT((id == Null) || (id < d_items.size()));
   if(id == Null)
      return false;
   else
      return d_items[id].d_new;
}

StringFileTable::ID StringFileTable::update(ID id, const char* s)
{
   ASSERT((id == Null) || (id < d_items.size()));

   if((id == Null) || !d_items[id].d_new)
      return add(s);
   else
   {
      delete[] d_items[id].d_name;
      d_items[id].d_name = copyString(s);
      d_changed = true;
      return id;
   }
}

StringFileTable::ID StringFileTable::add(const char* s)
{
   ASSERT(d_items.size() < ID_MAX);

   if(s == 0)
      return Null;

   ID idToUse = static_cast<ID>(d_items.size());

   ID id = 0;
   for(Container::const_iterator it = d_items.begin();
      it != d_items.end();
      ++it, ++id)
   {
      const char* name = it->d_name;
      if(!name)
         idToUse = id;
      else if(strcmp(name, s) == 0)
         return id;
   }

   d_changed = true;

   Item newItem(copyString(s));

   if (idToUse >= d_items.size())
   {
      d_items.push_back(newItem);
      ASSERT(d_items.size() < ID_MAX);
      return static_cast<ID>(d_items.size() - 1);
   }
   else
   {
      d_items[idToUse] = newItem;
      return idToUse;
   }
}

bool StringFileTable::isValid(ID id)
{
   return ( (id == Null) ||
            (
               (id < d_items.size()) &&
               (d_items[id].d_name != 0)
            )
          );
}

/*
 * Private functions
 */

void StringFileTable::readFile()
{
   // Todo:

   try
   {
      WinFileAsciiReader f(d_fileName);

      // Read id, version, count

      char* header = f.readLine();
      StringParse parser(header);
      char* id = parser.getToken();
      ASSERT(strcmp(id, s_id) == 0);
      if(strcmp(id, s_id) != 0)
         throw FileError();

      unsigned int version;
      if(!parser.getInt(version))
         throw FileError();

      ASSERT(version <= s_version);
      if(version > s_version)
         throw FileError();

      // Get how many there are
      // and create vector full of 0's.

      unsigned int count = 0;
      parser.getInt(count);
      if (count)
      {
         ASSERT(count < ID_MAX);

         // d_items.reserve(count);
         d_items.insert(d_items.begin(), count, Item());
      }

      ASSERT(parser.getToken() == 0);

      /*
       * Read names from file
       */

      char* line;
      while( (line = f.readLine()) != 0)
      {
         // Lines beginning with ; are comments

         if((line[0] == ';') || (line[0] == '\0'))
            continue;

         parser.init(line);

         // get id

         unsigned int id;
         if(!parser.getInt(id))
            throw FileError();

         char* name = parser.getToken();

         ASSERT(parser.getToken() == 0);

         ASSERT(id < d_items.size());

         if (id >= d_items.size())
         {
            d_items.insert(d_items.end(), id - d_items.size() + 1, Item());
         }


         d_items[id].d_name = copyString(name);
         d_items[id].d_new = false;
      }
   }
   catch(const FileError& e)
   {
      // Something bad happened... so restore to empty state
      d_changed = true;
      d_items.clear();
   }

   d_changed = false;
}

void StringFileTable::writeFile()
{
   WinFileAsciiWriter f(d_fileName);

   f.printf("%s %d %d\n",
      s_id,
      s_version,
      (int) d_items.size());

   ID id = 0;
   for(Container::const_iterator it = d_items.begin();
      it != d_items.end();
      ++it, ++id)
   {
      const char* s = it->d_name;
      if (s != 0)
      {
         f.printf("%d \"%s\"\n", (int) id, s);
      }
      it->d_new = false;
   }

   d_changed = false;
}