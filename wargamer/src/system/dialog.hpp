/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DIALOG_H
#define DIALOG_H

#ifndef __cplusplus
#error dialog.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Class for modeless dialog boxes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.10  1996/06/22 19:06:39  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.9  1996/02/23 14:08:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.8  1996/02/22 10:29:01  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.7  1996/02/19 16:17:35  Steven_Green
 * Moved EditDialog base class into MapWind.hpp
 *
 * Revision 1.6  1995/12/11 11:51:17  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1995/12/07 09:17:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/12/05 09:20:07  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/11/29 12:14:04  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/11/14 11:25:57  Steven_Green
 * Helper functions for combo controls
 *
 * Revision 1.1  1995/11/13 11:11:05  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "wind.hpp"
#ifdef DEBUG
#include "myassert.hpp"
#endif

/*
 * Enable the following line log almost all messages to dialog windows!
 */

// #define DEBUG_MESSAGES

struct ComboInit;

/*
 * Base class for modeless dialog box
 */

class ModelessDialog : public HWNDbase {

	bool d_deleteSelf;			// Set if should delete itself when window is destroyed

public:
	ModelessDialog() : d_deleteSelf(true) {  }
	virtual ~ModelessDialog() { }

	HWND getHWND() const { return hWnd; }
	void setDeleteSelfMode(bool mode) { d_deleteSelf = mode; }

private:
	virtual BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) = 0;

protected:
	SYSTEM_DLL static BOOL CALLBACK baseDialogProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	SYSTEM_DLL static BOOL CALLBACK baseModalDialogProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
public:
	SYSTEM_DLL static BOOL CALLBACK basePropsheetProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
protected:
	SYSTEM_DLL char* getItemIDText(int id);

	SYSTEM_DLL HWND createDialog(LPCTSTR dialName, HWND parent, Boolean showDialog = True);
	SYSTEM_DLL HWND createDialogIndirect(LPCDLGTEMPLATE dialog, HWND parent);

#ifdef DEBUG
	HWND CreateDialogParam(
		HINSTANCE  hInstance,
		LPCTSTR  lpTemplateName,
		HWND  hWndParent,
		DLGPROC  lpDialogFunc,
		LPARAM  dwInitParam)
		{
			FORCEASSERT("ModelessDialog::CreateDialogParam() called...\nUse createDialog instead\n");
			return NULL;
		}


	HWND CreateDialogIndirectParam(
		HINSTANCE  hInstance,
		LPCDLGTEMPLATE lpTemplateName,
		HWND  hWndParent,
		DLGPROC  lpDialogFunc,
		LPARAM  dwInitParam)
		{
			FORCEASSERT("ModelessDialog::CreateDialogIndirectParam() called...\nUse createDialogIndirect instead\n");
			return NULL;
		}
#endif

	/*
	 * Helper functions for combo controls
	 */

public:
	SYSTEM_DLL void initComboIDItems(int id, ComboInit* data);
public:
	SYSTEM_DLL void setComboIDValue(int id, int value);
protected:
	SYSTEM_DLL int getComboIDValue(int id);

	SYSTEM_DLL void setButtonIDCheck(int id, Boolean flag);
	SYSTEM_DLL Boolean getButtonIDCheck(int id);

	SYSTEM_DLL void setSpinIDValue(int id, int value);
	SYSTEM_DLL int getSpinIDValue(int id);

public:
	SYSTEM_DLL void enableIDControl(int id, BOOL flag);
	SYSTEM_DLL void showIDControl(int id, BOOL flag);
	SYSTEM_DLL void setIDText(int id, LPCTSTR text);
};

/*
 * Modal Dialog
 */

class ModalDialog : public ModelessDialog {
public:
	ModalDialog() { }
	virtual ~ModalDialog() { }

	SYSTEM_DLL int createDialog(LPCTSTR dlgName, HWND parent);
private:
	virtual BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) = 0;
};

/*
 * Support Functions
 */

int SYSTEM_DLL getComboValue(HWND hCombo);

void SYSTEM_DLL initComboItems(HWND dialog, int id, ComboInit* data);
void SYSTEM_DLL setComboValue(HWND dialog, int id, int value);
int SYSTEM_DLL getComboValue(HWND dialog, int id);
void SYSTEM_DLL setButtonCheck(HWND dialog, int id, Boolean flag);

void SYSTEM_DLL addComboItem(HWND hCombo, const char* name, int id);
void SYSTEM_DLL removeComboItems(HWND hCombo);

/*
 * Special Dialog box message cracker
 * Similar to HANDLE_MSG but does not return the result
 *
 * Usage:
 *		switch(msg)
 *		{
 *			HANDLE_DLG_MSG(WM_WHATEVER, function);
 *			default:
 *				return FALSE;
 *		}
 *		return TRUE;
 */

#define HANDLE_DLG_MSG(message, fn)    						\
    case (message):													\
	 HANDLE_##message((hWnd), (wParam), (lParam), (fn)); 	\
	 break;

#endif /* DIALOG_H */

