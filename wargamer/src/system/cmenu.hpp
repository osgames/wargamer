/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CMENU_HPP
#define CMENU_HPP

#include "sysdll.h"
#include "StringPtr.hpp"
#include "palwind.hpp"

#ifdef __WATCOM_CPLUSPLUS__
#include <string.hpp>
#else
#include <string>
typedef std::string String;
#endif

class DrawDIB;
class DrawDIBDC;
class ImageLibrary;
class MenuWindow;
class CustomMenu_Imp;
class PixelPoint;

/*----------------------------------------------------------------
 * structure for holding menu-item info
 *
 * Clients use this structure for setting / retrieving info
 * about a menuItem
 */

struct SYSTEM_DLL MenuItemInfo {
  enum State {
	 CMS_Checked   = 0x01,
	 CMS_Selected  = 0x02,
	 CMS_Enabled   = 0x04,
	 CMS_Seperator = 0x08
  };

  int d_id;
  UBYTE d_state;
  MenuWindow* d_subMenu; // if this item leads to a submenu
  // CString d_text;
  String d_text;
  RECT d_rect;           // coordinates of this item

  MenuItemInfo() :
	 d_id(-1),
	 d_state(CMS_Enabled),
	 d_subMenu(NULL)
  {
	 SetRect(&d_rect, 0, 0, 0, 0);
  }

  MenuItemInfo(const MenuItemInfo& mi);

  MenuItemInfo& operator = (const MenuItemInfo& mi);
};

/*-------------------------------------------------
 * Custom menu interface class
 */

struct SYSTEM_DLL CustomMenuData {
  enum Type {
		CMT_BarMenu,     // a main menu bar
		CMT_Popup,

#if 0
		CMT_BarTopSub,   // a main menu top-level sub menu
		CMT_PopupTop,    // a top-level pop-up menu (i.e. used with right-click pop-up menu's)
		CMT_PopupSub,    // a pop-up submenu
#endif
		CMT_HowMany
  } d_type;

  HWND d_hParent;
  HWND d_hCommand;      // this is the window that recieves the menu commands (may be different than parent)
  MenuWindow* d_tlMenu; // Menu window that opens this one
  int d_menuID;
  const DrawDIB* d_fillDib;
  const ImageLibrary* d_checkImages;
  HFONT d_hFont;       // normal font
  HFONT d_hULFont;     // underlined font
  CustomBorderInfo d_borderColors;
  COLORREF d_color;    // text color
  COLORREF d_hColor;   // text color for highlighted

  CustomMenuData() :
	 d_type(CMT_Popup),
	 d_hParent(0),
    d_tlMenu(0),
	 d_menuID(-1),
	 d_fillDib(0),
	 d_checkImages(0),
	 d_hFont(0),
	 d_hULFont(0)
  {
	 d_color = RGB(0, 0, 0);
	 d_hColor = RGB(255, 255, 255);
  }

};

class SYSTEM_DLL CustomMenu {
	 CustomMenu_Imp* d_menu;
  public:
	 CustomMenu();
	 ~CustomMenu();

	 // initialize
	 void init(CustomMenuData& data);
	 void init(CustomMenuData& data, HMENU hmenu);
	 void run(const PixelPoint& p);
    void hide();
	 void destroy();

	 Boolean enable(int id, Boolean set, Boolean byPosition = False);
	 Boolean checked(int id, Boolean set);

	 int width() const;
    int height() const;

	MenuWindow* getSubMenu(MenuWindow* h, int id);
	MenuWindow* createPopupMenu();
	static int getMenuItemCount(const MenuWindow* h);
	static BOOL insertMenuItem(MenuWindow* h, int i, BOOL f, const MenuItemInfo* info);

};


#endif
