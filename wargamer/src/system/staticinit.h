/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 * Order of Initialisation for static data
 */

#ifdef __WATCOM_CPLUSPLUS
#ifdef STATIC_INIT_WINFILE
#pragma initialize 33
#endif

#if defined(STATIC_INIT_DIB) || defined(STATIC_INIT_PALETTE) || defined(STATIC_INIT_FONT)
#pragma initialize 34
#endif
#endif   // watcom

/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */