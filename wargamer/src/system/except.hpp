/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef EXCEPT_HPP
#define EXCEPT_HPP

#ifndef __cplusplus
#error except.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Generic Exception
 *
 *----------------------------------------------------------------------
 */


#include "sysdll.h"
#include "mytypes.h"

class SYSTEM_DLL GeneralError {
    static Boolean doingError;
    static Boolean firstError;
    char *describe;
  public:
    GeneralError();
    GeneralError(const char* s, ...);

    const char* get() const;
  protected:
    void setMessage(const char* s);
};

/*
 * Virtual class that Application must provide an instance of
 *
 * Implemented in w95ext.cpp
 */

class SYSTEM_DLL ExceptionUtility {
   public:
     enum YesNo { Yes, No };		// Result of ask() function

     static void logError(const char* title, const char* text);
     // Add title and text to a log file

#if defined(DEBUG)
     static void logDebug(const char* text);
     // Add text to a debug File
#endif

     static YesNo ask(const char* title, const char* text);
     // Display title and text, and ask user if they want to quit
     // Return True to Quit, False to ignore

     static void alert(const char* title, const char* text);
     // Display Information to user and continue
 };

#endif /* EXCEPT_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
