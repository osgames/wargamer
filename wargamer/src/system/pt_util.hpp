/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PT_UTIL_HPP
#define PT_UTIL_HPP

#ifndef __cplusplus
#error pt_util.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Point Utilities
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"

template<class T>
inline Point<T> midPoint(const Point<T>& p1, const Point<T>& p2)
{
	return (p1 + p2) / 2;
}

template<class T>
inline Point<T> midPoint(const Point<T>& p1, const Point<T>& p2, int segment, int nSegments)
{
	return Point<T>( p1.x() + ((p2.x() - p1.x()) * segment) / nSegments,
						p1.y() + ((p2.y() - p1.y()) * segment) / nSegments);
}

/*
 * Return square of distance from point to line
 */

template<class T>
T pointFromLine(const Point<T>& e1, const Point<T>& e2, const Point<T>& p)
{
	T dy = e2.y() - e1.y();
	T dx = e2.x() - e1.x();

	if( (dx == 0) && (dy == 0) )		// Prevent divide by zero
	{
		return lineLength(e1, p);
	}

	T py = p.y() - e1.y();
	T px = p.x() - e1.x();

	// T result = abs((-dy * px) + (dx * py));
	T result = (-dy * px) + (dx * py);

#ifdef ROUND_UP_POINT_FROM_LINE
	T div = dx * dx + dy * dy;
	return (result * result + div/2) / div;
#else
	return (result * result) / (dx * dx + dy * dy);
#endif
}

template<class T>
T lineLength(const Point<T>& p1, const Point<T>& p2)
{
	T dx = p2.x() - p1.x();
	T dy = p2.y() - p1.y();

	if(dx < 0) dx = -dx;
	if(dy < 0) dy = -dy;

	if(dx > dy)
			return dx + dy / 2;
	else
			return dy + dx / 2;
}

#endif /* PT_UTIL_HPP */

