/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef MULDIV_HPP
#define MULDIV_HPP

/*
 * Multiply 2 numbers and divide by third, using intermediate 64 bit result
 */

#if 0
inline unsigned int UMulDiv(unsigned int a, unsigned int b, unsigned int c)
{
   _asm 
   {
      push edx
      mov eax,a
      imul b
      idiv c
      pop edx
   }
}   
#elif defined(__WATCOM_CPLUSPLUS__)
 unsigned int UMulDiv(unsigned int a, unsigned int b, unsigned int c);
 
 #pragma aux UMulDiv = \
    "imul edx"               \
    "idiv ecx"               \
   parm [eax] [edx] [ecx]  \
   modify [eax edx]

#elif 1

inline unsigned int UMulDiv(unsigned int a, unsigned int b, unsigned int c)
{
   unsigned __int64 m = a * b;
   return static_cast<unsigned int>(m / c);
   
}  
#elif 0
inline unsigned int UMulDiv(unsigned int a, unsigned int b, unsigned int c)
{
   return static_cast<unsigned int>(MulDiv(a,b,c));
}   

#elif 0

inline unsigned int UMulDiv(unsigned int a, unsigned int b, unsigned int c)
{
   ULONGLONG m = UInt32x32To64(a,b);
   return(m / c);
}   

#endif 

#endif   // MULDIV_HPP

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
