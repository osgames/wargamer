/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DIB_POLY_HPP
#define DIB_POLY_HPP

#ifndef __cplusplus
#error dib_poly.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Draw Filled Polygons into a DIB
 *
 * For now we just have a simple solid fill
 * Later on I might make it into a template that can be given a
 * drawing function, or make access to the scan line conversion
 * seperate.
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "dib.hpp"

namespace DIB_Utility
{


typedef int PolyPointIndex;

// This only included to simplify conversion of CivilWar code for wargamer

class SYSTEM_DLL Region
{
	public:
		Region(DrawDIB* dib);

		DrawDIB* dib() { return d_dib; }

		PixelDimension left() const { return d_rect.left(); }
		PixelDimension right() const { return d_rect.right(); }
		PixelDimension top() const { return d_rect.top(); }
		PixelDimension bottom() const { return d_rect.bottom(); }

	private:
		DrawDIB* d_dib;
		PixelRect d_rect;
};


class SYSTEM_DLL Polygon2D {
	PolyPointIndex 	nPoints;			// Number of points used
	bool 					pointsAlloced;	// Set if we created the points array
	PolyPointIndex 	maxPoints;		// Points allocated
	PixelPoint* 		points;			// Array of points
	ColourIndex			colour;			// Colour to use for straight colours

public:
				Polygon2D()
				{
					nPoints = 0;
					pointsAlloced = False;
					maxPoints = 0;
					colour = 0;
				}

				Polygon2D(PolyPointIndex nPoints, const PixelPoint* points = 0);
			  ~Polygon2D();

	void		add(const PixelPoint& p) { points[nPoints++] = p; };
	PolyPointIndex countPoints() const { return nPoints; }

	void		setColour(ColourIndex c) 		{ colour = c; }

	void clipPoly(Polygon2D* destPoly, const Region* bitmap) const;
	void render(Region* bm) const;
};


// Simple Solid Fill
SYSTEM_DLL void drawPoly(DrawDIB* dib, const PixelPoint* points, int nPoints, ColourIndex col);
SYSTEM_DLL void drawFilledTriangle(DrawDIB* dib, const PixelPoint* points, ColourIndex col);

};	// namespace DIB_Utility

#endif /* DIB_POLY_HPP */

