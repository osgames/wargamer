/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
#ifndef MMTIMER_HPP
#define MMTIMER_HPP

#ifndef __cplusplus
#error mmtimer.hpp is foruse with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Multi-Media Timer class
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "except.hpp"
#include "critical.hpp"
#include <mmsystem.h>


namespace Greenius_System
{

class Event;    // from sync.hpp

/*
 * Virtual base class for a timer
 *
 * Derived class must proved a proc() function.
 */

class MMTimer
{
   public:

      enum Mode
      { OneShot, Repeat
      };

      SYSTEM_DLL MMTimer(unsigned int ms, Mode repeat, bool autoStart, Event* event);
      // Create Timer with default of 100 milliseconds continuous
      SYSTEM_DLL virtual ~MMTimer();

      SYSTEM_DLL void start();
      SYSTEM_DLL void stop();

      SYSTEM_DLL void setRate(unsigned int ms);
      SYSTEM_DLL unsigned int getRate() const
      { return d_tickRate;
      }

      SYSTEM_DLL static DWORD getTime(void)
      { return timeGetTime();
      }


      virtual void proc() = 0;        // What to do in the interrupt function

      struct MMTimerError : public GeneralError
      {
         SYSTEM_DLL MMTimerError(const char* s) : GeneralError(s)
         {
         }
      };

   private:

      // void setPeriod(unsigned int ms);
      static void CALLBACK timerProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);

      /*
      * Data
      */

#ifdef DEBUG
      enum
      { ID = 'MMTM'
      } d_id;      // Check that this really is an MMTimer
#endif

      MMRESULT        d_timerID;
      UINT            d_tickRate;
      Mode            d_repeat;               // true for continuous, false for oneshot
      Event*          d_event;
      static unsigned int    s_resolution;
	  static int s_usage;
      static SharedData s_lock;

#if 0
      /*
      * Local static data used to coordinate timeBeginPeriod <-> timeEndPeriod
      */

      class TimePeriod
      {
         int d_usage;            // Count of how many instances there are
         UINT d_period;  // Value set in timeBeginPeriod
         public:
         TimePeriod();
         ~TimePeriod();

         void add(UINT ms);
         void remove();
      };

      static TimePeriod s_timePeriod;
      static SharedData s_lock;
#endif
};


};      // namespace Greenius_System


#endif /* MMTIMER_HPP */


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */

