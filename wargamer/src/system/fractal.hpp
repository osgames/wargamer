/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FRACTAL_HPP
#define FRACTAL_HPP

#ifndef __cplusplus
#error fractal.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Fractal Engine
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "gengine.hpp"
#include "mytypes.h"
// #include "random.hpp"

class SYSTEM_DLL FractalEngine : public GraphicEngine
{
	public:
		enum { BendRange = UBYTE_MAX + 1 };

	public:
		FractalEngine(GraphicEngine& output, UBYTE bendiness, int resolution);
		~FractalEngine();

		// Graphic Engine Implementation

		void addPoint(const GPoint& point);
		void close();
		// void setPen(int style, int width, COLORREF col) { d_output.setPen(style, width, col); }

		void setClip(const GRect& r);
		void setValues(UBYTE bendiness, int resolution);

	private:
		void doFractal(const GPoint& p1, const GPoint& p2);

	private:
		GPoint d_lastPoint;
		int d_nPoints;
		GraphicEngine& d_output;
		// RandomNumber d_rand;
		UBYTE d_bendiness;		// 0=straight, 255=very bendy
		int d_resolution;
		GRect d_clipRect;
		bool d_pointAdded;
};

#endif /* FRACTAL_HPP */

