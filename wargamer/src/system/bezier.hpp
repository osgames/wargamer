/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BEZIER_HPP
#define BEZIER_HPP

#ifndef __cplusplus
#error bezier.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Bezier Pipeline component
 *
 * It is given points one at a time and outputs points.
 *
 * This version will use only 1 control point per segment
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "gengine.hpp"


class SYSTEM_DLL BezierEngine : public GraphicEngine
{
	public:
		BezierEngine(GraphicEngine& output);
		~BezierEngine();

		void addPoint(const GPoint& point);
		void close();

	private:
		void doBezier(const GPoint* points, int nPoints);

	private:
		GPoint d_points[3];
		int d_nPoints;

		GraphicEngine& d_output;
};




#if 0
class BezierEngine3 : public GraphicEngine
{
	public:
		BezierEngine3(GraphicEngine& output);
		~BezierEngine3();

		void addPoint(const GPoint& point);
		void close();
		void setPen(int style, int width, COLORREF col) { d_output.setPen(style, width, col); }

	private:
		void doBezier(const GPoint* points, int nPoints);

	private:
		GPoint d_points[3];
		int d_nPoints;

		GraphicEngine& d_output;
};

class BezierEngine3a : public GraphicEngine
{
	public:
		BezierEngine3a(GraphicEngine& output);
		~BezierEngine3a();

		void addPoint(const GPoint& point);
		void close();
		void setPen(int style, int width, COLORREF col) { d_output.setPen(style, width, col); }

	private:
		void doBezier(const GPoint* points, int nPoints);

	private:
		GPoint d_points[3];
		int d_nPoints;

		GraphicEngine& d_output;
};



class BezierEngine4 : public GraphicEngine
{
	public:
		BezierEngine4(GraphicEngine& output);
		~BezierEngine4();

		void addPoint(const GPoint& point);
		void close();
		void setPen(int style, int width, COLORREF col) { d_output.setPen(style, width, col); }

	private:
		void doBezier(const GPoint* points, int nPoints);

	private:
		GPoint d_points[4];
		int d_nPoints;

		GraphicEngine& d_output;
};

/*
 * Parametric Version
 */

class BezierEngine3P : public GraphicEngine
{
	public:
		BezierEngine3P(GraphicEngine& output);
		~BezierEngine3P();

		void addPoint(const GPoint& point);
		void close();
		void setPen(int style, int width, COLORREF col) { d_output.setPen(style, width, col); }

	private:
		void doBezier(const GPoint* points, int nPoints);

	private:
		GPoint d_points[3];
		int d_nPoints;

		GraphicEngine& d_output;
};


/*
 * Thru engine should be given a Bezier3 as an engine
 */

class BezierEngineThru : public GraphicEngine
{
	public:
		BezierEngineThru(GraphicEngine& output);
		~BezierEngineThru();

		void addPoint(const GPoint& point);
		void close();
		void setPen(int style, int width, COLORREF col) { d_bezier.setPen(style, width, col); }

	private:
		GPoint d_points[2];
		int d_nPoints;

		BezierEngine3a d_bezier;
};

#endif

#endif /* BEZIER_HPP */

