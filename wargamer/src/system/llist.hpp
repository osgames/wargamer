/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef LLIST_H
#define LLIST_H

#ifndef __cplusplus
#error llist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Template for simple intrusive linked List
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  1996/02/28 09:33:33  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"


class SINode {
	SINode* next;
	friend class SIListBase;
};

class SYSTEM_DLL SIListBase {
	SINode* first;	
	SINode* last;	
public:
	SIListBase()
	{
		first = 0;
		last = 0;
	}

	~SIListBase()
	{
	}

	void append(SINode* node)
	{
		node->next = 0;
		if(last)
			last->next = node;
		else
			first = node;
		last = node;
	}

	void insert(SINode* node)
	{
		if(last == 0)
			last = node;
		node->next = first;
		first = node;
	}

	SINode* get()
	{
		SINode* node = first;
		if(node)
		{
			first = node->next;
			if(first == 0)
				last = 0;
		}
		return node;
	}

	Boolean isEmpty() const { return (first == 0); }
};



template<class T>
class SIList : public SIListBase {
public:
	void append(T* node) { SIListBase::append(node); }
	void insert(SINode* node) { SIListBase::insert(node); }
	T* get() { return (T*) SIListBase::get(); }

};





#endif /* LLIST_H */

