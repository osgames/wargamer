/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "fillwind.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "palette.hpp"

/*---------------------------------------------------
 * A Filled window
 * Actually it holds a static dib and fills it with a given background.
 */

// int FillWindow::s_instanceCount = 0;
// static DCObject* FillWindow::s_dc = 0;

FillWindow::FillWindow() :
	d_dib(0), // Unchecked added by Paul
	d_fillDib(0),
	d_shadowCX(0),
   d_needRedraw(true)
{
	d_size.cx = 0;
	d_size.cy = 0;

//	s_instanceCount++;
}

FillWindow::~FillWindow()
{
// 	ASSERT((s_instanceCount - 1) >= 0);
//
// 	if(--s_instanceCount == 0)
// 	{
// 	 if(s_dc)
// 		delete s_dc;
//
// 	 s_dc = 0;
// 	}

	if(d_dib)
        delete d_dib;
}

void FillWindow::allocateDib(int cx, int cy)
{
	d_size.cx = cx;
	d_size.cy = cy;
    d_needRedraw = true;
}


void FillWindow::setFillDib(const DrawDIB* fDib)
{
	d_fillDib = fDib;
    d_needRedraw = true;
}


void FillWindow::makeDIB()
{
	if(d_dib == 0 ||
		 d_dib->getWidth() < d_size.cx ||
		 d_dib->getHeight() < d_size.cy)
	{
		if(d_dib)
         d_dib->resize(d_size.cx, d_size.cy);
      else
   		d_dib = new DrawDIBDC(d_size.cx, d_size.cy);
		ASSERT(d_dib);
	}

// 	if(!s_dc)
// 	{
// 		s_dc = new DCObject;
// 		ASSERT(s_dc);
// 	}

	if(d_fillDib)  // && s_dc)
	{
		RECT r;
		SetRect(&r, 0, 0, d_size.cx - d_shadowCX, d_size.cy - d_shadowCX);

		d_dib->rect(0, 0, r.right, r.bottom, d_fillDib);

		// HBITMAP oldBitmap = (HBITMAP)s_dc->selectObject(d_dib->getHandle());

		// draw border
		CustomBorderWindow bw(d_borderColors);
		bw.drawThinBorder(d_dib->getDC(), r);

		// s_dc->selectObject(oldBitmap);
	}

    d_needRedraw = false;
}

void FillWindow::paint(HDC hdc, const RECT& ur)
{
    if(d_needRedraw)
        makeDIB();

	if(d_dib)   // && s_dc)
	{
		HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
		RealizePalette(hdc);
		// HBITMAP oldBitmap = (HBITMAP)s_dc->selectObject(d_dib->getHandle());

		// draw shadow
		if(d_shadowCX > 0)
		{
			// get right side
			RECT r;
			SetRect(&r, 0, 0, d_size.cx - d_shadowCX, d_size.cy - d_shadowCX);


			if(r.right <= ur.right)
			{
				BitBlt(d_dib->getDC(), r.right, 0, d_shadowCX, d_size.cy,
					hdc, r.right, 0, SRCCOPY);

				// darken right side
				d_dib->darkenRectangle(r.right, d_shadowCX, d_shadowCX, r.bottom, 50);
			}

			if(r.bottom <= ur.bottom)
			{
				// get bottom
				BitBlt(d_dib->getDC(), 0, r.bottom, d_size.cx, d_shadowCX,
					hdc, 0, r.bottom, SRCCOPY);

				// darken bottom
				d_dib->darkenRectangle(d_shadowCX, r.bottom, r.right - d_shadowCX, d_shadowCX, 50);
			}
		}

		BitBlt(hdc, ur.left, ur.top, ur.right - ur.left, ur.bottom - ur.top,
			d_dib->getDC(), ur.left, ur.top, SRCCOPY);

		// s_dc->selectObject(oldBitmap);
		SelectPalette(hdc, oldPal, FALSE);
	}
}

void FillWindow::paint(HDC hdc)
{
	// if(d_dib && s_dc)
    if(d_size.cx && d_size.cy)
	{
		RECT r;
		SetRect(&r, 0, 0, d_size.cx, d_size.cy);
		paint(hdc, r);
	}
}
