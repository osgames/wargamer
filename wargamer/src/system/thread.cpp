/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Thread handler
 *
 * Notes:
 *   Because of the way in which the names of Event and Mutex are
 *   created, there will be a problem if there are several instances
 *   the program running at once!
 *
 *   To resolve this, we could have an application name, that is set
 *   during the application's initialisation, based on how many other
 *   copies of the program are already running.  Or we could just
 *   embed a represenation of HINSTANCE into the name?
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "thread.hpp"
#include "memptr.hpp"

#ifdef __WATCOMC__
#include <process.h>		// for _beginthreadex
#endif

#include "myassert.hpp"

namespace Greenius_System
{

#undef DEBUG_THREAD
// #define DEBUG_THREAD
// #define DEBUG_EVENT

const unsigned int ThreadStackSize = 0x8000;

Thread::Thread(ThreadManager* tManager) :
  d_wantPause(),
  d_wantQuit(),
  d_isPaused(),
  d_pauseCount(0),
  d_running(false),
#ifdef DEBUG
  magic(Magic),
#endif
  d_tManager(tManager)
{
  //lint -save -e1506 ... handle() is a virtual function
#ifdef __WATCOMC__
  //lint -e(64) -e(511) ... warnings about threadProc type mismatch
  handle( (HANDLE) _beginthreadex(NULL, ThreadStackSize, threadProc, this, CREATE_SUSPENDED, &idThread));
#else
  handle( CreateThread(NULL, 0, threadProc, this, CREATE_SUSPENDED, &idThread));
#endif

  ASSERT(handle() != NULL);

  if(handle() == NULL)
    throw ThreadCantCreate();

#ifdef DEBUG_THREAD
  debugLog("Thread handle=%p, id=%ld\n", (void*) handle(), (long) idThread);
#endif
  //lint -restore
}

Thread::~Thread()
{
  //lint -save -e1506 ... handle() is a virtual function
#ifdef DEBUG_THREAD
  debugLog("Thread Deleted, handle=%p, id=%ld\n", (void*) handle(), (long) idThread);
#endif

  if(handle() != NULL)
  {
    // Exit properly

    if(d_running)
    {
      initExit();
#ifdef DEBUG
      debugLog("~Thread(): waiting for thread to finish\n");
#endif
      DWORD result = WaitForSingleObject(handle(), TimeOutMs);
#ifdef DEBUG
      debugLog("~Thread(): result=%d\n", (int) result);
#endif
      ASSERT(result == WAIT_OBJECT_0);

      if(result != WAIT_OBJECT_0)
      {
        TerminateThread(handle(), 0);

        d_running = false;
        if(d_tManager)
        {
          d_tManager->remove(this);
          d_tManager = 0;
        }
      }
    }

    idThread = 0;
    // finish();
  }
  //lint -restore
}

void Thread::start()
{
#ifdef DEBUG_THREAD
  debugLog("Thread started, handle=%p, id=%ld\n", (void*) handle(), (long) idThread);
#endif

  ASSERT(!d_running);
  ASSERT(handle() != NULL);

  // if(d_tManager)
  // {
  //     d_pauseCount = d_tManager->pauseCount();
  //     if(d_pauseCount)
  //     {
  // 		d_isPaused.set();
  //     }
  // }

	d_isPaused.reset();
  if(ResumeThread(handle()) == 0xFFFFFFFF)
  {
    throw ThreadCantStart();
  }

  // Wait for thread to start

  DWORD result = d_isPaused.wait(TimeOutMs);	// 20 seconds
#ifdef DEBUG
  debugLog("Thread::waited for thread to start: result = %d\n", (int) result);
#endif
  ASSERT(result == WAIT_OBJECT_0);
  if(result != WAIT_OBJECT_0)
    throw ThreadCantStart();
  d_isPaused.reset();
  d_wantPause.set();  // Tell thread we got its signal


  if(d_tManager)
  {
    d_tManager->add(this);
  }

}

void Thread::setPriority(Priority priority)
{
#ifdef DEBUG
  debugLog("Thread (%p), setting priority set to %d\n", (void*) handle(), (int) priority);
#endif

  ASSERT(handle() != NULL);
  BOOL result = SetThreadPriority(handle(), priority);
  ASSERT(result);
  if(result == 0)
    throw ThreadBadPriority();
}

ThreadReturn WINAPI Thread::threadProc(LPVOID param)
{
  //lint -save -e613 ... possible use of null pointer
  ASSERT(param != 0);

  Thread* thread = static_cast<Thread*>(param);

#ifdef DEBUG
  ASSERT(thread->isAThread());
#endif

  try
  {
    thread->d_running = true;
    thread->d_isPaused.set();  // let starting thread know we've started
    thread->d_wantPause.wait(); // Wait for starter to acknowledge
    thread->run();
  }
  catch(const WaitValue& value)
  {
#ifdef DEBUG
    static const char* waitNames[] = {
      "TWV_Quit",
      "TWV_Event",
      "TWV_TimeOut"
    };

    const char* name = waitNames[value];

    debugLog("thread (%p), caught exception WaitValue %s\n",
             (void*) thread->handle(),
             name);
#endif
  }
  catch(const GeneralError& e)
  {
    // gApp.pauseGame(TRUE);
    MessageBox(0, "Thread: General Error Exception\n", e.get(), MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
    PostQuitMessage(0);
  }
  catch(...)
  {
#ifdef DEBUG
    debugLog("thread (%p), caught unknown exception\n", (void*) thread->handle());
#endif
    MessageBox(0, "Thread: Exception", "Unknown", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
    PostQuitMessage(0);
  }

  /*
   * If we terminate, then I guess we should clear the handle
   */

  // No we shouldn't!  Other bits of code may refer to it.

#ifdef DEBUG
  debugLog("Thread (%p), has finished\n", (void*) thread->handle());
#endif

  // thread->finish();

  LockData lock(thread);

  ASSERT(thread->d_running);

  thread->d_running = false;
  thread->d_isPaused.set();        // In case pause was caled while thread was exitting

  if(thread->d_tManager)
    thread->d_tManager->remove(thread);

  return 0;
  //lint -restore
}


void Thread::pause(Boolean pauseMode)
{
#ifdef DEBUG
  debugLog("Thread::pause(%p, %s)\n", (void*) handle(), pauseMode ? "True" : "False");
#endif

//   ASSERT(d_running);

  if (d_running)
  {
    if(pauseMode)
    {
      initPause();
#ifdef DEBUG
      debugLog("Thread::pause(): waiting for isPaused\n");
#endif
      DWORD result = d_isPaused.wait(TimeOutMs);	// 20 seconds
#ifdef DEBUG
      debugLog("Thread::pause(): result = %d\n", (int) result);
#endif
      ASSERT(result == WAIT_OBJECT_0);
    }
    else
    {
      // If pauseCount==0, then thread was probably created during the previous pause

      if(d_pauseCount != 0)
      {
        ASSERT(d_pauseCount > 0);

        LockData lock(this);

        // Special case for unpausing ourself!

        if(GetCurrentThreadId() == idThread)
        {
          ASSERT(d_pauseCount == 1);
          d_isPaused.reset();
          d_pauseCount--;
        }
        else if(--d_pauseCount == 0)
        {
          d_wantPause.set();
        }
      }
    }
  }
}

void Thread::initPause()
{
#ifdef DEBUG
  debugLog("Thread::initPause(%p)\n", (void*) handle());
#endif

  // ASSERT(d_running);

  LockData lock(this);

  if (d_running)
  {

    /*
     * If we are trying to pause ourselves, then we need
     * to ignore it, but still signal the ThreadManager
     */

    if(GetCurrentThreadId() == idThread)
    {
#ifdef DEBUG
      debugLog("Thread::initPause(%p): Pausing self (pauseCount=%d)\n", (void*) handle(), (int) d_pauseCount);
#endif

      // Should not be possible to get here if we are already paused!

      ASSERT(d_pauseCount == 0);

      d_pauseCount++;
      d_isPaused.set();		// Keep the caller happy
    }
    else
    {
      if(d_pauseCount++ == 0)
        d_wantPause.set();
    }
  }
}

void Thread::initExit()
{
#ifdef DEBUG
  debugLog("Thread::initExit(%p)\n", (void*) handle());
#endif

  if(d_running)
    d_wantQuit.set();
}

void Thread::stop()
{
  if(d_running)
  {
    initExit();
    DWORD result = WaitForSingleObject(handle(), TimeOutMs);
#ifdef DEBUG
    debugLog("Thread::stop(): result=%d\n", (int) result);
#endif
    ASSERT(result == WAIT_OBJECT_0);
  }
}

Thread::WaitValue Thread::wait(SyncObject& syncOb, DWORD ms)
{
  ASSERT(d_running);

  enum
      {
        TW_First = 0,
        TW_Pause	= TW_First,
        TW_Exit,
        TW_Event,
        TW_HowMany
      };

  HANDLE hArray[TW_HowMany];
  hArray[TW_Event] = syncOb.handle();
  hArray[TW_Pause] = d_wantPause.handle();
  hArray[TW_Exit]  = d_wantQuit.handle();

  for(;;)
  {
    DWORD result = MsgWaitForMultipleObjects(TW_HowMany, hArray, FALSE, ms, QS_ALLINPUT);
    // DWORD result = WaitForMultipleObjectsEx(TW_HowMany, hArray, FALSE, ms, FALSE);

    if(result == WAIT_TIMEOUT)
      return TWV_TimeOut;
    else if(result == (WAIT_OBJECT_0 + TW_Event - TW_First))
      return TWV_Event;
    else if(result == (WAIT_OBJECT_0 + TW_Exit - TW_First))
    {
#ifdef DEBUG
      debugLog("Thread(%p) wantQuit was set\n", (void*) handle());
#endif
      throw TWV_Quit;
      // return TWV_Quit;
    }
    else if(result == (WAIT_OBJECT_0 + TW_Pause - TW_First))
    {
      // into pause mode...
      d_isPaused.set();

#ifdef DEBUG
      debugLog("Thread(%p) in pause mode\n", (void*) handle());
#endif

      // Wait for another Pause signal

      result = WaitForMultipleObjectsEx(2, hArray, FALSE, INFINITE, FALSE);

#ifdef DEBUG
      debugLog("Thread(%p) leaving pause mode, result=%d\n", (void*) handle(), (int) result);
#endif
      if(result == (WAIT_OBJECT_0 + TW_Exit - TW_First))
      {
#ifdef DEBUG
        debugLog("Thread(%p) wantQuit was set\n", (void*) handle());
#endif
        throw TWV_Quit;
        // return TWV_Quit;
      }

      // Loop round again.
      // paused signal is an autoevent, so will be cleared

      d_isPaused.reset();
    }
    else if(result == (WAIT_OBJECT_0 + TW_HowMany))
    {
      MSG msg ;

      // read all of the messages in this next loop
      // removing each message as we read it
      while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
      {

        // if it's a quit message we're out of here
        // if (msg.message == WM_QUIT)  return 1;

        // otherwise dispatch it
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }

    }
#ifdef DEBUG
    else
      FORCEASSERT("Unknown result from WaitForMultipleObjectsEx\n");
#endif
  }
}

/*==================================================================
 * Thread manager functions
 */

#ifdef DEBUG
int ThreadManager::s_instanceCount = 0;
#endif

ThreadManager::ThreadManager() :
  d_threadList(),
  d_lock()
{
#ifdef DEBUG
  ASSERT(s_instanceCount < MaxInstances);
  ++s_instanceCount;
  debugLog("ThreadManager::ThreadManager()\n");
#endif
}

ThreadManager::~ThreadManager()
{
#ifdef DEBUG
  ASSERT(s_instanceCount > 0);
  --s_instanceCount;
  debugLog("ThreadManager::~ThreadManager()\n");
#endif

  ASSERT(d_threadList.entries() == 0);	// they should have all been removed
}

void ThreadManager::add(Thread* thread)
{
#ifdef DEBUG
  debugLog("ThreadManager::add(%p)\n", (void*) thread->handle());
#endif


  ASSERT(!d_threadList.isMember(thread));

  d_lock.startWrite();
  d_threadList.append(thread);
  d_lock.endWrite();

#ifdef DEBUG
  debugLog("ThreadManager::append(%p) : contains %d entries\n",
           (void*) thread->handle(),
           (int) d_threadList.entries());
#endif
}

void ThreadManager::remove(Thread* thread)
{
#ifdef DEBUG
  debugLog("ThreadManager::remove(%p)\n", (void*) thread->handle());
#endif

  ASSERT(d_threadList.isMember(thread));

  d_lock.startWrite();
  d_threadList.remove(thread);
  d_lock.endWrite();

#ifdef DEBUG
  debugLog("ThreadManager::remove(%p) : contains %d entries\n",
           (void*) thread->handle(),
           (int) d_threadList.entries());
#endif
}

void ThreadManager::pause()
{
#ifdef DEBUG
  debugLog("ThreadManager::pause()\n");
#endif

  // ++d_pauseCount;

  if(d_threadList.entries())
  {
    MemPtr<HANDLE> handleList(d_threadList.entries());
    int n = 0;

    d_lock.startRead();

    for(PtrDListIter<Thread> iter(d_threadList);
        ++iter;
        n++)
    {
      Thread* thread = iter.current();

      handleList[n] = thread->pausedHandle().handle();
      thread->initPause();
    }

    ASSERT(n == d_threadList.entries());

    d_lock.endRead();

    // Wait for all signals

    const DWORD TimeOut = Thread::TimeOutMs;  // 20 * 1000;		// 20 seconds maximum wait!

#ifdef DEBUG
    debugLog("ThreadManager::pause(): waiting for threads to signal that they have paused\n");
#endif

    DWORD result = WaitForMultipleObjectsEx(n, handleList.get(), TRUE, TimeOut, FALSE);

#ifdef DEBUG
    debugLog("ThreadManager::pause(): result = %d\n", (int) result);
#endif

    ASSERT(result != WAIT_TIMEOUT);

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 354 9		// Disable unsigned result always >= 0
#endif
    ASSERT( (result >= WAIT_OBJECT_0) && (result < (WAIT_OBJECT_0 + n) ) );
#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 354 3
#endif
  }

}

void ThreadManager::unpause()
{
#ifdef DEBUG
  debugLog("ThreadManager::unpause()\n");
#endif

  PtrDListIter<Thread> iter(d_threadList);
  while(++iter)
  {
    Thread* thread = iter.current();
    thread->pause(False);
  }

  // --d_pauseCount;
}

void ThreadManager::terminate()
{
#ifdef DEBUG
  debugLog("ThreadManager::terminate()\n");
#endif

  if(d_threadList.entries())
  {
    MemPtr<HANDLE> handleList(d_threadList.entries());
    int n = 0;

    for(PtrDListIter<Thread> iter(d_threadList);
        ++iter;
        n++)
    {
      Thread* thread = iter.current();

      handleList[n] = thread->exittedHandle().handle();
      thread->initExit();

    }

    ASSERT(n == d_threadList.entries());

    // Wait for all signals

    const DWORD TimeOut = Thread::TimeOutMs; // 20 * 1000;		// 20 seconds maximum wait!

#ifdef DEBUG
    debugLog("ThreadManager::terminate(): waiting for threads to signal that they have exitted\n");
#endif

    DWORD result = WaitForMultipleObjectsEx(n, handleList.get(), TRUE, TimeOut, FALSE);

#ifdef DEBUG
    debugLog("ThreadManager::terminate(): result = %d\n", (int) result);
#endif

    ASSERT(result != WAIT_TIMEOUT);

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 354 9		// Disable unsigned result always >= 0
#endif
    ASSERT( (result >= WAIT_OBJECT_0) && (result < (WAIT_OBJECT_0 + n) ));
#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 354 3
#endif
  }
}



}	// namespace Greenius_System

/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */
