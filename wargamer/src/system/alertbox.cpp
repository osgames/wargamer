/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Alert Box
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "alertbox.hpp"

#include "misc.hpp"
#include "app.hpp"
#include "resdef.h"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "winctrl.hpp"
#include "resstr.hpp"
#include "scrnbase.hpp"

// #if !defined(EDITOR)
#ifdef DEBUG
#include "logwin.hpp"
#endif
// #endif	// !EDITOR


#define DISABLE_WINDOW  100

class ToggleWindow {
  protected:
	 BOOL display;
  public:
	 ToggleWindow() {display = TRUE;}
};

class AlertBox : public ModelessDialog, public ToggleWindow {
	 HWND parent;
	 char* text;
	 char* caption;
	 UINT type;
//	 char** buttonText;
//	 int* id;
	 int nButtons;
	 int* result;
  public:
	 AlertBox(HWND h);
	 ~AlertBox();
	 long displayAlertBox(LPCTSTR lpText, LPCTSTR lpCaption,	UINT uType);
  private:
	 BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	 void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	 void onPaint(HWND hWnd);
	 void onDestroy(HWND hWnd);
	 void onClose(HWND hWnd);

};

AlertBox::AlertBox(HWND h)
{
  parent = h;
  text = 0;
  caption = 0;
  type = -1;
}

AlertBox::~AlertBox()
{

  if(text)
  {
	 delete text;
	 text = 0;
  }
  if(caption)
  {
	 delete caption;
	 caption = 0;
  }

}

long AlertBox::displayAlertBox(LPCTSTR lpText, LPCTSTR lpCaption,	UINT uType)
{
  text = copyString(lpText);
  caption = copyString(lpCaption);
  type = uType;
  int result = DialogBoxParam(APP::instance(), alertBoxDlgName, parent, baseModalDialogProc, (LPARAM)(AlertBox*)this);
  return MAKELONG(result, display);
}

BOOL AlertBox::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

//	AlertBox* ab = (AlertBox*)lParam;
//	ASSERT(ab != 0);

	switch(msg)
	{
		case WM_INITDIALOG:
			HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
			break;

		case WM_DESTROY:
			HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
			break;
		case WM_COMMAND:
			HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
			break;
		case WM_CLOSE:
			HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
			break;
		case WM_PAINT:
			HANDLE_WM_CLOSE(hWnd, wParam, lParam, onPaint);
			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL AlertBox::onInitDialog(HWND hWnd, HWND hwndFocus, LPARAM lParam)
{
#if 0
  ASSERT(text != 0);
  HDC hdc = GetDC(hWnd);
  Fonts font;
  font.setFont(hdc, (dbY * 7) / 8, (dbX * 3) / 4);		// Was 14, 5
  int x = (dbX * 10) / 4;		// Was 28;
  Size s;
  GetTextExtentPoint32(hdc, text, lstrlen(text), &s);
#endif

   const char* buttonText[3];
   int id[3];

  switch(type)
  {
	 case MB_OK:
		nButtons = 1;
		// buttonText = new char*[nButtons];
		// ASSERT(buttonText != 0);
		// buttonText[0] = copyString(InGameText::get(IDS_OK));
		buttonText[0] = InGameText::get(IDS_OK);

//		id = new int[nButtons];
//		ASSERT(id != 0);
		id[0] = IDOK;
		break;

	 case MB_OKCANCEL:
		nButtons = 2;
		// buttonText = new char*[nButtons];
		// ASSERT(buttonText != 0);
		buttonText[0] = InGameText::get(IDS_OK);
		buttonText[1] = InGameText::get(IDS_Cancel);

//		id = new int[nButtons];
//		ASSERT(id != 0);
		id[0] = IDOK;
		id[1] = IDCANCEL;
		break;

	 case MB_YESNO:
		nButtons = 2;
		// buttonText = new char*[nButtons];
		// ASSERT(buttonText != 0);
		buttonText[0] = InGameText::get(IDS_Yes);
		buttonText[1] = InGameText::get(IDS_No);

//		id = new int[nButtons];
//		ASSERT(id != 0);
		id[0] = IDYES;
		id[1] = IDNO;
		break;

	 case MB_YESNOCANCEL:
		nButtons = 3;
		// buttonText = new char*[nButtons];
		// ASSERT(buttonText != 0);
		buttonText[0] = InGameText::get(IDS_Yes);
		buttonText[1] = InGameText::get(IDS_No);
		buttonText[2] = InGameText::get(IDS_Cancel);

//		id = new int[nButtons];
//		ASSERT(id != 0);
		id[0] = IDYES;
		id[1] = IDNO;
		id[2] = IDCANCEL;
		break;

	 default:
		nButtons = 0;
  }

//  int buttonW = 60;
//  int buttonH = 25;

//  LONG dbUnits = GetDialogBaseUnits();
//  LONG dbX = LOWORD(dbUnits);
//  LONG dbY = HIWORD(dbUnits);


  RECT r;
  GetClientRect(hWnd, &r);

//  int buttonW = (dbX * 24) / 4;
//  int buttonW = ScreenBase::x(40);     // (dbX * 32) / 4;
   int xSpace = ScreenBase::x(4);
   int buttonW = (r.right - r.left - xSpace) / 3 - xSpace;
   buttonW = minimum(buttonW, ScreenBase::x(40));
   int buttonH = ScreenBase::y(10);     // (dbY * 10) / 8;
//  int y = ScreenBase::y(40);           // (dbY * 40) / 8;
   int xDist = buttonW + xSpace;


  int y = r.bottom - buttonH - ScreenBase::y(4) - GetSystemMetrics(SM_CYCAPTION);

//  int startX = (r.right / (nButtons + 1)) - (buttonW / 2);  // for buttons
  int startX = r.right - xSpace - (nButtons * xDist);

  HWND hButton[3];

  for(int i = 0; i < nButtons; i++)
  {
	 hButton[i] = CreateWindow("BUTTON", buttonText[i],
		  BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE | WS_TABSTOP,
		  // (startX * (i+1)) + ((buttonW / 2) * i),
        startX + i * xDist,
		  y,
		  buttonW,
		  buttonH,
		  hWnd,
		  (HMENU)id[i],
		  wndInstance(hWnd),
		  NULL);
	 ASSERT(hButton[i] != 0);
  }

#if 0
  HWND hInactivate = CreateWindow("BUTTON", buttonText[i],
		BS_PUSHBUTTON | WS_CHILD | WS_TABSTOP,
		150, 5, buttonW, buttonH, hWnd, (HMENU)DISABLE_WINDOW,
		wndInstance(hWnd), NULL);
  ASSERT(hInactivate != 0);
#endif


  int w = GetSystemMetrics(SM_CXSCREEN);
  int h = GetSystemMetrics(SM_CYSCREEN);
  MoveWindow(hWnd, (w / 2) - (r.right/2), (h / 2) - (r.bottom/2), r.right, r.bottom, True);

  ASSERT(caption != 0);
  SetWindowText(hWnd, caption);
  return TRUE;
}

void AlertBox::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
	 case IDYES:
	 case IDNO:
	 case IDOK:
	 case IDCANCEL:
		EndDialog(hWnd, id);
		break;
	 case AB_DISABLE:
	 {
		if(codeNotify == BN_CLICKED)
		{
		  Button button(hWnd, id);
		  display = !button.getCheck();
// #if !defined(EDITOR)
#ifdef DEBUG
		  g_logWindow.printf("AlertBox::disabled = %d", (int)display);
#endif
// #endif
		}
	 }
  }

}


void AlertBox::onPaint(HWND hWnd)
{
//  LONG dbUnits = GetDialogBaseUnits();
//  LONG dbX = LOWORD(dbUnits);
//  LONG dbY = HIWORD(dbUnits);

  PAINTSTRUCT ps;

  HDC hdc = BeginPaint(hWnd, &ps);
  ASSERT(hdc != 0);

  Fonts font;
//  font.setFont(hdc, (dbY * 7) / 8, (dbX * 3) / 4);		// Was 14, 5
  font.setFont(hdc, ScreenBase::y(8), 0); // ScreenBase::x(4));		// Was 7,3
//  int x = ScreenBase::x(5);
//  int y = ScreenBase::y(20);
//  int w = ScreenBase::x(110);
//  int h = ScreenBase::y(40);
//  int dy = ScreenBase::y(7);
  RECT r;
  GetClientRect(hWnd, &r);
  r.left += ScreenBase::x(5);
  r.right -= ScreenBase::x(5);
  r.top += ScreenBase::y(6+9+6);    // 20);
  r.bottom -= ScreenBase::y(18);

  RECT r1 = r;
  DrawText(hdc, text, -1, &r1, DT_WORDBREAK | DT_CALCRECT);
  r.top += ((r.bottom - r.top) - (r1.bottom - r1.top)) / 3;
//   r.top = (r.bottom + r.top + r1.top - r1.bottom) / 2;
   r.bottom = r.top + r1.bottom - r1.top;

//  fillRECT(r, x, y, w, h);
  SetBkMode(hdc, TRANSPARENT);
  ASSERT(text != 0);
  DrawText(hdc, text, -1, &r, DT_WORDBREAK);
  font.deleteFont();
  EndPaint(hWnd, &ps);
}


void AlertBox::onDestroy(HWND hWnd)
{
}

void AlertBox::onClose(HWND hWnd)
{
  EndDialog(hWnd, IDCANCEL);
}

long
alertBox(HWND h, LPCTSTR lpText, LPCTSTR lpCaption,	UINT uType)
{
  AlertBox ab(h);
  return ab.displayAlertBox(lpText, lpCaption, uType);
}
