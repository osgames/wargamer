/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MYASSERT_H
#define MYASSERT_H

#ifndef __cplusplus
#error myassert.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	My Assert Macro
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <boost/static_assert.hpp>

#ifdef DEBUG

#include "mytypes.h"
#include "except.hpp"

extern void SYSTEM_DLL _myAssert(const char* expr, const char* file, int line);
extern void SYSTEM_DLL _myDebugMessage(const char* fmt, ...);
extern void SYSTEM_DLL _setDebugInfo(char* file, int line);

extern void SYSTEM_DLL debugLog(const char* fmt, ...);

#define ASSERT(expr)	\
	if(expr)		\
	{				\
	}				\
	else			\
		_myAssert(#expr, __FILE__,__LINE__)

#define FORCEASSERT(s) _myAssert(s, __FILE__, __LINE__)

#define debugMessage _setDebugInfo(__FILE__, __LINE__); _myDebugMessage

void SYSTEM_DLL alert(const char* title, const char* fmt, ...);

class SYSTEM_DLL AssertError : public GeneralError {
  public:
    AssertError(const char* buf);
};

#define IFDEBUG(s) s

#else

#define ASSERT(expr) ((void)0)
#define FORCEASSERT(s) ((void)0)
#define IFDEBUG(s) { }

inline void debugMessage(const char* fmt, ...) { }
inline void debugLog(const char* fmt, ...) { }
inline void alert(const char* title, const char* fmt, ...) { }


#endif

#define StaticAssert(b) BOOST_STATIC_ASSERT(b)


#endif /* MYASSERT_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2003/02/08 19:32:57  greenius
 * Reformatted source and put Log at bottom
 *
 * Revision 1.2  2002/11/16 18:03:33  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.7  1995/11/14 11:25:57  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.6  1995/11/13 11:11:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1995/10/29 16:02:49  Steven_Green
 * Added GeneralError
 *
 * Revision 1.4  1995/10/20 11:05:41  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1995/10/18 10:59:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/17 12:03:08  Steven_Green
 * debugMessage added
 *
 * Revision 1.1  1995/10/16 11:33:16  Steven_Green
 * Initial revision
 *
 *----------------------------------------------------------------------
 */
