##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

#####################################
# Makefile Include for System.LIB
#
# Execute from within System directory
#####################################

NAME = system
lnk_dependencies = sys_lib.mk

OBJS = memdebug.obj

all: $(TARGETS) .SYMBOLIC
	@%null

!include ..\lib95.mif
