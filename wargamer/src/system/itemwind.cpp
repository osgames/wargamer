/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Custom ListBox / ComboBox, written by Paul Sample and modified by
 * Steven.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "itemwind.hpp"
#include "dragwin.hpp"
#include "dib.hpp"
#include "cscroll.hpp"
#include "app.hpp"
#include "winctrl.hpp"
// #include "scn_res.h"
#include "palette.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "imglib.hpp"
#include "dib_util.hpp"
// #include "generic.hpp"
#include "scrnbase.hpp"
#include "misc.hpp"  // STL min/max/swap functions

DrawDIBDC* ItemWindow::s_windowDib = 0;
DrawDIBDC* ItemWindow::s_itemDib = 0;
UWORD ItemWindow::s_instanceCount = 0;
UINT ItemWindow::s_dragMsgID = 0;

/*
 * User Defined Message
 *
 * DrawDIBDC* OnGetDragDIB(HWND h, int id);
 */


const int IW_GETDRAGDIB = WM_USER;

#define HANDLE_IW_GETDRAGDIB(h, w, l, fn) \
    reinterpret_cast<LRESULT>((DrawDIBDC*)(fn)(h, LOWORD(w)))
#define FORWARD_IW_GETDRAGDIB(h, id, fn) \
    reinterpret_cast<DrawDIBDC*>((fn)((h), IW_GETDRAGDIB, MAKEWPARAM((id),0), 0))


int ItemWindowData::borderWidth() const
{
   if(d_flags & NoBorder)
      return 1;
   else
      return ScreenBase::x(4);
}

int ItemWindowData::borderHeight() const
{
   return borderWidth();
}


ItemWindow::ItemWindow(const ItemWindowData& data, const SIZE& s) :
  d_hScroll(0),
  d_data(data),
  d_showing(False),
  d_currentValue(0),
  d_topItem(0)
{
   ASSERT(data.d_hParent != 0);

   s_instanceCount++;
   s_dragMsgID = RegisterWindowMessage(DRAGLISTMSGSTRING);
   ASSERT(s_dragMsgID != 0);
   //  d_data = data;         // structure copy

   HWND hWnd = createWindow(d_data.d_exStyle,
      // GenericNoBackClass::className(),
      NULL,
      d_data.d_style,
      0, 0, s.cx, s.cy,
      d_data.d_hParent, NULL  // , APP::instance()
      );
   ASSERT(hWnd != NULL);
}

ItemWindow::~ItemWindow()
{
   selfDestruct();

   ASSERT((s_instanceCount - 1) >= 0);

   if(--s_instanceCount == 0)
   {
      if(s_windowDib)
      {
         delete s_windowDib;
         s_windowDib = 0;
      }
      if(s_itemDib)
      {
         delete s_itemDib;
         s_itemDib = 0;
      }
   }
}

LRESULT ItemWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
//   LRESULT result;

   if(msg == s_dragMsgID)
   {
      return SendMessage(d_data.d_hParent, msg, wParam, lParam);
   }

   switch(msg)
   {
   HANDLE_MSG(hWnd, WM_CREATE,  onCreate);
   HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
   HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
   HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
   HANDLE_MSG(hWnd, WM_MEASUREITEM, onMeasureItem);
   HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
   HANDLE_MSG(hWnd, WM_VSCROLL, onVScroll);
   HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
   HANDLE_MSG(hWnd, IW_GETDRAGDIB, onGetDragDIB);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL ItemWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#if 0
      if(d_data.d_dbX == 0)
   {
      // get base font size (in dialog units)
      const LONG dbUnits = GetDialogBaseUnits();
      d_data.d_dbX = LOWORD(dbUnits);
      d_data.d_dbY = HIWORD(dbUnits);
   }
#endif
   /*
   * Create User-drawn list box
   */

   {
      UINT flags = WS_CHILD |
         WS_VISIBLE |
         LBS_OWNERDRAWFIXED |
         LBS_HASSTRINGS |
         LBS_NOTIFY;

      if(!(d_data.d_flags & ItemWindowData::InclusiveSize))
         flags |= LBS_NOINTEGRALHEIGHT;

      HWND hList = CreateWindow("LISTBOX", NULL,
         flags,
         0, 0, 0, 0,
         hWnd, (HMENU)ItemListBox, APP::instance(), NULL);

      ASSERT(hList);

      if(d_data.d_flags & ItemWindowData::DragList)
      {
         BOOL result = MakeDragList(hList);
         ASSERT(result);
      }

      d_listBox.set(hList);
   }

   if(d_data.d_flags & ItemWindowData::HasScroll)
   {
      /*
      * Create custom scroll bar
      */

      CustomScrollBar::initCustomScrollBar(APP::instance());

      d_hScroll = csb_create(0, WS_CHILD | SBS_VERT, 0, 0, 15, 50,
         hWnd, APP::instance());

      ASSERT(d_hScroll != 0);


      /*
      * Initialize scroll bar
      */

      csb_setButtonIcons(d_hScroll, d_data.d_scrollBtns);
      csb_setThumbBk(d_hScroll, d_data.d_scrollThumbDib);
      csb_setScrollBk(d_hScroll, d_data.d_scrollFillDib);
      csb_setBorder(d_hScroll, &d_data.d_bColors);
      csb_setPage(d_hScroll, d_data.d_maxItemsShowing);

#if 0
         csb_setButtonIcons(d_hScroll, ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons));
      csb_setThumbBk(d_hScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground));
      csb_setScrollBk(d_hScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground));
      csb_setBorder(d_hScroll, &scenario->getBorderColors());
      csb_setPage(d_hScroll, d_data.d_maxItemsShowing);
#endif
   }

   if(d_data.d_flags & ItemWindowData::TextHeader)
   {
      HWND hwnd = CreateWindow("Static", NULL,
         WS_CHILD | WS_VISIBLE | SS_OWNERDRAW,
         0, 0, 0, 0,
         hWnd, (HMENU)ListHeader, APP::instance(), NULL);

      ASSERT(hwnd);
   }

   return TRUE;
}

void ItemWindow::onDestroy(HWND hWnd)
{
   d_listBox.clear();
}


BOOL ItemWindow::onEraseBk(HWND hwnd, HDC hdc)
{
   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);

   RECT r;
   GetClientRect(hwnd, &r);

   const int cx = r.right - r.left;
   const int cy = r.bottom - r.top;

   if(s_windowDib)
      BitBlt(hdc, 0, 0, cx, cy, s_windowDib->getDC(), 0, 0, SRCCOPY);

   SelectPalette(hdc, oldPal, FALSE);

   return TRUE;
}

void ItemWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
   switch(id)
   {
   case ItemListBox:
      if(codeNotify == LBN_SELCHANGE)
      {
         ListBox lb(hwndCtl);
         d_currentValue = lb.getValue();

         SendMessage(d_data.d_hParent, WM_COMMAND, MAKEWPARAM(d_data.d_id, 0), (LPARAM)hwnd);

         if(d_data.d_flags & ItemWindowData::NoAutoHide)
            ; // do nothing
         else
            hide();
      }
      break;
   }
}

void ItemWindow::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   /*
   * Special case returns
   *
   */

   if((d_data.d_flags & ItemWindowData::NoAutoHide))
      return;

   /*
   * If mouse is over our list window then release capture and pass it on
   * otherwise, release capture and close window
   */


   // get handle to list box
   // HWND hList = GetDlgItem(hwnd, ItemListBox);
   ASSERT(d_listBox.getHWND());

   // get its screen rect
   RECT r;
   GetWindowRect(d_listBox.getHWND(), &r);

   // convert to client rect
   POINT p;
   p.x = r.left;
   p.y = r.top;

   HRESULT result = ScreenToClient(hwnd, &p);
   ASSERT(result);

   SetRect(&r, p.x, p.y, p.x+(r.right-r.left), p.y+(r.bottom-r.top));

   // see if mouse is over list
   p.x = x;
   p.y = y;

   if(PtInRect(&r, p))
   {
      // if so, pass it on to the list box to handle

      // first, convert mouse to list coordinates
      result = ClientToScreen(hwnd, &p);
      ASSERT(result);

      result = ScreenToClient(d_listBox.getHWND(), &p);
      ASSERT(result);

      SendMessage(d_listBox.getHWND(), WM_LBUTTONDOWN, (WPARAM)keyFlags, MAKELPARAM(p.x, p.y));
   }

   // otherwise, see if we are over the scrollbar
   else if(d_data.d_flags & ItemWindowData::HasScroll)
   {
      ASSERT(d_hScroll);

      // get its screen rect
      GetWindowRect(d_hScroll, &r);

      // convert to client rect
      p.x = r.left;
      p.y = r.top;

      result = ScreenToClient(hwnd, &p);
      ASSERT(result);

      SetRect(&r, p.x, p.y, p.x+(r.right-r.left), p.y+(r.bottom-r.top));

      // see if mouse is over scroll
      p.x = x;
      p.y = y;

      if(PtInRect(&r, p))
      {
         // first, convert mouse to scrollbar coordinates
         result = ClientToScreen(hwnd, &p);
         ASSERT(result);

         result = ScreenToClient(d_hScroll, &p);
         ASSERT(result);

         SendMessage(d_hScroll, WM_LBUTTONDOWN, (WPARAM)keyFlags, MAKELPARAM(p.x, p.y));
      }
      else
         hide();
   }
   else
      hide();

}


void ItemWindow::hide()
{
   if(!(d_data.d_flags & ItemWindowData::NoAutoHide))
      ReleaseCapture();

   d_showing = False;
   ShowWindow(getHWND(), SW_HIDE);
}

void ItemWindow::onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem)
{
   lpMeasureItem->itemHeight = d_data.d_itemCY;
}

void ItemWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
   HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
   RealizePalette(lpDrawItem->hDC);

   switch(lpDrawItem->CtlID)
   {
   case ItemListBox:

      if(d_data.d_flags & ItemWindowData::OwnerDraw)
         drawListItem(lpDrawItem);
      else
         blitText(lpDrawItem);

      break;

   case ListHeader:
      drawHeader(lpDrawItem);
      break;
   }

   SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
}

/*
 * Default to using drawListItem
 */

DrawDIBDC* ItemWindow::onGetDragDIB(HWND h, int id)
{
   ListBox lb(d_listBox.getHWND());

   // Make ourselves a DIB

   PixelRect r;
   GetClientRect(d_listBox.getHWND(), &r);

   DrawDIBDC* dib = new DrawDIBDC(r.width(), d_data.d_itemCY);

   drawDragItem(dib, lb, id);

   return dib;
}

/*
 * Default drawDragItem
 */

void ItemWindow::drawDragItem(DrawDIBDC* dib, ListBox& lb, int id)
{
   DRAWITEMSTRUCT drawItem;
   drawItem.CtlType = ODT_LISTBOX;
   drawItem.CtlID = 0;
   drawItem.itemID = id;
   drawItem.itemAction = ODA_DRAWENTIRE;
   drawItem.itemState = 0;
   drawItem.hwndItem = lb.getHWND();
   drawItem.hDC = dib->getDC();
   drawItem.rcItem = PixelRect(0, 0, dib->getWidth(), dib->getHeight());
   drawItem.itemData = lb.getValue(id);

   drawListItem(&drawItem);
}

DrawDIBDC* ItemWindow::allocateItemDib(LONG cx, LONG cy)
{
   if(!s_itemDib ||
      s_itemDib->getWidth() < cx || s_itemDib->getHeight() < cy)
   {
      LONG oldCX = 0;
      LONG oldCY = 0;

      if(s_itemDib)
      {
         oldCX = s_itemDib->getWidth();
         oldCY = s_itemDib->getHeight();

         delete s_itemDib;
         s_itemDib = 0;
      }

      s_itemDib = new DrawDIBDC(maximum(oldCX, cx), maximum(oldCY, cy));
      ASSERT(s_itemDib);

      s_itemDib->setBkMode(TRANSPARENT);
   }

   if(d_data.d_hFont)
      s_itemDib->setFont(d_data.d_hFont);
   // default font
   else
   {
      int fontHeight = d_data.d_itemCY - 3;

      LogFont lf;
      lf.height(fontHeight);
      lf.weight(FW_MEDIUM);
      //  lf.face("Normal");
      lf.charset(Greenius_System::ANSI);    // otherwise we may get symbols!

      Font font;
      font.set(lf);

      s_itemDib->setFont(font);
   }

   return s_itemDib;
}

void ItemWindow::allocateWindowDib(LONG cx, LONG cy)
{
   if(!s_windowDib ||
      s_windowDib->getWidth() < cx || s_windowDib->getHeight() < cy)
   {
      LONG oldCX = 0;
      LONG oldCY = 0;

      if(s_windowDib)
      {
         oldCX = s_windowDib->getWidth();
         oldCY = s_windowDib->getHeight();

         delete s_windowDib;
         s_windowDib = 0;
      }

      s_windowDib = new DrawDIBDC(maximum(oldCX, cx), maximum(oldCY, cy));
   }

   ASSERT(s_windowDib);
}

void ItemWindow::setCurrentValue()
{
   ListBox lb(d_listBox.getHWND());  // getHWND(), ItemListBox);

   if(lb.getNItems() > 0)
      d_currentValue = lb.getValue();
}

// transfer bits from mainwindow's hdc to draw shadow
void ItemWindow::transferBits(const PixelPoint& p, int cx, int cy, int shadowCX, int shadowCY)
{
   POINT p2;
   p2.x = p.getX();
   p2.y = p.getY();

   ScreenToClient(APP::getMainHWND(), &p2);

   HDC hdc = GetDC(APP::getMainHWND());

   int x = cx - shadowCX;
   int y = 0;
   BitBlt(s_windowDib->getDC(), x, y, shadowCX, cy,
      hdc, x + p2.x, y + p2.y, SRCCOPY);

   x = 0;
   y = cy - shadowCY;
   BitBlt(s_windowDib->getDC(), x, y, cx, shadowCY,
      hdc, x + p2.x, y + p2.y, SRCCOPY);

   ReleaseDC(APP::getMainHWND(), hdc);
}

int ItemWindow::entries()
{
   ListBox lb(d_listBox.getHWND());  // getHWND(), ItemListBox);
   return lb.getNItems();
}

void ItemWindow::setCurrentValue(ULONG v)
{
   ListBox lb(d_listBox.getHWND());  // getHWND(), ItemListBox);
   lb.setValue(v);

   setCurrentValue();
   showScroll();
}

ItemWindowLayout::ItemWindowLayout() :
   s_listCX(0),
   s_listCY(0),
   s_shadowWidth(0),
   s_shadowHeight(0),
   s_headerCY(0),
   s_headerOffset(0),
   s_scrollW(0),
   s_offsetX(0),
   s_offsetY(0),
   s_totalW(0),
   s_totalH(0),
   s_p()
{
}

void ItemWindow::show(const PixelPoint& p)
{
   ItemWindowLayout info;

   info.s_p = p;

   ListBox lb(d_listBox.getHWND()); // getHWND(), ItemListBox);

   /*
   * Some useful values
   */

   const int entries = lb.getNItems();

   // if no entries, return
   if((entries == 0) && !(d_data.d_flags & ItemWindowData::NoAutoHide))
   {
      ShowWindow(getHWND(), SW_HIDE);
      return;
   }

   /*
   * Set mouse capture to this window
   */

   if(!(d_data.d_flags & ItemWindowData::NoAutoHide))
   {
      SetForegroundWindow(getHWND());
      SetCapture(getHWND());
   }

   d_showing = True;

   info.s_offsetX = d_data.borderWidth();
   info.s_offsetY = d_data.borderHeight();

   RECT r;        // Outer rectangle area

   if(d_data.d_flags & ItemWindowData::InclusiveSize)
   {
      GetClientRect(getHWND(), &r);
   }
   else
   {
      GetClientRect(d_listBox.getHWND(), &r);
   }

   // adjust rect for header
   if(d_data.d_flags & ItemWindowData::TextHeader)
   {
      info.s_headerCY = d_data.d_itemCY;;
      info.s_headerOffset = info.s_offsetY + info.s_headerCY;
   }
   else
      info.s_headerCY = 0;

   if(d_data.d_flags & ItemWindowData::ShadowBackground)
   {
      info.s_shadowWidth = ScreenBase::x(5);
      info.s_shadowHeight = info.s_shadowWidth;
   }


   if(d_data.d_flags & ItemWindowData::InclusiveSize)
   {
      info.s_listCX = r.right - r.left - info.s_offsetX * 2;   // - info.s_scrollW;
      info.s_listCY = r.bottom - r.top - info.s_offsetY * 2 - info.s_headerCY;
   }
   else
   {
      info.s_listCX = r.right - r.left;
      info.s_listCY = r.bottom - r.top;

      r.right += info.s_offsetX * 2;
      r.bottom += info.s_offsetY * 2;

//      r.right += info.s_scrollW;

      r.bottom += info.s_headerOffset;
   }

   /*
    * Calculate height if ListBox and number of items in it
    */

   if (d_data.d_flags & ItemWindowData::InclusiveSize)
   {
      ASSERT(d_data.d_itemCY != 0);
      d_data.d_maxItemsShowing = info.s_listCY / d_data.d_itemCY;
   }

   // adjust rect for scroll

   info.s_scrollW = 0;
   if(d_data.d_flags & ItemWindowData::HasScroll)
   {
      if(entries > d_data.d_maxItemsShowing)
      {
         info.s_scrollW = d_data.d_scrollCX;
         if(d_data.d_flags & ItemWindowData::InclusiveSize)
            info.s_listCX -= info.s_scrollW;
         else
            r.right += info.s_scrollW;
      }
      else
      {
         ShowWindow(d_hScroll, SW_HIDE);
      }
   }

   /*
   * Set position of list
   */

   if(entries == 0)
      ShowWindow(d_listBox.getHWND(), SW_HIDE);
   else
   {
      if(d_data.d_flags & ItemWindowData::InclusiveSize)
      {
         ASSERT(d_data.d_itemCY != 0);
            // calculate size of actual list box based on number of items

         // Ensure it is an exact multiple of Item Height

         // int height = entries * d_data.d_itemCY;
         // height = minimum(height, info.s_listCY);
         int nItems = std::min<int>(entries, d_data.d_maxItemsShowing);
         int height = nItems * d_data.d_itemCY;

         SetWindowPos(d_listBox.getHWND(), HWND_TOP, info.s_offsetX, info.s_offsetY + info.s_headerOffset, info.s_listCX, height, 0);
      }
      else
         SetWindowPos(d_listBox.getHWND(), HWND_TOP, info.s_offsetX, info.s_offsetY + info.s_headerOffset, 0, 0, SWP_NOSIZE);
      ShowWindow(d_listBox.getHWND(), SW_SHOW);
   }

   if(d_data.d_flags & ItemWindowData::TextHeader)
   {
      HWND h = GetDlgItem(getHWND(), ListHeader);
      ASSERT(h);

      // set position of header static constrol
      SetWindowPos(h, HWND_TOP, info.s_offsetX, info.s_offsetY, info.s_listCX, info.s_headerCY, 0);
   }

   info.s_totalW = r.right - r.left + info.s_shadowWidth;
   info.s_totalH = r.bottom - r.top + info.s_shadowHeight;

   allocateWindowDib(info.s_totalW, info.s_totalH);
   ASSERT(s_windowDib);
   drawBackground(s_windowDib, info);

   if(info.s_scrollW != 0)
      showScroll();

   /*
   * Set position of window
   */

   // clip it

   POINT cp = info.s_p;
   clipPoint(r, cp); // static_cast<POINT&>(p));
   SetWindowPos(getHWND(), HWND_TOP, cp.x, cp.y, info.s_totalW, info.s_totalH, SWP_SHOWWINDOW);
}

void ItemWindow::drawBackground(DrawDIBDC* dib, const ItemWindowLayout& info)
{
   DIB_Utility::InsertData id;
   if(!(d_data.d_flags & ItemWindowData::NoBorder))
   {
      ASSERT(d_data.d_windowFillDib);

      if(d_data.d_flags & ItemWindowData::ShadowBackground)
      {
         transferBits(info.s_p, info.s_totalW, info.s_totalH, info.s_shadowWidth, info.s_shadowHeight);

         s_windowDib->rect(0, 0, info.s_totalW - info.s_shadowWidth, info.s_totalH - info.s_shadowHeight, d_data.d_windowFillDib);

         /*
         *  draw pop-up insert
         */

         id.d_cRef = d_data.d_colorize;
         id.d_x = 0;
         id.d_y = 0;
         id.d_cx = info.s_totalW;
         id.d_cy = info.s_totalH;
         id.d_shadowCX = info.s_shadowWidth;
         id.d_shadowCY = info.s_shadowHeight;
         id.d_mode = DIB_Utility::InsertData::ShadowOnly;

         DIB_Utility::drawInsert(s_windowDib, id);

      }
      else
         s_windowDib->rect(0, 0, info.s_totalW, info.s_totalH, d_data.d_windowFillDib);

      /*
      * Draw border around window
      */

      RECT r;
      SetRect(&r, 0, 0, info.s_totalW - info.s_shadowWidth, info.s_totalH - info.s_shadowHeight);

      CustomBorderWindow cw(d_data.d_bColors);
      cw.drawThinBorder(s_windowDib->getDC(), r);
   }
   else
      s_windowDib->rect(0, 0, info.s_totalW, info.s_totalH, d_data.d_windowFillDib);

   ColourIndex c1 = s_windowDib->getColour(Palette::brightColour(id.d_cRef, 32, 255));
   ColourIndex c2 = s_windowDib->getColour(Palette::brightColour(id.d_cRef, 0, 128));

   /*
   * Draw Border around header
   */

   if(d_data.d_flags & ItemWindowData::TextHeader)
   {
      s_windowDib->frame(info.s_offsetX - 1, info.s_offsetY - 1, info.s_listCX + 2, info.s_headerCY + 2, c1, c2);
   }

   /*
   * Draw Border around list box
   */

   s_windowDib->frame(info.s_offsetX - 1, (info.s_offsetY + info.s_headerOffset) - 1, info.s_listCX + 2, info.s_listCY + 2, c1, c2);
}


/*
 * Adjust position so that box is on main window
 */

void ItemWindow::clipPoint(const RECT& r, POINT& p)
{
   /*
   * Get some useful values
   */

   ASSERT(APP::getMainHWND());

   RECT mRect;
   GetClientRect(APP::getMainHWND(), &mRect);

   const int screenCX = mRect.right - mRect.left;
   const int screenCY = mRect.bottom - mRect.top;

   // for left side
   p.x = maximum(0L, p.x);

   // for right side
   p.x = minimum(screenCX - (r.right-r.left), p.x);

   // for top
   p.y = maximum(0L, p.y);

   // for bottom
   p.y = minimum(screenCY - (r.bottom-r.top), p.y);
}


void ItemWindow::showScroll()
{
   if(d_data.d_flags & ItemWindowData::HasScroll)
   {
      ListBox lb(d_listBox.getHWND());   // getHWND(), ItemListBox);
      int entries = lb.getNItems();

      if(entries > d_data.d_maxItemsShowing)
      {
         ASSERT(d_hScroll);

         // HWND hwndList = GetDlgItem(getHWND(), ItemListBox);
         ASSERT(d_listBox.getHWND());

         RECT r;
         GetClientRect(d_listBox.getHWND(), &r);

         int itemsShowing = std::min<int>(d_data.d_maxItemsShowing, entries);
//         const LONG offsetX = (d_data.d_flags & ItemWindowData::NoBorder) ? 1 :
//            (4 * ScreenBase::dbX()) / ScreenBase::baseX();
//         const LONG offsetY = offsetX;
         LONG offsetX = d_data.borderWidth();
         LONG offsetY = d_data.borderHeight();

         d_topItem = maximum(0, minimum(lb.getIndex(), entries-d_data.d_maxItemsShowing));

         csb_setRange(d_hScroll, 0, entries);
         csb_setPosition(d_hScroll, d_topItem);
         csb_setPage(d_hScroll, d_data.d_maxItemsShowing);

         lb.setTop(d_topItem);

         SetWindowPos(d_hScroll, HWND_TOP,
            offsetX + (r.right - r.left), offsetY, d_data.d_scrollCX, r.bottom - r.top,
            SWP_SHOWWINDOW);

      }
   }
}

void ItemWindow::onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
   ASSERT(d_data.d_flags & ItemWindowData::HasScroll);

   int minP;
   int maxP;

   int page = csb_getPage(hwndCtl);

   csb_getRange(hwndCtl, &minP, &maxP);
   ASSERT(maxP > d_data.d_maxItemsShowing);

   switch(code)
   {
   case SB_PAGEUP:
      d_topItem = maximum(0, d_topItem-d_data.d_maxItemsShowing);
      break;
   case SB_PAGEDOWN:
      d_topItem = minimum(d_topItem+d_data.d_maxItemsShowing, maxP-d_data.d_maxItemsShowing);
      break;
   case SB_LINEUP:
      d_topItem = maximum(0, d_topItem-1);
      break;
   case SB_LINEDOWN:
      d_topItem = minimum(d_topItem+1, maxP-d_data.d_maxItemsShowing);
      break;
   case SB_THUMBPOSITION:
      d_topItem = pos;
      break;

   default:
      return;
   }

   csb_setPosition(hwndCtl, d_topItem);

   ListBox lb(d_listBox.getHWND()); // hwnd, ItemListBox);
   lb.setTop(d_topItem);
}

void ItemWindow::blitText(const DRAWITEMSTRUCT* lpDrawItem)
{
   // get text
   char text[200];
   SendMessage(lpDrawItem->hwndItem, LB_GETTEXT, lpDrawItem->itemID, reinterpret_cast<LPARAM>(text));

   blitText(lpDrawItem, text);
}

void ItemWindow::blitText(const DRAWITEMSTRUCT* lpDrawItem, const char* text)
{
   ASSERT(text != 0);
   const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
   const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

   allocateItemDib(itemCX, itemCY);

   ASSERT(s_itemDib);
   ASSERT(d_data.d_fillDib);

   // fill backfround
   if(!(lpDrawItem->CtlType == ODT_COMBOBOX))
   {
      s_itemDib->rect(0, 0, itemCX, itemCY, d_data.d_fillDib);
      if(!(lpDrawItem->itemState & ODS_SELECTED))
         itemDib()->darkenRectangle(0, 0, itemCX, itemCY, 20);
   }

   //  ListBox lb(lpDrawItem->hwndItem);

   /*
   * Put text
   */

   COLORREF oldCol = itemDib()->setTextColor(PALETTERGB(0, 0, 0));

   TEXTMETRIC tm;
   GetTextMetrics(s_itemDib->getDC(), &tm);

   LONG x = (ScreenBase::dbX() / ScreenBase::baseX());
   LONG y = ((itemCY - tm.tmHeight) / 2);
   wTextOut(itemDib()->getDC(), x, y, itemCX - (x + 2), text);

   BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
      s_itemDib->getDC(), 0, 0, SRCCOPY);

   itemDib()->setTextColor(oldCol);
}

int ItemWindow::currentIndex()
{
   ListBox lb(d_listBox.getHWND());  // getHWND(), ItemListBox);
   if(lb.getNItems() > 0)
   {
      return lb.getIndex();
   }

   return -1;
}

void ItemWindow::setCurrentIndex(int index)
{
   ListBox lb(d_listBox.getHWND());  // getHWND(), ItemListBox);
   if(lb.getNItems() > 0)
   {
      lb.set(index);
      setCurrentValue();
      showScroll();
   }
}

bool ItemWindow::overItem(const PixelPoint& p) const
{
   DragWindowObject ob;
   bool result = overItem(p, &ob);

   // if(result)
   // {
   //     ListBox lb(d_listBox.getHWND());
   //     if(ob.id() != lb.getIndex())
   //         setCurrentIndex(ob.id());
   // }

   return result;
}

bool ItemWindow::overItem(const PixelPoint& p, DragWindowObject* ob) const
{
   ListBox lb(d_listBox.getHWND());  // getHWND(), ItemListBox);

   int itemID = LBItemFromPt(lb.getHWND(), p, FALSE);
   if(itemID != -1)
   {
      if(ob)
         *ob = DragWindowObject(d_listBox.getHWND(), itemID);

      if(itemID != lb.getIndex())
      {
         // lb.set(itemID);
         d_currentValue = lb.getValue();
      }

      return true;
   }
   else
      return false;
}


void ItemWindow::move(const PixelRect& r)
{
   MoveWindow(getHWND(), r.left(), r.top(), r.width(), r.height(), TRUE);

   // SetWindowPos(d_listBox, HWND_TOP, 0, 0, r.width(), r.height(), SWP_NOMOVE);
   show(PixelPoint(r.left(), r.top()));
}

void ItemWindow::resetItems()
{
   ListBox lb(d_listBox.getHWND());
   lb.reset();
}


void ItemWindow::addItem(const char* name, int id)
{
   ListBox lb(d_listBox.getHWND());
   lb.add(name, id);
}

// int ItemWindow::getNItems()
// {
//    ListBox lb(d_listBox);
//    return lb.getNItems();
// }


ListBox ItemWindow::listBox()
{
   return ListBox(d_listBox.getHWND());
}


DrawDIBDC* ItemWindow::getDragDIB(HWND hListBox, int id)
{
   return FORWARD_IW_GETDRAGDIB(hListBox, id, SendMessage);
}


LRESULT ItemWindowListBox::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   if(msg == IW_GETDRAGDIB)
      return SendMessage(GetParent(hWnd), msg, wParam, lParam);
   else
      return defProc(hWnd, msg, wParam, lParam);
}



