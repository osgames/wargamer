/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "pathdial.hpp"
#include "myassert.hpp"
#include "resdef.h"
#include "misc.hpp"
#include "resstr.hpp"
#include <shlobj.h>

PathDial::PathDial()
{
  d_path = 0;

  d_oldPath = 0;
}

PathDial::~PathDial()
{
  if(d_oldPath)
	 delete d_oldPath;

}


BOOL PathDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		case WM_INITDIALOG:
			HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
			break;

		case WM_DESTROY:
			HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
			break;
		case WM_COMMAND:
			HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
			break;
		case WM_CLOSE:
			HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
			break;
		default:
			return FALSE;
	}

	return TRUE;
}


// static const char text[] = "File Not Found! Please make sure Wargamer CD is in your CD-Drive. If '%s' is not your CD-ROM drive click browse and select correct drive.";

BOOL PathDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
	HWND hText = GetDlgItem(hwnd, IDPD_TEXT);
	ASSERT(hText != 0);

	char buf[500];
	wsprintf(buf, InGameText::get(IDS_FileNotFound), d_oldPath);

	SetWindowText(hText, buf);

	HWND hBrowse = GetDlgItem(hwnd, IDPD_BROWSE);
	ASSERT(hBrowse != 0);
	EnableWindow(hBrowse, TRUE);

	return TRUE;
}

#if 0
void PathDial::onDestroy(HWND hwnd)
{


}
#endif

void PathDial::onClose(HWND hwnd)
{
  onCancel();
}

void PathDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
	 case IDPD_BROWSE:
		 onBrowse();
		 break;

	 case IDPD_CANCEL:
		 onCancel();
		 break;
  }
}

void PathDial::onBrowse()
{
	 BROWSEINFO bi;
	 LPSTR buffer = 0;
	 LPITEMIDLIST pidlBrowse = 0;    // PIDL selected by user

	 /*
	  *  Get pointer to IMalloc
	  */

	 LPMALLOC iMalloc = 0;
	 HRESULT result = SHGetMalloc(&iMalloc);   // pointer to a pointer!
	 ASSERT(result == NOERROR);
	 ASSERT(iMalloc != NULL);

	 /*
	  * Allocate a buffer to receive browse information.
	  */

	 buffer = (LPSTR)iMalloc->Alloc(MAX_PATH);
	 ASSERT(buffer != NULL);


	 /*
	  * Fill in the BROWSEINFO structure.
	  */

	 bi.hwndOwner = getHWND();
	 bi.pidlRoot = NULL;
	 bi.pszDisplayName = buffer;
	 bi.lpszTitle = InGameText::get(IDS_BrowseFolders);
	 bi.ulFlags = BIF_RETURNONLYFSDIRS;
	 bi.lpfn = NULL;
	 bi.lParam = 0;

	 /*
	  * Browse for a folder and return its PIDL.
	  */

	 pidlBrowse = SHBrowseForFolder(&bi);
	 if (pidlBrowse != NULL)
	 {
		  SHGetPathFromIDList(pidlBrowse, buffer);

		  lstrcpy(d_path, buffer);

		  iMalloc->Free(pidlBrowse);
	 }

	 iMalloc->Free(buffer);

	 EndDialog(getHWND(), IDOK);


}

void PathDial::onCancel()
{
  EndDialog(getHWND(), IDCANCEL);
}

int PathDial::displayPathDial(char* path, const char* oldPath)
{
	ASSERT(path != 0);
	d_path = path;

	ASSERT(oldPath != 0);
	d_oldPath = copyString(oldPath);

	return createDialog(pathDlgName, GetDesktopWindow());
}

int pathDialog(char* path, const char* oldPath)
{
  PathDial pathDial;

  return pathDial.displayPathDial(path, oldPath);
}
