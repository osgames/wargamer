/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "resstr.hpp"
#include "wmisc.hpp"
#include "app.hpp"

ResourceStrings::ResourceStrings(const int* ids, int entries) :
  d_entries(entries),
  d_strings(new SimpleString[d_entries])
{
    ASSERT(d_entries > 0);
    ASSERT(d_strings);
    ASSERT(ids);

    for(int i = 0; i < d_entries; i++)
    {
        if(ids[i] != -1)
        {
    	    idsToString(d_strings[i], ids[i]);
	        ASSERT(d_strings[i].toStr());
        }
    }
}

InGameText::Container InGameText::s_items;

const char* InGameText::get(ID id)
{
   // Is it in the map already?

   if(id == Null)
      return "";

   if (s_items.find(id) == s_items.end())
   {
      // Add it

      const int BUF_LEN = 256;
      char buffer[BUF_LEN];
      int nChar = LoadString(APP::instance(), id, buffer, BUF_LEN);
      ASSERT(nChar != 0);
      StringPtr str(buffer);
      s_items.insert(std::pair<const ID,StringPtr>(id, str));
   }

   return s_items[id];

}
