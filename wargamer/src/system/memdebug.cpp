/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Replacement's for new and delete, that do some elementary
 * debugging.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#ifdef __WATCOM_CPLUSPLUS__
#ifdef DEBUG

#include "sysdll.h"
#include "myassert.hpp"

#include <string.h>
#include <malloc.h>


/*
 * Structure to store information
 *
 * We want to check that we are not freeing memory that has not been
 * allocated
 */

static const ULONG validMemID = 'ALOC';
static const ULONG freeMemID  = 'FREE';

static const ULONG validTailID = 'EMEM';
static const ULONG freeTailID  = 'ENDF';


static UBYTE fillFree = 0xAA;				// Freshly deleted memory filled with this
static UBYTE fillAlloc = 0x55;			// Freshly malloced memory is filled with this

struct MemHead {
	ULONG blockSize;
	ULONG memID;
};

struct MemTail {
	ULONG memID;
};

/*
 * These overide the default library new/delete
 */

SYSTEM_DLL void*  operator new(size_t s)
{
	void* ad;

	size_t fullSize = s + sizeof(MemHead) + sizeof(MemTail);
	MemHead* memHead = (MemHead*) malloc(fullSize);
	ASSERT(memHead != 0);		// Out of memory

	memHead->memID = validMemID;
	memHead->blockSize = s;

	ad = memHead + 1;

	memset(ad, fillAlloc, s);

	MemTail* memTail = (MemTail*) ((char*)ad + s);

	memTail->memID = validTailID;

	return ad;
}

SYSTEM_DLL void  operator delete(void* p)
{
	// ASSERT(p != 0);		// Freeing 0 is allowable, but naughty...

	if(p)
	{
		MemHead* memHead = (MemHead*) p;
		memHead--;		// Backup to memory header

		ASSERT(memHead->memID == validMemID);

		MemTail* memTail = (MemTail*) ((char*)p + memHead->blockSize);

		ASSERT(memTail->memID == validTailID);

		memHead->memID = freeMemID;
		memTail->memID = freeTailID;

		memset(p, fillFree, memHead->blockSize);

		free(memHead);
	}
}

#endif	// DEBUG
#endif   // __WATCOM_CPLUSPLUS__

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */


