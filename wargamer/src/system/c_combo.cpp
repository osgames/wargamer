/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "c_combo.hpp"
#include "itemwind.hpp"
#include "winctrl.hpp"
#include "app.hpp"
// #include "generic.hpp"
#include "scrnbase.hpp"
#include "palette.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "imglib.hpp"

/*-----------------------------------------------------------
 * First, a popup list derived from ItemWindow
 */

class PopupItem : public ItemWindow {
  const UINT d_listCX;
public:
  PopupItem(const ItemWindowData& data, const SIZE& s) :
	 ItemWindow(data, s),
	 d_listCX(s.cx) {}

  ~PopupItem() {}

  // virtual functions from ItemWindow
  void init(const void* data) {}
  void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
  void drawCombo(DrawDIBDC* dib, const void* data) {}

  void setListSize();
  void add(const char* text, LPARAM param);
  void setValue(LPARAM param);
  void setIndex(int id);
  void reset();

  DrawDIBDC* dib() { return itemDib(); }

  HWND listHWND() const
  {
	 ListBox lb(getHWND(), ItemListBox);
	 return lb.getHWND();
  }
};

void PopupItem::add(const char* text, LPARAM param)
{
  ListBox lb(getHWND(), ItemListBox);
  lb.add(text, param);
}

void PopupItem::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  SendMessage(data().d_hParent, WM_DRAWITEM, data().d_id, reinterpret_cast<LPARAM>(lpDrawItem));
}

void PopupItem::setListSize()
{
  ListBox lb(getHWND(), ItemListBox);
  const LONG itemHeight = data().d_itemCY*lb.getNItems();
  SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, d_listCX, itemHeight, SWP_NOMOVE);
}

void PopupItem::reset()
{
  ListBox lb(getHWND(), ItemListBox);
  lb.reset();
}

/*----------------------------------------------------------
 *  Now, our actual combo window
 */

class CCombo_Imp : public WindowBaseND {
	 PopupItem* d_list;
	 CComboData d_data;

	 enum ID { ID_List = 200 };

#if 0
	 static DrawDIBDC* s_dib;
	 static int s_instanceCount;
#endif

  public:
	 CCombo_Imp(const CComboData& d);
	 ~CCombo_Imp();

	 void reset() { d_list->reset(); }
	 void add(const char* text, LPARAM param) { d_list->add(text, param); }
	 void setValue(LPARAM param) { d_list->setCurrentValue(param); }
	 void setIndex(int id) { d_list->setCurrentIndex(id); }
	 LPARAM getValue() { return d_list->currentValue(); }
	 int getIndex() { return d_list->currentIndex(); }
	 DrawDIBDC* dib() { return (d_list) ? d_list->dib() : 0; }

  private:
	 /*
	  * Window Messages
	  */

	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct);
	 void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
	 void onPaint(HWND hwnd);
	 BOOL onEraseBk(HWND hwnd, HDC hdc);
	 void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
};

//DrawDIBDC* CCombo_Imp::s_dib = 0;
//int CCombo_Imp::s_instanceCount = 0;

CCombo_Imp::CCombo_Imp(const CComboData& d) :
  d_list(0),
  d_data(d)
{

  HWND hWnd = createWindow(
		0,
		// GenericNoBackClass::className(),
        NULL,
		WS_CHILD,
		0, 0, d_data.d_cx, d_data.d_cy,
		d_data.d_hParent, NULL      //, APP::instance()
  );

  ASSERT(hWnd != NULL);
//  s_instanceCount++;
}

CCombo_Imp::~CCombo_Imp()
{
    selfDestruct();
    delete d_list;
}

/*
 * Window Messages
 */

LRESULT CCombo_Imp::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
	 HANDLE_MSG(hWnd, WM_CREATE, onCreate);
	 HANDLE_MSG(hWnd, WM_PAINT, onPaint);
	 HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
	 HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
	 HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);

	 default:
		return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL CCombo_Imp::onCreate(HWND hwnd, CREATESTRUCT* lpCreateStruct)
{
  /*
	* Create ListBox
	*/

  ItemWindowData ld;
  ld.d_hParent = hwnd;
  ld.d_style = WS_POPUP | WS_CLIPSIBLINGS;
  ld.d_id = ID_List;
  ld.d_hFont = d_data.d_hFont;
  ld.d_scrollCX = (5 * ScreenBase::dbX()) / ScreenBase::baseX();
  ld.d_itemCY = d_data.d_itemCY;
  ld.d_maxItemsShowing = d_data.d_maxItemsShowing;
  ld.d_windowFillDib = d_data.d_windowFillDib;
  ld.d_fillDib = d_data.d_fillDib;
  ld.d_colorize = d_data.d_colorize;
  ld.d_bColors = d_data.d_bColors;
  ld.d_flags |= ItemWindowData::ShadowBackground;
  ld.d_scrollBtns = d_data.d_scrollBtns;
  ld.d_scrollFillDib = d_data.d_scrollFillDib;
  ld.d_scrollThumbDib = d_data.d_scrollThumbDib;

  if(d_data.d_flags & CComboData::HasScroll)
	 ld.d_flags |= ItemWindowData::HasScroll;
  if(d_data.d_flags & CComboData::TextHeader)
	 ld.d_flags |= ItemWindowData::TextHeader;
  if(d_data.d_flags & CComboData::OwnerDraw)
	 ld.d_flags |= ItemWindowData::OwnerDraw;

  SIZE s;
  s.cx = d_data.d_cx;
  s.cy = ld.d_itemCY * ld.d_maxItemsShowing;
  d_list = new PopupItem(ld, s);
  ASSERT(d_list);

  return TRUE;
}

void CCombo_Imp::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), False);
  RealizePalette(hdc);

  d_list->allocateItemDib(d_data.d_cx, d_data.d_cy);
  ASSERT(d_list->dib());

  /*
	* First, draw borders and button
	*/

  d_list->dib()->rect(0, 0, d_data.d_cx, d_data.d_cy, d_data.d_fillDib);

  // draw border
  RECT r;
  SetRect(&r, 0, 0, d_data.d_cx, d_data.d_cy);

  CustomBorderWindow bw(d_data.d_bColors);
  bw.drawThinBorder(d_list->dib()->getDC(), r);

  SetRect(&r, CustomBorderWindow::ThinBorder, CustomBorderWindow::ThinBorder,
	  r.right - CustomBorderWindow::ThinBorder, r.bottom - CustomBorderWindow::ThinBorder);

  // put button
  if(d_data.d_comboBtn)
  {
	 const int w = d_data.d_cy - (2 * CustomBorderWindow::ThinBorder);
	 const int h = w;

	 r.right -= w;

	 d_data.d_comboBtn->stretchBlit(d_list->dib(), 0, r.right, r.top, w, h);
  }

  /*
	* Now, pass it off to parent and let it finish drawing
	*/

  DRAWITEMSTRUCT di;
  di.CtlType = ODT_COMBOBOX;
  di.hDC = d_list->dib()->getDC();
  di.CtlID = d_data.d_id;
  di.itemID = d_list->currentIndex();
  di.hwndItem = d_list->listHWND();
  di.rcItem = r;
  di.itemState = ODS_SELECTED;

//  SetRect(&di.rcItem, CustomBorderWindow::ThinBorder, CustomBorderWindow::ThinBorder,
//	 d_data.d_cx - (2 * CustomBorderWindow::ThinBorder), d_data.d_cy - (2 * CustomBorderWindow::ThinBorder));

  if(d_data.d_flags & CComboData::OwnerDraw)
  {
	 SendMessage(d_data.d_hParent, WM_DRAWITEM, d_data.d_id, reinterpret_cast<LPARAM>(&di));
  }
  else
  {
	 char text[200];
	 SendMessage(d_list->listHWND(), LB_GETTEXT, d_list->currentIndex(), reinterpret_cast<LPARAM>(text));
	 d_list->blitText(&di, text);
  }

  /*
	* Finally, blit dib to hdc
	*/

  BitBlt(hdc, 0, 0, d_data.d_cx, d_data.d_cy, d_list->dib()->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, False);

  EndPaint(hwnd, &ps);
}

BOOL CCombo_Imp::onEraseBk(HWND hwnd, HDC hdc)
{
  return TRUE;
}

void CCombo_Imp::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
	 case ID_List:
	 {
//		d_list->
		SendMessage(d_data.d_hParent, WM_COMMAND, MAKEWPARAM(d_data.d_id, CBN_SELCHANGE), reinterpret_cast<LPARAM>(d_list->listHWND()));
		InvalidateRect(hwnd, NULL, FALSE);
      UpdateWindow(hwnd);
		break;
	 }
  }
}

void CCombo_Imp::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  d_list->setListSize();

  /*
	* Show list
	*/

  // convert from client to screen
  POINT p;
  p.x = 0;
  p.y = d_data.d_cy;

  ClientToScreen(hwnd, &p);
  PixelPoint pt = p;
  d_list->show(pt);
}

/*------------------------------------------------------------
 *   Client Access
 */

CCombo::~CCombo()
{
  // destroy();
  delete d_combo;
}

void CCombo::init(const CComboData& cd)
{
  d_combo = new CCombo_Imp(cd);
  ASSERT(d_combo);
}

void CCombo::position(int x, int y)
{
  if(d_combo)
  {
	 SetWindowPos(d_combo->getHWND(), HWND_TOP, x, y, 0, 0, SWP_NOSIZE | SWP_SHOWWINDOW);
  }
}

DrawDIBDC* CCombo::dib()
{
  return (d_combo) ? d_combo->dib() : 0;
}

#if 0
void CCombo::destroy()
{
    delete d_combo;
//  if(d_combo)
//   {
// 	 DestroyWindow(d_combo->getHWND());
//     d_combo = 0;
//   }
}
#endif

HWND CCombo::getHWND() const
{
  return (d_combo) ? d_combo->getHWND() : 0;
}

void CCombo::reset()
{
  if(d_combo)
	 d_combo->reset();
}

void CCombo::add(const char* text, LPARAM param)
{
  if(d_combo)
	 d_combo->add(text, param);

}

void CCombo::add(const CComboInit* ci)
{
  ASSERT(ci);

  if(d_combo)
  {
	 int i = 0;
	 while(ci[i].d_text != 0)
	 {
		d_combo->add(ci[i].d_text, ci[i].d_param);
      i++;
	 }
  }
}

void CCombo::add(const CComboResInit* ci)
{
  ASSERT(ci);

  if(d_combo)
  {
	 int i = 0;
	 while(ci[i].d_textID != InGameText::Null)
	 {
		d_combo->add(InGameText::get(ci[i].d_textID), ci[i].d_param);
      i++;
	 }
  }
}

void CCombo::setValue(LPARAM param)
{
  if(d_combo)
	 d_combo->setValue(param);
}

void CCombo::setIndex(int id)
{
  if(d_combo)
	 d_combo->setIndex(id);
}

LPARAM CCombo::getValue()
{
  return (d_combo) ? d_combo->getValue() : 0;
}

int CCombo::getIndex()
{
  return (d_combo) ? d_combo->getIndex() : CB_NoIndex;
}


