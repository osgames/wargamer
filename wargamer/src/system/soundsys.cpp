/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "soundsys.hpp"

#include "random.hpp"
#include "resstr.hpp"
#include <stdio.h>

/*

        SOUND.CPP
        Low level sound system for both Campaign & Battle Games
        Provides access to the DirectSound object interface

*/


//#define MSGBOX_ERRORS


// global pointer
SoundSystem GlobalSoundSystemObj;


/*
Constructor
*/

SoundSystem::SoundSystem() {

    SetupOK = false;
    app_hwnd = NULL;
    lpDirectSound = NULL;

    CDAudioSetupOK = false;
    PlayingPlayList = false;

    PlayCheckFrequency = 100;
    PlayCheckCounter = PlayCheckFrequency;


}

/*
Destructor
*/

SoundSystem::~SoundSystem() {

    ShutDownDirectSound();
    SetupOK = false;

    if(IsCDPlaying() ) StopCDPlaying();

    CloseCDAudio();
    CDAudioSetupOK = false;

}


/*
Main function call to initialise sound
*/

void
SoundSystem::Init(HWND hwnd) {

    app_hwnd = hwnd;
    SetupOK = InitDirectSound();

    CDAudioSetupOK = OpenCDAudio();

}


/*
Initialise DirectSound object
Set cooperative level
*/

bool
SoundSystem::InitDirectSound() {

    HRESULT dsrval;

    // create a direct sound object
    dsrval = DirectSoundCreate(NULL, &lpDirectSound, NULL);
    if(dsrval != DS_OK) { DirectSoundError(dsrval); return false; }

    // set the sound device cooperative level
    dsrval = lpDirectSound->SetCooperativeLevel(app_hwnd, DSSCL_NORMAL);
    if(dsrval != DS_OK) { DirectSoundError(dsrval); return false; }

return true;
}



/*
Shut down DirectSound
*/

void SoundSystem::ShutDownDirectSound() {

//    HRESULT dsrval;
    if(lpDirectSound != NULL) {
        lpDirectSound->Release();
        lpDirectSound = NULL;
    }

}



/*
Perform error reports
*/

void
SoundSystem::DirectSoundError(HRESULT err) {

    const char* err_msg = NULL;

    switch(err) {
        case DSERR_ALLOCATED : { err_msg = InGameText::get(IDS_SoundInUse); break; }
        case DSERR_INVALIDPARAM : { err_msg = InGameText::get(IDS_InvalidParameter); break; }
        case DSERR_NOAGGREGATION : { err_msg = InGameText::get(IDS_ObjectNoAggregation); break; }
        case DSERR_NODRIVER : { err_msg = InGameText::get(IDS_NoSoundDriver); break; }
        case DSERR_OUTOFMEMORY : { err_msg = InGameText::get(IDS_NoMemory); break; }

        case DSERR_UNINITIALIZED : { err_msg = InGameText::get(IDS_DSoundNotInitialized); break; }
        case DSERR_UNSUPPORTED : { err_msg = InGameText::get(IDS_DSound_FunctionUnSupported); break; }
        default: { err_msg = InGameText::get(IDS_DSoundInternalError); break; }
    }

    if(err_msg != NULL) { MessageBox(app_hwnd, err_msg, InGameText::get(IDS_DSoundError), MB_OK); }

}









bool
SoundSystem::OpenCDAudio() {

   /*
   Open cdaudio
   */
   MCI_OPEN_PARMS mci_open;

   mci_open.lpstrDeviceType = (LPCSTR) MCI_DEVTYPE_CD_AUDIO;

   MCIERROR mcierr = mciSendCommand(
      NULL,
      MCI_OPEN,
      MCI_OPEN_TYPE | MCI_OPEN_TYPE_ID,// | MCI_OPEN_SHAREABLE,
      (DWORD) &mci_open
   );

   if(mcierr) {
      MessageBox(app_hwnd,
         InGameText::get(IDS_NoCDAudio),
         InGameText::get(IDS_MCIError),
         MB_OK);
      return false;
   }

   CDDeviceID = mci_open.wDeviceID;

   /*
   Set time format
   */
    MCI_SET_PARMS mci_set;
   mci_set.dwTimeFormat = MCI_FORMAT_TMSF;

   mcierr = mciSendCommand(
      CDDeviceID,
      MCI_SET,
      MCI_SET_TIME_FORMAT,
      (DWORD) &mci_set
   );

    if(mcierr) {
      MCIError(mcierr);
      return false;
   }

    return true;

}


bool
SoundSystem::CloseCDAudio() {
if(!CDAudioSetupOK) return false;

   /*
   Close all MCI devices opened
   */
    MCIERROR mcierr;
   mcierr = mciSendCommand(
      MCI_ALL_DEVICE_ID,
      MCI_CLOSE,
      MCI_WAIT,
      NULL
   );

    if(mcierr) {
      MCIError(mcierr);
      return false;
   }

    return true;

}



bool
SoundSystem::IsMediaPresent() {
if(!CDAudioSetupOK) return false;

   MCI_STATUS_PARMS mci_status;
   mci_status.dwItem = MCI_STATUS_MEDIA_PRESENT;

   MCIERROR mcierr = mciSendCommand(
      CDDeviceID,
      MCI_STATUS,
      MCI_STATUS_ITEM,
      (DWORD) &mci_status
   );

   if(mcierr) {
      MCIError(mcierr);
      return false;
   }

   if(mci_status.dwReturn) return true;
    else return false;
}



bool
SoundSystem::PlayCDTrack(unsigned int trackno) {
if((!CDAudioSetupOK) || (!IsMediaPresent()) ) return false;

   /*
   Get number of tracks on CD
   */
   MCI_STATUS_PARMS mci_status;
   mci_status.dwItem = MCI_STATUS_NUMBER_OF_TRACKS;

   MCIERROR mcierr = mciSendCommand(
      CDDeviceID,
      MCI_STATUS,
        MCI_STATUS_ITEM,
      (DWORD)(LPVOID) &mci_status
   );

   if(mcierr) {
      MCIError(mcierr);
      return false;
   }

   DWORD num_tracks = mci_status.dwReturn;

   if(trackno > num_tracks) {
      trackno = 1 + ((trackno - 1) % num_tracks);
//       if(trackno == 0) trackno++;
   }

   /*
   Get media length to end of track
   */
   mci_status.dwItem = MCI_STATUS_LENGTH;
   mci_status.dwTrack = trackno;

   mcierr = mciSendCommand(
      CDDeviceID,
      MCI_STATUS,
      MCI_STATUS_ITEM | MCI_TRACK,
      (DWORD) (LPVOID) &mci_status
   );

   if(mcierr) {
      MCIError(mcierr);
      return false;
   }

   DWORD end_length = mci_status.dwReturn;

   int end_minutes = MCI_MSF_MINUTE(end_length);
   int end_seconds = MCI_MSF_SECOND(end_length);
   int end_frames = MCI_MSF_FRAME(end_length);

   /*
   Play track from start to finish
   */
   MCI_PLAY_PARMS mci_play;
   mci_play.dwFrom = MCI_MAKE_TMSF(trackno, 0, 0, 0);
   mci_play.dwTo = MCI_MAKE_TMSF(trackno, end_minutes, end_seconds, end_frames);

   mcierr = mciSendCommand(
      CDDeviceID,
      MCI_PLAY,
      MCI_FROM | MCI_TO,
      (DWORD) &mci_play
   );

   if(mcierr) {
      MCIError(mcierr);
      return false;
   }

    return true;
}



bool
SoundSystem::StopCDPlaying() {
if((!CDAudioSetupOK) || (!IsMediaPresent()) ) return false;

   MCIERROR mcierr = mciSendCommand(
      CDDeviceID,
      MCI_STOP,
      0,
      NULL
   );

    if(mcierr) {
      MCIError(mcierr);
      return false;
   }

    return true;
}



bool SoundSystem::IsCDPlaying() 
{
if((!CDAudioSetupOK) || (!IsMediaPresent()) ) return false;

   /*
   Get the device's mode
   */
   MCI_STATUS_PARMS mci_status;
   mci_status.dwItem = MCI_STATUS_MODE;

   MCIERROR mcierr = mciSendCommand(
      CDDeviceID,
      MCI_STATUS,
      MCI_STATUS_ITEM,
      (DWORD) &mci_status
   );

   if(mcierr) {
      MCIError(mcierr);
      return false;
   }

   DWORD mode = mci_status.dwReturn;

    return (mode == MCI_MODE_PLAY);
}




bool
SoundSystem::IsCDTrackPlaying(unsigned int trackno) {
if((!CDAudioSetupOK) || (!IsMediaPresent()) ) return false;

   if(IsCDPlaying()) {

      /*
      Get the track's mode
      */
      MCI_STATUS_PARMS mci_status;
      mci_status.dwItem = MCI_STATUS_CURRENT_TRACK;

      MCIERROR mcierr = mciSendCommand(
         CDDeviceID,
         MCI_STATUS,
         MCI_STATUS_ITEM,
         (DWORD) &mci_status
      );

      if(mcierr) {
         MCIError(mcierr);
         return false;
      }

      DWORD current_track = mci_status.dwReturn;

      if(current_track  == trackno) return true;
      else return false;
   }
   return false;
}




bool
SoundSystem::SetPlayList(int num, int tracks...) {
if(!CDAudioSetupOK) return false;

    PlayingPlayList = false;

    if(num >= 64) return false;

    NumTracks = num;
    int * val = &tracks;

    for(int f=0; f<num; f++) {
        PlayList[f] = *val;
        val++;
    }

CurrentTrack = 0;
return true;
}



bool
SoundSystem::SetRandomPlayList(int num, int tracks...) {
if(!CDAudioSetupOK) return false;

    PlayingPlayList = false;

    if(num >= 64) return false;

    NumTracks = num;
    int * val = &tracks;

    for(int f=0; f<num; f++) {
        PlayList[f] = *val;
        val++;
    }

    // shuffle entries around some
    for(f=0; f<num; f++) {
        int a = random(0, num);
        int b = random(0, num);
        int tmp = PlayList[a];
        PlayList[a] = PlayList[b];
        PlayList[b] = tmp;
    }

   CurrentTrack = 0;
   return true;
}





bool
SoundSystem::ProcessPlayList() {
if(!CDAudioSetupOK) return false;

    if(! PlayingPlayList) return false;

    PlayCheckCounter--;
    if(PlayCheckCounter > 0) return false;

    PlayCheckCounter = PlayCheckFrequency;

    if(!IsMediaPresent()) return false;

   // if CD is in use, then return
   if(IsCDPlaying()) return false;

    // if CDs in use & playing the correct track, return
    //if(IsCDTrackPlaying(PlayList[CurrentTrack])) return false;

    // otherwise start playing
    CurrentTrack++;
    if(CurrentTrack >= NumTracks) CurrentTrack = 0;

    PlayCDTrack(PlayList[CurrentTrack]);


    return true;
}


bool
SoundSystem::StartPlayList(bool interrupt_current) {
if(!CDAudioSetupOK) return false;

    PlayingPlayList = true;
    PlayCheckCounter = PlayCheckFrequency;
    if(interrupt_current) PlayCDTrack(PlayList[CurrentTrack]);

    return true;
}

bool
SoundSystem::StopPlayList() {
if(!CDAudioSetupOK) return false;

    PlayingPlayList = false;
    PlayCheckCounter = 0;

    return true;

}




void SoundSystem::MCIError(MCIERROR mcierr) {

    String err_msg;
    char err_desc[256];

    mciGetErrorString(mcierr, err_desc, 255);

    switch(mcierr) {

        default : { err_msg = "Unknown Error\n"; break; }
    }

  // strcat(err_msg, err_desc);
    err_msg += err_desc;

#ifdef MSGBOX_ERRORS
    // if(err_msg != NULL) { MessageBox(app_hwnd, err_msg, "MCI Error", MB_OK); }
    MessageBox(app_hwnd, err_msg.c_str(), "MCI Error", MB_OK);
#endif

}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
