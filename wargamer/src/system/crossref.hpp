/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CROSSREF_HPP
#define CROSSREF_HPP

/*-------------------------------------------------------------------------------------------------------------------

        File : CROSSREF
        Description : Class to cross reference entries in a file
        
-------------------------------------------------------------------------------------------------------------------*/


#include "sysdll.h"

class SYSTEM_DLL CrossReferencer {

    private:

        char szSeperator[64];
        char szComment[64];
        char * FileBuffer;

    public:

        CrossReferencer(const char * filename);
        ~CrossReferencer(void);

        // define seperator between items (eg ' = ')
        bool DefineSeperator(const char * lpszStr);

        // define comment delimiter to start lines
        bool DefineComment(const char * lpszStr);

        char * GetDataStart(void);

        // int or char entries
        char * GetIntegerEntry(const char * lpszStr, int * retval, char * data_pos);
        char * GetCharacterEntry(const char * lpszStr, char * data_pos);

};


#endif
