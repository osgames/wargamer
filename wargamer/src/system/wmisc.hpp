/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef WMISC_H
#define WMISC_H

#ifndef __cplusplus
#error wmisc.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *      Miscellaneous support classes
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include <windows.h>
#include "myassert.hpp"
#include <stdio.h>
#include <stdarg.h>

class SimpleString;
class PixelRect;

namespace WindowsMisc
{

class SYSTEM_DLL ResString {
//        LPTSTR text;                    // Using TSTR allows it to work with UNICODE or ANSI
    String m_text;

#ifdef DEBUG
  public:
#endif
    // Prevent Copying

    ResString(const ResString& r);
    ResString& operator = (const ResString& r);

    void init(HINSTANCE hInst, UINT resId);
    // Initialise
  public:
    ResString(UINT resId);
    // Use APP::getInstance()

    ResString(HINSTANCE hInst, UINT resId) { init(hInst, resId); }
    // Load from specified Instance


    ~ResString();

    // operator LPTSTR() { return text; }
    const String& str() const { return m_text; }
    LPCTSTR c_str() const { return m_text.c_str(); }
};

void SYSTEM_DLL wTextOut(HDC hdc, int x, int y, LPCTSTR text);
void SYSTEM_DLL wTextOut(HDC hdc, int x, int y, const String& text);

void SYSTEM_DLL PASCAL wTextPrintf(HDC hdc, int x, int y, LPCTSTR text, ...);

void SYSTEM_DLL wTextOut(HDC hdc, int x, int y, int cx, const char* text);

void SYSTEM_DLL PASCAL wTextPrintf(HDC hdc, int x, int y, int cx, const char* text, ...);

void SYSTEM_DLL wTextOut(HDC hdc, const PixelRect& r, const char* text);

void SYSTEM_DLL wTextOut(HDC hdc, const PixelRect& r, const char* text, unsigned int flags);

void SYSTEM_DLL fillRECT(RECT& rect, int l, int t, int r, int b);

BOOL SYSTEM_DLL mouseInRect(RECT& rect, int mx, int my);

LONG SYSTEM_DLL textWidth(HDC hdc, LPCTSTR text);

LPVOID SYSTEM_DLL loadResource(HMODULE hMod, LPCTSTR lpName, LPCTSTR lpType);

void SYSTEM_DLL idsToString(SimpleString& s, UINT id);
// Getr a resource String into a SimpleString


// class ImageLibrary;




/*
 * Some common macros or inline functions
 */

inline HINSTANCE wndInstance(HWND hWnd)
{
  return (HINSTANCE) GetWindowLong(hWnd, GWL_HINSTANCE);
}


}              // namespace WindowsMisc

/*==========================================================
 * Wrapper for DeferWindowPosition functions
 */

class DeferWindowPosition {
    HDWP d_handle;
  public:
    DeferWindowPosition(int nWindows = 10)
    {
      d_handle = ::BeginDeferWindowPos(nWindows);
      ASSERT(d_handle != NULL);
    }

    ~DeferWindowPosition()
    {
      if(d_handle != NULL)
        ::EndDeferWindowPos(d_handle);
    }

    void setPos(HWND hWnd, HWND insAfter, int x, int y, int w, int h, UINT flags)
    {
      if(d_handle != NULL)
      {
        d_handle = ::DeferWindowPos(d_handle, hWnd, insAfter, x, y, w, h, flags);
        ASSERT(d_handle != NULL);
      }
    }
};


using namespace WindowsMisc;  // argh!!!!! pointless having namespace if you do this!


#endif /* WMISC_H */

/*
 *
 * $Log$
 * Revision 1.3  2001/07/30 09:27:23  greenius
 * Various bug fixes
 *
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.2  1996/02/16 17:34:14  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/18 11:06:49  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1995/10/18 10:59:53  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */
