/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "scrnbase.hpp"

struct BaseStruct {
  const UWORD d_baseX;
  const UWORD d_baseY;
  UWORD d_dbX;
  UWORD d_dbY;

  BaseStruct() :
	 d_baseX(40),
	 d_baseY(80),
	 d_dbX(0),
	 d_dbY(0) {}

  Boolean initialized() const
  {
	 return (d_dbX > 0 && d_dbY > 0);
  }
};

static BaseStruct s_bs;

void ScreenBase::calcBase()
{
#if 0
  const LONG screenCX = GetSystemMetrics(SM_CXSCREEN);    // LOWORD(dbUnits);
  const LONG screenCY = GetSystemMetrics(SM_CYSCREEN);    // HIWORD(dbUnits);

  const int base = 10;
  int m = 20;

  if(screenCX <= 640)
	 m = 15;
  else if(screenCX <= 800)
	 m = 16;
  else if(screenCX <= 1024)
	 m = 18;
  else if(screenCX <= 1152)
	 m = 19;

  s_bs.d_dbX = static_cast<UWORD>((s_bs.d_baseX * m) / base);
  s_bs.d_dbY = static_cast<UWORD>((s_bs.d_baseY * m) / base);
#else

    LONG dbUnits = GetDialogBaseUnits();
    LONG dbX = LOWORD(dbUnits);
    LONG dbY = HIWORD(dbUnits);

    // s_bs.d_dbX = MulDiv(dbX, s_bs.d_baseX, 4);
    s_bs.d_dbX = MulDiv(dbY, s_bs.d_baseX, 8);      // Maintain aspect ratio of pixels to dialog units
    s_bs.d_dbY = MulDiv(dbY, s_bs.d_baseY, 8);

#endif
}

UWORD ScreenBase::baseX()
{
  return s_bs.d_baseX;
}

UWORD ScreenBase::baseY()
{
  return s_bs.d_baseY;
}

UWORD ScreenBase::dbX()
{
  if(!s_bs.initialized())
	 calcBase();

  return s_bs.d_dbX;
}

UWORD ScreenBase::dbY()
{
  if(!s_bs.initialized())
	 calcBase();

  return s_bs.d_dbY;
}

int ScreenBase::x(int p_x)
{
	return MulDiv(p_x, dbX(), baseX());
}

int ScreenBase::y(int p_y)
{
	return MulDiv(p_y, dbY(), baseY());
}

