/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 * Thread Messages
 */

#include "stdinc.hpp"
#include "threadMsg.hpp"

#include "myassert.hpp"

namespace WargameMessage
{

void MessageBase::send()
{
   MessageQueue::instance()->put(this);
}




MessageQueue* MessageQueue::s_instance = 0;

MessageQueue* MessageQueue::instance()
{
   if(!s_instance)
      s_instance = new MessageQueue;
   return s_instance;
}

bool MessageQueue::isEmpty() const
{
   return d_messages.size() == 0;
}

MessageBase* MessageQueue::get()
{
   ASSERT(d_messages.size() > 0);

   MessageBase* msg = d_messages.front();
   d_messages.pop();

//   item->d_received.set();
//   MessageBase* msg = item->d_msg;
//   delete item;

   return msg;
}

void MessageQueue::put(MessageBase* msg)
{
//   MessageItem* item = new MessageItem;
//   item->d_msg = msg;
   d_messages.push(msg);
   d_msgInserted.set();
}


void MessageQueue::clear()
{
   while(!isEmpty())
   {
      MessageBase* msg = get();
      msg->clear();
   }
}




} // namespace

/*----------------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------------
 */


