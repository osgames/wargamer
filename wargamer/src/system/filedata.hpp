/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef FILEDATA_H
#define FILEDATA_H

#ifndef __cplusplus
#error filedata.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Filedata
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  1995/10/29 16:02:49  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1993/12/16  22:20:15  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"

#pragma pack ( push, 1 )

/*
 * Disk Format
 */

typedef union {
	UBYTE b[4];
} ILONG;

typedef union {
	UBYTE b[2];
} IWORD;

typedef struct {
	UBYTE b[1];
} IBYTE;

#pragma pack ( pop )


SYSTEM_DLL void putWord(IWORD* dest, UWORD src);
SYSTEM_DLL void putLong(ILONG* dest, ULONG src);

inline void putByte(IBYTE* dest, UBYTE src)
{
	dest->b[0] = src;
}

SYSTEM_DLL UWORD getWord(IWORD* src);
SYSTEM_DLL ULONG getLong(ILONG* src);

inline UBYTE getByte(IBYTE* src)
{
 	return src->b[0];
}


#endif /* FILEDATA_H */
