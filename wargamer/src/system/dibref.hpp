/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DIBREF_HPP
#define DIBREF_HPP

#ifndef __cplusplus
#error dibref.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	DIB pointer that is palette aware and counts instances
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "palette.hpp"
#include "dib.hpp"
#include "myassert.hpp"

/*
 * A DIB that is aware of Palette Changes
 */

class SYSTEM_DLL DibPal
{
	public:
		DibPal() : d_dib(0), d_palID(0), d_count(0) { }
		~DibPal() { ASSERT(d_count==0); delete d_dib; }

		void addRef() 
		{
			++d_count;
		}

		void delRef()
		{
			ASSERT(d_count > 0);
			if(!--d_count)
			{
				delete d_dib;
				d_dib = 0; 
			}
		}

		DrawDIBDC* operator->() { ASSERT(d_dib); return d_dib; }
		const DrawDIBDC* operator->() const { ASSERT(d_dib); return d_dib; }

		operator DrawDIBDC*() { ASSERT(d_dib); return d_dib; }
		operator const DrawDIBDC*() const { ASSERT(d_dib); return d_dib; }

		void allocate(int w, int h);


	private:
		DrawDIBDC* d_dib;
		Palette::PaletteID d_palID;
		int d_count;
};


#endif /* DIBREF_HPP */

