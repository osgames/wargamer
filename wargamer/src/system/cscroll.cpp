/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Custom Scroll Bar
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cscroll.hpp"
#include "palwind.hpp"
#include "bmp.hpp"
#include "dib.hpp"
#include "myassert.hpp"
#include "misc.hpp"
#include "wmisc.hpp"
#include "winctrl.hpp"
#include "palette.hpp"
#include "imglib.hpp"



namespace CustomScrollBar
{

const char CUSTOMSCROLLBARCLASS[] = "CUSTOMSCROLLBAR";

enum MessageID {
	CSBM_GETPOS = WM_USER,
	CSBM_SETPOS,
	CSBM_GETRANGE,
	CSBM_SETRANGE,
	CSBM_SETBTNICONS,
	CSBM_SETTHUMBBKGND,
	CSBM_SETSCROLLBKGND,
	CSBM_GETPAGE,
	CSBM_SETPAGE,
	CSBM_SETBORDER,
	CSBM_LAST
};


// int onGetPosition(HWND hwnd)
#define HANDLE_CSBM_GETPOS(hwnd, wParam, lParam, fn)	\
	(LRESULT)(fn)(hwnd)

// void onSetPosition(HWND hwnd, int pos);
#define HANDLE_CSBM_SETPOS(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, (int) wParam), 0L)

// void onGetRange(HWND hwnd, LPINT min, LPINT max);
#define HANDLE_CSBM_GETRANGE(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, (LPINT) wParam, (LPINT) lParam), 0L)

// void onSetRange(HWND hwnd, int min, int max);
#define HANDLE_CSBM_SETRANGE(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, (int) wParam, (int) lParam), 0L)

// void onSetButtonIcons(HWND hwnd, const ImageLibrary* il)
#define HANDLE_CSBM_SETBTNICONS(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, reinterpret_cast<const ImageLibrary*>(lParam)), 0L)

// void onSetThumbBk(HWND hwnd, const DrawDIB* thumbBk)
#define HANDLE_CSBM_SETTHUMBBKGND(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, reinterpret_cast<const DrawDIB*>(lParam)), 0L)

// void onSetScrollBk(HWND hwnd, const DrawDIB* scrollBk)
#define HANDLE_CSBM_SETSCROLLBKGND(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, reinterpret_cast<const DrawDIB*>(lParam)), 0L)

// int onGetPage(HWND hwnd)
#define HANDLE_CSBM_GETPAGE(hwnd, wParam, lParam, fn)	\
	(LRESULT)(fn)(hwnd)

// void onSetPage(HWND hwnd, int pos);
#define HANDLE_CSBM_SETPAGE(hwnd, wParam, lParam, fn)	\
	((fn)(hwnd, (int) wParam), 0L)

// void onSetBorder(HWND hwnd, const CustomBorderInfo* info)
#define HANDLE_CSBM_SETBORDER(hwnd, wParam, lParam, fn) \
	((fn)(hwnd, reinterpret_cast<CustomBorderInfo*>(lParam)), 0L)

class Util {
public:
  static const int s_defaultButtonCX;
  static const int s_defaultButtonCY;
};

const int Util::s_defaultButtonCX = 15;
const int Util::s_defaultButtonCY = 15;

struct CustomScrollBarInfo {
	int d_nMin;
	int d_nMax;
	int d_nPage;
	int d_nPos;

	CustomScrollBarInfo() :
	  d_nMin(0),
	  d_nMax(0),
	  d_nPage(0),
	  d_nPos(0) {}
};

struct CustomScrollBarImp	// : public CustomBorderWindow {
{
	HWND d_hParent;
	HWND d_hControl;
	HWND d_hPrevCapture;    // handle of window that previously captured mouse

	CustomScrollBarInfo d_info;
	CustomBorderInfo	d_borderColors;

	LONG d_style;

	const ImageLibrary* d_buttonIcons;
	const DrawDIB* d_thumbBkDib;
	const DrawDIB* d_scrollBkDib;
	DrawDIBDC* d_scrollDib;

	enum Mode {
		Mode_First = 0,
		UpButton = Mode_First,
		UpPage,
		ThumbButton,
		DownPage,
		DownButton,

		Mode_HowMany,

		NotClicked = Mode_HowMany,

		Mode_Last
	} d_mode;

	POINT d_lastPos;
	RECT d_rects[Mode_HowMany];

	CustomScrollBarImp();
	~CustomScrollBarImp();

	void resetMode() { d_mode = NotClicked; }

#ifdef DEBUG_SCROLL
	void validateThumbRect();
#endif

//	void drawThumb(const DRAWITEMSTRUCT* lpDrawItem);
};


static CustomBorderInfo defaultBorderColors = {
	PALETTERGB(189, 165, 123),
	PALETTERGB(255, 247, 215),
	PALETTERGB(230, 214, 123),
	PALETTERGB(115, 90, 66)
};


CustomScrollBarImp::CustomScrollBarImp() :
  d_hParent(0),
  d_hControl(0),
  d_hPrevCapture(0),
  d_mode(NotClicked),
  d_style(0),
  d_buttonIcons(0),
  d_thumbBkDib(0),
  d_scrollBkDib(0),
  d_scrollDib(0)
{
  d_borderColors = defaultBorderColors;

  for(int i = 0; i < Mode_HowMany; i++)
  {
	 SetRect(&d_rects[i], 0, 0, 0, 0);
  }

  d_lastPos.x = 0;
  d_lastPos.y = 0;
}

CustomScrollBarImp::~CustomScrollBarImp()
{
  if(d_scrollDib)
	 delete d_scrollDib;
}

#if 0
#ifdef DEBUG
void CustomScrollBarImp::validateThumbRect()
{
#if 0
	ASSERT(thumbRect.top >= 0);
	if(thumbRect.top < 0)
		thumbRect.top = 0;
	ASSERT(thumbRect.left >= 0);
	if(thumbRect.left < 0)
		thumbRect.left = 0;
	ASSERT(thumbRect.bottom >= thumbRect.top);
	if(thumbRect.bottom <= thumbRect.top)
		thumbRect.bottom = thumbRect.top + 1;
	ASSERT(thumbRect.right >= thumbRect.left);
	if(thumbRect.right <= thumbRect.left)
		thumbRect.right = thumbRect.left + 1;
#endif
}
#endif
#endif

static BOOL csbInitialised = FALSE;

CustomScrollBarImp* csb_getData(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = (CustomScrollBarImp*)GetWindowLong(hwnd, GWL_USERDATA);
  ASSERT(csb != 0);

  return csb;
}

CustomScrollBarInfo* csb_getInfoData(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = (CustomScrollBarImp*)GetWindowLong(hwnd, GWL_USERDATA);
  ASSERT(csb != 0);

  return &csb->d_info;
}

void csb_resetButtonPos(CustomScrollBarImp* csb)
{
  ASSERT(csb);
  ASSERT(csb->d_scrollDib);

  if((csb->d_style & (SBS_VERT | SBS_HORZ)) == SBS_VERT)
  {
	 SetRect(&csb->d_rects[CustomScrollBarImp::UpButton], 0, 0, csb->d_scrollDib->getWidth(), Util::s_defaultButtonCY);
	 SetRect(&csb->d_rects[CustomScrollBarImp::DownButton], 0, csb->d_scrollDib->getHeight()-Util::s_defaultButtonCY, csb->d_scrollDib->getWidth(), csb->d_scrollDib->getHeight());
  }
  else
  {
	 SetRect(&csb->d_rects[CustomScrollBarImp::UpButton], 0, 0, Util::s_defaultButtonCX, csb->d_scrollDib->getHeight());
	 SetRect(&csb->d_rects[CustomScrollBarImp::DownButton], csb->d_scrollDib->getWidth()-Util::s_defaultButtonCX, 0, csb->d_scrollDib->getWidth(), csb->d_scrollDib->getHeight());
  }

}

void csb_allocateDib(CustomScrollBarImp* csb, int cx, int cy)
{
  if(!csb->d_scrollDib ||
	  csb->d_scrollDib->getWidth() != cx ||
	  csb->d_scrollDib->getHeight() != cy)
  {
	 if(csb->d_scrollDib)
	 {
		delete csb->d_scrollDib;
		csb->d_scrollDib = 0;
	 }

	 csb->d_scrollDib = new DrawDIBDC(cx, cy);
  }

  ASSERT(csb->d_scrollDib);
  csb_resetButtonPos(csb);
}


UWORD csb_getIcon(CustomScrollBarImp::Mode mode, Boolean buttonPressed)
{
  if(mode == CustomScrollBarImp::UpButton)
	 return buttonPressed ? 2 : 0;
  else
	 return buttonPressed ? 3 : 1;
}

void csb_drawButton(CustomScrollBarImp* csb, CustomScrollBarImp::Mode mode)
{
  ASSERT(csb);
  ASSERT(mode == CustomScrollBarImp::UpButton || mode == CustomScrollBarImp::DownButton);

  if(csb->d_buttonIcons)
  {
	 const RECT& r = csb->d_rects[mode];

	 csb->d_buttonIcons->stretchBlit(csb->d_scrollDib,
		csb_getIcon(mode, (mode == csb->d_mode)),
		r.left, r.top, r.right-r.left, r.bottom-r.top);

	 InvalidateRect(csb->d_hControl, &r, FALSE);
  }
}

void csb_drawPage(CustomScrollBarImp* csb, CustomScrollBarImp::Mode mode)
{
  ASSERT(csb);
  ASSERT(csb->d_scrollDib);
  ASSERT(mode == CustomScrollBarImp::UpPage || mode == CustomScrollBarImp::DownPage);

  const RECT& r = csb->d_rects[mode];

  if(mode == csb->d_mode)
  {
	 ColourIndex ci = csb->d_scrollDib->getColour(PALETTERGB(0, 0, 0));
	 csb->d_scrollDib->rect(r.left, r.top, r.right-r.left, r.bottom-r.top, ci);
  }
  else
  {
      csb->d_scrollDib->rect(r.left, r.top, r.right-r.left, r.bottom-r.top, csb->d_scrollBkDib);
  }


  InvalidateRect(csb->d_hControl, &r, FALSE);
}

void csb_drawThumb(CustomScrollBarImp* csb)
{
  ASSERT(csb);
  ASSERT(csb->d_scrollDib);

  if(csb->d_thumbBkDib)
  {
	 RECT& r = csb->d_rects[CustomScrollBarImp::ThumbButton];
	 csb->d_scrollDib->rect(r.left, r.top, r.right-r.left, r.bottom-r.top, csb->d_thumbBkDib);

	 // draw borders
	 CustomBorderWindow cw(csb->d_borderColors);
	 cw.drawThinBorder(csb->d_scrollDib->getDC(), r);

	 InvalidateRect(csb->d_hControl, &r, FALSE);
  }
}

void csb_drawScroll(CustomScrollBarImp* csb)
{
  ASSERT(csb);
  ASSERT(csb->d_scrollDib);

  if(csb->d_scrollBkDib)
  {
	 // fill in background
	 RECT r;
	 GetClientRect(csb->d_hControl, &r);

	 int x = 0;
	 int y = 0;
	 int w = 0;
	 int h = 0;

	 if(csb->d_style & SBS_VERT)
	 {
		y = Util::s_defaultButtonCY;
		w = csb->d_scrollDib->getWidth();
		h = (r.bottom-r.top)-(2*Util::s_defaultButtonCY);
	 }
	 else
	 {
		x = Util::s_defaultButtonCX;
		w = (r.right-r.left)-(2*Util::s_defaultButtonCX);
		h = csb->d_scrollDib->getHeight();
	 }

	 csb->d_scrollDib->rect(x, y, w, h, csb->d_scrollBkDib);
  }

  csb_drawPage(csb, CustomScrollBarImp::UpPage);
  csb_drawPage(csb, CustomScrollBarImp::DownPage);
  csb_drawThumb(csb);

  UpdateWindow(csb->d_hControl);
}

/*
 * Recalculates page and thumb rects
 *
 * NB from Jim : this calculation line was causing overflows on scrollbars with with a large
 * range and/or a large CX or CY values.  I've changed it to an unsigned value AND have
 * halved two of the terms in the calculation.. but this will still cause overflows on
 * very large ranges and/or very large screen resolutions !!
 * (I also had to replace the mix & max template operators which were reporting comparison errors)
 */

void csb_resetThumb(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = csb_getData(hwnd);
  CustomScrollBarInfo* csbi = csb_getInfoData(hwnd);

  RECT r;
  GetClientRect(hwnd, &r);

  const LONG range = csbi->d_nMax - csbi->d_nMin;
  const LONG pos = csbi->d_nPos - csbi->d_nMin;

  // set thumb size
  if(range > 0 && csbi->d_nPage > 0)
  {
	 if(csb->d_style & SBS_VERT)
	 {
		const int scrollCY = (r.bottom-r.top)-(2*Util::s_defaultButtonCY);
		const int thumbHeight = (range > csbi->d_nPage) ? (scrollCY * csbi->d_nPage) / range : scrollCY;

		// NB :this replaced
		/*
		int y = Util::s_defaultButtonCY + ((scrollCY * pos) / range);
		y = maximum(Util::s_defaultButtonCY, y);
		y = minimum(y, static_cast<int>((r.bottom-Util::s_defaultButtonCY)-thumbHeight));
		*/

		// NB : with this
		unsigned int y = Util::s_defaultButtonCY + static_cast<unsigned int>( ((float)scrollCY*((float)pos/2.0f)) / (((float)range)/2.0f) ) ;
		if(y < Util::s_defaultButtonCY) 
         y = Util::s_defaultButtonCY;
		if(y > ((r.bottom-Util::s_defaultButtonCY)-thumbHeight))
         y = ((r.bottom-Util::s_defaultButtonCY)-thumbHeight);


		SetRect(&csb->d_rects[CustomScrollBarImp::UpPage], 0, Util::s_defaultButtonCY, r.right-r.left, y);
		SetRect(&csb->d_rects[CustomScrollBarImp::ThumbButton], 0, y, r.right-r.left, y + thumbHeight);
		SetRect(&csb->d_rects[CustomScrollBarImp::DownPage], 0, y + thumbHeight, r.right-r.left, r.bottom-Util::s_defaultButtonCY);
	 }
	 else
	 {
		const int scrollCX = (r.right-r.left)-(2*Util::s_defaultButtonCX);
		const int thumbWidth = (range > csbi->d_nPage) ? (scrollCX * csbi->d_nPage) / range : scrollCX;

		// NB :these replaced
		/*
		int x = Util::s_defaultButtonCX + ((scrollCX * pos) / range);
		x = maximum(Util::s_defaultButtonCX, x);
		x = minimum(x, static_cast<int>((r.right-Util::s_defaultButtonCX)-thumbWidth));
		*/


		// NB : with this
		unsigned int x = Util::s_defaultButtonCX + ( ((float)scrollCX*((float)pos/2.0f)) / (((float)range)/2.0f) );
		if(x < Util::s_defaultButtonCX) x = Util::s_defaultButtonCX;
		if(x > ((r.right-Util::s_defaultButtonCX)-thumbWidth)) x = ((r.right-Util::s_defaultButtonCX)-thumbWidth);
		
		
		SetRect(&csb->d_rects[CustomScrollBarImp::UpPage], Util::s_defaultButtonCX, 0, x, r.bottom-r.top);
		SetRect(&csb->d_rects[CustomScrollBarImp::ThumbButton], x, 0, x + thumbWidth, r.bottom-r.top);
		SetRect(&csb->d_rects[CustomScrollBarImp::DownPage], x + thumbWidth, 0, r.right-Util::s_defaultButtonCX, r.bottom-r.top);
	 }
  }

  // redraw
  csb_drawScroll(csb);

}

#ifdef DEBUG_SCROLL
static int nWndInstances = 0;
#endif

BOOL csb_onCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = new CustomScrollBarImp;
  ASSERT(csb != 0);
  if(csb == 0)
	 return FALSE;

  SetWindowLong(hwnd, GWL_USERDATA, (LONG) csb);

  csb->d_hParent = lpCreateStruct->hwndParent;
  csb->d_hControl = hwnd;
  csb->d_style = lpCreateStruct->style;

  csb_allocateDib(csb, lpCreateStruct->cx, lpCreateStruct->cy);

  return TRUE;
}

void csb_doButtonClicked(CustomScrollBarImp* csb)
{
  ASSERT(csb);
  ASSERT(csb->d_buttonIcons);
  ASSERT(csb->d_mode == CustomScrollBarImp::UpButton || csb->d_mode == CustomScrollBarImp::DownButton);

  if(csb->d_mode == CustomScrollBarImp::UpButton)
  {
	 if(csb->d_info.d_nPos > csb->d_info.d_nMin)
		SendMessage(csb->d_hParent, (csb->d_style & SBS_VERT) ? WM_VSCROLL : WM_HSCROLL, MAKEWPARAM(SB_LINEUP, --csb->d_info.d_nPos), (LPARAM)csb->d_hControl);
  }
  else
  {
	 if(csb->d_info.d_nPos < csb->d_info.d_nMax)
		SendMessage(csb->d_hParent, (csb->d_style & SBS_VERT) ? WM_VSCROLL : WM_HSCROLL, MAKEWPARAM(SB_LINEDOWN, ++csb->d_info.d_nPos), (LPARAM)csb->d_hControl);
  }
}


void csb_doPageClicked(CustomScrollBarImp* csb)
{
  ASSERT(csb);
  ASSERT(csb->d_scrollDib);
  ASSERT(csb->d_scrollBkDib);
  ASSERT(csb->d_mode == CustomScrollBarImp::UpPage || csb->d_mode == CustomScrollBarImp::DownPage);

  if(csb->d_mode == CustomScrollBarImp::UpPage)
  {
	 csb->d_info.d_nPos -= csb->d_info.d_nPage;
	 csb->d_info.d_nPos = maximum(csb->d_info.d_nMin, csb->d_info.d_nPos);

	 SendMessage(csb->d_hParent, (csb->d_style & SB_VERT) ? WM_VSCROLL : WM_HSCROLL, MAKEWPARAM(SB_PAGEUP, csb->d_info.d_nPos), (LPARAM)csb->d_hControl);
  }
  else
  {
	 csb->d_info.d_nPos += csb->d_info.d_nPage;
	 csb->d_info.d_nPos = minimum(csb->d_info.d_nPos, (csb->d_info.d_nMax - csb->d_info.d_nPage));

	 SendMessage(csb->d_hParent, (csb->d_style & SBS_VERT) ? WM_VSCROLL : WM_HSCROLL, MAKEWPARAM(SB_PAGEDOWN, csb->d_info.d_nPos), (LPARAM)csb->d_hControl);
  }
}

void csb_onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = csb_getData(hwnd);
  ASSERT(csb);

  csb->d_hPrevCapture = SetCapture(hwnd);

  Boolean found = False;

  POINT p;
  p.x = x;
  p.y = y;

  for(CustomScrollBarImp::Mode i = CustomScrollBarImp::Mode_First;
		i < CustomScrollBarImp::Mode_HowMany; INCREMENT(i))
  {
	 if(PtInRect(&csb->d_rects[i], p))
	 {
		csb->d_mode = i;
		found = True;
		break;
	 }
  }

  if(!found)
	 csb->d_mode = CustomScrollBarImp::NotClicked;
  else
  {
	 ASSERT(csb->d_mode < CustomScrollBarImp::Mode_HowMany);

	 switch(csb->d_mode)
	 {
		case CustomScrollBarImp::UpButton:
		case CustomScrollBarImp::DownButton:
		{
		  /*
			* Set timer so user can hold button down for auto scroll
			*/
		  const int timeOut = 100;
		  UINT result = SetTimer(csb->d_hControl, csb->d_mode, timeOut, NULL);
		  ASSERT(result);

		  csb_drawButton(csb, csb->d_mode);
		  UpdateWindow(csb->d_hControl);
		  csb_doButtonClicked(csb);
		  break;
		}

		case CustomScrollBarImp::UpPage:
		case CustomScrollBarImp::DownPage:
		  csb_drawPage(csb, csb->d_mode);
		  UpdateWindow(csb->d_hControl);

		  csb_doPageClicked(csb);
		  break;

	 }
  }
}

void csb_moveThumb(CustomScrollBarImp* csb, int difference)
{
  ASSERT(csb);

  RECT r;
  GetClientRect(csb->d_hControl, &r);

  const LONG range = csb->d_info.d_nMax - csb->d_info.d_nMin;
  int divisor = (csb->d_style & SBS_VERT) ?
		(r.bottom - r.top) - (2*Util::s_defaultButtonCY) :
		(r.right - r.left) - (2*Util::s_defaultButtonCX);

  int movePos = (divisor != 0) ? (difference * range) / divisor : 0;

  csb->d_info.d_nPos += movePos;
  csb->d_info.d_nPos = maximum(csb->d_info.d_nMin, csb->d_info.d_nPos);
  csb->d_info.d_nPos = minimum(csb->d_info.d_nMax - csb->d_info.d_nPage, csb->d_info.d_nPos);

  csb_resetThumb(csb->d_hControl);

}

void csb_onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = csb_getData(hwnd);
  ASSERT(csb);

  if(csb->d_mode == CustomScrollBarImp::ThumbButton)
  {
	 int difference = (csb->d_style & SBS_VERT) ? y - csb->d_lastPos.y : x - csb->d_lastPos.x;

	 if(difference != 0)
		csb_moveThumb(csb, difference);
  }

  csb->d_lastPos.x = x;
  csb->d_lastPos.y = y;
}

void csb_onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = csb_getData(hwnd);
  ASSERT(csb);

  CustomScrollBarImp::Mode oldMode = csb->d_mode;
  csb->resetMode();

  ASSERT(oldMode <= CustomScrollBarImp::Mode_HowMany);

  if(oldMode != CustomScrollBarImp::NotClicked)
  {
	 switch(oldMode)
	 {
		case CustomScrollBarImp::UpButton:
		case CustomScrollBarImp::DownButton:
		{
		  BOOL result = KillTimer(hwnd, oldMode);
		  ASSERT(result);

		  csb_drawButton(csb, oldMode);
		  UpdateWindow(csb->d_hControl);
		  break;
		}

		case CustomScrollBarImp::UpPage:
		case CustomScrollBarImp::DownPage:
		{
		  csb_drawPage(csb, oldMode);
		  UpdateWindow(csb->d_hControl);
		  break;
		}

		case CustomScrollBarImp::ThumbButton:
		{
#if 0
		  const int startX = csb->d_startDragPos.x;
		  const int startY = csb->d_startDragPos.y;
		  const LONG range = csb->d_info.d_nMax - csb->d_info.d_nMin;

		  RECT r;
		  GetClientRect(hwnd, &r);

		  int difference = 0;
		  int movePos = 0;

		  if(csb->d_style & SBS_VERT)
		  {
			 const int scrollCY = (r.bottom - r.top) - (2*Util::s_defaultButtonCY);
			 difference = y - startY;

			 if(scrollCY > 0)
			 {
				movePos = (difference * range) / scrollCY;
			 }
		  }
		  else
		  {
			 const int scrollCX = (r.right - r.left) - (2*Util::s_defaultButtonCX);
			 difference = x - startX;

			 if(scrollCX > 0)
			 {
				movePos = (difference * range) / scrollCX;
			 }
		  }

		  csb->d_info.d_nPos += movePos;
		  csb->d_info.d_nPos = maximum(csb->d_info.d_nMin, csb->d_info.d_nPos);
		  csb->d_info.d_nPos = minimum(csb->d_info.d_nMax - csb->d_info.d_nPage, csb->d_info.d_nPos);
#endif
		  SendMessage(csb->d_hParent, (csb->d_style & SBS_VERT) ? WM_VSCROLL : WM_HSCROLL, MAKEWPARAM(SB_THUMBPOSITION, csb->d_info.d_nPos), (LPARAM)csb->d_hControl);

		  break;
		}
	 }

  		SetForegroundWindow(csb->d_hParent);
  		if(csb->d_hPrevCapture)
  		{
	 		SetCapture(csb->d_hPrevCapture);
	 		csb->d_hPrevCapture = 0;
  		}
  		else
  		{
	 		BOOL result = ReleaseCapture();
    		ASSERT(result);
  		}
  }
}

void csb_onSize(HWND hwnd, UINT state, int cx, int cy)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = csb_getData(hwnd);
  ASSERT(csb);

  /*
	* Hide scroll bar if size is less than both buttons
	*/

  if(csb->d_style & SBS_VERT)
  {
	 if(cy < Util::s_defaultButtonCY*2)
	 {
		ShowWindow(hwnd, SW_HIDE);
		return;
	 }
  }
  else
  {
	 if(cx < Util::s_defaultButtonCX*2)
	 {
		ShowWindow(hwnd, SW_HIDE);
		return;
	 }
  }

  // reallocate dib in needed
  csb_allocateDib(csb, cx, cy);
  csb_drawButton(csb, CustomScrollBarImp::UpButton);
  csb_drawButton(csb, CustomScrollBarImp::DownButton);
  csb_resetThumb(hwnd);
}

int csb_onGetPosition(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  CustomScrollBarInfo* csbi = csb_getInfoData(hwnd);
  return csbi->d_nPos;
}

void csb_onSetPosition(HWND hwnd, int pos)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  CustomScrollBarInfo* csbi = csb_getInfoData(hwnd);
  csbi->d_nPos = pos;

  csb_resetThumb(hwnd);
}

void csb_onGetRange(HWND hwnd, LPINT min, LPINT max)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  CustomScrollBarInfo* csbi = csb_getInfoData(hwnd);
  *min = csbi->d_nMin;
  *max = csbi->d_nMax;
}

void csb_onSetRange(HWND hwnd, int min, int max)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  CustomScrollBarInfo* csbi = csb_getInfoData(hwnd);
  csbi->d_nMin = min;
  csbi->d_nMax = max;

  csb_resetThumb(hwnd);
}

void csb_onSetButtonIcons(HWND hwnd, const ImageLibrary* il)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  CustomScrollBarImp* csb = csb_getData(hwnd);
  csb->d_buttonIcons = il;
  csb_drawButton(csb, CustomScrollBarImp::UpButton);
  csb_drawButton(csb, CustomScrollBarImp::DownButton);
}

void csb_onSetThumbBk(HWND hwnd, const DrawDIB* thumbBkDib)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  CustomScrollBarImp* csb = csb_getData(hwnd);
  csb->d_thumbBkDib = thumbBkDib;
  csb_drawThumb(csb);
}

void csb_onSetScrollBk(HWND hwnd, const DrawDIB* scrollBkDib)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  CustomScrollBarImp* csb = csb_getData(hwnd);
  csb->d_scrollBkDib = scrollBkDib;
  csb_drawScroll(csb);
}

int csb_onGetPage(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  CustomScrollBarInfo* csbi = csb_getInfoData(hwnd);
  return csbi->d_nPage;
}

void csb_onSetPage(HWND hwnd, int page)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  CustomScrollBarInfo* csbi = csb_getInfoData(hwnd);
  csbi->d_nPage = page;

  csb_resetThumb(hwnd);
}


void csb_onSetBorder(HWND hwnd, const CustomBorderInfo* info)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  ASSERT(info != 0);

  CustomScrollBarImp* csb = csb_getData(hwnd);
  csb->d_borderColors = *info;
}

BOOL csb_onEraseBk(HWND hwnd, HDC hdc)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  return TRUE;
}

void csb_onPaint(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = csb_getData(hwnd);
  ASSERT(csb);

  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  if(csb->d_scrollDib)
	 BitBlt(hdc, 0, 0, csb->d_scrollDib->getWidth(), csb->d_scrollDib->getHeight(),
		 csb->d_scrollDib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);
  EndPaint(hwnd, &ps);
}

void csb_onDestroy(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = csb_getData(hwnd);
  ASSERT(csb);

  delete csb;
  SetWindowLong(hwnd, GWL_USERDATA, 0);
}

void csb_onTimer(HWND hwnd, UINT id)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  CustomScrollBarImp* csb = csb_getData(hwnd);
  ASSERT(csb);
  ASSERT(csb->d_mode == CustomScrollBarImp::UpButton || csb->d_mode == CustomScrollBarImp::DownButton);

  csb_doButtonClicked(csb);
}

LRESULT CALLBACK customScrollBarProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	LRESULT l;
	if(PaletteWindow::handlePalette(hWnd, msg, wParam, lParam, l))
		return l;

	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	csb_onCreate);
		HANDLE_MSG(hWnd, WM_DESTROY,	csb_onDestroy);
		HANDLE_MSG(hWnd, WM_PAINT,		csb_onPaint);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, csb_onMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, csb_onLButtonDown);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, csb_onLButtonUp);
		HANDLE_MSG(hWnd, WM_SIZE, csb_onSize);
		HANDLE_MSG(hWnd, WM_ERASEBKGND, csb_onEraseBk);
		HANDLE_MSG(hWnd, WM_TIMER, csb_onTimer);

		HANDLE_MSG(hWnd, CSBM_GETPOS, csb_onGetPosition);
		HANDLE_MSG(hWnd, CSBM_SETPOS, csb_onSetPosition);
		HANDLE_MSG(hWnd, CSBM_GETRANGE, csb_onGetRange);
		HANDLE_MSG(hWnd, CSBM_SETRANGE, csb_onSetRange);
		HANDLE_MSG(hWnd, CSBM_SETBTNICONS, csb_onSetButtonIcons);
		HANDLE_MSG(hWnd, CSBM_SETTHUMBBKGND, csb_onSetThumbBk);
		HANDLE_MSG(hWnd, CSBM_SETSCROLLBKGND, csb_onSetScrollBk);
		HANDLE_MSG(hWnd, CSBM_GETPAGE, csb_onGetPage);
		HANDLE_MSG(hWnd, CSBM_SETPAGE, csb_onSetPage);
		HANDLE_MSG(hWnd, CSBM_SETBORDER, csb_onSetBorder);
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}





/*=================================================================
 * Interface Functions
 */



int csb_getPosition(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  return SendMessage(hwnd, CSBM_GETPOS, 0, 0);
}

void

csb_setPosition(HWND hwnd, int pos)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  SendMessage(hwnd, CSBM_SETPOS, (WPARAM)pos, 0);
}

void

csb_getRange(HWND hwnd, LPINT min, LPINT max)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  SendMessage(hwnd, CSBM_GETRANGE, (WPARAM)(LPINT)min, (LPARAM)(LPINT)max);
}

void

csb_setRange(HWND hwnd, int min, int max)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  SendMessage(hwnd, CSBM_SETRANGE, (WPARAM)min, (LPARAM)max);
}

void

csb_setButtonIcons(HWND hwnd, const ImageLibrary* il)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  SendMessage(hwnd, CSBM_SETBTNICONS, 0, reinterpret_cast<LPARAM>(il));
}

void

csb_setThumbBk(HWND hwnd, const DrawDIB* dib)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  SendMessage(hwnd, CSBM_SETTHUMBBKGND, 0, reinterpret_cast<LPARAM>(dib));
}

void

csb_setScrollBk(HWND hwnd, const DrawDIB* dib)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  SendMessage(hwnd, CSBM_SETSCROLLBKGND, 0, reinterpret_cast<LPARAM>(dib));
}

int

csb_getPage(HWND hwnd)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  return SendMessage(hwnd, CSBM_GETPAGE, 0, 0);
}

void

csb_setPage(HWND hwnd, int page)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);

  SendMessage(hwnd, CSBM_SETPAGE, (WPARAM)page, 0);
}

void

csb_setBorder(HWND hwnd, const CustomBorderInfo* border)
{
  ASSERT_CLASS(hwnd, CUSTOMSCROLLBARCLASS);
  SendMessage(hwnd, CSBM_SETBORDER, 0, reinterpret_cast<LPARAM>(border));
}

void

initCustomScrollBar(HINSTANCE instance)
{
	if(!csbInitialised)
	{
		csbInitialised = TRUE;

		WNDCLASSEX wc;

		wc.cbSize			= sizeof(WNDCLASSEX);
		wc.style 			= /* CS_DBLCLKS | */ CS_PARENTDC;
		wc.lpfnWndProc 	= customScrollBarProc;
		wc.cbClsExtra 		= 0;
		// wc.cbWndExtra 		= sizeof(PopupIconData*);
		wc.cbWndExtra 		= 0;
		wc.hInstance 		= instance;
		wc.hIcon 			= NULL;
		wc.hCursor 			= LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground 	= NULL; //(HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName 	= NULL;
		wc.lpszClassName 	= CUSTOMSCROLLBARCLASS;
		wc.hIconSm 			= NULL;

		RegisterClassEx(&wc);
	}
}

HWND  csb_create(
		DWORD  dwExStyle,
		DWORD  dwStyle,
		int  x,
		int  y,
		int  nWidth,
		int  nHeight,
		HWND  hWndParent,
		HINSTANCE  hInstance)
{
	return CreateWindowEx(
		dwExStyle,
		CUSTOMSCROLLBARCLASS,
		0,
		dwStyle,
		x, y, nWidth, nHeight,
		hWndParent,
		NULL,
		hInstance,
		0);
}

};			// namespace CustomScrollBar

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */

