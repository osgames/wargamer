/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Bezier Engine
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "bezier.hpp"
#include "pt_util.hpp"
#include "simpstk.hpp"
/*
 * 3 Point Bezier with less lines
 */

BezierEngine::BezierEngine(GraphicEngine& output) :
	d_output(output),
	d_nPoints(0)
{
}

BezierEngine::~BezierEngine()
{
}


void BezierEngine::addPoint(const GPoint& point)
{
	if(d_nPoints < 3)
	{
		if(d_nPoints == 0)
			d_output.addPoint(point);

		d_points[d_nPoints++] = point;
	}
	else
	{
		// Split last 2 points

		GPoint mid = midPoint(d_points[1], d_points[2]);

		// Do Bezier of 0,1,mid

		GPoint bPoints[3];
		bPoints[0] = d_points[0];
		bPoints[1] = d_points[1];
		bPoints[2] = mid;

		doBezier(bPoints, 3);

		// Set up points to mid,2,point

		d_points[0] = mid;
		d_points[1] = d_points[2];
		d_points[2] = point;
	}
}

void BezierEngine::close()
{
	if(d_nPoints == 3)
		doBezier(d_points, d_nPoints);
	else
	{
		for(int i = 1; i < d_nPoints; ++i)
			d_output.addPoint(d_points[i]);
	}

	d_nPoints = 0;

	d_output.close();
}


#ifdef _MSC_VER
// Visual C++ won't allow templates of locally defined classes; error C2926
struct StackItem
{
	GraphicEngine::GPoint d_points[3];
};
#endif

void BezierEngine::doBezier(const GPoint* points, int nPoints)
{
	/*
	 * This can be optimized further by avoiding having to push
	 * the first item, and the 2nd curve in the loop.
	 */

	ASSERT(nPoints == 3);


#ifndef _MSC_VER
	struct StackItem
	{
		GPoint d_points[3];
	};
#endif

	SimpleStack<StackItem, 10> stack;

	StackItem item;
	item.d_points[0] = points[0];
	item.d_points[1] = points[1];
	item.d_points[2] = points[2];

	for(;;)
	{
		int dist = pointFromLine(item.d_points[0], item.d_points[2], item.d_points[1]);

		if(dist < 1)
		{
			d_output.addPoint(item.d_points[2]);
			if(stack.isEmpty())
				break;
			else
				stack.pop(item);
		}
		else
		{
			GPoint pa = midPoint(item.d_points[0], item.d_points[1]);
			GPoint pb = midPoint(item.d_points[1], item.d_points[2]);
			GPoint pc = midPoint(pa, pb);

			StackItem newItem;

			newItem.d_points[0] = pc;
			newItem.d_points[1] = pb;
			newItem.d_points[2] = item.d_points[2];
			stack.push(newItem);

			item.d_points[1] = pa;
			item.d_points[2] = pc;
		}
	}
}

#if 0		// Experimental

/*
 * Helper class to avoid infinite recursion
 */

class RecurseCount
{
	public:
		RecurseCount(int& counter) :
			d_counter(&counter)
		{
			++*d_counter;
		}

		~RecurseCount()
		{
			--*d_counter;
		}

		bool tooHigh(int maxValue, CString title) const
		{
			if(*d_counter > maxValue)
			{
				// AfxMessageBox("recursion too high");
				::MessageBox(NULL, "Recursion too high", title, MB_OK);
				return true;
			}
			return false;
		}

	private:
		int* d_counter;
};


/*
 * 3 Point Bezier
 */

BezierEngine3::BezierEngine3(GraphicEngine& output) :
	d_output(output),
	d_nPoints(0)
{
}

BezierEngine3::~BezierEngine3()
{
}


void BezierEngine3::addPoint(const GPoint& point)
{
	if(d_nPoints < 3)
	{
		if(d_nPoints == 0)
			d_output.addPoint(point);

		d_points[d_nPoints++] = point;
	}
	else
	{
		// Split last 2 points

		GPoint mid = midPoint(d_points[1], d_points[2]);

		// Do Bezier of 0,1,mid

		GPoint bPoints[3];
		bPoints[0] = d_points[0];
		bPoints[1] = d_points[1];
		bPoints[2] = mid;

		doBezier(bPoints, 3);

		// Set up points to mid,2,point

		d_points[0] = mid;
		d_points[1] = d_points[2];
		d_points[2] = point;
	}
}

void BezierEngine3::close()
{
	if(d_nPoints == 3)
		doBezier(d_points, d_nPoints);
	else
	{
		for(int i = 1; i < d_nPoints; ++i)
			d_output.addPoint(d_points[i]);
	}

	d_output.close();
}


void BezierEngine3::doBezier(const GPoint* points, int nPoints)
{
	static int recursion = 0;

	RecurseCount rc(recursion);

	if(rc.tooHigh(20, "Bezier 3"))
		return;

	ASSERT(nPoints == 3);
	
	/*
	 * If midpoint is close to line then just output the points
	 * otherwise split up and recurse
	 *
	 * We will replace the recursion with iteration soon
	 */

	int dist = pointFromLine(points[0], points[2], points[1]);

	// if(dist < 4)
	if(dist < 1)
	{
		for(int i = 1; i < nPoints; ++i)
			d_output.addPoint(points[i]);
	}
	else
	{
		GPoint pa = midPoint(points[0], points[1]);
		GPoint pb = midPoint(points[1], points[2]);
		GPoint pc = midPoint(pa, pb);

		GPoint p[3];
		p[0] = points[0];
		p[1] = pa;
		p[2] = pc;

		doBezier(p, 3);

		p[0] = pc;
		p[1] = pb;
		p[2] = points[2];
		doBezier(p, 3);
	}
}

/*
 * 3 Point Bezier with less lines
 */

BezierEngine3a::BezierEngine3a(GraphicEngine& output) :
	d_output(output),
	d_nPoints(0)
{
}

BezierEngine3a::~BezierEngine3a()
{
}


void BezierEngine3a::addPoint(const GPoint& point)
{
	if(d_nPoints < 3)
	{
		if(d_nPoints == 0)
			d_output.addPoint(point);

		d_points[d_nPoints++] = point;
	}
	else
	{
		// Split last 2 points

		GPoint mid = midPoint(d_points[1], d_points[2]);

		// Do Bezier of 0,1,mid

		GPoint bPoints[3];
		bPoints[0] = d_points[0];
		bPoints[1] = d_points[1];
		bPoints[2] = mid;

		doBezier(bPoints, 3);

		// Set up points to mid,2,point

		d_points[0] = mid;
		d_points[1] = d_points[2];
		d_points[2] = point;
	}
}

void BezierEngine3a::close()
{
	if(d_nPoints == 3)
		doBezier(d_points, d_nPoints);
	else
	{
		for(int i = 1; i < d_nPoints; ++i)
			d_output.addPoint(d_points[i]);
	}

	d_nPoints = 0;

	d_output.close();
}


void BezierEngine3a::doBezier(const GPoint* points, int nPoints)
{
#ifdef USE_RECURSION
	static int recursion = 0;

	RecurseCount rc(recursion);

	if(rc.tooHigh(10, "Bezier 3A"))
		return;

	ASSERT(nPoints == 3);
	
	/*
	 * If midpoint is close to line then just output the points
	 * otherwise split up and recurse
	 *
	 * We will replace the recursion with iteration soon
	 */

	int dist = pointFromLine(points[0], points[2], points[1]);

	// if(dist < 4)
	if(dist < 1)
	{
		/*
		 * This is the only difference from Bezier3
		 */

		d_output.addPoint(points[2]);
	}
	else
	{
		GPoint pa = midPoint(points[0], points[1]);
		GPoint pb = midPoint(points[1], points[2]);
		GPoint pc = midPoint(pa, pb);

		GPoint p[3];
		p[0] = points[0];
		p[1] = pa;
		p[2] = pc;

		doBezier(p, 3);

		p[0] = pc;
		p[1] = pb;
		p[2] = points[2];
		doBezier(p, 3);
	}
#else	// Iterate

	/*
	 * This can be optimized further by avoiding having to push
	 * the first item, and the 2nd curve in the loop.
	 */

	ASSERT(nPoints == 3);

	struct StackItem
	{
		GPoint d_points[3];
	};

	SimpleStack<StackItem, 10> stack;

	StackItem item;
	item.d_points[0] = points[0];
	item.d_points[1] = points[1];
	item.d_points[2] = points[2];

	for(;;)
	{
		int dist = pointFromLine(item.d_points[0], item.d_points[2], item.d_points[1]);

		if(dist < 1)
		{
			d_output.addPoint(item.d_points[2]);
			if(stack.isEmpty())
				break;
			else
				stack.pop(item);
		}
		else
		{
			GPoint pa = midPoint(item.d_points[0], item.d_points[1]);
			GPoint pb = midPoint(item.d_points[1], item.d_points[2]);
			GPoint pc = midPoint(pa, pb);

			StackItem newItem;

			newItem.d_points[0] = pc;
			newItem.d_points[1] = pb;
			newItem.d_points[2] = item.d_points[2];
			stack.push(newItem);

			item.d_points[1] = pa;
			item.d_points[2] = pc;
		}
	}

#endif
}
/*-------------------------------------------------------------------
 * 4 Point Bezier
 */

BezierEngine4::BezierEngine4(GraphicEngine& output) :
	d_output(output),
	d_nPoints(0)
{
}

BezierEngine4::~BezierEngine4()
{
}


void BezierEngine4::addPoint(const GPoint& point)
{
	if(d_nPoints < 4)
	{
		if(d_nPoints == 0)
			d_output.addPoint(point);

		d_points[d_nPoints++] = point;
	}
	else
	{
		// Split last 2 points

		GPoint mid = midPoint(d_points[2], d_points[3]);

		// Do Bezier of 0,1,mid

		GPoint bPoints[4];
		bPoints[0] = d_points[0];
		bPoints[1] = d_points[1];
		bPoints[2] = d_points[2];
		bPoints[3] = mid;

		doBezier(bPoints, 4);

		// Set up points to mid,2,point

		d_points[0] = mid;
		d_points[1] = d_points[3];
		d_points[2] = point;
		d_nPoints = 3;
	}
}

void BezierEngine4::close()
{
	if(d_nPoints >= 3)
	{
		while(d_nPoints < 4)
		{
			d_points[d_nPoints] = d_points[d_nPoints-1];
			++d_nPoints;
		}
		doBezier(d_points, d_nPoints);
	}
	else
	{
		for(int i = 1; i < d_nPoints; ++i)
			d_output.addPoint(d_points[i]);
	}

	d_output.close();
}

void BezierEngine4::doBezier(const GPoint* points, int nPoints)
{
	static int recursion = 0;

	RecurseCount rc(recursion);

	if(rc.tooHigh(10, "Bezier4"))
		return;

	ASSERT(nPoints == 4);
	
	/*
	 * If midpoint is close to line then just output the points
	 * otherwise split up and recurse
	 *
	 * We will replace the recursion with iteration soon
	 */

	int dist1 = pointFromLine(points[0], points[3], points[1]);
	int dist2 = pointFromLine(points[0], points[3], points[2]);

	// if(dist < 4)
	if( (dist1 < 1) && (dist2 < 1) )
	{
#if 0
		for(int i = 1; i < nPoints; ++i)
			d_output.addPoint(points[i]);
#else
		d_output.addPoint(points[3]);
#endif
	}
	else
	{
		GPoint ab = midPoint(points[0], points[1]);
		GPoint bc = midPoint(points[1], points[2]);
		GPoint cd = midPoint(points[2], points[3]);
		GPoint abbc = midPoint(ab, bc);
		GPoint bccd = midPoint(bc, cd);
		GPoint abbcbccd = midPoint(abbc, bccd);

		GPoint p[4];
		p[0] = points[0];
		p[1] = ab;
		p[2] = abbc;
		p[3] = abbcbccd;

		doBezier(p, 4);

		p[0] = abbcbccd;
		p[1] = bccd;
		p[2] = cd;
		p[3] = points[3];

		doBezier(p, 4);
	}
}



/*
 * Parametric Bezier
 */


/*
 * 3 Point Bezier with less lines
 */

BezierEngine3P::BezierEngine3P(GraphicEngine& output) :
	d_output(output),
	d_nPoints(0)
{
}

BezierEngine3P::~BezierEngine3P()
{
}


void BezierEngine3P::addPoint(const GPoint& point)
{
	if(d_nPoints < 3)
	{
		if(d_nPoints == 0)
			d_output.addPoint(point);

		d_points[d_nPoints++] = point;
	}
	else
	{
		// Split last 2 points

		GPoint mid = midPoint(d_points[1], d_points[2]);

		// Do Bezier of 0,1,mid

		GPoint bPoints[3];
		bPoints[0] = d_points[0];
		bPoints[1] = d_points[1];
		bPoints[2] = mid;

		doBezier(bPoints, 3);

		// Set up points to mid,2,point

		d_points[0] = mid;
		d_points[1] = d_points[2];
		d_points[2] = point;
	}
}

void BezierEngine3P::close()
{
	if(d_nPoints == 3)
		doBezier(d_points, d_nPoints);
	else
	{
		for(int i = 1; i < d_nPoints; ++i)
			d_output.addPoint(d_points[i]);
	}

	d_nPoints = 0;

	d_output.close();
}


void BezierEngine3P::doBezier(const GPoint* points, int nPoints)
{
	ASSERT(nPoints == 3);

#if 0
	// int dx = abs(points[0].x() - points[1].x()) + abs(points[1].x() - points[2].x());
	// int dy = abs(points[0].y() - points[1].y()) + abs(points[1].y() - points[2].y());

	// int nSegments = maximum(dx, dy) / 2;

	const int nSegments = 64;

	for(int segment = 1; segment < (nSegments - 1); ++segment)
	{

		GPoint pa = midPoint(points[0], points[1], segment, nSegments);
		GPoint pb = midPoint(points[1], points[2], segment, nSegments);

		pa = midPoint(pa, pb, segment, nSegments);

		d_output.addPoint(pa);
	}

	d_output.addPoint(points[2]);
#elif 0

	/*
	 * Forward Difference method
	 */

	float dx = 2 * (points[1].x() - points[0].x());
	float dy = 2 * (points[1].y() - points[0].y());

	float ddx = 2 * (points[2].x() + points[0].x() - 2 * points[1].x());
	float ddy = 2 * (points[2].y() + points[0].y() - 2 * points[1].y());

	float x = points[0].x();
	float y = points[0].y();

	for(int segment = 1; segment < 64; ++segment)
	{
		x += dx / 64;
		y += dy / 64;

		dx += ddx / 64;
		dy += ddy / 64;

		d_output.addPoint(GPoint(x,y));
	}
#elif 0	// Fixed point version

	/*
	 * Forward Difference method
	 */

	long dx = 64 * 2 * (points[1].x() - points[0].x());
	long dy = 64 * 2 * (points[1].y() - points[0].y());

	long ddx = 2 * (points[2].x() + points[0].x() - 2 * points[1].x());
	long ddy = 2 * (points[2].y() + points[0].y() - 2 * points[1].y());

	long x = 64 * 64 * points[0].x();
	long y = 64 * 64 * points[0].y();

	for(int segment = 1; segment < 64; ++segment)
	{
		x += dx;
		y += dy;

		dx += ddx;
		dy += ddy;

		d_output.addPoint(GPoint(x/(64*64),y/(64*64)));
	}

#elif 1	// Fixed point carry

	/*
	 * Forward Difference method
	 */

	class NumDem
	{
		public:
			NumDem(int i) : d_i(i), d_n(0) { }

			operator int() const { return d_i; }

			void add(int n, int d)
			{

				if(n >= 0)
				{
					d_n += n;
					while(d_n >= d)
					{
						++d_i;
						d_n -= d;
					}
				}
				else
				{
					d_n -= n;
					while(d_n >= d)
					{
						--d_i;
						d_n -= d;
					}
				}
			}


		private:
			int d_i;
			int d_n;
	};

	// const int nSegments = 8;
	int nSegments = 1 + lineLength(points[0], points[2]) / 8;

	int dxx = points[2].x() + points[0].x() - 2 * points[1].x();
	int dyy = points[2].y() + points[0].y() - 2 * points[1].y();

	NumDem dx = 2 *  (points[1].x() - points[0].x());
	NumDem dy = 2 *  (points[1].y() - points[0].y());

	// Adjustment

	dx.add(dxx, nSegments);
	dy.add(dyy, nSegments);

	NumDem ddx = 2 * dxx;
	NumDem ddy = 2 * dyy;

	NumDem x = points[0].x();
	NumDem y = points[0].y();

	for(int segment = 1; segment < nSegments; ++segment)
	{
		x.add(dx, nSegments);
		y.add(dy, nSegments);

		dx.add(ddx, nSegments);
		dy.add(ddy, nSegments);

		d_output.addPoint(GPoint(x,y));
	}

	d_output.addPoint(points[2]);


#endif
}


BezierEngineThru::BezierEngineThru(GraphicEngine& output) :
	d_bezier(output),
	d_nPoints(0)
{
}

BezierEngineThru::~BezierEngineThru()
{
}


void BezierEngineThru::addPoint(const GPoint& point)
{
	if(d_nPoints < 2)
	{
		if(d_nPoints == 0)
			d_bezier.addPoint(point);

		d_points[d_nPoints++] = point;
	}
	else
	{
		// Calculate extended point

		GPoint newPoint = 2 * d_points[1] - (d_points[0] + point) / 2;

		d_bezier.addPoint(newPoint);
		d_points[0] = newPoint;
		d_points[1] = point;
	}
}

void BezierEngineThru::close()
{
	if(d_nPoints == 2)
		d_bezier.addPoint(d_points[1]);

	d_bezier.close();
	d_nPoints = 0;
}
#endif

