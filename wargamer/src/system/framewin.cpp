/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Simple Window used to hold groups of child windows
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "framewin.hpp"
#include "wmisc.hpp"
// #include "generic.hpp"
#include "winerror.hpp"
#include "myassert.hpp"
#include "app.hpp"
#include "palette.hpp"

#if 0
FrameWindow::FrameWindow(HWND p)
{
  hwndParent = p;
}


FrameWindow::~FrameWindow() 
{
}

void
FrameWindow::init()
{
	RECT r;
	GetClientRect(hwndParent, &r);
	getInitialPosition(r);

	HWND hWnd = createWindow(
        0,
		// GenericNoBackClass::className(), 
        (LPCSTR)NULL,
		// WS_CHILD | WS_BORDER | WS_CLIPSIBLINGS,
		WS_CHILD | WS_CLIPSIBLINGS,
		r.left, r.top, r.right, r.bottom,
        hwndParent,
        NULL      // , wndInstance(hwndParent)
	);

	ASSERT(hWnd != NULL);

	if(hWnd == NULL)
		throw WinError("Can't create FrameWindow");
}

LRESULT

FrameWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
	  // HANDLE_MSG(hWnd, WM_SIZE, onSize);
	  HANDLE_MSG(hWnd, WM_PARENTNOTIFY, onParentNotify);
#ifdef DEBUG
	  // HANDLE_MSG(hWnd, WM_PAINT, onPaint);
#endif
		HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);


	  default:
		 return defProc(hWnd, msg, wParam, lParam);
  }
}

void FrameWindow::onParentNotify(HWND hwnd, UINT msg, HWND hwndChild, int idChild)
{
	FORWARD_WM_PARENTNOTIFY(GetParent(hwnd), msg, hwndChild, idChild, SendMessage);
}

#ifdef DEBUG

void

FrameWindow::onPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);

	RECT r;
	GetClientRect(hWnd, &r);

	HBRUSH hBrush = CreateHatchBrush(HS_DIAGCROSS, RGB(255,0,255));	// Purple!
	FillRect(hdc, &r, hBrush);
	DeleteObject(hBrush);

	const char message[] = "Empty FrameWindow";
	const size_t messageLength = lstrlen(message);

	TextOut(hdc, 1, 1, message, messageLength);

	EndPaint(hWnd, &ps);
}

#endif


BOOL FrameWindow::onEraseBk(HWND hwnd, HDC hdc)
{
	return TRUE;
}

#endif


/*
 * Simple Child Window class for making sample windows
 */

const char SimpleChildWindow::s_className[] = "SimpleChildWindow";
ATOM SimpleChildWindow::s_classAtom = NULL;

SimpleChildWindow::SimpleChildWindow(const CustomBorderInfo& borderColors) :
	WindowBaseND(),
	CustomBorderWindow(borderColors)
{
	registerClass();
}

HWND SimpleChildWindow::create(HWND hParent)
{
	ASSERT(getHWND() == NULL);

	RECT r;
	GetClientRect(hParent, &r);
	// getInitialPosition(r);

	createWindow(
		0,
		// s_className,			// szMainClass,
		NULL,
		WS_CHILD |
		WS_CLIPSIBLINGS,
		r.left, r.top, r.right, r.bottom,
		hParent,               	/* parent window */
		NULL               		/* menu handle (or child ID) */
		// APP::instance() 			/* program handle */
	);

	ShowWindow(getHWND(), SW_SHOW);

	return getHWND();
}

SimpleChildWindow::~SimpleChildWindow()
{
}

ATOM SimpleChildWindow::registerClass()
{
	if(!s_classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(SimpleChildWindow*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) (COLOR_BACKGROUND + 1);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		s_classAtom = RegisterClass(&wc);
	}

	ASSERT(s_classAtom != NULL);

	return s_classAtom;
}

LRESULT SimpleChildWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#if 0
	LRESULT l;
	if(handlePalette(hWnd, msg, wParam, lParam, l))
		return l;
#endif

	switch(msg)
	{
		// HANDLE_MSG(hWnd, WM_CREATE,			onCreate);
		// HANDLE_MSG(hWnd, WM_DESTROY,			onDestroy);
	   HANDLE_MSG(hWnd, WM_PAINT,				onPaint);
		HANDLE_MSG(hWnd, WM_ERASEBKGND, 		onEraseBk);
		// HANDLE_MSG(hWnd, WM_COMMAND,			onCommand);
		// HANDLE_MSG(hWnd, WM_SIZE,				onSize);
		// HANDLE_MSG(hWnd, WM_GETMINMAXINFO,	onGetMinMaxInfo);
		// HANDLE_MSG(hWnd, WM_NOTIFY,			onNotify);
		// HANDLE_MSG(hWnd, WM_CLOSE,				onClose);

	default:
		return defProc(hWnd, msg, wParam, lParam);
	}
}

void SimpleChildWindow::onPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);

	HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
	RealizePalette(hdc);


	RECT r;
	GetClientRect(hWnd, &r);

#ifdef DEBUG
	HBRUSH hBrush = CreateHatchBrush(HS_DIAGCROSS, RGB(255,0,255));	// Purple!
	FillRect(hdc, &r, hBrush);
	DeleteObject(hBrush);

	static const char message[] = "SimpleChildWindow";
	static const size_t messageLength = lstrlen(message);

	TextOut(hdc, 1, 1, message, messageLength);
#endif

	drawThickBorder(hdc, r);
	SelectPalette(hdc, oldPal, FALSE);

	EndPaint(hWnd, &ps);
}

BOOL SimpleChildWindow::onEraseBk(HWND hwnd, HDC hdc)
{
	return TRUE;
}

