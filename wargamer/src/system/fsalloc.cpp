/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "fsalloc.hpp"
#include "myassert.hpp"
#include "misc.hpp"

//================== BlockLink class and implementation =============

/*
 * Holds blocks of allocated memory
 */

class BlockLink {
	 void* d_block;           // block of storage
	 BlockLink* d_next;       // pointer to next block, or NULL

	 BlockLink(const BlockLink& b);
	 BlockLink& operator = (const BlockLink& b);

  public:
	 BlockLink(void* block, BlockLink* next) :
		d_block(block),
		d_next(next)
	 {}

	 ~BlockLink()
	 {
		if(d_block)
		  delete[] d_block;
	 }

	 BlockLink* next() { return d_next; }

};

//=================== BlockList class and implementation ==============

/*
 *  Allocates\frees memory chunks
 */

class BlockList {
	 BlockLink* d_blockList;                 // linked list of allocated blocks

	 BlockList(const BlockList&);            // unimplemented
	 BlockList& operator=(const BlockList&); // unimplemented

  public:

	 BlockList();
	 ~BlockList();

	 void* alloc(size_t size);
	 void free();
};

BlockList::BlockList() :
  d_blockList(0)
{
}

BlockList::~BlockList()
{
  free();
}

void* BlockList::alloc(size_t size)
{
  void* p = new char[size];
  ASSERT(p != 0);

  d_blockList = new BlockLink(p, d_blockList);
  ASSERT(d_blockList != 0);

  return p;
}

void BlockList::free()
{
  while(d_blockList)
  {
	 BlockLink* p = d_blockList;
	 d_blockList = d_blockList->next();
	 delete p;
  }
}

//==================== FixedSize_Allocator implementation ===============

/*
 *   Our actual memory manager
 */

enum {
	MinimumSize = 4,
	Default_ChunkSize = 100
};


#ifdef DEBUG

FixedSize_Allocator::FixedSize_Allocator(size_t size, size_t chunkSize, const char* className) :
  d_freeList(0),
  d_size(size),
  d_chunkSize((chunkSize > 0) ? chunkSize : Default_ChunkSize),
  d_blockAllocator(new BlockList),
  d_instanceCount(0),
  d_className(copyString(className))
{
  ASSERT(d_size >= MinimumSize);
  ASSERT(d_blockAllocator != 0);
  ASSERT(d_className != 0);
}
#else


FixedSize_Allocator::FixedSize_Allocator(size_t size, size_t chunkSize) :
  d_freeList(0),
  d_size(size),
  d_chunkSize((chunkSize > 0) ? chunkSize : Default_ChunkSize),
  d_blockAllocator(new BlockList)
{
  ASSERT(d_size >= MinimumSize);
  ASSERT(d_blockAllocator != 0);
}

#endif


FixedSize_Allocator::~FixedSize_Allocator()
{
#ifdef DEBUG
  if(d_instanceCount == 0)
#endif
	 delete d_blockAllocator;

#ifdef DEBUG
  ASSERT(d_instanceCount == 0);
  if(d_className)
	 delete[] d_className;
#endif

}

void

FixedSize_Allocator::dryUp()      // release memory
{
  d_blockAllocator->free();
  d_freeList = 0;
}

void

FixedSize_Allocator::replenish()  // need another chunk
{
  char* start = static_cast<char*>(d_blockAllocator->alloc(d_size*d_chunkSize));
  ASSERT(start != 0);

  char* last = &start[(d_chunkSize-1)*d_size];

  for(char* p = start; p < last; p += d_size)
  {
	 reinterpret_cast<Link*>(p)->next = reinterpret_cast<Link*>(p + d_size);
  }

  reinterpret_cast<Link*>(last)->next = 0;

  d_freeList = reinterpret_cast<Link*>(start);
}


