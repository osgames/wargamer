/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AUDIT_H
#define AUDIT_H

#ifndef __cplusplus
#error audit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Keep track of news and deletes
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "clog.hpp"
#include <set>

template<class T>
class AuditLog
{
   public:
   AuditLog(const char* logName) :
   d_log(new LogFileFlush(logName)),
   d_ownLog(true),
   d_total(0),
   d_list()
   {
   }

   AuditLog(LogFile* log) :
   d_log(log),
   d_ownLog(false),
   d_total(0),
   d_list()
   {
   }


   virtual ~AuditLog()
   {
      //--- Derived class must do this!
      // showState();

      if(d_ownLog)
         delete d_log;
   }

   virtual void logNew(T t, const char* text);
   virtual void logDelete(T t, const char* text);

   void logNew(T t)
   { logNew(t, "New"); }
   void logDelete(T t)
   { logDelete(t, "Delete"); }
   virtual void showState();

   void printf(const char* fmt, ...)
   {
      va_list vaList;
      va_start(vaList, fmt);
      vprintf(fmt, vaList);
      va_end(vaList);
   }

   void vprintf(const char* fmt, va_list args)
   {
      d_log->vprintf(fmt, args);
   }

   private:

   virtual void logItem(T t) = 0;

   protected:
   LogFile* d_log;

   private:
   bool d_ownLog;
   int d_total;

   typedef std::set<T, std::less<T> > AuditList;
   AuditList d_list;
};

template<class T>
void AuditLog<T>::logNew(T t, const char* text)
{
   std::pair<AuditList::iterator, bool> iResult = d_list.insert(t);
   ASSERT(iResult.second);

   ++d_total;

   *d_log << text << ": ";
   logItem(t);
   *d_log << ", total=" << d_total;
   *d_log << ", active=" << d_list.size() << endl;

}

template<class T>
void AuditLog<T>::logDelete(T t, const char* text)
{
   AuditList::size_type nItems = d_list.erase(t);
   ASSERT(nItems == 1);

   *d_log << text << ": ";
   logItem(t);
   *d_log << ", total=" << d_total;
   *d_log << ", active=" << d_list.size() << endl;
}


template<class T>
void AuditLog<T>::showState()
{
   *d_log << endl;
   *d_log << "----------------------------------------" << endl;
   *d_log << "Undeleted Items: " << d_list.size() << endl;
   *d_log << endl;
   {
      for(AuditList::const_iterator it = d_list.begin();
         it != d_list.end();
         ++it)
      {
         T t = *it;
         logItem(t);
         *d_log << endl;
      }
   }
}

#endif /* AUDIT_H */

