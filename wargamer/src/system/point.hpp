/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef POINT_HPP
#define POINT_HPP

#ifndef __cplusplus
#error point.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Template for Points fo different types
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "filebase.hpp"
#include "myassert.hpp"
#include <utility>      // to get != and other stuff...

class PointBase
{
        public:
                class PointError 
                {
                        public:
                                PointError() { }
                                ~PointError() { }
                };
};

template<class T>
class Point : public PointBase
{
                T d_x;
                T d_y;

        public:
                Point() : d_x(0), d_y(0) { }
                Point(T x, T y) : d_x(x), d_y(y) { }
                Point(const Point<T>& p) : d_x(p.d_x), d_y(p.d_y) { }

                Point<T>& operator = (const Point<T>& p)
                {
                        d_x = p.d_x;
                        d_y = p.d_y;
                        return *this;
                }
                 
                Boolean readData(FileReader& f)
                {
                        f >> d_x;
                        f >> d_y;
                        return f.isOK();
                }

                Boolean writeData(FileWriter& f) const
                {
                        f << d_x;
                        f << d_y;
                        return f.isOK();
                }

                // Accessors

                T x() const { return d_x; }
                T y() const { return d_y; }

                void x(T x) { d_x = x; }
                void y(T y) { d_y = y; }
};

template<class T>
inline bool operator == (const Point<T>& lhs, const Point<T>& rhs)
{
        return (lhs.x() == rhs.x()) && (lhs.y() == rhs.y());
}

#if 0  // def _MSC_VER // Visual C++ has some difficulty with <utility>::!=
// Let STL template library generate this to avoid ambiguous function

template<class T>
inline bool operator != (const Point<T>& lhs, const Point<T>& rhs)
{
        return !(lhs == rhs);
}
#endif
                

template<class T>
inline Point<T> operator + (const Point<T>& lhs, const Point<T>& rhs)
{
        return Point<T>(lhs.x() + rhs.x(), lhs.y() + rhs.y());
}

template<class T>
inline Point<T>& operator += (Point<T>& lhs, const Point<T>& rhs)
{
        lhs.x(lhs.x() + rhs.x());
        lhs.y(lhs.y() + rhs.y());
        return lhs;
}


template<class T>
inline Point<T> operator - (const Point<T>& lhs, const Point<T>& rhs)
{
        return Point<T>(lhs.x() - rhs.x(), lhs.y() - rhs.y());
}


template<class T>
inline Point<T>& operator -= (Point<T>& lhs, const Point<T>& rhs)
{
        lhs.x(lhs.x() - rhs.x());
        lhs.y(lhs.y() - rhs.y());
        return lhs;
}

template<class T>
inline Point<T> operator / (const Point<T>& lhs, int n)
{
        ASSERT(n != 0);
        if(n == 0)
                throw PointBase::PointError();
        return Point<T>(lhs.x()/n, lhs.y()/n);
}

template<class T>
inline const Point<T>& operator /= (Point<T>& lhs, int n)
{
        ASSERT(n != 0);
        if(n == 0)
                throw PointError();
        lhs.x(lhs.x()/n);
        lhs.y(lhs.y()/n);
        return lhs;
}

template<class T>
inline Point<T> operator * (const Point<T>& lhs, int n)
{
        return Point<T>(lhs.x()*n, lhs.y()*n);
}

template<class T>
inline const Point<T>& operator *= (Point<T>& lhs, int n)
{
        lhs.x(lhs.x()*n);
        lhs.y(lhs.y()*n);
        return lhs;
}

template<class T>
class Rect
{
                Point<T> d_topLeft;
                Point<T> d_size;
        public:
                Rect() : d_topLeft(), d_size() { }
                Rect(T x, T y, T w, T h) :
                        d_topLeft(x, y),
                        d_size(w, h)
                {
                }
                Rect(const Rect<T>& r) :
                        d_topLeft(r.d_topLeft),
                        d_size(r.d_size)
                {
                }
                Rect(const Point<T>& p1, const Point<T>& size) :
                        d_topLeft(p1),
                        d_size(size)
                {
                }

                Rect<T>& operator = (const Rect<T>& r)
                {
                        d_topLeft = r.d_topLeft;
                        d_size = r.d_size;
                        return *this;
                }

                const Point<T>& topLeft() const { return d_topLeft; }
                Point<T> bottomRight() const { return d_topLeft + d_size - Point<T>(1,1); }
                const Point<T>& size() const { return d_size; }

                T left()                const   { return d_topLeft.x(); }
                T top()         const   { return d_topLeft.y(); }
                T bottom()      const   { return d_topLeft.y() + d_size.y() - 1; }
                T right()       const { return d_topLeft.x() + d_size.x() - 1; }
                T width()       const { return d_size.x(); }
                T height()      const { return d_size.y(); }

                int centre_x()  const { return d_topLeft.x() + (d_size.x() /2); }
                int centre_y()  const { return d_topLeft.y() + (d_size.x() /2); }

                void left(T n)          
                {
                        ASSERT(n <= (d_topLeft.x() + d_size.x()));
                        d_size.x(d_topLeft.x() + d_size.x() - n);
                        d_topLeft.x(n); 
                }

                void top(T n)
                {
                        ASSERT(n <= (d_topLeft.y() + d_size.y()));
                        d_size.y(d_topLeft.y() + d_size.y() - n);
                        d_topLeft.y(n); 
                }

                void bottom(T n)        
                {
                        ASSERT(n >= d_topLeft.y());
                        d_size.y(n - d_topLeft.y() + 1); 
                }

                void right(T n)
                {
                        ASSERT(n >= d_topLeft.x());
                        d_size.x(n - d_topLeft.x() + 1); 
                }

                void width(T n) { d_size.x(n); }
                void height(T n)        { d_size.y(n); }

};


#endif /* POINT_HPP */

