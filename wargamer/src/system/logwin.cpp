/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * LogWin Interface
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "logwin.hpp"

/*
 * Global Variable
 */

static NullLogWindow s_nullLog;

LogWinPtr g_logWindow;

LogWinPtr::LogWinPtr()
 : d_logWindow(&s_nullLog) 
{ 
}

LogWinPtr::~LogWinPtr()
{ 
   clear();
}

void LogWinPtr::set(LogWindow* logWin) 
{
   clear();
   d_logWindow = logWin; 
}

void LogWinPtr::clear() 
{
   if(d_logWindow != &s_nullLog)
   {
      delete d_logWindow; 
   }
   d_logWindow = &s_nullLog;
}


void LogWinPtr::printf(const char* fmt, ...)
{
   if(d_logWindow)
   {
      va_list vaList;
      va_start(vaList, fmt);
      d_logWindow->vprintf(fmt, vaList);
      va_end(vaList);
   }
}



void LogWinPtr::vprintf(const char* fmt, va_list vaList)
{
   if(d_logWindow)
      d_logWindow->vprintf(fmt, vaList);
}

void LogWindow::printf(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
