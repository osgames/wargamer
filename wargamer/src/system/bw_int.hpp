/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BW_INT
#define BW_INT

#include "sysdll.h"

namespace BattleWindows_Internal
{
class BattleMapSelect;

/*
 * Pure Virtual Class that User Interface must implement
 * We may not need this, unless we have a modal interface such as
 * we do in the campaign
 *
 * Or we could add a create() function to this, and then
 * hide the BattleUnitUI inside the .cpp file
 * and get rid of BUI_UnitImp.
 */

class SYSTEM_DLL BattleUI_Interface
{
	public:
		virtual ~BattleUI_Interface() { }

		 /*
		  * Mouse Tracking functions
		  *
		  * Note that button down/up and onMove are still called
		  * even if dragging.
		  *
		  * e.g. for a typical dragging operation, the following will occur
		  *  onButtonDown
		  *  onMove...
		  *  onStartDrag
		  *  onMove...
		  *  onEndDrag
		  *  onButtonUp
		  */

		virtual void onButtonDown(const BattleMapSelect& info) = 0;
			// Button is pressed
		virtual void onButtonUp(const BattleMapSelect& info) = 0;
			// Button is released while not dragging
		virtual void onStartDrag(const BattleMapSelect& info) = 0;
			// Mouse is moved while button is held
		virtual void onEndDrag(const BattleMapSelect& info) = 0;
			// button is released while dragging

		virtual void onMove(const BattleMapSelect& info) = 0;
			// Mouse has moved
		virtual void overNothing() = 0;
			// Mouse is not over anything (e.g. off map)

};

}; // namespace

#endif
