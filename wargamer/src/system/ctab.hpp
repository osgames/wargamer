/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CTAB_HPP
#define CTAB_HPP

/*
 * Custom drawn tab
 */

#include "sysdll.h"
#include "palwind.hpp"

class DrawDIBDC;
//class DrawDib;
class ImageLibrary;

class SYSTEM_DLL CustomDrawnTab : public SubClassWindowBase {
	 const DrawDIB* d_bkFillDib;
	 DrawDIB* d_dib;          // background dib
//	 DrawDIB* d_maskDib;      // mask of tab image

	 static DrawDIBDC* s_tabDib;
	 static UWORD s_instanceCount;

	 const ImageLibrary* d_tabImages;

	 HWND d_tabHWND;
	 HFONT d_font;

    RECT d_tabRect;

	 UBYTE d_flags;
    CustomBorderInfo d_borderColors;

  public:
	 enum {
		NoBorder  = 0x01,
		ShadowTab = 0x02,
		StretchTab = 0x04,
	 };

	 CustomDrawnTab();
	 ~CustomDrawnTab();

	 void tabHWND(HWND hwnd);
	 void setBkFillDib(const DrawDIB* dib) { d_bkFillDib = dib; }
	 void images(const ImageLibrary* il) { d_tabImages = il; }
	 void setFont(HFONT hFont) { d_font = hFont; }
	 void setFlags(UBYTE flags) { d_flags = flags; }
	 void setBorderColor(CustomBorderInfo bd) { d_borderColors = bd; }

	 HWND tabHWND() const { return d_tabHWND; }

	 void copyBits();
	 void getDisplayRect(RECT& r);

  private:
	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 void onPaint(HWND hwnd);
	 BOOL onEraseBk(HWND hwnd, HDC hdc);
	 void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	 void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
     void onDestroy(HWND hwnd);

	 void drawTab(HDC hdc, int id, int iSel);
};








#endif
