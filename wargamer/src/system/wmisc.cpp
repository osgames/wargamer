/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *      Miscellaneous Window support classes and functions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "stdinc.hpp"
#include "wmisc.hpp"
#include "app.hpp"
#include "myassert.hpp"
#include <stdio.h>
#include "memptr.hpp"
#include "simpstr.hpp"
#include "misc.hpp"
#include "grtypes.hpp"

namespace WindowsMisc
{


/*
 * Load from Application's Resource
 */


ResString::ResString(UINT resID) :
  m_text()
{
  init(APP::instance(), resID);
}

/*
 * Load from an HINSTANCE's resource
 *
 * Note this will need to be modified to work with Unicode strings
 * under Windows NT.
 */

void ResString::init(HINSTANCE hInst, UINT resId)
{
  TCHAR buffer[256];
  
  int bufSize = LoadString(hInst, resId, buffer, 256);
  
  if(bufSize > 0)
  {
    m_text.assign(buffer, bufSize);
  }
  else
  {
    m_text.erase();  // m_text.clear());   //mtc100103
  }
}

#ifdef DEBUG

// LPTSTR copyTString(const LPTSTR s)
// {
//         LPTSTR text = new TCHAR[lstrlen(s) + 1];
//         lstrcpy(text, s);
//         return text;
// }

ResString::ResString(const ResString& r)
{
  FORCEASSERT("ResString Copy Constructor invoked");
//        text = copyTString(r.text);
  m_text = r.m_text;
}

ResString& ResString::operator = (const ResString& r)
{
  FORCEASSERT("ResString Copy Assignment invoked");
//        text = copyTString(r.text);
  m_text = r.m_text;
  return *this;
}

#endif


ResString::~ResString()
{
//  delete[] text;
}


/*
 * Convert a resource into a SimpleString
 * This will almost certainly need updating for UniCode.
 */

void

idsToString(SimpleString& s, UINT id)
{
  TCHAR buffer[256];
  int nChar = LoadString(APP::instance(), id, buffer, 256);
  ASSERT(nChar != 0);
  s += buffer;
}

LONG

textWidth(HDC hdc, LPCTSTR text)
{
  SIZE s;
  BOOL result = GetTextExtentPoint32(hdc, text, lstrlen(text), &s);
  ASSERT(result);

  return s.cx;
}

void wTextOut(HDC hdc, int x, int y, LPCTSTR text)
{
  TextOut(hdc, x, y, text, lstrlen(text));
}

void wTextOut(HDC hdc, int x, int y, const String& text)
{
  TextOut(hdc, x, y, text.c_str(), text.size());
}

void PASCAL wTextPrintf(HDC hdc, int x, int y, LPCTSTR text, ...)
{
	static const size_t BufferSize = 1024;
  MemPtr<TCHAR> buffer(BufferSize);

  va_list vaList;
  va_start(vaList, text);
  vbprintf(buffer.get(), BufferSize, text, vaList);
  va_end(vaList);

  wTextOut(hdc, x, y, buffer.get());
}

/*
 * This versions clips the text size
 */

void

wTextOut(HDC hdc, int x, int y, int cx, const char* text)
{
  MemPtr<char> buffer(1024);

  LONG textCX = textWidth(hdc, text);

  if(textCX > cx)    // was >=
  {
    TEXTMETRIC tm;
    GetTextMetrics(hdc, &tm);

    LONG charCX = tm.tmAveCharWidth;
    ASSERT(charCX > 0);

    const char dot = '.';
    const char null = '\0';

    int dotWidth = textWidth(hdc, ".");

    int nDots = minimum(3, lstrlen(text) - 1);
    int allowedWidth = cx - dotWidth * nDots;

//         int nToCut = ((textCX - cx) / charCX) + nDots;
    int nToCut = ((textCX - allowedWidth + charCX - 1) / charCX);

    lstrcpy(buffer.get(), text);

    char* c = buffer.get() + (lstrlen(text) - nToCut);
    while(nDots--)
    {
      *c++ = dot;
    }

    *c = null;

    text = buffer.get();
  }

  wTextOut(hdc, x, y, text);
}

void PASCAL

wTextPrintf(HDC hdc, int x, int y, int cx, const char* text, ...)
{
  MemPtr<char> buffer(1024);

  va_list vaList;
  va_start(vaList, text);
  vbprintf(buffer.get(), 1024, text, vaList);
  va_end(vaList);

  wTextOut(hdc, x, y, cx, buffer.get());
}


void wTextOut(HDC hdc, const PixelRect& r, const char* text)
{
  DrawText(hdc, text, -1, const_cast<RECT*>(&r), DT_WORDBREAK);
}


void wTextOut(HDC hdc, const PixelRect& r, const char* text, unsigned int flags)
{
  DrawText(hdc, text, -1, const_cast<RECT*>(&r), flags);
}


void

fillRECT(RECT& rect, int l, int t, int r, int b)
{
  rect.left = l;
  rect.top = t;
  rect.right = r;
  rect.bottom = b;
}

BOOL mouseInRect(RECT& rect, int mx, int my)
{
  if(mx >= rect.left && mx <= rect.right &&
     my >= rect.top && my <= rect.bottom)
    return TRUE;
  else
    return FALSE;
}


LPVOID

loadResource(HMODULE hMod, LPCTSTR lpName, LPCTSTR lpType)
{
  LPVOID data = 0;

  HRSRC hRes = FindResource(hMod, lpName, lpType);
  ASSERT(hRes != 0);

  HGLOBAL hGlob = LoadResource(hMod, hRes);
  ASSERT(hGlob != 0);

  data = LockResource(hGlob);
  ASSERT(data != NULL);

  return data;
}


}      // namespace WindowsMisc

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.6  2003/02/08 19:35:08  greenius
 * Replaced usage of basic_string::clear() with erase, to overcome MSVC6 implementation problem
 *
 * Revision 1.5  2002/11/16 18:03:34  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 *----------------------------------------------------------------------
 */
