/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 * $Id$
 *
 * Filename handling function
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 * 
 *    Rev 1.1   25 Jun 1993 15:44:14   sgreen
 * No change.
 * 
 *    Rev 1.0   28 Apr 1993 16:33:48   sgreen
 * Initial revision.
 * Revision 1.1  1992/10/17  00:11:14  sgreen
 * Initial revision
 *
 */

#include "stdinc.hpp"
#include "makename.hpp"
#include "myassert.hpp"
#include <string.h>
#include <stdio.h>
// #include "types.h"

/*
 * Make a filename with the given extension if one didn't already exist
 *
 * If force is true then any existing extension is replaced
 */

char* makeFilename(const char *src, const char *ext, bool force)
{
	char dest[FILENAME_MAX];

	ASSERT((strlen(src) + strlen(ext)) < FILENAME_MAX);

	strcpy(dest, src);

	/*
    * Find start of filename (lose path or drive)
    */

	char* s = strrchr(dest, '\\');
	if(s == NULL)
		s = strrchr(dest, ':');
	if(s == NULL)
		s = dest;

	/*
    * See if there is an extension
	 */

	s = strchr(s, '.');

	/*
	 * Add the new extension
	 */

	if(s == NULL)
		strcat(dest, ext);
	else if(force)
		strcpy(s, ext);

	char* retVal = new char[strlen(dest) + 1];
	strcpy(retVal, dest);
	return retVal;
}

