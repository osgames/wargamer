/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GAMECTRL_HPP
#define GAMECTRL_HPP

#ifndef __cplusplus
#error gamectrl.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Game Control Base Class
 * This is basically a global variable that provides access to
 * some generic game functions, such as counters, pause, etc...
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "systick.hpp"

class ThreadManager;

/*-------------------------------------------------
 * Game Controller must implement this class
 *
 * Typically this will be the main application
 */

class GameControlBase {
    public:
 	    SYSTEM_DLL GameControlBase();
	    SYSTEM_DLL virtual ~GameControlBase();

 	    SYSTEM_DLL bool pause(bool onOff);
		    // Set pause state to given value, and return old value

	    bool isPaused() const { return d_paused; }
		    // Return the current pause mode

	    SYSTEM_DLL void onTimer();
		    // Call this every time a WM_TIMER message arrives

	    SysTick::Value getTick() const { return d_procCount; }

        static GameControlBase* current() { return s_current; }

    private:
	    /*
	     * Virtual Functions that Application must provide
	     */

	    virtual void pauseGame(bool paused) = 0;
		    // Do whatever is neccessary to pause or unpause the game
		    // For example in a multi-threaded game, it may need to send
		    // a signal to each thread and wait until it has responded

	    virtual void updateTime(SysTick::Value ticks) = 0;
		    // Do whatever is needed when some time has passed

    private:
    	bool 			d_paused;		// True if game is paused
	    SysTick::Value  d_procCount;	// GameTicks since program began
#if !defined(USE_MM_TIMER)
    	BOOL 			d_firstMM;
	    DWORD 			d_lastMMtime;
#endif

        GameControlBase* d_prev;
        SYSTEM_DLL static GameControlBase* s_current;

};

/*
 * Game Control class.. the way the inner workings of the program
 * can pause the game and other useful things.
 */

class SYSTEM_DLL GameControl
{
    public:
 	    static bool pause(bool onOff) { return GameControlBase::current()->pause(onOff); }
		    // Set pause state to given value, and return old value

	    static bool isPaused() { return GameControlBase::current()->isPaused(); }
		    // Return the current pause mode

	    static SysTick::Value getTick() { return GameControlBase::current()->getTick(); }
		    // Return number of Ticks since program started

	    static void onTimer() { GameControlBase::current()->onTimer(); }
		    // Call this every time a WM_TIMER message arrives

      /*
       * handy class to pause the game and restore it to its old state
       * when it exits
       */

      class Pauser
      {
         public:
            Pauser()
            {
               d_oldState = pause(true);
            }

            ~Pauser()
            {
               pause(d_oldState);
            }

         private:
            bool d_oldState;
      };

};

#endif /* GAMECTRL_HPP */

