/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAPSTEST_HPP
#define CAPSTEST_HPP


#include "sysdll.h"

/*

  Filename : capstest.hpp
  
  Description : Class to test GDI capabilities

*/


struct CapsDescription {

	// General caps
	int driverVersion;
	int clipCapability;

	// Raster caps
	bool banding;
	bool blit;
	bool bitmap64k;
	bool dibBits;
	bool dibBitsToDevice;
	bool floodFill;
	bool gdi20;
	bool paletteBased;
	bool scaling;
	bool stretchBlit;
	bool stretchDibBits;

	// DrawDib caps
	bool canDrawDib;
	bool canStretchAndDraw;
	bool stretch1to1;
	bool stretch1to2;
	bool stretch1toN;
};




class SYSTEM_DLL CapsTest {

   public:

	   CapsTest(HDC hdc, LPBITMAPINFOHEADER bminfo);
	   ~CapsTest(void);

	   void DebugOut(void);

	   CapsDescription d_CapsDescription;

};


#endif


/*---------------------------------------------------------------
 * $Log$
 *---------------------------------------------------------------
 */
