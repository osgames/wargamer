/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cmenu.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "palette.hpp"
#include "winctrl.hpp"
#include "imglib.hpp"
// #include "dynarray.hpp"
#include "app.hpp"
// #include "generic.hpp"
#include "wmisc.hpp"
#include "scrnbase.hpp"

#include <vector>		// STL vector

#ifdef DEBUG
#define DEBUG_MENU
#ifdef DEBUG_MENU
#include "logwin.hpp"
#endif
#endif

/*-------------------------------------------------
 * MenuItemInfo
 */

MenuItemInfo& MenuItemInfo::operator = (const MenuItemInfo& mi)
{
  d_id = mi.d_id;
  d_state = mi.d_state;
  d_subMenu = mi.d_subMenu;
  d_rect = mi.d_rect;

  // if(mi.d_text.toStr())
  //	 d_text = copyString(mi.d_text.toStr());
  d_text = mi.d_text;

  return *this;
}

MenuItemInfo::MenuItemInfo(const MenuItemInfo& mi)
{
  	d_id = mi.d_id;
	d_state = mi.d_state;
	// ASSERT(mi.d_subMenu == 0);
	d_subMenu = mi.d_subMenu;
	d_text = mi.d_text;
	d_rect = mi.d_rect;
}


// class MenuItemList : public ResizeArray<MenuItemInfo> {
// };

class MenuItemList
{
      typedef std::vector<MenuItemInfo> Container;
	public:
       ~MenuItemList();

		void add(const MenuItemInfo& mi)
		{
			d_items.push_back(mi);
		}

		void insert(const MenuItemInfo& mi, Container::size_type id)
		{
			Container::iterator it = d_items.begin();
			it += id;
			d_items.insert(it, mi);
		}

		Container::size_type size() const { return d_items.size(); }

		void reserve(Container::size_type n) { d_items.reserve(n); }

		const MenuItemInfo& operator [] (int id) const { return d_items[id]; }
		MenuItemInfo& operator [] (int id) { return d_items[id]; }

	private:
		Container d_items;
};


/*-------------------------------------------------------------
 *  Menu Window
 */

struct MW_Const {
  const int dbX;
  const int dbY;
  const int baseX;
  const int baseY;
  const int offsetX;
  const int arrowCX;
  const int checkCX;
  const int offsetY;

  MW_Const() :
	 dbX(ScreenBase::dbX()),
	 dbY(ScreenBase::dbY()),
	 baseX(ScreenBase::baseX()),
	 baseY(ScreenBase::baseY()),
	 offsetX((2 * dbX) / baseX),
	 arrowCX((8 * dbX) / baseX),
	 checkCX((6 * dbX) / baseX),
	 offsetY((1 * dbY) / baseY) {}
};

class MenuWindow : public WindowBaseND {
	 CustomMenuData d_data;

	 MenuItemList d_items;

	 /*
	  * all menus share the same DibDC to prevent excessive instances of DC's
	  */

	 static DrawDIBDC* s_popupDib;
	 static DrawDIBDC* s_barDib;
	 static Palette::PaletteID s_popupPalID;
	 static Palette::PaletteID s_barPalID;
	 static UWORD s_instanceCount;

	 enum Flags {
		 Showing       = 0x01,
		 Active        = 0x02,
		 BottomUp      = 0x04
	 };

	 UBYTE d_flags;

	 // size of window
	 UINT d_cx;
	 UINT d_cy;
	 UINT d_itemCY;  // width of each item

	 // current item
	 int d_currentID;

	 MW_Const d_mwc;
  public:

	 MenuWindow(const CustomMenuData& data);

	 ~MenuWindow()
	 {
		ASSERT((s_instanceCount - 1) >= 0);

        // destroy();
        selfDestruct();

		if(--s_instanceCount == 0)
		{
		  if(s_popupDib)
		  {
			 delete s_popupDib;
			 s_popupDib = 0;
		  }

		  if(s_barDib)
		  {
			 delete s_barDib;
			 s_barDib = 0;
		  }
		}
	 }

	 void add(const MenuItemInfo& mi) { d_items.add(mi); }
	 // void destroy();

     void show(bool visible) { WindowBaseND::show(visible); }
     void enable(bool visible) { WindowBaseND::enable(visible); }

	 void show(const PixelPoint& p, bool captureMouse = true);
	 void hide();
	 void hideAll();
    void hideAllSubs();
	 void redraw();

	 bool enable(int id, bool set, bool byPosition)
	 {
		if(byPosition)
		{
		  int count = 0;
		  return setEnabledByPos(id, set, count);
		}
		else
		  return setState(id, MenuItemInfo::CMS_Enabled, set);
	 }

	 bool setEnabledByPos(int id, bool set, int& count);
	 bool checked(int id, bool set) { return setState(id, MenuItemInfo::CMS_Checked, set); }
	 bool setState(int id, UBYTE mask, bool set);
	 int width() const { return d_cx; }
	 int height() const { return d_cy; }
	 static void allocateStaticDib(CustomMenuData::Type type, int cx, int cy);
	 CustomMenuData::Type type() const { return d_data.d_type; }
	 int overWhichItem(int x, int y);
	 MenuItemList& menuItems() { return d_items; }
	 int currentID() const { return d_currentID; }
	 const MenuItemInfo* menuItem() const
	 {
		return menuItem(d_currentID);
	 }

	 MenuItemInfo* menuItem(int id)
	 {
		return (id != -1) ? &d_items[id] : 0;
	 }

	 const MenuItemInfo* menuItem(int id) const
	 {
		return (id != -1) ? &d_items[id] : 0;
	 }

	 int getItemCount() const
	 {
	 	return d_items.size();
	 }

	 BOOL insertMenuItem(int i, BOOL f, const MenuItemInfo* info);

	 const CustomMenuData& data() const { return d_data; }

	 void setParent(MenuWindow* parent);

  private:

	 /*
	  *   Message Functions
	  */

	 LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	 BOOL onEraseBk(HWND hwnd, HDC hdc);
	 void onPaint(HWND hWnd);
	 // void onDestroy(HWND hWnd) {}
	 void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	 void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
	 void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);

	 // internal
	 void drawMenu();
	 void drawItem(const MenuItemInfo& mi);
	 bool calcItemSizes();
	 void adjustPosition(const PixelPoint& p);

	 void onBarMenuMove(int x, int y);
	 void onBarMenuDown(int x, int y);
	 void onPopupMove(int x, int y);
	 void onPopupDown(int x, int y);

	 void selectItem(int id);
	 void showSub(const MenuItemInfo& mi, bool captureMouse);
	 void setInactive();

	 void setFlag(UBYTE mask, bool f)
	 {
		if(f)
		  d_flags |= mask;
		else
		  d_flags &= ~mask;
	 }
};

DrawDIBDC* MenuWindow::s_popupDib = 0;
DrawDIBDC* MenuWindow::s_barDib = 0;
UWORD MenuWindow::s_instanceCount = 0;
Palette::PaletteID MenuWindow::s_popupPalID = 0;
Palette::PaletteID MenuWindow::s_barPalID = 0;

MenuItemList::~MenuItemList()
{
    for(Container::iterator it = d_items.begin(); it != d_items.end(); ++it)
    {
       MenuItemInfo& item = *it;
       delete item.d_subMenu;
       item.d_subMenu = 0;
    }
}

MenuWindow::MenuWindow(const CustomMenuData& data) :
  d_data(data),
  d_flags((d_data.d_type != CustomMenuData::CMT_BarMenu) ? Active : 0),
  d_cx(0),
  d_cy(0),
  d_itemCY(0),
  d_currentID(-1)
{
  s_instanceCount++;

  // d_items.setBlockSize(30);
  d_items.reserve(30);

  DWORD d_style = (d_data.d_type != CustomMenuData::CMT_BarMenu) ?
	 WS_POPUP | WS_CLIPSIBLINGS : WS_CHILD | WS_CLIPSIBLINGS;

  HWND hWnd = createWindow(
		0,
		// GenericNoBackClass::className(),
        NULL,
		d_style,
		0, 0, 0, 0,
		d_data.d_hParent, NULL  // , APP::instance()
  );

  ASSERT(hWnd != NULL);

}

LRESULT MenuWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
	 HANDLE_MSG(hWnd, WM_CREATE,	onCreate);
	 // HANDLE_MSG(hWnd, WM_DESTROY,	onDestroy);
	 HANDLE_MSG(hWnd, WM_PAINT, onPaint);
	 HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
	 HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
	 HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
	 HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);

	 default:
		return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL MenuWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  return TRUE;
}

BOOL MenuWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  return TRUE;
}

void MenuWindow::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  const DrawDIBDC* dib = (d_data.d_type == CustomMenuData::CMT_BarMenu) ? s_barDib : s_popupDib;

  if(dib)
	 BitBlt(hdc, 0, 0, d_cx, d_cy, dib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hwnd, &ps);
}

/*
 * Determine which item we are over
 * return -1 if not over an item
 * if windowOnly is true we must be over the window
 */

const int notOverItem  = -1;
const int borderCX = CustomBorderWindow::ThinBorder;

int MenuWindow::overWhichItem(int x, int y)
{
  if(d_items.size() == 0)
	 return notOverItem;

  const int bw = (d_data.d_type == CustomMenuData::CMT_BarMenu) ? 0 : borderCX;
  y += bw;

  /*
	* Make sure we are over window
	*/

  if( (x < 0 || x >= d_cx || y < bw || y >= d_cy - bw) )
  {
	 return notOverItem;
  }

  POINT p;
  p.x = x;
  p.y = y;

  for(int i = 0; i < d_items.size(); i++)
  {
	 const MenuItemInfo& mi = d_items[i];

	 // if a popup, just check y coord
	 if(d_data.d_type != CustomMenuData::CMT_BarMenu)
	 {
		if(y >= mi.d_rect.top && y <= mi.d_rect.bottom)
		  return i;
	 }

	 // otherwise use whole rect
	 else
	 {
		if(PtInRect(&mi.d_rect, p))
		  return i;
	 }
  }

  return notOverItem;
}

void MenuWindow::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
  switch(d_data.d_type)
  {
	 case CustomMenuData::CMT_BarMenu:
	 {
		onBarMenuMove(x, y);
		break;
	 }

	 case CustomMenuData::CMT_Popup:
	 {
		onPopupMove(x, y);
		break;
	 }

#ifdef DEBUG
	 default:
		FORCEASSERT("Type not found");
#endif
  }
}

void MenuWindow::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
}

void MenuWindow::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
  switch(d_data.d_type)
  {
	 case CustomMenuData::CMT_BarMenu:
	 {
		onBarMenuDown(x, y);
		break;
	 }

	 case CustomMenuData::CMT_Popup:
	 {
		onPopupDown(x, y);
		break;
	 }
  }
}

void MenuWindow::onBarMenuMove(int x, int y)
{
  // we have to be active
  if(d_flags & Active)
  {
	 int id = overWhichItem(x, y);

	 /*
	  * if we're over an item
	  */

	 if(id != notOverItem)
	 {
		if(id != d_currentID)
		{
		  selectItem(id);

		  const MenuItemInfo& mi = d_items[id];
		  ASSERT(mi.d_subMenu);

		  showSub(mi, true);

		  // redraw our menu
		  redraw();
		}
	 }
  }
}

void MenuWindow::onBarMenuDown(int x, int y)
{

  /*
	* If we're over a menu item show or hide submenu
	*/

  if( !(d_flags & Active) )
  {
	 int id = overWhichItem(x, y);
	 if(id != notOverItem)
	 {
		MenuItemInfo& mi = d_items[id];
		ASSERT(mi.d_subMenu);

		if(mi.d_state & MenuItemInfo::CMS_Enabled)		// if we're not active, show menu
		{
		  d_flags |= Active;

#ifdef DEBUG
		  g_logWindow.printf("Clicked on New item");
#endif
		  d_currentID = id;
		  mi.d_state |= MenuItemInfo::CMS_Selected;

		  showSub(mi, true);
		  redraw();
		}
	 }
  }
}

void MenuWindow::onPopupMove(int x, int y)
{
  ASSERT(d_flags & Active);

  POINT p;
  p.x = x;
  p.y = y;

  bool newCapture = false;
  /*
	* if mouse is over submenu, Set capture to it
	*/

  if( (d_currentID != notOverItem) &&
		(d_items[d_currentID].d_subMenu) )
  {

	 // get sub coord.
	 RECT r;
	 GetWindowRect(d_items[d_currentID].d_subMenu->getHWND(), &r);

	 // convert point to screen
	 ClientToScreen(getHWND(), &p);

	 // see if we are over window
	 if(PtInRect(&r, p))
	 {
#ifdef DEBUG
		g_logWindow.printf("Setting capture to submenu");
#endif

		SetCapture(d_items[d_currentID].d_subMenu->getHWND());
		newCapture = true;
	 }
  }

  /*
	* Otherwise, find what item (if any) we are over
	*/

  if(!newCapture)
  {
	 int id = overWhichItem(x, y);

	 /*
	  * if over an item select that item
	  */

	 if(id != notOverItem)
	 {
		if(id != d_currentID)
		{
#ifdef DEBUG
		  g_logWindow.printf("Popup Move");
#endif

		  selectItem(id);
		  redraw();

		  // show menu
		  const MenuItemInfo& mi = d_items[id];

		  // if a sub-menu, show it
		  if( (mi.d_subMenu) &&
				(mi.d_state & MenuItemInfo::CMS_Enabled) )
		  {
			 showSub(mi, false);
		  }
		}
	 }

	 else if(d_data.d_tlMenu)
	 {
		/*
		 * if parent is a barmenu set mouse capture back to it
		 */

		if( (y < 0) &&
			 (d_data.d_tlMenu->type() == CustomMenuData::CMT_BarMenu) )
		{
		  MapWindowPoints(getHWND(), d_data.d_tlMenu->getHWND(), &p, 1);

		  ASSERT(d_data.d_tlMenu->currentID() != notOverItem);
		  int id = d_data.d_tlMenu->overWhichItem(p.x, p.y);

		  // if we're over bar item that is not us, then release capture
		  if( (id != notOverItem) &&
				(id != d_data.d_tlMenu->currentID()) )
		  {
#ifdef DEBUG
			 g_logWindow.printf("Popup Releasing Capture");
#endif

			 ReleaseCapture();
			 hide();
		  }
		}

		/*
		 * otherwise release capture and setcapture to parent
		 */

		else if(d_data.d_tlMenu->type() == CustomMenuData::CMT_Popup)
		{
#ifdef DEBUG
		  g_logWindow.printf("Releasing capture back to parent");
#endif
		  ReleaseCapture();
		  SetCapture(d_data.d_tlMenu->getHWND());
		}
	 }
  }
}

void MenuWindow::onPopupDown(int x, int y)
{
  ASSERT(d_flags & Active);

  int id = overWhichItem(x, y);

  ReleaseCapture();

  /*
	* if over an item, send command message
	*/

  bool hidden = false;
  if(id != notOverItem)
  {
	 const MenuItemInfo& mi = d_items[id];
	 if( !(mi.d_subMenu) &&
		  !(mi.d_state & MenuItemInfo::CMS_Seperator) &&
			(mi.d_state & MenuItemInfo::CMS_Enabled) )
	 {
		hide();
		SendMessage(d_data.d_hCommand, WM_COMMAND, MAKEWPARAM(mi.d_id, 0), NULL);
		hidden = true;
	 }
  }
  else
  {
	 hideAllSubs();
	 hideAll();
	 hidden = true;
  }

  // set bar menu as inactive
  if(hidden)
  {
	 if( (d_data.d_tlMenu) &&
		  (d_data.d_tlMenu->type() == CustomMenuData::CMT_BarMenu) )
	 {
		d_data.d_tlMenu->setInactive();
	 }
  }
}


void MenuWindow::selectItem(int id)
{
  if(d_currentID != notOverItem)
  {
	 d_items[d_currentID].d_state &= ~MenuItemInfo::CMS_Selected;

	 // if item leads to a popup menu, hide it
	 if(d_items[d_currentID].d_subMenu)
		d_items[d_currentID].d_subMenu->hideAllSubs();
  }

  // show new items menu
  d_currentID = id;
  MenuItemInfo& mi = d_items[id];
  mi.d_state |= MenuItemInfo::CMS_Selected;
}

void MenuWindow::redraw()
{
  drawMenu();
  InvalidateRect(getHWND(), NULL, TRUE);
  UpdateWindow(getHWND());
}

#if 0     // Let MenuItemList's destructor do this
void MenuWindow::destroy()
{
  for(int i = 0; i < d_items.size(); i++)
  {
	 if(d_items[i].d_subMenu)
	 {
		d_items[i].d_subMenu->destroy();
		d_items[i].d_subMenu = 0;
	 }
  }

  DestroyWindow(getHWND());
}
#endif

void MenuWindow::showSub(const MenuItemInfo& mi, bool captureMouse)
{
  ASSERT(mi.d_subMenu);


  // convert to screen coordinates
  POINT p;
  p.x = (d_data.d_type != CustomMenuData::CMT_BarMenu) ? d_cx - 5 : mi.d_rect.left;
  p.y = (d_data.d_type != CustomMenuData::CMT_BarMenu) ? mi.d_rect.top : mi.d_rect.bottom;

  ClientToScreen(getHWND(), &p);

  PixelPoint px(p.x, p.y);
  mi.d_subMenu->show(px, captureMouse);

}

/*
 * does the text string contain an ampersand?
 */

bool hasAmp(const char* text)
{
  const char* c = text;

  while(*c != '\0')
  {
	 if(*c == '&')
	 {
		return true;
	 }

	 c++;
  }

  return false;
}

bool MenuWindow::calcItemSizes()
{
  if(d_items.size() > 0)
  {
	 /*
	  * Determine size needed to display item
	  *
	  */

	 ASSERT(d_data.d_hFont);

	 HDC hdc = GetDC(getHWND());
	 ASSERT(hdc);

	 HFONT oldFont = reinterpret_cast<HFONT>(SelectObject(hdc, d_data.d_hFont));

	 // get size of ampersand
	 const char* amp = "&";
	 SIZE ampSize;
	 GetTextExtentPoint32(hdc, amp, lstrlen(amp), &ampSize);

	 d_itemCY = ampSize.cy + (2 * d_mwc.offsetY);

	 int x = (d_data.d_type != CustomMenuData::CMT_BarMenu) ? borderCX + d_mwc.checkCX : 0;
	 int y = (d_data.d_type != CustomMenuData::CMT_BarMenu) ? borderCX : 0;

	 for(int i = 0; i < d_items.size(); i++)
	 {
		MenuItemInfo& mi = d_items[i];

		// if a seperator
		if(mi.d_state & MenuItemInfo::CMS_Seperator)
		{
		  SetRect(&mi.d_rect, x, y, x + d_cx, y + (d_itemCY / 2));
		  y += (d_itemCY / 2);

		  d_cy = y;
		}

		else if(mi.d_text.c_str())
		{
		  /*
			* Get size of menu text
			*/

		  const char* text = mi.d_text.c_str();

		  SIZE s;
		  GetTextExtentPoint32(hdc, text, lstrlen(text), &s);

		  // if we have an ampersand subtract its width
		  if(hasAmp(text))
			 s.cx -= ampSize.cx;

		  // add offsets
		  s.cx += (d_mwc.offsetX * 2);
		  s.cy += (d_mwc.offsetY * 2);

		  // set item rect
		  SetRect(&mi.d_rect, x, y, x + s.cx, y + s.cy);

		  if(d_data.d_type == CustomMenuData::CMT_BarMenu)
		  {
			 x += s.cx;

			 // adjust total size as needed
			 d_cx = x;

			 if(s.cy > d_cy)
				d_cy = s.cy;
		  }

		  else
		  {
			 y += s.cy;

			 // adjust total size as needed
			 d_cy = y;

			 // add offsets to size
			 s.cx += ((2 * borderCX) + d_mwc.checkCX + d_mwc.arrowCX);

			 if(s.cx > d_cx)
				d_cx = s.cx;
		  }
		}
	 }

	 /*
	  * adjust total size
	  */

	 if(d_data.d_type != CustomMenuData::CMT_BarMenu)
		d_cy += (borderCX);

	 SelectObject(hdc, oldFont);
	 ReleaseDC(getHWND(), hdc);

	 return true;
  }

  return false;
}

void MenuWindow::allocateStaticDib(CustomMenuData::Type type, int cx, int cy)
{
  if(type == CustomMenuData::CMT_BarMenu)
  {
	 if(!s_barDib ||
		 (s_barDib->getWidth() < cx) ||
		 (s_barDib->getHeight() < cy) ||
		 (s_barPalID != Palette::paletteID()) )
	 {
		if(s_barDib)
		{
		  delete s_barDib;
		  s_barDib = 0;
		}

		s_barDib = new DrawDIBDC(cx, cy);
		ASSERT(s_barDib);
		s_barPalID = Palette::paletteID();
	 }
  }
  else
  {
	 if(!s_popupDib ||
		 (s_popupDib->getWidth() < cx) ||
		 (s_popupDib->getHeight() < cy) ||
		 (s_popupPalID != Palette::paletteID()) )
	 {
		if(s_popupDib)
		{
		  delete s_popupDib;
		  s_popupDib = 0;
		}

		s_popupDib = new DrawDIBDC(cx, cy);
		ASSERT(s_popupDib);
		s_popupPalID = Palette::paletteID();
	 }
  }
}

void MenuWindow::drawItem(const MenuItemInfo& mi)
{
  DrawDIBDC* dib = (d_data.d_type == CustomMenuData::CMT_BarMenu) ? s_barDib : s_popupDib;
  ASSERT(dib);

  // if a seperator
  if(mi.d_state & MenuItemInfo::CMS_Seperator)
  {
	 const int x = borderCX;
	 const int y = (mi.d_rect.bottom + mi.d_rect.top) / 2;
	 const int cx = d_cx - (2 * borderCX);
	 const int w = 1;
	 const int alterBy = 50;

	 dib->darkenRectangle(x, y, cx, w, alterBy);
	 dib->lightenRectangle(x, y + w, cx, w, alterBy);
  }

  // otherwise
  else if(mi.d_text.c_str())
  {
	 const char* text = mi.d_text.c_str();

	 dib->setBkMode(TRANSPARENT);

	 COLORREF color = !(mi.d_state & MenuItemInfo::CMS_Enabled) ?
			PALETTERGB(0, 0, 0) : (mi.d_state & MenuItemInfo::CMS_Selected) ?
		 d_data.d_hColor : d_data.d_color;

	 COLORREF oldColor = SetTextColor(dib->getDC(), color);

	 int x = mi.d_rect.left + d_mwc.offsetX;
	 int y = mi.d_rect.top + d_mwc.offsetY;

	 /*
	  * If we have an ampersand, we don't print that
	  * The letter following the ampersand is drawn underlined
	  *
	  */

	 if(hasAmp(text))
	 {
		char buf[100];

		/*
		 * Step 1 - get and display part of string up to the ampersand
		 */


		// extract first part
		const char* c = text;

		int i = 0;
		while(*c != '&')
		{
		  buf[i++] = *c++;
		}

		buf[i] = '\0';

		// set font and get width
		dib->setFont(d_data.d_hFont);

		SIZE s;
		GetTextExtentPoint32(dib->getDC(), buf, lstrlen(buf), &s);

		wTextOut(dib->getDC(), x, y, buf);
		x += s.cx;

		/*
		 * Now draw underlined letter
		 */

		c++;
		i = 0;
		buf[i++] = *c;
		buf[i] = '\0';

		// set underline font and get width
		dib->setFont(d_data.d_hULFont);

		GetTextExtentPoint32(dib->getDC(), buf, lstrlen(buf), &s);

		wTextOut(dib->getDC(), x, y, buf);
		x += s.cx;

		/*
		 * Now the remainder of our text
		 */

		c++;

		i = 0;
		while(*c != '\0')
		{
		  buf[i++] = *c++;
		}

		buf[i] = '\0';

		// set font and get width
		dib->setFont(d_data.d_hFont);

		GetTextExtentPoint32(dib->getDC(), buf, lstrlen(buf), &s);
		wTextOut(dib->getDC(), x, y, buf);
	 }
	 else
	 {
		// display it
		wTextOut(dib->getDC(), x, y, text);
	 }

	 /*
	  * draw any icons
	  */

	 if( (d_data.d_type != CustomMenuData::CMT_BarMenu) &&
		  (mi.d_subMenu || mi.d_state & MenuItemInfo::CMS_Checked) )
	 {
		enum {
		  CheckImage,
		  ArrowImage
		};

		const int cx = (d_mwc.arrowCX - (d_mwc.offsetX));
		const int cy = (d_itemCY) / 2;

		// use other static dib for mask
		CustomMenuData::Type otherType = (d_data.d_type == CustomMenuData::CMT_BarMenu) ?
			  CustomMenuData::CMT_Popup : CustomMenuData::CMT_BarMenu;

		allocateStaticDib(otherType, cx, cy);

		DrawDIBDC* mask = (otherType == CustomMenuData::CMT_BarMenu) ? s_barDib : s_popupDib;
		ASSERT(mask);
		ASSERT(mask != dib);

		ColourIndex ci = mask->getColour(PALETTERGB(255, 255, 255));
		mask->rect(0, 0, cx, cy, ci);
		mask->setMaskColour(ci);

		ASSERT(d_data.d_checkImages);

		dib->setMaskColour(ci);

		// draw arrow for submenu
		if(mi.d_subMenu)
		{
		  d_data.d_checkImages->stretchBlit(mask, ArrowImage, 0, 0, cx, cy);

		  const int arrowOffset = (d_cx - borderCX - d_mwc.arrowCX);

		  DIB_Utility::colorize(dib, mask, color, 80,
			  (arrowOffset + (((d_cx - arrowOffset) - cx) / 2)),
			  y + ((d_itemCY - cy) / 2),
			  cx, cy, 0, 0);
		}

		// we're checked
		if(mi.d_state & MenuItemInfo::CMS_Checked)
		{
		  d_data.d_checkImages->stretchBlit(mask, CheckImage, 0, 0, d_mwc.checkCX, cy);

		  DIB_Utility::colorize(dib, mask, color, 80,
			  d_mwc.offsetX, y + ((d_itemCY - cy) / 2), d_mwc.checkCX, cy, 0, 0);
		}
	 }

	 dib->setTextColor(oldColor);
  }
}

void MenuWindow::drawMenu()
{
   if((d_cx == 0) || (d_cy == 0))
      return;

  ASSERT(d_items.size() > 0);
  ASSERT(d_cx > 0);
  ASSERT(d_cy > 0);

  // first, allocate dibs as needed
  allocateStaticDib(d_data.d_type, d_cx, d_cy);

  // fill in static dib, draw borders if a popup
  if(d_data.d_type != CustomMenuData::CMT_BarMenu)
  {
	 ASSERT(s_popupDib);
	 ASSERT(d_data.d_fillDib);
	 s_popupDib->rect(0, 0, d_cx, d_cy, d_data.d_fillDib);

	 RECT r;
	 SetRect(&r, 0, 0, d_cx, d_cy);

	 CustomBorderWindow bw(d_data.d_borderColors);
	 bw.drawThinBorder(s_popupDib->getDC(), r);
  }
  else
  {
	 ASSERT(s_barDib);
	 ASSERT(d_data.d_fillDib);
	 s_barDib->rect(0, 0, d_cx, d_cy, d_data.d_fillDib);
  }

  /*
	* Now draw Items
	*/

  for(int i = 0; i < d_items.size(); i++)
  {
	 drawItem(d_items[i]);
  }

}

void MenuWindow::show(const PixelPoint& p, bool captureMouse)
{
  if(calcItemSizes())
  {
	 /*
	  * If this is a bar menu, adjust width to parent
	  */

	 if(d_data.d_type == CustomMenuData::CMT_BarMenu)
	 {
		ASSERT(d_data.d_hParent);

      // Don't do anything if minimized

      if(IsIconic(d_data.d_hParent))
         return;

		RECT r;
		GetClientRect(d_data.d_hParent, &r);

		d_cx = r.right - r.left;
	 }

	 drawMenu();

	 /*
	  * make sure entire window is within mainwindows rect
	  */

	 int x = p.getX();
	 int y = p.getY();

	 if(d_data.d_type != CustomMenuData::CMT_BarMenu)
	 {
		RECT r;
		GetWindowRect(APP::getMainHWND(), &r);
		// check right side
		if((x + d_cx) > r.right)
		{
		  if(d_data.d_tlMenu)
		  {
			 RECT r;
			 GetWindowRect(d_data.d_tlMenu->getHWND(), &r);

			 x = (r.left + 5) - d_cx;
		  }
		  else
			 x -= d_cx;
		}

		// check bottom
		if((y + d_cy) > r.bottom)
		{
		  y -= d_cy;

		  // if this is a submenu then adjust by item height
		  if(d_data.d_tlMenu)
		  {
			 y += d_itemCY;
			 d_flags |= BottomUp;
		  }
		}

		if(captureMouse)
		{
#ifdef DEBUG
		  	g_logWindow.printf("Mouse Captured");
#endif
		  SetCapture(getHWND());
		}
	 }

//	 HWND hAfter = (d_data.d_type == CustomMenuData::CMT_BarMenu) ? HWND_TOP : HWND_TOPMOST;
	 SetWindowPos(getHWND(), HWND_TOP, x, y, d_cx, d_cy, SWP_SHOWWINDOW);
	 setFlag(Showing, true);
  }
}

/*
 * Enable state flag,
 */

bool MenuWindow::setState(int id, UBYTE mask, bool set)
{
  bool result = false;

  /*
	* First go through ID'd items
	*/

  for(int i = 0; i < d_items.size(); i++)
  {
	 MenuItemInfo& mi = d_items[i];

	 if(!mi.d_subMenu && mi.d_id == id)
	 {
		if(set)
		  mi.d_state |= mask;
		else
		  mi.d_state &= ~mask;

		result = true;
		break;
	 }

	 if(!result && mi.d_subMenu)
	 {
		/*
		 * Go through any submenus and see if the id is located there
		 */

		result = mi.d_subMenu->setState(id, mask, set);
		if(result)
		  break;
	 }
  }

  return result;
}

bool MenuWindow::setEnabledByPos(int pos, bool set, int& count)
{
  bool result = false;

  /*
	* First go through ID'd items
	*/

  for(int i = 0; i < d_items.size(); i++)
  {
	 MenuItemInfo& mi = d_items[i];

	 if(pos == count)
	 {
		if(set)
		  mi.d_state |= MenuItemInfo::CMS_Enabled;
		else
		  mi.d_state &= ~MenuItemInfo::CMS_Enabled;

		result = true;
		break;
	 }
	 else
	 {
		count++;

		if(mi.d_subMenu)
		  result = mi.d_subMenu->setEnabledByPos(pos, set, count);

		if(result)
		  break;
	 }
  }

  return result;
}

void MenuWindow::setInactive()
{
  ASSERT(d_data.d_type == CustomMenuData::CMT_BarMenu);
  d_flags &= ~Active;

  /*
	* Set all items to unselected
	*/

  for(int i = 0; i < d_items.size(); i++)
  {
	 d_items[i].d_state &= ~MenuItemInfo::CMS_Selected;
  }

  d_currentID = notOverItem;

  redraw();
}

void MenuWindow::hide()
{
//  setFlag(Active, false);
  setFlag(BottomUp, false);
  ShowWindow(getHWND(), SW_HIDE);
  setFlag(Showing, false);

  /*
	* Set all items to unselected
	*/

  for(int i = 0; i < d_items.size(); i++)
  {
	 d_items[i].d_state &= ~MenuItemInfo::CMS_Selected;
  }

  d_currentID = notOverItem;
}

/*
 * Hide all windows from the bottom up
 */

void MenuWindow::hideAll()
{
  hide();
  if(d_data.d_tlMenu &&
	  d_data.d_tlMenu->type() != CustomMenuData::CMT_BarMenu)
  {
	 d_data.d_tlMenu->hideAll();
  }
}

/*
 * Hide all windows from the top down
 */

void MenuWindow::hideAllSubs()
{
  if( (d_currentID != notOverItem) &&
		(d_items[d_currentID].d_subMenu) )
  {
	 d_items[d_currentID].d_subMenu->hideAllSubs();
  }

  hide();
}

BOOL MenuWindow::insertMenuItem(int i, BOOL f, const MenuItemInfo* info)
{
	d_items.insert(*info, i);

	if(info->d_subMenu)
	{
		info->d_subMenu->setParent(this);
	}

	if(d_flags & Showing)
	{
		RECT r;
		GetClientRect(getHWND(), &r);

		show(PixelPoint(r.left,r.top));
	}

	return TRUE;
}


void MenuWindow::setParent(MenuWindow* parent)
{
	d_data.d_tlMenu = parent;
}


/*--------------------------------------------------------------------
 *  Implementation class
 */

class CustomMenu_Imp {
	 MenuWindow* d_menu;    // top-level menu

  public:
	 CustomMenu_Imp() :
		d_menu(0) {}

	 ~CustomMenu_Imp();

	 void init(CustomMenuData& data, HMENU hmenu);
	 void init(CustomMenuData& data);
	 void run(const PixelPoint& p)
	 {
		if(d_menu)
		  d_menu->show(p);
	 }

	 void destroy();

	 bool enable(int id, bool set, bool byPosition)
	 {
		if(d_menu)
		  return d_menu->enable(id, set, byPosition);
		else
		  return false;
	 }

	 bool checked(int id, bool set)
	 {
		if(d_menu)
		  return d_menu->checked(id, set);
		else
		  return false;
	 }

	 void hide() { d_menu->hideAllSubs(); }

	 int width() const { return d_menu->width(); }
	 int height() const { return d_menu->height(); }

		MenuWindow* getSubMenu(MenuWindow* h, int id)
		{
			if(h == 0)
				return d_menu;

			MenuItemInfo* info = h->menuItem(id);

			if(info)
				return info->d_subMenu;
			else
				return 0;
		}

		MenuWindow* createPopupMenu();

  private:

	 void convertMenu(HMENU h, MenuWindow* mw, CustomMenuData& data);
};

CustomMenu_Imp::~CustomMenu_Imp()
{
    destroy();
    // selfDestruct();
    // delete d_menu;
}

void CustomMenu_Imp::destroy()
{
  if(d_menu)
  {
     delete d_menu;
	 // d_menu->destroy();
	 d_menu = 0;
  }
}



void CustomMenu_Imp::init(CustomMenuData& data, HMENU hmenu)
{

  ASSERT(hmenu);

  /*
	* create window
	*/

  d_menu = new MenuWindow(data);
  ASSERT(d_menu);


  /*
	* Convert to our own data
	*/


  if(data.d_type == CustomMenuData::CMT_Popup)
  {
	 /*
	  * If this is a pop-up window, ignore the top-level menu
	  */

	 hmenu = GetSubMenu(hmenu, 0);
	 ASSERT(hmenu);
  }

  convertMenu(hmenu, d_menu, data);

}



void CustomMenu_Imp::init(CustomMenuData& data)
{
  /*
	* create window
	*/

  d_menu = new MenuWindow(data);
  ASSERT(d_menu);

  /*
	* load menu from resource
	*/

  HMENU h = LoadMenu(APP::instance(), MAKEINTRESOURCE(data.d_menuID));
  ASSERT(h);

  /*
	* Convert to our own data
	*/


  if(data.d_type == CustomMenuData::CMT_Popup)
  {
	 /*
	  * If this is a pop-up window, ignore the top-level menu
	  */

	 h = GetSubMenu(h, 0);
	 ASSERT(h);
  }

  convertMenu(h, d_menu, data);

  /*
	* Clear out window's menu
	*/

  DestroyMenu(h);
}

/*
 * Convert to our own data
 *
 * Warning! This function may be recursive
 */

void CustomMenu_Imp::convertMenu(HMENU h, MenuWindow* menu, CustomMenuData& data)
{
  // go through top-level menu
  int nItems = GetMenuItemCount(h);
  for(int i = 0; i < nItems; i++)
  {
	 MenuItemInfo mi;

	 char buf[200];
	 int result = GetMenuString(h, i, buf, 200, MF_BYPOSITION);
	 ASSERT(result == lstrlen(buf));

	 mi.d_text = buf;		// copyString(buf);

	 HMENU hSub = GetSubMenu(h, i);
	 if(hSub)    // Warning! Recursive, if here
	 {
		data.d_tlMenu = menu;
		data.d_type = CustomMenuData::CMT_Popup;
		mi.d_subMenu = new MenuWindow(data);
		ASSERT(mi.d_subMenu);

		menu->add(mi);

		convertMenu(hSub, mi.d_subMenu, data);
	 }
	 else
	 {
		// get id
		UINT id = GetMenuItemID(h, i);
		if(id != 0xFFFFFFFF)
		{
		  mi.d_id = id;
		}

		id = GetMenuState(h, i, MF_BYPOSITION);
		ASSERT(id != 0xFFFFFFFF);

		// see if this is a seperator
		if(id & MF_SEPARATOR)
		{
		  mi.d_state |= MenuItemInfo::CMS_Seperator;
		}

		menu->add(mi);
	 }
  }
}

MenuWindow* CustomMenu_Imp::createPopupMenu()
{
	CustomMenuData data = d_menu->data();
	data.d_tlMenu = 0;
	data.d_type = CustomMenuData::CMT_Popup;
	MenuWindow* mw = new MenuWindow(data);
	return mw;
}

/*----------------------------------------------------------------------
 *  Interface class
 */

CustomMenu::CustomMenu() :
  d_menu(new CustomMenu_Imp)
{
  ASSERT(d_menu);
}

CustomMenu::~CustomMenu()
{
  if(d_menu)
	 delete d_menu;
}

void CustomMenu::init(CustomMenuData& data, HMENU hmenu)
{
  if(d_menu)
	 d_menu->init(data, hmenu);
}

void CustomMenu::init(CustomMenuData& data)
{
  if(d_menu)
	 d_menu->init(data);
}

void CustomMenu::run(const PixelPoint& p)
{
  if(d_menu)
	 d_menu->run(p);
}

void CustomMenu::destroy()
{
  if(d_menu)
	 d_menu->destroy();
}

bool CustomMenu::enable(int id, bool set, bool byPosition)
{
  return d_menu->enable(id, set, byPosition);
}

bool CustomMenu::checked(int id, bool set)
{
  return d_menu->checked(id, set);
}

int CustomMenu::width() const
{
  return d_menu->width();
}

int CustomMenu::height() const
{
  return d_menu->height();
}

void CustomMenu::hide()
{
  d_menu->hide();
}


MenuWindow* CustomMenu::getSubMenu(MenuWindow* h, int id)
{
	return d_menu->getSubMenu(h, id);
}


int CustomMenu::getMenuItemCount(const MenuWindow* h)
{
	return h->getItemCount();
}

BOOL CustomMenu::insertMenuItem(MenuWindow* h, int i, BOOL f, const MenuItemInfo* info)
{
	return h->insertMenuItem(i,f,info);
}

MenuWindow* CustomMenu::createPopupMenu()
{
	return d_menu->createPopupMenu();
}

/*----------------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------------
 */