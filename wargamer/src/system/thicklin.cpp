/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Thick Line Graphic Engine Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "thicklin.hpp"
#include "dib.hpp"
#include <math.h> // for abs()

ThickLineEngine::ThickLineEngine(DrawDIB* dib) :
	d_dib(dib),
	d_thick(1),
	// d_col(RGB(0,0,0)),
	d_colIndex(0),
	// d_remapTable(0),
	d_circleSize(0),
	d_circleDIB(0),
	d_lastPoint(),
	d_closed(true)
	// d_lastDir(0)
{
	// d_colIndex = dib->getColour(d_col);
}

ThickLineEngine::~ThickLineEngine()
{
	delete d_circleDIB;
	d_circleDIB = 0;

#if 0
	if(d_remapTable != 0)
	{
		Palette::releaseTable(d_remapTable);
		d_remapTable = 0;
	}
#endif
}

void ThickLineEngine::addPoint(const GPoint& p)
{
	if(d_closed)
	{
		d_closed = false;

		// Draw filled circle

		d_dib->disc(p.x(), p.y(), d_thick, d_colIndex);
		// d_lastDir = 0;
	}
	else
	{
#if 0
		if(d_remapTable == 0)
		{
			d_remapTable = Palette::getColorizeTable(d_col, 30);
		}
#endif

		// Apply Bresenham from lastPoint to p
		// Must move in up/down/left/right steps
		// for the thick line algorithm to work.

		// For now just draw line from last point

		// d_dib->line(d_lastPoint.x(), d_lastPoint.y(), p.x(), p.y(), d_colIndex);

		int dx = p.x() - d_lastPoint.x();
		int dy = p.y() - d_lastPoint.y();
		int adx = abs(dx);
		int ady = abs(dy);

		int sx;
		if(dx > 0)
			sx = 1;
		else
			sx = -1;

		int sy;
		if(dy > 0)
			sy = 1;
		else
			sy = -1;

		if(adx >= ady)
		{	// X is major axis

			int yVal = adx / 2;

			while(d_lastPoint.x() != p.x())
			{
				yVal += ady;
				if(yVal >= adx)
				{
					yVal -= adx;
					plot(0,sy);
				}

				plot(sx,0);
			}
		}
		else
		{
			// Y is major axis
			int xVal = ady / 2;

			while(d_lastPoint.y() != p.y())
			{
				xVal += adx;
				if(xVal >= ady)
				{
					xVal -= ady;
					plot(sx,0);
				}

				plot(0,sy);
			}
		}
	}

	d_lastPoint = p;
}

void ThickLineEngine::close()
{
	// ASSERT(!d_closed);
	d_closed = true;
}

void ThickLineEngine::plot(int dx, int dy)
{
	ASSERT((dx == 0) || (dy == 0));

	GPoint lastPoint = d_lastPoint;
	d_lastPoint += GPoint(dx,dy);

	if(d_thick == 0)
	{
		d_dib->plot(d_lastPoint.x(), d_lastPoint.y(), d_colIndex);
	}
	else
	{
		/*
	 	 * Create circle DIB if not already made
	 	 */

		int dibSize = d_thick * 2 + 1;

		if( (d_circleDIB == 0) || (d_circleDIB->getWidth() < dibSize) )
		{
			if(d_circleDIB == 0)
				d_circleDIB = new DrawDIB(dibSize, dibSize);
			else
				d_circleDIB->resize(dibSize, dibSize);

			d_circleDIB->setMaskColour(d_colIndex ^ 0x80);
		}

		ASSERT(d_circleDIB != 0);

		if(d_circleSize != dibSize)
		{
			d_circleDIB->fill(d_circleDIB->getMask());
			d_circleDIB->circle(dibSize/2, dibSize/2, d_thick, d_colIndex);
			d_circleSize = dibSize;
		}

		/*
	 	 * Convert to 1 of 8 directions
	 	 */

#if 1
	#define BlitDot(x,y,w,h,dib,sx,sy)	\
		d_dib->blit(x, y, w, h,	dib, sx,sy)
#else
	#define BlitDot(x,y,w,h,dib,sx,sy)	\
		d_dib->remapDIB(dib,d_remapTable,x,y,w,h,sx,sy)
#endif

		if(dx < 0) 
		{	// Left
			BlitDot(d_lastPoint.x()-d_thick, d_lastPoint.y()-d_thick, d_thick+1, dibSize,
				d_circleDIB, 0,0);
		}
		else if(dx > 0)
		{	// Right
			BlitDot(d_lastPoint.x(), d_lastPoint.y()-d_thick, d_thick+1, dibSize,
				d_circleDIB, d_thick,0);
		}
		else if(dy < 0) 
		{	// Up
			BlitDot(d_lastPoint.x()-d_thick, d_lastPoint.y()-d_thick, dibSize, d_thick+1,
				d_circleDIB, 0,0);
		}
		else if(dy > 0)
		{	// Down
			BlitDot(d_lastPoint.x()-d_thick, d_lastPoint.y(), dibSize, d_thick+1,
				d_circleDIB, 0,d_thick);
		}

#if 0
		UBYTE combinedDir = dir | d_lastDir;

		// Now have 1 of 16 values
		// *=illegal

		// 0*: Stationary
		// 1 : Left
		// 2 : Right
		// 3*: Left, Right
		// 4 : Up
		// 5 : Left, Up
		// 6 : Right, Up
		// 7*: Left, Right, Up
		// 8 : Down
		// 9 : Left, Down
		// A : Right, Down
		// B*: Left, Right, Down
		// C*: Up, Down
		// D*: Left, Up, Down
		// E*: Right, Up, Down
		// F*: Left, Right, Up, Down

		d_lastPoint += GPoint(dx,dy);
		d_lastDir = dir;

	 	// d_dib->circle(d_lastPoint.x(), d_lastPoint.y(), d_thick, d_colIndex);
	 	d_dib->circle(d_lastPoint.x(), d_lastPoint.y(), d_thick, combinedDir);
#endif
	}
}

void ThickLineEngine::thickness(int thick)
{
	thick = thick / 2;		// Convert diameter into radius

	if(thick != d_thick)
	{
#if 0
		if(thick > d_thick)
		{
			delete d_circleDIB;
			d_circleDIB = 0;
		}
#endif

		d_thick = thick;
	}
}

#if 0
void ThickLineEngine::color(COLORREF col)
{
	// Get remap Table updated

	if( (col != d_col) && (d_remapTable != 0) )
	{
		Palette::releaseTable(d_remapTable);
		d_remapTable = 0;
	}

	d_col = col;
	d_colIndex = d_dib->getColour(d_col);
}
#else
void ThickLineEngine::color(ColourIndex col)
{
	d_colIndex = col;
}
#endif

