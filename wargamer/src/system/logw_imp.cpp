/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Log Window
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "logw_imp.hpp"

#if !defined(NOLOG)			// Do nothing if NOLOG defined

#include <windows.h>
#include <stdio.h>
#include <stdarg.h>
#include "app.hpp"
#include "wmisc.hpp"
#include "msgenum.hpp"
// #include "windsave.hpp"
#include "misc.hpp"


static const int MaxLogWindowLines = 1000;	// Maximum number of lines to store in log window

const char WindowsLogWindow::s_className[] = "LogWindow";
static WSAV_FLAGS s_saveFlags = WSAV_POSITION | WSAV_SIZE | WSAV_RESTORE | WSAV_MINIMIZE;

// static const char logWindowRegName[] = "logWindow";
// static const char logWindowRegKey[] = "Position";


ATOM WindowsLogWindow::classAtom = 0;

ATOM WindowsLogWindow::registerClass()
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_DBLCLKS;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(WindowsLogWindow*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}


WindowsLogWindow::WindowsLogWindow(HWND parent, const char* _title, const char* fileName) :
	LogFileFlush(fileName)
//	d_owner(0)
{
	init(parent, _title, fileName);
}
#if 0
WindowsLogWindow::WindowsLogWindow(LogWinPtr* own, HWND parent, const char* _title, const char* fileName) :
	LogFileFlush(fileName),
	d_owner(own)
{
	init(parent, _title, fileName);
	d_owner->set(this);
}
#endif

// void WindowsLogWindow::make(LogWinPtr* owner, HWND parent, const char* title, const char* fileName)
LogWindow* WindowsLogWindow::make(HWND parent, const char* title, const char* fileName)
{
	// new WindowsLogWindow(owner, parent, title, fileName);
	return new WindowsLogWindow(parent, title, fileName);
}

void WindowsLogWindow::init(HWND parent, const char* title, const char* fileName)
{
#ifdef DEBUG
	debugLog("WindowsLogWindow::create\n");
#endif

	registerClass();

	d_title = title;
	// showFlag = True;

	d_lineHeight = 0;
	d_clientHeight = 0;
	d_topLine = 0;
	d_topLineNumber = 0;
	d_atBottom = True;
	d_maxLines = MaxLogWindowLines;			// An arbitary value for the maximum number of lines that are stored


	long style = WS_POPUP | WS_CAPTION | WS_BORDER | WS_SYSMENU | WS_SIZEBOX | WS_MINIMIZEBOX |
			  WS_MAXIMIZEBOX | WS_SIZEBOX | WS_VSCROLL | WS_CLIPSIBLINGS;

	hWnd = createWindow(
		WS_EX_LEFT,
		// s_className,
		NULL,
		style,
		10,			      		/* init. x pos */
		60,			      		/* init. y pos */
		200,			      		/* init. x size */
		200,			      		/* init. y size */
		parent,				     	/* parent window */
		NULL
		// wndInstance(parent)
	);

	ASSERT(hWnd != NULL);

	SetWindowText(hWnd, title);
//	if(!restoreWindowState(logWindowRegKey, hWnd, WSAV_ALL, title))
   if(!getState(title, s_saveFlags))
      SetWindowPos(hWnd, HWND_TOP, 0,0,200,200, SWP_SHOWWINDOW);
}

WindowsLogWindow :: ~WindowsLogWindow()
{
	selfDestruct();
}

LRESULT WindowsLogWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_LOGWINDOW
	debugLog("LogWindow::procMessage(%s)\n",
		getWMdescription(hWnd, msg, wParam, lParam));
#endif

	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	onCreate);
		HANDLE_MSG(hWnd, WM_DESTROY,	onDestroy);
		HANDLE_MSG(hWnd, WM_PAINT, onPaint);
		HANDLE_MSG(hWnd, WM_VSCROLL, onVScroll);
		HANDLE_MSG(hWnd, WM_SIZE, onSize);
		HANDLE_MSG(hWnd, WM_QUERYOPEN, onQuery);
		HANDLE_MSG(hWnd, WM_SHOWWINDOW, onShow);
		HANDLE_MSG(hWnd, WM_SYSCOMMAND, onSysCommand);

	default:
		return defProc(hWnd, msg, wParam, lParam);
	}
}



BOOL WindowsLogWindow :: onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  HDC hdc = GetDC(hWnd);
  ASSERT(hdc != 0);
  font.setFont(hdc, 16, 0);

  TEXTMETRIC tm;
  GetTextMetrics(hdc, &tm);

  d_lineHeight = tm.tmHeight + tm.tmExternalLeading;

  SetBkMode(hdc, TRANSPARENT);
  ReleaseDC(hWnd, hdc);
  return TRUE;
}

void WindowsLogWindow::onSysCommand(HWND hwnd, UINT cmd, int x, int y)
{
	if( (cmd & ~0xf) == SC_CLOSE)
	{
		hide();
	}
	else
	{
		FORWARD_WM_SYSCOMMAND(hwnd, cmd, x, y, defProc);
	}
}

void WindowsLogWindow :: onSize(HWND hWnd, UINT state, int cx, int cy)
{
	Writer lock(&d_lock);

	// Set client Height

	d_clientHeight = cy;
	// d_linesOnDisplay = (d_clientHeight + d_lineHeight - 1) / d_lineHeight;
	d_linesOnDisplay = d_clientHeight / d_lineHeight;

	setTopLine();

	// Set up scroll bar

	setScrollBar();

	InvalidateRect(hWnd, NULL, True);
}

void WindowsLogWindow::setTopLine()
{
	Writer lock(&d_lock);

	if(d_textList.entries())
	{
		if(d_atBottom)
		{
			// Count up from bottom

			LogWindowText* bottomLine = d_textList.last();
			LogWindowText* topLine = bottomLine;
			int lineCount = d_textList.entries();
			int n = d_linesOnDisplay;

			while( (n > 0) && bottomLine && (lineCount > 0))
			{
				n--;
				lineCount--;
				topLine = bottomLine;
				bottomLine = d_textList.prev(bottomLine);
			}

			d_topLine = topLine;
			d_topLineNumber = lineCount;
		}
		else
		{
			if(d_topLine == 0)
			{
				d_topLine = d_textList.first();
				d_topLineNumber = 0;
			}
		}
	}
	else
	{
		d_topLine = 0;
		d_topLineNumber = 0;
	}
}

void WindowsLogWindow::setScrollBar()
{
	SCROLLINFO si;
	si.cbSize = sizeof(si);
	si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS | SIF_DISABLENOSCROLL;
	si.nMin = 0;
	si.nMax = d_textList.entries() - 1;
	si.nPage = d_linesOnDisplay;
	// si.nPage = d_clientHeight / d_lineHeight;		// Fiddle about because may not be exact multiple of lineHeight
	si.nPos = d_topLineNumber;
	// if(d_atBottom && (si.nPage != d_linesOnDisplay))
	// 	si.nPos++;
	si.nTrackPos = 0;
	SetScrollInfo(hWnd, SB_VERT, &si, TRUE);
}



BOOL WindowsLogWindow :: onQuery(HWND hWnd)
{
  PostMessage(hWnd, WM_VSCROLL, SB_BOTTOM, 0);
  return TRUE;
}

void WindowsLogWindow :: onShow(HWND hWnd, BOOL fShow, UINT status)
{
  PostMessage(hWnd, WM_VSCROLL, SB_BOTTOM, 0);
}



void WindowsLogWindow :: onVScroll(HWND hWnd, HWND hCntrl, UINT code, int pos)
{
	Writer lock(&d_lock);

	int oldTop = d_topLineNumber;
	Boolean oldBottom = d_atBottom;

  	switch(code)
	{
	 	case SB_TOP:
			d_topLine = d_textList.first();
			d_topLineNumber = 0;
			break;
	 	case SB_BOTTOM:
			d_atBottom = True;
			break;
	 	case SB_LINEUP:
			d_topLineNumber--;
			break;
	 	case SB_LINEDOWN:
			d_topLineNumber++;
			break;
	 	case SB_PAGEUP:
			d_topLineNumber -= (d_linesOnDisplay - 1);
			break;
	 	case SB_PAGEDOWN:
			d_topLineNumber += (d_linesOnDisplay - 1);
			break;
	 	case SB_THUMBPOSITION:
			d_topLineNumber = pos;
			break;
	 	default:
			break;
  	}

	/*
	 * Clip d_topLineNumber
	 */

	int maxLine = d_textList.entries() - d_linesOnDisplay;
	if(maxLine < 0)
		maxLine = 0;

	if(d_topLineNumber >= maxLine)
	{
		d_topLineNumber = maxLine;
		d_atBottom = True;
	}
	else
		d_atBottom = False;

	if(d_topLineNumber < 0)
		d_topLineNumber = 0;

	if(!d_atBottom)
	{
		if(oldTop != d_topLineNumber)
		{
			if(d_topLineNumber == 0)
			{
				d_topLine = d_textList.first();
			}
			else if(oldTop < d_topLineNumber)
			{
				for(int lineCount = oldTop; lineCount < d_topLineNumber; lineCount++)
				{
					d_topLine = d_textList.next(d_topLine);

					ASSERT(d_topLine != 0);
				}
			}
			else if(oldTop > d_topLineNumber)
			{
				for(int lineCount = oldTop; lineCount > d_topLineNumber; lineCount--)
				{
					d_topLine = d_textList.prev(d_topLine);

					ASSERT(d_topLine != 0);
				}
			}
		}
	}
	else
		setTopLine();

	if(oldBottom != d_atBottom)
	{
		setScrollBar();
		InvalidateRect(hWnd, NULL, True);
	}
	else if(oldTop != d_topLineNumber)
	{
		setScrollBar();
		ScrollWindow(hWnd, 0, (oldTop - d_topLineNumber) * d_lineHeight, NULL, NULL);
		UpdateWindow(hWnd);
	}

}


void WindowsLogWindow :: onPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);

	int y = 0;

	{
		Reader lock(&d_lock);

		LogWindowText* line = d_topLine;
		while(line && (y < ps.rcPaint.bottom))
		{
			if( (y + d_lineHeight) >= ps.rcPaint.top)
				wTextOut(hdc, 0, y, line->text());
			y += d_lineHeight;
			line = d_textList.next(line);
		}
	}
  	EndPaint(hWnd, &ps);
}

void WindowsLogWindow :: onDestroy(HWND hWnd)
{
	notifyParent();

//--- Dont do this... causes recursion
//	if(d_owner)
//		d_owner->clear();

   saveState(d_title, s_saveFlags);
}


void WindowsLogWindow :: vprintf(const char* fmt, va_list vaList)
{
	//------ This doesn't work, because vaList gets changed
	// va_list vaListCopy = vaList;
	// superLog::vprintf(fmt, vaListCopy);

  char buffer[512];
  buffer[511] = 0;
  vbprintf(buffer, 511, fmt, vaList);

  superLog::printf("%s", buffer);

  // Look for newlines and treat as different line

  char* start = buffer;
  while(*start)
  {
  		char* s = start;
		while(*s)
		{
			if( (*s == '\r') || (*s == '\n'))
			{
				*s++ = 0;
				break;
			}
			else
				++s;
		}

		LogWindowText* newLine = new LogWindowText(start);

		start = s;
		while( (*start == '\r') || (*start == '\n') )
			++start;

		Writer lock(&d_lock);

		d_textList.append(newLine);

		if(d_textList.entries() >= d_maxLines)
		{
			if(d_topLineNumber == 0)
			{
				d_topLine = d_textList.next(d_topLine);
			}
			else
				d_topLineNumber--;

			LogWindowText* first = d_textList.first();

			d_textList.unlink(first);
			delete first;
		}
	}

	setTopLine();

	setScrollBar();


	InvalidateRect(getHWND(), NULL, TRUE);

}

#if 0
void WindowsLogWindow :: show()
{
  if(!showFlag) {
	 ShowWindow(getHWND(), SW_RESTORE);
	 showFlag = TRUE;
  }
}

void WindowsLogWindow :: hide()
{
  if(showFlag) {
	 ShowWindow(getHWND(), SW_HIDE);
	 showFlag = FALSE;
  }
}
#endif

#endif	// NOLOG
