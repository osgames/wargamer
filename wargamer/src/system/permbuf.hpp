/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PERMBUF_HPP
#define PERMBUF_HPP

#ifndef __cplusplus
#error permbuf.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Permanent Buffer Templated Class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/*
 * Class for holding a permanent buffer
 * Caller must release it when finished with it
 */

class OwnedData {
	Boolean inUse;
public:
	OwnedData() { inUse = False; }

	Boolean reserve()
	{
		ASSERT(!inUse);

		if(!inUse)
		{
			inUse = True;
			return True;
		}
		else
			return False;
	}

	void release()
	{
		ASSERT(inUse);
		inUse = False;
	}
};

template<int BufSize>
class PermanentBuffer : public OwnedData {
	char text[BufSize + 1];
public:
	PermanentBuffer() { text[BufSize] = 0; }

	Boolean reserve()
	{
		if(OwnedData::reserve())
		{
			clear();
			return True;
		}
		else
			return False;
	}

	void clear() { text[0] = 0; }

	void printf(const char* fmt, ...)
	{
		va_list vaList;
		va_start(vaList, fmt);
		_vbprintf(text + strlen(text), BufSize, fmt, vaList);
		va_end(vaList);
	}

	void copy(const char* s)
	{
		ASSERT(strlen(s) <= BufSize);

		strncpy(text, s, BufSize);
	}

	void append(const char* s)
	{
		size_t pos = strlen(text);
		size_t maxSize = BufSize - pos;
		ASSERT(strlen(s) <= maxSize);
		strncpy(&text[pos], s, maxSize);
	}

	const char* get() { return text; }
};


#endif /* PERMBUF_HPP */

