/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef APP_H
#define APP_H

#ifndef __cplusplus
#error app.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Data Global To Application
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
// #include <windows.h>
#include "mytypes.h"

class APP {
	static HINSTANCE	s_hinst;					// Global Instance
	static LPSTR 		s_lpCmdLine;			// Command Line
	static int			s_nCmdShow;				// Initial show state
	static HWND			s_activeDialogue;		// Needed for isDialogMessage()
	static HWND			s_activePropsheet;	// Needed for PropSheet_IsDialogMessage
	static HWND			s_mainHWND;				// Handle to Main Window
	static DWORD      s_interfaceThreadID; // Thread ID to user-interface

 public:
	SYSTEM_DLL APP();
		// Construct from WinMain parameters
	virtual ~APP() { }
		// Virtual Destructor

	/*
	 * Manipulators
	 */


	SYSTEM_DLL int execute(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow);
		// Run the program...

	/*
	 * Accessor Functions
	 */

	SYSTEM_DLL static HINSTANCE instance();	// { return s_hinst; }


	SYSTEM_DLL static void setActiveDial(HWND hDial);
	SYSTEM_DLL static void clearActiveDial(HWND hDial);
		// Set/clear active Dialog (required for MessageLoop)

	SYSTEM_DLL static void setPropsheet(HWND hProp);
	SYSTEM_DLL static void clearPropsheet(HWND hProp);
		// Set/Clear active Property Sheet (required for MessageLoop)

	SYSTEM_DLL static HWND getMainHWND();
		// Return Handle of main Window
	SYSTEM_DLL static void setMainHWND(HWND hWnd);
		// Set the Main Window handle

	SYSTEM_DLL static DWORD getInterfaceThreadID();	// { return s_interfaceThreadID; }

	/*-----------------------------------------------------------
	 * Virtual Functions
	 * Declared Private because they are only ever called
	 * from within APP
	 */

 private:
	virtual BOOL initApplication() = 0;
		// Register Classes, and so forth

	virtual BOOL initInstance() = 0;
		// Set up for this Instance of the application

	virtual BOOL doCmdLine(LPCSTR cmd) { return TRUE; }
		// Process Commandline (not required for all applications)

	virtual void endApplication() = 0;
		// Tidy Up application (possibly ought to be done in destructor?)

	virtual Boolean processMessage(MSG* msg) = 0;
		// Process Message.
		//   Return True if application dealt with it
		//   False to continue as usual

	/*
	 * Internal Private Functions
	 */

	BOOL init(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow);
	int messageLoop();
};

#endif /* APP_H */
/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */


