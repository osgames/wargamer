/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef LOGWIN_HPP
#define LOGWIN_HPP

#ifndef __cplusplus
#error logwin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Interface for LogWindow
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <stdarg.h>
#include <windef.h>
#include "except.hpp"

class SYSTEM_DLL LogWindow
{
   // Private Functions to avoid accidental copying

   LogWindow(const LogWindow& logWin);
   const LogWindow& operator = (const LogWindow& logWin);

  public:
   LogWindow() { }

   // static LogWindow* make(HWND parent, const char* title, const char* fileName);
   
   // virtual void init(HWND parent, const char* title, const char* fileName) = 0;

   virtual ~LogWindow() { }

   virtual void vprintf(const char* fmt, va_list vaList) = 0;
   virtual void show() = 0;
   virtual void hide() = 0;
   // virtual HWND getHWND() = 0;

   void printf(const char* fmt, ...);

   class LogWindowError : public GeneralError {
    public:
      LogWindowError() : GeneralError("Log Window Error") { }
   };
};

/*
 * Pointer to LogWindow
 */

class SYSTEM_DLL LogWinPtr {
   LogWindow* d_logWindow;
 public:
   LogWinPtr();
   ~LogWinPtr();

   void set(LogWindow* logWin);
   void clear();

   LogWindow* get() { return d_logWindow; }

   void printf(const char* fmt, ...);
   void vprintf(const char* fmt, va_list vaList);

};

class SYSTEM_DLL NullLogWindow : 
   public LogWindow
{
  private:
      // constructor is private because they can only be created
      // by WindowsLogWindow::make()

    // NullLogWindow(LogWinPtr* owner, HWND parent, const char* title, const char* fileName);
  public:
    NullLogWindow() { }

    ~NullLogWindow() { }

    // static void make(LogWinPtr* owner, HWND parent, const char* title, const char* fileName) { }

    void vprintf(const char* fmt, va_list vaList) { }
    void printf(const char* fmt, ...) { }
    void show() { }
    void hide() { }
};

extern SYSTEM_DLL LogWinPtr g_logWindow;      // System logwindow

#endif /* LOGWIN_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
