/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SWGSPR_H
#define SWGSPR_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Sprite Header
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "filedata.hpp"

namespace SWG_Sprite
{

static const char SPRITE_ID[4] = { 'S','P','R','T' };


typedef struct {
	Boolean lzHuf:1;			// Set if LZhuf compression used on data
	Boolean rle:1;
	Boolean nybble:1;
	Boolean squeeze:1;
	Boolean shadow:1;
	Boolean remap:1;
} SpriteFlags;

#ifdef _MSC_VER
#pragma pack (push, 1)
#endif
#ifndef __WATCOM_CPLUSPLUS__
#define _Packed
#endif

typedef _Packed struct {
	UWORD width;
	UWORD height;
	UWORD fullWidth;
	UWORD fullHeight;
	UWORD xOffset;
	UWORD yOffset;
	UWORD anchorX;
	UWORD anchorY;
	UBYTE shadowColor;
	UBYTE transparentColor;
	UWORD paletteIndex;
	SpriteFlags flags;
} SpriteHeader;

_Packed struct PaletteHeader {
	UWORD nColors;
};
#ifdef _MSC_VER
#pragma pack (pop)
#endif



static const int RemapColorCount = 16;

typedef UBYTE REMAP_TABLE[RemapColorCount];

/*
 * Disk File structure
 */

typedef _Packed struct {
	char id[4];
	IWORD headerSize;
	IBYTE versionOrder;
	IWORD spriteCount;
	IWORD paletteCount;
} SpriteFileHeaderD;

/* Version order usage */

static const UBYTE IsIntel = 0x00;
static const UBYTE IsMotorola = 0x80;

static const UBYTE SPRITE_VERSION = 0x32;		/* Version 3.2 */

typedef _Packed struct {
	IBYTE flags;
	IBYTE headerSize;				// Size of this header
	ILONG dataSize;				// Size of data on disk (was UWORD in version 3.0)
	IWORD width;					// Width as stored on disk
	IWORD height;
	IWORD fullWidth;				// Width before squeezing
	IWORD fullHeight;
	IWORD xOffset;					// Amount squeezed from left
	IWORD yOffset;					// Amount squeezed from right
	IWORD anchorX;					// HotSpot
	IWORD anchorY;
	IBYTE shadowColor;
	IBYTE transparentColor;
	IWORD paletteIndex;			// Which palette belongs to this sprite?
} SpriteHeaderD;

_Packed struct PaletteHeaderD {
	IWORD nColors;
};


/*
 * Flags Usage
 */

enum FlagBits 
{
	RLE_B = 0,
	SQUEEZE_B = 1,
	SHADOW_B	= 2,
	NYBBLE_B	= 3,
	REMAP_B = 4,
	LZHUF_B = 5
};

};		// namespace SWG_Sprite

#endif /* SWGSPR_H */

