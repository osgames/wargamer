/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Simple Date and Time Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "datetime.hpp"
#include "myassert.hpp"
#include "misc.hpp"
#include "resstr.hpp"
#include <stdio.h>

namespace Greenius_System
{

Time::Ticks Greenius_System::Time::ticks() const
{
	Ticks min = d_hours * MinutesPerHour + d_minutes;
	Ticks seconds = min * SecondsPerMinute + d_seconds;
	return seconds * TicksPerSecond;
}

void Greenius_System::Time::set(Ticks ticks)
{
	Ticks secs = ticks / TicksPerSecond;
	Ticks mins = secs / SecondsPerMinute;
	// d_seconds = secs - mins * SecondsPerMinute;
	d_seconds = static_cast<Second>(secs % SecondsPerMinute);
	Ticks hours = mins / MinutesPerHour;
	// d_minutes = mins - hours * MinutesPerHour;
	d_minutes = static_cast<Minute>(mins % MinutesPerHour);
	Ticks days = hours / HoursPerDay;
	// d_hours = hours - days * HoursPerDay;
	d_hours = static_cast<Hour>(hours % HoursPerDay);
	if(days)
		throw OutOfRange();
}


Date::Date(MonthEnum month, Day day, Year year) :
    d_date(convertMonth(month), day, year)
{
}

zDate::month Date::convertMonth(MonthEnum m) const
{
    return static_cast<zDate::month>(m - January + zDate::jan);
}

Date::MonthEnum Date::convertMonth(zDate::month m) const
{
    return static_cast<MonthEnum>(m - zDate::jan + January);
}

/*
 * Get string containing name of month
 */

const char* Date::getMonthName(Month m, Boolean abrev)
{
#if 0
	static const char* abrevMonthNames[] = {
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"May",
		"Jun",
		"Jul",
		"Aug",
		"Sep",
		"Oct",
		"Nov",
		"Dec"
	};

	static const char* fullMonthNames[] = {
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	};
#endif

	ASSERT(m >= 0);
	ASSERT(m < 12);

   if(abrev)
      return InGameText::get(m + IDS_MonthNameAbrevs);
   else
      return InGameText::get(m + IDS_MonthNames);

// 	if(abrev)
// 		return abrevMonthNames[m];
// 	else
// 		return fullMonthNames[m];
}

const char* Date::toAscii(char* buffer)
{
	sprintf(buffer, "%d%s %s %d",
		(int) day(),
		getNths(day()),
		getMonthName(month(), True),
		(int) year());

	return buffer;
}


};  // namespace Greenius_System

