/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "CRC.hpp"

/*

  Filename : CRC.cpp

  Description : Some CRC generating utilities

*/




unsigned short
makeCRC(unsigned char * data, int length) {

	unsigned short CRC = 0;

	while (length--) {
		
		CRC = CRC ^ (*data++ << 8);

		for (int i = 0; i++ < 8;) {
			
			if(CRC & 0x8000) CRC = (CRC << 1) ^ 0x1021;
			else CRC <<= 1;
		}
	}

	return CRC;
}


unsigned short
addBufferCRC(unsigned short CRC, unsigned char * data, int length) {

	while (length--) {
		
		CRC = CRC ^ (*data++ << 8);

		for (int i = 0; i++ < 8;) {
			
			if(CRC & 0x8000) CRC = (CRC << 1) ^ 0x1021;
			else CRC <<= 1;
		}
	}

	return CRC;
}



unsigned short
addByteCRC(unsigned short CRC, unsigned char b) {

	CRC = CRC ^ (b << 8);

	for(int i=0; i++ < 8;) {

		if(CRC & 0x8000) CRC = (CRC << 1) ^ 0x1021;
		else CRC <<= 1;
	}

	return CRC;
}



unsigned short
addShortCRC(unsigned short CRC, unsigned short s) {

	int length = 2;
	char * data = (char *) &s;

	while(length--) {

		CRC = CRC ^ (*data++ << 8);

		for(int i=0; i++ < 8;) {

			if(CRC & 0x8000) CRC = (CRC << 1) ^ 0x1021;
			else CRC <<= 1;
		}
	}

	return CRC;
}


unsigned short
addIntCRC(unsigned short CRC, unsigned int i) {

	int length = 4;
	char * data = (char *) &i;

	while(length--) {

		CRC = CRC ^ (*data++ << 8);

		for(int i=0; i++ < 8;) {

			if(CRC & 0x8000) CRC = (CRC << 1) ^ 0x1021;
			else CRC <<= 1;
		}
	}

	return CRC;
}

















