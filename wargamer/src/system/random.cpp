/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Random Number Generators
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "random.hpp"
#include "myassert.hpp"
#include <stdlib.h>
#include <string.h>			// for memcpy
#include <time.h>

#if 0		// Old version

RandomNumber::RandomNumber(ULONG start)
{
	seed(start);
}

RandomNumber::RandomNumber()
{
	seed(rand() + (rand() << 8) + (rand() << 16) + (rand() << 24));
}

void RandomNumber::seed(ULONG start)
{

	static UBYTE startValue[6] = {
		'G', 'R', 'E', 'E', 'N', 'Y'
	};

	memcpy(value.bytes, startValue, sizeof(value.bytes));

	ULONG rev = 0;
	ULONG v = start;
	int i = 32;
	while(i--)
	{
		rev <<= 1;

		rev |= (v & 1);

		v >>= 1;
	}

	value.lw.lHi ^= start ^ rev;
	value.wl.lLo ^= start ^ rev;

	shuffle();
	shuffle();
}

inline void RandomNumber::shuffle()
{
	value.lw.lHi = value.wl.lLo - value.lw.lHi;
	value.lw.wLo ^= 0xe713;
}

ULONG RandomNumber::getL()
{
	shuffle();
	return value.lw.lHi;
}

UWORD RandomNumber::getW()
{
	shuffle();
	return value.words[0];
}

UBYTE RandomNumber::getB()
{
	shuffle();
	return value.bytes[0];
}

#ifdef __WATCOM_CPLUSPLUS__
ULONG getRange(ULONG value, ULONG range);
#pragma aux getRange = \
	"mul edx"			\
	"mov eax,edx"		\
	parm [eax] [edx]	\
	modify [edx]
#elif defined(_MSC_VER)
ULONG getRange(ULONG value, ULONG range)
{
   __asm
   {
      mov eax, value
      mul range
      mov eax,edx
   }
}
#else
ULONG getRange(ULONG value, ULONG range)
{
	ulonglong value = range * getL()
	return value >> 32;

}
#endif


int RandomNumber::get(int range)
{
	ASSERT(range >= 0);

	// return range ? getL() % range : 0;

	// Pseudo code is:
	//		ulonglong value = range * getL()
	//		return value >> 32;
	// We do it in assembler to be more efficient

	int value =  getRange(getL(), range);

	ASSERT(value < range);
	ASSERT(value >= 0);

	return value;
}

int RandomNumber::operator () (int from, int to)
{
	int range = to + 1 - from;

	if(range < 0)
		return get(-range) + to;
	else if(range > 0)
		return get(range) + from;
	else
		return from;
}

/*
 * Simple function for backwards compatibility
 */

int random(int n1, int n2)
{
	ASSERT(n2 > n1);

	return (rand() % (n2 - n1)) + n1;
}

#else

/*
 * McGill Super-Duper Randm Number Generator
 *
 * Seems to combine a conventional value = (K * value) MOD N
 * where K = 69069, and N = 2^32
 *
 * with a bit shifting xor method
 */


RandomNumber::RandomNumber(ULONG start)
{
	seed(start);
}

RandomNumber::RandomNumber()
{
/*
#ifdef DEBUG
	seed(0x389a801f);		// Always us same seed if in debug mode
#else
*/
	time_t theTime;
	time(&theTime);
	seed(theTime);
//#endif
}

void RandomNumber::seed(ULONG start)
{
	if(start == 0)
		start = 1;

	d_shiftValue = start ^ (start << 15) ^ (start >> 17) | 1;
	d_mulValue = d_shiftValue;
}

inline void RandomNumber::shuffle()
{
	const ULONG M = 69069;

	ULONG r = (d_shiftValue >> 15) ^ d_shiftValue;
	d_shiftValue = (r << 17) ^ r;
	d_mulValue *= M;
}

ULONG RandomNumber::getL()
{
	shuffle();
	return d_shiftValue ^ d_mulValue;
}


#ifdef __WATCOM_CPLUSPLUS__
ULONG getRange(ULONG value, ULONG range);
#pragma aux getRange = \
	"mul edx"			\
	"mov eax,edx"		\
	parm [eax] [edx]	\
	modify [edx]
#elif defined(_MSC_VER)
ULONG __cdecl getRange(ULONG value, ULONG range)
{
   ULONG result;
   __asm
   {
      mov eax, [value]
      mul [range]
      mov [result],edx
   }
   return result;
}
#else
ULONG getRange(ULONG value, ULONG range)
{
	ulonglong value = range * getL()
	return value >> 32;

}
#endif

/*
 * Get value between 0..(range-1)
 */


int RandomNumber::get(int range)
{
	ASSERT(range > 0);

	if(range <= 1)
		return 0;

	// return range ? getL() % range : 0;

	// Pseudo code is:
	//		ulonglong value = range * getL()
	//		return value >> 32;
	// We do it in assembler to be more efficient

	int value =  getRange(getL(), range);

	ASSERT(value < range);
	ASSERT(value >= 0);

	return value;
}

/*
 * returns number between from..to inclusive
 * e.g. 0,100 may return 100.
 */

int RandomNumber::operator () (int from, int to)
{
	// int range = to + 1 - from;
	int range = to - from;

	if(range < 0)
		return get(1-range) + to;
	else if(range > 0)
		return get(1+range) + from;
	else
		return from;
}

/*
 * return number biassed towards 0, with min/max values +/- (range-1)
 */

int RandomNumber::gauss(int range)
{
	return get(range) - get(range);
}

/*
 * Simple function for backwards compatibility
 *
 * Returns number between n1..n2-1
 */

int random(int n1, int n2)
{
	ASSERT(n2 > n1);

	static RandomNumber r;

	int value = r(n1, n2-1);

	ASSERT(value >= n1);
	ASSERT(value < n2);

	return value;
}


#endif
