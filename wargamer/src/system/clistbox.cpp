/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#if 0		// Don't use any more
/*
 * Custom List Box
 *
 * Based on old ItemWind
 * but restarted because it was getting too compliated and awkward to use
 */


#include "clistbox.hpp"
#include "generic.hpp"
#include "app.hpp"
#include "dib.hpp"
#include "winctrl.hpp"
#include "cscroll.hpp"
#include "fonts.hpp"
#include "palette.hpp"
#include "scrnbase.hpp"
#include "wmisc.hpp"
#include "misc.hpp"
#include "dib_util.hpp"

/*
 * Default initialiser
 */

CustomListBoxData::CustomListBoxData() :
		d_hParent(0),
		d_style(WS_POPUP | WS_CLIPSIBLINGS), // default style
		d_id(0),
		d_hFont(0),
		d_itemCY(10),
		d_scrollCX(10),
		d_maxItemsShowing(10),
		d_flags(0),
		d_dbX(0),
      d_dbY(0),
		d_windowFillDib(0),
		d_fillDib(0),
		d_scrollBtns(0),
		d_scrollFillDib(0),
		d_scrollThumbDib(0),
		d_colorize(PALETTERGB(128, 128, 128)) 
{
}

/*
 * Static members
 */

static int CustomListBox::s_instanceCount = 0;
static DrawDIBDC* CustomListBox::s_windowDIB = 0;
static DrawDIBDC* CustomListBox::s_itemDIB = 0;


/*===============================================================
 * Public Functions
 */

CustomListBox::CustomListBox(const CustomListBoxData& data, const PixelRect& where) :
	d_hList(NULL),
	d_hScroll(NULL),
	d_currentValue(0),
	d_topItem(0),
	d_data(data)
{
	ASSERT(data.d_hParent != 0);
	++s_instanceCount;

	HWND hWnd = createWindow(
		0,
		GenericNoBackClass::className(), 
		NULL,
		d_data.d_style,
		where.left(), where.top(), where.width(), where.height(),
		d_data.d_hParent,
		NULL, 
		APP::instance()
	);

	makeDIB();
}

CustomListBox::~CustomListBox()
{
	ASSERT(s_instanceCount > 0);

	if(--s_instanceCount == 0)
	{
		if(s_windowDIB)
		{
			delete s_windowDIB;
			s_windowDIB = 0;
		}
		if(s_itemDIB)
		{
			delete s_itemDIB;
			s_itemDIB = 0;
		}
	}
}

int CustomListBox::currentIndex()
{
	ListBox lb(d_hList);
	if(lb.getNItems() > 0)
		return lb.getIndex();
	else
		return -1;
}

void CustomListBox::setCurrentIndex(int index)
{
	ListBox lb(d_hList);
	if(lb.getNItems() > 0)
	{
		lb.set(index);
		d_currentValue = lb.getValue();
		showScroll();
	}
}

void CustomListBox::move(const PixelRect& r)
{
	MoveWindow(getHWND(), r.left(), r.top(), r.width(), r.height(), TRUE);
}

/*===============================================================
 * Protected Functions
 */

void CustomListBox::resetItems()
{
	ListBox lb(d_hList);
	lb.reset();
}

void CustomListBox::addItem(const char* name, int id)
{
	ListBox lb(d_hList);
	lb.add(name, id);
}

int CustomListBox::getNItems()
{
	ListBox lb(d_hList);
	return lb.getNItems();
}


DrawDIBDC* CustomListBox::allocateItemDib(LONG w, LONG h)
{
	if(!s_itemDIB ||
		(s_itemDIB->getWidth() < w) ||
		(s_itemDIB->getHeight() < h) )
	{
		if(s_itemDIB)
		{
			ULONG oldCX = s_itemDIB->getWidth();
			ULONG oldCY = s_itemDIB->getHeight();

			s_itemDIB->resize( maximum(oldCX,w), maximum(oldCY,h) );
		}
		else
			s_itemDIB = new DrawDIBDC(w, h);

		ASSERT(s_itemDIB);

		s_itemDIB->setBkMode(TRANSPARENT);
	}

	if(d_data.d_hFont)
		s_itemDIB->setFont(d_data.d_hFont);
	else
	{
		int fontHeight = d_data.d_itemCY - 3;

		LogFont lf;
		lf.height(fontHeight);
		lf.weight(FW_MEDIUM);

		Font font;
		font.set(lf);

   	s_itemDIB->setFont(font);
	}

	return s_itemDIB;
}


/*===============================================================
 * Private Functions
 */


LRESULT CustomListBox::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	LRESULT result;

	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,		onCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, 		onCommand);
		HANDLE_MSG(hWnd, WM_DRAWITEM, 	onDrawItem);
		HANDLE_MSG(hWnd, WM_MEASUREITEM, onMeasureItem);
		HANDLE_MSG(hWnd, WM_ERASEBKGND, 	onEraseBk);
		HANDLE_MSG(hWnd, WM_VSCROLL, 		onVScroll);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
		HANDLE_MSG(hWnd, WM_SIZE, 			onSize);

		default:
		return defProc(hWnd, msg, wParam, lParam);
	}
}

BOOL CustomListBox::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
	/*
	 * Create User-drawn list box
	 */

	d_hList = CreateWindow( "LISTBOX", NULL,
		  WS_CHILD | WS_VISIBLE | LBS_OWNERDRAWFIXED | LBS_HASSTRINGS | LBS_NOTIFY | LBS_NOINTEGRALHEIGHT,
		  0, 0, 0, 0,
		  hWnd, (HMENU)ID_ListBox, APP::instance(), NULL);

	ASSERT(d_hList != 0);

#if 0
	if(d_data.d_flags & ItemWindowData::DragList)
	{
		BOOL result = MakeDragList(h);
		ASSERT(result);
	}
#endif

	if(d_data.d_flags & CustomListBoxData::HasScroll)
	{
		/*
		 * Create custom scroll bar
		 */

		CustomScrollBar::initCustomScrollBar(APP::instance());

		d_hScroll = csb_create(0, WS_CHILD | SBS_VERT, 0, 0, 15, 50, hWnd, APP::instance());
		ASSERT(d_hScroll != 0);

		/*
		 * Initialize scroll bar
		 */

		csb_setButtonIcons(d_hScroll, d_data.d_scrollBtns);
		csb_setThumbBk(d_hScroll, d_data.d_scrollThumbDib);
		csb_setScrollBk(d_hScroll, d_data.d_scrollFillDib);
		csb_setBorder(d_hScroll, &d_data.d_bColors);
		csb_setPage(d_hScroll, d_data.d_maxItemsShowing);

	}

	if(d_data.d_flags & CustomListBoxData::TextHeader)
	{
		HWND hwnd = CreateWindow("Static", NULL,
			WS_CHILD | WS_VISIBLE | SS_OWNERDRAW,
			0, 0, 0, 0,
			hWnd, (HMENU)ID_Header, APP::instance(), NULL);

		ASSERT(hwnd);
	}

	return TRUE;
}

void CustomListBox::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch(id)
	{
		case ID_ListBox:
			if(codeNotify == LBN_SELCHANGE)
			{
				ListBox lb(hwndCtl);
				d_currentValue = lb.getValue();

				SendMessage(d_data.d_hParent, WM_COMMAND, MAKEWPARAM(d_data.d_id, 0), (LPARAM)hWnd);

				if(d_data.d_flags & CustomListBoxData::NoAutoHide)
					; // do nothing
				else
					hide();
			}
			break;
	}
}

void CustomListBox::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
	HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
	RealizePalette(lpDrawItem->hDC);

	switch(lpDrawItem->CtlID)
	{
		case ID_ListBox:

			if(d_data.d_flags & CustomListBoxData::OwnerDraw)
				drawListItem(lpDrawItem);
			else
				blitText(lpDrawItem);
			break;

		case ID_Header:
			drawHeader(lpDrawItem);
			break;
	}

	SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
}

BOOL CustomListBox::onEraseBk(HWND hwnd, HDC hdc)
{
  if(s_windowDIB)
  {
		HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
		RealizePalette(hdc);

		RECT r;
		GetClientRect(hwnd, &r);

		const int cx = r.right - r.left;
		const int cy = r.bottom - r.top;

		BitBlt(hdc, 0, 0, cx, cy, s_windowDIB->getDC(), 0, 0, SRCCOPY);

		SelectPalette(hdc, oldPal, FALSE);
	}

	return TRUE;
}

void CustomListBox::onDestroy(HWND hWnd)
{
	// TODO
}

void CustomListBox::onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
	// TODO
}

void CustomListBox::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  /*
	* Special case returns
	*
	*/

  if((d_data.d_flags & CustomListBoxData::NoAutoHide))
	 return;

  /*
	* If mouse is over our list window then release capture and pass it on
	* otherwise, release capture and close window
	*/


  // get handle to list box
  // HWND hList = GetDlgItem(hwnd, ItemListBox);
  // ASSERT(hList);

  // get its screen rect
  RECT r;
  GetWindowRect(d_hList, &r);

  // convert to client rect
  POINT p;
  p.x = r.left;
  p.y = r.top;

  Boolean result = ScreenToClient(hwnd, &p);
  ASSERT(result);

  SetRect(&r, p.x, p.y, p.x+(r.right-r.left), p.y+(r.bottom-r.top));

  // see if mouse is over list
  p.x = x;
  p.y = y;

  if(PtInRect(&r, p))
  {
	 // if so, pass it on to the list box to handle

	 // first, convert mouse to list coordinates
	 result = ClientToScreen(hwnd, &p);
	 ASSERT(result);

	 result = ScreenToClient(d_hList, &p);
	 ASSERT(result);

	 SendMessage(d_hList, WM_LBUTTONDOWN, (WPARAM)keyFlags, MAKELPARAM(p.x, p.y));
  }

  // otherwise, see if we are over the scrollbar
  else if(d_data.d_flags & CustomListBoxData::HasScroll)
  {
	 ASSERT(d_hScroll);

	 // get its screen rect
	 GetWindowRect(d_hScroll, &r);

	 // convert to client rect
	 p.x = r.left;
	 p.y = r.top;

	 result = ScreenToClient(hwnd, &p);
	 ASSERT(result);

	 SetRect(&r, p.x, p.y, p.x+(r.right-r.left), p.y+(r.bottom-r.top));

	 // see if mouse is over scroll
	 p.x = x;
	 p.y = y;

	 if(PtInRect(&r, p))
	 {
		// first, convert mouse to scrollbar coordinates
		result = ClientToScreen(hwnd, &p);
		ASSERT(result);

		result = ScreenToClient(d_hScroll, &p);
		ASSERT(result);

		SendMessage(d_hScroll, WM_LBUTTONDOWN, (WPARAM)keyFlags, MAKELPARAM(p.x, p.y));
	 }
	 else
		hide();
  }
  else
	 hide();

}

void CustomListBox::onSize(HWND hwnd, UINT state, int cx, int cy)
{
	// TODO
}

void CustomListBox::onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem)
{
  lpMeasureItem->itemHeight = d_data.d_itemCY;
}

void CustomListBox::showScroll()
{
	// TODO
}

void CustomListBox::hide()
{
  if(!(d_data.d_flags & CustomListBoxData::NoAutoHide))
	 ReleaseCapture();

  // d_showing = False;
  ShowWindow(getHWND(), SW_HIDE);
}


void CustomListBox::allocateWindowDib(LONG w, LONG h)
{
	if(!s_windowDIB ||
		(s_windowDIB->getWidth() < w) ||
		(s_windowDIB->getHeight() < h) )
	{
		if(s_windowDIB)
		{
			ULONG oldCX = s_windowDIB->getWidth();
			ULONG oldCY = s_windowDIB->getHeight();

			s_windowDIB->resize( maximum(oldCX,w), maximum(oldCY,h) );
		}
		else
			s_windowDIB = new DrawDIBDC(w, h);

		ASSERT(s_windowDIB);

		s_windowDIB->setBkMode(TRANSPARENT);
	}

  ASSERT(s_windowDIB);
}


void CustomListBox::blitText(const DRAWITEMSTRUCT* lpDrawItem)
{
  // get text
  char text[200];
  SendMessage(lpDrawItem->hwndItem, LB_GETTEXT, lpDrawItem->itemID, reinterpret_cast<LPARAM>(text));

  blitText(lpDrawItem, text);
}

void CustomListBox::blitText(const DRAWITEMSTRUCT* lpDrawItem, const char* text)
{
  ASSERT(text != 0);
  const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
  const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

  allocateItemDib(itemCX, itemCY);

  ASSERT(s_itemDIB);
  ASSERT(d_data.d_fillDib);

  // fill backfround
  if( !(lpDrawItem->CtlType == ODT_COMBOBOX) )
  {
	 s_itemDIB->rect(0, 0, itemCX, itemCY, d_data.d_fillDib);
	 if( !(lpDrawItem->itemState & ODS_SELECTED) )
		s_itemDIB->darkenRectangle(0, 0, itemCX, itemCY, 20);
  }

//  ListBox lb(lpDrawItem->hwndItem);

  /*
	* Put text
	*/

  TEXTMETRIC tm;
  GetTextMetrics(s_itemDIB->getDC(), &tm);

  LONG x = (ScreenBase::dbX() / ScreenBase::baseX());
  LONG y = ((itemCY - tm.tmHeight) / 2);
  wTextOut(s_itemDIB->getDC(), x, y, itemCX - (x + 2), text);

  BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
			s_itemDIB->getDC(), 0, 0, SRCCOPY);
}


/*
 * Create and fill in the background dib
 */

void CustomListBox::makeDIB()
{
  ListBox lb(getHWND(), ID_ListBox);

#if 0
  /*
	* Some useful values
	*/

  const int entries = lb.getNItems();

  // if no entries, return
  if(entries == 0)
  {
	 ShowWindow(getHWND(), SW_HIDE);
	 return;
  }

  /*
	* Set mouse capture to this window
	*/

  if(!(d_data.d_flags & CustomListBoxData::NoAutoHide))
  {
	 SetForegroundWindow(getHWND());
	 SetCapture(getHWND());
  }

  d_showing = True;

  ASSERT(entries > 0);
#endif

  LONG offsetX = (d_data.d_flags & CustomListBoxData::NoBorder) ? 1 :
	 (4 * ScreenBase::dbX()) / ScreenBase::baseX();
  LONG offsetY = offsetX;

  HWND hwndList = lb.getHWND();	// GetDlgItem(getHWND(), ItemListBox);
  ASSERT(hwndList);

  RECT r;
  GetClientRect(hwndList, &r);

  const int listCX = r.right - r.left;
  const int listCY = r.bottom - r.top;

  int shadowWidth = 0;
  int shadowHeight = 0;

  int headerCY = 0;
  int headerOffset = 0;
  r.right += offsetX * 2;
  r.bottom += offsetY * 2;

#if 0
  // adjust rect for header

  if(d_data.d_flags & CustomListBoxData::TextHeader)
  {
	 headerCY = d_data.d_itemCY;;
	 headerOffset = offsetY + headerCY;

	 r.bottom += headerOffset;

	 HWND h = GetDlgItem(getHWND(), ListHeader);
	 ASSERT(h);

	 // set position of header static constrol
	 SetWindowPos(h, HWND_TOP, offsetX, offsetY, listCX, headerCY, 0);
  }
#endif

#if 0
  // adjust rect for scroll
  if(d_data.d_flags & CustomListBoxData::HasScroll)
  {
	 if(entries > d_data.d_maxItemsShowing)
	 {
		r.right += d_data.d_scrollCX;
		showScroll();
	 }
	 else
	 {
		ShowWindow(d_hScroll, SW_HIDE);
	 }
  }
#endif

  if(d_data.d_flags & CustomListBoxData::ShadowBackground)
  {
	 shadowWidth = (5 * ScreenBase::dbX()) / ScreenBase::baseX();
	 shadowHeight = shadowWidth;

	 r.right += shadowWidth;
	 r.bottom += shadowHeight;
  }

  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;


  allocateWindowDib(cx, cy);
  ASSERT(s_windowDIB);

  DIB_Utility::InsertData id;
  if(!(d_data.d_flags & CustomListBoxData::NoBorder) )
  {
		ASSERT(d_data.d_windowFillDib);
#if 0
	 if(d_data.d_flags & CustomListBoxData::ShadowBackground)
	 {
		transferBits(p, cx, cy, shadowWidth, shadowHeight);

		s_windowDIB->rect(0, 0, cx - shadowWidth, cy - shadowHeight, d_data.d_windowFillDib);

		/*
		 *  draw pop-up insert
		 */

		id.d_cRef = d_data.d_colorize;
		id.d_x = 0;
		id.d_y = 0;
		id.d_cx = cx;
		id.d_cy = cy;
		id.d_shadowCX = shadowWidth;
		id.d_shadowCY = shadowHeight;
		id.d_mode = DIB_Utility::InsertData::ShadowOnly;

		DIB_Utility::drawInsert(s_windowDIB, id);

	 }
	 else
#endif
		s_windowDIB->rect(0, 0, cx, cy, d_data.d_windowFillDib);

	 /*
	  * Draw border around window
	  */

	 RECT r;
	 SetRect(&r, 0, 0, cx - shadowWidth, cy - shadowHeight);

	 CustomBorderWindow cw(d_data.d_bColors);
	 cw.drawThinBorder(s_windowDIB->getDC(), r);
  }

  /*
	* Draw Border around list box
	*/

  ColourIndex c1 = s_windowDIB->getColour(Palette::brightColour(id.d_cRef, 32, 255));
  ColourIndex c2 = s_windowDIB->getColour(Palette::brightColour(id.d_cRef, 0, 128));

  {
	 s_windowDIB->frame(offsetX - 1, (offsetY + headerOffset) - 1, listCX + 2, listCY + 2, c1, c2);
  }

  /*
	* Draw Border around header
	*/

  if(d_data.d_flags & CustomListBoxData::TextHeader)
  {
	 s_windowDIB->frame(offsetX - 1, offsetY - 1, listCX + 2, headerCY + 2, c1, c2);
  }

  /*
	* Set position of list
	*/

  SetWindowPos(hwndList, HWND_TOP, offsetX, offsetY + headerOffset, 0, 0, SWP_NOSIZE);

#if 0
  /*
	* Set position of window
	*/

  // clip it

  POINT cp = p;
  clipPoint(r, cp);	// static_cast<POINT&>(p));
  SetWindowPos(getHWND(), HWND_TOP, cp.x, cp.y, cx, cy, SWP_SHOWWINDOW);
#endif
	ShowWindow(getHWND(), SW_SHOW);
}


#if 0
#include "clistbox.hpp"
//#include "scenario.hpp"
#include "dib.hpp"
//#include "userint.hpp"
#include "cscroll.hpp"
#include "app.hpp"
#include "winctrl.hpp"
#include "scn_res.h"
#include "palette.hpp"
#include "fonts.hpp"
#include "wmisc.hpp"
#include "imglib.hpp"
//#include "scn_img.hpp"
#include "dib_util.hpp"
#include "generic.hpp"
#include "scrnbase.hpp"
#include "misc.hpp"	// STL min/max/swap functions

DrawDIBDC* ItemWindow::s_windowDib = 0;
DrawDIBDC* ItemWindow::s_itemDib = 0;
UWORD ItemWindow::s_instanceCount = 0;
UINT ItemWindow::s_dragMsgID = 0;

ItemWindow::ItemWindow(const ItemWindowData& data, const SIZE& s) :
  d_hScroll(0),
  d_data(data),
  d_showing(False),
  d_currentValue(0),
  d_topItem(0)
{
  ASSERT(data.d_hParent != 0);

  s_instanceCount++;
  s_dragMsgID = RegisterWindowMessage(DRAGLISTMSGSTRING);
  ASSERT(s_dragMsgID != 0);
//  d_data = data;         // structure copy

  HWND hWnd = createWindow(
		0,
		GenericNoBackClass::className(), NULL,
		d_data.d_style,
		0, 0, s.cx, s.cy,
		d_data.d_hParent, NULL, APP::instance()
  );
  ASSERT(hWnd != NULL);
}

ItemWindow::~ItemWindow()
{
  ASSERT((s_instanceCount - 1) >= 0);

  if(--s_instanceCount == 0)
  {
	 if(s_windowDib)
	 {
		delete s_windowDib;
		s_windowDib = 0;
	 }
	 if(s_itemDib)
	 {
		delete s_itemDib;
		s_itemDib = 0;
	 }
  }
}

LRESULT ItemWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  LRESULT result;

  if(msg == s_dragMsgID)
  {
	 return SendMessage(d_data.d_hParent, msg, wParam, lParam);
//	 return True;
  }

  switch(msg)
  {
	 HANDLE_MSG(hWnd, WM_CREATE,	onCreate);
	 HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
	 HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
	 HANDLE_MSG(hWnd, WM_MEASUREITEM, onMeasureItem);
	 HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
	 HANDLE_MSG(hWnd, WM_VSCROLL, onVScroll);
	 HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);

	 default:
		return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL ItemWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#if 0
  if(d_data.d_dbX == 0)
  {
	 // get base font size (in dialog units)
	 const LONG dbUnits = GetDialogBaseUnits();
	 d_data.d_dbX = LOWORD(dbUnits);
	 d_data.d_dbY = HIWORD(dbUnits);
  }
#endif
  /*
	* Create User-drawn list box
	*/

  {
	  HWND h = CreateWindow( "LISTBOX", NULL,
		  WS_CHILD | WS_VISIBLE | LBS_OWNERDRAWFIXED | LBS_HASSTRINGS | LBS_NOTIFY | LBS_NOINTEGRALHEIGHT,
		  0, 0, 0, 0,
		  hWnd, (HMENU)ItemListBox, APP::instance(), NULL);

	  ASSERT(h != 0);

	  if(d_data.d_flags & ItemWindowData::DragList)
	  {
		 BOOL result = MakeDragList(h);
		 ASSERT(result);
	  }
  }

  if(d_data.d_flags & ItemWindowData::HasScroll)
  {
	 /*
	  * Create custom scroll bar
	  */

	 CustomScrollBar::initCustomScrollBar(APP::instance());

	 d_hScroll = csb_create(0, WS_CHILD | SBS_VERT, 0, 0, 15, 50,
		hWnd, APP::instance());

	 ASSERT(d_hScroll != 0);


	 /*
	  * Initialize scroll bar
	  */

	 csb_setButtonIcons(d_hScroll, d_data.d_scrollBtns);
	 csb_setThumbBk(d_hScroll, d_data.d_scrollThumbDib);
	 csb_setScrollBk(d_hScroll, d_data.d_scrollFillDib);
	 csb_setBorder(d_hScroll, &d_data.d_bColors);
	 csb_setPage(d_hScroll, d_data.d_maxItemsShowing);

#if 0
	 csb_setButtonIcons(d_hScroll, ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons));
	 csb_setThumbBk(d_hScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground));
	 csb_setScrollBk(d_hScroll, scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground));
	 csb_setBorder(d_hScroll, &scenario->getBorderColors());
	 csb_setPage(d_hScroll, d_data.d_maxItemsShowing);
#endif
  }

  if(d_data.d_flags & ItemWindowData::TextHeader)
  {
	 HWND hwnd = CreateWindow("Static", NULL,
		WS_CHILD | WS_VISIBLE | SS_OWNERDRAW,
		0, 0, 0, 0,
		hWnd, (HMENU)ListHeader, APP::instance(), NULL);

	 ASSERT(hwnd);
  }

  return TRUE;
}

BOOL ItemWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  RECT r;
  GetClientRect(hwnd, &r);

  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;

  if(s_windowDib)
	 BitBlt(hdc, 0, 0, cx, cy, s_windowDib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);

  return TRUE;
}

void ItemWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
	 case ItemListBox:
		if(codeNotify == LBN_SELCHANGE)
		{
		  ListBox lb(hwndCtl);
		  d_currentValue = lb.getValue();

		  SendMessage(d_data.d_hParent, WM_COMMAND, MAKEWPARAM(d_data.d_id, 0), (LPARAM)hwnd);

		  if(d_data.d_flags & ItemWindowData::NoAutoHide)
			 ; // do nothing
		  else
			 hide();
		}
		break;
  }
}

void ItemWindow::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  /*
	* Special case returns
	*
	*/

  if((d_data.d_flags & ItemWindowData::NoAutoHide))
	 return;

  /*
	* If mouse is over our list window then release capture and pass it on
	* otherwise, release capture and close window
	*/


  // get handle to list box
  HWND hList = GetDlgItem(hwnd, ItemListBox);
  ASSERT(hList);

  // get its screen rect
  RECT r;
  GetWindowRect(hList, &r);

  // convert to client rect
  POINT p;
  p.x = r.left;
  p.y = r.top;

  Boolean result = ScreenToClient(hwnd, &p);
  ASSERT(result);

  SetRect(&r, p.x, p.y, p.x+(r.right-r.left), p.y+(r.bottom-r.top));

  // see if mouse is over list
  p.x = x;
  p.y = y;

  if(PtInRect(&r, p))
  {
	 // if so, pass it on to the list box to handle

	 // first, convert mouse to list coordinates
	 result = ClientToScreen(hwnd, &p);
	 ASSERT(result);

	 result = ScreenToClient(hList, &p);
	 ASSERT(result);

	 SendMessage(hList, WM_LBUTTONDOWN, (WPARAM)keyFlags, MAKELPARAM(p.x, p.y));
  }

  // otherwise, see if we are over the scrollbar
  else if(d_data.d_flags & ItemWindowData::HasScroll)
  {
	 ASSERT(d_hScroll);

	 // get its screen rect
	 GetWindowRect(d_hScroll, &r);

	 // convert to client rect
	 p.x = r.left;
	 p.y = r.top;

	 result = ScreenToClient(hwnd, &p);
	 ASSERT(result);

	 SetRect(&r, p.x, p.y, p.x+(r.right-r.left), p.y+(r.bottom-r.top));

	 // see if mouse is over scroll
	 p.x = x;
	 p.y = y;

	 if(PtInRect(&r, p))
	 {
		// first, convert mouse to scrollbar coordinates
		result = ClientToScreen(hwnd, &p);
		ASSERT(result);

		result = ScreenToClient(d_hScroll, &p);
		ASSERT(result);

		SendMessage(d_hScroll, WM_LBUTTONDOWN, (WPARAM)keyFlags, MAKELPARAM(p.x, p.y));
	 }
	 else
		hide();
  }
  else
	 hide();

}


void ItemWindow::hide()
{
  if(!(d_data.d_flags & ItemWindowData::NoAutoHide))
	 ReleaseCapture();

  d_showing = False;
  ShowWindow(getHWND(), SW_HIDE);
}

void ItemWindow::onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem)
{
  lpMeasureItem->itemHeight = d_data.d_itemCY;
}

void ItemWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  switch(lpDrawItem->CtlID)
  {
	 case ItemListBox:

		if(d_data.d_flags & ItemWindowData::OwnerDraw)
		  drawListItem(lpDrawItem);
		else
		  blitText(lpDrawItem);

		break;

	 case ListHeader:
		drawHeader(lpDrawItem);
		break;
  }

  SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
}


void ItemWindow::allocateItemDib(LONG cx, LONG cy)
{
  if(!s_itemDib ||
	  s_itemDib->getWidth() < cx || s_itemDib->getHeight() < cy)
  {
	 LONG oldCX = 0;
	 LONG oldCY = 0;

	 if(s_itemDib)
	 {
		oldCX = s_itemDib->getWidth();
		oldCY = s_itemDib->getHeight();

		delete s_itemDib;
		s_itemDib = 0;
	 }

	 s_itemDib = new DrawDIBDC(maximum(oldCX, cx), maximum(oldCY, cy));
	 ASSERT(s_itemDib);

	 s_itemDib->setBkMode(TRANSPARENT);
  }

  if(d_data.d_hFont)
	 s_itemDib->setFont(d_data.d_hFont);
  // default font
  else
  {
	 int fontHeight = d_data.d_itemCY - 3;

	 LogFont lf;
	 lf.height(fontHeight);
	 lf.weight(FW_MEDIUM);
//	 lf.face("Normal");

	 Font font;
	 font.set(lf);

    s_itemDib->setFont(font);
  }
}

void ItemWindow::allocateWindowDib(LONG cx, LONG cy)
{
  if(!s_windowDib ||
	  s_windowDib->getWidth() < cx || s_windowDib->getHeight() < cy)
  {
	 LONG oldCX = 0;
	 LONG oldCY = 0;

	 if(s_windowDib)
	 {
		oldCX = s_windowDib->getWidth();
		oldCY = s_windowDib->getHeight();

		delete s_windowDib;
		s_windowDib = 0;
	 }

	 s_windowDib = new DrawDIBDC(maximum(oldCX, cx), maximum(oldCY, cy));
  }

  ASSERT(s_windowDib);
}

void ItemWindow::setCurrentValue()
{
  ListBox lb(getHWND(), ItemListBox);

  if(lb.getNItems() > 0)
	 d_currentValue = lb.getValue();
}

// transfer bits from mainwindow's hdc to draw shadow
void ItemWindow::transferBits(const PixelPoint& p, int cx, int cy, int shadowCX, int shadowCY)
{
  POINT p2;
  p2.x = p.getX();
  p2.y = p.getY();

  ScreenToClient(APP::getMainHWND(), &p2);

  HDC hdc = GetDC(APP::getMainHWND());

  int x = cx - shadowCX;
  int y = 0;
  BitBlt(s_windowDib->getDC(), x, y, shadowCX, cy,
	  hdc, x + p2.x, y + p2.y, SRCCOPY);

  x = 0;
  y = cy - shadowCY;
  BitBlt(s_windowDib->getDC(), x, y, cx, shadowCY,
	  hdc, x + p2.x, y + p2.y, SRCCOPY);

  ReleaseDC(APP::getMainHWND(), hdc);
}

int ItemWindow::entries() 
{
  ListBox lb(getHWND(), ItemListBox);
  return lb.getNItems();
}

void ItemWindow::setCurrentValue(ULONG v)
{
  ListBox lb(getHWND(), ItemListBox);
  lb.setValue(v);

  setCurrentValue();
  showScroll();
}

void ItemWindow::show(const PixelPoint& p)
{
  ListBox lb(getHWND(), ItemListBox);

  /*
	* Some useful values
	*/

  const int entries = lb.getNItems();

  // if no entries, return
  if(entries == 0)
  {
	 ShowWindow(getHWND(), SW_HIDE);
	 return;
  }

  /*
	* Set mouse capture to this window
	*/

  if(!(d_data.d_flags & ItemWindowData::NoAutoHide))
  {
	 SetForegroundWindow(getHWND());
	 SetCapture(getHWND());
  }

  d_showing = True;

  ASSERT(entries > 0);

  const LONG offsetX = (d_data.d_flags & ItemWindowData::NoBorder) ? 1 :
	 (4 * ScreenBase::dbX()) / ScreenBase::baseX();
  const LONG offsetY = offsetX;

  HWND hwndList = GetDlgItem(getHWND(), ItemListBox);
  ASSERT(hwndList);

  RECT r;
  GetClientRect(hwndList, &r);

  const int listCX = r.right - r.left;
  const int listCY = r.bottom - r.top;

  int shadowWidth = 0;
  int shadowHeight = 0;

  int headerCY = 0;
  int headerOffset = 0;
  r.right += offsetX * 2;
  r.bottom += offsetY * 2;

  // adjust rect for header
  if(d_data.d_flags & ItemWindowData::TextHeader)
  {
	 headerCY = d_data.d_itemCY;;
	 headerOffset = offsetY + headerCY;

	 r.bottom += headerOffset;

	 HWND h = GetDlgItem(getHWND(), ListHeader);
	 ASSERT(h);

	 // set position of header static constrol
	 SetWindowPos(h, HWND_TOP, offsetX, offsetY, listCX, headerCY, 0);
  }

  // adjust rect for scroll
  if(d_data.d_flags & ItemWindowData::HasScroll)
  {
	 if(entries > d_data.d_maxItemsShowing)
	 {
		r.right += d_data.d_scrollCX;
		showScroll();
	 }
	 else
	 {
		ShowWindow(d_hScroll, SW_HIDE);
	 }
  }

  if(d_data.d_flags & ItemWindowData::ShadowBackground)
  {
	 shadowWidth = (5 * ScreenBase::dbX()) / ScreenBase::baseX();
	 shadowHeight = shadowWidth;

	 r.right += shadowWidth;
	 r.bottom += shadowHeight;
  }

  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;


  allocateWindowDib(cx, cy);
  ASSERT(s_windowDib);

  DIB_Utility::InsertData id;
  if(!(d_data.d_flags & ItemWindowData::NoBorder) )
  {
		ASSERT(d_data.d_windowFillDib);

	 if(d_data.d_flags & ItemWindowData::ShadowBackground)
	 {
		transferBits(p, cx, cy, shadowWidth, shadowHeight);

		s_windowDib->rect(0, 0, cx - shadowWidth, cy - shadowHeight, d_data.d_windowFillDib);

		/*
		 *  draw pop-up insert
		 */

		id.d_cRef = d_data.d_colorize;
		id.d_x = 0;
		id.d_y = 0;
		id.d_cx = cx;
		id.d_cy = cy;
		id.d_shadowCX = shadowWidth;
		id.d_shadowCY = shadowHeight;
		id.d_mode = DIB_Utility::InsertData::ShadowOnly;

		DIB_Utility::drawInsert(s_windowDib, id);

	 }
	 else
		s_windowDib->rect(0, 0, cx, cy, d_data.d_windowFillDib);

	 /*
	  * Draw border around window
	  */

	 RECT r;
	 SetRect(&r, 0, 0, cx - shadowWidth, cy - shadowHeight);

	 CustomBorderWindow cw(d_data.d_bColors);
	 cw.drawThinBorder(s_windowDib->getDC(), r);
  }

  /*
	* Draw Border around list box
	*/

  ColourIndex c1 = s_windowDib->getColour(Palette::brightColour(id.d_cRef, 32, 255));
  ColourIndex c2 = s_windowDib->getColour(Palette::brightColour(id.d_cRef, 0, 128));

  {
	 s_windowDib->frame(offsetX - 1, (offsetY + headerOffset) - 1, listCX + 2, listCY + 2, c1, c2);
  }

  /*
	* Draw Border around header
	*/

  if(d_data.d_flags & ItemWindowData::TextHeader)
  {
	 s_windowDib->frame(offsetX - 1, offsetY - 1, listCX + 2, headerCY + 2, c1, c2);
  }

  /*
	* Set position of list
	*/

  SetWindowPos(hwndList, HWND_TOP, offsetX, offsetY + headerOffset, 0, 0, SWP_NOSIZE);

  /*
	* Set position of window
	*/

  // clip it

  POINT cp = p;
  clipPoint(r, cp);	// static_cast<POINT&>(p));
  SetWindowPos(getHWND(), HWND_TOP, cp.x, cp.y, cx, cy, SWP_SHOWWINDOW);
}

void ItemWindow::clipPoint(const RECT& r, POINT& p)
{
  /*
	* Get some useful values
	*/

  ASSERT(APP::getMainHWND());

  RECT mRect;
  GetClientRect(APP::getMainHWND(), &mRect);

  const int screenCX = mRect.right - mRect.left;
  const int screenCY = mRect.bottom - mRect.top;

  // for left side
  p.x = maximum(0L, p.x);

  // for right side
  p.x = minimum(screenCX - (r.right-r.left), p.x);

  // for top
  p.y = maximum(0L, p.y);

  // for bottom
  p.y = minimum(screenCY - (r.bottom-r.top), p.y);
}


void ItemWindow::showScroll()
{
  if(d_data.d_flags & ItemWindowData::HasScroll)
  {
	 ListBox lb(getHWND(), ItemListBox);
	 int entries = lb.getNItems();

	 if(entries > d_data.d_maxItemsShowing)
	 {
		ASSERT(d_hScroll);

		HWND hwndList = GetDlgItem(getHWND(), ItemListBox);
		ASSERT(hwndList);

		RECT r;
		GetClientRect(hwndList, &r);

		int itemsShowing = minimum(d_data.d_maxItemsShowing, entries);
		const LONG offsetX = (d_data.d_flags & ItemWindowData::NoBorder) ? 1 :
		  (4 * ScreenBase::dbX()) / ScreenBase::baseX();
		const LONG offsetY = offsetX;

		d_topItem = maximum(0, minimum(lb.getIndex(), entries-d_data.d_maxItemsShowing));

		csb_setRange(d_hScroll, 0, entries);
		csb_setPosition(d_hScroll, d_topItem);

		lb.setTop(d_topItem);

		SetWindowPos(d_hScroll, HWND_TOP,
		  offsetX + (r.right - r.left), offsetY, d_data.d_scrollCX, r.bottom - r.top,
		  SWP_SHOWWINDOW);

	 }
  }
}

void ItemWindow::onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
	ASSERT(d_data.d_flags & ItemWindowData::HasScroll);

	int minP;
	int maxP;

	int page = csb_getPage(hwndCtl);

	csb_getRange(hwndCtl, &minP, &maxP);
	ASSERT(maxP > d_data.d_maxItemsShowing);

	switch(code)
	{
	  case SB_PAGEUP:
		 d_topItem = maximum(0, d_topItem-d_data.d_maxItemsShowing);
		 break;
	  case SB_PAGEDOWN:
		 d_topItem = minimum(d_topItem+d_data.d_maxItemsShowing, maxP-d_data.d_maxItemsShowing);
		 break;
	  case SB_LINEUP:
		 d_topItem = maximum(0, d_topItem-1);
		 break;
	  case SB_LINEDOWN:
		 d_topItem = minimum(d_topItem+1, maxP-d_data.d_maxItemsShowing);
		 break;
	  case SB_THUMBPOSITION:
		 d_topItem = pos;
		 break;

	  default:
		 return;
	}

	csb_setPosition(hwndCtl, d_topItem);

	ListBox lb(hwnd, ItemListBox);
	lb.setTop(d_topItem);
}

void ItemWindow::blitText(const DRAWITEMSTRUCT* lpDrawItem)
{
  // get text
  char text[200];
  SendMessage(lpDrawItem->hwndItem, LB_GETTEXT, lpDrawItem->itemID, reinterpret_cast<LPARAM>(text));

  blitText(lpDrawItem, text);
}

void ItemWindow::blitText(const DRAWITEMSTRUCT* lpDrawItem, const char* text)
{
  ASSERT(text != 0);
  const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
  const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

  allocateItemDib(itemCX, itemCY);

  ASSERT(s_itemDib);
  ASSERT(d_data.d_fillDib);

  // fill backfround
  if( !(lpDrawItem->CtlType == ODT_COMBOBOX) )
  {
	 s_itemDib->rect(0, 0, itemCX, itemCY, d_data.d_fillDib);
	 if( !(lpDrawItem->itemState & ODS_SELECTED) )
		itemDib()->darkenRectangle(0, 0, itemCX, itemCY, 20);
  }

//  ListBox lb(lpDrawItem->hwndItem);

  /*
	* Put text
	*/

  TEXTMETRIC tm;
  GetTextMetrics(s_itemDib->getDC(), &tm);

  LONG x = (ScreenBase::dbX() / ScreenBase::baseX());
  LONG y = ((itemCY - tm.tmHeight) / 2);
  wTextOut(itemDib()->getDC(), x, y, itemCX - (x + 2), text);

  BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
			s_itemDib->getDC(), 0, 0, SRCCOPY);
}

int ItemWindow::currentIndex()
{
  ListBox lb(getHWND(), ItemListBox);
  if(lb.getNItems() > 0)
  {
	 return lb.getIndex();
  }

  return -1;
}

void ItemWindow::setCurrentIndex(int index)
{
  ListBox lb(getHWND(), ItemListBox);
  if(lb.getNItems() > 0)
  {
	 lb.set(index);
	 setCurrentValue();
	 showScroll();
  }
}

Boolean ItemWindow::overItem(POINT& p)
{
  ListBox lb(getHWND(), ItemListBox);

  int itemID = LBItemFromPt(lb.getHWND(), p, FALSE);
  if(itemID != -1)
  {
	 if(itemID != lb.getIndex())
	   setCurrentIndex(itemID);
    return True;
  }
  else
	 return False;
}



void ItemWindow::move(const PixelRect& r)
{
	MoveWindow(getHWND(), r.left(), r.top(), r.width(), r.height(), TRUE);
}

#endif

#endif

