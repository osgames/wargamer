/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Configuration File Manager
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  2002/11/16 18:03:33  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1996/06/22 19:02:35  sgreen
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "config.hpp"
#include "misc.hpp"
#include "StringPtr.hpp"
#include <stdlib.h>
#include <stdio.h>

const int ConfigFile::MaxLineLength = 100;
const char ConfigFile::s_defLanguage[] = "default";
const char ConfigFile::s_sectionStartChar = '[';
const char ConfigFile::s_sectionEndChar = ']';

ConfigFile::ConfigFile(const char* name, const char* language) :
  WinFileAsciiReader(name),
  d_scanID(0),
  d_language(language),
  d_firstScan(false)
{
	d_fileName = name;
}

ConfigFile::~ConfigFile()
{
}

void ConfigFile::setLanguage(const char* lang)
{
   d_language = lang;
}


/*
 * Look at a buffer containing text such as:
 *   SomeString=SomeValue
 *
 * return the 1st character after the =
 * replace the = with 0
 *
 * Thus the caller can use:
 *		buffer : ID
 *    return : Value
 */

char* parseLine(char* buffer)
{
	char* s;

	/*
	 * Stop if there is a ; in the middle
	 */

	Boolean inString = False;
	for(s = buffer; *s; s++)
	{
		char c = *s;

		if(c == '\"')
			inString = !inString;
		else if(!inString)
		{
			if(c == ';')
			{
				*s = 0;
				break;
			}
		}
	}

	ASSERT(!inString);

	/*
	 * Remove trailing white space, line feeds, etc
	 */

	s = buffer + strlen(buffer) - 1;
	while( (s >= buffer) && isspace(*s))
		*s-- = 0;

	for(s = buffer; *s; s++)
	{
		if(isspace(*s) || (*s == '='))
		{
			*s++ = 0;

			// Skip to next non-space or =

			while((*s != '\0') && (isspace(*s) || (*s == '=')))
				s++;

			break;
		}
	}

	return s;
}

#ifdef WRITER
void writeSetting(const char* id, const char* setting)
{
	fputs(id, fp);
	if(setting && setting[0])
		fprintf(fp, "=%s", setting);
	fprintf(fp, "\n");
}
#endif

/*
 * Iterative way of scanning through for all instances of a particular keyword
 */

Boolean ConfigFile::startScan(const char* id)
{
   ASSERT(d_scanID == 0);
   d_scanID = id;
   d_firstScan = true;

	rewind();
	return isOK();
}

char* ConfigFile::nextScan()
{
   ASSERT(d_scanID != 0);

   if(d_firstScan)
   {
      d_firstScan = false;

      // look for language section

      bool result = findSection(d_language);
      if (result)
      {
         char* value = find(d_scanID);
         if(value)
            return value;
      }

      result = findSection(s_defLanguage);

      // ASSERT(result);

      // If no sections, then start at beginning of file.

      if(!result)
         rewind();

   }

   return find(d_scanID);
}

void ConfigFile::endScan()
{
   ASSERT(d_scanID != 0);
   d_scanID = 0;
	rewind();
}

bool ConfigFile::findSection(const char* lang)
{
   if(lang == 0)
      return false;

   rewind();

   char buf[MaxLineLength];
   sprintf(buf, "%c%s%c",
      s_sectionStartChar,
      lang,
      s_sectionEndChar);

   CString block = find(buf);
   return(block != 0);
}


char* ConfigFile::find(const char* id)
{
	while(isOK())
	{
		char* buffer = readLine();
		if(buffer != 0)
		{
			if(buffer[0] && (buffer[0] != ';'))
			{
				char* s = parseLine(buffer);

				if(stricmp(buffer, id) == 0)
					return copyString(s);
			}

         if ( (buffer[0] == s_sectionStartChar) &&
             (id[0] != s_sectionStartChar) )
         {
            break;
         }
		}
	}
   rewind();      // this clears the error
	return 0;
}


/*
 * Find a configuration variable
 *
 * Note that the return value muse be deleted after use.
 */

char* ConfigFile::get(const char* id)
{
#if 0
	rewind();

	while(isOK())
	{
		char* buffer = readLine();
		if(buffer != 0)
		{
			if(buffer[0] && (buffer[0] != ';'))
			{
				char* s = parseLine(buffer);

				if(stricmp(buffer, id) == 0)
					return copyString(s);
			}
		}
	}

	return 0;
#else
// 	startScan();
// 	char* value = find(id);
// 	endScan();
// 	return value;
   startScan(id);
   char* value = nextScan();
   endScan();
   return value;
#endif
}

/*
 * Set a configuration variable
 */

void ConfigFile::set(const char* id, const char* setting)
{
#ifdef WRITER
	static const char tempFileName[] = "config.tmp";

	if(!fp)
		fp = fopen(d_fileName, "r");

	rewind(fp);

	Boolean written = False;

 	FILE* fpOut = fopen(tempFileName, "w");
	if(fpOut)
	{
		if(fp)
		{
			MemPtr<char> buffer(MaxLineLength);

			while(!feof(fp))
			{
				if(fgets(buffer, MaxLineLength-1, fp))
				{
					buffer[MaxLineLength-1] = 0;

					if(buffer[0] && (buffer[0] != ';'))
					{
						char* s = parseLine(buffer);

						if(stricmp(buffer, id) == 0)
						{
							writeSetting(fpOut, id, setting);
							written = True;
						}
						else
							writeSetting(fpOut, buffer, s);
					}
					else
						fputs(buffer, fpOut);
				}
			}

			fclose(fp);
			fp = 0;
		}
		else
		{
			fprintf(fpOut, "; Configuration File for \"The Civil War\"\n\n");
		}

		if(!written)
			writeSetting(fpOut, id, setting);

		if(fclose(fpOut) == 0)
		{
			remove(d_fileName);
			rename(tempFileName, d_fileName);
		}
		else
			remove(tempFileName);
	}
#endif
}

/*
 * Get a boolean config value
 */

Boolean ConfigFile::getBool(const char* id, Boolean defVal)
{
	char* s = get(id);
	if(s)
	{
		if(stricmp(s, "ON") == 0)
			defVal = True;
		else if(stricmp(s, "YES") == 0)
			defVal = True;
		else if(stricmp(s, "1") == 0)
			defVal = True;
		else if(stricmp(s, "OFF") == 0)
			defVal = False;
		else if(stricmp(s, "NO") == 0)
			defVal = False;
		else if(stricmp(s, "0") == 0)
			defVal = False;
		delete[] s;
	}

	return defVal;
}

void ConfigFile::setBool(const char* id, Boolean flag)
{
	set(id, flag ? "On" : "Off");
}

void ConfigFile::setNum(const char* id, int n)
{
	char buffer[20];		// Long enough for integer in decimal (10 digits)

	// wsprintf(buffer, "%d", n);
	sprintf(buffer, "%d", n);

	set(id, buffer);
}

Boolean ConfigFile::getNum(const char* id, int& n)
{
	Boolean result = False;

	char* s = get(id);
	if(s)
	{
		if(isdigit(*s))
		{
			n = atoi(s);
			result = True;
		}
		delete[] s;
	}
	return result;
}

int ConfigFile::getEnum(const char* id, const char** enums, int defVal)
{
	char* s = get(id);

	if(s)
	{
		for(int v = 0; enums[v]; v++)
		{
			if(stricmp(enums[v], s) == 0)
			{
				defVal = v;
				break;
			}
		}
	}

	return defVal;
}

void ConfigFile::setEnum(const char* id, const char** enums, int val)
{
	set(id, enums[val]);
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
