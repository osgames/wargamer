/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SLLIST_HPP
#define SLLIST_HPP

#ifndef __cplusplus
#error sllist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Singly linked Intrusive Linked list (started by Paul)
 *
 * Usage is:
 *    class MyObject : public SLink { };
 *    SList<MyObject> list;
 *    SListIter<MyObject> iter = &list;
 *
 *    SListS<MyObject> staticList;
 *    SListSIter<MyObject> iter = &staticList
 *
 * If the list is static then the user is responsible for deleting nodes.
 * otherwise the list class deletes nodes when they are removed and upon
 * destruction of the list.
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "myassert.hpp"

/*
 * my first attempt at a template class
 * an intrusive single link list class, essentially copied from Strousteps book
 */

struct SLink {
  SLink* next;

  SLink() { next = 0; }
  virtual ~SLink() = 0;
};

inline SLink::~SLink() { }

class SList_Base {
	friend class ListIter;
	friend class ListIterR;
	  SLink* last;   // last->next is head of list

	  SLink* current;		// used by built-in iterator
	  SLink* prev;			// used by built-in iterator

	  int nItems;


	  /*
	   * Disable Copy Constructor and assignment
	   */


		SList_Base(const SList_Base&);
		SList_Base& operator = (const SList_Base&);

	protected:
	  bool dyn;		// List is responsible for deleting data
	protected:
	  SList_Base()
	  {
		last = 0;
		current = 0;
		prev = 0;
		nItems = 0;
		dyn=true;
	  }

	  virtual ~SList_Base()
	  {
		reset();
	  }

	  SYSTEM_DLL void insert(SLink* sLink);
	  SYSTEM_DLL void append(SLink* sLink);
	  SYSTEM_DLL SLink* get();
	  SLink* head() const { return last?last->next:0; }

	  // Iterator Functions

	  SLink* getCurrent() const {return current;}
	  SLink* first() { current = head(); prev = last; return current; }
	  // SLink* first() { return head(); }
	  SYSTEM_DLL SLink* next();
	  SLink* previous() const { return prev; }
	  SYSTEM_DLL SLink* previous(SLink* node);

	  SLink* next(const SLink* node) const { return (node == last) ? 0 : node->next; }
		// e.g. for(T* ptr = list->first(); ptr != 0; ptr = list->next(ptr)) { ... }

	  SLink* getLast() const { return last; }
	  SYSTEM_DLL void remove();
	  SYSTEM_DLL SLink* remove(SLink* node);
        // Remove node and return previous node
	  SYSTEM_DLL void reset();
	  int getNItems() const {return nItems;}

	  SYSTEM_DLL void insertAfter(SLink* prevNode, SLink* newNode);

	  SYSTEM_DLL bool isMember(SLink* node) const;
public:
	 // More Conventional Iterators

	 SYSTEM_DLL bool operator ++();
	 SYSTEM_DLL void rewind();
};


template<class T>
class SList : public SList_Base  {
  		/*
	 	 * Disable Copy Constructor
	 	 */
	private:
		SList(const SList<T>& list);
		SList<T>& operator = (const SList<T>& list);
  public:

  		SList() : SList_Base() { }

#if 0
  		/*
	 	 * Copy Constructor must copy the items
	 	 */

		SList(const SList<T>& list) { copy(list); }
		SList<T>& operator = (const SList<T>& list) { copy(list); return *this; }
#endif


	 void insert(T* a) {SList_Base::insert(a); }
	 void append(T* a) {SList_Base::append(a); }
	 T* get() { return static_cast<T*>(SList_Base::get()); }
	 T* head() const { return static_cast<T*>(SList_Base::head()); }
	 bool isMember(T* node) const { return SList_Base::isMember(node); }

	 // Iterator Functions
	 // Usage:
	 //	for(T* node = first(); node != 0; node = next()) { ... }

	 T* getCurrent() const { return static_cast<T*>(SList_Base::getCurrent()); }
	 T* first() { return static_cast<T*>(SList_Base::first()); }
	 T* next() { return static_cast<T*>(SList_Base::next()); }
	 T* prev() const { return static_cast<T*>(SList_Base::previous()); }
	 T* prev(T* node) { return static_cast<T*>(SList_Base::previous(node)); }

	 T* next(T* node) { return static_cast<T*>(SList_Base::next(node)); }
		// non-iterator version of next
		// e.g. for(T* ptr = list->first(); ptr != 0; ptr = list->next(ptr)) { ... }
	 const T* next(const T* node) const { return static_cast<const T*>(SList_Base::next(node)); }


	 // Other stuff

	 T* getLast() const { return static_cast<T*>(SList_Base::getLast());}
	 void remove() { SList_Base::remove(); }
	 void remove(T* item) { SList_Base::remove(item); }
	 void reset() { SList_Base::reset(); }
	 int getNItems() const { return SList_Base::getNItems(); }
	 int entries() const { return getNItems(); }

	 void insertAfter(T* prevNode, T* newNode) { SList_Base::insertAfter(prevNode, newNode); }

	private:
#if 0
		void copy(const SList<T>& list);
#endif
};




template<class T>
class SListS : public SList<T>  {
  public:
	 SListS()
	 {
		dyn = false;
	 }
};


class ListIter {
	 SLink* current;
	 SList_Base* list;
	 bool started;
  protected:
  	 ListIter() :
	 	// prev(0),
		current(0),
		list(0),
		started(false)
	 {
	 }

	 ListIter(SList_Base* l)
	 {
	 	init(l);
	 }

	 void init(SList_Base* l)
	 {
		list = l;
		rewind();
	 }

	 void init(SList_Base* l, SLink* item)
	 {
		list = l;
		current = item;
        started = false;
	 }


	 SYSTEM_DLL bool next();
	 SLink* getCurrent() const { ASSERT(started); return current; }
	 // SLink* getPrevious() const { return prev; }

	 SYSTEM_DLL void remove();			// Remove current item

	 void rewind()
	 {
		current = list->head();		// list->first();
		// prev = list->getLast();
		started = false;
	 }

};

/*
 * Typed version of ListIter
 */

template<class T>
class SListIter : private ListIter
{
public:
	SListIter() : ListIter() { }
	SListIter(SList<T>* l) : ListIter(l) { }
	
	void init(SList<T>* l) { ListIter::init(l); }
	void init(SList<T>* l, T* item) { ListIter::init(l, item); }

	bool next() { return ListIter::next(); }
	T* getCurrent() const { return static_cast<T*>(ListIter::getCurrent()); }

	// More common iterator methods

	bool operator ++() { return ListIter::next(); }
	T* current() const { return static_cast<T*>(ListIter::getCurrent()); }
	// T* prev() const { return static_cast<T*>(ListIter::getPrevious()); }
	void remove() { ListIter::remove(); }			// Remove current item
	void rewind() { ListIter::rewind(); }			// Restart the iterator

    T* operator->() const { return current(); }
    T& operator*() const { return *current(); }
    operator T* () const { return current(); }
};


template<class T>
class SListSIter : public SListIter<T>
{
public:
	SListSIter() : SListIter<T>() { }
	SListSIter(SListS<T>* l) : SListIter<T>(l) { }
};


/*
 * Read Only Iterator
 */

class ListIterR {
	 // const SLink* prev;
	 const SLink* current;
	 const SList_Base* list;
	 bool started;
  protected:
	 ListIterR() :
	 	// prev(0),
		current(0),
		list(0),
		started(false)
	 {
	 }

	 ListIterR(const SList_Base* l)
	 {
	 	init(l);
	 }

	 void init(const SList_Base* l)
	 {
		list = l;
		rewind();
	 }

	 SYSTEM_DLL bool next();
	 const SLink* getCurrent() const { ASSERT(started); return current; }
	 // const SLink* getPrevious() const { return prev; }

	 void rewind()
	 {
		// prev = list->getLast();
		current = list->head();		// list->first();
		started = false;
	 }
};

/*
 * Typed version of ListIter
 */

template<class T>
class SListIterR : private ListIterR
{
public:
	SListIterR() : ListIterR() { }
	SListIterR(const SList<T>* l) : ListIterR(l) { }
	void init(const SList<T>* l) { ListIterR::init(l); }

	bool next() { return ListIterR::next(); }
	const T* getCurrent() const { return static_cast<const T*>(ListIterR::getCurrent()); }

	// More common iterator methods

	bool operator ++() { return ListIterR::next(); }
	const T* current() const { return static_cast<const T*>(ListIterR::getCurrent()); }
	// const T* prev() const { return static_cast<const T*>(ListIterR::getPrevious()); }

	void rewind() { ListIterR::rewind(); }			// Restart the iterator

    const T* operator->() const { return getCurrent(); }
    const T& operator*() const { return *getCurrent(); }
    operator const T* () const { return getCurrent(); }
};


template<class T>
class SListSIterR : public SListIterR<T>
{
public:
	SListSIterR() : SListIterR<T>() { }
	SListSIterR(const SListS<T>* l) : SListIterR<T>(l) { }
};

#if 0
template<class T>
inline void SList<T>::copy(const SList<T>& list)
{
	reset();
	SListIterR<T> iter(&list);
	while(++iter)
	{
		append(new T(*iter.current()));
	}
}
#endif


#endif /* SLLIST_HPP */

