/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TRANSWIN_HPP
#define TRANSWIN_HPP

#include "sysdll.h"
#include "grtypes.hpp"

/*
 * A see-through window
 */

class DrawDIBDC;
class DrawDIB;

class SYSTEM_DLL SeeThroughWindow {
	 DrawDIBDC* d_dib;
//	 static int s_instanceCount;

	 HWND d_hParent;
    SIZE d_size;
  public:
	 SeeThroughWindow() :
      d_dib(0),
		d_hParent(0)
	 {
//		s_instanceCount++;
		d_size.cx = 0;
      d_size.cy = 0;
	 }
	 ~SeeThroughWindow();

	 void parent(HWND p) { d_hParent = p; }
	 void allocateDib(int cx, int cy);
	 DrawDIBDC* dib() const { return d_dib; }

	 void transferBits(PixelPoint& p);
	 void paint(HDC hdc);
};

#endif
