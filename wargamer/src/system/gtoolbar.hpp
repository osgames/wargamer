/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GTOOLBAR_HPP
#define GTOOLBAT_HPP

#include "sysdll.h"
#include "palwind.hpp"
#include "fonts.hpp"
#include "resstr.hpp"

struct TipData;
class ToolBar_Imp;
class DrawDIB;
class PixelPoint;
class ImageLibrary;

struct ToolButtonData {
  int d_menuID;       // id that coorospnds to menu item
  int d_style;
  UWORD d_imgID;
};

struct SYSTEM_DLL ToolBarInitData {
  HWND d_hParent;
  int d_nSections;
  TipData** d_tipData;
  ToolButtonData** d_buttonData;
  int* d_howMany;
  const DrawDIB* d_fillDib;
  const DrawDIB* d_buttonFillDib;
  const DrawDIB* d_capDib;
  const ImageLibrary* d_buttonImages;
  CustomBorderInfo d_borderColors;
  // const char** d_captionNames;
  const InGameText::ID* d_captionNames;
  const char** d_registryNames;
  LogFont d_capFont;

  ToolBarInitData() :
	 d_hParent(0),
	 d_nSections(0),
	 d_tipData(0),
	 d_buttonData(0),
	 d_howMany(0),
	 d_fillDib(0),
	 d_buttonFillDib(0),
	 d_capDib(0),
	 d_buttonImages(0),
	 d_borderColors(),
	 d_captionNames(0),
	 d_registryNames(0),
	 d_capFont() {}
};

class SYSTEM_DLL GToolBar {
	 ToolBar_Imp* d_toolBar;
  public:
	 GToolBar();
	 virtual ~GToolBar();

	 void init(const ToolBarInitData* td);
	 void position(const PixelPoint& p);

	 void show(bool visible);
     void enable(bool visible);
     bool isEnabled() const;
     bool isVisible() const;

	 // void destroy();
	 void setCheck(int menuID, Boolean f);
	 void enable(int menuID, Boolean f);
	 int height() const;
	 Boolean initialized() const { return (d_toolBar != 0); }
     HWND getHWND() const;

	 // void suspend(bool visible);

	 virtual void positionWindows() = 0;
     virtual void updateAll() = 0;
};


#endif
