/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef _TABLES_HPP
#define _TABLES_HPP

#include "sysdll.h"
#include "mytypes.h"
#include "myassert.hpp"

class SYSTEM_DLL TableBase {
	 UWORD d_width;
	 UWORD d_height;
	 UWORD d_nTables;

	 int d_resolution;   // if this is other than 0, then we use getFixed() rather than getInt()

	 Boolean d_initiated;

 public:
	TableBase();
	~TableBase() { }

	 Boolean init(const char* fileName, const char* chunkName, int resolution = 0);

	 Boolean isInitiated() const { return d_initiated; }
	 int getWidth() const { return d_width; }
	 int getHeight() const { return d_height; }
	 int getNTables() const { return d_nTables; }
	 int getResolution() const { return (d_resolution != 0) ? d_resolution : 1; }

 private:
	virtual void allocateTable(size_t size) = 0;
	virtual void storeItem(int i, ULONG value) = 0;
};


template<class T>
class Tables  : public TableBase {

	 T* d_table;


  public:
	 Tables();
	 ~Tables();



  protected:

	 const T* getTable() const { return d_table; }

 private:
 	virtual void allocateTable(size_t size)
	{
		d_table = new T[size];
		ASSERT(d_table != 0);
	}

	virtual void storeItem(int i, ULONG value)
	{
		ASSERT(d_table != 0);
		d_table[i] = static_cast<T>(value);
	}
};

template<class T>
Tables<T>::Tables()
{
  d_table = 0;
}

template<class T>
Tables<T>::~Tables()
{
  if(d_table)
	 delete[] d_table;
}


template<class T>
class Table1D : public Tables<T> {
  public:
	 T getValue(int i) const;
};

/*
 *  return array item for 1D table
 */

template<class T>
T Table1D<T>::getValue(int i) const
{
  ASSERT(isInitiated() == True);

  ASSERT(i < getWidth());

  // make sure its a 1D table
  ASSERT(getHeight() == 1);
  ASSERT(getNTables() == 1);

  const T* t = getTable();
  ASSERT(t != 0);

  return t[i];
}

template<class T>
class Table2D : public Tables<T> {
  public:
	 T getValue(int ix, int iy) const;
};

/*
 *  return array item for 2D table
 */

template<class T>
T Table2D<T>::getValue(int iy, int ix) const
{
  ASSERT(isInitiated() == True);

  ASSERT(ix < getWidth());
  ASSERT(iy < getHeight());

  // make sure its a 2D table
  ASSERT(getNTables() == 1);

  const T* t = getTable();
  ASSERT(t != 0);

  int index = (iy * getWidth()) + ix;
  ASSERT(index < getWidth()*getHeight());

  return t[index];
}




template<class T>
class Table3D : public Tables<T> {
  public:
	 T getValue(int ix, int iy, int iz) const;
};

/*
 *  return array item for 3D table
 */

template<class T>
T Table3D<T>::getValue(int iz, int iy, int ix) const
{
  ASSERT(isInitiated() == True);

  ASSERT(iz < getNTables());
  ASSERT(ix < getWidth());
  ASSERT(iy < getHeight());

  const T* t = getTable();
  ASSERT(t != 0);

  int index = (iz * (getWidth()*getHeight())) + (iy * getWidth()) + ix;
  ASSERT(index < (getNTables()*getWidth()*getHeight()));

  return t[index];
}

class SYSTEM_DLL StringTable {
	 char** d_table;
	 int d_entries;
	 Boolean d_initiated;
  public:
	 StringTable() : d_table(0), d_entries(0), d_initiated(False) {}
	 ~StringTable();

	 Boolean init(const char* fileName, const char* chunkName);
	 Boolean isInitiated() const { return d_initiated; }
	 const char* getValue(int index) const { ASSERT(index < d_entries); return d_table[index]; }

	 int entries() const { return d_entries; }
};

#endif
