/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SIMPSTK_HPP
#define SIMPSTK_HPP

#ifndef __cplusplus
#error simpstk.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Simple Stack
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "myassert.hpp"

template<class Type, int Size>
class SimpleStack
{
	public:
		SimpleStack() : d_top(0) { }
		~SimpleStack()
		{
			ASSERT(isEmpty());
			if(!isEmpty())
				throw Exception();
		}

		void push(const Type& t)
		{
			ASSERT(d_top < Size);
			if(d_top >= Size)
				throw Exception();
			d_items[d_top++] = t;
		}

		void pop(Type& t)
		{
			ASSERT(d_top != 0);
			if(d_top == 0)
				throw Exception();
			t = d_items[--d_top];
		}

		bool isEmpty() const
		{
			return d_top == 0;
		}

		class Exception
		{
			public:
				Exception() { }
				~Exception() { }
		};

	private:
		Type d_items[Size];
		int d_top;
};


#endif /* SIMPSTK_HPP */

