/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "tabwin.hpp"
#include "dib.hpp"
#include "palwind.hpp"
#include "app.hpp"
#include "palette.hpp"
// #include "generic.hpp"
#include "fonts.hpp"
#include "ctab.hpp"
#include "cbutton.hpp"
#include "dib_util.hpp"
#include "scrnbase.hpp"
#include "resstr.hpp"
#include "timer.hpp"

/*----------------------------------------------------------------------
 * tabbed info window
 */

DrawDIBDC* TabbedInfoWindow::s_dib = 0;
int TabbedInfoWindow::s_instanceCount = 0;

const int tBorderWidth = 1;

TabbedInfoWindow::TabbedInfoWindow(HWND hParent, int nItems, const char** text,
		 const DrawDIB* infoFillDib, const DrawDIB* bkDib,
		 const DrawDIB* buttonFillDib, const ImageLibrary* il,
		 HFONT tabFont, CustomBorderInfo bc) :
  d_hParent(hParent),
  d_infoFillDib(infoFillDib), //scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
  d_bkDib(bkDib), //scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground)),
  d_buttonFillDib(buttonFillDib),
  d_images(il),
  d_tabFont(tabFont),
  d_borderColors(bc),
  d_nItems(nItems),
  d_tabText(text),
  d_resText(0),
  d_winCX((ScreenBase::dbX() * 160) / ScreenBase::baseX()),
  d_winCY((ScreenBase::dbY() * 110) / ScreenBase::baseY()),
  d_offsetX((ScreenBase::dbX() * 3) / ScreenBase::baseX()),
  d_offsetY((ScreenBase::dbY() * 3) / ScreenBase::baseY()),
  d_shadowWidth((ScreenBase::dbX() * 2) / ScreenBase::baseX())
{
  ASSERT(d_hParent);
  ASSERT(d_infoFillDib);

  s_instanceCount++;

  SetRect(&d_dRect, 0, 0, 0, 0);

  HWND hWnd = createWindow(
		0,
		// GenericNoBackClass::className(),
        NULL,
		WS_POPUP | WS_CLIPSIBLINGS,
		0, 0, d_winCX, d_winCY,
		d_hParent, NULL // , APP::instance()
  );
}


TabbedInfoWindow::TabbedInfoWindow(HWND hParent, int nItems, const ResourceStrings& text,
		 const DrawDIB* infoFillDib, const DrawDIB* bkDib,
		 const DrawDIB* buttonFillDib, const ImageLibrary* il,
		 HFONT tabFont, CustomBorderInfo bc) :
  d_hParent(hParent),
  d_infoFillDib(infoFillDib), //scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground)),
  d_bkDib(bkDib), //scenario->getScenarioBkDIB(Scenario::BackPatterns::CherryWoodBackground)),
  d_buttonFillDib(buttonFillDib),
  d_images(il),
  d_tabFont(tabFont),
  d_borderColors(bc),
  d_nItems(nItems),
  d_tabText(0),
  d_resText(&text),
  d_winCX((ScreenBase::dbX() * 160) / ScreenBase::baseX()),
  d_winCY((ScreenBase::dbY() * 110) / ScreenBase::baseY()),
  d_offsetX((ScreenBase::dbX() * 3) / ScreenBase::baseX()),
  d_offsetY((ScreenBase::dbY() * 3) / ScreenBase::baseY()),
  d_shadowWidth((ScreenBase::dbX() * 2) / ScreenBase::baseX())
{
  ASSERT(d_hParent);
  ASSERT(d_infoFillDib);

  s_instanceCount++;

  SetRect(&d_dRect, 0, 0, 0, 0);

  HWND hWnd = createWindow(
		0,
		// GenericNoBackClass::className(),
        NULL,
		WS_POPUP | WS_CLIPSIBLINGS,
		0, 0, d_winCX, d_winCY,
		d_hParent, NULL // , APP::instance()
  );
}



TabbedInfoWindow::~TabbedInfoWindow()
{
  ASSERT((s_instanceCount - 1) >= 0);

  selfDestruct();

  if(--s_instanceCount == 0)
  {
	 if(s_dib)
		delete s_dib;

	 s_dib = 0;
  }
}

LRESULT TabbedInfoWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
	 HANDLE_MSG(hWnd, WM_CREATE,	onCreate);
	 HANDLE_MSG(hWnd, WM_NOTIFY, onNotify);
	 HANDLE_MSG(hWnd, WM_PAINT, onPaint);
	 HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
	 HANDLE_MSG(hWnd, WM_NCHITTEST, onNCHitTest);
//	 HANDLE_MSG(hWnd, WM_MOVE, onMove);
	 HANDLE_MSG(hWnd, WM_TIMER, onTimer);
//	 HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
//	 HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
//	 HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);

	 default:
		return defProc(hWnd, msg, wParam, lParam);
  }
}

BOOL TabbedInfoWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
  /*
	* Create a tabbed window
	*/

  HWND hwndTab = CreateWindow(
		WC_TABCONTROL, "",
		WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
			TCS_TOOLTIPS | TCS_FIXEDWIDTH,
		d_offsetX, d_offsetY,
		lpCreateStruct->cx - ((2 * d_offsetX) + d_shadowWidth),
		lpCreateStruct->cy - ((2 * d_offsetY) + d_shadowWidth),
		hWnd,
		(HMENU) ID_Tab,
		APP::instance(),
		NULL);

  ASSERT(hwndTab != NULL);

  /*
	* Set up the tabbed titles
	*
	* This must be done before using AdjustRect
	*/

  TC_ITEM tie;
  tie.mask = TCIF_TEXT | TCIF_PARAM;

  for(int i = 0; i < d_nItems; i++)
  {
  	 if(d_tabText)
	 {
	 	 ASSERT(d_resText == 0);
		 tie.pszText = const_cast<char*>(d_tabText[i]);
	 }
	 else
	 {
		 ASSERT(d_resText != 0);
	 	 tie.pszText = const_cast<char*>(d_resText->get(i));
	 }

	 tie.lParam = i;

	 TabCtrl_InsertItem(hwndTab, i, &tie);
  }

  /*
	* Set size of tabs (in Dialog Units)
	*/

  const LONG tw = (ScreenBase::dbX() * 27) / ScreenBase::baseX();
  const LONG th = (ScreenBase::dbY() * 13) / ScreenBase::baseY();

  TabCtrl_SetItemSize(hwndTab, tw, th);

//  GetWindowRect(hwndTab, &d_dRect);
//  TabCtrl_AdjustRect(hwndTab, FALSE, &d_dRect);

  /*
	* Initialize custom drawn tab class
	*/

  d_cTab.images(d_images);
  d_cTab.tabHWND(hwndTab);
  d_cTab.setBorderColor(d_borderColors);

#if 0
  LogFont lf;
  lf.height((6 * ScreenBase::dbY()) / ScreenBase::baseY());
  lf.weight(FW_MEDIUM);
  lf.face(scenario->fontName(Font_Decorative));

  Font font;
  font.set(lf);
#endif
  d_cTab.setFont(d_tabFont);
  d_cTab.setFlags(CustomDrawnTab::NoBorder | CustomDrawnTab::ShadowTab);

  /*
	* Get display area
	*/

  d_cTab.getDisplayRect(d_dRect);

  /*
	* Now, allocate dib
	*/

  DIB_Utility::allocateDib(&s_dib, lpCreateStruct->cx, lpCreateStruct->cy);
  ASSERT(s_dib);

  s_dib->setBkMode(TRANSPARENT);

  /*
	* Create close button
	*/

  const int buttonSZ = ((ScreenBase::dbX() * 7) / ScreenBase::baseX());
  const int offset = (ScreenBase::dbX() * 3) / ScreenBase::baseX();

  const int x = (lpCreateStruct->cx - (buttonSZ + offset));
  const int y = offset;

  HWND hButton = CreateWindow(CUSTOMBUTTONCLASS, "x",
				WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				x, y, buttonSZ, buttonSZ,
				hWnd, (HMENU)ID_Close, APP::instance(), NULL);

  ASSERT(hButton != 0);

  CustomButton cb(hButton);
  // cb.setFillDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::WoodBackground));
  cb.setFillDib(d_buttonFillDib); //scenario->getScenarioBkDIB(Scenario::BackPatterns::ButtonBackground));
  cb.setBorderColours(d_borderColors);

  return TRUE;
}

UINT TabbedInfoWindow::onNCHitTest(HWND hwnd, int x, int y)
{
  static POINT p;
  p.x = x;
  p.y = y;

  ScreenToClient(hwnd, &p);

  if(PtInRect(&d_dRect, p))
  {
	 return HTCAPTION;
  }
  else
  {
	 return HTCLIENT;
  }
}

void TabbedInfoWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
	 case ID_Close:
		hide();
		break;
  }
}

void TabbedInfoWindow::onMove(HWND hwnd, int x, int y)
{
}

LRESULT TabbedInfoWindow::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
	// Assume its from the tabbed dialog
	if(lpNMHDR->idFrom == ID_Tab)
	{
		switch(lpNMHDR->code)
		{
		case TCN_SELCHANGING:
			return FALSE;

		case TCN_SELCHANGE:
			onSelChange();
			forceRedraw();
			break;
		}
	}

	return TRUE;
}

void TabbedInfoWindow::onPaint(HWND hwnd)
{
  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  if(s_dib)
  {
	 BitBlt(hdc, 0, 0, d_winCX, d_winCY, s_dib->getDC(), 0, 0, SRCCOPY);
  }

  SelectPalette(hdc, oldPal, FALSE);
  EndPaint(hwnd, &ps);
}

void TabbedInfoWindow::forceRedraw()
{
  InvalidateRect(getHWND(), NULL, FALSE);
  UpdateWindow(getHWND());
}

void TabbedInfoWindow::run(const PixelPoint& p)
{
  const int offset = (ScreenBase::dbY() * 10) / ScreenBase::baseY();

  /*
	* make sure entire window is within mainwindows rect
	*/

  RECT r;
  GetWindowRect(APP::getMainHWND(), &r);

  int x = p.getX();
  int y = p.getY();

  // check right side
  if((x + d_winCX) > r.right)
  {
	 x -= (d_winCX + offset);
  }

  // check bottom
  if((y + d_winCY) > r.bottom)
  {
	 y -= (d_winCY + offset);
  }

  //  SetForegroundWindow(getHWND());
  // SetWindowPos(getHWND(), HWND_TOPMOST, x, y, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);
  SetWindowPos(getHWND(), HWND_TOP, x, y, 0, 0, SWP_NOSIZE);
  SetWindowPos(getHWND(), HWND_TOP, x, y, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);

  // Get it redrawn

  /*
   * Start timer
   */

   if(!d_timer.isRunning())
   {
     int rate = getRefreshRate();
     if(rate != 0)
        d_timer.initWMTimer(getHWND(), rate);
   }



  InvalidateRect(getHWND(), NULL, FALSE);

}

void TabbedInfoWindow::hide()
{
   if(d_timer.isRunning())
      d_timer.stopWMTimer();

  ShowWindow(getHWND(), SW_HIDE);
}


void TabbedInfoWindow::destroy()
{
  DestroyWindow(getHWND());
}

int TabbedInfoWindow::currentTab()
{
  int curSel = TabCtrl_GetCurSel(d_cTab.tabHWND());
  ASSERT(curSel < d_nItems);

  return curSel;
}

void TabbedInfoWindow::fillStatic()
{
  if(s_dib && d_infoFillDib && d_bkDib)
  {
	 static RECT r;
	 GetClientRect(getHWND(), &r);

	 /*
	  * fill with window background
	  */

	 s_dib->rect(0, 0, r.right - r.left, r.bottom - r.top, d_bkDib);

	 /*
	  * fill info section
	  */

	 s_dib->rect(d_dRect.left, d_dRect.top,
		  d_dRect.right - d_dRect.left, d_dRect.bottom - d_dRect.top, d_infoFillDib);

	 /*
	  * draw shadow
	  */


	 // right side shadow
	 s_dib->darkenRectangle(d_dRect.right + tBorderWidth, d_dRect.top + d_shadowWidth,
			d_shadowWidth, (d_dRect.bottom - d_dRect.top) + tBorderWidth, 50);

	 // bottom shadow
	 s_dib->darkenRectangle(d_dRect.left + d_shadowWidth, d_dRect.bottom + tBorderWidth,
			(d_dRect.right - d_dRect.left), d_shadowWidth, 50);

	 /*
	  * draw borders
	  */

	 static CustomBorderWindow bw(d_borderColors);
	 bw.drawThinBorder(s_dib->getDC(), r);

  }
}















/*
 * Default Timer Functions
 */

void TabbedInfoWindow::onRefresh()
{
}

int TabbedInfoWindow::getRefreshRate() const
{
   return 0;
}
