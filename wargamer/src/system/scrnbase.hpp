/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SCRNBASE_HPP
#define SCRNBASE_HPP

#include "sysdll.h"
#include "mytypes.h"

/*
 * Convert cordinates based on screen size
 *
 * Coordinates are a bit awkward here because it started off being
 * based on dialog units, but now depends on screen size
 *
 * The bigger the physical screen the larger the result.
 * It is intended so that bigger fonts can be used at higher resolutions
 *
 * e.g. at 1024 X 768: baseX = 40, dbX = 72... 
 *          value of 568 represents whole screen, x(12)=21, y(8)=14
 *      at  640 X 480: baseX = 40, dbX = 60... 
 *          value of 426 represents whole screen, x(12)=18, y(8)=12
 *      at  320 X 200: baseX = 40, dbX = 40
 *          value of 320 represents whole screen, x(12)=12, y(8)=8
 *
 * The idea is that x(4) is the width of 1 character and y(8) is height of a character
 *
 * An alternative method would be to use the user's font height settings
 * Then if he has a high resoultion, and a large monitor, he may be happy
 * with smaller fonts and a smaller screen area of a dialog.
 */

class SYSTEM_DLL ScreenBase {
public:
  static void calcBase();
  static UWORD baseX();
  static UWORD baseY();
  static UWORD dbX();
  static UWORD dbY();

  static int x(int p_x);
  static int y(int p_y);
};

/*
 * Get coordinate depending on physical screen size
 * e.g. input value assumes 1024 X 768 screen
 * output depends on screen resoultion
 *
 * e.g. BaseCords<1024>::get(256) will return 110 on a 640X480 screen.
 */


template<int WIDTH>
class BaseCords
{
    public:
        static int get(int p_x)
        {
            return MulDiv(p_x, GetSystemMetrics(SM_CXSCREEN), WIDTH);
        }
};

class Screen1024 : public BaseCords<1024> { };


#endif
