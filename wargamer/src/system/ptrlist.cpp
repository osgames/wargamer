/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Simplified Linked List
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1995/11/22 10:43:46  Steven_Green
 * Initial revision
 *
 * Revision 1.4  1994/07/19  19:53:03  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1994/03/10  14:27:18  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1994/02/17  14:57:44  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1994/02/15  23:22:28  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "ptrlist.hpp"

/*======================================================================
 * Doubly Linked List
 */

/*
 * Constructor for list
 */

DPtrListBase::DPtrListBase()
{
	first = 0;
	last = 0;
	count = 0;
}

void DPtrListBase::clear()
{
	while(entries())
		get();
}

/*
 * Add Item to end of list
 */

void DPtrListBase::append(void* data)
{
	DPtrLink* node = new DPtrLink;
	node->data = data;
	node->next = 0;
	node->prev = last;
	if(last)
		last->next = node;
	last = node;

	if(!first)
		first = node;

	count++;
}

/*
 * Add item to beginning of list
 */

void DPtrListBase::insert(void* data)
{
	DPtrLink* node = new DPtrLink;
	node->data = data;
	node->next = first;
	node->prev = 0;
	if(first)
		first->prev = node;
	first = node;

	if(!last)
		last = node;

	count++;
}

void* DPtrListBase::find(int n) const
{
	DPtrLink* link = first;

	while(link && n--)
	{
		link = link->next;
	}

	if(link)
		return link->data;
	else
		return 0;
}

int DPtrListBase::getID(void* el) const
{
	int count = 0;

	DPtrLink* link = first;
	while(link && (link->data != el))
	{
		link = link->next;
		count++;
	}

	if(link)
		return count;
	else
		return -1;
}

void* DPtrListBase::findLast() const
{
	if(last)
		return last->data;
	else
		return 0;
}

/*
 * Get and remove 1st item from list
 */

void*

DPtrListBase::get()
{
	if(first)
	{
		count--;
		DPtrLink* node = first;

		first = node->next;

		if(first)
			first->prev = 0;

		if(last == node)
			last = 0;

		void* data = node->data;
		delete node;

		return data;
	}
	else
		return 0;
}

void DPtrListBase::forAll(ForAllFunction function, void* data)
{
	DPtrLink* node = first;

	while(node)
	{
		DPtrLink* next = node->next;

		function(node->data, data);

		node = next;
	}
}

void DPtrListBase::removeNode(DPtrLink* link)
{
	if(link->prev)
		link->prev->next = link->next;
	else
		first = link->next;

	if(link->next)
		link->next->prev = link->prev;
	else
		last = link->prev;

	delete link;

	count--;
}

void DPtrListBase::remove(void* data)
{
	DPtrLink* link = first;

	while(link && (link->data != data))
		link = link->next;

	if(link)
		removeNode(link);
}


Boolean DPtrListBase::isMember(void* data)
{
	for(DPtrLink* link = first; link; link = link->next)
	{
		if(link->data == data)
			return True;
	}

	return False;
}

/*==============================================================
 * Double Link Iterator
 */

/*
 * Constructor
 */

DPtrListBaseIter::DPtrListBaseIter(DPtrListBase& list)
{
	container = &list;
	link = 0;
}

/*
 * Insert before current entry
 */

void DPtrListBaseIter::insert(void* data)
{
	if(link)
	{
		DPtrLink* node = new DPtrLink;
		node->data = data;

		node->next = link;
		node->prev = link->prev;

		if(link->prev)
			link->prev->next = node;
		else
			container->first = node;

		link->prev = node;
	}
	else
		container->insert(data);
	container->count++;
}

/*
 * Append after current entry
 */

void DPtrListBaseIter::append(void* data)
{
	if(link)
	{
		DPtrLink* node = new DPtrLink;
		node->data = data;

		node->prev = link;
		node->next = link->next;

		if(link->next)
			link->next->prev = node;
		else
			container->last = node;

		link->next = node;
	}
	else
		container->append(data);

	container->count++;
}

/*
 * Delete current item and set current to previous item
 */

void DPtrListBaseIter::remove()
{
	if(link)
	{
		DPtrLink* node = link;
		link = node->prev;

		container->removeNode(node);
	}
}

void* DPtrListBaseIter::current() const
{
	if(link)
		return link->data;
	else
		return 0;
}

int DPtrListBaseIter::find(void* data)
{
	link = container->first;

	while(link)
	{
		if(link->data == data)
			return 1;				// Success

		link = link->next;
	}

	return 0;						// Not found
}

/*
 * Advance to next item
 * return 0 if at end of list, or non-zero
 */

int DPtrListBaseIter::operator ++()
{
	if(link)
		link = link->next;
	else
		link = container->first;
	return (link != 0);
}

int DPtrListBaseIter::operator --()
{
	if(link)
	{
		link = link->prev;
		return (link != 0);
	}
	else
		return 0;
}

/*======================================================================
 * Singly Linked List
 */

/*
 * Constructor for list
 */

SPtrListBase::SPtrListBase()
{
	first = 0;
	last = 0;
	count = 0;
}

void SPtrListBase::clear()
{
	while(entries())
		get();
}

/*
 * Add Item to end of list
 */

void SPtrListBase::append(void* data)
{
	SPtrLink* node = new SPtrLink;
	node->data = data;
	node->next = 0;
	if(last)
		last->next = node;
	last = node;

	if(!first)
		first = node;

	count++;
}

/*
 * Add item to beginning of list
 */

void SPtrListBase::insert(void* data)
{
	SPtrLink* node = new SPtrLink;
	node->data = data;
	node->next = first;
	first = node;

	if(!last)
		last = node;

	count++;
}

void* SPtrListBase::find(int n) const
{
	SPtrLink* link = first;

	while(link && n--)
		link = link->next;

	if(link)
		return link->data;
	else
		return 0;
}

void* SPtrListBase::findLast() const
{
	if(last)
		return last->data;
	else
		return 0;
}

/*
 * Get and remove 1st item from list
 */

void* SPtrListBase::get()
{
	if(first)
	{
		count--;

		SPtrLink* node = first;

		first = node->next;

		if(last == node)
			last = 0;

		void* data = node->data;
		delete node;

		return data;
	}
	else
		return 0;
}

Boolean SPtrListBase::get(void*& value)
{
	if(first)
	{
		count--;

		SPtrLink* node = first;

		first = node->next;

		if(last == node)
			last = 0;

		value = node->data;
		delete node;

		return True;
	}
	else
		return False;
}



void SPtrListBase::forAll(ForAllFunction function, void* data)
{
	SPtrLink* node = first;

	while(node)
	{
		SPtrLink* next = node->next;

		function(node->data, data);

		node = next;
	}
}

/*==============================================================
 * Singly Link Iterator
 */

/*
 * Constructor
 */

SPtrListBaseIter::SPtrListBaseIter(SPtrListBase& list)
{
	container = &list;
	link = 0;
}

/*
 * Append after current entry
 */

void SPtrListBaseIter::append(void* data)
{
	if(link)
	{
		SPtrLink* node = new SPtrLink;
		node->data = data;

		node->next = link->next;

		if(!link->next)
			container->last = node;
		link->next = node;

	}
	else
		container->append(data);

	container->count++;
}

void* SPtrListBaseIter::current() const
{
	if(link)
		return link->data;
	else
		return 0;
}

/*
 * Advance to next item
 * return 0 if at end of list, or non-zero
 */

int SPtrListBaseIter::operator ++()
{
	if(link)
		link = link->next;
	else
		link = container->first;
	return (link != 0);
}

