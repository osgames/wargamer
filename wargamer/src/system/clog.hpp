/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CLOG_H
#define CLOG_H

#ifndef __cplusplus
#error clog.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Log File
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include <stdarg.h>

#if defined(NOLOG)

class SYSTEM_DLL LogFile {
public:
	LogFile(const char* name) { }
	virtual ~LogFile() { }
	void printf(const char* fmt, ...) { }
	void vprintf(const char* fmt, va_list args) { }
	void close() { }
	void write(const char* s) { }
};

class SYSTEM_DLL LogFileFlush : public LogFile
{
public:
	LogFileFlush(const char* name) : LogFile(name) { }
	void printf(const char* fmt, ...) { }
};


#else


struct _Log;

class SYSTEM_DLL LogFile {
private:
	_Log* logFile;
public:
	LogFile(const char* name);
	virtual ~LogFile();

	void printf(const char* fmt, ...);
	void vprintf(const char* fmt, va_list args);
	void close();			// Temporarily close log file

	void lock();
		// Stop any other thread from writing to logfile
		// Useful if about to log several lines and you don't want
		// it all mixed up with other thread's output

	void unLock();
		// Relinquish Lock

	void write(const char* s);

protected:
	virtual void endLine();

private:
	void printfNoLF(const char* fmt, ...);

	friend LogFile& operator << (LogFile& file, unsigned long n);
	friend LogFile& operator << (LogFile& file, long n);
	friend LogFile& operator << (LogFile& file, unsigned int n);
	friend LogFile& operator << (LogFile& file, int n);
	friend LogFile& operator << (LogFile& file, unsigned short n);
	friend LogFile& operator << (LogFile& file, short n);
	friend LogFile& operator << (LogFile& file, unsigned char c);
	friend LogFile& operator << (LogFile& file, char c);
	friend LogFile& operator << (LogFile& file, const char* s);
	friend LogFile& operator << (LogFile& file, LogFile& (*func)(LogFile&));
	friend LogFile& operator << (LogFile& file, double f);
	friend LogFile& endl(LogFile& file);
};

class SYSTEM_DLL LogFileFlush : public LogFile
{
public:
	LogFileFlush(const char* name) : LogFile(name) { }
private:
	virtual void endLine();
};



// Some common output commands to emulate iostream usage

inline LogFile&  operator << (LogFile& file, unsigned long n) { file.printfNoLF("%lu", n); return file; }
inline LogFile&  operator << (LogFile& file, long n) { file.printfNoLF("%ld", n); return file; }
inline LogFile&  operator << (LogFile& file, unsigned int n) { file.printfNoLF("%u", n); return file; }
inline LogFile&  operator << (LogFile& file, int n) { file.printfNoLF("%d", n); return file; }
inline LogFile&  operator << (LogFile& file, unsigned short n) { file.printfNoLF("%hu", n); return file; }
inline LogFile&  operator << (LogFile& file, short n) { file.printfNoLF("%hd", n); return file; }
inline LogFile&  operator << (LogFile& file, unsigned char c) { file.printfNoLF("%c", c); return file; }
inline LogFile&  operator << (LogFile& file, char c) { file.printfNoLF("%c", c); return file; }
inline LogFile&  operator << (LogFile& file, const char* s) { file.write(s); return file; }
inline LogFile&  operator << (LogFile& file, LogFile& (*func)(LogFile&)) { return func(file); }
inline LogFile&  operator << (LogFile& file, double f) { file.printfNoLF("%f", f); return file; }

inline LogFile&  endl(LogFile& file) { file.endLine(); return file; }
SYSTEM_DLL LogFile& sysTime(LogFile& file);	// Add system time in Milliseconds

SYSTEM_DLL LogFile& operator << (LogFile& file, bool b);

#endif	// NOLOG

#endif /* CLOG_H */

