/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Simple Message Box
 *
 * This needs changing:
 *		MYASSERT uses it, and we don't want myassert depending on the
 *		rest of the game.
 *
 *		Solution is for Assertion module to be given a virtual class
 *		for displaying messages (defaulting to cout?).  Then the
 *		program can change it on startup, or provide an overide.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "msgbox.hpp"

#include "app.hpp"
#include <windows.h>

// #include "wargame.hpp"

int 

messageBox(const char* title, const char* text, unsigned int uType)
{
	// BOOL oldPause = gApp.pauseGame(TRUE);
	// int result = MessageBox(gApp.getHwnd(), text, title, uType | MB_SETFOREGROUND);
	int result = MessageBox(APP::getMainHWND(), text, title, uType | MB_SETFOREGROUND);
	// gApp.pauseGame(oldPause);

	return result;
}

