/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PICLIB_H
#define PICLIB_H

#ifndef __cplusplus
#error piclib.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Picture Library
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */


#include "sysdll.h"

class DIB;



class SYSTEM_DLL PicLibrary
{
	public:

		/*
		 * todo:
		 *   Use a reference counting technique to allow Pictures to be copied
		 */

		class SYSTEM_DLL Picture
		{
         public:
      		Picture(const Picture&);
				Picture& operator=(const Picture&);
				Picture(const char* name);
				~Picture();

				operator const DIB* () const { return d_dib; }

				const DIB* operator ->() const { return d_dib; }

			private:
				DIB* d_dib;
		};

		static Picture get(const char* name);
};

#endif /* PICLIB_H */

