/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Device Context Manager
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "dc_man.hpp"
#include "sllist.hpp"
#include "critical.hpp"
#include "myassert.hpp"
			
#undef DEBUG_DC

class DeviceContextItem : public DCsave {
	friend class DeviceContextManager;

	HDC hdc;

public:
	DeviceContextItem();
	virtual ~DeviceContextItem();

	void save() { DCsave::save(hdc); }
	void restore() { DCsave::restore(hdc); }

	operator HDC() const { return hdc; }
};

class DeviceContextLinked : public DeviceContextItem, public SLink {
	friend class DeviceContextManager;

	Boolean inUse;

#ifdef DEBUG
	int timesUsed;
#endif

public:
	DeviceContextLinked();
	~DeviceContextLinked();
};

class DeviceContextManager : public SList<DeviceContextLinked>, public SharedData {
#ifdef DEBUG
	int nAlloced;
#endif
public:
	DeviceContextManager();
	~DeviceContextManager();

	DeviceContextItem* get();
	void release(DeviceContextItem* dc);
};


static DeviceContextManager manager;


DeviceContextItem::DeviceContextItem()
{
	hdc = CreateCompatibleDC(NULL);

	if(hdc == NULL)
		throw DeviceContextError();

#ifdef DEBUG		// We want to see this
	debugLog("DeviceContextItem::create() = %p\n", (void*) hdc);
#endif
}

DeviceContextItem::~DeviceContextItem()
{
#ifdef DEBUG_DC
	debugLog("~DeviceContextItem() hdc = %p\n", (void*) hdc);
#endif

	ASSERT(hdc != NULL);

	if(hdc)
	{
		DeleteDC(hdc);
		hdc = NULL;
	}
}

/*
 * Linked Device Contexts
 */

DeviceContextLinked::DeviceContextLinked()
{
#ifdef DEBUG_DC
	debugLog("+DeviceContextLinked()\n");
#endif

	inUse = False;

#ifdef DEBUG
	timesUsed = 0;
#endif

#ifdef DEBUG_DC
	debugLog("-DeviceContextLinked(), hdc=%p\n", (void*) hdc);
#endif
}

DeviceContextLinked::~DeviceContextLinked()
{
#ifdef DEBUG_DC
	debugLog("+~DeviceContextLinked(), hdc=%p\n", (void*) hdc);
#endif

	ASSERT(!inUse);

#ifdef DEBUG_DC
	debugLog("-~DeviceContextLinked()\n");
#endif
}


/*========================================================
 * DeviceContextManager
 */

DeviceContextManager::DeviceContextManager()
{
}

DeviceContextManager::~DeviceContextManager()
{
#ifdef DEBUG
	debugLog("+---------------------------------------\n");
	debugLog("DeviceContextManager statistics:\n");
	debugLog("Number of DC's created = %d\n", nAlloced);
#endif

	while(getNItems())
	{
		DeviceContextLinked* dc = SList<DeviceContextLinked>::get();

#ifdef DEBUG
		debugLog("DC at %p used %d times\n", (void*) dc->hdc, dc->timesUsed);
#endif

		delete dc;
	}

#ifdef DEBUG
	debugLog("------------------------\n");
#endif
}


DeviceContextItem* DeviceContextManager::get()
{
	LockData lock(this);

#ifdef DEBUG_DC
	debugLog("+DeviceContextManager::get()\n");
#endif

	DeviceContextLinked* dc = first();

	while(dc)
	{
		if(!dc->inUse)
			break;

		dc = next();
	}

	if(dc == 0)
	{
		dc = new DeviceContextLinked;

		if(dc == 0)
			throw DeviceContextError();

		append(dc);

#ifdef DEBUG
		nAlloced++;
#endif
	}

	dc->save();
	dc->inUse = True;

#ifdef DEBUG
	dc->timesUsed++;
#endif

#ifdef DEBUG_DC
	debugLog("-DeviceContextManager::get()\n");
#endif

	return dc;
}

void DeviceContextManager::release(DeviceContextItem* dc)
{
	LockData lock(this);

#ifdef DEBUG_DC
	debugLog("+~DeviceContextManager::release()\n");
#endif

	DeviceContextLinked* dcl = (DeviceContextLinked*) dc;

	ASSERT(dcl->inUse);

	dcl->inUse = False;
	dc->restore();

#ifdef DEBUG_DC
	debugLog("-DeviceContextManager::release()\n");
#endif

}

/*
 * Device Context Container
 */


DeviceContext::DeviceContext()
{
	item = manager.get();
	if(item == 0)
		throw DeviceContextError();
}


DeviceContext::~DeviceContext()
{
	if(item)
		manager.release(item);
}


DeviceContext::operator HDC() const
{
	return *item; 
}



