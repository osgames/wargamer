/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *	Machine independant file format
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1995/10/29 16:01:13  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1993/12/16  22:19:35  Steven_Green
 * Initial revision
 *
 * Revision 1.1  1993/12/16  16:42:54  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "filedata.hpp"

void putWord(IWORD* dest, UWORD src)
{
	dest->b[0] = UBYTE(src);
	dest->b[1] = UBYTE(src >> 8);
}

#if 0
void putByte(IBYTE* dest, UBYTE src)
{
	dest->b[0] = UBYTE(src);
}
#endif

void putLong(ILONG* dest, ULONG src)
{
	dest->b[0] = UBYTE(src);
	dest->b[1] = UBYTE(src >> 8);
	dest->b[2] = UBYTE(src >> 16);
	dest->b[3] = UBYTE(src >> 24);
}

#if 0
UBYTE getByte(IBYTE* src)
{
 	return src->b[0];
}
#endif

UWORD getWord(IWORD* src)
{
	return UWORD(src->b[0] + (src->b[1] << 8));
}

ULONG getLong(ILONG* src)
{
	return src->b[0] + (src->b[1] << 8) + (src->b[2] << 16) + (src->b[3] << 24);
}
