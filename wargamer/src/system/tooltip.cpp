/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Tooltip manager
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.2  2001/06/18 08:25:46  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.1  1995/11/13 11:08:49  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "tooltip.hpp"
#include "sllist.hpp"
#include "app.hpp"
#include "myassert.hpp"
#include "wmisc.hpp"
#include <windows.h>

#ifdef DEBUG
// #define DEBUG_TOOLTIP
#endif

#ifdef DEBUG_TOOLTIP
#include "clog.hpp"
LogFileFlush tipLog("tooltip.log");
#endif


/*===============================================================
 * New Stuff
 */

struct TipItem : public SLink {
    HWND d_control;		// Control's HWND
    int d_id;				// Control ID
    RECT d_r;					// Rectangle (valid if HWND == NULL)
    int d_tip;				// Resource ID for tip
    int d_hint;				// Resource ID for hint
};

class TipItemList : public SList<TipItem>
{
  public:
    TipItemList() : SList<TipItem>() { }
};

class TipItemIter : public SListIter<TipItem>
{
    // Unimplemented Copy constructor

    TipItemIter(const TipItemIter& iter);
    TipItemIter& operator = (const TipItemIter& iter);

  public:
    TipItemIter(TipItemList* list) : SListIter<TipItem>(list) { }
};

struct WTipItem : public SLink {
    HWND d_parent;			// Parent Window
    HWND d_tip;          // Tooltip window   
    TipItemList d_list;	// List of controls in this window
};

class WTipList : public SList<WTipItem>
{
  public:
  	WTipList() : SList<WTipItem>() { }
};

class WTipItemIter : public SListIter<WTipItem>
{
    // Unimplemented Copy constructor

    WTipItemIter(const WTipItemIter& iter);
    WTipItemIter& operator = (const WTipItemIter& iter);

  public:
    WTipItemIter(WTipList* list) : SListIter<WTipItem>(list) { }
};

class ToolTipImp {
    static ToolTipImp* s_toolImp;

    HintLineBase* d_hintLine;		// Hintline displayer
    Boolean d_initialised;			// Has it been initialised?
    WTipList d_list;					// List of windows
//	HWND d_hTT;							// Tooltip Handle
    HHOOK d_hook;						// Hook Handle
    Boolean d_lockHint;				// Set to disable tooltip from using hintline

    // Unimplemented copy constructor

    ToolTipImp(const ToolTipImp& tip);
    ToolTipImp& operator = (const ToolTipImp& tip);

  public:
    ToolTipImp();
    ~ToolTipImp();

    void addTip(HWND hDial, HWND hControl, int tipID, int hintID);
		// Add a single Tip for a window.

    void addTip(HWND hDial, int controlID, const RECT& r, int tipID, int hintID);
		// Add a single tip using a rectangle within parent window

    void addTips(HWND hDial, const TipData* tipData);
		// Add some tips

    void delTips(HWND hDial);
		// Remove all Tips belonging to given window

    void setHintDisplay(HintLineBase* hintDisplay) { d_hintLine = hintDisplay; }
		// Set the hint Line Displayer

    void showHint(const String& text)
    {
      d_lockHint = True;
      if(d_hintLine)
        d_hintLine->showHint(text);
    }

    void clearHint()
    {
      d_lockHint = False;
      if(d_hintLine)
        d_hintLine->clearHint();
    }

  private:
    void initialise();
		// If not already initialised, then create the tooltip window, etc.

    LRESULT procMessage(int code, WPARAM wParam, LPARAM lParam);
		// Message Hook Message Handler

    // LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
		// Process Notify Message

    void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
		// MOuse Move Message Handler

    WTipItem* makeWndList(HWND hDial);
		// Find list for given Window Handle
		// Create a new one if not found

    void setWinToolTip(HWND hDial, HWND hTip, const TipItem* item);
		// Create Window's tooltip

    HWND makeTipWindow(HWND parent);
		// Create a ToolTip window

    friend LRESULT CALLBACK tipGetMsgProc(int code, WPARAM wParam, LPARAM lParam);
};


#ifndef BEFORE_DLL


/*
 * Global Instanct of ToolTips
 */

static ToolTipImp g_tipImp;

/*
 * Wrapper Implementation
 */

void 

ToolTipManager::addTip(HWND hDial, HWND hControl, int tipID, int hintID)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("addTip(%p, %p, %d, %d",
                (void*) hDial,
                (void*) hControl,
                (int) tipID,
                (int) hintID);
#endif

	g_tipImp.addTip(hDial, hControl, tipID, hintID);
}

// Add a single tip using a rectangle within parent window

void

ToolTipManager::addTip(HWND hDial, int controlID, const RECT& r, int tipID, int hintID)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("addTip(%p, %d, %d,%d,%d,%d, %d, %d",
                (void*) hDial,
                (int) controlID,
                (int) r.left, (int)r.top, (int) r.right, (int) r.bottom,
                (int) tipID,
                (int) hintID);
#endif

	g_tipImp.addTip(hDial, controlID, r, tipID, hintID);
}


void

ToolTipManager::addTips(HWND hDial, const TipData* tipData)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("addTip(%p, tipData)",
                (void*) hDial);
#endif

	g_tipImp.addTips(hDial, tipData);
}

void

ToolTipManager::delTips(HWND hDial)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("delTips(%p)", (void*) hDial);
#endif

	g_tipImp.delTips(hDial);
}

void

ToolTipManager::setHintDisplay(HintLineBase* hintDisplay)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("setHintDisplay(%p)", (void*) hintDisplay);
#endif

	g_tipImp.setHintDisplay(hintDisplay);
}

void 

ToolTipManager::showHint(HINSTANCE hinst, UINT id)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("showHint(%p, %ud)", (void*)hinst, (unsigned int) id);
#endif

	ResString text(hinst, id);
	g_tipImp.showHint(text.str());
}

void

ToolTipManager::showHint(const String& text)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("showHint(%s)", text.c_str());
#endif

	g_tipImp.showHint(text);
}

void 

ToolTipManager::clearHint()
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("clearHint");
#endif

	g_tipImp.clearHint();
}


#else	// Old Non-DLL version

/*
 * Global Instanct of ToolTips
 */

ToolTipManager g_toolTip;

/*
 * Wrapper Implementation
 */

ToolTipManager::ToolTipManager() : d_tipData(new ToolTipImp)
{
	ASSERT(d_tipData != 0);

#ifdef DEBUG_TOOLTIP
	tipLog.printf("ToolTipManager created");
#endif
}

ToolTipManager::~ToolTipManager()
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("ToolTipManager Destructed");
#endif

	delete d_tipData;
}

// Add a single Tip for a window.

void 

ToolTipManager::addTip(HWND hDial, HWND hControl, int tipID, int hintID)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("addTip(%p, %p, %d, %d",
                (void*) hDial,
                (void*) hControl,
                (int) tipID,
                (int) hintID);
#endif

	if(d_tipData != 0)
		d_tipData->addTip(hDial, hControl, tipID, hintID);
}

// Add a single tip using a rectangle within parent window

void

ToolTipManager::addTip(HWND hDial, int controlID, const RECT& r, int tipID, int hintID)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("addTip(%p, %d, %d,%d,%d,%d, %d, %d",
                (void*) hDial,
                (int) controlID,
                (int) r.left, (int)r.top, (int) r.right, (int) r.bottom,
                (int) tipID,
                (int) hintID);
#endif

	if(d_tipData != 0)
		d_tipData->addTip(hDial, controlID, r, tipID, hintID);
}


void

ToolTipManager::addTips(HWND hDial, const TipData* tipData)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("addTip(%p, tipData)",
                (void*) hDial);
#endif

	if(d_tipData != 0)
		d_tipData->addTips(hDial, tipData);
}

void

ToolTipManager::delTips(HWND hDial)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("delTips(%p)", (void*) hDial);
#endif

	if(d_tipData != 0)
		d_tipData->delTips(hDial);
}

void

ToolTipManager::setHintDisplay(HintLineBase* hintDisplay)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("setHintDisplay(%p)", (void*) hintDisplay);
#endif

	if(d_tipData != 0)
		d_tipData->setHintDisplay(hintDisplay);
}

void 

ToolTipManager::showHint(HINSTANCE hinst, UINT id)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("showHint(%p, %ud)", (void*)hinst, (unsigned int) id);
#endif

	if(d_tipData != 0)
	{
		ResString text(hinst, id);
		d_tipData->showHint(text);
	}
}

void

ToolTipManager::showHint(const String& text)
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("showHint(%s)", text.c_str());
#endif

	if(d_tipData != 0)
		d_tipData->showHint(text);
}

void 

ToolTipManager::clearHint()
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("clearHint");
#endif

	if(d_tipData != 0)
		d_tipData->clearHint();
}

#endif	// BEFORE_DLL

/*
 * Tool Tip Implementation
 */

ToolTipImp* ToolTipImp::s_toolImp = 0;

ToolTipImp::ToolTipImp() :
	d_hintLine(0),
	d_initialised(False),
	d_list(),
//	d_hTT(NULL),
	d_hook(NULL),
	d_lockHint(False)
{
	s_toolImp = this;
}

ToolTipImp::~ToolTipImp()
{
#ifdef DEBUG_TOOLTIP
	tipLog.printf("ToolTipImp being destructed");
	if(d_list.entries() != 0)
	{
		tipLog.printf("There are undeleted hints:");
		WTipItemIter listIter(&d_list);
		while(++listIter)
		{
			WTipItem* list = listIter.current();

			tipLog.printf("%p: %d entries",
                    (void*) list->d_parent,
                    (int) list->d_list.entries());
		}
	}
#endif

	s_toolImp = 0;

	if(d_hook != NULL)
	{
		UnhookWindowsHookEx(d_hook);
		d_hook = NULL;
	}

	// May be an idea to DestroyWindow, but the chances are
	// that it will already have been destroyed by the application
	// when it closes down
#if 0
	if(d_hTT != NULL)
	{
		DestroyWindow(d_hTT);
		d_hTT = NULL;
	}
#endif
}

/*
 * Find a list of Tips for the given window
 * Create a new one if none already exist
 */

WTipItem* ToolTipImp::makeWndList(HWND hDial)
{
	WTipItemIter listIter(&d_list);
	while(++listIter)
	{
		WTipItem* list = listIter.current();

		if(list->d_parent == hDial)
			return list;
	}

	/*
	 * get here if no list found for our window
	 */

	WTipItem* tipItem = new WTipItem;
	tipItem->d_parent = hDial;

	tipItem->d_tip = makeTipWindow(tipItem->d_parent);

	d_list.append(tipItem);

	return tipItem;
}

void ToolTipImp::setWinToolTip(HWND hDial, HWND hTip, const TipItem* item)
{
	if(item->d_tip != TipData::None)
	{
		TOOLINFO ti;
		ti.cbSize = sizeof(TOOLINFO);

		if(item->d_control == NULL)
		{
			ti.uFlags = TTF_SUBCLASS;
			ti.uId = item->d_id;
			ti.rect = item->d_r;
		}
		else
		{
			ti.uFlags = TTF_IDISHWND | TTF_SUBCLASS;
			ti.uId = (UINT) (HWND) item->d_control;
		}

		ti.hwnd = hDial;

		if(item->d_tip == TipData::Callback)
		{
			ti.lpszText = LPSTR_TEXTCALLBACK;
			ti.hinst = NULL;
		}
		else
		{
			ti.lpszText = MAKEINTRESOURCE(item->d_tip);	// controlID);
			ti.hinst = wndInstance(hDial);	// APP:instance();
		}

		BOOL result = SendMessage(hTip, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);
		ASSERT(result == True);
	}
}


/*
 * Add a single tip to the control
 */

void ToolTipImp::addTip(HWND hDial, HWND hControl, int tipID, int hintID)
{
	ASSERT(hDial != NULL);
	ASSERT(hControl != NULL);

	initialise();

	WTipItem* tipItem = makeWndList(hDial);
	ASSERT(tipItem != 0);

	TipItem* item = new TipItem;
	ASSERT(item != 0);

	if(item != 0)
	{
		item->d_control = hControl;
		item->d_id = GetWindowLong(hControl, GWL_ID);
		item->d_tip  		= tipID;
		item->d_hint 		= hintID;

		tipItem->d_list.append(item);

		setWinToolTip(hDial, tipItem->d_tip, item);
	}
}

void ToolTipImp::addTip(HWND hDial, int controlID, const RECT& r, int tipID, int hintID)
{
	WTipItem* tipItem = makeWndList(hDial);
	ASSERT(tipItem != 0);

	TipItem* item = new TipItem;
	ASSERT(item != 0);

	if(item != 0)
	{
		item->d_control = NULL;		// Indicates that rectangle is valid
		item->d_id = controlID;
		item->d_r = r;
		item->d_tip = tipID;
		item->d_hint = hintID;

		tipItem->d_list.append(item);

		setWinToolTip(hDial, tipItem->d_tip, item);
	}
}

/*
 * Add Tips to the control
 */

void ToolTipImp::addTips(HWND hDial, const TipData* tipData)
{
	ASSERT(hDial != NULL);
	ASSERT(tipData != 0);

	initialise();

	/*
	 * Create window list
	 */

	WTipItem* tipItem = makeWndList(hDial);
	// WTipItem* tipItem = new WTipItem;
	// tipItem->d_parent = hDial;

	/*
	 * Add items to list and to ToolBar Window
	 */

	for(; (tipData->controlID >= 0); tipData++)
	{
		HWND hDlg = GetDlgItem(hDial, tipData->controlID);

		// ASSERT(hDlg != NULL);

		if(hDlg != NULL)
		{
			TipItem* item = new TipItem;
			ASSERT(item != 0);

			item->d_control 	= hDlg;
			item->d_id   		= tipData->controlID;
			item->d_tip  		= tipData->toolTipID;
			item->d_hint 		= tipData->hintID;

			tipItem->d_list.append(item);

			setWinToolTip(hDial, tipItem->d_tip, item);
		}
	}
}

/*
 * Remove Tips from control
 */

void ToolTipImp::delTips(HWND hDial)
{
	initialise();

	WTipItemIter listIter(&d_list);
	while(++listIter)
	{
		WTipItem* list = listIter.current();

		if(list->d_parent == hDial)
		{
			/*
			 * Remove items from ToolTip Window
			 */

			TipItemIter itemIter(&list->d_list);
			while(++itemIter)
			{
				TipItem* item = itemIter.current();

				TOOLINFO ti;
				ti.cbSize = sizeof(TOOLINFO);
				ti.uFlags = TTF_IDISHWND | TTF_SUBCLASS;
				ti.hwnd = list->d_parent;
				ti.uId = (UINT) (HWND) item->d_control;
				// ti.hinst = wndInstance(ti.hwnd);	// APP:instance();
				// ti.lpszText = tipData->toolTipID;

				SendMessage(list->d_tip, TTM_DELTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);
			}

			DestroyWindow(list->d_tip);

			listIter.remove();
			break;
		}
	}
}

/*
 * Initialise control
 */

void ToolTipImp::initialise()
{
	if(!d_initialised)
	{
#if 0
		d_hTT = CreateWindowEx(WS_EX_TOPMOST,
                           TOOLTIPS_CLASS,
                           NULL,
                           TTS_ALWAYSTIP,
                           CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                           NULL, 			// Parent
                           NULL,
                           APP::instance(),
                           NULL);

		ASSERT(d_hTT != NULL);
#endif
		// Set up Windows Hook

		d_hook = SetWindowsHookEx(WH_GETMESSAGE, tipGetMsgProc, APP::instance(), GetCurrentThreadId());

		ASSERT(d_hook != NULL);

		d_initialised = True;
	}
}

HWND ToolTipImp::makeTipWindow(HWND parent)
{
	HWND hTT = CreateWindowEx(
                            0,					// WS_EX_TOPMOST,
                                TOOLTIPS_CLASS,
                                NULL,
                                TTS_ALWAYSTIP,
                                CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                                parent, 			// Parent
                                NULL,
                                APP::instance(),
                                NULL);

	ASSERT(hTT != NULL);
	return hTT;
}
/*
 * My Call Back Function
 * There is no parameter to identify different instances, so
 * I'm going to have to use the global toolTip!
 */


LRESULT CALLBACK tipGetMsgProc(int code, WPARAM wParam, LPARAM lParam)
{
	if(ToolTipImp::s_toolImp != 0)
		return ToolTipImp::s_toolImp->procMessage(code, wParam, lParam);
	else
		return 0;
}

LRESULT ToolTipImp::procMessage(int code, WPARAM wParam, LPARAM lParam)
{
	if(code >= 0)
	{
		MSG* msg = (MSG*) lParam;

		/*
		 * Wow.. every message for our application will be
		 *       coming through here.  What shall we do with it?
		 */

		switch(msg->message)
		{
#if 0
      case WM_NOTIFY:	// Check for TTM_ messages
        HANDLE_WM_NOTIFY(msg->hwnd, msg->wParam, msg->lParam, onNotify);
        break;
#endif
      case WM_MOUSEMOVE:		// Check for over control
        HANDLE_WM_MOUSEMOVE(msg->hwnd, msg->wParam, msg->lParam, onMouseMove);
        // Drop through...
      case WM_LBUTTONDOWN:		// Pass onto ToolTip
      case WM_LBUTTONUP:
      case WM_MBUTTONDOWN:
      case WM_MBUTTONUP:
      case WM_RBUTTONDOWN:
      case WM_RBUTTONUP:
        // Forward to tooltip class

			{
			  WTipItemIter iter(&d_list);
			  while(++iter)
			  {
          WTipItem* item = iter.current();

          SendMessage(item->d_tip, TTM_RELAYEVENT, 0, (LPARAM) (LPMSG) msg);
			  }
			}

//		 	SendMessage(d_hTT, TTM_RELAYEVENT, 0, (LPARAM) (LPMSG) msg);
			break;
		}
	}

	if(code != HC_ACTION)
		return CallNextHookEx(d_hook, code, wParam, lParam);
	else
		return 0;
}

#if 0
/*
 * Don't need to do this, because we are setting the resource IDs
 * when adding items to the tooltip window.
 */

LRESULT ToolTipImp::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
  switch(lpNMHDR->code)
  {
    case TTN_SHOW:
      break;

    case TTN_NEEDTEXT:
    {
      /*
       * Find Tool, and set up text
       */

      LPTOOLTIPTEXT lptt = (LPTOOLTIPTEXT)lpNMHDR;

      lptt->hinst = NULL;
      lstrcpy(lptt->szText, "Test Popup");
      lptt->lpszText = 0;
      // displayTipText(stringID, controlID, numToolTipItems, lpNMHDR, NULL);
      break;
    }

    case TTN_POP:
      break;
	}

	return True;
}
#endif


/*
 * onMouseMove
 *
 * Scan tips for match.  Do hint if found or clear if not.
 *
 */


void ToolTipImp::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
	/*
	 * Make sure tip window is on top of everything else
	 * (e.g. when popupicon becomes topmost, the tips appear underneath it!)
	 * Maybe it would be better to have individual tooltip windows for each
	 * window instead of one global one?  But then how would we handle the
	 * hints?
	 */

//	SetWindowPos(d_hTT, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	POINT pt;
	pt.x = x;
	pt.y = y;

	/*
	 * If hintLine is not set, then don't waste our time
	 * If hintline is locked, then don't waste our time
	 */

	if( (d_hintLine != 0) && !d_lockHint)
	{

		// TTHITTESTINFO ttInfo;
		//
		// ttInfo.hwnd = d_hTT;
		// ttInfo.pt.x = x;
		// ttInfo.pt.y = y;
		// ttInfo.ti.cbSize = sizeof(TOOLINFO);

		/*
		 * Stupid thing, doesn't have a way of finding what item mouse is over
		 * we'll have to scan through each item ourselves!
		 *
		 * We may as well use GetWindowRect rather than faffing about
		 * sending TTN_HITTEST messages to the tooltip window.
		 *
		 * This also allows us to optimize the checks by only stepping into
		 * the item lists who's parents surround the point.
		 *
		 * Actually we don't even need to do that because the hwnd that this
		 * message is for must be one of our controls, so a simple check
		 * against HWND will work.  This isn't entirely true, because it
		 * might be a child window of one of out controls.  IsChild will
		 * return whether a window is a descendant.
		 */

		WTipItemIter listIter(&d_list);
		while(++listIter)
		{
			WTipItem* list = listIter.current();

			/*
			 * If the window is a parent of our window
			 */

			ASSERT(list != 0);
			ASSERT(hwnd != NULL);
			ASSERT(list->d_parent != NULL);

			if((list->d_parent == hwnd) || IsChild(list->d_parent, hwnd))
			{
				TipItemIter itemIter(&list->d_list);
				while(++itemIter)
				{
					TipItem* item = itemIter.current();

					/*
					 * If window is the item, or a child of it
					 * then we need go no further.
					 */

					if(item->d_control == NULL)
					{
						// Check rectangle

						if(PtInRect(&item->d_r, pt))
						{
							if(item->d_hint == TipData::Callback)
							{
#if 0
								TOOLTIPTEXT ttt;
								ttt.hdr.hwndFrom = list->d_parent;
								ttt.hdr.idFrom = item->d_id;
								ttt.hdr.code = TTN_NEEDTEXT;

								SendMessage(list->d_parent, WM_NOTIFY, (WPARAM)item->d_control, (LPARAM)(LPNMHDR)&ttt);

								ResString text(reinterpret_cast<int>(ttt.lpszText));
								d_hintLine->showHint(text);
#endif
								d_hintLine->showHint("Call back RECT Hint");
							}
							else if(item->d_hint != TipData::None)
							{
								ResString text(item->d_hint);		// Assume it is in application Instance
								d_hintLine->showHint(text.str());
							}
							return;
						}
					}
					else
            if( (item->d_control == hwnd) ||
                IsChild(item->d_control, hwnd) )
            {
              if(item->d_hint == TipData::Callback)
              {
#if 0
                TOOLTIPTEXT ttt;
                ttt.hdr.hwndFrom = list->d_parent;
                ttt.hdr.idFrom = item->d_id;
                ttt.hdr.code = TTN_NEEDTEXT;

                SendMessage(list->d_parent, WM_NOTIFY, (WPARAM)item->d_control, (LPARAM)(LPNMHDR)&ttt);

                ResString text(reinterpret_cast<int>(ttt.lpszText));
                d_hintLine->showHint(text);
#endif
                d_hintLine->showHint("Call back HWND Hint");
              }
              else if(item->d_hint != TipData::None)
              {
                ResString text(item->d_hint);		// Assume it is in application Instance
                d_hintLine->showHint(text.str());
              }
              return;
            }
				}
				// break;
			}
		}

		/*
		 * If we got here, then we're not over one of our tooltips
	 	 */

		d_hintLine->clearHint();
	}
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
