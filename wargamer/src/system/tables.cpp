/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Implementation of tables
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "tables.hpp"
#include "filecnk.hpp"
#include "idstr.hpp"
#include "misc.hpp"


TableBase::TableBase()
{
  d_width = 0;
  d_height = 0;
  d_nTables = 0;
 
  // d_chunkName = 0;
  
  d_resolution = 0;
  d_initiated = False;
}


Boolean

TableBase::init(const char* fileName, const char* chunkName, int resolution)
{
  	ASSERT(chunkName != 0);
	ASSERT(!d_initiated);
	ASSERT(fileName != 0);

	Boolean result = False;

  // d_chunkName = chunkName;

  	d_resolution = resolution;

	try
	{
		WinFileAsciiChunkReader f(fileName);
		ASSERT(f.isAscii());
		FileChunkReader fr(f, chunkName);
		if(!fr.isOK())
			throw FileError();

		/*
		 *  read in table data
		 *
		 *  If d_nTables = 1, and d_height = 1 then it is a 1D table
		 *  If d_nTables = 1 then it is a 2D table
		 *  Else 3D table
		 */

		 /*
	 	  *  Count number of tables
	 	  */

		d_nTables = (UWORD)fr.countEntries();
		if(!fr.isOK())
			throw FileError();

		/*
	 	 *   Count columns in table
	 	 */

		d_width = (UWORD)fr.countColumns();
		if(!fr.isOK())
			throw FileError();

		/*
	 	 *   Count rows in table
	 	 */

		d_height = (UWORD)fr.countRows();
		if(!fr.isOK())
			throw FileError();

		size_t size = d_width*d_height*d_nTables;

		/*
		 *  Allocate memory for table array
		 */

		ASSERT(size != 0);

		// d_table = new T[size];
		// ASSERT(d_table != 0);

		allocateTable(size);

		/*
		 *   Read in table data
		 */

		size_t i = 0;


		while(i < size)
		{
			if(fr.startItem())
			{

#ifdef DEBUG
				int h = 0;
#endif
				StringParse parse; 
				while(fr.getItem(parse))
				{

#ifdef DEBUG
					ASSERT(i < size);
					int w = 0;
#endif

					if(d_resolution)
					{
						UINT n;

						while(parse.getFixed(n, d_resolution))
						{
							ASSERT(i < size);
							// d_table[i++] = T(n);
							storeItem(i++, n);

#ifdef DEBUG
							w++;
							ASSERT(w <= d_width);
#endif
						}
					}
					else
					{
						int n;

						while(parse.getSInt(n))
						{
							ASSERT(i < size);
							// d_table[i++] = T(n);
							storeItem(i++, n);
#ifdef DEBUG
							w++;
							ASSERT(w <= d_width);
#endif
						}
					}

#ifdef DEBUG
					h++;
					ASSERT(h <= d_height);
#endif
				}

			}
		}

		result = True;

	}
	catch(const FileError& f)
	{
		throw GeneralError("Can not read Tables %s", fileName);
	}

  	d_initiated = result;
  
  	return result;
}


StringTable::~StringTable()
{
  for(int i = 0; i < d_entries; i++)
  {
	 delete[] d_table[i];
  }

  if(d_table)
	 delete[] d_table;
}

Boolean

StringTable::init(const char* fileName, const char* chunkName)
{

	ASSERT(chunkName != 0);
	ASSERT(!d_initiated);
	ASSERT(fileName != 0);

	Boolean result = False;

	try
	{
		WinFileAsciiChunkReader f(fileName);
		ASSERT(f.isAscii());
		FileChunkReader fr(f, chunkName);
		if(!fr.isOK())
			throw FileError();

		/*
		 *  read in table data
		 *
		 *  First, count strings in table
		 */

		d_entries = (UWORD)fr.countRows();
		if(!fr.isOK())
			throw FileError();


		/*
		 *  Allocate memory for table array
		 */

		d_table = new char*[d_entries];
		ASSERT(d_table != 0);

		/*
		 *   Read in table data
		 */

		int item = 0;

		if(fr.startItem())
		{

		  StringParse parse;
		  while(fr.getItem(parse))
		  {
			 ASSERT(item < d_entries);

          d_table[item] = copyString(parse);
			 item++;
		  }
		}

		result = True;

	}
	catch(const FileError& f)
	{
	  throw GeneralError("Can not read Tables %s", fileName);
	}

	d_initiated = result;

	return result;

}


#if 0
template<class T>
Tables<T>::Tables()
{
  d_width = 0;
  d_height = 0;
  d_nTables = 0;

  d_table = 0;

  d_chunkName = 0;
  d_resolution = 0;

  d_initiated = False;
}

template<class T>
Tables<T>::~Tables()
{
  if(d_table)
	 delete[] d_table;
}

template<class T>
void Tables<T>::init(const char* chunkName, int resolution)
{
  ASSERT(chunkName != 0);

  d_chunkName = chunkName;

  d_resolution = resolution;

  d_initiated = True;
}


template<class T>
Boolean Tables<T>::readData(const char* fileName)
{
	ASSERT(d_initiated == True);

	try
	{
		WinFileAsciiChunkReader f(fileName);

		ASSERT(f.isAscii());

		/*
		 *  Find chunk
		 */

		FileChunkReader fr(f, getChunkName());
		if(!fr.isOK())
		  throw FileError();

		/*
		 *  read in table data
		 *
		 *  If d_nTables = 1, and d_height = 1 then it is a 1D table
		 *  If d_nTables = 1 then it is a 2D table
		 *  Else 3D table
		 */

		/*
		 *  Count number of tables
		 */
		d_nTables = (UWORD)fr.countEntries();
		if(!fr.isOK())
		  throw FileError();

		/*
		 *   Count columns in table
		 */

		d_width = (UWORD)fr.countColumns();
		if(!fr.isOK())
		  throw FileError();

		/*
		 *   Count rows in table
		 */

		d_height = (UWORD)fr.countRows();
		if(!fr.isOK())
		  throw FileError();

		/*
		 *  Allocate memory for table array
		 */

		size_t size = d_width*d_height*d_nTables;
		ASSERT(size != 0);

		d_table = new T[size];
		ASSERT(d_table != 0);

		/*
		 *   Read in table data
		 */

		int i = 0;

		while(i < size)
		{

			if(fr.startItem())
			{
#ifdef DEBUG
				int h = 0;
#endif
				StringParse parse;

				while(fr.getItem(parse))
				{
#ifdef DEBUG
					int w = 0;
#endif

					if(d_resolution)
					{
						UINT n;

						while(parse.getFixed(n, d_resolution))
						{
							ASSERT(i < (d_width*d_height*d_nTables));
							d_table[i++] = T(n);

#ifdef DEBUG
							w++;
							ASSERT(w <= d_width);
#endif
						}
					}
					else
					{
						int n;

						while(parse.getSInt(n))
						{
							ASSERT(i < (d_width*d_height*d_nTables));
							d_table[i++] = T(n);
#ifdef DEBUG
							w++;
							ASSERT(w <= d_width);
#endif
						}
					}
#ifdef DEBUG
					h++;
					ASSERT(h <= d_height);
#endif
				}

			}
		}

		return True;

	}
	catch(FileError f)
	{
		throw GeneralError("Can not read Tables %s", fileName);
	}

}

/*
 *  return array item for 1D table
 */

template<class T>
T Table1D<T>::getValue(int i) const
{
  ASSERT(isInitiated() == True);

  ASSERT(i < getWidth());

  // make sure its a 1D table
  ASSERT(getHeight() == 1);
  ASSERT(getNTables() == 1);

  const T* t = getTable();
  ASSERT(t != 0);

  return t[i];
}


/*
 *  return array item for 2D table
 */

template<class T>
T Table2D<T>::getValue(int iy, int ix) const
{
  ASSERT(isInitiated() == True);

  ASSERT(ix < getWidth());
  ASSERT(iy < getHeight());

  // make sure its a 2D table
  ASSERT(getNTables() == 1);

  const T* t = getTable();
  ASSERT(t != 0);

  int index = (iy * getWidth()) + ix;
  ASSERT(index < getWidth()*getHeight());

  return t[index];
}



/*
 *  return array item for 3D table
 */

template<class T>
T Table3D<T>::getValue(int iz, int iy, int ix) const
{
  ASSERT(isInitiated() == True);

  ASSERT(iz < getNTables());
  ASSERT(ix < getWidth());
  ASSERT(iy < getHeight());

  const T* t = getTable();
  ASSERT(t != 0);

  int index = (iz * (getWidth()*getHeight())) + (iy * getWidth()) + ix;
  ASSERT(index < (getNTables()*getWidth()*getHeight()));

  return t[index];
}
#endif
