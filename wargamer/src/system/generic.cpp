/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Generic Window Class
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "generic.hpp"

#if 0   // Obsolete

#include "app.hpp"
#include "wind.hpp"
#include "myassert.hpp"

static const char GenericClass::s_className[] = "GenericClass";
static ATOM GenericClass::classAtom = 0;

static ATOM 

GenericClass::registerClass()
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_NOCLOSE;
		wc.lpfnWndProc = WindowBase::baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(GenericClass*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}

const char* GenericClass::className()
{
    registerClass(); 
    return s_className; 
}

// static const char*  GenericClass::className()
// {
// 	return s_className; 
// }

static const char GenericNoBackClass::s_className[] = "GenericNoBackClass";
static ATOM GenericNoBackClass::classAtom = 0;

static ATOM

GenericNoBackClass::registerClass()
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_NOCLOSE;
		wc.lpfnWndProc = WindowBase::baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(GenericNoBackClass*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = NULL;	// (HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}

const char* GenericNoBackClass::className()
{
    registerClass(); 
    return s_className; 
}

// static const char*  GenericNoBackClass::className() 
// {
// 	return s_className; 
// }

static const char GenericWindowBaseND::s_className[] = "GenericWindowBaseND";
static ATOM GenericWindowBaseND::classAtom = 0;

static ATOM GenericWindowBaseND::registerClass()
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_NOCLOSE;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(GenericWindowBaseND*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = NULL;	// (HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}


const char* GenericWindowBaseND::className()
{
    registerClass(); 
    return s_className; 
}

#endif
