/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "ctab.hpp"
#include "dib.hpp"
//#include "scenario.hpp"
#include "app.hpp"
#include "palette.hpp"
#include "wmisc.hpp"
#include "imglib.hpp"
//#include "scn_img.hpp"
// #include "scn_res.h"
#include "bargraph.hpp"
#include "dib_util.hpp"
// #include <algorithm>         // STL min/max/swap functions
#include "misc.hpp"

DrawDIBDC* CustomDrawnTab::s_tabDib = 0;
UWORD CustomDrawnTab::s_instanceCount = 0;

CustomDrawnTab::CustomDrawnTab() :
  d_bkFillDib(0),
  d_dib(0),
  d_tabImages(0), //ScenarioImageLibrary::get(ScenarioImageLibrary::CTabImages)),
  d_tabHWND(0),
  d_font(0),
  d_flags(0)
{
  s_instanceCount++;
}

CustomDrawnTab::~CustomDrawnTab()
{
  ASSERT((s_instanceCount - 1) >= 0);

  if(d_tabHWND)
  {
      clear(d_tabHWND);
      d_tabHWND = NULL;
  }

  if(--s_instanceCount == 0)
  {
	 if(s_tabDib)
		delete s_tabDib;

	 s_tabDib = 0;
  }

  if(d_dib)
	 delete d_dib;
}

LRESULT CustomDrawnTab::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
		HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);
		HANDLE_MSG(hWnd, WM_PAINT, onPaint);
//		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, onLButtonDown);
//		HANDLE_MSG(hWnd, WM_LBUTTONUP, onLButtonUp);
		HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
	 default:
		return defProc(hWnd, msg, wParam, lParam);
  }
}

void CustomDrawnTab::onDestroy(HWND hwnd)
{
  if(d_tabHWND)
  {
      clear(d_tabHWND);
      d_tabHWND = NULL;
  }
}

BOOL CustomDrawnTab::onEraseBk(HWND hwnd, HDC hdc)
{
  return TRUE;
}

void CustomDrawnTab::copyBits()
{
  const int width = d_tabRect.right - d_tabRect.left;
  const int height = d_tabRect.bottom - d_tabRect.top;

  if(d_dib == 0 ||
	  d_dib->getWidth() < width ||
	  d_dib->getHeight() < height)
  {
	 int oldCX = 0;
	 int oldCY = 0;

	 if(d_dib)
	 {
		oldCX = d_dib->getWidth();
		oldCY = d_dib->getHeight();

		delete d_dib;
		d_dib = 0;
	 }

	 d_dib = new DrawDIBDC(maximum(oldCX, width), maximum(oldCY, height));
  }

  ASSERT(d_dib);

  /*
	* copy background from screen dc
	*/

  RECT wRect;
  GetWindowRect(d_tabHWND, &wRect);

  HWND hDesk = GetDesktopWindow();
  ASSERT(hDesk);

  HDC screenDC = GetDC(hDesk);
  ASSERT(screenDC);

  BitBlt(s_tabDib->getDC(), 0, 0, width, height,
	  screenDC, wRect.left, wRect.top, SRCCOPY);

  ReleaseDC(hDesk, screenDC);

  d_dib->blit(0, 0, width, height, s_tabDib, 0, 0);
}

#if 0
void CustomDrawnTab::setValidateRegion()
{
  const int nTabs = TabCtrl_GetItemCount(hwnd);
  const int iSel = TabCtrl_GetCurSel(hwnd);
  for(int i = 0; i < nTabs; i++)
  {
	 static RECT r;
	 TabCtrl_GetItemRect(hwnd, i, &r);
  }
}
#endif

void CustomDrawnTab::getDisplayRect(RECT& destR)
{
  const int borderWidth = (d_flags & NoBorder) ? 1 : CustomBorderWindow::ThickBorder;

  /*
	* Get parents rect
	*/

  HWND hParent = GetParent(d_tabHWND);

  RECT wr;
  if(hParent)
  {
	 GetClientRect(hParent, &wr);
  }

  /*
	* Get our rect and convert to parent coord.
	*/

  RECT tr;
  GetWindowRect(d_tabHWND, &tr);

  if(hParent)
  {
	 POINT p;
	 p.x = tr.left;
	 p.y = tr.top;

	 ScreenToClient(hParent, &p);
	 SetRect(&tr, p.x, p.y, p.x + (tr.right - tr.left), p.y + (tr.bottom - tr.top));

  }

  SetRect(&destR, tr.left + borderWidth, tr.top + d_tabRect.bottom + borderWidth,
	  tr.right - borderWidth,
	  tr.bottom - borderWidth);
}

void CustomDrawnTab::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
  static POINT p;
  p.x = x;
  p.y = y;

  const int nTabs = TabCtrl_GetItemCount(hwnd);
  for(int i = 0; i < nTabs; i++)
  {
	 static RECT r;
	 TabCtrl_GetItemRect(hwnd, i, &r);

	 /*
	  * if we clicked on an item, send a WM_Notify msg to parent
	  */

	 if(PtInRect(&r, p))
	 {
		TabCtrl_SetCurSel(hwnd, i);

		ASSERT(d_dib);

		SetRect(&r, 0, 0, d_dib->getWidth(), d_dib->getHeight());
		InvalidateRect(hwnd, &r, FALSE);

		HWND hParent = GetParent(hwnd);
		ASSERT(hParent);

		NMHDR nm;
		nm.hwndFrom = hwnd;
		nm.idFrom = GetWindowLong(hwnd, GWL_ID);
		nm.code = TCN_SELCHANGE;

		SendMessage(hParent, WM_NOTIFY, nm.idFrom, reinterpret_cast<LPARAM>(&nm));
		break;
	 }

  }
}

void CustomDrawnTab::tabHWND(HWND hwnd)
{
  init(hwnd);
  d_tabHWND = hwnd;

  /*
	* get tab area rect
	*/

  RECT r;
  GetClientRect(d_tabHWND, &r);

  RECT dRect;
  TabCtrl_GetItemRect(d_tabHWND, 0, &dRect);

  /*
	* Allocate dib if not already done, or size has changed
	*/

  const int width = r.right - r.left;
  const int height = (dRect.bottom - dRect.top);
  ASSERT(width > 0);
  ASSERT(height > 0);

  DIB_Utility::allocateDib(&s_tabDib, width, height);
  ASSERT(s_tabDib);
  s_tabDib->setBkMode(TRANSPARENT);

  SetRect(&d_tabRect, 0, 0, width, height);
}

void CustomDrawnTab::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
}

void CustomDrawnTab::onPaint(HWND hwnd)
{
  /*
	* Some useful values
	*/

  int iSel = TabCtrl_GetCurSel(hwnd);
  const int nTabs = TabCtrl_GetItemCount(hwnd);

  if(d_dib)
  {
	 RECT r;

	 if(nTabs > 0)
	 {
		TabCtrl_GetItemRect(hwnd, nTabs - 1, &r);

		int w;
		int h;
		d_tabImages->getImageSize(0, w, h);

		SetRect(&r, 0, 0, r.left + w, d_dib->getHeight() + 1);
	 }
	 else
		SetRect(&r, 0, 0, d_dib->getWidth(), d_dib->getHeight() + 1);

	 InvalidateRect(hwnd, &r, FALSE);

  }

  PAINTSTRUCT ps;
  HDC hdc = BeginPaint(hwnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  if(iSel < nTabs)
  {
	 RECT r;
	 GetClientRect(hwnd, &r);
	 const int width = d_tabRect.right - d_tabRect.left;
	 const int height = d_tabRect.bottom - d_tabRect.top;
	 ASSERT(width >= 0);
	 ASSERT(height >= 0);

	 if(!d_dib)
		copyBits();

	 ASSERT(s_tabDib);
	 s_tabDib->blit(0, 0, width, height, d_dib, 0, 0);

	 /*
	  * Draw our tabs
	  *
	  * first draw unselected in reverse order
	  */

	 int i = nTabs;

	 while(i--)
	 {
		if(i != iSel)
		  drawTab(hdc, i, iSel);
	 }

	 // now draw selected tab
	 drawTab(hdc, iSel, iSel);

	 // draw border
	 if( !(d_flags & NoBorder) )
	 {
		RECT dRect = r;

		// get display rect
		TabCtrl_AdjustRect(hwnd, FALSE, &dRect);
		r.top = dRect.top - CustomBorderWindow::ThickBorder;

		CustomBorderWindow cw(d_borderColors);
		cw.drawThickBorder(hdc, r);
	 }

	 /*
	  * draw a single black border
	  */

	 else
	 {
		RECT iRect;
		TabCtrl_GetItemRect(hwnd, iSel, &iRect);

		const int borderCX = 1;
		const COLORREF cr = PALETTERGB(0, 0, 0);
		const COLORREF crw = PALETTERGB(245, 254, 241);

		// start at top corner
		int x = 0;
		int y = d_tabRect.bottom;
		int cx = r.right - r.left;
		int cy = r.bottom - r.top;

		// draw top line up to selected image
		if(iRect.left > 0)
		  Lines::putLine(hdc, x, y, iRect.left, y, borderCX, cr);

		x = iRect.left;

		// draw top line after selected image
		if(iRect.right < cx)
		{
			if(d_flags & StretchTab)
			{
				Lines::putLine(hdc, x,y, iRect.right,y, borderCX, crw);
				x = iRect.right;
			}
			else
			{
		  		int w;
		  		int h;
		  		d_tabImages->getImageSize(0, w, h);

		  		// draw line for selected
		  		Lines::putLine(hdc, x, y, x + w, y, borderCX, crw);

		  		x = iRect.left + w;
			}
		}

  		Lines::putLine(hdc, x, y, cx - 1, y, borderCX, cr);

		// left side
		x = 0;
		Lines::putLine(hdc, x, y, x, cy - 1, borderCX, cr);

		// right side
		x = cx - 1;
		Lines::putLine(hdc, x, y, x, cy - 1, borderCX, cr);

		// bottom
		x = 0;
		y = (r.bottom - r.top) - 1;
		Lines::putLine(hdc, x, y, cx - 1, y, borderCX, cr);
	 }

	 BitBlt(hdc, 0, 0, width, height, s_tabDib->getDC(), 0, 0, SRCCOPY);
  }

  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hwnd, &ps);
}

/*
 * Draw Tab
 */

void CustomDrawnTab::drawTab(HDC hdc, int id, int iSel)
{
  /*
	* Some useful values
	*/

  static RECT r;
  TabCtrl_GetItemRect(d_tabHWND, id, &r);

  const int width = r.right-r.left;
  const int height = (r.bottom-r.top);// + ThickBorder;

  if(d_font)
	 s_tabDib->setFont(d_font);

	int w;
	int h;
	int x;
	int y;

	if(d_flags & StretchTab)
	{
		const int imgNum = 0;

		w = width;
		h = height;

		x = r.left;
		y = d_tabRect.bottom - h;
		if(id == iSel)
			y += 3;

		d_tabImages->stretchBlitShadow(s_tabDib, imgNum, x,y, width,height, 5);
		if(iSel != id)
		{
			s_tabDib->darkenRectangle(x,y,width,height,25);
		}
	}
	else
	{
  		d_tabImages->getImageSize(0, w, h);

  		x = r.left; //(id == iSel) ? r.left : r.left + 3;
  		y = (id == iSel) ? (d_tabRect.bottom - h) : (d_tabRect.bottom - h)  + 3;

  		d_tabImages->blit(s_tabDib, 0, x, y, True, 5);

  		// if this isn't selected item, darken it
  		if(iSel != id)
	 		d_tabImages->darkenBlit(s_tabDib, 0, x, y, 25);
	 }

  /*
	* Get tab text
	*/

  const int bufferSize = 100;
  char text[bufferSize];

  TC_ITEM tci;
  tci.mask = TCIF_TEXT;
  tci.pszText = text;
  tci.cchTextMax = bufferSize;

  TabCtrl_GetItem(d_tabHWND, id, &tci);

  /*
	*  Write text to dib and blit
	*/

  SIZE s;
  GetTextExtentPoint32(s_tabDib->getDC(), text, lstrlen(text), &s);

  int textBorder = 7;
  int textW = w - textBorder * 2;
  int minX = x + textBorder;

  int textX = x + ((w - s.cx) / 2);
  int textY = y + ((h - s.cy) / 2);
  textX = maximum(minX, textX);

//  wTextOut(s_tabDib->getDC(), textX, textY, tci.pszText);
  wTextOut(s_tabDib->getDC(), textX, textY, textW, tci.pszText);

}

