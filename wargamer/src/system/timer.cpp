/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Timer used to keep game running
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.4  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.3  1996/02/29 11:01:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/15 10:50:34  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/11/07 10:39:01  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "timer.hpp"
#include "myassert.hpp"
#include <windows.h>
#include "except.hpp"
#if defined(USE_MM_TIMER)
#include <mmsystem.h>
#endif

/*
 * WM_TIMER style timers
 */

// static WMTimer* wmTimers = 0;			// Could be a list of timers


WMTimer::WMTimer() :
   d_id(0),
   d_tickRate(0),
   d_hWnd(NULL)
{
//	ASSERT(wmTimers == 0);			// Only one timer allowed
//	d_id = 0;
//	wmTimers = this;
}


WMTimer::~WMTimer()
{
//	ASSERT(wmTimers == this);
//	wmTimers = 0;
	stopWMTimer();
}

#if 0
VOID CALLBACK timerProc(HWND  hwnd,	UINT  uMsg,	UINT  idEvent,	DWORD  dwTime)
{
	Timer* t = timers;

	ASSERT(t != 0);

	/*
	 * Process the game
	 */

	t->procTimer();

	/*
	 * Get the timer going
	 */

	// t->initTimer();			// Set it off again
}
#endif

void

WMTimer::initWMTimer(HWND h, int tickRate)
{
	d_hWnd = h;
   d_tickRate = tickRate;

//	d_tickRate = 100;			// 100 Milliseconds
	d_id = SetTimer(d_hWnd, 0, d_tickRate, NULL);

#ifdef DEBUG
	debugLog("WMTimer: d_id=%u\n", (unsigned int) d_id);
#endif

	ASSERT(d_id != 0);
	if(d_id == 0)
		throw GeneralError("Can't initialise WMTimer");
}

void

WMTimer::stopWMTimer()
{
	if(d_id != 0)
	{
		KillTimer(d_hWnd, d_id);
		d_id = 0;
	}
}

#if defined(USE_MM_TIMER)

/*
 * Multi-Media Timer
 */

static MMTimer* mmTimers = 0;			// Could be a list of timers

MMTimer::MMTimer()
{
	ASSERT(mmTimers == 0);			// Only one timer allowed
	id = NULL;
	d_tickRate = 0;
	mmTimers = this;
}

MMTimer::~MMTimer()
{
	stopMMTimer();
}


/*
 * Multimedia Timer method
 */

void CALLBACK mmTimerProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
	MMTimer* t = mmTimers;

	// ASSERT(t != 0);

	/*
	 * Process the game
	 */

	if(t != 0)
		t->mmProcTimer();

	/*
	 * Get the timer going
	 */

	// t->initTimer();			// Set it off again
}

void MMTimer::initMMTimer()
{
	TIMECAPS tc;

	if(timeGetDevCaps(&tc, sizeof(TIMECAPS)) != TIMERR_NOERROR)
	{
		throw GeneralError("Can't get multimedia Timer Capabilities");
	}

	const UINT desiredRes = 100;		// 100 milliseconds
	UINT res = desiredRes;

	if(tc.wPeriodMin > res)
		res = tc.wPeriodMin;
	if(tc.wPeriodMax < res)
		res = tc.wPeriodMax;

#ifdef DEBUG
	debugLog("initTimer: min=%u, max=%u, res=%u\n",
		tc.wPeriodMin,
		tc.wPeriodMax,
		res);
#endif

	tickRate = res;

	MMRESULT tpResult = timeBeginPeriod(tickRate);

#ifdef DEBUG
	debugLog("Result pf timeBeginPeriod=%d\n", (int) tpResult);
#endif

	if(tpResult != TIMERR_NOERROR)
		throw GeneralError("Can not set Multimedia Timer Resolution");

	id = timeSetEvent(
		res,						/* Delay                        */
		res/4,  					/* Resolution... allow 25ms */
		mmTimerProc,			/* Callback function   */
		0,       				/* User data             */
		TIME_PERIODIC);      /* Event type (periodic) */

#ifdef DEBUG
	debugLog("MMTimer: id = %u\n", (unsigned int) id);
#endif

	ASSERT(id != NULL);

 	if(id == NULL)
		throw GeneralError("Can not initialise multimedia timer");
}

void MMTimer::stopMMTimer()
{
#ifdef DEBUG
	debugLog("stopMMTimer: id=%u, tickRate=%u\n", (unsigned int) id, (unsigned int) tickRate);
#endif

	if(id != NULL)
	{
		timeKillEvent(id);
		id = NULL;
	}

	if(tickRate != 0)
		timeEndPeriod(tickRate);
}


#endif 	// USE_MM_TIMER
