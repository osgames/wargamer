/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Load in .BMP file
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bmp.hpp"
#include "dib.hpp"
// #include "globwin.hpp"
#include "palette.hpp"
#include "winfile.hpp"
#include "myassert.hpp"

/*
 * Interal BMP Reading Functions
 */

class BMPReader : public BMP {
 public:
   static void readDIB(RawDIB* dib, const char* fileName, ReadMode mode, bool isDIB);
   static void readDIB(RawDIB* dib, HINSTANCE hinst, const char* resName, ReadMode mode, bool isDIB);
};

/*
 * Read BMP from File
 */

void BMPReader::readDIB(RawDIB* dib, const char* fileName, ReadMode mode, bool isDIB)
{
   ASSERT(fileName != 0);
   ASSERT(mode < RBMP_HowMany);

   /*
    * Open the file (Use a class so automatically closed)
    */

   WinFileBinaryReader fr(fileName);

   if(fr.getError())
   {
#ifdef DEBUG
      debugMessage("Can't open %s", fileName);
#endif
      throw BMPError();
   }

   /*
    * Read BITMAPFILEHEADER
    */

   BITMAPFILEHEADER header;
   if(!fr.read(&header, sizeof(header)))
      throw BMPError();

   /*
    * Read BITMAPINFO
    */

   BITMAPINFOHEADER bmh;
   if(!fr.read(&bmh, sizeof(bmh)))
      throw BMPError();

   /*
    * Check for valid type
    */

   if((bmh.biPlanes != 1) ||
      (bmh.biBitCount != 8) ||
      (bmh.biCompression != BI_RGB))
   {
#ifdef DEBUG
      debugMessage("%s is not 256 colour RGB Windows BMP", fileName);
#endif
      throw BMPError();
   }

   /*
    * find how many Colors there are
    */

   int nColors = bmh.biClrUsed;
   if(nColors == 0)
      nColors = (1 << bmh.biBitCount);

   ASSERT(nColors <= CPalette::MaxColors);

   /*
    * Read and Copy Colour Table
    */

   RGBQUAD pal[CPalette::MaxColors];

   // if(!fr.read(pal, sizeof(pal)))
   if(!fr.read(pal, nColors * sizeof(RGBQUAD)))
      throw BMPError();

   /*
    * create a palette
    */

   PalettePtr palette = CPalette::create(nColors, pal);


   // if((Palette::get() == NULL) || (mode == RBMP_ForcePalette) || (mode == RBMP_OnlyPalette))
   //    Palette::set(Palette::makeIdentityPalette(pal, 256));

   if((CPalette::defaultPalette() == 0) || (mode == RBMP_ForcePalette) || (mode == RBMP_OnlyPalette))
   {
      palette->makeDefault();
   }

   if(mode == RBMP_OnlyPalette)
   {
      return;
   }

   ASSERT(dib != 0);

   dib->setPalette(palette);

   /*
    * Create DIB
    */

    dib->resize(bmh.biWidth, bmh.biHeight);
//    if(isDIB)
//       dib->makeDIBSection(bmh.biWidth, bmh.biHeight);
//    else
//       dib->makeMemBitmap(bmh.biWidth, bmh.biHeight);

   /*
    * Read and Copy Bit Table
    *
    * Do it line by line so that it is right way up
    */

   PUCHAR dest = dib->getBits() + dib->getHeight() * dib->getStorageWidth();
   int y = dib->getHeight();
   while(y--)
   {
      dest -= dib->getStorageWidth();

      if(!fr.read(dest, dib->getStorageWidth()))
      {
         // delete dib;
         throw BMPError();
      }
   }

   /*
    * Remap it
    */

#if 0 // TODO: Sort out colour palettes
   dib->remap(pal);
#endif
}

/*
 * Read BMP From Resource
 */

void BMPReader::readDIB(RawDIB* dib, HINSTANCE hinst, const char* resName, ReadMode mode, bool isDIB)
{
   ASSERT(resName != 0);
   ASSERT(mode < RBMP_HowMany);

   /*
    * Load in the bitmap
    */

   HRSRC hRes = FindResource(hinst, resName, RT_BITMAP);
   ASSERT(hRes);
   if(!hRes)
      throw BMPError();
   HGLOBAL hGlobal = LoadResource(hinst, hRes);
   ASSERT(hGlobal);
   if(!hGlobal)
      throw BMPError();
   LPBITMAPINFO bm = (LPBITMAPINFO) LockResource(hGlobal);
   ASSERT(bm);
   if(!bm)
      throw BMPError();

   /*
    * Check for valid type
    */

   if((bm->bmiHeader.biPlanes != 1) ||
      (bm->bmiHeader.biBitCount != 8) ||
      (bm->bmiHeader.biCompression != BI_RGB))
   {
#ifdef DEBUG
      debugMessage("Resource %s is not 256 colour RGB Windows BMP", resName);
#endif
      throw BMPError();
   }

   /*
    * find how many Colors there are
    */

   int nColors = bm->bmiHeader.biClrUsed;
   if(nColors == 0)
      nColors = (1 << bm->bmiHeader.biBitCount);

   ASSERT(nColors <= CPalette::MaxColors);

   /*
    * create a palette
    */

   PalettePtr palette = CPalette::create(nColors, bm->bmiColors);


   // if((Palette::get() == NULL) || (mode == RBMP_ForcePalette) || (mode == RBMP_OnlyPalette))
   //    Palette::set(Palette::makeIdentityPalette(bm->bmiColors, 256));

   if((CPalette::defaultPalette() == 0) || (mode == RBMP_ForcePalette) || (mode == RBMP_OnlyPalette))
   {
      palette->makeDefault();
   }

   if(mode == RBMP_OnlyPalette)
      return;

   ASSERT(dib != 0);

   dib->setPalette(palette);

   dib->resize(bm->bmiHeader.biWidth, bm->bmiHeader.biHeight);
//    if(isDIB)
//       dib->makeDIBSection(bm->bmiHeader.biWidth, bm->bmiHeader.biHeight);
//    else
//       dib->makeMemBitmap(bm->bmiHeader.biWidth, bm->bmiHeader.biHeight);

   /*
    * Copy the image into bits
    * Image is stored upside down!!!
    * So we have to copy line by line to flip it
    */


   const UBYTE* src = reinterpret_cast<const UBYTE*>(
      bm->bmiColors + nColors);     //lint !e662 ... possible out of bounds
       // static_cast<const UBYTE*>(bm->bmiColors) +
       // nColors * sizeof(RGBQUAD));

   UBYTE* dest = dib->getBits() + dib->getHeight() * dib->getStorageWidth();
   int y = dib->getHeight();
   while(y--)
   {
     dest -= dib->getStorageWidth();
     memcpy(dest, src, dib->getStorageWidth());
     src += dib->getStorageWidth();
   }

   /*
    * Remap the images colours
    */
#if 0    // TODO: Sort out colour palettes
   dib->remap(bm->bmiColors);
#endif

   /*
    * Don't care what the SDK says, I'm going to unlock and free them
    * otherwise how can system know whether I've finished with it so
    * it can be freed from memory?
    */

   // UnlockResource(bm);
   FreeResource(hGlobal);
}


/*
 * External Functions
 */

void BMP::getPalette(const char* fileName)
{
   BMPReader::readDIB(0, fileName, RBMP_OnlyPalette, false);
}

void BMP::getPalette(HINSTANCE hinst, const char* resName)
{
   BMPReader::readDIB(0, hinst, resName, RBMP_OnlyPalette, false);
}


DIB* BMP::newDIB(const char* fileName, ReadMode mode)
{
    DIB* dib = new DIB;
   BMPReader::readDIB(dib, fileName, mode, false);
    return dib;
}

DrawDIB* BMP::newDrawDIB(const char* fileName, ReadMode mode)
{
   DrawDIB* dib = new DrawDIB;
   BMPReader::readDIB(dib, fileName, mode, false);
   return dib;
}

DIBDC* BMP::newDIBDC(const char* fileName, ReadMode mode)
{
   DIBDC* dib = new DIBDC;
   BMPReader::readDIB(dib, fileName, mode, true);
   return dib;
}

DrawDIBDC* BMP::newDrawDIBDC(const char* fileName, ReadMode mode)
{
   DrawDIBDC* dib = new DrawDIBDC;
   BMPReader::readDIB(dib, fileName, mode, true);
   return dib;
}


DIB* BMP::newDIB(HINSTANCE hinst, const char* resName, ReadMode mode)
{
   DIB* dib = new DIB;
   BMPReader::readDIB(dib, hinst, resName, mode, false);
   return dib;
}

DrawDIB* BMP::newDrawDIB(HINSTANCE hinst, const char* resName, ReadMode mode)
{
   DrawDIB* dib = new DrawDIB;
   BMPReader::readDIB(dib, hinst, resName, mode, false);
   return dib;
}

DIBDC* BMP::newDIBDC(HINSTANCE hinst, const char* resName, ReadMode mode)
{
   DIBDC* dib = new DIBDC;
   BMPReader::readDIB(dib, hinst, resName, mode, true);
   return dib;
}

DrawDIBDC* BMP::newDrawDIBDC(HINSTANCE hinst, const char* resName, ReadMode mode)
{
   DrawDIBDC* dib = new DrawDIBDC;
   BMPReader::readDIB(dib, hinst, resName, mode, true);
   return dib;
}

/*----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 * Revision 1.4  1995/11/22 10:43:46  Steven_Green
 * File Classes changed
 *
 * Revision 1.3  1995/10/25 09:52:13  Steven_Green
 * Bitmaps are top down
 *
 * Revision 1.2  1995/10/20 11:04:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/17 12:02:04  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */