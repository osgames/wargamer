/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GENGINE_HPP
#define GENGINE_HPP

#ifndef __cplusplus
#error gengine.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Graphic Engine template
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "point.hpp"

class GraphicEngine
{
	public:
		typedef Point<int> GPoint;
		typedef Rect<int> GRect;

		virtual ~GraphicEngine() = 0;
		virtual void addPoint(const GPoint& p) = 0;
		virtual void close() = 0;
};

inline GraphicEngine::~GraphicEngine() { }

#endif /* GENGINE_HPP */

