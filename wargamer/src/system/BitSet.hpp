/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BITSET_HPP
#define BITSET_HPP

#ifndef __cplusplus
#error BitSet.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Simple Set Class
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */


#include "sysdll.h"
#include "mytypes.h"
#include "myassert.hpp"


/*
 * Type will be UBYTE, UWORD or ULONG depending on maximum number of
 * entries
 */


template<class Type>
class BitSet 
{
      typedef UBYTE Index;
   public:
	   BitSet()
	   {
		   clear();
	   }

	   ~BitSet()
	   {
	   }

	   Boolean isMember(Index i) const
	   {
		   return (value & indexToMask(i)) != 0;
	   }

	   void include(Index i)
	   {
		   value |= indexToMask(i);
	   }

	   void exclude(Index i)
	   {
		   value &= ~indexToMask(i);
	   }

	   void clear()
	   {
		   value = 0;
	   }

	   void fill()
	   {
		   value = (Type) -1;		// Fill with 1's.
	   }

	   Boolean isEmpty() const 
	   {
		   return value == 0; 
	   }

	   Type get(void) { return value; }
   private:

	   Type indexToMask(Index i) const
	   {
		   ASSERT(i < (sizeof(Type) * CHAR_BIT));

		   return Type(1 << i); 
	   }

   // Private Data

	   Type value;
};


class Set8 : public BitSet<UBYTE>
{
};

class Set16 : public BitSet<UWORD>
{
};

class Set32 : public BitSet<ULONG>
{
};




#endif /* BITSET_HPP */

/*----------------------------------------------------------------------
 *
 * $Log$
 *
 * 19May2001: Renamed to BitSet to avoid confusion with stl::set<>
 *
 *----------------------------------------------------------------------
 */