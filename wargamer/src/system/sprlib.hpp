/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SPRLIB_H
#define SPRLIB_H

#ifndef __cplusplus
#error sprlib.h is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *      Sprite Library Interface
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "mytypes.h"
#include "swgspr.hpp"
#include "dib.hpp"
#include "StringPtr.hpp"

class FileReader;

namespace SWG_Sprite
{
/*
 * Define Caching Method
 */

#define SPRITECACHE_DLIST 1
#define SPRITECACHE_ARRAY 2

// #define SPRITECACHE_METHOD SPRITECACHE_DLIST
#define SPRITECACHE_METHOD SPRITECACHE_ARRAY

#if !defined(SPRITECACHE_METHOD)        || (SPRITECACHE_METHOD==0)
#error "SPRITECACHE_METHOD Not Defined"
#endif

/*
 * Forward References
 */

class SpriteLibrary;
class SpriteCacheList;
class PaletteCacheList;

class SYSTEM_DLL SpriteBlock : public DIB
{
        friend class SpriteLibrary;
        friend class SpriteCache;
        friend class SpriteCacheList;

#if SPRITECACHE_METHOD==SPRITECACHE_DLIST
                SpriteLibrary* d_library;
#elif SPRITECACHE_METHOD==SPRITECACHE_ARRAY
                mutable UBYTE d_refCount;
#else
                #error "SPRITECACHE_METHOD Undefined"
#endif
                UWORD d_fullWidth;
                UWORD d_fullHeight;
                UWORD d_xOffset;
                UWORD d_yOffset;
                SWORD d_anchorX;
                SWORD d_anchorY;
                UBYTE d_shadowColor;
                SpriteFlags d_flags;

        private:
                SpriteBlock(UWORD w, UWORD h, SpriteLibrary* l);
                ~SpriteBlock();
        public:
                void release() const;

                /*
                 * Utility Functions
                 */

                void blitTo(DrawDIB* dest, const PixelPoint& p) const;
                        // Blit sprite to a DIB using anchor, mask, etc

                PixelPoint topLeft(const PixelPoint& pos) const
                {
                        return PixelPoint(pos.getX() - d_anchorX, pos.getY() - d_anchorY);
                }

                PixelPoint anchor() const
                {
                        return PixelPoint(d_anchorX, d_anchorY);
                }

                COLORREF averageColor() const;

                /*
                 * Accessors
                 */

                int width() const { return getWidth(); }                // DIB::getWidth()
                int height() const { return getHeight(); }      // DIB::getHeight()

                PixelPoint fullSize() const { return PixelPoint(d_fullWidth, d_fullHeight); }

					 bool hasShadow() const { return d_flags.shadow; }
					 UBYTE shadowColor() const { ASSERT(d_flags.shadow); return d_shadowColor; }

#if SPRITECACHE_METHOD==SPRITECACHE_ARRAY
//        private:
        public:
                UBYTE addRef() const { ASSERT(d_refCount < UBYTE_MAX); return ++d_refCount; }
                UBYTE delRef() const { ASSERT(d_refCount != 0); return --d_refCount; }
                UBYTE refCount() const { return d_refCount; }
#endif


                // int adjustX(unsigned int x) const { return x - d_anchorX; }
                // int adjustY(unsigned int y) const { return y - d_anchorY; }
};


class SYSTEM_DLL SpriteLibrary {
                friend class SpriteBlock;

        public:
                typedef int Index;
                enum { NoGraphic = -1 };

                typedef int PaletteIndex;
        private:

                CString d_fileName;

                // ifstream fileHandle;
                FileReader* d_file;

                UWORD   d_spriteCount;                  // Number of sprites in file
                ULONG*  d_offsets;                              // File offsets
                UWORD           d_paletteCount;
                ULONG*  d_palOffsets;

                SpriteCacheList*        d_cache;
                PaletteCacheList*       d_palCache;

        public:
                SpriteLibrary(const char* name);
                ~SpriteLibrary();

                const SpriteBlock* read(Index n);

                int imageCount() const { return d_spriteCount; }

        private:
                void release(const SpriteBlock* sprite);
                // void release(SpriteIndex n);

                // void drawSprite(Region* bm, Point where, SpriteIndex i);
                // void drawAnchoredSprite(Region* bm, Point where, SpriteIndex i);
                // void drawCentredSprite(Region* bm, SpriteIndex i);
};


class SYSTEM_DLL SpriteBlockPtr
{
                friend class SpriteBlock;
                const SpriteBlock* d_sprite;

                // Disable Copy...

                SpriteBlockPtr(const SpriteBlockPtr&);
                SpriteBlockPtr& operator = (const SpriteBlockPtr&);


        public:
                SpriteBlockPtr(SpriteLibrary* lib, SpriteLibrary::Index n) :
                        d_sprite(0)
                {
                        d_sprite = lib->read(n);
                }

                SpriteBlockPtr(const SpriteBlock* sprite) :
                        d_sprite(sprite)
                {
                        sprite->addRef();
                }

                ~SpriteBlockPtr()
                {
                        if(d_sprite)
                                d_sprite->release();
                }

                operator const SpriteBlock* () const { return d_sprite; }

                const SpriteBlock* operator -> () const { return d_sprite; }

};


};              // namespace SWG_Sprite

#endif /* SPRLIB_H */

