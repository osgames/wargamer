/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef SOUNDSYS_HPP
#define SOUNDSYS_HPP

/*

        SOUND.HPP
        Low level sound system for both Campaign & Battle Games
        Provides access to the DirectSound & DirectSoundBuffer interfaces

*/




#include "sysdll.h"
#include <windows.h>
#include <mmsystem.h>
#include <dsound.h>

#if !defined(EXPORT_SYSTEM_DLL)
#else
#endif



#ifndef DSBCAPS_CTRLDEFAULT
#define DSBCAPS_CTRLDEFAULT 0xe0
#endif



class SoundSystem {

private:

    // application top window handle
    HWND app_hwnd;
    // flag set if DirectSound setup Ok
    bool SetupOK;
    // direct sound object pointer
    LPDIRECTSOUND lpDirectSound;

   DWORD CDDeviceID;

    // flag set if MCI CDAudio setup Ok
    bool CDAudioSetupOK;
    // flag determines wether to fade CD tracks in & out
    bool PlayingPlayList;

    // list of track to play
    int PlayList[64];
    // how many tracks are in play list
    int NumTracks;
    // number of current track
    int CurrentTrack;
    // frequency with which to check if track is playing / move to next one
    int PlayCheckFrequency;
    int PlayCheckCounter;


public:
    SYSTEM_DLL SoundSystem();
    SYSTEM_DLL ~SoundSystem();

    SYSTEM_DLL void Init(HWND hwnd);
    SYSTEM_DLL HWND GetHWND() { return app_hwnd; }
    SYSTEM_DLL bool IsSetupOK() { return SetupOK; }
    SYSTEM_DLL LPDIRECTSOUND GetDirectSound() { return lpDirectSound; }

    SYSTEM_DLL bool SetPlayList(int num, int tracks...);
    SYSTEM_DLL bool SetRandomPlayList(int num, int tracks...);
    SYSTEM_DLL bool ProcessPlayList();
    SYSTEM_DLL bool StartPlayList(bool interrupt_current);
    SYSTEM_DLL bool StopPlayList();

//private:
// it's actually quite handy to have access to this stuff
    bool InitDirectSound();
    void ShutDownDirectSound();
    void DirectSoundError(HRESULT err);

    bool OpenCDAudio();
    bool CloseCDAudio();

    SYSTEM_DLL bool IsMediaPresent();
    SYSTEM_DLL bool PlayCDTrack(unsigned int trackno);
    SYSTEM_DLL bool StopCDPlaying();
    SYSTEM_DLL bool IsCDPlaying();
    SYSTEM_DLL bool IsCDTrackPlaying(unsigned int trackno);




    void MCIError(MCIERROR mcierr);



};

// global pointer
extern SYSTEM_DLL SoundSystem GlobalSoundSystemObj;





#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
