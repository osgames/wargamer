/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Find File Iterator
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "fileiter.hpp"
#include "myassert.hpp"

FindFileIterator::FindFileIterator(const char* wild) :
	d_handle(NULL),
	d_valid(false)
{
	ASSERT(wild != 0);
	HANDLE h = FindFirstFile(wild, &d_data);
	if(h != INVALID_HANDLE_VALUE)
	{
		d_handle = h;
		d_valid = true;
	}
}

FindFileIterator::~FindFileIterator()
{
	if(d_handle != NULL)
		FindClose(d_handle);
}

const char* FindFileIterator::name()
{
	return d_data.cFileName;
}

bool FindFileIterator::operator++()
{
	ASSERT(d_valid);
	ASSERT(d_handle);
	if(!FindNextFile(d_handle, &d_data))
	{
		d_valid = false;
	}
	return d_valid;
}

