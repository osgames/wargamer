/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Debug Window for displaying 256 colour palette
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"

#include "palwin.hpp"
#include "app.hpp"
#include "dib.hpp"
#include "registry.hpp"
#include "resdef.h"
#include "wmisc.hpp"
#include "palette.hpp"
#include "myassert.hpp"

const char PalWindow::s_className[] = "PalWind";
static const char palWindowRegName[] = "palWindow";
ATOM PalWindow::classAtom = 0;
static WSAV_FLAGS s_saveFlags = WSAV_POSITION | WSAV_SIZE | WSAV_RESTORE | WSAV_MINIMIZE;


ATOM PalWindow::registerClass()
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_DBLCLKS;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(PalWindow*);
		wc.hInstance = APP::instance();
		wc.hIcon = LoadIcon(APP::instance(), MAKEINTRESOURCE(ICON_MAPWIN));
		wc.hCursor = LoadCursor(NULL, IDC_CROSS);
		wc.hbrBackground = NULL;
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}

// HWND makePalWindow(HWND parent)
PalWindow::PalWindow(HWND parent)	// MapWindow* parent)
{
	hdc = NULL;
	dib = NULL;

    registerClass();

	ASSERT(parent != NULL);
	// ASSERT(parent != 0);

	// mapWindow = parent;

	registerClass();

//	Boolean show = False;
//
// 	RECT r;
// 	if(!getRegistry(palWindowRegName, &r, sizeof(RECT), "\\palwin\\rect"))
// 	  show = True;
//
// 	POINT p;
// 	if(getRegistry(palWindowRegName, &p, sizeof(POINT), "\\palwin\\screen"))
// 	  checkResolution(r, p);
//

	HWND hWnd = createWindow(
		WS_EX_CONTEXTHELP,
		// getClassName(),
		"Palette",
			// WS_CHILD |
			WS_POPUP |
			WS_CAPTION |
			WS_BORDER |
			WS_SYSMENU |
			// WS_VISIBLE |
			WS_MINIMIZEBOX |
			WS_CLIPSIBLINGS,
		300,
		50,
		200, // 128,
		200, // 128,
		parent,		// ->getHWND(),
		NULL			// (HMENU) WID_PALETTE,
		// APP::instance()
	);

	ASSERT(hWnd != NULL);

	SetWindowText(hWnd, "Palette");

   getState(palWindowRegName, s_saveFlags);

// 	if(!show)
// 	  SetWindowPos(hWnd, HWND_TOP, r.top, r.top, r.right, r.bottom, SWP_SHOWWINDOW);
// 	else
// 	  ShowWindow(hWnd, SW_SHOW);
}

BOOL PalWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
	debugLog("PalWindow::onCreate(%p)\n", hWnd);

	hdc = GetDC(hWnd);

	alterWindowSize(hWnd, 128, 128, lpCreateStruct);
	dib = new DrawDIBDC(128, 128);

	/*
	 * Set the bits to something
	 */

	PUCHAR p = dib->getBits();

	const int BoxSize = 8;
	int cPerRow = dib->getWidth() / BoxSize;

	int i = dib->getHeight();
	while(i--)
	{
		PUCHAR p1 = p;

		if((i % BoxSize) == 0)
			memset(p, 0, dib->getStorageWidth());
		else if( (i % BoxSize) == (BoxSize - 1) )
			memset(p, 255, dib->getStorageWidth());
		else
		for(int j = 0; j < cPerRow; j++)
		{
			UCHAR col = UCHAR ((i / BoxSize) * cPerRow + j);

			*p1++ = 0;
			memset(p1, col, BoxSize - 2);
			p1 += BoxSize - 2;
			*p1++ = 255;
		}

		p += dib->getStorageWidth();
	}

	// ASSERT(globWin.palWind == 0);
	// globWin.palWind = this;
	// globWin.palWindUpdated();

	debugLog("PalWindow::onCreate()... End\n");

	return TRUE;
}


void PalWindow::onDestroy(HWND hWnd)
{
#ifdef DEBUG
	debugLog("PalWindow::onDestroy()\n");
#endif
//	saveWindowState(palWindowRegName, hWnd, WSAV_POSITION|WSAV_MINMAX, "\\palwind");

   saveState(palWindowRegName, s_saveFlags);

// 	RECT r;
// 	GetWindowRect(hWnd, &r);
// 	fillRECT(r, r.left, r.top, r.right-r.left, r.bottom-r.top);
// 	setRegistry(palWindowRegName, &r, sizeof(RECT), "\\palwin\\rect");
//
// 	POINT p;
// 	p.x = GetSystemMetrics(SM_CXSCREEN);
// 	p.y = GetSystemMetrics(SM_CYSCREEN);
// 	setRegistry(palWindowRegName, &p, sizeof(POINT), "\\palwin\\screen");

	// globWin.palWind = 0;
	// globWin.palWindUpdated();

	// HWND parent = GetParent(hWnd);
	// ASSERT(parent != NULL);
	// FORWARD_WM_PARENTNOTIFY(parent, WM_DESTROY, hWnd, 0, SendMessage);

	// ASSERT(mapWindow != 0);

	// mapWindow->removePalWindow();

	if(dib)
		delete dib;

	if(hdc)
	{
		ReleaseDC(hWnd, hdc);
		hdc = NULL;
	}

	// delete this;
#ifdef DEBUG
	debugLog("PalWindow::onDestroy()... End\n");
#endif
}

void PalWindow::onPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	BeginPaint(hWnd, &ps);

	RECT cRect;
	GetClientRect(hWnd, &cRect);

	HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
	RealizePalette(hdc);

	// Copy the DIB onto screen

	// DeviceContext dcDIB;
	// SelectObject(dcDIB, dib->getHandle());

	HDC dcDIB = dib->getDC();

	BitBlt(hdc, 0,0, dib->getWidth(), dib->getHeight(), dcDIB, 0,0, SRCCOPY);
	EndPaint(hWnd, &ps);
}

// void PalWindow::onClose(HWND hwnd)
// {
//     FORWARD_WM_PARENTNOTIFY(GetParent(hwnd), WM_CLOSE, hwnd, 0, SendMessage);
// }


LRESULT PalWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	LRESULT l;
	if(handlePalette(hWnd, msg, wParam, lParam, l))
		return l;

	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	 onCreate);
		HANDLE_MSG(hWnd, WM_DESTROY, onDestroy);
		HANDLE_MSG(hWnd, WM_PAINT,	 onPaint);
        // HANDLE_MSG(hWnd, WM_CLOSE,   onClose);
	default:
		return defProc(hWnd, msg, wParam, lParam);
	}
}



