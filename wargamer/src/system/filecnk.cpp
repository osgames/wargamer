/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	File Chunk
 * This is a method of combining lots of data into a single file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:39  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "filecnk.hpp"
#include "idstr.hpp"

/*
 * Indentifiers for Chunk Files:
 *
 * File always starts with:
 *			ULONG headerLength;
 *			UWORD version;
 *			char[] ChunkFileId
 *
 * A check for the ID at offset 6 is always 1st
 */

const char ChunkFileID[]				= "SWG_ChunkFile";
static const UWORD DChunkFileVersion = 0x0000;

/*
 * Identifier for end of File in asciimode
 */

const char  endChunkName[]				= "End";
const char chStartChunk = '[';
const char chEndChunk = ']';

const char chStartEntry = '{';
const char chEndEntry = '}';
const char chComment = '#';

/*===============================================================
 * Overall File Classes
 */

ChunkFileError::ChunkFileError()
{
#ifdef DEBUG
	throw GeneralError("ChunkFileError");
#endif
}

/*
 * Binary Writer
 */


WinFileBinaryChunkWriter::WinFileBinaryChunkWriter(LPCSTR name) :
	WinFileBinaryWriter(name)
{
	/*
	 * Write the header
	 */

	ULONG headerLength = 0;
	putULong(headerLength);			// We'll go back and rewrite this
	putUWord(DChunkFileVersion);
	write(ChunkFileID, sizeof(ChunkFileID));
	SeekPos endPos = getPos();
	headerLength = endPos;
	seekTo(0);
	putULong(headerLength);
	seekTo(endPos);
}


WinFileBinaryChunkWriter::~WinFileBinaryChunkWriter()
{
    // write an endChunk

    FileChunkWriter fc(*this, endChunkName);
}

/*
 * Ascii Writer
 */


WinFileAsciiChunkWriter::WinFileAsciiChunkWriter(LPCSTR name) :
	WinFileAsciiWriter(name)
{
	// Note that this printf is FileBase::printf rather than stdio.

	printf("%s %u.%u\r\n",
		ChunkFileID,
		(unsigned int) HIBYTE(DChunkFileVersion),
		(unsigned int) LOBYTE(DChunkFileVersion));
}


WinFileAsciiChunkWriter::~WinFileAsciiChunkWriter()
{
}

/*
 * Binary Reader
 */


WinFileBinaryChunkReader::WinFileBinaryChunkReader(LPCSTR name) :
	WinFileBinaryReader(name)
{
	/*
	 * Read header
	 */

	getULong(headerLength);
	UWORD version;
	getUWord(version);
	ASSERT(version == DChunkFileVersion);

	char id[sizeof(ChunkFileID)];
	read(id, sizeof(ChunkFileID));

	if(memcmp(id, ChunkFileID, sizeof(ChunkFileID)) != 0)
	{
		throw ChunkFileError();
	}
}


WinFileBinaryChunkReader::~WinFileBinaryChunkReader()
{
}

SeekPos WinFileBinaryChunkReader::skipHeader()
{
	seekTo(headerLength);
	return headerLength;
}

/*
 * Ascii Reader
 */


WinFileAsciiChunkReader::WinFileAsciiChunkReader(LPCSTR name) :
	WinFileAsciiReader(name)
{
}


WinFileAsciiChunkReader::~WinFileAsciiChunkReader()
{
}


char* WinFileAsciiChunkReader::readLine()
{
//	if(!isOK())
//      return 0;

	char* s;
	do
	{
		s = WinFileAsciiReader::readLine();

// 		if(!isOK())
// 			return 0;

	} while(s && (s[0] == chComment) || (s[0] == 0) );

	return s;
}

/*
 * File Chunk Writer
 * You create one of these when about to add a chunk to a file
 * Destruct it at end.
 */


FileChunkWriter::FileChunkWriter(FileWriter& file, const char* name) : f(file)
{
	pos = 0;

	if(f.isAscii())
	{
		f.printf("[%s]\r\n", name);
	}
	else
	{
		pos = f.getPos();
		f.putULong(0);
		f.putString(name);
	}
}


FileChunkWriter::~FileChunkWriter()
{
	if(f.isAscii())
	{
		;
	}
	else
	{
		SeekPos endPos = f.getPos();
		ULONG length = endPos - pos;
		f.seekTo(pos);
		f.putULong(length);
		f.seekTo(endPos);
	}
}

/*
 * Look for chunk in a file and seek to correct place.
 * Application uses isOK() to check if succesful
 */



FileChunkReader::FileChunkReader(FileReader& file, const char* name) :
	f(file)
{
	if(file.isAscii())
	{
		ok = False;

		file.seekTo(0);			// Start at beginning

		for(;;)
		{
			char* line = file.readLine();

			if(line == 0)
				return;

			if(line[0] == chStartChunk)
			{
				line++;
				char* s = strchr(line, chEndChunk);
				ASSERT(s != 0);
				if(s)
					*s = 0;

				if(stricmp(line, name) == 0)
				{
					ok = True;
					return;
				}

				if(stricmp(line, endChunkName) == 0)
					return;
			}
		}
	}
	else
	{
		ok = False;

		WinFileBinaryChunkReader& bcr = (WinFileBinaryChunkReader&) file;		// type change!
		SeekPos pos = bcr.skipHeader();

		ULONG l;

		for(;;)
		{
			if(!file.getULong(l))
				return;

			const char* s = file.getString();

			if(s == 0)
				return;

			if(strcmp(s, name) == 0)
			{
				ok = True;
				return;
			}

			if(strcmp(s, endChunkName) == 0)
				return;

			pos += l;

			file.seekTo(pos);
		}
	}
}

/*
 * Count the nunber of { ... } until the next [...]
 */

int

FileChunkReader::countEntries()
{
	ASSERT(f.isAscii());

	int n = 0;
	int nest = 0;

	SeekPos pos = f.getPos();

	for(;;)
	{
		char* s = f.readLine();

		ASSERT(s != 0);

		if(!s)
			return -1;

		if(*s == chStartEntry)
			nest++;
		else if(*s == chEndEntry)
		{
			if(--nest == 0)
				n++;
			ASSERT(nest >= 0);
		}
		else if(*s == chStartChunk)
			break;

		ASSERT(f.isOK());
	}

	ASSERT(nest == 0);

	f.seekTo(pos);

	return n;
}

/*
 *  Count number of columns in a table.
 *  It is assumed there are the same number of columns in each row
 */

int FileChunkReader::countColumns()
{
	ASSERT(f.isAscii());

	int nest = 0;
	int n = 0;

	SeekPos pos = f.getPos();

	for(;;)
	{
		char* s = f.readLine();

		ASSERT(s != 0);

		if(!s)
			return -1;

		if(*s == chStartEntry)
			nest++;
		else if(*s != chEndEntry)
		{
			if(--nest == 0)
			{
				StringParse parse;
				parse.init(s);

				while(parse.getToken())
				  n++;
			}
			ASSERT(nest == 0);
			break;

		}
#ifdef DEBUG
		else
		  FORCEASSERT("Table line not found");
#endif

		ASSERT(f.isOK());
	}

	ASSERT(nest == 0);

	f.seekTo(pos);

	return n;
}

/*
 *  Count number of rows in table.
 */

int FileChunkReader::countRows()
{
	ASSERT(f.isAscii());

	int n = 0;
	int nest = 0;

	SeekPos pos = f.getPos();

	for(;;)
	{
		char* s = f.readLine();

		ASSERT(s != 0);

		if(!s)
			return -1;

		if(*s == chStartEntry)
			nest++;
		else if(*s == chEndEntry)
		{
		  nest--;
		  break;
		}
		else
		  n++;

		ASSERT(f.isOK());
	}

	ASSERT(nest == 0);

	f.seekTo(pos);

	return n;
}

/*
 * start Item, basically just checks for a {
 */

Boolean

FileChunkReader::startItem()
{
	char* s = f.readLine();

	ASSERT(s != 0);
	ASSERT(*s == chStartEntry);

	if(s && (*s == chStartEntry))
		return True;
	else
		return False;			// Or throw something?
}

/*
 * Complex function, which reads a line and parses it for the 1st token
 */

Boolean

FileChunkReader::getItem(int& token, StringParse& parse, const MessagePair* mp)
{
	for(;;)
	{
		char* s = f.readLine();

		ASSERT(s != 0);

		if(s)
		{
			if(*s == chEndEntry)
				return False;

			parse.init(s);

			char* tok = parse.getToken();
			ASSERT(tok != 0);

			if(mpGetID(mp, tok, token))
				return True;
#ifdef DEBUG
			else
				debugLog("Ignoring token %s\n", tok);
#endif
			// else loop around....
		}
		else
		{
			return False;		// this could throw an error
		}
	}
}

/*
 * Function for table readers, reads one line at a time
 */

Boolean FileChunkReader::getItem(StringParse& parse)
{
	char* s = f.readLine();

	ASSERT(s != 0);

	if(s)
	{
		if(*s == chEndEntry)
			return False;

		parse.init(s);
      return True;
	}
	else
	{
			return False;
	}
}

