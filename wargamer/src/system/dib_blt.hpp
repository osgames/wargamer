/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef DIB_BLT_HPP
#define DIB_BLT_HPP

#ifndef __cplusplus
#error dib_blt.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Utility for stretching DIBs into other DIBs
 *
 *----------------------------------------------------------------------
 */

#include "sysdll.h"
#include "point.hpp"

class DrawDIB;
class DIB;

namespace DIB_Utility
{
	SYSTEM_DLL void stretchDIB(DrawDIB* destDIB, LONG destX, LONG destY, ULONG destW, ULONG destH, const DIB* srcDIB, LONG srcX, LONG srcY, ULONG srcW, ULONG srcH);
	SYSTEM_DLL void stretchDIBNoMask(DrawDIB* destDIB, LONG destX, LONG destY, ULONG destW, ULONG destH, const DIB* srcDIB, LONG srcX, LONG srcY, ULONG srcW, ULONG srcH);
	SYSTEM_DLL void stretchDIBMask(DrawDIB* destDIB, LONG destX, LONG destY, ULONG destW, ULONG destH, const DIB* srcDIB, LONG srcX, LONG srcY, ULONG srcW, ULONG srcH);

	SYSTEM_DLL void stretchFit(DrawDIB* destDIB, LONG destX, LONG destY, LONG destW, LONG destH, const DIB* src);
};

#endif /* DIB_BLT_HPP */

