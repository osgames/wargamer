/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 *
 *      Sprite Library Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "sprlib.hpp"
#include "makename.hpp"
#include "winfile.hpp"
#include "ptrlist.hpp"
#include "lzhuf.h"
#include "sllist.hpp"
#include "refptr.hpp"
#include "palette.hpp"

namespace SWG_Sprite
{



/*==========================================================
 * Sprite List Memory Exhaustion Handlers
 */

class SpriteCacheList;


class SpriteMemHandle {
        PtrDList<SpriteCacheList> libs;                 // List of open libraries
        Boolean initialised;
public:
        SpriteMemHandle() { initialised = False; }
        ~SpriteMemHandle();

        void add(SpriteCacheList* lib);
        void remove(SpriteCacheList* lib);

        Boolean freeUp(size_t s);
};

static SpriteMemHandle memHandle;

/*==========================================================
 * Sprite List class
 *
 * This needs updating to actually check whether things already
 * exist in the cache and reuse them if so.
 */


#if SPRITECACHE_METHOD==SPRITECACHE_DLIST

class SpriteCache {
friend class SpriteCacheList;
        SpriteLibrary::Index index;
        SpriteBlock* sprite;
        UBYTE used;
public:
        SpriteCache(SpriteLibrary::Index n, SpriteBlock* data)
        {
                index = n;
                sprite = data;
                used = 1;
        }
        ~SpriteCache() { delete sprite; }

        void operator ++() { used++; }
        UBYTE operator --() { used--; return used; }
};

class SpriteCacheList : public PtrDList<SpriteCache> {
public:
        SpriteCacheList() { memHandle.add(this); }
        ~SpriteCacheList() { clearAndDestroy(); memHandle.remove(this); }

        void add(SpriteLibrary::Index n, SpriteBlock* data);
        void free(SpriteLibrary::Index n);
        void free(const SpriteBlock* data);

        const SpriteBlock* findAndLock(SpriteLibrary::Index n);

        Boolean flush();                // Flush out unused entries

#ifdef DEBUG
        void check();
#endif
};



#ifdef DEBUG

/*
 * Check cache for any unreleased images
 */

void SpriteCacheList::check()
{
#if 0
        if(memLog && memLog->logFile)   // (memLog->mode != MemoryLog::None))
        {
                *memLog->logFile << "\nChecking Sprites\n";

                PtrDListIter<SpriteCache> iter = *this;

                Boolean found = False;

                while(++iter)
                {
                        SpriteCache* cache = iter.current();

                        if(cache->used)
                        {
                                *memLog->logFile << "  Image " << cache->index << " is in used " << (int)cache->used << " times\n";
                                found = True;
                        }
                }

                if(!found)
                        *memLog->logFile << "  No Unfreed images\n";

                *memLog->logFile << endl;
        }
#endif
}

#endif

/*
 * FLush out unused sprite entries
 *
 * This could be made more complex by freeing items in a certain order
 * until enough memory has been freed.
 */

Boolean SpriteCacheList::flush()
{
        Boolean freedSomething = False;

        PtrDListIter<SpriteCache> iter = *this;

        while(++iter)
        {
                SpriteCache* cache = iter.current();

                if(!cache->used)
                {
                        iter.remove();
                        delete cache;
                        freedSomething = True;
                }
        }

        return freedSomething;
}

void SpriteCacheList::add(SpriteLibrary::Index n, SpriteBlock* data)
{
        append(new SpriteCache(n, data));
}

/*
 * See if sprite is already in cache
 * If so, then increment its used count and return it
 */

SpriteBlock* SpriteCacheList::findAndLock(SpriteLibrary::Index n)
{
        if(entries())
        {
                PtrDListIter<SpriteCache> iter = *this;

                while(++iter)
                {
                        SpriteCache* cache = iter.current();

                        if(cache->index == n)
                        {
                                cache->used++;

                                /*
                                 * Should move it to the front of the queue so that
                                 * it is not deleted
                                 */

                                return cache->sprite;
                        }
                }
        }

        return 0;
}

void SpriteCacheList::free(SpriteLibrary::Index n)
{
        if(entries())
        {
                PtrDListIter<SpriteCache> iter = *this;

                while(++iter)
                {
                        SpriteCache* cache = iter.current();

                        if(cache->index == n)
                        {
                                if(!--cache->used)
                                {
#if 0   // Leave it in the cache!
                                        delete cache->sprite;
                                        iter.remove();
                                        delete cache;
#endif
                                }
                                return;
                        }
                }
        }
}

void SpriteCacheList::free(const SpriteBlock* data)
{
        if(entries())
        {
                PtrDListIter<SpriteCache> iter = *this;

                while(++iter)
                {
                        SpriteCache* cache = iter.current();

                        if(cache->sprite == data)
                        {
                                if(!--cache->used)
                                {
#if 0
                                        delete cache->sprite;
                                        iter.remove();
                                        delete cache;
#endif
                                }
                                return;
                        }
                }
        }
}

#elif SPRITECACHE_METHOD==SPRITECACHE_ARRAY

class SpriteCache {
        friend class SpriteCacheList;

        SpriteBlock* d_sprite;
public:
        SpriteCache() :
                d_sprite(0)
        {
        }

        ~SpriteCache() { delete d_sprite; }

        void set(SpriteBlock* sprite)
        {
                ASSERT(d_sprite == 0);          // Adding it twice!
                if(d_sprite)
                        free();
                d_sprite = sprite;
                d_sprite->addRef();     // d_refCount = 1;
        }

        void free()
        {
                ASSERT(d_sprite != 0);
                if(d_sprite)
                {
                        delete d_sprite;
                        d_sprite = 0;
                }
        }

        const SpriteBlock* get() { return d_sprite; }

#if 0
        UBYTE operator ++()
        {
                ASSERT(d_sprite != 0);
                return d_sprite->addRef();
        }

        UBYTE operator --()
        {
                ASSERT(d_sprite != 0);
                return d_sprite->delRef();
        }
#endif

        bool canFree() const
        {
                return (d_sprite != 0) && (d_sprite->refCount() == 0);
        }
};

class SpriteCacheList // : public PtrDList<SpriteCache> {
{
        int d_size;
        SpriteCache* d_entries;
public:
        SpriteCacheList(int size);
        ~SpriteCacheList();

        void add(SpriteLibrary::Index n, SpriteBlock* data);
        void free(SpriteLibrary::Index n);
        void free(const SpriteBlock* data);

        const SpriteBlock* findAndLock(SpriteLibrary::Index n);

        Boolean flush();                // Flush out unused entries

#ifdef DEBUG
        void check();
#endif
};

SpriteCacheList::SpriteCacheList(int size)
{
        d_size = size;
        d_entries = new SpriteCache[size];
        memHandle.add(this);
}

SpriteCacheList::~SpriteCacheList()
{
        memHandle.remove(this);
        delete[] d_entries;
}

inline void SpriteCacheList::add(SpriteLibrary::Index n, SpriteBlock* data)
{
        ASSERT(n < d_size);
        d_entries[n].set(data);
}

inline void SpriteCacheList::free(SpriteLibrary::Index n)
{
        ASSERT(n < d_size);
        // --d_entries[n];
        ASSERT(d_entries[n].get() != 0);
        d_entries[n].get()->delRef();
}

void SpriteCacheList::free(const SpriteBlock* data)
{
        ASSERT(data != 0);
        data->delRef();
}

const SpriteBlock* SpriteCacheList::findAndLock(SpriteLibrary::Index n)
{
        ASSERT(n < d_size);
        const SpriteBlock* sprite = d_entries[n].get();
        if(sprite == 0)
                return 0;
        else
        {
                // ++d_entries[n];
                sprite->addRef();
                return sprite;
        }
}

Boolean SpriteCacheList::flush()
{
        bool flushed = false;

        for(int n = 0; n < d_size; ++n)
        {
                if(!d_entries[n].canFree())
                {
                        d_entries[n].free();
                        flushed = true;
                }
        }
        return flushed;
}

#ifdef DEBUG
void SpriteCacheList::check()
{
}
#endif

#else
#error "SPRITECACHE_METHOD Not Defined"
#endif

/*===================================================================
 * Palette Cache
 */

class PaletteCache : public SLink, public RefBaseCount
{
                SpriteLibrary::PaletteIndex d_index;
                PalettePtr d_palette;
                // UWORD d_nColors;
                // RGBQUAD* d_colors;
        public:
                PaletteCache(SpriteLibrary::PaletteIndex index, UWORD nColors, RGBQUAD* colors) :
                        d_index(index),
                        d_palette(CPalette::create(nColors, colors))
                        // d_nColors(nColors),
                        // d_colors(colors)
                {
                }

                ~PaletteCache();

                SpriteLibrary::PaletteIndex index() const { return d_index; }
                // int nColors() const { return d_nColors; }
                // const RGBQUAD* rgbQuad() const { return d_colors; }
                const PalettePtr& palette() const { return d_palette; }
};

class PaletteCacheList : public SList<PaletteCache>
{
        public:
                PaletteCacheList() : SList<PaletteCache>() { }
                ~PaletteCacheList() { }

                const PaletteCache* get(SpriteLibrary::PaletteIndex i);
                void release(const PaletteCache* pal);
};

/*
 * Palette Cache functions
 */

PaletteCache::~PaletteCache()
{
        // delete[] d_colors;
}

const PaletteCache* PaletteCacheList::get(SpriteLibrary::PaletteIndex i)
{
        SListIter<PaletteCache> iter(this);
        while(++iter)
        {
                const PaletteCache* pal = iter.current();
                if(pal->index() == i)
                {
                        pal->addRef();
                        return pal;
                }
        }
        return 0;
}

void PaletteCacheList::release(const PaletteCache* pal)
{
        pal->delRef();
}

/*
 * Open library
 */

SpriteLibrary::SpriteLibrary(const char* name) :
        d_fileName(),
        d_file(0),
        d_spriteCount(0),
        d_offsets(0),
        d_paletteCount(0),
        d_palOffsets(0),
        d_cache(0),
        d_palCache(0)
{
        try
        {
                /*
                 * Make the filename
                 */

                // char fname[FILENAME_MAX];
                d_fileName = makeFilename(name, ".SPR", False);

                // fileName = copyString(fname);

                /*
                 * Open up the file and read table of offsets
                 */

                // fileHandle.open(fileName, ios::in | ios::binary);

#if 0
                fileOpen( fileHandle, fileName, ios::in | ios::binary );

                if(fileHandle.fail())
                        throw GeneralError("Couldn't open sprite library %s", name);
#endif

                d_file = new WinFileBinaryReader(d_fileName);   // Will throw a FileError if can not open it
                ASSERT(d_file != 0);

                /*
                 * Read Header
                 */

                SpriteFileHeaderD header;
                d_file->read(&header, sizeof(header));

                UBYTE version = getByte(&header.versionOrder) & ~(IsIntel | IsMotorola);
                ASSERT(version >= 0x32);                // this reader can only handle version 3.2 and above
                ASSERT(version <= SPRITE_VERSION);

                if((version < 0x32) || (version > SPRITE_VERSION))
                        throw GeneralError("%s is wrong version (%d.%d)",
                                (const char*) name, (int) version/16, (int) version & 15);

                d_spriteCount = getWord(&header.spriteCount);
                d_paletteCount = getWord(&header.paletteCount);

                /*
                 * Read offset table
                 */

                d_offsets = new ULONG[d_spriteCount];
                ASSERT(d_offsets != 0);
                int count = d_spriteCount;
                ULONG* p = d_offsets;
                while(count--)
                {
                        *d_file >> *p++;
                }

                /*
                 * Read palette Offset table
                 */

                d_palOffsets = new ULONG[d_paletteCount];
                ASSERT(d_palOffsets != 0);
                count = d_paletteCount;
                p = d_palOffsets;
                while(count--)
                {
                        *d_file >> *p++;
                }


#if SPRITECACHE_METHOD==SPRITECACHE_DLIST
                d_cache = new SpriteCacheList;
#elif SPRITECACHE_METHOD==SPRITECACHE_ARRAY
                d_cache = new SpriteCacheList(d_spriteCount);
#else
                #error "SPRITECACHE_METHOD Not Defined"
#endif
                d_palCache = new PaletteCacheList;

                ASSERT(d_cache != 0);
                ASSERT(d_palCache != 0);

        }
        catch(const FileError& e)
        {
           throw GeneralError("File Error initialising Sprite Library File %s", name);
        }
}

/*
 * Close a library
 */

SpriteLibrary::~SpriteLibrary()
{
#ifdef DEBUG
        /*
         * Check cache for any unfreed images
         */

        d_cache->check();

#endif

        delete d_palCache;
        // d_cache->clearAndDestroy();
        delete d_cache;
        delete[] d_palOffsets;
        delete[] d_offsets;
        delete d_file;
}

/*
 * Read a sprite from library
 */

const SpriteBlock* SpriteLibrary::read(SpriteLibrary::Index n)
{
        if( (n >= d_spriteCount))// || (n < 0) )
        {
           n = d_spriteCount - 1;
#ifdef DEBUG
           char buf[100];
           wsprintf(buf, "Illegal Sprite Number %d in %s", 
              static_cast<int>(n), 
              static_cast<const char*>(d_fileName) );
           FORCEASSERT(buf);
#endif
        }
        else if(n < 0)
        {
                throw GeneralError("Illegal Sprite Number %d in %s", static_cast<int>(n), static_cast<const char*>(d_fileName));
        }

        try
        {

                /*
                 * See if it's already in the cache
                 */

                const SpriteBlock* image = d_cache->findAndLock(n);
                if(!image)
                {
                        /*
                         * Move to offset
                         */

                        d_file->seekTo(d_offsets[n]);

                        /*
                         * Read Header
                         */

                        SpriteHeaderD head;

                        d_file->read(&head, sizeof(head));
                        // if(fileHandle.fail())
                        //      throw GeneralError("Couldn't read SpriteHeader of %d in %s", int(n), fileName);

                        /*
                         * Create image and read in data
                         */

                        UWORD width = getWord(&head.width);
                        UWORD height = getWord(&head.height);

                        SpriteBlock* newImage = new SpriteBlock(width, height, this);

                        newImage->d_fullWidth                     = getWord(&head.fullWidth);
                        newImage->d_fullHeight            = getWord(&head.fullHeight);
                        newImage->d_xOffset                       = getWord(&head.xOffset);
                        newImage->d_yOffset                       = getWord(&head.yOffset);
                        newImage->d_anchorX                       = getWord(&head.anchorX);
                        newImage->d_anchorY                       = getWord(&head.anchorY);
                        newImage->d_shadowColor           = getByte(&head.shadowColor);
                        // newImage->setTransparent(getByte(&head.transparentColor));
                        newImage->setMaskColour(getByte(&head.transparentColor));

                        UBYTE flags = getByte(&head.flags);
                        newImage->d_flags.rle             = (flags >> RLE_B)              & 1;
                        newImage->d_flags.lzHuf   = (flags >> LZHUF_B)    & 1;
                        newImage->d_flags.nybble  = (flags >> NYBBLE_B)   & 1;
                        newImage->d_flags.squeeze = (flags >> SQUEEZE_B)  & 1;
                        newImage->d_flags.shadow  = (flags >> SHADOW_B)   & 1;
                        newImage->d_flags.remap   = (flags >> REMAP_B)            & 1;

                        /*
                         * read data block
                         *
                         * Slightly complex because DIB's have each line DWORD aligned
                         * so must read a line at a time.
                         *
                         * When decoding LZHuf encoded images, then it means
                         * we have to go through an extra buffer
                         */

                        // UWORD dataSize = getWord(&head.dataSize);
                        size_t dataSize = getLong(&head.dataSize);

                        if(newImage->d_flags.lzHuf)
                        {
                                UBYTE* inBuf = new UBYTE[dataSize];
                                UBYTE* outBuf = new UBYTE[width * height];

                                d_file->read(inBuf, dataSize);

                                // if(!fileHandle.fail())
                                decode_lzhuf(inBuf, outBuf, dataSize, width * height);

                                // Copy outBuf to image
                                // It could be speeded up if we assume the format of the
                                // destination.

                                UBYTE* src = outBuf;
                                for(int i = 0; i < height; i++)
                                {
                                        memcpy(newImage->getAddress(0,i), src, width);
                                        src += width;
                                }

                                delete[] outBuf;
                                delete[] inBuf;
                        }
                        else
                        {
                                /*
                                 * Raw Read the data... assumes it is not nybble packed...
                                 */

                                for(int i = 0; i < height; i++)
                                {
                                        d_file->read(newImage->getAddress(0, i), width);
                                }
                        }

                        /*
                         * get palette and remap the image
                         */

                        UWORD palIndex = getWord(&head.paletteIndex);
                        if(palIndex != 0)
                        {
                           --palIndex;

                           ASSERT(d_palCache != 0);

                           const PaletteCache* pal = d_palCache->get(palIndex);
                           if(pal == 0)
                           {
                                    d_file->seekTo(d_palOffsets[palIndex]);

                                    PaletteHeaderD head;
                                    d_file->read(&head, sizeof(head));

                                    UWORD nColors = getWord(&head.nColors);
                                    ASSERT(nColors == 256);         // remap assumes 256 colours

                                    RGBQUAD* colors = new RGBQUAD[nColors];

                                    for(int i = 0; i < nColors; i++)
                                    {
                                          struct {
                                                   UBYTE s_r;
                                                   UBYTE s_g;
                                                   UBYTE s_b;
                                          } color;

                                          d_file->read(&color, sizeof(color));

                                          colors[i].rgbRed = color.s_r;
                                          colors[i].rgbGreen = color.s_g;
                                          colors[i].rgbBlue = color.s_b;
                                          colors[i].rgbReserved = 0;
                                    }


                                    PaletteCache* newPal = new PaletteCache(palIndex, nColors, colors);
                                    newPal->addRef();
                                    d_palCache->append(newPal);
                                    pal = newPal;
                           }

                           ASSERT(pal != 0);

                           //-- TODO: either remap or set palette.
#if 0
                           newImage->remap(pal->rgbQuad());
#else
                           newImage->setPalette(pal->palette());
                           newImage->remapTo(CPalette::defaultPalette());
#endif

                           d_palCache->release(pal);


									// Also remap shadow colour

									if(newImage->hasShadow())
									{
										const RGBQUAD* rgbq = pal->palette()->colors(); // rgbQuad();
										rgbq = &rgbq[newImage->d_shadowColor];
										COLORREF rgb = RGB(rgbq->rgbRed, rgbq->rgbGreen, rgbq->rgbBlue);

                              newImage->d_shadowColor =
                                 CPalette::defaultPalette()->getIndex(rgb);
									}

                        }


                        // if(fileHandle.fail())
                        //      throw GeneralError("Couldn't read Sprite data for %d in %s", int(n), fileName);

                        image = newImage;
                        d_cache->add(n, newImage);
                        newImage->addRef();
                }

                ASSERT(image != 0);

                return image;
        }
        catch(const FileError& e)
        {
           throw GeneralError("Could not read Sprite data for %d in %s", (int)n, (const char*)d_fileName);
        }
}

/*
 * Mark a sprite as unused
 *
 * This will be more complex when the cacheing system is in place.
 */

void SpriteLibrary::release(const SpriteBlock* sprite)
{
        d_cache->free(sprite);
}

#if 0
void SpriteLibrary::release(SpriteLibrary::Index n)
{
        d_cache->free(n);
}
#endif


/*
 * Memory Exhaustion Handling functions
 */

/*
 * This is the function that gets called if new fails
 */

Boolean spriteMemHandlerFunction(size_t s)
{
        return memHandle.freeUp(s);
}

Boolean SpriteMemHandle::freeUp(size_t s)
{
        if(initialised)
        {
                if(libs.entries())
                {
                        PtrDListIter<SpriteCacheList> iter = libs;
                        while(++iter)
                        {
                                SpriteCacheList* lib = iter.current();

                                if(lib->flush())
                                        return True;
                        }
                }
        }
        return False;
}

#if 0           // Stuff for new to call when out of memory
/*
 * Add a new library
 */

static MemHandler spriteMemHandler = { &spriteMemHandlerFunction, 0 };
#endif

SpriteMemHandle::~SpriteMemHandle()
{
#if 0           // Stuff for new to call when out of memory
        if(initialised)
                removeMemHandler(&spriteMemHandler);
#endif
}

void SpriteMemHandle::add(SpriteCacheList* lib)
{
#if 0           // Stuff for new to call when out of memory
        if(!initialised)
        {
                addMemHandler(&spriteMemHandler);
                initialised = True;
        }
#endif

        libs.append(lib);
}

void SpriteMemHandle::remove(SpriteCacheList* lib)
{
        libs.remove(lib);
}


/*
 * SpriteBlock Implementation
 */

SpriteBlock::SpriteBlock(UWORD w, UWORD h, SpriteLibrary* l) :
        DIB(w,h),
#if SPRITECACHE_METHOD==SPRITECACHE_DLIST
        d_library(l),
#elif SPRITECACHE_METHOD==SPRITECACHE_ARRAY
        d_refCount(0),
#else
                #error "SPRITECACHE_METHOD Undefined"
#endif
        d_fullWidth(w),
        d_fullHeight(h),
        d_xOffset(0),
        d_yOffset(0),
        d_anchorX(0),
        d_anchorY(0),
        d_shadowColor(0),
        d_flags()
{
}

SpriteBlock::~SpriteBlock()
{
}

/*
 * Simple way of releasing a Spriteblock without needing to know what library
 * it comes from.
 */

void SpriteBlock::release() const
{
#if SPRITECACHE_METHOD==SPRITECACHE_DLIST
        library->release(this);
#elif SPRITECACHE_METHOD==SPRITECACHE_ARRAY
        delRef();
#else
        #error "SPRITECACHE_METHOD Undefined"
#endif
}

/*
 * Copy sprite onto a DIB using anchor, shadow, etc
 */


void SpriteBlock::blitTo(DrawDIB* dest, const PixelPoint& p) const
{
        dest->blit(this, p.getX() - d_anchorX, p.getY() - d_anchorY);
}

/*
 * Find average colour in a sprite
 * Moved from batdisp.cpp
 */

COLORREF SpriteBlock::averageColor() const
{
    int width = getWidth();
    int height = getHeight();

    ASSERT(width != 0);
    ASSERT(height != 0);

    int total_red=0;
    int total_green=0;
    int total_blue=0;

    // HPALETTE pal = Palette::get();
    // PALETTEENTRY pal_entry;

    PalettePtr pal = getPalette();
    const RGBQUAD* rgbTable = pal->colors();

    int total_pixels = 0;

    const ColorIndex* srcPtr = getBits();
    size_t endAdjust = getStorageWidth() - width;
    for(int y = 0; y < height; y++, srcPtr += endAdjust)
    {
        for(int x = 0; x < width; ++x, ++srcPtr)
        {
            ColorIndex colIndex = *srcPtr;   // getAddress(x,y);

            if (!hasMask() || (colIndex != getMask()))
            {
                const RGBQUAD& rgb = rgbTable[colIndex];
                // GetPaletteEntries(pal, *col_index, 1, &pal_entry);

                total_red   += rgb.rgbRed;
                total_green += rgb.rgbGreen;
                total_blue  += rgb.rgbBlue;
                ++total_pixels;
            }
        }
    }

    if (total_pixels != 0)
    {
        total_red   /= total_pixels;
        total_green /= total_pixels;
        total_blue  /= total_pixels;
    }

    return PALETTERGB(total_red, total_green, total_blue);
}

};              // namespace SWG_Srpite


