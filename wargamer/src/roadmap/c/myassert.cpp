/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "myassert.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void _myAssert(const char* expr, const char* file, int line)
{
	printf("Assertion Failure! %s in %s at line %d", expr, file, line);

	exit(1);
}


/*
 * Helper Functions
 */

void error(const char* fmt, ...)
{
	char buffer[1000];

	va_list vaList;
	va_start(vaList, fmt);
	vsprintf(buffer, fmt, vaList);
	va_end(vaList);

	printf("\nError:\n%s\n\n", buffer);

	exit(1);
}


