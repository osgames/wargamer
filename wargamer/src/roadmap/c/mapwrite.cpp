/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "mapwrite.hpp"
#include "dib.hpp"
#include "bmp.hpp"
#include "file.hpp"
#include "myassert.hpp"


MapWrite::MapWrite(DrawDIBDC* map, const char* mapFile)
{
	ASSERT(map != 0);
	ASSERT(mapFile != 0);

	/*
	 *  create file
	 */

	AppFileWriter f(mapFile);

	/*
	 *  fill in BITMAPFILEHEADER and write to disk
	 */

	BITMAPFILEHEADER header;

	header.bfType = 0x4d42; 
	header.bfSize = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+(sizeof(RGBQUAD)*256)+(map->getStorageWidth()*map->getHeight());
	header.bfReserved1 = 0;
	header.bfReserved2 = 0;
	header.bfOffBits = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+(sizeof(RGBQUAD)*256);

	f.write(&header, sizeof(BITMAPFILEHEADER));


	/*
	 * fill in BITMAPINFOHEADER and write to disk
	 */

	BITMAPINFOHEADER bmi;

	bmi.biSize = sizeof(BITMAPINFOHEADER);
	bmi.biWidth = map->getWidth();
	bmi.biHeight = map->getHeight();
	bmi.biPlanes = 1;
	bmi.biBitCount = 8;
	bmi.biCompression = BI_RGB;
	bmi.biSizeImage = 0;
	bmi.biXPelsPerMeter = 0;
	bmi.biYPelsPerMeter = 0;
	bmi.biClrUsed = 0;
	bmi.biClrImportant = 0;

	f.write(&bmi, sizeof(BITMAPINFOHEADER));

	/*
	 *  get color table and write to disk
	 */

	LPRGBQUAD rgb = BMP::getPalette();

	f.write(rgb, sizeof(RGBQUAD)*256);

	/*
	 *  get Bitmap and write to disk
	 *  Bitmap is top-down but needs to be written bottom-up
	 *
	 */

	PUCHAR dest = map->getBits() + map->getHeight() * map->getStorageWidth();
	int y = map->getHeight();
	while(y--)
	{
	  dest -= map->getStorageWidth();

	  f.write(dest, map->getStorageWidth());
	}

}
