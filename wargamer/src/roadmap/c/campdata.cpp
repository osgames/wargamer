/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "campdata.hpp"
#include "file.hpp"
#include "myassert.hpp"

CampData::CampData(const char* dataFile) :
d_dataFile(dataFile)
{
	AppFileReader frData(d_dataFile);

	/*
	 *  read town data
	 */

	d_towns.readData(frData);

	/*
	 *  read connection data
	 */

	d_connections.readData(frData);
}
