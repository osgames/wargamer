/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#include "mapdraw.hpp"
#include "bmp.hpp"
#include "dib.hpp"
#include "campdata.hpp"

struct MapScaleInfo
{
	unsigned int s_widthMiles;	// Width between p1->p2 and p3->p4
	unsigned int s_heightMiles;	// Distance between p1..p3 and p2..p4
	MapPoint s_total;		// Logical Map
	MapPoint s_p[4];		// TopLeft, TopRight, BottomLeft, BottomRight
	int s_viewAngle;		// Angle in degrees that viewpoint is facing, e.g. 0=North, 90=East

	MapScaleInfo(
		unsigned int widthMiles,
		unsigned int heightMiles,
		MapPoint total,
		MapPoint p1,
		MapPoint p2,
		MapPoint p3,
		MapPoint p4,
		int viewAngle)
	{
		s_widthMiles = widthMiles;
		s_heightMiles = heightMiles;
		s_total = total;
		s_p[0] = p1;
		s_p[1] = p2;
		s_p[2] = p3;
		s_p[3] = p4;
		s_viewAngle = viewAngle;
	}

};

static MapScaleInfo mapInfo (
	700,
	583,
	MapPoint(4096,3072),
		MapPoint(596, 346),
		MapPoint(3473, 323),	
		MapPoint(256, 2732),
		MapPoint(3845, 2709),
	315
);

MapDraw::MapDraw(const char* fileName, CampData* cData)
{
	ASSERT(fileName != 0);
	ASSERT(cData != 0);

	/*
	 *  Read palette
	 */

	dib = BMP::newDrawDIBDC(fileName, BMP::RBMP_Normal);

   d_campData = cData;

	top = MapPoint(0, 0);

	PixelPoint p = PixelPoint(dib->getWidth(), dib->getHeight());
	MapPoint m;
	pixelSizeToMap(p, m);
	size = m;

	total = mapInfo.s_total;	// MapPoint(4096, 3072);

	p1 = mapInfo.s_p[0];			// MapPoint(153*4, 114*4);
	p2 = mapInfo.s_p[1];			// MapPoint(875*4, 108*4);
	p3 = mapInfo.s_p[2];			// MapPoint( 58*4, 728*4);
	p4 = mapInfo.s_p[3];			// MapPoint(973*4, 715*4);

	physicalWidth = MilesToDistance(mapInfo.s_widthMiles);
	physicalHeight = MilesToDistance(mapInfo.s_heightMiles);

	pixelWidth = dib->getWidth();			// Start off with a 4:1 (1024 pixels total)
}

MapDraw::~MapDraw()
{
  if(dib)
	 delete dib;
}

MapCord MapDraw::getMidX() const
{
	return top.getX() + size.getX() / 2;
}

MapCord MapDraw::getMidY() const
{
	return top.getY() + size.getY() / 2;
}


/*
 * Set X position to centre on x
 */

Boolean MapDraw::setMapX(MapCord x)
{
	MapCord maxX = total.getX() - size.getX();
	x -= size.getX() / 2;
	if(x > maxX)
		x = maxX;
	if(x < 0)
		x = 0;

	if(x != top.getX())
	{
		top.setX(x);
		return True;
	}
	else
		return False;
}

Boolean MapDraw::setMapY(MapCord y)
{
	MapCord maxY = total.getY() - size.getY();
	y -= size.getY() / 2;
	if(y > maxY)
		y = maxY;
	if(y < 0)
		y = 0;

	if(y != top.getY())
	{
		top.setY(y);
		return True;
	}
	else
		return False;
}

/*
 * Coordinate Conversion Functions
 */

MapCord MapDraw::pixelToMap(LONG p) const
{
	return MulDiv(p, total.getX(), pixelWidth);
}

LONG MapDraw::mapToPixel(MapCord m) const
{
	return MulDiv(m, pixelWidth, total.getX());
}

void MapDraw::mapToPixel(const MapPoint& mc, PixelPoint& pc) const
{
	pc.setX(mapToPixel(mc.getX() - top.getX()));
	pc.setY(mapToPixel(mc.getY() - top.getY()));
}

void MapDraw::pixelToMap(const PixelPoint& pc, MapPoint& mc) const
{
	mc.setX(pixelToMap(pc.getX()) + top.getX());
	mc.setY(pixelToMap(pc.getY()) + top.getY());
}

void MapDraw::pixelSizeToMap(const PixelPoint& pc, MapPoint& mc) const
{
	mc.setX(pixelToMap(pc.getX()));
	mc.setY(pixelToMap(pc.getY()));
}

LONG MapDraw::distanceToPixel(Distance d) const
{
	return MulDiv(d, pixelWidth, physicalWidth);
}

void MapDraw::mapToLocation(const MapPoint& mc, Location& lc) const
{
	const MapCord topY = (p1.getY() + p2.getY()) / 2;		// Average Y coordinates
	const MapCord botY = (p3.getY() + p4.getY()) / 2;
	const MapCord h = botY - topY;				// Overall height

	const MapCord topW = p2.getX() - p1.getX();				// Width at top
	const MapCord botW = p4.getX() - p3.getX();				// Width at bottom


	MapCord y = mc.getY();
	y -= topY;
	// y = MulDiv(y, physicalWidth, total.getX());
	y = MulDiv(y, physicalHeight, total.getY());
	y = MulDiv(y, total.getY(), h);
	lc.setY(y);

	MapCord x = mc.getX();
	MapCord xLeft = p1.getX() - MulDiv(mc.getY() - topY, p1.getX() - p3.getX(), h);
	x  -= xLeft;

	MapCord xW = topW + MulDiv(mc.getY() - topY, botW - topW, h);
	x = MulDiv(x, physicalWidth, xW);
	lc.setX(x);
}

void MapDraw::locationToMap(const Location& lc, MapPoint& mc) const
{
	const MapCord topY = (p1.getY() + p2.getY()) / 2;		// Average Y coordinates
	const MapCord botY = (p3.getY() + p4.getY()) / 2;
	const MapCord h = botY - topY;				// Overall height

	const MapCord topW = p2.getX() - p1.getX();				// Width at top
	const MapCord botW = p4.getX() - p3.getX();				// Width at bottom

	LONG y = lc.getY();
	y = MulDiv(y, h, total.getY());
	// y = MulDiv(y, total.getX(), physicalWidth);
	y = MulDiv(y, total.getY(), physicalHeight);
	y += topY;
	mc.setY(y);

	LONG x = lc.getX();
	MapCord xLeft = p1.getX() - MulDiv(y - topY, p1.getX() - p3.getX(), h);
	MapCord xW = topW + MulDiv(y - topY, botW - topW, h);
	x = MulDiv(x, xW, physicalWidth);
	x += xLeft;
	mc.setX(x);
}

void MapDraw::pixelToLocation(const PixelPoint& pc, Location& lc) const
{
	MapPoint mc;
	pixelToMap(pc, mc);
	mapToLocation(mc, lc);
}

void MapDraw::locationToPixel(const Location& lc, PixelPoint& pc) const
{
	MapPoint mc;
	locationToMap(lc, mc);
	mapToPixel(mc, pc);
}



void MapDraw::drawConnections()
{
//	ArrayIter<Connection> cl = d_campData->getConnections();
	ConnectionList& cl = d_campData->getConnections();
	TownList& tl = d_campData->getTowns();

	/*
	 * Use different colour depending on connection type
	 */

	// HDC hdc = gApp.getDibDC();
	// HBITMAP oldBM = (HBITMAP) SelectObject(hdc, dib->getHandle());

	ColourIndex colors[CT_Max];
	colors[CT_Road]  = dib->getColour(BMP::getPalette(), 128, 64, 64, 256);//dib->getColour(PALETTERGB(128,64,64));
	colors[CT_Rail]  = dib->getColour(BMP::getPalette(), 0, 0, 0, 256);//dib->getColour(PALETTERGB(0,0,0));
	colors[CT_River] = dib->getColour(BMP::getPalette(), 0, 0, 128, 256);//dib->getColour(PALETTERGB(0,0,128));


//	RGBQUAD rgb[256];
//	memcpy(rgb, BMP::getPalette(), sizeof(RGBQUAD)*256);

	for(IConnection i = 0; i < cl.entries(); i++)
	{
		Connection& con = cl[i];

		const Location& l1 = tl[con.node1].getLocation();
		const Location& l2 = tl[con.node2].getLocation();

		PixelPoint p1;
		PixelPoint p2;

		locationToPixel(l1, p1);
		locationToPixel(l2, p2);

		/*
		 * Would be good to do a clipping test to see whether to draw or not
		 */

		switch(con.how)
		{
		  case CT_River:
			 drawRiver(p1, p2, colors[con.how], &con);
			 break;
		  case CT_Road:
			 drawRoad(p1, p2, colors[con.how], &con);
			 break;
		  case CT_Rail:
			 drawRailLine(p1, p2, colors[con.how], &con);
			 break;
		}
	}

	// SelectObject(hdc, oldBM);
}

void MapDraw::drawRiver(const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con)
{
	dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), colour);
}


void MapDraw::drawRoad(const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con)
{
  dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), colour, (dib->getWidth() <= 1024) ? 1:2);
}

void MapDraw::drawRailLine(const PixelPoint& p1, const PixelPoint& p2, ColourIndex colour, const Connection* con)
{

	const int space = 7;
	const int width = dib->getWidth() <= 1024 ? 3:5;

	PixelPoint start(p1.getX(), p1.getY());
	PixelPoint end(p2.getX(), p2.getY());

	const  int y = p2.getY() - p1.getY();
	const  int x = p2.getX() - p1.getX();

	const  int dist = aproxDistance(x, y);

	if(dist > 0)
	{

	  if(dib->getWidth() <= 1024)
	  {
			dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), colour);
	  }
	  else
	  {
			{
			  POINT point1;
			  point1.x = p1.getX() + ((width * y) / (2 * dist));
			  point1.y = p1.getY() - ((width * x) / (2 * dist));

			  POINT point2;
			  point2.x = p2.getX() + ((width * y) / (2 * dist));
			  point2.y = p2.getY() - ((width * x) / (2 * dist));

			  dib->line(point1.x, point1.y, point2.x, point2.y, colour);
			}

			{
			  POINT point1;
			  point1.x = p1.getX() - ((width * y) / (2 * dist));
			  point1.y = p1.getY() + ((width * x) / (2 * dist));

			  POINT point2;
			  point2.x = p2.getX() - ((width * y) / (2 * dist));
			  point2.y = p2.getY() + ((width * x) / (2 * dist));

			  dib->line(point1.x, point1.y, point2.x, point2.y, colour);
			}
	  }

	  if(dib->getWidth() > 1024)
		 colour = con->quality == CQ_Poor?dib->getColour(BMP::getPalette(), 255, 255, 255, 256)://dib->getColour(PALETTERGB(255, 255, 255)):
					 con->quality == CQ_Average?dib->getColour(BMP::getPalette(), 200, 0, 0, 256)://dib->getColour(PALETTERGB(200, 0, 0)):
					 dib->getColour(BMP::getPalette(), 0, 0, 0, 256);//dib->getColour(PALETTERGB(0, 0, 0));

	  dib->rightAngleLines(width, space, start, end, colour);

	}
}


