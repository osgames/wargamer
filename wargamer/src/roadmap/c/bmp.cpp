/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Load in .BMP file
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.4  1995/11/22 10:43:46  Steven_Green
 * File Classes changed
 *
 * Revision 1.3  1995/10/25 09:52:13  Steven_Green
 * Bitmaps are top down
 *
 * Revision 1.2  1995/10/20 11:04:10  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/10/17 12:02:04  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "bmp.hpp"
#include "dib.hpp"
// #include "globwin.hpp"
#include "palette.hpp"
#include "file.hpp"
#include "myassert.hpp"

static RGBQUAD BMP::pal[256];

/*
 * Interal BMP Reading Functions
 */

class BMPReader : public BMP {
 public:
	static void readDIB(RawDIB* dib, const char* fileName, ReadMode mode);
// 	static void readDIB(RawDIB* dib, HINSTANCE hinst, const char* resName, ReadMode mode);
};

/*
 * Read BMP from File
 */

static void BMPReader::readDIB(RawDIB* dib, const char* fileName, ReadMode mode)
{
	ASSERT(fileName != 0);
	ASSERT(mode < RBMP_HowMany);

	/*
	 * Open the file (Use a class so automatically closed)
	 */

	AppFileReader fr(fileName);

	if(!fr.isOK())
	{
		throw BMPError();
	}

	/*
	 * Read BITMAPFILEHEADER
	 */

	BITMAPFILEHEADER header;
	if(!fr.read(&header, sizeof(header)))
		throw BMPError();

	/*
	 * Read BITMAPINFO
	 */

	BITMAPINFOHEADER bmh;
	if(!fr.read(&bmh, sizeof(bmh)))
		throw BMPError();

	/*
	 * Check for valid type
	 */

	if((bmh.biPlanes != 1) ||
		(bmh.biBitCount != 8) ||
		(bmh.biCompression != BI_RGB))
	{
	  error("This is not a valid BMP file!");
	}

	/*
	 * Read and Copy Colour Table
	 */

	if(!fr.read(pal, sizeof(pal)))
		throw BMPError();


	if((Palette::get() == NULL) || (mode == RBMP_ForcePalette) || (mode == RBMP_OnlyPalette))
		Palette::set(Palette::makeIdentityPalette(pal, 256));

	if(mode == RBMP_OnlyPalette)
	{
		return;
	}
	else
	{
		ASSERT(dib != 0);

		/*
		 * Create DIB
		 */

		dib->makeBitmapInfo(bmh.biWidth, bmh.biHeight);

//		LPRGBQUAD rgb = dib->getColourTable();
//		memcpy(rgb, pal, sizeof(RGBQUAD)*256);

		/*
		 * Read and Copy Bit Table
		 *
		 * Do it line by line so that it is right way up
		 */

		PUCHAR dest = dib->getBits() + dib->getHeight() * dib->getStorageWidth();
		int y = dib->getHeight();
		while(y--)
		{
			dest -= dib->getStorageWidth();

			if(!fr.read(dest, dib->getStorageWidth()))
			{
				// delete dib;
				throw BMPError();
			}
		}

		/*
		 * Remap it
		 */

//		dib->remap(pal);
	}
}

/*
 * External Functions
 */

static void BMP::getPalette(const char* fileName)
{
	BMPReader::readDIB(0, fileName, RBMP_OnlyPalette);
}


static DIB* BMP::newDIB(const char* fileName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, fileName, mode);

	DIB* dib = new DIB(rDib);
	return dib;
}

static DrawDIB* BMP::newDrawDIB(const char* fileName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, fileName, mode);

	DrawDIB* dib = new DrawDIB(rDib);
	return dib;
}

static DIBDC* BMP::newDIBDC(const char* fileName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, fileName, mode);

	DIBDC* dib = new DIBDC(rDib);
	return dib;
}

static DrawDIBDC* BMP::newDrawDIBDC(const char* fileName, ReadMode mode)
{
	RawDIB rDib;
	BMPReader::readDIB(&rDib, fileName, mode);

	DrawDIBDC* dib = new DrawDIBDC(rDib);
	return dib;
}



