/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOWN_H
#define TOWN_H

#ifndef __cplusplus
#error town.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Data Structures and Class for Definition of Campaign Locations
 *
 * Locations are nodes in the transport network, representing a
 * town, railway connection, port, etc.
 *
 * Locations will be stored in an array, so that they can be
 * referenced with 16 bits indeces instead of 32 bit pointers.
 *
 *----------------------------------------------------------------------
 *
 * IMPORTANT
 *   When updating these structures and classes.  Always remember to
 *   update the file readers/writers in wldfile.cpp
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"
#include "gameob.hpp"
#include "array.hpp"
#include "gamedefs.hpp"
#include "random.hpp"
// #include "townbld.hpp"
// #include "sync.hpp"

class RWLock {
 public:
	virtual ~RWLock() { }
};

// class DrawDIB;
// class MapWindow;

class FileReader;
class FileWriter;

/*
 * Types of connection
 * Defined as enumerations, but if we later decide to use bitfields
 * then we'd better define them as UBYTE instead.
 */

enum ConnectType {
	CT_Road,
	CT_Rail,
	CT_River,
	CT_Max
};

enum ConnectQuality {
	CQ_Poor,
	CQ_Average,
	CQ_Good,
	CQ_Max
};

class WhichSideChokePoint {
public:
  enum Type {
	 NoChokePoint,
	 ThisSide,
	 ThatSide,

	 HowMany
  };

#ifdef DEBUG
  static const char* name(Type t);
#endif
};

typedef UBYTE ConnectCapacity;
#define MaxConnectCapacity UBYTE_MAX

class SupplyLevel {
	UWORD value;
public:
	enum {
		MaxLevel = UWORD_MAX
	};

	SupplyLevel() { }			// Unitialised constructor
	SupplyLevel(const SupplyLevel& l) { value = l.value; }
	SupplyLevel(int v) { ASSERT(v <= MaxLevel); value = UWORD(v); }

	// SupplyLevel& operator = (SupplyLevel l) { value = l.value; }
	// SupplyLevel& operator = (UWORD v) { value = v; }

	operator int() const { return value; }

	SupplyLevel& operator += (const SupplyLevel& sup)
	{
		if( (value >= (MaxLevel - sup.value)))
			value = MaxLevel;
		else
			value += sup.value;
		return *this;
	}

	SupplyLevel& operator -= (const SupplyLevel& sup)
	{
		if(value <= sup.value)
			value = 0;
		else
			value -= sup.value;
		return *this;
	}
};

/*
 * Structure Defining a connection
 */

struct Connection {
	static const UWORD fileVersion;

	ITown node1;				// Where the connection goes between
	ITown node2;
	ConnectType how;					// What type of connection (road/rail/river)
	ConnectQuality quality;			// Poor, Average, Good
	ConnectCapacity capacity;		// How many units can use it in a given time
	WhichSideChokePoint::Type whichSide;  // If terminating at a chokepoint, which side are we on
	Boolean offScreen;            // Is an off screen connection?
   Distance distance;

	/*
	 * File Interface
	 */

	Connection()
	{
	  offScreen = False;
	  distance = 0;
	}

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;

	ITown getOtherTown(ITown t1) const;
};

/*
 * Maximum number of connections from a town to another
 */

#define MaxConnections 16

/*
 * Values used for towns
 */

enum TownSize {
	TOWN_Capital,
	TOWN_City,
	TOWN_Town,
	TOWN_Other,

	TownSize_MAX,
};

/*
 * Alignment of name against dot
 */

enum NameAlign {
	NA_TOPLEFT,
	NA_TOP,
	NA_TOPRIGHT,
	NA_RIGHT,
	NA_BOTTOMRIGHT,
	NA_BOTTOM,
	NA_BOTTOMLEFT,
	NA_LEFT,

	NA_MAX
};

/*
 * Class defining a Location Node
 * It is friendlier to call them Towns
 *
 * TODO:
 *   Replace side and startSide with nations.
 *   This may be needed for the Austria Neutrality bodge.
 */

class Town :
	public NamedItem,
	public SideObject,
	// public StartSideObject,
	public LocationObject
	// public MapDisplayObject
	// public StrengthPointList
{
	friend class TownList;

	static const UWORD fileVersion;

public:
	enum { MaxFortifications = UBYTE_MAX };
	typedef UBYTE FortLevel;

private:
	IConnection connections[MaxConnections];		// Where is it connected to?
	IProvince province;			// What State/Province/Area is it in?

	NameAlign nameAlign;			// How is the name displayed?
	SBYTE nameX;					// Pixel adjustment from above
	SBYTE nameY;

	/*
	 * Constant Values
	 */

	TownSize  size;				// What size of town is it?
	Attribute victory[2];		// Victory points for each side
	Attribute political;			// Political value
	Attribute manPowerRate;		// Manpower generation
	Attribute resourceRate;		// Resource generation
	Attribute supplyRate;		// Forage/Supply Value
	Attribute forage;				// Forage Value
	Attribute strength;			// size needed to take over

	UBYTE stacking;				// Maximum SPs allowed in this location
	TerrainType terrain;			// What type of terrain is here

	UBYTE flags;

	enum {
		mpTransferable = 0x01,	// Man power is tranferable when taken over
		isSupplySource = 0x02,	// Point is a supply source
		isDepot			= 0x04,	// Point is a supply depot
		// isCapital		= 0x08,	// Is the province's capital city
		siegeable		= 0x10,	// Location can be sieged
	};

	/*
	 * Variable Attributes
	 */

	FortLevel fortifications;	// Fortification Level
	SupplyLevel supplyLevel;	// Current Supply Level

	ICommandPosition fortUpgrade;		// Who is upgrading our fortifications?
	ICommandPosition supplyInstall;	// Who is installing suply depot?

	/*
	 * Current state flags
	 *		Add things here such as:
	 *			List of units at this location
	 *			Current stacking level
	 *			Supplied
	 *			Sieged (and list of units)
	 *
	 * Siege/Garrison/Occupied flags are updated during each logic loop
	 * rather than try to maintain complicated lists of units.
	 */

	Boolean garrison;			// Is there a garrison here?
	Boolean siege;				// Is there a siege here?
	Boolean raid;				// Town is being raided
	Boolean occupied;			// Are there units here?


	Boolean breached;       // A breach has occurred during siege
   Boolean devBomb;        // A devastating bombardment occured during breach

	int weeksUnderSiege;

	Boolean offScreen;      // Is this an offscreen location?

	HelpID helpID;          // Help ID, if any

	/*
	 * More stuff to be added includes:
	 *		Dominant Terrain
	 *		Is it under siege? etc...
	 *
	 * We may want to store a list of units at this location (or moving
	 * from it).
	 */

public:
	Town();
	~Town() { }

	void setSize(TownSize sz) { size = sz; }
	TownSize getSize() const { return size; }

	void setNameAlign(NameAlign a) { nameAlign = a; }
	NameAlign getNameAlign() const { return nameAlign; }

	SBYTE getNameX() const { return nameX; }
	void setNameX(SBYTE x) { nameX = x; }
	SBYTE getNameY() const { return nameY; }
	void setNameY(SBYTE y) { nameY = y; }

	void setProvince(IProvince p) { province = p; }
	IProvince getProvince() const { return province; }

	void setConnection(int r, IConnection i) { connections[r] = i; }
	IConnection getConnection(int r) const { return connections[r]; }
	const IConnection* getConnections() const { return connections; }

	Attribute getVictory(int i) const { return victory[i]; }
	void setVictory(int i, Attribute v) { victory[i] = v; }
	void setVictory(Attribute v) { victory[0] = victory[1] = v; }

	Attribute getPolitical() const { return political; }
	void setPolitical(Attribute p) { political = p; }

	Attribute getManPowerRate() const { return manPowerRate; }
	void setManPowerRate(Attribute r) { manPowerRate = r; }
	// temp function for generating random mp-rate
	void setManPowerRate() {manPowerRate = (Attribute)random(0, 255);}

	Attribute getResourceRate() const { return resourceRate; }
	void setResourceRate(Attribute p) { resourceRate = p; }
	// temp function for generating random mp-rate
	void setResourceRate() { resourceRate = (Attribute)random(0, 255); }

	Attribute getSupplyRate() const { return supplyRate; }
	void setSupplyRate(Attribute p) { supplyRate = p; }

	const SupplyLevel& getSupplyLevel() const { return supplyLevel; }
	void setSupplyLevel(const SupplyLevel& l) { supplyLevel = l; }
	void addSupplyLevel(const SupplyLevel& sup) { supplyLevel += sup; }

	Boolean getSupplyRange(Distance& fullRange, Distance& maxRange) const;

	UBYTE getStacking() const { return stacking; }
	void setStacking(UBYTE p) { stacking = p; }

	UBYTE getTerrain() const { return terrain; }
	void setTerrain(UBYTE t) { terrain = t; }

	FortLevel getFortifications() const { return fortifications; }
	void setFortifications(FortLevel p) { fortifications = p; }
	Boolean upgradeFortifications();

	Attribute getStrength() const { return strength; }
	void setStrength(Attribute sp) { strength = sp; }

	HelpID getHelpID() const { return helpID; }
	void setHelpID(HelpID id) { helpID = id; }

	// SPCount getStrength(Side s) const;
	//--- Moved to CampaignData

	void setGarrison(Boolean f) { garrison = f; }
	void setSiege(Boolean f) { siege = f; }
	void setRaid(Boolean f) { raid = f; }
	void setOccupied(Boolean f) { occupied = f; }
	Boolean isGarrisoned() const { return garrison; }
	Boolean isSieged() const { return siege; }
	Boolean isRaided() const { return raid; }
	Boolean isOccupied() const { return occupied; }

	void setBreached(Boolean f) { breached = f; }
	void setDevBomb(Boolean f) { devBomb = f; }
	Boolean isBreached() const { return breached; }
	Boolean isDevBomb() const { return devBomb; }

	void setOffScreen(Boolean f) { offScreen = f; }
   Boolean isOffScreen() const { return offScreen; }

	void resetWeeksUnderSiege() { weeksUnderSiege = 0; }
	int getWeeksUnderSiege() const { return weeksUnderSiege; }
	void incrementWeeksUnderSiege() { weeksUnderSiege++; }
	Boolean surrenderMinimumReached() const
	{
	  return (weeksUnderSiege >= (fortifications * 2) && weeksUnderSiege != 0);
	}
	Boolean halfSurrenderMinimumReached() const
	{
	  return (weeksUnderSiege >= fortifications && weeksUnderSiege != 0);
	}

	ICommandPosition getFortUpgrader() const { return fortUpgrade; }
	void setFortUpgrader(ICommandPosition cpi);

	ICommandPosition getSupplyInstaller() const { return supplyInstall; }
	void setSupplyInstaller(ICommandPosition cpi);

private:
	Boolean getFlag(UBYTE mask) const { return (flags & mask) != 0; }

	void setFlag(UBYTE mask, Boolean onOff)
	{
		if(onOff)
			flags |= mask;
		else
			flags &= ~mask;
	}

public:
	Boolean getMpTransferable() const 		{ return getFlag(mpTransferable); 	 }
	void 	  setMpTransferable(Boolean f) 	{ 			setFlag(mpTransferable, f); }
	Boolean getIsSupplySource() const 		{ return getFlag(isSupplySource); 	 }
	void 	  setIsSupplySource(Boolean f) 	{ 			setFlag(isSupplySource, f); }
	Boolean getIsDepot() const 				{ return getFlag(isDepot); 			 }
	void 	  setIsDepot(Boolean f) 			{ 			setFlag(isDepot, 			f); }
	// Boolean getIsCapital() const 				{ return getFlag(isCapital); 			 }
	// void 	  setIsCapital(Boolean f) 			{ 			setFlag(isCapital, 		f); }
	Boolean getSiegeable() const 				{ return getFlag(siegeable); 			 }
	void 	  setSiegeable(Boolean f) 			{ 			setFlag(siegeable, 		f); }

	Boolean getIsCapital() const 				{ return getSize() == TOWN_Capital;  }

	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;

	// void draw(const MapWindow* mapWindow, HDC hdc, DrawDIB* dib);


	void adjustConnections(IConnection ci);

	int addConnection(IConnection ci);
};

/*
 * Class Defining an Area, Province or State
 * I'm going to call them Province, because State and Area both
 * have alternative meanings.
 */

class Province : 
	public NamedItem, 
	public SideObject,
	public StartSideObject,
	public LocationObject
	// public MapDisplayObject 
{
	friend class ProvinceList;

	static const UWORD fileVersion;

	NamedItem shortName;			// Abreviated Name for display on zoomed out map
	ITown capital;					// Capital City
	ITown town;						// Index into town list for towns in this state
	UWORD nTowns;					// Number of towns in this state
	// AttributePoints resource;
	// AttributePoints manPower;
	Nationality nationality;
	// BuildItem builds[BasicUnitType::HowMany];

	Boolean offScreen;         // Is location offscreen?

	HelpID helpID;             // Help ID, if any

	/*
	 * Also need:
	 *		Manpower usage: high value reduces resource point production
	 *		Allocation of resource/manpower points to production
	 */

public:
	Province();
	~Province() { }

	void setShortName(char* s) { shortName.setName(s); }
	const char* getShortName() const { return shortName.getName(); }

	ITown getCapital() const { return capital; }
	void setCapital(ITown i) { capital = i; }

	// AttributePoints getResource() const { return resource; }
	// void setResource(AttributePoints r) { resource = r; }

	// AttributePoints getManPower() const { return manPower; }
	// void setManPower(AttributePoints m) { manPower = m; }

	void setTown(ITown t) { town = t; }
	ITown getTown() const { return town; }
	void setNTowns(UWORD n) { nTowns = n; }
	UWORD getNTowns() const { return nTowns; }

	void setNationality(Nationality n) { nationality = n; }
	Nationality getNationality() const { return nationality; }

	void setOffScreen(Boolean f) { offScreen = f; }
	Boolean isOffScreen() { return offScreen; }

	void setHelpID(HelpID id) { helpID = id; }
   HelpID getHelpID() const { return helpID; }


	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;

	// void draw(const MapWindow* mapWindow, HDC hdc, DrawDIB* dib);
};

/*
 * Class representing a set of Towns
 * Using Array because it makes access with indeces very fast.
 * The disadvantage is that it is difficult to add and remove
 * entries.
 *
 * If this is a problem, then use DynamicArray
 */

class TownList : 
	public Array<Town>,
	public RWLock
{
public:
	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;

	// void draw(const MapWindow* mapWindow, HDC hdc, DrawDIB* dib);
};

class TownIter : public CArrayIter<Town> {
	// typedef CArrayIter<Town> Super;

	const TownList* list;

public:
	TownIter(const TownList& tl);
	~TownIter();

	// const Town& current() const { return Super::current(); }
};

class TownIterWrite : public ArrayIter<Town> {
	typedef ArrayIter<Town> Super;

	TownList* list;
public:
	TownIterWrite(TownList& tl);
	~TownIterWrite();
};

class ProvinceList : 
	public Array<Province>, 
	public RWLock
{
public:
	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;

	// void draw(const MapWindow* mapWindow, HDC hdc, DrawDIB* dib);
};

class ProvinceIter : public CArrayIter<Province> {
	const ProvinceList* list;
public:
	ProvinceIter(const ProvinceList& tl);
	~ProvinceIter();
};

class ProvinceIterWrite : public ArrayIter<Province> {
	ProvinceList* list;
public:
	ProvinceIterWrite(ProvinceList& tl);
	~ProvinceIterWrite();
};

class ConnectionList :
	public Array<Connection>,
	public RWLock
{
public:
	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;

	// void draw(const MapWindow* mapWindow, HDC hdc, DrawDIB* dib);
};

class ConnectionIter : public ArrayIter<Connection> {
	ConnectionList* list;
public:
	ConnectionIter(ConnectionList& tl);
	~ConnectionIter();
};

class ConnectionIterWrite : public ArrayIter<Connection> {
	ConnectionList* list;
public:
	ConnectionIterWrite(ConnectionList& tl);
	~ConnectionIterWrite();
};

#endif /* TOWN_H */

