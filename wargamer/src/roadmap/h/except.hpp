/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef EXCEPT_HPP
#define EXCEPT_HPP

#include "mytypes.h"

class GeneralError {
	static Boolean doingError;
	static Boolean firstError;
	char *describe;
public:
	GeneralError() {}
	GeneralError(const char* s, ...);

	const char* get() { return 0; }
protected:
	void setMessage(const char* s);
};

#endif