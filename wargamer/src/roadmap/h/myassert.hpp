/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MYASSERT_HPP
#define MYASSERT_HPP

#pragma off(unreferenced)

#if defined(DEBUG)

extern void _myAssert(const char* expr, const char* file, int line);

#define ASSERT(expr)	\
	if(expr)		\
	{				\
	}				\
	else			\
		_myAssert(#expr, __FILE__,__LINE__)

#define FORCEASSERT(s) _myAssert(s, __FILE__, __LINE__)


inline void debugLog(const char* fmt, ...) {}

#else

#define ASSERT(expr) ((void)0)

#endif

void error(const char* fmt, ...);

#endif
