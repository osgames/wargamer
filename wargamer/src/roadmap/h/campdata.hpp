/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef CAMPDATA_HPP
#define CAMPDATA_HPP

#include "town.hpp"
#include "gamedefs.hpp"


class Town;
class Connection;

/*
 *  class for holding town and connection data
 */

class CampData {

	 const char* d_dataFile;

	 TownList d_towns;
	 ConnectionList d_connections;

  public:

	 CampData(const char* dataFile);
	 ~CampData() {}

	 TownList& getTowns() { return d_towns; }
	 Town& getTown(ITown t) { return d_towns[t]; }

    ConnectionList& getConnections() { return d_connections; }
	 Connection& getConnection(IConnection c) { return d_connections[c]; }


};


#endif