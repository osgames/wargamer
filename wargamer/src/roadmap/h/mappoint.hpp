/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MAPPOINT_HPP
#define MAPPOINT_HPP

#ifndef __cplusplus
#error mappoint.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Coordinates on Campaign Map
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"

/*
 * Map co-ordinates
 *
 * These are based on a big pixel size given as part of the map
 * specifications.
 *
 * The big size should at least as big as the max zoom level because
 * zoom level is defined in terms of mapUnits per pixel
 */

typedef long MapCord;


class MapPoint {
	MapCord x;
	MapCord y;

public:
	MapPoint() { x = 0; y = 0; }
	MapPoint(MapCord _x, MapCord _y) { x = _x; y = _y; }

	MapCord getX() const { return x; }
	MapCord getY() const { return y; }
	void setX(MapCord _x) { x = _x; }
	void setY(MapCord _y) { y = _y; }
};


#endif /* MAPPOINT_HPP */

