/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BLOSSES_HPP
#define BLOSSES_HPP

#include "batunit.hpp"
#include "obdefs.hpp"
#include "batdata.hpp"
#include "b_result.hpp"
#ifdef DEBUG
class LogFile;
#endif

class BattleGameInterface;
// battle-loss routines
// Includes morale loss, SP loss, Leader loss, and any related stuff
namespace B_LossUtil {

bool unitWipedOut(RPBattleData bd, const RefBattleCP& cp);
   // returns true if unit no longer has any SP strength

bool clearDestroyedSP(BattleGameInterface* bg, const RefBattleCP& cp, bool* shouldReorganize, bool realign = True);
   // removes any destroyed SP from a unit

#ifdef DEBUG
void procMoraleLoss(BattleGameInterface* bg, const RefBattleCP& cp, LogFile& lf);
#else
void procMoraleLoss(BattleGameInterface* bg, const RefBattleCP& cp);
#endif
enum MoraleTestTrigger {
   Trigger_CriticalLoss,
   Trigger_ThresholdReached,
   Trigger_SPLostToFire,
   Trigger_SeniorLeaderLoss,
   Trigger_UnsupportedArty,
   Trigger_NearbyOldGuardRouted,
   Trigger_130MoraleChargedByOldGuard,

   Trigger_HowMany
};

#ifdef DEBUG
void procMoraleTest(BattleGameInterface* bg, const RefBattleCP& cp, MoraleTestTrigger trigger, LogFile& lf);
#else
void procMoraleTest(BattleGameInterface* bg, const RefBattleCP& cp, MoraleTestTrigger trigger);
#endif
void procLeaderLossTest(BattleGameInterface* bg, const RefGLeader& leader, const RefBattleCP& cp);
void procCorpDeterminationTest(BattleGameInterface* bg, const RefBattleCP& cp);
#ifdef DEBUG
void applyLoss(RPBattleData bd, const RefBattleCP& cp, int losses, LogFile& lf);
#else
void applyLoss(RPBattleData bd, const RefBattleCP& cp, int losses);
#endif
#ifdef DEBUG
bool procRoutTest(BattleGameInterface* bg, const RefBattleCP& cp, LogFile& bcLog);
#else
bool procRoutTest(BattleGameInterface* bg, const RefBattleCP& cp);
#endif
void routUnit(BattleGameInterface* batgame, const RefBattleCP& cp, bool procCorpDetermination = True);
bool shouldProcArmyDetermination(RPBattleData bd, Side side);
bool procArmyDeterminationTest(BattleGameInterface* bg, Side side);
bool isDefeated(RPBattleData bd, Side s);
void procDefeated(BattleGameInterface* bg, Side winner, BattleResultTypes::HowDefeated brt);

};

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
