/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MOVEUTIL_HPP
#define MOVEUTIL_HPP

#include "bl_dll.h"
#include "batdata.hpp"
#include "batcord.hpp"
#include "batunit.hpp"
#include "bobdef.hpp"
#include "minmax.hpp"

//namespace BattleMeasure {  };
using namespace BOB_Definitions;
using namespace BattleMeasure;
//class RefBattleCP;

struct BattleOrderInfo;

typedef MinMaxRect<HexCord::Cord> HexArea;

namespace B_Logic
{

inline int abValue(int v)
{
  return (v >= 0) ? v : -v;
}

//bool getCPDest(RPBattleData bd, const RefBattleCP& cp, HexArea& area, Boolean useWantHex = True);
bool adjacentHex(const HexCord& hex1, const HexCord& hex2);
bool adjacentHexFrontal(RPBattleData bd, BattleUnit* bu, HexCord& currentHex, const HexCord& destHex);
bool defaultHex(RPBattleData bd, BattleUnit* bu,
  HexCord& currentHex, const HexCord& destHex, const HexCord* excludeHex = 0);
bool updateWhichWay(RPBattleData bd, const RefBattleCP& cp);
void syncMapToSP(RPBattleData bd, const RefBattleCP& cp);
bool adjacentToSameHex(RPBattleData bd, const HexCord& sHex1, const HexCord& sHex2,
   const HexCord::HexDirection hd1, const HexCord::HexDirection hd2);
//bool toTheRear(CPBattleData bd, const HexPosition::Facing facing,
//  HexCord& rearHex, const HexCord& destHex, UWORD nHexes);


// Modified: Steven, 14Apr99: 
//  Implementation moved to batdata to avoid BatData being dependent on BatLogic
inline bool roadHex(RPBattleData bd, const HexCord& hex) { return bd->roadHex(hex); }

HexCord::HexDirection hexDirection(RPBattleData bd, const HexCord& srcHex, const HexCord& destHex);
#ifdef DEBUG
bool checkAlignment(RPBattleData bd, HexCord& lHex, HexCord& bHex, HexPosition::Facing f);
const char* faceName(HexPosition::Facing f);
#endif
void plotHQRoute(RPBattleData bd, const RefBattleCP& cp);
#ifdef DEBUG
bool hexNotOccupied(RPBattleData bd, HexCord& hex,  BattleUnit* bu, CRefBattleCP excludeCP, LogFile& lf);
#else 
bool hexNotOccupied(RPBattleData bd, HexCord& hex,  BattleUnit* bu, CRefBattleCP excludeCP);
#endif
#ifdef DEBUG
bool hexNotOccupied(RPBattleData bd, BattleUnit* bu, CRefBattleCP excludeCP, LogFile& lf);
#else 
bool hexNotOccupied(RPBattleData bd, BattleUnit* bu, CRefBattleCP excludeCP);
#endif
CPManuever moveHow(RPBattleData db, const RefBattleCP& cp);
bool rightDestHex(RPBattleData bd, HexPosition::Facing f, const HexCord& destHexL, int nColumns, HexCord& hex);
void cleanUpRoute(RPBattleData bd, const RefBattleCP& cp);
void cleanUpRoute(RPBattleData bd, HexList& rList);

enum GoWhichWay {
  GoLeft,
  GoRight,
};

//GoWhichWay whichWay(HexPosition::Facing cf, HexPosition::Facing wf);

BATLOGIC_DLL bool findOrderLeft(RPBattleData bd, BattleOrderInfo& order, const int spCount, HexPosition::Facing& lastFacing,
  CPFormation& lastFormation, const HexCord& startHex, HexCord& leftHex, int& nDir);
bool canHexBeOccupied(RCPBattleData bd, const RefBattleCP& cp, const HexCord& hex, bool currentFormation, bool allowForBridge);
//bool toTheRear(const HexPosition::Facing facing, const HexCord& srcHex, const HexCord& destHex);
bool withinNHexes(const HexCord& srcHex, const HexCord& destHex, const int nHexes);
int distanceFromUnit(const HexCord& hex, const CRefBattleCP& cp);
int distanceFromUnit(const CRefBattleCP& cp, const CRefBattleCP& enemyCP);

enum MoveWhatDirection {
	MWD_Front,
    MWD_LeftFront,
    MWD_RightFront,
    MWD_Left,
    MWD_Right,
    MWD_LeftRear,
    MWD_RightRear,
	 MWD_Rear,
    MWD_HowMany
};
bool plotMove(
    CPBattleData bd, 
    const HexPosition::Facing facing, 
    const HexCord& startHex, 
    HexCord& destHex, 
    UWORD nHexes,
    MoveWhatDirection wd,
    RefBattleCP cp = NoBattleCP);

#ifdef DEBUG
bool plotMovementToTarget(RPBattleData bd, const RefBattleCP& cp, LogFile& lf);
#else
bool plotMovementToTarget(RPBattleData bd, const RefBattleCP& cp);
#endif
#ifdef DEBUG
bool plotRetreatHex(RPBattleData bd, const RefBattleCP& cp, const int nHexes, LogFile& lf);
#else
bool plotRetreatHex(RPBattleData bd, const RefBattleCP& cp, const int nHexes);
#endif
enum MoveOrientation {
   MO_Front,
   MO_Left,
   MO_Right,
   MO_Rear
};
enum MO_Angle {
	MOA_Narrow,
	MOA_Wide,
	MOA_NormalFrontWideBack,
	MOA_Normal
};
MoveOrientation moveOrientation(const HexPosition::Facing facing, const HexCord& srcHex, const HexCord& destHex, MO_Angle a = MOA_Normal);
   // in what general orientation are we moving?
   // i.e. either Front, Left, Right, or Rear

bool movingBackward(const RefBattleCP& cp, BattleUnit* bu);
   // is SP moving backwards relative to unit facing

bool inATrafficJam(RPBattleData bd, const RefBattleCP& cp, HexCord hex);
bool inATrafficJam(RPBattleData bd, const RefBattleCP& cp);
	 // is a CP in a traffic jam with another friendly unit
void getNewLeft(const RefBattleCP& cp, HexCord& leftHex);
//HexPosition::Facing avgUnitFacing(CRefBattleCP& cp);
bool unitIsFacing(HexPosition::Facing facing, const CRefBattleCP& cp2);
HexPosition::Facing frontFacing(const HexCord& leftHex, const HexCord& rightHex);

}; // namespace

#endif
