/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bu_move.hpp"
#include "batdata.hpp"
#include "batctrl.hpp"
#include "batarmy.hpp"
#include "bobutil.hpp"
#include "hexdata.hpp"
#include "bobiter.hpp"
#include "hexmap.hpp"
#include "minmax.hpp"
#include "bdeploy.hpp"
#include "bmanuevr.hpp"
#include "bspform.hpp"
#include "moveutil.hpp"
#include "bmove.hpp"
#include "sync.hpp"
#include "b_tables.hpp"
#include "scenario.hpp"
#include "wg_rand.hpp"
#include "blosses.hpp"
#include "batmsg.hpp"
#include "control.hpp"
#ifdef DEBUG
#include "options.hpp"
#include "random.hpp"
#include "clog.hpp"
static LogFile moveLog("Bat_Move.log");
#endif
#include <utility>

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif


namespace Local
{

typedef HexPosition::Facing Facing;

enum {
   East = 0x01,
   NE   = 0x02,
   NW   = 0x04,
   West = 0x08,
   SW   = 0x10,
   SE   = 0x20
};

inline UBYTE faceToMask(Facing mf)
{
    return (mf == HexPosition::East)          ? West :
         (mf == HexPosition::NorthEastFace) ? SW :
            (mf == HexPosition::NorthWestFace) ? SE :
            (mf == HexPosition::West)          ? East :
            (mf == HexPosition::SouthWestFace) ? NE :
            (mf == HexPosition::SouthEastFace) ? NW : 0;
}


};

namespace B_Logic
{

/*
 * Local utility functions and classes
 */

class BU_MoveImp {
   public:
         BU_MoveImp(BattleGameInterface* batgame);
      ~BU_MoveImp();

         void process();

    private:

         enum RoadType {
            Road,
            Track,
            RT_Undefined,
            NotOnRoad
         };
      bool startDeploy(const RefBattleCP& cp);
         // start deployement for a unit
      bool startManuever(const RefBattleCP& cp);
             // start manuever for a unit
      bool startMovement(const RefBattleCP& cp);
         // start movement for a unit
         bool startReorganize(const RefBattleCP& cp);
      bool finalMove(const RefBattleCP& cp);
      bool startSPFormationChange(const RefBattleCP& cp);
         // start formation change for an SP
      void getNextHex(BattleUnit* bu);
         // advance next hex
      void doMove(const RefBattleCP& cp);
         // start actual move
         bool moving(const RefBattleCP& cp);
             // physically move unit
      void deployOrg(const RefBattleCP& cp);
         // deploy a unit and its children (recursive!!)
         void deployOrgAndSisters(const RefBattleCP& cp);
             // deploy a unit and its sisters
         void placeUnit(const RefBattleUnit& bu, const HexCord& hex);
             // Set an SP's position and update hexMap
         void runSPFormations(const RefBattleCP& cp);
         bool runMovement(const RefBattleCP& cp);

         bool changingFace(const RefBattleCP& cp);
         void setUnitSpeed(const RefBattleCP& cp);
         bool moveAlong(BattleUnit* bu, const Distance moveDist, HexPosition::WhereAt where);
         bool movingOnRoad(BattleUnit* bu, RoadType& pt);
//       bool movingOnRoad(const RefBattleCP& cp, PathType& pt);
         bool canContinue(const RefBattleCP& cp, bool* changingFormation = 0);
         bool destHexesOccupied(const RefBattleCP& cp);
         bool updateArtillery(const RefBattleCP& cp);
//       void cancelRoute(const RefBattleCP& cp);
         bool canUpdate(const RefBattleCP& cp);
         bool canMove(const RefBattleCP& cp, const RefBattleSP& sp, const int column, const int row);
         void updateUnitMode(const RefBattleCP& cp);
         void clearMove(const RefBattleCP& cp, bool hold);
         HexPosition::Facing getFace(const RefBattleCP& cp, HexPosition::Facing rf, HexPosition::Facing mf);
         void setNextFacing(BattleUnit* bu);
         void sendRetreatMessage(const RefBattleCP& cp, int r);
         bool shouldRemove(const RefBattleCP& cp);
           void removeUnit(const RefBattleCP& cp);
            void runWhatOrderList(const RefBattleCP& cp);
         bool canCrossBridge(const RefBattleCP& cp);
         bool stillMovingOverBridge(const RefBattleCP& cp);
            bool needsToReadjust(const RefBattleCP& cp);
         bool adjacentToRear(HexPosition::Facing facing, const HexCord& hex1, const HexCord& hex2);
//            void syncHQToSP(const RefBattleCP& hq);

         bool moveUnit(const RefBattleCP& cp, BattleUnit* bu, Distance moveDist, bool& stillMoving);
         bool shouldMoveUnit(const RefBattleCP& cp, BattleUnit* bu, bool& stillMoving);

         bool safeMove(const RefBattleCP& cp);
         bool safeMoveUnit(BattleUnit* bu);
#ifdef DEBUG
         void instantMove(const RefBattleCP& cp);
#endif
    private:


         // Handy variables
         BattleGameInterface* d_batgame;
         PBattleData d_batData;
         BattleOB* d_ob;
         const TerrainTable& d_terrainTable;
         const BattleBuildings::BuildingTable* d_buildings;
         const BattleBuildings::BuildingList* d_buildingList;
         const BattleMap* d_map;
         BattleHexMap*  d_hexMap;

         // virtual classes for deployment, and movement
         Deploy_Int   d_formations;
         Manuever_Int d_manuevers;
         SPFormation_Int d_spFormations;
         Movement_Int d_movement;

         BattleTime::Tick d_lastTick;
         BattleTime::Tick d_nextTick;
};

BU_MoveImp::BU_MoveImp(BattleGameInterface* batgame) :
    d_batgame(batgame),
    d_batData(batgame->battleData()),
    d_terrainTable(d_batData->terrainTable()),
    d_ob(d_batData->ob()),
    d_buildings(d_batData->buildingTable()),
    d_buildingList(d_batData->buildingList()),
    d_map(d_batData->map()),
    d_hexMap(d_batData->hexMap()),
    d_formations(d_batData),
    d_manuevers(d_batData),
    d_spFormations(d_batData),
    d_movement(d_batData),
    d_lastTick(d_batData->getTick()),
    d_nextTick(d_batData->getTick())
{
   ASSERT(d_ob != 0);
   ASSERT(d_buildings != 0);
   ASSERT(d_buildingList != 0);
   ASSERT(d_map != 0);
   ASSERT(d_hexMap != 0);
}

BU_MoveImp::~BU_MoveImp()
{
}

bool BU_MoveImp::movingOnRoad(BattleUnit* bu, RoadType& pt)
{
      // get terrain
      const BattleTerrainHex& hexInfo = d_batData->getTerrain(bu->hex());
//    bool onRoad = False;

   if(hexInfo.isRoad())
   {
      if(hexInfo.d_path1.d_type == LP_Road || 
           hexInfo.d_path1.d_type == LP_SunkenRoad || 
           hexInfo.d_path1.d_type == LP_RoadBridge || 
           hexInfo.d_path2.d_type == LP_Road || 
           hexInfo.d_path2.d_type == LP_SunkenRoad || 
           hexInfo.d_path2.d_type == LP_RoadBridge)
         {
          
            pt = Road;
         }
         else
         {
            pt = Track;
         }  
         
         return True;
   }

   return False;
}

const HexPosition::Facing e   = HexPosition::East;
const HexPosition::Facing nep = HexPosition::NorthEastPoint;
const HexPosition::Facing ne  = HexPosition::NorthEastFace;
const HexPosition::Facing n   = HexPosition::North;
const HexPosition::Facing nw  = HexPosition::NorthWestFace;
const HexPosition::Facing nwp = HexPosition::NorthWestPoint;
const HexPosition::Facing w   = HexPosition::West;
const HexPosition::Facing swp = HexPosition::SouthWestPoint;
const HexPosition::Facing sw  = HexPosition::SouthWestFace;
const HexPosition::Facing s   = HexPosition::South;
const HexPosition::Facing se  = HexPosition::SouthEastFace;
const HexPosition::Facing sep = HexPosition::SouthEastPoint;
const HexPosition::Facing ud  = HexPosition::Facing_Undefined;
const HexPosition::Facing nc  = static_cast<HexPosition::Facing>(4);

const HexPosition::MoveTo m_ef  = HexPosition::East_Face;
const HexPosition::MoveTo m_nef = HexPosition::NE_Face;
const HexPosition::MoveTo m_nwf = HexPosition::NW_Face;
const HexPosition::MoveTo m_wf  = HexPosition::West_Face;
const HexPosition::MoveTo m_swf = HexPosition::SW_Face;
const HexPosition::MoveTo m_sef = HexPosition::SE_Face;
const HexPosition::MoveTo m_ce  = HexPosition::Center_East;
const HexPosition::MoveTo m_cne  = HexPosition::Center_NE;
const HexPosition::MoveTo m_cnw  = HexPosition::Center_NW;
const HexPosition::MoveTo m_cw  = HexPosition::Center_West;
const HexPosition::MoveTo m_csw  = HexPosition::Center_SW;
const HexPosition::MoveTo m_cse  = HexPosition::Center_SE;
const HexPosition::MoveTo m_cc  = HexPosition::Center_Center;
const HexPosition::MoveTo m_ud  = HexPosition::MoveTo_Undefined;

void BU_MoveImp::setNextFacing(BattleUnit* bu)
{
   ASSERT(bu->nextHex());
   ASSERT(bu->nextHex()->d_hex != bu->hex());
   ASSERT(bu->hexPosition().edgePos() || bu->hexPosition().centerPos());

   CRefBattleCP cp = BobUtility::getCP(bu);

   // if we are at the edge of a hex
   HexPosition::MoveTo srcPos = HexPosition::MoveTo_Undefined;
   HexPosition::MoveTo destPos = HexPosition::MoveTo_Undefined;

   if(bu->hexPosition().atEdge())
   {
      HexPosition::Facing srcDir = oppositeFace(moveDirection(bu->hex(), bu->nextHex()->d_hex));

      srcPos = (srcDir == HexPosition::East)          ? HexPosition::East_Face :
             (srcDir == HexPosition::NorthEastFace) ? HexPosition::NE_Face :
             (srcDir == HexPosition::NorthWestFace) ? HexPosition::NW_Face :
             (srcDir == HexPosition::West)          ? HexPosition::West_Face :
             (srcDir == HexPosition::SouthWestFace) ? HexPosition::SW_Face :
             (srcDir == HexPosition::SouthEastFace) ? HexPosition::SE_Face : HexPosition::MoveTo_Undefined;

      ASSERT(srcPos != HexPosition::MoveTo_Undefined);

      if(bu->routeList().entries() == 1)
      {
         destPos = HexPosition::Center_Center;
      }

      else
      {
         HexCord srcHex = bu->hex();
         //---Dodgy code... assumes nextHex as set up list's current position
         // HexCord nextHex = bu->nextHex()->d_hex;
         // HexCord next2Hex = bu->routeList().next()->d_hex;

         const HexList& rl = bu->routeList();
         const HexItem* nextItem = rl.head();
         const HexItem* next2Item = rl.next(nextItem);
         HexCord nextHex = nextItem->d_hex;
         HexCord next2Hex = next2Item->d_hex;

         if(adjacentHex(srcHex, nextHex) &&
            (adjacentHex(srcHex, next2Hex) || srcHex == next2Hex) )
         {
            destPos = HexPosition::Center_Center;
         }
         else
         {
            HexPosition::Facing destDir = moveDirection(nextHex, next2Hex);

            destPos = (destDir == HexPosition::East)          ? HexPosition::East_Face :
                           (destDir == HexPosition::NorthEastFace) ? HexPosition::NE_Face :
                           (destDir == HexPosition::NorthWestFace) ? HexPosition::NW_Face :
                           (destDir == HexPosition::West)          ? HexPosition::West_Face :
                           (destDir == HexPosition::SouthWestFace) ? HexPosition::SW_Face :
                           (destDir == HexPosition::SouthEastFace) ? HexPosition::SE_Face : HexPosition::MoveTo_Undefined;

            ASSERT(destPos != HexPosition::MoveTo_Undefined);
         }
      }

#ifdef DEBUG
      moveLog.printf("---- Unit at edge of hex");
      moveLog.printf("-----srcPos = %d, destPos = %d", (int)srcPos, (int)destPos);
#endif
   }

   else // atCenter
   {
      HexPosition::Facing destDir = moveDirection(bu->hex(), bu->nextHex()->d_hex); //, bu->routeList().next()->d_hex);

      destPos = (destDir == HexPosition::East)          ? HexPosition::East_Face :
                (destDir == HexPosition::NorthEastFace) ? HexPosition::NE_Face :
                (destDir == HexPosition::NorthWestFace) ? HexPosition::NW_Face :
                (destDir == HexPosition::West)          ? HexPosition::West_Face :
                (destDir == HexPosition::SouthWestFace) ? HexPosition::SW_Face :
                (destDir == HexPosition::SouthEastFace) ? HexPosition::SE_Face : HexPosition::MoveTo_Undefined;

      ASSERT(destPos != HexPosition::MoveTo_Undefined);
      if( (bu->hexPosition().moveTo() == HexPosition::NoWhere) ||
            (bu->hexPosition().moveTo() == HexPosition::Center_Center) ) // ||// ) //||
      {
         srcPos = HexPosition::Center_Center;
      }

      else
      {
         srcPos = bu->hexPosition().moveTo();
      }
//    ASSERT(srcPos != destPos);
#ifdef DEBUG
      moveLog.printf("---- Unit at center of hex");
      moveLog.printf("-----srcPos = %d, destPos = %d", (int)srcPos, (int)destPos);
#endif
      if(srcPos == destPos)
         return;
   }

   ASSERT(srcPos != HexPosition::MoveTo_Undefined);
   ASSERT(destPos != HexPosition::MoveTo_Undefined);
   ASSERT(srcPos < HexPosition::MoveTo_HowMany);
   ASSERT(destPos < HexPosition::MoveTo_HowMany);
#ifdef DEBUG
   const HexPosition::Facing s_faceToEdgeDB[HexPosition::MoveTo_HowMany][HexPosition::MoveTo_HowMany] = {
      // E   NE   NW   W    SW   SE    CE  CNE  CNW  CW   CSW  CSE  CC
      { ud,  ud,  nwp, w,   swp, ud,   ud,  ud,  ud,  ud,  ud,  ud, nc }, //w }, //w,   nwp, nwp, w,   swp, swp, ud }, //w   }, // E
      { ud,  ud,  ud,  swp, sw,  s,    ud,  ud,  ud,  ud,  ud,  ud, nc }, //s },//sw }, //s,   sw,  swp, swp, sw,  s,   ud }, //sw  }, // NE
      { sep, ud,  ud,  ud,  s,   se,   ud,  ud,  ud,  ud,  ud,  ud, nc }, //s }, //se }, //swp, swp, sw,  s,   s,   sw,  ud }, //se  }, // NW
      { e,   nep, ud,  ud,  ud,  sep,  ud,  ud,  ud,  ud,  ud,  ud, nc }, //e }, //e,   nep, nep, e,   swp, swp, ud }, //e   }, // W
      { nep, ne,  n,   ud,  ud,  ud,   ud,  ud,  ud,  ud,  ud,  ud, nc }, //n }, //ne }, //nep, ne,  n,   n,   ne,  nep, ud }, // ne  }, // SW
      { ud,  n,   nw,  nwp, ud,  ud,   ud,  ud,  ud,  ud,  ud,  ud, nc }, //n }, //nw }, //n,   n,   nw,  nwp, nwp, nw,  ud }, //nw  }, // SE

      { e,   n,   nwp, w,   swp, s,    ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CE
      { sep, se,  nwp, swp, sw,  s,    ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CNE
      { sep, nep, nw,  swp, s,   se,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CNW
      { e,   nep, n,   w,   s,   sep,  ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CW
      { nep, ne,  n,   nwp, sw,  sep,  ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CSW
      { nep, n,   nw,  nwp, swp, se,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CSE

      { nc,  nc,  nc,  nc,  nc,  nc,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }
   };
#endif
   // failsafe version
   const HexPosition::Facing s_faceToEdge[HexPosition::MoveTo_HowMany][HexPosition::MoveTo_HowMany] = {
      // E   NE   NW   W    SW   SE    CE  CNE  CNW  CW   CSW  CSE  CC
      { e,   nep, nwp, w,   swp, sep,   ud,  ud,  ud,  ud,  ud,  ud, nc }, //w }, //w,   nwp, nwp, w,   swp, swp, ud }, //w   }, // E
      { nep, ne,  n,   swp, sw,  s,    ud,  ud,  ud,  ud,  ud,  ud, nc }, //s },//sw }, //s,   sw,  swp, swp, sw,  s,   ud }, //sw  }, // NE
      { sep, n,   nw,  nwp, s,   se,   ud,  ud,  ud,  ud,  ud,  ud, nc }, //s }, //se }, //swp, swp, sw,  s,   s,   sw,  ud }, //se  }, // NW
      { e,   nep, nwp, w,   swp, sep,  ud,  ud,  ud,  ud,  ud,  ud, nc }, //e }, //e,   nep, nep, e,   swp, swp, ud }, //e   }, // W
      { nep, ne,  n,   swp, sw,  s,    ud,  ud,  ud,  ud,  ud,  ud, nc }, //n }, //ne }, //nep, ne,  n,   n,   ne,  nep, ud }, // ne  }, // SW
      { sep, n,   nw,  nwp, s,   se,   ud,  ud,  ud,  ud,  ud,  ud, nc }, //n }, //nw }, //n,   n,   nw,  nwp, nwp, nw,  ud }, //nw  }, // SE

      { e,   n,   nwp, w,   swp, s,    ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CE
      { sep, se,  nwp, swp, sw,  s,    ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CNE
      { sep, nep, nw,  swp, s,   se,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CNW
      { e,   nep, n,   w,   s,   sep,  ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CW
      { nep, ne,  n,   nwp, sw,  sep,  ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CSW
      { nep, n,   nw,  nwp, swp, se,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CSE

      { nc,  nc,  nc,  nc,  nc,  nc,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }
   };
   const HexPosition::Facing s_faceToCenter[HexPosition::MoveTo_HowMany][HexPosition::MoveTo_HowMany] = {
      // E   NE   NW   W    SW   SE    CE  CNE  CNW  CW   CSW  CSE  CC
      { e,   nep, nwp, w,   swp, sep,  ud,  ud,  ud,  ud,  ud,  ud, w }, //w,   nwp, nwp, w,   swp, swp, ud }, //w   }, // E
      { e,   ne,  nwp, swp, sw,  se,   ud,  ud,  ud,  ud,  ud,  ud, sw }, //s,   sw,  swp, swp, sw,  s,   ud }, //sw  }, // NE
      { sep, nep, nw,  w,   s,   se,   ud,  ud,  ud,  ud,  ud,  ud, se }, //swp, swp, sw,  s,   s,   sw,  ud }, //se  }, // NW
      { e,   nep, nwp, w,   sw,  sep,  ud,  ud,  ud,  ud,  ud,  ud, e }, //e,   nep, nep, e,   swp, swp, ud }, //e   }, // W
      { nep, ne,  n,   w,   sw,  se,   ud,  ud,  ud,  ud,  ud,  ud, ne }, //nep, ne,  n,   n,   ne,  nep, ud }, // ne  }, // SW
      { e,   n,   nw,  nwp, sw,  se,   ud,  ud,  ud,  ud,  ud,  ud, nw }, //n,   n,   nw,  nwp, nwp, nw,  ud }, //nw  }, // SE

      { e,   n,   nwp, w,   swp, s,    ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CE
      { sep, se,  nwp, swp, sw,  s,    ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CNE
      { sep, nep, nw,  swp, s,   se,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CNW
      { e,   nep, n,   w,   s,   sep,  ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CW
      { nep, ne,  n,   nwp, sw,  sep,  ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CSW
      { nep, n,   nw,  nwp, swp, se,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CSE

      { nc,  nc,  nc,  nc,  nc,  nc,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }
   };
#ifdef DEBUG
   HexPosition::Facing dbF = (bu->hexPosition().atEdge()) ? s_faceToEdgeDB[srcPos][destPos] :
       s_faceToCenter[srcPos][destPos];
   ASSERT(dbF != ud);
#endif

   HexPosition::Facing f = (bu->hexPosition().atEdge()) ? s_faceToEdge[srcPos][destPos] :
       s_faceToCenter[srcPos][destPos];
   ASSERT(f != ud);

   HexPosition::Facing newFace = bu->rFacing();
   if(!cp->movingBackwards() && !cp->retreating())
   {
      newFace = (f == nc || f == ud) ? bu->facing() : f;

      if(f != nc && f != ud && movingBackward(const_cast<BattleCP*>(cp), bu))
         newFace = oppositeFace(newFace);
   }

#ifdef DEBUG
   if(movingBackward(const_cast<BattleCP*>(cp), bu))
      moveLog.printf("---- %s is moving backwards", cp->getName());
   if(cp->targetList().entries() > 0)
   {
      moveLog.printf("---- Enemy is near %s", cp->getName());
   }
#endif

   bu->hexPosition().moveFrom(srcPos);
   bu->hexPosition().moveTo(destPos);
   bu->nextFacing(newFace);
}

// make sure we can safely move
bool BU_MoveImp::safeMoveUnit(BattleUnit* bu)
{
   HexList& rList = bu->routeList();
   HexItem* hi = rList.first();
   HexItem* lastHI = 0;
   while(hi)
   {
      if(lastHI != 0)
      {
         if(lastHI->d_hex != hi->d_hex && !adjacentHex(hi->d_hex, lastHI->d_hex))
            return False;
      }

      lastHI = hi;
      hi = rList.next();
   }

   return True;
}

bool BU_MoveImp::safeMove(const RefBattleCP& cp)
{
   bool safe = True;
   if(cp->getRank().sameRank(Rank_Division))
   {
      for(std::vector<DeployItem>::iterator di = cp->mapBegin();
          di != cp->mapEnd();
          ++di)
      {
         if(di->active())
         {
            if(!safeMoveUnit(di->d_sp))
            {
               safe = False;
               break;
            }
         }
      }
   }
   else
   {
      safe = safeMoveUnit(cp);
   }

   if(!safe)
   {
      FORCEASSERT("Oops!! Unsafe movement!");
#ifdef DEBUG
      moveLog.printf("%s has an unsafe route. Resetting to hold", cp->getName());
#endif
      if(cp->getRank().sameRank(Rank_Division))
      {
         DeployItem* di = cp->wantDeployItem(0, 0);
         ASSERT(di);
         if(di && di->active())
         {
            cp->setFacing(di->d_sp->rFacing());
         }

         cp->nextFormation(cp->formation());
         cp->getCurrentOrder().divFormation(cp->formation());
         cp->getCurrentOrder().manuever(CPM_Hold);
      }

      clearMove(cp, True);
   }

   return safe;
}

bool BU_MoveImp::adjacentToRear(HexPosition::Facing facing, const HexCord& hex1, const HexCord& hex2)
{
    for(int i = 0; i < 2; i++)
    {
      HexCord::HexDirection hd = (i == 0) ? leftRear(facing) : rightRear(facing);
      HexCord hex;
      if(d_batData->moveHex(hex1, hd, hex) && hex == hex2)
         return True;
    }
    
    return False;
}
        
                
// do we need to readjust XX deployment in case of akward positioning?
bool BU_MoveImp::needsToReadjust(const RefBattleCP& cp)
{
   if(cp->needsReadjustment())
      return True;

   if(cp->getRank().isHigher(Rank_Division) || cp->formation() >= CPF_Deployed)
      return False;

    DeployItem* di1 = cp->currentDeployItem(0, 0);
    ASSERT(di1);
    ASSERT(di1->active());
    if(di1 && di1->active())
    {
       for(int r = 1; r < cp->rows(); r++)
       {
          DeployItem* di2 = cp->currentDeployItem(0, r);
          ASSERT(di2);
          if(di2 && di2->active() &&
              (di1->d_sp->hex() == di2->d_sp->hex() ||
               !adjacentHex(di1->d_sp->hex(), di2->d_sp->hex()) ||
               !adjacentToRear(cp->facing(), di1->d_sp->hex(), di2->d_sp->hex())) )
          {
               return True;
          }

          di1 = di2;
       }
    }
    return False;
}

inline bool hasBridge(const BattleTerrainHex& hi)
{
   return (hi.d_path1.d_type == LP_RoadBridge || hi.d_path2.d_type == LP_RoadBridge ||
         hi.d_path1.d_type == LP_TrackBridge || hi.d_path2.d_type == LP_TrackBridge);
}

inline bool hasRiver(const BattleTerrainHex& hi)
{
   return (hi.d_path1.d_type == LP_River || hi.d_path2.d_type == LP_River ||
         hi.d_path1.d_type == LP_SunkenRiver || hi.d_path2.d_type == LP_SunkenRiver);
}

bool BU_MoveImp::stillMovingOverBridge(const RefBattleCP& cp)
{
   bool stillOnBridge = False;

    if(cp->getRank().isHigher(Rank_Division))
    {
      if(!cp->attached())
        {
         HexItem* nextHex = (cp->routeList().entries() > 1) ?
            reinterpret_cast<HexItem*>(cp->nextHex()->next) : 0;

         if(nextHex)
         {
            const BattleTerrainHex& hexInfo = d_batData->getTerrain(nextHex->d_hex);
            if(hasRiver(hexInfo))
               {
                  stillOnBridge = True;
               }

               {
                  const BattleTerrainHex& hexInfo = d_batData->getTerrain(cp->hex());
                  if(hasRiver(hexInfo))
                  {
                     stillOnBridge = True;
                  }
                }
            }
        }
    }

    // if a Division
    else
    {
      // go through each SP and see if any occupy  river hex,
        // or will occupy a river hex
       for(std::vector<DeployItem>::iterator di = cp->mapBegin();
         di != cp->mapEnd();
            ++di)
        {
         if(di->active())
            {
            HexItem* nextHex = (di->d_sp->routeList().entries() > 1) ?
               reinterpret_cast<HexItem*>(di->d_sp->nextHex()->next) : 0;

            if(nextHex)
                {
               const BattleTerrainHex& hexInfo = d_batData->getTerrain(nextHex->d_hex);
                    if(hasRiver(hexInfo))
                    {
                     stillOnBridge = True;
                        break;
                    }
                }

                {
                  const BattleTerrainHex& hexInfo = d_batData->getTerrain(di->d_sp->hex());
                    if(hasRiver(hexInfo))
                    {
                     stillOnBridge = True;
                        break;
                    }
                }
            }
        }

    }

    if(!stillOnBridge)
    {
#ifdef DEBUG
      moveLog.printf("%s has completed moving across bridge", cp->getName());
#endif
      cp->movingOverBridge(False);
    }

    return stillOnBridge;
}

/*
 * Set Unit Speed
 *
 */

void BU_MoveImp::setUnitSpeed(const RefBattleCP& cp)
{
#ifdef DEBUG
   moveLog.printf("\n---------------------------------------");
   moveLog.printf("Setting speed for %s", cp->getName());
   if(cp->sp() != NoBattleSP)
   {
      moveLog.printf("SP fromFromation = %d, formationTween = %d",
             static_cast<int>(cp->sp()->fromFormation()),
             static_cast<int>(cp->sp()->formationTween()));
   }
#endif

    if(cp->movingOverBridge())
    {
      stillMovingOverBridge(cp);
    }

   if(cp->getRank().isHigher(Rank_Division))
   {
       if(cp->attached())
          return;

       enum {
          Normal,
          Evading,
          LValues_HowMany
       } lValue = Normal; // TODO: evading leader

       // TODO: move table to scenario tables
       // values are in yards per minute
       static const UWORD s_table[LValues_HowMany] = {
          180, 300
       };

       cp->speed(ypm(s_table[lValue]));
#ifdef DEBUG
       moveLog.printf("-------- final speed = %d yds per minute\n",
          static_cast<int>(s_table[lValue]));
#endif
       return;
   }

   /*
    * Speed is checked for each SP. The slowest speed is the
    * speed all SP in the unit will use.
    *
    */

   // convert unit type flags to local enum
   enum LocalUnitType {
      Inf,
      LCav,
      HCav,
      FootArt,
      HvyArt,
      HorseArt,

      End,
      Undefined = End
   } unitType = (cp->generic()->isInfantry())       ? Inf :
             (cp->generic()->isHeavyCavalry())   ? HCav :
             (cp->generic()->isCavalry())        ? LCav :
             (cp->generic()->isHeavyArtillery()) ? HvyArt :
             (cp->generic()->isHorseArtillery()) ? HorseArt :
             (cp->generic()->isArtillery())      ? FootArt : Undefined;

   ASSERT(!(unitType == Undefined));
   int realSpeed = 0;
   UWORD lowestMult = UWORD_MAX;

   int index;
   if(cp->routing())
      index = 6;
   else if((cp->generic()->isCavalry() || cp->generic()->isHorseArtillery()) && cp->charging())
      index = 5;
   else
      index = cp->sp()->fromFormation();
#ifdef DEBUG
   static const TCHAR* s_txt[End] = {
         "Infantry",
         "Light Cavalry",
         "Heavy Cavalry",
         "Foot Artillery",
         "Heavy Artillery",
         "Horse Artillery"
   };
   moveLog.printf("Unittype = %s, index = %d", s_txt[unitType], index);
#endif

   enum {
         March,
         Column,
         Other
   } formation = (cp->sp()->fromFormation() == SP_MarchFormation) ? March :
               (cp->sp()->fromFormation() == SP_ColumnFormation) ? Column : Other;

   const Table3D<UWORD>& tTable = BattleTables::terrainModifiers();
   const Table2D<UBYTE>& bsTable = BattleTables::basicMovementRate();
   const Table2D<UBYTE>& rdTable = BattleTables::roadMovementRate();


   bool inAJam = False;
   RoadType rt = RT_Undefined;
//    if(goOn)
   {
      /*
       * get basic speed
       */

      ASSERT(cp->sp()->fromFormation() < SP_Formation_HowMany);

      /*
       * Modifiy for terrain
       * Go through each SP to find lowest modifier
       */

      const Table3D<UWORD>& pTable = BattleTables::pathModifiers();
      const Table3D<UBYTE>& dTable = BattleTables::terrainDisorder();


      int nDisordered = 0;
      for(int r = 0; r < cp->rows(); r++)
      {
         int nInAJam = 0;
         for(int c = 0; c < cp->columns(); c++)
         {

            //for(vector<DeployItem>::iterator di = cp->mapBegin();
            //   di != cp->mapEnd();
            //   di++)
            DeployItem* di = cp->currentDeployItem(c, r);
            if((!di) ||
               (!di->active()) ||
               (!di->d_sp->isMoving()) )
            {
               continue;
            }

            // see if we are in a traffic jam
            // We are in a jam if hex is occupied by another XX
            //if(!inAJam)
            //inAJam = inATrafficJam(d_batData, cp, di->d_sp->hex());
            if(inATrafficJam(d_batData, cp, di->d_sp->hex()))
               nInAJam++;

            // get terrain
            const BattleTerrainHex& hexInfo = d_batData->getTerrain(di->d_sp->hex());

            int value = UWORD_MAX;

            // PathType pt;
            bool canGoOnRoad = (cp->formation() == CPF_March && cp->spFormation() == SP_MarchFormation);
            RoadType rt2;
            if(canGoOnRoad && movingOnRoad(di->d_sp, rt2))
            {
               value = 100;

               if(rt == RT_Undefined || rt2 > rt)
                  rt = rt2;
            }
            else
            {
               rt = NotOnRoad;
               // get terrain modifiers
               int v1 = tTable.getValue(unitType, hexInfo.d_terrainType, formation);
               //ASSERT(v1 != 0);

               // check path modifiers if not moving over bridge
               if(v1 > 0 && !cp->movingOverBridge())
               {
                  // get path modifiers (i.e streams, rivers)
                  for(int i = 0; i < 2; i++)
                  {
                     PathType pt = (i == 0) ? hexInfo.d_path1.d_type : hexInfo.d_path2.d_type;

                     if(pt != LP_None)
                     {
                        int v2 = pTable.getValue(unitType, pt, formation);
                        int v = (v1 * v2) / pTable.getResolution();
                        if(v < v1)
                           v1 = v;
                     }
                  }
               }

               // Count up sp that are in disordering terrain
               if(dTable.getValue(unitType, hexInfo.d_terrainType, formation))
                  nDisordered++;

               value = v1;

               // adjust for moving backwards
               if(cp->movingBackwards() && !cp->routing())
                  value *= .5;
            }

            if(value < lowestMult)
               lowestMult = value;
         }

         if(!inAJam && nInAJam * 2 >= cp->columns())
            inAJam = True;
      }

      // If in a traffic Jam
//    if(lowestMult != UWORD_MAX && inAJam)
//       lowestMult = maximum(1, lowestMult * .5);

      realSpeed = (rt == Road || rt == Track) ? rdTable.getValue(unitType, rt) : bsTable.getValue(unitType, index);
      ASSERT(realSpeed != 0);


#ifdef DEBUG
      moveLog.printf("Unmodified speed for %s is %d ypm", cp->getName(), realSpeed);
#endif
   }

   if(lowestMult != UWORD_MAX)
   {
      ASSERT(lowestMult <= 100);

      if(lowestMult < 100)
         realSpeed = MulDiv(realSpeed, lowestMult, tTable.getResolution());

      // modify speed for traffic jam
      if(inAJam)
      {
         realSpeed *= .5;
#ifdef DEBUG
         moveLog.printf("In a traffic jam! Speed = %d", realSpeed);
#endif
      }

      // modify for moving over bridge
      if(rt != Road && rt != Track)
      {
         // adjust speed for moving over bridge
         // since this is a momumental abstraction, we need a momumental adjustment
         // Just making this up for now based on Number of columns
         if(cp->movingOverBridge() && cp->columns() > 1)
         {
            // we'll make it simple at just divid by number of columns
            realSpeed /= cp->columns();
#ifdef DEBUG
            moveLog.printf("Speed after moving over bridge modifier = %d", realSpeed);
#endif
         }
      }
#ifdef DEBUG
      else
      {
         moveLog.printf("%s is moving on Road", cp->getName());
      }
#endif

      // the absolute minimum speed we can go is 10 ypm
      realSpeed = maximum(10, realSpeed);
#ifdef DEBUG
      moveLog.printf("-------- final speed = %d yds per minute\n", realSpeed);
#endif

      cp->speed(ypm(realSpeed));
      // if we have an attached unit
      RefBattleCP hq = BobUtility::getAttachedLeader(d_batData, cp);
      if(hq != NoBattleCP)
         hq->speed(ypm(realSpeed));
   }
}

inline Side getOtherSide(const Side& s)
{
    return  (s == 0) ? 1 : 0;
}

bool hexInList(const HexCord& hex, const HexList& list)
{
    SListIterR<HexItem> iter(&list);
    while(++iter)
    {
      if(iter.current()->d_hex == hex)
      return True;
    }

    return False;
}

// remove this unit from the map
void BU_MoveImp::removeUnit(const RefBattleCP& cp)
{
   for(std::vector<DeployItem>::iterator di = cp->mapBegin();
         di != cp->mapEnd();
         ++di)
   {
      if(di->active())
      {
         BattleHexMap::iterator from = d_batData->hexMap()->find(di->d_sp);
         d_batData->hexMap()->remove(from);

         di->d_wantSP = NoBattleSP;
         di->d_sp = NoBattleSP;
      }
   }
    
   if(cp->getRank().isHigher(Rank_Division))
   {
      BattleHexMap::iterator from = d_batData->hexMap()->find(cp);
      d_batData->hexMap()->remove(from);
   }

   cp->setFled();

   {
      if(B_LossUtil::isDefeated(d_batData, cp->getSide()))
      {
         B_LossUtil::procDefeated(d_batgame, (cp->getSide() == 0) ? 1 : 0, BattleResultTypes::Morale);
      }
      else if(B_LossUtil::shouldProcArmyDetermination(d_batData, cp->getSide()))
         B_LossUtil::procArmyDeterminationTest(d_batgame, cp->getSide());
   }

}

// Has unit reached mapedge and should be removed?
bool BU_MoveImp::shouldRemove(const RefBattleCP& cp)
{
   HexCord bLeft;
   HexCord mSize;
   d_batData->getPlayingArea(&bLeft, &mSize);
   HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());
   if(cp->getRank().sameRank(Rank_Division))
   {
      int rowsStillOnMap = 0;
      int nRows = 0;
      for(int r = 0; r < cp->rows(); r++)
      {
         DeployItem* di = cp->currentDeployItem(0, r);
         ASSERT(di);
         if(di && di->active())
         {
            if(di->d_sp->hex().x() >= bLeft.x() ||
               di->d_sp->hex().y() >= bLeft.y() ||
               di->d_sp->hex().x() < tRight.x() ||
               di->d_sp->hex().y() < tRight.y())
            {
               rowsStillOnMap++;
            }

            nRows++;
         }
         // End
      }

      if(nRows > 1)
      {
         if(cp->offMap())
         {
            if(rowsStillOnMap > 1)
               cp->offMap(False);
         }
         else
            return (rowsStillOnMap <= 1);
      }
      else
      {
         if(cp->offMap())
         {
            if(rowsStillOnMap > 0)
               cp->offMap(False);
         }
         else
            return (rowsStillOnMap == 0);
      }
   }
   else
   {
      bool allFled = True;
      for(TBattleUnitIter<BUIV_All> iter(cp, BUIV_All());
         !iter.isFinished();
         iter.next())
      {
         if(iter.cp() != cp)
         {
            if(!iter.cp()->hasQuitBattle())
            {
               allFled = False;
               break;
            }
         }
      }

      if(allFled &&
         (cp->hex().x() < bLeft.x() ||
          cp->hex().y() < bLeft.y() ||
          cp->hex().x() >= tRight.x() ||
          cp->hex().y() >= tRight.y()) )
      {
         return True;
      }

      if(cp->offMap() &&
         (cp->hex().x() >= bLeft.x() &&
          cp->hex().y() >= bLeft.y() &&
          cp->hex().x() < tRight.x() &&
          cp->hex().y() < tRight.y()) )
      {
         cp->offMap(False);
      }
   }

   return False;
}

bool BU_MoveImp::canMove(const RefBattleCP& cp, const RefBattleSP& sp, const int column, const int row)
{
   ASSERT(sp->nextHex());
   ASSERT(cp->deploying());
   ASSERT(column < cp->columns());
   ASSERT(row < cp->rows());

   HexCord wantHex = sp->nextHex()->d_hex;

   for(int r = 0; r < cp->rows(); r++)
   {
      for(int c = 0; c < cp->columns(); c++)
      {
         if( (r > row) ||
             (r == row &&
             (cp->deployWhichWay() == CPD_DeployLeft &&
              c >= column || cp->deployWhichWay() != CPD_DeployLeft && c <= column) ) )
         {
            r = cp->rows();
            break;
         }

         //DeployItem& di = map[(cp->columns() * r) + c];
         DeployItem* di = cp->currentDeployItem(c, r);
         ASSERT(di);
         if(!di || !di->active())
            continue;

         if( di->d_sp->isMoving() &&
             ((di->d_sp->hex() == wantHex && di->d_sp->hexPosition().distance() <= HexPosition::centerPos()) ||
              (di->d_sp->nextHex() && di->d_sp->nextHex()->d_hex == sp->nextHex()->d_hex)) )
         {
            sp->markingTime(True);
#ifdef DEBUG
            moveLog.printf("SP at column %d, row %d cannot continue", column, row);
            moveLog.printf("Is blocked by SP at column %d, row %d", c, r);
#endif
            return False;
         }
      }
   }

#ifdef DEBUG
   moveLog.printf("SP at column %d, row %d can continue", column, row);
#endif
   sp->markingTime(False);
   return True;
}

// See if dest hexes are already occupied
bool BU_MoveImp::destHexesOccupied(const RefBattleCP& cp)
{
   // if a XX
   if(cp->getRank().sameRank(Rank_Division))
   {

     // See if any other unit are occupying next hex
     // Iterate all SPs from top to bottom
      for(std::vector<DeployItem>::iterator di = cp->mapBegin();
        di != cp->mapEnd();
          di++)
     {
         ASSERT(cp->getRank().sameRank(Rank_Division));
         ASSERT(cp->sp() != NoBattleSP);

         if( (di->active()) &&
             (di->d_sp->nextHex()) &&
             (di->d_sp->strength() > 0) )
       {
#ifdef DEBUG
         if(!hexNotOccupied(d_batData, di->d_sp, cp, moveLog))
#else
         if(!hexNotOccupied(d_batData, di->d_sp, cp))
#endif
            {
               return True;
          }
         }
     }
     // End
   }

   else
   {
      if(cp->nextHex())
      {
#ifdef DEBUG
         if(!hexNotOccupied(d_batData, cp, cp, moveLog))
#else
         if(!hexNotOccupied(d_batData, cp, cp))
#endif
            return True;
      }
   }

    return False;
}


bool BU_MoveImp::canCrossBridge(const RefBattleCP& cp)
{
   // can only cross if on a normal movement
   if( (cp->deploying() || cp->onManuever()) )
//       (cp->getRank().sameRank(Rank_Division) && (cp->spFormation() == SP_LineFormation || cp->spFormation() == SP_SquareFormation)) )
    {
      return False;
    }

    // HQ
   if(cp->getRank().isHigher(Rank_Division))
    {
      if(!cp->attached())
        {
         HexItem* nextHex = (cp->routeList().entries() > 1) ?
            reinterpret_cast<HexItem*>(cp->nextHex()->next) : 0;

         if(nextHex)
         {
            const BattleTerrainHex& hexInfo = d_batData->getTerrain(nextHex->d_hex);
               if(hasBridge(hexInfo))
               {
               cp->movingOverBridge(True);
                }
            }
        }
    }

   // a Division
   else
   {
      // if any front line SP next hex is a bridge, whole CP can cross
      for(int r = 0; r < cp->rows(); r++)
      {
         for(int c = 0; c < cp->columns(); c++)
         {
            DeployItem* di = cp->currentDeployItem(c, r);
            ASSERT(di);
            if(di && di->active())
            {
               HexItem* nextHex = (di->d_sp->routeList().entries() > 1) ?
                  reinterpret_cast<HexItem*>(di->d_sp->nextHex()->next) : 0;

               if(nextHex)
               {
                  const BattleTerrainHex& hexInfo = d_batData->getTerrain(nextHex->d_hex);
                  if(hasBridge(hexInfo))
                  {
#ifdef DEBUG
                     moveLog.printf("SP (c=%d, r=%d) of %s wants to cross bridge",
                              c, r, cp->getName());
#endif
                     cp->movingOverBridge(True);
                           break;
                  }
               }
            }
         }
      }
   }
   return cp->movingOverBridge();
}

bool BU_MoveImp::canContinue(const RefBattleCP& cp, bool* changingFormation)
{
    // If we are crossing a bridge, other terrain doesn't matter
    bool crossingBridge = (cp->movingOverBridge() || canCrossBridge(cp));
//     return True;

    bool canGoOn = True;

    // convert unit type flags to local enum
    enum {
         Inf,
         LCav,
         HCav,
         FootArt,
         HvyArt,
         HorseArt,

         End,
         Undefined = End
    } unitType = (cp->getRank().isHigher(Rank_Division) || cp->generic()->isInfantry()) ? Inf :
             (cp->generic()->isHeavyCavalry())   ? HCav :
             (cp->generic()->isCavalry())        ? LCav :
             (cp->generic()->isHeavyArtillery()) ? HvyArt :
             (cp->generic()->isHorseArtillery()) ? HorseArt :
             (cp->generic()->isArtillery())      ? FootArt : Undefined;


   ASSERT(!(unitType == Undefined));   // !(a==b) to prevent Visual C++ problem with !=

    enum LFEnum {
         March,
       Column,
       Other
    } formation = (cp->sp() == NoBattleSP || cp->sp()->fromFormation() == SP_MarchFormation) ? March :
                  (cp->sp()->fromFormation() == SP_ColumnFormation) ? Column : Other;

   const Table3D<UBYTE>& tTable = BattleTables::prohibitedTerrain();

    /*
     * go through each sp in front row and
     * see if they are moving into prohibited terrain
     */

   LFEnum lf = formation;
   if(cp->getRank().sameRank(Rank_Division))
   {
      for(int r = 0; r < cp->rows(); r++)
      {
         for(int c = 0; c < cp->columns(); c++)
         {
            //int index = (cp->columns() * r) + c;
            //DeployItem& di = map[index];
            DeployItem* di = cp->currentDeployItem(c, r);
            ASSERT(di);

            if(!di || !di->active() || !di->d_sp->isMoving())
               continue;

            HexItem* nextHex = (di->d_sp->routeList().entries() > 1) ?
               reinterpret_cast<HexItem*>(di->d_sp->nextHex()->next) : 0;

            if(nextHex)
            {
               const BattleTerrainHex& hexInfo = d_batData->getTerrain(nextHex->d_hex);
               canGoOn = tTable.getValue(unitType, hexInfo.d_terrainType, lf) != 0;

               // if terrain type is not prohibited, check path type (i.e. Rivers)
               if(!crossingBridge && canGoOn)
               {
                     const Table3D<UWORD>& pTable = BattleTables::pathModifiers();
                     // get path modifiers (i.e streams, rivers)

                     // If we have a water obstacle
                     if( (pTable.getValue(unitType, hexInfo.d_path1.d_type, lf) == 0) ||
                         (pTable.getValue(unitType, hexInfo.d_path2.d_type, lf) == 0) )
                     {
                        canGoOn = False;
                     }
               }

               if(!canGoOn)  // prohibited terrain. See if we are at a road and in march formation
               {
                  if(lf == SP_MarchFormation && roadHex(d_batData, nextHex->d_hex))
                        canGoOn = True;
                  else
                  {

                     // if we are, see if we can change formation
                     if(lf > 0)
                     {
#ifdef _MSC_VER   // Visual C++ wont use local types in templates
                        lf = static_cast<LFEnum>(lf - 1);
#else
                        DECREMENT(lf);
#endif
                        c--;
                     }
                     else
                     {
                        r = cp->rows();
                        break;
                     }
                  }
               }
            }
         }
      }
    }

   else
   {
      HexItem* nextHex = (cp->routeList().entries() > 1) ? reinterpret_cast<HexItem*>(cp->nextHex()->next) : 0;
      if(nextHex)
      {
         if(crossingBridge)
            canGoOn = True;
         else
         {
            const BattleTerrainHex& hexInfo = d_batData->getTerrain(nextHex->d_hex);
            canGoOn = tTable.getValue(unitType, hexInfo.d_terrainType, lf) != 0;

            // if terrain type is not prohibited, check path type (i.e. Rivers)
            if(canGoOn)
            {
               const Table3D<UWORD>& pTable = BattleTables::pathModifiers();
               // get path modifiers (i.e streams, rivers)

               // If we have a water obstacle
               if( (pTable.getValue(unitType, hexInfo.d_path1.d_type, lf) == 0) ||
                   (pTable.getValue(unitType, hexInfo.d_path2.d_type, lf) == 0) )
               {
                  canGoOn = False;
               }
            }
         }
      }
   }

   // if we can go on
   if(canGoOn)
   {
      // if changing formation, do that
      if(!(lf == formation))  // use !(a==b) to get round Visual C++ problem with !=
      {
         cp->getCurrentOrder().spFormation(static_cast<SPFormation>(lf));
         cp->getCurrentOrder().whatOrder().add(WhatOrder::SPFormation);
         canGoOn = False;
         if(changingFormation)
            *changingFormation = True;
      }
   }

   return canGoOn;
}

bool BU_MoveImp::updateArtillery(const RefBattleCP& cp)
{
   // if this is an artillery unit, limber them up if not already so
   // TODO: sort out prolonging
   if( (cp->getRank().sameRank(Rank_Division)) &&
      (cp->generic()->isArtillery()) &&
       (cp->sp() != NoBattleSP) &&
       (cp->sp()->formation() != SP_LimberedFormation) )
   {
      cp->getCurrentOrder().spFormation(SP_LimberedFormation);
      cp->getCurrentOrder().whatOrder().insert(WhatOrder::SPFormation);
      return True;
   }

    return False;
}

void BU_MoveImp::doMove(const RefBattleCP& cp)
{
   // Unchecked
// setUnitSpeed(cp);
   // End
   cp->allSPAtCenter(True);
   cp->rallying(False);
   runMovement(cp);
}

#ifdef DEBUG
// do instant movement for debugging
void BU_MoveImp::instantMove(const RefBattleCP& cp)
{
   HexPosition::Facing facing = cp->facing();
   if(cp->sp() != NoBattleSP)
   {
      for(std::vector<DeployItem>::iterator di = cp->mapBegin();
         di != cp->mapEnd();
             di++)
    {
      if(di->active() && di->d_sp->nextHex())
      {
        di->d_sp->setDeploying(False);
        di->d_sp->setMoving(False);
        di->d_sp->setFacing(di->d_sp->routeList().getLast()->d_facing);

             if(di->d_sp->routeList().getLast()->d_facing != facing)
               facing = di->d_sp->routeList().getLast()->d_facing;

        placeUnit(di->d_sp, di->d_sp->routeList().getLast()->d_hex);
         }
      }
  }
   // do cp
  else
   {
    if(cp->nextHex())
    {
         cp->setDeploying(False);
         cp->setMoving(False);
         placeUnit(cp, cp->routeList().getLast()->d_hex);
    }
   }

   if( (cp->deploying() && cp->formation() == cp->nextFormation()) ||
      (cp->moving()) )
   {
      cp->moveMode(BattleCP::Holding);
  }

  if(cp->facing() != facing)
    cp->setFacing(facing);
}
#endif

/*
 * start deployment process
 * Note: deployment is done incrementally
 * i.e. If in March formation and you want to go to Extended formation
 * then you must first go through Massed, and then Deployed
 */

bool BU_MoveImp::startDeploy(const RefBattleCP& cp)
{
   ASSERT(cp != NoBattleCP);
   ASSERT(!cp->deploying());
   ASSERT(cp->formation() != cp->getCurrentOrder().divFormation());

   if(cp->movingOverBridge() && cp->formation() != CPF_March)
      return False;

   const int spCount = cp->spCount();
   ASSERT(spCount > 0);

   const int c_maxSP = 12;
   ASSERT(spCount  - 1 <  c_maxSP);

   static bool s_needsDeploy[CPF_Last][c_maxSP] = {
      { False, False, False, False, False, False, False, False, False, False, False, False }, // March
      { False, True,  True,  True,  True,  True,  True,  True,  True,  True,  True,  True  }, // Massed
      { False, False, False, False, True,  True,  True,  True,  True,  True,  True,  True  }, // Deployed
      { False, False, True,  True,  True,  True,  True,  True,  True,  True,  True,  True  }  // Extended
   };

   static bool s_needsRedeploy[CPF_Last][c_maxSP] = {
      { False, True,  True,  True,  True,  True,  True,  True,  True,  True,  True,  True  }, // March
      { False, False, False, False, True,  True,  True,  True,  True,  True,  True,  True  }, // Massed
      { False, True,  True,  True,  True,  True,  True,  True,  True,  True,  True,  True  }, // Deployed
      { False, False, False, False, False, False, False, False, False, False, False, False }  // Extended
   };

   // get increment value
   int by = (cp->formation() < cp->getCurrentOrder().divFormation()) ? + 1 : -1;
   CPFormation nextFormation = clipValue(
      static_cast<CPFormation>(cp->formation() + by), 
      CPF_First, 
      CPF_Last);

   bool shouldDeploy = (cp->formation() < nextFormation) ?
      s_needsDeploy[nextFormation][spCount - 1] : s_needsRedeploy[nextFormation][spCount - 1];

   if(!shouldDeploy)
   {
      cp->formation(nextFormation);
      cp->nextFormation(nextFormation);
      return True;
   }

   else if(!updateArtillery(cp))
   {
      // determine next formation
      cp->nextFormation(nextFormation);

      // deploy to next formation
      ASSERT(cp->nextFormation() < CPF_Last);
      if(d_formations.deploy(cp) && safeMove(cp))
      {
         // set deployment flag
         cp->moveMode(BattleCP::Deploying);

#ifdef DEBUG
         moveLog.printf("------ Deploying %s --------------", cp->getName());

         // go through and halt all units
         for(std::vector<DeployItem>::iterator di = cp->mapBegin();
             di != cp->mapEnd();
             di++)
         {
            if(di->active())
            {
               moveLog.printf("--------------- Route for SP %ld at hex %d, %d (str = %d)",
                  reinterpret_cast<LONG>(di->d_sp),
                  static_cast<int>(di->d_sp->hex().x()),
                  static_cast<int>(di->d_sp->hex().y()),
                  static_cast<int>(di->d_sp->strength()));

               SListIterR<HexItem> riter(&di->d_sp->routeList());
               while(++riter)
               {
                  moveLog.printf("-------------------------- hex %d, %d",
                     static_cast<int>(riter.current()->d_hex.x()),
                     static_cast<int>(riter.current()->d_hex.y()));
               }
            }

            moveLog.printf("\n");
         }
#endif
         doMove(cp);
      }
      else
      {
         if(GamePlayerControl::getControl(cp->getSide()) == GamePlayerControl::AI)
         {
            BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_UnableToDeploy, cp);
            d_batgame->sendPlayerMessage(msg);
         }
         clearMove(cp, True);
      }

      return True;
   }

   return False;

}

bool BU_MoveImp::startManuever(const RefBattleCP& cp)
{
    ASSERT(cp != NoBattleCP);
    ASSERT(!cp->onManuever());

   // set deployment flag
   if(!cp->movingOverBridge() && !updateArtillery(cp)) // &&
   {
      cp->moveMode(BattleCP::OnManuever);
      ASSERT(cp->getCurrentOrder().manuever() < CPM_Last);
      d_manuevers.startManuever(cp);
#ifdef DEBUG
      moveLog.printf("\n-------- Starting Manuever for %s(%s) - %s",
         cp->getName(), scenario->getSideName(cp->getSide()),
             BattleOrderInfo::manueverName(cp->getCurrentOrder().manuever()));

      // go through and halt all units
      for(std::vector<DeployItem>::iterator di = cp->mapBegin();
               di != cp->mapEnd();
               di++)
      {
         if(di->active())
         {
            moveLog.printf("%ld (%s) has %d route nodes",
               reinterpret_cast<LONG>(di->d_sp), d_batData->ob()->spName(di->d_sp),
            static_cast<int>(di->d_sp->routeList().entries()));
         }
      }

#endif
      if(cp->onManuever() && safeMove(cp))
      {
         doMove(cp);
         return True;
      }
      else
         return False;
   }

   return False;
}

bool BU_MoveImp::startMovement(const RefBattleCP& cp)
{
   ASSERT(cp != NoBattleCP);
   ASSERT(!cp->onManuever());
   ASSERT(cp->getCurrentOrder().nextWayPoint());
#ifdef DEBUG
   HexCord leftHex = cp->leftHex();
   HexCord destHex = cp->getCurrentOrder().nextWayPoint()->d_hex;
   moveLog.printf("Starting movement for %s", cp->getName());
   moveLog.printf("------ will attempt to move from hex(%d, %d) to hex(%d, %d)",
      static_cast<int>(leftHex.x()), static_cast<int>(leftHex.y()),
      static_cast<int>(destHex.x()), static_cast<int>(destHex.y()));
#endif

    // set deployment flag
   bool artyUpdating = updateArtillery(cp);
   if( (cp->sp() == NoBattleSP || cp->getCurrentOrder().nextWayPoint()->d_hex == cp->leftHex() || !artyUpdating) &&
       (d_movement.startMovement(cp)) &&
       (safeMove(cp)) )
   {
         doMove(cp);
         return True;
   }
   else if(!artyUpdating)
      finalMove(cp);

   return False;
}

bool BU_MoveImp::startReorganize(const RefBattleCP& cp)
{
    // set deployment flag
    cp->moveMode(BattleCP::Deploying);
    // Unchecked
//    setUnitSpeed(cp);
#ifdef DEBUG
     moveLog.printf("Starting reorganization for %s", cp->getName());
#endif
      // End
     RefBattleCP attachedTo = BobUtility::getAttachedLeader(d_batData, cp);
     if(attachedTo != NoBattleCP)
     {
      plotHQRoute(d_batData, attachedTo);
        attachedTo->setMoving(True);
     }

    return True;
}

bool BU_MoveImp::startSPFormationChange(const RefBattleCP& cp)
{
    ASSERT(cp != NoBattleCP);
    ASSERT(cp->getRank().sameRank(Rank_Division));
    ASSERT(!cp->liningUp());
    if(cp->sp() != NoBattleSP && !BobUtility::changingSPFormation(cp)) //cp->spFormation() != SP_ChangingFormation)
    {
         // set deployment flag
         cp->rallying(False);
         cp->moveMode(BattleCP::LiningUp);
         ASSERT(cp->getCurrentOrder().spFormation() < SP_Formation_HowMany);

#ifdef DEBUG
         moveLog.printf("Starting SPFormation change for %s", cp->getName());
         moveLog.printf("Changing from %s to %s",
            BattleOrderInfo::spFormationName(cp->spFormation()),
            BattleOrderInfo::spFormationName(cp->getCurrentOrder().spFormation()));
#endif
         return d_spFormations.startChange(cp);

//      return True;
    }

   return False;
}

/*
 * Determine next hex we should move to
 */

void BU_MoveImp::getNextHex(BattleUnit* bu)
{
    ASSERT(bu);

    if(bu->nextHex())
    {
      bu->removeRouteNode();
//  if(bu->nextHex())
//    bu->setRFacing(bu->nextHex()->d_facing);
    }
}

bool BU_MoveImp::moveAlong(BattleUnit* bu, const Distance moveDist, HexPosition::WhereAt where)
{
    switch(where)
    {
#ifdef DEBUG
    case HexPosition::MovingToCenter:
    case HexPosition::MovingToEdge:
         // do nothing
      break;
#endif

      case HexPosition::CenterHex:
         // see if we need to change position facing
      if(bu->nextHex())
         {
//      ASSERT(bu->nextHex()->d_hex != bu->hex());
//      bu->hexPosition().facing(moveDirection(bu->hex(), bu->nextHex()->d_hex));
      }
      else
             return False;
//      return (bu->nextFacing() != bu->facing());

      break;

    case HexPosition::EdgeOfHex:
         // get next hex
      ASSERT(bu->nextHex());
      placeUnit(bu, bu->nextHex()->d_hex);
      break;

#ifdef DEBUG
      default:
         FORCEASSERT("Improper whichWay");
#endif
   }

    return True;
}

HexPosition::Facing BU_MoveImp::getFace(const RefBattleCP& cp, HexPosition::Facing rf, HexPosition::Facing mf)
{
    if( (cp->deploying() && cp->formation() > cp->nextFormation()) ||
      (cp->onManuever() && (cp->getCurrentOrder().manuever() == CPM_RWheelL || cp->getCurrentOrder().manuever() == CPM_RWheelR)) )
    {
    // if moving backwards, return opposite of wFace
      // convert facings to local enums
    enum MoveFace {
       EF, NEF, NWF, WF, SWF, SEF,
          MoveFace_HowMany, MoveFace_Undefined = MoveFace_HowMany
      } moveFace = (mf == HexPosition::East) ? EF :
                  (mf == HexPosition::NorthEastFace) ? NEF :
                 (mf == HexPosition::NorthWestFace) ? NWF :
                 (mf == HexPosition::West) ? WF :
                 (mf == HexPosition::SouthWestFace) ? SWF :
                 (mf == HexPosition::SouthEastFace) ? SEF : MoveFace_Undefined;

      ASSERT(!(moveFace == MoveFace_Undefined));

      enum RFace {
         NE, N, NW, SW, S, SE,
         RFace_HowMany, RFace_Undefined = RFace_HowMany
    } rFace =    (rf == HexPosition::NorthEastPoint) ? NE :
                         (rf == HexPosition::North) ? N :
                         (rf == HexPosition::NorthWestPoint) ? NW :
                 (rf == HexPosition::SouthWestPoint) ? SW :
                         (rf == HexPosition::South) ? S :
                 (rf == HexPosition::SouthEastPoint) ? SE : RFace_Undefined;

    ASSERT(!(rFace == RFace_Undefined));
      static const bool s_isOpposite[RFace_HowMany][MoveFace_HowMany] = {
         // E      NEF    NWF    W      SWF   SEF
      { False, False, False, True,  True,  False }, // NE
      { False, False, False, False, True,  True  }, // N
      { True,  False, False, False, False, True  }, // NW
         { True,  True,  False, False, False, False }, // SW
      { False, True,  True,  False, False, False }, // S
         { False, False, True,  True,  False, False }  // SE
      };

      return (s_isOpposite[rFace][moveFace]) ? oppositeFace(mf) : mf;
    }

    return mf;
}
#if 0
void convertToCenter(DeployItem& di)
{
   if(di.d_sphexPosition().moveTo() != di.d_sp->d_hexPosition().moveFrom())
   {
const HexPosition::MoveTo m_ef  = HexPosition::East_Face;
const HexPosition::MoveTo m_nef = HexPosition::NE_Face;
const HexPosition::MoveTo m_nwf = HexPosition::NW_Face;
const HexPosition::MoveTo m_wf  = HexPosition::West_Face;
const HexPosition::MoveTo m_swf = HexPosition::SW_Face;
const HexPosition::MoveTo m_sef = HexPosition::SE_Face;
const HexPosition::MoveTo m_ce  = HexPosition::Center_East;
const HexPosition::MoveTo m_cne  = HexPosition::Center_NE;
const HexPosition::MoveTo m_cnw  = HexPosition::Center_NW;
const HexPosition::MoveTo m_cw  = HexPosition::Center_West;
const HexPosition::MoveTo m_csw  = HexPosition::Center_SW;
const HexPosition::MoveTo m_cse  = HexPosition::Center_SE;
const HexPosition::MoveTo m_cc  = HexPosition::Center_Center;
const HexPosition::MoveTo m_ud  = HexPosition::MoveTo_Undefined;
      HexPosition::MoveTo s_moveTo[HexPosition::MoveTo_HowMany][HexPosition::MoveTo_HowMany] = {
         // E     NE      NW     W     SW       SE    CE      CNE    CNW    CW     CSW    CSE  CC
         { m_ce,  m_cne, m_cne, m_cc,  m_cse, m_cse,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud, m_cc }, //w,   nwp, nwp, w,   swp, swp, ud }, //w   }, // E
         { m_ce,  m_cne, m_cne, m_cc,  m_cse, m_cse,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud, m_cc }, //w,   nwp, nwp, w,   swp, swp, ud }, //w   }, // E
         { m_ce, m_cne, m_cne, m_cc,  m_cse, m_cse,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud, m_cc }, //w,   nwp, nwp, w,   swp, swp, ud }, //w   }, // E
         { m_ce, m_cne, m_cne, m_cc,  m_cse, m_cse,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud, m_cc }, //w,   nwp, nwp, w,   swp, swp, ud }, //w   }, // E
         { m_ce, m_cne, m_cne, m_cc,  m_cse, m_cse,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud, m_cc }, //w,   nwp, nwp, w,   swp, swp, ud }, //w   }, // E
         { m_ce, m_cne, m_cne, m_cc,  m_cse, m_cse,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud,  m_ud, m_cc }, //w,   nwp, nwp, w,   swp, swp, ud }, //w   }, // E

         { e,   n,   nwp, w,   swp, s,    ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CE
         { sep, se,  nwp, swp, sw,  s,    ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CNE
         { sep, nep, nw,  swp, s,   se,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CNW
         { e,   nep, n,   w,   s,   sep,  ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CW
         { nep, ne,  n,   nwp, sw,  sep,  ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CSW
         { nep, n,   nw,  nwp, swp, se,   ud,  ud,  ud,  ud,  ud,  ud,  ud  }, // CSE

         { e,   ne,  nw,  w,   sw,  se,   ud,  ud,  ud,  ud,  ud,  ud,  ud  } // CC
      };

      di.d_sphexPosition.moveTo(s_moveTo[di.d_sp->hexPosition().moveFrom()][di.d_sp->hexPosition().moveTo()];
   }
}
#endif
#if 0
// cancel route, but don't change move flags so facing will be updated
void BU_MoveImp::cancelRoute(const RefBattleCP& cp)
{
    cp->routeList().reset();
    // go through and halt all units
    for(std::vector<DeployItem>::iterator di = cp->deployMap().begin();
      di != cp->deployMap().end();
      di++)
   {
      if(di->active())
      di->d_sp->routeList().reset();
   }
}
#endif
// clear out routes, waypoints, etc.
void BU_MoveImp::clearMove(const RefBattleCP& cp, bool hold)
{
    // Unchecked
#ifdef DEBUG
    moveLog.printf("Clearing move for %s", cp->getName());
#endif
    // End
    cp->retreating(False);
    if(hold || cp->getCurrentOrder().mode() == BattleOrderInfo::Hold)
    {
         cp->getCurrentOrder().wayPoints().reset();
         cp->wantColumns(cp->columns());
         cp->wantRows(cp->rows());
    }
    for(std::vector<DeployItem>::iterator di = cp->mapBegin();
      di != cp->mapEnd();
      di++)
   {
      if(di->active())
      {
         di->d_sp->routeList().reset();
//         di->d_sp->setMoving(False);
         di->syncHexToSP();
      }
   }

   // do attached leader if any
   RefBattleCP hq = BobUtility::getAttachedLeader(d_batData, cp);
   if(hq != NoBattleCP)
   {
      hq->setMoving(False);
      hq->routeList().reset();
   }

   if(cp->getRank().isHigher(Rank_Division))
   {
      if(cp->attached() && cp->getCurrentOrder().mode() != BattleOrderInfo::Attach)
         cp->setUnattached();

      cp->setMoving(False);
      cp->routeList().reset();
   }

   // Unchecked
   cp->speed(0);
   // End
}

/*
 * Process moving units
 */

inline void setFlag(UBYTE mask, bool f, UBYTE& flag)
{
   if(f)
      flag |= mask;
   else
      flag &= ~mask;
}

// Unchecked
bool BU_MoveImp::changingFace(const RefBattleCP& cp)
{
  bool changing = False;

  for(std::vector<DeployItem>::iterator di = cp->mapBegin();
      di != cp->mapEnd();
         di++)
  {
    if(!di->active() || !di->d_sp->isMoving())
      continue;

      bool thisSPChanging = False;
    // process if changing facing
    if(di->d_sp->formation() == SP_ChangingFormation)
      {
#ifdef DEBUG
         moveLog.printf("SP %ld (%s str = %d) is changing face. FormationTween = %d",
                reinterpret_cast<LONG>(di->d_sp),
                d_batData->ob()->spName(di->d_sp),
           static_cast<int>(di->d_sp->strength()),
                di->d_sp->formationTween());
#endif
         if( !(d_spFormations.runFacingChange(di->d_sp)) )
         {
            thisSPChanging = changing = True;
         }
      }

    // process if we need to change facing
    if(!(thisSPChanging))
      {
         if(di->d_sp->wantFacing() != di->d_sp->nextFacing())
         {
#ifdef DEBUG
            moveLog.printf("\n----- SP %ld (%s str = %d) is starting face change (%s to %s)",
                reinterpret_cast<LONG>(di->d_sp),
                d_batData->ob()->spName(di->d_sp),
           static_cast<int>(di->d_sp->strength()),
                faceName(di->d_sp->facing()), faceName(di->d_sp->nextFacing()));
#endif
            d_spFormations.startFacingChange(di->d_sp);
            changing = True;
         }
      }
   }

  return changing;
}
// end
inline void updateFacing(BattleUnit* bu)
{
#ifdef DEBUG
   moveLog.printf("SP is setting final facing");
#endif

    bu->nextFacing(bu->rFacing());
}

inline void finishMove(BattleUnit* bu)
{
#ifdef DEBUG
    moveLog.printf("Unit has finished moving (dist = %d)", static_cast<int>(bu->hexPosition().distance()));
#endif
    bu->setMoving(False);
}

/*
 * Physically move units
 */

bool BU_MoveImp::moveUnit(const RefBattleCP& cp, BattleUnit* bu, Distance moveDist, bool& stillMoving)
{
   HexPosition::WhereAt wa = bu->hexPosition().moveAlong(moveDist);
   if(wa == HexPosition::EdgeOfHex)
   {
      cp->speed(0);
#ifdef DEBUG
      moveLog.printf("Setting next facing for SP");
#endif
      setNextFacing(bu);
   }

   if(moveAlong(bu, moveDist, wa))
   {
      stillMoving = True;
   }

#ifdef DEBUG
   moveLog.printf("SP after move = %ld", bu->hexPosition().distance());
   moveLog.printf("------- Move-Facing -- %s", faceName(bu->hexPosition().facing()));
   moveLog.printf("------- Relative-Facing -- %s", faceName(bu->facing()));
#endif

   return (wa == HexPosition::CenterHex);
}

bool BU_MoveImp::shouldMoveUnit(const RefBattleCP& cp, BattleUnit* bu, bool& stillMoving)
{
   bool thisUnitMoving = True;
   if(cp->allSPAtCenter())
   {
      ASSERT(bu->hexPosition().atCenter());

      // Bodge, moving position back to center
      if(!bu->hexPosition().atCenter())
      {
#ifdef DEBUG
         moveLog.printf("WARNING! SP is out of sync(Not at centerPos, dist = %d)",
            bu->hexPosition().distance());
#endif
         if(bu->hexPosition().distance() >= HexPosition::centerPos())
            bu->hexPosition().moveAlong(HexPosition::centerPos());

         bu->hexPosition().moveAlong(HexPosition::centerPos());
      }

      // if at the end of the line
      if(!bu->nextHex())
      {
         thisUnitMoving = False;
         if(bu->nextFacing() == bu->facing())
         {
            if(bu->facing() != bu->rFacing())
            {
               updateFacing(bu);
               stillMoving = True;
            }
            else
               finishMove(bu);
         }
      }

      else
      {
#ifdef DEBUG
         HexCord nextHex = bu->nextHex()->d_hex;
#endif
         if(bu->hex() == bu->nextHex()->d_hex)
         {
            stillMoving = True;

            if(!cp->moveStarted())
            {
               //getSpeed = True;
               cp->moveStarted(True);
            }

            if(cp->moveStarted())//getSpeed)
               bu->setRFacing(bu->nextHex()->d_facing);

            getNextHex(bu);
         }

         if(bu->nextHex())
         {
            if(bu->nextHex()->d_hex == bu->hex())
            {
               return False;
            }
            else
            {
               bu->hexPosition().facing(moveDirection(bu->hex(), bu->nextHex()->d_hex));
#ifdef DEBUG
               moveLog.printf("Setting next facing for SP");
#endif
               setNextFacing(bu);

            }
         }
         else
         {
            thisUnitMoving = False;
            if(bu->facing() != bu->rFacing())
            {
               updateFacing(bu);
               stillMoving = True;
            }
            else
               finishMove(bu);
         }
      }
   }

   else if(bu->hexPosition().atCenter())
   {
      if(!bu->nextHex() && bu->facing() == bu->rFacing())
         finishMove(bu);

      thisUnitMoving = False;
   }

   return thisUnitMoving;
}

bool BU_MoveImp::moving(const RefBattleCP& cp)
{
#ifdef DEBUG
   moveLog.printf("\n---------------------------------------");
   moveLog.printf("Processing move for %s", cp->getName());
#endif

   if(cp->speed() == 0)
      setUnitSpeed(cp);

   bool allAtCenter = False;
   RefBattleCP hq = (cp->getRank().sameRank(Rank_Division)) ?
   BobUtility::getAttachedLeader(d_batData, cp) : cp;

   bool stillMoving = changingFace(cp);
   //bool getSpeed = False;

   // see if we can continue movement
   if(cp->allSPAtCenter() && destHexesOccupied(cp))
   {
      // if terrain is passable
      clearMove(cp, !canContinue(cp));
      allAtCenter = True;
   }

   // we can continue
   else
   {
      if(cp->allSPAtCenter())
      {
         bool changingSPFormation = False;
         if(!canContinue(cp, &changingSPFormation))
         {
            if(!changingSPFormation)
               clearMove(cp, True);

            return False;
         }
      }

      Distance moveDist = 0;//distanceTraveled(d_batData->getTick() - d_lastTick, cp->speed());

      /*
       * Move each SP as needed
       */

      for(int r = 0; r < cp->rows(); r++)
      {
         for(int c = 0; c < cp->columns(); c++)
         {
            //DeployItem& di = deployMap[(cp->columns() * r) + c];
            DeployItem* di = cp->currentDeployItem(c, r);
            ASSERT(di);

            // skip any that are inactive (i.e. no SP in that hex) or not moving
            if(!di || !di->active() || !di->d_sp->isMoving())
               continue;

#ifdef DEBUG
            if(cp->allSPAtCenter())
            {
               moveLog.printf("Route for SP %ld (%s str = %d) (at Hex (%d, %d))------------",
                  reinterpret_cast<LONG>(di->d_sp),
                  d_batData->ob()->spName(di->d_sp),
                  static_cast<int>(di->d_sp->strength()),
                  static_cast<int>(di->d_sp->hex().x()), static_cast<int>(di->d_sp->hex().y()));

               SListIterR<HexItem> rIter(&di->d_sp->routeList());
               while(++rIter)
               {
                  moveLog.printf("---------------- x = %d, y = %d",
                      static_cast<int>(rIter.current()->d_hex.x()),
                      static_cast<int>(rIter.current()->d_hex.y()));
               }
            }
#endif

            if(shouldMoveUnit(cp, di->d_sp, stillMoving))
            {
               if(moveDist == 0)
                  moveDist = distanceTraveled(d_batData->getTick() - d_lastTick, cp->speed());

               if(moveUnit(cp, di->d_sp, moveDist, stillMoving))
               {
                  allAtCenter = True;
               }
            }
         }
      }

      // move HQ
      if(hq != NoBattleCP)
      {
#ifdef DEBUG
         moveLog.printf("Moving HQ(%s) of %s", (hq == cp) ? "Unattached" : "Attached", hq->getName());
#endif
         // if attached to this unit
         if(hq != cp)
         {
            // find unit that we share hex with and copy
            if(hq->companionSP() != NoBattleSP)
            {
               // do a direct copy
               if(hq->hex() != hq->companionSP()->hex())
               {
                  //placeUnit(hq, hq->companionSP()->hex());
                  BattleHexMap::iterator from = d_hexMap->find(hq);
#ifdef DEBUG
                  d_batData->assertHexOnMap(hq->companionSP()->hex());
#endif
                  hq->position(hq->companionSP()->position());
                  hq->position().hexPosition().moveTo(hq->companionSP()->position().hexPosition().moveTo());
                  hq->position().hexPosition().moveFrom(hq->companionSP()->position().hexPosition().moveFrom());
                  hq->setRFacing(hq->companionSP()->rFacing());
                  hq->setFacing(hq->companionSP()->facing());
                  d_hexMap->move(from);
               }
               else
               {
                  hq->position(hq->companionSP()->position());
                  hq->position().hexPosition().moveTo(hq->companionSP()->position().hexPosition().moveTo());
                  hq->position().hexPosition().moveFrom(hq->companionSP()->position().hexPosition().moveFrom());
                  hq->setRFacing(hq->companionSP()->rFacing());
                  hq->setFacing(hq->companionSP()->facing());
               }

               hq->allSPAtCenter(allAtCenter);
            }
            else
               FORCEASSERT("HQ not where it should be!");
         }

         // not attached
         else
         {
            if(shouldMoveUnit(cp, cp, stillMoving))
            {
               if(moveDist == 0)
                  moveDist = distanceTraveled(d_batData->getTick() - d_lastTick, cp->speed());

               if(moveUnit(cp, cp, moveDist, stillMoving))
               {
                  allAtCenter = True;
               }
            }
         }
      }

#ifdef DEBUG
      moveLog.printf("move distance = %ld", moveDist);
#endif
   }

   cp->allSPAtCenter(allAtCenter);
   return stillMoving;
}

#if 0
bool BU_MoveImp::moving(const RefBattleCP& cp)
{
#ifdef DEBUG
   moveLog.printf("\n---------------------------------------");
   moveLog.printf("Processing move for %s", cp->getName());

   if(Options::get(OPT_InstantMove))
   {
      instantMove(cp);
      return False;
   }
#endif

   if(cp->speed() == 0)
      setUnitSpeed(cp);

   bool allAtCenter = False;
   RefBattleCP hq = (cp->getRank().sameRank(Rank_Division)) ?
   BobUtility::getAttachedLeader(d_batData, cp) : cp;

   bool stillMoving = changingFace(cp);
   //bool getSpeed = False;

   // see if we can continue movement
   if(cp->allSPAtCenter() && destHexesOccupied(cp))
   {
      // if terrain is passable
      clearMove(cp, !canContinue(cp));
      allAtCenter = True;
   }

   // we can continue
   else
   {
      if(cp->allSPAtCenter())
      {
         bool changingSPFormation = False;
         if(!canContinue(cp, &changingSPFormation))
         {
            if(!changingSPFormation)
               clearMove(cp, True);

            return False;
         }
      }

      const Distance moveDist = distanceTraveled(d_batData->getTick() - d_lastTick, cp->speed());

#ifdef DEBUG
      moveLog.printf("move distance = %ld", moveDist);
#endif

      /*
       * Move each SP as needed
       */

      for(int r = 0; r < cp->rows(); r++)
      {
         for(int c = 0; c < cp->columns(); c++)
         {
            //DeployItem& di = deployMap[(cp->columns() * r) + c];
            DeployItem* di = cp->currentDeployItem(c, r);
            ASSERT(di);

            // skip any that are inactive (i.e. no SP in that hex) or not moving
            if(!di || !di->active() || !di->d_sp->isMoving())
               continue;

            // if we're at the center see if we continue moving
            bool thisSPMoving = True;
            if(cp->allSPAtCenter())
            {
               ASSERT(di->d_sp->hexPosition().atCenter());

               // Bodge, moving position back to center
               if(!di->d_sp->hexPosition().atCenter())
               {
#ifdef DEBUG
                  moveLog.printf("WARNING! SP %ld (%s) is out of sync(Not at centerPos, dist = %d)",
                           reinterpret_cast<LONG>(di->d_sp),
                           d_batData->ob()->spName(di->d_sp),
                           di->d_sp->hexPosition().distance());
#endif
                  if(di->d_sp->hexPosition().distance() >= HexPosition::centerPos())
                           di->d_sp->hexPosition().moveAlong(HexPosition::centerPos());

                  di->d_sp->hexPosition().moveAlong(HexPosition::centerPos());
               }

#ifdef DEBUG
               moveLog.printf("Route for SP %ld (%s str = %d) (at Hex (%d, %d))------------",
                  reinterpret_cast<LONG>(di->d_sp),
                  d_batData->ob()->spName(di->d_sp),
                  static_cast<int>(di->d_sp->strength()),
                  static_cast<int>(di->d_sp->hex().x()), static_cast<int>(di->d_sp->hex().y()));

               SListIterR<HexItem> rIter(&di->d_sp->routeList());
               while(++rIter)
               {
                  moveLog.printf("---------------- x = %d, y = %d",
                      static_cast<int>(rIter.current()->d_hex.x()),
                      static_cast<int>(rIter.current()->d_hex.y()));
               }
#endif
               // if at the end of the line
               if(!di->d_sp->nextHex())
               {
                  thisSPMoving = False;
                  if(di->d_sp->nextFacing() == di->d_sp->facing())
                  {
                     if(di->d_sp->facing() != di->d_sp->rFacing())
                     {
                        updateFacing(di->d_sp, c, r);
                        stillMoving = True;
                     }
                     else
                        finishMove(di->d_sp, c, r);
                  }
               }

               else
               {
#ifdef DEBUG
                  HexCord nextHex = di->d_sp->nextHex()->d_hex;
#endif
                  if(di->d_sp->hex() == di->d_sp->nextHex()->d_hex)
                  {
                     stillMoving = True;

                     if(!cp->moveStarted())
                     {
                        //getSpeed = True;
                        cp->moveStarted(True);
                     }

                     if(cp->moveStarted())//getSpeed)
                        di->d_sp->setRFacing(di->d_sp->nextHex()->d_facing);

                     getNextHex(di->d_sp);
                  }

                  if(di->d_sp->nextHex())
                  {
                     //if( (di->d_sp->nextHex()->d_hex == di->d_sp->hex()) ||
                     //    ( (cp->deploying()) && (!canMove(cp, di->d_sp, c, r))  )  )
                     if(di->d_sp->nextHex()->d_hex == di->d_sp->hex())
                     {
                        thisSPMoving = False;
                        continue;
                     }
                     else
                     {
                        di->d_sp->hexPosition().facing(moveDirection(di->d_sp->hex(), di->d_sp->nextHex()->d_hex));
#ifdef DEBUG
                        moveLog.printf("Setting next facing for SP (r%d, c%d)", r, c);
#endif
                        setNextFacing(di->d_sp);

                     }
                  }
                  else
                  {
                     thisSPMoving = False;
                     if(di->d_sp->facing() != di->d_sp->rFacing())
                     {
                        updateFacing(di->d_sp, c, r);
                        stillMoving = True;
                     }
                     else
                        finishMove(di->d_sp, c, r);
                  }
               }
            }
            else if(di->d_sp->hexPosition().atCenter())
            {
               if(!di->d_sp->nextHex() && di->d_sp->facing() == di->d_sp->rFacing())
                  finishMove(di->d_sp, c, r);

               thisSPMoving = False;
            }


            if(thisSPMoving)
            {
               HexPosition::WhereAt wa = di->d_sp->hexPosition().moveAlong(moveDist);

               if(wa == HexPosition::EdgeOfHex)
               {
                  //getSpeed = True;
                  cp->speed(0);
#ifdef DEBUG
                  moveLog.printf("Setting next facing for SP (r%d, c%d)", r, c);
#endif
                  setNextFacing(di->d_sp);
               }

               if(moveAlong(di->d_sp, moveDist, wa))
               {
                  stillMoving = True;
               }

#ifdef DEBUG
               moveLog.printf("SP %ld after move = %ld",
               reinterpret_cast<LONG>(di->d_sp), di->d_sp->hexPosition().distance());
               moveLog.printf("------- Move-Facing -- %s", faceName(di->d_sp->hexPosition().facing()));
               moveLog.printf("------- Relative-Facing -- %s", faceName(di->d_sp->facing()));
#endif

               allAtCenter = (wa == HexPosition::CenterHex);
            }
         }
      }

      // move HQ, if any
      if(hq != NoBattleCP && (hq == cp || stillMoving))
      {
#ifdef DEBUG
         moveLog.printf("Moving HQ(%s) of %s", (hq == cp) ? "Unattached" : "Attached", hq->getName());
#endif
         bool setSpeed = False;
         bool finishedMove = False;

         if(hq->hexPosition().atCenter())
         {

            if(!hq->nextHex())
            {
               finishedMove = True;
               hq->setMoving(False);
            }
            else if(hq->hex() == hq->nextHex()->d_hex)
            {
               getNextHex(hq);

               if(hq->nextHex())
                  ;//setspeed = True;
               else
               {
                  finishedMove = True;
                  hq->setMoving(False);
               }
            }
         }

         if(!finishedMove)
         {
            HexPosition::WhereAt wa = hq->hexPosition().moveAlong(moveDist);

            if(wa == HexPosition::EdgeOfHex)
            {
               setNextFacing(hq);
               setSpeed = True;
            }

            if(moveAlong(hq, moveDist, wa))
            {
               stillMoving = True;
            }

            // don't set speed if this is an attached leader
            // it has already been set
            if(setSpeed && hq == cp)
               cp->speed(0);
            //   setUnitSpeed(hq);

            allAtCenter = (wa == HexPosition::CenterHex);
         }
      }
   }

   cp->allSPAtCenter(allAtCenter);

   //if(getSpeed)
   // setUnitSpeed(cp);
   //if(stillMoving && cp->speed() == 0)
   //   setUnitSpeed(cp);

   return stillMoving;
}
#endif

void BU_MoveImp::runSPFormations(const RefBattleCP& cp)
{
   if(d_spFormations.run(cp))
   {
      cp->moveMode(BattleCP::Holding);
#ifdef DEBUG
      moveLog.printf("End SPFormation change for %s, new formation = %s(order=%s)",
         cp->getName(),
         BattleOrderInfo::spFormationName(cp->spFormation()),
         BattleOrderInfo::spFormationName(cp->getCurrentOrder().spFormation()));
#endif
   }
}

bool BU_MoveImp::finalMove(const RefBattleCP& cp)
{
   if(needsToReadjust(cp))
   {
#ifdef DEBUG
      moveLog.printf("Trying to readjust SP for %s", cp->getName());
#endif
      if(d_formations.adjustDeployment(cp))
      {
#ifdef DEBUG
         moveLog.printf("------- Will readjust SP");
#endif
         cp->moveMode(BattleCP::Deploying);
         cp->needsReadjustment(False);
         return False;;
      }
      else
      {
#ifdef DEBUG
         moveLog.printf("------Unable to readjust at this time");
#endif
         cp->needsReadjustment(True);
      }
   }


   if( (cp->getRank().sameRank(Rank_Division)) &&
       (cp->generic()->isArtillery()) )
   {
      cp->getCurrentOrder().spFormation(SP_UnlimberedFormation);
      cp->getCurrentOrder().whatOrder().add(WhatOrder::SPFormation);
   }

   return True;
}

bool BU_MoveImp::runMovement(const RefBattleCP& cp)
{
   // if we're deploying, or otherwise moving
   if( (cp->deploying()) ||
      (cp->onManuever()) ||
      (cp->moving()) )
    {
      if(!moving(cp))
      {
             bool stillMoving = False;
#ifdef DEBUG
             moveLog.printf("%s has completed movement", cp->getName());
#endif

             // end for deployment
//                 bool needsToAdjust = False;
             if(cp->deploying())
             {
                  cp->formation(cp->nextFormation());
                  cp->rows(cp->wantRows());
                  cp->columns(cp->wantColumns());
                  cp->lockMap();
             }

             // end for manuever, or other movement
             else
             {
                  if(cp->onManuever())
                  {
                     cp->getCurrentOrder().manuever(CPM_Hold);
                  }

                  cp->syncMapToSP();
             }

             stillMoving = (cp->formation() != cp->getCurrentOrder().divFormation() ||
                           cp->getCurrentOrder().wayPoints().entries() > 1 ||
                                cp->fleeingTheField());

             bool runFinish = (!stillMoving && cp->getCurrentOrder().wayPoints().entries() == 0);

             cp->moveMode(BattleCP::Holding);
             updateWhichWay(d_batData, cp);
             cp->moveStarted(False);
             cp->allSPAtCenter(True);
             cp->movingBackwards(False);
             cp->movingLeft(False);
             cp->movingRight(False);
             cp->retreating(False);
             cp->speed(0);

             if(runFinish)
               finalMove(cp);
#if 0
             if(needsToReadjust(cp))
             {
#ifdef DEBUG
               moveLog.printf("Trying to readjust SP for %s", cp->getName());
#endif
               if(d_formations.adjustDeployment(cp))
               {
#ifdef DEBUG
                  moveLog.printf("------- Will readjust SP");
#endif
                  cp->moveMode(BattleCP::Deploying);
                  cp->needsReadjustment(False);
                  stillMoving = True;
               }
               else
               {
#ifdef DEBUG
                  moveLog.printf("------Unable to readjust at this time");
#endif
                  cp->needsReadjustment(True);
               }
            }
            if(!stillMoving)
            {
//                   cp->moveMode(BattleCP::Holding);
               if( (cp->getRank().sameRank(Rank_Division)) &&
                   (cp->generic()->isArtillery()) )
               {
#ifdef DEBUG
                  if(lstrcmpi("VI Corps Artillery", cp->getName()) == 0)
                  {
                     int i = 0;
                  }
#endif
                  //if(cp->getCurrentOrder().wayPoints().entries() == 1 &&
                  //   cp->leftHex() == cp->getCurrentOrder().wayPoints().first()->d_hex)
                  {
                     cp->getCurrentOrder().spFormation(SP_UnlimberedFormation);
                     cp->getCurrentOrder().whatOrder().add(WhatOrder::SPFormation);
                  }
               }
            }
#endif

            return stillMoving;
      }
      else
      {
         if(!cp->deploying())
            cp->syncMapToSP();

         return True;
      }
   }

   return False;
}

/*
 * Update what unit is doing
 * i.e. it should retreat or charge or whatever
 */


bool BU_MoveImp::canUpdate(const RefBattleCP& cp)
{
  if(cp->holding())
    return True;
   else if(cp->deploying() || cp->onManuever() || cp->liningUp())
    return False;

   else if(cp->allSPAtCenter())
   {
#if 0
      for(BattleSPIter iter(d_batData->ob(), cp); !iter.isFinished(); iter.next())
      {
         if(iter.sp()->rFacing() != iter.sp()->facing())
             return False;
      }
#endif
      return True;
   }

   return False;
}

void BU_MoveImp::sendRetreatMessage(const RefBattleCP& cp, int nHexes)
{
   if(cp->fleeingTheField())
   {
      // send player messages
      // our side
      {
         BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_FleeingTheField, cp);
         d_batgame->sendPlayerMessage(msg);
      }
      // their side
      if(cp->targetList().entries() != 0)
      {
         {
            BattleMessageInfo msg(cp->targetList().first()->d_cp->getSide(), BattleMessageID::BMSG_EnemyFleeing, cp->targetList().first()->d_cp);
            msg.target(cp);
            d_batgame->sendPlayerMessage(msg);
         }
      }
   }
   else if(cp->routing())
   {
      // send player messages
      // our side
      {
         BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_Routing, cp);
         d_batgame->sendPlayerMessage(msg);
      }
      // their side
      if(cp->targetList().entries() != 0)
      {
         {
            BattleMessageInfo msg(cp->targetList().first()->d_cp->getSide(), BattleMessageID::BMSG_EnemyRouting, cp->targetList().first()->d_cp);
            msg.target(cp);
            d_batgame->sendPlayerMessage(msg);
         }
      }
   }

   else
   {
      // send player messages
      // our side
      {
         BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_Retreating, cp);
         msg.cp(cp);
         msg.n1(nHexes * BattleMeasure::XYardsPerHex);
         d_batgame->sendPlayerMessage(msg);
      }
      // their side
      if(cp->targetList().entries() != 0)
      {
         {
            BattleMessageInfo msg(cp->targetList().first()->d_cp->getSide(), BattleMessageID::BMSG_EnemyRetreating, cp->targetList().first()->d_cp);
            msg.target(cp);
            d_batgame->sendPlayerMessage(msg);
         }
      }
   }
}

void BU_MoveImp::updateUnitMode(const RefBattleCP& cp)
{
    // update moving unit only if  all sp at center
    // always update holding unit
    if(!cp->attached())
    {
//       cp->charging(False);
         cp->unlockMode();
         int r = (cp->targetList().closeRange() / BattleMeasure::XYardsPerHex) - 1;
         ASSERT(r >= 0);

         const int c_maxRange = 6;

         if(r < c_maxRange || cp->fleeingTheField() || cp->nearEnemyMode() >= BattleCP::NE_Retreat2)
         {
             if(r >= c_maxRange)
                r = c_maxRange - 1;
#ifdef DEBUG
             moveLog.printf("Updating unit mode (%s) for %s (%s)",
                  BattleCP::nearEnemyName(cp->nearEnemyMode()),
                  cp->getName(),
                  scenario->getSideName(cp->getSide()));
             moveLog.printf("----- Enemy is %d hexes away", r + 1);
#endif

             ASSERT(cp->nearEnemyMode() < BattleCP::NE_HowMany);

             enum DoNext {
                  NoChange,
                  Hold,
                  Charge,
                  Retreat
             };

             static const DoNext s_doNext[BattleCP::NE_HowMany][c_maxRange] = {
             //  1 hex    2 hexes    3 hexes   etc...
                  { NoChange, NoChange, NoChange, NoChange, NoChange, NoChange }, // NotNear
                  { Hold,     Hold,     Hold,     Hold,     Hold,     Hold     }, // HoldInPlace
                  { Hold/*Charge*/,   Charge,   Charge,   Charge,   Charge,   Charge   }, // Charge
                  { Hold/*Charge*/,   NoChange, NoChange, NoChange, NoChange, NoChange }, // ChargeAt1
                  { Hold/*Charge*/,   Charge,   NoChange, NoChange, NoChange, NoChange }, // ChargeAt2
                  { Hold/*Charge*/,   Charge,   Charge,   NoChange, NoChange, NoChange }, // ChargeAt3
                  { Hold,     NoChange, NoChange, NoChange, NoChange, NoChange }, // HaltAt1
                  { Hold,     Hold,     NoChange, NoChange, NoChange, NoChange }, // HaltAt2
                  { Hold,     Hold,     Hold,     NoChange, NoChange, NoChange }, // HaltAt3
                  { Hold,     Hold,     Hold,     Hold,     NoChange, NoChange }, // HaltAt4
                  { Hold,     Hold,     Hold,     Hold,     Hold,     NoChange }, // HaltAt5
                  { Hold,     Hold,     Hold,     Hold,     Hold,     Hold     }, // HaltAt6
                  { Retreat,  NoChange, NoChange, NoChange, NoChange, NoChange }, // KeepAway2
                  { Retreat,  Retreat,  NoChange, NoChange, NoChange, NoChange }, // KeepAway3
                  { Retreat,  Retreat,  Retreat,  NoChange, NoChange, NoChange }, // KeepAway4
                  { Retreat,  Retreat,  Retreat,  Retreat,  NoChange, NoChange }, // KeepAway5
                  { Retreat,  Retreat,  Retreat,  Retreat,  Retreat,  NoChange },  // KeepAway6
                  { Retreat,  Retreat,  Retreat,  Retreat,  Retreat,  Retreat  },  // Retreat2
                  { Retreat,  Retreat,  Retreat,  Retreat,  Retreat,  Retreat  },  // Retreat3
                  { Retreat,  Retreat,  Retreat,  Retreat,  Retreat,  Retreat  },  // Retreat4
                  { Retreat,  Retreat,  Retreat,  Retreat,  Retreat,  Retreat  },  // Retreat5
                  { Retreat,  Retreat,  Retreat,  Retreat,  Retreat,  Retreat  }   // Retreat6
             };

             static const UBYTE s_retreatHowMany[BattleCP::NE_HowMany][c_maxRange] = {
             // 1 2  3  4  5  6  hexes away
                  { 0, 0, 0, 0, 0, 0 }, // NotNear
                  { 0, 0, 0, 0, 0, 0 }, // HoldInPlace
                  { 0, 0, 0, 0, 0, 0 }, // Charge
                  { 0, 0, 0, 0, 0, 0 }, // ChargeAt1
                  { 0, 0, 0, 0, 0, 0 }, // ChargeAt2
                  { 0, 0, 0, 0, 0, 0 }, // ChargeAt3
                  { 0, 0, 0, 0, 0, 0 }, // HaltAt1
                  { 0, 0, 0, 0, 0, 0 }, // HaltAt2
                  { 0, 0, 0, 0, 0, 0 }, // HaltAt3
                  { 0, 0, 0, 0, 0, 0 }, // HaltAt4
                  { 0, 0, 0, 0, 0, 0 }, // HaltAt5
                  { 0, 0, 0, 0, 0, 0 }, // HaltAt6
                  { 1, 0, 0, 0, 0, 0 }, // KeepAway2
                  { 2, 1, 0, 0, 0, 0 }, // KeepAway3
                  { 3, 2, 1, 0, 0, 0 }, // KeepAway4
                  { 4, 3, 2, 1, 0, 0 }, // KeepAway5
                  { 5, 4, 3, 2, 1, 0 }, // KeepAway6
                  { 2, 2, 2, 2, 2, 2 }, // Retreat2
                  { 3, 3, 3, 3, 3, 3 }, // Retreat3
                  { 4, 4, 4, 4, 4, 4 }, // Retreat4
                  { 5, 5, 5, 5, 5, 5 }, // Retreat5
                  { 6, 6, 6, 6, 6, 6 }, // Retreat6
             };

             BattleCP::MoveMode m = cp->moveMode();

             // if unit is routing or fleeing the field set to retreat
             if(cp->routing() || cp->fleeingTheField())
             {
#ifdef DEBUG
                moveLog.printf("%s is %s", cp->getName(), (cp->routing()) ? "Routing" : "Fleeing");
#endif
                cp->nearEnemyMode((cp->fleeingTheField()) ? BattleCP::NE_Retreat6 : BattleCP::NE_KeepAway6);
             }

             DoNext doNext = (cp->fleeingTheField()) ? Retreat : s_doNext[cp->nearEnemyMode()][r];
             switch(doNext)
             {
                  case Charge:
                  // plot-route to enemy
#ifdef DEBUG
                      moveLog.printf("Charging");
#endif
                      // set to hold if within 1 hex
//                    cp->charging(True);
                      // if we do not have route to enemy, get one
#ifdef DEBUG
                      plotMovementToTarget(d_batData, cp, moveLog);
#else
                      plotMovementToTarget(d_batData, cp);
#endif
                      break;

                  case Hold:
                  // set to hold
                      clearMove(cp, False);//cancelRoute(cp);
#ifdef DEBUG
                      moveLog.printf("Holding");
#endif
                      break;

                  case Retreat:
                      // plot route away from enemy
                      if(cp->fleeingTheField())
                      {
                         if(cp->getCurrentOrder().wayPoints().entries() == 0)
                         {
#ifdef DEBUG
                            if(plotRetreatHex(d_batData, cp, 6, moveLog))
#else
                            if(plotRetreatHex(d_batData, cp, 6))
#endif
                            {
                              sendRetreatMessage(cp, 6);
                            }
                         }
                      }
                      else if(!cp->retreating())
                      {
                           clearMove(cp, False); //cancelRoute(cp);
                           int retreatHowMany = s_retreatHowMany[cp->nearEnemyMode()][r];
#ifdef DEBUG
                           if(plotRetreatHex(d_batData, cp, retreatHowMany, moveLog))
#else
                           if(plotRetreatHex(d_batData, cp, retreatHowMany))
#endif
                           {
                              sendRetreatMessage(cp, s_retreatHowMany[cp->nearEnemyMode()][r]);
                           }
                      }
                      break;

#ifdef DEBUG
                  case NoChange:
                      moveLog.printf("No Change");
                      break;
#endif
             }
         }
   }
}

/*
 * Deploy an organization(recursive!)
 */

void BU_MoveImp::deployOrg(const RefBattleCP& cp)
{
    if(cp->child() != NoBattleCP)
      deployOrgAndSisters(cp->child());

    if(cp->inCombat())
    {
       if(B_LossUtil::clearDestroyedSP(d_batgame, cp, 0, False))
          return;
    }

    //   updateUnitMode(cp);
    if( (cp->allSPAtCenter()) &&
       (!cp->liningUp() && !cp->deploying() && !cp->onManuever()) )
    {
       if(!shouldRemove(cp))
       {
          if(cp->clearMove())
          {
             clearMove(cp, cp->fleeingTheField());
             cp->clearMove(False);
          }

          updateUnitMode(cp);

          if(cp->getRank().sameRank(Rank_Division) &&
               cp->shouldReorganize() &&
               cp->nearEnemyMode() < BattleCP::NE_KeepAway2 &&
               cp->targetList().closeRange() > (4 * BattleMeasure::XYardsPerHex))
          {
             bool reorg = False;
             if(B_LossUtil::clearDestroyedSP(d_batgame, cp, &reorg, cp->rallying()))
                  return;

             if(!cp->rallying() || reorg)
             {
                clearMove(cp, False);
                if(d_formations.reorganize(cp) && startReorganize(cp))
                {
                     cp->shouldReorganize(False);
                }
             }
          }
       }
       else
       {
          removeUnit(cp);
          return;
       }
    }

    if(cp->attached())
      return;

    // if changing SP Formation
    if(cp->liningUp())
      runSPFormations(cp);

    // or moving
    else if(runMovement(cp))
      ;

    else if(cp->getCurrentOrder().whatOrder().entries() > 0)
    {
        runWhatOrderList(cp);
    }

   // or if we should move
    else if(cp->getCurrentOrder().nextWayPoint())
    {
      if(!startMovement(cp) && cp->getCurrentOrder().whatOrder().entries() > 0)
         runWhatOrderList(cp);
    }
}

void BU_MoveImp::runWhatOrderList(const RefBattleCP& cp)
{
      WhatOrderList& wl = cp->getCurrentOrder().whatOrder();

      bool remove = True;
        bool loop;
        do
        {
         loop = False;
         WhatOrder* wo = wl.first();
         switch(wo->d_whatOrder)
         {
            // changin div deployment
            case WhatOrder::CPFormation:
               if(cp->getCurrentOrder().divFormation() == cp->formation())
                  ; //remove = True;
               else
               {
                  remove = (abValue(cp->getCurrentOrder().divFormation() - cp->formation()) == 1);

                  if(!startDeploy(cp))
                  {
                      remove = False;
                  }
               }

             break;

             case WhatOrder::SPFormation:
               ASSERT(cp->sp() != NoBattleSP && cp->getRank().sameRank(Rank_Division));
               if(cp->getCurrentOrder().spFormation() == cp->sp()->fromFormation())
                  ;//remove = True;
               else
               {
                  if(!startSPFormationChange(cp))
                  {
                   //  remove = False;
                      cp->getCurrentOrder().spFormation(cp->sp()->fromFormation());

                  }
               }

               break;

               case WhatOrder::Manuever:
               {

                  if(!startManuever(cp))
                  {
                     remove = False;
                     WhatOrder* wo2 = wl.first();
                     loop = (wo2 != wo);
                  }
               }
               break;

               case WhatOrder::Facing:
               {
                  if(cp->getRank().sameRank(Rank_Division))
                  {
                     CPManuever m = moveHow(d_batData, cp);
                     if(m != CPM_Forward)
                     {
                        cp->getCurrentOrder().manuever(m);
                        if(!startManuever(cp))
                        {
                           remove = False;
                           WhatOrder* wo2 = wl.first();
                           loop = (wo2 != wo);
                        }
                        else
                           remove = (cp->facing() == cp->getCurrentOrder().facing());
                     }
                  }
                  else
                     cp->setFacing(cp->getCurrentOrder().facing());

                  break;
               }

               case WhatOrder::CPLineHow:
                   break;
         }

         if(remove)
            wl.remove(wo);
        } while(loop);
}

/*
 * Deploy an organization and its children (recursively!)
 */

void BU_MoveImp::deployOrgAndSisters(const RefBattleCP& cp)
{
   {  // braces needed until compiler complies with new standard
//       int loop = 0;
       for(RefBattleCP unit = cp; unit != NoBattleCP; unit = unit->sister())
//     for(BattleUnitIter iter(cp); !iter.isFinished(); iter.sister())
       {
          if(!unit->hasQuitBattle() && unit->active())
               deployOrg(unit);
#if 0
          if(++loop >= 100)
          {
            FORCEASSERT("Infinte loop n deployOrgAndSisters()");
            break;
          }
#endif
     }
   }
}

/*
 * Deploy Proc
 */

void BU_MoveImp::process()
{
    BattleData::WriteLock lock(d_batData);
  for(Side side = 0; side < d_ob->sideCount(); ++side)
  {
#ifdef DEBUG_DEPLOY
    moveLog.printf("Deploying Side %d", static_cast<int>(side));
#endif

    RefBattleCP cp = d_ob->getTop(side);

    deployOrgAndSisters(cp);

#ifdef DEBUG_DEPLOY
    moveLog.printf("------------------------");
#endif
  }

  d_lastTick = d_batData->getTick();
}

/*
 * Set an Units/SPs position and update hexMap
 */

void BU_MoveImp::placeUnit(const RefBattleUnit& bu, const HexCord& hex)
{
   BattleHexMap::iterator from = d_hexMap->find(bu);


#ifdef DEBUG
#if 0
   CRefBattleCP cp = BobUtility::getCP(bu);
   if(cp != NoBattleCP && cp->getRank().isHigher(Rank_Division) && cp->attached())
   {
       moveLog.printf("Placing HQ %s at hex(%d, %d)", cp->getName(),
      static_cast<int>(hex.x()), static_cast<int>(hex.y()));
   }
#endif
   d_batData->assertHexOnMap(hex);

#endif
   ASSERT(bu->nextHex());

   bu->setRFacing(bu->nextHex()->d_facing);
   bu->position(BattlePosition(hex, bu->hexPosition()));
   d_hexMap->move(from);

   // do victory points if any
   const BattleTerrainHex& hexInfo = d_batData->getTerrain(hex);
   BattleTerrainHex* pHexInfo = const_cast<BattleTerrainHex*>(&hexInfo);
   if(hexInfo.d_victoryPoints > 0)
   {
      Side ourSide = BobUtility::getSide(bu);

      if(hexInfo.d_victorySide != ourSide)
      {
         if(hexInfo.d_victorySide != SIDE_Neutral)
         {
            // Send message
            CRefBattleCP enemyCinc = BobUtility::getCinc(d_batData->ob(), (ourSide == 0) ? 1 : 0);
            if(enemyCinc != NoBattleCP &&
               GamePlayerControl::getControl(enemyCinc->getSide()) == GamePlayerControl::AI)
            {
               BattleMessageInfo msg(enemyCinc->getSide(), BattleMessageID::BMSG_EnemyTookVP, enemyCinc);
               msg.hex(hex);
               d_batgame->sendPlayerMessage(msg);
            }
         }

         pHexInfo->d_victorySide = ourSide;
      }
   }
}

/*-------------------------------------------------------------
 * Client-access
 */

BU_MoveInt::BU_MoveInt(BattleGameInterface* batgame) :
   d_bm(new BU_MoveImp(batgame))
{
  ASSERT(d_bm);
}

BU_MoveInt::~BU_MoveInt()
{
  if(d_bm)
    delete d_bm;
}

void BU_MoveInt::process()
{
  if(d_bm)
    d_bm->process();
}



}; // namespace B_Logic

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.4  2002/11/16 18:03:29  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
