/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Overnight Recovery
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "overnight.hpp"
#include "moveutil.hpp"
#include "pdeploy.hpp"
#include "batdata.hpp"
#include "bobiter.hpp"
#include "hexmap.hpp"
#include "bobutil.hpp"
#ifdef DEBUG
#include "clog.hpp"
LogFile oLog("BOvernight.log");
#endif

void recoverMorale(RPBattleData bd, const RefBattleCP& cp)
{
#ifdef DEBUG
   oLog.printf("Recovering morale for %s -> startMorale=%d, currentMorale=%d",
      cp->getName(), static_cast<int>(cp->startMorale()), static_cast<int>(cp->morale()));
#endif

   // recover from shaken, disorded, routing, etc.
   cp->wavering(False);
   cp->routing(False);
   cp->shaken(False);
   cp->disorder(3);

   // recover morale
   CRefBattleCP cinc = BobUtility::getCinc(bd->ob(), cp->getSide());

   // base recovery value is 80% of starting total
   int value = 80;

   // modifiers to value

   // if cinc has charisma over 200  (+5)
   if(cinc->leader()->getCharisma() >= 200)
      value += 5;

   // if leaders staff is less than units subordination (-5)
   if(cinc->leader()->getStaff() < bd->ob()->ob()->getCommandSubordination(cinc->generic()))
      value -= 5;

    value = clipValue(value, 0, 100);
    Attribute newMorale = static_cast<Attribute>(MulDiv(cp->startMorale(), value, 100));

    newMorale = maximum(cp->morale(), newMorale);
    cp->morale(newMorale);

#ifdef DEBUG
   oLog.printf("Recovering morale for %s -> recovered Morale=%d",
      cp->getName(), static_cast<int>(cp->morale()));
#endif

   BobUtility::setCommandMorale(cp);
}

void clearMove(RPBattleData bd, const RefBattleCP& cp)
{
   cp->moveMode(BattleCP::Holding);
   cp->retreating(False);
   cp->getCurrentOrder().wayPoints().reset();
   cp->getCurrentOrder().order().d_whatOrder.reset();
   cp->nextFire(0);
   cp->nextShockCheck(0);
   cp->setNextOrderCheck(0);
   cp->unlockMode();
   cp->postureLocked(False);
   cp->aggressionLocked(False);
   cp->aggression(cp->getCurrentOrder().aggression());
   cp->posture(cp->getCurrentOrder().posture());
   cp->setFacing(cp->getCurrentOrder().facing());
   cp->getCurrentOrder().finishOrder();
   cp->formation(cp->getCurrentOrder().divFormation());
   cp->nextFormation(cp->formation());
   cp->shootingGuns(False);
   cp->shootingMuskets(False);
   cp->takingGunHits(False);
   cp->takingMusketHits(False);

   int columns;
   int rows;
   BobUtility::getRowsAndColoumns(cp->formation(), cp->spCount(), columns, rows);
   cp->columns(columns);
   cp->wantColumns(columns);
   cp->rows(rows);
   cp->wantRows(rows);


  for(std::vector<DeployItem>::iterator di = cp->mapBegin();
    di != cp->mapEnd();
    di++)
  {
    if(di->active())
    {
      di->d_sp->routeList().reset();
         di->d_sp->setMoving(False);
         di->d_sp->setTween(0);
         di->d_sp->setFormation(cp->getCurrentOrder().spFormation());
         di->d_sp->shootingGuns(False);
         di->d_sp->shootingMuskets(False);
         di->d_sp->takingGunHits(False);
         di->d_sp->takingMusketHits(False);
         di->d_sp->position(BattlePosition(di->d_sp->hex(), HexPosition()));
    }
  }

  // do attached leader if any
  RefBattleCP hq = BobUtility::getAttachedLeader(bd, cp);
  if(hq != NoBattleCP)
  {
    hq->setMoving(False);
    hq->routeList().reset();
  }

   if(cp->getRank().isHigher(Rank_Division))
  {
      cp->setMoving(False);
      cp->routeList().reset();
      cp->position(BattlePosition(cp->hex(), HexPosition()));
  }
}

void placeUnit(RPBattleData bd, const RefBattleUnit& bu, const HexCord& hex, HexPosition::Facing facing)
{
   BattleHexMap::iterator from = bd->hexMap()->find(bu);

#ifdef DEBUG
   bd->assertHexOnMap(hex);
#endif
   bu->setFacing(facing);
   bu->nextFacing(facing);
   bu->setRFacing(facing);
   bu->position(BattlePosition(hex, HexPosition()));
   bd->hexMap()->move(from);
}

bool findDeployPosition(
         BattleData* bd,
      const RefBattleCP& cp,
      HexCord& hex)
{
   // we set up a new order, based on last order, except with waypoints cleared
   BattleOrder order;
   cp->lastOrder(&order);
   order.wayPoints().reset();
   order.order().d_whatOrder.reset();

   HexCord bLeft;
   HexCord mSize;
   bd->getPlayingArea(&bLeft, &mSize);
   HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

   const int nDirections = 8;
   static const POINT p[nDirections] = {
              {  1,  0 },  // E
              {  1,  1 },  // NE
              {  0,  1 },  // N
              { -1,  1 },  // NW
              { -1,  0 },  // W
              { -1, -1 },  // SW
              {  0, -1 },  // S
              {  1, -1 },  // SE
  };

   enum LFacings {
    NE, N, NW, SW, S, SE, LFacings_HowMany
  } lFacing = (cp->facing() == HexPosition::NorthEastPoint) ? NE :
          (cp->facing() == HexPosition::North)          ? N :
          (cp->facing() == HexPosition::NorthWestPoint) ? NW :
          (cp->facing() == HexPosition::SouthWestPoint) ? SW :
          (cp->facing() == HexPosition::South)          ? S : SE;

   static const UBYTE s_canMoveThisWay[LFacings_HowMany][nDirections] = {
      {  False, False, False, False, True,  True,  True,  False  }, // NE
      {  False, False, False, False, False, True,  True,  True  }, // N
      {  True,  False, False, False, False, False, True,  True  }, // NW
      {  True,  True,  False, False, False, False, False, True  }, // SW
    {  True,  True,  True,  False, False, False, False, False  }, // S
      {  False, True,  True,  True,  False, False, False, False }, // SE
   };

  for(int i = 0; i < nDirections; i++)
  {
    bool found = True;
    HexCord tHex = hex;

    int loop = 0;
      HexList list;
      while(!PlayerDeploy::playerDeploy(bd, cp, tHex, &order.order(), &list, PlayerDeploy::FILLHEXLIST | PlayerDeploy::ENEMYONLY))
    {
         list.reset();
      if(!s_canMoveThisWay[lFacing][i])
      {
        found = False;
        break;
      }

      if(++loop >= 1000)
      {
        FORCEASSERT("Infinite loop in assignDeployPosition()");
        break;
      }

      int newX = tHex.x() + p[i].x;
      int newY = tHex.y() + p[i].y;

      if(newX < bLeft.x() ||
        newX >= tRight.x() ||
        newY < bLeft.y() ||
        newY >= tRight.y())
      {
        found = False;
        break;
      }

      found = True;
      tHex.x(static_cast<UBYTE>(newX));
      tHex.y(static_cast<UBYTE>(newY));
    }

    if(found)
      {
        RefBattleCP hq = BobUtility::getAttachedLeader(bd, cp);

         // phsyically move division to move position
         if(cp->getRank().sameRank(Rank_Division))
         {
            ASSERT(list.entries() == cp->spCount());
            if(list.entries() != cp->spCount())
               return False;

            for(int r = 0; r < cp->wantRows(); r++)
            {
               for(int c = 0; c < cp->wantColumns(); c++)
               {
                  DeployItem* di = cp->wantDeployItem(c, r);
                  ASSERT(di);
                  if(di && di->active())
                  {
                     ASSERT(list.first());
                     if(!list.first())
                        return False;

                     placeUnit(bd, di->d_sp, list.first()->d_hex, order.facing());
                     list.remove(list.first());
                     di->syncHexToSP();
                  }
               }
            }

            // place any attached leader
            if(hq != NoBattleCP)
            {
              r = (order.divFormation() == CPF_Extended) ? 0 : minimum(1, cp->wantRows() - 1);
              int c = maximum(0, (cp->wantColumns() / 2) - 1);
              DeployItem* di = cp->wantDeployItem(c, r);
              ASSERT(di);
              if(di)
              {
                ASSERT(di->active());
                if(di->active())
                  placeUnit(bd, hq, di->d_wantHex, order.facing());
              }
            }
         }
         else
         {
            ASSERT(list.entries() == 1);
            if(list.entries() == 0)
               return False;

            placeUnit(bd, cp, list.first()->d_hex, cp->facing());
            if(hq != NoBattleCP)
            {
               placeUnit(bd, cp, list.first()->d_hex, order.facing());
            }
         }

         // set last order as current order
         cp->setCurrentOrder(order);

         // clear out any acting orders, or orders in list
         BattleOrder ord;
         cp->getActingOrder(ULONG_MAX, &ord);

         BattleOrderList* bl = const_cast<BattleOrderList*>(cp->getOrderList());
         while(bl->getOrder(ULONG_MAX, &ord));

      return True;
      }
  }

  return False;
}

void moveForward(BattleData* bd, const RefBattleCP& cp)
{
   ASSERT(cp->offMap());

//   BattleOrder& order = cp->getCurrentOrder();
   BattleOrder order;
   cp->lastOrder(&order);
   HexCord left = (order.wayPoints().entries() > 0) ? order.wayPoints().getLast()->d_hex : cp->leftHex();

#ifdef DEBUG
   oLog.printf("Reinforcement %s moving forward -> currentHex(%d, %d)",
      cp->getName(), static_cast<int>(left.x()), static_cast<int>(left.y()));
#endif

   if(!findDeployPosition(bd, cp, left))
   {
      FORCEASSERT("Position not found");
   }
   else
   {
#ifdef DEBUG
      HexCord newHex = cp->leftHex();
      oLog.printf("Moved forward %s to -> newHex(%d, %d)",
            cp->getName(), static_cast<int>(newHex.x()), static_cast<int>(newHex.y()));
#endif
      clearMove(bd, cp);
   }
}

bool moveBackUnit(BattleData* bd, const RefBattleCP& cp)
{
   int backHowMany = ((10 - (cp->targetList().closeRange() / BattleMeasure::XYardsPerHex)) / 2) + 1;
   ASSERT(backHowMany > 0);

   HexCord curLeft = cp->leftHex();
   HexCord newLeft;

#ifdef DEBUG
   oLog.printf("Moving back %s %d hexes -> currentHex(%d, %d)",
      cp->getName(), backHowMany, static_cast<int>(curLeft.x()), static_cast<int>(curLeft.y()));
#endif

   for(;;)
   {
      while(!B_Logic::plotMove(bd, cp->facing(), curLeft, newLeft, backHowMany, B_Logic::MWD_Rear))
      {
         if(--backHowMany == 0)
            return False;
      }

      if(!findDeployPosition(bd, cp, newLeft))
      {
         if(--backHowMany == 0)
            return False;
      }
      else
      {
         // clear any moves
#ifdef DEBUG
         HexCord newHex = cp->leftHex();
         oLog.printf("Moved back %s to -> newHex(%d, %d)",
            cp->getName(), static_cast<int>(newHex.x()), static_cast<int>(newHex.y()));
#endif
         clearMove(bd, cp);
         break;
      }
   }

   return True;
}

void finishMove(BattleData* bd, const RefBattleCP& cp)
{
   HexCord left = cp->leftHex();

#ifdef DEBUG
   oLog.printf("Finishing Move for %s currentHex(%d, %d)",
      cp->getName(), static_cast<int>(left.x()), static_cast<int>(left.y()));
#endif

   if(!findDeployPosition(bd, cp, left))
   {
      FORCEASSERT("Position not found");
   }
   else
   {
#ifdef DEBUG
      HexCord newHex = cp->leftHex();
      oLog.printf("------ newHex(%d, %d)",
           static_cast<int>(newHex.x()), static_cast<int>(newHex.y()));
#endif
      clearMove(bd, cp);
   }
}

void B_Logic::overnightRecovery(BattleData* batData)
{
   for(BattleUnitIter iter(batData->ob()); !iter.isFinished(); iter.next())
   {
      bool stopMove = True;
      bool recover = (iter.cp()->getRank().sameRank(Rank_Division));

      // any unit fleeing, set as fled
      if(iter.cp()->fleeingTheField())
      {
         iter.cp()->setFled();
         stopMove = False;
         recover = False;
      }

      // any reinforcements, move to reinforcment position
      else if(iter.cp()->offMap())
      {
         moveForward(batData, iter.cp());
         stopMove = False;
      }

      // if unit is within 10 hexes of enemy, move back
      else if(iter.cp()->targetList().closeRangeRear() == UWORD_MAX &&
              iter.cp()->targetList().closeRange() < (10 * BattleMeasure::XYardsPerHex) &&
              !iter.cp()->attached())
      {
         if(moveBackUnit(batData, iter.cp()))
            stopMove = False;
      }

      // or any other units that are currently moving, deploying, changing SP formation, etc. stop
      if(stopMove)
      {
         if(iter.cp()->moving()     ||
            iter.cp()->deploying()  ||
            iter.cp()->onManuever() ||
            iter.cp()->liningUp()   ||
            iter.cp()->needsReadjustment())
         {
            finishMove(batData, iter.cp());
         }
         else
            clearMove(batData, iter.cp());
      }

      if(recover)
      {
         recoverMorale(batData, iter.cp());
      }
   }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2002/11/24 13:17:48  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
