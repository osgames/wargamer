/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Deploy Battle Units
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bdeploy.hpp"
#include "batdata.hpp"
//#include "bobdef.hpp"
#include "batarmy.hpp"
#include "bobutil.hpp"
#include "hexdata.hpp"
#include "bobiter.hpp"
#include "hexmap.hpp"
#include "moveutil.hpp"
#include "b_tables.hpp"
#include "broute.hpp"
#include "scenario.hpp"
#include "pdeploy.hpp"
#include <math.h>
//#include "fsalloc.hpp"
#ifdef DEBUG
#include "random.hpp"
#include "reorglog.hpp"
#endif
#include <utility>

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif


namespace Local {
bool shouldBeVisible(RPBattleData bd, const RefBattleCP& cp, const RefBattleSP& sp)
{
        const UnitTypeItem& uti = bd->ob()->getUnitType(sp);

        switch(uti.getBasicType())
        {
         case BasicUnitType::Infantry:
                return (cp->generic()->isInfantry());

         case BasicUnitType::Artillery:
                return (cp->generic()->isArtillery());

         case BasicUnitType::Cavalry:
                return (cp->generic()->isCavalry());
        }

        return False;
}

inline int squared(int v)
{
        return (v * v);
}

inline double getDist(const HexCord& srcHex, const HexCord& destHex)
{
        return sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y()));
}

inline int getDistI(const HexCord& srcHex, const HexCord& destHex)
{
        return static_cast<int>(sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y())));
}


};

namespace B_Logic
{


/*------------------------------------------------------------------
 * Abstract Deplyment class
 */

class DeployBase {
                PBattleData d_batData;
                BattleOB* d_ob;
        protected:
                struct SPPosition {
                  UBYTE d_r;  // row position
                  UBYTE d_c;  // column position

                  enum { Undefined = UBYTE_MAX };
                                                                };

                                         enum { MaxSP = 12 };
        public:

                                 DeployBase(RPBattleData bd) :
                                                                                                d_batData(bd),
                                                                d_ob(d_batData->ob()) {}
                        virtual ~DeployBase() {}

                                 bool deploy(const RefBattleCP& cp);
                                 bool adjustDeployment(const RefBattleCP& cp);
                                 bool reorganize(const RefBattleCP& cp);

                        // virtual functions
                                 virtual SPPosition findDeployPosition(int oldC, int oldR,
                                                                                                CPDeployHow whichWay, int spCount) = 0;
                                 virtual SPPosition findRedeployPosition(int oldC, int oldR,
                                                                CPDeployHow whichWay, int spCount) = 0;
                                                 static DeployBase* allocate(RPBattleData bd, CPFormation f);

                                 enum WhichSide { LeftSide, RightSide, WhichSide_HowMany };
        protected:

                                 // stuff derived classes use
                                 WhichSide getWhichSide(const RefBattleCP& cp, int c, int r);
                                 bool findDeployHexes(const RefBattleCP& cp, const CPFormation formation,
                                                HexArea& area);
                                 void setToNewMap(DeployItem& di, const RefBattleSP& sp);
                                 bool DeployBase::addToMap(const RefBattleCP& cp, const HexCord& startHex, WhichSide whichWay,
                                                                 int nDir, HexCord::HexDirection hd, int c, const int r, HexArea& area, Boolean skipFirst = False);
                                 bool getDestHex(const RefBattleSP& sp, const RefBattleCP& cp, HexCord& destHex);
                                 bool plotAdjustedRoute(const RefBattleCP& cp);
                                 bool canMoveInto(const RefBattleCP& cp, const HexCord& startHex,
                                                                                                                                const HexCord::HexDirection hd, const int newColumn);
                                 bool getHex(HexCord& hex, const HexCord::HexDirection hd,
                                                                                                                                                                                                const HexCord startHex);

                                 bool nextDirection(const RefBattleCP& cp, const HexCord& hex, HexCord::HexDirection& hd,
                                                                                                                                HexPosition::Facing& newFace, const WhichSide ws, const int newColumn);

                         void clearMove(const RefBattleCP& cp);
                                 bool plotDeployRoute(const RefBattleCP& cp);
                                 bool canExclude(HexPosition::Facing f, const HexCord& h1, const HexCord& h2);
                                 bool stayInPlace(const RefBattleCP& cp, int c, int r);
                                 bool getWaypoint(const RefBattleCP& cp, int c, int r, HexCord& hex);
                                 bool deployHex(const RefBattleCP& cp, const HexCord& hex);
                                                 bool spInWantMap(const RefBattleCP& cp, const RefBattleSP& sp);
                        PBattleData batData() const { return d_batData; }
                                 BattleOB* ob() const { return d_ob; }
};

#define NOPOS SPPosition::Undefined, SPPosition::Undefined

bool DeployBase::spInWantMap(const RefBattleCP& cp, const RefBattleSP& sp)
{
   for(std::vector<DeployItem>::iterator di = cp->mapBegin();
                                 di != cp->mapEnd();
                                 di++)
                 {
                         if(di->d_wantSP == sp)
                                 return True;
                 }

                 return False;
}

bool DeployBase::addToMap(const RefBattleCP& cp, const HexCord& startHex, WhichSide whichWay,
                                                int nDir, HexCord::HexDirection hd, int c, const int r, HexArea& area, Boolean skipFirst)
{
        HexCord hex = startHex;
        while(nDir--)
        {
                ASSERT(c >= 0);
                ASSERT(c < cp->wantColumns());

                // test if hex is occupied by another unit, or we cannot pass due to terrain etc...
                if(!canHexBeOccupied(d_batData, cp, hex, True, False))
                                    return False;

                if(!skipFirst)
                        {
                        cp->addToMap(NoBattleSP, &hex, c, r);
                        area.update(hex);
                }
                else
                {
                        nDir++;
                        skipFirst = False;
                }

                if(whichWay == RightSide)
                        c++;
                else
                        c--;

                if(nDir)
                {
                        HexCord uHex = hex;
                                    if(!d_batData->moveHex(uHex, hd, hex))
                                return False;
                }
        }

        return True;
}

                                /*------------------------- Deployment --------------------------------
                                 * How many Columns? Rows?
                                                 *
                                 * 'March'    is 1 Column   (Single-file)
                                 * 'Massed'   is 2 Columns (2 Single-files)
                                                 * 'Deployed' is 2 Rows  (2 Lines)
                        * 'Extened'  is 1 Row    (Extended Line)
                                 *
                        * Deployment from 'March' to 'Massed' to 'Deployed' to 'Extended' -- Centered:
                                 *   (Note:  1 represents first SP in CP's SPList. This is the 'base' SP)
                                 *
                                 *
                                 *    7 3 5 1 2 6 4 8
                                 *
                                 *        5 1 2 6
                                 *        7 3 4 8
                                 *
                        *          1 2
                                 *          3 4
                                 *          5 6
                                 *          7 8
                                 *
                                 *          1
                                                 *          2
                                 *          3
                                 *          4
                                 *          5
                                 *          6
                                 *          7
                                 *          8
                                 *
                                 *
                                 * From 'March' to 'Massed' to 'Deployed' to 'Extended' -- DeployRight:
                                    *   (Note:  1 represents first SP in CP's SPList. This is the 'base' SP)
                                 *
                        *          1 2 5 6 3 4 7 8
                                                 *
                                 *          1 2 5 6
                                 *          3 4 7 8
                                 *
                                 *          1 2
                                 *          3 4
                                 *          5 6
                                 *          7 8
                        *
                                 *          1
                                 *          2
                                 *          3
                                 *          4
                                 *          5
                                 *          6
                                 *          7
                                 *          8
                                 *
                                                 *
                                 * From 'March' to 'Massed' to 'Deployed' to 'Extended' -- DeployLeft:
                                 *   (Note: uppercase 1 represents first SP in CP's SPList. This is the 'base' SP)
                                 *
                                 *     8 7 4 3 6 5 2 1
                                 *
                        *             6 5 2 1
                                 *             8 7 4 3
                        *
                        *                 2 1
                                    *                 4 3
                        *                 6 5
                        *                 8 7
                                    *
                        *                   1
                        *                   2
                        *                   3
                        *                   4
                        *                   5
                        *                   6
                        *                   7
                        *                   8
                        */

bool DeployBase::findDeployHexes(const RefBattleCP& cp, const CPFormation formation,
        HexArea& area)
{
        const int spCount = cp->spCount(); //BobUtility::countSP(cp);
        ASSERT(spCount > 0);
        const HexPosition::Facing facing = cp->facing();
        bool deploying = (cp->formation() < cp->nextFormation());

        HexCord hex;
        if(deploying || cp->deployWhichWay() != CPD_DeployCenter)
        {
                hex = (cp->deployWhichWay() == CPD_DeployLeft) ? cp->rightHex() : cp->leftHex();
        }
        else
        {
                hex = cp->centerHex();
        }

        /*
         * Calculate new rows and columns
             */

        int columns;
        int rows;
        BobUtility::getRowsAndColoumns(formation, spCount, columns, rows);

        cp->wantRows(rows);
        cp->wantColumns(columns);

        // Unchecked
//        vector<DeployItem>& map = cp->deployMap();
        // End

        HexCord nextHex = hex;
        BattleCP::HexOffset whichWay = cp->whichWay();

        CPDeployHow dh = ((!deploying && cp->formation() <= CPF_Deployed || deploying && cp->nextFormation() < CPF_Deployed) && cp->deployWhichWay() == CPD_DeployCenter) ?
                                        CPD_DeployRight : cp->deployWhichWay();

        for(int r = 0; r < rows; r++)
        {
                        switch(dh)
                {
                        case CPD_DeployRight:
                        {
                                HexCord::HexDirection d = rightFlank(facing);
                                if(!addToMap(cp, nextHex, RightSide, columns, d, 0, r, area))
                                        return False;

                                break;
                        }

                        case CPD_DeployCenter:
                        {
                                 //ASSERT(columns >= 3);

                                // how many do we add to right side?  columns / 2 rounded + 1
                                int nDirR = (deploying) ? (cp->columns() + (((columns - cp->columns()) + 1) / 2)) : columns; //((columns + 1) / 2) + 1;
                                // how many to the left? columns / 2 exclusive
                                int nDirL = (deploying) ? columns - nDirR : 0;
                                ASSERT(nDirL >= 0);

                                if(!addToMap(cp, nextHex, RightSide, nDirR, rightFlank(facing), nDirL, r, area) ||
                                         !addToMap(cp, nextHex, LeftSide, nDirL, leftFlank(facing), nDirL, r, area, True))
                                {
                                        return False;
                                }
                                break;
                        }

                        case CPD_DeployLeft:
                        {
                                                const int lc = cp->columns() - 1;
                                int nDir = columns;

                                if(!addToMap(cp, nextHex, LeftSide, nDir, leftFlank(facing), columns - 1, r, area))
                                {
                                        return False;
                                }

                                break;
                        }
                        }

                HexCord::HexDirection hd = (whichWay == BattleCP::HO_Left) ? rightRear(facing) : leftRear(facing);
                        whichWay = (whichWay == BattleCP::HO_Left) ? BattleCP::HO_Right : BattleCP::HO_Left;
                HexCord h;
                if(d_batData->moveHex(nextHex, hd, h))
                        nextHex = h;
                else
                        return False;
        }

        return True;
}


// determine if an SP stays in its current place whilst redeploying
bool DeployBase::stayInPlace(const RefBattleCP& cp, int c, int r)
{
        // if deploying
        if(cp->formation() < cp->nextFormation())
        {
                                                 static const UBYTE s_staticRows[CPF_Last] = {
                1, 2, 1, 1
         };

         return (r < s_staticRows[cp->formation()]);
  }

        // if redeploying
        else
  {
             switch(cp->deployWhichWay())
         {
                                                                case CPD_DeployRight:
                                                                        switch(cp->formation())
                  {
                         case CPF_Massed:
                                return (c == 0 && r == 0);
                                                                                                 case CPF_Deployed:
                                return (c < 2 && r < 2);
                         case CPF_Extended:
                                break;
                  }
                  break;

                case CPD_DeployCenter:
                {
                  int startC = (cp->columns() % 2 == 0) ? maximum(0, (cp->columns() / 2) - 1) :
                                        cp->columns() / 2;

                  if(cp->formation() == CPF_Deployed)
                  {
                                     return (r < 2 && (c == startC || c == startC + 1));
                  }
                                                                        break;
                                         }

                case CPD_DeployLeft:
                  if(cp->formation() == CPF_Massed)
                  {
                                                                                                 return (r == 0 && c == cp->columns() - 1);
                                                }

                  break;
                                 }

         return False;
  }
}

bool DeployBase::deployHex(const RefBattleCP& cp, const HexCord& hex)
{
   for(std::vector<DeployItem>::iterator di = cp->mapBegin();
                di != cp->mapEnd();
                di++)
  {
         if(di->active() && di->d_wantHex == hex)
                return True;
  }

        return False;
}

bool DeployBase::getWaypoint(const RefBattleCP& cp, int c, int r, HexCord& hex)
{
   bool deploying = (cp->formation() < cp->nextFormation());
   //vector<DeployItem>& map = cp->deployMap();

   // Unchecked

   // End

   switch(cp->formation())
   {
      case CPF_March:
// Unchecked
         if(deploying)
         {
            // the way point is the hex to the left or right
            // of the last hex in the new row
            if(r > cp->wantRows() + 1)
            {
               HexCord::HexDirection hd = (cp->deployWhichWay() == CPD_DeployRight) ?
                  rightFlank(cp->facing()) : leftFlank(cp->facing());

               // Unchecked
               int c = 0;//(cp->deployWhichWay() == CPD_DeployRight) ? 0 : 1;
               // End
#if 1
               int r = cp->wantRows() + 1;
               DeployItem* di = cp->currentDeployItem(c, r); //cp->map[(cp->columns() * cp->wantRows()) + c];
               ASSERT(di);
               if(di)
               {
                  if(!deployHex(cp, di->d_hex) && d_batData->moveHex(di->d_hex, hd, hex))
                     return True;
               }
#else
               DeployItem& di = map[(cp->columns() * cp->wantRows()) + c];
               if(!deployHex(cp, di.d_hex) && d_batData->moveHex(di.d_hex, hd, hex))
                  return True;
#endif
            }
         }
         else
         {
            if(r > cp->rows())
            {
#if 1
               int c = (cp->deployWhichWay() == CPD_DeployRight) ? 0 : 1;
               int r = cp->rows() - 1;
               DeployItem* di = cp->currentDeployItem(c, r); //map[(cp->columns() * (cp->rows() - 1)) + c];
               ASSERT(di);
               if(di)
               {
                  hex = di->d_wantHex;
                  return True;
               }
#else
               int c = (cp->deployWhichWay() == CPD_DeployRight) ? 0 : 1;
               DeployItem& di = map[(cp->columns() * (cp->rows() - 1)) + c];
               hex = di.d_wantHex;
                  return True;
#endif
            }
         }
         break;

         case CPF_Massed:
            if(deploying)
            {
               // the way point is the 3rd row of this column
               if(r > 2)
               {
                  int col = (r == 4 && c == 0 && cp->deployWhichWay() == CPD_DeployCenter) ?
                     1 : c;
#if 1
                  int r = 0;

                  DeployItem* di = cp->currentDeployItem(c, r);
                  ASSERT(di);
                  if(di && di->active() && !deployHex(cp, di->d_sp->hex()))
                  {
                     hex = di->d_sp->hex();
                     return True;
                  }
#else
                  DeployItem& di = map[(cp->columns() * 2) + col];
                  if(!deployHex(cp, di.d_sp->hex()))
                  {
                     hex = di.d_sp->hex();
                     return True;
                  }
#endif
               }
            }
            else
            {
            }
            break;

         case CPF_Deployed:
            if(deploying)
            {
            }
            else
            {
            }
            break;

         case CPF_Extended:
            if(deploying)
            {
            }
            else
            {
            }
            break;
   }

   return False;
}

bool DeployBase::plotDeployRoute(const RefBattleCP& cp)
{
//   vector<DeployItem>& map = cp->deployMap();
   bool deploying = (cp->formation() < cp->nextFormation());
   // add exclude hexes
   // Unchecked
   //HexList excludeHexes;
   // End
   //  addExcludeHexes(cp, excludeHexes);

   // go through map and add units staying in place to exclude list
   int start = (cp->deployWhichWay() == CPD_DeployLeft) ? 0 : cp->columns() - 1;
   int end = (cp->deployWhichWay() == CPD_DeployLeft) ? cp->columns() : -1;
   int inc = (cp->deployWhichWay() == CPD_DeployLeft) ? 1 : -1;

   for(int r = 0; r < cp->rows(); r++)
   {
      for(int c = start; c != end; c += inc)
      {
//         int index = maximum(0, (cp->columns() * r) + c);   // column = 0, row = 2
         DeployItem* di = cp->currentDeployItem(c, r);
         ASSERT(di);
//         DeployItem& di = map[index];

         if(!di || !di->active())
            continue;

         // get final dest
         HexCord hex;
         if(getDestHex(di->d_sp, cp, hex))
            ;
         else
         {
            FORCEASSERT("Hex not found");
            return False;
         }

         // if this is row 1 or 2, just add hex to excluded hexes
         // as they remain in place
         // Unchecked
         //if(di->d_sp->hex() == hex)//stayInPlace(cp, c, r)) //r < s_staticRows[cp->formation()])
         //{
           // excludeHexes.newItem(di->d_sp->hex());
         //}
         // End
      }
   }

   bool canCrossBridge = (cp->formation() == CPF_March && cp->movingOverBridge());

   // Reiterate through list and plot route for each SP
   for(r = 0; r < cp->rows(); r++)
   {
      for(int c = start; c != end; c += inc)
      {
//         int index = maximum(0, (cp->columns() * r) + c);   // column = 0, row = 2
//         DeployItem& di = map[index];
         DeployItem* di = cp->currentDeployItem(c, r);
         ASSERT(di);

         if(!di || !di->active())
            continue;

         di->d_sp->routeList().reset();

         // Unchecked
         //HexList list;
         HexCord hex;

         // find intermediate dest, if any
         //if(getWaypoint(cp, c, r, hex))
            //list.newItem(hex);

         // get final dest
#if 1
         if(!getDestHex(di->d_sp, cp, hex))
         {
            FORCEASSERT("Hex not found");
            return False;
         }
#else
         if(getDestHex(di->d_sp, cp, hex))
            list.newItem(hex);
         else
         {
            FORCEASSERT("Hex not found");
            return False;
         }
#endif
         if(di->d_sp->hex() != hex)
         {
            B_Route br(batData(), cp, di->d_sp);
            //bool routePlotted = False;
            //if(canCrossBridge)
               //routePlotted = br.plotRoute(di->d_sp->hex(), hex, 0, False);
            //else
               //routePlotted = br.plotRoute(di->d_sp->hex(), list, &excludeHexes, True);

            //if(routePlotted)
            if(br.plotRoute(di->d_sp->hex(), hex, 0, False))
            {
               // remove first nodes, since they are equal to current hex
               ASSERT(di->d_sp->routeList().entries() > 0);

               // update facings, maintain current face
               // until last hex, then switch to cp facing
               SListIter<HexItem> iter(&di->d_sp->routeList());
               while(++iter)
               {
                  // go througg each node an make sure we can move
                  #ifdef DEBUG
                  if(!hexNotOccupied(d_batData, iter.current()->d_hex, 0, cp, reorgLog) ||
                     !canHexBeOccupied(d_batData, cp, iter.current()->d_hex, True, canCrossBridge) )
                  #else
                  if(!hexNotOccupied(d_batData, iter.current()->d_hex, 0, cp) ||
                     !canHexBeOccupied(d_batData, cp, iter.current()->d_hex, True, canCrossBridge) )
                  #endif
                  {
                     return False;
                  }

                  iter.current()->d_facing = (iter.current() != di->d_sp->routeList().getLast()) ?
                     di->d_sp->facing() : cp->facing();
               }

               // if deploying (but not while redeploying)
               // add hex to excluded list
               //excludeHexes.newItem(hex);
            }
            else
            {
               return False;
            }
         }
         else if(di->d_sp->facing() != cp->facing())
            di->d_sp->routeList().newItem(hex, cp->facing());
         // End
      }
   }
   return True;
}

bool DeployBase::canExclude(HexPosition::Facing f, const HexCord& h1, const HexCord& h2)
{

  return True;
}

void DeployBase::setToNewMap(DeployItem& di, const RefBattleSP& sp)
{
        sp->setMoving(True);
  di.d_wantSP = sp;
}



bool DeployBase::getHex(HexCord& hex, const HexCord::HexDirection hd,
  const HexCord startHex)
{
  if(d_batData->moveHex(startHex, hd, hex))
   {
                                 return True;
  }

  FORCEASSERT("Hex not found! -- getHex()");
  return False;
}

bool DeployBase::canMoveInto(const RefBattleCP& cp, const HexCord& startHex,
  const HexCord::HexDirection hd, const int newColumn)
{
   HexCord hex;
   if(d_batData->moveHex(startHex, hd, hex))
   {
      /*
       * See if hex is in new map
       */

      for(int r = 0; r < cp->wantRows(); r++)
      {
         for(int c = 0; c < cp->wantColumns(); c++)
         {
            DeployItem* cdi = cp->wantDeployItem(c, r);
            ASSERT(cdi);

            if(cdi && cdi->d_wantHex == hex)
               return (c == newColumn);
         }
      }

      return True;
  }

  return False;
}

bool DeployBase::nextDirection(const RefBattleCP& cp, const HexCord& hex,
  HexCord::HexDirection& hd, HexPosition::Facing& newFace,
  const WhichSide ws, const int newColumn)
{
  // see if we can go to the front
        if(canMoveInto(cp, hex, leftFront(cp->facing()), newColumn))
             hd = leftFront(cp->facing());

  else if(canMoveInto(cp, hex, rightFront(cp->facing()), newColumn))
             hd = rightFront(cp->facing());

        // other wise go to left or right flank
  else
        {
                        switch(ws)
                                 {
                case LeftSide:
                  hd = leftFlank(cp->facing());
//                newFace = leftTurn(cp->facing());
                  return True;

                case RightSide:
//                newFace = rightTurn(cp->facing());
                  hd = rightFlank(cp->facing());
                  return True;

                default:
                           FORCEASSERT("Hex Direction not found");
                  return False;
         }
  }

  return True;
}

bool DeployBase::plotAdjustedRoute(const RefBattleCP& cp)
{
  ASSERT(cp->formation() <= CPF_Massed);
  ASSERT(cp->rows() == cp->wantRows());
  ASSERT(cp->columns() == cp->wantColumns());
  Boolean adjusted = False;
//  HexList excludeHexes;
  int start = (cp->deployWhichWay() == CPD_DeployLeft) ? 0 : cp->wantColumns() - 1;
  int end = (cp->deployWhichWay() == CPD_DeployLeft) ? cp->wantRows() : -1;
  int inc = (cp->deployWhichWay() == CPD_DeployLeft) ? 1 : -1;
  for(int r = 0; r < cp->wantRows(); r++)
  {
         for(int c = start; c != end; c += inc)
         {
                DeployItem* di = cp->wantDeployItem(c, r);
                ASSERT(di);
                //DeployItem& di = cp->deployItem(c, r);
                if(!di || !di->active())
                  continue;

                ASSERT(di->d_wantSP == NoBattleSP);
//              ASSERT(di.d_sp->hex() == di.d_hex);
                di->d_wantSP = di->d_sp;
                if(di->d_sp->hex() != di->d_wantHex)
                {
                  di->d_sp->routeList().reset();

                  B_Route br(batData(), cp, di->d_sp);
                  if(!br.plotRoute(di->d_sp->hex(), di->d_wantHex, 0, !cp->movingOverBridge()))
                     return False;//FORCEASSERT("Route not found");
                  else
                  {
                    // update facings, maintain current face
                    // until last hex, then switch to cp facing
                    SListIter<HexItem> iter(&di->d_sp->routeList());
                    while(++iter)
                    {
                      // go througg each node an make sure we can move their
                      #ifdef DEBUG
                      if(!hexNotOccupied(d_batData, iter.current()->d_hex, 0, cp, reorgLog))
                      #else
                      if(!hexNotOccupied(d_batData, iter.current()->d_hex, 0, cp))
                      #endif
                      {
                        return False;
                      }
                      iter.current()->d_facing = (iter.current() != di->d_sp->routeList().getLast()) ?
                            di->d_sp->facing() : cp->facing();
                    }

                    di->d_sp->setMoving(True);
                    adjusted = True;
                    //excludeHexes.newItem(di.d_wantHex);
                  }
                }
         }
  }

  return adjusted;
}

bool DeployBase::adjustDeployment(const RefBattleCP& cp)
{
        // plot destination hexes
        if(cp->sp() != NoBattleSP && cp->formation() <= CPF_Massed)
        {
                                        // useful constants
          const int spCount = cp->spCount();
          ASSERT(spCount > 0);

          HexCord hex = (cp->deployWhichWay() == CPD_DeployLeft) ? cp->rightHex() :
                   (cp->deployWhichWay() == CPD_DeployCenter) ? cp->centerHex() : cp->leftHex();

          // get new deploy map
          HexArea area;
          if(!findDeployHexes(cp, cp->formation(), area))
            return False;

          if(plotAdjustedRoute(cp))
          {
#if 0
             RefBattleCP attachedTo = BobUtility::getAttachedLeader(batData(), cp);
             if(attachedTo != NoBattleCP)
             {
               plotHQRoute(batData(), attachedTo);
                attachedTo->setMoving(True);
             }
#endif
             return True;

          }
          else
            clearMove(cp);
        }

        return False;
}

DeployBase::WhichSide DeployBase::getWhichSide(const RefBattleCP& cp, int c, int r)
{
        WhichSide ws = RightSide;
  Boolean found = False;

        // find which way we want to move
        switch(cp->deployWhichWay())
   {
         case CPD_DeployLeft:
                                                                ws = LeftSide;
                                         break;

         case CPD_DeployCenter:
                if( (c >= (cp->columns() / 2)) ||
                         (r == cp->rows() - 1 && (cp->rows() % 2) == 0) )
                {
                  ws = RightSide;
                        }
                else
                {
                           ws = LeftSide;
                                         }
                                                                break;

                                 case CPD_DeployRight:
                ws = RightSide;
                break;

#ifdef DEBUG
         default:
                FORCEASSERT("Hex Direction not found");
#endif
        }

  return ws;
}

bool DeployBase::getDestHex(const RefBattleSP& sp, const RefBattleCP& cp, HexCord& destHex)
{
   for(std::vector<DeployItem>::iterator wantDI = cp->mapBegin();
       wantDI != cp->mapEnd();
       wantDI++)
   {
      if(wantDI->active() && wantDI->d_wantSP == sp)
      {
         destHex = wantDI->d_wantHex;
         return True;
      }
   }

   return False;
}

/*---------------------------------------------------------------------
 * Derived deploy classes
 */

// 'March'
class DeployMarch : public DeployBase {
  public:
         DeployMarch(RPBattleData bd) : DeployBase(bd) {}
         ~DeployMarch() {}

   DeployBase::SPPosition findDeployPosition(int oldC, int oldR,
           CPDeployHow whichWay, int spCount)
   {
      FORCEASSERT("findDeployPosition should not be called in DeployMarch");
      // SPPosition spPos;
      // return spPos;
      return SPPosition();
   }

   DeployBase::SPPosition findRedeployPosition(int oldC, int oldR,
         CPDeployHow whichWay, int spCount);
};

DeployMarch::SPPosition DeployMarch::findRedeployPosition(int oldC, int oldR,
  CPDeployHow whichWay, int spCount)
{
  ASSERT(oldR < MaxSP / 2);
  const int maxColumns = 2;
   static const SPPosition s_right[MaxSP / 2][maxColumns] = {
                                        { {  0, 0 }, {  1, 0} },
                         { {  2, 0 }, {  3, 0} },
               { {  4, 0 }, {  5, 0} },
                                        { {  6, 0 }, {  7, 0} },
                                        { {  8, 0 }, {  9, 0} },
          { { 10, 0 }, { 11, 0} }
  };

  static const SPPosition s_left[MaxSP][maxColumns] = {
                                        { {  1, 0 }, {  0, 0} },
          { {  3, 0 }, {  2, 0} },
                         { {  5, 0 }, {  4, 0} },
          { {  7, 0 }, {  6, 0} },
          { {  9, 0 }, {  8, 0} },
          { { 11, 0 }, { 10, 0} }
  };

  return (whichWay == CPD_DeployLeft) ?
          s_left[oldR][oldC] : s_right[oldR][oldC];
}

//----------------------------------------------------------------------
// 'Massed'
class DeployMassed : public DeployBase {
  public:
         DeployMassed(RPBattleData bd) : DeployBase(bd) {}
                                 ~DeployMassed() {}

//       void deploySP(const RefBattleCP& cp, const int spCount);
//       void redeploySP(const RefBattleCP& cp, const int spCount);
//       bool addExcludeHexes(const RefBattleCP& cp, HexList& excludeList);
         SPPosition findDeployPosition(int oldC, int oldR,
                CPDeployHow whichWay, int spCount);
                                 SPPosition findRedeployPosition(int oldC, int oldR,
                CPDeployHow whichWay, int spCount);
};

DeployMassed::SPPosition DeployMassed::findDeployPosition(int oldC, int oldR,
  CPDeployHow whichWay, int spCount)
{
  const int c_cutOff = 1;
  ASSERT(oldC == 0);
  ASSERT(oldR < MaxSP - c_cutOff);
  ASSERT(spCount - (c_cutOff + 1) >= 0);
  ASSERT(spCount - (c_cutOff + 1) < MaxSP - c_cutOff);
  ASSERT(oldR <= spCount);

  static const SPPosition s_right[MaxSP - c_cutOff][MaxSP] = {
                                 // 2 SP
                        {
                { 0, 0 }, { 0, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
             },
                                 // 3 SP
         {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                        },
                                 // 4 SP
         {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
         // 5 SP
                                                 {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { 2, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 6 SP
                        {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { 2, 0 }, { 2, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 7 SP
         {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { 2, 0 }, { 2, 1 }, { 3, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                                 // 8 SP
         {
                                                                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { 2, 0 }, { 2, 1 }, { 3, 0 }, { 3, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                        // 9 SP
         {
                                                                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { 2, 0 }, { 2, 1 }, { 3, 0 }, { 3, 1 }, { 4, 0 }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 10 SP
                                 {
                                                             { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { 2, 0 }, { 2, 1 }, { 3, 0 }, { 3, 1 }, { 4, 0 }, { 4, 1 }, { NOPOS }, { NOPOS }
         },
         // 11 SP
                                 {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { 2, 0 }, { 2, 1 }, { 3, 0 }, { 3, 1 }, { 4, 0 }, { 4, 1 }, { 5, 0 }, { NOPOS }
                                 },
         // 12 SP
         {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { 2, 0 }, { 2, 1 }, { 3, 0 }, { 3, 1 }, { 4, 0 }, { 4, 1 }, { 5, 0 }, { 5, 1 }
         }
   };

  static const SPPosition s_left[MaxSP - c_cutOff][MaxSP] = {
         // 2 SP
         {
                { 0, 1 }, { 0, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
         // 3 SP
                                 {
                { 0, 1 }, { 0, 0 }, { 1, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                        // 4 SP
                                 {
                { 0, 1 }, { 0, 0 }, { 1, 1 }, { 1, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                                 // 5 SP
                                 {
                                         { 0, 1 }, { 0, 0 }, { 1, 1 }, { 1, 0 }, { 2, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                                 // 6 SP
         {
                                                                                                { 0, 1 }, { 0, 0 }, { 1, 1 }, { 1, 0 }, { 2, 1 }, { 2, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 7 SP
         {
                { 0, 1 }, { 0, 0 }, { 1, 1 }, { 1, 0 }, { 2, 1 }, { 2, 0 }, { 3, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                        // 8 SP
         {
                { 0, 1 }, { 0, 0 }, { 1, 1 }, { 1, 0 }, { 2, 1 }, { 2, 0 }, { 3, 1 }, { 3, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
             // 9 SP
                                 {
                { 0, 1 }, { 0, 0 }, { 1, 1 }, { 1, 0 }, { 2, 1 }, { 2, 0 }, { 3, 1 }, { 3, 0 }, { 4, 0 }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
         // 10 SP
         {
                { 0, 1 }, { 0, 0 }, { 1, 1 }, { 1, 0 }, { 2, 1 }, { 2, 0 }, { 3, 1 }, { 3, 0 }, { 4, 1 }, { 4, 0 }, { NOPOS }, { NOPOS }
                        },
         // 11 SP
         {
                                                                { 0, 1 }, { 0, 0 }, { 1, 1 }, { 1, 0 }, { 2, 1 }, { 2, 0 }, { 3, 1 }, { 3, 0 }, { 4, 1 }, { 4, 0 }, { 5, 0 }, { NOPOS }
                                 },
         // 12 SP
                        {
                                                                { 0, 1 }, { 0, 0 }, { 1, 1 }, { 1, 0 }, { 2, 1 }, { 2, 0 }, { 3, 1 }, { 3, 0 }, { 4, 1 }, { 4, 0 }, { 5, 1 }, { 5, 0 }
         }
        };

  return (whichWay == CPD_DeployLeft) ? s_left[spCount - (c_cutOff + 1)][oldR] :
          s_right[spCount - (c_cutOff + 1)][oldR];
}

DeployMassed::SPPosition DeployMassed::findRedeployPosition(int oldC, int oldR,
  CPDeployHow whichWay, int spCount)
{
  const int c_maxRows = 2;
  const int c_cutOff = 4;
        const int c_maxColumns = 2;
  ASSERT(oldC < MaxSP - c_cutOff);
        ASSERT(oldR < c_maxRows);
  ASSERT(spCount - (c_cutOff + 1) >= 0);
   ASSERT(spCount - (c_cutOff + 1) < MaxSP - c_cutOff);

        static const SPPosition s_right[MaxSP - c_cutOff][c_maxRows][MaxSP / 2] = {
                         // 5 SP
          {
                                                                 { { 0, 0 }, { 0, 1 }, { 2, 0 },  { NOPOS }, { NOPOS }, { NOPOS } },
                                                                 { { 1, 0 }, { 1, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
          },
          // 6 SP
                         {
                 { { 0, 0 }, { 0, 1 }, { 2, 1 },  { NOPOS }, { NOPOS }, { NOPOS } },
                                                                 { { 1, 0 }, { 1, 1 }, { 2, 0 },  { NOPOS }, { NOPOS }, { NOPOS } }
          },
          // 7 SP
          {
                 { { 0, 0 }, { 0, 1 }, { 2, 0 },  { 2, 1 },  { NOPOS }, { NOPOS } },
                 { { 1, 0 }, { 1, 1 }, { 3, 0 },  { NOPOS }, { NOPOS }, { NOPOS } }
          },
          // 8 SP
                         {
                 { { 0, 0 }, { 0, 1 }, { 2, 0 },  { 2, 1 },  { NOPOS }, { NOPOS } },
                         { { 1, 0 }, { 1, 1 }, { 3, 0 },  { 3, 1 },  { NOPOS }, { NOPOS } }
                                        },
          // 9 SP
                                        {
                 { { 0, 0 }, { 0, 1 }, { 2, 0 },  { 2, 1 },  { 4, 0 },  { NOPOS } },
                 { { 1, 0 }, { 1, 1 }, { 3, 0 },  { 3, 1 },  { NOPOS }, { NOPOS } }
          },
                                        // 10 SP
          {
                                          { { 0, 0 }, { 0, 1 }, { 2, 0 },  { 2, 1 },  { 4, 1 },  { NOPOS } },
                                                                                                 { { 1, 0 }, { 1, 1 }, { 3, 0 },  { 3, 1 },  { 4, 0 },  { NOPOS } }
                                        },
          // 11 SP
          {
                                                                 { { 0, 0 }, { 0, 1 }, { 2, 0 },  { 2, 1 },  { 4, 0 },  { 4, 1 } },
                                          { { 1, 0 }, { 1, 1 }, { 3, 0 },  { 3, 1 },  { 5, 0 },  { NOPOS } }
                                        },
          // 12 SP
          {
                 { { 0, 0 }, { 0, 1 }, { 2, 0 },  { 2, 1 },  { 4, 0 },  { 4, 1 } },
                 { { 1, 0 }, { 1, 1 }, { 3, 0 },  { 3, 1 },  { 5, 0 },  { 5, 1 } }
          }
  };

  static const SPPosition s_left[MaxSP - c_cutOff][c_maxRows][MaxSP / 2] = {
                         // 5 SP
          {
                                                                 { { 2, 0 }, { 0, 0 }, { 0, 1 },  { NOPOS }, { NOPOS }, { NOPOS } },
                 { { 1, 0 }, { 1, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
                                        },
          // 6 SP
               {
                 { { 2, 0 }, { 0, 0 }, { 0, 1 },  { NOPOS }, { NOPOS }, { NOPOS } },
                                                                 { { 2, 1 }, { 1, 0 }, { 1, 1 },  { NOPOS }, { NOPOS }, { NOPOS } }
          },
          // 7 SP
                         {
                                                                 { { 2, 0 }, { 2, 1 }, { 0, 0 },  { 0, 1 },  { NOPOS }, { NOPOS } },
                 { { 3, 0 }, { 1, 0 }, { 1, 1 },  { NOPOS }, { NOPOS }, { NOPOS } }
          },
                                        // 8 SP
               {
                                                { { 2, 0 }, { 2, 1 }, { 0, 0 },  { 0, 1 },  { NOPOS }, { NOPOS } },
                 { { 3, 0 }, { 3, 1 }, { 1, 0 },  { 1, 1 },  { NOPOS }, { NOPOS } }
          },
          // 9 SP
          {
                 { { 3, 0 }, { 2, 0 }, { 2, 1 },  { 0, 0 },  { 0, 1 },  { NOPOS } },
                 { { 4, 0 }, { 3, 1 }, { 1, 0 },  { 1, 1 },  { NOPOS }, { NOPOS } }
          },
          // 10 SP
          {
                                          { { 4, 0 }, { 2, 0 }, { 2, 1 },  { 0, 0 },  { 0, 1 },  { NOPOS } },
                                                                 { { 4, 1 }, { 3, 0 }, { 3, 1 },  { 1, 0 },  { 1, 1 },  { NOPOS } }
          },
                                        // 11 SP
          {
                 { { 4, 0 }, { 4, 1 }, { 2, 0 },  { 2, 1 },  { 0, 0 },  { 0, 1 } },
                 { { 5, 0 }, { 3, 0 }, { 3, 1 },  { 1, 0 },  { 1, 1 },  { NOPOS } }
                                        },
          // 12 SP
          {
                                                                                                 { { 4, 0 }, { 4, 1 }, { 2, 0 },  { 2, 1 },  { 0, 0 },  { 0, 1 } },
                                                { { 5, 0 }, { 5, 1 }, { 3, 0 },  { 3, 1 },  { 1, 0 },  { 1, 1 } }
          }
  };

  return (whichWay == CPD_DeployLeft) ?
                                                                                                                                s_left[spCount - (c_cutOff + 1)][oldR][oldC] :
                                                                                  s_right[spCount - (c_cutOff + 1)][oldR][oldC];
}

//---------------------------------------------------------------
// 'Deployed'
class DeployDeployed : public DeployBase {
  public:
         DeployDeployed(RPBattleData bd) : DeployBase(bd) {}
         ~DeployDeployed() {}

                        SPPosition findDeployPosition(int oldC, int oldR,
                CPDeployHow whichWay, int spCount);
                                 SPPosition findRedeployPosition(int oldC, int oldR,
                CPDeployHow whichWay, int spCount);
};

DeployDeployed::SPPosition DeployDeployed::findDeployPosition(int oldC, int oldR,
                CPDeployHow whichWay, int spCount)
{
        const int c_cutOff = 4;
        const int c_maxColumns = 2;
  ASSERT(oldR < MaxSP - c_cutOff);
  ASSERT(oldC < c_maxColumns);
#if 0
   if(spCount <= c_cutOff)
        {
         static const SPPosition s_pos[MaxSP / 2][c_maxColumns] = {
                                         { {  0, 0  }, {  0, 1  } },
                { {  1, 0  }, {  1, 1  } },
                { {  NOPOS }, {  NOPOS } },
                { {  NOPOS }, {  NOPOS } },
                { {  NOPOS }, {  NOPOS } },
                { {  NOPOS }, {  NOPOS } }
         };

         return s_pos[oldR][oldC];
        }
#endif
        ASSERT(spCount - (c_cutOff + 1) >= 0);
  ASSERT(spCount - (c_cutOff + 1) < MaxSP - c_cutOff);

  static const SPPosition s_right[MaxSP - c_cutOff][MaxSP / 2][c_maxColumns] = {
                                 { // 5 SP
                { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                                                                { {  0, 2 },                                         {  SPPosition::Undefined, SPPosition::Undefined } },
                                                                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                         { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                 },
         { // 6 SP
                                                                { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                { {  1, 2 }, {  0, 2 } },
                                         { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
                        { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
         },
         { // 7 SP
                { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                { {  0, 2 }, {  0, 3 } },
                                                                { {  1, 2 }, {  SPPosition::Undefined, SPPosition::Undefined } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                         { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
             },
         { // 8 SP
                { {  0, 0 }, {  0, 1 } },
                                                                { {  1, 0 }, {  1, 1 } },
                { {  0, 2 }, {  0, 3 } },
                { {  1, 2 }, {  1, 3 } },
                                                                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                                                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
         },
         { // 9 SP
                                         { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                                                                { {  0, 2 }, {  0, 3 } },
                { {  1, 2 }, {  1, 3 } },
                { {  0, 4 }, {  SPPosition::Undefined, SPPosition::Undefined} },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                        },
         { // 10 SP
                { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                { {  0, 2 }, {  0, 3 } },
                        { {  1, 2 }, {  1, 3 } },
                { {  1, 4 }, {  0, 4 } },
                                                                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
         },
                                 { // 11 SP
                                         { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                { {  0, 2 }, {  0, 3 } },
                                                                { {  1, 2 }, {  1, 3 } },
                { {  1, 4 }, {  0, 4 } },
                        { {  0, 5 }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                 },
                                 { // 12 SP
                { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                                                                { {  0, 2 }, {  0, 3 } },
                                         { {  1, 2 }, {  1, 3 } },
                                                                { {  1, 4 }, {  0, 4 } },
                { {  0, 5 }, {  1, 5 } },
         },
  };

  static const SPPosition s_center[MaxSP - c_cutOff][MaxSP / 2][c_maxColumns] = {
         { // 5 SP
                { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                { {  0, 2 },                                         {  SPPosition::Undefined, SPPosition::Undefined } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
                                                                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                 },
             { // 6 SP
                                         { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                                                                { {  1, 2 }, {  0, 2 } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                                                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                 },
         { // 7 SP
                { {  0, 1 }, {  0, 2 } },
                                                                                                { {  1, 1 }, {  1, 2 } },
                { {  0, 0 }, {  0, 3 } },
                                         { {  1, 0 }, {  SPPosition::Undefined, SPPosition::Undefined } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
         },
         { // 8 SP
                { {  0, 1 }, {  0, 2 } },
                                         { {  1, 1 }, {  1, 2 } },
                { {  0, 0 }, {  0, 3 } },
                { {  1, 0 }, {  1, 3 } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                 },
         { // 9 SP
                                                                { {  0, 1 }, {  0, 2 } },
                { {  1, 1 }, {  1, 2 } },
                { {  0, 0 }, {  0, 3 } },
                                                                { {  1, 0 }, {  0, 4 } },
                                                                { {  1, 3 }, {  NOPOS } },
                                                                { {  NOPOS }, { NOPOS } },
                                                 },
                                 { // 10 SP
                                                                { {  0, 1 }, {  0, 2 } },
                                                                { {  1, 1 }, {  1, 2 } },
                                                                { {  0, 0 }, {  0, 4 } },
                                                                { {  1, 0 }, {  1, 4 } },
                                                                { {  1, 3 }, {  0, 3 } },
                                                                { {  NOPOS }, { NOPOS } },
                        },
                                 { // 11 SP
                                                                                                { {  0, 2 }, {  0, 3 } },
                                                                { {  1, 2 }, {  1, 3 } },
                                                                { {  0, 1 }, {  0, 4 } },
                                                                { {  1, 1 }, {  1, 4 } },
                                                                { {  0, 0 }, {  0, 5 } },
                                                                { {  1, 0 }, {  NOPOS } },
                                 },
                                 { // 12 SP
                                                                { {  0, 2 }, {  0, 3 } },
                                                                { {  1, 2 }, {  1, 3 } },
                                                                { {  0, 1 }, {  0, 4 } },
                { {  1, 1 }, {  1, 4 } },
                { {  0, 0 }, {  0, 5 } },
                { {  1, 0 }, {  1, 5 } },
                        },
  };

        static const SPPosition s_left[MaxSP - c_cutOff][MaxSP / 2][c_maxColumns] = {
                                 { // 5 SP
                { {  0, 1 }, {  0, 2 } },
                { {  1, 0 }, {  1, 1 } },
                                                                                                { {  0, 0 },                                         {  SPPosition::Undefined, SPPosition::Undefined } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
                                                                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                        },
         { // 6 SP
                { {  0, 1 }, {  0, 2 } },
                { {  1, 1 }, {  1, 2 } },
                { {  0, 0 }, {  1, 0 } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
                                                             { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
         },
                                 { // 7 SP
                { {  0, 2 }, {  0, 3 } },
                                                                { {  1, 1 }, {  1, 2 } },
                { {  0, 1 }, {  1, 0 } },
                { {  0, 0 }, {  SPPosition::Undefined, SPPosition::Undefined } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                                                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                        },
         { // 8 SP
                                                                { {  0, 2 }, {  0, 3 } },
                                                                { {  1, 2 }, {  1, 3 } },
                { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
                                                                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                 },
         { // 9 SP
                { {  0, 3 }, {  0, 4 } },
                                                             { {  1, 2 }, {  1, 3 } },
                { {  0, 1 }, {  0, 2 } },
                { {  0, 0 }, {  1, 1 } },
                { {  1, 0 }, {  SPPosition::Undefined, SPPosition::Undefined} },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
         },
                        { // 10 SP
                { {  0, 3 }, {  0, 4 } },
                                                                { {  1, 3 }, {  1, 4 } },
                { {  0, 1 }, {  0, 2 } },
                                                                                                { {  1, 1 }, {  1, 2 } },
                { {  0, 0 }, {  1, 0 } },
                { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
         },
                                 { // 11 SP
                { {  0, 4 }, {  0, 5 } },
                                         { {  1, 3 }, {  1, 4 } },
                                                                { {  0, 2 }, {  0, 3 } },
                                                                { {  1, 1 }, {  1, 2 } },
                { {  0, 1 }, {  1, 0 } },
                { {  0, 0 }, {  SPPosition::Undefined, SPPosition::Undefined} },
                                 },
         { // 12 SP
                                                                { {  0, 4 }, {  0, 5 } },
                { {  1, 4 }, {  1, 5 } },
                { {  0, 2 }, {  0, 3 } },
                { {  1, 2 }, {  1, 3 } },
                                         { {  0, 0 }, {  0, 1 } },
                { {  1, 0 }, {  1, 1 } },
         },
  };

  return (whichWay == CPD_DeployLeft) ? s_left[spCount - (c_cutOff + 1)][oldR][oldC] :
                                                                (whichWay == CPD_DeployRight) ? s_right[spCount - (c_cutOff + 1)][oldR][oldC] :
                                                                                                s_center[spCount - (c_cutOff + 1)][oldR][oldC];
}

DeployDeployed::SPPosition DeployDeployed::findRedeployPosition(int oldC, int oldR,
                CPDeployHow whichWay, int spCount)
{
        const int c_cutOff = 1;
   ASSERT(oldC < MaxSP - c_cutOff);
  ASSERT(oldR == 0);
        ASSERT(spCount - (c_cutOff + 1) >= 0);
        ASSERT(spCount - (c_cutOff + 1) < MaxSP - c_cutOff);

  static const SPPosition s_right[MaxSP - c_cutOff][MaxSP] = {
                                 // 2 SP
         {
                                                                { 0, 0 }, { 1, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 3 SP
         {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                        },
         // 4 SP
         {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 5 SP
                        {
                { 0, 0 }, { 0, 1 }, { 0, 2 }, { 1, 0 }, { 1, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                                                 },
         // 6 SP
         {
                { 0, 0 }, { 0, 1 }, { 0, 2 }, { 1, 0 }, { 1, 1 }, { 1, 2 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
         // 7 SP
         {
                                                                { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 1, 0 }, { 1, 1 }, { 1, 2 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                        },
         // 8 SP
             {
                                                                { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                                 // 9 SP
         {
                { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 10 SP
         {
                                         { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { NOPOS }, { NOPOS }
         },
         // 11 SP
         {
                { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 0, 5 }, { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { NOPOS }
                                 },
                        // 11 SP
                                 {
                { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 0, 5 }, { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { 1, 5 }
         }
  };

   static const SPPosition s_center[MaxSP - c_cutOff][MaxSP] = {
         // 2 SP
                                 {
                                                                { 0, 0 }, { 1, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                        },
         // 3 SP
                                 {
                { 0, 0 }, { 0, 1 }, { 1, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
         // 4 SP
             {
                { 1, 0 }, { 0, 0 }, { 0, 1 }, { 1, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 5 SP
         {
                                         { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 1, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 6 SP
         {
                                                                { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 1, 1 }, { 1, 2 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                        // 7 SP
         {
                { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 1, 1 }, { 1, 2 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                                 // 8 SP
         {
                { 1, 0 }, { 1, 1 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 1, 2 }, { 1, 3 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
                                 // 9 SP
         {
                                                             { 1, 0 }, { 1, 1 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 1, 2 }, { 1, 3 }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
         // 10 SP
                                 {
                { 1, 0 }, { 1, 1 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { NOPOS }, { NOPOS }
         },
         // 11 SP
         {
                { 1, 0 }, { 1, 1 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 0, 5 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { NOPOS }
         },
             // 12 SP
                        {
                { 1, 0 }, { 1, 1 }, { 1, 2 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 0, 5 }, { 1, 3 }, { 1, 4 }, { 1, 5 }
         }
        };

        static const SPPosition s_left[MaxSP - c_cutOff][MaxSP] = {
                        // 2 SP
         {
                { 1, 0 }, { 0, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
         // 3 SP
         {
                                                                { 1, 0 }, { 0, 0 }, { 0, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
         // 4 SP
         {
                                         { 1, 0 }, { 1, 1 }, { 0, 0 }, { 0, 1 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                                 // 5 SP
         {
                        { 1, 0 }, { 1, 1 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 6 SP
         {
                { 1, 0 }, { 1, 1 }, { 1, 2 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
         },
         // 7 SP
                        {
                { 1, 0 }, { 1, 1 }, { 1, 2 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                                 },
             // 8 SP
                                 {
                { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }
                        },
         // 9 SP
                                 {
                { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { NOPOS }, { NOPOS }, { NOPOS }
         },
                                 // 10 SP
                                 {
                { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { NOPOS }, { NOPOS }
         },
                                 // 11 SP
                        {
                                                                { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 0, 5 }, { NOPOS }
         },
         // 11 SP
         {
                { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { 1, 5 }, { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 0, 5 }
         }
  };

  return (whichWay == CPD_DeployLeft) ? s_left[spCount - (c_cutOff + 1)][oldC] :
                        (whichWay == CPD_DeployRight) ? s_right[spCount - (c_cutOff + 1)][oldC] :
                                                                s_center[spCount - (c_cutOff + 1)][oldC];
}

//-------------------------------------------------------------------------
// 'Extended'
class DeployExtended : public DeployBase
{
   public:
   DeployExtended(RPBattleData bd) : DeployBase(bd)
   {
   }
   ~DeployExtended()
   {
   }

   SPPosition findDeployPosition(int oldC, int oldR,
      CPDeployHow whichWay, int spCount);
   SPPosition findRedeployPosition(int oldC, int oldR,
      CPDeployHow whichWay, int spCount)
   {
      FORCEASSERT("findRedeployPosition() should not be called in DeployExtended");
      // SPPosition spPos;
      // return spPos;
      return SPPosition();
   }
};

DeployExtended::SPPosition DeployExtended::findDeployPosition(int oldC, int oldR,
                CPDeployHow whichWay, int spCount)
{
  const int c_cutOff = 2;
  const int c_maxRows = 2;
  ASSERT(oldC < MaxSP - c_cutOff);
        ASSERT(oldR < c_maxRows);
   ASSERT(spCount - (c_cutOff + 1) >= 0);
        ASSERT(spCount - (c_cutOff + 1) < MaxSP - c_cutOff);

  static const SPPosition s_right[MaxSP - c_cutOff][c_maxRows][MaxSP / 2] = {
         // 3 SP
                        {
                { { 0, 0 }, { 0, 1 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } },
                { { 0, 2 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
                                 },
                                 // 4 SP
             {
                { { 0, 0 }, { 0, 1 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } },
                                                                { { 0, 2 }, { 0, 3 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
         },
                                 // 5 SP
                        {
                { { 0, 0 }, { 0, 1 },  { 0, 2 },  { NOPOS }, { NOPOS }, { NOPOS } },
                { { 0, 3 }, { 0, 4 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
         },
         // 6 SP
         {
                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { NOPOS }, { NOPOS }, { NOPOS } },
                { { 0, 3 }, { 0, 4 },  { 0, 5 }, { NOPOS }, { NOPOS }, { NOPOS } }
         },
         // 7 SP
                                 {
                                         { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { NOPOS }, { NOPOS } },
                                                                { { 0, 4 }, { 0, 5 },  { 0, 6 }, { NOPOS }, { NOPOS }, { NOPOS } }
         },
         // 8 SP
         {
                                                                                                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { NOPOS }, { NOPOS } },
                                         { { 0, 4 }, { 0, 5 },  { 0, 6 }, { 0, 7 },  { NOPOS }, { NOPOS } }
         },
                                 // 9 SP
                                 {
                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { 0, 4 },  { NOPOS } },
                { { 0, 5 }, { 0, 6 },  { 0, 7 }, { 0, 8 },  { NOPOS }, { NOPOS } }
                                 },
         // 10 SP
                                 {
                        { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { 0, 4 },  { NOPOS } },
                                         { { 0, 5 }, { 0, 6 },  { 0, 7 }, { 0, 8 },  { 0, 9 },  { NOPOS } }
         },
         // 11 SP
         {
                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { 0, 4 },  { 0, 5 } },
                { { 0, 6 }, { 0, 7 },  { 0, 8 }, { 0, 9 },  { 0, 10 }, { NOPOS } }
         },
         // 12 SP
         {
                                                                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { 0, 4 },  { 0, 5 } },
                { { 0, 6 }, { 0, 7 },  { 0, 8 }, { 0, 9 },  { 0, 10 }, { 0, 11 } }
                        }
  };

  static const SPPosition s_center[MaxSP - c_cutOff][c_maxRows][MaxSP / 2] = {
                                 // 3 SP
         {
                                         { { 0, 0 }, { 0, 1 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } },
                                                                { { 0, 2 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
                                 },
             // 4 SP
         {
                                                                { { 0, 1 }, { 0, 2 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } },
                { { 0, 0 }, { 0, 3 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
                                 },
         // 5 SP
         {
                                         { { 0, 1 }, { 0, 2 },  { 0, 3 },  { NOPOS }, { NOPOS }, { NOPOS } },
                { { 0, 0 }, { 0, 4 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
         },
             // 6 SP
         {
                { { 0, 1 }, { 0, 2 },  { 0, 3 }, { NOPOS }, { NOPOS }, { NOPOS } },
                { { 0, 0 }, { 0, 4 },  { 0, 5 }, { NOPOS }, { NOPOS }, { NOPOS } }
         },
                                 // 7 SP
         {
                                                                { { 0, 1 }, { 0, 2 },  { 0, 3 }, { 0, 4 },  { NOPOS }, { NOPOS } },
                                         { { 0, 0 }, { 0, 5 },  { 0, 6 }, { NOPOS }, { NOPOS }, { NOPOS } }
         },
         // 8 SP
                                 {
                { { 0, 2 }, { 0, 3 },  { 0, 4 }, { 0, 5 },  { NOPOS }, { NOPOS } },
                { { 0, 0 }, { 0, 1 },  { 0, 6 }, { 0, 7 },  { NOPOS }, { NOPOS } }
                        },
                                 // 9 SP
         {
                { { 0, 2 }, { 0, 3 },  { 0, 4 }, { 0, 5 },  { 0, 6 },  { NOPOS } },
                                                                { { 0, 0 }, { 0, 1 },  { 0, 7 }, { 0, 8 },  { NOPOS }, { NOPOS } }
         },
                                 // 10 SP
             {
                { { 0, 2 }, { 0, 3 },  { 0, 4 }, { 0, 5 },  { 0, 6 },  { NOPOS } },
                { { 0, 0 }, { 0, 1 },  { 0, 7 }, { 0, 8 },  { 0, 9 }, { NOPOS } }
                        },
         // 11 SP
         {
                { { 0, 2 }, { 0, 3 },  { 0, 4 }, { 0, 5 },  { 0, 6 },  { 0, 7 } },
                { { 0, 0 }, { 0, 1 },  { 0, 8 }, { 0, 9 },  { 0, 10 }, { NOPOS } }
         },
         // 12 SP
                                                 {
                { { 0, 3 }, { 0, 4 },  { 0, 5 }, { 0, 6 },  { 0, 7 },  { 0, 8 } },
                                                                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 9 },  { 0, 10 }, { 0, 11 } }
         }
  };

        static const SPPosition s_left[MaxSP - c_cutOff][c_maxRows][MaxSP / 2] = {
         // 3 SP
         {
                                                                { { 0, 1 }, { 0, 2 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } },
                                         { { 0, 0 }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
         },
         // 4 SP
                                 {
                { { 0, 2 }, { 0, 3 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } },
                                                                { { 0, 0 }, { 0, 1 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
         },
         // 5 SP
         {
                { { 0, 2 }, { 0, 3 },  { 0, 4 },  { NOPOS }, { NOPOS }, { NOPOS } },
                                         { { 0, 0 }, { 0, 1 },  { NOPOS }, { NOPOS }, { NOPOS }, { NOPOS } }
             },
                        // 6 SP
         {
                { { 0, 3 }, { 0, 4 },  { 0, 5 }, { NOPOS }, { NOPOS }, { NOPOS } },
                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { NOPOS }, { NOPOS }, { NOPOS } }
                                 },
         // 7 SP
                                 {
                { { 0, 3 }, { 0, 4 },  { 0, 5 }, { 0, 6 },  { NOPOS }, { NOPOS } },
                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { NOPOS }, { NOPOS }, { NOPOS } }
                                    },
                                 // 8 SP
         {
                { { 0, 4 }, { 0, 5 },  { 0, 6 }, { 0, 7 },  { NOPOS }, { NOPOS } },
                                                                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { NOPOS }, { NOPOS } }
                                 },
                        // 9 SP
         {
                                                                { { 0, 4 }, { 0, 5 },  { 0, 6 }, { 0, 7 },  { 0, 8 },  { NOPOS } },
                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { NOPOS }, { NOPOS } }
                                 },
         // 10 SP
         {
                { { 0, 5 }, { 0, 6 },  { 0, 7 }, { 0, 8 },  { 0, 9 },  { NOPOS } },
                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { 0, 4 },  { NOPOS } }
         },
                        // 11 SP
         {
                                         { { 0, 5 }, { 0, 6 },  { 0, 7 }, { 0, 8 },  { 0, 9 },  { 0, 10 } },
                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { 0, 4 },  { NOPOS } }
         },
                                                 // 12 SP
         {
                                                                { { 0, 6 }, { 0, 7 },  { 0, 8 }, { 0, 9 },  { 0, 10 }, { 0, 11 } },
                { { 0, 0 }, { 0, 1 },  { 0, 2 }, { 0, 3 },  { 0, 4 },  { 0, 5 } }
         }
  };

        return (whichWay == CPD_DeployLeft) ? s_left[spCount - (c_cutOff + 1)][oldR][oldC] :
                                                                                                (whichWay == CPD_DeployRight) ? s_right[spCount - (c_cutOff + 1)][oldR][oldC] :
                                                                s_center[spCount - (c_cutOff + 1)][oldR][oldC];
}

void DeployBase::clearMove(const RefBattleCP& cp)
{
    ASSERT(cp->getRank().sameRank(Rank_Division));

    for(std::vector<DeployItem>::iterator di = cp->mapBegin();
        di != cp->mapEnd();
        di++)
    {
        if(di->active())
        {
            di->d_sp->routeList().reset();
            di->d_sp->setMoving(False);
        }
    }

    cp->getCurrentOrder().divFormation(cp->formation());
    cp->wantRows(cp->rows());
    cp->wantColumns(cp->columns());
    cp->syncMapToSP();
}

//------------------------------------------------------------------
bool DeployBase::deploy(const RefBattleCP& cp)
{
   ASSERT(cp->formation() != cp->nextFormation());

   HexArea area;

   // plot destination hexes
   if(cp->getRank().sameRank(Rank_Division) && cp->sp() != NoBattleSP)
   {
      // useful constants
      const HexPosition::Facing facing = cp->facing();
      const int spCount = cp->spCount();
      ASSERT(spCount > 0);

      const int c_maxSP = 12;
      ASSERT(spCount  - 1 <  c_maxSP);

      static bool s_needsDeploy[CPF_Last][c_maxSP] = {
         { False, False, False, False, False, False, False, False, False, False, False, False }, // March
         { False, True,  True,  True,  True,  True,  True,  True,  True,  True,  True,  True  }, // Massed
         { False, False, False, False, True,  True,  True,  True,  True,  True,  True,  True  }, // Deployed
         { False, False, True,  True,  True,  True,  True,  True,  True,  True,  True,  True  }  // Extended
      };

      static bool s_needsRedeploy[CPF_Last][c_maxSP] = {
         { False, True,  True,  True,  True,  True,  True,  True,  True,  True,  True,  True  }, // March
         { False, False, False, False, True,  True,  True,  True,  True,  True,  True,  True  }, // Massed
         { False, False, True,  True,  True,  True,  True,  True,  True,  True,  True,  True  }, // Deployed
         { False, False, False, False, False, False, False, False, False, False, False, False }  // Extended
      };

      bool shouldDeploy = (cp->formation() < cp->nextFormation()) ?
         s_needsDeploy[cp->nextFormation()][spCount - 1] : s_needsRedeploy[cp->nextFormation()][spCount - 1];

      if(shouldDeploy)
      {
         // get new deploy map
         if(!findDeployHexes(cp, cp->nextFormation(), area))
         {
            clearMove(cp);
            return False;
         }
         //vector<DeployItem>& map = cp->deployMap();

         /*
          * go through old map and reassign sp's to new map
          */

         for(int r = 0; r < cp->rows(); r++)
         {
            for(int c = 0; c < cp->columns(); c++)
            {
               //DeployItem& cdi = map[(cp->columns() * r) + c];
               DeployItem* cdi = cp->currentDeployItem(c, r);

               if(!cdi || !cdi->active())
                  continue;


               SPPosition spPos = (cp->formation() < cp->nextFormation()) ?
                  findDeployPosition(c, r, cp->deployWhichWay(), spCount) :
                  findRedeployPosition(c, r, cp->deployWhichWay(), spCount);

               ASSERT(spPos.d_r != SPPosition::Undefined);
               ASSERT(spPos.d_c != SPPosition::Undefined);
               //DeployItem& di = cp->deployItem(spPos.d_c, spPos.d_r);
               DeployItem* di = cp->wantDeployItem(spPos.d_c, spPos.d_r);
               ASSERT(di);
               if(!di)
               {
                  clearMove(cp);
                  return False;
               }

               setToNewMap(*di, cdi->d_sp);
            }
         }

         if(plotDeployRoute(cp))
         {
            cleanUpRoute(batData(), cp);
#if 0
            // plot route for attached leader (if any)
            RefBattleCP attachedTo = BobUtility::getAttachedLeader(batData(), cp);
            if(attachedTo != NoBattleCP)
            {
               plotHQRoute(batData(), attachedTo);
               attachedTo->setMoving(True);
            }
#endif
         }
         // route not found
         else
         {
            clearMove(cp);

            return False;
         }
      }
      else
      {
         cp->formation(cp->nextFormation());
      }
   }

   return True;
}

bool DeployBase::reorganize(const RefBattleCP& cp)
{
#ifdef DEBUG
   reorgLog.printf("\n-----------------------------------");
   reorgLog.printf("%s is reorganizing, rallying = %d", cp->getName(), static_cast<int>(cp->rallying()));
#endif
   if(!cp->rallying())
   {
      // Note: at this point, no SP have actually been removed
        // move up SP to replacing any destroyed (strength = 0) in front row
      bool movingUp = False;
      for(int c = 0; c < cp->columns(); c++)
      {
         //DeployItem& di = cp->deployItem(c, 0);
         DeployItem* di = cp->currentDeployItem(c, 0);
         ASSERT(di);
         // see if sp has strength of 0
         if(di && di->active() && di->d_sp->strength() == 0)
         {
            DeployItem* diP = di;
            // if so, bring one up from the rear on the same row
            for(int r = 1; r < cp->rows(); r++)
            {
               DeployItem* di2 = cp->currentDeployItem(c, r);
               if(di2 && di2->active() && di2->d_sp->strength() > 0)
               {
                  {
                     // plot a route to the front
                     B_Route br(batData(), cp, di2->d_sp);
                     if(br.plotRoute(di2->d_sp->hex(), diP->d_hex, 0, True))
                     {
#ifdef DEBUG
                        reorgLog.printf("SP %ld, c = %d, r = %d is moving up",
                           reinterpret_cast<long>(di2->d_sp), c, r);
#endif
                        cp->addToMap(di2->d_sp, &diP->d_hex, c, r - 1);
                        di2->d_sp->setMoving(True);
                        movingUp = True;
                     }
                     else
                     {
#ifdef DEBUG
                        reorgLog.printf("SP %ld, c = %d, r = %d unable to find route",
                           reinterpret_cast<long>(di2->d_sp), c, r);
#endif
#if 0
                        cp->wantRows(cp->rows());
                        cp->wantColumns(cp->columns());
                        cp->syncMapToSP();
#endif
                        clearMove(cp);
                     }
                  }

                  {
                     // plot a ghost route to the rear
                     // Note: this is needed to keep everything in sync
                     //       until we completely reorganize during rest\rally
                     B_Route br(batData(), cp, diP->d_sp);
                     if(br.plotRoute(diP->d_sp->hex(), di2->d_hex, 0, True))
                     {
#ifdef DEBUG
                        reorgLog.printf("SP %ld, c = %d, r = %d is dead",
                           reinterpret_cast<long>(di->d_sp), c, r - 1);
#endif
                        cp->addToMap(diP->d_sp, &di2->d_hex, c, r);
                        diP->d_sp->setMoving(True);
                     }
                     else
                     {
#ifdef DEBUG
                        reorgLog.printf("SP %ld, c = %d, r = %d is unable to find ghost route",
                           reinterpret_cast<long>(diP->d_sp), c, r - 1);
#endif
                        FORCEASSERT("Unable to find route Deploy::reorganize()");
#if 0
                        cp->wantRows(cp->rows());
                        cp->wantColumns(cp->columns());
                        cp->syncMapToSP();
#endif
                        clearMove(cp);
                        movingUp = False;
                     }
                  }

//                        diP = &di2;
                  break;
               }

//                    else if(!di2.active())
//                      break;
            }
         }
      }

#ifdef DEBUG
      DeployItem* di = cp->wantDeployItem(0, 0);
      ASSERT(di && di->active());
      if(di && di->active())
         reorgLog.printf("%s has succesfully completed moving up rear SP", cp->getName());
#endif
       return movingUp;
   }

   else // rallying
   {
      // If here we need to completely reorganize if we have any gaps, etc.
      // find new top-left hex for unit
      HexCord leftHex;
      getNewLeft(cp, leftHex);
      int oldCols = cp->columns();
      int oldRows = cp->rows();
      if(PlayerDeploy::playerDeploy(d_batData, cp, leftHex, &cp->getCurrentOrder().order(), 0, PlayerDeploy::REORGANIZE | PlayerDeploy::NEVERFAIL))
      {
         // find a sp for each hex in deploymap
         for(int r = 0; r < cp->rows(); r++)
         {
            for(int c = 0; c < cp->columns(); c++)
            {
               DeployItem* di = cp->currentDeployItem(c, r);
               ASSERT(di);
               if(!di)
                  continue;

               RefBattleSP bestSP = NoBattleSP;
               int bestDist = -1;

               for(BattleSPIter iter(d_batData->ob(), cp, BattleSPIter::OnDisplay);
                  !iter.isFinished();
                  iter.next())
               {
                  if(Local::shouldBeVisible(d_batData, cp, iter.sp()) && !spInWantMap(cp, iter.sp()))
                  {
                     if(iter.sp()->hex() == di->d_wantHex)
                     {
                        bestSP = iter.sp();
                        break;
                     }

                     int dist = Local::getDistI(iter.sp()->hex(), di->d_wantHex);
                     if( (bestDist == -1 || dist < bestDist) )
                     {
                        bestSP = iter.sp();
                        bestDist = dist;
                     }

                     else if(dist == bestDist && adjacentHex(di->d_wantHex, iter.sp()->hex()))
                     {
                        HexCord hex;
                        if(d_batData->moveHex(di->d_wantHex, rightFlank(cp->facing()), hex) && hex == iter.sp()->hex())
                        {
                           bestSP = iter.sp();
                        }
                     }
                  }
               }

               if(bestSP != NoBattleSP)
               {
#ifdef DEBUG
                  reorgLog.printf("SP %ld is assigned to pos c = %d, r = %d",
                     reinterpret_cast<long>(bestSP), c, r);
#endif
                  di->d_wantSP = bestSP;
                  di->d_sp = bestSP;
                  di->d_hex = di->d_wantHex;
               }
            }
         }

//       HexList excludeList;
         for(r = 0; r < cp->rows(); r++)
         {
            for(int c = 0; c < cp->columns(); c++)
            {
               DeployItem* di = cp->currentDeployItem(c, r);
               ASSERT(di);
               if(di && di->active())
               {
                  if(di->d_sp->hex() != di->d_hex)
                  {
                     B_Route br(batData(), cp, di->d_sp);
                     if(br.plotRoute(di->d_sp->hex(), di->d_hex, 0, True))
                        di->d_sp->setMoving(True);
                     else
                     {
                        FORCEASSERT("Route not found in DeployBase::reorganize()");
                        cp->needsReadjustment(True);
//                      return False;
                     }
                  }

//                excludeList.newItem(di.d_hex);
               }
            }
         }

         BobUtility::countArtillery(batData()->ob(), cp);
#ifdef DEBUG
         DeployItem* di = cp->wantDeployItem(0, 0);
         ASSERT(di);
         ASSERT(di->active());
         if(di && di->active())
            reorgLog.printf("%s has succesfully completed reorganization", cp->getName());
#endif
         cp->shouldReorganize(False);
         return !cp->needsReadjustment();//True;
      }
      else
      {
#ifdef DEBUG
         reorgLog.printf("Unable to reorganize");
         FORCEASSERT("Unable to reorganize!");
#endif
         return False;
      }
   }
}

/*-----------------------------------------------------------
 * Allocator for derived classes
 */

DeployBase* DeployBase::allocate(RPBattleData bd, CPFormation f)
{
            if(f == CPF_March)
                                    return new DeployMarch(bd);

        else if(f == CPF_Massed)
                        return new DeployMassed(bd);

        else if(f == CPF_Deployed)
                        return new DeployDeployed(bd);

        else if(f == CPF_Extended)
                        return new DeployExtended(bd);

        else
                        return 0;
}


/*----------------------------------------------------------------
 * Client access
 */

Deploy_Int::Deploy_Int(RPBattleData batData) :
        d_formations(new DeployBase*[CPF_Last])
{
  ASSERT(d_formations);
  for(CPFormation cf = CPF_First; cf < CPF_Last; INCREMENT(cf))
  {
                        d_formations[cf] = DeployBase::allocate(batData, cf);
                        ASSERT(d_formations[cf]);
  }
}

Deploy_Int::~Deploy_Int()
{
            for(CPFormation cf = CPF_First; cf < CPF_Last; INCREMENT(cf))
        {
                        delete d_formations[cf];
        }

        delete[] d_formations;
}

bool Deploy_Int::deploy(const RefBattleCP& cp)
{
        ASSERT(cp->nextFormation() < CPF_Last);
            return d_formations[cp->nextFormation()]->deploy(cp);
}

bool Deploy_Int::adjustDeployment(const RefBattleCP& cp)
{
        ASSERT(cp->nextFormation() < CPF_Last);
        return d_formations[cp->nextFormation()]->adjustDeployment(cp);
}

bool Deploy_Int::reorganize(const RefBattleCP& cp)
{
        ASSERT(cp->nextFormation() < CPF_Last);
//        ASSERT(cp->formation() == cp->nextFormation());
        return d_formations[cp->nextFormation()]->reorganize(cp);
}

//---------------- Player deployment routines
void placeUnit(RPBattleData bd, const RefBattleUnit& unit, const HexCord& hex)
{
            PlayerDeploy::placeUnit(bd, unit, hex);
}



// Deploy cp on map, using cp->getCurrentOrder() for facing, and deployment values
bool playerDeploy(RPBattleData batData, const RefBattleCP& cp,
                        const HexCord& startHex, HexList* hl, bool initializing)
{
  return PlayerDeploy::playerDeploy(batData, cp, startHex, hl, (initializing) ? PlayerDeploy::INITIALIZE : 0);
//          return PlayerDeploy::playerDeploy(batData, cp, startHex, hl, (initializing) ? PlayerDeploy::Initializing : PlayerDeploy::None);
}

#if 0
//---------------------------------------------------------------
/*-----------------------------------------------------------
 *  Initialize Deployment
 */

class Instant_DeployUtility
{
                  public:
                                         Instant_DeployUtility(RPBattleData batData);
                                         ~Instant_DeployUtility() { }
                                         void deployAll();
                                         void deployAll(Side side);

                                         typedef MinMaxRect<HexCord::Cord> HexArea;

                        private:
                                         enum OrgDeployType {
                                                                FrontLine,
                                                                ArtyLine,
                                                                ReserveLine,
                                                                HQ,

                                                                OrgDeployType_HowMany
                                         };
#if 0
                                         enum ShiftHow {
                                                                ShiftRight,
                                                                ShiftUpRight,
                                                                ShiftUp,
                                                                ShiftUpLeft,
                                                                ShiftLeft,
                                                                ShiftDownLeft,
                                                                ShiftDown,
                                                                ShiftDownRight,

                                                                Shift_HowMany,
                                                                NoShift
                                         };

                                         enum LUnitType {
                                                Inf,
                                                LCav,
                                                HCav,
                                                FootArt,
                                                HvyArt,
                                                HorseArt,

                                                End,
                                                Undefined = End
                                         };
#endif
                                         void deployOrgAndSisters(const RefBattleCP& cp, HexPosition::Facing facing, HexArea& area);
                                                                // Deploy a unit and its sisters around the hex
                                         void deployOrganization(const RefBattleCP& cp, HexPosition::Facing facing,
                                                                 HexArea& area, OrgDeployType& dt);
                                                                                                // Deploy a unit and all below centred roughly on hex
                        private:

                                         // Handy variables

                                         PBattleData d_batData;
                                         BattleOB* d_ob;
                                         const TerrainTable& d_terrainTable;
                                         const BattleBuildings::BuildingTable* d_buildings;
                                         const BattleBuildings::BuildingList* d_buildingList;
                                         const BattleMap* d_map;
                                         BattleHexMap*   d_hexMap;

};

Instant_DeployUtility::Instant_DeployUtility(RPBattleData batData) :
                        d_batData(batData),
                  d_terrainTable(batData->terrainTable())
{
                  // Get information from batdata

                  d_ob = batData->ob();
                  // d_terrainTable = batData->terrainTable();
                  d_buildings = batData->buildingTable();
                        d_buildingList = batData->buildingList();
        d_map = batData->map();
                        d_hexMap = batData->hexMap();

                  ASSERT(d_ob != 0);
                  ASSERT(d_buildings != 0);
                  ASSERT(d_buildingList != 0);
                           ASSERT(d_map != 0);
                  ASSERT(d_hexMap != 0);
}

/*
 * Process top level units on each side
 *
 */

void Instant_DeployUtility::deployAll()
{
#ifdef DEBUG
                                                             reorgLog.printf("------------------------");
#endif

                           for(Side side = 0; side < d_ob->sideCount(); ++side)
                           {
#ifdef DEBUG
                                                             reorgLog.printf("Deploying Side %d", static_cast<int>(side));
#endif

                                         const HexCord& mapSize = d_map->getSize();
                                         HexPosition::Facing facing;

                                         HexCord lHex;
                                         HexCord rHex;

                                         lHex.x(0);
                                         rHex.x(mapSize.x() - 1);

                                         int sideCY = mapSize.y() / 3;

                                         ASSERT((side == 0) || (side == 1));
                                         if(side == 0)
                                         {
                                                                facing = HexPosition::North;
                                                                lHex.y(0);
                                                                rHex.y(sideCY);
                                         }
                                         else if(side == 1)
                                         {
                                                                facing = HexPosition::South;
                                                                lHex.y((mapSize.y() - 1) - sideCY);
                                                                rHex.y(mapSize.y() - 1);
                                         }

                                         HexArea area;
                                         area.update(lHex);
                                         area.update(rHex);

                                         RefBattleCP cp = d_ob->getTop(side);
                                         if(cp != NoBattleCP)
                                                                deployOrgAndSisters(cp, facing, area);

#ifdef DEBUG
                                         reorgLog.printf("------------------------");
#endif

                  }

}

/*******************************************
Start of James's unchecked change
*******************************************/

void Instant_DeployUtility::deployAll(Side side)
{
#ifdef DEBUG
                                         reorgLog.printf("------------------------");
#endif


#ifdef DEBUG
                                         reorgLog.printf("Deploying Side %d", static_cast<int>(side));
#endif

                                         const HexCord& mapSize = d_map->getSize();
                                         HexPosition::Facing facing;

                                         HexCord lHex;
                                         HexCord rHex;

                                         lHex.x(0);
                                         rHex.x(mapSize.x() - 1);

                                         int sideCY = mapSize.y() / 3;

                                         ASSERT((side == 0) || (side == 1));
                                                                                                                                                                 if(side == 0)
                                         {
                                                                facing = HexPosition::North;
                                                                lHex.y(0);
                                                                                                rHex.y(sideCY);
                                         }
                                         else if(side == 1)
                                         {
                                                                                                                                                                                                                                                                facing = HexPosition::South;
                                                                lHex.y((mapSize.y() - 1) - sideCY);
                                                                rHex.y(mapSize.y() - 1);
                                         }

                                         HexArea area;
                                                                                                                                                                 area.update(lHex);
                                         area.update(rHex);

                                         RefBattleCP cp = d_ob->getTop(side);
                                         if(cp != NoBattleCP)
                                                                                                                                                                                                                                                                deployOrgAndSisters(cp, facing, area);

#ifdef DEBUG
                                         reorgLog.printf("------------------------");
#endif


}
/*******************************************
End of James's unchecked change
*******************************************/

/*
 * Recursive function to deploy an organization
 * At the moment all units are put in top of each other!
 *
 * These 2 functions are mutually exclusive
 */

void Instant_DeployUtility::deployOrgAndSisters(const RefBattleCP& cp, HexPosition::Facing facing, HexArea& area)
{
                        ASSERT(cp != NoBattleCP);

#ifdef DEBUG
                                    reorgLog.printf("Deploying %s and children at %d,%d -- %d,%d  facing=%d",
                                         cp->getName(),
                                         static_cast<int>(area.minX()),
                                         static_cast<int>(area.minY()),
                                         static_cast<int>(area.maxX()),
                                         static_cast<int>(area.maxY()),
                                         static_cast<int>(facing));
#endif

                        // Count sisters
                        int sisterCount = 0;
                        int nInfCav = 0;
                        int nArt = 0;
                        {       // braces needed until compiler complies with new standard
                                         for(RefBattleCP unit = cp; unit != NoBattleCP; unit = unit->sister())
                                         {
                                                                // Do not count  XX's that are attached to XXXX's or XXXXX's
                                                                if(cp->getRank().isHigher(unit->getRank().getRankEnum()))
                                                                        ;
                                                                else
                                                                        ++sisterCount;

                                                                if(unit->getRank().sameRank(Rank_Division))
                                                                {
                                                                        if(unit->generic()->isInfantry() || unit->generic()->isCavalry())
                                                                                        nInfCav++;
                                                                        else if(unit->generic()->isArtillery())
                                                                                        nArt++;
                                                                }
                                         }
                        }

                        ASSERT(sisterCount >= 1);

                        // if deploying XX's, deploy some in front, some in reserve
                        //    i.e.
                        //
                        //    Inf   Inf
                        //       Art
                        //       Inf(Res)

                        int sectorCX = 0;
                        int sectorCY = 0;
                        int reserveSectorCX = 0;
                        int reserveSectorCY = 0;
                        int artSectorCX = 0;
                        int artSectorCY = 0;
                        int nFront = 0;
                        if(cp->getRank().sameRank(Rank_Division))
                        {
                                nFront = (nInfCav > 2) ? ((4 * nInfCav) + 3) / 6 : nInfCav;
//                              if(nFront == 1);
                                if(nFront == 0)
                                        nFront = (nArt > 2) ? ((4 * nArt) + 3) / 6 : nArt;

                                ASSERT(nFront != 0);

                                int nFrontType = (nInfCav > 0) ? nInfCav : nArt;

                                if(nFrontType - nFront > 0)
                                {
                                                reserveSectorCX = (area.maxX() - area.minX()) / (nFrontType - nFront);
                                                reserveSectorCY = (area.maxY() - area.minY()) * .66;
                                }

                                if(nInfCav > 0 && nArt > 0)
                                {
                                                artSectorCX = (area.maxX() - area.minX()) / nArt;
                                                artSectorCY = (area.maxY() - area.minY()) * .75;
                                }

                                sectorCX = (area.maxX() - area.minX()) / nFront;
                                sectorCY = (area.maxY() - area.minY()) - reserveSectorCY;
                        }
                        else
                        {
                                sectorCX = (area.maxX() - area.minX()) / sisterCount;
                                sectorCY = (area.maxY() - area.minY());
                        }

                        int count = 0;
                        int nFrontCount = 0;
                        int nResCount = 0;
                        int nArtCount = 0;
                        HexCord lHex;
                        HexCord rHex;
                        OrgDeployType dt;
                        for(RefBattleCP unit = cp; unit != NoBattleCP; unit = unit->sister(), count++)
                        {
                                if(cp->getRank().isHigher(unit->getRank().getRankEnum()))
                                {
                                         lHex.x(((area.minX() + area.maxX()) / 2) - 5);
                                         lHex.y(((area.minY() + area.maxY()) / 2) - 5);
                                         rHex.x(((area.minX() + area.maxX()) / 2) + 5);
                                         rHex.y(((area.minY() + area.maxY()) / 2) + 5);

                                         HexArea sArea;
                                         sArea.update(lHex);
                                         sArea.update(rHex);
                                         dt = FrontLine;
                                         deployOrganization(unit, facing, sArea, dt);
                                }
                                else
                                {
                                         HexArea sArea;

                                         // if we are deploying XXX's or higher, give each equal sectors
                                         if(unit->getRank().isHigher(Rank_Division))
                                         {
                                                 lHex.x(area.minX() + (count * sectorCX));
                                                 lHex.y(area.minY());
                                                 rHex.x(lHex.x() + sectorCX);
                                                 rHex.y(area.maxY());

                                                 dt = HQ;
                                         }

                                         // deploy XX's
                                         else
                                         {
                                                 if( (unit->generic()->isInfantry() || unit->generic()->isCavalry() || nInfCav == 0) )
                                                 {
#ifdef DEBUG
                                                                 if(nInfCav == 0)
                                                                         ASSERT(cp->generic()->isArtillery());
#endif
                                                                 // front-line unit
                                                                 if(nFrontCount < nFront)
                                                                 {
                                                                                        lHex.x(area.minX() + (nFrontCount * sectorCX));
                                                                                        lHex.y((facing == HexPosition::North) ? area.minY() + reserveSectorCY : area.minY());
                                                                                        rHex.x(lHex.x() + sectorCX);
                                                                                        rHex.y(lHex.y() + sectorCY);

                                                                                        dt = FrontLine;
                                                                                        nFrontCount++;
                                                                 }

                                                                 // reserve unit
                                                                 else
                                                                 {
                                                                                        lHex.x(area.minX() + (nResCount * reserveSectorCX));
                                                                                        lHex.y((facing == HexPosition::North) ? area.minY() : area.maxY() - reserveSectorCY);
                                                                                        rHex.x(lHex.x() + reserveSectorCX);
                                                                                        rHex.y(lHex.y() + reserveSectorCY);

                                                                                        dt = ReserveLine;
                                                                                        nResCount++;
                                                                 }

                                                 }
                                                 else
                                                 {
                                                                 ASSERT(unit->generic()->isArtillery());
                                                                 lHex.x(area.minX() + (nArtCount * artSectorCX));
                                                                 lHex.y((facing == HexPosition::North) ? area.minY() : area.maxY() - artSectorCY);
                                                                 rHex.x(area.minX() + artSectorCX);
                                                                 rHex.y(lHex.y() + artSectorCY);

                                                                 dt = ArtyLine;
                                                                 nArtCount++;
                                                 }
                                         }

                                         sArea.update(lHex);
                                         sArea.update(rHex);

                                         deployOrganization(unit, facing, sArea, dt);
                                }
                        }

}

//ORIGINAL FUNCTIONS WHICH HAS SOME SP-ITERATOR PROBLEMS
void Instant_DeployUtility::deployOrganization(const RefBattleCP& cp, HexPosition::Facing facing,
                        HexArea& area, OrgDeployType& dt)
{
        ASSERT(cp != NoBattleCP);

        // deploy children, if any (recursive!!)
        RefBattleCP child = cp->child();
        if(child != NoBattleCP)
        {
                deployOrgAndSisters(cp->child(), facing, area);
        }


        // set initial formations and facing
        static const CPFormation s_formation[OrgDeployType_HowMany] = {
         CPF_Massed,     // Front line
         CPF_Massed,     // Arty line
         CPF_March,      // Reseve line
         CPF_March       // HQ
        };

        const int areaCX = (area.maxX() - area.minX());

        // if a XX
        HexCord startHex;
        if(cp->getRank().sameRank(Rank_Division))
        {
                ASSERT(dt != HQ);
                CPFormation divFormation = s_formation[dt];
                cp->getCurrentOrder().divFormation(divFormation);
                cp->getCurrentOrder().facing(facing);
                startHex.x(area.minX() + (areaCX / 2));
                startHex.y((facing == HexPosition::North) ? area.maxY() - 1 : area.minY() + 1);
        }

        // else if a HQ
        else
        {
                startHex.x(((area.maxX() + area.minX()) / 2) + cp->getRank().getRankEnum());
                startHex.y(((area.maxY() + area.minY()) / 2) + cp->getRank().getRankEnum());
        }

//  const int xOffset = (facing == HexPosition::North) ? 1 : -1;
//  const int yOffset = (facing == HexPosition::North) ? -1 : 1;

        const int nDirections = 8;
        static const POINT p[nDirections] = {
                                 {  1,  0 },  // E
                                 {  1,  1 },  // NE
                                 {  0,  1 },  // N
                                 { -1,  1 },  // NW
                                 { -1,  0 },  // W
                                 { -1, -1 },  // SW
                                 {  0, -1 },  // S
                                 {  1, -1 },  // SE
        };

        bool found = False;
        for(int i = 0; i < nDirections; i++)
        {
                HexCord hex = startHex;

                int count = 0;
                found = playerDeploy(d_batData, cp, hex);

                if(!found)
                {
                        do
                        {
                                const HexCord& size = d_batData->map()->getSize();

                                int newX = hex.x() + p[i].x;
                                int newY = hex.y() + p[i].y;

                                if(newX < area.minX()  ||
                                         newX > area.maxX() ||
                                         newX < 0 ||
                                         newX >= size.x() ||
                                         newY < area.minY()  ||
                                         newY > area.maxY() ||
                                         newY < 0 ||
                                         newY >= size.y())
                                {
                                        break;
                                }

                                hex.x(static_cast<UBYTE>(newX));
                                hex.y(static_cast<UBYTE>(newY));

                                found = playerDeploy(d_batData, cp, hex);
                        } while(!found);
                }

                if(found)
                {
                        startHex = hex;
                        break;
                }
        }

        if(cp->sp())
                        cp->hex(startHex);

        ASSERT(found);
}


/*
 * Set an SP's position and update hexMap
 */
#if 0
void Instant_DeployUtility::placeSP(const RefBattleSP& sp, const HexCord& hex, bool move)
{
#ifdef DEBUG
                  d_batData->assertHexOnMap(hex);
#endif


                        HexPosition hp(moveRight(sp->facing()), static_cast<HexPosition::HexDistance>(sp->hexPosition().centerPos()));
                        sp->position(BattlePosition(hex, hp));

                        if(move)
                        {
                         d_hexMap->add(sp);
                        }
}

/*
 * Set an CP's position and update hexMap
 */

void Instant_DeployUtility::placeCP(const RefBattleCP& cp, const HexCord& hex, bool move)
{
#ifdef DEBUG
                        d_batData->assertHexOnMap(hex);
#endif
                        HexPosition hp(moveRight(cp->facing()), static_cast<HexPosition::HexDistance>(cp->hexPosition().centerPos()));
                        cp->position(BattlePosition(hex, hp));
                        if(move)
                        {
                         d_hexMap->add(cp);
                        }
}
#endif
#endif
/*
 * Deploy Units on Battlefield
 */

void deployUnits(BattleData* batData)
{
#if 0
        ASSERT(batData != 0);

        BobUtility::initUnitTypeFlags(batData);

        Instant_DeployUtility deployUtil(batData);
        deployUtil.deployAll();
#endif
}

/*******************************************
Start of James's unchecked change
*******************************************/

void deployUnits(BattleData* batData, Side side)
{
#if 0
        ASSERT(batData != 0);

        BobUtility::initUnitTypeFlags(batData);

        Instant_DeployUtility deployUtil(batData);
        deployUtil.deployAll(side);
#endif
}

/*******************************************
End of James's unchecked change
*******************************************/

#if 1
void
deployUnitsFromSave(BattleData * batdata) {
    BobUtility::initUnitTypeFlags(batdata);
    for(Side side = 0; side < batdata->ob()->sideCount(); ++side) {

        BattleCP * cp = batdata->ob()->getTop(side);
        if(cp) {

            cp->visibility(Visibility::Full);
            BattleSP * sp = cp->sp();
            while(sp) {
                sp->visibility(Visibility::Full);
                        sp = sp->sister();
                  }

                  playerDeploy(batdata, cp, cp->hex(), 0);
                  if(cp->child() ) deployChildrenFromSave(batdata, cp->child());
            }
      }
}


void
deployChildrenFromSave(BattleData * batdata, BattleCP * cp) {

      cp->visibility(Visibility::Full);
      BattleSP * sp = cp->sp();
      while(sp) {
            sp->visibility(Visibility::Full);
            sp = sp->sister();
      }

    playerDeploy(batdata, cp, cp->hex(), 0);
      cp->morale(BobUtility::baseMorale(batdata->ob(), cp));

      if(cp->child() ) deployChildrenFromSave(batdata, cp->child() );
      if(cp->sister() ) deployChildrenFromSave(batdata, cp->sister() );

}
#endif
};




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2002/11/24 13:17:48  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
