/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef REORGLOG_HPP
#define REORGLOG_HPP

#ifndef __cplusplus
#error boblog.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Battle OB Log
 *
 *----------------------------------------------------------------------
 */

#ifdef DEBUG

#include "clog.hpp"

extern LogFile reorgLog;

#endif


#endif /* BOBLOG_HPP */
