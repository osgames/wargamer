/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "brecover.hpp"
#include "batdata.hpp"
#include "batctrl.hpp"
#include "bob_cp.hpp"
#include "battime.hpp"
#include "sync.hpp"
#include "bobutil.hpp"
#include "batarmy.hpp"
#include "bobiter.hpp"
#include "cmbtutil.hpp"
#include "wg_rand.hpp"
#include "moveutil.hpp"
#include "b_tables.hpp"
//#include "batcord.hpp"
//#include "batlist.hpp"
#ifdef DEBUG
#include "clog.hpp"
static LogFile rLog("BRecover.log");
#endif

namespace B_Logic {

//-------------------------------------------------------------
// Recover base class
class RecoverBase {
   public:
    enum Types {
      Morale,
      Rout,
      Shaken,
      Disorder,
      Aggression,
      Fatigue,

      Type_HowMany
    };
   private:
    BattleGameInterface* d_batgame;
    BattleData* d_batData;

    BattleMeasure::BattleTime::Tick d_nextTick;
    BattleMeasure::BattleTime::Tick d_interval;
   public:
    RecoverBase() :
       d_batgame(0),
       d_batData(0),
       d_nextTick(0),
       d_interval(0) {}

    void nextTick(BattleMeasure::BattleTime::Tick t) { d_nextTick = t; }
    BattleMeasure::BattleTime::Tick nextTick() const { return d_nextTick; }

    void init(BattleGameInterface* batgame, BattleMeasure::BattleTime::Tick interval)
    {
       d_batgame = batgame;
       d_batData = batgame->battleData();
       d_interval = interval;
    }

    BattleGameInterface* batgame() const { return d_batgame; }
    BattleData* batData() const { return d_batData; }

    void run();
    bool shouldRun() const { return (nextTick() < d_batData->getTick()); }

    void newDay() { d_nextTick = 0; }
   private:

    virtual void recoverUnit(const RefBattleCP& cp) = 0;
#ifdef DEBUG
    virtual const char* name() const = 0;
#endif
};

void RecoverBase::run()
{
   {
        BattleData::WriteLock lock(d_batData);

#ifdef DEBUG
    rLog.printf("------------ Processing %s Recovery --------------", name());
    BattleTime btime = d_batData->getDateAndTime();
    Greenius_System::Time time = btime.time();
    Greenius_System::Date date = btime.date();

    rLog << "Finished logic at " << sysTime;
    rLog << ", time=" << (int) time.hours() << ":" << (int) time.minutes() << ":" << (int) time.seconds();
    rLog << ", Date=" << date.day() << "/" << 1+date.month() << "/" << date.year() << endl;
#endif

    // process fire combat
    for(Side side = 0; side < d_batData->ob()->sideCount(); ++side)
    {
#ifdef DEBUG
      rLog.printf("Battle Side %d", static_cast<int>(side));
#endif

//      RefBattleCP cp = d_batData->ob()->getTop(side);

      for(BattleUnitIter iter(d_batData->ob(), side); !iter.isFinished(); iter.next())
      {
         // process XX's only
         if(iter.cp()->getRank().sameRank(Rank_Division))
          recoverUnit(iter.cp());
      }

#ifdef DEBUG
      rLog.printf("------------------------\n");
#endif
    }

    // set next time to process
    d_nextTick = d_batData->getTick() + d_interval;
   }
}

enum {
  RMorale,
  RParentNear,
  RFatigue,
};

//-------------------------------------------------------------
//   RecoverMorale class -- Recover lost morale

class RecoverMorale : public RecoverBase {
   public:
    RecoverMorale() : RecoverBase()  {}

   private:
    void recoverUnit(const RefBattleCP& cp);
#ifdef DEBUG
    const char* name() const { return "Morale"; }
#endif
};

void RecoverMorale::recoverUnit(const RefBattleCP& cp)
{
   /*
    * To recover morale a unit must
    *    1. Be holding (i.e. not moving)
    *    2. Be at least 1760 yards from enemy ( 8 hexes)
    *    3. Need to recover morale
    */

   if(cp->startMorale() == cp->morale() ||
       cp->moveMode() != BattleCP::Holding ||
       cp->inCombat() ||
       cp->targetList().closeRange() < (6 * BattleMeasure::XYardsPerHex))
   {
//      cp->rallying(False);
//      cp->routing(False);
//      cp->shaken(False);
      return;
   }


#ifdef DEBUG
   rLog.printf("------- %s is Recovering Morale (%d)(%d)",
      cp->getName(), static_cast<int>(cp->startMorale()), static_cast<int>(cp->morale()));
#endif

   cp->rallying(True);
   bool shouldReorganize = False;
   for(std::vector<DeployItem>::const_iterator di = cp->mapBegin();
       di != cp->mapEnd();
       ++di)
   {
      if(di->active() && di->d_sp->strength(100) == 0)
      {
         shouldReorganize = True;
         break;
      }
   }

   if(shouldReorganize)
      cp->shouldReorganize(True);

   cp->posture(BattleOrderInfo::Defensive);

   // recover morale
   // we can recover morale if
   //    1. Unit has not reached critical loss
   //    2. And disorder is 2 or 3
   //    3. Unit is not shaken or routed
   //    4. We need to recover

   if(cp->criticalLossReached() ||
      cp->disorder() <= 1 ||
      cp->shaken() ||
      cp->routing())
   {
       return;
    }

    RefBattleCP attachedLeader = BobUtility::getAttachedLeader(batData(), cp);
    RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();

    // morale recovered = leader->charisma / 20 (rounded)
    const Table1D<SWORD>& table = BattleTables::recovery();
    UBYTE moraleRecovered = 0;
    if(table.getValue(RMorale) > 0)
      moraleRecovered = (leader->getCharisma() + (table.getValue(RMorale) / 2)) / table.getValue(RMorale);

#ifdef DEBUG
    rLog.printf("Unmodified moraleRecovered = %d", static_cast<int>(moraleRecovered));
#endif

    // TODO: modifiers to moraleRecovered
    // add 3 if non-attached corp leader with charisma 160+ is within 3 hexes
    if(cp->parent() != NoBattleCP)
    {
      if(!cp->parent()->attached() &&
         distanceFromUnit(cp, cp->parent()) <= 3)
      {
         moraleRecovered += static_cast<UBYTE>(table.getValue(RParentNear));//3;
      }
    }

    cp->morale(static_cast<Attribute>(minimum<int>(cp->startMorale(), cp->morale() + moraleRecovered)));

#ifdef DEBUG
    rLog.printf("Morale after recovery = %d", static_cast<int>(cp->morale()));
#endif
}

//-------------------------------------------------------------
//   RecoverFromRout class -- Recover from rout status

class RecoverFromRout : public RecoverBase {
    public:
      RecoverFromRout() : RecoverBase() {}

    private:
      void recoverUnit(const RefBattleCP& cp);
#ifdef DEBUG
      const char* name() const { return "Recover From Rout"; }
#endif
};

void RecoverFromRout::recoverUnit(const RefBattleCP& cp)
{
    // we must be rallying to recover
    if(cp->fleeingTheField() ||
          cp->criticalLossReached() ||
         !cp->routing())
    {
         return;
    }

#ifdef DEBUG
    rLog.printf("%s is attempting to recover from routing status", cp->getName());
#endif

    // see if we are a safe distance from enemy
    enum {
      Morale171,
      Morale135,
      Morale101,
      Morale66,
      Morale0,
      M_HowMany
    } moraleEnum = (cp->morale() >= 171) ? Morale171 :
                           (cp->morale() >= 135) ? Morale135 :
                           (cp->morale() >= 101) ? Morale101 :
                           (cp->morale() >= 66)  ? Morale66  : Morale0;

    static UWORD s_range[M_HowMany] = {
      4 * BattleMeasure::XYardsPerHex,
      5 * BattleMeasure::XYardsPerHex,
      6 * BattleMeasure::XYardsPerHex,
      7 * BattleMeasure::XYardsPerHex,
      8 * BattleMeasure::XYardsPerHex
    };

    if(cp->targetList().closeRange() <= s_range[moraleEnum])
      return;

    // get leader
    RefBattleCP attachedLeader = BobUtility::getAttachedLeader(batData(), cp);
    RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();

    // raw chance is percent of morale
    int chance = cp->moralePercent();
#ifdef DEBUG
    rLog.printf("Raw chance of recovering is %d%%", chance);
#endif

    // modify --------------------------------------------------------

    // +10 / +5 unit commander has charisma of 170 or 145
    if(cp->leader()->getCharisma() >= 170)
      chance += 10;
    else if(cp->leader()->getCharisma() >= 145)
      chance += 5;
#ifdef DEBUG
    rLog.printf("Chance after unit leader charisma modifier = %d%%", chance);
#endif

    // + 30, 20, 10, or 5 for attached leader
    if(attachedLeader != NoBattleCP)
    {
      if(leader->getCharisma() >= 200)
         chance += 30;
      else if(leader->getCharisma() >= 180)
         chance += 20;
      else if(leader->getCharisma() >= 160)
         chance += 10;
      else if(leader->getCharisma() >= 145)
         chance += 5;
#ifdef DEBUG
      rLog.printf("Chance after attached leader charisma modifier = %d%%", chance);
#endif
    }

    // +10 if enemy not within 8 hexes
    if(cp->targetList().closeRange() > (8 * BattleMeasure::XYardsPerHex))
      chance += 10;
#ifdef DEBUG
    rLog.printf("Chance after enemy not within 8 hexes modifier = %d%%", chance);
#endif

    // -10 if more than 8 hexes from corps commander
    CRefBattleCP parent = (cp->parent() != NoBattleCP) ? cp->parent() : BobUtility::getCinc(batData()->ob(), cp->getSide());
    if(parent)
    {
      if(B_Logic::distanceFromUnit(parent->hex(), cp) > 8)
         chance -= 10;
#ifdef DEBUG
      rLog.printf("Chance after XXX commander not within 8 hexes modifier = %d%%", chance);
#endif
    }

    // -10 enemy within 6
    if(cp->targetList().closeRange() <= (6 * BattleMeasure::XYardsPerHex))
      chance -= 10;
#ifdef DEBUG
    rLog.printf("Chance after enemy within 6 hexes modifier = %d%%", chance);
#endif

    // process result
    if(CRandom::get(100) < chance)
    {
       cp->routing(False);
#ifdef DEBUG
       rLog.printf("%s has recovered from routing status", cp->getName());
#endif
    }
}

//-------------------------------------------------------------
//   RecoverFromShaken class -- Recover from shaken status

class RecoverFromShaken : public RecoverBase {
    public:
      RecoverFromShaken() : RecoverBase() {}

    private:
      void recoverUnit(const RefBattleCP& cp);
#ifdef DEBUG
      const char* name() const { return "Recover From Shaken"; }
#endif
};

void RecoverFromShaken::recoverUnit(const RefBattleCP& cp)
{
    // return if we are not shaken
    if(!cp->shaken())
       return;

#ifdef DEBUG
    rLog.printf("%s is attempting to recover from shaken status", cp->getName());
#endif

    // cannot recover from shaken until we have recovered from routed
    if(cp->routing())
       return;

    // get leader
    RefBattleCP attachedLeader = BobUtility::getAttachedLeader(batData(), cp);
    RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();

    // If in a shock battle and a XXX or higher leader is not attached, we cannot recover
    if(cp->inCloseCombat() && attachedLeader == NoBattleCP)
       return;

    // base chance to recover is units morale as a % of 255
    int chance = cp->moralePercent();
#ifdef DEBUG
    rLog.printf("Base chance to recover is %d%%", chance);
#endif

    // modify
    //----------------------------------------------------

    // +1 for every 2 points of leader charisma over 144
    // Apply this to both, if unit has XXX or higher attached
    int dif = maximum(0, leader->getCharisma() - 144);
    chance += (dif / 2);

    if(attachedLeader != NoBattleCP)
    {
      dif = maximum(0, cp->leader()->getCharisma() - 144);
      chance += (dif / 2);
    }
#ifdef DEBUG
    rLog.printf("Chance to recover after charisma modifier %d%%", chance);
#endif

    // + 10 if no enemy within 8 hexes
    if(cp->targetList().closeRange() > (8 * BattleMeasure::XYardsPerHex))
      chance += 10;

#ifdef DEBUG
    rLog.printf("Chance to recover after enemy not within 8 hexes modifier %d%%", chance);
#endif

    // +20 if friendly non-shaken/routed within 4 hexes
    if(Combat_Util::nonRoutingFriendsNear(batData(), cp, 4))
      chance += 20;
#ifdef DEBUG
    rLog.printf("Chance to recover after friends within 4 modifier %d%%", chance);
#endif

    // +20 if in 'rallying' status
    if(cp->rallying())
      chance += 20;
#ifdef DEBUG
    rLog.printf("Chance to recover after 'Rallying' modifier %d%%", chance);
#endif

    // chance is halved, if outside command radius of XXX leader
    int nHexes;
    if(BobUtility::whatCommandRadius(batData()->ob(), cp, nHexes) ==  BobUtility::WhatCRadius::OutOfRadius)
      chance *= .5;
#ifdef DEBUG
    rLog.printf("Chance to recover after command radius modifier %d%%", chance);
#endif

    // halved if non shaken/routin enemy within 2 hexes
    if(cp->targetList().closeRange() <= (2 * BattleMeasure::XYardsPerHex))
    {
      // go through list. if any are unshaken\unrouting then modifty
      SListIterR<BattleItem> iter(&cp->targetList());
      while(++iter)
      {
         if(!iter.current()->d_cp->routing() && ! iter.current()->d_cp->shaken())
         {
             chance *= .5;
             break;
         }
      }
    }
#ifdef DEBUG
    rLog.printf("Chance to recover after enemy within 2 modifier %d%%", chance);
#endif

    // test to see if we recover
    if(CRandom::get(100) < chance)
    {
       // recovered !
       cp->shaken(False);
#ifdef DEBUG
       rLog.printf("%s has recovered from 'Shaken'", cp->getName());
#endif
    }
}

//-------------------------------------------------------------
//   RecoverDisorder class -- Recover disorder

class RecoverDisorder : public RecoverBase {
    public:
      RecoverDisorder() : RecoverBase() {}

    private:
      void recoverUnit(const RefBattleCP& cp);
#ifdef DEBUG
      const char* name() const { return "Recover Disorder"; }
#endif
};

void RecoverDisorder::recoverUnit(const RefBattleCP& cp)
{
    if(cp->disorder() == 3)
      return;

#ifdef DEBUG
    rLog.printf("%s is attempting to recover from disorder", cp->getName());
#endif

    // cannot recover if routing, shaken, or in shock battle
    if(cp->fleeingTheField() ||
          cp->routing() ||
          cp->shaken() ||
          cp->inCloseCombat())
    {
      return;
    }

    // get leader
    RefBattleCP attachedLeader = BobUtility::getAttachedLeader(batData(), cp);
    RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();

    // raw chance is morale percent
    int chance = cp->moralePercent();

#ifdef DEBUG
    rLog.printf("Raw chance for recovery = %d%%", chance);
#endif

    // modify--------------------------------------------------

    // +1 for every 2 points of leader charisma over 144
    // Apply this to both, if unit has XXX or higher attached
    int dif = maximum(0, leader->getCharisma() - 144);
    chance += (dif / 2);

    if(attachedLeader != NoBattleCP)
    {
      dif = maximum(0, cp->leader()->getCharisma() - 144);
      chance += (dif / 2);
    }
#ifdef DEBUG
    rLog.printf("Chance to recover after charisma modifier %d%%", chance);
#endif

    // +10 if no enemy with 4 hexes
    if(cp->targetList().closeRange() > (4 * BattleMeasure::XYardsPerHex))
      chance += 10;
#ifdef DEBUG
    rLog.printf("Chance after enemy outside of 4 hexes = %d%%", chance);
#endif

    // +10 if rallying
    if(cp->rallying())
      chance += 10;
#ifdef DEBUG
    rLog.printf("Chance after rallying = %d%%", chance);
#endif

    // -10 if in line and morale is 120 or less
    if(cp->spFormation() == SP_LineFormation && cp->morale() <= 120)
      chance -= 10;
#ifdef DEBUG
    rLog.printf("Chance after line-morale modifier = %d%%", chance);
#endif

    // +10 if cav and have cav specialist
    if(cp->generic()->isCavalry() &&
         (cp->leader()->isSpecialistType(Specialist::Cavalry) || leader->isSpecialistType(Specialist::Cavalry)) )
    {
      chance += 10;
#ifdef DEBUG
      rLog.printf("Chance after specialist modifier = %d%%", chance);
#endif
    }

    // chance is halved, if outside command radius of XXX leader
    int nHexes;
    if(BobUtility::whatCommandRadius(batData()->ob(), cp, nHexes) ==  BobUtility::WhatCRadius::OutOfRadius)
      chance *= .5;
#ifdef DEBUG
    rLog.printf("Chance to recover after command radius modifier %d%%", chance);
#endif

    // process test
    if(CRandom::get(100) < chance)
    {
      // decrease disorder 1 level
      cp->decreaseDisorder();
#ifdef DEBUG
    rLog.printf("%s has recovored 1 disorder level to %d",
          cp->getName(), static_cast<int>(cp->disorder()));
#endif

    }
}

//-------------------------------------------------------------
//   RecoverAggression class -- Recover lost aggression

class RecoverAggression : public RecoverBase {
    public:
      RecoverAggression() : RecoverBase() {}

    private:
      void recoverUnit(const RefBattleCP& cp);
#ifdef DEBUG
      const char* name() const { return "Recover Aggression"; }
#endif
};

void RecoverAggression::recoverUnit(const RefBattleCP& cp)
{
    // recover only if aggression is lower than ordered aggression
    if(//cp->aggression() >= cp->lastOrderedAggression() &&
        //cp->posture() == cp->getCurrentOrder().posture() &&
        !cp->postureLocked() &&
        !cp->aggressionLocked())//)cp->getCurrentOrder().aggression())
     {
      return;
    }

#ifdef DEBUG
    rLog.printf("%s is attempting to recover lost aggression", cp->getName());
#endif

    // cannot recover if routing, shaken, or in shock battle
    if(cp->fleeingTheField() ||
          cp->routing() ||
          cp->shaken() ||
          cp->inCloseCombat())
    {
      return;
    }

    // get leader
    RefBattleCP attachedLeader = BobUtility::getAttachedLeader(batData(), cp);
    RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();

    // chance to recover is leaders aggresion as a % of 255
    int chance = (leader->getAggression() * 100) / Attribute_Range;

#ifdef DEBUG
    rLog.printf("Raw chance of recovery = %d%%", chance);
#endif

    // modifiers
    //------------------------------------------------------------
    // + 10 if leader is cav specialist with cav
    if(cp->generic()->isCavalry() && leader->isSpecialistType(Specialist::Cavalry))
      chance += 10;
#ifdef DEBUG
    rLog.printf("Chance after cav specialist modifier = %d%%", chance);
#endif

   // X .5 if at critical loss
   if(cp->criticalLossReached())
    chance *= .5;
#ifdef DEBUG
   rLog.printf("Chance after critical loss modifier = %d%%", chance);
#endif

   // X .5 if fatigue is 65 or less
   if(cp->fatigue() <= 65)
    chance *= .5;
#ifdef DEBUG
   rLog.printf("Chance after fatigue modifier = %d%%", chance);
#endif

   // proc chance
   if(CRandom::get(100) < chance)
   {
         // recovering 1 aggression level

        cp->postureLocked(False);
        cp->posture(cp->getCurrentOrder().posture());
//       cp->aggression(static_cast<BattleOrderInfo::Aggression>(min(3, cp->aggression() + 1)), SLONG_MAX);
      if(cp->aggression() < cp->getCurrentOrder().aggression())
         cp->increaseAggression();

        if(cp->aggression() == cp->getCurrentOrder().aggression())
         cp->aggressionLocked(False);

#ifdef DEBUG
         rLog.printf("%s recoved 1 aggression point", cp->getName());
#endif
   }
}

class RecoverFatigue : public RecoverBase {
    public:
      RecoverFatigue() : RecoverBase() {}

    private:
      void recoverUnit(const RefBattleCP& cp);
#ifdef DEBUG
      const char* name() const { return "Recover Fatigue"; }
#endif
};

void RecoverFatigue::recoverUnit(const RefBattleCP& cp)
{
   // If moving or enemy is within 2 hexes, we cannot recover fatigue
   if(cp->moving() ||
      cp->deploying() ||
      cp->liningUp() ||
      cp->onManuever() ||
      cp->disorder() <= 1 ||
      cp->targetList().closeRange() <= (2 * BattleMeasure::XYardsPerHex))
   {
      return;
   }

   // remove 1 fatigue point
   const Table1D<SWORD>& table = BattleTables::recovery();
   cp->removeFatigue(static_cast<UBYTE>(table.getValue(RFatigue)));
}

//----------------------------------------------------------
// static instances of the above
const BattleMeasure::BattleTime::Tick TicksPerMin = 60 * BattleMeasure::BattleTime::TicksPerSecond;

static RecoverMorale     s_morale;//(4 * TicksPerMin);
static RecoverFromRout   s_rout;//(8 * TicksPerMin);
static RecoverFromShaken s_shaken;//(4 * TicksPerMin);
static RecoverDisorder   s_disorder;//(4 * TicksPerMin);
static RecoverAggression s_aggress;//(8 * TicksPerMin);
static RecoverFatigue    s_fatigue;
static RecoverBase* s_items[RecoverBase::Type_HowMany] = {
    &s_morale,
    &s_rout,
    &s_shaken,
    &s_disorder,
    &s_aggress,
    &s_fatigue
};

//-----------------------------------------------------------
// Client access

B_Recover::B_Recover(BattleGameInterface* batgame)
{
   const Table1D<UBYTE>& iTable = BattleTables::recoveryTime();

   for(int i = 0; i < RecoverBase::Type_HowMany; i++)
      s_items[i]->init(batgame, (iTable.getValue(i) * TicksPerMin));
}

void B_Recover::process()
{
   for(int i = 0; i < RecoverBase::Type_HowMany; i++)
   {
      if(s_items[i]->shouldRun())
         s_items[i]->run();
   }
}

void B_Recover::newDay()
{
   for(int i = 0; i < RecoverBase::Type_HowMany; i++)
   {
      s_items[i]->newDay();
   }
}

}; // end namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
