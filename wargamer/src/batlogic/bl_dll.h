/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BL_DLL_H
#define BL_DLL_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * BattleLogic DLL header
 *
 *----------------------------------------------------------------------
 */

#if defined(NO_WG_DLL)
    #define BATLOGIC_DLL
#elif defined(EXPORT_BATLOGIC_DLL)
    #define BATLOGIC_DLL __declspec(dllexport)
#else
    #define BATLOGIC_DLL __declspec(dllimport)
#endif



#endif /* BL_DLL_H */

