/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Game Logic
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "batlogic.hpp"
#include "batdata.hpp"
#include "building.hpp"
#include "blog.hpp"
#include "batctrl.hpp"
// #include "thread.hpp"
#include "sync.hpp"
#include "critical.hpp"
#include "batcord.hpp"
#include "b_send.hpp"
#include "bu_move.hpp"
#include "b_combat.hpp"
#include "bu_ord.hpp"
#include "brecover.hpp"
#include "b_rction.hpp"
#include "b_fow.hpp"
#include "b_reinf.hpp"
#include "control.hpp"

#if !defined(NOLOG)
// using B_Logic::logfile;
#endif
#include "b_end.hpp"

using namespace B_Logic;

// using B_Logic::BU_MoveInt;
// using B_Logic::B_CombatInt;
// using B_Logic::B_OrderProcInt;
// using B_Logic::B_Reaction;
// using B_Logic::B_FogOfWar;
// using B_Logic::B_Recover;

/*
 * Battle Logic Implementation
 */

class BattleLogicImp
{
   public:
      BattleLogicImp(BattleGameInterface* batgame);
      ~BattleLogicImp();

      void runLogic(BattleMeasure::BattleTime::Tick tick);
         // Run tick battleticks of the game

      void reinforced();// { d_reinforced = True; }
      void newDay()
      {
         d_nextTick = 0;
         d_combatInt.newDay();
         d_reinforceInt.newDay();
         d_fogOfWar.newDay();
         d_recover.newDay();
      }
   private:
      BattleGameInterface* d_batgame;
      PBattleData     d_batData;

      B_ReinforceInt  d_reinforceInt;
      BU_MoveInt      d_moveInt;
      B_CombatInt     d_combatInt;
      B_OrderProcInt  d_orderInt;
      B_Reaction      d_reaction;
      B_FogOfWar      d_fogOfWar;
      B_Recover       d_recover;


      bool d_reinforced;
      BattleMeasure::BattleTime::Tick d_nextTick;
};

BattleLogicImp::BattleLogicImp(BattleGameInterface* batgame) :
   d_batgame(batgame),
   d_batData(batgame->battleData()),
   d_reinforceInt(batgame),
   d_moveInt(batgame),
   d_combatInt(batgame),
   d_orderInt(d_batData),
   d_reaction(batgame),
   d_fogOfWar(batgame),
   d_recover(batgame),
   d_reinforced(False),
   d_nextTick(0)
{



#if !defined(NOLOG)
   logfile << "Creating BattleLogic at " << sysTime << endl;
#endif
   ASSERT(d_batData != 0);

   
   /*
   * Do an initial pass here, to ensuere that the BattleDespatcher
   * has a valid pointer to batData
   */
   procBattleDespatch(d_batData);
   
}

BattleLogicImp::~BattleLogicImp()
{


#if !defined(NOLOG)
   logfile << "Destructing BattleLogic at " << sysTime << endl;
#endif
}


void BattleLogicImp::runLogic(BattleMeasure::BattleTime::Tick ticks)
{
   using BattleMeasure::BattleTime;

//#if defined(SHOW_LOG)
#if !defined(NOLOG)
// static int count = 0;
// logfile << "Running logic " << count++ << " at " << sysTime << endl;
#endif
//#endif
   procBattleDespatch(d_batData);


   d_batData->addTime(ticks);

   // run various processes
   if(d_batData->getTick() >= d_nextTick)
   {
      // add any reinforcements if historical
      if(d_batData->type() == BattleData::Historical)
      {
        d_reinforceInt.processHistorical();
      }
      d_orderInt.process();       // process orders
      d_combatInt.process();      // process combat / close enemy routines
      d_reaction.process();       // process reaction mechanics
      d_moveInt.process();        // process movement
      d_fogOfWar.process();       // process Fog of War routines
      d_recover.process();        // process recovery for morale, rout, disorder, etc.


      d_batData->processBuildings(ticks); // process burning buildings state



        BattleFinish results;
        if(checkBattleOver(d_batData, &results))
        {
            ASSERT(results.why() != BattleResults::UnknownWhy);
            d_batgame->requestBattleOver(results);
        }

      // run next process in 5 game seconds
       d_nextTick = d_batData->getTick() + (5 * BattleMeasure::BattleTime::TicksPerSecond);
   }

   d_batData->setChanged();      // Ask to be redrawn...

//#if defined(SHOW_LOG)
#if !defined(NOLOG)
#if 0
   BattleTime btime = d_batData->getDateAndTime();
   Greenius_System::Time time = btime.time();
   Greenius_System::Date date = btime.date();

   logfile << "Finished logic at " << sysTime;
   logfile << ", time=" << (int) time.hours() << ":" << (int) time.minutes() << ":" << (int) time.seconds();
   logfile << ", Date=" << date.day() << "/" << date.month() << "/" << date.year() << endl;
#endif
#endif
//#endif
}

void BattleLogicImp::reinforced()
{
   d_reinforceInt.process();
}

/*==============================================================
 * Interface functions
 */

BattleLogic::BattleLogic(BattleGameInterface* batgame) :
   d_imp(new BattleLogicImp(batgame))
{
   ASSERT(batgame != 0);
   ASSERT(d_imp != 0);
}

BattleLogic::~BattleLogic()
{
   delete d_imp;
}

void BattleLogic::updateTime(BattleMeasure::BattleTime::Tick tick)
{
   ASSERT(d_imp != 0);
   d_imp->runLogic(tick);
}

void BattleLogic::start()
{
}

void BattleLogic::reinforced()
{
   ASSERT(d_imp);
   d_imp->reinforced();
}

void BattleLogic::newDay()
{
   ASSERT(d_imp);
   d_imp->newDay();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
