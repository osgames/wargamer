/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "b_combat.hpp"
#include "batctrl.hpp"
#include "batarmy.hpp"
#include "b_tables.hpp"
#include "sync.hpp"
#include "batcord.hpp"
#include "hexmap.hpp"
#include "bobiter.hpp"
#include "batlist.hpp"
#include "bobutil.hpp"
#include "wg_rand.hpp"
#include "b_detect.hpp"
#include "cmbtutil.hpp"
#include "blosses.hpp"
#include "bshockl.hpp"
#include "moveutil.hpp"
#include "scenario.hpp"
#include "batmsg.hpp"
#include "building.hpp"

#ifdef DEBUG
#include "clog.hpp"
static LogFile bcLog("BattleCombat.log");
#endif


namespace B_Logic
{

class B_CombatImp {
      BattleGameInterface* d_batgame;
      PBattleData d_batData;
    B_DetectInt d_bDetect;
    BattleTime::Tick d_nextTick;

      enum CavVsCavOutcome{
         Tie,
         WinningTie,
         Win1,
         Win2,
         Win3,
         Win4,
         Win5,

         CVC_HowMany
    };

      enum InfVsCavOutcome {
         CavLoss10,
         CavLoss8,
         CavLoss4,
         CavLoss3,
         CavLoss1,
         InfVsCavTie,
         InfLoss1,
         InfLoss3,
         InfLoss5,
         InfLoss9,
         InfLoss10,

         IVC_HowMany
      };

      // ShockData struct
    // data used by shock combat routines
      struct ShockData {
         enum { NumberSides = 2 };
         enum Flags {   // various flags
             Inf                 = 0x00000001,
             Cav                 = 0x00000002,
             Art                 = 0x00000004,
             Massed              = 0x00000008,
             InPursuit           = 0x00000010,
             Lines               = 0x00000020,
             Columns             = 0x00000040,
             Attacking           = 0x00000080,
             Defending           = 0x00000100,
             Shaken              = 0x00000200,
             Routing             = 0x00000400,
             Unsupported         = 0x00000800,
             Retreating          = 0x00001000,
             CorpCavSpecialist   = 0x00002000,
             InMediumCover       = 0x00004000,
             InHeavyCover        = 0x00008000,
             InSuperHeavyCover   = 0x00010000,
             Deployed            = 0x00020000,
             Extended            = 0x00040000,
             IntoEnemyFlank      = 0x00080000,
             March               = 0x00100000,
             ChangingSPFormation = 0x00200000,
             Result_Routing      = 0x00400000,
             Result_Retreated    = 0x00800000,
             Result_Retired      = 0x01000000,
             Result_FallenBack   = 0x02000000,
             HasHorseArtillery   = 0x04000000,
             Squared             = 0x08000000,
             ClosedColumns       = 0x10000000,
             SPMarch             = 0x20000000,
             HasArtillery        = 0x40000000,
             HasCavAttached      = 0x80000000
         };
         enum Flags2 {
             CriticalLossReached  = 0x00000001,
             InRough              = 0x00000002,
             InMarsh              = 0x00000004,
             CrossingRiver        = 0x00000008,
             CrossingStream       = 0x00000010,
             CrossingSunkenStream = 0x00000020,
             HasCavSpecialist     = 0x00000040,
             UnsupportedArtillery = 0x00000080,
             AttackDefendVP       = 0x00000100
         };

         SWORD       d_fireValue[NumberSides]; // Fire points
         SWORD       d_spsInCombat[NumberSides];
         UWORD       d_spsInBuiltUp[NumberSides];
         Nationality d_nation[NumberSides];
         ULONG       d_flags[NumberSides];
         ULONG       d_flags2[NumberSides];
         UWORD       d_attachedCharisma[NumberSides];
         UWORD       d_impact[NumberSides];
         UWORD       d_morale[NumberSides];
         UWORD       d_fatigue[NumberSides];
         UWORD       d_disorder[NumberSides];
         UWORD       d_disorderLost[NumberSides];
         UWORD       d_charisma[NumberSides];
         UWORD       d_aggression[NumberSides];
         UBYTE       d_losses[NumberSides]; // What percent losses do units suffer?
         UBYTE       d_dieRoll[NumberSides];
         BattleOrderInfo::Posture d_posture[NumberSides];
         int         d_elevation[NumberSides];

         ShockData()
         {
             for(int s = 0; s < NumberSides; s++)
             {
               d_fireValue[s] = 0;
               d_spsInCombat[s] = 0;
               d_spsInBuiltUp[s] = 0;
               d_nation[s] = scenario->getDefaultNation(s);
               d_flags[s] = 0;
               d_flags2[s] = 0;
               d_attachedCharisma[s] = 0;
               d_impact[s] = 0;
               d_morale[s] = 0;
               d_fatigue[s] = 0;
               d_disorder[s] = 0;
               d_disorderLost[s] = 0;
               d_charisma[s] = 0;
               d_aggression[s] = 0;
          d_losses[s] = 0;
          d_dieRoll[s] = 0;
          d_posture[s] = BattleOrderInfo::Defensive;
          d_elevation[s] = 0;
         }
      }
    };

   public:
    B_CombatImp(BattleGameInterface* batgame) :
      d_batgame(batgame),
      d_batData(batgame->battleData()),
      d_bDetect(batgame),
      d_nextTick(0) {}
    ~B_CombatImp() {}

    void process();
    void newDay();
   private:

    void doOrgCombat(const RefBattleCP& cp);
    void doOrgAndSisters(const RefBattleCP& cp, bool forCombat = True);
    void clearAllFlags();
    void clearFlags(const RefBattleCP& cp);
    void procShockBuildings(Side side, ShockData& sd, ShockBattleItem& sbi);
    void procFireBuildings(const RefBattleCP& cp, const RefBattleCP& enemyCP, int fireValue);
    void procRoundWinner(ShockData& sd, ShockBattleItem& sbi);
//    void clearTakingHitsFlags(const RefBattleCP& cp);
//    void clearShootingFlags(const RefBattleCP& cp);
//    void clearTakingHitsFlags(const RefBattleCP& cp);
    void doFireCombat(const RefBattleCP& cp);
    void doFireLosses(const RefBattleCP& cp, const RefBattleCP& inflictingCP, const UWORD fireValue, const int dieRoll);
    UINT doShockLosses(SideShockBattleList& sl, const int totalLoss);
    bool canCharge(const RefBattleCP& cp);
    bool procShockCombat(ShockBattleItem& sbi);
    void getNation(Side s, SideShockBattleList& sl, ShockData& sd);
    Side getOtherSide(Side s) { return (s == 0) ? 1 : 0; }
    void applyLoss(const RefBattleCP& cp, const RefBattleCP& inflictingCP, int losses, bool fireCombat = False);
    void infVsInfShockBattle(ShockData& sd, ShockBattleItem& sbi, bool withModifiers = True);
    void procShockUnsupportedArtillery(ShockData& sd, ShockBattleItem& sbi);
    void procPostShockMoraleInfVsInf(ShockData& sd, ShockBattleItem& sbi);
    void cavVsCavShockBattle(ShockData& sd, ShockBattleItem& sbi);
    void procCavVsCavOutcome(ShockData& sd, ShockBattleItem& sbi, CavVsCavOutcome outcome);
    void infVsCavShockBattle(ShockData& sd, ShockBattleItem& sbi);
    void procInfVsCavOutcome(ShockData& sd, ShockBattleItem& sbi, InfVsCavOutcome outcome);
    void getElevation(Side s, SideShockBattleList& sl, ShockData& sd);
    void pursuitTest(const RefBattleCP& cp, const RefGLeader& leader);
    void doRecall(const RefBattleCP& cp, const RefGLeader& leader, ShockBattleItem& sbi);
    void trophyTest(const RefBattleCP& cp, SideShockBattleList& list);
   void procCavCharge(const RefBattleCP& cp);
    bool firingIntoFlank(const RefBattleCP& cp, const RefBattleCP& enemyCP);
    bool continueShockCombat(ShockBattleItem& sbi);
    void clearBattle(ShockBattleItem& sbi);
//  void routUnit(const RefBattleCP& cp);
};

void B_CombatImp::process()
{
//  if(d_batData->getTick() >= d_nextTick)
   // d_batData->lock.startWrite();

   BattleData::WriteLock lock(d_batData);

   {

#ifdef DEBUG_COMBAT
    bcLog.printf("------------ Processing Fire Combat --------------");
#endif

    // first iterate through all units and clear battle flags
//    clearAllFlags();

    // process fire combat
    for(Side side = 0; side < d_batData->ob()->sideCount(); ++side)
    {
#ifdef DEBUG_COMBAT
      bcLog.printf("Battle Side %d", static_cast<int>(side));
#endif

      RefBattleCP cp = d_batData->ob()->getTop(side);

//    if(cp->nextFire() < d_batData->getTick())
//      clearFlags(cp);
      if(!cp->hasQuitBattle() && cp->active())
         doOrgAndSisters(cp);

#ifdef DEBUG_COMBAT
      bcLog.printf("------------------------\n");
#endif
    }

//    d_nextTick = d_batData->getTick() + (120 * BattleMeasure::BattleTime::TicksPerSecond);
   }

   // process shock combat
   {
#ifdef DEBUG_COMBAT
    bcLog.printf("------------ Processing Shock Combat --------------");
#endif

      ShockBattleListIter iter(&d_batData->shockBattles());
      while(++iter)
      {
         if(iter.current()->d_nextRound <= d_batData->getTick())
         {
            if(!procShockCombat(*iter.current()))
               iter.remove();
         }
      }
   }

   // clear out any obsolete battle flags
   {
       if(d_batData->getTick() >= d_nextTick)
      {
          clearAllFlags();
          d_nextTick = d_batData->getTick() + (30 * BattleMeasure::BattleTime::TicksPerSecond);
       }
   }

}

void B_CombatImp::newDay()
{
   d_nextTick = 0;

   ShockBattleListIter iter(&d_batData->shockBattles());
   while(++iter)
      iter.current()->d_nextRound = 0;
}

void B_CombatImp::clearAllFlags()
{
    for(Side side = 0; side < d_batData->ob()->sideCount(); ++side)
    {
//    RefBattleCP cp = d_batData->ob()->getTop(side);
      for(BattleUnitIter iter(d_batData->ob(), side); !iter.isFinished(); iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Division) && iter.cp()->inCombat())
           clearFlags(iter.cp());
      }
    }
}

// See if any buildings catch fire
void B_CombatImp::procShockBuildings(Side side, ShockData& sd, ShockBattleItem& sbi)
{
   BattleBuildings::BuildingList* bl = d_batData->buildingList();
   BattleBuildings::BuildingTable* bt = d_batData->buildingTable();
   Side otherSide = getOtherSide(side);
   SListIter<SideShockBattleItem> iter(&sbi.d_units[otherSide]);
   while(++iter)
   {
      // Unchecked
      //vector<DeployItem>& map = iter.current()->d_cp->deployMap();
      // go through front row and see if any buildings catch fire
      for(int c = 0; c < iter.current()->d_cp->columns(); c++)
      {
         //int index = (iter.current()->d_cp->columns() * 0) + c;
         //const DeployItem& di = map[index];
         const DeployItem* di = iter.current()->d_cp->currentDeployItem(c, 0);
         ASSERT(di);
         if(di && di->active())
         {
            BattleBuildings::BuildingListEntry * bi = bl->get(di->d_sp->hex());
            if(bi != NULL)
            {
               Combat_Util::CoverType ct = (sd.d_flags[otherSide] & ShockData::InMediumCover) ? Combat_Util::Medium :
                                                         (sd.d_flags[otherSide] & ShockData::InHeavyCover)  ? Combat_Util::Heavy : Combat_Util::SuperHeavy;

               ASSERT(ct < Combat_Util::Cover_HowMany);
               static const int s_table[Combat_Util::Cover_HowMany] = {
                     0, 5, 4, 3, 2
               };

               if(CRandom::get(100) < s_table[ct])
               {
                  // Set it on fire
#ifdef DEBUG
                  bcLog.printf("Buildings in hex %d, %d have been set afire",
                     (int)di->d_sp->hex().x(), (int)di->d_sp->hex().y());
#endif
                  /*
                  * Set building on fire, with a random interval
                  * and random chance of playing explosion sound
                  */
                  if(bi->status() == BattleBuildings::BUILDING_NORMAL) {
                     bi->status(BattleBuildings::BUILDING_BURNING);
                     bi->effectCount(random(0,BUILDING_BURNING_INTERVAL/2));
                     if(random(0,100) < 25)
                        d_batData->soundSystem()->triggerSound(GetSmashSound(), BATTLESOUNDPRIORITY_MEDIUM, di->d_sp->hex() );
                  }
               }
            }
         }
      }

      // End
   }
}

void B_CombatImp::procFireBuildings(const RefBattleCP& cp, const RefBattleCP& enemyCP, int fireValue)
{
   ASSERT(cp->generic()->isArtillery());
   BattleBuildings::BuildingList* bl = d_batData->buildingList();
   BattleBuildings::BuildingTable* bt = d_batData->buildingTable();

   for(std::vector<DeployItem>::iterator di = enemyCP->mapBegin();
       di != enemyCP->mapEnd();
       ++di)
   {
      if(di->active())
      {
         BattleBuildings::BuildingListEntry * bi = bl->get(di->d_sp->hex());
         if(bi != NULL)
         {
            // get hex cover-type
            HexCord hex = di->d_sp->hex();
            Combat_Util::CoverType ct = Combat_Util::hexCoverType(hex, d_batData, False);
            ASSERT(ct < Combat_Util::Cover_HowMany);

            const int c_base = 10;
            const int s_table[Combat_Util::Cover_HowMany] = {
                15, 13, 10, 7, 4
            };

            // multiply by fire value for chance
            int chance = (fireValue * s_table[ct]) / c_base;
            if(CRandom::get(100) < chance)
            {
               // Set it on fire
#ifdef DEBUG
               bcLog.printf("Buildings in hex %d, %d have been set afire",
                     (int)di->d_sp->hex().x(), (int)di->d_sp->hex().y());
#endif
               /*
               * Set building on fire, with a random interval
               * and random chance of playing explosion sound
               */
               if(bi->status() == BattleBuildings::BUILDING_NORMAL) {
                  bi->status(BattleBuildings::BUILDING_BURNING);
                  bi->effectCount(random(0,BUILDING_BURNING_INTERVAL/2));
                  if(random(0,100) < 25)
                     d_batData->soundSystem()->triggerSound(GetSmashSound(), BATTLESOUNDPRIORITY_MEDIUM, di->d_sp->hex() );
               }
            }
         }

#if 0
         UWORD bi = bl->get(di->d_sp->hex());
         if(bi != NoBuilding)
         {
            // get hex cover-type
            HexCord hex = di->d_sp->hex();
            Combat_Util::CoverType ct = Combat_Util::hexCoverType(hex, d_batData);
            ASSERT(ct < Combat_Util::Cover_HowMany);

            const int c_base = 10;
            const int s_table[Combat_Util::Cover_HowMany] = {
                15, 13, 10, 7, 4
            };

            // multiply by fire value for chance
            int chance = (fireValue * s_table[ct]) / c_base;
            if(CRandom::get(100) < chance)
            {
               // Set it on fire
#ifdef DEBUG
               bcLog.printf("Buildings in hex %d, %d have been set afire",
                     (int)di->d_sp->hex().x(), (int)di->d_sp->hex().y());
#endif
               BattleBuildings::BuildingInfo* info = bt->info(bi);
               std::vector<BattleBuildings::BuildingDisplayInfo>& bdi = info->displayInfo();
               for(std::vector<BattleBuildings::BuildingDisplayInfo>::iterator vi = bdi.begin();
                   vi != bdi.end();
                   ++vi)
               {
                  (*vi).status(BattleBuildings::BUILDING_BURNING);
               }
            }
         }
#endif
      }
   }
}

bool B_CombatImp::firingIntoFlank(const RefBattleCP& cp, const RefBattleCP& enemyCP)
{
   if(enemyCP->sp() != NoBattleSP)
   {
      // calculate avg facing of enemy
      LONG value = 0;
      int count = 0;
      for(std::vector<DeployItem>::iterator di = enemyCP->mapBegin();
          di != enemyCP->mapEnd();
          ++di)
      {
         if(di->active())
         {
            value += di->d_sp->facing();
            count++;
         }
      }

      if(count > 0)
      {
         // get raw avg
         HexPosition::Facing f = static_cast<HexPosition::Facing>(value / count);
         // get close hexpoint
         HexPosition::Facing theirFace = (f < 42 && f >= 0) ? HexPosition::NorthEastPoint :
                                         (f <= 86)          ? HexPosition::North :
                                         (f <= 128)         ? HexPosition::NorthWestPoint :
                                         (f < 170)          ? HexPosition::SouthWestPoint :
                                         (f <= 213)         ? HexPosition::South : HexPosition::SouthEastPoint;

         HexPosition::Facing bFace = oppositeFace(theirFace);
         if(bFace != cp->facing())
         {
            if(moveLeft(cp->facing()) == bFace)
            {
               //HexCord ourLeft = cp->leftHex();
               HexCord ourRight = cp->rightHex();
               HexCord theirLeft = enemyCP->leftHex();
               //HexCord theirRight = enemyCP->rightHex();
               //MoveOrientation moL = moveOrientation(cp->facing(), ourLeft, theirLeft, MOA_Narrow);
               MoveOrientation mo = moveOrientation(cp->facing(), ourRight, theirLeft, MOA_Narrow);
#ifdef DEBUG
               if(mo == MO_Left)
               {
                  bcLog.printf("%s(f=%d) is firing into the flank of %s(f=%d)",
                     cp->getName(), static_cast<int>(cp->facing()), enemyCP->getName(), static_cast<int>(enemyCP->facing()));
                  return True;
               }
               else
                  return False;
#else
               return (mo == MO_Left);
#endif
            }
            if(moveRight(cp->facing()) == bFace)
            {
               //HexCord ourLeft = cp->leftHex();
               HexCord ourLeft = cp->leftHex();
               HexCord theirRight = enemyCP->rightHex();
               //HexCord theirRight = enemyCP->rightHex();
               //MoveOrientation moL = moveOrientation(cp->facing(), ourLeft, theirLeft, MOA_Narrow);
               MoveOrientation mo = moveOrientation(cp->facing(), ourLeft, theirRight, MOA_Narrow);
#ifdef DEBUG
               if(mo == MO_Right)
               {
                  bcLog.printf("%s(f=%d) is firing into right flank of %s(f=%d)",
                     cp->getName(), static_cast<int>(cp->facing()), enemyCP->getName(), static_cast<int>(enemyCP->facing()));
                  return True;
               }
               else
                  return False;
#else
               return (mo == MO_Right);
#endif
            }
            else
            {
#ifdef DEBUG
               bcLog.printf("%s(f=%d) is firing into deep flank of %s(f=%d)",
                     cp->getName(), static_cast<int>(cp->facing()), enemyCP->getName(), static_cast<int>(enemyCP->facing()));
#endif
               return True;
            }
         }
         //return (oppositeFace(avgFace) != cp->facing());
      }
   }
   return False;
}

void B_CombatImp::getNation(Side s, SideShockBattleList& sl, ShockData& sd)
{
   // set nationality of this side of a shock battle
   // if just one unit is present return its nation
   if(sl.entries() == 1)
  {
    sd.d_nation[s] = sl.first()->d_cp->getNation();
    return;
  }

    // go through each unit in list. Nationality is the on with the
   // most SP's present in the battle
   UWORD* nations = new UWORD[scenario->getNumNations()];
   ASSERT(nations);

   memset(nations, 0, scenario->getNumNations() * sizeof(UWORD));

  // icrement nation value in table for each SP in battle
  SideShockBattleListIterR iter(&sl);
  while(++iter)
    nations[iter.current()->d_cp->getNation()]++;

  // nation with most SP is it
  Nationality bestN = sd.d_nation[s];
  UWORD bestTotal = 0;
  for(int n = 0; n < scenario->getNumNations(); n++)
  {
      if(nations[n] > bestTotal)
      {
      bestN = n;
      bestTotal = nations[n];
      }
  }

  sd.d_nation[s] = bestN;

   delete[] nations;
}

void B_CombatImp::getElevation(Side s, SideShockBattleList& sl, ShockData& sd)
{
   // set Elevation to average of  this side
   // if just one unit is present return its elevation
   if(sl.entries() == 1)
   {
    sd.d_elevation[s] = Combat_Util::averageHeight(sl.first()->d_cp, d_batData);
    return;
  }

  else
   {
      int spCount = 0;
      int elevation = 0;

      SideShockBattleListIterR iter(&sl);
      while(++iter)
      {
         elevation += Combat_Util::averageHeight(iter.current()->d_cp, d_batData);
      spCount += iter.current()->d_cp->spCount();
      }

    if(spCount > 0)
         sd.d_elevation[s] = elevation / spCount;
   }
}

bool B_CombatImp::continueShockCombat(ShockBattleItem& sbi)
{
   for(Side s = 0; s < d_batData->ob()->sideCount(); s++)
   {
      SideShockBattleList& sl = sbi.d_units[s];
      SideShockBattleListIter iter(&sl);
      bool stillInCombat = False;
      while(++iter)
      {
//       bool mb = iter.current()->d_cp->movingBackwards();
//       int range = iter.current()->d_cp->targetList().closeRange();
#ifdef DEBUG
         bcLog.printf("%s %s moving backwards, range = %d yards, NearEnemyMode = %s",
            iter.current()->d_cp->getName(),
            (iter.current()->d_cp->movingBackwards()) ? "is" : "is not",
            iter.current()->d_cp->targetList().closeRange(),
            BattleCP::nearEnemyName(iter.current()->d_cp->nearEnemyMode()));
#endif
//       if(iter.current()->d_cp->movingBackwards() ||
//           iter.current()->d_cp->nearEnemyMode() >= BattleCP::NE_KeepAway2 ||
//           iter.current()->d_cp->targetList().closeRange() > BattleMeasure::XYardsPerHex)
         if(Combat_Util::bestShockTarget(iter.current()->d_cp) == NoBattleCP)
         {
            iter.current()->d_cp->inCloseCombat(False);
            iter.current()->d_cp->charging(False);
#ifdef DEBUG
            bcLog.printf("Removing %s from shock battle", iter.current()->d_cp->getName());
#endif
            iter.remove();
         }
         else
            stillInCombat = True;
      }

      if(!stillInCombat)
      return False;
   }

   return True;
}

void B_CombatImp::clearBattle(ShockBattleItem& sbi)
{
#ifdef DEBUG
      bool oneSideOnly = False;
#endif
      const Table1D<SWORD>& table = BattleTables::moraleBonus();
      enum {
         InfVsInf,
         CavVsCav,
         CavVsInf,
         InfVsCav
      };

      for(Side s = 0; s < 2; s++)
      {
         SideShockBattleListIter iter(&sbi.d_units[s]);
         while(++iter)
         {
            RefBattleCP cp = iter.current()->d_cp;
            cp->inCloseCombat(False);
            cp->charging(False);

            // give morale bonus
            if(cp->targetList().entries() > 0)
            {
               Attribute newMorale = cp->morale();

               CRefBattleCP enemyCP = cp->targetList().first()->d_cp;
               if(cp->generic()->isInfantry() &&
                  enemyCP->generic()->isInfantry() &&
                  (enemyCP->movingBackwards() || enemyCP->routing() || enemyCP->fleeingTheField()) )
               {
                  newMorale = minimum(255, newMorale + table.getValue(InfVsInf));
               }

               else if(cp->generic()->isCavalry() &&
                       enemyCP->generic()->isCavalry() &&
                       (enemyCP->movingBackwards() || enemyCP->routing() || enemyCP->fleeingTheField()) )
               {
                  newMorale = minimum(255, newMorale + table.getValue(CavVsCav));
               }

               else if(cp->generic()->isCavalry() &&
                  enemyCP->generic()->isInfantry() &&
                  (enemyCP->routing() || enemyCP->fleeingTheField()) )
               {
                  newMorale = minimum(255, newMorale + table.getValue(CavVsInf));
               }

               else if(cp->generic()->isInfantry() &&
                       enemyCP->generic()->isCavalry() &&
                       (enemyCP->routing() || enemyCP->fleeingTheField()) )
               {
                  newMorale = minimum(255, newMorale + table.getValue(InfVsCav));
               }

#ifdef DEBUG
               bcLog.printf("Old morale for %s=%d, new morale=%d",
                  cp->getName(), static_cast<int>(cp->morale()), static_cast<int>(newMorale));
#endif
               cp->morale(newMorale);
            }
         }
#ifdef DEBUG
         if(sbi.d_units[s].entries() > 0)
         {
            if(!oneSideOnly)
            {
               oneSideOnly = True;
            }
            else
               FORCEASSERT("Side Error in clearBattle()");
         }
#endif
      }
}

bool B_CombatImp::procShockCombat(ShockBattleItem& sbi)
{
#ifdef DEBUG
   bcLog.printf("------------ Processing Shock Battle --------------");
   BattleTime btime = d_batData->getDateAndTime();
   Greenius_System::Time time = btime.time();
   Greenius_System::Date date = btime.date();

   bcLog << "Finished logic at " << sysTime;
   bcLog << ", time=" << (int) time.hours() << ":" << (int) time.minutes() << ":" << (int) time.seconds();
   bcLog << ", Date=" << date.day() << "/" << 1+date.month() << "/" << date.year() << endl;
   {
      for(Side s = 0; s < d_batData->ob()->sideCount(); s++)
      {
         bcLog.printf("%s units", scenario->getSideName(s));
         bcLog.printf("-----------------------");
         SideShockBattleList& sl = sbi.d_units[s];
         SideShockBattleListIter iter(&sl);
         while(++iter)
         {
            bcLog.printf("     %s", iter.current()->d_cp->getName());
         }
      }
   }
#endif

   if(!continueShockCombat(sbi))//.d_flags & ShockBattleItem::BattleOver)
   {
#ifdef DEBUG
      bcLog.printf("Shock battle has ended, units moved away");
#endif
      clearBattle(sbi);
      return False;
   }

   ShockData sd;
   sbi.d_round++;
    // Get data.
    // This includes total fireValues, misc flags, morale, disorder, etc...
   for(Side s = 0; s < ShockData::NumberSides; s++)
   {
      // go though each friendly cp in list
      SideShockBattleListIter iter(&sbi.d_units[s]);
      bool supported = False;
      bool supportedArty = False;
      while(++iter)
      {
         int fireValue = 0;

         RefBattleCP cp = iter.current()->d_cp;
         RefBattleCP attachedLeader = BobUtility::getAttachedLeader(d_batData, cp);
         RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();

         if(leader->getRankLevel().isHigher(Rank_Division) && leader->isSpecialistType(Specialist::Cavalry))
            sd.d_flags[s] |= ShockData::CorpCavSpecialist;

         if(cp->leader()->isSpecialistType(Specialist::Cavalry))
            sd.d_flags2[s] |= ShockData::HasCavSpecialist;

         // nationality
         getNation(s, sbi.d_units[s], sd);
         // elevation
         getElevation(s, sbi.d_units[s], sd);

         if(!supportedArty)
         {
            if(cp->generic()->isArtillery())
               supportedArty = Combat_Util::artillerySupported(d_batData, cp);//Combat_Util::nonRoutingFriendsNear(d_batData, cp, 4);
         }

         if(!supported)
            supported = Combat_Util::nonRoutingFriendsNear(d_batData, cp, 4);

         // posture
         if(cp->posture() == BattleOrderInfo::Offensive)
             sd.d_posture[s] = BattleOrderInfo::Offensive;

         // add charisma
         sd.d_charisma[s] += leader->getCharisma();
         sd.d_aggression[s] += cp->aggression();

         // impact
         sd.d_impact[s] += BobUtility::unitImpact(d_batData->ob(), cp);

         // Critical loss already reached
         if(cp->criticalLossReached())
            sd.d_flags2[s] |= ShockData::CriticalLossReached;

         // sp formation flag
         if(cp->spFormation() == SP_ChangingFormation)
             sd.d_flags[s] |= ShockData::ChangingSPFormation;
         else if(cp->spFormation() == SP_ClosedColumnFormation)
             sd.d_flags[s] |= ShockData::ClosedColumns;
         else if(cp->spFormation() == SP_SquareFormation)
             sd.d_flags[s] |= ShockData::Squared;
         else if(cp->spFormation() == SP_ColumnFormation)
             sd.d_flags[s] |= ShockData::Columns;
         else if(cp->spFormation() == SP_MarchFormation)
             sd.d_flags[s] |= ShockData::SPMarch;
         else if(cp->spFormation() == SP_LineFormation)
             sd.d_flags[s] |= ShockData::Lines;

         // get unit deployment flags
         if(cp->formation() == CPF_March)
            sd.d_flags[s] |= ShockData::March;
         else if(cp->formation() == CPF_Massed)
             sd.d_flags[s] |= ShockData::Massed;
         else if(cp->formation() == CPF_Deployed)
             sd.d_flags[s] |= ShockData::Deployed;
         else
            sd.d_flags[s] |= ShockData::Extended;

         // flags for cover
         Combat_Util::CoverType ct = Combat_Util::coverType(cp, d_batData);
         if(ct == Combat_Util::Medium)
             sd.d_flags[s] |= ShockData::InMediumCover;
         else if(ct == Combat_Util::Heavy)
             sd.d_flags[s] |= ShockData::InHeavyCover;
         else if(ct == Combat_Util::SuperHeavy)
             sd.d_flags[s] |= ShockData::InSuperHeavyCover;

         // flags for terrain
         GraphicalTerrainEnum gt = Combat_Util::terrainType(d_batData, cp);
         if(gt == GT_Rough)
            sd.d_flags2[s] |= ShockData::InRough;
         else if(gt == GT_Marsh)
            sd.d_flags2[s] |= ShockData::InMarsh;

         // flags for crossing river/streams
         if(Combat_Util::crossingRiver(d_batData, cp))
            sd.d_flags2[s] |= ShockData::CrossingRiver;
         else if(Combat_Util::crossingSunkenStream(d_batData, cp))
            sd.d_flags2[s] |= ShockData::CrossingSunkenStream;
         else if(Combat_Util::crossingStream(d_batData, cp))
            sd.d_flags2[s] |= ShockData::CrossingStream;

         // flags for shaken or routed
         if(cp->shaken())
             sd.d_flags[s] |= ShockData::Shaken;
         if(cp->routing())
             sd.d_flags[s] |= ShockData::Routing;

         // retreating
         if(cp->retreating() || cp->movingBackwards() || cp->routing() || cp->fleeingTheField())
            sd.d_flags[s] |= ShockData::Retreating;

         // flags for attacking into enemy flank
         SideShockBattleListIter iter2(&sbi.d_units[getOtherSide(s)]);
         bool intoFlank = False;
         while(++iter2)
         {
            if(firingIntoFlank(cp, iter2.current()->d_cp))
            {
               intoFlank = True;
                  break;
            }
         }

         if(intoFlank)
            sd.d_flags[s] |= ShockData::IntoEnemyFlank;

         // set unit type flags
         // adding available arty to firevalue if inf or cav XX
         if(cp->generic()->isInfantry())
         {
            sd.d_flags[s] |= ShockData::Inf;

            if(cp->holding())
            {
               fireValue += (minimum(cp->columns(), cp->nArtillery()));
               sd.d_spsInCombat[s] += (minimum(cp->columns(), cp->nArtillery()));
            }

            if(cp->nArtillery() > 0)
               sd.d_flags[s] |= ShockData::HasArtillery;

            // see if we have attache Cav SP
//          RefBattleSP sp = cp->sp();
//          while(sp != NoBattleSP)
            for(BattleSPIter spIter(d_batData->ob(), cp, BSPI_ModeDefs::All);
                  !spIter.isFinished();
                  spIter.next())
            {
               const UnitTypeItem& uti = d_batData->ob()->getUnitType(spIter.sp());
               if(uti.getBasicType() == BasicUnitType::Cavalry)
               {
                  sd.d_flags[s] |= ShockData::HasCavAttached;
                  break;
               }
            }
         }
         else if(cp->generic()->isArtillery())
            sd.d_flags[s] |= ShockData::Art;

         else if(cp->generic()->isCavalry())
         {
            sd.d_flags[s] |= ShockData::Cav;
            if(cp->holding())
            {
               fireValue += (minimum(cp->columns(), cp->nArtillery()));
               sd.d_spsInCombat[s] += (minimum(cp->columns(), cp->nArtillery()));
            }
            // End

            if(cp->nArtillery() > 0)
               sd.d_flags[s] |= ShockData::HasHorseArtillery;
         }

         // add charisma, morale, etc...
         if(attachedLeader != NoBattleCP &&
            leader->getCharisma() > sd.d_attachedCharisma[s])
         {
            sd.d_attachedCharisma[s] = leader->getCharisma();
         }


         if(cp->getCurrentOrder().wayPoints().entries() == 0)
         {
            if(!(sd.d_flags[s] & ShockData::Attacking))
               sd.d_flags[s] |= ShockData::Defending;
         }
         else if(!(cp->movingBackwards() || cp->routing() || cp->fleeingTheField()))
         {
            sd.d_flags[s] |= ShockData::Attacking;
         }

         sd.d_morale[s] += cp->morale();
         sd.d_fatigue[s] += cp->fatigue();
         sd.d_disorder[s] += cp->disorder();
         sd.d_disorderLost[s] += (3 - cp->disorder());

         // add front-line hexes that are adjacent to the enemy to fire value
         for(int c = 0; c < cp->columns(); c++)
         {
            int index = maximum(0, (cp->columns() * 0) + c);
            //vector<DeployItem>& map = cp->deployMap();

            DeployItem* di = cp->currentDeployItem(c, 0);//map[index];
            ASSERT(di);
            if(di && di->active())
            {
               enum { Left, Right, LR_HowMany };
               for(int i = 0; i < LR_HowMany; i++)
               {
                  HexCord::HexDirection hd = (i == Left) ? leftFront(cp->facing()) : rightFront(cp->facing());
                  HexCord hex;
                  if(d_batData->moveHex(di->d_sp->hex(), hd, hex))
                  {
                     if(Combat_Util::enemyUnderHex(d_batData, hex, cp))
                     {
                        fireValue++;
                        sd.d_spsInCombat[s]++;

                        // if sp occupies built up terrain(i.e town, village, farm)
                        const BattleTerrainHex& hexInfo1 = d_batData->getTerrain(di->d_sp->hex());
                        const BattleTerrainHex& hexInfo2 = d_batData->getTerrain(hex);
                        if(hexInfo1.d_terrainType == GT_Town ||
                           hexInfo1.d_terrainType == GT_Village ||
                           hexInfo1.d_terrainType == GT_Farm)
                        {
                           sd.d_spsInBuiltUp[s]++;
                        }

                        if( (hexInfo1.d_victoryPoints != 0) ||
                            (hexInfo2.d_victoryPoints != 0 && sd.d_flags[s] & ShockData::Attacking) )
                        {
                           sd.d_flags2[s] |= ShockData::AttackDefendVP;
                        }
                        break;
                     }
                  }
               }
            }
         }

         // modify fire value
         //if(cp->generic()->isInfantry())
         {
            // X 2 Attacking into flank
            if(sd.d_flags[s] & ShockData::IntoEnemyFlank)
               fireValue *= 2;

            //X 1.5 Each arty sp against concentrated
            if(cp->holding() &&
               cp->nArtillery() > 0 &&
               cp->targetList().entries() > 0 &&
               cp->targetList().first()->d_cp->formation() <= CPF_Massed)
            {
               fireValue = (fireValue * 1.5) * minimum(cp->columns(), cp->nArtillery());
            }

            // X .5 if Closed or Square
            if(cp->spFormation() == SP_ClosedColumnFormation || cp->spFormation() == SP_SquareFormation)
               fireValue *= .5;

            // X .5 if shaken
            if(cp->shaken())
               fireValue *= .5;

            // X .5 if artillery is limbered
            if(!cp->holding() && cp->nArtillery() > 0)
               fireValue *= .5;
         }

         sd.d_fireValue[s] += fireValue;
      }

      if(!supported)
         sd.d_flags[s] |= ShockData::Unsupported;
      if(!supportedArty)
         sd.d_flags2[s] |= ShockData::UnsupportedArtillery;
#ifdef DEBUG
      bcLog.printf("%s has %d Fire-points", scenario->getSideName(s), sd.d_fireValue[s]);
#endif

   }

   enum {
      InfVsInf,
      CavVsCav,
      InfVsCav,
      BT_Undefined
   } battleType = BT_Undefined;

   // rewind and add fatigue and calc battle type
   for(s = 0; s < ShockData::NumberSides; s++)
   {
      // add fatigue for each unit
      SideShockBattleListIter iter(&sbi.d_units[s]);
      while(++iter)
      {
         iter.current()->d_cp->addFatigue(2);
      }

      Side otherSide = getOtherSide(s);

      // Inf vs Inf
      if( ((sd.d_flags[s] & ShockData::Inf) && (sd.d_flags[otherSide] & ShockData::Inf)) ||
          ((sd.d_flags[s] & ShockData::Inf) && (sd.d_flags[otherSide] & ShockData::Art)) ||
          ((sd.d_flags[s] & ShockData::Art) && (sd.d_flags[otherSide] & ShockData::Inf)) )
      {
         battleType = InfVsInf;
      }

      // Cav vs Cav
      else if( ((sd.d_flags[s] & ShockData::Cav) && (sd.d_flags[otherSide] & ShockData::Cav)) )
      {
         battleType = CavVsCav;
      }

      // Inf vs Cav
      else
         battleType = InfVsCav;
   }

   ASSERT(battleType != BT_Undefined);
   switch(battleType)
   {
      case InfVsInf:
         infVsInfShockBattle(sd, sbi);
         procPostShockMoraleInfVsInf(sd, sbi);
         procShockUnsupportedArtillery(sd, sbi);
         break;

      case CavVsCav:
         cavVsCavShockBattle(sd, sbi);
         break;

      case InfVsCav:
         infVsCavShockBattle(sd, sbi);
         procShockUnsupportedArtillery(sd, sbi);
         break;
   }

   procRoundWinner(sd, sbi);

   // get next round
   // TODO: get proper interval
// if(!sbi.d_flags & ShockBattleItem::BattleOver)
   {
      const LONG TicksPerMinute = (60 * BattleMeasure::BattleTime::TicksPerSecond);
      const UBYTE s_goIn[BattleOrderInfo::Aggress_HowMany] = {
         32, 16, 8, 4
      };

      UBYTE bestValue = UBYTE_MAX;
      for(Side s = 0; s < scenario->getNumSides(); s++)
      {
         SideShockBattleListIterR iter(&sbi.d_units[s]);
         while(++iter)
         {
            if(s_goIn[iter.current()->d_cp->aggression()] < bestValue)
               bestValue = s_goIn[iter.current()->d_cp->aggression()];
         }

         if(bestValue == 4)
            break;
      }

         // go in 4 miniutes (default, may change)
      sbi.d_nextRound = d_batData->getTick() + (bestValue * TicksPerMinute);
#ifdef DEBUG
      bcLog.printf("Next shock batttle in %d minutes", bestValue);
#endif
   }

   return True;
}

void B_CombatImp::procShockUnsupportedArtillery(ShockData& sd, ShockBattleItem& sbi)
{
   if(sd.d_flags2[0] & ShockData::UnsupportedArtillery ||
      sd.d_flags2[1] & ShockData::UnsupportedArtillery)
   {
      for(Side s = 0; s < 2; s++)
      {
         // go through each unit in list and apply result
         SideShockBattleListIter iter(&sbi.d_units[s]);
         while(++iter)
         {
            RefBattleCP cp = iter.current()->d_cp;
            if(cp->generic()->isArtillery() &&
               cp->nearEnemyMode() < BattleCP::NE_Retreat2 &&
               !Combat_Util::artillerySupported(d_batData, cp))
            {
#ifdef DEBUG
               B_LossUtil::procMoraleTest(d_batgame, cp, B_LossUtil::Trigger_UnsupportedArty, bcLog);
#else
               B_LossUtil::procMoraleTest(d_batgame, cp, B_LossUtil::Trigger_UnsupportedArty);
#endif
            }
         }
      }
   }
}

void B_CombatImp::procRoundWinner(ShockData& sd, ShockBattleItem& sbi)
{
   int dieRoll0 = sd.d_dieRoll[0];
   int dieRoll1 = sd.d_dieRoll[1];
   if(dieRoll0 >= 5 && dieRoll0 - dieRoll1 >= 3)
      sbi.d_winnerLastRound = 0;
   else if(dieRoll1 >= 5 && dieRoll1 - dieRoll0 >= 3)
      sbi.d_winnerLastRound = 1;
   else
      sbi.d_winnerLastRound = SIDE_Neutral;

#ifdef DEBUG
   if(sbi.d_winnerLastRound != SIDE_Neutral)
   {
      bcLog.printf("---- %s wins round (French roll=%d, Allied roll=%d------",
         scenario->getSideName(sbi.d_winnerLastRound), dieRoll0, dieRoll1);
   }
#endif
}

void B_CombatImp::infVsInfShockBattle(ShockData& sd, ShockBattleItem& sbi, bool withModifiers)
{
   for(Side s = 0; s < ShockData::NumberSides; s++)
   {
      const int MaxSPValues = 27;
      const int DieRollValues = 12;
      const int BaseV = 100;

      sd.d_fireValue[s] = minimum<int>(MaxSPValues - 1, sd.d_fireValue[s]);

#ifdef DEBUG
      bcLog.printf("%s is doing Inf vs Inf shock battle", scenario->getSideName(s));
      bcLog.printf("------ Fire value = %d", sd.d_fireValue[s]);
#endif

      const Table2D<UBYTE>& lossTable = BattleTables::infVsInfShockLoss();

      int dieRoll = CRandom::get(1, 10);

#ifdef DEBUG
         bcLog.printf("Unmodified die-roll = %d", dieRoll);
#endif

      // die-roll modifiers
      //--------------------------------------
      Side otherSide = getOtherSide(s);

      if(withModifiers)
      {
         enum {
            EnemyIsShaken,
            ColumnsVsShaken,
            MoraleLessThan,
            FatigueLessThan,
            EnemyInSuperHeavy,
            EnemyInHeavy,
            EnemyInMedium,
            EnemyUphill,
            InMarsh,
            InRough,
            CrossingRiver,
            CrossingSunkenStream,
            CrossingStream,
            AgainstInfCav,
            EnemyUnsupportedArty,
            AttachedCharisma,
            Impact,
            ColumnsVsRetreating,
            EnemyChangingSPFormation,
            EnemyHasArtWeDoNot,
            DefendingInCover,
            DeployedExtended,
            ChangingSPFormation,
            LinesVsColumns,
            LinesVsColumns50,
            ColumnsVsLines,
            Disorder2OrMore,
            ChargingRound1,
            WonLastRoundOff,
            WonLastRoundDef
         };

         const Table1D<SWORD>& mTable = BattleTables::infVsInfDieRollModifiers();

         // +2 if enemy is shaken
         if(sd.d_flags[otherSide] & ShockData::Shaken)
         {
            dieRoll += (sd.d_flags[s] & ShockData::Columns) ? mTable.getValue(ColumnsVsShaken) :
               mTable.getValue(EnemyIsShaken);//2;
         }
#ifdef DEBUG
         bcLog.printf("Die-roll after shaken enemy modifier = %d", dieRoll);
#endif

         // -1 for each 50 points of morale below enemy
         ASSERT(sbi.d_units[s].entries() > 0);
         ASSERT(sbi.d_units[otherSide].entries() > 0);

         // Note: morale is averaged by number of units in the action
         UWORD ourMorale = sd.d_morale[s] / sbi.d_units[s].entries();
         UWORD theirMorale = sd.d_morale[otherSide] / sbi.d_units[otherSide].entries();

         int dif = theirMorale - ourMorale;
         if(dif > 0 && mTable.getValue(MoraleLessThan) > 0)
            dieRoll -= minimum(4, dif / mTable.getValue(MoraleLessThan));

#ifdef DEBUG
         bcLog.printf("Die-roll after morale comparison modifier = %d", dieRoll);
#endif

         // -1 for each 50 points of fatigue below enemy
         // Note: morale is averaged by number of units in the action
         UWORD ourFatigue = sd.d_fatigue[s] / sbi.d_units[s].entries();
         UWORD theirFatigue = sd.d_fatigue[otherSide] / sbi.d_units[otherSide].entries();

         dif = theirFatigue - ourFatigue;
         if(dif > 0 && mTable.getValue(FatigueLessThan) > 0)
            dieRoll -= minimum(4, dif / mTable.getValue(FatigueLessThan));

#ifdef DEBUG
         bcLog.printf("Die-roll after fatigue comparison modifier = %d", dieRoll);
#endif

         UWORD ourImpact = sd.d_impact[s] / sbi.d_units[s].entries();
         UWORD theirImpact = sd.d_impact[otherSide] / sbi.d_units[otherSide].entries();

         dif = ourImpact - theirImpact;
         if(dif > 0 && mTable.getValue(Impact) > 0)
            dieRoll += dif / mTable.getValue(Impact);

#ifdef DEBUG
         bcLog.printf("Die-roll after impact comparison modifier = %d", dieRoll);
#endif

         // modifiers for enemy in cover
         // -1 if medium cover
         // -2 Heavy cover
         if(sd.d_flags[otherSide] & ShockData::InSuperHeavyCover)
            dieRoll += mTable.getValue(EnemyInSuperHeavy);//3;
         else if(sd.d_flags[otherSide] & ShockData::InHeavyCover)
            dieRoll += mTable.getValue(EnemyInHeavy);//-= 2;
         else if(sd.d_flags[otherSide] & ShockData::InMediumCover)
            dieRoll += mTable.getValue(EnemyInMedium);

#ifdef DEBUG
         bcLog.printf("Die-roll after enemy cover modifier = %d", dieRoll);
#endif

         if(sd.d_flags[s] & ShockData::Defending &&
             (sd.d_flags[s] & ShockData::InSuperHeavyCover ||
              sd.d_flags[s] & ShockData::InHeavyCover ||
              sd.d_flags[s] & ShockData::InMediumCover) )
         {
            dieRoll += mTable.getValue(DefendingInCover);//3;
#ifdef DEBUG
            bcLog.printf("Die-roll after defending in cover modifier = %d", dieRoll);
#endif
         }


         // -1 if enemy is uphill
         if(sd.d_elevation[otherSide] >= sd.d_elevation[s] + 5)
            dieRoll += mTable.getValue(EnemyUphill);

         // if attacking from rough / Marsh
         if(sd.d_flags2[s] & ShockData::InMarsh)
            dieRoll += mTable.getValue(InMarsh);//-= 2;
         else if(sd.d_flags2[s] & ShockData::InRough)
            dieRoll += mTable.getValue(InRough);//--;

         // for rivers/streams
         if(sd.d_flags2[s] & ShockData::CrossingRiver)
            dieRoll += mTable.getValue(CrossingRiver);//-= 3;
         else if(sd.d_flags2[s] & ShockData::CrossingSunkenStream)
            dieRoll += mTable.getValue(CrossingSunkenStream);//-= 2;
         else if(sd.d_flags2[s] & ShockData::CrossingStream)
            dieRoll += mTable.getValue(CrossingStream);//--;

#ifdef DEBUG
         bcLog.printf("Die-roll after terrain modifiers = %d", dieRoll);
#endif

         // -1 in action against Inf and Cav
         if(sd.d_flags[otherSide] & ShockData::Inf &&
            sd.d_flags[otherSide] & ShockData::Cav &&
            !(sd.d_flags[s] & ShockData::Squared || sd.d_flags[s] & ShockData::ClosedColumns) )
         {
            dieRoll += mTable.getValue(AgainstInfCav);
#ifdef DEBUG
            bcLog.printf("Die-roll after against Inf-Cav modifier = %d", dieRoll);
#endif
         }

         if(sd.d_flags2[otherSide] & ShockData::UnsupportedArtillery)
         {
            dieRoll += mTable.getValue(EnemyUnsupportedArty);
#ifdef DEBUG
            bcLog.printf("Die-roll after unsupported enemy arty modifier = %d", dieRoll);
#endif
         }

         if(sd.d_attachedCharisma[s] > 0)
         {
            int theirCharisma = (sd.d_attachedCharisma[otherSide] > 0) ?
               sd.d_attachedCharisma[otherSide] : sd.d_charisma[otherSide] / sbi.d_units[otherSide].entries();

            if(sd.d_attachedCharisma[s] > theirCharisma && mTable.getValue(AttachedCharisma) > 0)
            {
               int dif = sd.d_attachedCharisma[s] - theirCharisma;
               dieRoll += dif / mTable.getValue(AttachedCharisma);
#ifdef DEBUG
               bcLog.printf("Die-roll after attached leader charisma modifier = %d", dieRoll);
#endif
            }
         }

         if(sd.d_flags[s] & ShockData::Columns && sd.d_flags[otherSide] & ShockData::Retreating)
         {
            dieRoll += mTable.getValue(ColumnsVsRetreating);
#ifdef DEBUG
            bcLog.printf("Die-roll after columns vs retreating modifier = %d", dieRoll);
#endif
         }

         if(sd.d_flags[otherSide] & ShockData::ChangingSPFormation)
         {
            dieRoll += mTable.getValue(EnemyChangingSPFormation);
#ifdef DEBUG
            bcLog.printf("Die-roll after enemy changin formation modifier = %d", dieRoll);
#endif
         }

         if(sd.d_flags[s] & ShockData::ChangingSPFormation)
         {
            dieRoll += mTable.getValue(ChangingSPFormation);
#ifdef DEBUG
            bcLog.printf("Die-roll after changin formation modifier = %d", dieRoll);
#endif
         }

         if(!(sd.d_flags[s] & ShockData::HasArtillery) &&
             (sd.d_flags[otherSide] & ShockData::HasArtillery) )
         {
            dieRoll += mTable.getValue(EnemyHasArtWeDoNot);
#ifdef DEBUG
            bcLog.printf("Die-roll after enemy has arty, we do not modifier = %d", dieRoll);
#endif
         }

         if( (sd.d_flags[s] & ShockData::Deployed || sd.d_flags[s] & ShockData::Extended) &&
             !(sd.d_flags[s] & ShockData::Shaken) )
         {
            dieRoll += mTable.getValue(DeployedExtended);
#ifdef DEBUG
            bcLog.printf("Die-roll after deployed/extended modifier = %d", dieRoll);
#endif
         }

         if( !(sd.d_flags[s] & ShockData::Shaken) &&
              (sd.d_flags[s] & ShockData::Lines) &&
              (sd.d_flags[otherSide] & ShockData::Columns) )
         {
            dieRoll += (sd.d_fireValue[s] >= sd.d_fireValue[otherSide] * 1.5) ?
               mTable.getValue(LinesVsColumns50) : mTable.getValue(LinesVsColumns);
#ifdef DEBUG
            bcLog.printf("Die-roll after lines vs columns modifier = %d", dieRoll);
#endif
         }

         if( !(sd.d_flags[otherSide] & ShockData::Shaken) &&
              (sd.d_flags[otherSide] & ShockData::Lines) &&
              (sd.d_flags[s] & ShockData::Columns) )
         {
            dieRoll += mTable.getValue(ColumnsVsLines);
#ifdef DEBUG
            bcLog.printf("Die-roll after columns vs lines modifier = %d", dieRoll);
#endif
         }

         UWORD ourDisorder = sd.d_disorder[s] / sbi.d_units[s].entries();
         UWORD theirDisorder = sd.d_disorder[otherSide] / sbi.d_units[otherSide].entries();
         if(ourDisorder - theirDisorder >= 2)
         {
            dieRoll += mTable.getValue(Disorder2OrMore);
#ifdef DEBUG
            bcLog.printf("Die-roll after disorder comparison modifier = %d", dieRoll);
#endif
         }

         if(sbi.d_round == 1 &&
            sd.d_flags[s] & ShockData::Attacking)
         {
            dieRoll += mTable.getValue(ChargingRound1);
#ifdef DEBUG
            bcLog.printf("Die-roll after charging round 1 modifier = %d", dieRoll);
#endif
         }

         if(sbi.d_winnerLastRound == s)
         {
            dieRoll += mTable.getValue((sd.d_flags[s] & ShockData::Attacking) ? WonLastRoundOff : WonLastRoundDef);
#ifdef DEBUG
            bcLog.printf("Die-roll after won last round modifier = %d", dieRoll);
#endif
         }

         dieRoll = clipValue(dieRoll, 0, 11);
         sd.d_dieRoll[s] = dieRoll;
      }

#ifdef DEBUG
      bcLog.printf("Final die-roll = %d", dieRoll);
#endif

      // process buildings on fire
      if(sd.d_flags[otherSide] & ShockData::InMediumCover ||
         sd.d_flags[otherSide] & ShockData::InHeavyCover ||
         sd.d_flags[otherSide] & ShockData::InSuperHeavyCover)
      {
         procShockBuildings(s, sd, sbi);
      }

      ASSERT(dieRoll < DieRollValues);
      ASSERT(sd.d_fireValue[s] < MaxSPValues);

      sd.d_losses[otherSide] = lossTable.getValue(sd.d_fireValue[s], dieRoll);//s_loss[sd.d_fireValue[s]][dieRoll];
      UINT losses = doShockLosses(sbi.d_units[otherSide], sd.d_losses[otherSide]);

#ifdef DEBUG
      bcLog.printf("%s losses total %d.%d SP (%d%% loss)",
         scenario->getSideName(getOtherSide(s)),
         losses / 100, (losses % 100) / 10, lossTable.getValue(sd.d_fireValue[s], dieRoll));
#endif
   }

}

struct InfVsInfOutcomeResult {
   BattleCP::NearEnemyMode d_nPullBack; // pull back how many hexes
    bool  d_chanceToPullBack;
    UBYTE d_moraleLost;
    UBYTE d_disorderLost;
    UBYTE d_aggressionLost;
    bool  d_chanceForDisorderLoss;
    bool  d_chanceForAggressIncrease;
    bool  d_aggressTest;
    bool  d_postureTest;
    bool  d_rout;
    bool  d_toDefensive;
    bool  d_routTest;

};

void B_CombatImp::procPostShockMoraleInfVsInf(ShockData& sd, ShockBattleItem& sbi)
{
#ifdef DEBUG
    bcLog.printf("-------Processing Post Shock Morale Test");
#endif
   // This is processed in the following priority
   // loser > def posture vs off posture > who scored least hits > lowest current morale
   Side startSide = SIDE_Neutral;
   int end;
   int inc;

   if( (sd.d_losses[0] >= (2 * sd.d_losses[1])) )
      startSide = 0;

   else if( (sd.d_losses[1] >= (2 * sd.d_losses[0])) )
      startSide = 1;

   else
   {
      if(sd.d_posture[0] == BattleOrderInfo::Offensive && sd.d_posture[1] == BattleOrderInfo::Defensive)
         startSide = 0;
      else if(sd.d_posture[1] == BattleOrderInfo::Offensive && sd.d_posture[0] == BattleOrderInfo::Defensive)
         startSide = 1;
      else
      {
         if(sd.d_losses[0] > sd.d_losses[1])
            startSide = 0;
         else if(sd.d_losses[1] > sd.d_losses[0])
            startSide = 1;
         else
            startSide = (CRandom::get(100) % 2 == 0) ? 0 : 1;
      }
   }

   ASSERT(startSide != SIDE_Neutral);
   if(startSide == 0)
   {
      end = ShockData::NumberSides;
      inc = 1;
   }
   else
   {
      end = -1;
      inc = -1;
   }

   for(int s = startSide; s != end; s += inc)
   {
      Side otherSide = getOtherSide(s);

      ASSERT(sbi.d_units[s].entries() > 0);
      ASSERT(sbi.d_units[otherSide].entries() > 0);
      // Note: morale is averaged by number of units in the action
      UWORD ourMorale = sd.d_morale[s] / sbi.d_units[s].entries();
      UWORD theirMorale = sd.d_morale[otherSide] / sbi.d_units[otherSide].entries();
      UWORD ourFatigue = sd.d_fatigue[s] / sbi.d_units[s].entries();
      UWORD theirFatigue = sd.d_fatigue[otherSide] / sbi.d_units[otherSide].entries();

      // Unit does not need to take test if
      // 1. It suffered less than 3% loss this round
      // 2. AND fatigue and morale are over 65
      // 3. If it inflicted 3X the losses on enemy and #2
      if( ( (ourMorale > 65) &&
            (ourFatigue > 65) &&
            (sd.d_losses[s] < 3) ) ||
            (sd.d_losses[s] >= (3 * sd.d_losses[otherSide])) )
      {
#ifdef DEBUG
         bcLog.printf("%s does not need to test", scenario->getSideName(s));
#endif
         continue;
      }

      UWORD ourCharisma = sd.d_charisma[s] / sbi.d_units[s].entries();
      UWORD theirCharisma = sd.d_charisma[otherSide] / sbi.d_units[otherSide].entries();
      UWORD ourAggression = sd.d_aggression[s] / sbi.d_units[s].entries();
      UWORD theirAggression = sd.d_aggression[otherSide] / sbi.d_units[otherSide].entries();
      int ourLoss = sd.d_losses[s];
      int theirLoss = sd.d_losses[otherSide];
#ifdef DEBUG
      bcLog.printf("%s is taking test", scenario->getSideName(s));
#endif

      // get raw die roll of between 2 - 20
      int dieRoll = CRandom::get(2, 20);

#ifdef DEBUG
      bcLog.printf("Raw die-roll = %d", dieRoll);
#endif

      enum {
         CharismaOver200,
         CharismaOver190,
         CharismaOver170,
         CharismaOver145,
         Morale25,
         MaxAggression,
         UpHill,
         Disorder,
         SPMarch,
         SPInBuiltUp,
         EnemyFallenBack,
         EnemyRetired,
         EnemyRetreated,
         EnemyRouted,
         LossDoubleEnemies,
         FatigueLess90,
         FatigueLess65,
         AtCriticalLoss,
         Losses25,
         Unsupported,
         LineExtended,
         Aggress0,
         LostRound,
         SPColumns,
         AttackDefendVP,
      };
      const Table1D<SWORD>& mTable = BattleTables::infVsInfMoraleDieRollModifiers();

      // for leader charisma
      if(ourCharisma >= 200)
         dieRoll += mTable.getValue(CharismaOver200);//4;
      else if(ourCharisma >= 190)
         dieRoll += mTable.getValue(CharismaOver190);//3;
      else if(ourCharisma >= 170)
         dieRoll += mTable.getValue(CharismaOver170);
      else if(ourCharisma >= 145)
         dieRoll += mTable.getValue(CharismaOver145);

#ifdef DEBUG
      bcLog.printf("Die-roll after charisma modifier = %d", dieRoll);
#endif

      // +1 for each 25 of morale
      if(mTable.getValue(Morale25) > 0)
         dieRoll += (ourMorale / mTable.getValue(Morale25));
#ifdef DEBUG
      bcLog.printf("Die-roll after morale modifier = %d", dieRoll);
#endif

      // +1 if current aggression == 3
      if(ourAggression == 3)
         dieRoll += mTable.getValue(MaxAggression);

#ifdef DEBUG
      bcLog.printf("Die-roll after aggression modifier = %d", dieRoll);
#endif

      // +1 if uphill of enemy
      if(sd.d_elevation[s] >= sd.d_elevation[otherSide] + 5)
         dieRoll += mTable.getValue(UpHill);

#ifdef DEBUG
      bcLog.printf("Die-roll after uphill modifier = %d", dieRoll);
#endif

      // -1 for each disorder below 3
      dieRoll -= ((-mTable.getValue(Disorder)) * sd.d_disorderLost[s]);

#ifdef DEBUG
      bcLog.printf("Die-roll after disorder modifier = %d", dieRoll);
#endif

      // -1 if in March
      if(sd.d_flags[s] & ShockData::SPMarch)
         dieRoll += mTable.getValue(SPMarch);
      else if(  (sd.d_flags[s] & ShockData::Columns) &&
               !(sd.d_flags[s] & ShockData::CriticalLossReached) )
      {
         dieRoll += mTable.getValue(SPColumns);
      }
#ifdef DEBUG
      bcLog.printf("Die-roll after 'March' modifier = %d", dieRoll);
#endif

      // +1 for each front-line SP in a building hex
      dieRoll += (mTable.getValue(SPInBuiltUp) * sd.d_spsInBuiltUp[s]);

#ifdef DEBUG
      bcLog.printf("Die-roll after SPs in built-up terrain modifier = %d", dieRoll);
#endif

      // for nationality on defensive posture
      // TODO: move table to scenario tables
      const int nNations = 14;
      static const UBYTE s_nationDefTable[nNations] = {
      // F, P, A, S, R, D, I, B, SX, M, M, M, M
          0, 0, 0, 0, 1, 0, 0, 0,  0, 0, 0, 0, 0
      };

      dieRoll += (s_nationDefTable[sd.d_nation[s]] * ourAggression);

#ifdef DEBUG
    bcLog.printf("Die-roll after nation defense modifier = %d", dieRoll);
#endif

      // +1 if enemy has fallen back
      if(sd.d_flags[otherSide] & ShockData::Result_FallenBack)
         dieRoll += mTable.getValue(EnemyFallenBack);//++;
      // +2 if enemy has retired
      else if(sd.d_flags[otherSide] & ShockData::Result_Retired)
         dieRoll += mTable.getValue(EnemyRetired);//2;
      // +3 if enemy has retreated
      else if(sd.d_flags[otherSide] & ShockData::Result_Retreated)
         dieRoll += mTable.getValue(EnemyRetreated);//3;
      // +2 if enemy has retired
      else if(sd.d_flags[otherSide] & ShockData::Result_Routing)
         dieRoll += mTable.getValue(EnemyRouted);

#ifdef DEBUG
      bcLog.printf("Die-roll after enemy result modifier = %d", dieRoll);
#endif

      // TODO: remaining modifiers

      // -1 if losses percent is double enemies
      if(sd.d_losses[s] >= (2 * sd.d_losses[otherSide]))
         dieRoll += mTable.getValue(LossDoubleEnemies); //-= 1;

      // -1 if fatigue is less than 90
      if(ourFatigue <= 90)
         dieRoll += mTable.getValue(FatigueLess90);//-= 1;
      else if(ourFatigue <= 65)
         dieRoll += mTable.getValue(FatigueLess65);//-= 2;

      // -2 if at critical loss
      if(sd.d_flags2[s] & ShockData::CriticalLossReached)
         dieRoll += mTable.getValue(AtCriticalLoss); //-= 2;

      // -2 if suffered 25% or greater losses
      if(sd.d_losses[s] >= 25)
         dieRoll += mTable.getValue(Losses25);//-= 2;

      // Unsupported -1
      if(sd.d_flags[s] & ShockData::Unsupported)
         dieRoll += mTable.getValue(Unsupported);//-= 1;

      if(ourMorale <= 110 && (sd.d_flags[s] & ShockData::Extended || sd.d_flags[s] & ShockData::Lines))
         dieRoll += mTable.getValue(LineExtended);//-= 1;

      if(ourAggression == 0)
         dieRoll += mTable.getValue(Aggress0);

      if(ourLoss >= theirLoss * 2)
         dieRoll += mTable.getValue(LostRound);

      if(sd.d_flags2[s] & ShockData::AttackDefendVP)
         dieRoll += mTable.getValue(AttackDefendVP);

      enum {
         Renew,
         OK,
         Hold,
         FallBack1,
         FallBack2,
         FallBack3,
         Retire,
         Retreat1,
         Retreat2,
         Rout,

         OC_HowMany
      } outcome = (dieRoll >= 22) ? Renew :
                        (dieRoll >= 17) ? OK :
                        (dieRoll >= 13) ? Hold :
                        (dieRoll == 12) ? FallBack1 :
                        (dieRoll == 11) ? FallBack2 :
                        (dieRoll == 10) ? FallBack3 :
                        (dieRoll >= 7)  ? Retire :
                        (dieRoll >= 4)  ? Retreat1 :
                        (dieRoll >= 1)  ? Retreat2 : Rout;

      // process outcome

      // TODO: move table to scenario tables
      const static InfVsInfOutcomeResult s_outcome[OC_HowMany] = {
         // pb,                     ctpb,   ml, dl,  al,   cdl,   cfai,  at,    pt,     r,    d,     rt
         { BattleCP::NE_Default,   False,  0,   0,   0,  False,  True,  False, False, False, False, False }, // Renew
         { BattleCP::NE_Default,   False,  5,   0,   0,  True,   False, False, False, False, False, False }, // OK
         { BattleCP::NE_Default,   False,  5,   1,   0,  False,  False, True,  False, False, False, False }, // Hold
         { BattleCP::NE_KeepAway2, True,  10,   1,   0,  False,  False, True,  False, False, False, False }, // Retreat1
         { BattleCP::NE_KeepAway2, True,  10,   1,   0,  False,  False, True,  False, False, False, False }, // Retreat2
         { BattleCP::NE_Retreat2,  True,  10,   1,   0,  False,  False, True,  False, False, False, False }, // Retreat3
         { BattleCP::NE_Retreat2,  False, 10,   2,   1,  False,  False, False, True,  False, False, False }, // Retire
         { BattleCP::NE_Retreat4,  False, 10,   2,   1,  False,  False, False, True,  False, False, False }, // Retreat1
         { BattleCP::NE_Retreat4,  False, 20,   3,   2,  False,  False, False, False, False, True,  True  }, // Retreat2
         { BattleCP::NE_Retreat6,  False, 20,   3,   2,  False,  False, False, False, True,  True,  False }  // Rout
      };


      switch(outcome)
      {
         case Renew:
#ifdef DEBUG
            bcLog.printf("Outcome -- Renew");
#endif
         break;

         case OK:
#ifdef DEBUG
             bcLog.printf("Outcome -- OK");
#endif
             break;

         case Hold:
#ifdef DEBUG
             bcLog.printf("Outcome -- Hold");
#endif
             break;

         case FallBack1:
         sd.d_flags[s] |= ShockData::Result_FallenBack;
#ifdef DEBUG
             bcLog.printf("Outcome -- FallBack1");
#endif
             break;

         case FallBack2:
             sd.d_flags[s] |= ShockData::Result_FallenBack;
#ifdef DEBUG
             bcLog.printf("Outcome -- FallBack2");
#endif
         break;

         case FallBack3:
             sd.d_flags[s] |= ShockData::Result_FallenBack;
#ifdef DEBUG
             bcLog.printf("Outcome -- FallBack3");
#endif
         break;

         case Retire:
             sd.d_flags[s] |= ShockData::Result_Retired;
#ifdef DEBUG
             bcLog.printf("Outcome -- Retire");
#endif
             break;

         case Retreat1:
         sd.d_flags[s] |= ShockData::Result_Retreated;
#ifdef DEBUG
             bcLog.printf("Outcome -- Retreat1");
#endif
             break;

         case Retreat2:
             sd.d_flags[s] |= ShockData::Result_Retreated;
#ifdef DEBUG
         bcLog.printf("Outcome -- Retreat2");
#endif
         break;

         case Rout:
             sd.d_flags[s] |= ShockData::Result_Routing;
#ifdef DEBUG
             bcLog.printf("Outcome -- Rout");
#endif
         break;
      }

      const InfVsInfOutcomeResult& or = s_outcome[outcome];

      // go through each unit in list and apply result
      SideShockBattleListIter iter(&sbi.d_units[s]);
      while(++iter)
      {
         RefBattleCP cp = iter.current()->d_cp;
         RefBattleCP attachedLeader = BobUtility::getAttachedLeader(d_batData, cp);
         RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();

         // remove any morale
         if(or.d_moraleLost > 0)
             cp->removeMorale(or.d_moraleLost);

      // remove any disorder
         if(or.d_disorderLost > 0)
             cp->disorder(static_cast<UBYTE>(maximum(0, cp->disorder() - or.d_disorderLost)));

      // remove any aggression
         if(or.d_aggressionLost > 0)
            {
               int loop = or.d_aggressionLost;
                while(loop--)
                {
                  cp->decreaseAggression();
                }
//          cp->aggression(static_cast<BattleOrderInfo::Aggression>(maximum(0, cp->aggression() - or.d_aggressionLost)), SLONG_MAX);
            }

         // change to defensive posture
         if(or.d_toDefensive)
            {
               cp->setDefensive();
//          cp->posture(BattleOrderInfo::Defensive, SLONG_MAX);
         }
         // aggression increase
         if(or.d_chanceForAggressIncrease)
         {
             // a 10% chance of increasing
             if(cp->aggression() < BattleOrderInfo::Aggress_HowMany - 1 &&
                  CRandom::get(100) <= 10)
             {
                  cp->increaseAggression();
//              cp->aggression(static_cast<BattleOrderInfo::Aggression>(cp->aggression() + 1), SLONG_MAX);
             }
         }

         // aggression test
         if(or.d_aggressTest)
         {
#ifdef DEBUG
            bcLog.printf("%s is taking aggression test", cp->getName());
#endif
            // leader (attached leader or actual) aggression as a % of 255
            int aggress = leader->getAggression();

            // if in line and morale is lower than 110 mult aggression by .8
            if(cp->morale() <= 110 && cp->spFormation() == SP_LineFormation)
               aggress = (aggress * 8) / 10;

            int ldrPercent = (aggress * 100) / Attribute_Range;

            // chance is halved it belowe critical loss
            if(cp->criticalLossReached())
               ldrPercent /= 2;

            if(CRandom::get(100) <= ldrPercent)
            {
               // aggression drops 1
                    cp->decreaseAggression();
//             cp->aggression(static_cast<BattleOrderInfo::Aggression>(maximum(0, cp->aggression() - 1)), SLONG_MAX);
#ifdef DEBUG
               bcLog.printf("%s drops 1 aggression point", cp->getName());
#endif
            }
         }

         // posture change test
         if(or.d_toDefensive && cp->posture() == BattleOrderInfo::Offensive)
         {
#ifdef DEBUG
            bcLog.printf("%s is taking posture test", cp->getName());
#endif
            // leader (attached leader or actual) aggression as a % of 255
            int ldrPercent = (leader->getAggression() * 100) / Attribute_Range;
            if(CRandom::get(100) <= ldrPercent)
            {
               // aggression drops 1
//             cp->posture(BattleOrderInfo::Defensive, SLONG_MAX);
               cp->setDefensive();
#ifdef DEBUG
               bcLog.printf("%s changes to defensive posture", cp->getName());
#endif
            }
         }

         // rout test
         if(or.d_routTest)
         {
#ifdef DEBUG
            B_LossUtil::procRoutTest(d_batgame, cp, bcLog);
#else
            B_LossUtil::procRoutTest(d_batgame, cp);
#endif
         }

         // pull back,
         if(or.d_nPullBack >= BattleCP::NE_KeepAway2)
         {
                if(or.d_nPullBack > cp->nearEnemyMode())
                {
                     cp->nearEnemyMode(or.d_nPullBack);
                    cp->lockMode();
                }

            sbi.d_flags |= ShockBattleItem::BattleOver;
         }
      }

   }
}

void B_CombatImp::cavVsCavShockBattle(ShockData& sd, ShockBattleItem& sbi)
{
    // get a die roll from each side
    // we then compare dieRolls to reach a result
   for(Side s = 0; s < ShockData::NumberSides; s++)
    {
#ifdef DEBUG
      bcLog.printf("%s is doing Cav vs Cav shock battle", scenario->getSideName(s));
#endif

      Side otherSide = getOtherSide(s);
      UWORD ourMorale = sd.d_morale[s] / sbi.d_units[s].entries();
      UWORD theirMorale = sd.d_morale[otherSide] / sbi.d_units[otherSide].entries();
      UWORD ourFatigue = sd.d_fatigue[s] / sbi.d_units[s].entries();
      UWORD theirFatigue = sd.d_fatigue[otherSide] / sbi.d_units[otherSide].entries();
      UWORD ourImpact = sd.d_impact[s] / sbi.d_units[s].entries();
      UWORD theirImpact = sd.d_impact[otherSide] / sbi.d_units[otherSide].entries();

      // roll a die between 1 - 10
      int dieRoll = CRandom::get(1, 10);

#ifdef DEBUG
    bcLog.printf("Raw die roll = %d", dieRoll);
#endif

      // modifiers----------------------
      enum {
         AttachedLeaderCharisma,
         MoraleGreater,
         ImpactGreater,
         FatigueGreater,
         OtherSideCriticalLoss,
         HasCavSpecialist,
         LinesVsColumns,
         InMarsh,
         InRough,
         CrossingRiver,
         CrossingStream,
         CrossingSunkenStream,
         EnemyShaken,
         DeployedVsMassed,
         HasHorseArty,
         HigherGround,
         EnemyChangingFormation,
         ChargingRound1,
         WonLastRound
      };

      const Table1D<SWORD>& mTable = BattleTables::cavVsCavDieRollModifiers();

      // +1 for each 50 attached leader charisma > enemy
      int dif = maximum(0, sd.d_attachedCharisma[s] - sd.d_attachedCharisma[otherSide]);
      if(dif > 0 && mTable.getValue(AttachedLeaderCharisma) > 0)
         dieRoll += (dif / mTable.getValue(AttachedLeaderCharisma));

#ifdef DEBUG
      bcLog.printf("Die roll after attached charisma modifier = %d", dieRoll);
#endif

      // +1 for each 20 morale is higher
      dif = maximum(0, ourMorale - theirMorale);
      if(dif > 0 && mTable.getValue(MoraleGreater) > 0)
         dieRoll += (dif / mTable.getValue(MoraleGreater));

#ifdef DEBUG
      bcLog.printf("Die roll after morale modifier = %d", dieRoll);
#endif

      // +1 for each 40 impact higher
      dif = maximum(0, ourImpact - theirImpact);
      if(dif > 0 && mTable.getValue(ImpactGreater) > 0)
         dieRoll += ((dif / mTable.getValue(ImpactGreater)) + 1);

#ifdef DEBUG
      bcLog.printf("Die roll after impact modifier = %d", dieRoll);
#endif

    // +1 for each 50 fatigue is higher
      dif = maximum(0, ourFatigue - theirFatigue);
      if(dif > 0  && mTable.getValue(FatigueGreater) > 0)
         dieRoll += (dif / mTable.getValue(FatigueGreater));

#ifdef DEBUG
      bcLog.printf("Die roll after fatigue = %d", dieRoll);
#endif

      // enemy is at critical loss +2
      if(sd.d_flags2[otherSide] & ShockData::CriticalLossReached)
         dieRoll += mTable.getValue(OtherSideCriticalLoss);

#ifdef DEBUG
      bcLog.printf("Die roll after enemy at critical loss = %d", dieRoll);
#endif

      // +1 if we have a cav specialist
      if(sd.d_flags2[s] & ShockData::HasCavSpecialist)
         dieRoll += mTable.getValue(HasCavSpecialist);

#ifdef DEBUG
      bcLog.printf("Die roll after specialist modifier = %d", dieRoll);
#endif

      // +2 for lines vs columns
      if(sd.d_flags[s] & ShockData::Lines && sd.d_flags[s] & ShockData::Columns)
         dieRoll += mTable.getValue(LinesVsColumns);

#ifdef DEBUG
      bcLog.printf("Die roll after lines vs columns = %d", dieRoll);
#endif

      // if attacking from rough / Marsh
      if(sd.d_flags2[s] & ShockData::InMarsh)
         dieRoll += mTable.getValue(InMarsh); //2;
      else if(sd.d_flags2[s] & ShockData::InRough)
         dieRoll += mTable.getValue(InRough);

      // for rivers/streams
      if(sd.d_flags2[s] & ShockData::CrossingRiver)
         dieRoll += mTable.getValue(CrossingRiver);//3;
      else if(sd.d_flags2[s] & ShockData::CrossingSunkenStream)
         dieRoll += mTable.getValue(CrossingSunkenStream);//2;
      else if(sd.d_flags2[s] & ShockData::CrossingStream)
         dieRoll += mTable.getValue(CrossingStream);

#ifdef DEBUG
      bcLog.printf("Die roll after terrain modifiers = %d", dieRoll);
#endif

      // TODO: +1 unit is in puruit mode

      // +1 for each disorder level higher than enemy
      dieRoll += maximum(0, sd.d_disorderLost[s] - sd.d_disorderLost[otherSide]);

#ifdef DEBUG
      bcLog.printf("Die roll after disorder modifier = %d", dieRoll);
#endif

    // +4 if enemy is shaken
    if(sd.d_flags[otherSide] & ShockData::Shaken)
      dieRoll += mTable.getValue(EnemyShaken);//4;

#ifdef DEBUG
    bcLog.printf("Die roll after shaken modifier = %d", dieRoll);
#endif

    // +1 deployed vs massed or extended
    if(sd.d_flags[s] & ShockData::Deployed &&
          (sd.d_flags[otherSide] & ShockData::Massed || sd.d_flags[otherSide] & ShockData::Extended))
    {
      dieRoll += mTable.getValue(DeployedVsMassed);
    }

#ifdef DEBUG
    bcLog.printf("Die roll after 'deployed vs massed' modifier = %d", dieRoll);
#endif

      // +1 has attached horse artillery
      if(sd.d_flags[s] & ShockData::HasHorseArtillery)
         dieRoll += mTable.getValue(HasHorseArty);

#ifdef DEBUG
    bcLog.printf("Die roll after horse artillery modifier = %d", dieRoll);
#endif

    // +1 if on higher elevation
      if(sd.d_elevation[s]  > sd.d_elevation[otherSide] + 5)
      dieRoll += mTable.getValue(HigherGround);

#ifdef DEBUG
      bcLog.printf("Die roll after elevation modifier = %d", dieRoll);
#endif

    // +1 enemy is changing formation
    if(sd.d_flags[otherSide] & ShockData::ChangingSPFormation)
      dieRoll += mTable.getValue(EnemyChangingFormation);

#ifdef DEBUG
    bcLog.printf("Die roll after enemy changing formation modifier = %d", dieRoll);
#endif

      if(sbi.d_round == 1 &&
         sd.d_flags[s] & ShockData::Attacking)
      {
         dieRoll += mTable.getValue(ChargingRound1);
#ifdef DEBUG
         bcLog.printf("Die-roll after charging round 1 modifier = %d", dieRoll);
#endif
      }

      if(sbi.d_winnerLastRound == s)
      {
         dieRoll += mTable.getValue(WonLastRound);
#ifdef DEBUG
         bcLog.printf("Die-roll after won last round modifier = %d", dieRoll);
#endif
      }

      #ifdef DEBUG
    bcLog.printf("Raw final roll = %d", dieRoll);
#endif
      sd.d_dieRoll[s] = dieRoll;
    }

   int dif = abValue(sd.d_dieRoll[0] - sd.d_dieRoll[1]);
  CavVsCavOutcome outcome = (dif == 0) ? Tie :
                            (dif == 1) ? WinningTie :
                                          (dif <= 3) ? Win1 :
                            (dif <= 5) ? Win2 :
                            (dif <= 7) ? Win3 :
                                          (dif <= 9) ? Win4 : Win5;

   procCavVsCavOutcome(sd, sbi, outcome);
}

struct CavVsCavResult {
   UWORD  d_moraleLoss;
   UWORD  d_aggressLoss;
   UWORD  d_disorderLoss;
   float  d_spMult;
   bool   d_recall;
   bool   d_rout;
   bool   d_moraleCheck;
   bool   d_disorderCheck;
   bool   d_trophyCheck;
   bool   d_artyMustEscape;
   bool   d_pursuitTest;
};

void B_CombatImp::procCavVsCavOutcome(ShockData& sd, ShockBattleItem& sbi, CavVsCavOutcome outcome)
{
   Side winningSide = SIDE_Neutral;
   if(outcome != Tie)
    winningSide = (sd.d_dieRoll[0] > sd.d_dieRoll[1]) ? 0 : 1;

   Side losingSide = (winningSide != SIDE_Neutral) ? getOtherSide(winningSide) : winningSide;

   // TODO: move table to scenario tables
    static const CavVsCavResult s_table[ShockData::NumberSides][CVC_HowMany] = {
    // Winner
    {
      // ML, AL, DL, SPM, Rcll,  Rt,   Mc,    Dc,    tc,    ame    pt
         {  0, 0, 0,   1, False, False, False, True,  False, False, False },   // Tie
         {  0, 1, 0,   1, False, False, False, True,  False, False, False },   // Winning Tie
         {  0, 0, 0, 1.5, False, False, False, True,  False, False, False },   // Win1
         {  0, 0, 0, 1.5, False, False, False, False, False, False, False },   // Win2
         {  0, 0, 0,   2, False, False, False, False, False, False, False },   // Win3
         {  0, 0, 0,   2, False, False, False, False, True,  False, True  },   // Win4
         {  0, 0, 0,   2, False, False, False, False, True,  False, True  }    // Win5
      },
   // Loser
      {
         {  0, 0, 0,   1, False, False, False, True,  False, False, False },   // Tie
         {  0, 1, 0,   1, False, False, False, True,  False, False, False },   // Winning Tie
         { 10, 0, 0,   1, False, False, False, True,  False, False, False },   // Win1
         { 20, 1, 1,  .5, True,  False, False, False, False, False, False },   // Win2
         { 30, 1, 1,  .5, True,  False, False, False, False, False, False },   // Win3
         { 40, 1, 1, .25, True,  False,  True,  False, False, True,  False },   // Win4
         { 50, 2, 2, .25, False, True,  False, False, False, True,  False }    // Win5
      }
   };

   for(Side s = 0; s < ShockData::NumberSides; s++)
   {
      ASSERT(outcome < CVC_HowMany);
      CavVsCavResult result = s_table[(s == winningSide) ? 0 : 1][outcome];
#ifdef DEBUG
      bcLog.printf("Processing CavVsCav outcome for %s", scenario->getSideName(s));
      bcLog.printf("------- Morale Loss = %d", static_cast<int>(result.d_moraleLoss));
      bcLog.printf("------- Aggression Loss = %d", static_cast<int>(result.d_aggressLoss));
      bcLog.printf("------- Disorder Loss = %d", static_cast<int>(result.d_disorderLoss));
      bcLog.printf("------- SP Mult = %f", result.d_spMult);
      bcLog.printf("------- Recall = %s", (result.d_recall) ? "True" : "False");
      bcLog.printf("------- Rout = %s", (result.d_rout) ? "True" : "False");
      bcLog.printf("------- Morale Check = %s", (result.d_moraleCheck) ? "True" : "False");
      bcLog.printf("------- Disorder Check = %s", (result.d_disorderCheck) ? "True" : "False");
      bcLog.printf("------- Trophy Check = %s", (result.d_trophyCheck) ? "True" : "False");
      bcLog.printf("------- Arty Must Escape = %s", (result.d_artyMustEscape) ? "True" : "False");
#endif

      // modify fireValue
      if(result.d_spMult != 1)
         sd.d_fireValue[s] *= result.d_spMult;

      // go through each unit for this side an process result
      SideShockBattleListIterR iter(&sbi.d_units[s]);
      while(++iter)
      {
         const RefBattleCP& cp = iter.current()->d_cp;

#ifdef DEBUG
         bcLog.printf("Processing result for %s", cp->getName());
#endif

         RefBattleCP attachedLeader = BobUtility::getAttachedLeader(d_batData, cp);
         RefGLeader leader = (attachedLeader != NoBattleCP) ?
         attachedLeader->leader() : cp->leader();

         // proc morale loss
         if(result.d_moraleLoss > 0)
            cp->removeMorale(static_cast<Attribute>(result.d_moraleLoss));

         // proc aggression loss
         if(result.d_aggressLoss > 0)
            {
               int loop = result.d_aggressLoss;
                while(loop--)
                  cp->decreaseAggression();
//          cp->aggression(static_cast<BattleOrderInfo::Aggression>(maximum(0, cp->aggression() - result.d_aggressLoss)), SLONG_MAX);
            }

      // proc disorder loss
         if(result.d_disorderLoss > 0)
            cp->disorder(static_cast<UBYTE>(maximum(0, cp->disorder() - result.d_disorderLoss)));

         if(result.d_moraleCheck)
         {
            // percent chance to pass test is morale as a percent of 255
            int percent = cp->moralePercent();

            // modify by
            if(leader->getCharisma() > 230)
                percent *= 1.4;
            else if(leader->getCharisma() > 190)
                percent *= 1.3;
            else if(leader->getCharisma() > 160)
                percent *= 1.2;
            else if(leader->getCharisma() > 130)
                percent *= 1.1;

            // -20 if at critical loss
            if(cp->criticalLossReached())
               percent -= 20;
            if(Combat_Util::nonRoutingFriendsNear(d_batData, cp, 4))
               percent -= 10;

            // if we fail test we recall, otherwise we rout
            if(CRandom::get(100) > percent)
            {
                result.d_recall = True;
                result.d_rout = False;
            }
            else
            {
               result.d_recall = True;
               result.d_rout = True;
            }
         }

         // proc recall
         if(result.d_recall)
             doRecall(cp, leader, sbi);

         // proc rout
         if(result.d_rout)
         {
            B_LossUtil::routUnit(d_batgame, cp);
         }
         // proc Disorder check
         if(result.d_disorderCheck)
         {
            // chance for not losing disorder is unit control + Tact ability / 4
            int chance = (BobUtility::unitControl(d_batData->ob(), cp) + leader->getTactical()) / 4;

            // modify
            // X .5 if unit lost to a Win2 result
            if(s == losingSide && outcome == Win2)
               chance *= .5;
            else if(s == losingSide && outcome == Win3)
             chance *= .33;

            else if(s == winningSide && outcome > WinningTie)
               chance *= 1.1;

            // X 1.1 if cav specialist
            if(cp->leader()->isSpecialistType(Specialist::Cavalry))
               chance *= 1.1;

            if(CRandom::get(100) > chance)
            {
               static const UBYTE s_lossDis[CPF_Last] = {
                   1, 1, 2, 3
               };

               cp->disorder(static_cast<UBYTE>(maximum(0, cp->disorder() - s_lossDis[cp->formation()])));
            }

         }

         // proc Trophy Check
         if(result.d_trophyCheck)
            trophyTest(cp, sbi.d_units[losingSide]);

         // proc Arty Must Escape
         if(result.d_artyMustEscape)
         {
             // all horse Arty SP must attemp to escape
            SideShockBattleListIter iter(&sbi.d_units[s]);
            while(++iter)
            {
//             RefBattleSP sp = iter.current()->d_cp->sp();
//             while(sp != NoBattleSP)
               for(BattleSPIter spIter(d_batData->ob(), cp, BSPI_ModeDefs::ArtilleryOnly);
                     !spIter.isFinished();
                     spIter.next())
               {
                  // chance is 70 -20 if shaken
                  int chance = (cp->shaken()) ? 50 : 70;
                  if(CRandom::get(100) > chance)
                  {
                     spIter.sp()->strength(0);
#ifdef DEBUG
                     bcLog.printf("SP %ld (%s) did not escaped. Strength = 0",
                           reinterpret_cast<LONG>(spIter.sp()), d_batData->ob()->spName(spIter.sp()));
#endif
                  }
//                sp = sp->sister();
               }
            }
         }

         // pursuit test
      if(result.d_pursuitTest)
         pursuitTest(cp, leader);
      }
    }

   // Process losses. This uses the InfVsInfCombat routine  (without modifiers)
   infVsInfShockBattle(sd, sbi, False);
}

void B_CombatImp::infVsCavShockBattle(ShockData& sd, ShockBattleItem& sbi)
{
   int infDieRoll = 0;
   int cavDieRoll = 0;

   // get a die roll from each side
   // we then compare dieRolls to reach a result
   for(Side s = 0; s < ShockData::NumberSides; s++)
   {
#ifdef DEBUG
      bcLog.printf("%s (%s)is doing Inf vs Cav shock battle",
      scenario->getSideName(s), (sd.d_flags[s] & ShockData::Cav) ? "Cavalry" : "Infantry");
#endif

      Side otherSide = getOtherSide(s);
      UWORD ourMorale = sd.d_morale[s] / sbi.d_units[s].entries();
      UWORD theirMorale = sd.d_morale[otherSide] / sbi.d_units[otherSide].entries();
      UWORD ourFatigue = sd.d_fatigue[s] / sbi.d_units[s].entries();
      UWORD theirFatigue = sd.d_fatigue[otherSide] / sbi.d_units[otherSide].entries();
      UWORD ourImpact = sd.d_impact[s] / sbi.d_units[s].entries();
      UWORD theirImpact = sd.d_impact[otherSide] / sbi.d_units[otherSide].entries();
      UWORD ourDisorder = sd.d_disorder[s] / sbi.d_units[s].entries();
      UWORD theirDisorder = sd.d_disorder[otherSide] / sbi.d_units[otherSide].entries();
      UWORD ourCharisma = sd.d_charisma[s] / sbi.d_units[s].entries();
      UWORD theirCharisma = sd.d_charisma[otherSide] / sbi.d_units[otherSide].entries();

      // roll a die between 1 - 10
      int dieRoll = CRandom::get(1, 10);

#ifdef DEBUG
      bcLog.printf("Raw die roll = %d", dieRoll);
#endif

      // modifiers----------------------
      if(sd.d_flags[s] & ShockData::Cav)
      {
         enum {
            ForMorale,
            ImpactNotSquared,
            ImpactSquared,
            Charisma,
            CavSpecialist,
            Disorder,
            ColumnsAgainstInf,
            AgainstExtended,
            AgainstUnsupportedArt,
            IntoFlank,
            IntoFlankClosed,
            HasArt,
            AgainstMarch,
            FatigueGreater50,
            InMarsh,
            InRough,
            CrossingRiver,
            CrossingSunkenStream,
            CrossingStream,
            LinesAgainstArt,
            WonLastRound
         };
         const Table1D<SWORD>& mTable = BattleTables::infVsCavCavDieRollModifiers();

         // +1 for each 20 of morale
         if(mTable.getValue(ForMorale) > 0)
            dieRoll += (ourMorale / mTable.getValue(ForMorale));
#ifdef DEBUG
         bcLog.printf("Die roll after morale modifier = %d", dieRoll);
#endif

         // +1 for 40 impact, unless...
         if( (!(sd.d_flags[otherSide] & ShockData::Squared) || (sd.d_flags[otherSide] & ShockData::Shaken)) &&
               (!(sd.d_flags[otherSide] & ShockData::ClosedColumns) || (sd.d_flags[otherSide] & ShockData::Shaken || sd.d_disorder[otherSide] == 0)) )
         {
            if(mTable.getValue(ImpactNotSquared) > 0)
               dieRoll += (ourImpact / mTable.getValue(ImpactNotSquared));
         }
         // +1 for each 80 impact unless...
         else
         {
            if(mTable.getValue(ImpactSquared) > 0)
               dieRoll += (ourImpact / mTable.getValue(ImpactSquared));
         }

#ifdef DEBUG
         bcLog.printf("Die roll after impact modifier = %d", dieRoll);
#endif

         // +1 for each 40 charisma higher than infantry
         int dif = maximum(0, ourCharisma - theirCharisma);
         if(dif > 0 && mTable.getValue(Charisma) > 0)
            dieRoll += (dif / mTable.getValue(Charisma));
#ifdef DEBUG
         bcLog.printf("Die roll after charisma modifier = %d", dieRoll);
#endif

         // +1 for a corps or better cav spec
         if(sd.d_flags[s] & ShockData::CorpCavSpecialist)
             dieRoll += mTable.getValue(CavSpecialist);//++;
#ifdef DEBUG
         bcLog.printf("Die roll after Corp Cav Specialist modifier = %d", dieRoll);
#endif

         // +1 for each disorder level higher than inf
         dieRoll += (mTable.getValue(Disorder) * maximum(0, ourDisorder - theirDisorder));
#ifdef DEBUG
         bcLog.printf("Die roll after disorder modifier = %d", dieRoll);
#endif

         // +1 Cav in Column against inf, or line against art
         if(sd.d_flags[s] & ShockData::Columns && sd.d_flags[otherSide] & ShockData::Inf)
         {
             dieRoll += mTable.getValue(ColumnsAgainstInf);
#ifdef DEBUG
             bcLog.printf("Die roll after sp formations modifier = %d", dieRoll);
#endif
         }

         if(sd.d_flags[s] & ShockData::Lines && sd.d_flags[otherSide] & ShockData::Art)
         {
             dieRoll += mTable.getValue(LinesAgainstArt);
#ifdef DEBUG
             bcLog.printf("Die roll after sp formations modifier = %d", dieRoll);
#endif
         }

         // +2 if infantry are extended
         if(sd.d_flags[otherSide] & ShockData::Extended)
         {
             dieRoll += mTable.getValue(AgainstExtended);//2;
#ifdef DEBUG
             bcLog.printf("Die roll after against extended modifier = %d", dieRoll);
#endif
         }

         // TODO: -2 for  Inf against blown Cav
         // TODO: -1 for raining or -2 for raining against cc or squares

         // +2 attacking unsupprted artillery
         if(sd.d_flags2[otherSide] & ShockData::UnsupportedArtillery &&
            sd.d_flags[otherSide] & ShockData::Art)
         {
             dieRoll += mTable.getValue(AgainstUnsupportedArt);//2;
#ifdef DEBUG
             bcLog.printf("Die roll after against unsupported artillery modifier = %d", dieRoll);
#endif
         }

         // +3 for attacking into flank
         if(  (sd.d_flags[s] & ShockData::IntoEnemyFlank) &&
             !(sd.d_flags[otherSide] & ShockData::Squared) )
         {
             dieRoll += (sd.d_flags[otherSide] & ShockData::ClosedColumns) ?
               mTable.getValue(IntoFlankClosed) : mTable.getValue(IntoFlank);//3;
#ifdef DEBUG
             bcLog.printf("Die roll after into enemy flank modifier = %d", dieRoll);
#endif
         }

         // +1 for has horse arty attached
         if(sd.d_flags[s] & ShockData::HasHorseArtillery)
         {
             dieRoll += mTable.getValue(HasArt);//++;
#ifdef DEBUG
             bcLog.printf("Die roll after has attached artillery modifier = %d", dieRoll);
#endif
         }

         // +3 attacking in march
         if(sd.d_flags[otherSide] & ShockData::SPMarch)
         {
            dieRoll += mTable.getValue(AgainstMarch);//3;
#ifdef DEBUG
            bcLog.printf("Die roll after attacking march modifier = %d", dieRoll);
#endif
         }

         // +1 if at least 50 more fatigue
         if(ourFatigue >= theirFatigue + 50)
         {
            dieRoll += mTable.getValue(FatigueGreater50);//++;
#ifdef DEBUG
            bcLog.printf("Die roll after fatigue modifier = %d", dieRoll);
#endif
         }

         // if attacking from rough / Marsh
         if(sd.d_flags2[s] & ShockData::InMarsh)
            dieRoll += mTable.getValue(InMarsh); //-= 2;
         else if(sd.d_flags2[s] & ShockData::InRough)
            dieRoll += mTable.getValue(InRough);//--;

         // for rivers/streams
         if(sd.d_flags2[s] & ShockData::CrossingRiver)
            dieRoll += mTable.getValue(CrossingRiver); //-= 3;
         else if(sd.d_flags2[s] & ShockData::CrossingSunkenStream)
            dieRoll += mTable.getValue(CrossingSunkenStream);//-= 2;
         else if(sd.d_flags2[s] & ShockData::CrossingStream)
            dieRoll += mTable.getValue(CrossingStream);//--;

#ifdef DEBUG
         bcLog.printf("Die roll after terrain modifiers = %d", dieRoll);
#endif

         if(sbi.d_winnerLastRound == s)
         {
            dieRoll += mTable.getValue(WonLastRound);
#ifdef DEBUG
            bcLog.printf("Die-roll after won last round modifier = %d", dieRoll);
#endif
         }

      #ifdef DEBUG
         bcLog.printf("Final Cavalry die-roll = %d", dieRoll);
#endif
         cavDieRoll = dieRoll;
      }
      else
      {
         ASSERT(sd.d_flags[s] & ShockData::Inf || sd.d_flags[s] & ShockData::Art);

         const Table1D<SWORD>& mTable = BattleTables::infVsCavInfDieRollModifiers();
         enum {
            MoraleSquared,
            MoraleClosed,
            MoraleOther,
            Impact,
            DisorderHigher,
            HasArt,
            FatigueGreater50,
            HasCav,
            InMarsh,
            InRough,
            CrossingRiver,
            CrossingSunkenStream,
            CrossingStream,
            Charisma,
            WonLastRound
         };

         // for morale
         enum {
             V10, V15, V20, V_HowMany
         } value = (sd.d_flags[s] & ShockData::Squared && !(sd.d_flags[s] & ShockData::Shaken)) ? V10 :
                        (sd.d_flags[s] & ShockData::ClosedColumns && !(sd.d_flags[s] & ShockData::Shaken)) ? V15 : V20;

         static const UBYTE s_index[V_HowMany] = {
            MoraleSquared, MoraleClosed, MoraleOther
         };

         if(mTable.getValue(s_index[value]) > 0)
            dieRoll += ourMorale / mTable.getValue(s_index[value]);//mEvery[value];
#ifdef DEBUG
         bcLog.printf("Die roll after morale modifier = %d (+1 for every %d morale",
               dieRoll, static_cast<int>(mTable.getValue(s_index[value])));
#endif

         // each 40 impact, unless shaken
         if( !(sd.d_flags[s] & ShockData::Shaken) && mTable.getValue(Impact) > 0)
             dieRoll += ourImpact / mTable.getValue(Impact);//40;
#ifdef DEBUG
         bcLog.printf("Die roll after impact modifier = %d", dieRoll);
#endif

         // each 40 impact, unless shaken
         int dif = ourCharisma - theirCharisma;
         if(dif > 0 && mTable.getValue(Charisma) > 0)
             dieRoll += dif / mTable.getValue(Charisma);//40;
#ifdef DEBUG
         bcLog.printf("Die roll after charisma modifier = %d", dieRoll);
#endif

         // +1 for each disorder higher than enemy
         dieRoll += (mTable.getValue(DisorderHigher) * maximum(0, ourDisorder - theirDisorder));

#ifdef DEBUG
         bcLog.printf("Die roll after disorder modifier = %d", dieRoll);
#endif

         // + 1 for has artillery
         if(sd.d_flags[s] & ShockData::HasArtillery)
             dieRoll += mTable.getValue(HasArt);//++;
#ifdef DEBUG
         bcLog.printf("Die roll after artillery modifier = %d", dieRoll);
#endif

         // +1 if 50 more fatigue
         if(ourFatigue >= theirFatigue + 50)
            dieRoll += mTable.getValue(FatigueGreater50);//++;
#ifdef DEBUG
         bcLog.printf("Die roll after artillery modifier = %d", dieRoll);
#endif

         // +1 if attached cav
         if(sd.d_flags[s] & ShockData::HasCavAttached)
            dieRoll += mTable.getValue(HasCav);//++;
#ifdef DEBUG
         bcLog.printf("Die roll after attached cav modifier = %d", dieRoll);
#endif

         // if attacking from rough / Marsh
         if(sd.d_flags2[s] & ShockData::InMarsh)
            dieRoll += mTable.getValue(InMarsh); //-= 2;
         else if(sd.d_flags2[s] & ShockData::InRough)
            dieRoll += mTable.getValue(InRough);//--;

         // for rivers/streams
         if(sd.d_flags2[s] & ShockData::CrossingRiver)
            dieRoll += mTable.getValue(CrossingRiver); //-= 3;
         else if(sd.d_flags2[s] & ShockData::CrossingSunkenStream)
            dieRoll += mTable.getValue(CrossingSunkenStream);//-= 2;
         else if(sd.d_flags2[s] & ShockData::CrossingStream)
            dieRoll += mTable.getValue(CrossingStream);//--;

#ifdef DEBUG
         bcLog.printf("Die roll after terrain modifiers = %d", dieRoll);
#endif

         if(sbi.d_winnerLastRound == s)
         {
            dieRoll += mTable.getValue(WonLastRound);
#ifdef DEBUG
            bcLog.printf("Die-roll after won last round modifier = %d", dieRoll);
#endif
         }

         infDieRoll = dieRoll;
      }

      sd.d_dieRoll[s] = dieRoll;
   }

    // subract inf from cav die roll to get out come
    int dif = cavDieRoll - infDieRoll;
#ifdef DEBUG
    bcLog.printf("Cav-Inf difference = %d", dif);
#endif
    InfVsCavOutcome outcome = (dif <= -10) ? CavLoss10 :
                                           (dif <= -8)  ? CavLoss8 :
                                           (dif <= -4)  ? CavLoss4 :
                                           (dif <= -3)  ? CavLoss3 :
                                           (dif <= -1)  ? CavLoss1 :
                                           (dif == 0)   ? InfVsCavTie :
                                           (dif == 1)   ? InfLoss1 :
                                           (dif <= 3)   ? InfLoss3 :
                                           (dif <= 5)   ? InfLoss5 :
                                           (dif <= 9)   ? InfLoss9 : InfLoss10;

   procInfVsCavOutcome(sd, sbi, outcome);
}

struct InfVsCavResult {
    UBYTE d_chanceToInflict;
    UBYTE d_losses;
     UBYTE d_moraleLoss;
    UBYTE d_aggressLoss;
     UBYTE d_disorderLoss;

     bool  d_aggressCheck;
     bool  d_toDefPos;
    bool  d_recall;
     bool  d_rout;
     bool  d_pursuitTest;
    bool  d_trophyTest;
};

void B_CombatImp::procInfVsCavOutcome(ShockData& sd, ShockBattleItem& sbi, InfVsCavOutcome outcome)
{
   Side cavSide = (sd.d_flags[0] & ShockData::Cav) ? 0 : 1;
   Side infSide = (cavSide == 0) ? 1 : 0;

   for(Side s = 0; s < scenario->getNumSides(); s++)
   {
      Side otherSide = getOtherSide(s);

      const int nValues = 2;
      static const InfVsCavResult result[nValues][IVC_HowMany] = {
      // Cav
         {//cti  osl, ml, al, dl, ac,   tdp,   rc,    rt     pt
            {  0,  0, 10,  1,  1, False, False, False, True,  False, False }, // CavLoss10
            {  0,  0, 10,  0,  1, True,  False, True,  False, False, False }, // CavLoss8
            {  0,  0,  5,  0,  1, True,  False, True,  False, False, False }, // CavLoss4
            { 10,  1,  0,  0,  1, False, False, False, False, False, False }, // CavLoss3
            { 10,  1,  0,  0,  1, False, False, False, False, False, False }, // CavLoss1
            { 10,  1,  0,  0,  1, False, False, False, False, False, False }, // InfVsCavTie
            { 20,  1,  0,  0,  0, False, False, False, False, False, False }, // InfLoss1
            { 25,  1,  0,  0,  0, False, False, False, False, True,  False }, // InfLoss3
            { 25,  5,  0,  0,  0, False, False, False, False, True,  False }, // InfLoss5
            { 50,  5,  0,  0,  0, False, False, False, False, True,  True  }, // InfLoss9
            { 50, 10,  0,  0,  0, False, False, False, False, True,  True  }  // InfLoss10
         },
      // Inf
         {
            { 50,  1,  0,  0,  0, False, False, False, False, False, False }, // CavLoss10
            { 40,  1,  0,  0,  0, False, False, False, False, False, False }, // CavLoss8
            { 30,  1,  0,  0,  0, False, False, False, False, False, False }, // CavLoss4
            { 20,  1,  0,  0,  0, True,  False, False, False, False, False }, // CavLoss3
            { 20,  1,  0,  0,  0, True,  False, False, False, False, False }, // CavLoss1
            { 20,  1,  0,  0,  1, True,  False, False, False, False, False }, // InfVsCavTie
            { 15,  1,  5,  1,  1, False, True,  False, False, False, False }, // InfLoss1
            { 10,  1, 10,  2,  2, False, True,  True,  False, False, False }, // InfLoss3
            {  5,  1, 20,  2,  2, False, True,  False, True,  False, False }, // InfLoss5
            {  0,  0, 30,  2,  2, False, True,  False, True,  False, False }, // InfLoss9
            {  0,  0, 50,  2,  2, False, True,  False, True,  False, False }  // InfLoss10
         }
      };

      InfVsCavResult rs = result[(s == cavSide) ? 0 : 1][outcome];
      const Table2D<UBYTE>& lossTable = BattleTables::infVsCavShockLoss();
      const Table2D<UBYTE>& lossChanceTable = BattleTables::infVsCavChanceToInflictShockLoss();

#ifdef DEBUG
      bcLog.printf("Processing InfVsCav outcome for %s (%s)",
      scenario->getSideName(s), (s == cavSide) ? "Cav" : "Inf");
      bcLog.printf("----------- Chance to inflict casualties = %d%%", static_cast<int>(lossChanceTable.getValue((s == cavSide) ? 0 : 1, outcome)));
      bcLog.printf("----------- Losses  = %d.%d%%", static_cast<int>(lossTable.getValue((s == cavSide) ? 0 : 1, outcome) / 10), static_cast<int>(lossTable.getValue((s == cavSide) ? 0 : 1, outcome) % 10));
      bcLog.printf("----------- Morale loss = %d", static_cast<int>(rs.d_moraleLoss));
      bcLog.printf("----------- Aggress loss = %d", static_cast<int>(rs.d_aggressLoss));
      bcLog.printf("----------- Disorder loss = %d", static_cast<int>(rs.d_disorderLoss));
      bcLog.printf("----------- Aggression Test = %s", (rs.d_aggressCheck) ? "True" : "False");
      bcLog.printf("----------- To def posture = %s", (rs.d_toDefPos) ? "True" : "False");
      bcLog.printf("----------- Recall \\ Retreat = %s", (rs.d_recall) ? "True" : "False");
      bcLog.printf("----------- Rout = %s", (rs.d_rout) ? "True" : "False");
      bcLog.printf("----------- Pursuit Test = %s", (rs.d_pursuitTest) ? "True" : "False");
#endif

      SideShockBattleListIter iter(&sbi.d_units[s]);
      while(++iter)
      {
         RefBattleCP cp = iter.current()->d_cp;
         RefBattleCP attachedLeader = BobUtility::getAttachedLeader(d_batData, cp);
         RefGLeader leader = (attachedLeader != NoBattleCP) ?
         attachedLeader->leader() : cp->leader();

         // apply losses to other side
//       if(rs.d_chanceToInflict > 0)
         UBYTE chanceToInflict = lossChanceTable.getValue((s == cavSide) ? 0 : 1, outcome);
         if(chanceToInflict)
         {
            int losses = 0;
            // Each SP in combat have a % chance to inflict SP strength loss

            int chances = sd.d_spsInCombat[s];//minimum(cp->columns(), sd.d_fireValue[s]);
            // End
            //          int lPercent = rs.d_losses;
            int loss = lossTable.getValue((s == cavSide) ? 0 : 1, outcome);//rs.d_losses;
            while(chances--)
            {
               if(CRandom::get(100) < chanceToInflict)
                  losses += loss;
            }

            // apply losses to other side
            sd.d_losses[otherSide] += losses;
         }

         // process morale loss
         if(rs.d_moraleLoss > 0)
             cp->removeMorale(rs.d_moraleLoss);

         // process disorder loss
         if(rs.d_disorderLoss > 0)
            cp->disorder(static_cast<UBYTE>(maximum(0, cp->disorder() - rs.d_disorderLoss)));

         // process aggression loss
         if(rs.d_aggressLoss > 0)
         {
            int loop = rs.d_aggressLoss;
            while(loop--)
               cp->decreaseAggression();
         // cp->aggression(static_cast<BattleOrderInfo::Aggression>(maximum(0, cp->aggression() - rs.d_aggressLoss)), SLONG_MAX);
         }

         // aggression check
         if(rs.d_aggressCheck)
         {
            // chance of not dropping 1 aggression level is leader aggression as a % 255
            int chance = (leader->getAggression() * 100) / Attribute_Range;
            if(CRandom::get(100) >= chance)
               cp->decreaseAggression();
//             cp->aggression(static_cast<BattleOrderInfo::Aggression>(maximum(0, cp->aggression() - 1)), SLONG_MAX);
         }

      // to def posture
         if(rs.d_toDefPos)
            cp->setDefensive();
          //   cp->posture(BattleOrderInfo::Defensive, SLONG_MAX);

         // recall
         if(rs.d_recall)
            doRecall(cp, leader, sbi);

         // rout
         if(rs.d_rout)
         {
            B_LossUtil::routUnit(d_batgame, cp);
         }

         // pursuit test
         if(rs.d_pursuitTest)
            pursuitTest(cp, leader);

         if(rs.d_trophyTest)
            trophyTest(cp, sbi.d_units[otherSide]);
      }
   }

    // rewind and process actual losses
   for(s = 0; s < scenario->getNumSides(); s++)
   {
      if(sd.d_losses[s] > 0)
      {
#ifdef DEBUG
         bcLog.printf("%s is taking %d%% losses",
            scenario->getSideName(s), static_cast<int>(sd.d_losses[s]));
#endif
         doShockLosses(sbi.d_units[s], sd.d_losses[s]);
      }
   }
}

void B_CombatImp::pursuitTest(const RefBattleCP& cp, const RefGLeader& leader)
{
#ifdef DEBUG
    bcLog.printf("%s is taking pursuit test", cp->getName());
#endif
   if(cp->aggressionLocked() || cp->postureLocked())
      return;
        
   // does cav 'lose its head' and go off in pursuit?
   // chance for NOT puruing is unit control + ta / 4
    int chance = (BobUtility::unitControl(d_batData->ob(), cp) + leader->getTactical()) / 4;

    // modify
   if(cp->posture() == BattleOrderInfo::Offensive)
      chance *= .6;
    else
      chance *= 1.2;

   if(CRandom::get(100) > chance)
   {
#ifdef DEBUG
      bcLog.printf("Fails test. Will pursue");
#endif
//    // TODO set pursue flag in CP
//    while(cp->aggression() < 3)
//          cp->increaseAggression();
      cp->aggression(static_cast<BattleOrderInfo::Aggression>(3));
      cp->posture(BattleOrderInfo::Offensive);
    }
}

void B_CombatImp::doRecall(const RefBattleCP& cp, const RefGLeader& leader, ShockBattleItem& sbi)
{
    static const BattleCP::NearEnemyMode aggTable[BattleOrderInfo::Aggress_HowMany] = {
          BattleCP::NE_Retreat5,
          BattleCP::NE_Retreat4,
          BattleCP::NE_Retreat3,
          BattleCP::NE_Retreat2
    };

    // if leader is not a cavalry specialist, we lose 1 disorder
    if(cp->generic()->isCavalry() &&
         (cp->leader()->isSpecialistType(Specialist::Cavalry) || leader->isSpecialistType(Specialist::Cavalry)) )
    {
    }
    else
       cp->increaseDisorder();

    int leaderTAPercent = (leader->getTactical() * 100) / Attribute_Range;
    if(CRandom::get(100) > leaderTAPercent)
       cp->increaseDisorder();

     if(aggTable[cp->aggression()] > cp->nearEnemyMode())
     {
       cp->nearEnemyMode(aggTable[cp->aggression()]);
         cp->lockMode();
     }

    sbi.d_flags |= ShockBattleItem::BattleOver;
}

void B_CombatImp::trophyTest(const RefBattleCP& cp, SideShockBattleList& list)
{
    // a 3% chance per enemy SP of capturing a trophy
    // +5 morale for each trophy

    UWORD add = 0;

    SideShockBattleListIter iter(&list);
    while(++iter)
    {
       for(BattleSPIter spIter(d_batData->ob(), iter.current()->d_cp, BSPI_ModeDefs::All);
             !spIter.isFinished();
             spIter.next())
       {
          if(CRandom::get(100) <= 3)
          {
             add += 5;

             // TODO: get this from resource
             const int nTrophyTypes = 3;
             static const TCHAR* s_txt[nTrophyTypes] = {
                  "Standard",
                  "Eagle",
                  "Battalion Commander"
             };
             int i = CRandom::get(nTrophyTypes);
             ASSERT(i < nTrophyTypes);

             // send player message
             {
                  // our side
                  BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_CapturedTrophy, cp);
                  msg.s1(s_txt[i]);
                  msg.target(iter.current()->d_cp);
                  d_batgame->sendPlayerMessage(msg);
             }
             {
                  // their side
                  BattleMessageInfo msg(iter.current()->d_cp->getSide(), BattleMessageID::BMSG_EnemyCapturedTrophy, iter.current()->d_cp);
                  msg.s1(s_txt[i]);
                  d_batgame->sendPlayerMessage(msg);
             }
         }
//       sp = sp->sister();
      }
    }

    if(add > 0)
    {
#ifdef DEBUG
         bcLog.printf("Adding %d morale to %s for capturing trophy(s)",
         static_cast<UWORD>(add), cp->getName());
#endif
      cp->addMorale(static_cast<Attribute>(add));

         // if disorder = 0 or 1 add 1
      if(cp->disorder() <= 1)
         cp->decreaseDisorder();

      // if shaken, unshaken
    cp->shaken(False);

    }
}

#if 0
inline void clearBattleFlags(BattleUnit* bu)
{
//   bu->takingGunHits(False);
//   bu->takingMusketHits(False);
   bu->shootingGuns(False);
   bu->shootingMuskets(False);
}

inline void clearTakingHitsFlag(BattleUnit* bu)
{
   bu->takingGunHits(False);
   bu->takingMusketHits(False);
}
#endif

bool isMoving(const RefBattleCP& cp)
{
      bool stillMoving = False;
      for(std::vector<DeployItem>::const_iterator di = cp->mapBegin();
          di != cp->mapEnd();
          ++di)
      {
         if( (di->active()) && (di->d_sp->isMoving()) )
         {
            stillMoving = True;
                  break;
         }
      }

      return stillMoving;
}

void clearTakingGuns(const RefBattleCP& cp)
{
#ifdef DEBUG
    bcLog.printf("Clearing takingGunHits flag for %s", cp->getName());
#endif
    for(std::vector<DeployItem>::iterator di = cp->mapBegin();
        di != cp->mapEnd();
        di++)
    {
      if(di->active())
             di->d_sp->takingGunHits(False);
    }

    cp->takingGunHits(False);
}

void clearTakingMuskets(const RefBattleCP& cp)
{
#ifdef DEBUG
    bcLog.printf("Clearing takingMusketHits flag for %s", cp->getName());
#endif
    for(std::vector<DeployItem>::iterator di = cp->mapBegin();
            di != cp->mapEnd();
        di++)
    {
      if(di->active())
         di->d_sp->takingMusketHits(False);
    }

    cp->takingMusketHits(False);
}

void clearShootingMuskets(const RefBattleCP& cp)
{
#ifdef DEBUG
    bcLog.printf("Clearing shootingMuskets flag for %s", cp->getName());
#endif
    for(std::vector<DeployItem>::iterator di = cp->mapBegin();
            di != cp->mapEnd();
            di++)
    {
      if(di->active())
         di->d_sp->shootingMuskets(False);
      }

      cp->shootingMuskets(False);
}

void clearShootingGuns(const RefBattleCP& cp)
{
#ifdef DEBUG
      bcLog.printf("Clearing shootingGuns flag for %s", cp->getName());
#endif
      for(std::vector<DeployItem>::iterator di = cp->mapBegin();
         di != cp->mapEnd();
            di++)
      {
         if(di->active())
         di->d_sp->shootingGuns(False);
      }

      cp->shootingGuns(False);
}

void B_CombatImp::clearFlags(const RefBattleCP& cp)
{
    // if no one is within Musket Range, clear those flags
    int nHexes = (cp->generic()->isCavalry()) ? 1 : 2;
    int closeRange = cp->targetList().closeRangeFront();
    int fireRange = Combat_Util::getRange(d_batData, cp);
    if(cp->shootingMuskets())
    {
       if(closeRange > (nHexes * BattleMeasure::XYardsPerHex))
            clearShootingMuskets(cp);
    }

    // if no one is within arty range, or we are moving
    if(cp->shootingGuns() && (isMoving(cp) || closeRange > fireRange) )
       clearShootingGuns(cp);

    // now the difficult part. Find out whether anyone is shooting at us
    if(cp->takingMusketHits() || cp->takingGunHits())
    {
         bool takingMuskets = False;
         bool takingGuns = False;
         for(BattleUnitIter iter(d_batData->ob(), (cp->getSide() == 0) ? 1 : 0); !iter.isFinished(); iter.next())
         {
            if(iter.cp()->getRank().sameRank(Rank_Division))
            {
                  BattleItem* bi = iter.cp()->targetList().find(cp);
                  if(bi)
                  {
                      if(bi->d_position == BattleItem::Front && bi->d_fireValue > 0)
                      {
                           if(bi->d_range <= (2 * BattleMeasure::XYardsPerHex))
                              takingMuskets = True;

                           if( (cp->takingGunHits()) &&
                               (iter.cp()->shootingGuns()) &&
                               (iter.cp()->generic()->isArtillery() || iter.cp()->nArtillery() > 0) &&
                               (!isMoving(iter.cp())) &&
                               (bi->d_range <= Combat_Util::getRange(d_batData, iter.cp())) )
                           {
                               takingGuns = True;
                           }
                      }
                  }

                  if(takingGuns && takingMuskets)
                     break;
             }
         }

         if(cp->takingMusketHits() && !takingMuskets)
            clearTakingMuskets(cp);

      if(cp->takingGunHits() && !takingGuns)
        clearTakingGuns(cp);
   }
}

bool B_CombatImp::canCharge(const RefBattleCP& cp)
{
    ASSERT(cp->targetList().closeRangeFront() <= (1 * BattleMeasure::XYardsPerHex));

#ifdef DEBUG
   bcLog.printf("-------------------------------------------");
   bcLog.printf("------- Testing %s for shock combat", cp->getName());
#endif

   // we will not charge if shaken or routing
   if(cp->nearEnemyMode() >= BattleCP::NE_KeepAway2 ||
     cp->movingBackwards() ||
       cp->shaken() ||
       cp->routing() ||
       cp->fleeingTheField())
   {
#ifdef DEBUG
      bcLog.printf("%s cannot charge, NearEnemyMode = %s",
         cp->getName(), BattleCP::nearEnemyName(cp->nearEnemyMode()));
#endif
      return False;
   }


   // ------------------------------------------
   // Automatic charge

   // TODO: this unit is in pursuit mode and has not reached Critical Loss Level

   RefBattleCP bestTarget = Combat_Util::bestShockTarget(cp);
   if(bestTarget == NoBattleCP)
   {
#ifdef DEBUG
      bcLog.printf("No unit close enough for melee!");
#endif
         return False;
   }

   enum OutCome {
      ChargeWithElan,
      Charge1,
      Charge2,
      Wait,
      Balk
   } outCome = Wait;

   if( (bestTarget->shaken()) ||
         (bestTarget->routing()) ||
         (bestTarget->disorder() == 0) ||
         (bestTarget->formation() == CPF_March) ||
         (bestTarget->spFormation() == SP_MarchFormation) )
   {
#ifdef DEBUG
      bcLog.printf("Auto charging");
#endif
      outCome = ChargeWithElan;
   }

    // If not auto charge, then do test
   if(outCome == Wait)
   {
      ASSERT(bestTarget != NoBattleCP);

      RefBattleCP attachedLeader = BobUtility::getAttachedLeader(d_batData, cp);
      RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();

    // First, get a random value between 2 - 20
      int dieRoll = CRandom::get(2, 20);
#ifdef DEBUG
      bcLog.printf("Unmodified die roll = %d", dieRoll);
#endif

      const Table1D<SWORD>& mTable = BattleTables::chargeTestDieRollModifiers();
      enum {
         Morale,
         Charisma,
         AttachedCharisma,
         CavCharging,
         EnemyDisorder,
         OurDisorder,
         ChargingReadyArt,
         LineMorale100,
         LineMorale130,
         LineMorale,
         ChargingDoubleMorale,
         FatigueLess65,
         FatigueLess130,
         InCover,
      };
      // now modify
      //-----------------------------

      // +1 for each 20 pts of morale
      if(mTable.getValue(Morale) > 0)
         dieRoll += (cp->morale() / mTable.getValue(Morale));//(cp->morale() / 20);

#ifdef DEBUG
      bcLog.printf("Die roll after unit morale modifier = %d", dieRoll);
#endif

      // + 1 if leader has charisma of 170 or higher
      if(cp->leader()->getCharisma() >= 170)
         dieRoll += mTable.getValue(Charisma);

#ifdef DEBUG
      bcLog.printf("Die roll after leader charisma modifier = %d", dieRoll);
#endif

      // +1 for each 70 charisma of attached leader (if any)
      if(attachedLeader != NoBattleCP)
      {
         const int c_cInc = mTable.getValue(AttachedCharisma);//70;
         if(c_cInc > 0)
            dieRoll += (leader->getCharisma() / c_cInc);
      }

#ifdef DEBUG
      bcLog.printf("Die roll after attached leader charisma modifier = %d", dieRoll);
#endif

      // +3 if cav charging. unless enemy is unshaken squares
      if(cp->generic()->isCavalry() &&
          (bestTarget->spFormation() != SP_SquareFormation || bestTarget->shaken()))
      {
         dieRoll += mTable.getValue(CavCharging);//3;
      }

#ifdef DEBUG
    bcLog.printf("Die roll after cavalry modifier = %d", dieRoll);
#endif

      // +1 for enemy at disorder 1 or 0
      if(bestTarget->disorder() <= 1)
         dieRoll += mTable.getValue(EnemyDisorder);//++;

#ifdef DEBUG
    bcLog.printf("Die roll after enemy disorder modifier = %d", dieRoll);
#endif

      // -1 for each disorder lost
      dieRoll += (mTable.getValue(OurDisorder) * (3 - cp->disorder()));

#ifdef DEBUG
      bcLog.printf("Die roll after disorder modifier = %d", dieRoll);
#endif

      // -1 for each enemy artillery being charge that is ready to fire
    if( (bestTarget->generic()->isArtillery() && bestTarget->sp() && bestTarget->spFormation() == SP_UnlimberedFormation) ||
            (!bestTarget->generic()->isArtillery() && bestTarget->holding() && bestTarget->nArtillery() > 0) )
      {
         dieRoll += (mTable.getValue(ChargingReadyArt) * minimum(bestTarget->columns(), bestTarget->nArtillery()));
      }

#ifdef DEBUG
    bcLog.printf("Die roll after enemy artillery modifier = %d", dieRoll);
#endif

      // for infantry being in line
      if(cp->generic()->isInfantry() && cp->spFormation() == SP_LineFormation)
      {
         // -4 if morale is < than 100
         if(cp->morale() < 100)
             dieRoll += mTable.getValue(LineMorale100);//-= 4;
         // -3 if morale is < 130
         else if(cp->morale() < 130)
             dieRoll += mTable.getValue(LineMorale130);//-= 3;
         // otherwise -2
         else
            dieRoll += mTable.getValue(LineMorale);//-= 2;
      }
#ifdef DEBUG
      bcLog.printf("Die roll after Infantry in line \\ morale modifier = %d", dieRoll);
#endif

      // -1 charging a unit with double morale
      if(bestTarget->morale() >= (2 * cp->morale()))
         dieRoll += mTable.getValue(ChargingDoubleMorale);//--;

#ifdef DEBUG
      bcLog.printf("Die roll after Enemy morale modifier = %d", dieRoll);
#endif

    // for fatigue
      if(cp->fatigue() <= 65)
         dieRoll += mTable.getValue(FatigueLess65);//-= 2;
      else if(cp->fatigue() <= 129)
         dieRoll += mTable.getValue(FatigueLess130);//--;

#ifdef DEBUG
      bcLog.printf("Die roll after fatigue modifier = %d", dieRoll);
#endif

      // -2 if unit is in cover
      if(Combat_Util::coverType(cp, d_batData) != Combat_Util::None)
         dieRoll += mTable.getValue(InCover);//-= 2;

#ifdef DEBUG
      bcLog.printf("Die roll after restricted terrain modifier = %d", dieRoll);
#endif
      // TODO: remaining modifiers

      outCome = (dieRoll >= 22) ? ChargeWithElan :
                     (dieRoll <= 7)  ? Balk :
                     (dieRoll <= 12) ? Wait :
                     (dieRoll <= 16) ? Charge2 : Charge1;
   }

    switch(outCome)
    {
       case ChargeWithElan:
       case Charge1:
       case Charge2:
       {

#ifdef DEBUG
          bcLog.printf("Outcome = Charge!");
#endif
          d_batData->addToShockCombatList(cp);
          ASSERT(cp->inCloseCombat());
          // if a cavalry unit, add 3 fatigue
          if(cp->generic()->isCavalry())
            cp->addFatigue(3);

          for(std::vector<DeployItem>::iterator di = cp->mapBegin();
                di != cp->mapEnd();
                ++di)
          // End
          {
             if(di->active())
                di->d_sp->shootingMuskets(True);
          }

          cp->shootingMuskets(True);
//     cp->inCloseCombat(True);
          break;
       }

       case Wait:
       case Balk:
       {
#ifdef DEBUG
          if(outCome == Wait)
          {
             // test to see if we move into line formation
             bcLog.printf("Outcome = Wait!");
          }
          else
         bcLog.printf("Outcome = Balk!");
#endif

          int testEvery[BattleOrderInfo::Aggress_HowMany] = {
             32, 16, 8, 4
          };
          ASSERT(cp->aggression() < BattleOrderInfo::Aggress_HowMany);

#ifdef DEBUG
          bcLog.printf("Test again in %d minutes", testEvery[cp->aggression()]);
#endif
          cp->nextShockCheck(d_batData->getTick() + (testEvery[cp->aggression()] * (60 * BattleMeasure::BattleTime::TicksPerSecond)));
          break;
       }
    }

    if(cp->inCloseCombat())
    {
         /*
          * Send a player message, one for each side
          */
         {
            // this side
             BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_IntoCloseCombat, cp);
             msg.target(bestTarget);
             d_batgame->sendPlayerMessage(msg);
         }
         {
             // that side
             BattleMessageInfo msg(bestTarget->getSide(), BattleMessageID::BMSG_IntoCloseCombat, bestTarget);
             msg.target(cp);
             d_batgame->sendPlayerMessage(msg);
         }

         return True;
    }

    return False;
}

// Process Cavalry beginning charge at Infantry
void B_CombatImp::procCavCharge(const RefBattleCP& cp)
{
   bool breakingOff = False;
   ASSERT(cp->generic()->isCavalry());
   BattleItem* bi = 0;
   SListIter<BattleItem> iter(&cp->targetList());
   while(++iter)
   {
      if(iter.current()->d_position == BattleItem::Front &&
          (iter.current()->d_cp->generic()->isInfantry() || iter.current()->d_cp->generic()->isArtillery()) )
      {
         bi = iter.current();
         break;
      }
   }

   // If we don't have an inf or art target, return
   if(!bi)
      return;

   // process for each infantry in target list
   //bool friendlyNear = Combat_Util::nonRoutingFriends(d_batData, cp, 2);

   iter.rewind();

   while(++iter)
   {
      bi = iter.current();
      if(bi->d_range > (2 * BattleMeasure::XYardsPerHex) || bi->d_position != BattleItem::Front || bi->d_cp->generic()->isCavalry())
         continue;

      // will not charge if inf is in medium cover
      if(Combat_Util::coverType(bi->d_cp, d_batData) >= Combat_Util::Medium)
      {
         breakingOff = True;
         break;
      }

      // get die roll between 1-10
      int dieRoll = CRandom::get(1, 10);

      const Table1D<SWORD>& mTable = BattleTables::cavChargeTestDieRollModifiers();
      enum {
         EnemyMorale,
         EnemyTacAb210,
         EnemyTacAb180,
         EnemyTacAb145,
         EnemyUphill,
         AllDisordered,
         SomeDisordered,
         EnemySquared,
         EnemyDisorder,
         EnemyEngaged,
         IntoFlankSquared,
         IntoFlank,
         EnemyMoving,
         EnemyShaken,
         EnemyInMarch,
         EnemyChangingFormation,
         EnemyChangingDeployment,
         EnemyInLine,
         EnemyExtended,
         EnemyCriticalLoss,
         CombinedArms,
      };

#ifdef DEBUG
      bcLog.printf("\nProcessing Cav vs Inf Charge Test for %s vs %s",
         cp->getName(), bi->d_cp->getName());

      bcLog.printf("Raw die-roll = %d", dieRoll);
#endif

      // +1 for each 20 of current morale (of the infantry)
      if(mTable.getValue(EnemyMorale) > 0)
         dieRoll += (bi->d_cp->morale() / mTable.getValue(EnemyMorale));

#ifdef DEBUG
      bcLog.printf("Die-roll after morale modifier = %d", dieRoll);
#endif

      // tactical ability
      Attribute ta = bi->d_cp->leader()->getTactical();
      if(ta >= 210)
         dieRoll += mTable.getValue(EnemyTacAb210);//3
      else if(ta >= 180)
         dieRoll += mTable.getValue(EnemyTacAb180);//2;
      else if(ta >= 145)
         dieRoll += mTable.getValue(EnemyTacAb145);//++;

#ifdef DEBUG
      bcLog.printf("Die-roll after tactical ability modifier = %d", dieRoll);
#endif

      // Inf is on higher elevation
      if(Combat_Util::averageHeight(bi->d_cp, d_batData) > Combat_Util::averageHeight(cp, d_batData))
         dieRoll += mTable.getValue(EnemyUphill);//++;

#ifdef DEBUG
      bcLog.printf("Die-roll after elevation modifier = %d", dieRoll);
#endif

      // Occupying disordering terrain (to cav)
      const Table3D<UBYTE>& dTable = BattleTables::terrainDisorder();
      int nDis = 0;
      // check front-row only
      for(int c = 0; c < bi->d_cp->wantColumns(); c++)
      {
         DeployItem* di = bi->d_cp->wantDeployItem(c, 0);
         ASSERT(di);
         if(di && di->active())
         {
            const BattleTerrainHex& hexInfo = d_batData->getTerrain(di->d_sp->hex());
            if(dTable.getValue(1, hexInfo.d_terrainType, 2))
               nDis++;
         }
      }

      int nCols = (bi->d_cp->wantColumns() % 2 == 0) ? bi->d_cp->wantColumns() / 2 : (bi->d_cp->wantColumns() / 2) + 1;
      if(nDis >= (nCols))
         dieRoll += mTable.getValue(AllDisordered);//2;
      else if(nDis > 0)
         dieRoll += mTable.getValue(SomeDisordered);//++;

#ifdef DEBUG
      bcLog.printf("Die-roll after disordered terrain (for cav) modifier = %d", dieRoll);
#endif

      // +2 if Closed or Squared
      if(bi->d_cp->sp()->formation() == SP_ClosedColumnFormation ||
          bi->d_cp->sp()->formation() == SP_SquareFormation)
      {
         dieRoll += mTable.getValue(EnemySquared);//2;
      }

#ifdef DEBUG
      bcLog.printf("Die-roll after disordered Closed / Squared modifier = %d", dieRoll);
#endif

      // -1 for each disorder level lost
      dieRoll += (mTable.getValue(EnemyDisorder) * (3 - bi->d_cp->disorder()));

#ifdef DEBUG
      bcLog.printf("Die-roll after disorder modifier = %d", dieRoll);
#endif

      // inf already engaged -1
      if(bi->d_cp->inCombat())
      {
         dieRoll += mTable.getValue(EnemyEngaged);//--;
      }

      // if combined arms
      SListIterR<BattleItem> eIter(&bi->d_cp->targetList());
      while(++eIter)
      {
         CRefBattleCP fCP = eIter.current()->d_cp;
         if(eIter.current()->d_range <= (3 * BattleMeasure::XYardsPerHex) &&
            fCP->generic()->isInfantry() &&
            (!(fCP->movingBackwards() || fCP->routing() || fCP->fleeingTheField())))
         {
            dieRoll += mTable.getValue(CombinedArms);//--;
         }
      }


#ifdef DEBUG
      bcLog.printf("Die-roll after already in combat modifier = %d", dieRoll);
#endif

      // cav into inf flank
      if(firingIntoFlank(cp, bi->d_cp))
      {
         if(bi->d_cp->sp()->formation() == SP_ClosedColumnFormation ||
             bi->d_cp->sp()->formation() == SP_SquareFormation)
         {
            dieRoll += mTable.getValue(IntoFlankSquared);//--;
         }
         else
            dieRoll += mTable.getValue(IntoFlank);//-= 3;

#ifdef DEBUG
         bcLog.printf("Die-roll after flank modifier = %d", dieRoll);
#endif
      }

      // moving
      if(bi->d_cp->moving())
         dieRoll += mTable.getValue(EnemyMoving);//--;

      //shaken
      if(bi->d_cp->shaken())
         dieRoll += mTable.getValue(EnemyShaken);//--;

      // in March
      if(bi->d_cp->sp()->formation() == SP_MarchFormation)
         dieRoll += mTable.getValue(EnemyInMarch);//-= 3;

      // changing SP formation
      if(bi->d_cp->liningUp())
         dieRoll += mTable.getValue(EnemyChangingFormation);//--;

      // changing div deployment
      if(bi->d_cp->deploying())
         dieRoll += mTable.getValue(EnemyChangingDeployment);//--;

      // TODO: for light rain

      // current morale of 130 or less and in line
      if(bi->d_cp->morale() <= 130 && bi->d_cp->sp()->formation() == SP_LineFormation)
         dieRoll += mTable.getValue(EnemyInLine);//--;

      // extended
      if(bi->d_cp->formation() == CPF_Extended)
         dieRoll += mTable.getValue(EnemyExtended);//-= 2;

      if(bi->d_cp->criticalLossReached())
         dieRoll += mTable.getValue(EnemyCriticalLoss);//-= 2;

#ifdef DEBUG
      bcLog.printf("Final die-roll = %d", dieRoll);
#endif

      // process outcome
      if(dieRoll >= 11)
      {
         // see if inf has time to change to squared or closed
         if(bi->d_cp->sp()->destFormation() == SP_ClosedColumnFormation ||
             bi->d_cp->sp()->destFormation() == SP_SquareFormation)
         {
            breakingOff = (dieRoll >= 13);
         }
         else if(bi->d_cp->generic()->isInfantry())
         {
            const Table3D<SWORD>& table = BattleTables::spfInfChangeTime();
            Nationality n = (scenario->getNationType(bi->d_cp->getNation()) == MajorNation) ?
               bi->d_cp->getNation() : scenario->getDefaultNation(bi->d_cp->getSide());

            int cavTime = 60 * (bi->d_range / BattleMeasure::XYardsPerHex);
            int addValue = (dieRoll == 11) ? 60 :
                                  (dieRoll == 12) ? 45 :
                                  (dieRoll == 13) ? 30 :
                                  (dieRoll == 14) ? 15 : 0;
            int infTime1 = table.getValue(n, bi->d_cp->sp()->fromFormation(), SP_SquareFormation) + addValue;
            int infTime2 = table.getValue(n, bi->d_cp->sp()->fromFormation(), SP_ClosedColumnFormation) + addValue;
            if(infTime1 <= cavTime || infTime2 <= cavTime)
            {
               // if no infantry is attack us also, form square
               bool infAttack = False;
               SListIter<BattleItem> liter(&bi->d_cp->targetList());
               while(++liter)
               {
                  if(liter.current()->d_cp->generic()->isInfantry() &&
                     liter.current()->d_range <= (4 * BattleMeasure::XYardsPerHex) &&
                     !iter.current()->d_cp->movingBackwards())
                  {
                     infAttack = (CRandom::get(100) < CRandom::get(98, 100));
                  }
               }

               if(!infAttack)
               {
                  bi->d_cp->getCurrentOrder().order().d_spFormation = (infTime1 <= cavTime) ?
                     SP_SquareFormation : SP_ClosedColumnFormation;
                  bi->d_cp->getCurrentOrder().order().d_whatOrder.insert(WhatOrder::SPFormation);
#ifdef DEBUG
                  bcLog.printf("Infantry changing formation");
#endif
               }
               breakingOff = (dieRoll >= 13);
            }
         }
      }

      else if(dieRoll >= 8)
         ; // do nothing

      else if(dieRoll == 7)
         bi->d_cp->increaseDisorder();

      else if(dieRoll >= 4)
      {
         bi->d_cp->increaseDisorder();
         // the 2 following if statements are acculative
         if(dieRoll <= 5)
         {
            bi->d_cp->increaseDisorder();
            bi->d_cp->removeMorale(5);
         }
         if(dieRoll <= 4)
         {
            bi->d_cp->increaseDisorder();
            bi->d_cp->removeMorale(5);
         }

         if(bi->d_cp->disorder() == 0)
            bi->d_cp->shaken(True);

         bi->d_cp->nextFire(d_batData->getTick() + (120 * BattleMeasure::BattleTime::TicksPerSecond));
      }
      else
      {
         bi->d_cp->removeMorale(20);
         B_LossUtil::routUnit(d_batgame, bi->d_cp);
         pursuitTest(cp, cp->leader());
      }

      if(breakingOff)
         break;
   }

   if(!breakingOff)
   {
      cp->charging(True);
   }

   else
   {
#ifdef DEBUG
      bcLog.printf("%s is breaking off the charge", cp->getName());
#endif
      int pullBack = 4 - cp->aggression();
      BattleCP::NearEnemyMode nem = (pullBack <= 1) ? BattleCP::NE_Retreat2 :
                             (pullBack == 2) ? BattleCP::NE_Retreat3 :
                             (pullBack == 3) ? BattleCP::NE_Retreat4 : BattleCP::NE_Retreat5;

        if(nem > cp->nearEnemyMode())                               
        {
         cp->nearEnemyMode(nem);
            cp->lockMode();
        }    
   }
}

/*
 * Deploy an organization(recursive!)
 */

void B_CombatImp::doOrgCombat(const RefBattleCP& cp)
{
    if(cp->child() != NoBattleCP)
      doOrgAndSisters(cp->child());


    // if we're Massed, Deployed, or Extended AND
   // enemy is in range, AND fire time has elapsed, do combat
    // also, we cannot be changing sp formation
//   if( (cp->getRank().sameRank(Rank_Division)) &&
//    (!cp->inCloseCombat()) &&
    if(cp->nextFire() < d_batData->getTick())// )
    {
      if(d_bDetect.enemyInRange(cp))
      {
         if(cp->targetList().closeRange() <= (8 * BattleMeasure::XYardsPerHex))
             d_bDetect.doEnemyNear(cp);

         if(cp->getRank().sameRank(Rank_Division))
         {
             if(!cp->inCloseCombat() &&
                  cp->generic()->isCavalry() &&
                  cp->nearEnemyMode() >= BattleCP::NE_Charge &&
                  cp->nearEnemyMode() <= BattleCP::NE_ChargeAt3 &&
                  cp->targetList().closeRangeFront() <= (2 * BattleMeasure::XYardsPerHex) &&
                  !cp->charging() &&
                  cp->allSPAtCenter())
             {
                procCavCharge(cp);
             }

             if(!cp->inCloseCombat() &&
                  cp->targetList().closeRangeFront() <= (1 * BattleMeasure::XYardsPerHex) &&
                  cp->allSPAtCenter() &&
                  cp->nextShockCheck() <= d_batData->getTick() &&
                  canCharge(cp))
             {
             }

             // do combat
             // Note: Cannot do combat if in 'March' Deployment, or changing SP formation
                 SPFormation fromFormation = cp->spFormation();
             if( (!cp->inCloseCombat()) &&
                (cp->formation() > CPF_March) &&
                     (fromFormation != SP_UnknownFormation) &&
                (fromFormation == cp->destSPFormation()) )
             {
#ifdef DEBUG
                  bcLog.printf("Enemy is near %s (at %d hexes), doing combat",
                  cp->getName(), static_cast<int>(cp->targetList().closeRange()));
#endif
                  doFireCombat(cp);
//          B_LossUtil::clearDestroyedSP(d_batData, cp);
             }
         }
         else
             cp->nextFire(d_batData->getTick() + (120 * BattleMeasure::BattleTime::TicksPerSecond));
             // do near enemy routines if enemy is within 6 hexes

#ifdef DEBUG
         bcLog.printf("----------------------------------------\n");
#endif
      }
      else if(cp->targetList().closeRange() <= (8 * BattleMeasure::XYardsPerHex))
      {
         d_bDetect.doEnemyNear(cp);
      }

    }
}

/*
 * Deploy an organization and its children (recursively!)
 */

void B_CombatImp::doOrgAndSisters(const RefBattleCP& cp, bool forCombat)
{
   {   // braces needed until compiler complies with new standard
    int loop = 0;  
    for(RefBattleCP unit = cp; unit != NoBattleCP; unit = unit->sister())
    {
      if(unit->hasQuitBattle() || !unit->active())
         continue;

      if(forCombat)
        doOrgCombat(unit);
      else
        clearFlags(unit);

          if(++loop >= 100)
          {
            FORCEASSERT("Infinte loop n deployOrgAndSisters()");
            break;
          }
    }
  }
}

void B_CombatImp::doFireCombat(const RefBattleCP& cp)
{
   BattleList& blist = cp->targetList();
   ASSERT(blist.entries() > 0);

   // if within 2 hexes, deduct 1 fatigue point for firing
   if(cp->generic()->isArtillery() || blist.closeRangeFront() <= (2 * BattleMeasure::XYardsPerHex))
    cp->addFatigue(1);


#ifdef DEBUG
   bcLog.printf("Doing Fire-Combat for %s", cp->getName());
    
    for(int c = 0; c < cp->wantColumns(); c++)
    {
       DeployItem* di = cp->wantDeployItem(c, 0);
       ASSERT(di);
       if(di && di->active())
       {
         bcLog.printf("SP (col = %d, row = 0) %s moving, %s shooting muskets, %s shooting guns",
         c, (di->d_sp->isMoving()) ? "is" : "is not",
            (di->d_sp->shootingMuskets()) ? "is" : "is not",
            (di->d_sp->shootingGuns()) ? "is" : "is not");
       }
       bcLog.printf("%s %s shooting guns", cp->getName(), (cp->shootingGuns()) ? "is" : "is not");
    }
#endif

   /*
   * Go through each enemy in list and calculate losses for each
   * This is done by cross-referencing a modified die-roll with
   * modified fire-value
   */

   // go through each enemy individually
#ifdef DEBUG
   {
      SListIterR<BattleItem> liter(&blist);
      while(++liter)
      {
         bcLog.printf("%s is a target of %s",
               liter.current()->d_cp->getName(), cp->getName());
      }
   }
#endif

   UWORD totalFireValue = 0;
   UWORD totalUnmodified = 0;
   bool firedGuns = False;
   SListIterR<BattleItem> liter(&blist);
   while(++liter)
   {
      const BattleItem* bi = liter.current();
      ASSERT(bi);

      // we can only fire to ther front
      // skip past any units on flank or rear
      if(bi->d_position != BattleItem::Front)
         continue;

      // If we have a player designated target, skip past if not this unit
      if(cp->targetCP() != NoBattleCP && cp->targetCP() != bi->d_cp)
         continue;

      // first get unmodified fire value and die roll
      UWORD fireValue = bi->d_fireValue;
        totalUnmodified += fireValue;

      // if unit is routing, we cannot fire
      if(cp->routing())
         fireValue = 0;

      if(fireValue == 0)
         continue;

      int dieRoll = CRandom::get(1, 10); // value between 1 and 10
      // if an artillery specialist commanding artillery, roll twice taking best roll
      if(cp->generic()->isArtillery() && cp->leader()->isSpecialistType(Specialist::Artillery))
      {
         int newRoll = CRandom::get(1, 10);
         if(newRoll > dieRoll)
            dieRoll = newRoll;
      }

#ifdef DEBUG
      bcLog.printf("Calculating losses for %s, unmodified fireValue = %d, dieRoll = %d",
         bi->d_cp->getName(), static_cast<int>(fireValue), dieRoll);
      if(cp->targetCP() != NoBattleCP)
      bcLog.printf("%s is a player-designated target", bi->d_cp->getName());
#endif
#if 1
      // target is in 'March' (Not sure if this is XX or SP)

      const Table1D<SWORD>& mTable = BattleTables::fireCombatFireValueMultipliers();
      enum {
         TargetInMarch,
         FiringIntoFlank,
         NotInLine,
         ChangingDeployment,
         Moving,
         Disorder0,
         Disorder1,
         Disorder2,
         Disorder3,
         TargetRetreating,
         CrossingRiver,
         CrossingMarsh,
         Shaken,
      };

      int d = mTable.getResolution() / 2;
      if(bi->d_cp->spFormation() == SP_MarchFormation)
      {
//       fireValue *= 2;
         fireValue = ((fireValue * mTable.getValue(TargetInMarch)) + d) / mTable.getResolution();
#ifdef DEBUG
         bcLog.printf("FireValue after enemy in SP March modifier = %d", fireValue);
#endif
      }

#endif
      // X 1.5 for firing into flank of enemy
      bool intoFlank = firingIntoFlank(cp, bi->d_cp);

      if(intoFlank)
      {
//       fireValue *= 1.5;
         fireValue = ((fireValue * mTable.getValue(FiringIntoFlank)) + d) / mTable.getResolution();
#ifdef DEBUG
         bcLog.printf("%s is firing into the flank of %s",
            cp->getName(), bi->d_cp->getName());
#endif
      }

      // if in march, closed, square, or column (fire value X .5)
      if(!cp->generic()->isArtillery() && cp->spFormation() != SP_LineFormation)
      {
//       fireValue *= .5;
         fireValue = ((fireValue * mTable.getValue(NotInLine)) + d) / mTable.getResolution();
#ifdef DEBUG
         bcLog.printf("FireValue after SP not in line modifier = %d", fireValue);
#endif
      }

      // is infantry and is changing divisional deployment
      if(cp->formation() != cp->nextFormation())
      {
//       fireValue *= .5;
         fireValue = ((fireValue * mTable.getValue(ChangingDeployment)) + d) / mTable.getResolution();
#ifdef DEBUG
         bcLog.printf("FireValue after changing deployment modifier = %d", fireValue);
#endif
      }
      // TODO: for moving at over half speed
      else if(cp->speed() > 0) //!cp->holding() || !cp->allSPAtCenter())
      {
         fireValue = ((fireValue * mTable.getValue(Moving)) + d)  / mTable.getResolution();
//       fireValue *= .5;
#ifdef DEBUG
         bcLog.printf("FireValue after moving modifier = %d", fireValue);
#endif
      }

      // modify for disordered unit
      // disorder level 0 (X0.5)
      static UBYTE s_dTable[BattleCP::Disorder::MaxValue + 1] = {
          Disorder0, Disorder1, Disorder2, Disorder3 //5, 7, 9, 10
      };

//    const int c_base = 10;
//    fireValue = (fireValue * s_dTable[cp->disorder()]) / c_base;
      fireValue = ((fireValue * mTable.getValue(s_dTable[cp->disorder()])) + d) / mTable.getResolution();

#ifdef DEBUG
      bcLog.printf("FireValue after disorder modifier = %d", fireValue);
#endif

      // TODO: modify artillery firing on unlimbered arty
      // TODO: infantry firing in heavy rain
      // TODO: Artillery firing at 3 hexes or more in heavy mud
      if(cp->nArtillery() > 0 &&
          cp->targetList().closeRangeFront() > (3 * BattleMeasure::XYardsPerHex))
      {
      }

      // Target is retreating or routing
      if( (bi->d_cp->routing()) || (bi->d_cp->retreating()) )
      {
//       fireValue *= .5;
         fireValue = ((fireValue * mTable.getValue(TargetRetreating)) + d) / mTable.getResolution();

#ifdef DEBUG
         bcLog.printf("FireValue after retreating modifier = %d", fireValue);
#endif
      }


      // Unit currently crossing river or in Marsh
      if(Combat_Util::crossingRiver(d_batData, cp) || Combat_Util::crossingStream(d_batData, cp))
      {
//       fireValue *= .5;
         fireValue = ((fireValue * mTable.getValue(CrossingRiver)) + d) / mTable.getResolution();
#ifdef DEBUG
         bcLog.printf("FireValue after crossing river modifier = %d", fireValue);
#endif
      }

      if(Combat_Util::terrainType(d_batData, cp) == GT_Marsh)
      {
//       fireValue *= .5;
         fireValue = ((fireValue * mTable.getValue(CrossingMarsh)) + d) / mTable.getResolution();

#ifdef DEBUG
         bcLog.printf("FireValue after in marsh modifier = %d", fireValue);
#endif
      }

      // X .25 if firing unit is shaken
      if(cp->shaken())
      {
//       fireValue *= .25;
         fireValue = ((fireValue * mTable.getValue(Shaken)) + d) / mTable.getResolution();

#ifdef DEBUG
         bcLog.printf("FireValue after shaken modifier = %d", fireValue);
#endif
      }

      // calc final value (must be between 0 - 30)
      if(fireValue > 0)
      {
         totalFireValue += fireValue;

         const Table2D<UWORD>& tbTable = BattleTables::fireCombatPoints();
         fireValue = clipValue((fireValue + (tbTable.getResolution() / 2)) / tbTable.getResolution(), 0, 30);

         /*
          * Modify die roll
          */

         int modifyBy = 0;

         const Table1D<SWORD>& mTable = BattleTables::fireCombatDieRollModifiers();
         enum {
            FireValue210,
            FireValue180,
            FireValue150,
            FireValue120,
            FireValue105,
            FireValue80,
            TargetClosedOrLimbered,
            TargetSquared,
            ArtyAgainstSquared,
            ArtyAgainstMarch,
            ArtyAgainstColumn,
            EnemyInNoCover,
            EnemyInLightCover,
            EnemyInMediumCover,
            EnemyInHeavyCover,
            EnemyInSuperHeavyCover,
            FiringUphill,
            FatigueLess65,
            FatigueLess130,
            TargetInAJam,
            BehindWall,
            TargetCrossingRiver,
            MoraleLess65,
            TargetIsChargingCav,
            TargetMovingAway,
            TargetBehindWalls,
         };



         Attribute avgValue = BobUtility::unitFireValue(d_batData->ob(), cp);
         // fire value is greater than 200
         if(avgValue >= 210)
            modifyBy += mTable.getValue(FireValue210);//3;
         else if(avgValue >= 181)
            modifyBy += mTable.getValue(FireValue180);//+= 2;
         else if(avgValue >= 150)
            modifyBy += mTable.getValue(FireValue150);//+= 1;
         else if(avgValue < 80)
            modifyBy += mTable.getValue(FireValue80);//-= 3;
         else if(avgValue < 105)
            modifyBy += mTable.getValue(FireValue105);//-= 2;
         else if(avgValue < 120)
            modifyBy += mTable.getValue(FireValue120);//-= 1;

         // for target formations
         // target is in closed or limbered formation
         if( (bi->d_cp->generic()->isInfantry() && bi->d_cp->spFormation() == SP_ClosedColumnFormation) ||
               (bi->d_cp->generic()->isArtillery() && bi->d_cp->spFormation() == SP_LimberedFormation) )
         {
            modifyBy += mTable.getValue(TargetClosedOrLimbered);//1;
         }

         // target is in square formation
         if(bi->d_cp->spFormation() == SP_SquareFormation)
            modifyBy += mTable.getValue(TargetSquared);//2;

         // artillery firing at closed or square
         if( (cp->generic()->isArtillery() || cp->nArtillery() > 0) &&
               (bi->d_cp->spFormation() == SP_ClosedColumnFormation || bi->d_cp->spFormation() == SP_SquareFormation) )
         {
            modifyBy += mTable.getValue(ArtyAgainstSquared);//1;
         }

         // Artillery firing at column, massed
         if(cp->nArtillery() > 0 || cp->generic()->isArtillery())
         {
            if(bi->d_cp->sp() && bi->d_cp->spFormation() == SP_ColumnFormation)
               modifyBy += mTable.getValue(ArtyAgainstColumn);//1;
            else if(bi->d_cp->sp() && bi->d_cp->spFormation() == SP_MarchFormation)
               modifyBy += mTable.getValue(ArtyAgainstMarch);//2;
         }

         Combat_Util::CoverType ct = Combat_Util::coverType(bi->d_cp, d_batData);
         ASSERT(ct < Combat_Util::Cover_HowMany);
         // TODO: move this to scenario table
         static const UBYTE s_cTable[Combat_Util::Cover_HowMany] = {
            EnemyInNoCover, EnemyInLightCover, EnemyInMediumCover, EnemyInHeavyCover, EnemyInSuperHeavyCover //0, -1, -2, -3, -4
         };
         modifyBy += mTable.getValue(s_cTable[ct]);

         // modify if unit is firing uphill
         BattleTerrainHex::Height ourHeight = Combat_Util::averageHeight(cp, d_batData);
         BattleTerrainHex::Height theirHeight = Combat_Util::averageHeight(bi->d_cp, d_batData);
         if(ourHeight + 3 < theirHeight)
            modifyBy += mTable.getValue(FiringUphill);//1;

         // Modify for fatigue
         // if less than 65 (-2)
         if(cp->fatigue() <= 65)
            modifyBy += mTable.getValue(FatigueLess65);//-= 2;
         // less than 129 (-1)
         else if(cp->fatigue() <= 129)
            modifyBy += mTable.getValue(FatigueLess130);//-= 1;

         // +1 if target is moving through another unit
         if(inATrafficJam(d_batData, bi->d_cp))
            modifyBy += mTable.getValue(TargetInAJam);//1;

         // +1 for firing behind walls
         if(Combat_Util::behindWall(d_batData, cp))
            modifyBy += mTable.getValue(BehindWall);//++;

         // Target is crossing river
         if(Combat_Util::crossingRiver(d_batData, cp) || Combat_Util::crossingStream(d_batData, bi->d_cp))
            modifyBy += mTable.getValue(TargetCrossingRiver);//++;

         // firing units morale is 65 or less
         if(cp->morale() <= 65)
            modifyBy += mTable.getValue(MoraleLess65);//--;

         // target is charging cavalry
         if(bi->d_cp->generic()->isCavalry() && bi->d_cp->charging())
            modifyBy += mTable.getValue(TargetIsChargingCav);//--;

         // target is moving away from unit
         if(bi->d_cp->movingBackwards())
            modifyBy += mTable.getValue(TargetMovingAway);//--;

         // target is behind walls
         if(Combat_Util::behindWall(d_batData, bi->d_cp))
            modifyBy += mTable.getValue(TargetBehindWalls);//--;


         // get final die roll (must be between 0 - 11
         dieRoll = clipValue(dieRoll + modifyBy, 0, 11);


#ifdef DEBUG
         bcLog.printf("Modified fireValue = %d, dieRoll = %d",
             static_cast<int>(fireValue), dieRoll);
#endif

         /*
          * Get and apply losses to target
          */

         // set takingHits flag
         for(std::vector<DeployItem>::iterator di = bi->d_cp->mapBegin();
               di != bi->d_cp->mapEnd();
               di++)
         {
            if(di->active())
            {
               di->d_sp->takingMusketHits(bi->d_cp->takingMusketHits());
               di->d_sp->takingGunHits(bi->d_cp->takingGunHits());
               if(di->d_sp->takingGunHits())
                  firedGuns = True;
            }
         }

         doFireLosses(bi->d_cp, cp, fireValue, dieRoll);
      }

   }

   // remove 2 ammo-points if artillry or inf/cav with arty attached
   // or 1 if in def. posture and target is 5 or more hexes away
   if( (firedGuns) &&
      (cp->generic()->isArtillery() || cp->nArtillery() > 0) &&
      (cp->ammoSupply() > 0) )
   {
      UBYTE removeHowMany = (cp->posture() == BattleOrderInfo::Defensive &&
                           blist.closeRangeFront() >= (5 * BattleMeasure::XYardsPerHex)) ? 1 : 2;

      cp->removeAmmoSupply(removeHowMany);

      // if this is an artillery only unit, and it has a player designated target,
      // test for building fires
      if(cp->generic()->isArtillery() && cp->targetCP() != NoBattleCP)
      {
         const Table2D<UWORD>& tbTable = BattleTables::fireCombatPoints();
         procFireBuildings(cp, cp->targetCP(), totalFireValue / tbTable.getResolution());
      }
#ifdef DEBUG
      bcLog.printf("%s has %d ammo points remaining",
          cp->getName(), static_cast<int>(cp->ammoSupply()));
#endif
   }

   // If this is unsupprted artillery, run a morale test
   if(cp->generic()->isArtillery() &&
      cp->targetList().closeRange() <= (2 * BattleMeasure::XYardsPerHex) &&
      cp->nearEnemyMode() < BattleCP::NE_Retreat2 &&
      !Combat_Util::artillerySupported(d_batData, cp))
   {
#ifdef DEBUG
      B_LossUtil::procMoraleTest(d_batgame, cp, B_LossUtil::Trigger_UnsupportedArty, bcLog);
#else
      B_LossUtil::procMoraleTest(d_batgame, cp, B_LossUtil::Trigger_UnsupportedArty);
#endif
   }

#ifdef DEBUG
   bcLog.printf("totalFireValue for %s = %d", cp->getName(), (int)totalFireValue);
#endif
   if(totalUnmodified > 0)
      cp->nextFire(d_batData->getTick() + (120 * BattleMeasure::BattleTime::TicksPerSecond));
}

UINT B_CombatImp::doShockLosses(SideShockBattleList& sl, const int totalLoss)
{
   // first get total SP strength
   UINT totalSPStr = 0;
   {
      SideShockBattleListIter iter(&sl);
      while(++iter)
      {
         totalSPStr += BobUtility::unitSPStrength(d_batData, iter.current()->d_cp);
      }
   }

   // get total to lose
   //UINT totalLoss = (totalSPStr * percent) / 100;

   // divy loss up among CP if we have more than one unit
   if(sl.entries() > 1)
   {
      int losses = totalLoss;
      UINT lossPer = 10;
      while(losses > 0)
      {
         int index = CRandom::get(sl.entries());
         ASSERT(index < sl.entries());

         SideShockBattleListIter iter(&sl);
         for(int i = 0; ++iter; i++)
         {
            if(i == index)
            {
               RefBattleCP targetCP = Combat_Util::bestShockTarget(iter.current()->d_cp);
               applyLoss(iter.current()->d_cp, targetCP, lossPer);
               break;
            }
         }

         losses -= lossPer;
      }
   }

   // otherwise it all goes to the lone unit
   else if(sl.entries() == 1)
   {
      RefBattleCP targetCP = Combat_Util::bestShockTarget(sl.first()->d_cp);
      applyLoss(sl.first()->d_cp, targetCP, totalLoss);
   }

   return totalLoss;
}

void B_CombatImp::applyLoss(const RefBattleCP& cp, const RefBattleCP& inflictingCP, int losses, bool fireCombat)
{
    // apply actual losses to cp
#ifdef DEBUG
    B_LossUtil::applyLoss(d_batData, cp, losses, bcLog);
#else
    B_LossUtil::applyLoss(d_batData, cp, losses);
#endif

    // process morale loss
#ifdef DEBUG
    B_LossUtil::procMoraleLoss(d_batgame, cp, bcLog);
#else
    B_LossUtil::procMoraleLoss(d_batgame, cp);
#endif

    // run morale test if we have lost more than 1 SP
    UINT curStr = BobUtility::unitSPStrength(d_batData, cp);
    if(maximum<int>(0, cp->lastStrength() - curStr) >= 50)
    {
         B_LossUtil::MoraleTestTrigger tr = (cp->criticalLossReached()) ? B_LossUtil::Trigger_CriticalLoss :
                                            (fireCombat) ? B_LossUtil::Trigger_SPLostToFire :
                                            B_LossUtil::Trigger_HowMany;

         if(tr != B_LossUtil::Trigger_HowMany)
            {
#ifdef DEBUG
             B_LossUtil::procMoraleTest(d_batgame, cp, tr, bcLog);
#else
             B_LossUtil::procMoraleTest(d_batgame, cp, tr);
#endif
         }

         // A 50/50 chance of doing a leader loss test
         if(CRandom::get(100) < 50)
         {
            RefBattleCP attachedLeader = BobUtility::getAttachedLeader(d_batData, cp);
            RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();
            B_LossUtil::procLeaderLossTest(d_batgame, leader, cp);
         }

         // Send player message to both side
         const int perLosses = (cp->startStrength() > 0) ? 100 - ((curStr * 100) / cp->startStrength()) : 0;
         if(perLosses > 0)
         {
             // our side
             {
                  BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_TakingLosses, cp);
                  msg.n1(perLosses);
                  d_batgame->sendPlayerMessage(msg);
             }

             // their side
             if(inflictingCP != NoBattleCP)
             {
                  BattleMessageInfo msg(inflictingCP->getSide(), BattleMessageID::BMSG_InflictingLosses, inflictingCP);
                  msg.target(cp);
                  d_batgame->sendPlayerMessage(msg);
             }
         }

         cp->lastStrength(curStr);
    }
}

void B_CombatImp::doFireLosses(const RefBattleCP& cp, const RefBattleCP& inflictingCP, const UWORD fireValue, const int dieRoll)
{
    ASSERT(fireValue <= 30);
    ASSERT(dieRoll <= 11);

#ifdef DEBUG
    bcLog.printf("\n------------ Applying Losses for %s", cp->getName());
#endif

   const Table2D<UWORD>& table = BattleTables::fireCombatCasualty();
   int losses = table.getValue(fireValue, dieRoll);

   // cut losses in half if a 'pure' artillery unit
   if(cp->generic()->isArtillery())
      losses *= .5;

// if(losses > 0)
//  cp->shouldReorganize(True);

#ifdef DEBUG
   bcLog.printf("Losses = %d.%d SP", static_cast<int>(losses / 100),
      static_cast<int>((losses % 100) / 10));
#endif

   /*
   * For now losses break down as follows
   * 1. Equal chance for loss to be applied to any inf or cav sp
   * 2. half the above chance for artillery (that is part of a inf or cav XX)
   */

   applyLoss(cp, inflictingCP, losses, True);
}

/*-------------------------------------------------------------
 * Client-access
 */

B_CombatInt::B_CombatInt(BattleGameInterface* batgame) :
   d_ci(new B_CombatImp(batgame))
{
   ASSERT(d_ci);
}

B_CombatInt::~B_CombatInt()
{
   if(d_ci)
      delete d_ci;
}

void B_CombatInt::process()
{
   if(d_ci)
      d_ci->process();
}

void B_CombatInt::newDay()
{
   if(d_ci)
      d_ci->newDay();
}

}; // end namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/11/26 10:34:41  greenius
 * Current Directory detection improved
 *
 * Battlegame sound system initialised properly
 *
 * BattleAI Message queue problems fixed
 *
 * Order of Battles loading
 *
 * HexMap uses reference counted units
 *
 * BattleEditor open-file dialogs use wrapped classes
 *
 * PathFind compiler warnings removed
 *
 * Orphan detection in BattleOB detected and removed
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
