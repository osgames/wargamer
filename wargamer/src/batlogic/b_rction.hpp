/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef B_RCTION_HPP
#define B_RCTION_HPP

//#include "batdata.hpp"

class BattleGameInterface;
namespace B_Logic {
class ReactionManager;

class B_Reaction {
      ReactionManager* d_rm;
   public:
      B_Reaction(BattleGameInterface* batgame);
    ~B_Reaction();
      void process();
};

}; // end namespace
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
