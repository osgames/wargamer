/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/

// Obsolete deployment code I do not want to get rid of as yet
// NOT part of source

/*
UWORD B_Route::defaultRoute(const HexCord& srcHex,  const HexCord& destHex,
        HexList* excludeHexes, HexList& list, bool addSrc)
{
  UWORD bestCost = Util::RouteNotFound;
  if(!Util::prohibitedTerrain(d_batData, d_cp, destHex))
  {
         HexCord hex = srcHex;
         if(addSrc)
                list.newItem(hex, d_cp->facing());

         HexList xHexes;
         if(excludeHexes)
                xHexes = *excludeHexes;

         int loops = 0;
         while(hex != destHex)
         {
                UWORD cost = defaultHex(hex, destHex, &xHexes);

                // add hex to exclude hexes so we don't get stuck in an infinite loop
                if(excludeHexes)
                  xHexes.newItem(hex);

                if(cost != Util::RouteNotFound)
                {
                  list.newItem(hex, d_cp->facing());
                  if(bestCost == Util::RouteNotFound)
                         bestCost = 0;

                  bestCost += cost;
                }
                else
                {
                  list.reset();
                  return Util::RouteNotFound;
                }
                if(++loops >= 300)
                {
//                FORCEASSERT("Infinite loop in ::defaultRoute()");
                  list.reset();
                  return Util::RouteNotFound;
                }
         }

         if(excludeHexes)
                excludeHexes->addList(xHexes);
  }

  return bestCost;
}


*/
// Do an exhaustive search for the best route. any branch of search whose cost
// exceeds default cost, will be discontinued
bool B_Route::searchNodes(const HexCord& srcHex, const HexCord& destHex,
  HexList* excludeHexes, HexList& list, const UWORD defaultCost, UWORD& cost)
{
  UWORD bestCost = defaultCost;
  cost += Util::terrainCost(d_batData, d_cp, srcHex);

  if(cost < defaultCost)
  {
         list.newItem(srcHex, d_cp->facing());

         if(excludeHexes)
           excludeHexes->newItem(srcHex);

         if(srcHex == destHex)
                return True;

         HexList bestList;

         for(HexCord::HexDirection hd = HexCord::First; hd < HexCord::Count; INCREMENT(hd))
         {
                HexCord nextHex;
                if( (d_batData->moveHex(srcHex, hd, nextHex)) &&
                         (!excludeHexes || !Util::hexInList(nextHex, *excludeHexes)) &&
                         (!Util::prohibitedTerrain(d_batData, d_cp, nextHex)) )
                {
                  UWORD thisCost = cost;
                  HexList tList;
                  HexList nExcludeList;
                  if(excludeHexes)
                         nExcludeList = *excludeHexes;

                  if(searchNodes(srcHex, destHex, &nExcludeList, tList, bestCost, thisCost))
                  {
                         bestList = tList;
                         bestCost = thisCost;
                  }
                }
         }

         if(bestCost < defaultCost)
         {
                list = bestList;
      cost += bestCost;
         }
  }

  return False;
}
 #if 0
    // if 1st row we are already where we want to be
    if(r == 0)
    {
      alreadyThere(cdi.d_sp, map);
    }

    // rows 2 or greater
    else
    {
      int newRow = r / 2;

      switch(cp->deployWhichWay())
      {
        case CPD_DeployRight:
        case CPD_DeployCenter:
          deployRight(cp, cdi, newRow);
          break;

        case CPD_DeployLeft:
          deployLeft(cp, cdi, newRow);
          break;
      }
    }
#endif

#if 0
  // if sp count is 4 or less, don't do anything;
  if(spCount <= 4)
    return;

  ASSERT(cp->wantColumns() == 2);
  ASSERT(cp->rows() == 2);
  std::vector<DeployItem>& map = cp->deployMap();

  /*
   * go through old map and reassign sp's to new map
   */

  for(int r = 0; r < cp->rows(); r++)
  {
    for(int c = 0; c < cp->columns(); c++)
    {
      DeployItem& di = map[(cp->columns() * r) + c];
      if(!di.active())
        continue;
#if 0
      switch(cp->deployWhichWay())
      {
        case CPD_DeployRight:
        {
          // if row 0 or 1 and column 0 or 1 we are where we want to be
          if(c < 2 && r < 2)
            alreadyThere(di.d_sp, map);
          else
          {
            int newRow;
            if(r == 0)
            {
              if((c == 2 || c == 3))
                newRow = 2;
              else
              {
                ASSERT(c < 6); // no more than 6 row possible
                newRow = 4;
              }
            }
            else
            {
              if((c == 2 || c == 3))
                newRow = 3;
              else
              {
                newRow = (cp->columns() % 2 == 0) ? 5 : 4;
              }
            }

            deployRight(cp, di, newRow);
          }
          break;
        }

        case CPD_DeployCenter:
        {
          int startC = (cp->columns() % 2 == 0) ? maximum(0, (cp->columns() / 2) - 1) :
               cp->columns() / 2;

          if( (c == startC || c == startC + 1) && (r < 2) )
            alreadyThere(di.d_sp, map);
          else
          {
            int newRow;
            // if column 4 or less add 2 to row
            if(c < 4)
            {
              newRow = r + 2;
            }
            else
            {
              newRow = (c == 4) ? 4 : 5;
            }

            deployRight(cp, di, newRow);
          }
          break;
        }

        case CPD_DeployLeft:
        {
          if(r == 0 && (c == cp->columns() - 2 || c == cp->columns() - 1))
            alreadyThere(di.d_sp, map);
          else
          {
            if(r == 0)
              ;
            else
            {
              UBYTE cutoff = (spCount % 2 == 0) ? cp->columns() - 2 :
                cp->columns() - 3;


            }
          }

          break;
        }
      }
#endif
    }
  }

  plotDeployRoute(cp);
#endif



#if 0
//  int i = 0;    // cannot exceed # SP's
  for(int r = 0; r < cp->rows(); r++)
  {
    int startC = 0;
    int endC = cp->columns();
    int inc = 1;

    if(cp->deployWhichWay() == CPD_DeployLeft)
    {
      startC = cp->columns() - 1;
      endC = -1;
      inc = -1;
    }

    for(int c = startC; c != endC; c += inc) //, i++)
    {
      int index = maximum(0, (cp->columns() * r) + c);
      DeployItem& cdi = map[index];
      if(!cdi.active())
        continue;

      // if rows 1 or 2 we are already where we want to be
      if(r < 2)
      {
        alreadyThere(cdi.d_sp, map);
      }

      // rows 3 or greater
      else
      {
        int  newRow = ((r % 2) == 0) ? 0 : 1;
        WhichSide whichSide = (c == 0) ? LeftSide : RightSide;
        if( (r == (cp->rows() - 1)) &&
            ( (whichSide == LeftSide && cp->deployWhichWay() != CPD_DeployLeft) ||
              (whichSide == RightSide && cp->deployWhichWay() == CPD_DeployLeft) ) &&
            ((r % 2) == 0) )
        {
          if((spCount % 2) == 0)
            newRow = 1;

          whichSide = (whichSide == RightSide) ? LeftSide : RightSide;
        }

        switch(cp->deployWhichWay())
        {
          case CPD_DeployRight:
            deployRight(cp, cdi, newRow);
            break;

          case CPD_DeployCenter:
          {
            deployCenter(cp, cdi, newRow, whichSide);
            break;
          }

          case CPD_DeployLeft:
            deployLeft(cp, cdi, newRow, True);
            break;
        }
      }
    }
  }
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Deploy Battle Units
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "bdeploy.hpp"
#include "batdata.hpp"
//#include "bobdef.hpp"
#include "batarmy.hpp"
#include "bobutil.hpp"
#include "hexdata.hpp"
#include "bobiter.hpp"
#include "hexmap.hpp"
#include "moveutil.hpp"
#include "broute.hpp"
#include <math.h>
//#include "fsalloc.hpp"
#ifdef DEBUG
#include "random.hpp"
#include "boblog.hpp"
#endif

namespace B_Logic
{


/*------------------------------------------------------------------
 * Abstract Deplyment class
 */

class DeployBase {
      PBattleData d_batData;
      BattleOB* d_ob;
  protected:
      struct SPPosition {
        UBYTE d_r;  // row position
        UBYTE d_c;  // column position

        enum { Undefined = UBYTE_MAX };
      };

      enum { MaxSP = 12 };
  public:

    DeployBase(RPBattleData bd) :
      d_batData(bd),
      d_ob(d_batData->ob()) {}
    virtual ~DeployBase() {}

    void deploy(const RefBattleCP& cp);
    bool adjustDeployment(const RefBattleCP& cp);

    // virtual functions
    virtual void deploySP(const RefBattleCP& cp, const int spCount) = 0;
    virtual void redeploySP(const RefBattleCP& cp, const int spCount) = 0;
    virtual bool addExcludeHexes(const RefBattleCP& cp, HexList& excludeList) = 0;

    static DeployBase* allocate(RPBattleData bd, CPFormation f);

    enum WhichSide { LeftSide, RightSide, WhichSide_HowMany };
  protected:

    // stuff derived classes use
    WhichSide getWhichSide(const RefBattleCP& cp, int c, int r);
    void findDeployHexes(const RefBattleCP& cp, const CPFormation formation,
       HexArea& area);
    void getRowsAndColoumns(const CPFormation f, const int spCount, int& columns, int& rows);
    bool alreadyThere(const RefBattleSP& sp, std::vector<DeployItem>& map);
    void setToNewMap(DeployItem& di, const RefBattleSP& sp);
    void DeployBase::addToMap(const RefBattleCP& cp, const HexCord& startHex, WhichSide whichWay,
       int nDir, HexCord::HexDirection hd, int c, const int r, HexArea& area, Boolean skipFirst = False);

    bool getDestHex(const RefBattleSP& sp, std::vector<DeployItem>& map, HexCord& destHex);

    bool deployCenter(const RefBattleCP& cp, const DeployItem& cdi,
        int row, WhichSide whichSide, Boolean extended = False);
    bool deployRight(const RefBattleCP& cp, const DeployItem& cdi, int row);
    bool deployLeft(const RefBattleCP& cp, const DeployItem& cdi, int row, Boolean fromLeft = False);

    bool plotAdjustedRoute(const RefBattleCP& cp);
    bool canMoveInto(const RefBattleCP& cp, const HexCord& startHex,
            const HexCord::HexDirection hd, const int newColumn);
    bool getHex(HexCord& hex, const HexCord::HexDirection hd,
            const HexCord startHex);

    bool nextDirection(const RefBattleCP& cp, const HexCord& hex, HexCord::HexDirection& hd,
            HexPosition::Facing& newFace, const WhichSide ws, const int newColumn);

    bool plotDeployRoute(const RefBattleCP& cp);
    bool canExclude(HexPosition::Facing f, const HexCord& h1, const HexCord& h2);
    bool stayInPlace(const RefBattleCP& cp, int c, int r);
    bool getWaypoint(const RefBattleCP& cp, int c, int r, HexCord& hex);

    PBattleData batData() const { return d_batData; }
    BattleOB* ob() const { return d_ob; }
};

void DeployBase::getRowsAndColoumns(const CPFormation f, const int spCount, int& columns, int& rows)
{

   if(f == CPF_March)
   {
     columns = 1;
     rows = ((spCount % 2) == 0) ? spCount : spCount + 1;
   }
   else if(f == CPF_Massed)
   {
     columns = 2;
     rows = (spCount + 1) / 2;
   }
   else if(f == CPF_Deployed)
   {
     columns = (spCount + 1) / 2;
     rows = 2;
   }
   else
   {
     columns = spCount;
     rows = 1;
   }
}

void DeployBase::addToMap(const RefBattleCP& cp, const HexCord& startHex, WhichSide whichWay,
   int nDir, HexCord::HexDirection hd, int c, const int r, HexArea& area, Boolean skipFirst)
{
  HexCord hex = startHex;
  while(nDir--)
  {
    ASSERT(c >= 0);
    ASSERT(c < cp->wantColumns());

    if(!skipFirst)
    {
      cp->addToMap(NoBattleSP, &hex, c, r);
      area.update(hex);
    }
    else
    {
      nDir++;
      skipFirst = False;
    }

    if(whichWay == RightSide)
      c++;
    else
      c--;

    if(nDir)
    {
      HexCord uHex = hex;
      if(d_batData->moveHex(uHex, hd, hex))
        ;  // Note: need to figure out what to do if we're at an
           // edge or impassable terrain
#ifdef DEBUG
      else
        FORCEASSERT("Didn't find hex");
#endif
    }
  }
}

   /*------------------------- Deployment --------------------------------
    * How many Columns? Rows?
    *
    * 'March'    is 1 Column   (Single-file)
    * 'Massed'   is 2 Columns (2 Single-files)
    * 'Deployed' is 2 Rows  (2 Lines)
    * 'Extened'  is 1 Row    (Extended Line)
    *
    * Deployment from 'March' to 'Massed' to 'Deployed' to 'Extended' -- Centered:
    *   (Note:  1 represents first SP in CP's SPList. This is the 'base' SP)
    *
    *
    *    7 3 5 1 2 6 4 8
    *
    *        5 1 2 6
    *        7 3 4 8
    *
    *          1 2
    *          3 4
    *          5 6
    *          7 8
    *
    *          1
    *          2
    *          3
    *          4
    *          5
    *          6
    *          7
    *          8
    *
    *
    * From 'March' to 'Massed' to 'Deployed' to 'Extended' -- DeployRight:
    *   (Note:  1 represents first SP in CP's SPList. This is the 'base' SP)
    *
    *          1 2 5 6 3 4 7 8
    *
    *          1 2 5 6
    *          3 4 7 8
    *
    *          1 2
    *          3 4
    *          5 6
    *          7 8
    *
    *          1
    *          2
    *          3
    *          4
    *          5
    *          6
    *          7
    *          8
    *
    *
    * From 'March' to 'Massed' to 'Deployed' to 'Extended' -- DeployLeft:
    *   (Note: uppercase 1 represents first SP in CP's SPList. This is the 'base' SP)
    *
    *     8 7 4 3 6 5 2 1
    *
    *             6 5 2 1
    *             8 7 4 3
    *
    *                 2 1
    *                 4 3
    *                 6 5
    *                 8 7
    *
    *                   1
    *                   2
    *                   3
    *                   4
    *                   5
    *                   6
    *                   7
    *                   8
    */

void DeployBase::findDeployHexes(const RefBattleCP& cp, const CPFormation formation,
  HexArea& area)
{
   const int spCount = cp->spCount(); //BobUtility::countSP(cp);
   ASSERT(spCount > 0);
   const HexPosition::Facing facing = cp->facing();
   bool deploying = (cp->formation() < cp->nextFormation());

   HexCord hex;
   if(deploying || cp->deployWhichWay() != CPD_DeployCenter)
   {
     hex = (cp->deployWhichWay() == CPD_DeployLeft) ? cp->rightHex() : cp->leftHex();
   }
   else
   {
     hex = cp->centerHex();
   }

   /*
    * Calculate new rows and columns
    */

   int columns;
   int rows;
   getRowsAndColoumns(formation, spCount, columns, rows);

   cp->wantRows(rows);
   cp->wantColumns(columns);
   std::vector<DeployItem>& map = cp->deployMap();

   HexCord nextHex = hex;
   BattleCP::HexOffset whichWay = cp->whichWay();

   CPDeployHow dh = (!deploying && cp->formation() == CPF_Deployed && cp->deployWhichWay() == CPD_DeployCenter) ?
     CPD_DeployRight : cp->deployWhichWay();

   for(int r = 0; r < rows; r++)
   {
     switch(dh)
     {
       case CPD_DeployRight:
       {
         HexCord::HexDirection d = rightFlank(facing);
         addToMap(cp, nextHex, RightSide, columns, d, 0, r, area);
         break;
       }

       case CPD_DeployCenter:
       {
         ASSERT(columns >= 4);

         // how many do we add to right side?  columns / 2 rounded + 1
         int nDirR = (deploying) ?
            (cp->columns() + (((columns - cp->columns()) + 1) / 2)) :
            columns; //((columns + 1) / 2) + 1;
         // how many to the left? columns / 2 exclusive
         int nDirL = (deploying) ? columns - nDirR : 0;
         ASSERT(nDirL >= 0);

         addToMap(cp, nextHex, RightSide, nDirR, rightFlank(facing), nDirL, r, area);
         addToMap(cp, nextHex, LeftSide, nDirL, leftFlank(facing), nDirL, r, area, True);
         break;
       }

       case CPD_DeployLeft:
       {
         const int lc = cp->columns() - 1;
         int nDir = columns;

         addToMap(cp, nextHex, LeftSide, nDir, leftFlank(facing),
            columns - 1, r, area);

         break;
       }
     }

     HexCord::HexDirection hd = (whichWay == BattleCP::HO_Left) ? rightRear(facing) : leftRear(facing);
     whichWay = (whichWay == BattleCP::HO_Left) ? BattleCP::HO_Right : BattleCP::HO_Left;
     HexCord h;
     if(d_batData->moveHex(nextHex, hd, h))
       nextHex = h;
     else
       FORCEASSERT("Hex not found");
   }

}


// determine if an SP stays in its current place whilst redeploying
bool DeployBase::stayInPlace(const RefBattleCP& cp, int c, int r)
{
  // if deploying
  if(cp->formation() < cp->nextFormation())
  {
    static const UBYTE s_staticRows[CPF_Last] = {
      1, 2, 1, 1
    };

    return (r < s_staticRows[cp->formation()]);
  }

  // if redeploying
  else
  {
    switch(cp->deployWhichWay())
    {
      case CPD_DeployRight:
        switch(cp->formation())
        {
          case CPF_Massed:
            return (c == 0 && r == 0);
          case CPF_Deployed:
            return (c < 2 && r < 2);
          case CPF_Extended:
            break;
        }
        break;

      case CPD_DeployCenter:
      {
        int startC = (cp->columns() % 2 == 0) ? maximum(0, (cp->columns() / 2) - 1) :
               cp->columns() / 2;

        if(cp->formation() == CPF_Deployed)
        {
          return (r < 2 && (c == startC || c == startC + 1));
        }
        break;
      }

      case CPD_DeployLeft:
        if(cp->formation() == CPF_Massed)
        {
          return (r == 0 && c == cp->columns() - 1);
        }

        break;
    }

    return False;
  }
}

bool DeployBase::getWaypoint(const RefBattleCP& cp, int c, int r, HexCord& hex)
{
  bool deploying = (cp->formation() < cp->nextFormation());
  std::vector<DeployItem>& map = cp->deployMap();

  switch(cp->formation())
  {
    case CPF_March:
      if(deploying)
      {
        // the way point is the hex to the left or right
        // of the last hex in the new row
        if(r > cp->wantRows())
        {
          HexCord::HexDirection hd = (cp->deployWhichWay() == CPD_DeployRight) ?
            rightFlank(cp->facing()) : leftFlank(cp->facing());

          int c = (cp->deployWhichWay() == CPD_DeployRight) ? 0 : 1;
          DeployItem& di = cp->deployItem(c, cp->wantRows() - 1);
          if(d_batData->moveHex(di.d_wantHex, hd, hex))
            return True;
        }
      }
      else
      {
        if(r > cp->rows())
        {
          int c = (cp->deployWhichWay() == CPD_DeployRight) ? 0 : 1;
          DeployItem& di = map[(cp->columns() * (cp->rows() - 1)) + c];
          hex = di.d_wantHex;
          return True;
        }
      }
      break;

    case CPF_Massed:
      if(deploying)
      {
        // the way point is the 3rd row of this column
        if(r > 2)
        {
          DeployItem& di = map[(cp->columns() * 2) + c];
          hex = di.d_sp->hex();
          return True;
        }
      }
      else
      {
      }
      break;

    case CPF_Deployed:
      if(deploying)
      {
      }
      else
      {
      }
      break;

    case CPF_Extended:
      if(deploying)
      {
      }
      else
      {
      }
      break;
  }

  return False;
}

bool DeployBase::plotDeployRoute(const RefBattleCP& cp)
{
  ASSERT(cp->formation() < CPF_Extended);

  std::vector<DeployItem>& map = cp->deployMap();

  bool deploying = (cp->formation() < cp->nextFormation());
  // add exclude hexes
  HexList excludeHexes;
//  addExcludeHexes(cp, excludeHexes);

  // go through map and add units staying in place to exclude list
  for(int r = 0; r < cp->rows(); r++)
  {
    for(int c = 0; c < cp->columns(); c++)
    {
      int index = maximum(0, (cp->columns() * r) + c);   // column = 0, row = 2
      DeployItem& di = map[index];

      if(!di.active())
        continue;

      // if this is row 1 or 2, just add hex to excluded hexes
      // as they remain in place
      if(stayInPlace(cp, c, r)) //r < s_staticRows[cp->formation()])
      {
        excludeHexes.newItem(di.d_sp->hex());
      }
    }
  }

  // Reiterate through list and plot route for each SP
  for(r = 0; r < cp->rows(); r++)
  {
    for(int c = 0; c < cp->columns(); c++)
    {
      int index = maximum(0, (cp->columns() * r) + c);   // column = 0, row = 2
      DeployItem& di = map[index];

      if(!di.active())
        continue;

      if(!stayInPlace(cp, c, r)) //r < s_staticRows[cp->formation()])
      {
        di.d_sp->routeList().reset();

        HexList list;
        HexCord hex;

        // find intermediate dest, if any
        if(getWaypoint(cp, c, r, hex))
          list.newItem(hex);

        // get final dest
        if(getDestHex(di.d_sp, map, hex))
          list.newItem(hex);
        else
        {
          FORCEASSERT("Hex not found");
          return False;
        }

        if(di.d_sp->hex() != hex)
        {
          B_Route br(batData(), cp, di.d_sp);
          if(br.plotRoute(di.d_sp->hex(), list, &excludeHexes))
          {
            // remove first nodes, since they are equal to current hex
            ASSERT(di.d_sp->routeList().entries() > 0);
            di.d_sp->removeRouteNode();

            // update facings, maintain current face
            // until last hex, then switch to cp facing
            SListIter<HexItem> iter(&di.d_sp->routeList());
            while(++iter)
            {
              iter.current()->d_facing = (iter.current() != di.d_sp->routeList().getLast()) ?
                 di.d_sp->facing() : cp->facing();
            }

            // if deploying (but not while redeploying)
            // add hex to excluded list
            if(deploying)
              excludeHexes.newItem(hex);
          }
          else
          {
            FORCEASSERT("Route not found");
            return False;
          }
        }
        else if(di.d_sp->facing() != cp->facing())
          di.d_sp->routeList().newItem(hex, cp->facing());
      }
    }
  }

  return True;
}

bool DeployBase::canExclude(HexPosition::Facing f, const HexCord& h1, const HexCord& h2)
{

  return True;
}


bool DeployBase::alreadyThere(const RefBattleSP& sp, std::vector<DeployItem>& map)
{
  // find position in new map and set
  for(int i = 0; i < map.size(); i++)
  {
    if(map[i].d_wantHex == sp->hex())
    {
      sp->setMoving(True);
      map[i].d_wantSP = sp;
      return True;
    }
  }

  FORCEASSERT("Not Found");
  return False;
}

void DeployBase::setToNewMap(DeployItem& di, const RefBattleSP& sp)
{
  sp->setMoving(True);
  di.d_wantSP = sp;
}



bool DeployBase::getHex(HexCord& hex, const HexCord::HexDirection hd,
  const HexCord startHex)
{
  if(d_batData->moveHex(startHex, hd, hex))
  {
    return True;
  }

  FORCEASSERT("Hex not found! -- getHex()");
  return False;
}

bool DeployBase::canMoveInto(const RefBattleCP& cp, const HexCord& startHex,
  const HexCord::HexDirection hd, const int newColumn)
{
  HexCord hex;
  if(d_batData->moveHex(startHex, hd, hex))
  {
    /*
     * See if hex is in new map
     */

    for(int r = 0; r < cp->wantRows(); r++)
    {
      for(int c = 0; c < cp->wantColumns(); c++)
      {
        DeployItem& cdi = cp->deployItem(c, r);

        if(cdi.d_wantHex == hex)
          return (c == newColumn);
      }
    }

    return True;
  }

  return False;
}

bool DeployBase::nextDirection(const RefBattleCP& cp, const HexCord& hex,
  HexCord::HexDirection& hd, HexPosition::Facing& newFace,
  const WhichSide ws, const int newColumn)
{
  // see if we can go to the front
  if(canMoveInto(cp, hex, leftFront(cp->facing()), newColumn))
    hd = leftFront(cp->facing());

  else if(canMoveInto(cp, hex, rightFront(cp->facing()), newColumn))
    hd = rightFront(cp->facing());

  // other wise go to left or right flank
  else
  {
    switch(ws)
    {
      case LeftSide:
        hd = leftFlank(cp->facing());
//      newFace = leftTurn(cp->facing());
        return True;

      case RightSide:
//      newFace = rightTurn(cp->facing());
        hd = rightFlank(cp->facing());
        return True;

      default:
        FORCEASSERT("Hex Direction not found");
        return False;
    }
  }

  return True;
}

bool DeployBase::deployLeft(const RefBattleCP& cp, const DeployItem& cdi, int row, Boolean fromLeft)
{
  if(fromLeft)
  {
    for(int c = cp->wantColumns() - 1; c >= 0; c--)
    {
      DeployItem& di = cp->deployItem(c, row);
      if(di.d_wantSP == NoBattleSP)
      {
        setToNewMap(di, cdi.d_sp);
//      plotDeployRoute(cp, di, c, row);
        return True;
      }
    }
  }

  else
  {
    for(int c = 0; c < cp->wantColumns(); c++)
    {
      DeployItem& di = cp->deployItem(c, row);
      if(di.d_wantSP == NoBattleSP)
      {
        setToNewMap(di, cdi.d_sp);
//      plotDeployRoute(cp, di, c, row);
        return True;
      }
    }
  }

  FORCEASSERT("Not found in deployLeft()");
  return False;
}

bool DeployBase::deployCenter(const RefBattleCP& cp, const DeployItem& cdi,
   int row, WhichSide whichSide, Boolean extended)
{
  for(int c = 0; c + 1 < cp->wantColumns(); c++)
  {
    DeployItem& di = cp->deployItem(c, row);
    DeployItem& ndi = cp->deployItem(c + 1, row);

    if(whichSide == LeftSide)
    {
      if(extended)
        return deployLeft(cp, cdi, row);

      else if( (di.d_wantSP == NoBattleSP) &&
               (ndi.d_wantSP != NoBattleSP) )
      {
        setToNewMap(di, cdi.d_sp);
//      plotDeployRoute(cp, di, c, row);
        return True;
      }
    }
    else
    {
      if( (di.d_wantSP != NoBattleSP) &&
          (ndi.d_wantSP == NoBattleSP) )
      {
        setToNewMap(ndi, cdi.d_sp);
//      plotDeployRoute(cp, ndi, c + 1, row);
        return True;
      }
    }
  }

  FORCEASSERT("Not found in deployCenter()");
  return False;
}

bool DeployBase::deployRight(const RefBattleCP& cp, const DeployItem& cdi, int row)
{
  for(int c = 0; c < cp->wantColumns(); c++)
  {
    DeployItem& di = cp->deployItem(c, row);
    if(di.d_wantSP == NoBattleSP)
    {
      setToNewMap(di, cdi.d_sp);
//    plotDeployRoute(cp, di, c, row);
      return True;
    }
  }

  FORCEASSERT("Not found in deployRight()");
  return False;
}

bool DeployBase::plotAdjustedRoute(const RefBattleCP& cp)
{
  ASSERT(cp->rows() == cp->wantRows());
  ASSERT(cp->columns() == cp->wantColumns());
  Boolean adjusted = False;
  for(int r = 0; r < cp->wantRows(); r++)
  {
    for(int c = 0; c < cp->wantColumns(); c++)
    {
      DeployItem& di = cp->deployItem(c, r);
      if(!di.active())
        continue;

      ASSERT(di.d_wantSP == NoBattleSP);
      ASSERT(di.d_sp->hex() == di.d_hex);
      di.d_wantSP = di.d_sp;
      if(di.d_hex != di.d_wantHex)
      {
        di.d_sp->routeList().reset();

        B_Route br(batData(), cp, di.d_sp);
        if(!br.plotRoute(di.d_hex, di.d_wantHex, 0))
          FORCEASSERT("Route not found");
      }
    }
  }

  return adjusted;
}

bool DeployBase::adjustDeployment(const RefBattleCP& cp)
{
   // plot destination hexes
   if(cp->sp() != NoBattleSP && cp->columns() == 2)
   {
     // useful constants
     const int spCount = cp->spCount();
     ASSERT(spCount > 0);

     HexCord hex = (cp->deployWhichWay() == CPD_DeployLeft) ? cp->rightHex() :
        (cp->deployWhichWay() == CPD_DeployCenter) ? cp->centerHex() : cp->leftHex();

     // get new deploy map
     HexArea area;
     findDeployHexes(cp, cp->formation(), area);

     if(plotAdjustedRoute(cp))
     {
       cp->moveMode(BattleCP::Deploying);

       RefBattleCP attachedTo = BobUtility::getAttachedLeader(batData(), cp);
       if(attachedTo != NoBattleCP)
       {
         plotHQRoute(batData(), attachedTo);
         attachedTo->setMoving(True);
       }

       return True;
     }
   }

   return False;
}

DeployBase::WhichSide DeployBase::getWhichSide(const RefBattleCP& cp, int c, int r)
{
  WhichSide ws = RightSide;
  Boolean found = False;

  // find which way we want to move
  switch(cp->deployWhichWay())
  {
    case CPD_DeployLeft:
      ws = LeftSide;
      break;

    case CPD_DeployCenter:
      if( (c >= (cp->columns() / 2)) ||
          (r == cp->rows() - 1 && (cp->rows() % 2) == 0) )
      {
        ws = RightSide;
      }
      else
      {
        ws = LeftSide;
      }
      break;

    case CPD_DeployRight:
      ws = RightSide;
      break;

#ifdef DEBUG
    default:
      FORCEASSERT("Hex Direction not found");
#endif
  }

  return ws;
}

bool DeployBase::getDestHex(const RefBattleSP& sp, std::vector<DeployItem>& map, HexCord& destHex)
{
  for(std::vector<DeployItem>::iterator wantDI = map.begin();
      wantDI != map.end();
      wantDI++)
  {
    if(wantDI->active() && wantDI->d_wantSP == sp)
    {
      destHex = wantDI->d_wantHex;
      return True;
    }
  }

  return False;
}

/*---------------------------------------------------------------------
 * Derived deploy classes
 */

// 'March'
class DeployMarch : public DeployBase {
  public:
    DeployMarch(RPBattleData bd) : DeployBase(bd) {}
    ~DeployMarch() {}

    void deploySP(const RefBattleCP& cp, const int spCount);
    void redeploySP(const RefBattleCP& cp, const int spCount);
    DeployBase::SPPosition findRedeployPosition(int oldC, int oldR,
      CPDeployHow whichWay, int spCount);
    bool addExcludeHexes(const RefBattleCP& cp, HexList& excludeList);
};

bool DeployMarch::addExcludeHexes(const RefBattleCP& cp, HexList& excludeList)
{
#if 0
  std::vector<DeployItem>& map = cp->deployMap();

  // redeploying
  ASSERT(cp->formation() == CPF_Massed);
  {
    HexCord::HexDirection ld = (cp->deployWhichWay() == CPD_DeployRight) ?
             leftFlank(cp->facing()) : rightFlank(cp->facing());
    HexCord::HexDirection rd = (cp->deployWhichWay() == CPD_DeployRight) ?
             rightFlank(cp->facing()) : leftFlank(cp->facing());

    int lc = (cp->deployWhichWay() == CPD_DeployRight) ? 0 : 1;
    int rc = (lc == 0) ? 1 : 0;
    // exclude all hexes to the left of next deployment
    for(int r = 0; r < cp->wantRows(); r++)
    {
      DeployItem& di = map[maximum(0, (cp->wantColumns() * r) + lc)];
      if(!di.active())
        continue;

      DeployItem* ldi = (r > 0) ? &map[maximum(0, (cp->wantColumns() * r - 1) + lc)] : 0;

      if(r == 0 ||
         ldi && ldi->active() && canExclude(cp->facing(), di.d_wantHex, ldi->d_wantHex))
      {
        HexCord eh;
        if(batData()->moveHex(di.d_wantHex, ld, eh))
          excludeList.newItem(eh);

        if(r > cp->rows())
        {
          if(batData()->moveHex(di.d_wantHex, rd, eh))
            excludeList.newItem(eh);
        }
      }
      else
        break;
    }
  }
#endif
  return True;
}

DeployBase::SPPosition DeployMarch::findRedeployPosition(int oldC, int oldR,
  CPDeployHow whichWay, int spCount)
{
  ASSERT(oldR < MaxSP);
  ASSERT(oldR <= spCount);

  static const SPPosition s_right[MaxSP] = {
     { 0, 0 },
     { 0, 1 },
     { 1, 0 },
     { 1, 1 },
     { 2, 0 },
     { 2, 1 },
     { 3, 0 },
     { 3, 1 },
     { 4, 0 },
     { 4, 1 },
     { 5, 0 },
     { 5, 1 }
  };

  static const SPPosition s_left[MaxSP] = {
     { 0, 1 },
     { 0, 0 },
     { 1, 1 },
     { 1, 0 },
     { 2, 1 },
     { 2, 0 },
     { 3, 1 },
     { 3, 0 },
     { 4, 1 },
     { 4, 0 },
     { 5, 1 },
     { 5, 0 }
  };

  const SPPosition* pos = (whichWay == CPD_DeployLeft) ? s_left : s_right;
  return pos[oldR];
}

void DeployMarch::deploySP(const RefBattleCP& cp, const int spCount)
{
  ASSERT(cp->formation() == CPF_Massed);
  ASSERT(cp->columns() == 2);

  std::vector<DeployItem>& map = cp->deployMap();
  for(int r = 0; r < cp->rows(); r++)
  {
    for(int c = 0; c < cp->columns(); c ++)
    {
      DeployItem& cdi = map[(cp->columns() * r) + c];

      if(cdi.active())
      {
        SPPosition spPos = findRedeployPosition(c, r, cp->deployWhichWay(), spCount);

        DeployItem& di = cp->deployItem(spPos.d_c, spPos.d_r);
        setToNewMap(di, cdi.d_sp);
      }
    }
  }

  plotDeployRoute(cp);
}


void DeployMarch::redeploySP(const RefBattleCP& cp, const int spCount)
{
#if 0
  int newR = 0;
  int newC = 0;

  std::vector<DeployItem>& map = cp->deployMap();
  int startC = (cp->deployWhichWay() == CPD_DeployRight) ? 0 : 1;
  int endC = (cp->deployWhichWay() == CPD_DeployRight) ? cp->columns() : -1;
  int incV = (cp->deployWhichWay() == CPD_DeployRight) ? 1 : -1;

  for(int r = 0; r < cp->rows(); r++)
  {
    for(int c = startC; c != endC; c += incV, newR++)
    {
      int index = maximum(0, (cp->columns() * r) + c);
      DeployItem& di = map[index];

      if(stayInPlace(cp, c, r))
        alreadyThere(di.d_sp, map);
      else
      {
        cp->addToMap(di.d_sp, 0, newC, newR);
        di.d_sp->setMoving(True);
      }
    }
  }
  plotDeployRoute(cp);
#endif
}

//----------------------------------------------------------------------
// 'Massed'
class DeployMassed : public DeployBase {
  public:
    DeployMassed(RPBattleData bd) : DeployBase(bd) {}
    ~DeployMassed() {}

    void deploySP(const RefBattleCP& cp, const int spCount);
    void redeploySP(const RefBattleCP& cp, const int spCount);
    bool addExcludeHexes(const RefBattleCP& cp, HexList& excludeList);
    SPPosition findDeployPosition(int oldC, int oldR,
      CPDeployHow whichWay, int spCount);
    SPPosition findRedeployPosition(int oldC, int oldR,
      CPDeployHow whichWay, int spCount);
};

DeployBase::SPPosition DeployMassed::findDeployPosition(int oldC, int oldR,
  CPDeployHow whichWay, int spCount)
{
  ASSERT(oldR < MaxSP);
  ASSERT(oldR <= spCount);

  static const SPPosition s_right[MaxSP] = {
     { 0, 0 },
     { 0, 1 },
     { 1, 0 },
     { 1, 1 },
     { 2, 0 },
     { 2, 1 },
     { 3, 0 },
     { 3, 1 },
     { 4, 0 },
     { 4, 1 },
     { 5, 0 },
     { 5, 1 }
  };

  static const SPPosition s_left[MaxSP] = {
     { 0, 1 },
     { 0, 0 },
     { 1, 1 },
     { 1, 0 },
     { 2, 1 },
     { 2, 0 },
     { 3, 1 },
     { 3, 0 },
     { 4, 1 },
     { 4, 0 },
     { 5, 1 },
     { 5, 0 }
  };

  const SPPosition* pos = (whichWay == CPD_DeployLeft) ? s_left : s_right;
  return pos[oldR];
}

DeployBase::SPPosition DeployMassed::findRedeployPosition(int oldC, int oldR,
  CPDeployHow whichWay, int spCount)
{
  ASSERT(oldR < MaxSP / 2);
  const int maxColumns = 2;
  static const SPPosition s_right[MaxSP / 2][maxColumns] = {
     { {  0, 0 }, {  1, 0} },
     { {  2, 0 }, {  3, 0} },
     { {  4, 0 }, {  5, 0} },
     { {  6, 0 }, {  7, 0} },
     { {  8, 0 }, {  9, 0} },
     { { 10, 0 }, { 11, 0} }
  };

  static const SPPosition s_left[MaxSP][maxColumns] = {
     { {  1, 0 }, {  0, 0} },
     { {  3, 0 }, {  2, 0} },
     { {  5, 0 }, {  4, 0} },
     { {  7, 0 }, {  6, 0} },
     { {  9, 0 }, {  8, 0} },
     { { 11, 0 }, { 10, 0} }
  };

  return (whichWay == CPD_DeployLeft) ?
     s_left[oldR][oldC] : s_right[oldR][oldC];
}

// exclude hexes from movement
bool DeployMassed::addExcludeHexes(const RefBattleCP& cp, HexList& excludeList)
{
#if 0
  std::vector<DeployItem>& map = cp->deployMap();

  // deploying
  if(cp->formation() < cp->nextFormation())
  {
    HexCord::HexDirection ld = (cp->deployWhichWay() == CPD_DeployRight) ?
             leftFlank(cp->facing()) : rightFlank(cp->facing());
    HexCord::HexDirection rd = (cp->deployWhichWay() == CPD_DeployRight) ?
             rightFlank(cp->facing()) : leftFlank(cp->facing());

    // exclude all hexes to the left of current deployment
    for(int r = 0; r < cp->rows(); r++)
    {
      DeployItem& di = map[maximum(0, (cp->columns() * r) + 0)];
      if(!di.active())
        continue;

      DeployItem* ldi = (r > 0) ? &map[maximum(0, (cp->columns() * r - 1) + 0)] : 0;

      if(r == 0 ||
         ldi && ldi->active() && canExclude(cp->facing(), di.d_sp->hex(), ldi->d_sp->hex()))
      {
        HexCord eh;
        if(batData()->moveHex(di.d_hex, ld, eh))
            excludeList.newItem(eh);

        if(r > cp->wantRows() && batData()->moveHex(di.d_hex, rd, eh))
            excludeList.newItem(eh);
      }
      else
        break;
    }
  }

#endif
  return False;
}

void DeployMassed::deploySP(const RefBattleCP& cp, const int spCount)
{
  // if sp count is 1 or less, don't do anything;
  if(spCount <= 1)
    return;

  ASSERT(cp->columns() == 1);
  ASSERT(cp->wantColumns() == 2);
  std::vector<DeployItem>& map = cp->deployMap();

  /*
   * go through old map and reassign sp's to new map
   */

//  int i = 0;    // cannot exceed # SP's
  for(int r = 0; r < cp->rows(); r++) //, i++)
  {
    const int c = 0;
    DeployItem& cdi = map[(cp->columns() * r) + c];

    if(!cdi.active())
      continue;

    SPPosition spPos = (cp->formation() < cp->nextFormation()) ?
       findDeployPosition(c, r, cp->deployWhichWay(), spCount) :
       findRedeployPosition(c, r, cp->deployWhichWay(), spCount);

    DeployItem& di = cp->deployItem(spPos.d_c, spPos.d_r);
    setToNewMap(di, cdi.d_sp);
  }

  plotDeployRoute(cp);
}

void DeployMassed::redeploySP(const RefBattleCP& cp, const int spCount)
{
}

//---------------------------------------------------------------
// 'Deployed'
class DeployDeployed : public DeployBase {
  public:
    DeployDeployed(RPBattleData bd) : DeployBase(bd) {}
    ~DeployDeployed() {}

    void deploySP(const RefBattleCP& cp, const int spCount);
    void redeploySP(const RefBattleCP& cp, const int spCount);
    SPPosition findDeployPosition(int oldC, int oldR,
      CPDeployHow whichWay, int spCount);
    SPPosition findRedeployPosition(int oldC, int oldR,
      CPDeployHow whichWay, int spCount);
    bool addExcludeHexes(const RefBattleCP& cp, HexList& excludeList)
    {
      return True;
    }
};

DeployBase::SPPosition DeployDeployed::findDeployPosition(int oldC, int oldR,
      CPDeployHow whichWay, int spCount)
{
  const int c_cutOff = 4;
  const int c_maxRows = 12;
  const int c_maxColumns = 2;
  ASSERT(oldR < c_maxRows - c_cutOff);
  ASSERT(oldC < c_maxColumns);
  ASSERT(spCount - (c_cutOff + 1) >= 0);
  ASSERT(spCount - (c_cutOff + 1) < c_maxRows - c_cutOff);

  static const SPPosition s_right[c_maxRows - c_cutOff][MaxSP / 2][c_maxColumns] = {
    { // 5 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 2 },                                         {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 6 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  1, 2 }, {  0, 2 } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 7 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 8 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  1, 3 } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 9 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  1, 3 } },
      { {  0, 4 }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 10 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  1, 3 } },
      { {  1, 4 }, {  0, 4 } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 11 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  1, 3 } },
      { {  1, 4 }, {  0, 4 } },
      { {  0, 5 }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 12 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  1, 3 } },
      { {  1, 4 }, {  0, 4 } },
      { {  0, 5 }, {  1, 5 } },
    },
  };

  static const SPPosition s_center[c_maxRows - c_cutOff][MaxSP / 2][c_maxColumns] = {
    { // 5 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 2 },                                         {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 6 SP
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  1, 2 }, {  0, 2 } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 7 SP
      { {  0, 1 }, {  0, 2 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 0 }, {  0, 3 } },
      { {  1, 0 }, {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 8 SP
      { {  0, 1 }, {  0, 2 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 0 }, {  0, 3 } },
      { {  1, 0 }, {  1, 3 } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 9 SP
      { {  0, 1 }, {  0, 2 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 0 }, {  0, 3 } },
      { {  1, 0 }, {  1, 3 } },
      { {  0, 4 }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 10 SP
      { {  0, 1 }, {  0, 2 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 0 }, {  0, 3 } },
      { {  1, 0 }, {  1, 3 } },
      { {  1, 4 }, {  0, 4 } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 11 SP
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  1, 3 } },
      { {  0, 1 }, {  0, 4 } },
      { {  1, 1 }, {  1, 4 } },
      { {  0, 0 }, {  0, 5 } },
      { {  1, 0 }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 12 SP
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  1, 3 } },
      { {  0, 1 }, {  0, 4 } },
      { {  1, 1 }, {  1, 4 } },
      { {  0, 0 }, {  0, 5 } },
      { {  1, 0 }, {  1, 5 } },
    },
  };

  static const SPPosition s_left[c_maxRows - c_cutOff][MaxSP / 2][c_maxColumns] = {
    { // 5 SP
      { {  0, 1 }, {  0, 2 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 0 },                                         {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 6 SP
      { {  0, 1 }, {  0, 2 } },
      { {  1, 1 }, {  1, 2 } },
      { {  0, 0 }, {  1, 0 } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 7 SP
      { {  0, 2 }, {  0, 3 } },
      { {  1, 1 }, {  1, 2 } },
      { {  0, 1 }, {  1, 0 } },
      { {  0, 0 }, {  SPPosition::Undefined, SPPosition::Undefined } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 8 SP
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  1, 3 } },
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 9 SP
      { {  0, 3 }, {  0, 4 } },
      { {  1, 2 }, {  1, 3 } },
      { {  0, 1 }, {  0, 2 } },
      { {  1, 0 }, {  1, 1 } },
      { {  0, 0 }, {  SPPosition::Undefined, SPPosition::Undefined} },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 10 SP
      { {  0, 3 }, {  0, 4 } },
      { {  1, 3 }, {  1, 4 } },
      { {  0, 1 }, {  0, 2 } },
      { {  1, 1 }, {  1, 2 } },
      { {  0, 0 }, {  1, 0 } },
      { {  SPPosition::Undefined, SPPosition::Undefined }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 11 SP
      { {  0, 4 }, {  0, 5 } },
      { {  1, 3 }, {  1, 4 } },
      { {  0, 2 }, {  0, 3 } },
      { {  1, 1 }, {  1, 2 } },
      { {  0, 1 }, {  1, 0 } },
      { {  0, 0 }, {  SPPosition::Undefined, SPPosition::Undefined} },
    },
    { // 12 SP
      { {  0, 4 }, {  0, 5 } },
      { {  1, 4 }, {  1, 5 } },
      { {  0, 2 }, {  0, 3 } },
      { {  1, 2 }, {  1, 3 } },
      { {  0, 0 }, {  0, 1 } },
      { {  1, 0 }, {  1, 1 } },
    },
  };

  return (whichWay == CPD_DeployLeft) ? s_left[spCount - (c_cutOff + 1)][oldR][oldC] :
         (whichWay == CPD_DeployRight) ? s_right[spCount - (c_cutOff + 1)][oldR][oldC] :
         s_center[spCount - (c_cutOff + 1)][oldR][oldC];
}

DeployBase::SPPosition DeployDeployed::findRedeployPosition(int oldC, int oldR,
      CPDeployHow whichWay, int spCount)
{
  SPPosition spPos;
  return spPos;
}

void DeployDeployed::deploySP(const RefBattleCP& cp, const int spCount)
{
  // if spCount is 4 or less, do nothing
  bool deploying = (cp->formation() < cp->nextFormation());
  if( (deploying && spCount <= 4) )
    return;

  ASSERT(cp->columns() == 2);
  std::vector<DeployItem>& map = cp->deployMap();

  /*
   * go through old map and reassign sp's to new map
   */

  for(int r = 0; r < cp->rows(); r++)
  {
    for(int c = 0; c < cp->columns(); c++)
    {
      DeployItem& cdi = map[(cp->columns() * r) + c];

      if(!cdi.active())
        continue;

      SPPosition spPos = (cp->formation() < cp->nextFormation()) ?
         findDeployPosition(c, r, cp->deployWhichWay(), spCount) :
         findRedeployPosition(c, r, cp->deployWhichWay(), spCount);

      ASSERT(spPos.d_r != SPPosition::Undefined);
      ASSERT(spPos.d_c != SPPosition::Undefined);
      DeployItem& di = cp->deployItem(spPos.d_c, spPos.d_r);
      setToNewMap(di, cdi.d_sp);
    }
  }

  plotDeployRoute(cp);

}

void DeployDeployed::redeploySP(const RefBattleCP& cp, const int spCount)
{
}

//-------------------------------------------------------------------------
// 'Extended'
class DeployExtended : public DeployBase {
  public:
    DeployExtended(RPBattleData bd) : DeployBase(bd) {}
    ~DeployExtended() {}

    void deploySP(const RefBattleCP& cp, const int spCount);
    void redeploySP(const RefBattleCP& cp, const int spCount);
    bool addExcludeHexes(const RefBattleCP& cp, HexList& excludeList)
    {
      return True;
    }
};

void DeployExtended::deploySP(const RefBattleCP& cp, const int spCount)
{
   /*
    * Special case of 2 or less SP
    */

   if(spCount <= 2)
   {
     // don't do nothing
     return;
   }

  ASSERT(cp->rows() == 2);
  std::vector<DeployItem>& map = cp->deployMap();

  /*
   * go through old map and reassign sp's to new map
   */

  for(int r = 0; r < cp->rows(); r++)
  {
    for(int c = 0; c < cp->columns(); c++)
    {
      int index = maximum(0, (cp->columns() * r) + c);
      DeployItem& cdi = map[index];

      if(!cdi.active())
        continue;

      // if rows 1 we are already where we want to be
      if(r < 1)
      {
        alreadyThere(cdi.d_sp, map);
      }

      // row 2
      else
      {
        WhichSide whichSide = (c < (cp->columns() / 2)) ? LeftSide : RightSide;
        const int newRow = 0;

        switch(cp->deployWhichWay())
        {
          case CPD_DeployRight:
            deployRight(cp, cdi, newRow);
            break;

          case CPD_DeployCenter:
            deployCenter(cp, cdi, newRow, whichSide, True);
            break;

          case CPD_DeployLeft:
            deployLeft(cp, cdi, newRow, False);
            break;
        }
      }
    }
  }

  plotDeployRoute(cp);
}

void DeployExtended::redeploySP(const RefBattleCP& cp, const int spCount)
{
}

//------------------------------------------------------------------
void DeployBase::deploy(const RefBattleCP& cp)
{
   ASSERT(cp->formation() != cp->nextFormation());

   HexArea area;

   // plot destination hexes
   if(cp->sp() != NoBattleSP)
   {
     // useful constants
     const HexPosition::Facing facing = cp->facing();
     const int spCount = cp->spCount();
     ASSERT(spCount > 0);

     // get new deploy map
     findDeployHexes(cp, cp->nextFormation(), area);

     // deploying
//   if(cp->nextFormation() > cp->formation())
       deploySP(cp, spCount);

     // redeploying (going in reverse!)
//   else
//     redeploySP(cp, spCount);

//   getCPDest(d_batData, cp, area);
   }

    RefBattleCP attachedTo = BobUtility::getAttachedLeader(batData(), cp);
    if(attachedTo != NoBattleCP)
    {
      plotHQRoute(batData(), attachedTo);
      attachedTo->setMoving(True);
    }

#if 0
   B_Route br(batData(), cp, cp);
   br.plotRoute(cp->hex(), cp->destHex(), 0);
#endif
}

/*-----------------------------------------------------------
 * Allocator for derived classes
 */

static DeployBase* DeployBase::allocate(RPBattleData bd, CPFormation f)
{
  if(f == CPF_March)
    return new DeployMarch(bd);

  else if(f == CPF_Massed)
    return new DeployMassed(bd);

  else if(f == CPF_Deployed)
    return new DeployDeployed(bd);

  else if(f == CPF_Extended)
    return new DeployExtended(bd);

  else
    return 0;
}


/*----------------------------------------------------------------
 * Client access
 */

Deploy_Int::Deploy_Int(RPBattleData batData) :
  d_formations(new DeployBase*[CPF_Last])
{
  ASSERT(d_formations);
  for(CPFormation cf = CPF_First; cf < CPF_Last; INCREMENT(cf))
  {
    d_formations[cf] = DeployBase::allocate(batData, cf);
    ASSERT(d_formations[cf]);
  }
}

Deploy_Int::~Deploy_Int()
{
  for(CPFormation cf = CPF_First; cf < CPF_Last; INCREMENT(cf))
  {
    delete d_formations[cf];
  }

  delete[] d_formations;
}

void Deploy_Int::deploy(const RefBattleCP& cp)
{
  ASSERT(cp->nextFormation() < CPF_Last);
  d_formations[cp->nextFormation()]->deploy(cp);
}

bool Deploy_Int::adjustDeployment(const RefBattleCP& cp)
{
  ASSERT(cp->nextFormation() < CPF_Last);
  return d_formations[cp->nextFormation()]->adjustDeployment(cp);
}


/*-----------------------------------------------------------
 *  Initialize Deployment
 */

class Instant_DeployUtility
{
   public:
      Instant_DeployUtility(RPBattleData batData);
      ~Instant_DeployUtility() { }
      void deployAll();

      typedef MinMaxRect<HexCord::Cord> HexArea;

   private:

      void deployOrgAndSisters(const RefBattleCP& cp, const HexCord& hex, HexPosition::Facing facing, HexArea* area);
         // Deploy a unit and its sisters around the hex
      void deployOrganization(const RefBattleCP& cp, const HexCord& hex, HexPosition::Facing facing, HexArea* area);
         // Deploy a unit and all below centred roughly on hex

      void placeSP(const RefBattleSP& sp, const HexCord& hex);
         // Set an SP's position and update hexMap
      void placeCP(const RefBattleCP& cp, const HexCord& hex);
         // Set an CP's position and update hexMap


   private:

      // Handy variables

      PBattleData d_batData;
      BattleOB* d_ob;
      const TerrainTable& d_terrainTable;
      const BattleBuildings::BuildingTable* d_buildings;
      const BattleBuildings::BuildingList* d_buildingList;
      const BattleMap* d_map;
      BattleHexMap*  d_hexMap;

};

Instant_DeployUtility::Instant_DeployUtility(RPBattleData batData) :
   d_batData(batData),
   d_terrainTable(batData->terrainTable())
{
   // Get information from batdata

   d_ob = batData->ob();
   // d_terrainTable = batData->terrainTable();
   d_buildings = batData->buildingTable();
   d_buildingList = batData->buildingList();
   d_map = batData->map();
   d_hexMap = batData->hexMap();

   ASSERT(d_ob != 0);
   ASSERT(d_buildings != 0);
   ASSERT(d_buildingList != 0);
   ASSERT(d_map != 0);
   ASSERT(d_hexMap != 0);
}

/*
 * Process top level units on each side
 *
 */

void Instant_DeployUtility::deployAll()
{
#ifdef DEBUG
      bobLog.printf("------------------------");
#endif

   for(Side side = 0; side < d_ob->sideCount(); ++side)
   {
#ifdef DEBUG
      bobLog.printf("Deploying Side %d", static_cast<int>(side));
#endif
      // Count how many SPs there are altogether

      int spCount = BobUtility::countSP(d_ob, side);

#ifdef DEBUG
      bobLog.printf("Side %d has %d SPs",
         static_cast<int>(side),
         spCount);
#endif

      const HexCord& mapSize = d_map->getSize();
      HexPosition::Facing facing;
      HexCord hex;
      hex.x(mapSize.x()/2);
      ASSERT((side == 0) || (side == 1));
      if(side == 0)
      {
         facing = HexPosition::North;
         hex.y(mapSize.y()/4);
      }
      else if(side == 1)
      {
         facing = HexPosition::South;
         hex.y(mapSize.y() - mapSize.y() / 4);
      }

      HexArea area;

      RefBattleCP cp = d_ob->getTop(side);
      if(cp != NoBattleCP)
         deployOrgAndSisters(cp, hex, facing, &area);

#ifdef DEBUG
      bobLog.printf("------------------------");
#endif
   }
}

/*
 * Recursive function to deploy an organization
 * At the moment all units are put in top of each other!
 *
 * These 2 functions are mutually exclusive
 */


void Instant_DeployUtility::deployOrgAndSisters(const RefBattleCP& cp, const HexCord& hex, HexPosition::Facing facing, HexArea* area)
{
   ASSERT(cp != NoBattleCP);

#ifdef DEBUG
   bobLog.printf("Deploying %s and children at %d,%d, facing=%d",
      cp->getName(),
      static_cast<int>(hex.x()),
      static_cast<int>(hex.y()),
      static_cast<int>(facing));
#endif

   ASSERT(area != 0);

   area->set(hex);

   HexCord::Cord edge[2];
   MinMax<HexCord::Cord> edges = hex.x();
   enum { Centre, Left, Right } spread = Centre;


   // Count sisters

   int sisterCount = 0;

   {  // braces needed until compiler complies with new standard
      for(RefBattleCP unit = cp; unit != NoBattleCP; unit = unit->sister())
      {
         ++sisterCount;
      }
   }

   // If even number, then start off offset

   if( (sisterCount & 1) == 0)
      spread = Left;

   {  // braces needed until compiler complies with new standard
      for(RefBattleCP unit = cp; unit != NoBattleCP; unit = unit->sister())
      {
         HexCord unitPosition = hex;
         if(spread == Centre)
         {
            unitPosition = hex;
         }
         else
         {
            // Get approximate width of unit
            // Assume that they are 2 deep with a one hex gap

            int spCount = BobUtility::countSP(unit);
            int unitCount = BobUtility::countUnits(unit);

            int front = 1 + (spCount + 1) / 2 + unitCount;
            int spacing = 1 + (front + 1) / 2;

            // Set unit centre to current edge + half of width

            if(spread == Left)
            {
               unitPosition.x(edges.minValue() - spacing);
               unitPosition.y(hex.y());
            }
            else if(spread == Right)
            {
               unitPosition.x(edges.maxValue() + spacing);
               unitPosition.y(hex.y());
            }
            else
               FORCEASSERT("Illegal value for spread");
         }

         HexArea newArea;
         deployOrganization(unit, unitPosition, facing, &newArea);
         area->combine(newArea);

         // HexArea newArea;
         // deployOrgAndChildren(unit->child(), unitPosition, facing, &newArea);
         // area->combine(newArea);

         edges += newArea.x();
         spread = (spread == Left) ? Right : Left; // Centre, Left, Right, Left, Right, ...
      }
   }

#ifdef DEBUG
   bobLog << "area=" << *area << endl;
#if 0
   bobLog.printf("area=%d..%d,%d..%d",
      static_cast<int>(area->minX()),
      static_cast<int>(area->maxX()),
      static_cast<int>(area->minY()),
      static_cast<int>(area->maxY()));
#endif
#endif
}


void Instant_DeployUtility::deployOrganization(const RefBattleCP& cp, const HexCord& hex, HexPosition::Facing facing, HexArea* area)
{
   ASSERT(cp != NoBattleCP);
   ASSERT(area != 0);

   RefBattleCP child = cp->child();
   if(child != NoBattleCP)
      deployOrgAndSisters(cp->child(), hex, facing, area);

   // Place any strength points
   // for now we place in a single column

// int spCount = BobUtility::countSP(cp);
   HexCord spHex = hex;
   const int columns = 1;//    = (spCount + 1) / 2;

   BobUtility::allocateDeployMap(d_batData->ob(), cp);
   cp->moveMode(BattleCP::Holding);

   int rowCount = 0;

   int yOffset = (facing < HexPosition::West) ? -1 : +1;

// SPPosition::SPSide whichSide = ((hex.y() % 2) == 0) ?
//    SPPosition::SP_LeftSide : SPPosition::SP_RightSide;

   BattleCP::HexOffset whichWay = ((hex.y() % 2) == 0) ?
      BattleCP::HO_Left : BattleCP::HO_Right;

   BattleSPIter spIter(d_batData->ob(), cp);
   while(!spIter.isFinished())
   {
#ifdef DEBUG
      bobLog.printf("Placing SP %d,%s at %d,%d",
         (int) d_ob->getIndex(spIter.sp()),
         d_ob->spName(spIter.sp()),
         static_cast<int>(hex.x()),
         static_cast<int>(hex.y()));
#endif
      cp->addToMap(spIter.sp(), &spHex, 0, rowCount++);
      placeSP(spIter.sp(), spHex);
      area->update(spHex);

      spHex.y(spHex.y() + yOffset);


      // should really set the facing to the formations
      // but for debugging the display functions we want a variety
#ifdef DEBUG_DISPLAY
      static const ULONG RandomSeed = 0xEA230872;
      static RandomNumber obRand(RandomSeed);

      spIter.sp()->setFormation(SPFormation(obRand(0, SP_Formation_HowMany-1)));
      spIter.sp()->setFacing(obRand(0, HexPosition::FacingResolution-1));
#else
      // if this is an artillery unit, limber them up if not already so
      // TODO: sort out prolonging
#if 0
      if( (cp->getRank().sameRank(Rank_Division)) &&
          (cp->generic()->isArtillery()) )
      {
        spIter.sp()->setFormation(SP_UnlimberedFormation);
      }
      else
#endif
        spIter.sp()->setFormation(SP_Default);

      spIter.sp()->setFacing(facing);
      spIter.sp()->nextFacing(facing);
#endif

      spIter.next();
   }

   for(int i = 0; i < cp->deployMap().size(); i++)
     cp->lockMapItem(i);
   // Place as back average of all lower units positions


   HexCord cpHex = hex;
   if(facing < HexPosition::West)
      cpHex.y(area->minY() - 1);
   else
      cpHex.y(area->maxY() + 1);

#ifdef DEBUG
   bobLog.printf("Placing %s at %d,%d, facing=%d, spread=%d",
      cp->getName(),
      static_cast<int>(cpHex.x()),
      static_cast<int>(cpHex.y()),
      static_cast<int>(facing));
#endif

   cp->setFacing(facing);
   cp->syncMapToSP();

   if(cp->showHQ())
     placeCP(cp, cpHex);

   area->update(cpHex);
   BobUtility::countArtillery(d_batData->ob(), cp);
}

/*
 * Set an SP's position and update hexMap
 */

void Instant_DeployUtility::placeSP(const RefBattleSP& sp, const HexCord& hex)
{
#ifdef DEBUG
   d_batData->assertHexOnMap(hex);
#endif
   HexPosition hp(moveRight(sp->facing()), static_cast<HexPosition::HexDistance>(sp->hexPosition().centerPos()));
   sp->position(BattlePosition(hex, hp));
   d_hexMap->add(sp);
}

/*
 * Set an CP's position and update hexMap
 */

void Instant_DeployUtility::placeCP(const RefBattleCP& cp, const HexCord& hex)
{
#ifdef DEBUG
   d_batData->assertHexOnMap(hex);
#endif
   HexPosition hp(moveRight(cp->facing()), static_cast<HexPosition::HexDistance>(cp->hexPosition().centerPos()));
   cp->position(BattlePosition(hex, hp));
   d_hexMap->add(cp);
}

/*
 * Deploy Units on Battlefield
 */

void deployUnits(BattleData* batData)
{
   ASSERT(batData != 0);

   Instant_DeployUtility deployUtil(batData);
   deployUtil.deployAll();
}

};


























// plot road movement. Warning!! Recursive function!!
bool B_Route::plotRoadRoute(const HexCord& srcHex, const HexCord& destHex,
        const HexCord::HexDirection lastHD,
        HexList& destList, HexList& crossRoads, UWORD& totalCost)
{
  ASSERT(srcHex != destHex);
  HexList list;
  UWORD bestCost = UWORD_MAX;

  // For now, plot only if start and end hexes are road / track hexes
  if(!roadHex(d_batData, srcHex) || !roadHex(d_batData, destHex))
         return False;

  // terrain data
  const BattleTerrainHex& hi = d_batData->getTerrain(srcHex);

  // get number of pathes leading out of hex
  Util::HexRoadData hd[HexCord::Count];
  const int nPaths = Util::numberPaths(hi, lastHD, hd);
  ASSERT(nPaths > 0);

  // if a crossroads, add to that list
  if(nPaths > 1)
         crossRoads.newItem(srcHex);

  // loop through each path, finding best way to go
  for(int i = 0; i < nPaths; i++)
  {
         ASSERT(hd[i].d_hd != HexCord::Stationary);

         // get next hex
         HexCord hex;
         if(d_batData->moveHex(srcHex, hd[i].d_hd, hex))
         {
                // it should be a road!
                ASSERT(roadHex(d_batData, hex));

                int tCost = 0;
                HexList hl;
                HexCord::HexDirection lastHD = hd[i].d_hd;
                PathType lastType = hd[i].d_type;

                // loop while we're going just one way
                for(;;)
                {
                  // if this route cost more than best route
                  // or hex is a crossroad already visited hex (circled around) break loop
                  tCost += Util::pathPoint(lastType);

                  if( (Util::hexInList(hex, crossRoads)) ||
                                (tCost >= bestCost) ||
                                (hex == srcHex) )
                  {
                         break;
                  }

                  // add item to local list
                  hl.newItem(hex);

                  // break loop if we're at destination
                  if(hex == destHex)
                  {
                         if(tCost < bestCost)
                         {
                                list = hl;
                                bestCost = tCost;
                         }
                         break;
                  }

                  // terrain data for new hex
                  const BattleTerrainHex& hi2 = d_batData->getTerrain(hex);

                  // find paths leading out of new hex
                  Util::HexRoadData hd2[HexCord::Count];
                  int nNextPaths = Util::numberPaths(hi2, lastHD, hd2);
                  if(nNextPaths == 0)
                         break;

                  // if we have more than one path, recurse
                  if(nNextPaths > 1)
                  {
                         // recursive!!
                         HexList tList;
                         UWORD cost = 0;
                         if( (plotRoadRoute(hex, destHex, lastHD, tList, crossRoads, cost)) &&
                                  ((cost + tCost) < bestCost) )
                         {
                                bestCost = cost + tCost;

                                SListIterR<HexItem> iter(&tList);
                                while(++iter)
                                  hl.newItem(iter.current()->d_hex);

                                list = hl;
                         }

                         break;
                  }

                  // otherwise, keep looping
                  else
                  {
                         HexCord nextHex;
                         if(d_batData->moveHex(hex, hd2[0].d_hd, nextHex))
                         {
                                hex = nextHex;
                                lastHD = hd2[0].d_hd;
                                lastType = hd2[0].d_type;
                         }
                         else
                                break;
                  }
                }
         }
  }

  if(bestCost != UWORD_MAX)
  {
         totalCost += bestCost;
         destList = list;
  }

  return (list.entries() > 0);
}

























HexPosition::Facing Movement_Imp::roadFacing(const HexCord::HexDirection* enterHD,
      const HexCord::HexDirection* exitHD, const HexPosition::Facing currentFace)
{
  static const HexPosition::Facing s_exitNFacing[HexCord::Count] = {
     HexPosition::NorthEastPoint,
     HexPosition::North,
     HexPosition::North,
     HexPosition::NorthWestPoint,
     HexPosition::SouthWestPoint,
     HexPosition::SouthEastPoint
  };

  static const HexPosition::Facing s_exitSFacing[HexCord::Count] = {
     HexPosition::SouthEastPoint,
     HexPosition::NorthEastPoint,
     HexPosition::NorthWestPoint,
     HexPosition::SouthWestPoint,
     HexPosition::South,
     HexPosition::South
  };

  static const HexPosition::Facing s_enterNFacing[HexCord::Count] = {
     HexPosition::NorthWestPoint,
     HexPosition::South,
     HexPosition::South,
     HexPosition::NorthEastPoint,
     HexPosition::North,
     HexPosition::North
  };

  static const HexPosition::Facing s_enterSFacing[HexCord::Count] = {
     HexPosition::SouthWestPoint,
     HexPosition::North,
     HexPosition::North,
     HexPosition::SouthEastPoint,
     HexPosition::South,
     HexPosition::South
  };

  static const HexPosition::Facing s_bothN[HexCord::Count][HexCord::Count] = {
    { HexPosition::Facing_Undefined, HexPosition::North,            HexPosition::North,            HexPosition::NorthWestPoint,   HexPosition::South,            HexPosition::South },
    { HexPosition::NorthEastPoint,   HexPosition::Facing_Undefined, HexPosition::North,            HexPosition::NorthWestPoint,   HexPosition::South,            HexPosition::South },
    { HexPosition::NorthEastPoint,   HexPosition::North,            HexPosition::Facing_Undefined, HexPosition::NorthWestPoint,   HexPosition::South,            HexPosition::South },
    { HexPosition::NorthEastPoint,   HexPosition::North,            HexPosition::North,            HexPosition::Facing_Undefined, HexPosition::South,            HexPosition::South },
    { HexPosition::NorthEastPoint,   HexPosition::North,            HexPosition::North,            HexPosition::NorthWestPoint,   HexPosition::Facing_Undefined, HexPosition::South },
    { HexPosition::NorthEastPoint,   HexPosition::North,            HexPosition::North,            HexPosition::NorthWestPoint,   HexPosition::South,            HexPosition::Facing_Undefined }
  };

  static const HexPosition::Facing s_bothS[HexCord::Count][HexCord::Count] = {
    { HexPosition::Facing_Undefined, HexPosition::North         ,   HexPosition::North,            HexPosition::SouthWestPoint,   HexPosition::South,            HexPosition::South },
    { HexPosition::SouthEastPoint,   HexPosition::Facing_Undefined, HexPosition::North,            HexPosition::SouthWestPoint,   HexPosition::South,            HexPosition::South },
    { HexPosition::SouthEastPoint,   HexPosition::North,            HexPosition::Facing_Undefined, HexPosition::SouthWestPoint,   HexPosition::South,            HexPosition::South },
    { HexPosition::SouthEastPoint,   HexPosition::North,            HexPosition::North,            HexPosition::Facing_Undefined, HexPosition::South,            HexPosition::South },
    { HexPosition::SouthEastPoint,   HexPosition::North,            HexPosition::North,            HexPosition::SouthWestPoint,   HexPosition::Facing_Undefined, HexPosition::South },
    { HexPosition::SouthEastPoint,   HexPosition::North,            HexPosition::North,            HexPosition::SouthWestPoint,   HexPosition::South,            HexPosition::Facing_Undefined }
  };

  if(currentFace <= HexPosition::NorthWestPoint)
  {
    return (enterHD && !exitHD) ? s_enterNFacing[*enterHD] :
           (exitHD && !enterHD) ? s_exitNFacing[*exitHD] :
           (exitHD && enterHD)  ? s_bothN[*enterHD][*exitHD] : HexPosition::Facing_Undefined;
  }
  else
  {
    return (enterHD && !exitHD) ? s_enterSFacing[*enterHD] :
           (exitHD && !enterHD) ? s_exitSFacing[*exitHD] :
           (exitHD && enterHD)  ? s_bothS[*enterHD][*exitHD] : HexPosition::Facing_Undefined;
  }
}

void Movement_Imp::updateRoadFacing(const RefBattleCP& cp, const HexCord& srcHex,
  HexList& list, const HexPosition::Facing currentFace)
{
   ASSERT(list.entries() >= 2);
   BattleOrder& order = cp->getCurrentOrder();
   ASSERT(order.nextWayPoint());

   // set start facing
   HexPosition::Facing destFace = (order.nextWayPoint()) ?
      order.wayPoints().getLast()->d_order.d_facing : order.facing();

   HexCord::HexDirection lastHD = HexCord::Stationary;
   HexItem* last = list.first();
   while(last)
   {
     HexItem* next = list.next();
     if(next)
     {
       HexCord::HexDirection hd = hexDirection(d_batData, last->d_hex, next->d_hex);
       last->d_facing = (lastHD == HexCord::Stationary) ?
          roadFacing(0, &hd, destFace) : roadFacing(&lastHD, &hd, destFace);

       lastHD = HexCord::oppositeDirection(hd);
     }

     else
     {
       ASSERT(lastHD != HexCord::Stationary);
       last->d_facing = roadFacing(&lastHD, 0, destFace);
     }

     last = next;
   }
}

























#if 0
bool BU_MoveImp::movingOnRoad(const RefBattleCP& cp, PathType& pt)
{
   PathType roadType = LP_Road;
   if( (cp->sp()->fromFormation() == SP_MarchFormation && cp->formation() == CPF_March) )
   {
      bool onRoad = True;

      for(std::vector<DeployItem>::iterator di = cp->deployMap().begin();
            di != cp->deployMap().end();
            di++)
      {
         if(!di->active() || !di->d_sp->isMoving())
            continue;

         // if SP formation is 'March' with XX deployed in 'March'
      // (i.e. the whole XX in single file)
      // test to see if moving on road or track
      PathType p;
         if(!movingOnRoad(di->d_sp, p))
         {
        onRoad = False;
            break;
      }
      else
      {
            if( (roadType == LP_Road || roadType == LP_RoadBridge) &&
                  (p == LP_Track || p == LP_TrackBridge) )
        {
               roadType = LP_Track;
        }
         }
      }
      pt = roadType;
    return onRoad;
  }

  return False;

}


void HiZoneList::calcZonesUnitsOnMap(int nCorps, int nZones, Hi_OpPlan::Plan& plan)
{
#ifdef DEBUG
   gameInfo()->log("upDateZones(Zones=%d) -- Deployment Phase -- Units on Map", nZones);
#endif

   if(nZones <= 1)
      return;

   // first, find the left and right boundries of our side
   HexPosition::Facing avgFace = WG_BattleAI_Utils::averageFacing(gameInfo()->battleData(), gameInfo()->side());//gameInfo()->battleData()->ob()->getTop(gameInfo()->side()));

#ifdef DEBUG
   gameInfo()->log("Average facing = %d", static_cast<int>(avgFace));
#endif

   HexCord leftHex;
   HexCord rightHex;
   WG_BattleAI_Utils::LocalCPList frontCPs;
   ConstBattleUnitIter iter = gameInfo()->ourUnits();
   getSideLeftRightBoundry(gameInfo()->battleData(), iter, avgFace, leftHex, rightHex, &frontCPs);

#ifdef DEBUG
   gameInfo()->log("Left Boundry -> (%d, %d), Right Boundry -> (%d, %d)",
         static_cast<int>(leftHex.x()), static_cast<int>(leftHex.y()),
         static_cast<int>(rightHex.x()), static_cast<int>(rightHex.y()));

   {
      SListIterR<WG_BattleAI_Utils::LocalCP> iter(&frontCPs);
      while(++iter)
      {
         gameInfo()->log("%s is a front-line XXX", iter.current()->d_cp->getName());
      }
   }

#endif

   int nFrontCPs = frontCPs.entries();
   nZones = minimum(3, nFrontCPs);
   ASSERT(nZones > 0);

   HexCord zLeftHex = leftHex;
   while(nZones && frontCPs.entries() > 0)
   {
      RefBattleCP rightCP = NoBattleCP;
      if(nZones < frontCPs.entries())
      {
         int loop = (frontCPs.entries() / nZones) + 1;
         while(loop--)
         {
            if(frontCPs.entries() > 0)
            {
               rightCP = const_cast<BattleCP*>(frontCPs.first()->d_cp);
               frontCPs.remove(frontCPs.first());
            }
            else
               break;
         }
      }
      else
      {
         rightCP = const_cast<BattleCP*>(frontCPs.first()->d_cp);
         frontCPs.remove(frontCPs.first());
      }

      HexCord zRightHex;
      if(frontCPs.entries() > 0)
      {
//       RefBattleCP nextCP = const_cast<BattleCP*>(frontCPs.first()->d_cp);
         HexPosition::Facing corpAvgFace1 = WG_BattleAI_Utils::averageFacing(rightCP);
         HexCord corpLeft1;
         HexCord corpRight1;
         WG_BattleAI_Utils::getCorpLeftRightBoundry(rightCP, corpAvgFace1, corpLeft1, corpRight1);

         HexPosition::Facing corpAvgFace2 = WG_BattleAI_Utils::averageFacing(frontCPs.first()->d_cp);
         HexCord corpLeft2;
         HexCord corpRight2;
         WG_BattleAI_Utils::getCorpLeftRightBoundry(frontCPs.first()->d_cp, corpAvgFace2, corpLeft2, corpRight2);

         zRightHex.x((corpRight1.x() + corpLeft2.x()) / 2);
         zRightHex.y((corpRight1.y() + corpLeft2.y()) / 2);
      }
      else
         zRightHex = rightHex;

      HiZone* z = createZone(zLeftHex, zRightHex);
      ASSERT(z);

      nZones--;

      zLeftHex = zRightHex;
   }

}

void HiZoneList::updateDefensive(Hi_OpPlan::Plan plan)
{
   int nZones = size();
#ifdef DEBUG
   gameInfo()->log("upDateZones(Zone Count=%d) -- Defensive", nZones);
#endif

   const HexCord::Cord ourEdge = gameInfo()->whichEdge();
   const HexCord::Cord otherEdge = gameInfo()->otherEdge();
   const BattleMap* map = gameInfo()->map();
   RandomNumber& lRand = gameInfo()->random();

   for(int z = 0; z < nZones; z++)
   {
      HiZone* zone = (*this)[z];
      while(zone->nBoundries() > 0)
         zone->removeTopBoundry();

      HexCord leftHex;
      HexCord rightHex;
//    for(int phase = 0; phase < nPhases; phase++)
      {
         AdvBoundry ab = advanceZoneBoundries(z, nZones, plan, 0);

//       if(phase == 0)
         {
            leftHex = zone->leftHex();
            rightHex = zone->rightHex();
//          zone->removeTopBoundry();
//          ASSERT(zone->nBoundries() == 0);
         }

         HexPosition::Facing facing = WG_BattleAI_Utils::frontFacing(leftHex, rightHex);
//       int lrVar = lRand(1, (phase == 1) ? 2 : 4);
//       int fbVar = lRand(1, (phase == 1) ? 3 : 4);

         int leftLR = ab.d_leftLR;// + lRand.gauss(lrVar);
         int leftFB = ab.d_leftFB;// + lRand.gauss(fbVar);
         int rightLR = ab.d_rightLR;// + lRand.gauss(lrVar);
         int rightFB = ab.d_rightFB;// + lRand.gauss(fbVar);

         HexCord hex;
         WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), facing, leftLR, leftFB, leftHex, hex);
         leftHex = hex;

         WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), facing, rightLR, rightFB, rightHex, hex);
         rightHex = hex;

         zone->addBoundry(leftHex, rightHex);
      }

      BattleMeasure::BattleTime::Tick startW = zoneStartTime(z, nZones, plan);

      int variance = lRand(10);    // / 2;
      if(variance > 0)
         startW =  static_cast<BattleMeasure::BattleTime::Tick>(maximum(0, startW + BattleMeasure::minutes(lRand.gauss(variance))));

      zone->startWhen(startW);

#ifdef DEBUG
      int inMins = minutesPerTick(static_cast<BattleMeasure::BattleTime::Tick>(maximum(0, startW - gameInfo()->battleData()->getTick())));
      gameInfo()->log("zone #%d(%s) -- start order in %d minutes",
                        z, (const char*)zone->name(), inMins);
#endif
   }
}

bool
playerDeploy(
   RPBattleData batData,
   const RefBattleCP & cp,
   const HexCord & startHex,
   BattleOrderInfo* oi,
   HexList* hl,
   unsigned int flags)
{

   BattleOB* ob = batData->ob();

   // if this is a HQ unit
   if(cp->getRank().isHigher(Rank_Division))
   {

      if((!(flags & PDEPLOY_NEVERFAIL)) && (!canHexBeOccupied(batData, cp, startHex, (flags & PDEPLOY_TERRAINONLY), False, False)))
         return False;

      else if(flags & PDEPLOY_INITIALIZE)
         placeUnit(batData, cp, startHex);

      if(flags & PDEPLOY_FILLHEXLIST)
         hl->newItem(startHex);

      return True;
   }


   if(flags & PDEPLOY_INITIALIZE)
   {
      UINT str = BobUtility::unitSPStrength(batData, cp, True);
      cp->startMorale(cp->morale());
      cp->startStrength(str);
      cp->lastStrength(str);
   }


   // first, we need to count SP that will be visible,
   // An Inf XX only Inf SP will be visible
   // A Cav XX only Cav SP will be visible
   // An Arty xX only Arty SP will be visible

   int spCount = PDeploy_Local::visibleCount(batData, cp, (flags & PDEPLOY_NOCOUNTZEROSP));

   if(spCount > 0)
   {

      HexCord::HexDirection hdR = rightFlank(oi->d_facing);

      // If here, we have visible SP
      // Calculate rows and columns according to current order
      int columns;
      int rows;
      BobUtility::getRowsAndColoumns(oi->d_divFormation, spCount, columns, rows);


      std::vector<DeployItem>& deployMap = cp->deployMap();    // Get this here to simplify code

      if(!(flags & PDEPLOY_FILLHEXLIST))
      {

         if(flags & PDEPLOY_INITIALIZE || flags & PDEPLOY_REORGANIZE)
         {
            cp->columns(columns);
            cp->rows(rows);
            cp->wantColumns(columns);
            cp->wantRows(rows);

            // A Bodge because the editor is adding
            // non-type SP (i.e. Arty in a Inf XX)
            // So we need to clear it out completely and start from scratch
            while(deployMap.size() > 0)
               deployMap.pop_back();

            ASSERT(deployMap.size() == 0);

            // allocate vector if we havn't already done so
            int howMuch = ((columns * rows) % 2 == 0) ? columns * rows : (columns * rows) + 1;

            if(deployMap.size() == 0 || howMuch != deployMap.size())
            {

                /*
                 * I'm not convinced that this will work properly
                 * if hl==0 and NOT initializing then this could
                 * wipe out the deployment map because just after
                 * this section it returns if !initializing
                 * So this assertion checks that the new deployment map
                 * is smaller than the existing one if not intializing
                         *
                 * Note from Paul:
                 *   This will work because the only time the vector may actualy be resized is
                 *   from DeployBase::reorganize(). This routine does the actual assigning of SP to the vector
                 *   so all it needs from this routine is to resize the vector and assign hexes to it.
                 *
                 * Note from Jim..
                 *    I've scrapped all the hl==0 checks, & it now works on the 'flags' bit-field
                  */

//                   ASSERT((flags & PDEPLOY_INITIALIZE) || (flags & PDEPLOY_REORGANIZE) || (howMuch <= deployMap.size()));

                 if(howMuch != deployMap.size()) {
                  while(deployMap.size() > 0)
                     deployMap.pop_back();
                   }

                  ASSERT(deployMap.size() == 0);
                  deployMap.resize(howMuch);
               }

               ASSERT(cp->deployMap().size() == howMuch);
            }
      }


        // now assign hexes to map
        HexCord lHex = startHex;
        for(int r = 0; r < rows; r++) {

         HexCord hex = lHex;
            for(int c = 0; c < columns; c++) {

            if((!(flags & PDEPLOY_NEVERFAIL)) && (!canHexBeOccupied(batData, cp, hex,  (flags & PDEPLOY_TERRAINONLY), !(flags & (PDEPLOY_REORGANIZE | PDEPLOY_NOCOUNTZEROSP)), False))) return False;

            else {

                            if((flags & PDEPLOY_INITIALIZE) || (flags & PDEPLOY_REORGANIZE)) cp->addToMap(NoBattleSP, &hex, c, r);

                            if(flags & PDEPLOY_FILLHEXLIST) {
                   if(spCount > 1 && r == rows - 1 && c == columns - 1 && (spCount % 2) != 0);
                   else  hl->newItem(hex);
                }
                        }

                        HexCord nextHex;
                        if(batData->moveHex(hex, hdR, nextHex)) hex = nextHex;
                        else return False;
         }

                  if(r + 1 < rows) {

            HexCord::HexDirection hdB;

                        if((flags & PDEPLOY_INITIALIZE) || (flags & PDEPLOY_REORGANIZE)) {
               hdB = (r % 2 == 0) ? rightRear(oi->d_facing) : leftRear(oi->d_facing);
                        }

                        else {
               if(cp->whichWay() == BattleCP::HO_Left) {
                  hdB = (r % 2 == 0) ? rightRear(oi->d_facing) : leftRear(oi->d_facing);
                              }
                              else {
                  hdB = (r % 2 == 0) ? leftRear(oi->d_facing) : rightRear(oi->d_facing);
                              }
                        }

                        HexCord nextHex;
                        if(batData->moveHex(lHex, hdB, nextHex)) lHex = nextHex;
                        else return False;
         }
      }

            if(flags & PDEPLOY_FILLHEXLIST) return (hl->entries() > 0);

            if(!(flags & PDEPLOY_INITIALIZE)) return True;

            // assign SP to map
            {
                  int r = 0;
                  int c = 0;
                  RefBattleSP sp = cp->sp();
                  while(sp != NoBattleSP) {

            if(PDeploy_Local::shouldBeVisible(ob, sp)) {

                              cp->addToMap(sp, 0, c, r);

                              DeployItem& di = cp->deployItem(c, r);
                              di.sync();

                              ASSERT(di.active());

                              // intialize facing
                              sp->setFacing(oi->d_facing);
                    sp->nextFacing(oi->d_facing);
                    sp->setRFacing(oi->d_facing);

                    placeUnit(batData, sp, di.d_hex);

                    if(++c == columns) {
                        r++;
                        c = 0;
                    }

                    sp->hexPosition().init(oi->d_facing);
            }

                sp = sp->sister();
            }
        }

        cp->syncMapToSP();
        cp->formation(oi->d_divFormation);
        cp->nextFormation(oi->d_divFormation);
        cp->setFacing(oi->d_facing);
        BobUtility::countArtillery(ob, cp);

        // Another bodge because the editor is setting illegal SPFormation values
        if(cp->getRank().sameRank(Rank_Division) && cp->generic()->isArtillery())
        {
           oi->d_spFormation = SP_UnlimberedFormation;
           for(std::vector<DeployItem>::iterator di = cp->deployMap().begin();
               di != cp->deployMap().end();
               ++di)
           {
              if(di->active())
              di->d_sp->setFormation(SP_UnlimberedFormation);
           }
        }
            
    }

    return True;
}
















#if 0


/*

  Does a player deploy, returning the hexlist irrespective of wether deployment was possible or not

*/

bool
getPlayerDeployHexes(
   RPBattleData batData,
   const RefBattleCP& cp,
   const HexCord& startHex,
   BattleOrder * order,
   HexList* hl,
   DeployFlags flags) {

    BattleOrderInfo* oi = (order->nextWayPoint()) ?
                &order->wayPoints().getLast()->d_order : &order->order();

    BattleOB* ob = batData->ob();

    // if this is a HQ unit
      if(cp->getRank().isHigher(Rank_Division))
    {

        if(flags == Initializing)
            placeUnit(batData, cp, startHex);

        if(hl)
            hl->newItem(startHex);
             
        return True;
    }

 // Unchecked -- Added by Paul
   if(flags == Initializing)
    {
         // Unchecked by Paul
         UINT str = BobUtility::unitSPStrength(batData, cp, True);
         // end unchecked
      cp->startMorale(cp->morale());
      cp->startStrength(str);
      cp->lastStrength(str);
   }

   
    // first, we need to count SP that will be visible,
    // An Inf XX only Inf SP will be visible
    // A Cav XX only Cav SP will be visible
    // An Arty xX only Arty SP will be visible

      int spCount = PDeploy_Local::visibleCount(batData, cp);

    if(spCount > 0)
    {
        HexCord::HexDirection hdR = rightFlank(oi->d_facing);

        // If here, we have visible SP
        // Calculate rows and columns according to current order
        int columns;
        int rows;
        BobUtility::getRowsAndColoumns(oi->d_divFormation, spCount, columns, rows);


        std::vector<DeployItem>& deployMap = cp->deployMap();    // Get this here to simplify code

        if(!hl)
        {
#if 0
            // initialize deployment map
            cp->columns(columns);
            cp->rows(rows);
            cp->wantColumns(columns);
            cp->wantRows(rows);
#endif
            if(flags == Initializing || flags == Reorganizing)
            {
               cp->columns(columns);
               cp->rows(rows);
               cp->wantColumns(columns);
               cp->wantRows(rows);

               // A Bodge because the editor is adding
               // non-type SP (i.e. Arty in a Inf XX)
               // So we need to clear it out completely and start from scratch
               while(deployMap.size() > 0)
                  deployMap.pop_back();

               ASSERT(deployMap.size() == 0);
            }

            // allocate vector if we havn't already done so
            int howMuch = ((columns * rows) % 2 == 0) ? columns * rows : (columns * rows) + 1;

            if(deployMap.size() == 0 || howMuch != deployMap.size())
            {
                /*
                 * I'm not convinced that this will work properly
                 * if hl==0 and NOT initializing then this could
                 * wipe out the deployment map because just after
                 * this section it returns if !initializing
                 * So this assertion checks that the new deployment map
                 * is smaller than the existing one if not intializing
                 *
                 * Note from Paul:
                 *   This will work because the only time the vector may actualy be resized is
                 *   from DeployBase::reorganize(). This routine does the actual assigning of SP to the vector
                 *   so all it needs from this routine is to resize the vector and assign hexes to it.
                 */

                ASSERT(flags == Initializing || flags == Reorganizing || (howMuch <= deployMap.size()));

                if(howMuch != deployMap.size())
                {
                  while(deployMap.size() > 0)
                     deployMap.pop_back();
                }

                ASSERT(deployMap.size() == 0);
                deployMap.resize(howMuch);
//                deployMap.reserve(howMuch);
//                deployMap.insert(deployMap.begin(), howMuch, DeployItem());
            }

            // The bodge above takes care of this. As stated above the deploymap needs to be
            // started from scratch
#if 0             
            else if(initializing)    // added by Steven to clear out existing deployMap
            {   // Set each element to default DeployItem
                // otherwise loading badly saved historical battles caused problems
                fill(deployMap.begin(), deployMap.end(), DeployItem());
            }
#endif
            ASSERT(cp->deployMap().size() == howMuch);
        }

        // now assign hexes to map
        HexCord lHex = startHex;
        for(int r = 0; r < rows; r++)
        {
            HexCord hex = lHex;
            for(int c = 0; c < columns; c++)
            {

                   if(flags == Initializing || flags == Reorganizing)
                     cp->addToMap(NoBattleSP, &hex, c, r);

                   if(hl)
                   {
                     if(r == rows - 1 && c == columns - 1 && (spCount % 2) != 0)
                        ;
                     else 
                        hl->newItem(hex);
                   }

                else
                    return False;

                HexCord nextHex;
                if(batData->moveHex(hex, hdR, nextHex))
                    hex = nextHex;
                else
                    return False;
            }

            if(r + 1 < rows)
            {
                HexCord::HexDirection hdB;

                if(flags == Initializing || flags == Reorganizing)
                {
                    hdB = (r % 2 == 0) ? rightRear(oi->d_facing) :
                            leftRear(oi->d_facing);
                }
                else
                {
                    if(cp->whichWay() == BattleCP::HO_Left)
                    {
                        hdB = (r % 2 == 0) ? rightRear(oi->d_facing) :
                                leftRear(oi->d_facing);
                    }
                    else
                    {
                        hdB = (r % 2 == 0) ? leftRear(oi->d_facing) :
                                rightRear(oi->d_facing);
                    }
                }

                HexCord nextHex;
                if(batData->moveHex(lHex, hdB, nextHex))
                    lHex = nextHex;
                else
                    return False;
            }
        }

        if(hl)
                return (hl->entries() > 0);

        if(flags != Initializing)
                return True;

        // assign SP to map
        {
            int r = 0;
            int c = 0;
            RefBattleSP sp = cp->sp();
            while(sp != NoBattleSP)
            {
                if(PDeploy_Local::shouldBeVisible(ob, sp))
                {
                    cp->addToMap(sp, 0, c, r);

                    DeployItem& di = cp->deployItem(c, r);
                    di.sync();

                    ASSERT(di.active());

//                    BattleOrder& order = cp->getCurrentOrder();

                    // intialize facing
                    sp->setFacing(oi->d_facing);
                    sp->nextFacing(oi->d_facing);
                    sp->setRFacing(oi->d_facing);

                    placeUnit(batData, sp, di.d_hex);

                    if(++c == columns)
                    {
                        r++;
                        c = 0;
                    }

                    sp->hexPosition().init(oi->d_facing);
                }

                sp = sp->sister();
            }
        }

        cp->syncMapToSP();
        cp->formation(oi->d_divFormation);
        cp->nextFormation(oi->d_divFormation);
        cp->setFacing(oi->d_facing);
        BobUtility::countArtillery(ob, cp);

#ifdef DEBUG
        if(cp->getRank().sameRank(Rank_Division))
        {
            DeployItem& di = cp->deployItem(0, 0);
            ASSERT(di.active());
        }
#endif
        // Another bodge because the editor is setting illegal SPFormation values
        if(cp->getRank().sameRank(Rank_Division) && cp->generic()->isArtillery())
        {
           oi->d_spFormation = SP_UnlimberedFormation;
           for(std::vector<DeployItem>::iterator di = cp->deployMap().begin();
               di != cp->deployMap().end();
               ++di)
           {
              if(di->active())
              di->d_sp->setFormation(SP_UnlimberedFormation);
           }
        }
            
    }

    return True;
}





#endif

#if 0
void HiZone::assignUnitsOnMap()
{
   HexPosition::Facing avgFace = WG_BattleAI_Utils::averageFacing(gameInfo()->cinc());

   WG_BattleAI_Utils::LocalCPList llist;
   RefBattleCP lastCP = NoBattleCP;
   HexCord lastLeft;
   for(HiUnitPList::iterator it = d_units.begin();
         it != d_units.end();
         ++it)
   {
      HiUnitInfo* unit = *it;
      ASSERT(!unit->objective());


      HexCord corpLeft;
      HexCord corpRight;
      WG_BattleAI_Utils::getCorpLeftRightBoundry(unit->cp(), avgFace,
               corpLeft, corpRight, 0);

      bool shouldAdd = False;
      if(lastCP == NoBattleCP)
         shouldAdd = True;
      else
      {
         WG_BattleAI_Utils::MoveOrientation mo = WG_BattleAI_Utils::moveOrientation(
                  avgFace, lastLeft, corpLeft);


         if(mo != WG_BattleAI_Utils::MO_Rear)
         {
            if(mo == WG_BattleAI_Utils::MO_Front &&
                WG_BattleAI_Utils::getDist(lastLeft, corpLeft) >= 4)
            {
               llist.reset();
            }

            shouldAdd = True;
         }
      }

      if(shouldAdd)
      {
         lastCP = const_cast<BattleCP*>(unit->cp());
         lastLeft = corpLeft;
         llist.add(lastCP);
      }
   }

   SListIterR<WG_BattleAI_Utils::LocalCP> iter(&llist);
   while(++iter)
   {
      HiUnitInfo* hi = getUnit(iter.current()->d_cp);
      ASSERT(hi);

      addUnitToObjective(hi, d_frontLineObjective);
#ifdef DEBUG
      gameInfo()->log("Adding %s to Front-Line Obj", iter.current()->d_cp->getName());
#endif
   }

   // Add any units not added to reserve
   for(it = d_units.begin();
         it != d_units.end();
         ++it)
   {
      HiUnitInfo* unit = *it;
      if(!unit->objective())
      {
         addUnitToObjective(unit, d_reserveObjective);
#ifdef DEBUG
         gameInfo()->log("Adding %s to Reserve Obj", unit->cp()->getName());
#endif
      }
   }
}
#endif

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
