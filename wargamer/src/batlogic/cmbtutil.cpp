/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "cmbtutil.hpp"
#include "bob_cp.hpp"
#include "bob_sp.hpp"
#include "bobiter.hpp"
#include "bobutil.hpp"
#include "moveutil.hpp"
#include "batctrl.hpp"
#include "batmsg.hpp"
#include "b_tables.hpp"
#include "b_los.hpp"
#include "resstr.hpp"
#include <math.h>


using namespace BattleMeasure;

namespace Combat_Util {

inline int squared(int v)
{
   return (v * v);
}
inline int abValue(int v) { return (v >= 0) ? v : -v; }
inline int getDist(const HexCord& src, const HexCord& dest)
{
   return static_cast<int>(sqrt(squared(dest.x() - src.x()) + squared(dest.y() - src.y())));
}

// This should probably be elsewhere
const char* coverTypeName(CoverType ct)
{
  ASSERT(ct < Cover_HowMany);

#if 0
   // TODO get strings from resource
   static const char* s_cNames[Cover_HowMany] = {
      "None",
      "Fair",
      "Good",
      "Excellent",
      "Fortified"
   };

   return s_cNames[ct];
#endif

   return InGameText::get(ct + IDS_BAT_COVERTYPE);

}

enum {
    East = 0x01,
    NE   = 0x02,
    NW   = 0x04,
    West = 0x08,
    SW   = 0x10,
    SE   = 0x20
};
bool behindWall(const BattleTerrainHex& hexInfo, HexPosition::Facing facing)
{
   if(hexInfo.d_edge.d_type != 2)
      return False;
   else if(facing == HexPosition::NorthEastPoint)
      return (hexInfo.d_edge.d_edges & East || hexInfo.d_edge.d_edges & NE);
   else if(facing == HexPosition::North)
      return (hexInfo.d_edge.d_edges & NE || hexInfo.d_edge.d_edges & NW);
   else if(facing == HexPosition::NorthWestPoint)
      return (hexInfo.d_edge.d_edges & West || hexInfo.d_edge.d_edges & NW);
   else if(facing == HexPosition::SouthWestPoint)
      return (hexInfo.d_edge.d_edges & West || hexInfo.d_edge.d_edges & SW);
   else if(facing == HexPosition::South)
      return (hexInfo.d_edge.d_edges & SW || hexInfo.d_edge.d_edges & SE);
   else if(facing == HexPosition::SouthEastPoint)
      return (hexInfo.d_edge.d_edges & East || hexInfo.d_edge.d_edges & SE);
#ifdef DEBUG
   else
      FORCEASSERT("Improper type in behindWall()");
#endif
   return False;
}

CoverType hexCoverType(const HexCord& hex, RCPBattleData bd, bool checkPath, Situation sit, HexPosition::Facing facing)
{
   static const CoverType s_cTable[Sit_HowMany][GT_HowMany] = {
      {
         None,    // NoValue
         None,    // Grass
         None,    // Water
         Light,   // Orchard
         Light,   // Vineyard
         Medium,  // L-Wood
         Heavy,   // H-Wood
         Light,   // Plouged
         Light,   // Corn-field
         Light,   // Wheat-field
         Light,   // Rough
         Light,   // Marsh
         Heavy,   // Town,
         Medium,  // Village
         Light,   // Farm
         Medium   // Cemetery
      },
      {
         None,    // NoValue
         None,    // Grass
         None,    // Water
         Light,   // Orchard
         Light,   // Vineyard
         Light,   // L-Wood
         Medium,  // H-Wood
         None,    // Plouged
         None,    // Corn-field
         None,    // Wheat-field
         Light,   // Rough
         None,    // Marsh
         Heavy,   // Town,
         Medium,  // Village
         Light,   // Farm
         Light    // Cemetery
      }
   };

   static const CoverType s_pathType[LP_HowMany] = {
      None, // None
      None, // Road
      None, // Track
      None, // River
      None, // Stream,
      Medium, // Sunken Road
      Medium, // Sunken Track
      None,
      None,
      None,
      None
   };

   // get terrain
   const BattleTerrainHex& hexInfo = bd->getTerrain(hex);
   ASSERT(hexInfo.d_terrainType < GT_HowMany);
   ASSERT(sit < Sit_HowMany);

   CoverType type;
   if(!checkPath)
      type = s_cTable[sit][hexInfo.d_terrainType];
   else
   {
      CoverType bestType = s_cTable[sit][hexInfo.d_terrainType];
      if(s_pathType[hexInfo.d_path1.d_type] > bestType)
         bestType = s_pathType[hexInfo.d_path1.d_type];

      if(s_pathType[hexInfo.d_path2.d_type] > bestType)
         bestType = s_pathType[hexInfo.d_path1.d_type];

      type = bestType;
   }

   // add 1 to cover type if behind walls
   if(facing != HexPosition::Facing_Undefined &&
       behindWall(hexInfo, facing))
   {
      type = static_cast<CoverType>(minimum(Cover_HowMany - 1, type + 1));
   }

   return type;
}

CoverType coverType(const RefBattleCP& cp, RCPBattleData bd, bool checkPath)
{
   //50% of a units SP must be in a particualr terrain type
   // to benefit, though if 40% is in medium cover and 20% is in
   // heavy cover then they would be in medium cover
   int coverByType[Cover_HowMany] = {
    0, 0, 0, 0, 0
   };

   // Count only front-line SP
// for(vector<DeployItem>::iterator di = cp->deployMap().begin();
//    di != cp->deployMap().end();
//    di++)
   for(int c = 0; c < cp->wantColumns(); c++)
   {
      DeployItem* di = cp->wantDeployItem(c, 0);
      ASSERT(di);
      if(di && di->active())
      {
         Situation sit = (cp->inCloseCombat()) ? Sit_ShockCombat : Sit_FireCombat;
         CoverType ct = hexCoverType(di->d_sp->hex(), bd, checkPath, sit, cp->facing()); //s_cTable[hexInfo.d_terrainType];
         coverByType[ct]++;
      }
   }

   int nCover = Cover_HowMany;
   int rTotal = 0;
   int nCols = (cp->wantColumns() % 2 == 0) ? cp->wantColumns() / 2 : (cp->wantColumns() / 2) + 1;
// int spCount = cp->spCount();
   while(nCover--)
   {
      rTotal += coverByType[nCover];
      if(rTotal >= nCols)
         break;
   }

   return static_cast<CoverType>(nCover);
}

GraphicalTerrainEnum terrainType(RPBattleData bd, const RefBattleCP& cp)
{
   int types[GT_HowMany] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

   // Count only front-line SP
   for(int c = 0; c < cp->wantColumns(); c++)
   {
      DeployItem* di = cp->wantDeployItem(c, 0);
      ASSERT(di);
      if(di && di->active())
      {
         // get terrain
         const BattleTerrainHex& hexInfo = bd->getTerrain(di->d_sp->hex());
         ASSERT(hexInfo.d_terrainType < GT_HowMany);
         types[hexInfo.d_terrainType]++;
      }
   }
   // End

#if 0
   CoverType ct = coverType(cp, bd, False);
   ASSERT(ct < Cover_HowMany);

   static const CoverType s_cTable[GT_HowMany] = {
         None,    // NoValue
         None,    // Grass
         None,    // Water
         Light,   // Orchard
         Light,   // Vineyard
         Medium,  // L-Wood
         Heavy,   // H-Wood
         Light,   // Plouged
         Light,   // Corn-field
         Light,   // Wheat-field
         Light,   // Rough
         Light,   // Marsh
         Heavy,   // Town,
         Medium,  // Village
         Light,   // Farm
         Medium   // Cemetery
   };

   GraphicalTerrainEnum bestType = GT_None;
   int bestNum = 0;
   for(int t = 1; t < GT_HowMany; t++)
   {
      if(s_cTable[t] == ct && types[t] > 0)
      {
         if(bestType == GT_None || types[t] > bestNum)
         {
            bestNum = types[t];
            bestType = static_cast<GraphicalTerrainEnum>(t);
         }
      }
   }
#endif

   GraphicalTerrainEnum bestType = GT_None;
   int bestNum = 0;
   for(int t = 1; t < GT_HowMany; t++)
   {
      if(bestType == GT_None || types[t] > bestNum)
      {
         bestNum = types[t];
         bestType = static_cast<GraphicalTerrainEnum>(t);
      }
   }

   ASSERT(bestType != GT_None);
   return bestType;
}

bool crossingRiver(RPBattleData bd, const RefBattleCP& cp)
{
   // Count only front-line SP
   for(int c = 0; c < cp->wantColumns(); c++)
   {
      DeployItem* di = cp->wantDeployItem(c, 0);
      ASSERT(di);
      if(di && di->active())
      {
         // get terrain
         const BattleTerrainHex& hexInfo = bd->getTerrain(di->d_sp->hex());
         if(hexInfo.d_path1.d_type == LP_River || hexInfo.d_path2.d_type == LP_River ||
             hexInfo.d_path1.d_type == LP_SunkenRiver || hexInfo.d_path2.d_type == LP_SunkenRiver)
         {
            return True;
         }
      }
   }
   return False;
}

bool crossingStream(RPBattleData bd, const RefBattleCP& cp)
{
   // Count only front-line SP
   for(int c = 0; c < cp->wantColumns(); c++)
   {
      DeployItem* di = cp->wantDeployItem(c, 0);
      ASSERT(di);
      if(di && di->active())
      {
         // get terrain
         const BattleTerrainHex& hexInfo = bd->getTerrain(di->d_sp->hex());
         if(hexInfo.d_path1.d_type == LP_Stream || hexInfo.d_path2.d_type == LP_Stream ||
             hexInfo.d_path1.d_type == LP_SunkenStream || hexInfo.d_path2.d_type == LP_SunkenStream)
         {
            return True;
         }
      }
   }

   return False;
}

bool crossingSunkenStream(RPBattleData bd, const RefBattleCP& cp)
{
   // Count only front-line SP
   for(int c = 0; c < cp->wantColumns(); c++)
   {
      DeployItem* di = cp->wantDeployItem(c, 0);
      ASSERT(di);
      if(di && di->active())
      {
         // get terrain
         const BattleTerrainHex& hexInfo = bd->getTerrain(di->d_sp->hex());
         if(hexInfo.d_path1.d_type == LP_SunkenStream || hexInfo.d_path2.d_type == LP_SunkenStream)
         {
            return True;
         }
      }
   }

   return False;
}

bool behindWall(RPBattleData bd, const RefBattleCP& cp)
{
   int nBehindWall = 0;
   // Count only front-line SP
   for(int c = 0; c < cp->wantColumns(); c++)
   {
      DeployItem* di = cp->wantDeployItem(c, 0);
      ASSERT(di);
      if(di && di->active())
      {
         // get terrain
         const BattleTerrainHex& hexInfo = bd->getTerrain(di->d_sp->hex());
         if(behindWall(hexInfo, cp->facing()))
            nBehindWall++;
      }
   }

   int nCols = (cp->wantColumns() % 2 == 0) ?
         cp->wantColumns() / 2 : (cp->wantColumns() / 2) + 1;

   return (nBehindWall >= nCols);
}

BattleTerrainHex::Height averageHeight(const RefBattleCP& cp, RPBattleData bd)
{
   int value = 0;
   int count = 0;
   for(std::vector<DeployItem>::iterator di = cp->mapBegin();
      di != cp->mapEnd();
      di++)
   {
    if(di->active())
    {
      // get terrain
      const BattleTerrainHex& hexInfo = bd->getTerrain(di->d_sp->hex());
      value += hexInfo.d_height;
      count++;
    }
  }

   return (count > 0) ? static_cast<BattleTerrainHex::Height>(value / count) : 0;
}

// return main shock target
RefBattleCP bestShockTarget(const RefBattleCP& cp)
{
  RefBattleCP bestTarget = NoBattleCP;
  UINT bestFV = 0;

  SListIterR<BattleItem> iter(&cp->targetList());
  while(++iter)
  {
    if(//iter.current()->d_position == BattleItem::Front &&
         iter.current()->d_range <= (1 * BattleMeasure::XYardsPerHex) &&
         iter.current()->d_cp->sp()->hexPosition().distance() == cp->sp()->hexPosition().distance())
    {
      if(iter.current()->d_fireValue >= bestFV)
      {
         bestTarget = iter.current()->d_cp;
         bestFV = iter.current()->d_fireValue;
      }
    }
  }

  return bestTarget;
}

// see if a unit is in the hex
bool nonRoutingFriendlyUnitsUnderHex(RPBattleData bd, const HexCord& hex, const RefBattleCP& cp, bool noSiblings)
{
   BattleList list;
   if(BobUtility::unitsUnderHex(bd, hex, cp, list, BobUtility::Friendly, True, 0))
   {
      SListIterR<BattleItem> iter(&list);
      while(++iter)
      {
         ASSERT(iter.current()->d_cp != NoBattleCP);
         ASSERT(iter.current()->d_cp->getSide() == cp->getSide());
         if(!noSiblings)
         {
            if( (iter.current()->d_cp != cp) && (!iter.current()->d_cp->routing()) )
               return True;
         }
         else
         {
            if( (iter.current()->d_cp != cp) &&
                  (!iter.current()->d_cp->routing()) &&
                  (iter.current()->d_cp->parent() != cp->parent()) )
            {
               return True;
            }
         }
      }
   }

   return False;
}

// see if a unit is in the hex
bool enemyUnderHex(RPBattleData bd, const HexCord& hex, const RefBattleCP& cp)
{
   return BobUtility::unitsUnderHex(bd, hex, cp, BobUtility::Enemy, True);
}

bool nonRoutingFriendsNear(RPBattleData bd, const RefBattleCP& cp, const int nHexes, bool noSiblings)
   // return true if non-routing friendly units of cp are within nHexes
{
// enum { Front, Right, Rear, Left };

   // go through each outer sp and see if cp is within nHexes
   for(int r = 0; r < cp->wantRows(); r++)
   {
    for(int c = 0; c < cp->wantColumns(); c++)
    {
      if(r == 0 || r == cp->wantRows() - 1 || c == 0 || c == cp->wantColumns() - 1)
      {
         DeployItem* di = cp->wantDeployItem(c, r);
         ASSERT(di);
         if(di && di->active())
         {
          // search to the front or rear
          if(r == 0 || r == cp->wantRows() - 1)
          {
            HexCord::HexDirection hd = (r == 0) ? leftFront(cp->facing()) : leftRear(cp->facing());
            HexCord::HexDirection rHD = rightFlank(cp->facing());
            HexCord lHex = di->d_sp->hex();
            int loop = nHexes;
            int goRightHowMany = 1;
            while(loop)
            {
               HexCord hex;
               if(bd->moveHex(lHex, hd, hex))
                lHex = hex;
               else
                break;

               goRightHowMany++;
               HexCord rHex = lHex;
               bool breakOuterLoop = False;
               for(int i = 0; i < goRightHowMany; i++)
               {
                if(nonRoutingFriendlyUnitsUnderHex(bd, rHex, cp, noSiblings))
                  return True;

                if(bd->moveHex(rHex, rHD, hex))
                  rHex = hex;
                else
                {
                  breakOuterLoop = True;
                  break;
                }
               }

               if(breakOuterLoop)
                break;

               loop = maximum(0, loop - 1);
            }
          }

          // search to one flank or the other
          if(c == 0 || c == cp->wantColumns() - 1)
          {
            HexCord::HexDirection hd = (c == 0) ? leftFlank(cp->facing()) : rightFlank(cp->facing());
            HexCord lHex = di->d_sp->hex();
            int loop = nHexes;
            while(loop)
            {
               HexCord hex;
               if(bd->moveHex(lHex, hd, hex))
                  lHex = hex;
               else
                  break;

               if(nonRoutingFriendlyUnitsUnderHex(bd, lHex, cp, noSiblings))
                  return True;

               loop = maximum(0, loop - 1);
            }
          }

          // search to left/right front/rear
          if( (r == 0 || r == cp->wantRows() - 1) &&
                (c == 0 || c == cp->wantColumns() - 1) )
          {
             HexPosition::Facing f = HexPosition::Facing_Undefined;
             if(r == 0 && c == 0)
                f = leftTurn(cp->facing());
             else if(r == 0 && c == cp->wantColumns() - 1)
                f = rightTurn(cp->facing());
             else if(r == cp->wantRows() - 1 && c == 0)
             {
                f = leftTurn(cp->facing());
                f = leftTurn(f);
             }
             else if(r == cp->wantRows() - 1 && c == cp->wantColumns() - 1)
             {
                f = rightTurn(cp->facing());
                f = rightTurn(f);
             }

             ASSERT(f != HexPosition::Facing_Undefined);

             HexCord::HexDirection hd = leftFront(f);
             HexCord::HexDirection rHD = rightFlank(f);
             HexCord lHex = di->d_sp->hex();
             int loop = nHexes;
             int goRightHowMany = 1;
             while(loop)
             {
                HexCord hex;
                if(bd->moveHex(lHex, hd, hex))
                   lHex = hex;
                else
                   break;

                goRightHowMany++;
                HexCord rHex = lHex;
                bool breakOuterLoop = False;
                for(int i = 0; i < goRightHowMany; i++)
                {
                   // do not check extreme left and right hexes
                   // they have already been covered
                   if(i > 0 && i < goRightHowMany - 1)
                   {
                      if(nonRoutingFriendlyUnitsUnderHex(bd, rHex, cp, noSiblings))
                         return True;
                   }

                   if(bd->moveHex(rHex, rHD, hex))
                      rHex = hex;
                   else
                   {
                      breakOuterLoop = True;
                      break;
                   }
                }

                if(breakOuterLoop)
                   break;

                loop = maximum(0, loop - 1);
             }
          }
         }
      }
    }
   }

   return False;
}

#if 0
void getNextHexLOS(RPBattleData bd, const HexCord& hex, HexCord& tHex,
    UWORD& xs, UWORD& ys, const int incX, const int incY)
{
      if(xs)
      {
         tHex.x(tHex.x() + incX);
      }

      if(ys)
      {
         tHex.y(tHex.y() + incY);
      }

      if(B_Logic::adjacentHex(bd, hex, tHex))
      {
         ys = static_cast<UWORD>(maximum(0, ys - 1));
         xs = static_cast<UWORD>(maximum(0, xs - 1));
      }
      else
      {
         // if that doesn't work...
         ASSERT(xs);
         ASSERT(ys);

         tHex = hex;

         if(ys > xs)
         {
            tHex.y(tHex.y() + incY);
            if(!B_Logic::adjacentHex(bd, hex, tHex))
               FORCEASSERT("Hex not found in lineOfSight()");
            else
               ys = static_cast<UWORD>(maximum(0, ys - 1));
         }
         else
         {
            tHex.x(tHex.x() + incX);
            if(!B_Logic::adjacentHex(bd, hex, tHex))
               FORCEASSERT("Hex not found in lineOfSight()");
            else
               xs = static_cast<UWORD>(maximum(0, xs - 1));
         }
      }
}

bool lineOfSight(RPBattleData bd, const HexCord& srcHex, const HexCord& destHex, HexList* hl, RefBattleCP cp, bool* hasFriendly)
{
   ASSERT(srcHex != destHex);

   const SWORD dy = destHex.y() - srcHex.y();
   const SWORD dx = destHex.x() - srcHex.x();

   UWORD dist = static_cast<UWORD>(sqrt(squared(dx) + squared(dy)));

   // we can always see / fire into next hex
   if(dist <= 1)
      return True;

   if(dist > 36)
      return False;

   const BattleTerrainHex& startHexInfo = bd->getTerrain(srcHex);
   const BattleTerrainHex& destHexInfo = bd->getTerrain(destHex);

   HexCord hex = srcHex;
   const int incY = (dy >= 0) ? 1 : -1;
   const int incX = (dx >= 0) ? 1 : -1;
   UWORD xs = (dx >= 0) ? dx : -dx;
   UWORD ys = (dy >= 0) ? dy : -dy;

   HexCord tHex;
// int count = 0;
   while(ys || xs) //hex != destHex)
   {
      // get next hex
      tHex = hex;
      getNextHexLOS(bd, hex, tHex, xs, ys, incX, incY);

      // we can always see into the last hex
      if( /*(count++ > 0) &&*/ (xs || ys) )
      {
         //---------------------------------------
         //    const BattleTerrainHex& lastHexInfo = bd->getTerrain(hex);
         const BattleTerrainHex& thisHexInfo = bd->getTerrain(tHex);

         struct LOSStruct {
            UBYTE d_seeThrough; // how many hexes of this type of terrain can we see through?
            UBYTE d_height;     // height in meters of this terrain.
         };

         static const LOSStruct s_losTable[GT_HowMany] = {
            { 0,           0  }, // GT_None  shouldn't be used
            { UBYTE_MAX, 0  }, // GT_Grass
            { UBYTE_MAX, 0  }, // GT_Water
            { 4,              5  }, // GT_Orchard
            { 4,              3  }, // GT_Vineyard
            { 2,           10 }, // GT_LightWood
            { 1,           30 }, // GT_DenseWood
            { UBYTE_MAX, 0  },// GT_PloughedField
            { 4,         2  }, // GT_CornField
            { 4,         2  }, // GT_WheatField
            { 3,         1  }, // GT_Rough
            { UBYTE_MAX, 0  }, // GT_Marsh
            { 1,         20 }, // GT_Town
            { 2,         15 }, // GT_Village
            { UBYTE_MAX, 0  }, // GT_Farm
            { UBYTE_MAX, 0  }  // GT_Cemetary
         };

         ASSERT(thisHexInfo.d_terrainType > 0);
         ASSERT(thisHexInfo.d_terrainType < GT_HowMany);

         if(cp != NoBattleCP && hasFriendly)
         {
            if(!BobUtility::unitsUnderHex(bd, tHex, cp, BobUtility::Friendly, True))
               *hasFriendly = True;
         }

         int maxThis = (hasFriendly && *hasFriendly) ? 7 : 0; // can we shoot over friendly head
         int eyeLevel = startHexInfo.d_height + 2; // 6 ft tall human
         int tHexHeight = thisHexInfo.d_height + maximum(maxThis, s_losTable[thisHexInfo.d_terrainType].d_height);
         int destHeadLevel = destHexInfo.d_height + 2; // 6 ft tall human

         // first. a quick check for direct elevation obstruction
         if(eyeLevel < tHexHeight && tHexHeight > destHeadLevel)
            return False;

         bool canSeeOver = False;
         // if we are higher than top of this hex
         if(eyeLevel > tHexHeight)
         {
            // if top of this hex is higher than
//          if( (abValue(eyeLevel - destHeadLevel) > 2) &&
            if(tHexHeight > destHeadLevel) //)
            {
               int h1 = eyeLevel - tHexHeight;
               int d1 = getDist(srcHex, tHex) * BattleMeasure::XYardsPerHex;
               Wangle startToHereAngle = direction(d1, h1);

               int h2 = tHexHeight - destHeadLevel;
               int d2 = getDist(tHex, destHex) * BattleMeasure::XYardsPerHex;
               Wangle hereToThereAngle = direction(d2, h2);

               if(startToHereAngle < hereToThereAngle)
                  return False;
            }

            // we can see over
         }

         // or we are lower than top of this hex
         else
         {
            int h1 = tHexHeight - eyeLevel;
            int d1 = getDist(srcHex, tHex) * BattleMeasure::XYardsPerHex;
            Wangle startToHereAngle = direction(d1, h1);

            int h2 = destHeadLevel - tHexHeight;
            int d2 = getDist(tHex, destHex) * BattleMeasure::XYardsPerHex;
            Wangle hereToThereAngle = direction(d2, h2);

            if(startToHereAngle > hereToThereAngle)
               return False;
         }
      }

      // update hex
      hex = tHex;

      // add to list if not NULL
      if(hl)
         hl->newItem(hex);
   }

   ASSERT(hex == destHex);

   return True;
}
#endif

int getRange(RCPBattleData bd, const CRefBattleCP& cp)
{
   ASSERT(cp->sp() != NoBattleSP);

   /*
   * Find sp type to access range
   * Use type that provides greates range
   * i.e. If this is an Inf XX with 5 inf and 2 Heavy Art SP, then range would be
   * for the artillery
   */

   const Table1D<UWORD>& table = BattleTables::fireCombatRange();
   UWORD bestRange = 0;

// RefBattleSP sp = cp->sp();
// while(sp != NoBattleSP)
   for(ConstBattleSPIter spIter(bd->ob(), cp, BSPI_ModeDefs::All);
         !spIter.isFinished();
         spIter.next())
   {
      const UnitTypeItem& uti = bd->ob()->getUnitType(spIter.sp());
      if(!cp->holding() && uti.getBasicType() == BasicUnitType::Artillery)
      {
//       sp = sp->sister();
         continue;
      }

      // convert type to local enum
      enum {
         Inf,
         Cav,
         FootArt,
         HvyArt,
         HorseArt,
         Type_HowMany
      } type = (uti.getBasicType() == BasicUnitType::Infantry) ? Inf :
                   (uti.getBasicType() == BasicUnitType::Cavalry || uti.getBasicType() == BasicUnitType::Special) ? Cav :
                   (uti.isMounted()) ? HorseArt : FootArt; // TODO: Heavy Arty

      int range = table.getValue(type);
      if(range > bestRange)
         bestRange = range;

//    sp = sp->sister();
   }

   return bestRange;
}

// can CP bombard targetCP?
bool canBombard(RCPBattleData bd, const CRefBattleCP& cp, const CRefBattleCP& targetCP)
{
   // not if they are the same side
   if(cp->getSide() == targetCP->getSide())
      return False;

   // not if we or them are a HQ
   if(cp->getRank().isHigher(Rank_Division) || targetCP->getRank().isHigher(Rank_Division))
      return False;

   // not if we are moving or changing sp formation
   if(cp->sp()->formation() == SP_ChangingFormation ||
      cp->moving() ||
      cp->onManuever())
   {
      return False;
   }

   // not if we are out of range
   if(getRange(bd, cp) < (B_Logic::distanceFromUnit(cp, targetCP) * BattleMeasure::XYardsPerHex))
      return False;

   // not if we do not have a line of sight
   bool hasLOS = False;
   B_LineOfSight los(bd);
   for(ConstBattleSPIter ourSP(bd->ob(), cp); !ourSP.isFinished(); ourSP.next())
   {
      for(ConstBattleSPIter theirSP(bd->ob(), targetCP); !theirSP.isFinished(); theirSP.next())
      {
         if(los.FastLOS(bd->map(), ourSP.sp()->hex(), theirSP.sp()->hex(), cp->getSide()))
         {
            hasLOS = True;
            break;
         }
      }
   }

   return hasLOS;
}

// Unchecked
// is this artillery unit supported?
// A supported arty unit is one that has a friendly unit
// at least half its size within 2 hexes of rear or 1 to either flank
bool artillerySupported(RPBattleData bd, const RefBattleCP& cp)
{
   ASSERT(cp->generic()->isArtillery());
   for(BattleUnitIter iter(bd->ob(), cp->getSide()); !iter.isFinished(); iter.next())
   {
      if(iter.cp()->getRank().sameRank(Rank_Division) && iter.cp() != cp)
      {
         int dist = B_Logic::distanceFromUnit(cp, iter.cp());
         if(dist <= 2)
         {
            HexCord ourLeft = cp->leftHex();
            HexCord ourRight = cp->rightHex();
            HexCord theirLeft = iter.cp()->leftHex();
            HexCord theirRight = iter.cp()->rightHex();

            B_Logic::MoveOrientation mo1 = B_Logic::moveOrientation(cp->facing(), ourLeft, theirRight, B_Logic::MOA_Narrow);
            B_Logic::MoveOrientation mo2 = B_Logic::moveOrientation(cp->facing(), ourRight, theirLeft, B_Logic::MOA_Narrow);

            enum { Front, Flank, Rear } orientation =
               (dist == 0 || mo1 == B_Logic::MO_Rear || mo2 == B_Logic::MO_Rear)  ? Rear :
               (mo1 == B_Logic::MO_Left || mo2 == B_Logic::MO_Right) ? Flank : Rear;

            switch(orientation)
            {
               case Rear:
                  if(iter.cp()->spCount(True) * 2 >= cp->spCount(True))
                     return True;
                  break;
               case Flank:
                  if(iter.cp()->spCount(True) * 2 >= cp->spCount(True) && dist <= 1)
                     return True;
                  break;
               case Front:
                  return False;
            }
         }
      }
   }

   return False;
}
// End

}; // end namespace


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
