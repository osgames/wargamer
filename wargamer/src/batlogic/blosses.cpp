/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "blosses.hpp"
#include "broute.hpp"
#include "bob_sp.hpp"
#include "bob_cp.hpp"
#include "bdeploy.hpp"
#include "fsalloc.hpp"
#include "sllist.hpp"
#include "batarmy.hpp"
#include "hexmap.hpp"
#include "bobutil.hpp"
#include "wg_rand.hpp"
#include "moveutil.hpp"
#include "cmbtutil.hpp"
#include "bobiter.hpp"
#include "batctrl.hpp"
#include "batmsg.hpp"
#include "pdeploy.hpp"
#include "b_tables.hpp"
#include "resstr.hpp"
//#include "b_result.hpp"
#ifdef DEBUG
#include "scenario.hpp"
#include "clog.hpp"
#include "reorglog.hpp"
static LogFile lLog("BattleLoss.log");
#endif
namespace B_LossUtil {

enum Outcome {
    Fail1, Fail2, Fail3, Fail_HowMany, Pass = Fail_HowMany
};


inline int squared(int v)
{
  return (v * v);
}

inline double getDist(const HexCord& srcHex, const HexCord& destHex)
{
   return sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y()));
}

//--------------------------------------------------
// Local list class for merging SP with a strength of 50% or less

struct SPItem : public SLink {
  RefBattleSP d_sp;

  SPItem() : d_sp(NoBattleSP) {}
  SPItem(const RefBattleSP& sp) : d_sp(sp) {}

  void* operator new(size_t size);
#ifdef _MSC_VER
  void operator delete(void* deadObject);
#else
  void operator delete(void* deadObject, size_t size);
#endif

  SPItem(const SPItem& hi) : d_sp(hi.d_sp) {}

  SPItem& operator = (const SPItem& hi)
  {
    d_sp = hi.d_sp;
    return *this;
   }
};

class SPItemList : public SList<SPItem> {
public:
   SPItemList& operator = (const SPItemList& spl)
   {
       // reset list
     reset();
     addList(spl);
     return *this;
    }

   void addList(const SPItemList& list);
    SPItem* newItem(const RefBattleSP& sp)
   {
     SPItem* spi = new SPItem(sp);
     ASSERT(spi);

     append(spi);
     return spi;
   }
};

const int chunkSize = 15;

#ifdef DEBUG
static FixedSize_Allocator itemAlloc(sizeof(SPItem), chunkSize, "SPItem");
#else
static FixedSize_Allocator itemAlloc(sizeof(SPItem), chunkSize);
#endif

void* SPItem::operator new(size_t size)
{
  ASSERT(size == sizeof(SPItem));
  return itemAlloc.alloc(size);
}

#ifdef _MSC_VER
void SPItem::operator delete(void* deadObject)
{
  itemAlloc.free(deadObject, sizeof(SPItem));
}
#else
void SPItem::operator delete(void* deadObject, size_t size)
{
  ASSERT(size == sizeof(SPItem));
  itemAlloc.free(deadObject, size);
}
#endif

//-------------------------------------------------------------------------
// append list to current list
void SPItemList::addList(const SPItemList& spl)
{
   // copy
  SListIterR<SPItem> iter(&spl);
  while(++iter)
  {
    SPItem* spi = new SPItem(iter.current()->d_sp);
    ASSERT(spi);

    append(spi);
  }
}

bool findDeployItem(const RefBattleCP& cp, const RefBattleSP& sp, DeployItem** di)
{
   for(int r = 0; r < cp->rows(); r++)
   {
      for(int c = 0; c < cp->columns(); c++)
      {
         DeployItem* thisDI = cp->currentDeployItem(c, r);
         ASSERT(thisDI);
         if(thisDI && thisDI->active() && thisDI->d_sp == sp)
         {
            *di = thisDI;
            return True;
         }
      }
   }

   return False;
}

// reorganize SP within a XX. Merging eligible types
bool reorganizeSP(RPBattleData bd, const RefBattleCP& cp)
{
  SPItemList list; // A list of eligible SP to be built up

  // Add any SP that are at 50% or below in strength to the list
//  for(vector<DeployItem>::iterator di = cp->deployMap().begin();
//    di != cp->deployMap().end();
//    di++)
//  RefBattleSP sp = cp->sp();
//  while(sp != NoBattleSP)
  for(BattleSPIter iter(bd->ob(), cp, BattleSPIter::OnDisplay);
            !iter.isFinished();
        iter.next())
  {
    if(iter.sp()->strength() <= 50)
    {
#ifdef DEBUG
      if(list.entries() == 0)
      {
        lLog.printf("--------------------------------------------");
        lLog.printf("-------- Reorganizing %s", cp->getName());
      }
#endif
         list.newItem(iter.sp());
#ifdef DEBUG
      lLog.printf("SP %ld (%s) is at %d%% strength",
             reinterpret_cast<LONG>(iter.sp()), bd->ob()->spName(iter.sp()),
             static_cast<int>(iter.sp()->strength(100)));
#endif
    }

//    sp = sp->sister();
  }

   // go through list and merge eligible types
  while(list.entries() > 1)
  {
    SPItem* spi = list.first();

    if(spi->d_sp->strength() > 50)
      {
      list.remove(spi);
      continue;
    }

      // Merge with first eligible type found
    bool merged = False;
    SListIterR<SPItem> iter(&list);
    while(++iter)
    {
      if(iter.current() != spi &&
         iter.current()->d_sp->getUnitType() == spi->d_sp->getUnitType() &&
             iter.current()->d_sp->strength() <= 50)
      {
        iter.current()->d_sp->setStrength(static_cast<UBYTE>(minimum(100, iter.current()->d_sp->strength() + spi->d_sp->strength())));
        spi->d_sp->setStrength(0);
#ifdef DEBUG
            lLog.printf("SP %ld (%s)(%d%%) merged with\n",
           reinterpret_cast<LONG>(spi->d_sp), bd->ob()->spName(spi->d_sp),
           static_cast<int>(spi->d_sp->strength(100)));
         lLog.printf("SP %ld (%s)(%d%%)",
           reinterpret_cast<LONG>(iter.current()->d_sp), bd->ob()->spName(iter.current()->d_sp),
           static_cast<int>(iter.current()->d_sp->strength(100)));
#endif
        //merged = True;
        break;
      }
      }

      list.remove(spi);
  }

  return True;
}

bool unitWipedOut(RPBattleData bd, const RefBattleCP& cp)
{
   for(int r = 0; r < cp->rows(); r++)
   {
      for(int c = 0; c < cp->columns(); c++)
      {
         DeployItem* di = cp->currentDeployItem(c, r);
         ASSERT(di);
         if(di && di->active() && di->d_sp->strength() > 0)
         {
            return False;
         }
      }
   }

   return True;
}

bool clearDestroyedSP(BattleGameInterface* bg, const RefBattleCP& cp, bool* shouldReorganize, bool realign)
{
    BattleData* bd = bg->battleData();
    ASSERT(cp->getRank().sameRank(Rank_Division));

    if(unitWipedOut(bd, cp))
    {
#ifdef DEBUG
         reorgLog.printf("%s is wiped-out", cp->getName());
#endif
         // remove all from map
         for(int r = 0; r < cp->rows(); r++)
         {
             for(int c = 0; c < cp->columns(); c++)
             {
                  DeployItem* di = cp->currentDeployItem(c, r);
                  if(di && di->active())
                  {
                      ASSERT(di->d_sp->strength() == 0);
                      BattleHexMap::iterator from = bd->hexMap()->find(di->d_sp);
                      bd->hexMap()->remove(from);

                      di->d_wantSP = NoBattleSP;
                      di->d_sp = NoBattleSP;
                  }
             }
         }

         procLeaderLossTest(bg, cp->leader(), cp);
         // bd->ob()->removeBattleUnits(cp);
            cp->setDead();
         return True;
    }

    if(realign)
         ;//reorganizeSP(bd, cp);
    else
         return False;

    /*
    *  Remove any SP that have a strength of 0
    */

   ASSERT(shouldReorganize);
   *shouldReorganize = False;

   HexCord leftHex;
   B_Logic::getNewLeft(cp, leftHex);
   if(!PlayerDeploy::playerDeploy(bd, cp, leftHex, 0, PlayerDeploy::NOCOUNTZEROSP))
   {
#ifdef DEBUG
      reorgLog.printf("Unable to reorganize %s. Unable to deploy to new position",
      cp->getName());
#endif
      return False;
   }

#ifdef DEBUG
    reorgLog.printf("Clearing dead SP from %s", cp->getName());
#endif
// RefBattleSP sp = cp->sp();
// while(sp != NoBattleSP)
    for(int r = 0; r < cp->rows(); r++)
    {
         for(int c = 0; c < cp->columns(); c++)
         {
             DeployItem* di = cp->currentDeployItem(c, r);
             ASSERT(di);
             if(di && di->active() && di->d_sp->strength() == 0)
             {
#ifdef DEBUG
                  reorgLog.printf("SP %ld, c = %d, r = %d has been removed",
                     reinterpret_cast<long>(di->d_sp), c, r);
#endif
                  // remove from map
                  BattleHexMap::iterator from = bd->hexMap()->find(di->d_sp);
                  bd->hexMap()->remove(from);
                  ASSERT(!bd->hexMap()->isUnitPresent(di->d_sp));

                  // unlink sp from unit
                  // cp->removeStrengthPoint(di.d_sp);
                        di->d_sp->setDead();

                  // remove reference from deployMap
                  di->d_sp = NoBattleSP;
                  di->d_wantSP = NoBattleSP;
                  *shouldReorganize = True;
             }
         }
    }

    return False;
}

void countFleeing(RPBattleData bd, Side side, int& xxxCount, int& nFleeing)
{
   xxxCount = 0;
   nFleeing = 0;


    // first count a corps XXs and determine if XXXs are fleeing
    {
      for(BattleUnitIter iter(bd->ob(), side);
         !iter.isFinished();
         iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Corps)) // && iter.cp()->sp() == NoBattleSP)
         {
               int nXXCount = 0;
               int nXXFleeing = 0;

            for(TBattleUnitIter<BUIV_All> xxIter(iter.cp(), BUIV_All());
               !xxIter.isFinished();
               xxIter.next())
            {
               if(xxIter.cp()->getRank().sameRank(Rank_Division))// && iter.cp()->sp() == NoBattleSP)
               {
                     if(xxIter.cp()->hasQuitBattle() ||
                           xxIter.cp()->fleeingTheField())
                  {
                     nXXFleeing++;
                     }

                     nXXCount++;
                  }
               }

            if(nXXFleeing == nXXCount)
                {
               iter.cp()->fleeingTheField(True);
                    iter.cp()->nearEnemyMode(BattleCP::NE_Retreat6);
                    iter.cp()->clearMove(True);
                }
            }
        }
    }
#if 0
    // do the same for armies (in case an XXXXX is in charge
    {
      for(BattleUnitIter iter(bd->ob(), side);
         !iter.isFinished();
         iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Army)) // && iter.cp()->sp() == NoBattleSP)
         {
               int nXXXCount = 0;
               int nXXXFleeing = 0;

            for(TBattleUnitIter<BUIV_All> xxIter(iter.cp(), BUIV_All());
               !xxIter.isFinished();
               xxIter.next())
            {
               if(xxIter.cp()->getRank().sameRank(Rank_Corps))// && iter.cp()->sp() == NoBattleSP)
               {
                     if(xxIter.cp()->hasQuitBattle() ||
                           xxIter.cp()->fleeingTheField())
                  {
                     nXXXFleeing++;
                     }

                     nXXXCount++;
                  }
               }

            if(nXXXFleeing == nXXXCount)
                {
               iter.cp()->fleeingTheField(True);
                    iter.cp()->nearEnemyMode(BattleCP::NE_Retreat6);
                    iter.cp()->clearMove(True);
                }
            }
        }
    }
#endif


   for(TBattleUnitIter<BUIV_All> iter(bd->ob(), side, BUIV_All());
         !iter.isFinished();
         iter.next())
   {
      if(iter.cp()->getRank().sameRank(Rank_Corps))// && iter.cp()->sp() == NoBattleSP)
      {
         if(iter.cp()->hasQuitBattle() ||
            iter.cp()->wavering() ||
            iter.cp()->fleeingTheField())
         {
            nFleeing++;
         }

         xxxCount++;
      }
   }
}

bool isDefeated(RPBattleData bd, Side s)
{
   // We are defeated if total morale is 40 or less
   // or all corps are wavering, fleeing or left

   RefBattleCP topCP = bd->ob()->getTop(s);
   int totalMorale = 0;
   int spCount = 0;
   for(RefBattleCP unit = topCP; unit != NoBattleCP; unit = unit->sister())
   {
      if(!unit->hasQuitBattle())
      {
         spCount += BobUtility::countSP(unit);
         totalMorale += (unit->morale() * spCount);
      }
   }

   if(spCount == 0 || (totalMorale / spCount) < 40)
      return True;

// RefBattleCP topCP = BobUtility::getCinc(bd->ob(), s);
// if(topCP->morale() <= 40)
//    return True;

   int count;
   int fleeing;
   countFleeing(bd, s, count, fleeing);
   if(fleeing == count)
      return True;

   return False;
}

void procDefeated(BattleGameInterface* bg, Side winner, BattleResultTypes::HowDefeated brt)
{
   if(!bg->isOver())
   {
      BattleFinish result;
      result.defeated(winner, brt);
      bg->requestBattleOver(result);
   }
}

#ifdef DEBUG
void procMoraleLoss(BattleGameInterface* bg, const RefBattleCP& cp, LogFile& lf)
#else
void procMoraleLoss(BattleGameInterface* bg, const RefBattleCP& cp)
#endif
{
    RPBattleData bd = bg->battleData();
    if(cp->getRank().isHigher(Rank_Division))
      return;

#ifdef DEBUG
   lLog.printf("---------- Processing Morale loss for %s %s", scenario->getSideName(cp->getSide()), cp->getName());
   lLog.printf("Start morale = %d, current morale = %d",
       static_cast<int>(cp->startMorale()), static_cast<int>(cp->morale()));
#endif

   enum {
      MoraleGreaterThan200,
      MoraleGreaterThan170,
      MoraleGreaterThan150,
      MoraleGreaterThan130,
      MoraleGreaterThan115,
      MoraleGreaterThan95,
      MoraleGreaterThan70,
      MoraleGreaterThan40,
      MoraleGreaterThan0,

      NValues
   } startMorale = (cp->startMorale() >= 200) ? MoraleGreaterThan200 :
                           (cp->startMorale() >= 170) ? MoraleGreaterThan170 :
                           (cp->startMorale() >= 150) ? MoraleGreaterThan150 :
                           (cp->startMorale() >= 130) ? MoraleGreaterThan130 :
                           (cp->startMorale() >= 115) ? MoraleGreaterThan115 :
                           (cp->startMorale() >= 95) ? MoraleGreaterThan95 :
                           (cp->startMorale() >= 70) ? MoraleGreaterThan70 :
                           (cp->startMorale() >= 40) ? MoraleGreaterThan40 : MoraleGreaterThan0;

   const Table1D<UBYTE>& noEffectTable = BattleTables::noEffectCutoff();
   const Table1D<SWORD>& lossPercentTable = BattleTables::moraleLossMultiplier();
   const Table1D<UBYTE>& criticalLossTable = BattleTables::criticalLossCutoff();
#if 0
   static const UBYTE s_noEffectTable[NValues] = {
      20, 15, 13, 10, 8, 5, 3, 0, 0
   };

   static const float s_lossPercentTable[NValues] = {
      3.5, 2.8, 2.4, 2.3, 2.1, 1.9, 1.6, 1.4, 1
   };

   static const UBYTE s_criticalLossTable[NValues] = {
      60, 50, 40, 35, 30, 25, 20, 15, 15
   };
#endif

   //------------------------------------
   // get loss percent
   // counting current strentgth first
   UINT str = 0;
   {
//    RefBattleSP sp = cp->sp();
//    while(sp != NoBattleSP)
      for(BattleSPIter spIter(bd->ob(), cp, BSPI_ModeDefs::All);
            !spIter.isFinished();
            spIter.next())
      {
         str += spIter.sp()->strength();
//       sp = sp->sister();
      }
   }
   ASSERT(cp->startStrength() >= str);
   ASSERT(cp->startStrength() > 0);

   UBYTE lossPercent = (cp->startStrength() > 0) ?
         static_cast<UBYTE>(100 - ((100 * str) / cp->startStrength())) : 0;

#ifdef DEBUG
   lLog.printf("Start Strength = %d, current strength = %d",
       static_cast<int>(cp->startStrength()), static_cast<int>(str));
   lLog.printf("Loss percent = %d%%", static_cast<int>(lossPercent));
#endif

   // if greater than NoEffect table, process loss
// if(lossPercent > s_noEffectTable[startMorale])
   if(lossPercent > noEffectTable.getValue(startMorale))//s_noEffectTable[startMorale])
   {
      // run morale test if this is the first time to reach this loss percent
      if(!cp->noEffectCutoffReached())
      {
#ifdef DEBUG
         procMoraleTest(bg, cp, Trigger_ThresholdReached, lf);
#else
         procMoraleTest(bg, cp, Trigger_ThresholdReached);
#endif
         cp->noEffectCutoffReached(True);
      }

      // morale is startMorale - (lossPercent * tableValue)
      int rawValue = cp->startMorale() - ((lossPercent * lossPercentTable.getValue(startMorale)) / lossPercentTable.getResolution());
      Attribute newMorale = clipValue(
         static_cast<Attribute>(rawValue), 
         static_cast<Attribute>(0), 
         Attribute_Range);
//      Attribute newMorale = static_cast<Attribute>(clipValue(rawValue, 0, Attribute_Range));
//    Attribute newMorale = static_cast<Attribute>(clipValue(cp->startMorale() - (lossPercent * s_lossPercentTable[startMorale]), 0, Attribute_Range));

      if(newMorale != cp->morale())
      {
         cp->morale(newMorale);
         BobUtility::setCommandMorale(cp);

         if(isDefeated(bd, cp->getSide()))
         {
            procDefeated(bg, (cp->getSide() == 0) ? 1 : 0, BattleResultTypes::Morale);
         }

#ifdef DEBUG
         lLog.printf("New morale = %d", newMorale);
#endif
      }
   }

// if(lossPercent > s_criticalLossTable[startMorale])
   if(lossPercent > criticalLossTable.getValue(startMorale))//s_criticalLossTable[startMorale])
   {
      // if this is the first time we have reached critical loss, run morale test
      if(!cp->criticalLossReached())
      {
#ifdef DEBUG
         procMoraleTest(bg, cp, Trigger_CriticalLoss, lf);
#else
         procMoraleTest(bg, cp, Trigger_CriticalLoss);
#endif
         cp->criticalLossReached(True);
      }
   }
}

#ifdef DEBUG
bool procRoutTest(BattleGameInterface* bg, const RefBattleCP& cp, LogFile& bcLog)
#else
bool procRoutTest(BattleGameInterface* bg, const RefBattleCP& cp)
#endif
{
#ifdef DEBUG
            bcLog.printf("------%s is taking rout test", cp->getName());
#endif
            // generate a random number between 0 and 100
            int dieRoll = CRandom::get(100);

#ifdef DEBUG
            bcLog.printf("Raw die-roll = %d", dieRoll);
#endif

            // modify
            // +1 for each remaining   10 of morale
            dieRoll += (cp->morale() / 10);

            // unit is shaken -20
            if(cp->shaken())
               dieRoll -= 20;

            // unit at or below critical loss -20
            if(cp->criticalLossReached())
               dieRoll -= 20;

            // unit is extended
            if(cp->formation() == CPF_Extended)
               dieRoll -= 10;

            // if unit is in line and has morale lower than 110
            if(cp->morale() <= 110 && cp->spFormation() == SP_LineFormation)
               dieRoll -= 10;
             // +5 for leader charisma over 145
            if(cp->leader()->getCharisma() >= 200)
               dieRoll += 20;
            else if(cp->leader()->getCharisma() >= 190)
               dieRoll += 15;
            else if(cp->leader()->getCharisma() >= 170)
               dieRoll += 10;
            else if(cp->leader()->getCharisma() >= 145)
               dieRoll += 5;

#ifdef DEBUG
            bcLog.printf("Die-roll after leader charisma modifier = %d", dieRoll);
#endif

            // -1 for each disorder level below 3
            dieRoll -= (3 - cp->disorder());

             // +10 for starting battle with 180+ morale

         // -15 for starting battle with 65- morale

            // unit at or below critical loss -20
            if(cp->criticalLossReached())
               dieRoll -= 20;

            if(cp->formation() == CPF_Extended)
               dieRoll -= 10;

            if(cp->morale() <= 110 && cp->spFormation() == SP_LineFormation)
               dieRoll -= 10;

            if(cp->shaken())
               dieRoll -= 10;

#ifdef DEBUG
            bcLog.printf("Final die-Roll = %d", dieRoll);
#endif

             // process result
            enum {
               Level1,
               Level2,
               Level3,
               Level4,
               Level5
            } outCome = (dieRoll <= 15) ? Level1 :
                              (dieRoll <= 37) ? Level2 :
                              (dieRoll <= 65) ? Level3 :
                              (dieRoll <= 90) ? Level4 : Level5;

            switch(outCome)
            {
               case Level1:
                  // rout
#ifdef DEBUG
                  bcLog.printf("Result Level1 ---Unit routs");
#endif
                  routUnit(bg, cp);
                  break;

               case Level2:
#ifdef DEBUG
                  bcLog.printf("Result Level2 ---Unit increases 2 disorder, drops 10 morale, becomes Shaken, retreats 6");
#endif

                  cp->increaseDisorder();
                  cp->increaseDisorder();
                  cp->nearEnemyMode(BattleCP::NE_Retreat6);
                        cp->lockMode();
                  cp->shaken(True);
                  cp->removeMorale(10);
                  break;

               case Level3:
#ifdef DEBUG
                  bcLog.printf("Result Level3 ---Unit increases 2 disorder, drops 10 morale, becomes Shaken, retreats 4");
#endif

                  cp->increaseDisorder();
                  cp->increaseDisorder();
                  cp->nearEnemyMode(BattleCP::NE_Retreat4);
                        cp->lockMode();
                  cp->shaken(True);
                  cp->removeMorale(10);
                  break;

               case Level4:
#ifdef DEBUG
                  bcLog.printf("Result Level4 ---Unit increases 2 disorder, drops 5 morale, becomes Shaken, retreats 4");
#endif

                  cp->increaseDisorder();
                  cp->increaseDisorder();
                  cp->nearEnemyMode(BattleCP::NE_Retreat4);
                        cp->lockMode();
                  cp->shaken(True);
                  cp->removeMorale(5);

                  break;

               case Level5:
#ifdef DEBUG
                  bcLog.printf("Result Level5 ---Unit increases 2 disorder, 1 aggression, drops 5 morale, becomes Shaken, retreats 3");
#endif

                  cp->increaseDisorder();
                  cp->increaseDisorder();
                  cp->nearEnemyMode(BattleCP::NE_Retreat3);
                        cp->lockMode();
                  //cp->aggression(static_cast<BattleOrderInfo::Aggression>(max(0, cp->aggression() - 1)));
                        cp->decreaseAggression();
                  cp->shaken(True);
                  cp->removeMorale(5);
                  break;
            }

   return False;
}

struct OutcomeActions {
   UBYTE d_loseMorale;
   UBYTE d_loseDisorder;
   UBYTE d_loseAggression;
   UBYTE d_loseFatigue;
   bool  d_shaken;
   bool  d_rout;
// bool  d_pullBack4;
   bool  d_postureDefensive;
   BattleCP::NearEnemyMode d_pullBackLowAggression;
   BattleCP::NearEnemyMode d_pullBack;
   bool  d_routTest;
};

const BattleCP::NearEnemyMode r0 = BattleCP::NE_NotNear;
const BattleCP::NearEnemyMode r2 = BattleCP::NE_Retreat2;
const BattleCP::NearEnemyMode r3 = BattleCP::NE_Retreat3;
const BattleCP::NearEnemyMode r4 = BattleCP::NE_Retreat4;
const BattleCP::NearEnemyMode r5 = BattleCP::NE_Retreat5;
const BattleCP::NearEnemyMode r6 = BattleCP::NE_Retreat6;
#ifdef DEBUG
void procOutcome(BattleGameInterface* bg, const RefBattleCP& cp, Outcome outcome, MoraleTestTrigger trigger, LogFile& lf)
#else
void procOutcome(BattleGameInterface* bg, const RefBattleCP& cp, Outcome outcome, MoraleTestTrigger trigger)
#endif
{
   RPBattleData bd = bg->battleData();
   if(outcome == Pass)
      return;

   static const OutcomeActions s_actions[Fail_HowMany][Trigger_HowMany] = {
      {   // Fail1
         { 5,  1, 1, 2, False, False, False, r2, r0, False }, // Critical loss
         { 5,  1, 0, 2, False, False, False, r2, r0, False }, // Threshold reached
         { 0,  1, 0, 2, False, False, False, r2, r0, False }, // SP Lost to fire
         { 5,  1, 1, 4, False, False, False, r0, r0, False }, // Loss of senior leader
         { 0,  0, 0, 2, False, False, False, r0, r3, False }, // Unsupported artillery
         { 5,  1, 1, 4, True,  True,  False, r0, r0, False }, // Nearby old guard routed
         { 5,  1, 0, 4, True,  True,  True,  r0, r0, False }, // 130 Morale charged by old guard
      },
      { // Fail2
         { 5,  2, 1, 4, True,  False, False, r0, r3, False }, // Critical loss
         { 5,  1, 1, 4, False, False, False, r0, r2, False }, // Threshold reached
         { 5,  1, 0, 4, False, False, False, r0, r2, False }, // SP Lost to fire
         { 10, 1, 1, 8, False, False, True,  r0, r2, False }, // Loss of senior leader
         { 5,  0, 1, 4, False, False, False, r0, r3, False }, // Unsupported artillery
         { 10, 2, 1, 8, True,  True,  False, r0, r0, False }, // Nearby old guard routed
         { 10, 2, 0, 8, True,  True,  True,  r0, r0, False }, // 130 Morale charged by old guard
      },
      { // Fail3
         { 10, 2, 1, 8, True,  False, False, r0, r3, True  }, // Critical loss
         { 10, 1, 1, 8, False, False, False, r0, r4, False }, // Threshold reached
         { 5,  1, 1, 8, False, False, False, r0, r2, False }, // SP Lost to fire
         { 20, 1, 1, 16, True,  False, True, r0, r3, True  }, // Loss of senior leader
         { 5,  0, 1, 8, True,  False, False, r0, r3, False }, // Unsupported artillery
         { 10, 2, 1, 8, False, True,  False, r0, r0, False }, // Nearby old guard routed
         { 10, 2, 1, 8, False, True,  True,  r0, r0, False }, // 130 Morale charged by old guard
      }
   };

   ASSERT(outcome < Fail_HowMany);
   ASSERT(trigger < Trigger_HowMany);

   const OutcomeActions& actions = s_actions[outcome][trigger];

#ifdef DEBUG
   lf.printf("Outcome for %s is %s",
       cp->getName(),
         (outcome == Fail1) ? "Fail1" : (outcome == Fail2) ? "Fail2" : "Fail3");

   lf.printf("Actions -- lose morale = %d", actions.d_loseMorale);
   lf.printf("           lose disorder = %d", actions.d_loseDisorder);
   lf.printf("           lose fatiuge = %d", actions.d_loseFatigue);
   lf.printf("           lose aggression = %d", actions.d_loseAggression);
   lf.printf("           shaken = %s", (actions.d_shaken) ? "True" : "False");
   lf.printf("           rout = %s", (actions.d_rout) ? "True" : "False");
// lLog.printf("           pull back 4 = %s", BattleCP::nearEnemyName(actions.d_pullBack4) ? "True" : "False");
   lf.printf("           posture defensive = %s", (actions.d_postureDefensive) ? "True" : "False");
    lf.printf("           pull back low aggress = %s", BattleCP::nearEnemyName(actions.d_pullBackLowAggression));
    lf.printf("           pull back = %s", BattleCP::nearEnemyName(actions.d_pullBack));
#endif

   // remove any morale
   cp->removeMorale(actions.d_loseMorale);

   // remove any disorder
   if(actions.d_loseDisorder != 0)
   {
      cp->disorder(static_cast<UBYTE>(maximum(0, cp->disorder() - actions.d_loseDisorder)));
      // add fatigue for adding disorer
      cp->addFatigue(actions.d_loseDisorder);
   }

   // remove any fatigue
   cp->addFatigue(actions.d_loseFatigue);
   // remove any aggression

    if(actions.d_loseAggression > 0)
    {
      int loop = actions.d_loseAggression;
        while(loop--)
        {
         cp->decreaseAggression();
        }
    }
// cp->aggression(static_cast<BattleOrder::Aggression>(max(0, cp->aggression() - actions.d_loseAggression)), SLONG_MAX);

   // change posture
    if(actions.d_postureDefensive)
      cp->setDefensive();
// cp->posture((actions.d_postureDefensive) ? BattleOrderInfo::Defensive : cp->posture(), SLONG_MAX);

   // status
   if(actions.d_shaken && !actions.d_rout)
   {
      cp->shaken(True);
      cp->addFatigue(5);
   }
   else if(actions.d_shaken && actions.d_rout)
   {
      if(cp->shaken())
      {
         routUnit(bg, cp);
         cp->addFatigue(10);
      }
      else
      {
         cp->shaken(True);
         cp->addFatigue(5);
      }
   }
   else if(actions.d_rout)
   {
      routUnit(bg, cp);
   }

   else if(actions.d_routTest)
   {
      // do rout test
#ifdef DEBUG
      procRoutTest(bg, cp, lLog);
#else
      procRoutTest(bg, cp);
#endif
   }

   if(actions.d_pullBack != r0)
   {
#if 0
      int pullback = actions.d_pullBack + (cp->targetList().closeRangeFront() / BattleMeasure::XYardsPerHex);
      BattleCP::NearEnemyMode nem = (pullback <= 2) ? BattleCP::NE_KeepAway2 :
                                                   (pullback <= 3) ? BattleCP::NE_KeepAway3 :
                                                   (pullback <= 4) ? BattleCP::NE_KeepAway4 :
                                                   (pullback <= 5) ? BattleCP::NE_KeepAway5 : BattleCP::NE_KeepAway6;
#endif
      if(actions.d_pullBack > cp->nearEnemyMode())
        {
         cp->nearEnemyMode(actions.d_pullBack);
#ifdef DEBUG
         lf.printf("Near enemy mode = %s", BattleCP::nearEnemyName(cp->nearEnemyMode()));
#endif
            cp->lockMode();
        }
   }

   else if(actions.d_pullBackLowAggression > 0 &&
               cp->aggression() <= 1)
   {
#if 0
      int pullback = actions.d_pullBackLowAggression + (cp->targetList().closeRangeFront() / BattleMeasure::XYardsPerHex);
      BattleCP::NearEnemyMode nem = (pullback <= 2) ? BattleCP::NE_KeepAway2 :
                                                   (pullback <= 3) ? BattleCP::NE_KeepAway3 :
                                                   (pullback <= 4) ? BattleCP::NE_KeepAway4 :
                                                   (pullback <= 5) ? BattleCP::NE_KeepAway5 : BattleCP::NE_KeepAway6;
#endif
      if(actions.d_pullBackLowAggression > cp->nearEnemyMode())
        {
         cp->nearEnemyMode(actions.d_pullBackLowAggression);
            cp->lockMode();
        }
   }

// if(actions.d_pullBack4)
//    cp->aggression(static_cast<BattleOrder::Aggression>(0), SLONG_MAX);
}

#ifdef DEBUG
void procMoraleTest(BattleGameInterface* bg, const RefBattleCP& cp, MoraleTestTrigger trigger, LogFile& lf)
#else
void procMoraleTest(BattleGameInterface* bg, const RefBattleCP& cp, MoraleTestTrigger trigger)
#endif
{
//  return;
   RPBattleData bd = bg->battleData();
   if(cp->getRank().isHigher(Rank_Division))
      return;

    RefBattleCP attachedLeader = BobUtility::getAttachedLeader(bd, cp);
    RefGLeader leader = (attachedLeader != NoBattleCP) ? attachedLeader->leader() : cp->leader();

#ifdef DEBUG
    lf.printf("---------- Processing Morale Test for %s", cp->getName());
   lf.printf("Start morale = %d, current morale = %d",
      static_cast<int>(cp->startMorale()), static_cast<int>(cp->morale()));
#endif

   // get initial die roll
   int dieRoll = CRandom::get(10);

#ifdef DEBUG
   lf.printf("Unmodified die-roll = %d", dieRoll);
#endif

   const Table1D<SWORD>& mTable = BattleTables::moraleTestDieRollModifiers();
   enum {
      ForMorale,
      CharismaOver170,
      AttachedLeaderCharisma220,
      AttachedLeaderCharisma190,
      AttachedLeaderCharisma150,
      AttachedLeaderCharisma100,
      ParentWithin4,
      InCover,
      Uphill,
      ArtySpecialist,
      Extended,
      MoraleLess110,
      PerDisorder,
      Shaken,
      TakingGunHits,
      FatigueLess65,
      NoNonRoutingFriendsNear,
      CriticalLoss,
   };

   // +1 for each 20 of morale
   if(mTable.getValue(ForMorale) > 0)
      dieRoll += (cp->morale() / mTable.getValue(ForMorale));

#ifdef DEBUG
   lf.printf("die-roll after morale modifier = %d", dieRoll);
#endif

   // + 1 if leader has charisma of 170 or higher
   if(cp->leader()->getCharisma() >= 170)
      dieRoll += mTable.getValue(CharismaOver170);

#ifdef DEBUG
  lf.printf("Die roll after leader charisma modifier = %d", dieRoll);
#endif

  // for attached leader (if any)
   if(attachedLeader != NoBattleCP)
   {
      if(leader->getCharisma() >= 220)
         dieRoll += mTable.getValue(AttachedLeaderCharisma220);//4;
      else if(leader->getCharisma() >= 190)
         dieRoll += mTable.getValue(AttachedLeaderCharisma190);//3;
      else if(leader->getCharisma() >= 150)
         dieRoll += mTable.getValue(AttachedLeaderCharisma150);//2;
      else if(leader->getCharisma() >= 101)
         dieRoll += mTable.getValue(AttachedLeaderCharisma100);

#ifdef DEBUG
      lf.printf("Die roll after attached-leader charisma modifier = %d", dieRoll);
#endif
   }
   else // for parent leader, if not attached
   {
      RefBattleCP parentCP = cp->parent();
      if(parentCP != NoBattleCP &&
         !parentCP->attached() &&
         parentCP->leader()->getCharisma() >= 170 &&
         B_Logic::distanceFromUnit(parentCP->hex(), cp) <= 4)
      {
         dieRoll += mTable.getValue(ParentWithin4);//++;
      }
#ifdef DEBUG
      lf.printf("Die roll after parent leader charisma modifier = %d", dieRoll);
#endif
   }

   // +1 if uphill of nearest enemy, or we are in cover
  if(Combat_Util::coverType(cp, bd) != Combat_Util::None)
    dieRoll += mTable.getValue(InCover);
  else
  {
      // get nearest enemy
    if(cp->targetList().entries() > 0 &&
          Combat_Util::averageHeight(cp, bd) > Combat_Util::averageHeight(cp->targetList().first()->d_cp, bd))
    {
      dieRoll += mTable.getValue(Uphill);
    }
  }

#ifdef DEBUG
  lf.printf("Die roll after elevation\\cover modifier = %d", dieRoll);
#endif

   // +1 if unit is arty and has specialist leader attached
  if( (cp->generic()->isArtillery()) &&
      (cp->leader()->isSpecialistType(Specialist::Artillery) || leader->isSpecialistType(Specialist::Artillery)) )
   {
    dieRoll += mTable.getValue(ArtySpecialist);
   }

#ifdef DEBUG
   lf.printf("Die roll after Artillery\\Specialist modifier = %d", dieRoll);
#endif

  // TODO: +1 for unit in pursuit mode

  // -1 Unit is extended
  if(cp->formation() == CPF_Extended)
      dieRoll += mTable.getValue(Extended);

#ifdef DEBUG
  lf.printf("Die roll after Extended modifier = %d", dieRoll);
#endif

  // -1 if current morale of 110 or less
  if(cp->morale() <= 110)
    dieRoll += mTable.getValue(MoraleLess110);//--;

#ifdef DEBUG
  lf.printf("Die roll after Line Formation \\ morale modifier = %d", dieRoll);
#endif

   // -1 for each disorder level lost
  dieRoll += (mTable.getValue(PerDisorder) * (3 - cp->disorder()));

#ifdef DEBUG
  lf.printf("Die roll after disorder modifier = %d", dieRoll);
#endif

   // -1 unit is shaken
   if(cp->shaken())
      dieRoll += mTable.getValue(Shaken);//--;

#ifdef DEBUG
   lLog.printf("Die roll after shaken modifier = %d", dieRoll);
#endif

   // infantry with morale of 110 or less, being shot at by arty, has no arty itself
   if(cp->generic()->isInfantry() &&
         cp->morale() <= 100 &&
       cp->takingGunHits() &&
       cp->nArtillery() == 0)
   {
      dieRoll += mTable.getValue(TakingGunHits);//--;
   }

#ifdef DEBUG
   lf.printf("Die roll after inf\\morale 110\\arty\no arty modifier = %d", dieRoll);
#endif

    // -1 unit has current fatigue of 65 or less
    if(cp->fatigue() <= 65)
      dieRoll += mTable.getValue(FatigueLess65);//--;

#ifdef DEBUG
    lf.printf("Die roll after fatigue modifier = %d", dieRoll);
#endif

   // -1 No other non-routing Friendly Units within 4 hexes
   if(!Combat_Util::nonRoutingFriendsNear(bd, cp, 4))
      dieRoll += mTable.getValue(NoNonRoutingFriendsNear);//--;

#ifdef DEBUG
   lf.printf("Die roll after nonRouting nearby friends modifier = %d", dieRoll);
#endif

   // TODO: remaining modifiers
   // unit at or below critcal loss
   if(cp->criticalLossReached())
      dieRoll += mTable.getValue(CriticalLoss);//--;
   // TODO: modifier for raining


   //-----------------------------------------------------
   // proc outcome
   Outcome outcome = (dieRoll >= 11) ? Pass :
                              (dieRoll >= 7)  ? Fail1 :
                              (dieRoll >= 4)  ? Fail2 : Fail3;

#ifdef DEBUG
   procOutcome(bg, cp, outcome, trigger, lf);
#else
   procOutcome(bg, cp, outcome, trigger);
#endif

}

// Test to see if this leader is hit
// first a local struct
struct LeaderHitResult {
   UBYTE d_chanceKilled;
   UBYTE d_chanceSeriousWound;
   UBYTE d_chanceLightWound;
   UBYTE d_chanceHorseKilled;
};

void procLeaderLossTest(BattleGameInterface* bg, const RefGLeader& leader, const RefBattleCP& cp)
{
#ifdef DEBUG
   lLog.printf("\n---------- Testing %s for Leader Casualty", leader->getName());
#endif

  BattleData* bd = bg->battleData();
   //
   const int nNations = 14;
   static const UBYTE s_chanceToTest[nNations] = {
      6, 5, 3, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5
    };

   if(CRandom::get(100) >= s_chanceToTest[leader->getNation()])
      return;

#ifdef DEBUG
   lLog.printf("%s will be hit", leader->getName());
#endif

   const int nDieRollValues = 10;

   RefBattleCP leaderCP = battleCP(leader->command());

   // get a raw die roll between 0 - 9
   int dieRoll = CRandom::get(nDieRollValues);
#ifdef DEBUG
   lLog.printf("Raw die roll = %d", dieRoll);
#endif

   // modify
    // +2 if leader is lucky
   if(leader->isLucky())
    {
      dieRoll += 2;
#ifdef DEBUG
      lLog.printf("Die roll after 'Lucky' modifier = %d", dieRoll);
#endif
   }

    // +1 if enemy is over 880 yards away
    if(cp->targetList().closeRange() > (4 * BattleMeasure::XYardsPerHex))
   {
      dieRoll++;
#ifdef DEBUG
         lLog.printf("Die roll after enemy over 880 yards modifier = %d", dieRoll);
#endif
   }

   // -1 if unit is wiped-out
   int spStrength = 0;
   for(std::vector<DeployItem>::iterator di = cp->mapBegin();
         di != cp->mapEnd();
         di++)
   {
      if(di->active())
         spStrength += di->d_sp->strength();
    }

   if(spStrength == 0)
    {
      dieRoll--;
#ifdef DEBUG
      lLog.printf("Die roll after 'wiped-out' modifier = %d", dieRoll);
#endif
   }

   dieRoll = clipValue(dieRoll, 0, nDieRollValues - 1);

    enum {
         Killed,
         SeriousWound,
         LightWound,
         HorseKilled,
         Escaped,
         Outcome_HowMany
   } outcome = Escaped;

   static const LeaderHitResult s_table[nDieRollValues] = {
   // chance   kia   sw    lw   hk
                  { 100,   0,   0,   0 }, // 0
                  {  75,  25,   0,   0 }, // 1
                  {   0, 100,   0,   0 }, // 2
                  {   0,  50,  50,   0 }, // 3
                  {   0,   0, 100,   0 }, // 4
                  {   0,   0,  75,  25 }, // 5
                           {   0,   0,   0, 100 }, // 6
                  {   0,   0,   0, 100 }, // 7
                  {   0,   0,   0,   0 }, // 8
                           {   0,   0,   0,   0 }, // 9
   };

   if(s_table[dieRoll].d_chanceKilled > 0)
   {
      if(s_table[dieRoll].d_chanceKilled == 100)
         outcome = Killed;
      else
      {
             outcome = (CRandom::get(100) < s_table[dieRoll].d_chanceKilled) ?
                   Killed : SeriousWound;
      }
   }

    else if(s_table[dieRoll].d_chanceSeriousWound > 0)
   {
      if(s_table[dieRoll].d_chanceSeriousWound == 100)
             outcome = SeriousWound;
      else
      {
         outcome = (CRandom::get(100) < s_table[dieRoll].d_chanceSeriousWound) ?
            SeriousWound : LightWound;
      }
   }

   else if(s_table[dieRoll].d_chanceLightWound > 0)
    {
      if(s_table[dieRoll].d_chanceLightWound == 100)
         outcome = LightWound;
         else
      {
             outcome = (CRandom::get(100) < s_table[dieRoll].d_chanceLightWound) ?
            LightWound : Escaped;
      }
   }

   else
      outcome = Escaped;

#if 0
    static const char* s_text[Outcome_HowMany] = {
      "Killed",
      "Serious Wound",
      "Light Wound",
         "Horse Killed",
      "Escaped"
   };
#endif

#ifdef DEBUG
   lLog.printf("%s -- outcome = %s", leader->getName(), InGameText::get(IDS_BAT_LEADERLOSS + outcome));  // s_text[outcome]);
#endif

   bool determinationTest = False;
   // process outcome
   switch(outcome)
   {
         case Killed:
      {
         // If enemy is within 220 yards there is a  75 - 25 chance of being captured
             if(cp->targetList().closeRange() <= (BattleMeasure::XYardsPerHex) &&
             CRandom::get(100) < 25)
             {
#ifdef DEBUG
            lLog.printf("%s has been captured", leader->getName());
#endif
            // if captured, set as wounded with -50 to health attribute
            leader->isCaptured(True);
            leader->isSeriouslyWounded(True);
                  leader->setHealth(static_cast<Attribute>(maximum(0, leader->getHealth() - 50)));
             }
         else
            leader->isDead(True);

             determinationTest = leader->command()->getRank().isHigher(Rank_Division);
         // unlink leader
         bd->ob()->ob()->killAndReplaceLeader(leader);
             break;
      }

      case SeriousWound:
      {
         // if with a routing or destroyed unit and enemy is within
         // 220 yards then leader is captured (unless 'lucky' then a 50-50 chance)
         if( (spStrength == 0 || cp->routing()) &&
               (cp->targetList().closeRange() <= (BattleMeasure::XYardsPerHex)) )
             {
            bool captured = True;
            if(leader->isLucky() && CRandom::get(100) < 50)
                      captured = False;

                  if(captured)
            {
#ifdef DEBUG
               lLog.printf("%s has been captured", leader->getName());
#endif
               leader->isCaptured(True);
            }
             }

         determinationTest = leader->command()->getRank().isHigher(Rank_Division);
         leader->isSeriouslyWounded(True);
         leader->setHealth(static_cast<Attribute>(maximum(0, leader->getHealth() - 50)));

         bd->ob()->ob()->killAndReplaceLeader(leader);
         break;
         }

      case LightWound:
      {
         // if with a destroyed unit and enemy is within
         // 220 yards then leader is captured (unless 'lucky' then a 50-50 chance)
         if( (spStrength == 0) &&
               (cp->targetList().closeRange() <= (BattleMeasure::XYardsPerHex)) )
         {
                  bool captured = True;
            if(leader->isLucky() && CRandom::get(100) < 50)
               captured = False;

            if(captured)
                  {
#ifdef DEBUG
               lLog.printf("%s has been captured", leader->getName());
#endif
               leader->isCaptured(True);
               determinationTest = leader->command()->getRank().isHigher(Rank_Division);
            }
             }

         leader->isLightlyWounded(True);
         leader->setHealth(static_cast<Attribute>(maximum(0, leader->getHealth() - 5)));

             if(leader->isCaptured())
            bd->ob()->ob()->killAndReplaceLeader(leader);
      }

      case HorseKilled:
      {
         leader->isHorseKilled(True);
      }
   }

   // if killed,  serious wounded, or captured
   // there are morale implications
    if(outcome == Killed ||
       outcome == SeriousWound ||
       leader->isCaptured())
    {
#ifdef DEBUG
         lLog.printf("%s is new leader for %s", leaderCP->leader()->getName(), leaderCP->getName());
#endif

      if(cp == leaderCP)
      {
         ASSERT(cp->getRank().sameRank(Rank_Division));

       if(CRandom::get(100) > cp->moralePercent())
            cp->decreaseAggression();
//       cp->aggression(static_cast<BattleOrderInfo::Aggression>(max(0, cp->aggression() - 1)));
      }
      else if(determinationTest)
      {
             ASSERT(leaderCP->getRank().isHigher(Rank_Division));
             ASSERT(leaderCP->child() != NoBattleCP);

         if(leaderCP->getRank().sameRank(Rank_Corps))
            procCorpDeterminationTest(bg, leaderCP);
         else
         {
            CRefBattleCP constAttachedTo = (leaderCP->attached()) ?
                  leaderCP->getCurrentOrder().attachTo() : NoBattleCP;
            RefBattleCP attachedTo = const_cast<RefBattleCP>(constAttachedTo);

            if(attachedTo != NoBattleCP)
            {
               if(attachedTo->getRank().sameRank(Rank_Division))
                  procCorpDeterminationTest(bg, attachedTo->parent());
               else
                  procCorpDeterminationTest(bg, attachedTo);
            }
            else
            {
                      // go through sub units and see if any are with in 8 hexes
            }
         }
      }
    }

    // send player message
    BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_LeaderHit, cp);
    msg.s1(cp->leader()->getName());
    msg.s2(InGameText::get(IDS_BAT_LEADERLOSS + outcome));  // s_text[outcome]);
    bg->sendPlayerMessage(msg);
}



bool shouldProcArmyDetermination(RPBattleData bd, Side side)
{
   // we process army determination test if half or more of XXX's are
   // 1. Wavering
   // 2. Fleeing
   // 3. Fled or otherwise left the field
   int xxxCount = 0;
   int nFleeing = 0;
   countFleeing(bd, side, xxxCount, nFleeing);

#ifdef DEBUG
   lLog.printf("%s -- %d corps / %d fleeing-wavering", scenario->getSideName(side), xxxCount, nFleeing);
#endif
   return (nFleeing >= (xxxCount + 1) / 2);
}

// Army determination test
bool procArmyDeterminationTest(BattleGameInterface* bg, Side side)
{
   if(bg->isOver())
      return False;

#ifdef DEBUG
   lLog.printf("\n-----------Processing Army Determination Test for %s", scenario->getSideName(side));
#endif

   // Base chance of commander passing test is aggression * current aggression level
   // as a percent of 1020
   RefBattleCP topCP = BobUtility::getCinc(bg->battleData()->ob(), side);
   int chance = ((topCP->leader()->getAggression() * (topCP->aggression() + 1)) * 100) / 1020;
#ifdef DEBUG
   lLog.printf("Raw chance of passing = %d", chance);
#endif

   // - 10 at defensive posture
   if(topCP->posture() == BattleOrderInfo::Defensive)
      chance -= 10;

   int fleeing = 0;
   int count = 0;
   countFleeing(bg->battleData(), side, count, fleeing);

   // - 25 if 75% or more are fleeing / wavering
   if(count > 0 && (fleeing * 100) / count >= 75)
   {
      chance -= 25;
   }

   // Overall morale 65 or less
   if(topCP->morale() <= 65)
      chance -= 25;

   // for each corp with morale 150 - 175
   int n150 = 0;
   int n175 = 0;
   for(BattleUnitIter iter(topCP); !iter.isFinished(); iter.next())
   {
      if(iter.cp()->getRank().sameRank(Rank_Corps))
      {
         if(iter.cp()->morale() >= 175)
            n175++;
         else if(iter.cp()->morale() >= 150)
            n150++;
      }
   }

   chance += (n150 * 10);
   chance += (n175 * 20);

#ifdef DEBUG
   lLog.printf("final chance = %d", chance);
#endif
   if(CRandom::get(100) > chance)
   {
#ifdef DEBUG
      lLog.printf("%s has failed. Battle will end", topCP->getName());
#endif
      // Battle over
      procDefeated(bg, (side == 0) ? 1 : 0, BattleResultTypes::Determination);
      return False;
   }

   return True;
}

// Corps determination test routines
//---------------------------------------------------------------------
void procDeterminationTestOutcome(BattleGameInterface* bg, const RefBattleCP& cp, const int dieRoll)
{
    BattleData* bd = bg->battleData();

#ifdef DEBUG
    lLog.printf("Processing outcome -----------------");
#endif


    enum Outcome {
         Passed,
         Outcome1,
         Outcome2,
         Outcome3,
         Outcome4,
         Failed,

         Outcome_HowMany
    } outcome = (dieRoll >= 11) ? Passed :
                (dieRoll >= 9)  ? Outcome1 :
                (dieRoll >= 7)  ? Outcome2 :
                (dieRoll >= 4)  ? Outcome3 :
                (dieRoll >= 2)  ? Outcome4 : Failed;

    struct ResultStruct {
      BattleCP::NearEnemyMode d_lowDisorderRetreatHowMany;
      UBYTE d_moraleLoss;
      UBYTE d_spLossIfRouting;
      UBYTE d_spLossIfCloseCombat;
      UBYTE d_aggressLoss;
      UBYTE d_disorderLoss;
      bool  d_toDefensive;
      bool  d_shouldRout;
      bool  d_criticalLossShouldFlee;
      bool  d_allShouldFlee;
      bool  d_wavers;
      bool  d_routShouldFlee;
   };

    static const ResultStruct s_outcomeResult[Outcome_HowMany] = {
         // r  ml splr splcc, al  dl   td     sr    clsf   asf    w    rsf
         {  r0,  0,  0,  0,    0,  0, False, False, False, False, False, False }, // Pass
         {  r4,  5,  0,  0,    0,  0, False, False, False, False, False, False }, // Outcome1
         {  r4, 10,  0,  0,    1,  0, False, False, False, False, False, False }, // Outcome2
         {  r6, 15,  1,  1,    1,  0, True,  False, True,  False, True,  False }, // Outcome3
         {  r6, 20,  1,  1,    2,  2, True,  False, True,  False, True,  True  }, // Outcome4
         {  r6, 40,  1,  1,    4,  4, True,  True,  False, True,  True,  False }  // Failed
    };

#ifdef DEBUG
    static const char* s_outcomeTxt[Outcome_HowMany] = {
         "Passed",
         "Outcome1",
         "Outcome2",
         "Outcome3",
         "Outcome4",
         "Failed"
    };

    lLog.printf("Outcome = %s", s_outcomeTxt[outcome]);

#endif

    const ResultStruct& result = s_outcomeResult[outcome];

    // process result for each unit in corp
    for(BattleUnitIter iter(cp); !iter.isFinished(); iter.next())
    {
//    if(iter.cp()->getRank().sameRank(Rank_Division))
         bool isXX = iter.cp()->getRank().sameRank(Rank_Division);
         {
            // If a XX has not reached no-effect cutoff and result is 1-3, ignore
            if( (outcome == Passed) ||
                (isXX && (outcome <= Outcome3) && !iter.cp()->noEffectCutoffReached()) )
            {
               continue;
            }
             // retreat X amount of hexes
             if(result.d_lowDisorderRetreatHowMany != r0 &&
                   isXX &&
                   iter.cp()->disorder() <= 1)
             {
                  // only units with a disorder of 0 or 1 retreat
//                BattleCP::NearEnemyMode nem = (result.d_lowDisorderRetreatHowMany == 4) ?
//                      BattleCP::NE_KeepAway4 : BattleCP::NE_KeepAway6;

                if(result.d_lowDisorderRetreatHowMany > iter.cp()->nearEnemyMode())
                     {
                  iter.cp()->nearEnemyMode(result.d_lowDisorderRetreatHowMany);
                        iter.cp()->lockMode();
                     }
             }

             // morale loss
             iter.cp()->removeMorale(result.d_moraleLoss);
         }

         // sp loss if routing
         if(result.d_spLossIfRouting > 0 &&
               isXX &&
               iter.cp()->routing() &&
               iter.cp()->targetList().closeRange() <= (5 * BattleMeasure::XYardsPerHex))
         {
             // we lose a Full SP for each value
#ifdef DEBUG
             applyLoss(bd, iter.cp(), result.d_spLossIfRouting * 100, lLog);
#else
             applyLoss(bd, iter.cp(), result.d_spLossIfRouting * 100);
#endif
         }

         // sp loss if in close combat
         if(result.d_spLossIfCloseCombat > 0 &&
               isXX &&
               iter.cp()->inCloseCombat())
         {
             // we lose a Full SP for each value
#ifdef DEBUG
             applyLoss(bd, iter.cp(), result.d_spLossIfCloseCombat * 100, lLog);
#else
             applyLoss(bd, iter.cp(), result.d_spLossIfCloseCombat * 100);
#endif
         }

         // aggression loss
         if(result.d_aggressLoss > 0)
            {
               int loop = result.d_aggressLoss;
                while(loop--)
                  cp->decreaseAggression();
            //  iter.cp()->aggression(static_cast<BattleOrderInfo::Aggression>(max(0, iter.cp()->aggression() - result.d_aggressLoss)), UWORD_MAX);
         }

         // disorder loss
         if(result.d_disorderLoss > 0)
             iter.cp()->disorder(static_cast<UBYTE>(maximum(0, iter.cp()->disorder() - result.d_disorderLoss)));

         // to defensive posture
         if(result.d_toDefensive)// && iter.cp()->posture() == BattleOrderInfo::Offensive)
             cp->setDefensive(); //iter.cp()->posture(BattleOrderInfo::Defensive, UWORD_MAX);

         // routing
         if(result.d_shouldRout)
             routUnit(bg, iter.cp(), False);

         // wavers
         if(result.d_wavers)
            iter.cp()->wavering(True);

         // Fleeing the field
         if(result.d_allShouldFlee ||
               result.d_routShouldFlee ||
               result.d_criticalLossShouldFlee)
         {
             bool shouldFlee = False;
             if(result.d_allShouldFlee)
                  shouldFlee = True;
             else
             {
                  if(result.d_criticalLossShouldFlee &&
                        isXX &&
                        iter.cp()->criticalLossReached())
                  {
                      shouldFlee = True;
                  }

                  if(!shouldFlee &&
                        result.d_routShouldFlee &&
                        isXX &&
                        iter.cp()->routing())
                  {
                      shouldFlee = True;
                  }
             }

             if(shouldFlee)
             {
                  routUnit(bg, iter.cp(), False);
                  iter.cp()->fleeingTheField(True);
                        iter.cp()->nearEnemyMode(BattleCP::NE_Retreat6);
#ifdef DEBUG
                  lLog.printf("%s is fleeingTheField()", iter.cp()->getName());
#endif
                  if(!isDefeated(bd, iter.cp()->getSide()))
                  {
                     if(shouldProcArmyDetermination(bd, iter.cp()->getSide()))
                     {
                        procArmyDeterminationTest(bg, iter.cp()->getSide());
                     }
                  }
                  else
                  {
                     procDefeated(bg, (iter.cp()->getSide() == 0) ? 1 : 0, BattleResultTypes::Morale);
                  }
             }
         }
    }

    // wavers
    if(result.d_wavers)
       cp->wavering(True);


}

bool isKeyTerrain(const BattleTerrainHex& hexInfo)
{
   // If a victory location
   if(hexInfo.d_victoryPoints > 0)
      return True;

   // for terrain type
   if(hexInfo.d_terrainType == GT_Town ||
      hexInfo.d_terrainType == GT_Village ||
      hexInfo.d_terrainType == GT_Farm ||
      hexInfo.d_terrainType == GT_Cemetary)
   {
      return True;
   }

   // If a crossroads
   if(hexInfo.d_path1.isRoad() &&
      hexInfo.d_path2.isRoad())
   {
      return True;
   }

   // If a bridge
   if(hexInfo.d_path1.d_type == LP_RoadBridge ||
      hexInfo.d_path1.d_type == LP_TrackBridge ||
      hexInfo.d_path2.d_type == LP_RoadBridge ||
      hexInfo.d_path2.d_type == LP_TrackBridge)
   {
      return True;
   }

   // Others?...
   return False;
}

// Get army leader attached to a corp
CRefBattleCP getAttachedArmyLeader(BattleData* bd, const RefBattleCP& corpCP)
{
   // If attached to corp HQ
   CRefBattleCP armyCP = NoBattleCP;

   // If attached to a subordinate
   for(BattleUnitIter iter(corpCP); !iter.isFinished(); iter.next())
   {
      armyCP = BobUtility::getAttachedLeader(bd, iter.cp());
      if(armyCP)
      {
         if(armyCP != corpCP)
            break;
         else
            armyCP = NoBattleCP;
      }
   }

   return armyCP;
}

void procCorpDeterminationTest(BattleGameInterface* bg, const RefBattleCP& cp)
{
    BattleData* bd = bg->battleData();
    ASSERT(cp->getRank().isHigher(Rank_Division));

    // If no units are attached, return
    if(cp->child() == NoBattleCP)
         return;

#ifdef DEBUG
    lLog.printf("\n--------------- Processing Corp Determination for %s %s",
      scenario->getSideName(cp->getSide()), cp->getName());
#endif

   // get any relevant data pertaining to the entire command
   //-------------------------------------------------------
    // useful flags
   enum {
      AllCav               = 0x01,
      AllAtNoEffectLevel   = 0x02,
      NonRoutingFriendNear = 0x04,
      AllAtCriticalLoss    = 0x08,
      AllAtLowDisorder     = 0x10,
      AllAtLowMorale       = 0x20,
      KeyTerrain           = 0x40
   };

   UBYTE d_flags = AllCav | AllAtNoEffectLevel | AllAtCriticalLoss | AllAtLowDisorder | AllAtLowMorale;
   int startStrength = 0;
   int curStrength = 0;
   int nUnits = 0;
   int nRouting = 0;
   {
      for(BattleUnitIter iter(cp); !iter.isFinished(); iter.next())
      {
         if(iter.cp()->getRank().sameRank(Rank_Division))
         {
            if( (d_flags & AllCav) &&
                  ( (iter.cp()->generic()->isInfantry()) ||
                     (iter.cp()->generic()->isArtillery() && !iter.cp()->generic()->isHorseArtillery()) ) )
            {
               d_flags &= ~AllCav;
            }

            curStrength += BobUtility::unitSPStrength(bd, iter.cp());
            startStrength += iter.cp()->startStrength();
                  nUnits++;

                  if(d_flags & AllAtNoEffectLevel &&
                !iter.cp()->noEffectCutoffReached())
            {
               d_flags &= ~AllAtNoEffectLevel;
            }

            if(d_flags & AllAtCriticalLoss &&
                !iter.cp()->criticalLossReached())
            {
               d_flags &= ~AllAtCriticalLoss;
            }

            if( (d_flags & AllAtLowDisorder) &&
                  !(iter.cp()->disorder() > 1))
                // !iter.cp()->disorder() > 1)
            {
               d_flags &= ~AllAtLowDisorder;
            }

            if( (d_flags & AllAtLowMorale) &&
                !(iter.cp()->morale() >= 65))
            {
               d_flags &= ~AllAtLowMorale;
            }

            if(!(d_flags & NonRoutingFriendNear) &&
                  (Combat_Util::nonRoutingFriendsNear(bd, iter.cp(), 8, True)) )
            {
               d_flags |= NonRoutingFriendNear;
            }

            // See if we are holding any key terrain.
            // Key terrain being defined as
            if(!(d_flags & KeyTerrain) )
            {
               for(std::vector<DeployItem>::iterator di = iter.cp()->mapBegin();
                   di != iter.cp()->mapEnd();
                   ++di)
               {
                  if(di->active())
                  {
                     const BattleTerrainHex& hexInfo = bd->getTerrain(di->d_sp->hex());
                     if(isKeyTerrain(hexInfo))
                     {
                        d_flags |= KeyTerrain;
                        break;
                     }
                  }
               }
            }

            if(iter.cp()->routing())
               nRouting++;
         }
      }
   }

   // Get average morale
   Attribute aMorale = BobUtility::averageMorale(cp);
#ifdef DEBUG
   lLog.printf("Average morale = %d", static_cast<int>(aMorale));
#endif

   // Get Initial die roll (value between 1-10)
   int dieRoll = CRandom::get(1, 10);
   const Table1D<SWORD>& mTable = BattleTables::corpDeterminationDieRollModifiers();
   enum {
      Morale,
      PureCav,
      HoldingKeyTerrain,
      RussianInf,
      Charisma,
      ParentWithin8,
      AttachedLeaderCharisma,
      RoutLoss75,
      RoutLoss66,
      RoutLoss50,
      NoNonRoutingFriendNear,
      NoEffectLevel,
      CriticalLoss,
      LowDisorder,
      LowMorale,
      Over75NotRouted
   };

#ifdef DEBUG
   lLog.printf("Initial die-roll = %d", dieRoll);
#endif

   // +1 for each 25 or morale
   if(mTable.getValue(Morale) > 0)
      dieRoll += (aMorale / mTable.getValue(Morale));

#ifdef DEBUG
   lLog.printf("Die-roll after morale modifier = %d", dieRoll);
#endif

   // +2 if all units are cav and/or horse arty

    if(d_flags & AllCav)
   {
      dieRoll += mTable.getValue(PureCav);//2;
#ifdef DEBUG
      lLog.printf("Die roll after All-Cav modifier = %d", dieRoll);
#endif
   }

   if(d_flags & KeyTerrain)
   {
      dieRoll += mTable.getValue(HoldingKeyTerrain);//2;
#ifdef DEBUG
      lLog.printf("Die roll after Holding key-terrain modifier = %d", dieRoll);
#endif
   }

   // for Russian Infantry corps
   const UBYTE nMajorNations = 5;
   const UBYTE russia = 4;
   if(cp->generic()->isInfantry() &&
      cp->getNation() == russia)
   {
      dieRoll += mTable.getValue(RussianInf);
#ifdef DEBUG
      lLog.printf("Die-roll after russian infantry modifier = %d", dieRoll);
#endif

      // for nation in nation
      // TODO: need a nation member of batdata for what nation the battle
      //       is being fought in
   }

   // +1 for each 80 of charisma
   if(mTable.getValue(Charisma) > 0)
      dieRoll += (cp->leader()->getCharisma() / mTable.getValue(Charisma));
#ifdef DEBUG
   lLog.printf("Die-roll after leader charisma modifier = %d", dieRoll);
#endif

   CRefBattleCP attachedLeaderCP = getAttachedArmyLeader(bd, cp);
   // if no attached leader but parent is within 8 hexes
   if(attachedLeaderCP == NoBattleCP)
   {
      if(cp->parent() != NoBattleCP &&
          cp->parent()->leader()->getCharisma() >= 170 &&
          B_Logic::distanceFromUnit(cp->parent()->hex(), cp) <= 8)
      {
         dieRoll += mTable.getValue(ParentWithin8);//++;
#ifdef DEBUG
         lLog.printf("Die-roll after army leader modifier = %d", dieRoll);
#endif
      }
   }
   else
   {
      // +1 for eache 80 of charisma of attached leader
      if(mTable.getValue(AttachedLeaderCharisma) > 0)
         dieRoll += (attachedLeaderCP->leader()->getCharisma() / mTable.getValue(AttachedLeaderCharisma));
#ifdef DEBUG
      lLog.printf("Die-roll after attached leader modifier = %d", dieRoll);
#endif
   }

   // modifiers for strength / routing
   const int pRouting = (nUnits > 0) ? (nRouting * 100) / nUnits : 0;
    const int pStrengthLoss = (startStrength > 0) ?
         100 - ((curStrength * 100) / startStrength) : 0;

   // -7 if either is 75% or greater
   if(pRouting >= 75 || pStrengthLoss >= 75)
      dieRoll += mTable.getValue(RoutLoss75);//-= 7;

   // -4 if either is 66% or greater
   else if(pRouting >= 66 || pStrengthLoss >= 66)
      dieRoll += mTable.getValue(RoutLoss66);//-= 4;

   // -2 if either is 50% or greater
   else if(pRouting >= 50 || pStrengthLoss >= 50)
      dieRoll += mTable.getValue(RoutLoss50);//-= 2;

#ifdef DEBUG
   lLog.printf("Die-roll after routing / strength loss modifier = %d", dieRoll);
#endif

   // -1 if no non routing friend near (from another corps)
   if(!(d_flags & NonRoutingFriendNear))
   {
      dieRoll += mTable.getValue(NoNonRoutingFriendNear);//--;
#ifdef DEBUG
      lLog.printf("Die-roll after non-routing friend within 8 hexes = %d", dieRoll);
#endif
   }

   // -1 if all are below no effect level
    if(d_flags & AllAtNoEffectLevel)
   {
         dieRoll += mTable.getValue(NoEffectLevel);//--;
#ifdef DEBUG
      lLog.printf("Die-roll after all at no effect modifier = %d", dieRoll);
#endif
   }

   // -2 if all at critical loss
   if(d_flags & AllAtCriticalLoss)
   {
      dieRoll += mTable.getValue(CriticalLoss);//-= 2;
#ifdef DEBUG
      lLog.printf("Die-roll after all at critical loss modifier = %d", dieRoll);
#endif
   }

   // -1 if all are at low disorder
   if(d_flags & AllAtLowDisorder)
   {
      dieRoll += mTable.getValue(LowDisorder); //--;
#ifdef DEBUG
      lLog.printf("Die-roll after all at low disorder modifier = %d", dieRoll);
#endif
   }

   // -1 if all are at low disorder
   if(d_flags & AllAtLowMorale)
   {
         dieRoll += mTable.getValue(LowMorale);//--;
#ifdef DEBUG
         lLog.printf("Die-roll after all at low morale modifier = %d", dieRoll);
#endif
   }

   // +2 if 75% if more of corp not routing / fled
   int nDiv = 0;
   int nOutOfIt = 0;
   for(TBattleUnitIter<BUIV_All> iter(cp, BUIV_All());
         !iter.isFinished();
         iter.next())
   {
      if(iter.cp()->getRank().sameRank(Rank_Division))// && iter.cp()->sp() == NoBattleSP)
      {
         if(iter.cp()->hasQuitBattle() ||
            iter.cp()->routing() ||
            iter.cp()->fleeingTheField())
         {
            nOutOfIt++;
         }

         nDiv++;
      }
   }

#ifdef DEBUG
   lLog.printf("%s has %d divisions, %d are routed, fled", cp->getName(), nDiv, nOutOfIt);
#endif
   if(nDiv > 0 && (nOutOfIt * 100) / nDiv <= 25)
   {
      dieRoll += mTable.getValue(Over75NotRouted);
#ifdef DEBUG
      lLog.printf("Die-roll after 75% of corp not routed, modifier = %d", dieRoll);
#endif

   }

   // Process result
   //----------------------------------------------------------------------
   procDeterminationTestOutcome(bg, cp, dieRoll);
}

#ifdef DEBUG
void applyLoss(RPBattleData bd, const RefBattleCP& cp, int losses, LogFile& lf)
#else
void applyLoss(RPBattleData bd, const RefBattleCP& cp, int losses)
#endif
{
    // get percent chance loss may be artillery
    // apply only if this is inf or cav XX
    if(unitWipedOut(bd, cp))
         return;

    int nArty = (cp->generic()->isArtillery()) ? 0 : cp->nArtillery();
    int nTotal = cp->spCount() + nArty;
    ASSERT(nTotal > 0);

    int chanceForArtyLoss = MulDiv(nArty, 100, nTotal) / 2;

    // apply loss for every 10 points of losses
    const UWORD lossPer = 10;
    while(losses > 0)
    {
      // see if we loose artillery
      if(CRandom::get(100) < chanceForArtyLoss)
      {
         ASSERT(cp->nArtillery() >= 1);
         int nCounted = 0;
         int chance = MulDiv(1, 100, cp->nArtillery());

         RefBattleSP sp = cp->sp();
         while(sp != NoBattleSP)
         {
             const UnitTypeItem& uti = bd->ob()->getUnitType(sp);
             if(uti.getBasicType() == BasicUnitType::Artillery)
             {
               if(++nCounted >= cp->nArtillery() || CRandom::get(100) < chance)
               {
                  sp->applyLoss(lossPer);
#ifdef DEBUG
                  lf.printf("%ld (%s) is lossing 10 points (%d remaining)",
                      reinterpret_cast<LONG>(sp), uti.getName(), sp->strength());

                  if(sp->strength() == 0)
                  {
                     // ?
                  }
#endif
                  break;
               }
             }

             sp = sp->sister();
         }
      }

      // if not an arty loss, pick random sp
      else
      {
          bool lossApplied = False;
#ifdef DEBUG
          int loops = 0;
#endif
          for(;;)
          {
#ifdef DEBUG
               if(++loops == 3000)
               {
                   FORCEASSERT("Infinite loop in applyLoss()");
                   break;
               }
#endif
               // a 50% chance loss will be in front row
#if 1
               int c = CRandom::get(cp->columns());
               int r = (CRandom::get(100) < 50) ? 0 : CRandom::get(cp->rows());
               DeployItem* di = cp->currentDeployItem(c, r);
               ASSERT(di);
#else
               int index = (CRandom::get(100) < 50) ?
                   CRandom::get(cp->columns()) : CRandom::get(cp->spCount());

               std::vector<DeployItem>& map = cp->deployMap();
               DeployItem& di = map[index];
#endif
               if(di && di->active() && di->d_sp->strength() > 0)
               {
                   di->d_sp->applyLoss(lossPer);
                   if(di->d_sp->strength() == 0)
                        cp->shouldReorganize(True);

#ifdef DEBUG
                   const UnitTypeItem& uti = bd->ob()->getUnitType(di->d_sp);
                   if(r == 0)
                           lf.printf("Front-line SP in column %d takes losses", c);

                   lf.printf("%ld (%s) is lossing 10 points (%d remaining)",
                           reinterpret_cast<LONG>(di->d_sp), uti.getName(), di->d_sp->strength());
#endif
                   break;
               }

               if(unitWipedOut(bd, cp))
                   return;
          }
      }

      losses -= lossPer;
    }
}

void routUnit(BattleGameInterface* batgame, const RefBattleCP& cp, bool procCorpDetermination)
{
   cp->routing(True);
    while(cp->aggression() > 0)
      cp->decreaseAggression();
// cp->aggression(static_cast<BattleOrderInfo::Aggression>(0), SLONG_MAX);
   cp->disorder(static_cast<UBYTE>(0));
// cp->posture(BattleOrderInfo::Defensive);
   cp->setDefensive();
   cp->clearMove(True);

   if(procCorpDetermination)
   {
      if(cp->parent() != NoBattleCP && cp->parent()->getRank().sameRank(Rank_Corps))
         procCorpDeterminationTest(batgame, cp->parent());
   }

   // send player message
   {
      // our side
      BattleMessageInfo msg(cp->getSide(), BattleMessageID::BMSG_Routing, cp);
      batgame->sendPlayerMessage(msg);
   }

   // their side (for each unit in target list)
   SListIter<BattleItem> iter(&cp->targetList());
   while(++iter)
   {
      BattleMessageInfo msg(iter.current()->d_cp->getSide(), BattleMessageID::BMSG_EnemyRouting, iter.current()->d_cp);
      msg.target(cp);
      batgame->sendPlayerMessage(msg);
   }

}

};

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.5  2002/11/24 13:17:48  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * Revision 1.4  2001/07/30 09:27:23  greenius
 * Various bug fixes
 *
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
