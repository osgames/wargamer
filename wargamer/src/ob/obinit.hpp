/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OBINIT_H
#define OBINIT_H

#ifndef __cplusplus
#error obinit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Initialise Order of Battle Type flags
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#include "ob_dll.h"

class OrderBattle;

namespace GOB_Utility
{

OB_DLL void initUnitTypeFlags(OrderBattle* ob);

};  //namespace GOB_Utility


#endif /* OBINIT_H */

