/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Results Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "b_result.hpp"
#include "scenario.hpp"
#include <numeric>       // STL Algorithms

namespace {     // private functions

int percent(BattleLosses::ManCount n, BattleLosses::ManCount d)
{
    if(d == 0)
        return 100;
    else
        return (n * 100 + d/2) / d;     // rounds to nearest integer
}

};      // private namespace

/*
 * BattleLosses functions
 */

BattleLosses::BattleLosses()
{
    reset();
}

void BattleLosses::reset()
{
    for(int i = 0; i < BasicUnitType::HowMany; ++i)
    {
        d_losses[i] = 0;
        d_startStrength[i] = 0;
    }

    // fill(&d_losses[0], &d_losses[BasicUnitType::HowMany], 0);
    // fill(&d_startStrength[0], &d_startStrength[BasicUnitType::HowMany], 0);
}

BattleLosses::ManCount BattleLosses::losses() const
{
   return std::accumulate(&d_losses[0], &d_losses[BasicUnitType::HowMany], 0);
}

BattleLosses::ManCount BattleLosses::startStrength() const
{
    return std::accumulate(&d_startStrength[0], &d_startStrength[BasicUnitType::HowMany], 0);
}

int BattleLosses::lossPercent(BasicUnitType::value t) const
{
    return percent(losses(t), startStrength(t));
}

int BattleLosses::lossPercent() const
{
    return percent(losses(), startStrength());
}

/*
 * Battle Results functions
 */

BattleResults::BattleResults(const BattleFinish& bf) :
    d_finish(bf),
    d_losses(new BattleLosses[scenario->getNumSides()]),
    d_level(Draw),
    d_continue(Withdraw)
{
}

BattleResults::~BattleResults()
{
    delete[] d_losses;
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
