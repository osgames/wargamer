/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Leader List
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "leadlist.hpp"
#include "cplog.hpp"

const char* LeaderList::getChunkName() const
{
	return leaderChunkName;
}


/*
 * Garbage Colletion
 */

void LeaderList::cleanup()
{
#ifdef DEBUG
	cpLog.printf("Leader Garbage Collect");
	int count = 0;
#endif

	for(PoolIndex i = 0; i < entries(); i++)
	{
		if(isUsed(i))
		{
			if(get(i).refCount() == 0)
			{
#ifdef DEBUG
				++count;
				cpLog.printf("Deleted %3d, %s", (int)i, (const char*)get(i).getName());
#endif
				remove(i);
			}
		}
	}
#ifdef DEBUG
	cpLog.printf("Removed %d Leaders, %d of %d remaining", count, (int)used(), (int)entries());
#endif
}



