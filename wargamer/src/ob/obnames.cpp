/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 * Order of Battle Name Tables
 */

#include "stdinc.hpp"
#include "obNames.hpp"
#include "scenario.hpp"
#include "winfile.hpp"

namespace
{

StringFileTable* s_leaderNames = 0;
StringFileTable* s_cpNames = 0;

};    // private namespace

const char* OB_Names::getLeaderName(StringFileTable::ID id)
{
   ASSERT(s_leaderNames);
   if(id == StringFileTable::Null)
      return "Unnamed Leader";
   else if(s_leaderNames)
      return s_leaderNames->get(id);
   else
      return "Unknown Leader";
}

const char* OB_Names::getCPName(StringFileTable::ID id)
{
   ASSERT(s_cpNames);
   if(id == StringFileTable::Null)
      return "Unnamed Unit";
   else if(s_cpNames)
      return s_cpNames->get(id);
   else
      return "Unknown Unit";
}

StringFileTable::ID OB_Names::setLeaderName(const char* s)
{
   ASSERT(s_leaderNames);
   if(s_leaderNames)
      return s_leaderNames->add(s);
   throw GeneralError("LeaderName table is not intialised");
}

StringFileTable::ID OB_Names::setCPName(const char* s)
{
   ASSERT(s_cpNames);
   if(s_cpNames)
      return s_cpNames->add(s);
   throw GeneralError("UnitName table is not intialised");
}

StringFileTable::ID OB_Names::updateCPName(StringFileTable::ID id, const char* s)
{
   ASSERT(s_cpNames);
   if(s_cpNames)
      return s_cpNames->update(id, s);
   throw GeneralError("UnitName table is not intialised");
}

StringFileTable::ID OB_Names::updateLeaderName(StringFileTable::ID id, const char* s)
{
   ASSERT(s_leaderNames);
   if(s_cpNames)
      return s_leaderNames->update(id, s);
   throw GeneralError("LeaderName table is not intialised");
}




bool OB_Names::isValidCPName(StringFileTable::ID id)
{
   if(s_cpNames)
      return s_cpNames->isValid(id);
   else
      return false;
}

bool OB_Names::isValidLeaderName(StringFileTable::ID id)
{
   if(s_leaderNames)
      return s_leaderNames->isValid(id);
   else
      return false;
}


bool OB_Names::isNewCPName(StringFileTable::ID id)
{
   if(s_cpNames)
      return s_cpNames->isNew(id);
   else
      return true;
}

bool OB_Names::isNewLeaderName(StringFileTable::ID id)
{
   if(s_leaderNames)
      return s_leaderNames->isNew(id);
   else
      return true;
}


void OB_Names::create()
{
   ASSERT(s_leaderNames == 0);
   ASSERT(s_cpNames == 0);

   CString fName = scenario->makeScenarioFileName("leaderNames.dat", false);
   s_leaderNames = new StringFileTable(fName);

   fName = scenario->makeScenarioFileName("unitNames.dat", false);
   s_cpNames = new StringFileTable(fName);
}

void OB_Names::destroy()
{
   ASSERT(s_leaderNames);
   ASSERT(s_cpNames);

   delete s_leaderNames;
   s_leaderNames = 0;
   delete s_cpNames;
   s_cpNames = 0;
}


bool OB_Names::writeFile()
{
   try
   {
      s_leaderNames->writeFile();
      s_cpNames->writeFile();
   }
   catch(const FileError& e)
   {
      return false;
   }

   return true;
}

/*
 * $Log$
 */
