This directory contains source code for creating OB.DLL

OB.DLL contains code for generic processing of the Order of Battle.

By using pointers to Campaign and Battle Information, this module
will be independant of both the Campaign and Battle.  Seperate
interfaces for the Campaign and Battle are provided elsewhere.


Change:
Campaign and Battle OBs will hold seperate data structures
instead of the generic OB having pointers.

Problem with pointers is that generic OB would need to be dependant
on the campaign and battle classes so that it can delete them.  It
also complicates things in other ways.

As long as the application accesses the OB through higher level
campaign or battle OB classes, then it will all be transparent.
e.g. instead of saying
	cp->kill();
the application must do:
	army->kill(cpi);

We will also remove the IRefPtr business and leave that to the army
classes.
