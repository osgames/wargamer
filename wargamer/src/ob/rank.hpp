/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef RANK_HPP
#define RANK_HPP

#ifndef __cplusplus
#error rank.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Rank of units and leaders
 *
 *----------------------------------------------------------------------
 */

#include "ob_dll.h"
#include "mytypes.h"
#include "myassert.hpp"

class FileReader;
class FileWriter;

/*=====================================================================
 * Ranks....
 *
 * These are complicated because:
 *		1.	After Corps can be either an army or wing commander depending
 *			on whether it is attached or not
 *		2.	There can be unlimited levels above Army
 *
 * Updated 24 June 98 by Paul...
 *   Rank is now more narrowly defined. i.e.
 *   an Army Group is always an Army Group,
 *   an Army is always an Army, etc.
 *
 *   Armys and lower ranks can attach to Army-Groups
 *
 *   When applied to a leader, rank represents his command-ability, i.e.
 *   the highest rank he can effectively command
 */


enum RankEnum {
	Rank_God,				// Special entry point (not real)
	Rank_President,		// Controller of whole army (not real)

	Rank_ArmyGroup,
	Rank_Army,
	Rank_Corps,
	Rank_Division,

#if 0
	Rank_Executive,		// Any level above Army
	Rank_Army,
	Rank_ArmyWing,			// If detached it is Army, If attached then Wing.
	Rank_Corps,
	Rank_Division,
	Rank_Brigade,
#endif

	Rank_HowMany,
	Rank_Undefined
};


class OB_DLL Rank {
	static const UWORD fileVersion;

	RankEnum value;
public:
	Rank() { value = Rank_Undefined; }
	Rank(RankEnum r) { value = r; }
	Rank& operator = (RankEnum r) { value = r; return *this; }

	Boolean isHigher(const Rank& r) const { return value < r.value; }
	Boolean isLower(const Rank& r) const { return value > r.value; }

	Boolean isHigher(RankEnum r) const { return value < r; }
	Boolean isLower(RankEnum r) const { return value > r; }

	Boolean sameRank(const Rank& r) const { return value == r.value; }
	Boolean sameRank(RankEnum r) const { return value == r; }

	void setRank(RankEnum r) { value = r; }
	void setRank(const Rank& r) { value = r.value; }
	const Rank& getRank() const { return *this; }

	RankEnum getRankEnum() const { return value; }

	void promote()
	{
		ASSERT(value <= Rank_Division);

		if(value > Rank_ArmyGroup)
			value = RankEnum(value - 1);
	}

	void demote()
	{
		ASSERT(value <= Rank_Division);

		if(value < Rank_Division)
			value = RankEnum(value + 1);
	}

	const char* getRankName(Boolean attached = True) const;

	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;
};

inline const char* getRankName(const Rank& r, Boolean attached)
{
	return r.getRankName(attached);
}

// Convert level of dependants to a rank rating
// e.g. levelToRank(2, Rank_Division)
// will return ArmyWing


inline Rank levelToRank(int l, RankEnum bottomRank)
{
	int r = bottomRank - l;
	if(r < 0)
		return Rank(Rank_ArmyGroup);
	else
		return Rank(RankEnum(r));
}

#endif /* RANK_HPP */

