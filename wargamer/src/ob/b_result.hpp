/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef B_RESULT_HPP
#define B_RESULT_HPP

#ifndef __cplusplus
#error b_result.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle Results Information
 *
 *----------------------------------------------------------------------
 */

#include "ob_dll.h"
#include "gamedefs.hpp"
#include "unittype.hpp"
#include "obdefs.hpp"
// #include <string.hpp>

struct BattleResultTypes
{
    typedef int ManCount;
        // actual number of men, e.g. 70% SP = 700 men

    enum Why
    {
        UnknownWhy,
        EndDay,             // Day is over, no winner
        Surrender,          // Player wants rest of day calculated
        Defeated,           // Battle is really over

        Why_HowMany
    };

    enum HowDefeated
    {
        UnknownHow,
        RanAway,        // Side all left battlefield
        Destroyed,      // Side all destroyed
        Morale,         // Low Morale
        Determination,  // Leader failed Determination test

        How_HowMany
    };

    enum BattleContinueMode
    {
        Withdraw,
        Continue
    };

    enum VictoryLevel
    {
        Draw,
        WinningDraw,
        MinorVictory,
        MajorVictory,
        CrushingVictory,

        VictoryLevel_HowMany
    };
};

/*
 * Info for each side that can be passed to results screen
 */

class BattleLosses : public BattleResultTypes
{
        friend class BattleResults;

    private:    // only BattleResults can create it
        BattleLosses();
        ~BattleLosses() { }

    public:
        /*
         * Access functions and useful utilities
         */

        OB_DLL void reset();

        ManCount losses(BasicUnitType::value t) const { return d_losses[t]; }
        // const ManCount& losses(BasicUnitType::value t) const { return d_losses[t]; }
        OB_DLL ManCount losses() const;        // total
        void addLosses(BasicUnitType::value t, ManCount n)
        {
            d_losses[t] += n;
        }

        ManCount startStrength(BasicUnitType::value t) const { return d_startStrength[t]; }
        // const ManCount& startStrength(BasicUnitType::value t) const { return d_startStrength[t]; }
        OB_DLL ManCount startStrength() const; // total
        void addStartStrength(BasicUnitType::value t, ManCount n)
        {
            d_startStrength[t] += n;
        }

        OB_DLL int lossPercent(BasicUnitType::value t) const;
        OB_DLL int lossPercent() const;        // overall


    private:
        ManCount d_losses[BasicUnitType::HowMany];
        ManCount d_startStrength[BasicUnitType::HowMany];
};



class BattleFinish : public BattleResultTypes
{
    public:

        /*
         * Public Functions
         */

        ~BattleFinish()
        {
        }

        BattleFinish() :
            d_why(UnknownWhy),
            d_how(UnknownHow),
            d_winner(SIDE_Neutral)
        {
        }

        void endDay()
        {
            d_why = EndDay;
        }

        void surrender(Side s)
        {
            d_why = Surrender;
            d_winner = otherSide(s);
        }

        void defeated(Side winner, HowDefeated how)
        {
            d_why = Defeated;
            d_winner = winner;
            d_how = how;
        }

        void winner(Side side) { d_winner = side; }

        /*
         * Get functions
         */

        Why why() const { return d_why; }
        Side winner() const { return d_winner; }
        HowDefeated how() const { return d_how; }

    private:
        // Data Members

        Why d_why;
        Side d_winner;
        HowDefeated d_how;

};

/*
 * This is passed onto victory results screen
 * Campaign and Tactical subclass it to do the calculations and
 * return specific information needed by the  screen.
 */

class BattleResults : public BattleResultTypes
{
    public:
        OB_DLL BattleResults(const BattleFinish& bf);
        OB_DLL virtual ~BattleResults() = 0;

        const BattleLosses* losses(Side side) const { return &d_losses[side]; }

        Why why() const { return d_finish.why(); }

        void winner(Side side) { d_finish.winner(side); }
        Side winner() const { return d_finish.winner(); }
        HowDefeated how() const { return d_finish.how(); }

        void level(VictoryLevel level) { d_level = level; }
        VictoryLevel level() const { return d_level; }

        void shouldContinue(BattleContinueMode b) { d_continue = b; }
        BattleContinueMode willContinue() const { return d_continue; }

        virtual String battleName() const = 0;
        virtual ConstRefGLeader leader(Side s) const = 0;
        virtual ConstRefGenericCP cp(Side s) const = 0;

    protected:
        BattleLosses* losses(Side side) { return &d_losses[side]; }

    private:
        BattleLosses* d_losses;
        BattleFinish d_finish;
        VictoryLevel d_level;
        BattleContinueMode d_continue;        // set if battle is to continue next day
};


#endif /* B_RESULT_HPP */

