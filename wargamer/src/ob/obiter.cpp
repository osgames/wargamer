/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 * Order of Battle Iterators
 */

#include "stdinc.hpp"
#include "obiter.hpp"
#include "ob.hpp"

/*================================================================
 * Side Iterator
 */

OBSideIter::OBSideIter(const OrderBattle* ob) :
	d_ob(ob),
	d_next(-1),
	d_lock(ob)
{
	ASSERT(ob != 0);
}

bool OBSideIter::operator ++()
{
	if(++d_next < d_ob->sideCount())
		return True;
	else
	{
		d_next--;
		return False;
	}
}

Side OBSideIter::current() const
{
	ASSERT(d_next >= 0);
	ASSERT(d_next < d_ob->sideCount());

	return d_next;
}

void OBSideIter::reset()
{
	d_next = -1;
}


/*==================================================================
 * Unit Iterator
 */


G_UnitIter::G_UnitIter(OrderBattle* a) :
  d_container(a),
  d_cpi(NoGenericCP),
  d_top(a->topCommand()),
  d_started(False),
  d_lock(a)
{
	ASSERT(a != 0);
	ASSERT(d_container != 0);
}

G_UnitIter::G_UnitIter(OrderBattle* a, const RefGenericCP& startUnit) :
  d_container(a),
  d_cpi(startUnit),
  d_top(startUnit),
  d_started(False),
  d_lock(a)
{
	ASSERT(a != 0);
	ASSERT(d_container != 0);
	reset(startUnit);
}

G_UnitIter::G_UnitIter(const RefGenericCP& startUnit) :
  d_container(0),
  d_cpi(startUnit),
  d_top(startUnit),
  d_started(False),
  d_lock(0)
{
   ASSERT(startUnit != NoGenericCP);
   reset(startUnit);
}



void G_UnitIter::resetSide(Side s)
{
   ASSERT(d_container);
	reset(d_container->getFirstUnit(s));
}

void G_UnitIter::reset()
{
	// ASSERT(d_container != 0);
	d_cpi = NoGenericCP;
	d_started = False;
}

void G_UnitIter::reset(const RefGenericCP& startUnit)
{
	// ASSERT(d_container != 0);
   ASSERT(startUnit != NoGenericCP);
	d_top = startUnit;
	reset();
}


RefGenericCP G_UnitIter::current() const
{
	ASSERT(d_started);
	ASSERT(d_cpi != NoGenericCP);

	return d_cpi;
}

/*
 * Iterating using sister()
 * starts at top and does all of it's sisters
 */


bool G_UnitIter::sister()
{
	// ASSERT(d_container != 0);

	if(!d_started)
	{
		d_started = True;
		d_cpi = d_top;
		return (d_cpi != NoGenericCP);
	}
	else
	{
		ASSERT(d_cpi != NoGenericCP);
		d_cpi = d_cpi->getSister();
		return ( (d_cpi != NoGenericCP) && (d_cpi != d_top) );
	}

}

/*
 * Iterating using next()
 * Does top and all of it's children
 *
 * Added outer loop to auto-ignore deleted units
 */

bool G_UnitIter::next()
{
	// ASSERT(d_container != 0);

	if(!d_started)
	{
		d_started = True;

		d_cpi = d_top;
	}
	else
	{
		ASSERT(d_cpi != NoGenericCP);

		RefGenericCP oldcpi = d_cpi;

		d_cpi = d_cpi->getChild();

		while( (d_cpi == NoGenericCP) && (oldcpi != d_top) && (oldcpi != NoGenericCP) )
		{
			ASSERT(oldcpi != NoGenericCP);
			ASSERT(oldcpi != d_top);

			d_cpi = oldcpi->getSister();
			if(d_cpi == NoGenericCP)
			{
				oldcpi = oldcpi->getParent();
			}
		}
	}

	return (d_cpi != NoGenericCP);
}

/*
 * ConstUnitIter: There must be a way of sharing code with UnitIter
 */

G_ConstUnitIter::G_ConstUnitIter(const OrderBattle* a) :
  d_container(a),
  d_cpi(NoGenericCP),
  d_top(NoGenericCP),
  d_started(False),
  d_lock(a)
{
	ASSERT(a != 0);
	ASSERT(d_container != 0);
}

G_ConstUnitIter::G_ConstUnitIter(const OrderBattle* a, const ConstRefGenericCP& startUnit) :
  d_container(a),
  d_cpi(startUnit),
  d_top(startUnit),
  d_started(False),
  d_lock(a)
{
	ASSERT(a != 0);
	ASSERT(d_container != 0);
	reset(startUnit);
}

G_ConstUnitIter::G_ConstUnitIter(const ConstRefGenericCP& startUnit) :
  d_container(0),
  d_cpi(startUnit),
  d_top(startUnit),
  d_started(False),
  d_lock(0)
{
   ASSERT(startUnit != NoGenericCP);
   reset(startUnit);
}


void G_ConstUnitIter::resetSide(Side s)
{
   ASSERT(d_container);
	reset(d_container->getFirstUnit(s));
}

void G_ConstUnitIter::reset()
{
	// ASSERT(d_container != 0);
	d_cpi = NoGenericCP;
	d_started = False;
}

void G_ConstUnitIter::reset(const ConstRefGenericCP& startUnit)
{
	// ASSERT(d_container != 0);
   ASSERT(startUnit != NoGenericCP);
	d_top = startUnit;
	reset();
}


ConstRefGenericCP G_ConstUnitIter::current() const
{
	ASSERT(d_started);
	ASSERT(d_cpi != NoGenericCP);

	return d_cpi;
}

/*
 * Iterating using sister()
 * starts at top and does all of it's sisters
 */


bool G_ConstUnitIter::sister()
{
	// ASSERT(d_container != 0);

	if(!d_started)
	{
		d_started = True;
		d_cpi = d_top;
		return (d_cpi != NoGenericCP);
	}
	else
	{
		ASSERT(d_cpi != NoGenericCP);
		d_cpi = d_cpi->getSister();
		return ( (d_cpi != NoGenericCP) && (d_cpi != d_top) );
	}

}

/*
 * Iterating using next()
 * Does top and all of it's children
 *
 * Added outer loop to auto-ignore deleted units
 */

bool G_ConstUnitIter::next(bool intoChildren)
{
	// ASSERT(d_container != 0);

	if(!d_started)
	{
		d_started = True;
		d_cpi = d_top;
	}
	else
	{
		ASSERT(d_cpi != NoGenericCP);

		ConstRefGenericCP oldcpi = d_cpi;

		if(intoChildren)
			d_cpi = d_cpi->getChild();
		else
			d_cpi = NoGenericCP;

		while( (d_cpi == NoGenericCP) && (oldcpi != d_top) && (oldcpi != NoGenericCP) )
		{
			ASSERT(oldcpi != NoGenericCP);
			ASSERT(oldcpi != d_top);

			d_cpi = oldcpi->getSister();
			if(d_cpi == NoGenericCP)
			{
				oldcpi = oldcpi->getParent();
			}
		}
	}

	return (d_cpi != NoGenericCP);
}





/*---------------------------------------------------------------
 * SPIter
 */


G_SPIter::G_SPIter(OrderBattle* ob, const RefGenericCP& startUnit) :
	d_ob(ob),
	ui(ob, startUnit),
	si(ob)
{
//	ASSERT(ob != 0);
	ASSERT(startUnit != NoGenericCP);

	bool flag = newUnit();
#ifdef DEBUG
	ASSERT(flag);
#endif
}

bool G_SPIter::newUnit()
{
	if(ui.next())
	{
      // ASSERT(d_ob);
		// si.setSP(d_ob->getSPEntry(ui.current()));
      si.setSP(ui.current()->getSPEntry());
		return True;
	}
	else
		return False;
}

bool G_SPIter::operator ++()
{
	for(;;)
	{
		if(++si)
			return True;
		else
		{
			if(!newUnit())
				return False;
		}
	}
}

/*
 * Const_SPIter
 */

G_ConstSPIter::G_ConstSPIter(const OrderBattle* ob, const ConstRefGenericCP& startUnit) :
	d_ob(ob),
	ui(ob, startUnit),
	si(ob)
{
	// ASSERT(ob != 0);
	ASSERT(startUnit != NoGenericCP);

	bool flag = newUnit();
#ifdef DEBUG
	ASSERT(flag);
#endif
}

bool G_ConstSPIter::newUnit()
{
	if(ui.next())
	{
		// si.setSP(d_ob->getSPEntry(ui.current()));
      si.setSP(ui.current()->getSPEntry());
		return True;
	}
	else
		return False;
}

bool G_ConstSPIter::operator ++()
{
	for(;;)
	{
		if(++si)
			return True;
		else
		{
			if(!newUnit())
				return False;
		}
	}
}

/*---------------------------------------------------------
 * CPIter
 *
 * Combination of Side and Unit iterators
 * Can't see why can't just use UnitIter using God as the top cp?
 */


G_CPIter::G_CPIter(OrderBattle* ob) :
	ni(ob),
	ui(ob)
{
	if(++ni)
		ui.resetSide(ni.current());
#ifdef DEBUG
	else
		throw GeneralError("Can't initialise CPIter... no Nations!");
#endif
}

bool G_CPIter::sister()
{
	for(;;)
	{
		if(ui.sister())
			return True;
		else
		{
			if(++ni)
				ui.resetSide(ni.current());		// and loop
			else
				return False;
		}
	}
}

bool G_CPIter::next()
{
	for(;;)
	{
		if(ui.next())
			return True;
		else
		{
			if(++ni)
				ui.resetSide(ni.current());		// and loop
			else
				return False;
		}
	}
}

void G_CPIter::reset()
{
	ni.reset();

	if(++ni)
		ui.resetSide(ni.current());
#ifdef DEBUG
	else
		throw GeneralError("Can't reset CPIter... no Nations!");
#endif

}


/*
 * ConstCPIter
 */

G_ConstCPIter::G_ConstCPIter(const OrderBattle* ob) :
	ni(ob),
	ui(ob)
{
	if(++ni)
		ui.resetSide(ni.current());
#ifdef DEBUG
	else
		throw GeneralError("Can't initialise CPIter... no Nations!");
#endif
}

bool G_ConstCPIter::sister()
{
	for(;;)
	{
		if(ui.sister())
			return True;
		else
		{
			if(++ni)
				ui.resetSide(ni.current());		// and loop
			else
				return False;
		}
	}
}

bool G_ConstCPIter::next()
{
	for(;;)
	{
		if(ui.next())
			return True;
		else
		{
			if(++ni)
				ui.resetSide(ni.current());		// and loop
			else
				return False;
		}
	}
}

void G_ConstCPIter::reset()
{
	ni.reset();

	if(++ni)
		ui.resetSide(ni.current());
#ifdef DEBUG
	else
		throw GeneralError("Can't reset CPIter... no Nations!");
#endif

}

/*---------------------------------------------------------------
 * StrengthPointIter
 * goes through strength points directly attached to unit
 */

G_StrengthPointIter::G_StrengthPointIter(OrderBattle* a) :
	d_lock(a),
	d_isp(NoStrengthPoint),
	d_next(NoStrengthPoint),
	d_valid(False)
{
	ASSERT(a != 0);
}

G_StrengthPointIter::G_StrengthPointIter(OrderBattle* a, const ISP& isp) :
	d_lock(a)
{
	ASSERT(a != 0);

	// d_list = a->spList();
	setSP(isp);
}

G_StrengthPointIter::G_StrengthPointIter(OrderBattle* a, const RefGenericCP& cp) :
	d_lock(a)
{
	ASSERT(a != 0);
	setSP(cp->getSPEntry());
}

void G_StrengthPointIter::setSP(const ISP& isp)
{
	// d_currentSP = 0;
	d_isp = NoStrengthPoint;
	d_next = isp;
	d_valid = true;
}

void G_StrengthPointIter::setCP(const RefGenericCP& cp)
{
	setSP(cp->getSPEntry());
}

bool G_StrengthPointIter::operator ++()
{
	ASSERT(d_valid);

	d_isp = d_next;

	if(d_isp != NoStrengthPoint)
	{
		d_next = d_next->getNext();
	}

	if(d_isp == NoStrengthPoint)
		d_valid = False;

	return d_valid;
}

ISP G_StrengthPointIter::current() const
{
	ASSERT(d_valid);
	ASSERT(d_isp != NoStrengthPoint);
 	return d_isp;
}

/*
 * Const StrengthPointIter
 */

G_ConstStrengthPointIter::G_ConstStrengthPointIter(const OrderBattle* a) :
	d_lock(a)
{
	ASSERT(a != 0);

	// d_list = a->spList();
	d_isp = NoStrengthPoint;
	d_next = NoStrengthPoint;
	d_valid = False;
}

G_ConstStrengthPointIter::G_ConstStrengthPointIter(const OrderBattle* a, const ConstISP& isp) :
	d_lock(a)
{
	ASSERT(a != 0);

	// d_list = a->spList();
	setSP(isp);
}

void G_ConstStrengthPointIter::setSP(const ConstISP& isp)
{
	// d_currentSP = 0;
	d_isp = NoStrengthPoint;
	d_next = isp;
	d_valid = true;
}


bool G_ConstStrengthPointIter::operator ++()
{
	ASSERT(d_valid);

	d_isp = d_next;

	if(d_isp != NoStrengthPoint)
	{
		d_next = d_next->getNext();
	}

	if(d_isp == NoStrengthPoint)
		d_valid = False;

	return d_valid;
}

ConstISP G_ConstStrengthPointIter::current() const
{
	ASSERT(d_valid);
	ASSERT(d_isp != NoStrengthPoint);
 	return d_isp;
}


