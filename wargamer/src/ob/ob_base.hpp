/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OB_BASE_HPP
#define OB_BASE_HPP

#ifndef __cplusplus
#error ob_base.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Base function for order of battle so that member objects
 * can access it without a cyclic dependency
 *
 *----------------------------------------------------------------------
 */

#include "obdefs.hpp"
// #include "cp.hpp"
// #include "sp.hpp"
// #include "leader.hpp"

class BaseOrderBattle
{
	public:
		virtual ~BaseOrderBattle() = 0;

		virtual ISP sp(SPIndex) = 0;
		virtual RefGLeader leader(LeaderIndex) = 0;
		virtual RefGenericCP command(CPIndex) = 0;

		virtual LeaderIndex getIndex(const ConstRefGLeader&) const = 0;
		virtual SPIndex getIndex(const ConstISP&) const = 0;
		virtual CPIndex getIndex(const ConstRefGenericCP&) const = 0;
};

// inline BaseOrderBattle::~BaseOrderBattle() { }

#endif /* OB_BASE_HPP */

