/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef NATIONS_HPP
#define NATIONS_HPP

/*
 * Changeable Nations data
 */

#include "ob_dll.h"
#include "gamedefs.hpp"
#include "array.hpp"

class FileReader;
class FileWriter;

class OB_DLL Nations {
    static UWORD fileVersion;

    Side d_side;              // what side are we allied with
    Side d_friendlySide;      // what side are we friendly with
    Attribute d_morale;       // national morale level
    UWORD d_nLeadersCreated;  // How many leaders have we created?
    UBYTE d_nDepotsAllowed;          // Number depots player may build
    UBYTE d_nDepotsBuilt;          // Number depots built
    UBYTE d_nDepotsUC;          // Number depots under construction

	 Boolean d_neverEnter;     // this nation should never enter the game
	 Boolean d_wantsToDefect;
	 ULONG d_tick;
  public:
	 Nations() :
		d_side(SIDE_Neutral),
		d_friendlySide(SIDE_Neutral),
		d_morale(Attribute_Range),
		d_nDepotsAllowed(20),
		d_nDepotsBuilt(0),
		d_nDepotsUC(0),
		d_neverEnter(False),
		d_wantsToDefect(False),
		d_nLeadersCreated(0),
		d_tick(ULONG_MAX) { }

	 ~Nations() {}

	 Boolean active() const { return (!d_neverEnter && d_side != SIDE_Neutral); }
	 Boolean neverEnter() const { return d_neverEnter; }
	 Boolean wantsToDefect() const { return d_wantsToDefect; }
	 Side getSide() const { return d_side; }
	 Side getFriendlySide() const { return d_friendlySide; }
	 Attribute getMorale() const { return d_morale; }
	 UWORD nLeadersCreated() const { return d_nLeadersCreated; }
	 UBYTE nDepotsAllowed() const { return d_nDepotsAllowed; }
	 UBYTE nDepotsBuilt() const { return d_nDepotsBuilt; }
	 UBYTE nDepotsUC() const { return d_nDepotsUC; }
	 Boolean shouldReactivate(ULONG t)
	 {
		if(t > d_tick)
		{
		  d_tick = ULONG_MAX;
		  return True;
		}

		return False;
	 }

	 void neverEnter(Boolean f) { d_neverEnter = f; }
	 void wantsToDefect(Boolean f) { d_wantsToDefect = f; }
	 void setSide(Side s) { d_side = s; }
	 void setFriendlySide(Side s) { d_friendlySide = s; }
	 void setMorale(Attribute m) { d_morale = m; }
	 void incNLeadersCreated() { d_nLeadersCreated++; }
	 void reactivateIn(ULONG t) { d_tick = t; }
	 void setNDepotsAllowed(UBYTE n) { d_nDepotsAllowed = n; }
	 void setNDepotsBuilt(UBYTE n) { d_nDepotsBuilt = n; }
	 void setNDepotsUC(UBYTE n) { d_nDepotsUC = n; }
	 void incNDepotsBuilt()     { d_nDepotsBuilt++; }
	 void decNDepotsBuilt()     { d_nDepotsBuilt = (d_nDepotsBuilt > 0) ? d_nDepotsBuilt - 1 : 0; }
	 void incNDepotsUC()        { d_nDepotsUC++; }
	 void decNDepotsUC()        { d_nDepotsUC = (d_nDepotsUC > 0) ? d_nDepotsUC - 1 : 0; }

	 /*
	  * file interface
	  */

	 Boolean read(FileReader& f);
	 Boolean write(FileWriter& f) const;

};


class OB_DLL NationsList {
	 static UWORD fileVersion;
	 Array<Nations> d_nations;
  public:
	 NationsList() {}
	 ~NationsList() {}

	 void setAllegiance(Nationality n, Side s);
	 void wantsToDefect(Nationality n, Boolean f);
	 void setNeverEnter(Nationality n);
	 void setMorale(Nationality n, Attribute m);
	 void incLeadersCreated(Nationality n);
	 void reactivateIn(Nationality n, ULONG t);
	 void setNDepotsAllowed(Nationality n, UBYTE num);
	 void setNDepotsBuilt(Nationality n, UBYTE num);
	 void setNDepotsUC(Nationality n, UBYTE num);
	 void incNDepotsBuilt(Nationality n);
	 void decNDepotsBuilt(Nationality n);
	 void incNDepotsUC(Nationality n);
	 void decNDepotsUC(Nationality n);

	 Side getAllegiance(Nationality n) const;
	 Side getFriendlyTo(Nationality n) const;
	 Boolean isActive(Nationality n) const;
	 Boolean nationShouldNotEnter(Nationality n) const;
	 Boolean wantsToDefect(Nationality n) const;
	 Attribute getMorale(Nationality n) const;
	 UWORD nLeadersCreated(Nationality n) const;
	 UBYTE getNDepotsAllowed(Nationality n) const;
	 UBYTE getNDepotsBuilt(Nationality n) const;
	 UBYTE getNDepotsUC(Nationality n) const;
	 Boolean shouldReactivate(Nationality n, ULONG t);
	 int entries() const { return d_nations.entries(); }
	 static const char* getChunkName();

	 /*
	  *  file interface
	  */

	 Boolean read(FileReader& f);
	 Boolean write(FileWriter& f) const;
};

#endif
