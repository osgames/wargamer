/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Initialise Order of Battle Type flags
 *
 *----------------------------------------------------------------------
-*/

#include "stdinc.hpp"
#include "obinit.hpp"
#include "ob.hpp"
#include "cp.hpp"

void GOB_Utility::initUnitTypeFlags(OrderBattle* ob)
{
    // for(G_UnitIter iter(ob); !iter.isFinished(); iter.next())


    G_UnitIter iter(ob);
    while(iter.next())
    {
       RefGenericCP cp = iter.current();

      if(cp->getRank().isHigher(Rank_Division))
       {
          cp->clearFlags();
       }
    }

    iter.reset();
    while(iter.next())
    {
       // This line somehow went missing???
       RefGenericCP cp = iter.current();

       if(cp->getRank().sameRank(Rank_Division))
       {
          cp->clearFlags();
          const int maxSP = 14;
          const int maxType = 12;
          const int maxOtherType = 2;
          int nInf    = 0;
          int nCav    = 0;
          int nArt    = 0;
          int nOther  = 0;
          int spCount = 0;
          bool isHeavyCav = True;
          bool isGuard = True;
          bool isOldGuard = True;
          bool isMounted = True;
          // TODO: Add heavy artillery to unit type table
          bool isHeavyArt = False;

          // count types

          G_StrengthPointIter spIter(ob, cp);
          while(++spIter)
          {
             ISP sp = spIter.current();

            const UnitTypeItem& uti = ob->getUnitType(sp->getUnitType());
            switch(uti.getBasicType())
            {
               case BasicUnitType::Infantry:
                  nInf++;
                  break;
               case BasicUnitType::Cavalry:
                  nCav++;
                  break;
               case BasicUnitType::Artillery:
                  nArt++;
                  break;
               case BasicUnitType::Special:
                  nOther++;
                  break;
          #ifdef DEBUG
               default:
                  FORCEASSERT("Improper type in unitypes");
          #endif
            }

            // check various flags
            if(!uti.isMounted())
               isMounted = False;

            if(!uti.isGuard())
               isGuard = False;

            if(!uti.isOldGuard())
               isOldGuard = False;

            if(uti.getBasicType() != BasicUnitType::Cavalry || uti.isLightCavalry())
               isHeavyCav = False;

            // TODO: check for heavy artillery
          }

          spCount = nInf + nArt + nCav + nOther;
          ASSERT(spCount <= maxSP);

          // if we have infantry, then this is an infantry XX
          // no matter what other SP we may have
          if(nInf > 0)
          {
            cp->makeInfantryType(True);
            ASSERT(nInf <= maxType);
            ASSERT(nCav <= maxOtherType);
            ASSERT(nArt <= maxOtherType);
            ASSERT(!isMounted);
          }

          // if we have cavalry, then this is a cavalry XX
          // cavalry can only contain Cav and Horse Artillery
          else if(nCav > 0)
          {
            cp->makeCavalryType(True);
            ASSERT(isMounted);
            ASSERT(nInf == 0);
            ASSERT(nCav <= maxType);
            ASSERT(nArt <= maxOtherType);
            ASSERT(nOther == 0);
          }

          // if we have artillery, make it an arty XX
          // artillery can only contain artillery SP or Special types
          else if(nArt > 0)
          {
            cp->makeArtilleryType(True);
            ASSERT(nArt <= maxType);
            ASSERT(nInf == 0);
            ASSERT(nCav == 0);
          }

          // if we have only special type
          else
          {
            // probably shoudn't be here
            cp->makeInfantryType(True);
          }

          // set any flags
          cp->makeGuard(isGuard);
          cp->makeOldGuard(isOldGuard);
          cp->makeHeavyCavalryType(isHeavyCav);
          cp->makeHeavyArtilleryType(isHeavyArt);
          cp->makeHorseArtilleryType(isMounted && cp->isArtillery());

          bool isInf = cp->isInfantry();
          bool isCav = cp->isCavalry();
          bool isArt = cp->isArtillery();

          RefGenericCP parent = cp->getParent();
          while(parent != NoGenericCP && parent->getRank().isLower(Rank_President))
          {
            if(isInf)
               parent->makeInfantryType(True);

            if(isCav)
               parent->makeCavalryType(True);

            if(isArt)
               parent->makeArtilleryType(True);

            parent = parent->getParent();
          }

       }
    }
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
