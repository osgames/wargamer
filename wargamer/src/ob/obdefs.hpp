/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OBDEFS_HPP
#define OBDEFS_HPP

#ifndef __cplusplus
#error obdefs.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Order of Battle Definitions
 *
 *----------------------------------------------------------------------
 */

// #define NO_REFPTR_IMP

#include "mytypes.h"
#include "poolarry.hpp"
#include "refptr.hpp"

class StrengthPoint;
typedef RefPtr<StrengthPoint> ISP;
typedef CRefPtr<StrengthPoint> ConstISP;
static const ISP NoStrengthPoint = 0;

inline StrengthPoint* constSPtoSP(const ConstISP& isp)
{
   const StrengthPoint* sp = isp;
   return const_cast<StrengthPoint*>(sp);
}


class GenericCP;
typedef RefPtr<GenericCP> RefGenericCP;
typedef CRefPtr<GenericCP> ConstRefGenericCP;
static const RefGenericCP NoGenericCP = 0;
// #define NoGenericCP static_cast<GenericCP*>(0)
// #define NoGenericCP RefGenericCP(0)


class GLeader;
typedef RefPtr<GLeader> RefGLeader;
typedef CRefPtr<GLeader> ConstRefGLeader;
static const RefGLeader NoGLeader = 0;

inline GLeader* constGLeaderToGLeader(const ConstRefGLeader& iLeader)
{
   const GLeader* leader = iLeader;
   return const_cast<GLeader*>(leader);
}


/*
 * Simple Array Indeces used internally by OB
 */

typedef PoolIndex CPIndex;
typedef PoolIndex LeaderIndex;
typedef PoolIndex SPIndex;

static const CPIndex NoCPIndex = PoolIndex_NULL;
static const LeaderIndex NoLeaderIndex = PoolIndex_NULL;
static const SPIndex NoSPIndex = PoolIndex_NULL;


#include "cp.hpp"
#include "sp.hpp"
#include "leader.hpp"


#endif /* OBDEFS_HPP */

