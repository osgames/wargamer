/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SP_HPP
#define SP_HPP

#ifndef __cplusplus
#error sp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Strength Point Item
 *
 *----------------------------------------------------------------------
 */

#include "ob_dll.h"
#include "gamedefs.hpp"
#include "obdefs.hpp"
#include "refptr.hpp"

// #include "cp.hpp"
// #include "leader.hpp"

// #ifdef DEBUG
// #define LOG_SP_REF
// #endif

class BaseOrderBattle;
class BattleSP;

class OB_DLL StrengthPoint :
	public RefBaseCount
{
	static const UWORD fileVersion;

#ifdef DEBUG
	static int s_nextID;
	int d_id;
#endif

	UnitType	d_unitType;		// 1 of 64 types
	ISP 		d_next;

// ---------- Unchecked Update -----------------------
	BattleSP* d_battleInfo;
// ---------------------------------------------------
public:

	StrengthPoint();

	void setUnitType(UnitType t) { d_unitType = t; }
	UnitType getUnitType() const { return d_unitType; }

	void setNext(const ISP& n) { d_next = n; }
	const ISP& getNext() const { return d_next; }
	// ISP& r_next() { return d_next; }

	void battleInfo(BattleSP* bi)  { d_battleInfo = bi; }
	BattleSP* battleInfo() { return d_battleInfo; }
	const BattleSP* battleInfo() const { return d_battleInfo; }

#ifdef LOG_SP_REF
		int addRef() const;
		int delRef() const;
#else
		int addRef() const { return RefBaseCount::addRef(); }
		int delRef() const { return RefBaseCount::delRef(); }
#endif
		int refCount() const { return RefBaseCount::refCount(); }

		// StrengthPoint* operator -> () { return this; }
		// const StrengthPoint* operator -> () const { return this; }

	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f, BaseOrderBattle& ob);
	Boolean writeData(FileWriter& f, const BaseOrderBattle& ob) const;

#ifdef DEBUG
	int getID() const { return d_id; }
#endif
};

// ----------------- Unchecked update ----------------------
inline const BattleSP* battleSP(const ConstISP& isp)
{
  return (isp != NoStrengthPoint) ? isp->battleInfo() : 0;
}


inline BattleSP* battleSP(const ISP& isp)
{
  return (isp != NoStrengthPoint) ? isp->battleInfo() : 0;
}
//-------------------------------------------------------------

typedef StrengthPoint StrengthPointItem;	// Backwards compatibility


typedef RefPtr<StrengthPoint> RefRealSP;
typedef CRefPtr<StrengthPoint> CRefRealSP;

static StrengthPoint* const NoRealSP = 0;


#endif /* SP_HPP */

