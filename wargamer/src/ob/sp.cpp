/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Strength Point Item
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "sp.hpp"
#include "wldfile.hpp"
#include "filebase.hpp"		// FileReader and FileWriter
#include "ob_base.hpp"
#include "leader.hpp"
#ifdef DEBUG
#include "cplog.hpp"
#include "todolog.hpp"
#endif

#ifdef DEBUG
int StrengthPoint::s_nextID = 0;
#endif

StrengthPoint::StrengthPoint() :
	d_next(NoStrengthPoint),
	d_unitType(NoUnitType),
// ---------- Unchedked Update ----------------
	d_battleInfo(0)
// ---------------------------------------------
{
#ifdef DEBUG
	d_id = s_nextID++;
#endif
}


const UWORD StrengthPoint::fileVersion = 0x0003;

Boolean StrengthPoint::readData(FileReader& f, BaseOrderBattle& ob)
{
	if(f.isAscii())
	{
#ifdef DEBUG
		todoLog.printf("TODO: StrengthPoint::readData().ascii unwritten\n");
#endif
		return False;
	}
	else
	{
		UWORD version;
		f.getUWord(version);
		ASSERT(version <= fileVersion);

		if(version >= 0x0003)
		{
			f >> d_unitType;

			SPIndex sp;
			f >> sp;
			d_next = ob.sp(sp);
			// f >> d_next;
		}
		else
		{
			UWORD spVersion;
			f >> spVersion;		// Old strengthpoint version

			UBYTE b;
			f.getUByte(b);
			d_unitType = (UnitType) b;

			if(spVersion < 0x0001)
			{
				UBYTE experience;
				UBYTE strength;

				f.getUByte(experience);
				f.getUByte(strength);
			}
			else if(spVersion < 0x0002)
			{
				UBYTE flags;		// Used to be used for straggler flag
				f.getUByte(flags);
			}

			if(version < 0x0001)
			{
				UWORD listVersion;
				f >> listVersion;
			}

			SPIndex sp;
			f >> sp;
			d_next = ob.sp(sp);
			// f >> d_next;
		}
	}

	return f.isOK();
}

Boolean StrengthPoint::writeData(FileWriter& f, const BaseOrderBattle& ob) const
{
	if(f.isAscii())
	{
		f.printf(startItemString);
		f.printf(fmt_sddn, strengthPointToken,
			(int) d_unitType, (int) ob.getIndex(d_next));
		f.printf(endItemString);
	}
	else
	{
		f << fileVersion;
		f << d_unitType;
		f << ob.getIndex(d_next);
	}

	return f.isOK();
}

#ifdef LOG_SP_REF
int StrengthPoint::addRef() const
{
	int value = RefBaseCount::addRef(); 
	cpLog.printf("Add SP %3d = %d", d_id, value);
	return value;
}

int StrengthPoint::delRef() const
{
	int value = RefBaseCount::delRef(); 
	cpLog.printf("Del SP %3d = %d", d_id, value);
	return value;
}
#endif

