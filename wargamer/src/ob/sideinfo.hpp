/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef SIDEINFO_HPP
#define SIDEINFO_HPP

#ifndef __cplusplus
#error sideinfo.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Information about each side in an OB
 *
 * This used to be compos.ccp\class Nation
 *
 *----------------------------------------------------------------------
 */

#include "ob_dll.h"
#include "mytypes.h"
// #include "icompos.hpp"
#include "gamedefs.hpp"
#include "obdefs.hpp"
#include "rank.hpp"
#include "cp.hpp"
#include "leader.hpp"
#include "sp.hpp"

class FileReader;
class FileWriter;

class BaseOrderBattle;

class OB_DLL OBSideInfo
{
	static const UWORD fileVersion;

	RefGenericCP	d_president;			// Entry point to side's President
	RefGLeader 		d_supremeLeader;              // SHQ
	UWORD				d_rankCount[Rank_HowMany];				// Next name, e.g. 54th Division
public:
	OBSideInfo();
	~OBSideInfo() { }

	void setSupremeLeader(const RefGLeader& who) { d_supremeLeader = who; }
   RefGLeader getSupremeLeader() const { return d_supremeLeader; }

	void setPresident(const RefGenericCP& t) { d_president = t; }
	RefGenericCP getPresident() const { return d_president; }

	UWORD nextRankCount(Rank r);
	void clearRankCounts();

	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f, BaseOrderBattle& ob);
	Boolean writeData(FileWriter& f, const BaseOrderBattle& ob) const;
};






#endif /* SIDEINFO_HPP */

