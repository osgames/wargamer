/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Ranks
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "rank.hpp"
#include "filebase.hpp"
#include "wldfile.hpp"
#include "todolog.hpp"
#include "resstr.hpp"

const char* Rank::getRankName(Boolean attached) const
{
	ASSERT(value < Rank_HowMany);

#if 0
	static const char* rankStr[] = {
		"God",				// Entry point to entire OB
		"President",		// Commander of entire side

		"Army Group",		// Controller of Armies
		"Army",				// Controller of Corps
		"Corps",				// Controller of Divisions
		"Division"			// Controller of Brigades or SPs

	};
#endif

#if 0
	RankEnum r = value;

	if(r == Rank_ArmyWing && !attached)
		r = Rank_Army;

	return rankStr[r];
#endif
	// return rankStr[value];

   return InGameText::get(value + IDS_RankEnum);
}


const UWORD Rank::fileVersion = 0x0001;

Boolean Rank::readData(FileReader& f)
{
	if(f.isAscii())
	{
#ifdef DEBUG
		todoLog.printf("TODO: Rank::readData().ascii unwritten\n");
#endif
		return False;
	}
	else
	{
		UWORD version;
		f.getUWord(version);
		ASSERT(version <= fileVersion);

		UBYTE b;
		f.getUByte(b);

		// bodge for enum change
		if(version == 0x0000)
		{
		  if( (b == 5) || (b == 6) )
			 b--;
		}

		if(b >= Rank_Undefined)
			b = Rank_Division;

		value = static_cast<RankEnum>(b);
	}

	return f.isOK();
}

Boolean Rank::writeData(FileWriter& f) const
{
	if(f.isAscii())
	{
		f.printf(fmt_sdn, rankToken, (int) value);
	}
	else
	{
		f.putUWord(fileVersion);
		f.putUByte(value);
	}

	return f.isOK();
}



