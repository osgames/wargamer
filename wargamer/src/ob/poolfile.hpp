/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef POOLFILE_HPP
#define POOLFILE_HPP

#ifndef __cplusplus
#error poolfile.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	PoolArray that can be saved to a file
 *
 *----------------------------------------------------------------------
 */

#include "poolarry.hpp"
#include "filecnk.hpp"
#include "wldfile.hpp"
#ifdef DEBUG
#include "todolog.hpp"
#endif

// #pragma warning 4 9;		// Disable virtual destructor warning

template<class T>
class PoolArrayFileObject : public PoolArray<T>
{
private:
	// void remove(PoolIndex i) { PoolArray<T>::remove(i); }		// Demote to protected
public:
	virtual ~PoolArrayFileObject() = 0;

	virtual Boolean readData(FileReader& f);
	virtual Boolean writeData(FileWriter& f) const;
	virtual const char* getChunkName() const = 0;
};

template<class T>
inline PoolArrayFileObject<T>::~PoolArrayFileObject() { }


static const UWORD PoolArrayVersion	= 0x0000;

template<class T>
Boolean PoolArrayFileObject<T>::readData(FileReader& f)
{
	FileChunkReader fr(f, getChunkName());
	if(!fr.isOK())
		return False;

	if(f.isAscii())
	{
#ifdef DEBUG
		todoLog.printf("TODO: PoolArrayFileObject<T>::readItems.ascii unwritten\n");
#endif
		return False;
	}
	else
	{
		UWORD version;
		f.getUWord(version);
		ASSERT(version <= PoolArrayVersion);
		UWORD count;
		f.getUWord(count);

		if(count != 0)
		{
			reserve(count, true);		// Reserve and mark as used
		}
			// init(count);
	}

	for(PoolIndex i = 0; i < entries(); i++)
	{
		UBYTE flag;
		f.getUByte(flag);
		if(flag)
		{
			get(i).readData(f);
			// get(i).addRef();
		}
		else
			remove(i);
	}

	return f.isOK();
}

template<class T>
Boolean PoolArrayFileObject<T>::writeData(FileWriter& f) const
{
	FileChunkWriter fc(f, getChunkName());

	if(f.isAscii())
	{
		f.printf(fmt_sdn, entriesToken, (int) entries());
	}
	else
	{
		f.putUWord(PoolArrayVersion);
		f.putUWord(entries());
	}

	for(PoolIndex i = 0; i < entries(); i++)
	{
		UBYTE flag = isUsed(i);

		if(f.isAscii())
		{
			if(!flag)
				f.printf(fmt_sn, unusedToken);
		}
		else
			f.putUByte(flag);

		if(flag)
			get(i).writeData(f);

	}

	return f.isOK();
}



#endif /* POOLFILE_HPP */

