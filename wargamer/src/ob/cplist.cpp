/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	CommandPosition List
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "cplist.hpp"
#include "filecnk.hpp"
#include "wldfile.hpp"
#ifdef DEBUG
// #include "unitlog.hpp"
#include "todolog.hpp"
#include "cplog.hpp"

#endif	//DEBUG

GenericCPList::~GenericCPList() 
{
#ifdef DEBUG
	/*
	 * Note that this line is important because it removes the
	 * ICommmandPosition reference in cuLog, which would otherwise
	 * crash it later on.
	 */

	//--- This should be done in campdata\compos.cpp
	// cuLog.printf(NoCPIndex, "GenericCPList deleted");
#endif
	// ICommandPosition::setList(0); 
}

CPIndex GenericCPList::add()
{
	CPIndex cpi = Super::add();
	ASSERT(cpi != NoCPIndex);
	get(cpi).setSelf(cpi);
	return cpi;
}

const char* GenericCPList::getChunkName() const
{
	return commandChunkName;
}

static const UWORD GenericCPListVersion	= 0x0000;

Boolean GenericCPList::readData(FileReader& f, BaseOrderBattle& ob)
{
	// Boolean result = Super::readData(f, ob);

	FileChunkReader fr(f, getChunkName());
	if(!fr.isOK())
		return False;

	if(f.isAscii())
	{
#ifdef DEBUG
		todoLog.printf("TODO: PoolArrayFileObject<T>::readItems.ascii unwritten\n");
#endif
		return False;
	}
	else
	{
		UWORD version;
		f.getUWord(version);
		ASSERT(version <= GenericCPListVersion);
		UWORD count;
		f.getUWord(count);

		if(count != 0)
		{
			reserve(count, true);		// Reserve and mark as used
		}
			// init(count);
	}

	for(PoolIndex i = 0; i < entries(); i++)
	{
		UBYTE flag;
		f.getUByte(flag);
		if(flag)
		{
			get(i).readData(f, ob);
			get(i).setSelf(i);
			// get(i).addRef();
		}
		else
			remove(i);
	}


#if 0
	if(f.isOK())
	{
		for(CPIndex i = 0; i < entries(); i++)
		{
			if(isUsed(i))
			{
				GenericCP& cp = get(i);
				cp.setSelf(i);

				// Fix up reference counts

				if(cp.getChild() != NoCPIndex)
					addRef(cp.getChild());
				if(cp.getParent() != NoCPIndex)
					addRef(cp.getParent());
				if(cp.getSister() != NoCPIndex)
					addRef(cp.getSister());
			}
		}
	}
#endif
	return f.isOK();	// result;
}



Boolean GenericCPList::writeData(FileWriter& f, const BaseOrderBattle& ob) const
{
	FileChunkWriter fc(f, getChunkName());

	if(f.isAscii())
	{
		f.printf(fmt_sdn, entriesToken, (int) entries());
	}
	else
	{
		f.putUWord(GenericCPListVersion);
		f.putUWord(entries());
	}

	for(PoolIndex i = 0; i < entries(); i++)
	{
		UBYTE flag = isUsed(i);

		if(f.isAscii())
		{
			if(!flag)
				f.printf(fmt_sn, unusedToken);
		}
		else
			f.putUByte(flag);

		if(flag)
			get(i).writeData(f, ob);

	}

	return f.isOK();
}



/*
 * Garbage Colletion
 */

void GenericCPList::cleanup()
{
#ifdef DEBUG
	cpLog.printf("CP Garbage Collect");
	int count = 0;
#endif

	for(PoolIndex i = 0; i < entries(); i++)
	{
		if(isUsed(i))
		{
			if(get(i).refCount() == 0)
			{
#ifdef DEBUG
				++count;
				cpLog.printf("Deleted %3d, %s", (int)i, (const char*)get(i).getName());
#endif
				remove(i);
			}
		}
	}
#ifdef DEBUG
	cpLog.printf("Removed %d CPs, %d of %d remaining", count, (int)used(), (int)entries());
#endif
}



