/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef OB_HPP
#define OB_HPP

#ifndef __cplusplus
#error ob.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Generic Order of Battle Interface
 *
 * Campaign and Battle build on this for Armies and BattleArmy
 *
 *----------------------------------------------------------------------
 */

#include "ob_dll.h"
#include "ob_base.hpp"
#include "sync.hpp"		// For RWLock
#include "gamedefs.hpp"
#include "obdefs.hpp"
#include "cplist.hpp"
#include "splist.hpp"
#include "leadlist.hpp"
#include "sideinfo.hpp"
#include "unittype.hpp"
#include "nations.hpp"
#include "obiter.hpp"

class OB_DLL OrderBattle :
	public RWLock,
	public BaseOrderBattle
{
	static const UWORD fileVersion;

 public:
	enum {
		MaxSPperDivision = 12
	};

    class ReadLock
    {
        public:
            ReadLock(const OrderBattle* ob) : d_ob(ob) { ob->startRead(); }
            ~ReadLock() { d_ob->endRead(); d_ob = 0; }
        private:
            const OrderBattle* d_ob;
    };

    class WriteLock
    {
        public:
            WriteLock(OrderBattle* ob) : d_ob(ob) { ob->startWrite(); }
            ~WriteLock() { d_ob->endWrite(); d_ob = 0; }
        private:
            OrderBattle* d_ob;
    };

 private:
	Side	 						d_nSides;
	GenericCPList	 	      d_commands;
	LeaderList				 	d_leaders;
	StrengthPointList 		d_sps;
	RefGenericCP			   d_topCommand;		// slot to allow easy access to OB
	OBSideInfo*					d_nations;
	NationsList             d_nationsList;    // Changeable Nation data
	ULONG 						d_obChanged;		// Incremented every time a change is made to Order of Battle
	UnitTypeTable	      	d_unitTypes;		// Unit Type table
	UWORD*                  d_leadersCreated; // Number of leaders created (except Pool leaders)

	/*
	 * Functions
	 */

	// Disable Copy constructor and assign

	OrderBattle(const OrderBattle&);
	OrderBattle& operator = (const OrderBattle&);

 public:
	OrderBattle();
	~OrderBattle();

	/*
	 * Index <--> value functions
	 */

	virtual SPIndex getIndex(const ConstISP& spi) const { return d_sps.getIndex(spi); }
	virtual CPIndex getIndex(const ConstRefGenericCP& cpi) const { return d_commands.getIndex(cpi); }
	virtual LeaderIndex getIndex(const ConstRefGLeader& l) const { return d_leaders.getIndex(l); }

	virtual ISP sp(SPIndex sp) { return d_sps[sp]; }
	virtual RefGLeader leader(LeaderIndex l) { return d_leaders[l]; }
	virtual RefGenericCP command(CPIndex cp) { return d_commands[cp]; }

	ISP getSP(SPIndex sp) { return d_sps[sp]; }
	ConstISP getSP(SPIndex sp) const { return d_sps[sp]; }

	RefGenericCP getCommand(CPIndex cpi) { return d_commands[cpi]; }
	ConstRefGenericCP getCommand(CPIndex cpi) const { return d_commands[cpi]; }

	RefGLeader getLeader(LeaderIndex ileader) { return d_leaders[ileader]; }
	ConstRefGLeader getLeader(LeaderIndex ileader) const { return d_leaders[ileader]; }

	/*
	 * Accessors
	 */

	const UnitTypeTable& getUnitTypes() const { return d_unitTypes; }
	int getMaxUnitType() const { return d_unitTypes.entries(); }
	const UnitTypeItem& getUnitType(UnitType n) const { return d_unitTypes[n]; }

	// useful hasType functions
	Boolean hasBasicType(const ConstRefGenericCP& cpi, BasicUnitType::value t) const;
	Boolean hasSpecialType(const ConstRefGenericCP& cpi, SpecialUnitType::value t) const;
	Boolean hasInfantry(const ConstRefGenericCP& cpi) const { return hasBasicType(cpi, BasicUnitType::Infantry); }
	Boolean hasCavalry(const ConstRefGenericCP& cpi) const { return hasBasicType(cpi, BasicUnitType::Cavalry); }
	Boolean hasArtillery(const ConstRefGenericCP& cpi) const { return hasBasicType(cpi, BasicUnitType::Artillery); }
	Boolean hasEngineer(const ConstRefGenericCP& cpi) const { return hasSpecialType(cpi, SpecialUnitType::Engineer); }
	Boolean hasBridgeTrain(const ConstRefGenericCP& cpi) const { return hasSpecialType(cpi, SpecialUnitType::BridgeTrain); }
	Boolean hasSiegeTrain(const ConstRefGenericCP& cpi) const { return hasSpecialType(cpi, SpecialUnitType::SiegeTrain); }

	// UBYTE getSideCount() const { return d_nSides; }
	Side sideCount() const { return d_nSides; }

	ConstRefGenericCP getPresident(Side s) const { return getNation(s)->getPresident(); }
	RefGenericCP getPresident(Side s) { return getNation(s)->getPresident(); }
	void setPresident(Side s, const RefGenericCP& cp) { getNation(s)->setPresident(cp); }

	ConstRefGenericCP getFirstUnit(Side s) const;
	RefGenericCP getFirstUnit(Side s);

	RefGenericCP getTop() const { return d_topCommand; }
	RefGenericCP topCommand() const { return d_topCommand; }
	void topCommand(const RefGenericCP& cpi) { d_topCommand = cpi; }

	static const char* getUnitName(const ConstRefGenericCP& cp, char* buffer = 0);

	void setSupremeLeader(Side side, const RefGLeader& iLeader);
	RefGLeader getSupremeLeader(Side side) const;
	RefGLeader getSupremeLeaderID(Side side) const { return getSupremeLeader(side); }
	RefGenericCP getSHQ(Side side) const;
	void chooseNewSHQ(Side side);

	/*
	 * Changeable Nations data
	 */

	NationsList& getNations() { return d_nationsList; }
	const NationsList& getNations() const { return d_nationsList; }
	Side getNationAllegiance(Nationality n) const { return d_nationsList.getAllegiance(n); }
	Boolean isNationActive(Nationality n) const { return d_nationsList.isActive(n); }
	Boolean shouldNationEnter(Nationality n) { return !d_nationsList.nationShouldNotEnter(n); }
	Attribute getNationMorale(Nationality n) const { return d_nationsList.getMorale(n); }
	Boolean shouldReactivateNation(Nationality n, ULONG t) { return d_nationsList.shouldReactivate(n, t); }

	void setNationAllegiance(Nationality n, Side s) { d_nationsList.setAllegiance(n, s); }
	void nationNeverEnters(Nationality n) { d_nationsList.setNeverEnter(n); }
	void setNationMorale(Nationality n, Attribute m) { d_nationsList.setMorale(n, m); }
	void reactivateNationIn(Nationality n, ULONG t) { d_nationsList.reactivateIn(n, t); }

	static RefGLeader getUnitLeader(const ConstRefGenericCP& cpi)
	{
		ASSERT(cpi->leader() != NoGLeader);
		return cpi->leader();
	}

	ConstRefGLeader whoShouldCommand(const ConstRefGLeader& leader1, const ConstRefGLeader& leader2) const;

	static Rank getRank(const ConstRefGenericCP& cpi) { return cpi->getRank(); }

#if !defined(EDITOR)
	Boolean isPlayerUnit(const ConstRefGenericCP& cpi) const;
#endif	// EDITOR

	RefGenericCP createCommand();
	void deleteCommand(const RefGenericCP& cpi);
		// Simply marks cp as dead
		// Used to Return true if unit really deleted (i.e. no outstanding references)

	// Leader attach/detatch/transfer functions
	RefGLeader createLeader(Side side, Nationality n, RankEnum defRank, Boolean noModify);
	void deleteLeader(const RefGLeader& iLeader);
	void attachLeader(const RefGenericCP& cpi, const RefGLeader& gi);
	void detachLeader(const RefGLeader& leader);
	void detachLeader(const RefGenericCP& cpi);
	void killAndReplaceLeader(const RefGLeader& iLeader);
		// Delete Leader.  If he was attached to a unit, then automatically
		// replace him using autoPromote.
	void autoPromote(const RefGenericCP& cpi);
	LeaderList& leaderList() { return d_leaders; }
   const LeaderList& leaderList() const { return d_leaders; }

	/*
	 * Shouldn't really need these next 2 functions
	 * They are only in so that batarmy can bodge up old saved games
	 */

	StrengthPointList* spList() { return &d_sps; }
	GenericCPList* cpList() { return &d_commands; }

	ISP getSPEntry(const RefGenericCP& cp) { return cp->getSPEntry(); }
	ConstISP getSPEntry(const ConstRefGenericCP& cp) const { return cp->getSPEntry(); }

	ISP createStrengthPoint();
	void deleteStrengthPoint(const ISP& isp);
	void deleteSPchain(const RefGenericCP& cpi);

	const char* spName(const ConstISP& isp) const;

	// const StrengthPoint* getStrengthPoint(const ISP& isp) const { return &d_sps[isp]; }
	// StrengthPoint* getStrengthPoint(const ISP& isp) { return &d_sps[isp]; }

	void attachStrengthPoint(const RefGenericCP& cpi, const ISP& spi);
	Boolean detachStrengthPoint(const RefGenericCP& cpi, const ISP& spi);

	SPCount getUnitSPCount(const ConstRefGenericCP& cpi, Boolean all) const;

	static ConstRefGenericCP getTopParent(const ConstRefGenericCP& cpi);
	static RefGenericCP getTopParent(const RefGenericCP& cpi);

	// Automatic with references

	Boolean canLeaderTakeControl(const ConstRefGLeader& iLeader, const ConstRefGenericCP& cpi) const;
	static Boolean canNationControlNation(Nationality nControl, Nationality nSubordinate);
	Boolean canNationControlThisUnit(const ConstRefGenericCP& cpi, Nationality n) const;

	void makeUnitName(const RefGenericCP& cpi, const Rank& r);
		// Make up unit name using nation->rankCount
		// Note that the unit's side must have been set first!

	// Boolean findSupplyUnit(const RefGenericCP& cpi, RefGenericCP& supplyCP, ISP& supplySP) const;
	// Boolean findEngineer(RefGenericCP cpi, RefGenericCP& engCP, ISP& engSP);
	Boolean findSpecialType(const RefGenericCP& cpi, RefGenericCP& typeCP, ISP& typeSP, SpecialUnitType::value type);
		// combines findSupplyUnit and findEngineer

	SPCount getNType(const ConstRefGenericCP& cpi, BasicUnitType::value type, Boolean except) const;

	Attribute getBaseMorale(const ConstRefGenericCP& cpi) const;

	BombValue getBombardmentValue(const ConstRefGenericCP& cpi) const;

	SPCount getUnitSPValue(const ConstRefGenericCP& cpi) const;
	SPCount getUnitSPValue(const ConstRefGenericCP& cpi, BasicUnitType::value type) const;

	SPCount getSPListCount(const ConstRefGenericCP& cpi) const;

	UWORD getCommandSubordination(const ConstRefGenericCP& cpi) const;
		// returns combined subordination rating of Commands direct subordinates

	UWORD getCommandSubordination(const ConstRefGLeader& leader) const;


	void setUnitSpecialistFlags(const RefGenericCP& cpi);

	/*
	 * File Interface
	 */

	Boolean readData(FileReader& f);
	Boolean writeData(FileWriter& f) const;


	OBSideInfo* getNation(Side n) const
	{
		// ASSERT(n >= 0);
		ASSERT(n < d_nSides);
		return &d_nations[n];
	}

	/*
	 * Garbage collection
	 */

	void cleanup();
		// Garbage collection

	void createEmptyOB(int nSides);
		// Create empty OB ready for number of sides

	void reset();
		// remove presidents, etc.

	void deleteAll();
		// delete all units, leaders and sps

 private:
		void setSPEntry(const RefGenericCP& cp, const ISP& spi);
		void setSPNext(const ISP& sp, const ISP& spi);

		void noteOBChanged() { d_obChanged++; }
	public:
		void setLeaderNation(const RefGLeader& l, Nationality n);
		Nationality getLeaderNation(const ConstRefGLeader& l) const;

};



#endif /* OB_HPP */

