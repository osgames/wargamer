/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef WG_MAIN_HPP
#define WG_MAIN_HPP

#include "gwind.h"

class RealMainWindow;

class GWIND_DLL MainWindow {
	 RealMainWindow* d_mw;
	 HWND d_hMain;
  public:
	 MainWindow();
	 ~MainWindow();

	 HWND getHWND() const { return d_hMain; }
};


#endif
