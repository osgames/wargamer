/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AI_PANEL_HPP
#define AI_PANEL_HPP

#ifndef __cplusplus
#error AI_ctrl.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Control Panel for AI
 *
 * used for debugging... allows AI to be paused, rate changed, etc
 *
 *----------------------------------------------------------------------
 */

#include "gwind.h"
#include "gamedefs.hpp"
struct AI_TimerTypes
{
   //---- This should be UWORD... 
   //---- but I am leaving it as int until Paul has finished with aib_side
   // typedef UWORD TimerRate;
   typedef int TimerRate;
};

class AI_ControlInterface : public AI_TimerTypes
{
   public:

      virtual ~AI_ControlInterface() = 0;
      virtual void pause(bool mode) = 0;
      virtual void setRate(TimerRate ms) = 0;
      virtual TimerRate getRate() const = 0;
      virtual TimerRate getMinRate() const = 0;
      virtual TimerRate getMaxRate() const = 0;

      virtual String getTitle() const = 0;
};

inline AI_ControlInterface::~AI_ControlInterface() { }


class AI_ControlPanelImp;

class AI_ControlPanel
{
   public:
      GWIND_DLL AI_ControlPanel();
      GWIND_DLL ~AI_ControlPanel();

      GWIND_DLL void init(AI_ControlInterface* ai);
      GWIND_DLL void clear();

   private:
      AI_ControlPanelImp* d_imp;
};


#endif /* AI_CTRL_HPP */


