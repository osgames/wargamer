/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "titlebar.hpp"

// #include "wind.hpp"
#include "fonts.hpp"
// #include "generic.hpp"
// #include "app.hpp"
#include "scenario.hpp"
#include "bmp.hpp"
#include "DIB.hpp"
// #include "DIB_util.hpp"
#include "DIB_blt.hpp"
#include "palette.hpp"
#include <memory>
// #include "cbutton.hpp"




TitleBarClass::TitleBarClass(HWND hparent, ModeEnum mode, RECT * rect) {

   d_mode = mode;
   d_hparent = hparent;
   d_titleText = 0;

   bkDIB = NULL;
   screenDIB = NULL;

    bkDIB = loadBMPDIB("frontend\\LeatherBar.bmp");
    ASSERT(bkDIB != NULL);

    createWindow(
        WS_EX_LEFT,
        // GenericNoBackClass::className(),
        "TitleBar",
        WS_CHILD | WS_CLIPSIBLINGS,
        rect->left, rect->top, rect->right, rect->bottom,
        hparent,
        NULL);
        // APP::instance() );

   ASSERT(getHWND());

    SetWindowText(getHWND(),"TitleBar");

   drawScreenDIB();
}


TitleBarClass::~TitleBarClass(void) 
{
    selfDestruct();

   if(screenDIB) delete screenDIB;
   if(bkDIB) delete bkDIB;

}

DIB* TitleBarClass::loadBMPDIB(const char* name) {

   std::auto_ptr<char> fName(scenario->makeScenarioFileName(name));
   DIB* dib = BMP::newDIB(fName.get(), BMP::RBMP_Normal);
   return dib;
}

void TitleBarClass::setSize(RECT * rect) {

   MoveWindow(getHWND(), rect->left, rect->top, rect->right, rect->bottom, FALSE);

    drawScreenDIB();
//    displayBar();

}





void
TitleBarClass::displayBar(void) {

   if(d_mode == Normal) {
   
      ShowWindow(getHWND(), SW_SHOW);
      InvalidateRect(getHWND(),NULL,FALSE);
   }

   else {
      
      ShowWindow(getHWND(), SW_HIDE);
   }

}




void
TitleBarClass::drawScreenDIB(void) {

   if(bkDIB == NULL) return;

    RECT rect;
    GetClientRect(getHWND(), &rect);

    int width = rect.right - rect.left;
    int height = rect.bottom - rect.top;

    if(screenDIB == NULL) { screenDIB = new DrawDIBDC(width,height); }

    else if (screenDIB->getWidth() != width || screenDIB->getHeight() != height) {
        delete(screenDIB);
        screenDIB = new DrawDIBDC(width,height);
   }

   DIB_Utility::stretchDIB(screenDIB,0,0,width,height,bkDIB,0,0,bkDIB->getWidth(),bkDIB->getHeight());

   HDC hdc = screenDIB->getDC();

   // draw frame
   int xoffset = width / 32;
   int yoffset = height / 6;
   int xinset = width - (xoffset*2);
   int yinset = height - (yoffset*2);

   screenDIB->frame(
      xoffset,
      yoffset,
      xinset,
      yinset,
      screenDIB->getColour(RGB(148,115,33))
   );
   screenDIB->frame(
      xoffset+1,
      yoffset+1,
      xinset-2,
      yinset-2,
      screenDIB->getColour(RGB(148,115,33))
   );

   if(d_titleText) {

      // we need a consistent way for working font sizes
      const int minFontHeight = 20;
      int fontHeight = 30;
      fontHeight = (fontHeight * (height - (yoffset*2))) / 1024;
      int h = maximum(fontHeight, minFontHeight) + 5;

      LogFont logFont;
      logFont.height(h);
      logFont.weight(FW_MEDIUM);
      logFont.face(scenario->fontName(Font_Bold));

      Fonts font;
      font.set(hdc, logFont);

      COLORREF color = RGB(148,115,33);
      COLORREF oldColor = SetTextColor(hdc, color);

      RECT rect;
      rect.left = 0;
      rect.top = 0;
      rect.right = screenDIB->getWidth();
      rect.bottom = screenDIB->getHeight();

      SetBkMode(hdc, TRANSPARENT);

      DrawText(hdc, d_titleText, lstrlen(d_titleText), &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

      SetTextColor(hdc, oldColor);
   }

}









LRESULT
TitleBarClass::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {

    switch(msg) {

//        HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
        HANDLE_MSG(hWnd, WM_SIZE, onSize);
        HANDLE_MSG(hWnd, WM_PAINT, onPaint);

        default:
            return defProc(hWnd, msg, wParam, lParam);
    }
}



void
TitleBarClass::onSize(HWND hwnd, UINT state, int cx, int cy) {

}


void
TitleBarClass::onPaint(HWND hwnd) {

    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(hwnd, &ps);
    HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
    RealizePalette(hdc);

    // blit screen DIB to window
   if(screenDIB) {
      BitBlt(hdc,0,0,screenDIB->getWidth(),screenDIB->getHeight(),screenDIB->getDC(),0,0,SRCCOPY);
   }

    SelectPalette(hdc, oldPal, FALSE);
    EndPaint(hwnd, &ps);

}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
