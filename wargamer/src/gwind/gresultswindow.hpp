/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GResultsWindow_HPP
#define GResultsWindow_HPP

#ifndef __cplusplus
#error GResultsWindow.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Generic Battle Results Window
 *
 *----------------------------------------------------------------------
 */

#include "gwind.h"
#include "b_result.hpp"
// #include "string.hpp"

class DeferWindowPosition;

/*
 * Campaign and Battle use this window, passing in a results
 * class that can be inherited to provide specific information
 *
 * Client should use create() to obtain a pointer to the window.
 */


class ResultWindowUser : public BattleResultTypes
{
    public:
        virtual const BattleResults* results() const = 0;
        virtual HWND hwnd() const = 0;
        virtual void onButton(BattleContinueMode mode) = 0;
        virtual GWIND_DLL bool willShowEnemyResults(Side s) const;
};


class CGenericResultsWindow
{
    public:
        GWIND_DLL static CGenericResultsWindow* create(ResultWindowUser* owner);

        virtual ~CGenericResultsWindow() = 0;

        virtual void show() = 0;
        virtual void hide() = 0;
        virtual void setPosition(LONG x, LONG y, LONG w, LONG h, DeferWindowPosition* def = 0) = 0;
};

inline CGenericResultsWindow::~CGenericResultsWindow() { }

#endif /* GResultsWindow_HPP */

