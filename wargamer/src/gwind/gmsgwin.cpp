/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Generic Player Message Window
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "gmsgwin.hpp"
#include "app.hpp"
#include "bmp.hpp"
#include "dib.hpp"
#include "colours.hpp"
#include "scenario.hpp"
#include "scn_res.h"
#include "wmisc.hpp"
#include "registry.hpp"
#include "itemwind.hpp"
#include "scn_img.hpp"
#include "resdef.h"
#include "help.h"
#include "winctrl.hpp"
#include "scrnbase.hpp"

namespace {		// Private namespace

/*
 * Some miscellaneous constants
 */

const int FROMX 	 = 40;
const int FROMY 	 = 29;
const int DATEX 	 = 170;
const int DATEY 	 = 128;
const int MESSAGEX = 12;
const int MESSAGEY = FROMY+32;   // 52;

static const PixelRect s_messageRect(MESSAGEX, MESSAGEY, 242, DATEY);
static const PixelRect s_fromRect(FROMX, FROMY, 242, MESSAGEY);
static const PixelRect s_dateRect(DATEX, DATEY, 242, DATEY+16);

static WSAV_FLAGS s_saveFlags = WSAV_ENABLED | WSAV_POSITION;

// const int FROMX 	 = 69;
// const int FROMY 	 = 35;
// const int DATEX 	 = 170;
// const int DATEY 	 = 110;
// const int MESSAGEX = 18;
// const int MESSAGEY = 75;
//
// static const PixelRect s_messageRect(MESSAGEX, MESSAGEY, 240, 110);
// static const PixelRect s_fromRect(FROMX, FROMY, 240, FROMY+32);
// static const PixelRect s_dateRect(DATEX, DATEY, 240, DATEY+16);
//
};		// End of private namespace

/*---------------------------------------------------------
 * MessageList Window class
 */

class MessageListWindowData
{
	public:
		MessageListWindowData() { }
		~MessageListWindowData() { }
	private:
};


class MessageListWindow : public ItemWindow {
	public:
  		MessageListWindow(const ItemWindowData& data, MListWindowUser* user, const SIZE& s) :
	 		ItemWindow(data, s),
	 		d_user(user)
  		{
  		}

  		~MessageListWindow() { selfDestruct(); }

  		void init(const void* data);
  		void drawListItem(const DRAWITEMSTRUCT* lpDrawItem);
  		void drawCombo(DrawDIBDC* dib, const void* data) {}
	private:

		MListWindowUser* d_user;


};



void MessageListWindow::init(const void* dataPtr)
{
  	/*
	 * Some useful values
	 */

	// const LONG offsetX = (3*dbX())/4;
	// const LONG offsetY = (3*dbY())/8;
	const LONG offsetX = ScreenBase::x(3);      // *dbX())/4;
	const LONG offsetY = ScreenBase::y(3);      // *dbY())/8;

	/*
	 * In case our dib hasn't been allocated
	 */

	if(!itemDib())
		allocateItemDib(10, 10);

	 /*
	  * Fill List box
	  */

  ListBox lb(listBox());
  lb.reset();

  int itemWidth = d_user->fillListBox(&lb, itemDib()->getDC());


  int nItems = lb.getNItems();

	d_user->setListIndex(lb);

	 /*
	  * Set size/position of list box
	  */

  	int itemsShowing = minimum<int>(data().d_maxItemsShowing, nItems);
  	const LONG itemHeight = data().d_itemCY*itemsShowing;
  	SetWindowPos(lb.getHWND(), HWND_TOP, 0, 0, itemWidth, itemHeight, SWP_NOMOVE);

  /*
	* if we're already showing, and we need a scroll bar, show it
	*/

  if(showing() && nItems > data().d_maxItemsShowing)
	 showScroll();
}

void MessageListWindow::drawListItem(const DRAWITEMSTRUCT* lpDrawItem)
{
  const int itemCX = lpDrawItem->rcItem.right-lpDrawItem->rcItem.left;
  const int itemCY = lpDrawItem->rcItem.bottom-lpDrawItem->rcItem.top;

  /*
	* Allocate dib if needed
	*/

  allocateItemDib(itemCX, itemCY);

  ASSERT(itemDib());
  ASSERT(fillDib());

  itemDib()->rect(0, 0, itemCX, itemCY, fillDib());

  if( !(lpDrawItem->itemState & ODS_SELECTED) )
	 itemDib()->darkenRectangle(0, 0, itemCX, itemCY, 20);


  /*
	* Find item in message list
	*/

  ListBox lb(lpDrawItem->hwndItem);
  int nItems = lb.getNItems();

  if(nItems > 0)
  {
	 ASSERT(lpDrawItem->itemID < nItems);

	 d_user->drawListItem(itemDib(), &lpDrawItem->rcItem, lb.getValue(lpDrawItem->itemID));
  }

  BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, itemCX, itemCY,
			itemDib()->getDC(), 0, 0, SRCCOPY);
}

// id arrays for tooltips and statwind


TipData GenericMessageWindow::s_tipData[] = {
	{ IDMW_PREV,			TTS_IDMW_PREV,      SWS_IDMW_PREV			},
	{ IDMW_NEXT,			TTS_IDMW_NEXT,      SWS_IDMW_NEXT			},
	{ IDMW_DELETE,			TTS_IDMW_DELETE,    SWS_IDMW_DELETE		},
	{ IDMW_ORDER,			TTS_IDMW_ORDER,     SWS_IDMW_ORDER		},
	{ IDMW_DELETEALL,		TTS_IDMW_DELETEALL, SWS_IDMW_DELETEALL	},
	{ IDMW_SETTINGS,		TTS_IDMW_SETTINGS,  SWS_IDMW_SETTINGS	},
	{ IDMW_DONTSHOW,		TTS_IDMW_NOSHOW,    SWS_IDMW_NOSHOW		},
	{ IDMW_LISTBUTTON,	TTS_IDMW_LIST,      SWS_IDMW_LIST			},
	EndTipData
};

/*
 * Statics
 */

WNDPROC GenericMessageWindow::lpfnWndProc = 0;
const char GenericMessageWindow::s_className[] = "MessageWindow";
ATOM GenericMessageWindow::classAtom = NULL;

/*
 * Sub class procedure for STATIC areas of message window
 */


LRESULT CALLBACK GenericMessageWindow::msgWinSubClassProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  ASSERT(lpfnWndProc != 0);

  GenericMessageWindow* mw = (GenericMessageWindow*)GetWindowLong(hwnd, GWL_USERDATA);
  ASSERT(mw != 0);

  int id = GetWindowLong(hwnd, GWL_ID);

  switch(msg)
  {
	 case WM_SETCURSOR:
		return mw->setCursor();

	 case WM_MOUSEMOVE:
		if(!mw->overIcon() || mw->overWhichIcon() != id)
		{
		  int oldID = mw->overWhichIcon();

		  mw->setOverIcon(True);
		  mw->overWhichIcon(id);
		  InvalidateRect(hwnd, NULL, FALSE);

		  if(oldID != id && oldID < GenericMessageWindow::IDMW_HOWMANY)
		  {
			 HWND h = GetDlgItem(mw->getHWND(), oldID);
			 ASSERT(h != 0);
			 InvalidateRect(h, NULL, FALSE);
		  }

		}
		break;
  }

  return CallWindowProc(lpfnWndProc, hwnd, msg, wParam, lParam);
}



ATOM GenericMessageWindow::registerClass()
{
	if(!classAtom)
	{
		WNDCLASS wc;

		wc.style = CS_OWNDC | CS_DBLCLKS;
		wc.lpfnWndProc = baseWindProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = sizeof(GenericMessageWindow*);
		wc.hInstance = APP::instance();
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = NULL;			// (HBRUSH) (1 + COLOR_3DFACE);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_className;
		classAtom = RegisterClass(&wc);
	}

	ASSERT(classAtom != 0);

	return classAtom;
}

const int captionHeight = 22;

GenericMessageWindow::GenericMessageWindow() :
	d_listWindow(0),
	d_msgIndex(0),
	d_messagesRead(0),
	// d_showing(False),
	CustomBorderWindow(scenario->getBorderColors()),
	d_customDial(scenario->getBorderColors()),
	d_dib(0),
	d_staticDib(BMP::newDrawDIB(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_MSGWINSTATIC), BMP::RBMP_Normal)),
	d_selectDib(BMP::newDrawDIB(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_MSGWINSELECT), BMP::RBMP_Normal)),
	d_hashedDib(BMP::newDrawDIB(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_MSGWINHASHED), BMP::RBMP_Normal)),
	d_iconPos((IconPos*)loadResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(PR_MSGWINICONS), RT_IMAGEPOS)),
	d_overWhichIcon(IDMW_HOWMANY),
	d_overIcon(False),
	d_changeCursor(False),
	d_cursor(scenario->getGloveCursor())
{
}

void GenericMessageWindow::init(HWND parent)
{
	ASSERT(d_staticDib != 0);
	ASSERT(d_selectDib != 0);
	ASSERT(d_hashedDib != 0);

	d_dib = new DrawDIBDC(d_staticDib->getWidth(), d_staticDib->getHeight());
	ASSERT(d_dib != 0);

	const int fontSize = 13;
	HFONT hFont = FontManager::getFont(scenario->fontName(Font_Normal), fontSize);
	ASSERT(hFont);

	d_dib->setFont(hFont);
	d_dib->setBkMode(TRANSPARENT);
	d_dib->blitFrom(d_staticDib);

	ASSERT(d_iconPos != 0);

	/*
	 * Some useful constants
	 */

//	const int borderWidth = GetSystemMetrics(SM_CXBORDER);
   const int borderWidth = CustomBorderWindow::ThickBorder;
	const int captionCY = GetSystemMetrics(SM_CYCAPTION);

   // -1 is a bodge to get around the client area being drawn in the wrong place

	const int cx = (d_dib->getWidth()+(2*borderWidth)) - 1;
	const int cy = (d_dib->getHeight()+(2*borderWidth)+captionCY) - 1;
	// const int cx = (d_dib->getWidth()+(2*borderWidth));
	// const int cy = (d_dib->getHeight()+(2*borderWidth)+captionCY);

	/*
	 * Get registry data
	 */

 	RECT r;
 	SetRect(&r, 200, 200, cx, cy);
//
// 	POINT p;
// 	if(getRegistry("position", &p, sizeof(POINT), registryName()))
// 	  SetRect(&r, p.x, p.y, cx, cy);
//
// 	if(getRegistry("screen", &p, sizeof(POINT), registryName()))
// 	  checkResolution(r, p);

	// if(!getRegistry("showstate", &d_showing, sizeof(Boolean), registryName()))
	//   d_showing = True;

	if(classAtom == NULL)
		registerClass();

	HWND hWnd = createWindow(
		WS_EX_LEFT,
		// className,
			NULL,
			WS_POPUP |
			WS_CAPTION |
			WS_CLIPSIBLINGS,
		r.left,			      		/* init. x pos */
		r.top,			      		/* init. y pos */
		r.right,			      		/* init. x size */
		r.bottom,	 	      		/* init. y size */
		parent,				     	/* parent window */
		NULL
		// wndInstance(parent)
	);

	ASSERT(hWnd != NULL);

    getState(registryName(), s_saveFlags, true);

	// if(d_showing)
	// 	show();
}

GenericMessageWindow::~GenericMessageWindow()
{
    delete d_listWindow;
    d_listWindow = 0;

    selfDestruct();

    if(d_dib)
	    delete d_dib;

    if(d_staticDib)
	    delete d_staticDib;

    if(d_selectDib)
	    delete d_selectDib;

    if(d_hashedDib)
	    delete d_hashedDib;
}

/*
 * Special Private Window Message(s)
 */

enum {
	WM_MSGWIN_ADDMESSAGE = WM_USER,
};


#define HANDLE_WM_MSGWIN_ADDMESSAGE(hwnd, wParam, lParam, fn) \
	((fn)(lParam), 0)

inline void FORWARD_WM_MSGWIN_ADDMESSAGE(HWND hwnd, LPARAM lparam, DLGPROC fn)
{
	(fn)(hwnd, WM_MSGWIN_ADDMESSAGE, 0, lparam);
}

void GenericMessageWindow::postMessage(LPARAM lparam)
{
	FORWARD_WM_MSGWIN_ADDMESSAGE(getHWND(), lparam, PostMessage);
}

LRESULT GenericMessageWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGE_WINDOW
	msgLog.printf("GenericMessageWindow::procMessage(%s)",
		getWMdescription(hWnd, msg, wParam, lParam));
#endif

	LRESULT l;
	if(handlePalette(hWnd, msg, wParam, lParam, l))
		return l;

	switch(msg)
	{
		HANDLE_MSG(hWnd, WM_CREATE,	onCreate);
		HANDLE_MSG(hWnd, WM_DESTROY,	onDestroy);
		HANDLE_MSG(hWnd, WM_PAINT, onPaint);
		HANDLE_MSG(hWnd, WM_CLOSE, onClose);
		HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
		HANDLE_MSG(hWnd, WM_CONTEXTMENU, onContextMenu);
		HANDLE_MSG(hWnd, WM_MEASUREITEM, onMeasureItem);
		HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, onMouseMove);
		HANDLE_MSG(hWnd, WM_MSGWIN_ADDMESSAGE, onAddMessage);
	default:
		return defProc(hWnd, msg, wParam, lParam);
	}
}

const IconPos* GenericMessageWindow::getIconPosition(int id)
{
  ASSERT(id <= IDMW_LISTBUTTON);

  return &d_iconPos[id];
}

HWND GenericMessageWindow::createControl(HWND hParent, int id, int x, int y, int w, int h)
{
  HWND hwnd = CreateWindow(
		  "STATIC", NULL, WS_VISIBLE | WS_CHILD | SS_OWNERDRAW | SS_NOTIFY,
		  x, y, w, h,
		  hParent, (HMENU)id, APP::instance(), NULL);
  ASSERT(hwnd != NULL);

  lpfnWndProc = (WNDPROC) SetWindowLong(hwnd, GWL_WNDPROC, (DWORD) msgWinSubClassProc);
  SetWindowLong(hwnd, GWL_USERDATA, (DWORD)this);

  return hwnd;
}

BOOL GenericMessageWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
#ifdef DEBUG
  // msgLog.printf("GenericMessageWindow::onCreate");
#endif

  // caption font
  int capCY = GetSystemMetrics(SM_CYCAPTION);
  const int fontHeight = capCY - 2;
  ASSERT(fontHeight >= 3);		// check for silly size?

  LogFont lf;
  lf.face(scenario->fontName(Font_Bold));
  lf.height(fontHeight);
  lf.weight(FW_MEDIUM);
  lf.charset(Greenius_System::ANSI);		// otherwise we may get symbols!

  // other stuff
  d_customDial.setCaptionBkDib(scenario->getScenarioBkDIB(Scenario::BackPatterns::MenuBarBackground));
  d_customDial.init(hWnd);
  d_customDial.setStyle(Greenius_System::CustomDialog::Close);
  d_customDial.setCaptionFont(lf);

  for(int i = IDMW_FIRST; i < IDMW_LASTSTATIC; i++)
  {
	 const IconPos* pos = getIconPosition(i);
	 createControl(hWnd, i, pos->x, pos->y, pos->w, pos->h);
  }

  const LONG dbUnits = GetDialogBaseUnits();

  SIZE s;
  s.cx = lpCreateStruct->cx;
  s.cy = lpCreateStruct->cy;

  ItemWindowData ld;
  ld.d_hParent = hWnd;
  ld.d_id = IDMW_LISTBOX;
  // ld.d_dbX = LOWORD(dbUnits);
  // ld.d_dbY = HIWORD(dbUnits);
  ld.d_scrollCX = 10;
  ld.d_itemCY = ScreenBase::y(8);   // static_cast<UWORD>((8*ld.d_dbY)/8);
  ld.d_maxItemsShowing = 10;
  ld.d_flags = ItemWindowData::HasScroll | ItemWindowData::ShadowBackground | ItemWindowData::OwnerDraw;
  ld.d_windowFillDib = scenario->getSideBkDIB(SIDE_Neutral);
  ld.d_fillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::PaperBackground);
  ld.d_scrollBtns = ScenarioImageLibrary::get(ScenarioImageLibrary::VScrollButtons);
  ld.d_scrollThumbDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollThumbBackground);
  ld.d_scrollFillDib = scenario->getScenarioBkDIB(Scenario::BackPatterns::VScrollBackground);
  ld.d_bColors = scenario->getBorderColors();

  d_listWindow = new MessageListWindow(ld, this, s);
  ASSERT(d_listWindow);

  setCaption();

  g_toolTip.addTips(hWnd, s_tipData);

  enableButtons();

  return TRUE;
}



BOOL GenericMessageWindow::setCursor()
{
	SetCursor(d_cursor);
	return TRUE;
}

void GenericMessageWindow::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
	if(d_overIcon)
	{
		d_overIcon = False;

		ASSERT(d_overWhichIcon < IDMW_LISTBOX);

		HWND h = GetDlgItem(hwnd, d_overWhichIcon);
		ASSERT(h != 0);

		InvalidateRect(h, NULL, FALSE);
	}
}


void GenericMessageWindow::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
  static const DWORD ids[] = {
	  IDMW_PREV,      IDH_MW_PreviousMsg,
	  IDMW_NEXT,      IDH_MW_NextMsg,
	  IDMW_DELETE,    IDH_MW_DeleteMsg,
	  IDMW_DELETEALL, IDH_MW_DeleteAllMsg,
	  IDMW_ORDER,     IDH_MW_Order,
	  IDMW_SETTINGS,  IDH_MW_Settings,
	  IDMW_DONTSHOW,  IDH_MW_DontShow,
	  IDMW_LISTBUTTON,IDH_MW_ShowList,
	  0, 0
  };


//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
}



void GenericMessageWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
#ifdef DEBUG_MESSAGE_WINDOW
	msgLog.printf("GenericMessageWindow::onCommand(), ID %d", id);
#endif

	switch(id)
	{
	  case IDMW_NEXT:
		  if(codeNotify == STN_CLICKED)
			nextMessage();
		 break;

	  case IDMW_PREV:
		 if(codeNotify == STN_CLICKED)
		 {
#if defined(DEBUG)
			// logWindow->printf("Button pushed");
#endif
			prevMessage();
		 }
		 break;

	  case IDMW_DELETE:
		 if(codeNotify == STN_CLICKED)
			deleteMessage();
		 break;

	  case IDMW_ORDER:
		 if(codeNotify == STN_CLICKED)
			orderDialog();
		 break;

	  case IDMW_LISTBUTTON:
	  {
		 if(codeNotify == STN_CLICKED)
		 {
			onShowList();
		 }
		 break;
	  }

	  case IDMW_LISTBOX:
	  {
			if(d_listWindow)
			{
				setFromListBox(d_listWindow->listBox());
			}
			break;
	  }

#if 0
	  case IDMW_HIDE:
		 if(codeNotify == BN_CLICKED)
			onClose(getHWND());
		 break;
#endif

	  case IDMW_DONTSHOW:
		 if(codeNotify == STN_CLICKED)
			onDontShow();
		 break;

	  case IDMW_DELETEALL:
		 if(codeNotify == STN_CLICKED)
			onDeleteAll();
		 break;

	  case IDMW_SETTINGS:
		 if(codeNotify == STN_CLICKED)
		 	doSettings();
		 break;


#ifdef DEBUG_MESSAGEWINDOW
	  default:
		 debugLog("GenericMessageWindow::onCommand(), unknown ID %d\n", id);
#endif
	}
	d_overIcon = False;
}

void GenericMessageWindow::onClose(HWND hWnd)
{
  // show(false);
  enable(false);
  windowUpdated();
}


void GenericMessageWindow::onPaint(HWND hWnd)
{
  PAINTSTRUCT ps;

  HDC hdc = BeginPaint(hWnd, &ps);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  draw(hdc);

  SelectPalette(hdc, oldPal, FALSE);

  EndPaint(hWnd, &ps);
}


void GenericMessageWindow::onDestroy(HWND hWnd)
{
#ifdef DEBUG
  // msgLog.printf("GenericMessageWindow::onDestroy");
#endif

  g_toolTip.delTips(hWnd);

//   RECT r;
//   GetWindowRect(hWnd, &r);
//
//   POINT p;
//   p.x = r.left;
//   p.y = r.top;
//
//   setRegistry("position", &p, sizeof(POINT), registryName());

  // setRegistry("showstate", &d_showing, sizeof(Boolean), registryName());
  saveState(registryName(), s_saveFlags);

//   POINT pRes;
//   pRes.x = GetSystemMetrics(SM_CXSCREEN);
//   pRes.y = GetSystemMetrics(SM_CYSCREEN);
//   setRegistry("screen", &pRes, sizeof(POINT), registryName());

#if 0  // Done in destructor
  if(d_listWindow)
  {
	 DestroyWindow(d_listWindow->getHWND());
	 d_listWindow = 0;
  }
#endif

  // windowDestroyed(hWnd);
}

void GenericMessageWindow::onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem)
{
  lpMeasureItem->itemHeight = 13;
}


void GenericMessageWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  switch(lpDrawItem->CtlID)
  {
	 case IDMW_SETTINGS:
	 case IDMW_DONTSHOW:
	 case IDMW_LISTBUTTON:
	 case IDMW_ORDER:
	 case IDMW_DELETE:
	 case IDMW_PREV:
	 case IDMW_NEXT:
	 case IDMW_DELETEALL:
		drawButton(lpDrawItem);
		break;

  }
}


void GenericMessageWindow::drawButton(const DRAWITEMSTRUCT* lpDrawItem)
{
  ASSERT(d_hashedDib != 0);
  ASSERT(d_selectDib != 0);
  ASSERT(d_staticDib != 0);

  const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
  const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

  const IconPos* pos = getIconPosition(lpDrawItem->CtlID);

  DrawDIB* dib = 0;

  if(d_overIcon)
  {
	 ASSERT(d_overWhichIcon < IDMW_LISTBOX);
	 if(lpDrawItem->CtlID == d_overWhichIcon)
		dib = d_selectDib;
  }

  if(!dib)
	 dib = (lpDrawItem->itemState & ODS_DISABLED) ? d_hashedDib : d_staticDib;

  ASSERT(dib != 0);

  ASSERT(d_dib);
  d_dib->blit(0, 0, cx, cy, dib, pos->x, pos->y);

  HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
  RealizePalette(lpDrawItem->hDC);

  BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, cx, cy,
			d_dib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(lpDrawItem->hDC, oldPal, FALSE);
}

/*
 * ListBox or the show as list button has been pressed
 */

void GenericMessageWindow::onShowList()
{
  ASSERT(d_listWindow);

  if(d_listWindow)
  {
	 MessageListWindowData data;

	 d_listWindow->init(&data);

	 HWND hwndCtl = GetDlgItem(getHWND(), IDMW_LISTBUTTON);
	 ASSERT(hwndCtl);

	 RECT r;
	 GetWindowRect(hwndCtl, &r);

	 PixelPoint p(r.left, r.top);
	 d_listWindow->show(p);
  }
}

void GenericMessageWindow::setListIndex(ListBox& lb)
{
	int msgIndex = (d_msgIndex > 0) ? d_msgIndex - 1 : 0;
	ASSERT(msgIndex < lb.getNItems());
	lb.set(msgIndex);
}

void GenericMessageWindow::setCaption()
{
	char buf[200];

//	wsprintf(buf, "%d of %d", d_msgIndex, messageCount());
	wsprintf(buf, "%d / %d", d_msgIndex, messageCount());
	d_customDial.setCaption(buf);

	SendMessage(getHWND(), WM_NCPAINT, 0, 0);

}

void GenericMessageWindow::postChanged()
{
	enableButtons();
	updateStatus();

	setCaption();

	InvalidateRect(getHWND(), NULL, FALSE);
}

/*
 * Set up the Window buttons
 * This can be called from another thread.
 */

void GenericMessageWindow::enableButtons()
{
#ifdef DEBUG_MESSAGE_WINDOW
	msgLog.printf(">enableButtons()");
#endif

	int numMessages = messageCount();

	WindowControl prev(getHWND(), IDMW_PREV);
	prev.enable(d_msgIndex > 1);

	WindowControl next(getHWND(), IDMW_NEXT);
	next.enable(d_msgIndex < numMessages);

	WindowControl deleteM(getHWND(), IDMW_DELETE);
	deleteM.enable(numMessages > 0);

	WindowControl order(getHWND(), IDMW_ORDER);
	order.enable(numMessages > 0);

	WindowControl deleteAll(getHWND(), IDMW_DELETEALL);
	deleteAll.enable(numMessages > 0);

	WindowControl showList(getHWND(), IDMW_LISTBUTTON);
	showList.enable(numMessages > 0);

	WindowControl noShow(getHWND(), IDMW_DONTSHOW);
	noShow.enable(numMessages > 0);


#ifdef DEBUG_MESSAGE_WINDOW
	msgLog.printf("<enableButtons()");
#endif
}

/*
 * Draw Message
 *
 * Updated by Steven to avoid excessive Font Creation
 * and avoid duplicated code
 */

void GenericMessageWindow::draw(HDC hdc)
{
#ifdef DEBUG_MESSAGE_WINDOW
	msgLog.printf(">draw()");
#endif

	d_dib->blitFrom(d_staticDib);

	drawMessage(d_dib->getDC(), s_messageRect, s_fromRect, s_dateRect);

	BitBlt(hdc, 0, 0, d_dib->getWidth(), d_dib->getHeight(),
          d_dib->getDC(), 0, 0, SRCCOPY);


#ifdef DEBUG_MESSAGE_WINDOW
	msgLog.printf("<draw()");
#endif
}

#if 0
void GenericMessageWindow::toggle()
{
	d_showing = !d_showing;
   ShowWindow(getHWND(), (d_showing) ? SW_SHOW : SW_HIDE);
}

void GenericMessageWindow::show()
{
	if(d_showing)
	  ShowWindow(getHWND(), SW_SHOW);
}

void GenericMessageWindow::hide()
{
	ShowWindow(getHWND(), SW_HIDE);
	d_showing = False;

}

void GenericMessageWindow::destroy()
{
	DestroyWindow(getHWND());
}
#endif
