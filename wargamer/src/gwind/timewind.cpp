/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Time Control Window
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "timewind.hpp"
#include "scenario.hpp"
#include "app.hpp"
// #include "generic.hpp"
#include "button.hpp"
#include "wmisc.hpp"
#include "dib.hpp"
#include "ctbar.hpp"
#include "palwind.hpp"
#include "dib_util.hpp"
#include "scn_img.hpp"
#include "imglib.hpp"
#include "resdef.h"
// #include "resstr.hpp"
#include "tooltip.hpp"
#include "help.h"
#include "fonts.hpp"
#include "resstr.hpp"

/*
 * Tooltip Data
 */

static TipData tipData[] = {
   { CC_SPEED, TTS_CC_SPEED,  SWS_CC_SPEED },
   { CC_PAUSE, TTS_CC_PAUSE,  SWS_CC_PAUSE },
   { CC_DATE1, TTS_CC_DATE1,  SWS_CC_DATE1 },
   { CC_DATE2, TTS_CC_DATE2,  SWS_CC_DATE2 },
   { CC_DATE3, TTS_CC_DATE3,  SWS_CC_DATE3 },
   EndTipData
};

enum {
  CCI_DayOff,
  CCI_WeekOff,
  CCI_FortNightOff,
  CCI_DayOn,
  CCI_WeekOn,
  CCI_FortNightOn,
  CCI_FreezeOff,
  CCI_FreezeOn,
  CCI_HowMany
};


TimeControlWindow::TimeControlWindow() :
    d_parent(NULL),
    d_trackBar(0),
    d_images(0),
    d_dib(0),
    d_fillDib(0),
    d_jumping(NotJumping),
    d_lastDay(0)
{
}

void TimeControlWindow::init(HWND parent)
{
    d_parent = parent;
    d_fillDib = scenario->getSideBkDIB(SIDE_Neutral);

   initToolButton(APP::instance());

   HWND hWnd = createWindow(
      WS_EX_LEFT,
      // GenericNoBackClass::className(),
         NULL,
         WS_CHILD |
         WS_CLIPSIBLINGS,     /* Style */
      0,                      /* init. x pos */
      0,                      /* init. y pos */
      219,                    /* init. x size */
      48,                     /* init. y size */
      d_parent,               /* parent window */
      NULL
      // wndInstance(d_parent)
   );

   ASSERT(hWnd != NULL);
}

TimeControlWindow::~TimeControlWindow()
{
    selfDestruct();

    if(d_trackBar)
    {
      delete d_trackBar;
       d_trackBar = 0;
    }

    if(d_dib)
    {
      delete d_dib;
       d_dib = 0;
    }
}

int TimeControlWindow::getWidth() const
{
    RECT r;
    GetWindowRect(getHWND(), &r);
    return r.right - r.left;
}

void TimeControlWindow::setPosition(LONG x, LONG y, LONG w, LONG h)
{
    SetWindowPos(getHWND(), HWND_TOP,
       x, y, w, h,
       SWP_SHOWWINDOW);
}

void TimeControlWindow::update(bool force)
{
#ifdef DEBUG_UPDATE
   debugLog("TimeControlWindow::update()\n");
#endif

#if !defined(EDITOR)
   /*
    * Turn off button when time jumping has finished
    */

   bool jumpFinished = (d_jumping != NotJumping) && !isTimeJumping();

   if(force || jumpFinished)
   {
      if (d_jumping != NotJumping)
      {
         HWND button = GetDlgItem(getHWND(), d_jumping);
         ASSERT(button);
         if(button)
            InvalidateRect(button, NULL, FALSE);

         if(jumpFinished)
         {
            d_jumping = NotJumping;
            force = true;     // make sure date is redisplayed in correct colours
         }
      }
   }
#endif   // !EDITOR

   TimeValue day = getTime();   // d_time->getCTime().getDate();
   if(force || (day != d_lastDay))
   {
      d_lastDay = day;
      HWND hDate = GetDlgItem(getHWND(), CC_MONTH);
      ASSERT(hDate != NULL);
      if(hDate)
         InvalidateRect(hDate, NULL, FALSE);
   }
}

// void TimeControlWindow::show()
// {
//     ShowWindow(getHWND(), SW_SHOW);
// }
//
// void TimeControlWindow::hide()
// {
//     ShowWindow(getHWND(), SW_HIDE);
// }


/*
 * Default implementation of virtual functions
 */

bool TimeControlWindow::isTimeFrozen() const
{
    return false;
}

void TimeControlWindow::freezeTime(bool freeze)
{
}

TimeControlWindow::RateValue TimeControlWindow::getTimeRange() const
{
    return 0;
}

void TimeControlWindow::setRate(RateValue n)
{
}

TimeControlWindow::RateValue TimeControlWindow::getRate() const
{
    return 0;
}

void TimeControlWindow::setJump(JumpValue n)
{
}

bool TimeControlWindow::isTimeJumping()
{
    return false;
}

void TimeControlWindow::stopJump()
{
}

// virtual int TimeControlWindow::getValue() const
// {
//     return 0;
// }
//
// virtual int TimeControlWindow::getValue(RateValue rate) const
// {
//     return 0;
// }

TimeControlWindow::TimeValue TimeControlWindow::getTime() const
{
    return 0;
}

String TimeControlWindow::displayTime() const
{
    return "";
}

String TimeControlWindow::displayRate() const
{
    return "";
}







/*=======================================================
 * Windows message handler
 */

LRESULT TimeControlWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MESSAGES
   debugLog("TimeControlWindow::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   LRESULT l;
   if(PaletteWindow::handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,   onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,  onDestroy);
//    HANDLE_MSG(hWnd, WM_PAINT, onPaint);
#if !defined(EDITOR)
      HANDLE_MSG(hWnd, WM_COMMAND, onCommand);
      HANDLE_MSG(hWnd, WM_HSCROLL, onHScroll);
#endif      // !EDITOR
      HANDLE_MSG(hWnd, WM_CONTEXTMENU, onContextMenu);
      HANDLE_MSG(hWnd, WM_DRAWITEM, onDrawItem);
      HANDLE_MSG(hWnd, WM_ERASEBKGND, onEraseBk);

      default:
        return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL TimeControlWindow::onEraseBk(HWND hwnd, HDC hdc)
{
  ASSERT(d_fillDib);

  RECT r;
  GetClientRect(hwnd, &r);

  const int cx = r.right - r.left;
  const int cy = r.bottom - r.top;

  DIB_Utility::allocateDib(&d_dib, cx, cy);
  ASSERT(d_dib);

  d_dib->rect(0, 0, cx, cy, d_fillDib);

  CustomBkWindow cw(scenario->getBorderColors());
  cw.drawThinBorder(d_dib->getDC(), r);

  HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
  RealizePalette(hdc);

  BitBlt(hdc, 0, 0, cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);

  SelectPalette(hdc, oldPal, FALSE);

  return TRUE;
}

/*
 * ONCREATE Message
 */

//const int CClockIcons = CCI_HowMany;

BOOL TimeControlWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
   /*
    *  Now allocate and create Image libraries
    */

   // for order icons
   d_images = ScenarioImageLibrary::get(ScenarioImageLibrary::CClockIcons);
   ASSERT(d_images != 0);

   int trackX = 25;
   int trackY = 22;
   int trackWidth = 110;
   int trackHeight = 15;

   const int xBorder = 2;
   const int yBorder = 2;

   if(hasJumps())
   {
      trackWidth = 110;
   }
   else
   {
      trackWidth = lpCreateStruct->cx - trackX - xBorder;
   }

#if !defined(EDITOR)

   HWND hFreeze = addToolButton(hWnd, CC_PAUSE, 3, 22, 15, 15);
   {
      ToolButton bFreeze = hFreeze;
      bFreeze.setCheck(isTimeFrozen());
   }

   HWND hTrack = CreateWindow(TRACKBAR_CLASS, (LPSTR)NULL,
      TBS_AUTOTICKS | TBS_HORZ | TBS_AUTOTICKS | WS_CHILD | WS_VISIBLE | WS_TABSTOP,
      trackX, trackY, trackWidth, trackHeight,
      // 25, 22, 110, 15,
      hWnd, (HMENU)CC_SPEED, wndInstance(hWnd), NULL);
   ASSERT(hTrack != 0);
   SendMessage(hTrack, TBM_SETRANGE, TRUE, MAKELONG(0, getTimeRange() - 1));
   SendMessage(hTrack, TBM_SETPOS, TRUE, getRate());

   d_trackBar = new CustomTrackBar(hWnd, hTrack, d_fillDib, scenario->getBorderColors());
   ASSERT(d_trackBar != 0);
#endif   // !EDITOR


   int monthX = 80;  // 100;
   int monthY = 2;
   const int monthCX = lpCreateStruct->cx - monthX - 8;      // 100
   const int monthCY = 18;

   addOwnerDrawStatic(hWnd, CC_MONTH, monthX, monthY, monthCX, monthCY);

#if !defined(EDITOR)
   int rateX = 8;
   int rateY = 2;
   const int rateCX = 70;     // 80;
   const int rateCY = 18;

   addOwnerDrawStatic(hWnd, CC_RATE, rateX, rateY, rateCX, rateCY);

   if(hasJumps())
   {
      addOwnerDrawButton(hWnd, 0, CC_DATE1, 145, 22, 19, 19);
      addOwnerDrawButton(hWnd, 0, CC_DATE2, 169, 22, 19, 19);
      addOwnerDrawButton(hWnd, 0, CC_DATE3, 193, 22, 19, 19);
   }

   updateFreeze();
#endif   // !EDITOR

   g_toolTip.addTips(hWnd, tipData);

   return TRUE;

}

void TimeControlWindow::onDestroy(HWND hWnd)
{
#ifdef DEBUG
   debugLog("TimeControlWindow::onDestroy()\n");
#endif

   g_toolTip.delTips(hWnd);

}

static DWORD ids[] = {
  CC_RATE,   IDH_GameSpeed,
  CC_PAUSE,  IDH_Pause,
  CC_MONTH,  IDH_GameDate,
  CC_SPEED,  IDH_GameSpeedSlider,
  CC_DATE1,  IDH_AdvTimeDay,
  CC_DATE2,  IDH_AdvTimeWeek,
  CC_DATE3,  IDH_AdvTimeFortnight,
  0, 0
};

void TimeControlWindow::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
}



static UWORD idToCCImage(int id, bool focus)
{
  if(id == CC_DATE1)
    return focus ? CCI_DayOn : CCI_DayOff;
  else if(id == CC_DATE2)
    return focus ? CCI_WeekOn : CCI_WeekOff;
  else
    return focus ? CCI_FortNightOn : CCI_FortNightOff;
}

void TimeControlWindow::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT * lpDrawItem)
{
#ifdef DEBUG_MESSAGES
   debugLog("TimeControlWindow::onDrawItem(%d, 0x%04x %d)\n",
      (int) lpDrawItem->CtlID,
      (int) lpDrawItem->itemState,
      (int) lpDrawItem->itemAction);
#endif

   const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
   const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

   HPALETTE oldPal = SelectPalette(lpDrawItem->hDC, Palette::get(), FALSE);
   RealizePalette(lpDrawItem->hDC);

   switch(lpDrawItem->CtlID)
   {
#if !defined(EDITOR)
      case CC_DATE1:
      case CC_DATE2:
      case CC_DATE3:
      {
        DIB_Utility::allocateDib(&d_dib, cx, cy);
        ASSERT(d_dib);

        d_dib->rect(0, 0, cx, cy, d_fillDib);

        bool focus = (lpDrawItem->itemState & ODS_SELECTED) ? true : false;

        if(d_jumping == lpDrawItem->CtlID)
        {
            focus = true;
        }

        UWORD index = idToCCImage(lpDrawItem->CtlID, focus);
        d_images->blit(d_dib, index, 0, 0);

        BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top, cx, cy,
           d_dib->getDC(), 0, 0, SRCCOPY);

        break;
      }
      case CC_RATE:
        drawRate(lpDrawItem);
        break;

      case CC_PAUSE:
      {
        DIB_Utility::allocateDib(&d_dib, cx, cy);
        ASSERT(d_dib);

        d_dib->rect(0, 0, cx, cy, d_fillDib);

        UWORD index = (UWORD)(lpDrawItem->itemState & (ODS_SELECTED | ODS_CHECKED) ? CCI_FreezeOn : CCI_FreezeOff);

        d_images->blit(d_dib, index, 0, 0);

        BitBlt(lpDrawItem->hDC, lpDrawItem->rcItem.left, lpDrawItem->rcItem.top,
               cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);


        break;
      }
#endif   // !EDITOR
      case CC_MONTH:
        drawDate(lpDrawItem);
        break;

      default:
#ifdef DEBUG
        wTextOut(lpDrawItem->hDC, 0, 0,"User Drawn");
        debugMessage("TimeControlWindow::onDrawItem Unknown User Drawn ID (%d)\n", (int) lpDrawItem->CtlID);
#else
        break;
#endif
   }
}

void TimeControlWindow::drawDate(const DRAWITEMSTRUCT* lpDrawItem)
{
   HDC hdc = lpDrawItem->hDC;
   const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
   const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

    String buffer(displayTime());

   DIB_Utility::allocateDib(&d_dib, cx, cy);

   ASSERT(d_dib);
   ASSERT(d_fillDib);
   d_dib->rect(0, 0, cx, cy, d_fillDib);
   d_dib->setBkMode(TRANSPARENT);

// int fontHeight = 14;
   int fontHeight = cy - 4;

   LogFont lf;
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));

   Font font;
   font.set(lf);

   d_dib->setFont(font);

   COLORREF color;

   if(d_jumping == NotJumping)
      color = scenario->getColour("MenuText");
   else
      color = RGB(255,0,0);

   d_dib->setTextColor(color);

   // Right Align

   d_dib->setTextAlign(TA_RIGHT | TA_TOP);

   wTextOut(d_dib->getDC(), cx - 2, 2, buffer.c_str());

   BitBlt(hdc, 0, 0, cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);
}

#if !defined(EDITOR)

enum IDEnum {
   Frozen,
   SecPerDay,
   IDEnum_HowMany
};

#if 0
static const int s_stringIDs[IDEnum_HowMany] = {
  IDS_CCLOCK_FROZEN,
  IDS_CCLOCK_SECPERDAY
};
static ResourceStrings s_strings(s_stringIDs, IDEnum_HowMany);
#endif

void TimeControlWindow::drawRate(const DRAWITEMSTRUCT* lpDrawItem)
{
   HDC hdc = lpDrawItem->hDC;
   const int cx = lpDrawItem->rcItem.right - lpDrawItem->rcItem.left;
   const int cy = lpDrawItem->rcItem.bottom - lpDrawItem->rcItem.top;

   String buffer;
   if(isTimeFrozen())
      buffer = InGameText::get(IDS_CCLOCK_FROZEN);
   else
      buffer = displayRate();

//    String buffer(displayRate());

   DIB_Utility::allocateDib(&d_dib, cx, cy);

   ASSERT(d_dib);
   ASSERT(d_fillDib);
   d_dib->rect(0, 0, cx, cy, d_fillDib);
   d_dib->setBkMode(TRANSPARENT);

   int fontHeight = cy - 4;
// int fontHeight = 14;

   LogFont lf;
   lf.height(fontHeight);
   lf.weight(FW_MEDIUM);
   lf.face(scenario->fontName(Font_Bold));

   Font font;
   font.set(lf);

   d_dib->setFont(font);
   d_dib->setTextColor(scenario->getColour("MenuText"));

   d_dib->setTextAlign(TA_LEFT | TA_TOP);

   wTextOut(d_dib->getDC(), 2, 2, buffer.c_str());

   BitBlt(hdc, 0, 0, cx, cy, d_dib->getDC(), 0, 0, SRCCOPY);
}

void TimeControlWindow::updateFreeze()
{
    bool frozen = isTimeFrozen();

   ToolButton b(getHWND(), CC_PAUSE);
   b.setCheck(frozen);

   TrackBar tb(getHWND(), CC_SPEED);
   tb.enable(!frozen);
}
#endif   // !EDITOR

#if !defined(EDITOR)

void TimeControlWindow::onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
#ifdef DEBUG_MESSAGES
   debugLog("ClockWindow::onHScroll(%p %p %u %d)\n",
      hwnd, hwndCtl, code, pos);

   HWND hTrack = GetDlgItem(hwnd, CC_SPEED);
   ASSERT(hTrack != NULL);
   ASSERT(hTrack == hwndCtl);
#endif

   /*
    * Simplest method is to simply get current position on any message
    * Alternatively, if only want to change speed when dragging has finished
    * then check on TB_ENDTRACK
    */

   switch(code)
   {
   case TB_ENDTRACK:
   case TB_BOTTOM:
   case TB_LINEDOWN:
   case TB_LINEUP:
   case TB_PAGEDOWN:
   case TB_PAGEUP:
   case TB_TOP:
      pos = SendMessage(hwndCtl, TBM_GETPOS, 0, 0);
      break;
   case TB_THUMBPOSITION:
   case TB_THUMBTRACK:
      break;
   }

#ifdef DEBUG_MESSAGES
   debugLog("Setting speed to %d\n", pos);
#endif

   setRate(pos);
   HWND hRate = GetDlgItem(hwnd, CC_RATE);
   ASSERT(hRate != 0);
   InvalidateRect(hRate, NULL, TRUE);
}

// void TimeControlWindow::setJump(int days, int id)
void TimeControlWindow::setJump(JumpValue n, int id)
{
   // Invalidate previous jump button

   if (d_jumping != NotJumping)
   {
      HWND button = GetDlgItem(getHWND(), d_jumping);
      if(button)
         InvalidateRect(button, NULL, FALSE);
   }

   // If click again, then stop jumping

   if (d_jumping == id)
   {
      d_jumping = NotJumping;
      stopJump();
   }
   else
   {
      setJump(n); // GameDay(d_time->getCTime().getDate() + days));
      d_jumping = id;
   }

   update(true);
}

void TimeControlWindow::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
#ifdef DEBUG_MESSAGES
   debugLog("ClockWindow::onCommand(%p %d %p %u)\n",
      hwnd, id, hwndCtl, codeNotify);
#endif
   switch(id)
   {
   case CC_DATE1:
   case CC_DATE2:
   case CC_DATE3:
      if(codeNotify == BN_CLICKED)
      {
#ifdef DEBUG_MESSAGES
         debugLog("Advance by %d\n", int(id - CC_DATE1));
#endif
         setJump(id - CC_DATE1, id);
      }
      break;

   case CC_PAUSE:
      if(codeNotify == BN_CLICKED)
      {
#ifdef DEBUG_MESSAGES
         debugLog("Freeze button pressed\n");
#endif
         freezeTime(!isTimeFrozen());
         updateFreeze();
         HWND hRate = GetDlgItem(hwnd, CC_RATE);
         ASSERT(hRate != 0);
         InvalidateRect(hRate, NULL, FALSE);
      }
      break;

   default:
#ifdef DEBUG_MESSAGES
      debugLog("ClockWindow::onCommand, unknown id %d\n", id);
#endif
      break;
   }

}


#endif   // !EDITOR

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
