/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef MULTIPLAYERDIALOG_HPP
#define MULTIPLAYERDIALOG_HPP

#include "DirectPlay.hpp"
#include "gwind.h"

/*

  Filename : MultiPlayerDialog.hpp

  Description : Dialog class for setting up a multiplayer connection.

  Notes : There are two modes - Host & Join
        Hosting will initialize a connection, and start the game
        Joining will search for active games on the chosen connection, and join it

        Creating the dialog will initialize the global CDirectPlay * g_directPlay instance
        When the game exits, it should delete this variable if it is non-NULL

*/


struct HostGameParams {
   
   char * GameName;
   int MaxPlayers;
   char * Password;
};

struct JoinGameParams {
   
   char * Address;
};




class CMultiPlayerDialogUser {

public:

   GWIND_DLL CMultiPlayerDialogUser(void) { }
   GWIND_DLL virtual ~CMultiPlayerDialogUser(void) { }

   GWIND_DLL virtual void connectionHosting(void) = 0;
   GWIND_DLL virtual void connectionJoined(void) = 0;
   GWIND_DLL virtual void connectionCancelled(void) = 0;
};



class CMultiPlayerDialog {
   
public:
   
   enum DialogModeEnum {
      Host,
      Join
   };

   GWIND_DLL CMultiPlayerDialog(HWND parent);
   GWIND_DLL ~CMultiPlayerDialog(void);

   void registerUser(CMultiPlayerDialogUser * user) { d_user = user; }

   CMultiPlayerDialogUser * d_user;

   HostGameParams d_hostGameParams;
   JoinGameParams d_joinGameParams;
   DialogModeEnum d_mode;

   HWND d_hostDialogHwnd;
   HWND d_joinDialogHwnd;
   HWND d_mainDialogHwnd;

   int d_lastConnectionIndex;

   void destroyDialog(void);
   void reinitDirectPlay(void);
   void reinitCombos(void);
   bool validateDialogEntries(void);
   void refreshSessionsList(void);
   bool startSession(void);
   void getSessionCaps(DPlaySessionDesc * desc);

private:

   

   char session_text[1024];

   bool initDialog(HWND parent);
   bool hostSession(DPlayConnectionDesc * connection_desc);
   bool joinSession(DPlaySessionDesc * session_desc);
};


BOOL CALLBACK DPlayDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);


#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
