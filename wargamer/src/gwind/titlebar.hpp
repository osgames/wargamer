/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TITLEBAR_HPP
#define TITLEBAR_HPP


#include "wind.hpp"
#include "gwind.h"

class DIB;
class DrawDIBDC;


class GWIND_DLL TitleBarClass : public WindowBaseND {

public:

	enum ModeEnum {
		Normal,
		Hidden
	};

	TitleBarClass(HWND hparent, ModeEnum mode, RECT * rect);
	~TitleBarClass();

	DIB * loadBMPDIB(const char* name);

	LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	void onSize(HWND hwnd, UINT state, int cx, int cy);
	void onPaint(HWND hwnd);

	ModeEnum getMode(void) { return d_mode; }
	void setMode(ModeEnum mode) { d_mode = mode; drawScreenDIB(); }

	void setText(const char* text) { d_titleText = text; }

	void displayBar(void);
	void drawScreenDIB(void);

	void setSize(RECT * rect);


private:

	HWND d_hparent;
	ModeEnum d_mode;
	const char* d_titleText;

    DIB * bkDIB;
    DrawDIBDC * screenDIB;
};




#endif
