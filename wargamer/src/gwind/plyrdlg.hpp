/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PLYRDLG_HPP
#define PLYRDLG_HPP

#ifndef __cplusplus
#error plyrdlg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Player Controller Dialog
 *
 *----------------------------------------------------------------------
 */

#include "gwind.h"
#include "gamedefs.hpp"
#include "control.hpp"

class PlayerSettingsDial;

class PlayerSettingsContainer
{
    public:

        /*
         * Client access functions
         */

        PlayerSettingsContainer() : d_psDial(0) { }
        GWIND_DLL virtual ~PlayerSettingsContainer();
        GWIND_DLL void showPlayerSettings();

    private:    // these accessed as friend
        friend class PlayerSettingsDial;

        /*
         * functions called internally
         */

        void playerSettingsDestroyed();

        /*
         * Functions called from within PlayerDialog that derived class
         * must provide
         */

        virtual void setController(Side side, GamePlayerControl::Controller control) = 0;
        virtual HWND getHWND() const = 0;

    private:
        PlayerSettingsDial* d_psDial;
};

#endif /* PLYRDLG_HPP */

