/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GMSGWIN_HPP
#define GMSGWIN_HPP

#ifndef __cplusplus
#error gmsgwin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Generic Message Window, reused by campaign and battle campaign windows
 *
 *----------------------------------------------------------------------
 */

#include "gwind.h"
#include "palwind.hpp"
#include "cust_dlg.hpp"
#include "tooltip.hpp"
#include "winctrl.hpp"
#include "dlist.hpp"

class MessageListWindow;

/*
 * Various structures and specialist classes
 */

struct IconPos {
  UWORD x;
  UWORD y;
  UWORD w;
  UWORD h;
  UWORD hotX;
  UWORD hotY;
};

/*
 * Base class for Messages
 * This is a hard class, not virtual
 */

class MessageItem : public DLink
{
	public:
		MessageItem() : d_read(false) { }
		~MessageItem() { }

		void setRead() { d_read = true; }
		bool isRead() const { return d_read; }

	private:
		bool	d_read;

};
// 
// inline MessageItem::~MessageItem()
// {
// }

class MListWindowUser
{
	public:
		virtual ~MListWindowUser() = 0;

		virtual int fillListBox(ListBox* lb, HDC hdc) = 0;
		virtual void drawListItem(DrawDIBDC* dib, const RECT* r, LPARAM value) = 0;
		virtual void setListIndex(ListBox& lb) = 0;
};

inline MListWindowUser::~MListWindowUser()
{
}




class GWIND_DLL GenericMessageWindow :
	public WindowBaseND,
	private PaletteWindow,
	private CustomBorderWindow,
	// private SuspendableWindow,
	private MListWindowUser
{
	public:
		enum {
			IDMW_FIRST = 0,
			IDMW_PREV = IDMW_FIRST,
			IDMW_NEXT,
			IDMW_DELETE,
			IDMW_ORDER,
			IDMW_DELETEALL,
			IDMW_SETTINGS,
			IDMW_DONTSHOW,
			IDMW_LISTBUTTON,

			IDMW_LASTSTATIC,

			IDMW_LISTBOX = IDMW_LASTSTATIC,

			IDMW_HOWMANY
		};

		GenericMessageWindow();
		virtual ~GenericMessageWindow() = 0;		// make sure it is pure.

		void init(HWND parent);

		HWND getHWND() const { return WindowBaseND::getHWND(); }

		// void toggle();
		// void show();
		// void hide();
		// void destroy();
		// bool isShowing() { return d_showing;}
		// void suspend(bool visible) { SuspendableWindow::suspend(visible); }

		int getMessagesRead() { return d_messagesRead; }
		BOOL setCursor();

		int getNumMessages() const { return messageCount(); }
		void clear() { onDeleteAll(); }

	protected:
		int msgIndex() const { return d_msgIndex; }
		void incMsgIndex() { ++d_msgIndex; }
		void decMsgIndex() { --d_msgIndex; }
		void msgIndex(int n) { d_msgIndex = n; }

		int readCount() const { return d_messagesRead; }
		void incReadCount() { ++d_messagesRead; }
		void decReadCount() { --d_messagesRead; }

		void postChanged();
		void postMessage(LPARAM lparam);

	private:

		/*
		 * Private Functions
		 */

		static LRESULT CALLBACK msgWinSubClassProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

		void setOverIcon(Boolean f) { d_overIcon = True; }
		Boolean overIcon() const { return d_overIcon; }

		int overWhichIcon() const { return d_overWhichIcon; }
		void overWhichIcon(int id) { d_overWhichIcon = id; }

		static ATOM registerClass();
		LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
		void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
		void onPaint(HWND hWnd);
		void onDestroy(HWND hWnd);
		void onMove(HWND hWnd, int x, int y);
		void onClose(HWND hWnd);
		void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
		void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);
		void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
		void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
		void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
		UINT onNCHitTest(HWND hwnd, int x, int y);
		BOOL onEraseBackground(HWND hwnd, HDC hdc);
		void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
		void drawListBox(const DRAWITEMSTRUCT* lpDrawItem);
		void drawButton(const DRAWITEMSTRUCT* lpDrawItem);

		void draw(HDC hdc);

		void drawMoveWindowLines();
		const IconPos* getIconPosition(int id);
		HWND createControl(HWND hParent, int id, int x, int y, int w, int h);
		void setCaption();
		void onShowList();

		void enableButtons();

#ifdef DEBUG
	 	void checkPalette() { }
#endif
		void setListIndex(ListBox& lb);

		/*
		 * Private virtual functions
		 */

		virtual const char* registryName() const = 0;
		virtual void onAddMessage(LPARAM lparam) = 0;

		virtual bool nextMessage() = 0;
		virtual bool prevMessage() = 0;
		virtual void deleteMessage() = 0;
		virtual void orderDialog() = 0;
		virtual void drawMessage(HDC hdc, const PixelRect& rect, const PixelRect& fromRect, const PixelRect& dateRect) = 0;
		virtual void onDontShow() = 0;
		virtual void onDeleteAll() = 0;
		virtual void doSettings() = 0;
		virtual void windowUpdated() = 0;	// notify owner that window is closed
		// virtual void windowDestroyed(HWND hwnd) = 0;
		virtual int messageCount() const = 0;
		virtual void updateStatus() = 0;
		virtual void setFromListBox(const ListBox& lb) = 0;

        virtual const char* className() const { return s_className; }

		/*
		 * Private  Data
		 */

		static const char s_className[];
		static ATOM classAtom;
		static WNDPROC lpfnWndProc;
		static TipData s_tipData[];

		Greenius_System::CustomDialog d_customDial;

		MessageListWindow* d_listWindow;

	 	// MessageItem* d_currentMessage;
	 	UWORD d_msgIndex;						   // Current message number (1=first, 0=none)
	 	UWORD d_messagesRead;  	     		   // Current messages that have been read

		// Boolean d_showing;

		DrawDIBDC* d_dib;
		DrawDIB* d_staticDib;
		DrawDIB* d_selectDib;
		DrawDIB* d_hashedDib;

		const IconPos* d_iconPos;

		UWORD d_overWhichIcon;
		Boolean d_overIcon;
		Boolean d_changeCursor;

		HCURSOR d_cursor;

};

/*
 * Templated message list
 */

template<class Item>
class MsgWinList
{
	public:
		MsgWinList() { }
		~MsgWinList() { }

		int size() const { return d_items.entries(); }

		Item* next(const Item* item) { return d_items.next(item); }
		Item* prev(const Item* item) { return d_items.prev(item); }
		void push_back(Item* item) { d_items.append(item); }
		void erase(Item* item) { d_items.unlink(item); Item::destroy(item); }

		class const_iterator
		{
			public:
				const_iterator(const const_iterator& it) :
					d_list(it.d_list),
					d_item(it.d_item)
				{
				}

				const_iterator(const	DiList<Item>* list, const Item* item) :
					d_list(list),
					d_item(item)
				{
					ASSERT(list != 0);
				}

				const_iterator& operator =(const const_iterator& it)
				{
					d_list = it.d_list;
					d_item = it.d_item;
					return *this;
				}

				bool operator == (const const_iterator& it) const
				{
					return (d_list == it.d_list) && (d_item == it.d_item);
				}

				const_iterator& operator ++()
				{
					d_item = d_list->next(d_item);
					return *this;
				}

				const Item* operator ->() const
				{
					ASSERT(d_item != 0);
					return d_item;
				}

				operator const Item* () const { return d_item; }

			private:
				const DiList<Item>* d_list;
				const Item* d_item;
		};

		const_iterator begin() const
		{
			return const_iterator(&d_items, d_items.first());
		}

		const_iterator end() const
		{
			return const_iterator(&d_items, 0);
		}

		static itemToLParam(const Item* item)
		{
			return reinterpret_cast<LPARAM>(item);
		}

		static Item* lParamToItem(LPARAM l)
		{
			return reinterpret_cast<Item*>(l);
		}

	private:
		DiList<Item> d_items;
};

/*
 * Templated shared parts of Message Window
 */

template<class Item>
class TMessageWindow : public GenericMessageWindow
{
		typedef MsgWinList<Item> MessageList;
	public:

		TMessageWindow();
		virtual ~TMessageWindow() = 0;

	protected:
		int messageCount() const { return d_msgList.size(); }		

		void postMessage(Item* msg) { GenericMessageWindow::postMessage(d_msgList.itemToLParam(msg)); }

	private:
		void onDeleteAll();
		void deleteMessage();
		void setFromListBox(const ListBox& lb);
		bool nextMessage();
		bool prevMessage();
		void onAddMessage(LPARAM lparam);

		#ifdef DEBUG
			void assertMsgIndex() const
			{
				ASSERT(msgIndex() <= messageCount());
				if(d_currentMessage == 0)
				{
					ASSERT(msgIndex() == 0);
				}
				else
				{
					ASSERT(msgIndex() > 0);
					ASSERT(messageCount() != 0);
				}
			}
		#else
			void assertMsgIndex() const { }
		#endif
		
		virtual void playSound() const = 0;

	protected:
	 	MessageList d_msgList;
		Item* d_currentMessage;
		
		enum {
			WhoFrom,
			DateSent,
			Description,

			TextItems_HowMany,

			SpaceCX = 15
		};

		UWORD d_textCX[TextItems_HowMany];
};

template<class Item>
inline
TMessageWindow<Item>::TMessageWindow() :
	GenericMessageWindow(),
	d_currentMessage(0)
{
	for(int i = 0; i < TextItems_HowMany; i++)
		d_textCX[i] = 0;
}

template<class Item>
inline
TMessageWindow<Item>::~TMessageWindow()
{
}

template<class Item>
inline
void TMessageWindow<Item>::onDeleteAll()
{
	while(d_currentMessage)
		deleteMessage();
}


template<class Item>
inline
void TMessageWindow<Item>::deleteMessage()
{
	ASSERT(d_currentMessage);

	Item* next = d_msgList.next(d_currentMessage);
	Item* prev = d_msgList.prev(d_currentMessage);

	if(d_currentMessage->isRead())
	  decReadCount();

	ASSERT(readCount() <= messageCount());
	ASSERT(readCount() >= 0);

	d_msgList.erase(d_currentMessage);
	d_currentMessage = 0;

	if(next)
	{
	  d_currentMessage = next;
	}
	else
	{
	  if(prev)
		 d_currentMessage = prev;

	  decMsgIndex();
	}

	assertMsgIndex();

	postChanged();

}

template<class Item>
inline
void TMessageWindow<Item>::setFromListBox(const ListBox& lb)
{
	Item* msg = d_msgList.lParamToItem(lb.getValue());
	ASSERT(msg);

	if(msg)
	{
		d_currentMessage = msg;
		msgIndex(lb.getIndex()+1);
   	postChanged();
	}
}


/*
 * Advance to next message
 */

template<class Item>
inline
bool TMessageWindow<Item>::nextMessage()
{
	Item* next = d_msgList.next(d_currentMessage);
	if(next)
	{
		d_currentMessage = next;
		assertMsgIndex();
		incMsgIndex();
		assertMsgIndex();
		postChanged();
		return true;
	}
	return false;
}

template<class Item>
inline
bool TMessageWindow<Item>::prevMessage()
{
	Item* prev = d_msgList.prev(d_currentMessage);
	if(prev)
	{
		d_currentMessage = prev;
		assertMsgIndex();
		decMsgIndex();
		assertMsgIndex();
		postChanged();
		return true;
	}
	return false;
}

template<class Item>
inline
void TMessageWindow<Item>::onAddMessage(LPARAM lparam)
{
	// CampaignMessageItem* newMsg = reinterpret_cast<CampaignMessageItem*>(lparam);
	Item* newMsg = d_msgList.lParamToItem(lparam);

#ifdef DEBUG_MESSAGE_WINDOW
	msgLog.printf(">onAddMessage");
#endif

	if(msgIndex() == d_msgList.size())
	{
	  d_currentMessage = newMsg;
	  incMsgIndex();
	}

	d_msgList.push_back(newMsg);

	playSound();

	postChanged();

#ifdef DEBUG_MESSAGE_WINDOW
	msgLog.printf("<onAddMessage");
#endif
}


#endif /* GMSGWIN_HPP */

