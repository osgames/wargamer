/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Player Settings Dialogue
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "plyrdlg.hpp"
#include "dialog.hpp"
#include "resdef.h"
#include "scenario.hpp"
#include "control.hpp"
#include "help.h"
#include "palwind.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "app.hpp"
#include "palette.hpp"
#include "winctrl.hpp"
#include "imglib.hpp"
#include "scn_res.h"
#include "cbutton.hpp"
#include "resstr.hpp"

/*--------------------------------------------------------------------
 * Old way (actually... this is what the game is using)
 */


class PlayerSettingsDial : public ModelessDialog {
	 PlayerSettingsContainer* d_owner;

	 GamePlayerControl::Controller control[2];
	 HWND hwndParent;

	 enum Mode {
		HasButtons,
		NoButtons
	 } d_mode;


	 PlayerSettingsDial(const PlayerSettingsDial&);
	 PlayerSettingsDial& operator =(const PlayerSettingsDial&);
  public:
     // PlayerSettingsDial();
	 // PlayerSettingsDial(const char* resName, HWND parent);   // For use with Wizard propsheet
	 PlayerSettingsDial(HWND parent, PlayerSettingsContainer* owner);
	 ~PlayerSettingsDial() { }

  private:
	 BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	 void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	 BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	 void onDestroy(HWND hwnd);
	 void onClose(HWND hwnd);
	 void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
	 void onNotify(HWND hwnd, int id, NMHDR* lpNMHDR);
	 void onOK();
};



PlayerSettingsDial::PlayerSettingsDial(HWND parent, PlayerSettingsContainer* owner) :
	d_owner(owner),
	hwndParent(parent),
	d_mode(HasButtons)
{
  ASSERT(parent != NULL);

  HWND dhwnd = createDialog(playerSettingsDlgName, parent);
  ASSERT(dhwnd != NULL);
  SetWindowText(dhwnd, InGameText::get(IDS_PlayerSettings));
}


#if 0
PlayerSettingsDial::PlayerSettingsDial(const char* resName, HWND parent) : // for use with Front End
	d_owner(0),
	hwndParent(parent),
	d_mode(NoButtons)
{
  ASSERT(parent != NULL);
  ASSERT(resName != 0);

  HWND dhwnd = createDialog(resName, parent, False);
  ASSERT(dhwnd != NULL);
}

/*
 * Constructor used by front end
 */

PlayerSettingsDial::PlayerSettingsDial() : // for use with Front End
	d_owner(0),
	hwndParent(0),
	d_mode(NoButtons)
{
}
#endif

BOOL PlayerSettingsDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		case WM_INITDIALOG:
			HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
			break;

		case WM_DESTROY:
			HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
			break;
		case WM_COMMAND:
			HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
			break;
		case WM_CLOSE:
			HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
			break;
		case WM_CONTEXTMENU:
			HANDLE_WM_CONTEXTMENU(hWnd, wParam, lParam, onContextMenu);
			break;
		case WM_NOTIFY:
			HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
			break;

		default:
			return FALSE;
	}

	return TRUE;
}

static DWORD ids[] = {
	PSD_PLAYER1_PLAYER,	IDH_PSD_Player,
	PSD_PLAYER1_AI,   	IDH_PSD_AI,
	PSD_PLAYER1_REMOTE,  IDH_PSD_Remote,
	PSD_PLAYER2_PLAYER,	IDH_PSD_Player,
	PSD_PLAYER2_AI,   	IDH_PSD_AI,
	PSD_PLAYER2_REMOTE,  IDH_PSD_Remote,
	0,  0
};

void PlayerSettingsDial::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
}

BOOL PlayerSettingsDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
	static struct
	{
		int playerBox;
		int player;
		int ai;
		int remote;
	} boxID[2] =
	{
		{ PSD_PLAYER1_BOX, PSD_PLAYER1_PLAYER, PSD_PLAYER1_AI, PSD_PLAYER1_REMOTE },
		{ PSD_PLAYER2_BOX, PSD_PLAYER2_PLAYER, PSD_PLAYER2_AI, PSD_PLAYER2_REMOTE }
	};

	for(Side side = 0; side < 2; side++)
	{
		HWND hBox = GetDlgItem(hwnd, boxID[side].playerBox);
		ASSERT(hBox != 0);

		SetWindowText(hBox, scenario->getSideName(side));

        control[side] = GamePlayerControl::getControl(side);

		setButtonCheck(hwnd, boxID[side].player, control[side] == GamePlayerControl::Player);
		setButtonCheck(hwnd, boxID[side].ai, 	  control[side] == GamePlayerControl::AI);
		setButtonCheck(hwnd, boxID[side].remote, control[side] == GamePlayerControl::Remote);
	}

  return TRUE;
}

void PlayerSettingsDial::onDestroy(HWND hwnd)
{
    d_owner->playerSettingsDestroyed();
  SendMessage(hwndParent, WM_PARENTNOTIFY, (WPARAM)WM_DESTROY, (LPARAM)hwnd);
}

void PlayerSettingsDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
	 case PSD_PLAYER1_PLAYER:
		control[0] = GamePlayerControl::Player;
		break;

	 case PSD_PLAYER1_AI:
		control[0] = GamePlayerControl::AI;
		break;

	 case PSD_PLAYER1_REMOTE:
		control[0] = GamePlayerControl::Remote;
		break;

	 case PSD_PLAYER2_PLAYER:
		control[1] = GamePlayerControl::Player;
		break;

	 case PSD_PLAYER2_AI:
		control[1] = GamePlayerControl::AI;
		break;

	 case PSD_PLAYER2_REMOTE:
		control[1] = GamePlayerControl::Remote;
		break;

	 case PSD_CANCEL:
		PostMessage(hwnd, WM_CLOSE, 0, 0);
		break;

	 case PSD_OK:
		onOK();
		PostMessage(hwnd, WM_CLOSE, 0, 0);
		break;

  }
}

void PlayerSettingsDial::onClose(HWND hwnd)
{
  DestroyWindow(hwnd);
}

void PlayerSettingsDial::onOK()
{
    for(Side s = 0; s < 2; s++)
    {
        if(d_owner)
	        d_owner->setController(s, control[s]);
	    GamePlayerControl::setControl(s, control[s]);
    }
}


void PlayerSettingsDial::onNotify(HWND hwnd, int id, NMHDR* lpNMHDR)
{
  switch(lpNMHDR->code)
  {

	 case PSN_SETACTIVE:
	 {
		HWND hPropSheet = GetParent(hwnd);
		PostMessage(hPropSheet, PSM_SETWIZBUTTONS, 0, (LPARAM)(DWORD)PSWIZB_NEXT);

	  /*
		* Center Dialog
		*/

		RECT r;
		GetWindowRect(hPropSheet, &r);

		int cx = GetSystemMetrics(SM_CXSCREEN);
		int cy = GetSystemMetrics(SM_CYSCREEN);

		int x = (cx-(r.right-r.left))/2;
		int y = (cy-(r.bottom-r.top))/2;


		MoveWindow(hPropSheet, x, y, r.right, r.bottom, TRUE);

	 }

	 case PSN_WIZNEXT:
		onOK();
		break;

  }

}


PlayerSettingsContainer::~PlayerSettingsContainer()
{
    delete d_psDial;
}

void PlayerSettingsContainer::showPlayerSettings()
{
    if(!d_psDial)
    {
	    d_psDial = new PlayerSettingsDial(getHWND(), this);
	    ASSERT(d_psDial != 0);
    }
}

void PlayerSettingsContainer::playerSettingsDestroyed()
{
    d_psDial = 0;
}

