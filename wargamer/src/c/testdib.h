/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TESTDIB_H
#define TESTDIB_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Definitions for TestDIB Resources
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif


#define MAIN_MENU 1

#define MAIN_ICON 1

#define IDM_EXIT           100
#define IDM_PALETTEWINDOW  101


#ifdef __cplusplus
};
#endif

#endif /* TESTDIB_H */

