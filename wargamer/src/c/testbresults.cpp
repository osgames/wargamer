/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Test Battle Results Screen
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "app.hpp"
#include "myassert.hpp"
#include "wind.hpp"
#include "GResultsWindow.hpp"
#include "scenario.hpp"
#include "random.hpp"
#include "bmp.hpp"
#include "palwind.hpp"

#define HANDLE_WM_SIZING(hwnd, wParam, lParam, fn) \
    ((fn)((hwnd),(UINT)(wParam),(RECT*)(lParam)),TRUE)

class TestBattleResults : public BattleResults
{
    public:
        TestBattleResults(const BattleFinish& f) :
            BattleResults(f)
        {
            d_leader[0].setName(copyString("Napoleon"));
            d_leader[0].setSide(0);
            d_leader[0].portrait(copyString("Napoleon"));

            d_leader[1].setName(copyString("Alexander"));
            d_leader[1].setSide(1);
            d_leader[1].portrait(copyString("Czar Alexander"));

            d_cp[0].setName(copyString("French Army"));
            d_cp[0].side(0);

            d_cp[1].setName(copyString("Allied Army"));
            d_cp[1].side(1);
        }

        void makeRandom();

        virtual String battleName() const;
        virtual ConstRefGLeader leader(Side s) const;
        virtual ConstRefGenericCP cp(Side s) const;
    private:
        GLeader d_leader[2];
        GenericCP d_cp[2];

};

class MainWindow : 
    public WindowBaseND, 
    public PaletteWindow, 
    public ResultWindowUser
{
    public:
        MainWindow(BattleResults* results);
        ~MainWindow();
    private:
       LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
       BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
       void onDestroy(HWND hwnd);
       void onClose(HWND hwnd);
       void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
       void onPaint(HWND hwnd);
       BOOL onEraseBk(HWND hwnd, HDC hdc);
       void onSize(HWND hwnd, UINT state, int cx, int cy);
      void onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo);
       void onSizing(HWND hwnd, UINT side, RECT* rect);

       void checkMenu(int id, BOOL flag);
       void enableMenu(int id, BOOL flag);

       static ATOM registerClass();

        virtual const BattleResults* results() const;
        virtual HWND hwnd() const;
        virtual void onButton(BattleContinueMode mode);

        virtual const char* className() const { return s_className; }

    private:

        CGenericResultsWindow* d_window;
        ResultWindowUser* d_user;
        BattleResults* d_results;

       static ATOM s_classAtom;
       static const char s_className[];
       static const char s_title[];
};



class TestResultApp :
    public APP
{
    public:
        TestResultApp();
        ~TestResultApp();

        BOOL initApplication();
        BOOL initInstance();
        void endApplication();
        BOOL doCmdLine(LPCSTR cmd);

        Boolean processMessage(MSG* msg);
                // Process Message.
                //   Return True if application dealt with it
                //   False to continue as usual
    private:
        TestBattleResults* d_results;
        MainWindow* d_mainWind;
};


/*========================================================================
 * Main Windows Function
 */

int PASCAL WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
        int result = 0;
        try
        {
                TestResultApp application;
                result = application.execute(hInstance, hPrevInstance, lpszCmdLine, nCmdShow);
        }
#if defined(DEBUG)
        catch(AssertError e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Quit Program", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
#endif
        catch(GeneralError e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "General Error Exception", e.get(), MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
        catch(...)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Unknown", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }

        return result;
}




TestResultApp::TestResultApp() :
    d_mainWind(0)
{
}

TestResultApp::~TestResultApp()
{
    endApplication();
}

BOOL TestResultApp::initApplication()
{

    Scenario::create("nap1813.swg");
    BMP::getPalette(scenario->getPaletteFileName());
    return TRUE;
}

BOOL TestResultApp::initInstance()
{
    // Create a window with a custom button in

    BattleFinish f;
    f.defeated(0, BattleFinish::Destroyed);
    d_results = new TestBattleResults(f);
    d_results->makeRandom();

    d_mainWind = new MainWindow(d_results);

    return true;
}

void TestResultApp::endApplication()
{
    // Delete window
    delete d_mainWind;
    d_mainWind = 0;

    delete d_results;
    d_results = 0;

    if(scenario())
       Scenario::kill();

    ASSERT(!scenario);
}

BOOL TestResultApp::doCmdLine(LPCSTR cmd)
{
    return true;
}


Boolean TestResultApp::processMessage(MSG* msg)
{
    return false;
}




    // To test fill in with random values
void TestBattleResults::makeRandom()
{
    for(Side side = 0; side < scenario->getNumSides(); ++side)
    {
        BattleLosses* loss = losses(side);
        loss->reset();

        for(BasicUnitType::value t = static_cast<BasicUnitType::value>(0); t < BasicUnitType::HowMany; INCREMENT(t))
        {
            ManCount n = random(0, 100) * UnitTypeConst::menPerType(t);
            loss->addStartStrength(t, n);
            loss->addLosses(t, random(0,n));
        }
    }
}


virtual String TestBattleResults::battleName() const
{
    return "Battle of \'Test Results\'";
}

ConstRefGLeader TestBattleResults::leader(Side s) const
{
    return &d_leader[s];
}

ConstRefGenericCP TestBattleResults::cp(Side s) const
{
    return &d_cp[s];
}



MainWindow::MainWindow(BattleResults* results) :
    d_window(0),
    d_results(results)
{
    registerClass();
    createWindow(0,
        // s_className,
        s_title,
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
        NULL,
        NULL);
        // APP::instance());
    SetWindowText(getHWND(), s_title);

    d_window = CGenericResultsWindow::create(this);

    PixelRect r;
    GetClientRect(getHWND(), &r);
    d_window->setPosition(r.left(), r.top(), r.width(), r.height());
    d_window->show();
}

MainWindow::~MainWindow()
{
    delete d_window;
}

LRESULT MainWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT result;
    if(handlePalette(hWnd, msg, wParam, lParam, result))
       return result;

   switch(msg)
   {
        HANDLE_MSG(hWnd, WM_CREATE,         onCreate);
        HANDLE_MSG(hWnd, WM_DESTROY,         onDestroy);
        HANDLE_MSG(hWnd, WM_CLOSE,           onClose);
        HANDLE_MSG(hWnd, WM_COMMAND,         onCommand);
        HANDLE_MSG(hWnd, WM_PAINT,          onPaint);
        HANDLE_MSG(hWnd, WM_ERASEBKGND,     onEraseBk);
        HANDLE_MSG(hWnd, WM_SIZE,            onSize);
      HANDLE_MSG(hWnd, WM_GETMINMAXINFO,  onGetMinMaxInfo);
        HANDLE_MSG(hWnd, WM_SIZING,         onSizing);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL MainWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
    return TRUE;
}

void MainWindow::onDestroy(HWND hwnd)
{
}

void MainWindow::onClose(HWND hwnd)
{
    PostQuitMessage(0);
}

void MainWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
}

void MainWindow::onPaint(HWND hwnd)
{
    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(hwnd, &ps);

    // .. nothing

    EndPaint(hwnd, &ps);
}

BOOL MainWindow::onEraseBk(HWND hwnd, HDC hdc)
{
    return TRUE;
}

void MainWindow::onSize(HWND hwnd, UINT state, int cx, int cy)
{
    // resize inner window

    if(d_window)
        d_window->setPosition(0, 0, cx, cy);


}


void MainWindow::onSizing(HWND hwnd, UINT side, RECT* rect)
{
    // prevent from being too narrow
    // Width must be bigger than (height * 1024) / 768

    int w = rect->right - rect->left;
    int h = rect->bottom - rect->top;
    int minW = (h * 5) / 4;
    int maxH = (w * 4) / 5;

    if(w < minW)
    {
        switch(side)
        {
            case WMSZ_TOP:
            case WMSZ_BOTTOM:
            case WMSZ_TOPRIGHT:
            case WMSZ_BOTTOMRIGHT:
                rect->right = rect->left + minW;
                break;
            case WMSZ_TOPLEFT:
            case WMSZ_BOTTOMLEFT:
                rect->left = rect->right - minW;
                break;
            case WMSZ_RIGHT:
            case WMSZ_LEFT:
                rect->bottom = rect->top + maxH;
                break;
        }
    }




}

void MainWindow::onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo)
{
   const int minimumWidth = 640;
   const int minimumHeight = 480;

   lpMinMaxInfo->ptMinTrackSize.x = minimumWidth;
   lpMinMaxInfo->ptMinTrackSize.y = minimumHeight;
}


void MainWindow::checkMenu(int id, BOOL flag)
{
        HMENU menu = GetMenu(getHWND());
        ASSERT(menu != NULL);

        if(menu)
                CheckMenuItem(menu, id, MF_BYCOMMAND | (flag ? MF_CHECKED : MF_UNCHECKED));
}

void MainWindow::enableMenu(int id, BOOL flag)
{
        ASSERT((flag == TRUE) || (flag == FALSE));

        HMENU menu = GetMenu(getHWND());
        ASSERT(menu != NULL);

        if(menu)
                EnableMenuItem(menu, id, MF_BYCOMMAND | (flag ? MF_ENABLED : MF_GRAYED));
}

static ATOM MainWindow::registerClass()
{
   if(!s_classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(MainWindow*);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;    // LoadIcon(APP::instance(), MAKEINTRESOURCE(MAIN_ICON));
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = NULL;   // (HBRUSH) (COLOR_BACKGROUND + 1);
      wc.lpszMenuName = NULL;     // MAKEINTRESOURCE(MAIN_MENU);
      wc.lpszClassName = s_className;  // szMainClass;   // szAppName;
      s_classAtom = RegisterClass(&wc);
   }

   ASSERT(s_classAtom != 0);

   return s_classAtom;
}

static ATOM MainWindow::s_classAtom = NULL;
static const char MainWindow::s_className[] = "TestBattleResult";
static const char MainWindow::s_title[] = "Test Battle Results";


const BattleResults* MainWindow::results() const
{
    return d_results;
}

virtual HWND MainWindow::hwnd() const
{
    return getHWND();
}

virtual void MainWindow::onButton(BattleContinueMode mode)
{
    PostQuitMessage(0);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
