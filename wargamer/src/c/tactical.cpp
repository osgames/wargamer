/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#if 0
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Tactical Battle within a campaign
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "cbatmsg.hpp"
#include "campint.hpp"
#include "compos.hpp"

void StartTacticalMsg::run()
{
   d_game->startTactical(d_battle);
}

#if 0
void WargameMessage::SurrenderBattle::run()
{
   d_game->stopTactical();
}
#endif

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
