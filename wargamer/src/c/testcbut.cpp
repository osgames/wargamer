/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Test Custom Button
 *
 * Link with SYSTEM.LIB
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "app.hpp"
#include "myassert.hpp"
#include "wind.hpp"
#include "cbutton.hpp"

class MainWindow : public WindowBaseND
{
    public:
        MainWindow();
        ~MainWindow();
    private:
      LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
       BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
        void onClose(HWND hwnd);
        void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);

       static ATOM registerClass();

      static ATOM s_classAtom;
      static const char s_className[];

        HWND d_but1;
        HWND d_but2;
};

class TestCButtonApp :
    public APP
{
    public:
        TestCButtonApp();
        ~TestCButtonApp();

        BOOL initApplication();
        BOOL initInstance();
        void endApplication();
        BOOL doCmdLine(LPCSTR cmd);

        Boolean processMessage(MSG* msg);
                // Process Message.
                //   Return True if application dealt with it
                //   False to continue as usual

    private:
        MainWindow* d_mainWind;
};


/*========================================================================
 * Main Windows Function
 */

int PASCAL WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
        int result = 0;
        try
        {
                TestCButtonApp application;
                result = application.execute(hInstance, hPrevInstance, lpszCmdLine, nCmdShow);
        }
#if defined(DEBUG)
        catch(AssertError e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Quit Program", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
#endif
        catch(GeneralError e)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "General Error Exception", e.get(), MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }
        catch(...)
        {
                // gApp.pauseGame(TRUE);
                MessageBox(0, "Exception", "Unknown", MB_ICONSTOP | MB_TASKMODAL | MB_SETFOREGROUND);
        }

        return result;
}

/*
 * Test Window class
 */

ATOM MainWindow::registerClass()
{
   if(!s_classAtom)
   {
      WNDCLASS wc;

      wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
      wc.lpfnWndProc = baseWindProc;
      wc.cbClsExtra = 0;
      wc.cbWndExtra = sizeof(MainWindow*);
      wc.hInstance = APP::instance();
      wc.hIcon = NULL;    // LoadIcon(APP::instance(), MAKEINTRESOURCE(ICON_WARGAMER));
      wc.hCursor = LoadCursor(NULL, IDC_ARROW);
      wc.hbrBackground = NULL;   // (HBRUSH) (COLOR_BACKGROUND + 1);
      wc.lpszMenuName = NULL;
      wc.lpszClassName = s_className;  // szMainClass;   // szAppName;
      s_classAtom = RegisterClass(&wc);
   }

   ASSERT(s_classAtom != 0);

   return s_classAtom;
}

static ATOM MainWindow::s_classAtom = 0;
static const char MainWindow::s_className[] = "TestCButton";

MainWindow::MainWindow() :
    d_but1(NULL),
    d_but2(NULL)
{
    registerClass();

    createWindow(0,
        s_className,
        "Test Custom Button",
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
        NULL,
        NULL,
        APP::instance());
}

MainWindow::~MainWindow()
{
}


LRESULT MainWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
#ifdef DEBUG_MAINWIND_MESSAGES
   debugLog("MainWindow_Imp::procMessage(%s)\n",
      getWMdescription(hWnd, msg, wParam, lParam));
#endif

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_COMMAND,        onCommand);
      HANDLE_MSG(hWnd, WM_CLOSE,          onClose);
        HANDLE_MSG(hWnd, WM_CREATE,             onCreate);

   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}

BOOL MainWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
    const char text1[] = "1";
    const char text2[] = "2";

    d_but1 = CreateWindow(CUSTOMBUTTONCLASS, text1,
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        10,10,20,20,
        hWnd, (HMENU)1, APP::instance(), NULL);

    CustomButton b1(d_but1);
    // b1.setText(text1);

    d_but2 = CreateWindow(
        CUSTOMBUTTONCLASS, text2,
        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE,
        10,10,0,0,
        hWnd, (HMENU)2, APP::instance(), NULL);

    CustomButton b2(d_but2);
    // b2.setText(text2);

    return TRUE;
}

void MainWindow::onClose(HWND hWnd)
{
    PostQuitMessage(0);
}

void MainWindow::onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
    switch(id)
    {
        case 1:
            MoveWindow(d_but2, 30,10,20,20, TRUE);
            break;
        case 2:
            MoveWindow(d_but2, 30,10,0,0, TRUE);
            break;
    }
}




TestCButtonApp::TestCButtonApp() :
    d_mainWind(0)
{
}

TestCButtonApp::~TestCButtonApp()
{
    endApplication();
}

BOOL TestCButtonApp::initApplication()
{
    CustomButton::initCustomButton(APP::instance());
    return TRUE;
}

BOOL TestCButtonApp::initInstance()
{
    // Create a window with a custom button in

    d_mainWind = new MainWindow;
    return true;
}

void TestCButtonApp::endApplication()
{
    // Delete window

    delete d_mainWind;
    d_mainWind = 0;
}

BOOL TestCButtonApp::doCmdLine(LPCSTR cmd)
{
    return true;
}


Boolean TestCButtonApp::processMessage(MSG* msg)
{
    return false;
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
