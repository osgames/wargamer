# Microsoft Developer Studio Generated NMAKE File, Based on Battle.dsp
!IF "$(CFG)" == ""
CFG=Battle - Win32 Debug
!MESSAGE No configuration specified. Defaulting to Battle - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "Battle - Win32 Release" && "$(CFG)" != "Battle - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Battle.mak" CFG="Battle - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Battle - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Battle - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Battle - Win32 Release"

OUTDIR=.\..\..\exe
INTDIR=.\o\release
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\Battle.dll"

!ELSE 

ALL : "system - Win32 Release" "ob - Win32 Release" "gwind - Win32 Release" "gamesup - Win32 Release" "batgame - Win32 Release" "$(OUTDIR)\Battle.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batgame - Win32 ReleaseCLEAN" "gamesup - Win32 ReleaseCLEAN" "gwind - Win32 ReleaseCLEAN" "ob - Win32 ReleaseCLEAN" "system - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\testbat.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\Battle.dll"
	-@erase "$(OUTDIR)\Battle.exp"
	-@erase "$(OUTDIR)\Battle.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /GR /GX /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\gwind" /I "..\batgame" /I "..\batdata" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATTLE_DLL" /Fp"$(INTDIR)\Battle.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Battle.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:no /pdb:"$(OUTDIR)\Battle.pdb" /machine:I386 /out:"$(OUTDIR)\Battle.dll" /implib:"$(OUTDIR)\Battle.lib" 
LINK32_OBJS= \
	"$(INTDIR)\testbat.obj" \
	"$(OUTDIR)\batgame.lib" \
	"$(OUTDIR)\gamesup.lib" \
	"$(OUTDIR)\gwind.lib" \
	"$(OUTDIR)\ob.lib" \
	"$(OUTDIR)\system.lib"

"$(OUTDIR)\Battle.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Battle - Win32 Debug"

OUTDIR=.\..\..\exe
INTDIR=.\o\debug
# Begin Custom Macros
OutDir=.\..\..\exe
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\BattleDB.dll"

!ELSE 

ALL : "system - Win32 Debug" "ob - Win32 Debug" "gwind - Win32 Debug" "gamesup - Win32 Debug" "batgame - Win32 Debug" "$(OUTDIR)\BattleDB.dll"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"batgame - Win32 DebugCLEAN" "gamesup - Win32 DebugCLEAN" "gwind - Win32 DebugCLEAN" "ob - Win32 DebugCLEAN" "system - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\testbat.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\BattleDB.dll"
	-@erase "$(OUTDIR)\BattleDB.exp"
	-@erase "$(OUTDIR)\BattleDB.ilk"
	-@erase "$(OUTDIR)\BattleDB.lib"
	-@erase "$(OUTDIR)\BattleDB.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\gwind" /I "..\batgame" /I "..\batdata" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_BATTLE_DLL" /Fp"$(INTDIR)\Battle.pch" /YX"stdinc.hpp" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Battle.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /dll /incremental:yes /pdb:"$(OUTDIR)\BattleDB.pdb" /debug /machine:I386 /out:"$(OUTDIR)\BattleDB.dll" /implib:"$(OUTDIR)\BattleDB.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\testbat.obj" \
	"$(OUTDIR)\batgameDB.lib" \
	"$(OUTDIR)\gamesupDB.lib" \
	"$(OUTDIR)\gwindDB.lib" \
	"$(OUTDIR)\obDB.lib" \
	"$(OUTDIR)\systemDB.lib"

"$(OUTDIR)\BattleDB.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Battle.dep")
!INCLUDE "Battle.dep"
!ELSE 
!MESSAGE Warning: cannot find "Battle.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "Battle - Win32 Release" || "$(CFG)" == "Battle - Win32 Debug"
SOURCE=.\testbat.cpp

"$(INTDIR)\testbat.obj" : $(SOURCE) "$(INTDIR)"


!IF  "$(CFG)" == "Battle - Win32 Release"

"batgame - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" 
   D:
   cd "..\c"

"batgame - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ELSEIF  "$(CFG)" == "Battle - Win32 Debug"

"batgame - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" 
   D:
   cd "..\c"

"batgame - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\batgame"
   $(MAKE) /$(MAKEFLAGS) /F .\batgame.mak CFG="batgame - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Battle - Win32 Release"

"gamesup - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" 
   D:
   cd "..\c"

"gamesup - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ELSEIF  "$(CFG)" == "Battle - Win32 Debug"

"gamesup - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" 
   D:
   cd "..\c"

"gamesup - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\GAMESUP"
   $(MAKE) /$(MAKEFLAGS) /F .\gamesup.mak CFG="gamesup - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Battle - Win32 Release"

"gwind - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" 
   D:
   cd "..\c"

"gwind - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ELSEIF  "$(CFG)" == "Battle - Win32 Debug"

"gwind - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" 
   D:
   cd "..\c"

"gwind - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\src\gwind"
   $(MAKE) /$(MAKEFLAGS) /F .\gwind.mak CFG="gwind - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Battle - Win32 Release"

"ob - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" 
   D:
   cd "..\c"

"ob - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ELSEIF  "$(CFG)" == "Battle - Win32 Debug"

"ob - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" 
   D:
   cd "..\c"

"ob - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\OB"
   $(MAKE) /$(MAKEFLAGS) /F .\ob.mak CFG="ob - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ENDIF 

!IF  "$(CFG)" == "Battle - Win32 Release"

"system - Win32 Release" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" 
   D:
   cd "..\c"

"system - Win32 ReleaseCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Release" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ELSEIF  "$(CFG)" == "Battle - Win32 Debug"

"system - Win32 Debug" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" 
   D:
   cd "..\c"

"system - Win32 DebugCLEAN" : 
   d:
   cd "\SRC\SOURCEFORGE\WARGAMER\SRC\SYSTEM"
   $(MAKE) /$(MAKEFLAGS) /F .\system.mak CFG="system - Win32 Debug" RECURSE=1 CLEAN 
   D:
   cd "..\c"

!ENDIF 


!ENDIF 

