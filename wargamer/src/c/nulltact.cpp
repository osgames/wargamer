/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#if 0
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Tactical battle for Campaign Only compilation
 *
 * Just changes message back into calculated mode.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"// 
#include "cbatint.hpp"
#include "cbatmsg.hpp"
#include "msgbox.hpp"
#include "cbattle.hpp"
#include "compos.hpp"

void StartTacticalMsg::run()
{
   const char* title = "Tactical Battle";
   const char* text = 
      "The Tactical battle is not available in this version of the program."
      "The Battle results will be calculated instead";

   messageBox(title, text, MB_ICONINFORMATION | MB_OK);

   d_battle->setMode(BattleMode::Calculated);
}

#if 0
void SurrenderBattle::run()
{
   const char* title = "Surrender Battle";
   const char* text = 
      "The Tactical battle is not available in this version of the program.";

   messageBox(title, text, MB_ICONINFORMATION | MB_OK);
}
#endif

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
