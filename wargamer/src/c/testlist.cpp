/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Test Program for Intrusive Single linked lists
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "sllist.hpp"
#include <iostream.h>

class TestObject : public SLink
{
    public:
        TestObject(int n) : d_value(n)
        {
            cout << "Created object " << d_value << endl;
        }


        ~TestObject()
        {
            cout << "Delete object " << d_value << endl;
        }

        int value() const { return d_value; }

    private:
        int d_value;
};

inline ostream& operator << (ostream str, const TestObject& ob)
{
    cout << ob.value();
    return str;
}


class TestList : public SList<TestObject>
{
    public:
        void print();
        void remove(TestObject* ob)
        {
            cout << "Removing " << *ob << endl;
            SList<TestObject>::remove(ob);
            print();
        }
};

void TestList::print()
{
    cout << "List contains <";

    bool first = true;

    SListIterR<TestObject> iter(this);
    while(++iter)
    {
        const TestObject* ob = iter.current();

        if(!first)
        {
            cout << ", ";
        }
        first = false;

        cout << *ob;
    }

    cout << ">" << endl;
}

class TestIter : public SListIter<TestObject>
{
    public:
        TestIter(TestList* list) : d_list(list), SListIter<TestObject>(list) { }

        void remove()
        {
            cout << "iterator::remove(" << *current() << ")" << endl;
            SListIter<TestObject>::remove();
            d_list->print();
        }

    private:
        TestList* d_list;

};

void main()
{
    TestList list;

    for(int i = 0; i < 10; ++i)
    {
        TestObject* ob = new TestObject(i);

        if(i < 5)
            list.append(ob);
        else
            list.insert(ob);
    }

    /*
     * test iterator
     */

    list.print();

    /*
     * Test deletion
     */

    i = 0;

    TestObject* obs[10];

    {
        SListIter<TestObject> iter(&list);
        while(++iter)
        {
            obs[i++] = iter.current();
        }
    }

    list.remove(obs[3]);
    list.remove(obs[0]);
    list.remove(obs[9]);

    /*
     * Test Deletion from iterator
     */

    i = 0;
    {
        for(TestIter iter(&list); ++iter; ++i)
        {
            if(i == 3)
                iter.remove();
            else
                cout << "Iterating over " << *iter.current() << endl;
        }
    }

    i = 0;
    {
        for(TestIter iter(&list); ++iter; ++i)
        {
            if( (i == 0) || (i == 5) )
                iter.remove();
            else
                cout << "Iterating over " << *iter.current() << endl;
        }
    }

    if(i != 6)
        cout << "Error: Iterator::delete did not work properly with iterator" << endl;

    {
        for(TestIter iter(&list); ++iter; ++i)
        {
            iter.remove();
        }
    }

}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
