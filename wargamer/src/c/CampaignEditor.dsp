# Microsoft Developer Studio Project File - Name="CampaignEditor" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CampaignEditor - Win32 Editor Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CampaignEditor.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CampaignEditor.mak" CFG="CampaignEditor - Win32 Editor Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CampaignEditor - Win32 Editor Debug" (based on "Win32 (x86) Application")
!MESSAGE "CampaignEditor - Win32 Editor Release" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CampaignEditor - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Editor Debug"
# PROP BASE Intermediate_Dir "Editor Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\campgame" /I "..\campdata" /I "..\camp_ai" /I "..\campwind" /I "..\ob" /I "..\campLogic" /I "..\mapwind" /I "..\frontend" /I "..\campedit" /D "EDITOR" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /i "..\.." /d "_DEBUG" /d "EDITOR" /d "CUSTOMIZE" /d "DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 gdi32.lib comctl32.lib User32.lib winmm.lib /nologo /subsystem:windows /debug /machine:I386 /out:"..\..\exe/CampaignEditorDB.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "CampaignEditor - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Editor Release"
# PROP BASE Intermediate_Dir "Editor Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MD /W3 /Gm /GR /GX /ZI /Od /I "..\h" /I "..\system" /I "..\gamesup" /I "..\..\res" /I "..\gwind" /I "..\campgame" /I "..\campdata" /I "..\camp_ai" /I "..\campwind" /I "..\ob" /I "..\campLogic" /I "..\mapwind" /I "..\frontend" /I "..\campedit" /D "NDEBUG" /D "EDITOR" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /i "..\.." /d "NDEBUG" /d "EDITOR" /d "CUSTOMIZE"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib gdi32.lib comctl32.lib User32.lib winmm.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "CampaignEditor - Win32 Editor Debug"
# Name "CampaignEditor - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\res\campaign.rc
# End Source File
# Begin Source File

SOURCE=..\..\res\english.rc
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\..\res\german.rc
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\..\res\multiplayer.rc
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\..\res\resource.rc
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\..\res\version.rc
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\wargamer.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\wgedit.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\res\resource.h
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\res\version.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
