/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

// ---------------------------------------------------------------------------------------------------

typedef struct tagKEYDATA
{
	char Text[100];
	int Level;
	float CheckSum;
} KEYDATA,*LPKEYDATA;

//

KEYDATA KeyData;

// ---------------------------------------------------------------------------------------------------

void Encrypt(unsigned char *Data,int Size);

// ---------------------------------------------------------------------------------------------------

void main()
{
	char Text[100],Num[5];
	int Level;

	printf("Wargamer Keycode Generator v0.01\n\n");
    
	printf("Enter Text :");
	fgets(Text,60,stdin);

    
  
	if(strlen(Text) > 60)
	{
		printf("Text too long. aborting.\n");
		return;
	}

	printf("Enter Game Level (0-9) :");

	fgets(Num,4,stdin);

	sscanf(Num,"%d",&Level);

	if(Level < 1 || Level > 9)
	{
		printf("\nLevel out of range! aborting.\n");
		return;
	}
    
    // Set the randomise number
    {
    	int i,r;
        
        for(i=0;i<strlen(Text);i++)
        	r += Text[i];
        
        srand(r);
	}
  
	strcpy(KeyData.Text,Text);
	KeyData.Level = Level;
	KeyData.CheckSum = 3.141f;

	Encrypt((unsigned char*)&KeyData,sizeof(KEYDATA));
}

// ---------------------------------------------------------------------------------------------------

void Encrypt(unsigned char *Data,int Size)
{
	FILE *fp;
	int FinalSize = Size*16;
	int Pos = 0,i,b,r = 0;
	unsigned char *NewData = (unsigned char *)malloc(FinalSize);
	unsigned char *Used;
	int Offset = 5555,Offset2 = 555;
	int Test;
	int OrigOffset;

	Used = (unsigned char *)malloc(FinalSize*8);

	Test = 1;

	while(Test)
	{
		Test = 0;
        
		if(r++ > 500)
        {
        	printf("Failed to create code. Aborting.\n");
            return;
        }

		// printf(".");

		OrigOffset = Offset = (rand()%10000)+1000;
		Offset2 = (rand()%2000)+500;

		Pos = 0;

		memset(Used,0,FinalSize*8);
		memset(NewData,0,FinalSize);

		for(i=0;i<Size;i++)
		{
			for(b=0;b<8;b++)
			{
				char c = ((*(Data+i))>>b);

				Used[Pos] = 1;

				if(c&1)
					*(NewData+(Pos/8)) |= ((c&1)<<(Pos%8));

				Pos += Offset;
				Pos = (Pos%(FinalSize*8));

				if(Used[Pos])
					Test = 1;

				Offset += Offset2;
			}
		}
	}


	for(i=0;i<FinalSize*8;i++)
	{
		if(!Used[i])
		{
			char c = (rand()&1);

			*(NewData+(i/8)) |= ((c&1)<<(i%8));
		}
	}

	//	printf("Using [%d] [%d]\n",OrigOffset,Offset2);
   
	if((fp = fopen("code.key","wb")) == NULL)
	{
		printf("Failed to write to file.\n");
		return;
	}


	fwrite(&OrigOffset,sizeof(int),1,fp);
	fwrite(&Offset2,sizeof(int),1,fp);
	fwrite(NewData,sizeof(char),FinalSize,fp);

	fclose(fp);

    printf("Written to file [code.key]\n");

}

// ---------------------------------------------------------------------------------------------------
