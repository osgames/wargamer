/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------------------- */
/* ---------------------------------------------------------------------------------------- */


/* ---------------------------------------------------------------------------------------- */
/* -------------------------------*  I N C L U D E S  *------------------------------------ */ 
/* ---------------------------------------------------------------------------------------- */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

/* ---------------------------------------------------------------------------------------- */
/* -------------------------------*  T Y P E D E F S  *------------------------------------ */ 
/* ---------------------------------------------------------------------------------------- */

// ---------------------------------------------------------------------------------------------------

typedef struct tagKEYDATA
{
	char Text[100];
	int Level;
	float CheckSum;

} KEYDATA,*LPKEYDATA;

/* ---------------------------------------------------------------------------------------- */
/* -----------------------------*  P R O T O T Y P E S  *---------------------------------- */ 
/* ---------------------------------------------------------------------------------------- */

LPKEYDATA DECODE_KEY(char *Filename);

/* ---------------------------------------------------------------------------------------- */
/* ------------------------------*  V A R I A B L E S  *----------------------------------- */ 
/* ---------------------------------------------------------------------------------------- */


/* ---------------------------------------------------------------------------------------- */
/* ------------------------------*  F U N C T I O N S  *----------------------------------- */ 
/* ---------------------------------------------------------------------------------------- */


/* ---------------------------------------------------------------------------------------- */
//	NAME			: main
//	INPUTS			:
//	OUTPUTS			:
//	NOTES			: just a quick test main to see if it works
/* ---------------------------------------------------------------------------------------- */

void main()
{
	LPKEYDATA KeyBlock;

	printf("Reading Key [code.key]\n");

	KeyBlock = DECODE_KEY("code.key");

	if(KeyBlock)
	{
		printf("Got Key : Text [%s] Level [%d]\n",KeyBlock->Text,KeyBlock->Level);
	}
	else
	{
		printf("Failed to read Key!!!\n");
	}
}

/* ---------------------------------------------------------------------------------------- */
//	NAME			: DECODE_KEY
//	INPUTS			: Filename of the Key file
//	OUTPUTS			: NULL if failed to read or decode correctly, or pointer the Key Block
//	NOTES			:
/* ---------------------------------------------------------------------------------------- */

LPKEYDATA DECODE_KEY(char *Filename)
{
	LPKEYDATA KeyData;
	FILE *fp;
	int Size,Offset1,Offset2;
	int NewSize,i,b,Pos = 0;
	unsigned char *Data,*NewData;

	if((fp = fopen(Filename,"rb")) == NULL)
	{
		return(NULL);
	}

	fseek(fp,0,SEEK_END);

	Size = ftell(fp);

	fseek(fp,0,SEEK_SET);

	Data = (unsigned char*)malloc(Size);
	fread(&Offset1,sizeof(int),1,fp);
	fread(&Offset2,sizeof(int),1,fp);

	Size -= (sizeof(int)*2);

	fread(Data,sizeof(char),Size,fp);

	fclose(fp);

	NewSize = Size/16;

	// We have predefined offset values that we can check against.
	if(Offset1 < 1000 || Offset1 > 11000)
		return(NULL);
	if(Offset2 < 500 || Offset2 > 2500)
		return(NULL);

	// Is the decoded data going to the same size as the structure.
	if(NewSize != sizeof(KEYDATA))
		return(NULL);

	NewData = (unsigned char*)malloc(NewSize);

	memset(NewData,0,NewSize);

	for(i=0;i<NewSize;i++)
	{
		for(b=0;b<8;b++)
		{
			unsigned char c = ((*(Data+(Pos/8)))>>(Pos%8));

			*(NewData+i) |= ((c&1)<<b);

			Pos += Offset1;
			Pos = (Pos%(Size*8));
		
			Offset1 += Offset2;
		}
	}

	// Cast the data we have decoded to the KEYDATA structure
	KeyData = (LPKEYDATA)NewData;

	// Is the checksum wrong?
	if(KeyData->CheckSum != 3.141f)
		return(NULL);

	return(KeyData);
}

/* ---------------------------------------------------------------------------------------- */
