##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

######################################################
#
# Makefile for KeyCode
#
######################################################

name = KeyCode

CDIR=.
HDIR=.
EXT=exe
ROOT=.
EXE_DIR=.

# !include $(ROOT)\wgpaths.mif
LNK=$(name).lk

lnk_dependencies += keycode.mk

OBJS = keycode.obj

all :: $(TARGETS) .SYMBOLIC

!include ..\dosnt.mif

linkit : .PROCEDURE
	 @%append $(LNK) option stack=64k
	 @%append $(LNK) debug DWARF all
!ifdef NODEBUG
#	 @%append $(LNK) OPTION VFREMOVAL
	 @%append $(LNK) option ELIMINATE
!endif
	 @%append $(LNK) option SYMFILE




