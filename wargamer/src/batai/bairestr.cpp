/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "bairestr.hpp"
#include "hexmap.hpp"
#include "hexdata.hpp"
//#include <algo.h>
#if 0
using BattleMeasure::HexCord;
//using BattleMeasure::HexPosition;

namespace WG_BattleAI_Internal
{

RTerrain::RTerrain(const RTerrain& tl) :
   d_type(tl.d_type)
{
   for(RTerrain::const_iterator it = tl.begin();
         it != tl.end();
         ++it)
   {
      add((*it));
   }
}

RTerrain& RTerrain::operator = (const RTerrain& tl)
{
   d_type = tl.d_type;
   for(RTerrain::const_iterator it = tl.begin();
         it != tl.end();
         ++it)
   {
      add((*it));
   }
   return *this;
}

void RTerrain::add(BattleMeasure::HexCord hex)
{
      ASSERT(!inList(hex));
      push_back(BattleMeasure::HexCord(hex));
}

const BattleMeasure::HexCord* RTerrain::find(BattleMeasure::HexCord hex)
{
   for(RTerrain::iterator it = begin();
         it != end();
         ++it)
   {
      HexCord lHex = (*it);
      if(hex == lHex)
         return &(*it);
   }

   return 0;
#if 0
      struct isHex
      {
          isHex(BattleMeasure::HexCord hex) : d_hex(hex) { }
          bool operator()(const BattleMeasure::HexCord& hex) { return hex == d_hex; }
          BattleMeasure::HexCord d_hex;
      };

      iterator it = find_if(begin(), end(), isHex(hex));
      if(it != end())
          return &(*it);
      else
          return 0;
#endif
}

bool RTerrain::inList(BattleMeasure::HexCord hex)
{
      return (find(hex) != 0);
}

//--------------------------------------------
void RTerrainList::add(RTerrain* ter)
{
   push_back(ter);
}

bool RTerrainList::inList(BattleMeasure::HexCord hex)
{
   for(RTerrainList::iterator it = begin();
         it != end();
         ++it)
   {
      if((*it)->inList(hex))
         return True;
   }

   return False;
}
// ---------------------------------------------

static RTerrainList s_list;

RTerrainList* restrictiveTerrain()
{
   return &s_list;
}

struct HexRTerrainData {
   HexCord::HexDirection d_hd;
   PathType d_type;

   HexRTerrainData() :
             d_hd(HexCord::Stationary),
             d_type(LP_None) {}
};


int numberPaths(const BattleTerrainHex& hi, const HexCord::HexDirection lastHD,
                         HexRTerrainData* whichHD)
{
   // hex face values (as used by BattleTerrainHex) going from E, NE, NW, W, SW, SE
   static const UBYTE s_faceValue[HexCord::End] = {
                        0x01, 0x02, 0x04, 0x08, 0x10, 0x20
   };

   // first, count number of hexsides that have paths running from them
   const int nPathTypes = 2;
   int nPaths = 0;

   for(int p = 0; p < nPathTypes; p++)
   {
             PathType type = (p == 0) ? hi.d_path1.d_type : hi.d_path2.d_type;

             // if path is not a river, skip
             if(type != LP_River)
                  continue;

             EdgeMap edges = (p == 0) ? hi.d_path1.d_edges : hi.d_path2.d_edges;

             // go through each hex direction and see if a road passes through
             for(HexCord::HexDirection hd = HexCord::First; hd < HexCord::End; INCREMENT(hd))
             {
                        if( ((lastHD == HexCord::Stationary) || (hd != HexCord::oppositeDirection(lastHD))) &&
                                                (edges & s_faceValue[hd]) )
                        {
                           ASSERT(nPaths < HexCord::Count);

                           if(whichHD)
                           {
                                     whichHD[nPaths].d_hd = hd;
                                     whichHD[nPaths].d_type = type;
                           }

                           nPaths++;
                        }
             }
   }

   return nPaths;
}

bool hexInList(const HexCord& hex, const HexList& list)
{
   SListIterR<HexItem> iter(&list);
   while(++iter)
   {
             if(iter.current()->d_hex == hex)
                        return True;
   }

   return False;
}

// Warning!!! May be recursive
void followRiver(
      RCPBattleData bd,
      const HexCord& startHex,
      HexCord::HexDirection lastHD,
      HexList& crossRivers)
{
   RTerrain* terrain = new RTerrain(RTerrain::River);
   ASSERT(terrain);

   terrain->add(startHex);
   const BattleTerrainHex& hexInfo = bd->getTerrain(startHex);

   HexRTerrainData hd[HexCord::Count];
   int nPaths = numberPaths(hexInfo, lastHD, hd);

   // if a crossroads, add to that list
   if(nPaths > 1)
      crossRivers.newItem(startHex);

   // loop through each path, finding best way to go
   for(int i = 0; i < nPaths; i++)
   {
      ASSERT(hd[i].d_hd != HexCord::Stationary);

      // get next hex
      HexCord hex;
      if(bd->moveHex(startHex, hd[i].d_hd, hex))
      {
         const BattleTerrainHex& nHexInfo = bd->getTerrain(hex);

         // it should be a river!
         ASSERT(nHexInfo.d_path1.d_type == LP_River || nHexInfo.d_path2.d_type == LP_River);

         HexCord::HexDirection lastHD = hd[i].d_hd;
         PathType lastType = hd[i].d_type;

         // loop while we're going just one way
         for(;;)
         {
            if(hexInList(hex, crossRivers))
               break;

            terrain->add(hex);

            HexRTerrainData hd2[HexCord::Count];
            int nPaths = numberPaths(nHexInfo, lastHD, hd2);
            if(nPaths == 0)
               break;

            if(nPaths > 1)
            {
               // Warning!! Recursing
               while(nPaths--)
                  followRiver(bd, hex, lastHD, crossRivers);
            }
            // otherwise, keep looping
            else
            {
               HexCord nextHex;
               if(bd->moveHex(hex, hd2[0].d_hd, nextHex))
               {
                  hex = nextHex;
                  lastHD = hd2[0].d_hd;
                  lastType = hd2[0].d_type;
               }
               else
                  break;
            }
         }
      }
   }

   s_list.add(terrain);
}

void findRivers(RCPBattleData bd)
{
   const HexCord& mapSize = bd->map()->getSize();
   for(int y = 0; y < mapSize.y(); y++)
   {
      for(int x = 0; x < mapSize.x(); x++)
      {
         HexCord hex(x, y);
         const BattleTerrainHex& hexInfo = bd->getTerrain(hex);

         if( (hexInfo.d_path1.d_type == LP_River || hexInfo.d_path2.d_type == LP_River) &&
               (!s_list.inList(hex)) )
         {
            HexList crossRivers;
            followRiver(bd, hex, HexCord::Stationary, crossRivers);
         }
      }
   }
}

void initRestrictiveTerrain(RCPBattleData bd)
{
   findRivers(bd);
}


};  // namespace WG_BattleAI_Internal
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
