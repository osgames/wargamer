/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *---------------------------------------------------  -------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Mid-Level Planner
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aib_mid.hpp"
#include "aib_data.hpp"
#include "aib_area.hpp"
#include "aib_util.hpp"
#include "m_cp.hpp"
#include "pdeploy.hpp"
#include "bobutil.hpp"
#include "hexdata.hpp"
#include "b_send.hpp"
#include "mid_op.hpp"
#include "misc.hpp"
#include "filebase.hpp"
#include "corpdepl.hpp"
#include "midutil.hpp"

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

namespace WG_BattleAI_Internal
{

using namespace BOB_Definitions;

static UWORD s_areaVersion = 0x0000;
bool HexAreaList::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_areaVersion);

   int items;
   f >> items;
   while(items--)
   {
      HexArea area;
      area.readData(f);
      push_back(area);
#ifdef DEBUG
//      HexArea hi = back();
#endif
   }

   return f.isOK();
}

bool HexAreaList::writeData(FileWriter& f) const
{
   f << s_areaVersion;

   int items = size();
   f << items;

   for(std::list<HexArea>::const_iterator hi = begin();
       hi != end();
       ++hi)
   {
      (*hi).writeData(f);
   }

   return f.isOK();
}

MidLevelPlanner::MidLevelPlanner(GameInfo* gameInfo, HiObjective* ob) :
    GameInfoUser(gameInfo),
    d_objective(ob),
    d_units(new MidUnitList),
    d_flags(0),
    d_nextReinforcementCheck(0),
    d_bombardTime(0)
{
#ifdef DEBUG
    gameInfo->log("MidLevelPlanner created");
    // logObjective();
#endif

    // Warning.. do not make any calls to ob-> because ob is still being
    // initialized and so pure virtual functions may get called.
}

MidLevelPlanner::~MidLevelPlanner()
{
#ifdef DEBUG
   gameInfo()->log("MidLevelPlanner deleted");
#endif
   delete d_units;
}

void MidLevelPlanner::process()
{
#ifdef DEBUG
   gameInfo()->log("\n----------- MidLevelPlanner::process");
#endif

   // TODO: Check MidUnitInfoList for any removed units
   // Add new units
   if(d_units->size() == 0)
   {
      addUnits();

      // deploy
      procDeploy();
   }

   // Game in progress
   if(!gameInfo()->isDeploying())
      procGameInProgress();
}

void MidLevelPlanner::addUnits()
{
   ASSERT(d_objective->units().size() == 1);
   d_units->addUnit((*d_objective->units().begin()));
   d_cincDI.cp((*d_objective->units().begin()));
   d_cincDI.updateOrder();
   d_cincDI.ordered(True);
}

void MidLevelPlanner::checkForReinforcements()
{
   // has any Divisions arrived as reinforcements belonging to top unit?
   MidUnitInfo& mi = d_units->front();
   if(mi.cp()->hasQuitBattle())
      return;

   for(ConstBattleUnitIter iter(mi.cp()); !iter.isFinished(); iter.next())
   {
      if(iter.cp()->getRank().sameRank(Rank_Division) &&
         iter.cp() != mi.cp() &&
         !mi.map().inMap(iter.cp()))
      {
         MU_DeployItem* di = mi.map().add(iter.cp(), 1);
         di->updateOrder();

         // issue an order to move up to the front
         const HexArea& area = d_objective->area();
         HexPosition::Facing f = WG_BattleAI_Utils::frontFacing(area[0], area[1]);
         HexCord hex = mi.cp()->leftHex();
         makeOrder(*di, CPF_March, SP_MarchFormation, CPF_March, SP_MarchFormation, f, &hex, False);
#ifdef DEBUG
         gameInfo()->log("%s's %s has arrived as reinforcement. Will move to hex(%d, %d)",
            mi.cp()->getName(),
            iter.cp()->getName(),
            static_cast<int>(hex.x()),
            static_cast<int>(hex.y()));
#endif
      }
   }
}

void MidLevelPlanner::procMessage(const BattleMessageInfo& msg)
{
   if(d_cincDI.cp() != NoBattleCP)
   {
      MidLevel_Op mlop(gameInfo(), this);
      mlop.procMessage(msg);
   }
#ifdef DEBUG
   else
      FORCEASSERT("Mid-Level Planner has not been initialized!");
#endif
}

void MidLevelPlanner::procDeploy()
{
#ifdef DEBUG
   gameInfo()->log("MidLevelPlanner::procDeploy");
   logObjective();
#endif

   // Go through each corps, independent division,
   // determine composition, etc.
   if(gameInfo()->isDeploying())
   {
      if(d_units->size() <= 0)
         return;

//      const MidUnitInfo& mi = (*d_objective->units().begin());
      MidUnitInfo& mi = d_units->front();
      if(mi.cp()->hasQuitBattle())
         return;

      enum { Left, Right };
      const HexArea& area = d_objective->area();

      const int objWidth = WG_BattleAI_Utils::getDist(area[0], area[1]);
      const HexPosition::Facing facing = WG_BattleAI_Utils::frontFacing(area[0], area[1]);

      // get composition
      CRefBattleCP cp = mi.cp();
      UBYTE nInfXX    = 0;
      UBYTE nCavXX    = 0;
      UBYTE nArtXX    = 0;
      int   totalXXSP = 0;
      corpsTypes(gameInfo()->battleData(),
            cp, nInfXX, nCavXX, nArtXX, totalXXSP, True);

      UBYTE nTotalXX = nInfXX + nCavXX + nArtXX;

      CorpDeployment cd;
      if(!corpDeployment(cp, nInfXX, nCavXX, nArtXX, cd, gameInfo()->random()))
      {
         FORCEASSERT("Corp deployment not found");
         return;
      }
      ASSERT(cd.nFront() > 0);

      // go through each unit in corps and assign deploy position
      BasicUnitType::value bt = (nInfXX > 0) ? BasicUnitType::Infantry :
                      (nCavXX > 0) ? BasicUnitType::Cavalry : BasicUnitType::Artillery;

      // first do the front row
      SPFormation sf = (d_objective->posture() == BattleOrderInfo::Offensive) ?
            SP_ColumnFormation : SP_LineFormation;

      assignDivisionalPositions(&mi, cd, CorpDeployment::Front, area[Left], area[Right],
            facing, CPF_Deployed,  sf, bt);

      // Now do back row
      if(cd.nRear() > 0)
      {
         assignDivisionalPositions(&mi, cd, CorpDeployment::Rear, area[Left], area[Right],
            facing, CPF_Massed, SP_MarchFormation, bt);
      }

#ifdef DEBUG
      gameInfo()->log("Assigning deploy map -------------------");
      for(int r = 0; r < 2; r++)
      {
         int cols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
         for(int c = 0; c < cols; c++)
         {
            const MU_DeployItem& di = mi.map().item(c, r);
            gameInfo()->log("%s is assigned r = %d, c = %d", di.cp()->getName(), r, c);
         }
      }
#endif
   }
   else
   {
      initDeployMap();
#ifdef DEBUG
      gameInfo()->log("(Mid-Level) %s has arrived as reinforcement", d_cincDI.cp()->getName());
#endif
   }

   onObjectiveAchieved();
}

void MidLevelPlanner::initDeployMap()
{
   for(MidUnitList::iterator it = d_units->begin();
         it != d_units->end();
         ++it)
   {
       addToDeployMapOnMap(&(*it));
   }
}

void MidLevelPlanner::sendCPAggPos(bool playing)
{
   if(playing)
   {
      for(MidUnitList::iterator it = d_units->begin();
            it != d_units->end();
            ++it)
      {
         BattleOrder ord;
         (*it).cp()->lastOrder(&ord);
         ord.posture(d_objective->posture());
         ord.aggression(d_objective->aggression());
         sendBattleOrder((*it).cp(), &ord);
      }
   }
   else
   {
      for(MidUnitList::iterator it = d_units->begin();
            it != d_units->end();
            ++it)
      {
         RefBattleCP cp = const_cast<BattleCP*>((*it).cp());
         cp->getCurrentOrder().posture(d_objective->posture());
         cp->getCurrentOrder().aggression(d_objective->aggression());
         cp->posture(d_objective->posture());
         cp->aggression(d_objective->aggression());
      }
   }
}

void MidLevelPlanner::addToDeployMapOffMap(
         MidUnitInfo* mi,
         CorpDeployment& cd,
         CorpDeployment::Where where,
         BasicUnitType::value bt)
{
   MidUnitDeployMap& map = mi->map();
   CRefBattleCP parent = mi->cp();

   bool hasLineXX = False;
   bool hasSupportXX = False;

   // add units to a local unit list
   while(cd.iter(where))
   {
      for(ConstBattleUnitIter uIter(parent); !uIter.isFinished(); uIter.next())
      {
         if( (uIter.cp() != parent || parent->getRank().sameRank(Rank_Division)) &&
             !(map.inMap(uIter.cp())) )
         {
            bool add = False;
            bool isLine = False;

            RefBattleCP subCP = const_cast<BattleCP*>(uIter.cp());

            // a line SP
            if( (bt == BasicUnitType::Infantry && subCP->generic()->isInfantry()) ||
                           (bt == BasicUnitType::Cavalry && subCP->generic()->isCavalry()) )
            {
               add = (cd.item(where) == CorpDeployment::LineXX);
               isLine = hasLineXX = True;
            }

            else
            {
               add = (cd.item(where) == CorpDeployment::SupportXX);
               hasSupportXX = True;
            }

            if(add)
            {
               // set deployment hex
               MU_DeployItem* di = map.add(subCP, static_cast<int>(where));
               di->setLine(isLine);
               di->setSupport(!isLine);
               di->updateOrder();
               di->ordered(True);
               break;
            }
         }
      }
    }
}

void sortList(
   WG_BattleAI_Utils::LocalCPList& list,
   WG_BattleAI_Utils::LocalCPList& unsortedList,
   const HexCord& leftHex)
{
   SListIter<WG_BattleAI_Utils::LocalCP> uiter(&unsortedList);
   while(++uiter)
   {
      if(list.entries() == 0)
      {
         list.add(uiter.current()->d_cp);
      }
      else
      {
         HexCord uLeft = const_cast<BattleCP*>(uiter.current()->d_cp)->leftHex();
         int uDist = WG_BattleAI_Utils::getDist(uLeft, leftHex);
         SListIter<WG_BattleAI_Utils::LocalCP> siter(&list);
         CRefBattleCP lastCP = NoBattleCP;
         bool added = False;
         while(++siter)
         {
            HexCord sLeft = const_cast<BattleCP*>(siter.current()->d_cp)->leftHex();
            int sDist = WG_BattleAI_Utils::getDist(sLeft, leftHex);

            if(uDist < sDist)
            {
               if(lastCP != NoBattleCP)
                  list.addInsertAfter(lastCP, uiter.current()->d_cp);
               else
                  list.addInsert(uiter.current()->d_cp);

               added = True;
            }

            lastCP = siter.current()->d_cp;
         }

         if(!added)
         {
            list.add(uiter.current()->d_cp);
         }
      }
   }

   ASSERT(list.entries() == unsortedList.entries());
}

void MidLevelPlanner::addToDeployMapOnMap(MidUnitInfo* mi)
{
   MidUnitDeployMap& map = mi->map();
//   HexPosition::Facing corpAvgFace = WG_BattleAI_Utils::averageFacing(mi->cp());
#ifdef DEBUG
   gameInfo()->log("Setting deployMap for %s ----------", mi->cp()->getName());
#endif

   // get facing of our objective
   const HexArea& area = d_objective->area();
//   HexPosition::Facing corpFacing = WG_BattleAI_Utils::frontFacing(area[0], area[1]);

   // build up a local list of front-line units
   WG_BattleAI_Utils::LocalCPList frontCPsUnsorted;
   WG_BattleAI_Utils::LocalCPList rearCPsUnsorted;
   for(ConstBattleUnitIter uIter(mi->cp()); !uIter.isFinished(); uIter.next())
   {
      if(uIter.cp()->getRank().sameRank(Rank_Division))
      {
         // determine if we are closer to the left or right
         HexCord divLeft = const_cast<BattleCP*>(uIter.cp())->leftHex();
         HexCord divRight = const_cast<BattleCP*>(uIter.cp())->rightHex();

         int distToLeft = WG_BattleAI_Utils::getDist(divLeft, area[0]);
         int distToRight = WG_BattleAI_Utils::getDist(divRight, area[1]);
         int closeDist = minimum(distToLeft, distToRight);

         WG_BattleAI_Utils::MoveOrientation mo = (distToLeft <= distToRight) ?
               WG_BattleAI_Utils::moveOrientation(uIter.cp()->facing(), divLeft, area[0], WG_BattleAI_Utils::WideAngle) :
               WG_BattleAI_Utils::moveOrientation(uIter.cp()->facing(), divRight, area[1], WG_BattleAI_Utils::WideAngle);

         if(closeDist <= 3 ||
            mo != WG_BattleAI_Utils::MO_Front ||
            divLeft == area[0] ||
            uIter.cp() == mi->cp())
         {
            frontCPsUnsorted.add(uIter.cp());
         }
         else
         {
            rearCPsUnsorted.add(uIter.cp());
         }
      }
   }

   WG_BattleAI_Utils::LocalCPList frontCPs;
   WG_BattleAI_Utils::LocalCPList rearCPs;

   sortList(frontCPs, frontCPsUnsorted, area[0]);
   sortList(rearCPs, rearCPsUnsorted, area[0]);

   SListIterR<WG_BattleAI_Utils::LocalCP> fiter(&frontCPs);
   while(++fiter)
   {
      // add to front
#ifdef DEBUG
      gameInfo()->log("%s(%s) is added to front", fiter.current()->d_cp->getName(), fiter.current()->d_cp->leader()->getName());
#endif
      MU_DeployItem* di = map.add(fiter.current()->d_cp, 0);
      di->updateOrder();
      di->ordered(True);
   }

   SListIterR<WG_BattleAI_Utils::LocalCP> riter(&rearCPs);
   while(++riter)
   {
      // add to rear
#ifdef DEBUG
      gameInfo()->log("%s(%s) is added to rear", riter.current()->d_cp->getName(), riter.current()->d_cp->leader()->getName());
#endif
      MU_DeployItem* di = map.add(riter.current()->d_cp, 1);
      di->updateOrder();
      di->ordered(True);
   }

#if 0
   WG_BattleAI_Utils::LocalCPList frontCPs;
   HexCord corpLeft;
   HexCord corpRight;
   WG_BattleAI_Utils::getCorpLeftRightBoundry(mi->cp(), corpAvgFace, corpLeft, corpRight, &frontCPs);

   SListIterR<WG_BattleAI_Utils::LocalCP> iter(&frontCPs);
   while(++iter)
   {
      // add to front
#ifdef DEBUG
      gameInfo()->log("%s is added to front", iter.current()->d_cp->getName());
#endif
      MU_DeployItem* di = map.add(iter.current()->d_cp, 0);
      di->updateOrder();
      di->ordered(True);
   }

   // now add any others to rear
   for(ConstBattleUnitIter uIter(mi->cp()); !uIter.isFinished(); uIter.next())
   {
      if(uIter.cp()->getRank().sameRank(Rank_Division) &&
         !(map.inMap(uIter.cp())) )
      {
#ifdef DEBUG
         gameInfo()->log("%s is added to rear", uIter.cp()->getName());
#endif
         MU_DeployItem* di = map.add(const_cast<BattleCP*>(uIter.cp()), 1);
         di->updateOrder();
         di->ordered(True);
      }
   }
#endif
}

void MidLevelPlanner::assignDivisionalPositions(
      MidUnitInfo* mi,
      CorpDeployment& cd,
      CorpDeployment::Where where,
      const HexCord& leftHex,
      const HexCord& rightHex,
      HexPosition::Facing facing,
      CPFormation divFor,
      SPFormation spFor,
      BasicUnitType::value bt)
{
   cd.rewind(where);
   if(gameInfo()->isDeploying())
      addToDeployMapOffMap(mi, cd, where, bt);

   MidUnitDeployMap& map = mi->map();
   const int nCols = (where == CorpDeployment::Front) ? map.nFront() : map.nRear();
   if(nCols <= 0)
      return;

//   CRefBattleCP parent = mi->cp();

   // total corp frontage in hexes
   const int totalFrontage = WG_BattleAI_Utils::getDist(leftHex, rightHex);

   // now allocate positions going left to right
   HexCord bLeft;
   HexCord mSize;
   gameInfo()->battleData()->getPlayingArea(&bLeft, &mSize);
   HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

   // first count total hexes that are to be occupied
   RandomNumber& lRand = gameInfo()->random();
   int gap = (lRand(100) < lRand(50)) ? 2 : 1; // gap between units, if space allows
   int frontage = 0;
   for(int c = 0; c < nCols; c++)
   {
      RefBattleCP cp = const_cast<BattleCP*>(map.item(c, static_cast<int>(where)).cp());
#ifdef DEBUG
      const char* name = cp->getName();
#endif
      if(cp->hasQuitBattle())
         continue;

      CPFormation cpFor = (divFor == CPF_Last || cp->nextFormation() > divFor) ? cp->nextFormation() : divFor;

      int spCount = WG_BattleAI_Utils::countSP(gameInfo()->battleData(), cp, True);
      int rows = 0;
      int cols = 0;
      BobUtility::getRowsAndColoumns(cpFor, spCount, cols, rows);

//    BobUtility::getRowsAndColoumns((where == CorpDeployment::Front && cp->generic()->isArtillery()) ?
//                         CPF_Extended : cpFor, spCount, cols, rows);
      frontage += (c < nCols - 1) ? (cols + gap) : cols;
   }

   int difPer = 0;
   if(frontage > totalFrontage) // no room
   {
      frontage = totalFrontage;
      difPer = maximum(1, ((frontage - totalFrontage) + (nCols / 2)) / nCols);
   }

   const int spacing = frontage / nCols;
   HexCord lHex = leftHex;

   // if a Corp rear unit, move back 4-6 hexes from left hex
   if(where == CorpDeployment::Rear)
   {
//      int rowsBack = lRand(4, 6);//(facing > HexPosition::West) ? 6 : -6;
      HexCord tHex;
      if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), facing, 0, -lRand(4, 6), lHex, tHex))
         FORCEASSERT("Hex not found");
      else
         lHex = tHex;

//      lHex.y(clipValue(lHex.y() + rowsBack, bLeft.y() + 1, tRight.y() - 2));//maximum(0, lHex.y() + rowsBack)); //(facing > HexPosition::West) ? 6 : -6);
   }

   // Find starting left position
   int goRight = (totalFrontage - frontage) / 2;
   HexCord hex = lHex;
   if(goRight > 0)
   {
      HexCord::HexDirection hd = BattleMeasure::rightFlank(facing);
      if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), hd, goRight, lHex, hex))
      {
         FORCEASSERT("Hex not found");
         return;
      }
   }

   // now assign positions to each division
   BattleData* bdnc = const_cast<BattleData*>(reinterpret_cast<const BattleData*>(gameInfo()->battleData()));
   for(c = 0; c < nCols; c++)
   {
      MU_DeployItem& di = map.item(c, static_cast<int>(where));
#ifdef DEBUG
      const char* name = di.cp()->getName();
#endif

      if(di.cp()->hasQuitBattle())
         continue;

      // determine divisional and sp formations
      CPFormation cpFor2 = (divFor == CPF_Last || di.cp()->nextFormation() > divFor) ? di.cp()->nextFormation() : divFor;
      SPFormation spFor2  = (di.cp()->generic()->isArtillery()) ? SP_UnlimberedFormation :
                            (spFor == SP_UnknownFormation || di.cp()->sp()->destFormation() > spFor) ? di.cp()->sp()->destFormation() : spFor;

      RefBattleCP cp = const_cast<BattleCP*>(di.cp());

      // if we are just deploying, set current order only
      if(gameInfo()->isDeploying())
      {
         cp->getCurrentOrder().facing(facing);
         cp->getCurrentOrder().divFormation(cpFor2);
         cp->getCurrentOrder().spFormation(spFor2);
         cp->formation(cpFor2);
         cp->posture(d_objective->posture());
         cp->aggression(d_objective->aggression());
         cp->getCurrentOrder().posture(d_objective->posture());
         cp->getCurrentOrder().aggression(d_objective->aggression());
         for(BattleSPIter spIter(bdnc->ob(), cp); !spIter.isFinished(); spIter.next())
         {
            spIter.sp()->setFormation(spFor2);
         }
      }

      di.updateOrder();

      HexCord tHex = hex;
      // find new position
      if(!WG_BattleAI_Utils::findDeployPosition(gameInfo()->battleData(), cp, facing, tHex, gameInfo()->isDeploying()))
      {
         if(gameInfo()->isDeploying())
         {
#ifdef DEBUG
            gameInfo()->log("Unable to deploy %s, removing from battle", cp->getName());
#endif
            cp->setFled();
         }
      }
      else
      {
         // issue move order if moving
         const HexArea& area = d_objective->area();
         //if(gameInfo()->isDeploying())
         di.objHex(tHex);

         if(shouldMove(di, area))
         {
            // if move distance is greater than 15 hexes, march em
            CPFormation cpFor1 = cpFor2;
            SPFormation spFor1 = spFor2;
#if 0
            if(!di.cp()->firstOrderSent())
            {
               //int dist = WG_BattleAI_Utils::getDist(di.cp()->leftHex(), tHex);
               //cpFor1 = (dist >= 15) ? CPF_March : cpFor2;
               //spFor1 = (dist >= 15) ? SP_MarchFormation : spFor2;
               //di.cp()->noRoadMove(False);
            }
#endif
            bool canDoRoadMove = False;
            // TODO: see if we want to do road movement
            const_cast<BattleCP*>(di.cp())->noRoadMove(!canDoRoadMove);
#if 0//def DEBUG
            if(lstrcmpi("4th Division", di.cp()->getName()) == 0)
            {
               int i = 0;
            }
#endif
            // if changing formation, make sure we can
            if(cpFor1 != di.cp()->formation())
            {
               bool willDeploy = False;
               for(int i = 0; i < CPD_Last; i++)
               {
                  di.order().deployHow(static_cast<CPDeployHow>(i));
                  if(canDeploy(gameInfo()->battleData(), di, cpFor1))
                  {
                     willDeploy = True;
                     break;
                  }
               }

               if(!willDeploy)
               {
                  cpFor1 = cpFor2 = di.cp()->formation();
               }
            }

            if(di.cp()->generic()->isArtillery() && di.bombarding())
               ;
            else
               makeOrder(di, cpFor1, spFor1, cpFor2, spFor2, facing, &tHex, (where == CorpDeployment::Rear));
         }


         // get position for next division
         if(c < (nCols - 1))
         {
            HexCord tHex;
            int goRight = maximum(0, (di.cp()->columns() + gap) - difPer);
            if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), BattleMeasure::rightFlank(facing), goRight, hex, tHex))
            {
               if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), facing, 0, -gameInfo()->random(5, 8), hex, tHex))
                  FORCEASSERT("Hex not found");
               else
                  hex = tHex;
            }
            else
               hex = tHex;
         }
      }
   }

   // place HQ
   if(where == CorpDeployment::Front && mi->cp()->getRank().isHigher(Rank_Division))
   {
      d_cincDI.updateOrder();

      HexCord hex;
      hex.x((leftHex.x() + rightHex.x()) / 2);
      hex.y((leftHex.y() + rightHex.y()) / 2);
      HexCord destHex = hex;
      if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), facing, 0, -lRand(4, 6), hex, destHex))
         FORCEASSERT("Hex not found");

      if(!WG_BattleAI_Utils::findDeployPosition(gameInfo()->battleData(), const_cast<BattleCP*>(d_cincDI.cp()), facing, destHex, gameInfo()->isDeploying()))
         FORCEASSERT("Could deploy unit");
      else if(!gameInfo()->isDeploying())
      {
         if(d_cincDI.cp()->hex() != destHex)
            makeOrder(d_cincDI, divFor, spFor, divFor, spFor, facing, &destHex, True);
      }
   }
}

void MidLevelPlanner::onObjectiveAchieved()
{
   setFlag(ObjectiveAchieved, True);
   d_flags &= ~Reacting;
   d_flags &= ~Retreating;
   d_flags &= ~FinishedBombard;
   bombardFor(0);
}

void MidLevelPlanner::procGameInProgress()
{
   if(gameInfo()->battleData()->type() == BattleData::Historical &&
      gameInfo()->battleData()->getTick() >= d_nextReinforcementCheck)
   {
      checkForReinforcements();
   }

   // reissue orders if new objective
   bool setObj = (d_objective->getNewObjective());// || settingAround());
   if(setObj)
   {
#ifdef DEBUG
      gameInfo()->log("MidLevelPlanner::procGameInProgress for %s", d_cincDI.cp()->getName());
      logObjective();
#endif
      doNewObjective();
   }
   // Unchecked - ammended by Paul 28/9/99
   else
   {
      MidLevel_Op mop(gameInfo(), this);
      mop.process();
   }
   // End

   // issue any orders that need to be issued
   if(issueOrders())
      setObj = True;


   // check units and see what they are up to
   if(!setObj &&
      !objectiveAchieved() &&
      checkForObjectiveAchieved())
   {
      onObjectiveAchieved();
   }
}

int MidLevelPlanner::frontWidth() const
{
   MidUnitInfo& mi = d_units->front();
   if(!mi.cp()->hasQuitBattle() &&
      mi.map().container().size() > 0 &&
      mi.map().nFront() <= mi.map().container().size())
   {
      HexCord left;
      HexCord right;
      for(int c = 0; c < mi.map().nFront(); c++)
      {
         MU_DeployItem& di = mi.map().item(c, 0);
         if(di.cp()->hasQuitBattle())
         {
            continue;
         }

         if(left == HexCord(0, 0))
            left = di.cp()->leftHex();

         right = di.cp()->rightHex();
      }

      if(left != HexCord(0, 0))
         return WG_BattleAI_Utils::getDist(left, right);
#if 0
      const MU_DeployItem& di1 = mi.map().item(0, 0);
      const MU_DeployItem& di2 = mi.map().item(mi.map().nFront() - 1, 0);

      HexCord leftHex = di1->cp()->leftHex();
      HexCord rightHex = di2->cp()->rightHex();

      return WG_BattleAI_Utils::getDist(leftHex, rightHex);
#endif
   }

   return 0;
}


void MidLevelPlanner::issueOrder(
    const CRefBattleCP& cp,
    BattleOrder& batOrder)
{
#ifdef DEBUG
    HexCord destHex = (batOrder.lastWayPoint()) ? batOrder.lastWayPoint()->d_hex :
             (cp->sp() != NoBattleSP) ? cp->leftHex() : cp->hex();

    gameInfo()->log("MidLevelPlanner::issueOrder() (%s) for %s -------- ",
             BattleOrderInfo::orderName(batOrder.mode()),
             cp->getName());

    gameInfo()->log("--------- order has %d waypoints", batOrder.wayPoints().entries());

    if(cp->getRank().sameRank(Rank_Division))
    {
         if(cp->formation() != batOrder.divFormation())
         {
             gameInfo()->log("Changing XX deployment from %s to %s",
                  BattleOrderInfo::divFormationName(cp->formation()),
                  BattleOrderInfo::divFormationName(batOrder.divFormation()));
         }

         if(cp->sp() != NoBattleSP && cp->sp()->fromFormation() != batOrder.spFormation())
         {
             gameInfo()->log("Changing SP formation from %s to %s",
                  BattleOrderInfo::spFormationName(cp->sp()->fromFormation()),
                  BattleOrderInfo::spFormationName(batOrder.spFormation()));
         }

         if(cp->facing() != batOrder.facing())
         {
             gameInfo()->log("Changing facing from %d to %d",
                  static_cast<int>(cp->facing()),
                  static_cast<int>(batOrder.facing()));
         }
    }

    if(batOrder.lastWayPoint())
    {
         gameInfo()->log("---> %s-> destHex = (%d, %d)",
             cp->getName(),
             static_cast<int>(destHex.x()), static_cast<int>(destHex.y()));
    }
#endif

    sendBattleOrder(cp, &batOrder);
}


bool MidLevelPlanner::issueOrders()
{
   bool issued = False;

   MidUnitInfo& mi = d_units->front();
   MidUnitDeployMap& map = mi.map();

   for(int r = 0; r < 2; r++)
   {
      int nCols = (r == 0) ? map.nFront() : map.nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di = map.item(c, r);
         if(!di.cp()->hasQuitBattle() &&
            !di.ordered() &&
            di.canSend(gameInfo()->battleData()->getTick()))
         {
            di.ordered(True);
            issueOrder(di.cp(), di.order());
            issued = True;
         }
      }
   }

   if(d_cincDI.cp()->getRank().isHigher(Rank_Division) &&
      !d_cincDI.cp()->hasQuitBattle() &&
      !d_cincDI.ordered() &&
      d_cincDI.canSend(gameInfo()->battleData()->getTick()))
   {
      d_cincDI.ordered(True);
      issueOrder(d_cincDI.cp(), d_cincDI.order());
      issued = True;
   }

   return issued;
}

void MidLevelPlanner::makeOrder(
   MU_DeployItem& di,
   CPFormation divFor,
   SPFormation spFor,
   CPFormation destDivFor,
   SPFormation destSPFor,
   HexPosition::Facing facing,
   const HexCord* destHex,
   bool delaySend)
{
   bool shouldIssue = False;
   // issue any formation orders

   // issue any movement order
   if(destHex && *destHex != const_cast<BattleCP*>(di.cp())->leftHex())
   {
      //di.objHex(*destHex);
      shouldIssue = True;
      //WG_BattleAI_Utils::setMoveOrder(const_cast<BattleOrder*>(&di.order()), *destHex, facing);
      setMoveOrder(di, *destHex, facing, True);

      if(di.cp()->getRank().sameRank(Rank_Division) && di.order().wayPoints().entries() > 0)
      {
         if(destDivFor != divFor)
         {
            WayPoint* wp = di.order().wayPoints().getLast();

            wp->d_order.d_divFormation = destDivFor;
            wp->d_order.d_whatOrder.add(WhatOrder::CPFormation);
         }

         if(destSPFor != spFor)
         {
            WayPoint* wp = di.order().wayPoints().getLast();

            wp->d_order.d_spFormation = destSPFor;
            wp->d_order.d_whatOrder.add(WhatOrder::SPFormation);
         }
      }

      di.sendWhen(gameInfo()->battleData()->getTick());
#if 0
      // set time to issue order
      if(!delaySend)
         di.sendWhen(gameInfo()->battleData()->getTick());
      else
      {
         // if back row
         enum LUnitType {
            HQ,
            Inf,
            Cav,
            HrsArt,
            FtArt,

            LUnitType_HowMany
         } lunitType = (di.cp()->getRank().isHigher(Rank_Division)) ? HQ :
                       (di.cp()->generic()->isInfantry()) ? Inf :
                       (di.cp()->generic()->isCavalry()) ? Cav :
                       (di.cp()->generic()->isHorseArtillery()) ? HrsArt : FtArt;

         // Number of minutes to wait to issue order
         static int s_table[LUnitType_HowMany] = {
            15, 0, 12, 8, 0
         };

         int v = s_table[lunitType];
         if(v != 0)
            v = maximum(0, v + gameInfo()->random().gauss(4));

         di.sendWhen(gameInfo()->battleData()->getTick() + BattleMeasure::minutes(v));
      }
#endif
   }

   if(di.cp()->getRank().sameRank(Rank_Division))
   {
      if(divFor != di.order().divFormation())
      {
         di.order().divFormation(divFor);
         di.order().order().d_whatOrder.add(WhatOrder::CPFormation);
         shouldIssue = True;
      }

      if(!di.cp()->generic()->isArtillery() &&
         spFor != di.order().spFormation())
      {
         di.order().spFormation(spFor);
         di.order().order().d_whatOrder.add(WhatOrder::SPFormation);
         shouldIssue = True;
      }
   }

   if(facing != di.cp()->facing())
   {
      di.order().facing(facing);
      di.order().order().d_whatOrder.add(WhatOrder::Facing);
      shouldIssue = True;
   }

   if(di.cp()->aggression() != d_objective->aggression())
   {
      di.order().aggression(d_objective->aggression());
      shouldIssue = True;
   }

   if(di.cp()->posture() != d_objective->posture())
   {
      di.order().posture(d_objective->posture());
      shouldIssue = True;
   }


   di.ordered(!shouldIssue);

}

HexCord MidLevelPlanner::leftFront() const
{
   const MidUnitInfo& mi = d_units->front();
   return (mi.map().nFront() > 0) ? mi.map().item(0, 0).cp()->leftHex() : HexCord(0, 0);
}

HexCord MidLevelPlanner::rightFront() const
{
   const MidUnitInfo& mi = d_units->front();
   return (mi.map().nFront() > 0) ? mi.map().item(mi.map().nFront() - 1, 0).cp()->rightHex() : HexCord(0, 0);
}

bool MidLevelPlanner::doNewObjective()
{
#ifdef DEBUG
   gameInfo()->log("Processing new Objective");
#endif

   if(d_units->size() <= 0)
      return True;

//   const MidUnitInfo& miRef = (*d_objective->units().begin());
//   MidUnitInfo* mi = const_cast<MidUnitInfo*>(&miRef);
   MidUnitInfo& mi = d_units->front();
   if(mi.cp()->hasQuitBattle())
      return True;

   d_objective->getNewObjective(False);
   if(checkForObjectiveAchieved())
   {
      onObjectiveAchieved();
      return False;
   }

   enum { Left, Right };
   d_oldFlags = d_flags;
   d_flags = 0;
   if(d_oldFlags & NoCheckAlignment)
      d_flags |= NoCheckAlignment;
   if(d_oldFlags & FinishedBombard)
      d_flags |= FinishedBombard;

   d_bombardTime = 0;
   d_objective->reorienting(False);
#if 0
   setFlag(ObjectiveAchieved, False);
   setFlag(Reacting, False);
   setFlag(Retreating, False);
   setFlag(
#endif
   // first, update orders for all XX's
   MidUnitDeployMap& map = mi.map();
   for(int r = 0; r < 2; r++)
   {
      int nCols = (r == 0) ? map.nFront() : map.nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di = map.item(c, r);
         if(di.cp()->hasQuitBattle())
            continue;
         di.updateOrder();
         di.ordered(True);
         di.attacking(False);
         di.bombarding(False);
         di.changingSPFormation(False);
         di.changingDivFormation(False);
      }
   }

   // if reinforcement, adjust objective boundries for unit size
#if 0
   if(d_objective->reinforcement())
   {
      const HexArea& area = d_objective->area();
      int nCols = map.nFront();
      int frontage = 0;
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di = map.item(c, 0);
         if(di.cp()->hasQuitBattle())
            continue;

         int rows = 0;
         int cols = 0;
         BobUtility::getRowsAndColoumns(CPF_Deployed, spCount, cols, rows);
         frontage += cols;
      }

      frontage += (2 * maximum(0, (nCols - 1)));
      int objFrontage = WG_BattleAI_Util::getDist(area[0], area[1]);

      if(frontage > objFrontage)
      {
         int moveHowMany = (frontage - objFrontage) / 2;
         HexAreaList& areaList = d_objectives->areaList();
         for(HexAreaList::iterator ha = areaList.begin();
             ha != areaList.end();
             ++ha)
         {
            HexArea& a = (*ha);
            if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(),
         }
      }
   }
#endif

   // Now calculate new orders
   const HexArea& area = d_objective->area();
   const HexPosition::Facing facing = WG_BattleAI_Utils::frontFacing(area[0], area[1]);

   // get composition
   CRefBattleCP cp = mi.cp();
   UBYTE nInfXX    = 0;
   UBYTE nCavXX    = 0;
   UBYTE nArtXX    = 0;
   int   totalXXSP = 0;
   corpsTypes(gameInfo()->battleData(),
         cp, nInfXX, nCavXX, nArtXX, totalXXSP, False);

   UBYTE nTotalXX = nInfXX + nCavXX + nArtXX;

   CorpDeployment cd;
   if(!corpDeployment(cp, nInfXX, nCavXX, nArtXX, cd, gameInfo()->random(), True))
   {
      FORCEASSERT("Corp deployment not found");
      return False;
   }
   ASSERT(cd.nFront() > 0);

   // go through each unit in corps and assign deploy position
   BasicUnitType::value bt = (nInfXX > 0) ? BasicUnitType::Infantry :
                           (nCavXX > 0) ? BasicUnitType::Cavalry : BasicUnitType::Artillery;

   // first do the front row
   CPFormation divFor = (d_objective->mode() == HiObjective::FrontLine || d_objective->areaList().size() == 1) ?
      CPF_Deployed : CPF_Last;

   SPFormation spFor = (d_objective->mode() == HiObjective::FrontLine || d_objective->areaList().size() == 1) ?
      SP_ColumnFormation : SP_UnknownFormation;

#ifdef DEBUG
   if(lstrcmpi("II Corps", cp->getName()) == 0)
   {
      int i = 0;
   }
#endif
   assignDivisionalPositions(&mi, cd, CorpDeployment::Front, area[Left], area[Right],
         facing, divFor, spFor, bt);

   // Now do back row
   if(cd.nRear() > 0)
   {
      assignDivisionalPositions(&mi, cd, CorpDeployment::Rear, area[Left], area[Right],
                           facing, CPF_Massed, SP_MarchFormation, bt);
   }

//   d_objective->reinforcement(False);
   return True;
}

bool MidLevelPlanner::checkForObjectiveAchieved()
{
   if(d_units->size() <= 0)
      return True;

//   const MidUnitInfo& miRef = (*d_objective->units().begin());
//   MidUnitInfo* mi = const_cast<MidUnitInfo*>(&miRef);
   MidUnitInfo& mi = d_units->front();

   if(mi.cp()->hasQuitBattle())
      return True;

   enum { Left, Right };
   const HexArea& area = d_objective->area();
//   const HexPosition::Facing facing = WG_BattleAI_Utils::frontFacing(area[Left], area[Right]);

   MidUnitDeployMap& map = mi.map();
   bool reachedObjectiveLine = True;

   for(int c = 0; c < map.nFront(); c++)
   {
      MU_DeployItem& di = map.item(c, 0);
      if(di.cp()->hasQuitBattle())
         continue;

      // if enemy is within 4 hexes, and we are reacting
      // we have not reached objective
      if( //(di.cp()->targetList().closeRange() <= (4 * BattleMeasure::XYardsPerHex) && d_flags & Reacting) ||
          (shouldMove(di, area)) )
      {
         reachedObjectiveLine = False;
         break;
      }
   }

#ifdef DEBUG
   if(reachedObjectiveLine)
   {
      gameInfo()->log("%s has reached its objective. %d more objectives to go",
         mi.cp()->getName(), d_objective->nBoundries() - 1);
   }
#endif
   return reachedObjectiveLine;
}

bool MidLevelPlanner::shouldMove(const MU_DeployItem& di, const HexArea& area)
{
   if(di.cp()->leftHex() == di.objHex())
      return False;

   enum { Left, Right };
   HexPosition::Facing objFace = WG_BattleAI_Utils::frontFacing(area[0], area[1]);
   HexCord leftHex = di.cp()->leftHex();
   WG_BattleAI_Utils::AngleType at = (WG_BattleAI_Utils::getDist(leftHex, area[Left]) <= 2 || WG_BattleAI_Utils::getDist(leftHex, area[Right]) <= 2) ?
      WG_BattleAI_Utils::NormalAngle : WG_BattleAI_Utils::WideAngle;

   WG_BattleAI_Utils::MoveOrientation moL = WG_BattleAI_Utils::moveOrientation(objFace/*di.cp()->facing()*/, leftHex, area[Left], at);//WG_BattleAI_Utils::ObjectiveAngle);
   WG_BattleAI_Utils::MoveOrientation moR = WG_BattleAI_Utils::moveOrientation(objFace/*di.cp()->facing()*/, leftHex, area[Right], at);//WG_BattleAI_Utils::ObjectiveAngle);

   if(moL == WG_BattleAI_Utils::MO_Front &&
      moR == WG_BattleAI_Utils::MO_Front)
   {
      return True;
   }

   return False;
}

bool MidLevelPlanner::settingAround()
{
#if 0
   if(d_units->size() <= 0)
      return True;

//   const MidUnitInfo& miRef = (*d_objective->units().begin());
//   MidUnitInfo* mi = const_cast<MidUnitInfo*>(&miRef);
   MidUnitInfo& mi = d_units->front();
   if(mi.cp()->hasQuitBattle())
      return True;

   MidUnitDeployMap& map = mi.map();
   bool reachedObjectiveLine = True;

   enum { Left, Right };
   const HexArea& area = d_objective->area();
   const HexPosition::Facing facing = WG_BattleAI_Utils::frontFacing(area[Left], area[Right]);

   for(int c = 0; c < map.nFront(); c++)
   {
      MU_DeployItem& di = map.item(c, 0);
      if(di.cp()->hasQuitBattle())
         continue;

      if(!di.cp()->moving() &&
         !di.cp()->inCombat() &&
         di.objHex() !=
         shouldMove(di, area))
      {
         return True;
      }
   }
#endif
   return False;
}

bool MidLevelPlanner::evaluateObj()
{
    bool orderChanged = False;
    MidLevel_Op mlops(gameInfo(), this);

    d_oldFlags = d_flags;
    d_flags = 0;
    // Checks...
    // enemy nearby
    mlops.checkForEnemyToFront();

#if 0
    // see if we should move up rear units
    mlops.moveUpRear();

    // do reactions...
    if( (d_flags & ConsiderRetreat) )
       mlops.considerRetreat();
//  else if( (d_flags & EnemyToFront) )
//     mlops.considerHold();
#endif
    if( (d_flags & EnemyToFront) ) //&& !(oldFlags & EnemyToFront) )
    {
         mlops.considerHold();
         mlops.doEnemyToFront();
    }
    return (d_flags != d_oldFlags);
}


#ifdef DEBUG
const char* HiObjective::modeString() const
{
    static char* modeStrings[] = {
         "Frontline",
         "Reserves"
    };

    ASSERT(mode() >= 0);
    ASSERT(mode() < ModeCount);

    if( (mode() >= 0) && (mode() < ModeCount) )
         return modeStrings[mode()];
    else
         return "Illegal";
}

void MidLevelPlanner::logObjective()
{
   gameInfo()->stream() << d_objective->modeString() << " objective=" << d_objective->area();
   gameInfo()->logStream();
   gameInfo()->stream() << "Units=" << d_objective->units().size();
   gameInfo()->logStream();
}
#endif

static UWORD s_version = 0x0004;
bool MidLevelPlanner::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   OrderBattle* ob = const_cast<OrderBattle*>(gameInfo()->battleData()->ob()->ob());
   d_units->readData(f, ob);
   d_targets.readData(f, ob);
   if(version >= 0x0004)
   {
      f >> d_flags;
      f >> d_oldFlags;
   }
   else
   {
      UBYTE b;
      f >> b;
      d_flags = b;
      f >> b;
      d_oldFlags = b;
   }

   if(version >= 0x0001)
      d_cincDI.readData(f, ob);

   if(version >= 0x0002)
      f >> d_nextReinforcementCheck;

   if(version >= 0x0003)
      f >> d_bombardTime;

   // A bodge
   MidLevel_Op mlops(gameInfo(), this);
   mlops.updateFrontLine();

   return f.isOK();
}

bool MidLevelPlanner::writeData(FileWriter& f) const
{
   f << s_version;
   OrderBattle* ob = const_cast<OrderBattle*>(gameInfo()->battleData()->ob()->ob());
   d_units->writeData(f, ob);
   d_targets.writeData(f, ob);
   f << d_flags;
   f << d_oldFlags;
   d_cincDI.writeData(f, ob);
   f << d_nextReinforcementCheck;
   f << d_bombardTime;
   return f.isOK();
}

#ifdef DEBUG
void MidLevelPlanner::logHistorical()
{
   MidUnitInfo& mi = d_units->front();

   gameInfo()->log("------------------ MidLevelPlanner for %s", mi.cp()->getName());
   gameInfo()->log("---------------------------------------------------");

   for(int r = 0; r < 2; r++)
   {
      int nCols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di = mi.map().item(c, r);
         gameInfo()->log("-------- %s is in %s", di.cp()->getName(), (r == 0) ? "Front" : "Rear");
      }
   }
}
#endif
};  // namespace WG_BattleAI_Internal

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2003/02/08 19:38:51  greenius
 * Replaced use of basic_string::clear() with erase so that it will
 * compile with MSVC version 6.
 *
 * Fixed bad use of stringstream buffer in Battle AI logStream().
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
