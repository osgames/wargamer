/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
/*
 * Battle AI Main Class
 *
 * The Battlegame holds an instance of one of these
 */

#include "gamedefs.hpp"
//#include "string.hpp"

#if defined(NO_WG_DLL)
    #define BATAI_DLL
#elif defined(EXPORT_BATAI_DLL)
    #define BATAI_DLL __declspec(dllexport)
#else
    #define BATAI_DLL __declspec(dllimport)
#endif



class BattleData;
class BattleMessageInfo;
// class String;
class FileReader;
class FileWriter;

namespace WG_BattleAI_Internal { class BattleAIImp; };
using WG_BattleAI_Internal::BattleAIImp;

class BattleAI
{
		// Disable Copying
		BattleAI(const BattleAI&);
		BattleAI& operator = (const BattleAI&);

     public:

        BATAI_DLL BattleAI(const BattleData* batData);
        BATAI_DLL BattleAI(const BattleData* batData, String& title);
        BATAI_DLL ~BattleAI();

        BATAI_DLL void add(Side s);
        BATAI_DLL void remove(Side s);

        BATAI_DLL void kill();
            // Remove all AIs

        BATAI_DLL void sendMessage(const BattleMessageInfo& msg);

        BATAI_DLL void doDeploy();
        BATAI_DLL void doDeploy(Side s);
            // Do Deployment

        BATAI_DLL void start();
          // Start the game

		BATAI_DLL void endDay();	// pause AI at and of day
		BATAI_DLL void newDay();	// Restart for new day

        bool has(Side s) const;

        BATAI_DLL bool convertScript(const char* battleName);

        // File interface
        BATAI_DLL bool readData(FileReader& f);
        BATAI_DLL bool writeData(FileWriter& f) const;

     private:
        BattleAIImp* d_imp;

};
