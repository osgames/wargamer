/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "aib_util.hpp"
#include "bob_cp.hpp"
#include "bobiter.hpp"
#include "hexmap.hpp"
#include "pdeploy.hpp"
#include "fsalloc.hpp"
#include "b_tables.hpp"
#include "bobutil.hpp"
using BattleMeasure::HexPosition;

namespace WG_BattleAI_Utils
{
const int chunkSize = 20;

#ifdef DEBUG
static FixedSize_Allocator itemAlloc(sizeof(LocalCP), chunkSize, "LocalCP");
#else
static FixedSize_Allocator itemAlloc(sizeof(LocalCP), chunkSize);
#endif

void* LocalCP::operator new(size_t size)
{
   return itemAlloc.alloc(sizeof(LocalCP));
}

#ifdef _MSC_VER
void LocalCP::operator delete(void* deadObject)
{
   itemAlloc.free(deadObject, sizeof(LocalCP));
}
#else
void LocalCP::operator delete(void* deadObject, size_t size)
{
    ASSERT(size == sizeof(LocalCP));
   itemAlloc.free(deadObject, size);
}
#endif


GoWhichWay whichWay(HexPosition::Facing cf, HexPosition::Facing wf)
{
   ASSERT(wf != cf);
   const UBYTE rFaceBase = 128;
   int dif = cf - rFaceBase;

   ASSERT((wf - dif) >= 0 && (wf - dif) < 256);
    wf = wf - dif;

  return (wf < rFaceBase) ? GoRight : (wf > rFaceBase) ? GoLeft : GoNone;
}

HexPosition::Facing frontFacing(const HexCord& leftHex, const HexCord& rightHex)
   // Which direction does the front face? (as defined by the line leftHex - rightHex)
{
   SWORD x = rightHex.x() - leftHex.x();
   SWORD y = rightHex.y() - leftHex.y();

   // get byte angle
   Bangle bFace = wangleToBangle(direction(x, y));

   return (bFace <= 32 || bFace >= 233) ? HexPosition::North :
             (bFace <= 64 ) ? HexPosition::NorthWestPoint :
             (bFace <= 96 ) ? HexPosition::SouthWestPoint :
                   (bFace <= 160) ? HexPosition::South :
             (bFace <= 192) ? HexPosition::SouthEastPoint : HexPosition::NorthEastPoint;

}

bool moveHex(RCPBattleData bd, HexCord::HexDirection hd, int howMany, const HexCord& startHex, HexCord& hex)
{
   ASSERT(howMany >= 0);

   hex = startHex;
   while(howMany--)
   {
      HexCord tHex;
         if(!bd->moveHex(hex, hd, tHex))
      {
         //FORCEASSERT("Hex not found");
         return False;
      }
      else
         hex = tHex;
   }

   return True;
}

HexPosition::Facing closeFace(HexPosition::Facing f)
{
   return (f < 42 && f >= 0) ? HexPosition::NorthEastPoint :
             (f <= 86)          ? HexPosition::North :
             (f <= 128)         ? HexPosition::NorthWestPoint :
             (f < 170)          ? HexPosition::SouthWestPoint :
             (f <= 213)         ? HexPosition::South : HexPosition::SouthEastPoint;

}

bool shouldBeVisible(RCPBattleData bd, const CRefBattleCP& cp, const CRefBattleSP& sp)
{
   const UnitTypeItem& uti = bd->ob()->getUnitType(sp);

   switch(uti.getBasicType())
   {
      case BasicUnitType::Infantry:
             return (cp->generic()->isInfantry());

      case BasicUnitType::Artillery:
         return (cp->generic()->isArtillery());

      case BasicUnitType::Cavalry:
         return (cp->generic()->isCavalry());
   }

   return False;
}

int countSP(RCPBattleData bd, const CRefBattleCP& cp, bool deploying)
{
   if(deploying)
   {
      int count = 0;
      CRefBattleSP sp = cp->sp();
      while(sp != NoBattleSP)
      {
         if(shouldBeVisible(bd, cp, sp))
            count++;

         sp = sp->sister();
      }

      return count;
   }
   else
         return cp->spCount();
}

bool moveHex(
   RCPBattleData bd,
   HexPosition::Facing facing,
   int howManyLR,
   int howManyFB,
   const HexCord& startHex,
   HexCord& hex)
{
   HexCord wHex = startHex;

   // first, go forward or back
   if(howManyFB != 0)
   {
      enum GoHow { Left, Right } goHow = Left;
      int howMany = (howManyFB > 0) ? howManyFB : -howManyFB;

      while(howMany--)
      {
         HexCord::HexDirection hd;
         if(howManyFB > 0)
            hd = (goHow == Left) ? BattleMeasure::leftFront(facing) : BattleMeasure::rightFront(facing);
         else
            hd = (goHow == Left) ? BattleMeasure::leftRear(facing) : BattleMeasure::rightRear(facing);

         HexCord tHex;
         if(!bd->moveHex(wHex, hd, tHex))
                  break;
         else
            wHex = tHex;

         goHow = (goHow == Left) ? Right : Left;
      }
   }


   // do left right
   if(howManyLR != 0)
   {
         HexCord::HexDirection hd;
      hd = (howManyLR > 0) ? BattleMeasure::rightFlank(facing) : BattleMeasure::leftFlank(facing);

         int howMany = (howManyLR > 0) ? howManyLR : -howManyLR;
         while(howMany--)
         {
             HexCord tHex;
             if(!bd->moveHex(wHex, hd, tHex))
                  break;
             else
                  wHex = tHex;
         }
    }

    hex = wHex;
    return True;
}

bool needsOrder(const CRefBattleCP& cp)
{
   if(cp->getRank().sameRank(Rank_Division))
   {
      if(cp->getCurrentOrder().wayPoints().entries() > 0)
         return False;

         if(cp->inCombat())
             return False;

      bool stillMoving = False;
         for(std::vector<DeployItem>::const_iterator di = cp->mapBegin();
            di != cp->mapEnd();
            ++di)
      {
         if( (di->active()) &&
               (di->d_sp->isMoving() || di->d_sp->routeList().entries() > 0) )
         {
            stillMoving = True;
            break;
         }
      }

      return !(stillMoving);
   }

   else
   {
         return !(cp->routeList().entries() > 0 ||
                   cp->getCurrentOrder().wayPoints().entries() > 0);

   }
}

HexPosition::Facing averageFacing(const CRefBattleCP& cp)
{
   ULONG tFace = 0;
   int nUnits = 0;
   for(ConstBattleUnitIter uIter(cp); !uIter.isFinished(); uIter.next())
   {
         if(uIter.cp()->getRank().sameRank(Rank_Division))
      {
         tFace += uIter.cp()->facing();
         nUnits++;
      }
   }

   if(nUnits > 0)
   {
      return closeFace(static_cast<HexPosition::Facing>(tFace / nUnits));
   }
   else
      return cp->facing();
}

HexPosition::Facing averageFacing(RCPBattleData bd, Side s)
{
   ULONG tFace = 0;
   int nUnits = 0;
   for(ConstBattleUnitIter uIter(bd->ob(), s); !uIter.isFinished(); uIter.next())
   {
      if(uIter.cp()->getRank().sameRank(Rank_Division))
      {
         tFace += uIter.cp()->facing();
         nUnits++;
      }
   }

   if(nUnits > 0)
   {
      return closeFace(static_cast<HexPosition::Facing>(tFace / nUnits));
   }
   else
      return HexPosition::North;
}

#if 0
bool unitsOnMap(RCPBattleData bd, Side s)
{
   CRefBattleCP cp = bd->ob()->getTop(s);
   if(cp->getRank().sameRank(Rank_Division))
   {
   }
   else
      return const_cast<BattleData*>(reinterpret_cast<const BattleData*>(bd))->hexMap()->isUnitPresent();
}
#endif

inline int abValue(int v)
{
    return (v >= 0) ? v : -v;
}

bool unitIsFacing(HexPosition::Facing facing, const CRefBattleCP& cp2)
// Is unit facing 'facing'? 
{
   HexPosition::Facing bFace = cp2->facing();

// if(facing == bFace)
//    return False;
   const HexPosition::Facing base = 64;
   int dif = base - facing;

   if(bFace + dif >= HexPosition::FacingResolution || bFace + dif < 0)
   {
      bFace = (bFace + dif >= HexPosition::FacingResolution) ?
                  (bFace + dif) - HexPosition::FacingResolution :
                  HexPosition::FacingResolution + (bFace + dif);
   }
   else
      bFace += dif;

   return (bFace >= 128);
}

bool unitsAreFacing(const CRefBattleCP& cp1, const CRefBattleCP& cp2)
{
   return unitIsFacing(cp1->facing(), cp2);
}

// in what general orientation are we moving?
// i.e. either Front, Left, Right, or Rear
MoveOrientation moveOrientation(
   const HexPosition::Facing facing,
   const HexCord& srcHex,
   const HexCord& destHex,
   AngleType type)
{
   if(srcHex == destHex)
   {
      return (type == ObjectiveAngle) ? MO_Rear : MO_Front;
   }

   SWORD x = destHex.x() - srcHex.x();
   SWORD y = destHex.y() - srcHex.y();

   // get byte angle
   Bangle bFace = wangleToBangle(direction(x, y));

   const HexPosition::Facing base = 64;
   int dif = base - facing;

   if(bFace + dif >= HexPosition::FacingResolution || bFace + dif < 0)
   {
      bFace = (bFace + dif >= HexPosition::FacingResolution) ?
               (bFace + dif) - HexPosition::FacingResolution :
               HexPosition::FacingResolution + (bFace + dif);
   }
   else
      bFace += dif;

   switch(type)
   {
      case NormalAngle:
      {
         if( (bFace < 32 || bFace > 224) )
            return MO_Right;
         else if(bFace <= 96)
            return MO_Front;
         else if(bFace < 160)
            return MO_Left;
         else
            return MO_Rear;
      }
      case ShortAngle:
      {
         if( (bFace < 59 || bFace > 197) )
            return MO_Right;
         else if(bFace <= 69)
            return MO_Front;
         else if(bFace < 188)
            return MO_Left;
         else
            return MO_Rear;
      }
      case LessExtremeShortAngle:
      {
         if( (bFace < 44 || bFace > 212) )
            return MO_Right;
         else if(bFace <= 84)
            return MO_Front;
         else if(bFace < 172)
            return MO_Left;
         else
            return MO_Rear;
      }
      case WideAngle:
      {
         if( (bFace < 15 || bFace > 240) )
            return MO_Right;
         else if(bFace <= 113)
            return MO_Front;
         else if(bFace < 143)
            return MO_Left;
         else
            return MO_Rear;
      }
      case ObjectiveAngle:
      {
         return ( (bFace < 128 && bFace > 0) ) ? MO_Front : MO_Rear;
      }
      case BoundryAngle:
      {
         return ( (bFace <= 64 && bFace > 192) ) ? MO_Right : MO_Left;
      }

      default:
      {
         FORCEASSERT("Improper type in moveOrientation()");
         return MO_Front;
      }
   }

}

Hi_OpPlan::Plan selectPlan(
//  BAI_StrObjList& obl,
//  HexCord mapSize,
    GameInfo* gi,
    int nCorps,
    int& nZones)
//  RandomNumber& lRand,
//  BattleOrderInfo::Posture pos)
{
// int nCorps = d_units->corpsCount();
    if(nZones == 0)
         nZones = maximum(1, minimum((nCorps > 2) ? ((4 * nCorps) + 3) / 6 : nCorps, 3));

    ASSERT( (nZones >= 1) && (nZones <= 3) );

// RandomNumber& lRand = gameInfo()->random();
    Hi_OpPlan::Plan plan = Hi_OpPlan::Plan_None;
    BattleOrderInfo::Posture pos = gi->cinc()->posture();

    int start = (pos == BattleOrderInfo::Offensive) ? Hi_OpPlan::Plan_FirstOffensive : Hi_OpPlan::Plan_FirstDefensive;
    int end = (pos == BattleOrderInfo::Offensive) ? Hi_OpPlan::Plan_LastOffensive : Hi_OpPlan::Plan_LastDefensive;

#ifdef DEBUG
    int loop = 0;
#endif
    while(plan == Hi_OpPlan::Plan_None)
    {
#ifdef DEBUG
         if(++loop >= 1000)
         {
             FORCEASSERT("Infinite loop in selectPlan()");
             break;
         }
#endif
         // For now, randomly select a plan
         Hi_OpPlan::Plan p2 = static_cast<Hi_OpPlan::Plan>(gi->random(start, end));

         // Se if we can use it
         if(Hi_OpPlan::canIssue(gi, p2, nZones))
             plan = p2;
    }
#ifdef DEBUG
    gi->log("Plan - %s selected", Hi_OpPlan::planName(plan));
#endif
    gi->plan(plan);
    return plan;
}

void getCorpLeftRightBoundry(
         const CRefBattleCP& cp,
         HexPosition::Facing avgFace,
         HexCord& leftHex,
         HexCord& rightHex,
         LocalCPList* cpList)
{
    ASSERT(cp->getRank().sameRank(Rank_Corps) ||
             (cp->getRank().sameRank(Rank_Division) && (cp->parent() == NoBattleCP || cp->parent()->getRank().isHigher(Rank_Corps))));

    CRefBattleCP leftCP = NoBattleCP;
    CRefBattleCP rightCP = NoBattleCP;
    LocalCPList llist;
    LocalCPList rlist;
    for(BattleUnitIter iter(const_cast<BattleCP*>(cp)); !iter.isFinished(); iter.next())
    {
         if(iter.cp()->getRank().sameRank(Rank_Division))
         {
             if(leftCP == NoBattleCP)
             {
                  leftCP = iter.cp();
                  rightCP = iter.cp();
                  llist.add(leftCP);
                  rlist.add(rightCP);
             }
             else
             {
                  HexCord oldLHex = const_cast<BattleCP*>(leftCP)->leftHex();
                  HexCord oldRHex = const_cast<BattleCP*>(rightCP)->rightHex();
                  MoveOrientation moL = moveOrientation(avgFace, oldLHex, const_cast<BattleCP*>(iter.cp())->leftHex(), WideAngle);//, False);
                  MoveOrientation moR = moveOrientation(avgFace, oldRHex, const_cast<BattleCP*>(iter.cp())->rightHex(), WideAngle);//, False);

                  // left side
                  if(moL == WG_BattleAI_Utils::MO_Front ||
                     moL == WG_BattleAI_Utils::MO_Left)
                  {
                      leftCP = iter.cp();
#if 1
                      if(moL == MO_Front)
                      {
                           llist.reset();
                      }
                      llist.addInsert(leftCP);
#endif
                  }
#if 1
                  else if(moL == WG_BattleAI_Utils::MO_Right)
                  {
                     llist.add(iter.cp());
                  }
#endif
                  // right side
                  if(moR == WG_BattleAI_Utils::MO_Front || moR == WG_BattleAI_Utils::MO_Right)
                  {
                      rightCP = iter.cp();
#if 1
                      if(moR == MO_Front)
                      {
                           rlist.reset();
                      }
                      rlist.add(rightCP);
#endif
                  }
#if 1
                  else if(moR == WG_BattleAI_Utils::MO_Left)
                  {
                     rlist.add(iter.cp());
                  }
#endif
             }
         }
    }

    ASSERT(leftCP != NoBattleCP);
    ASSERT(rightCP != NoBattleCP);

    leftHex = const_cast<BattleCP*>(leftCP)->leftHex();
    rightHex = const_cast<BattleCP*>(rightCP)->rightHex();

    if(cpList)
    {
         // add to CP list from left to right
         cpList->add(leftCP);
         if(leftCP != rightCP)
         {
            LocalCPList list;
            list = llist;
            list += rlist;

            list.removeCP(leftCP);

//            CRefBattleCP lastCP = leftCP;
            while(list.entries() > 0)
            {
               SListIter<LocalCP> iter(&list);
               CRefBattleCP nextLeftCP = NoBattleCP;
               while(++iter)
               {
                  if(nextLeftCP == NoBattleCP)
                  {
                     nextLeftCP = iter.current()->d_cp;
                  }
                  else
                  {
                     HexCord oldLHex = const_cast<BattleCP*>(nextLeftCP)->leftHex();
                     MoveOrientation moL = moveOrientation(avgFace, oldLHex, const_cast<BattleCP*>(iter.current()->d_cp)->leftHex(), ShortAngle);//, False);
                     if(moL != WG_BattleAI_Utils::MO_Right)
                     {
                        nextLeftCP = iter.current()->d_cp;
                     }
                  }
               }

               ASSERT(nextLeftCP != NoBattleCP);
               list.removeCP(nextLeftCP);
               cpList->add(nextLeftCP);
            }
         }
//       *cpList = llist;
//       *cpList += rlist;
    }
}

bool findDeployPosition(
         RCPBattleData bd,
         const CRefBattleCP& cp,
         HexPosition::Facing facing,
         HexCord& hex,
         bool isDeploying)
{
//    BattleData* bdnc = const_cast<BattleData*>(reinterpret_cast<const BattleData*>(bd));
    const BattleData* bdc = bd;
    BattleData* bdnc = const_cast<BattleData*>(bdc);

    HexCord bLeft;
    HexCord mSize;
    bdnc->getPlayingArea(&bLeft, &mSize);
    HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

    const int nDirections = 8;
    static const POINT p[nDirections] = {
      {  1,  0 },  // E
      {  1,  1 },  // NE
      {  0,  1 },  // N
      { -1,  1 },  // NW
      { -1,  0 },  // W
      { -1, -1 },  // SW
      {  0, -1 },  // S
      {  1, -1 },  // SE
   };

    enum LFacings {
      NE, N, NW, SW, S, SE, LFacings_HowMany
   } lFacing = (facing == HexPosition::NorthEastPoint) ? NE :
                     (facing == HexPosition::North)          ? N :
                     (facing == HexPosition::NorthWestPoint) ? NW :
                     (facing == HexPosition::SouthWestPoint) ? SW :
                     (facing == HexPosition::South)          ? S : SE;

   static const UBYTE s_canMoveThisWay[LFacings_HowMany][nDirections] = {
      {  False, False, False, True,  True,  True,  True,  True  }, // NE
      {  True,  False, False, False, True,  True,  True,  True  }, // N
      {  True,  True,  False, False, False, True,  True,  True  }, // NW
      {  True,  True,  True,  False, False, False, True,  True  }, // SW
      {  True,  True,  True,  True,  False, False, False, True  }, // S
      {  True,  True,  True,  True,  True,  False, False, False }, // SE
   };

   for(int i = 0; i < nDirections; i++)
   {
      bool found = True;
      HexCord tHex = hex;

      int loop = 0;
//      while(!PlayerDeploy::playerDeploy(bdnc, const_cast<BattleCP*>(cp), tHex, 0, (isDeploying) ? PlayerDeploy::Initializing : PlayerDeploy::None))
      while(!PlayerDeploy::playerDeploy(bdnc, const_cast<BattleCP*>(cp), tHex, 0, (isDeploying) ? PlayerDeploy::INITIALIZE : PlayerDeploy::TERRAINONLY))
      {
         if(!s_canMoveThisWay[lFacing][i])
         {
            found = False;
            break;
         }

         if(++loop >= 1000)
         {
            FORCEASSERT("Infinite loop in assignDeployPosition()");
            break;
         }


         int newX = tHex.x() + p[i].x;
         int newY = tHex.y() + p[i].y;

         if(newX < bLeft.x() ||
            newX >= tRight.x() ||
            newY < bLeft.y() ||
            newY >= tRight.y())
         {
            found = False;
            break;
         }

         found = True;
         tHex.x(static_cast<UBYTE>(newX));
         tHex.y(static_cast<UBYTE>(newY));
      }

      if(found)
      {
         hex = tHex;
         return True;
      }
   }

   return False;
}

int getRange(RPBattleData bd, const CRefBattleCP& cp, bool current)
{
   ASSERT(cp->sp() != NoBattleSP);

   /*
   * Find sp type to access range
   * Use type that provides greates range
   * i.e. If this is an Inf XX with 5 inf and 2 Heavy Art SP, then range would be
   * for the artillery
    */

   const Table1D<UWORD>& table = BattleTables::fireCombatRange();
   UWORD bestRange = 0;

   CRefBattleSP sp = cp->sp();
   while(sp != NoBattleSP)
   {
      const UnitTypeItem& uti = bd->ob()->getUnitType(sp);
      if(current && !cp->holding() && uti.getBasicType() == BasicUnitType::Artillery)
      {
         sp = sp->sister();
         continue;
      }

      // convert type to local enum
         enum {
         Inf,
         Cav,
         FootArt,
         HvyArt,
         HorseArt,
         Type_HowMany
      } type = (uti.getBasicType() == BasicUnitType::Infantry) ? Inf :
                   (uti.getBasicType() == BasicUnitType::Cavalry || uti.getBasicType() == BasicUnitType::Special) ? Cav :
                   (uti.isMounted()) ? HorseArt : FootArt; // TODO: Heavy Arty

      int range = table.getValue(type);
      if(range > bestRange)
             bestRange = range;

      sp = sp->sister();
   }

   return bestRange;
}

void setMoveOrder(
    BattleOrder* batOrder,
    const HexCord& tHex,
    const HexPosition::Facing facing)
{
    BattleOrderInfo* order = (batOrder->nextWayPoint()) ?
             &batOrder->wayPoints().getLast()->d_order : &batOrder->order();

    order->d_mode = BattleOrderInfo::Move;
//  order->d_whatOrder.add(WhatOrder::Facing);

    WayPoint* wp = new WayPoint(tHex, *order);
    ASSERT(wp);
#if 0
    if(facing != order->d_facing)
    {
       wp->d_order.d_whatOrder.reset();
       wp->d_order.d_facing = facing;
       wp->d_order.d_whatOrder.add(WhatOrder::Facing);
    }
#endif
    batOrder->wayPoints().append(wp);
}

// get the distance from unit to hex
// returns 0 if hex is occupied by cp (or one of its SPs)
int distanceFromUnit(const HexCord& hex, const RefBattleCP& cp)
{
   if(cp->getRank().isHigher(Rank_Division))
      return getDist(cp->hex(), hex);

   // get dist to closest SP
   int bestDist = UWORD_MAX;
   for(std::vector<DeployItem>::iterator di = cp->mapBegin();
      di != cp->mapEnd();
      di++)
   {
      if(di->active())
      {
         if(di->d_sp->hex() == hex)
         {
            bestDist = 0;
            break;
         }

         int dist = getDist(di->d_sp->hex(), hex);
         if(dist < bestDist)
            bestDist = dist;
      }
   }

   return bestDist;
}
bool hasGuard(const CRefBattleCP& cp)
{
   for(ConstBattleUnitIter iter(cp);
       !iter.isFinished();
       iter.next())
   {
      if(iter.cp()->getRank().sameRank(Rank_Division) &&
          (iter.cp()->generic()->isGuard() ||
           iter.cp()->generic()->isOldGuard()) )
      {
         return True;
      }
   }

   return False;
}

#ifdef DEBUG
const char* strRatioText(StrRatio r)
{
   static const char* s_text[Str_HowMany] = {
      "3 to 1",
      "2 to 1",
      "1.5 to 1",
      "1.2 to 1",
      "Equal",
      "1 to 1.2",
      "1 to 1.5",
      "1 to 2",
      "1 to 3"
   };

   return (r < Str_HowMany) ? s_text[r] : "Out of range";
}
#endif

StrRatio getSideStrRatio(RCPBattleData bd, const CRefBattleCP& cinc)
{
//   CRefBattleCP enemyCinc = BobUtility::getCinc(const_cast<BattleData*>(reinterpret_cast<const BattleData*>(bd))->ob(), (cinc->getSide() == 0) ? 1 : 0);
   CRefBattleCP enemyCinc = BobUtility::getCinc(const_cast<BattleData*>(static_cast<const BattleData*>(bd))->ob(), (cinc->getSide() == 0) ? 1 : 0);
   if(enemyCinc == NoBattleCP)
   {
      FORCEASSERT("Oops! Enemy has no Cinc!");
      return Str3To1;
   }

   // Get side counts
   BobUtility::UnitCounts us;
   BobUtility::UnitCounts them;
   {
      CRefBattleCP cp = enemyCinc;
      while(cp != NoBattleCP)
      {
         BobUtility::unitsInManpower(cp, bd, them);
         cp = cp->sister();
      }
   }

   {
      CRefBattleCP cp = cinc;
      while(cp != NoBattleCP)
      {
         BobUtility::unitsInManpower(cp, bd, us);
         cp = cp->sister();
      }
   }

   int ourCount = us.d_nInf + us.d_nCav + us.d_nArtMP;
   int theirCount = them.d_nInf + them.d_nCav + them.d_nArtMP;

   if(ourCount >= theirCount * 3)
      return Str3To1;
   else if(ourCount >= theirCount * 2)
      return Str2To1;
   else if(ourCount >= theirCount * 1.5)
      return Str1_5To1;
   else if(ourCount >= theirCount * 1.2)
      return Str1_2To1;
   else if(theirCount >= ourCount * 3)
      return Str1To3;
   else if(theirCount >= ourCount * 2)
      return Str1To2;
   else if(theirCount >= ourCount * 1.5)
      return Str1To1_5;
   else if(theirCount >= ourCount * 1.2)
      return Str1To1_2;
   else
      return Str_Equal;
}


}; // end namespace WG_BattleAI_Utils

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2002/11/24 13:17:47  greenius
 * Added Tim Carne's editor changes including InitEditUnitTypeFlags.
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
