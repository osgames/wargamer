/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_SIDE_HPP
#define AIB_SIDE_HPP

#ifndef __cplusplus
#error aib_side.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle AI for a particular side
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "thread.hpp"
#include "sync.hpp"
#include "sllist.hpp"
#include "ai_panel.hpp"

#ifdef DEBUG
#define HAS_CONTROL_PANEL
#endif

class BattleData;
class BattleMessageInfo;
class AI_ControlPanel;
class FileReader;
class FileWriter;

namespace WG_BattleAI_Internal
{

class HighLevelPlanner;
class BattleAIData;
class BattleMessageQueue;


class BattleSideAI :
  public Greenius_System::Thread,
#ifdef HAS_CONTROL_PANEL
	public AI_ControlInterface,
#endif
	 public SLink
{
	 public:
        friend class BAI_ScriptReader;
		  BattleSideAI(const BattleData* batData);
        BattleSideAI(Side s, const BattleData* batData,  const char* batFile, FileWriter& f);
		  BattleSideAI(Side s, const BattleData* batData, String title);
		  ~BattleSideAI();

		  void doDeploy();
		  void start();
		  void sendMessage(const BattleMessageInfo& msg);
		  Side side() const;

			void endDay()		// Pause at end of day
			{
				pause(true);
			}

			void newDay();		// restart new day

        void setPlanner(HighLevelPlanner* p) { d_planner = p; }
#ifdef HAS_CONTROL_PANEL

		  /*
			* functions for AI_ControlInterface
			*/

		  virtual void pause(bool mode) { Thread::pause(mode); }
		  virtual void setRate(int ms) { d_timer.setRate(ms); }
		  virtual int getRate() const { return d_timer.getRate(); }
		  virtual int getMinRate() const { return s_minThinkSpeed; }
		  virtual int getMaxRate() const { return s_maxThinkSpeed; }
		  virtual String getTitle() const;
#endif

		  // file interface
		  bool readData(FileReader& f);
		  bool writeData(FileWriter& f) const;

	/*----------------------------------------
	 * Private Functions
	 */

	private:

		void run();
			// Run the AI thread
      // Unchecked
      void setAggPosture();
      // End
	 private:

		WaitableTimer d_timer;
		int d_rate;

		  #ifdef HAS_CONTROL_PANEL
				AI_ControlPanel d_panel;
		  #endif

        const BattleData*  d_batData;
		  BattleAIData* d_aiData;     // Data accessed by entire game
		  HighLevelPlanner* d_planner;
		  BattleMessageQueue* d_messages;

		  static int s_defaultThinkSpeed;
		  static int s_minThinkSpeed;
		  static int s_maxThinkSpeed;
};

};      // namespace BattleAI

#endif /* AIB_SIDE_HPP */


