/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hi Level Zone List
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "h_zlist.hpp"
#include "hexdata.hpp"
#include "aib_sobj.hpp"
#include "batdata.hpp"
#include "aib_util.hpp"
#include "b_tables.hpp"
#include "sllist.hpp"
#include "fsalloc.hpp"
#include "filebase.hpp"
namespace WG_BattleAI_Internal
{

void getSideLeftRightBoundry(
   RCPBattleData bd,
   ConstBattleUnitIter& iter,
   HexPosition::Facing avgFace,
   HexCord& leftHex,
   HexCord& rightHex,
   WG_BattleAI_Utils::LocalCPList* cpList = 0)// list of front line CPs
{
   CRefBattleCP leftCP = NoBattleCP;
   CRefBattleCP rightCP = NoBattleCP;
   WG_BattleAI_Utils::LocalCPList llist;
   WG_BattleAI_Utils::LocalCPList rlist;

   for(; !iter.isFinished(); iter.next())
   {
      if(iter.cp()->getRank().sameRank(Rank_Division))
      {
         if(leftCP == NoBattleCP)
         {
            leftCP = iter.cp();
            rightCP = iter.cp();
            if(cpList)
            {
               llist.add(leftCP);
               rlist.add(rightCP);
            }
         }
         else
         {
            HexCord oldLHex = const_cast<BattleCP*>(leftCP)->leftHex();
            HexCord oldRHex = const_cast<BattleCP*>(leftCP)->rightHex();
            WG_BattleAI_Utils::MoveOrientation moL = WG_BattleAI_Utils::moveOrientation(avgFace, oldLHex, const_cast<BattleCP*>(iter.cp())->leftHex());
            WG_BattleAI_Utils::MoveOrientation moR = WG_BattleAI_Utils::moveOrientation(avgFace, oldRHex, const_cast<BattleCP*>(iter.cp())->rightHex());

            // left side
            if(moL == WG_BattleAI_Utils::MO_Front || moL == WG_BattleAI_Utils::MO_Left)
            {
//             CRefBattleCP oldCP = leftCP;
               leftCP = iter.cp();

               if(cpList)
               {
                  // Clear out if moving up
                  if(moL == WG_BattleAI_Utils::MO_Front)
                     llist.reset();

                  llist.add(leftCP);
               }

            }

            // right side
            if(moR == WG_BattleAI_Utils::MO_Front || moR == WG_BattleAI_Utils::MO_Right)
            {
               rightCP = iter.cp();

               if(cpList)
               {
                  // Clear out if moving up
                  if(moR == WG_BattleAI_Utils::MO_Front)
                     rlist.reset();

                  rlist.add(rightCP);
               }
            }
         }
      }
   }

   if(cpList)
   {
      const int nLists = 2;
      WG_BattleAI_Utils::LocalCPList* list[nLists] = {
         &llist,
         &rlist
      };

      for(int i = 0; i < nLists; i++)
      {
         SListIterR<WG_BattleAI_Utils::LocalCP> iter(list[i]);
         while(++iter)
         {
            ASSERT(iter.current()->d_cp->getRank().sameRank(Rank_Division));
            CRefBattleCP addCP = (iter.current()->d_cp->parent() != NoBattleCP && iter.current()->d_cp->parent()->getRank().sameRank(Rank_Corps)) ?
                           iter.current()->d_cp->parent() : iter.current()->d_cp;
            cpList->add(addCP);
         }
      }

//    *cpList = llist;
//    *cpList += rlist;
   }

   ASSERT(leftCP != NoBattleCP);
   ASSERT(rightCP != NoBattleCP);
   HexCord::HexDirection hdL = BattleMeasure::leftFlank(avgFace);
   HexCord::HexDirection hdR = BattleMeasure::rightFlank(avgFace);

   // go left until we reach the end of the board
   leftHex = const_cast<BattleCP*>(leftCP)->leftHex();
   HexCord nextHex;
   while(bd->moveHex(leftHex, hdL, nextHex))
      leftHex = nextHex;

   // go right until we reach the end of the board
   rightHex = const_cast<BattleCP*>(rightCP)->rightHex();
   while(bd->moveHex(rightHex, hdR, nextHex))
      rightHex = nextHex;
}


inline bool riverHex(PathType pt)
{
   return (pt == LP_River);
}

HiZoneList::~HiZoneList()
{
      while(size())
      {
          HiZone* zone = back();
          pop_back();
          delete zone;
      }
}

HiZone* HiZoneList::createZone(
   HexCord::Cord left,
   HexCord::Cord right,
   int depth,
   const HexCord& bLeft,
   const HexCord& tRight)
{
   // pair<iterator, bool> result = Container::insert(HiZone(left,right));
   // ASSERT(result.second);
   const HexCord::Cord ourEdge = gameInfo()->whichEdge();
   const HexCord::Cord otherEdge = gameInfo()->otherEdge();

   for(iterator it = Container::begin(); it != Container::end(); ++it)
   {
      HiZone* zone = *it;
      if( (ourEdge > otherEdge && left >= zone->leftX()) ||
            (ourEdge < otherEdge && left <= zone->leftX()) )
      {
         break;
      }
   }

   const HexCord::Cord y = (ourEdge > otherEdge) ? tRight.y() - depth : bLeft.y() + depth;

   HexCord leftHex(left, y);
   HexCord rightHex(right, y);

   it = Container::insert(it, new HiZone(gameInfo(), leftHex, rightHex));
   HiZone* zone = *it;

   return zone;
}

HiZone* HiZoneList::createZone(HexCord left, HexCord right)
{
   HiZone* z = new HiZone(gameInfo(), left, right);
   ASSERT(z);

   Container::push_back(z);
#ifdef DEBUG
   gameInfo()->log("Created Zone: %s", (const char*)z->name());
   gameInfo()->log("------------> left(%d, %d), right(%d, %d)",
       (int)left.x(), (int)left.y(), (int)right.x(), (int)right.y());
#endif
   return z;
}

// void HiZoneList::deleteZone(HiZone* zone)
// {
//     ASSERT(Container::find(*zone) != Container::end());
//     Container::size_type result = Container::erase(zone);
//     ASSERT(result == 1);
// }

void HiZoneList::deleteZone(const iterator& it)
{
      HiZone* zone = *it;
      Container::erase(it);
      delete zone;
}

/*===============================================================
 * Public Functions
 */

void HiZoneList::updateZones(int nCorps, int nZones, Hi_OpPlan::Plan& plan)
{

   if(gameInfo()->isDeploying())
   {
      doDeployZones(nCorps, nZones, plan);
   }
   else
      doCalcNewZones();

   d_phase = 0;
   d_nPhases = 4;

   updatePlan(plan);
}

// Recalculate initial zone bounrdries during game
void HiZoneList::doCalcNewZones()
{
#ifdef DEBUG
   gameInfo()->log("Alert!!! --------- upDateZones(Zones=%d) -- Game in progress");
#endif

   // go through an set new initial boundries to current position
   for(int z = 0; z < size(); z++)
   {
      HiZone* zone = (*this)[z];

      // get old facing
      HexCord zLeft = zone->leftHex();
      HexCord zRight = zone->rightHex();
      HexPosition::Facing f = WG_BattleAI_Utils::frontFacing(zLeft, zRight);

      // clear out old boundries
      while(zone->nBoundries() > 0)
         zone->removeTopBoundry();

      // get left-right most unit positions
      HexCord left = zone->leftFront();
      HexCord right = zone->rightFront();
      HexCord hex;

      // add a little lateral space
      {
         HexCord::HexDirection hd = BattleMeasure::leftFlank(f);
         if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), hd, 5, left, hex))
            left = hex;
      }

      {
         HexCord::HexDirection hd = BattleMeasure::rightFlank(f);
         if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), hd, 5, right, hex))
            right = hex;
      }

      zone->addBoundry(left, right);
      zone->recalcBoundries(True);
   }
}

void HiZoneList::doDeployZones(int nCorps, int nZones, Hi_OpPlan::Plan& plan)
{
   ASSERT(gameInfo()->isDeploying());
#ifdef DEBUG
   gameInfo()->log("upDateZones(Zones=%d) -- Deployment Phase", nZones);
#endif

   /*
    * Create zones if needed
    */

   ASSERT( (nZones >= 1) && (nZones <= 3) );
   ASSERT(Container::size() == 0);
   calcZones(nZones, plan);
}


void HiZoneList::calcZones(int nZones, Hi_OpPlan::Plan plan)
{
#ifdef DEBUG
   gameInfo()->log("updateZones(Zones=%d) -- Deployment Phase -- Units not on Map", nZones);
#endif
   // Create new zones

// HexCord mapSize = gameInfo()->map()->getSize();
   HexCord bLeft;
   HexCord mSize;
   gameInfo()->battleData()->getPlayingArea(&bLeft, &mSize);
   HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());
   int mapW = mSize.x();
   int mapH = mSize.y();

   const HexCord::Cord ourEdge = gameInfo()->whichEdge();
   const HexCord::Cord otherEdge = gameInfo()->otherEdge();

   int depth = maximum(10, (mapH / 2) - gameInfo()->random(5, 8)); //(mapH * 33) / 100;
   int overlap = (mapW * 2) / 100;     // 20% overlap
   int battleWidth = mapW + overlap;

   int x1 = (ourEdge > otherEdge) ? tRight.x() - 1 : bLeft.x();

   int zones = nZones;
   while(zones)
   {
      int w = battleWidth / zones;
      battleWidth -= w;

      int right = minimum(tRight.x() - 1, maximum(bLeft.x(), (ourEdge > otherEdge) ? x1 - w : x1 + w));

      HiZone* zone = createZone(x1, right, depth, bLeft, tRight);

#ifdef DEBUG
      gameInfo()->log("Created Zone: %s", (const char*)zone->name());
#endif

      if(ourEdge > otherEdge)
         x1 -= w;
      else
         x1 += w;

      if(--zones)
      {
         int lap = overlap / nZones;
         overlap -= lap;
         if(ourEdge > otherEdge)
            x1 += lap;
         else
            x1 -= lap;
      }
   }

   /*
    * Assign Priorities
    */

   assignPriority(nZones, plan);
}

void HiZoneList::assignPriority(int nZones, Hi_OpPlan::Plan plan)
{
   int z = 0;
   for(iterator it = begin(); it != end(); ++it, z++)
   {

      RandomNumber& lRand = gameInfo()->random();
      int averageP = zonePriority(z, nZones, plan);
      BAI_AggressPosStruct agPos;
      zoneAggPos(z, nZones, plan, agPos);
      ZoneOrientation zo = zoneOrientation(z, nZones, plan);
      int variance = lRand(2, 10);  // get a random variance between 6 and 15 // / 2;
      int p = maximum(1, averageP + lRand.gauss(variance));
// int p = maximum(5, averageP + gameInfo()->random().gauss(variance));
      (*it)->priority(p);
      (*it)->aggression(agPos.d_aggression);
      (*it)->posture(agPos.d_posture);
      (*it)->zoneOrientation(zo);
//    assignAggPos((*it));

#ifdef DEBUG
      gameInfo()->log("Assign p=%d to z%d(%s)",
                        (int) (*it)->priority(),
                        z,
                        (const char*)(*it)->name());
#endif
   }
}

void HiZoneList::zoneAggPos(int zone, int nZones, Hi_OpPlan::Plan plan, BAI_AggressPosStruct& agPos)
{
   const int zoneMax = 3;
   ASSERT(nZones <= zoneMax);

   struct LAggPos {
//    BattleOrderInfo::Aggression;
//    BattleOrderInfo::Posture;
       UBYTE d_aggLow;
       UBYTE d_aggHigh;
       UBYTE d_posLow;
       UBYTE d_posHigh;
   } laggPos;

   if(nZones == 1)
   {
      static const LAggPos s_table[1] = {
         { 2, 3, 1, 1 }
      };
      laggPos = s_table[0];
   }
   else if(nZones == 2)
   {
      ASSERT(zone < 2);
      static const LAggPos s_table[Hi_OpPlan::Plan_HowMany][2] = {
         {
            { 3, 3, 1, 1 },
            { 2, 3, 1, 1 }
         }, // Off_SingleLeft
         {
            { 2, 3, 1, 1 },
            { 3, 3, 1, 1 }
         }, // Off_SingleRIght
         {
            { 0, 0, 0, 0 },
            { 0, 0, 0, 0 }
         }, // Off_Double
         {
            { 0, 0, 0, 0 },
            { 0, 0, 0, 0 }
         }, // Off_Center
         {
            { 2, 3, 1, 1 },
            { 2, 3, 1, 1 }
         }, // Off_BroadFront
         {
            { 2, 3, 1, 1 },
            { 2, 2, 1, 1 }
         }, // Off_EchelonLeft
         {
            { 2, 2, 1, 1 },
            { 2, 3, 1, 1 }
         }, // Off_EchelonRight

         {
            { 2, 3, 0, 0 },
            { 2, 3, 0, 0 }
         },  // Def_BroadFront
         {
            { 2, 3, 0, 0 },
            { 1, 2, 0, 0 }
         },  // Def_Left
         {
            { 1, 2, 0, 0 },
            { 2, 3, 0, 0 }
         },  // Def_Right
      };

      laggPos = s_table[plan][zone];
   }
   else
   {
      static const LAggPos s_table[Hi_OpPlan::Plan_HowMany][zoneMax] = {
         {
            { 3, 3, 1, 1 },
            { 2, 3, 1, 1 },
            { 2, 2, 1, 1 }
         }, // Off_SingleLeft
         {
            { 2, 2, 1, 1 },
            { 2, 3, 1, 1 },
            { 3, 3, 1, 1 }
         }, // Off_SingleRIght
         {
            { 2, 3, 1, 1 }, //40,
            { 2, 2, 1, 1 },
            { 2, 3, 1, 1 }
         }, // Off_Double
         {
            { 2, 2, 1, 1 },
            { 2, 3, 1, 1 },
            { 2, 2, 1, 1 }
         }, // Off_Center
         {
            { 2, 3, 1, 1 },
            { 2, 3, 1, 1 },
            { 2, 3, 1, 1 }
         }, // Off_BroadFront
         {
            { 3, 3, 1, 1 },
            { 2, 3, 1, 1 },
            { 2, 2, 1, 1 }
         }, // Off_EchelonLeft
         {
            { 2, 2, 1, 1 },
            { 2, 3, 1, 1 },
            { 3, 3, 1, 1 }
         }, // Off_EchelonRight

         {
            { 2, 3, 0, 0 },
            { 2, 3, 0, 0 },
            { 2, 3, 0, 0 }
         },  // Def_BroadFront
         {
            { 2, 3, 0, 0 },
            { 2, 3, 0, 0 },
            { 2, 3, 0, 0 }
         },  // Def_Left
         {
            { 2, 3, 0, 0 },
            { 2, 3, 0, 0 },
            { 2, 3, 0, 0 }
         },  // Def_Right
      };

      laggPos = s_table[plan][zone];
   };

   ASSERT(laggPos.d_aggLow < BattleOrderInfo::Aggress_HowMany);
   ASSERT(laggPos.d_aggHigh < BattleOrderInfo::Aggress_HowMany);
   if(laggPos.d_aggLow != laggPos.d_aggHigh)
      agPos.d_aggression = static_cast<BattleOrderInfo::Aggression>(gameInfo()->random(laggPos.d_aggLow, laggPos.d_aggHigh));
   else
      agPos.d_aggression = static_cast<BattleOrderInfo::Aggression>(laggPos.d_aggLow);

   ASSERT(laggPos.d_posLow < BattleOrderInfo::Posture_HowMany);
   ASSERT(laggPos.d_posHigh < BattleOrderInfo::Posture_HowMany);
   if(laggPos.d_posLow != laggPos.d_posHigh)
      agPos.d_posture = static_cast<BattleOrderInfo::Posture>(gameInfo()->random(laggPos.d_posLow, laggPos.d_posHigh));
   else
      agPos.d_posture = static_cast<BattleOrderInfo::Posture>(laggPos.d_posLow);
}

ZoneOrientation HiZoneList::zoneOrientation(int zone, int nZones, Hi_OpPlan::Plan plan)
{
   const int zoneMax = 3;
   ASSERT(nZones <= zoneMax);

   ZoneOrientation zo = ZO_Default;
   if(nZones == 1)
   {
      static const ZoneOrientation s_table[1] = {
         ZO_Centered
      };
      zo = s_table[0];
   }
   else if(nZones == 2)
   {
      ASSERT(zone < 2);
#if 0
      static const ZoneOrientation s_table[Hi_OpPlan::Plan_HowMany][2] = {
         {
            ZO_Right,
            ZO_Left
         }, // Off_SingleLeft
         {
            ZO_Right,
            ZO_Left
         }, // Off_SingleRIght
         {
            ZO_Right,
            ZO_Left
         }, // Off_Double
         {
            ZO_Right,
            ZO_Left
         }, // Off_Center
         {
            ZO_Right,
            ZO_Left
         }, // Off_BroadFront
         {
            ZO_Right,
            ZO_Left
         }, // Off_EchelonLeft
         {
            ZO_Right,
            ZO_Left
         }, // Off_EchelonRight

         {
            ZO_Right,
            ZO_Left
         },  // Def_BroadFront
         {
            ZO_Centered,
            ZO_Left
         },  // Def_Left
         {
            ZO_Right,
            ZO_Left
         },  // Def_Right
      };
#endif
      static const ZoneOrientation s_table[2] = {
         ZO_Right, ZO_Left
      };
      zo = s_table[zone];
   }
   else
   {
#if 0
      static const ZoneOrientation s_table[Hi_OpPlan::Plan_HowMany][zoneMax] = {
         {
            ZO_Centered,
            ZO_Left,
            ZO_Left
         }, // Off_SingleLeft
         {
            ZO_Right,
            ZO_Right,
            ZO_Centered
         }, // Off_SingleRIght
         {
            ZO_Centered,
            ZO_Centered,
            ZO_Centered
         }, // Off_Double
         {
            ZO_Right,
            ZO_Centered,
            ZO_Left
         }, // Off_Center
         {
            ZO_Centered,
            ZO_Centered,
            ZO_Centered
         }, // Off_BroadFront
         {
            ZO_Centered,
            ZO_Left,
            ZO_Left
         }, // Off_EchelonLeft
         {
            ZO_Right,
            ZO_Right,
            ZO_Centered
         }, // Off_EchelonRight

         {
            ZO_Right,
            ZO_Centered,
            ZO_Left
         },  // Def_BroadFront
         {
            ZO_Centered,
            ZO_Left,
            ZO_Left
         },  // Def_Left
         {
            ZO_Right,
            ZO_Right,
            ZO_Centered
         },  // Def_Right
      };
#endif
      static const ZoneOrientation s_table[3] = {
         ZO_Right, ZO_Centered, ZO_Left
      };

      zo = s_table[zone];
   };

   return zo;
}

int HiZoneList::zonePriority(int zone, int nZones, Hi_OpPlan::Plan plan)
{
   const int zoneMax = 3;
   ASSERT(nZones <= zoneMax);

   if(nZones == 1)
   {
      return 100;
   }
   else if(nZones == 2)
   {
      ASSERT(zone < 2);
      static const int s_table[Hi_OpPlan::Plan_HowMany][2] = {
         { 65, 35 }, // Off_SingleLeft
         { 35, 65 }, // Off_SingleRIght
         { -1, -1 }, // Off_Double
         { -1, -1 }, // Off_Center
         { 50, 50 }, // Off_BroadFront
         { 65, 35 }, // Off_EchelonLeft
         { 35, 64 }, // Off_EchelonRight

         { 50, 50 },  // Def_Center
         { 60, 40 },  // Def_Left
         { 40, 60 }  // Def_Right
      };

      int v = s_table[plan][zone];
      ASSERT(v != -1);
      return v;
   }
   else
   {
      static const int s_table[Hi_OpPlan::Plan_HowMany][zoneMax] = {
         { 45, 35, 20 }, // Off_SingleLeft
         { 20, 35, 45 }, // Off_SingleRIght
         { 40, 20, 40 }, // Off_Double
         { 25, 50, 25 }, // Off_Center
         { 33, 33, 33 }, // Off_BroadFront
         { 45, 35, 20 }, // Off_EchelonLeft
         { 20, 35, 45 }, // Off_EchelonRight

         { 33, 33, 33 },  // Def_Center
         { 45, 35, 20 },  // Def_Left
         { 20, 35, 45 }  // Def_Right
      };

      return s_table[plan][zone];
   };
}

BattleMeasure::BattleTime::Tick HiZoneList::zoneStartTime(int zone, int nZones, Hi_OpPlan::Plan plan)
{
   const int zoneMax = 3;
   ASSERT(nZones <= zoneMax);

   RandomNumber& lRand = gameInfo()->random();

   if(nZones == 1)
   {
      return (gameInfo()->battleData()->getTick() + BattleMeasure::minutes(lRand(10)));
   }
   else if(nZones == 2)
   {
      ASSERT(zone < 2);
      static const int s_table[Hi_OpPlan::Plan_HowMany][2] = {
         { 20,  0 }, // Off_SingleLeft
         {  0, 20 }, // Off_SingleRIght
         { -1, -1 }, // Off_Double
         { -1, -1 }, // Off_Center
         {  0,  0 }, // Off_BroadFront
         { 20,  0 }, // Off_EchelonLeft
         {  0, 20 }, // Off_EchelonRight

         {  0,  0 },  // Def_BroadFront
         {  0,  0 },  // Def_BroadFront
         {  0,  0 },  // Def_BroadFront
      };

      int v = s_table[plan][zone];
      ASSERT(v != -1);
      return (gameInfo()->battleData()->getTick() + BattleMeasure::minutes(v));
   }
   else
   {
      static const int s_table[Hi_OpPlan::Plan_HowMany][zoneMax] = {
         { 20, 10,  0 }, // Off_SingleLeft
         {  0, 10, 20 }, // Off_SingleRIght
         { 20,  0, 10 }, // Off_Double
         { 10, 20,  0 }, // Off_Center
         {  5,  5,  5 }, // Off_BroadFront
         { 20, 10,  0 }, // Off_EchelonLeft
         {  0, 10, 20 }, // Off_EchelonRight

         {  0,  0,  0 },  // Def_BroadFront
         {  0,  0,  0 },  // Def_BroadFront
         {  0,  0,  0 },  // Def_BroadFront
      };

      return (gameInfo()->battleData()->getTick() + BattleMeasure::minutes(s_table[plan][zone]));
   };
}


HiZoneList::AdvBoundry HiZoneList::advanceZoneBoundries(
   int zone,
   int nZones,
   Hi_OpPlan::Plan plan,
   int phase)
{
   const int zoneMax = 3;
   const int nPhases = 4;
   ASSERT(nZones <= zoneMax);
   ASSERT(phase < nPhases);
   ASSERT(zone < zoneMax);

   RandomNumber& lRand = gameInfo()->random();

   if(nZones == 1)
   {
      static const AdvBoundry s_table[nPhases] = {
            { 0, 0,  0,  0 },
            { 0, 0, 10, 10 },
            { 0, 0, 10, 10 },
            { 0, 0, 10, 10 }
      };

      return s_table[phase];
   }

   else if(nZones == 2)
   {
      ASSERT(zone < 2);
      static const AdvBoundry s_table[nPhases][Hi_OpPlan::Plan_HowMany][2] = {
         // Phase 1
         {
            { // Off_SingleLeft
               { 0, -3, 0, 0 }, // Zone 1
               { -3, 0, -5, -5 }, // Zone 2
            },

            { // Off_SingleRight
               { 0, 3, -5, -5 }, // Zone 1
               { 3, 0, 0, 0 }, // Zone 2
            },

            { // Off_Double
               { 0, 0,  0,  0 }, // Zone 1
               { 0, 0,  0,  0 }, // Zone 2
            },

            { // Off_Center
               { 0, 0,  0,  0 }, // Zone 1
               { 0, 0,  0,  0 }, // Zone 2
            },

            { // Off_Broadfront
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },

            { // Off_EchelonLeft
               { 0, -3, 0, 0 }, // Zone 1
               { -3, 0, -5, -5 }, // Zone 2
            },

            { // Off_EchelonRight
               { 0, 3, -5, -5 }, // Zone 1
               { 3, 0, 0, 0 }, // Zone 2
            },

            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
            { // Def_Left
               { 0, -3, 0, 0 }, // Zone 1
               { -3, 0, -5, -5 }, // Zone 2
            },
            { // Def_Right
               { 0, 3, -5, -5 }, // Zone 1
               { 3, 0, 0, 0 }, // Zone 2
            },
         },

         // Phase 2
         {
            { // Off_SingleLeft
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0,  8,  8 }, // Zone 2
            },

            { // Off_SingleRight
               { 0, 0,  8,  8 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
            },

            { // Off_Double
               { 0, 0,  0,  0 }, // Zone 1
               { 0, 0,  0,  0 }, // Zone 2
            },

            { // Off_Center
               { 0, 0,  0,  0 }, // Zone 1
               { 0, 0,  0,  0 }, // Zone 2
            },

            { // Off_Broadfront
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
            },

            { // Off_EchelonLeft
               { 0, 0, 10,  10 }, // Zone 1
               { 0, 0,  8,   8 }, // Zone 2
            },

            { // Off_EchelonRight
               { 0, 0,  8,  8 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
            },

            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
            { // Def_Left
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
            { // Def_Right
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
         },

         // Phase 2
         {
            { // Off_SingleLeft
               { 5, 5, 10,  8 }, // Zone 1
               { 0, 0,  4,  4 }, // Zone 2
            },

            { // Off_SingleRight
               {  0,  0,  4,  4 }, // Zone 1
               { -5, -5,  8, 10 }, // Zone 2
            },

            { // Off_Double
               { 0, 0,  0,  0 }, // Zone 1
               { 0, 0,  0,  0 }, // Zone 2
            },

            { // Off_Center
               { 0, 0,  0,  0 }, // Zone 1
               { 0, 0,  0,  0 }, // Zone 2
            },

            { // Off_Broadfront
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
            },

            { // Off_EchelonLeft
               { 0, 0, 10,  10 }, // Zone 1
               { 0, 0,  4,   4 }, // Zone 2
            },

            { // Off_EchelonRight
               { 0, 0,  4,  4 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
            },

            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
         },

         // Phase 3
         {
            { // Off_SingleLeft
               { 10, 10, 10, 2 }, // Zone 1
               { 0,   0,  0, 0 }, // Zone 2
            },

            { // Off_SingleRight
               {   0,   0,  0,  0 }, // Zone 2
               { -10, -10,  2, 10 }, // Zone 1
            },

            { // Off_Double
               { 0, 0,  0,  0 }, // Zone 1
               { 0, 0,  0,  0 }, // Zone 2
            },

            { // Off_Center
               { 0, 0,  0,  0 }, // Zone 1
               { 0, 0,  0,  0 }, // Zone 2
            },

            { // Off_Broadfront
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
            },

            { // Off_EchelonLeft
               { 0, 0, 10,  10 }, // Zone 1
               { 0, 0,  4,   4 }, // Zone 2
            },

            { // Off_EchelonRight
               { 0, 0,  4,  4 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
            },

            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
            },
         }
      };

      return s_table[phase][plan][zone];
   }

   else
   {
      ASSERT(nZones == 3);
      static const AdvBoundry s_table[nPhases][Hi_OpPlan::Plan_HowMany][zoneMax] = {
         // Phase 1
         {
            { // Off_SingleLeft
               { 0, 3, -6, -6 }, // Zone 1
               { 3, 3, -3, -3 }, // Zone 2
               { 3, 0, 0, 0 }, // Zone 2
            },

            { // Off_SingleRight
               { 0, -3, 0, 0 }, // Zone 1
               { -7, -3, -3, -3 }, // Zone 2
               { -3, 0, -6, -6 }, // Zone 3
            },

            { // Off_Double
               { 0, -2, 0, 0 }, // Zone 1
               { -2, 2, -5, -5 }, // Zone 2
               { 2, 0, 0, 0 }, // Zone 3
            },

            { // Off_Center
               { 0, 0, -5, -5 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, -5, -5 }, // Zone 3
            },

            { // Off_Broadfront
               { 0, 0, -5, -5 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, -5, -5 }, // Zone 3
            },

            { // Off_EchelonLeft
               { 0, -3, 0, 0 }, // Zone 1
               { -3, -3, -3, -3 }, // Zone 2
               { -3, 0, -6, -6 }, // Zone 3
            },

            { // Off_EchelonRight
               { 0, 3, -6, -6 }, // Zone 1
               { 3, 3, -3, -3 }, // Zone 2
               { 3, 0, 0, 0 }, // Zone 2
            },

            { // Def_Broadfron
               { 0, 0, -5, -5 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, -5, -5 }, // Zone 3
            },
            { // Def_Left
               { 0, -3, 0, 0 }, // Zone 1
               { -3, -3, -3, -3 }, // Zone 2
               { -3, 0, -6, -6 }, // Zone 3
            },
            { // Def_Right
               { 0, 3, -6, -6 }, // Zone 1
               { 7, 3, -3, -3 }, // Zone 2
               { 3, 0, 0, 0 }, // Zone 2
            },
         },
         // Phase 2
         {
            { // Off_SingleLeft
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0,  8,  8 }, // Zone 2
               { 0, 0,  8,  8 }, // Zone 3
            },

            { // Off_SingleRight
               { 0, 0,  8,  8 }, // Zone 1
               { 0, 0,  8,  8 }, // Zone 2
               { 0, 0, 10, 10 }, // Zone 3
            },

            { // Off_Double
               { 0, 0,  10,  10 }, // Zone 1
               { 0, 0,   8,   8 }, // Zone 2
               { 0, 0,  10,  10 }, // Zone 3
            },

            { // Off_Center
               { 0, 0,   8,   8 }, // Zone 1
               { 0, 0,  10,  10 }, // Zone 2
               { 0, 0,   8,   8 }, // Zone 3
            },

            { // Off_Broadfront
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
               { 0, 0, 10, 10 }, // Zone 3
            },

            { // Off_EchelonLeft
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0,  8,  8 }, // Zone 2
               { 0, 0,  8,  8 }, // Zone 3
            },

            { // Off_EchelonRight
               { 0, 0,  8,  8 }, // Zone 1
               { 0, 0,  8,  8 }, // Zone 2
               { 0, 0, 10, 10 }, // Zone 3
            },

            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, 0, 0 }, // Zone 3
            },
            { // Def_Left
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, 0, 0 }, // Zone 3
            },
            { // Def_Right
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, 0, 0 }, // Zone 3
            },
         },

         // Phase 2
         {
            { // Off_SingleLeft
               { 5, 5,  10, 8 }, // Zone 1
               { 5, 5,  6,  2 }, // Zone 2
               { 0, 0,  2,  2 }, // Zone 3
            },

            { // Off_SingleRight
               {  0,  0,  2,  2 }, // Zone 1
               { -5, -5,  2,  6 }, // Zone 2
               { -5, -5,  8,  10 }, // Zone 3
            },

            { // Off_Double
               {  5,  5,  10,  8 }, // Zone 1
               {  0,  0,   4,   4 }, // Zone 2
               { -5, -5,   8,  10 }, // Zone 3
            },

            { // Off_Center
               { 0, 0,   4,   4 }, // Zone 1
               { 0, 0,  10,  10 }, // Zone 2
               { 0, 0,   4,   4 }, // Zone 3
            },

            { // Off_Broadfront
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
               { 0, 0, 10, 10 }, // Zone 3
            },

            { // Off_EchelonLeft
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0,  5,  5 }, // Zone 2
               { 0, 0,  2,  2 }, // Zone 3
            },

            { // Off_EchelonRight
               { 0, 0,  2,  2 }, // Zone 1
               { 0, 0,  5,  5 }, // Zone 2
               { 0, 0, 10, 10 }, // Zone 3
            },

            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, 0, 0 }, // Zone 3
            },
            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, 0, 0 }, // Zone 3
            },
            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, 0, 0 }, // Zone 3
            },
         },

         // Phase 3
         {
            { // Off_SingleLeft
               { 10, 10,  10, 2 }, // Zone 1
               {  5,  5,  2,  0 }, // Zone 2
               {  0,  0,  0,  0 }, // Zone 3
            },

            { // Off_SingleRight
               {   0,   0,  0,   0 }, // Zone 1
               {  -5,  -5,  0,   2 }, // Zone 2
               { -10, -10,  2,  10 }, // Zone 3
            },

            { // Off_Double
               {  10,  10,  10,  2 }, // Zone 1
               {  0,  0,   0,   0 }, // Zone 2
               { -10, -10,   2,  10 }, // Zone 3
            },

            { // Off_Center
               { 0, 0,   4,   4 }, // Zone 1
               { 0, 0,  10,  10 }, // Zone 2
               { 0, 0,   4,   4 }, // Zone 3
            },

            { // Off_Broadfront
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0, 10, 10 }, // Zone 2
               { 0, 0, 10, 10 }, // Zone 3
            },

            { // Off_EchelonLeft
               { 0, 0, 10, 10 }, // Zone 1
               { 0, 0,  5,  5 }, // Zone 2
               { 0, 0,  2,  2 }, // Zone 3
            },

            { // Off_EchelonRight
               { 0, 0,  2,  2 }, // Zone 1
               { 0, 0,  5,  5 }, // Zone 2
               { 0, 0, 10, 10 }, // Zone 3
            },

            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, 0, 0 }, // Zone 3
            },
            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, 0, 0 }, // Zone 3
            },
            { // Def_Broadfron
               { 0, 0, 0, 0 }, // Zone 1
               { 0, 0, 0, 0 }, // Zone 2
               { 0, 0, 0, 0 }, // Zone 3
            },
         }
      };

      return s_table[phase][plan][zone];
   }
}

void HiZoneList::updatePlan(Hi_OpPlan::Plan plan)
{
   int nZones = size();

#ifdef DEBUG
   gameInfo()->log("upDateZones(ZoneCount=%d) -- Offensive", nZones);
#endif

   const HexCord::Cord ourEdge = gameInfo()->whichEdge();
   const HexCord::Cord otherEdge = gameInfo()->otherEdge();
   const BattleMap* map = gameInfo()->map();
   RandomNumber& lRand = gameInfo()->random();
   bool def = (plan >= Hi_OpPlan::Plan_FirstDefensive && plan <= Hi_OpPlan::Plan_LastDefensive);

   const int nPhases = 4;
   for(int z = 0; z < nZones; z++)
   {
      HiZone* zone = (*this)[z];
      HexCord leftHex;
      HexCord rightHex;
      for(int phase = 0; phase < nPhases; phase++)
      {
         AdvBoundry ab = advanceZoneBoundries(z, nZones, plan, phase);

         if(phase == 0)
         {
            leftHex = zone->leftHex();
            rightHex = zone->rightHex();
         }

         HexPosition::Facing facing = WG_BattleAI_Utils::frontFacing(leftHex, rightHex);

         int leftLR = ab.d_leftLR;// + lRand.gauss(lrVar);
         int leftFB = ab.d_leftFB;// + lRand.gauss(fbVar);
         int rightLR = ab.d_rightLR;// + lRand.gauss(lrVar);
         int rightFB = ab.d_rightFB;// + lRand.gauss(fbVar);

         HexCord hex;
         if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), facing, leftLR, leftFB, leftHex, hex))
            leftHex = hex;

         if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), facing, rightLR, rightFB, rightHex, hex))
            rightHex = hex;

         zone->addBoundry(leftHex, rightHex);
         if(def)
            break;
      }

      // remvoe top boundry, this was just an initial guide
      zone->removeTopBoundry();
#ifdef DEBUG
      if(def)
         ASSERT(zone->nBoundries() == 1);
#endif

//      zone->initObjectives();
      BattleMeasure::BattleTime::Tick startW = zoneStartTime(z, nZones, plan);

      int variance = lRand(10);    // / 2;
      if(variance > 0)
         startW =  static_cast<BattleMeasure::BattleTime::Tick>(maximum(0, startW + BattleMeasure::minutes(lRand.gauss(variance))));

      zone->startWhen(startW);

#ifdef DEBUG
      gameInfo()->log("Zone left = (%d, %d), Zone right = (%d, %d)",
          static_cast<int>(zone->leftHex().x()), static_cast<int>(zone->leftHex().y()),
          static_cast<int>(zone->rightHex().x()), static_cast<int>(zone->rightHex().y()));

      int inMins = BattleMeasure::minutesPerTick(static_cast<BattleMeasure::BattleTime::Tick>(maximum(0, startW - gameInfo()->battleData()->getTick())));
      gameInfo()->log("zone #%d(%s) -- start order in %d minutes",
                        z, (const char*)zone->name(), inMins);
#endif
   }
}


static UWORD s_version = 0x0000;
bool  HiZoneList::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   int items;
   f >> items;

   while(items--)
   {
      HiZone* zone = new HiZone(gameInfo());
      zone->readData(f);
      Container::push_back(zone);
   }

   f >> d_phase;
   f >> d_nPhases;

   return f.isOK();
}

bool HiZoneList::writeData(FileWriter& f) const
{
   f << s_version;

   int items = Container::size();
   f << items;

   for(Container::const_iterator it = begin(); it != end(); ++it)
   {
      (*it)->writeData(f);
   }

   f << d_phase;
   f << d_nPhases;

   return f.isOK();
}

};  // namespace WG_BattleAI_Internal


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
