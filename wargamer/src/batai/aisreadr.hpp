/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef AISREADR
#define AISREADR

#include "batdata.hpp"

class FileReader;
class FileWriter;
class StringParse;

namespace WG_BattleAI_Internal
{
class BattleSideAI;
class HighLevelPlanner;
class ScriptReader;
class HiZone;
class RealHiObjective;

// Historical Battle AI script reader
class BAI_ScriptReader {
      const TCHAR*    d_fileName;
      BattleSideAI* d_sideAI;
      HighLevelPlanner* d_planner;
      PBattleData d_batData;
   public:
      BAI_ScriptReader(RPBattleData bd, const TCHAR* fileName, HighLevelPlanner* planner);
      BAI_ScriptReader(RPBattleData bd, const TCHAR* fileName, BattleSideAI* ai);
      BAI_ScriptReader(RPBattleData bd, const TCHAR* fileName, BattleSideAI* ai, FileWriter& bFile);
   private:
      void convertToBinary(FileReader& f, FileWriter& bFile);
      void convertPlan(FileReader& f, FileWriter& bFile, int plan);
      void readBinary(FileReader& f);
      void readBinaryPlan(FileReader& f, int plan);
      void read(FileReader& f);
      void getPlan(FileReader& f);
      void getSideBoundries(FileReader& f);
      void getSide(FileReader& f, Side s);
      void readZone(FileReader& f);
      void allocateZone(HiZone** zone, StringParse& parser);
      int countZones(FileReader& f);
      void allocateUnit(HiZone* zone, FileReader& f, StringParse& parser);
      void allocateObj(HiZone* zone, FileReader& f);
      void setObjBoundry(RealHiObjective* obj, StringParse& parser);
      void assignObjUnit(HiZone* zone, RealHiObjective* obj, FileReader& f, StringParse& parser);
};

}; // end namespace
#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
