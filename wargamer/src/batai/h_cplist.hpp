/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef H_CPLIST_HPP
#define H_CPLIST_HPP

#ifndef __cplusplus
#error h_cplist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hi level Unit List
 *
 *----------------------------------------------------------------------
 */

#include "aib_info.hpp"
#include "hi_unit.hpp"
#include "contain.hpp"
#include <list>
class FileReader;
class FileWriter;
class OrderBattle;

namespace WG_BattleAI_Internal
{

class HiUnitList : public GameInfoUser, public STLContainer<std::list<HiUnitInfo> >
{
        friend class BAI_ScriptReader;
    public:
        HiUnitList(GameInfo* gameInfo);
        ~HiUnitList() { }

        bool updateUnits();
            // Create Unit List and/or check for new reinforcements

        int corpsCount() const { return d_corpsCount; }
        int divisionCount() const { return d_divisionCount; }
        HiUnitInfo* find(CRefBattleCP cp);

		  // file interface
		  bool readData(FileReader& f, OrderBattle* ob);
		  bool writeData(FileWriter& f, OrderBattle* ob) const;
	 private:
		void addUnit(CRefBattleCP cp);

	 private:
		int d_corpsCount;
		int d_divisionCount;
};

};  // namespace WG_BattleAI_Internal

#endif /* H_CPLIST_HPP */

