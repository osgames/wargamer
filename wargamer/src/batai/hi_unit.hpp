/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef HI_UNIT_HPP
#define HI_UNIT_HPP

#ifndef __cplusplus
#error hi_unit.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * High level Unit and Leader Information
 *
 *----------------------------------------------------------------------
 */


#include "bobdef.hpp"
#include "leader.hpp"

class FileReader;
class FileWriter;
class OrderBattle;

namespace WG_BattleAI_Internal
{

class HiZone;
class RealHiObjective;

/*
 * A High Level Unit is information about corps or unattached divisions
 *
 * A unit may be assigned to 1 Zone and 1 Objective
 */

class HiUnitInfo
{
	 public:
		  HiUnitInfo() : d_cp(NoBattleCP), d_zone(0), d_objective(0), d_priority(0) { }
		  HiUnitInfo(CRefBattleCP cp) : d_cp(cp), d_zone(0), d_objective(0), d_priority(0) { }
		  ~HiUnitInfo();

		  void zone(HiZone* zone) { d_zone = zone; }
		  HiZone* zone() const { return d_zone; }
		  bool inZone() const { return d_zone != 0; }

		  void objective(RealHiObjective* ob) { d_objective = ob; }
		  RealHiObjective* objective() const { return d_objective; }

		  void priority(int p) { d_priority = p; }
		  int priority() const { return d_priority; }

		  CRefBattleCP cp() const { return d_cp; }

		  // file interface
		  bool readData(FileReader& f, OrderBattle* ob);
		  bool writeData(FileWriter& f, OrderBattle* ob) const;
	 private:
		  CRefBattleCP d_cp;
		  HiZone* d_zone;
		  RealHiObjective* d_objective;
		  int d_priority;
};


inline bool operator<(const HiUnitInfo& lhs, const HiUnitInfo& rhs)
{
    return lhs.cp() < rhs.cp();
}

inline bool operator==(const HiUnitInfo& lhs, const HiUnitInfo& rhs)
{
    return lhs.cp() == rhs.cp();
}


class HiLeaderInfo
{
    public:
        HiLeaderInfo() : d_leader(NoGLeader), d_unit(0) { }
        HiLeaderInfo(RefGLeader leader) : d_leader(leader), d_unit(0) { }
        ~HiLeaderInfo() { }

        void unit(HiUnitInfo* unit) { d_unit = unit; }
        HiUnitInfo* unit() { return d_unit; }

    private:
        RefGLeader d_leader;
        HiUnitInfo* d_unit;
};



};  // namespace WG_BattleAI_Internal

#endif /* HI_UNIT_HPP */

