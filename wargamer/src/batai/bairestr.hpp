/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BAIRESTR
#define BAIRESTR
#if 0
#include "batcord.hpp"
#include "batdata.hpp"
//#include "contain.hpp"
#include <list>


namespace WG_BattleAI_Internal
{

// List of restricted terrain
class RTerrain : public list<BattleMeasure::HexCord>
{
		public:
				enum Type {
					HeavyWoods,
					Town,
					River
				};

				RTerrain(Type t) : d_type(t) {}
				RTerrain(const RTerrain& tl);
				RTerrain& operator = (const RTerrain& tl);

				~RTerrain() { }

				void add(BattleMeasure::HexCord hex);
				bool inList(BattleMeasure::HexCord hex);
				const BattleMeasure::HexCord* find(BattleMeasure::HexCord hex);

				Type type() const { return d_type; }
#ifdef DEBUG
				const TCHAR* name()
				{
					static const TCHAR* s_names[] = {
						"Heavy Woods", "Town", "River"
					};

					return s_names[d_type];
				}
#endif
		private:
			 Type d_type;
};

class RTerrainList : public list<RTerrain*> {
	public:
		RTerrainList() {}
		~RTerrainList()
		{
			for(RTerrainList::iterator it = begin();
					it != end();
					++it)
			{
				delete (*it);
				(*it) = 0;
			}
		}

		void add(RTerrain* ter);
		bool inList(BattleMeasure::HexCord hex);
};
#if 0
class RestrictedTerrainManager {
		RTerrainList d_masterList;
	public:
		Restrict
		const RTerrainList& masterList() const { return d_masterList; }
		RTerrainList& masterList() { return d_masterList; }
};
#endif
// For now use a global instance until I decide where to put this
RTerrainList* restrictiveTerrain();
void initRestrictiveTerrain(RCPBattleData bd);

}; // end namespace
#endif
#endif
