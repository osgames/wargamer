/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Shared Battle AI Data
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aib_data.hpp"
#include "batdata.hpp"
#include "hexdata.hpp"
#include "key_terr.hpp"
#include "filebase.hpp"
#include "bobutil.hpp"
#include <utility>

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

namespace WG_BattleAI_Internal
{

inline bool roadHex(PathType pt)
{
  return (pt == LP_Road || pt == LP_SunkenRoad || pt == LP_RoadBridge);
}

inline bool trackHex(PathType pt)
{
  return (pt == LP_Track || pt == LP_SunkenTrack || pt == LP_TrackBridge);
}

inline bool roadBridgeHex(PathType pt)
{
  return (pt == LP_RoadBridge);
}

inline bool trackBridgeHex(PathType pt)
{
   return (pt == LP_TrackBridge);
}

BattleAIData::BattleAIData(Side side, const BattleData* batData) :
      GameInfo(side),
      d_batData(batData),
      d_strObjectives(),
      d_plan(Hi_OpPlan::Plan_None),
      d_flags(UpdateStrObjectives),
      d_cinc(NoBattleCP)
{
}

BattleAIData::~BattleAIData()
{
}

/*
 * Process a tick of AI thought... maybe used to alter enemy info accuracy, etc...
 */

void BattleAIData::process()
{
// d_lock.startWrite();
   // Find Cinc if we have none
   if(d_cinc == NoBattleCP)
   {
      setCinc();
      ASSERT(d_cinc != NoBattleCP);
#if 0//def DEBUG
#if 0
      BattleOrderInfo::Posture pos = (random(100) <= 50) ?
          BattleOrderInfo::Defensive : BattleOrderInfo::Offensive;
      const_cast<BattleCP*>(d_cinc)->posture(pos);
      const_cast<BattleCP*>(d_cinc)->getCurrentOrder().posture(pos);
      log("Cinc (%s of %s) has %s posture",
         d_cinc->leader()->getName(),
         d_cinc->getName(),
         BattleOrderInfo::postureName(d_cinc->posture()));
#else
      if(side() == 1)
      {
         const_cast<BattleCP*>(d_cinc)->posture(BattleOrderInfo::Offensive);
         const_cast<BattleCP*>(d_cinc)->getCurrentOrder().posture(BattleOrderInfo::Offensive);
      }
#endif
#endif
   }

   // Update Strategic Objectives if need be
   if(d_flags & UpdateStrObjectives)
   {
      updateStrObjectives();
      setFlag(UpdateStrObjectives, False);
   }

// d_lock.endWrite();
}

inline int squared(int v) { return (v * v); }
inline int getDist(const HexCord& srcHex, const HexCord& destHex)
{
    return static_cast<int>(sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y())));
}


void BattleAIData::updateStrObjectives()
{
   ASSERT(d_cinc != NoBattleCP);
   const int targetDepth = 20; // Looking for key terrain with 15 hexes of objEdge
// const HexCord mapSize = map()->getSize();
   HexCord bLeft;
   HexCord mSize;
   battleData()->getPlayingArea(&bLeft, & mSize);
   HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

   HexCord::Cord ourEdge = whichEdge();
   HexCord::Cord theirEdge = otherEdge();
   HexCord::Cord objEdge = (d_cinc->posture() == BattleOrderInfo::Offensive) ?
      theirEdge : ourEdge;
   HexCord::Cord objTop = (objEdge == 0) ? targetDepth : objEdge - targetDepth;
// HexCord sideLeft((objEdge == 0) ? 0 : mapSize.x() - 1, objTop);
// HexCord sideRight((objEdge == 0) ? mapSize.x() - 1 : 0, objTop);

   // get key terrain and pick some objectives
   const KeyTerrainList* kt = battleData()->keyTerrainList();
#ifdef DEBUG
   {
      SListIter<KeyTerrainPoint> iter(const_cast<KeyTerrainList*>(kt)->get());
      while(++iter)
      {
         HexCord hex = iter.current()->hex();
         log("Key Terrain(%s) at hex(%d, %d)",
            iter.current()->getDescription(), static_cast<int>(hex.x()), static_cast<int>(hex.y()));
      }
   }
  log("-----------------------------------\n");
#endif

   int topPriority = 0;
   SListIter<KeyTerrainPoint> iter(const_cast<KeyTerrainList*>(kt)->get());
   while(++iter)
   {
      HexCord hex = iter.current()->hex();
      bool shouldAdd = False;
      if(d_cinc->posture() == BattleOrderInfo::Offensive)
      {
         if(ourEdge == 0)
            shouldAdd = (hex.y() >= objTop);
         else
            shouldAdd = (hex.y() <= objTop);
      }
      else
      {
         if(ourEdge == 0)
            shouldAdd = (hex.y() <= objTop);
         else
            shouldAdd = (hex.y() >= objTop);
      }

      if(shouldAdd)
      {
         const int nKeyTypes = 8;
         static int s_priorityTable[nKeyTypes] = {
             25, 30, 35, 40, 50, 20, 40, 30
         };

         ASSERT(iter.current()->type() < nKeyTypes);
         int priority = s_priorityTable[iter.current()->type()];
         if(hex.x() >= (bLeft.x() + 20) && hex.x() <= (tRight.x() - 20))
            priority += 4;
         else if(hex.x() >= (bLeft.x() + 10) && hex.x() <= (tRight.x() - 10))
            priority += 2;

         // if another objective is with a given distance
         int closeDist = UWORD_MAX;
         SListIter<KeyTerrainPoint> sIter(const_cast<KeyTerrainList*>(kt)->get());
         while(++sIter)
         {
            if(hex != sIter.current()->hex())
            {
               int dist = getDist(hex, sIter.current()->hex());
               if(dist < closeDist)
                  closeDist = dist;
            }
         }

         if(closeDist <= 5)
            priority += 5;
         else if(closeDist <= 10)
            priority += 2;

#ifdef DEBUG
         log("Key Terrain(%s, priority=%d) at hex(%d, %d) added to Strategic Obj list",
            iter.current()->getDescription(), priority,
            static_cast<int>(hex.x()), static_cast<int>(hex.y()));
#endif
         if(priority > topPriority)
         {
            d_strObjectives.insert(hex, d_cinc->posture());
            topPriority = priority;
         }
         else
            d_strObjectives.add(hex, d_cinc->posture());
      }
   }

   // if no objectives
   if(d_strObjectives.entries() == 0)
   {
      // for now just make it the center of the objective edge
      HexCord hex((bLeft.x() + tRight.x()) / 2, objEdge);
      d_strObjectives.add(hex, d_cinc->posture());
   }

#ifdef DEBUG
   {
      log("Strategic Objectives ---------------");
      SListIterR<BAI_StrObj> iter(&d_strObjectives);
      while(++iter)
      {
         log("---------- hex(%d, %d)",
            static_cast<int>(iter.current()->d_hex.x()), static_cast<int>(iter.current()->d_hex.y()));
      }
   }
#endif
}

void BattleAIData::setCinc()
{
   d_cinc = BobUtility::getCinc(const_cast<BattleOB*>(battleData()->ob()), side());
   ASSERT(d_cinc != NoBattleCP);
#if 0

   CRefBattleCP bestCP = NoBattleCP;

   for(ConstBattleUnitIter unitIter = ourUnits();
         !unitIter.isFinished();
         unitIter.next())
   {
      CRefBattleCP cp = unitIter.cp();
      if(bestCP == NoBattleCP || cp->getRank().isHigher(bestCP->getRank().getRankEnum()))
         bestCP = cp;
      else if(cp->getRank().sameRank(bestCP->getRank().getRankEnum()))
      {
         ConstRefGLeader bestLeader = battleData()->ob()->ob()->whoShouldCommand(cp->leader(), bestCP->leader());
         if(bestLeader == cp->leader())
            bestCP = cp;
      }
   }

   ASSERT(bestCP != NoBattleCP);
   d_cinc = bestCP;
   // for now set aggress posture for testing offensive
   const_cast<BattleCP*>(d_cinc)->aggression(BattleOrderInfo::EngageAtAllCost);
   const_cast<BattleCP*>(d_cinc)->posture(BattleOrderInfo::Offensive);
   const_cast<BattleCP*>(d_cinc)->getCurrentOrder().posture(BattleOrderInfo::Offensive);
   const_cast<BattleCP*>(d_cinc)->getCurrentOrder().aggression(BattleOrderInfo::EngageAtAllCost);
#endif
}


/*
 * Process Player Message... can be used to update info about enemy, etc...
 */

void BattleAIData::procMessage(const BattleMessageInfo& msg)
{
    // TODO:
}

ConstBattleUnitIter BattleAIData::ourUnits() const
{
    return ConstBattleUnitIter(d_batData->ob(), side());
}

// file interface
static UWORD s_version = 0x0000;
bool BattleAIData::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   d_strObjectives.readData(f);
   UBYTE b;
   f >> b;
   d_plan = static_cast<Hi_OpPlan::Plan>(b);

   f >> d_flags;

   CPIndex cpi;
   f >> cpi;
   OrderBattle* ob = const_cast<OrderBattle*>(d_batData->ob()->ob());
   d_cinc = BobUtility::cpIndexToCP(cpi, *ob);

   return f.isOK();
}

bool BattleAIData::writeData(FileWriter& f) const
{
   f << s_version;

   d_strObjectives.writeData(f);

   UBYTE b = static_cast<UBYTE>(d_plan);
   f << b;
   f << d_flags;

   OrderBattle* ob = const_cast<OrderBattle*>(d_batData->ob()->ob());
   CPIndex cpi = BobUtility::cpToCPIndex(const_cast<BattleCP*>(d_cinc), *ob);
   f << cpi;

   return f.isOK();
}

};      // namespace WG_BattleAI_Internal


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
