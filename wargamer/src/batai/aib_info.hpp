/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_INFO_HPP
#define AIB_INFO_HPP

#ifndef __cplusplus
#error aib_info.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle AI Game Information
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "bobiter.hpp"
#include "random.hpp"
#include "hiopplan.hpp"

#ifdef DEBUG
#define HAS_LOG_WINDOW
#endif

#include "hiopplan.hpp"

#if defined(HAS_LOG_WINDOW)
#include "logwin.hpp"
// #include <strstrea.h>
#include <sstream>
#endif

class BattleData;
class BattleMap;
class BattleMessageInfo;

namespace WG_BattleAI_Internal
{

class BAI_StrObjList;

/*
 * Read Only Class accessible by the planner's
 */

class GameInfo
{
  public:
    GameInfo(Side s);

    virtual ~GameInfo() = 0;

    Side side() const { return d_side; }
    Side otherSide() const { return d_side ^ 1; }

    virtual ConstBattleUnitIter ourUnits() const = 0;
    virtual const BattleMap* map() const = 0;
    virtual const BattleData* battleData() const = 0;
    virtual const BAI_StrObjList& strObjectives() const = 0;
    virtual const CRefBattleCP& cinc() const = 0;
    virtual Hi_OpPlan::Plan plan() const = 0;

    HexCord::Cord getBattleEdge(Side side) const;
    HexCord::Cord whichEdge() const;
    HexCord::Cord otherEdge() const;
    // return Y coordinate of our end of the battlefield

    RandomNumber& random() { return d_random; }
    int random(int n)  { return d_random(n); }
    int random(int from, int to)  { return d_random(from, to); }

    virtual void plan(Hi_OpPlan::Plan plan) = 0;

#if defined(HAS_LOG_WINDOW)
    void log(const char* fmt, ...);
    std::ostream& stream() { return d_stream; }
    void logStream();
#endif

    void deploying(bool f) { d_deploying = f; }
    bool isDeploying() const { return d_deploying; }

  private:
    Side d_side;
    RandomNumber d_random;
    bool d_deploying;
#if defined(HAS_LOG_WINDOW)
    LogWinPtr d_logWindow;
    std::ostringstream d_stream;
#endif
};

inline GameInfo::~GameInfo() { }

/*
 * Class to derive from to contain a pointer to the GameInfo
 */

class GameInfoUser
{
  public:
    GameInfoUser(GameInfo* gameInfo) : d_gameInfo(gameInfo) { }

    GameInfo* gameInfo() { return d_gameInfo; }
    const GameInfo* gameInfo() const { return d_gameInfo; }

  private:
    GameInfo* d_gameInfo;
};


class BattleMessageProcessor
{
  public:
    virtual void procMessage(const BattleMessageInfo& msg) = 0;
};

}  // namespace WG_BattleAI_Internal
 
#endif /* AIB_INFO_HPP */

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */

