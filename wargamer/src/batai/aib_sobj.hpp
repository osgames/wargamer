/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_SOBJ_HPP
#define AIB_SOBJ_HPP

#include "sllist.hpp"
#include "batcord.hpp"
#include "batord.hpp"

class FileReader;
class FileWriter;

namespace WG_BattleAI_Internal
{
using BattleMeasure::HexCord;
#if 1
// Strategic objective of battle
// There can be more than one, though the number should be limited
struct BAI_StrObj : public SLink {
   HexCord d_hex;
   BattleOrderInfo::Posture d_posture;
#if 0
   enum Posture {  // are we defending this area or attacking toward?
          Defensive,
          Offensive,

          Posture_HowMany
   } d_posture;
#endif

   // various constructors
   BAI_StrObj() :
      d_hex(),
      d_posture(BattleOrderInfo::Defensive) {}

   BAI_StrObj(const HexCord& hex, BattleOrderInfo::Posture pos) :
      d_hex(hex),
      d_posture(pos) {}

   BAI_StrObj(const BAI_StrObj& so) :
      d_hex(so.d_hex),
      d_posture(so.d_posture) {}

   BAI_StrObj& operator = (const BAI_StrObj& so)
   {
      d_hex = so.d_hex;
      d_posture = so.d_posture;

      return *this;
   }

   void* operator new(size_t size);
#ifdef _MSC_VER
   void operator delete(void* deadObject);
#else
   void operator delete(void* deadObject, size_t size);
#endif

   // file read / write routines for saved games
   bool readData(FileReader& f);
   bool writeData(FileWriter& f) const;
};

class BAI_StrObjList : public SList<BAI_StrObj> {
public:

BAI_StrObj* add(const BattleMeasure::HexCord& hex, BattleOrderInfo::Posture pos)
{
   BAI_StrObj* so = new BAI_StrObj(hex, pos);
   ASSERT(so);

   append(so);
   return so;
}

BAI_StrObj* insert(const BattleMeasure::HexCord& hex, BattleOrderInfo::Posture pos)
{
   BAI_StrObj* so = new BAI_StrObj(hex, pos);
   ASSERT(so);

   SList<BAI_StrObj>::insert(so);
   return so;
}

// file read / write routines for saved games
bool readData(FileReader& f);
bool writeData(FileWriter& f) const;

};
#else
class BAI_StrObjList {
   public:
      enum {
         None
      };
};
#endif
}; // end namespace WG_BattleAI_Internal

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
