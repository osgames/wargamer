/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle AI:
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "batai.hpp"
#include "aib_side.hpp"
#include "aib_log.hpp"
#include "batmsg.hpp"
#include "bairestr.hpp"
// #include "string.hpp"
#include "filebase.hpp"
#include "filecnk.hpp"

namespace LUtils {
const char s_dot = '.';
const char s_null = '\0';
const char* s_ext = "hbb";
const char* s_hext = "wgb";

bool isHistorical(const char* title)
{
   const char* c = title;
   while(*c != s_dot && *c != s_null)
      c++;

   if(*c == s_null)
      return False;

   c++;
   const char* ce = s_hext;
   int loop = 0;
   while(*c != s_null)
   {
      if(loop++ >= 3)
         return False;

      int ch = tolower(*c);
      if(ch != *ce)
         return False;

      c++;
      ce++;
   }

   return True;
}

bool convertToBinaryScriptFile(String& scriptFile, const char* title)
{
   const char* c = title;
   while(*c != s_dot && *c != s_null)
   {
      scriptFile += *c;
      c++;
   }

   if(*c == s_null)
      return False;

   scriptFile += s_dot;
   for(int i = 0; i < 3; i++)
   {
      scriptFile += s_ext[i];
   }

   return True;
}
};

namespace WG_BattleAI_Internal
{

class BattleAIImp
{
        // Disable Copying
            BattleAIImp(const BattleAIImp&);
            BattleAIImp& operator = (const BattleAIImp&);

      public:

            BattleAIImp(const BattleData* batData);
            BattleAIImp(const BattleData* batData, String title);
            ~BattleAIImp();

            void add(Side s);
            void remove(Side s);
            bool has(Side s) const;

            void kill();
                  // Remove all AIs

            void sendMessage(const BattleMessageInfo& msg);

            void doDeploy();
            void doDeploy(Side s);
            void start();

            void endDay();
            void newDay();

            bool convertScript(const char* battleName);

            // file interface
            bool readData(FileReader& f);
            bool writeData(FileWriter& f) const;
      private:
            BattleSideAI* find(Side side);
            const BattleSideAI* find(Side side) const;

      private:

            // bool d_paused;
            const BattleData* d_batData;
            SList<BattleSideAI> d_container;
            String d_title;
};



/*
 * Constructor
 */

BattleAIImp::BattleAIImp(const BattleData* batData, String title) :
      d_container(),
      d_batData(batData),
      d_title(title)
      // d_paused(true)
{
#ifdef DEBUG
    bataiLog.printf("BattleAI constructed");
#endif
#if 0
      if(restrictiveTerrain()->size() == 0)
         initRestrictiveTerrain(batData);
#endif
}

BattleAIImp::BattleAIImp(const BattleData* batData) :
      d_container(),
      d_batData(batData),
      d_title()
      // d_paused(true)
{
#ifdef DEBUG
    bataiLog.printf("BattleAI constructed");
#endif
}

/*
 * Destructor
 */

BattleAIImp::~BattleAIImp()
{
#ifdef DEBUG
    bataiLog.printf("BattleAI destructed");
#endif
}

/*
 * Find the AI for a given side
 */

BattleSideAI* BattleAIImp::find(Side side)
{
      SListIter<BattleSideAI> iter(&d_container);
      while(++iter)
      {
            BattleSideAI* ai = iter.current();
            if(ai->side() == side)
                  return ai;
      }
      return 0;
}

const BattleSideAI* BattleAIImp::find(Side side) const
{
      SListIterR<BattleSideAI> iter(&d_container);
      while(++iter)
      {
            const BattleSideAI* ai = iter.current();
            if(ai->side() == side)
                  return ai;
      }
      return 0;
}

bool BattleAIImp::has(Side side) const
{
      SListIterR<BattleSideAI> iter(&d_container);
      while(++iter)
      {
            const BattleSideAI* ai = iter.current();
            if(ai->side() == side)
                  return True;
      }
      return False;
}

/*
 * Create AI for side
 * Note, it must be started later with ai->start();
 */

void BattleAIImp::add(Side side)
{
      BattleSideAI* ai = find(side);

      if(!ai)
      {
#ifdef DEBUG
         bataiLog.printf("BattleAI add side(%d)", int(side));
#endif

         ai = new BattleSideAI(side, d_batData, d_title);
         d_container.append(ai);
      }

      ASSERT(ai);

      // return ai;
}

/*
 * Remove AI for side
 */

void BattleAIImp::remove(Side side)
{
#ifdef DEBUG
    bataiLog.printf("BattleAI remove side(%d)", int(side));
#endif
    SListIter<BattleSideAI> iter(&d_container);
    while(++iter)
    {
        BattleSideAI* ai = iter.current();
        if(ai->side() == side)
        {
            iter.remove();
            return;
            }
    }
    FORCEASSERT("Side not in AI");
}


bool BattleAIImp::convertScript(const char* battleName)
{
   if(LUtils::isHistorical(battleName))
   {
      String aiFile;
      if(!LUtils::convertToBinaryScriptFile(aiFile, battleName))
      {
         GeneralError("Error converting to script file");
         return False;
      }
      else
      {
         const char* name = aiFile.c_str();
         WinFileBinaryChunkWriter file(aiFile.c_str());
         for(int s = 0; s < 2; s++)
         {
            BattleSideAI(s, d_batData, battleName, file);
         }
      }

      return True;
   }
   else
      return False;
}

/*
 * Kill all AIs
 */

void BattleAIImp::kill()
{
#ifdef DEBUG
    bataiLog.printf("BattleAI kill");
#endif
    d_container.reset();
}

/*
 * Process game Information message
 */


void BattleAIImp::sendMessage(const BattleMessageInfo& msg)
{
#ifdef DEBUG
    bataiLog.printf("BattleAI sendMessage");
#endif
    BattleSideAI* ai = find(msg.side());
    ASSERT(ai);
    if(ai)
        ai->sendMessage(msg);
}

template<class T, class F>
inline for_each(SList<T>* list, F func)
{
    SListIter<T> iter(list);
    while(++iter)
        func(iter.current());
}


void BattleAIImp::doDeploy()
{
#ifdef DEBUG
    bataiLog.printf("BattleAI doDeploy");
#endif
#if 0
      struct Deploy
      {
            void operator()(BattleSideAI* ai) { ai->doDeploy(); }
      };

      for_each(&d_container, Deploy());
#endif
      SListIter<BattleSideAI> iter(&d_container);
      while(++iter)
      {
            BattleSideAI* ai = iter.current();
            ai->doDeploy();
      }
}

void BattleAIImp::doDeploy(Side s)
{
   BattleSideAI* ai = find(s);
   if(ai)
      ai->doDeploy();
}

void BattleAIImp::start()
{
#ifdef DEBUG
      bataiLog.printf("BattleAI start");
#endif
#if 0
      struct Start
      {
            void operator()(BattleSideAI* ai) { ai->start(); }
      };

      for_each(&d_container, Start());
#endif
      SListIter<BattleSideAI> iter(&d_container);
      while(++iter)
      {
            BattleSideAI* ai = iter.current();
            ai->start();
    }
}

void BattleAIImp::endDay()
{
   SListIter<BattleSideAI> iter(&d_container);
   while(++iter)
   {
      BattleSideAI* ai = iter.current();
      ai->endDay();
   }
}

void BattleAIImp::newDay()
{
   SListIter<BattleSideAI> iter(&d_container);
   while(++iter)
   {
      BattleSideAI* ai = iter.current();
      ai->newDay();
   }
}


static UWORD s_fileVersion = 0x0000;
bool BattleAIImp::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_fileVersion);

   UBYTE nSides;

   f >> nSides;
   ASSERT(nSides <= 2);
   while(nSides--)
   {
      BattleSideAI* ai = new BattleSideAI(d_batData);
      ASSERT(ai);
      ai->readData(f);
      d_container.append(ai);
   }

   return f.isOK();
}

bool BattleAIImp::writeData(FileWriter& f) const
{
   f << s_fileVersion;

   // count sides
   UBYTE nSides = static_cast<UBYTE>(d_container.entries());

   ASSERT(nSides <= 2);
   f << nSides;

   SListIterR<BattleSideAI> iter(&d_container);
   while(++iter)
   {
      iter.current()->writeData(f);
   }

   return f.isOK();
}

};      // namespace WG_BattleAI_Internal


/*
 * Interface class
 */


BattleAI::~BattleAI()
{
    delete d_imp;
}

BattleAI::BattleAI(const BattleData* batData) :
    d_imp(new BattleAIImp(batData))
{
}

BattleAI::BattleAI(const BattleData* batData, String& title) :
    d_imp(new BattleAIImp(batData, title))
{
}

void BattleAI::add(Side s)
{
    d_imp->add(s);
}

void BattleAI::remove(Side s)
{
      d_imp->remove(s);
}

void BattleAI::kill()
{
      d_imp->kill();
}

void BattleAI::sendMessage(const BattleMessageInfo& msg)
{
      d_imp->sendMessage(msg);
}

void BattleAI::doDeploy()
{
      d_imp->doDeploy();
}

void BattleAI::doDeploy(Side s)
{
      d_imp->doDeploy(s);
}

void BattleAI::start()
{
      d_imp->start();
}

bool BattleAI::convertScript(const char* battleName)
{
   return d_imp->convertScript(battleName);
}

bool BattleAI::has(Side s) const
{
      return d_imp->has(s);
}

bool BattleAI::readData(FileReader& f)
{
   return d_imp->readData(f);
}

bool BattleAI::writeData(FileWriter& f) const
{
   return d_imp->writeData(f);
}


void BattleAI::endDay()
{
   d_imp->endDay();
}

void BattleAI::newDay()
{
   d_imp->newDay();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
