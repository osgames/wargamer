/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef MIDUTIL_HPP
#define MIDUTIL_HPP

#include "bobdef.hpp"
#include "batcord.hpp"
//#include "batdata.hpp"

using BattleMeasure::HexPosition;
using BattleMeasure::HexCord;

class HexArea;
class BattleData;

namespace WG_BattleAI_Internal {
class MidUnitInfo;
class MU_DeployItem;

bool allHolding(const CRefBattleCP& corpCP);
bool frontHolding(MidUnitInfo& mi);
bool inBoundry(const CRefBattleCP& cp, const HexArea& objArea);
void clearAllMoves(MidUnitInfo& mi);
void clearMove(MU_DeployItem& di);
bool enemyInSight(const CRefBattleCP& cp, const BattleData* bd);
bool corpsInLine(MidUnitInfo& mi, bool frontOnly);
void setHoldOrder(MU_DeployItem& di);
void setAttachOrder(MU_DeployItem& di, const CRefBattleCP& cp);
void setMoveOrder(MU_DeployItem& di, HexCord hex, HexPosition::Facing facing, bool clearOld);
bool canDeploy(const BattleData* bd, MU_DeployItem& di, BOB_Definitions::CPFormation divFor);
}; // end namespace MidOpUtil

#endif

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
