/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef H_LDLIST_HPP
#define H_LDLIST_HPP

#ifndef __cplusplus
#error h_ldlist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hi Level Leader List
 *
 *----------------------------------------------------------------------
 */

#include "aib_info.hpp"
#include "hi_unit.hpp"
#include "contain.hpp"
#include <vector>

namespace WG_BattleAI_Internal
{

class HiUnitList;

class HiLeaderList : public GameInfoUser, public STLContainer<std::vector<HiLeaderInfo> >
{
    public:
        HiLeaderList(GameInfo* gameInfo, HiUnitList* units);

        void assignLeaders();

    private:
        HiUnitList* d_units;
};


};  // namespace WG_BattleAI_Internal

#endif /* H_LDLIST_HPP */

