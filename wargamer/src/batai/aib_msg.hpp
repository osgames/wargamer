/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_MSG_HPP
#define AIB_MSG_HPP

#ifndef __cplusplus
#error aib_msg.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Player Message Queue
 *
 *----------------------------------------------------------------------
 */

class BattleMessageInfo;
class FileReader;
class FileWriter;

namespace WG_BattleAI_Internal
{

class RealQueue;
class BattleMessageQueue
{
		public:
				BattleMessageQueue();
				~BattleMessageQueue();

				void push(const BattleMessageInfo& msg);

				bool isEmpty() const;
				void pop();
				const BattleMessageInfo& get() const;

				// file interface
				bool readData(FileReader& f);
				bool writeData(FileWriter& f) const;
		private:
				// TODO: Actually implement the queue!
				RealQueue* d_queue;
};



};  // namespace WG_BattleAI_Internal

#endif /* AIB_MSG_HPP */

