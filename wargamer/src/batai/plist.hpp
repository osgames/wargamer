/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef PLIST_HPP
#define PLIST_HPP

#ifndef __cplusplus
#error plist.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * List of Pointers
 *
 *----------------------------------------------------------------------
 */

#include "contain.hpp"
#include <vector>
// #include <algo.h>

template<class T>
class PtrList : 
        public STLContainer<std::vector<T*> >
{
    public:
        PtrList<T>() { }

        void insert(T* ptr) 
        {
            ASSERT(!isMember(ptr));
            Container::push_back(ptr); 
        }

        void insert(iterator it, T* ptr)
        {
            ASSERT(!isMember(ptr));
            Container::insert(it, ptr); 
        }

        void erase(T* ptr)
        {
            ASSERT(isMember(ptr));
            iterator it = std::find(begin(), end(), ptr);
            erase(it); 
        }

        void erase(iterator it) { Container::erase(it); }

        size_t size() const { return Container::size(); }

        T* operator[](size_t n) { return Container::operator[](n); }
        const T* operator[](size_t n) const { return Container::operator[](n); }

        bool isMember(T* ptr)
        {
#ifdef _MSC_VER
           using namespace std::rel_ops;
#endif

           return std::find(begin(), end(), ptr) != end();
        }

    private:
};


#endif /* PLIST_HPP */

