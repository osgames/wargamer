/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef AIB_AREA_HPP
#define AIB_AREA_HPP

#ifndef __cplusplus
#error aib_area.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hex Area
 *
 *----------------------------------------------------------------------
 */

#include "batcord.hpp"
#include <ostream>

class FileReader;
class FileWriter;

using BattleMeasure::HexCord;

/*
 * Define an area as 4 corners
 */

class HexArea
{
	 public:
		  HexArea() { }
		  HexArea(const HexCord& p1, const HexCord& p2, const HexCord& p3, const HexCord& p4);
		  HexArea(const HexArea& hexArea);
		  ~HexArea() { }

		  HexArea& operator = (const HexArea& area);
		  bool operator==(const HexArea& area) const;
		  bool operator<(const HexArea& area) const;

		  HexCord& operator[](int i) { checkIndex(i); return d_points[i]; }
		  const HexCord& operator[](int i) const { checkIndex(i); return d_points[i]; }

		  // file interface
		  bool readData(FileReader& f);
		  bool writeData(FileWriter& f) const;

	 private:

		  void checkIndex(int i) const
		  {
				ASSERT( (i >= 0) && (i < 4) );
		  }

		  HexCord d_points[4];
};

std::ostream& operator<<(std::ostream& str, const HexArea& area);


#endif /* AIB_AREA_HPP */


