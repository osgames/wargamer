/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *------------------------------  ----------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle AI Hi-level Objectives
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "aib_hiob.hpp"
#include "filebase.hpp"
#include "bobutil.hpp"
namespace WG_BattleAI_Internal
{

RealHiObjective::RealHiObjective(GameInfo* gameInfo, Mode mode) :
      d_list(),
      d_units(),
      d_planner(0),
      d_mode(mode),
      d_flags(GetNewObjective)
{
      d_planner = new MidLevelPlanner(gameInfo, this);
}

RealHiObjective::RealHiObjective(GameInfo* gameInfo) :
      d_list(),
      d_units(),
      d_planner(0),
      d_mode(HiObjective::FrontLine),
      d_flags(GetNewObjective)
{
      d_planner = new MidLevelPlanner(gameInfo, this);
}

RealHiObjective::~RealHiObjective()
{
      delete d_planner;
}

// void RealHiObjective::makePlanner(GameInfo* gameInfo)
// {
//     ASSERT(!d_planner);
//     d_planner = new MidLevelPlanner(gameInfo, this);
// }


HiObjectiveList::~HiObjectiveList()
{
      while(size())
      {
          RealHiObjective* ob = back();
          pop_back();
          delete ob;
      }
}

RealHiObjective* HiObjectiveList::create(GameInfo* gameInfo, HiObjective::Mode mode)
{
      push_back(new RealHiObjective(gameInfo, mode));
      RealHiObjective* obj = back();
      // obj->makePlanner(gameInfo);
      return obj;
}


void RealHiObjective::process()
{
      ASSERT(d_planner);
      if(d_planner)
            d_planner->process();
}

void RealHiObjective::procMessage(const BattleMessageInfo& msg)
{
      ASSERT(d_planner);
      if(d_planner)
            d_planner->procMessage(msg);
}

void RealHiObjective::sendCPAggPos()
{
   ASSERT(d_planner);
   if(d_planner)
      d_planner->sendCPAggPos();
}


static UWORD s_version = 0x0002;
bool RealHiObjective::readData(FileReader& f, OrderBattle* ob)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   d_list.readData(f);

   int items;
   f >> items;
   while(items--)
   {
      CPIndex cpi;
      f >> cpi;
      CRefBattleCP cp = BobUtility::cpIndexToCP(cpi, *ob);
      addUnit(cp);
   }

   d_planner->readData(f);

   UBYTE b;
   f >> b;
   d_mode = static_cast<Mode>(b);

   d_agPos.readData(f);

   if(version >= 0x0001)
   {
      if(version < 0x0002)
      {
         f >> b;
         d_flags = b;
      }
      else
         f >> d_flags;
   }

   return f.isOK();
}

bool RealHiObjective::writeData(FileWriter& f, OrderBattle* ob) const
{
   f << s_version;

   d_list.writeData(f);

   int items = d_units.size();
   f << items;
   for(UnitList::const_iterator it = d_units.begin();
       it != d_units.end();
       ++it)
   {
      CPIndex cpi = BobUtility::cpToCPIndex(const_cast<BattleCP*>((*it)), *ob);
      f << cpi;
   }

   d_planner->writeData(f);
   UBYTE b = static_cast<UBYTE>(d_mode);
   f << b;

   d_agPos.writeData(f);
   f << d_flags;
   return f.isOK();
}

static UWORD s_listVersion = 0x0000;
bool HiObjectiveList::readData(FileReader& f, GameInfo* gameInfo)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_listVersion);

   int items;
   f >> items;
   while(items--)
   {
      push_back(new RealHiObjective(gameInfo));
      RealHiObjective* obj = back();

      OrderBattle* ob = const_cast<OrderBattle*>(gameInfo->battleData()->ob()->ob());
      obj->readData(f, ob);
   }

   return f.isOK();
}

bool HiObjectiveList::writeData(FileWriter& f, const GameInfo* gameInfo) const
{
   f << s_listVersion;

   OrderBattle* ob = const_cast<OrderBattle*>(gameInfo->battleData()->ob()->ob());
   int items = Container::size();
   f << items;
   for(Container::const_iterator it = begin();
       it != end();
       ++it)
   {
     (*it)->writeData(f, ob);
   }

   return f.isOK();
}

#ifdef DEBUG
void RealHiObjective::logHistorical(GameInfo* gameInfo)
{
   gameInfo->log("---------------Objective");
   gameInfo->log("------------------------");

   for(std::list<HexArea>::const_iterator hi = d_list.begin();
       hi != d_list.end();
       ++hi)
   {
      gameInfo->log("Boundry --- left=(%d, %d), right=(%d, %d)",
         static_cast<int>((*hi)[0].x()), static_cast<int>((*hi)[0].y()),
         static_cast<int>((*hi)[1].x()), static_cast<int>((*hi)[1].y()));
   }

   d_planner->logHistorical();
}
#endif

};  // namespace WG_BattleAI_Internal

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
