/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#ifndef BA_AGPOS_HPP
#define BA_AGPOS_HPP

#include "batord.hpp"

class FileReader;
class FileWriter;

namespace WG_BattleAI_Internal
{
struct BAI_AggressPosStruct {
   BattleOrderInfo::Aggression d_aggression;
   BattleOrderInfo::Posture d_posture;

   BAI_AggressPosStruct() :
      d_aggression(BattleOrderInfo::Aggress_Default),
      d_posture(BattleOrderInfo::Posture_Default) {}

   bool readData(FileReader& f);
   bool writeData(FileWriter& f) const;
};

};
#endif
/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
