/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "baitable.hpp"

static const char s_updateZoneModifiersChunkName[] =        "UpdateZoneModifiers";
static const char s_considerRetreatModifiersChunkName[] =        "ConsiderRetreatModifiers";
static const char s_replaceDivModifiersChunkName[] =        "ReplaceDivModifiers";
static const char s_shouldSupportModifiersChunkName[] =        "ShouldSupportModifiers";
// TODO: this needs to be got from Scenario
const char* s_tableDataName = "nap1813\\baitable.dat";

class BattleAITablesImp {
    Table1D<SWORD> d_updateZoneModifiers;
    Table1D<SWORD> d_considerRetreatModifiers;
    Table1D<SWORD> d_replaceDivModifiers;
    Table1D<SWORD> d_shouldSupportModifiers;
public:

    const Table1D<SWORD>& updateZoneModifiers()
    {
      if(!d_updateZoneModifiers.isInitiated())
      {
        d_updateZoneModifiers.init(s_tableDataName, s_updateZoneModifiersChunkName);
      }

      return d_updateZoneModifiers;
    }
    const Table1D<SWORD>& considerRetreatModifiers()
    {
      if(!d_considerRetreatModifiers.isInitiated())
      {
        d_considerRetreatModifiers.init(s_tableDataName, s_considerRetreatModifiersChunkName);
      }

      return d_considerRetreatModifiers;
    }
    const Table1D<SWORD>& replaceDivModifiers()
    {
      if(!d_replaceDivModifiers.isInitiated())
      {
        d_replaceDivModifiers.init(s_tableDataName, s_replaceDivModifiersChunkName);
      }

      return d_replaceDivModifiers;
    }
    const Table1D<SWORD>& shouldSupportModifiers()
    {
      if(!d_shouldSupportModifiers.isInitiated())
      {
        d_shouldSupportModifiers.init(s_tableDataName, s_shouldSupportModifiersChunkName);
      }

      return d_shouldSupportModifiers;
    }
};

/*---------------------------------------------------------------------
 * Client access
 */

static BattleAITablesImp s_tables;

/*--------------------------------------------------
 * SPFormation change tables
 */

const Table1D<SWORD>& BattleAITables::updateZoneModifiers()
{
  return s_tables.updateZoneModifiers();
}
const Table1D<SWORD>& BattleAITables::considerRetreatModifiers()
{
  return s_tables.considerRetreatModifiers();
}
const Table1D<SWORD>& BattleAITables::replaceDivModifiers()
{
  return s_tables.replaceDivModifiers();
}
const Table1D<SWORD>& BattleAITables::shouldSupportModifiers()
{
  return s_tables.shouldSupportModifiers();
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
