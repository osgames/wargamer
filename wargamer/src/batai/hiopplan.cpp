/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "hiopplan.hpp"
#include "aib_info.hpp"
#include "aib_sobj.hpp"
#include "batdata.hpp"

namespace WG_BattleAI_Internal
{

bool Hi_OpPlan::canIssue(
   GameInfo* gi,
   Hi_OpPlan::Plan plan,
   int nZones)
{
   BattleOrderInfo::Posture pos = gi->cinc()->posture();
   const BAI_StrObjList& obl = gi->strObjectives();
// HexCord mapSize = gi->map()->getSize();

   HexCord bLeft;
   HexCord mSize;
   gi->battleData()->getPlayingArea(&bLeft, &mSize);
   HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());

   ASSERT( (nZones >= 1) && (nZones <= 3) );
   ASSERT(pos < BattleOrderInfo::Posture_HowMany);
   ASSERT(obl.entries() > 0);
   ASSERT(plan < Plan_HowMany);

   const BAI_StrObj* mainObj = const_cast<BAI_StrObjList*>(&obl)->first();

   enum {     // Is objective to left right or center
      ObjLeft,
      ObjCenter,
      ObjRight,
      ObjPosition_HowMany
   } objPos = ObjCenter;

   if(nZones > 1)
   {
      HexCord::Cord ourEdge = gi->whichEdge();
      if(nZones == 2)
      {
         HexCord::Cord center = (bLeft.x() + tRight.x()) / 2;
         if(ourEdge == 0)
            objPos = (mainObj->d_hex.x() < center) ? ObjLeft : ObjRight;
         else
            objPos = (mainObj->d_hex.x() > center) ? ObjLeft : ObjRight;
      }

      else
      {
         ASSERT(nZones == 3);
         HexCord::Cord aThird = mSize.x() / 3;
         if(ourEdge == 0)
         {
            objPos = (mainObj->d_hex.x() < (bLeft.x() + aThird)) ?  ObjLeft :
                         (mainObj->d_hex.x() <= (bLeft.x() + (aThird * 2))) ? ObjCenter : ObjRight;
         }
         else
         {
            objPos = (mainObj->d_hex.x() < (bLeft.x() + aThird)) ?  ObjRight :
                         (mainObj->d_hex.x() <= (bLeft.x() + (aThird * 2))) ? ObjCenter : ObjLeft;
         }
      }
   }

   enum {
      ZoneMax = 3
   };

   static const bool s_table[BattleOrderInfo::Posture_HowMany][ObjPosition_HowMany][ZoneMax][Plan_HowMany] = {
      // Def
      {
         { // ObjLeft
            { False, False, False, False, False, False, False, True,  False, False  }, // 1 Zone
            { False, False, False, False, False, False, False, False, True,  False  }, // 2 Zones
            { False, False, False, False, False, False, False, True,  True,  False  }, // 3 Zones
         },
         { // ObjCenter
            { False, False, False, False, False, False, False, True,  False, False  }, // 1 Zone
            { False, False, False, False, False, False, False, False, True,  True    }, // 2 Zones
            { False, False, False, False, False, False, False, True,  False, False  }, // 3 Zones
         },
         { // ObjRight
            { False, False, False, False, False, False, False, True,  False,  False  }, // 1 Zone
            { False, False, False, False, False, False, False, False, False,  True  }, // 2 Zones
            { False, False, False, False, False, False, False, True,  False,  True  }, // 3 Zones
         }
      },
      // Off
      {
         { // Left
            { False, False, False, False, True, True, True, False, False, False  }, // 1 Zone
            { True,  True,  False, False, True, True, True, False, False, False  }, // 2 Zones
            { True,  True,  True,  True,  True, True, True, False, False, False  }, // 3 Zones
         },
         { // Center
            { False, False, False, False, True, True, True, False, False, False  }, // 1 Zone
            { True,  True,  False, False, True, True, True, False, False, False  }, // 2 Zones
            { True,  True,  True,  True,  True, True, True, False, False, False  }, // 3 Zones
         },
         { // Right
            { False, False, False, False, True, True, True, False, False, False  }, // 1 Zone
            { True,  True,  False, False, True, True, True, False, False, False  }, // 2 Zones
            { True,  True,  True,  True,  True, True, True, False, False, False  }, // 3 Zones
         }
      }
   };

   return s_table[pos][objPos][nZones - 1][plan];
}


#ifdef DEBUG
const TCHAR* Hi_OpPlan::planName(Plan op)
{
   static const TCHAR* s_text[Plan_HowMany] = {
      "Single-Left Evelopement",
      "Single-Right Evelopement",
      "Double Evelopement",
      "Center Attack",
      "Broadfront Attack",
      "Echelon-Left Attack",
      "Echelon-Right Attack",
      "Centered Defense",
      "Left Defense",
      "Right Defense"
   };

   ASSERT(op < Plan_HowMany);
   return s_text[op];
}

#endif

#if 0
// derived planning classes------------------------------------------------

// Single-Left Envelopement
// -----------------------------
class OP_SingleLeft : public Hi_OpPlan {
public:
  OP_SingleLeft() {}
  ~OP_SingleLeft() {}

  // virtual functions from Hi_OpPlan
  bool allocZones(int nZones);
  bool allocUnits();
};

bool OP_SingleLeft::allocZones(int nZones)
{
  return True;
}

bool OP_SingleLeft::allocUnits()
{
  return True;
}

// Single-Right Envelopement
// -----------------------------
class OP_SingleRight : public Hi_OpPlan {
public:
  OP_SingleRight() {}
  ~OP_SingleRight() {}

  // virtual functions from Hi_OpPlan
  bool allocZones(int nZones);
  bool allocUnits();
};

bool OP_SingleRight::allocZones(int nZones)
{
  return True;
}

bool OP_SingleRight::allocUnits()
{
  return True;
}

// -----------------------------------------------
// Allocators for derived classes

Hi_OpPlan* Hi_OpPlan::alloc(Hi_OpPlan::Off_Plan op)
{
  switch(op)
  {
    case SingleLeft:
      return new OP_SingleLeft;
  
    case SingleRight:
      return new OP_SingleRight;
  }
  return 0;
} 
#endif
}; // end namespace

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
