/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Hi level Leader List
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "h_ldlist.hpp"

namespace WG_BattleAI_Internal
{

HiLeaderList::HiLeaderList(GameInfo* gameInfo, HiUnitList* units) : 
    GameInfoUser(gameInfo), 
    d_units(units) 
{
}


void HiLeaderList::assignLeaders()
{
#ifdef DEBUG
    gameInfo()->log("assignLeaders");
#endif
}

};  // namespace WG_BattleAI_Internal





/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
