/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "mid_op.hpp"
#include "aib_mid.hpp"
#include "aib_util.hpp"
#include "aib_area.hpp"
#include "m_cp.hpp"
#include "batmsg.hpp"
#include "bobutil.hpp"
#include "midutil.hpp"
#include "baitable.hpp"
#include "bobdef.hpp"
#include <math.h>

#ifdef DEBUG
#include "scenario.hpp"
#include "batlist.hpp"
#endif

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

using namespace BOB_Definitions;
using namespace BattleMeasure;

namespace LocalUtils {

bool inList(const CRefBattleCP& cp, const WG_BattleAI_Utils::LocalCPList& list)
{
    SListIterR<WG_BattleAI_Utils::LocalCP> iter(&list);
    while(++iter)
    {
         if(cp == iter.current()->d_cp)
             return True;
    }

    return False;
}

inline int squared(int v)
{
   return (v * v);
}

inline double getDist(const HexCord& srcHex, const HexCord& destHex)
{
   return sqrt(squared(destHex.x() - srcHex.x()) + squared(destHex.y() - srcHex.y()));
}

};

namespace WG_BattleAI_Internal
{


bool MidLevel_Op::checkForEnemyToFront()
{
//  int nHexes = 8 + gameInfo()->random().gauss(3);
    d_planner->d_targets.resetItems();
    d_planner->setFlag(MidLevelPlanner::NewEnemySighted, False);

#ifdef DEBUG
    gameInfo()->log("%s is searching for enemy to front", d_planner->d_cincDI.cp()->getName());
#endif
    const HexArea& objArea = d_planner->d_objective->area();
    bool hasTargets = False;
    for(MidUnitList::iterator it = d_planner->d_units->begin();
             it != d_planner->d_units->end();
             ++it)
    {
         MidUnitDeployMap& map = (*it).map();

         for(int c = 0; c < map.nFront(); c++)
         {
             MU_DeployItem& di = map.item(c, 0);
             if(!di.cp()->hasQuitBattle())
             {
                  BattleListIterR bliter(&di.cp()->targetList());
                  while(++bliter)
                  {
                     const BattleItem* bi = bliter.current();
                     if(!bi->d_cp->hasQuitBattle())
                     {
                        hasTargets = True;
                        if(inBoundry(bi->d_cp, objArea))
                        {
                           d_planner->enemyToFront(True);
                           if(!d_planner->d_targets.addFireValue(bi->d_cp, bi->d_range, bi->d_fireValue, bi->d_position))
                           {
#ifdef DEBUG
                              gameInfo()->log("New enemy sighted %s", bi->d_cp->getName());
#endif
                              d_planner->setFlag(MidLevelPlanner::NewEnemySighted, True);
                           }
                        }

                     }
                  }
             }
         }
    }

    d_planner->d_targets.cleanUpList();
    d_planner->d_targets.moveUpCloseUnit();
#ifdef DEBUG
    if(d_planner->d_targets.entries() > 0)
    {
       CRefBattleCP objLeaderCP = objLeader();
       gameInfo()->log("\nTarget List for obj (%s's %s)", objLeaderCP->leader()->getName(), objLeaderCP->getName());
       gameInfo()->log("---------------------------------");
       BattleListIterR bliter(&d_planner->d_targets);
       while(++bliter)
       {
          const BattleItem* bi = bliter.current();
             gameInfo()->log("%s %s (%s) at %d yards, to the %s",
             scenario->getSideName(bi->d_cp->getSide()),
             bi->d_cp->getName(),
             bi->d_cp->leader()->getName(),
             static_cast<int>(bi->d_range),
             BattleItem::positionText(bi->d_position));
       }
       gameInfo()->log("End of Target List-----------------------\n");
       gameInfo()->log("Close enemy is %d hexes away", static_cast<int>(d_planner->d_targets.closeRangeFront()));
    }
#endif
    if(hasTargets)
      d_planner->d_flags |= MidLevelPlanner::NoCheckAlignment;
    else
      d_planner->d_flags &= ~MidLevelPlanner::NoCheckAlignment;
    return (d_planner->d_targets.entries() > 0);
}

bool MidLevel_Op::checkFlanks()
{
   MidUnitInfo& mi = d_planner->d_units->front();
#ifdef DEBUG
   gameInfo()->log("Checking flanks for %s", mi.cp()->getName());
#endif

   // clear old Flags
   d_planner->d_objective->flankedLeft(False);
   d_planner->d_objective->flankedRight(False);
   d_planner->d_objective->flankedRear(False);

   MidUnitDeployMap& map = mi.map();
   for(int r = 0; r < 2; r++)
   {
      int nCols = (r == 0) ? map.nFront() : map.nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di = map.item(c, r);
         if(di.cp()->hasQuitBattle())
            continue;

         // clear old Flanking flags
         di.flankedLeft(False);
         di.flankingLeft(False);
         di.flankedRight(False);
         di.flankingRight(False);
         di.flankedRear(False);

#ifdef DEBUG
         gameInfo()->log("----- checking %s", di.cp()->getName());
#endif
         SListIterR<BattleItem> iter(&di.cp()->targetList());
         while(++iter)
         {
            CRefBattleCP enemyCP = iter.current()->d_cp;
            CRefBattleCP eTargetCP = (enemyCP->targetList().entries() > 0) ?
               const_cast<BattleCP*>(enemyCP)->targetList().first()->d_cp : NoBattleCP;

            bool hasSupport = (eTargetCP != NoBattleCP &&
                               eTargetCP != di.cp() &&
                               enemyCP->targetList().closeRange() <= (4 * BattleMeasure::XYardsPerHex) &&
                               (!(eTargetCP->movingBackwards() || eTargetCP->routing() || eTargetCP->fleeingTheField()) ) );
#ifdef DEBUG
            if(hasSupport)
               gameInfo()->log("%s is supported by %s", di.cp()->getName(), eTargetCP->getName());
#endif
            switch(iter.current()->d_position)
            {
               case BattleItem::LeftFlank:
               case BattleItem::LeftRear:
                  if(c == 0)
                  {
                     if(hasSupport ||
                        iter.current()->d_cp->movingBackwards() ||
                        iter.current()->d_cp->routing() ||
                        iter.current()->d_cp->fleeingTheField())
                     {
                     }
                     else
                     {
#ifdef DEBUG
                        gameInfo()->log("----- obj is flanked left by %s", iter.current()->d_cp->getName());
#endif
                        d_planner->d_objective->flankedLeft(True);
                     }
                  }

                  if(iter.current()->d_range <= (6 * BattleMeasure::XYardsPerHex))
                  {
                     if(hasSupport)
                     {
#ifdef DEBUG
                        gameInfo()->log("----- %s is flanking right of %s",
                           di.cp()->getName(),
                           iter.current()->d_cp->getName());
#endif
                        di.flankingRight(True);
                     }
                     else
                     {
#ifdef DEBUG
                        gameInfo()->log("----- %s is flanked left by %s",
                           di.cp()->getName(),
                           iter.current()->d_cp->getName());
#endif
                        di.flankedLeft(True);
                     }
                  }

                  break;
               case BattleItem::RightFlank:
               case BattleItem::RightRear:
                  if(c == nCols - 1)
                  {
                     if(hasSupport ||
                        iter.current()->d_cp->movingBackwards() ||
                        iter.current()->d_cp->routing() ||
                        iter.current()->d_cp->fleeingTheField())
                     {
                     }
                     else
                     {
#ifdef DEBUG
                        gameInfo()->log("----- obj is flanked right by %s", iter.current()->d_cp->getName());
#endif
                        d_planner->d_objective->flankedRight(True);
                     }
                  }

                  if(iter.current()->d_range <= (6 * BattleMeasure::XYardsPerHex))
                  {

                     if(hasSupport)
                     {
#ifdef DEBUG
                        gameInfo()->log("----- %s is flanking left of %s",
                           di.cp()->getName(),
                           iter.current()->d_cp->getName());
#endif
                        di.flankingLeft(True);
                     }
                     else
                     {
#ifdef DEBUG
                        gameInfo()->log("----- %s is flanked right by %s",
                           di.cp()->getName(),
                           iter.current()->d_cp->getName());
#endif
                        di.flankedRight(True);
                     }
                  }

                  break;
               case BattleItem::Rear:
               {
                  int rows = (map.nRear() > 0) ? 2 : 1;
                  if(r == rows - 1)
                  {
                     if(!hasSupport)
                     {
#ifdef DEBUG
                        gameInfo()->log("----- obj is flanked to the rear by %s", iter.current()->d_cp->getName());
#endif
                        d_planner->d_objective->flankedRear(True);
                     }
                  }

                  if(iter.current()->d_range <= (6 * BattleMeasure::XYardsPerHex))
                  {

                     if(!hasSupport)
                     {
#ifdef DEBUG
                        gameInfo()->log("----- %s is flanked to the rear by %s",
                           di.cp()->getName(),
                           iter.current()->d_cp->getName());
#endif
                        di.flankedRear(True);
                     }
                  }

                  break;
               }
            }
         }
      }
   }

   if(d_planner->d_objective->flankedLeft() ||
      d_planner->d_objective->flankedRight() ||
      d_planner->d_objective->flankedRear())
   {
      d_planner->setFlag(MidLevelPlanner::ConsiderRetreat, True);
      return True;
   }
   else
      return False;
}

bool MidLevel_Op::engaged()
{
   for(MidUnitList::iterator it = d_planner->d_units->begin();
             it != d_planner->d_units->end();
             ++it)
   {
         MidUnitDeployMap& map = (*it).map();
         for(int r = 0; r < 2; r++)
         {
            int nCols = (r == 0) ? map.nFront() : map.nRear();
            for(int c = 0; c < nCols; c++)
            {
               MU_DeployItem& di = map.item(c, r);
               if(di.cp()->hasQuitBattle())
                  continue;

               if(di.cp()->shootingMuskets() ||
                  di.cp()->takingMusketHits() ||
                  di.cp()->inCloseCombat() ||
                  (di.cp()->takingGunHits() && di.cp()->targetList().closeRangeFront() <= (2 * BattleMeasure::XYardsPerHex)) )
               {
                  return True;
               }
            }
         }
   }

   return False;
}

MidLevel_Op::EnemyRatio MidLevel_Op::getOurStrVsEnemy()
{
   int ourStr = 0;
   int theirStr = 0;
   getOurStrVsEnemy(ourStr, theirStr);
   if(ourStr >= theirStr * 3)
      return Ratio_GreaterThan3;
   else if(ourStr >= theirStr * 2)
      return Ratio_GreaterThan2;
   else if(ourStr >= theirStr * 1.5)
      return Ratio_GreaterThan1_5;
   else if(ourStr >= theirStr * 1.2)
      return Ratio_GreaterThan1_2;
   else if(theirStr >= ourStr * 3)
      return Ratio_LessThan3;
   else if(theirStr >= ourStr * 2)
      return Ratio_LessThan2;
   else if(theirStr >= ourStr * 1.5)
      return Ratio_LessThan1_5;
   else if(theirStr >= ourStr * 1.2)
      return Ratio_LessThan1_5;
   else
      return Ratio_Equal;
}

bool MidLevel_Op::shouldReact()
{
   // React only if enemy is fairly close
   if(d_planner->d_targets.closeRange() > (6 * BattleMeasure::XYardsPerHex) ||
      d_planner->d_flags & MidLevelPlanner::Retreating ||
      d_planner->d_objective->reorienting())
   {
      return False;
   }

   // If we are already reacting we probably don;t want to react again
   // unless necesary
   int chance = 0;
   if(!(d_planner->d_flags & MidLevelPlanner::Reacting))
   {
      // a random chance we may react
      chance = gameInfo()->random(97, 99);

      // modify chance
      CRefBattleCP objLeaderCP = objLeader();

      // Leader aggression
      if(objLeaderCP->leader()->getAggression() >= 150)
         chance += 5;
      else if(objLeaderCP->leader()->getAggression() < 100)
         chance -= 5;

      // Leader initiative
      if(objLeaderCP->leader()->getInitiative() >= 150)
         chance += 5;
      else if(objLeaderCP->leader()->getInitiative() < 100)
         chance -= 5;

   }
   else
   {
      // modify chance from 0
      // if new enemy is sighted
      if(d_planner->d_flags & MidLevelPlanner::NewEnemySighted)
         chance += 5;

      // if targets have moved closer, or otherwise changed
      if(d_planner->d_targets.changed())
         chance += 2;

      if(d_planner->bombarding())
         chance -= 5;
      //if(rh == Retreat)
      //   chance += gameInfo()->random(50, 70);
   }

#ifdef DEBUG
   gameInfo()->log("Chance to react = %d", chance);
#endif
   return (gameInfo()->random(100) < chance);
}

bool MidLevel_Op::doEnemyToFront(bool alwaysCheck)
{
//  if(!(d_planner->d_oldFlags & MidLevelPlanner::EnemyToFront) || d_planner->d_targets.changed())
    if(alwaysCheck || (!engaged() && shouldReact()))
    {
#ifdef DEBUG
         gameInfo()->log("MidLevel_Op::doEnemyToFront()");
         gameInfo()->log(" ----- Enemy detected to front");
#endif
         Reactions rh = reactHow();
         d_planner->setFlag(MidLevelPlanner::Reacting, True);
         switch(rh)
         {
             case None:
                  return False;

             case AdvToFC:
             case AdvToCC:
             case AdvToBomb:
                  doAdvanceToEnemy(rh);
                  return True;

             case Halt:
                  doHold();
                  d_planner->setFlag(MidLevelPlanner::Reacting, False);
                  return False;

             case Retreat:
                  doRetreatFromEnemy(gameInfo()->random(2, 4));
                  return True;
         }
    }

    return False;
}

void MidLevel_Op::getOurStrVsEnemy(int& ourStr, int& enemyStr)
{
   ourStr = 0;
   enemyStr = 0;
   // get our total frontline str
   for(MidUnitList::iterator it = d_planner->d_units->begin();
             it != d_planner->d_units->end();
             ++it)
   {
         MidUnitDeployMap& map = (*it).map();

         for(int c = 0; c < map.nFront(); c++)
         {
             MU_DeployItem& di = map.item(c, 0);
             if(di.cp()->hasQuitBattle())
                continue;

             ourStr += di.cp()->lastStrength();
         }
   }

   // total enemy
   SListIterR<BattleItem> iter(&d_planner->d_targets);
   while(++iter)
   {
      const BattleItem* bi = iter.current();
      if(bi->d_range <= (5 * BattleMeasure::XYardsPerHex))
         enemyStr += bi->d_cp->lastStrength();
   }
}

void MidLevel_Op::getPrecentOfStartMorale(int& perOfStart)
{
   perOfStart = 0;

   int totalCur = 0;
   int totalStart = 0;
   // get our total frontline str
   for(MidUnitList::iterator it = d_planner->d_units->begin();
             it != d_planner->d_units->end();
             ++it)
   {
         MidUnitDeployMap& map = (*it).map();
         for(int r = 0; r < 2; r++)
         {
            int nCols = (r == 0) ? map.nFront() : map.nRear();
            for(int c = 0; c < nCols; c++)
            {
                MU_DeployItem& di = map.item(c, r);
                totalCur += di.cp()->morale();
                totalStart += di.cp()->startMorale();
            }
         }
   }

   if(totalStart > 0)
      perOfStart = (totalCur * 100) / totalStart;
}

// Should this XX be supported?
bool MidLevel_Op::shouldSupport(MU_DeployItem& di)
{
   RandomNumber& lrand = gameInfo()->random();

   const Table1D<SWORD>& mTable = BattleAITables::shouldSupportModifiers();
   enum {
      MoraleDiv,
      Retreating,
      Wavering,
      Flanked,
      CloseCombat,
      FireCombat,
      Disorder,
      Enemy31,
      Enemy21,
      Enemy151,
      Enemy121,
      EnemyEqual,
      CriticalLoss,
      NoEffectCutoff,
      MoraleLess65,
      MoraleLess100
   };

   int percentOfStart = (di.cp()->startMorale() > 0) ?
      minimum(100, (di.cp()->morale() * 100) / di.cp()->startMorale()) : 0;
   int chance = (mTable.getValue(MoraleDiv) > 0) ? (100 - percentOfStart) / mTable.getValue(MoraleDiv) : 0;

#ifdef DEBUG
   gameInfo()->log("\n--------------------");
   gameInfo()->log("Testing %s for needed support", di.cp()->getName());
   gameInfo()->log("%% of Start-Morale = %d, Raw chance = %d", percentOfStart, chance);
#endif

   // retreating
   if(di.cp()->movingBackwards())
   {
      chance += mTable.getValue(Retreating);
#ifdef DEBUG
      gameInfo()->log("------ chance after retreating modifier %d", chance);
#endif
   }

   if(di.cp()->wavering() || di.cp()->shaken())
   {
      chance += mTable.getValue(Wavering);
#ifdef DEBUG
      gameInfo()->log("------ chance after wavering modifier %d", chance);
#endif
   }

   if(di.flankedLeft() ||
      di.flankedRight() ||
      di.flankedRear())
   {
      chance += mTable.getValue(Flanked);
#ifdef DEBUG
      gameInfo()->log("------ chance after flanked modifier %d", chance);
#endif
   }

   if(di.cp()->inCloseCombat())
   {
      chance += mTable.getValue(CloseCombat);
#ifdef DEBUG
      gameInfo()->log("------ chance after close-combat modifier %d", chance);
#endif
   }
   else if(di.cp()->takingMusketHits())
   {
      chance += mTable.getValue(FireCombat);
#ifdef DEBUG
      gameInfo()->log("------ chance after musket-hits modifier %d", chance);
#endif
   }

   chance += (mTable.getValue(Disorder) * (3 - di.cp()->disorder()));
#ifdef DEBUG
   gameInfo()->log("------ chance after disorder modifier %d", chance);
#endif

   // if unit is larger
   int ourStr = 0;
   int enemyStr = 0;
   getOurStrVsEnemy(ourStr, enemyStr);

   if(enemyStr >= ourStr * 3)
      chance += mTable.getValue(Enemy31);//30;
   else if(enemyStr >= ourStr * 2)
      chance += mTable.getValue(Enemy21);
   else if(enemyStr >= ourStr * 1.5)
      chance += mTable.getValue(Enemy151);
   else if(enemyStr >= ourStr * 1.2)
      chance += mTable.getValue(Enemy121);
   else if(enemyStr >= ourStr)
      chance += mTable.getValue(EnemyEqual);

#ifdef DEBUG
   gameInfo()->log("------ chance after comparative strength modifier %d", chance);
#endif

   // if critical loss reached
   if(di.cp()->criticalLossReached())
   {
      chance += mTable.getValue(CriticalLoss);
#ifdef DEBUG
      gameInfo()->log("------ chance after critical loss modifier %d", chance);
#endif
   }

   else if(di.cp()->noEffectCutoffReached())
   {
      chance += mTable.getValue(NoEffectCutoff);
#ifdef DEBUG
      gameInfo()->log("------ chance after no-effect-cutoff reached modifier %d", chance);
#endif
   }

   // for morale
   if(di.cp()->morale() <= 65)
   {
      chance += mTable.getValue(MoraleLess65);
   }
   else if(di.cp()->morale() <= 100)
   {
      chance += mTable.getValue(MoraleLess100);
   }


   // see if we need to send this guy back
   if(di.cp()->movingBackwards())
   {
      int nChance = 100 - chance;
      if(lrand(100) < nChance)
      {
#ifdef DEBUG
         gameInfo()->log("%s is retreating but we are sending him back into the frey", di.cp()->getName());
#endif
         setMoveOrder(di, di.objHex(), di.cp()->facing(), True);
         di.order().aggression(static_cast<BattleOrderInfo::Aggression>(minimum(3, di.order().aggression() + 1)));
      }
   }

#ifdef DEBUG
   gameInfo()->log("------ chance after morale loss modifier %d", chance);

   bool result = (lrand(100) < chance);
   gameInfo()->log("Final chance = %d, result = %s",
       chance, (result) ? "will be supported" : "no support");
   return result;
#else
   return (lrand(100) < chance);
#endif
}

// Should this XX be replaced?
bool MidLevel_Op::shouldReplace(MU_DeployItem& di)
{
   // I'm just sorta making this up as I go along
   RandomNumber& lrand = gameInfo()->random();
#ifdef DEBUG
   gameInfo()->log("Testing if %s is to be replaced", di.cp()->getName());
#endif
   const Table1D<SWORD>& mTable = BattleAITables::replaceDivModifiers();
   enum {
      MoraleDiv,
      Retreating,
      Routing,
      CriticalLoss,
      Wavering,
      Disorder,
      MoraleLess65,
      MoraleLess100,
      MoraleLess150,
      MoraleOver150,
      NoAggChange,
      NoDisorder
   };

   int percentOfStart = (di.cp()->startMorale() > 0) ?
      (di.cp()->morale() * 100) / di.cp()->startMorale() : 0;
   int chance = (mTable.getValue(MoraleDiv) > 0) ? (100 - percentOfStart) / mTable.getValue(MoraleDiv) : 0;


   // If retreating or routing or fleeing the field, replace
   if(di.cp()->routing() ||
      di.cp()->fleeingTheField())
   {
      chance += mTable.getValue(Routing);
   }
   else if(di.cp()->movingBackwards())
   {
      chance += mTable.getValue(Retreating);
   }

   // If shaken, maybe replace
   if(di.cp()->criticalLossReached())
      chance += mTable.getValue(CriticalLoss);

   if(di.cp()->wavering() || di.cp()->shaken())
      chance += mTable.getValue(Wavering);

   chance += (mTable.getValue(Disorder) * (3 - di.cp()->disorder()));

   if(di.cp()->noEffectCutoffReached())
   {
      if(di.cp()->morale() < 65)
         chance += mTable.getValue(MoraleLess65);//10;
      else if(di.cp()->morale() < 100)
         chance += mTable.getValue(MoraleLess100);//5;
      else if(di.cp()->morale() < 150)
         chance += mTable.getValue(MoraleLess150);
      else
         chance += mTable.getValue(MoraleOver150);//10;
   }

   if(di.cp()->aggression() == di.cp()->getCurrentOrder().aggression())
      chance += mTable.getValue(NoAggChange);//-10;

   if(di.cp()->disorder() == 3)
      chance += mTable.getValue(NoDisorder);

   bool replaced = (lrand(100) < chance);

   return replaced;
}

inline int ab_Value(int v)
{
   return (v >= 0) ? v : -v;
}

// Can we, or should       we bring up this unit to the front
bool MidLevel_Op::canBringUp(MU_DeployItem& di)//const CRefBattleCP& cp)
{

   CRefBattleCP cp = di.cp();
#ifdef DEBUG
   gameInfo()->log("Testing %s to see we can bring him up to the front-line", cp->getName());
#endif
   if(di.attacking())
   {
#ifdef DEBUG
      gameInfo()->log("Failed. Already attacking");
#endif
      return False;
   }
   // if its already retreating we cannot
   if(cp->hasQuitBattle() ||
      cp->movingBackwards() ||
      cp->routing() ||
      cp->fleeingTheField())
   {
#ifdef DEBUG
      gameInfo()->log("Failed. Already retreating");
#endif
      return False;
   }

   int chance = gameInfo()->random(50, 70);//((cp->leader()->getAggression() + cp->leader()->getCharisma() + cp->leader()->getInitiative() + gameInfo()->random(75, 180)) * 100) / 765;
#ifdef DEBUG
   gameInfo()->log("Raw chance = %d", chance);
#endif

   // modify for leader attributes
   int lValue = ((cp->leader()->getAggression() + cp->leader()->getCharisma() + cp->leader()->getInitiative()) * 100) / 765;
   chance += (lValue / 5);
#ifdef DEBUG
   gameInfo()->log("Chance after leader modifier = %d", chance);
#endif

   // if shaken
   chance += (cp->shaken() || cp->wavering()) ? -10 : 5;
#ifdef DEBUG
   gameInfo()->log("Chance after shaken-wavering modifier = %d", chance);
#endif

   // if critical loss has been reached
   if(cp->criticalLossReached())
      chance -= 20;
   else if(cp->noEffectCutoffReached())
      chance -= 5;
   else
      chance += 5;

#ifdef DEBUG
   gameInfo()->log("Chance after loss modifier = %d", chance);
#endif

   // morale
   if(cp->morale() < 65)
      chance -= 10;
   else if(cp->morale() < 100)
      chance -= 5;
   else if(cp->morale() < 130)
      ;
   else if(cp->morale() < 160)
      chance += 5;
   else
      chance += 10;


#ifdef DEBUG
   gameInfo()->log("Chance after morale modifier = %d", chance);
   bool moveUp = (gameInfo()->random(100) <= chance);
   gameInfo()->log("%s moving up. Final chance was %d",
         (moveUp) ? "Is" : "Is not", chance);
   return moveUp;
#else
   return (gameInfo()->random(100) <= chance);
#endif
}

MU_DeployItem* MidLevel_Op::findCloseType(
      BasicUnitType::value t,
      MidUnitDeployMap& map,
      int col,
      int& newC)
{
   int bestDist = -1;
   newC = -1;
   for(int c = 0; c < map.nRear(); c++)
   {
      MU_DeployItem& di = map.item(c, 1);
      if(di.cp()->hasQuitBattle())
         continue;

      if(di.cp()->generic()->isInfantry() && t == BasicUnitType::Infantry ||
          di.cp()->generic()->isCavalry() && t == BasicUnitType::Cavalry ||
          di.cp()->generic()->isArtillery() && t == BasicUnitType::Artillery)
      {
         if( (di.ordered()) &&
               (bestDist == -1 || ab_Value(c - col) < bestDist) &&
               (canBringUp(di)) )
         {
            newC = c;
            bestDist = ab_Value(c - col);
         }
      }
   }

   if(newC != -1 && map.nRear() > 0 && newC < map.nRear())
   {
      MU_DeployItem& di = map.item(newC, 1);
#ifdef DEBUG
      gameInfo()->log("Bringing up %s to support", di.cp()->getName());
#endif
      return &di;
   }
   else
      return 0;
}

bool MidLevel_Op::needsNewOrder(MU_DeployItem& di, const HexCord& destHex, HexPosition::Facing facing)
{
   if(di.cp()->hasQuitBattle() ||
      //di.bombarding() ||
       (di.order().wayPoints().entries() > 0 &&
        di.order().wayPoints().getLast()->d_hex == destHex &&
        di.order().wayPoints().getLast()->d_order.d_facing == facing))
   {
      return False;
   }

   return True;
}

bool MidLevel_Op::findSupport(MidUnitDeployMap& map, MU_DeployItem& di, int col)
{
   RandomNumber& lrand = gameInfo()->random();
   const HexArea& objArea = d_planner->d_objective->area();
   HexPosition::Facing ourFace = WG_BattleAI_Utils::frontFacing(objArea[0], objArea[1]);

   // Get state of our flank
   int cRange = 6 * BattleMeasure::XYardsPerHex;
   const BattleList& tl = di.cp()->targetList();
   enum {
      EnemyOnRight,
      EnemyOnLeft,
      EnemyOnRear,
      FS_None
   } flankState = (di.flankedRight()) ? EnemyOnRight :
                  (di.flankedLeft())  ? EnemyOnLeft :
                  (di.flankedRear())  ? EnemyOnRear : FS_None;

   int newCol = -1;
   BasicUnitType::value bt1;// = (rvalue <= 60) ? BasicUnitType::Cavalry : BasicUnitType::Infantry;
   BasicUnitType::value bt2;// = (bt1 == BasicUnitType::Cavalry) ? BasicUnitType::Infantry : BasicUnitType::Cavalry;
   BasicUnitType::value bt3;// = BasicUnitType::Artillery;
   int rvalue = gameInfo()->random(100);
   if(!(flankState == FS_None))     // silly Visual C++ template problem with !=
   {
      bt1 = (rvalue <= 60) ? BasicUnitType::Cavalry : BasicUnitType::Infantry;
      bt2 = (bt1 == BasicUnitType::Cavalry) ? BasicUnitType::Infantry : BasicUnitType::Cavalry;
      bt3 = BasicUnitType::Artillery;
   }
   else
   {
      bt1 = BasicUnitType::Artillery;
      bt2 = (rvalue <= 60) ? BasicUnitType::Cavalry : BasicUnitType::Infantry;
      bt3 = (bt2 == BasicUnitType::Cavalry) ? BasicUnitType::Infantry : BasicUnitType::Cavalry;
   }


   MU_DeployItem* supportDI = findCloseType(bt1, map, col, newCol);
   if(!supportDI)
   {
      supportDI = findCloseType(bt2, map, col, newCol);
      if(!supportDI)
         supportDI = findCloseType(bt3, map, col, newCol);
   }

   if(supportDI)
   {
      HexCord orderLeft;
      HexCord startHex;
      int moveHorz = 0;
      int moveRear = 0;

      // Support flank
      if(flankState != None)
      {
         // move up to support the flank
         moveHorz = (flankState == EnemyOnRight) ? 1 :
                    (flankState == EnemyOnLeft)  ? -supportDI->cp()->columns() : 0;
         moveRear = (flankState == EnemyOnRear) ? -2 : -3;

         startHex = (flankState == EnemyOnRight) ?
               di.cp()->rightHex() : di.cp()->leftHex();
      }

      // Otherwise, move up artillery
      else
      {
         enum { MoveToLeft, MoveToRight } moveWhichWay;

         // if we are on the left end, move to right of unit
         if(col == 0)
            moveWhichWay = MoveToRight;
         // if on right end, move to left
         else if(col == map.nFront() - 1)
            moveWhichWay = MoveToLeft;
         // otherwise go either way
         else
         {
            // for now do it randomly
            moveWhichWay = (lrand(100) < 50) ? MoveToLeft : MoveToRight;

            // TODO... A more thorugh search for a position
         }

         moveHorz = (moveWhichWay == MoveToLeft) ?
               -supportDI->cp()->columns() : 1;
         moveRear = -1;

         startHex = (moveWhichWay == MoveToLeft) ?
               di.cp()->leftHex() : di.cp()->rightHex();
      }

      if(((moveHorz == 0 && moveRear == 0) || WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), ourFace, moveHorz, moveRear, startHex, orderLeft)) &&
         //needsNewOrder(*supportDI, orderLeft, ourFace) &&
         launchSortie(supportDI, orderLeft, supportDI->cp()->leftHex(), ourFace))
      {
#if 0
         d_planner->d_objective->flankedLeft(False);
         d_planner->d_objective->flankedRight(False);
         d_planner->d_objective->flankedRear(False);
#endif
         return True;
      }
   }

   return False;
}

bool MidLevel_Op::findReplacement(MidUnitDeployMap& map, MU_DeployItem& di, int col)
{
   RandomNumber& lrand = gameInfo()->random();
   BasicUnitType::value bt;

#ifdef DEBUG
   gameInfo()->log("Finding replacement for %s", di.cp()->getName());
#endif
   if(di.cp()->generic()->isInfantry())
   {
      bt = (lrand(100) <= lrand(10, 40)) ? BasicUnitType::Infantry : BasicUnitType::Cavalry;
   }

   else if(di.cp()->generic()->isCavalry())
   {
      bt = (lrand(100) <= lrand(10, 40)) ? BasicUnitType::Cavalry : BasicUnitType::Infantry;
   }

   else
   {
      ASSERT(di.cp()->generic()->isArtillery());
      if(lrand(100) <= lrand(60, 80))
         bt = (lrand(100) <= lrand(40, 60)) ? BasicUnitType::Cavalry : BasicUnitType::Infantry;
      else
         bt = BasicUnitType::Artillery;
   }

   int loops = 0;
   MU_DeployItem* newDI = 0;
   int newCol = -1;
   do
   {
      switch(bt)
      {
         case BasicUnitType::Infantry:
         {
            newDI = findCloseType(bt, map, col, newCol);
            if(!newDI)
            {
               bt = BasicUnitType::Cavalry;
            }
            break;
         }
         case BasicUnitType::Cavalry:
         {
            newDI = findCloseType(bt, map, col, newCol);
            if(!newDI)
            {
               bt = BasicUnitType::Infantry;
            }
            break;
         }
         case BasicUnitType::Artillery:
         {
            newDI = findCloseType(bt, map, col, newCol);
            if(!newDI)
               loops = 2;;
            break;
         }
      }

      if(++loops >= 2)
         break;

   } while(newDI == 0);

   if(newDI)
   {
      // swap
      ASSERT(newCol != -1);
      MU_DeployItem oldFrontDI = map.item(col, 0);
      MU_DeployItem& frontDI = map.item(col, 0);
      MU_DeployItem oldRearDI = map.item(newCol, 1);
      MU_DeployItem& rearDI = map.item(newCol, 1);

      frontDI = oldRearDI;
      rearDI = oldFrontDI;

      HexCord rLeft = rearDI.cp()->leftHex();
      HexCord fLeft = frontDI.cp()->leftHex();

      //clearMove(frontDI);
      //clearMove(rearDI);

      //frontDI.ordered(False);
      //rearDI.ordered(False);

#ifdef DEBUG
      gameInfo()->log("%s (c%d), is replacing %s(c%d)",
         frontDI.cp()->getName(),
         newCol,
         rearDI.cp()->getName(),
         col);
#endif

      //WG_BattleAI_Utils::setMoveOrder(&frontDI.order(), rLeft, rearDI.cp()->facing());
      launchSortie(&frontDI, fLeft, fLeft, frontDI.cp()->facing());
      //WG_BattleAI_Utils::setMoveOrder(&rearDI.order(), fLeft, frontDI.cp()->facing());
      setMoveOrder(rearDI, fLeft, frontDI.cp()->facing(), True);

      if(frontDI.cp()->formation() < CPF_Massed)
      {
          frontDI.order().divFormation(CPF_Massed);
          frontDI.order().order().d_whatOrder.add(WhatOrder::CPFormation);
      }

      d_planner->moveUpRear(True);
      return True;
   }
   else
   {
      d_planner->considerRetreat(True);
      return False;
   }
}

bool MidLevel_Op::considerRetreat()
{
   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle() || d_planner->d_flags & MidLevelPlanner::Retreating)
      return False;

   CRefBattleCP leaderCP = objLeader();
   const Table1D<SWORD>& mTable = BattleAITables::considerRetreatModifiers();
   enum {
       LeaderValue,
       Agg0,
       Agg1,
       Agg2,
       Agg3,
       ForLosses,
       Wavering,
       Routing,
       CriticalLoss,
       NoEffectCutoff,
       LowLosses,
       MoraleLess65,
       MoraleLess100,
       MoraleOver180,
       MoraleOver150,
       LeaderCharismaOver180,
       LeaderCharismaOver140
   };

   // chance of retreating is converse of leaders charisma ond aggression
   int v = 512 - (leaderCP->leader()->getAggression() + leaderCP->leader()->getCharisma());
   int chance = 0;
   if(mTable.getValue(LeaderValue) > 0)
      chance += ((v * 100) / 512) / mTable.getValue(LeaderValue);

#ifdef DEBUG
   gameInfo()->log("%s's %s is considering retreat, raw chance = %d",
         leaderCP->getName(), leaderCP->leader()->getName(), chance);
#endif
   // modify for number of units retreating
   int nRetreating = 0;
   int nUnits = 0;
   for(int r = 0; r < 2; r++)
   {
      int nCols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di = mi.map().item(c, r);
         if(di.cp()->hasQuitBattle())
            continue;

         nUnits++;
         if(di.cp()->movingBackwards() || di.cp()->routing() || di.cp()->fleeingTheField())
            nRetreating++;
      }
   }

   if(nRetreating < (nUnits + 2) / 4)
      chance -= 10;//+= mTable.getValue(OverHalfRetreating);
   else if(nRetreating < (nUnits + 1) / 2)
      chance -= 5;

   // modify chance for leader aggression
   BattleOrderInfo::Aggression aggress = leaderCP->aggression();
   ASSERT(aggress < BattleOrderInfo::Aggress_HowMany);
   const int s_aggMod[BattleOrderInfo::Aggress_HowMany] = {
         mTable.getValue(Agg0), mTable.getValue(Agg1), mTable.getValue(Agg2), mTable.getValue(Agg3)
   };

    chance += s_aggMod[aggress];

    // modify for shape of our units
    for(MidUnitList::iterator it = d_planner->d_units->begin();
          it != d_planner->d_units->end();
          ++it)
    {
         // items we are checking for
         int startStr = 0;
         int curStr = 0;

         MidUnitDeployMap& map = (*it).map();
         for(int r = 0; r < 2; r++)
         {
             int nCols = (r == 0) ? map.nFront() : map.nRear();
             for(int c = 0; c < nCols; c++)
             {
                  MU_DeployItem& di = map.item(c, r);
                  if(di.cp()->hasQuitBattle())
                     continue;

                  startStr += di.cp()->startStrength();
                  curStr += di.cp()->lastStrength();

                  if(di.cp()->shaken() || di.cp()->wavering())
                     chance += mTable.getValue(Wavering);//1;
                  if(di.cp()->routing() || di.cp()->fleeingTheField())
                     chance += mTable.getValue(Routing);
                  if(di.cp()->criticalLossReached())
                     chance += mTable.getValue(CriticalLoss);
                  else if(di.cp()->noEffectCutoffReached())
                     chance += mTable.getValue(NoEffectCutoff);
                  else
                     chance += mTable.getValue(LowLosses);

                  if(di.cp()->morale() < 65)
                     chance += mTable.getValue(MoraleLess65);
                  else if(di.cp()->morale() < 100)
                     chance += mTable.getValue(MoraleLess100);
                  else if(di.cp()->morale() >= 180)
                     chance += mTable.getValue(MoraleOver180);
                  else if(di.cp()->morale() >= 150)
                     chance += mTable.getValue(MoraleOver150);

                  Attribute leaderCharisma = di.cp()->leader()->getCharisma();
                  if(leaderCharisma >= 180)
                     chance += mTable.getValue(LeaderCharismaOver180);
                  else if(leaderCharisma >= 140)
                     chance += mTable.getValue(LeaderCharismaOver140);
             }
         }

         int percentLoss = (startStr != 0) ? (curStr * 100) / startStr : 0;
         if(mTable.getValue(ForLosses) > 0)
            chance += percentLoss / mTable.getValue(ForLosses);
    }

#ifdef DEBUG
    gameInfo()->log("Modified chance = %d", chance);
#endif

    if(gameInfo()->random(100) <= chance)
    {
         // go through units and and set Retreat orders
         for(MidUnitList::iterator it = d_planner->d_units->begin();
               it != d_planner->d_units->end();
               ++it)
         {
#ifdef DEBUG
             gameInfo()->log("%s is retreating", (*it).cp()->getName());
#endif
             MidUnitDeployMap& map = (*it).map();
             for(int r = 0; r < 2; r++)
             {
                int nCols = (r == 0) ? map.nFront() : map.nRear();
                for(int c = 0; c < nCols; c++)
                {
                     MU_DeployItem& di = map.item(c, r);
                     if(di.cp()->hasQuitBattle())
                        continue;

                     HexCord leftHex = di.cp()->leftHex();
                     HexCord hex;
                     if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), di.cp()->facing(), 0, -(gameInfo()->random(4, 7)), leftHex, hex))// &&
//                       needsNewOrder(di, hex, di.cp()->facing()))
                     {
                        setMoveOrder(di, hex, di.cp()->facing(), True);
                     }
                }
             }
         }

         d_planner->d_flags |= MidLevelPlanner::Retreating;
         return True;
    }

    return False;
}

bool MidLevel_Op::moveUpRear()
// See if Corp reserves should move up to assist or replace front units
{
   if(d_planner->d_objective->flankedLeft() ||
      d_planner->d_objective->flankedRight() ||
      d_planner->d_objective->flankedRear())
   {
      return False;
   }

    // Go through front-line
    bool movingUp = False;
    for(MidUnitList::iterator it = d_planner->d_units->begin();
             it != d_planner->d_units->end();
             ++it)
    {
#ifdef DEBUG
         gameInfo()->log("\n--------------------------------");
         gameInfo()->log("Testing %s for bringing up reserves", (*it).cp()->getName());
#endif
         MidUnitDeployMap& map = (*it).map();

         for(int c = 0; c < map.nFront(); c++)
         {
             MU_DeployItem& di = map.item(c, 0);
             if(di.cp()->hasQuitBattle())
                continue;

             if(shouldReplace(di) &&
                findReplacement(map, di, c))
             {
                movingUp = True;
             }
         }
#ifdef DEBUG
         gameInfo()->log("--------------------------------\n");
#endif
    }

    return movingUp;
}

bool MidLevel_Op::moveUpSupport()
// See if Corp reserves should move up to assist front units
{
    bool supporting = False;
    // Go through front-line
    for(MidUnitList::iterator it = d_planner->d_units->begin();
             it != d_planner->d_units->end();
             ++it)
    {
#ifdef DEBUG
         gameInfo()->log("\n--------------------------------");
         gameInfo()->log("Testing %s for bringing up support", (*it).cp()->getName());
#endif
         MidUnitDeployMap& map = (*it).map();

         for(int c = 0; c < map.nFront(); c++)
         {
             MU_DeployItem& di = map.item(c, 0);
             if(di.cp()->hasQuitBattle())
                continue;

             if(shouldSupport(di) &&
                  findSupport(map, di, c))
             {
#ifdef DEBUG
                gameInfo()->log("--------------------------------\n");
#endif
                supporting = True;
             }
         }
#ifdef DEBUG
         gameInfo()->log("--------------------------------\n");
#endif
    }

    return supporting;
}

HexPosition::Facing avgDivFacing(const CRefBattleCP& cp)
{
   int count = 0;
   int totalFace = 0;
   for(std::vector<DeployItem>::const_iterator di = cp->mapBegin();
         di != cp->mapEnd();
         ++di)
   {
      if(di->active())
      {
         totalFace += di->d_sp->facing();
         count++;
      }
   }

   if(count)
   {
      HexPosition::Facing f = totalFace / count;
      return WG_BattleAI_Utils::closeFace(f);
   }

   return HexPosition::North;
}

bool MidLevel_Op::considerHold()
{
    // See if any XX's should halt in place
    for(MidUnitList::iterator it = d_planner->d_units->begin();
          it != d_planner->d_units->end();
          ++it)
    {
         // items we are checking for
         MidUnitDeployMap& map = (*it).map();
         for(int c = 0; c < map.nFront(); c++)
         {
            MU_DeployItem& di = map.item(c, 0);
            if(di.cp()->hasQuitBattle())
               continue;

            if(di.cp()->inCombat() &&
                di.cp()->moving() &&
                di.cp()->targetList().entries() > 0 &&
                di.cp()->targetList().closeRangeFront() <= gameInfo()->random(3, 5))
            {
#ifdef DEBUG
               gameInfo()->log("Considering %s for hold", di.cp()->getName());
#endif
               bool shouldHold = False;

               // See if we are moving parallel to enemy,
               // if so we should probably stop as they are firing into our flank
               CRefBattleCP enemyCP = const_cast<BattleCP*>(di.cp())->targetList().first()->d_cp;
               HexPosition::Facing ourFacing = avgDivFacing(di.cp());
               HexPosition::Facing enemyFacing = enemyCP->facing();

               if(ourFacing != BattleMeasure::oppositeFace(enemyFacing))
               {
                  int chance = ((di.cp()->leader()->getTactical() + di.cp()->leader()->getInitiative()) * 100) / 510;
                  chance += gameInfo()->random(30, 40);
                  shouldHold = (gameInfo()->random(100) <= chance);
               }

               // other reasons we may hold...
               if(shouldHold)
               {
#ifdef DEBUG
                  gameInfo()->log("%s is to Hold", di.cp()->getName());
#endif
                  setHoldOrder(di);
               }
            }
         }
    }
    return False;
}

bool MidLevel_Op::doHold()
{
#ifdef DEBUG
   gameInfo()->log("Running doHold()");
#endif

   // hold all units in place
   for(MidUnitList::iterator it = d_planner->d_units->begin();
         it != d_planner->d_units->end();
         ++it)
   {
      MidUnitDeployMap& map = (*it).map();

      for(int r = 0; r < 2; r++)
      {
         int nCols = (r == 0) ? map.nFront() : map.nRear();
         for(int c = 0; c < nCols; c++)
         {
            MU_DeployItem& di = map.item(c, r);
            if(di.cp()->hasQuitBattle())
               continue;

            setHoldOrder(di);
#ifdef DEBUG
            gameInfo()->log("%s is to Hold", di.cp()->getName());
#endif
         }
      }
   }

   return True;
}

// gt left destination hex, based on enemy left (relative to us)
bool MidLevel_Op::getStartingLeft(
   const HexCord& enemyLeft,
//   int& howMany,
   int spacing,
   HexPosition::Facing ourFacing,
   HexCord& startLeft)
{
   MidUnitInfo& mi = d_planner->d_units->front();
   ASSERT(!mi.cp()->hasQuitBattle());

   for(int c = 0; c < mi.map().nFront(); c++)
   {
      MU_DeployItem& di = mi.map().item(c, 0);
      if(di.cp()->hasQuitBattle())
         continue;

      HexCord ourLeft = di.cp()->leftHex();

      WG_BattleAI_Utils::MoveOrientation mo = WG_BattleAI_Utils::moveOrientation(
         ourFacing, ourLeft, enemyLeft, WG_BattleAI_Utils::BoundryAngle);//LessExtremeShortAngle);

      if(mo == WG_BattleAI_Utils::MO_Left)
      {
#ifdef DEBUG
         gameInfo()->log("enemy left to our left");
#endif
         startLeft = enemyLeft;
         break;
      }
      else
      {
#if 0
         mo = WG_BattleAI_Utils::moveOrientation(ourFacing, ourLeft, enemyLeft, WG_BattleAI_Utils::LessExtremeShortAngle);
        // HexCord::HexDirection hd = leftFlank(f);
         int dist = WG_BattleAI_Utils::getDist(ourLeft, enemyLeft);
         int lr = (mo == WG_BattleAI_Utils::MO_Right) ? dist / 3 : 0;
#endif
#ifdef DEBUG
         gameInfo()->log("enemy left to our front-right");
#endif
         HexCord::HexDirection hd = leftFlank(ourFacing);
         int goLeft = spacing * (mi.map().nFront() - (1 + c));
         if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), hd, goLeft, enemyLeft, startLeft))
            return False;
         else
            break;
      }
   }

   return True;
}

CRefBattleCP MidLevel_Op::closeEnemy(AgainstWhat w)
{
   int closeDist = UWORD_MAX;
   CRefBattleCP enemyCP = NoBattleCP;
   SListIterR<BattleItem> iter(&d_planner->d_targets);
   while(++iter)
   {
      bool isType = False;
      switch(w)
      {
         case AW_Inf:
            isType = (iter.current()->d_cp->generic()->isInfantry());
            break;
         case AW_Cav:
            isType = (iter.current()->d_cp->generic()->isCavalry());
            break;
         case AW_Art:
            isType = (iter.current()->d_cp->generic()->isArtillery());
            break;
         case AW_FiringArt:
            isType = (iter.current()->d_cp->shootingGuns());
            break;
      }

      if(isType &&
         iter.current()->d_range < closeDist)
      {
         enemyCP = iter.current()->d_cp;
         closeDist = iter.current()->d_range;
      }
   }

   return enemyCP;
}

MU_DeployItem* MidLevel_Op::findReserve(BasicUnitType::value t, HexCord target)
{
   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle())
      return 0;

   MU_DeployItem* bestDI = 0;
   int bestDist = UWORD_MAX;
   for(int c = 0; c < mi.map().nRear(); c++)
   {
      MU_DeployItem& di = mi.map().item(c, 1);
      if(di.cp()->hasQuitBattle())
         continue;

      if ( (t == BasicUnitType::Infantry && di.cp()->generic()->isInfantry()) ||
           (t == BasicUnitType::Cavalry && di.cp()->generic()->isCavalry())  ||
           (t == BasicUnitType::Artillery && di.cp()->generic()->isArtillery()) )
      {
         int dist = WG_BattleAI_Utils::distanceFromUnit(target, const_cast<BattleCP*>(di.cp()));
         if(dist < bestDist &&
            !di.cp()->movingBackwards() &&
            !di.cp()->routing() &&
            !di.cp()->fleeingTheField() &&
            di.cp()->morale() > 60)
         {
            bestDI = &di;
            bestDist = dist;
         }
      }
   }

   return bestDI;
}

// Launch a sortie, that is a limited attack against enemy,
bool MidLevel_Op::launchSortie(
   MU_DeployItem* di,
   const HexCord targetHex,    // target dest
   const HexCord destHex,      // return dest
   HexPosition::Facing facing)
{
#ifdef DEBUG
   gameInfo()->log("%s is launching sortie!", di->cp()->getName());
#endif
   d_planner->d_flags |= MidLevelPlanner::Reacting;
   //clearMove(*di);
   //di->ordered(False);
   di->attacking(True);
   di->order().posture(BattleOrderInfo::Offensive);
   di->order().aggression(BattleOrderInfo::EngageAtAllCost);
   //WG_BattleAI_Utils::setMoveOrder(&di->order(), targetHex, facing);
   setMoveOrder(*di, targetHex, facing, True);
   if(targetHex != destHex)
      setMoveOrder(*di, destHex, facing, False); //WG_BattleAI_Utils::setMoveOrder(&di->order(), destHex, facing);
   if(di->cp()->formation() <= CPF_Massed)
   {
      di->order().divFormation(CPF_Deployed);
      di->order().order().d_whatOrder.add(WhatOrder::CPFormation);
   }
   shouldAttach(di->cp(), AW_Attacking);
   return True;
}

// i.e. a quick charge to regain ground, and then backing up a bit
bool MidLevel_Op::launchSortie(AgainstWhat w)
{
   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle())
      return False;

   // Find closest enemy of type, and charge
   CRefBattleCP enemyCP = closeEnemy(w);
   if(enemyCP == NoBattleCP)
      return False;

   //  Find unit to launch
   HexCord targetHex = const_cast<BattleCP*>(enemyCP)->rightHex();
   MU_DeployItem* di = findReserve(BasicUnitType::Cavalry, targetHex);
   if(di)
   {
#ifdef DEBUG
      gameInfo()->log("%s will launch a sortie attack on %s",
         di->cp()->getName(), enemyCP->getName());
#endif
      HexCord curHex = di->cp()->leftHex();
      return launchSortie(di, targetHex, curHex, di->cp()->facing());
   }

   return False;
}

bool MidLevel_Op::enemyCavNear()
{
   SListIterR<BattleItem> iter(&d_planner->d_targets);
   while(++iter)
   {
      if(iter.current()->d_cp->generic()->isCavalry() &&
         iter.current()->d_range <= (3 * BattleMeasure::XYardsPerHex))
      {
#ifdef DEBUG
         gameInfo()->log("Enemy Cav nearby! (%s)", iter.current()->d_cp->getName());
#endif
         return True;
      }
   }

   return False;
}

// Determine whether we move out of range of enemy artillery
bool MidLevel_Op::movingOutOfRange()
{
   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle())
      return False;

   // first determine if we do something or not
   CRefBattleCP leaderCP = objLeader();
   // chance of doing something is converse of leaders charisma ond aggression
   int v = 512 - (leaderCP->leader()->getAggression() + leaderCP->leader()->getCharisma());
   int chance = ((v * 100) / 512) / 7;

   if(frontHolding(mi))
      chance += 10;

   if(d_planner->d_flags & MidLevelPlanner::Reacting ||
      d_planner->d_flags & MidLevelPlanner::Retreating ||
      d_planner->bombarding())
   {
      chance -= 50;
   }

   if(mi.cp()->morale() < 65)
      chance += 10;
   else if(mi.cp()->morale() < 100)
      chance += 5;
   else if(mi.cp()->morale() > 180)
      chance -= 10;
   else if(mi.cp()->morale() > 140)
      chance -= 5;

   if(gameInfo()->random(100) > chance)
      return False;

   // If here, we are doing something
   if(d_planner->d_targets.entries() > 0)//checkForEnemyToFront())
   {
      // Find closest enemy range
      int closeRange = UWORD_MAX;
      for(int r = 0; r < 2; r++)
      {
         int nCols = (r == 0) ?  mi.map().nFront() : mi.map().nRear();
         for(int c = 0; c < nCols; c++)
         {
            MU_DeployItem& di = mi.map().item(c, r);
            if(di.cp()->hasQuitBattle())
               continue;

            if(di.cp()->takingGunHits() &&
               di.cp()->targetList().closeRange() > (2 * BattleMeasure::XYardsPerHex) &&
               di.cp()->holding() &&
               di.cp()->targetList().closeRange() < closeRange)
            {
               closeRange = di.cp()->targetList().closeRange();
            }
         }
      }

      if(closeRange != UWORD_MAX)
      {
#ifdef DEBUG
         gameInfo()->log("Testing %s for moving out of range", mi.cp()->getName());
#endif
         RefGLeader l = mi.cp()->leader();
         int chance = MulDiv((l->getTactical() + l->getInitiative()) - l->getAggression(), 100, 256);
//       int dieRoll = gameInfo()->random(10);

         // a random bonus
         //chance += gameInfo()->random().gauss(20);
//       int dieRoll =

         if(mi.cp()->posture() == BattleOrderInfo::Defensive)
            chance += 10;
         else
            chance -= 10;

         bool shouldRetreat = False;
         if(chance < 20) // attack
         {
#ifdef DEBUG
            gameInfo()->log("---- Attacking!");
#endif
            launchSortie(AW_FiringArt);
         }
         else if(chance < 30) // nothing
         {
#ifdef DEBUG
            gameInfo()->log("----- Nothing!");
#endif
         }
         else if(chance < 60) // line
         {
#ifdef DEBUG
            gameInfo()->log("------- To Line!");
#endif
            // First test for enemy cav near
            if(enemyCavNear() && gameInfo()->random(100) <= 80)
               shouldRetreat = True;

            // All units taking artillery fire go to line formation
            else
            {
               for(int r = 0; r < 2; r++)
               {
                  for(int c = 0; c < mi.map().nFront(); c++)
                  {
                     MU_DeployItem& di = mi.map().item(c, 0);
                     if(di.cp()->hasQuitBattle())
                        continue;

                     di.updateOrder();
                     di.ordered(False);

                     if(di.cp()->sp() &&
                        !di.cp()->generic()->isArtillery() &&
                        di.cp()->spFormation() != SP_LineFormation &&
                        di.cp()->takingGunHits())
                     {
                        di.order().spFormation(SP_LineFormation);
                        di.order().order().d_whatOrder.add(WhatOrder::SPFormation);
#ifdef DEBUG
                        gameInfo()->log("-------%s is changing formation from SP March formation", di.cp()->getName());
#endif
                     }
                  }
               }
            }
         }
         else
            shouldRetreat = True;

         if(shouldRetreat) // retreat
         {
#ifdef DEBUG
            gameInfo()->log("-------- Retreating!");
#endif
            // move out of range, or attack, or something

            // for now, move out of range

            int inHexes = closeRange / BattleMeasure::XYardsPerHex;
            int range = 8 - inHexes;
            if(range > 0)
            {
               doRetreatFromEnemy(range);
#ifdef DEBUG
               gameInfo()->log("%s will move back(%d hexes) out of artillery range",
                  mi.cp()->getName(), range);
#endif
               return True;
            }
         }
      }
   }

   return False;
}

// move some units to front if need be
// i.e. insure an XXX corps has at least 2 XX div up front (if available)
bool MidLevel_Op::updateFrontLine()
{
   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle() || mi.cp()->getRank().sameRank(Rank_Division) || mi.map().nRear() == 0)
      return False;

   int nInf = 0;
   int nCav = 0;
   int nArt = 0;
   int nInfFront = 0;
   int nCavFront = 0;
   int nArtFront = 0;
   for(int r = 0; r < 2; r++)
   {
      int nCols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem& di = mi.map().item(c, r);
         if(!di.cp()->hasQuitBattle())
         {
            if(di.cp()->generic()->isInfantry())
            {
               nInf++;
               if(r == 0)
                  nInfFront++;
            }
            if(di.cp()->generic()->isCavalry())
            {
               nCav++;
               if(r == 0)
                  nCavFront++;
            }
            if(di.cp()->generic()->isArtillery())
            {
               nArt++;
               if(r == 0)
                  nArtFront++;
            }
         }
      }
   }

   if(nInfFront == 1 && nInf > 2)
   {
      int newC = 0;
      MU_DeployItem& ldi = mi.map().item(0, 0);
      // Move up a rear infantry.
      // Go to outside of suppoting unit
      if(ldi.cp()->hasQuitBattle())
         ;//newC = 0;
      else
      {
         MU_DeployItem& rdi = mi.map().item(mi.map().nFront() - 1, 0);
         if(rdi.cp()->hasQuitBattle())
            newC = mi.map().nFront();
         else
         {
            if(rdi.cp()->generic()->isCavalry() || rdi.cp()->generic()->isArtillery())
            {
               newC = mi.map().nFront();
            }
         }
      }

      int oldC = 0;
      int start = (newC == 0) ? 0 : mi.map().nRear() - 1;
      int end = (newC == 0) ? mi.map().nRear() : -1;
      int inc = (newC == 0) ? 1 : -1;
      {
         for(int c = start; c != end; c += inc)
         {
            MU_DeployItem& diR = mi.map().item(c, 1);
            if(!diR.cp()->hasQuitBattle() && diR.cp()->generic()->isInfantry())
            {
               oldC = c;
               break;
            }
         }
      }

      MU_DeployItem copyDI = mi.map().item(oldC, 1);
      if(newC == 0)
      {
         for(int r = 0; r < 2; r++)
         {
            int nCol = (r == 0) ? mi.map().nFront() : mi.map().nRear();
            for(int c = 0; c < nCol; c++)
            {
               MU_DeployItem& nDI = mi.map().item(c, r);
               MU_DeployItem nCopy = nDI;
#ifdef DEBUG
               const char* name1 = copyDI.cp()->getName();
               const char* name2 = nDI.cp()->getName();
#endif
               nDI = copyDI;

               if( (r == 1) &&
                   (c >= oldC) )
               {
                  break;
               }

               copyDI = nCopy;
            }
         }
      }

      else
      {
         if(oldC != 0)
         {
            for(int c = 0; c < mi.map().nRear(); c++)
            {
               MU_DeployItem& nDI = mi.map().item(c, 1);
               MU_DeployItem nCopy = nDI;
               nDI = copyDI;

               if(c >= oldC)
                  break;

               copyDI = nCopy;
            }
         }
      }

      // Now insert into front line, adjust rest of map as we go
      mi.map().nFront(mi.map().nFront() + 1);
      mi.map().nRear(mi.map().nRear() - 1);

#ifdef DEBUG
      gameInfo()->log("Updated deploymap for %s", mi.cp()->getName());
      {
         for(int r = 0; r < 2; r++)
         {
            int nCols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
            for(int c = 0; c < nCols; c++)
            {
               MU_DeployItem& di = mi.map().item(c, r);
               gameInfo()->log("--------> %s is a %s unit", di.cp()->getName(), (r == 0) ? "Front-line" : "Reserve");
            }
         }
      }
#endif
      return True;
   }

   return False;
}

// make sure units are moving cohesively
bool MidLevel_Op::doUpdateMove()
{
   MidUnitInfo& mi = d_planner->d_units->front();

   // If we only have one division there is no need to check
   if(mi.cp()->getRank().sameRank(Rank_Division) ||
      mi.map().nFront() + mi.map().nRear() <= 1 ||
      //d_planner->d_targets.closeRange() <= (3 * BattleMeasure::XYardsPerHex) ||
      //d_planner->d_flags & MidLevelPlanner::Reacting ||
      d_planner->d_flags & MidLevelPlanner::NoCheckAlignment ||
      d_planner->d_flags & MidLevelPlanner::Retreating)
   {
      // set any stopped units moving again
      if(d_planner->d_flags & MidLevelPlanner::CatchingUp)
      {
         for(int r = 0; r < 2; r++)
         {
            int nCol = (r == 0) ? mi.map().nFront() : mi.map().nRear();
            for(int c = 0; c < nCol; c++)
            {
               MU_DeployItem& di = mi.map().item(c, r);
               if(!di.cp()->hasQuitBattle())
               {
#ifdef DEBUG
                  gameInfo()->log("%s is to move to hex(%d,%d) (last time)",
                     di.cp()->getName(), static_cast<int>(di.objHex().x()), static_cast<int>(di.objHex().y()));
#endif
                  setMoveOrder(di, di.objHex(), di.cp()->facing(), True);
                  di.adjustCorpHolding(False);
                  di.wasCorpHolding(False);
               }
            }
         }

         d_planner->d_flags &= ~MidLevelPlanner::CatchingUp;
         return True;
      }

      return False;
   }

   // Update
   bool inLine = corpsInLine(mi, (d_planner->d_flags & MidLevelPlanner::Reacting) != 0);
   if(!inLine  || d_planner->d_flags & MidLevelPlanner::CatchingUp)
   {
      for(int r = 0; r < 2; r++)
      {
         //if(r == 1 && d_planner->d_flags & MidLevelPlanner::Reacting)
         //{
         //   break;
         //}

         int nCols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
         for(int c = 0; c < nCols; c++)
         {
            MU_DeployItem& di = mi.map().item(c, r);
            if(!di.cp()->hasQuitBattle())
            {
               // If we are already playing catchup
               if(d_planner->d_flags & MidLevelPlanner::CatchingUp)
               {
                  if(inLine || (!di.adjustCorpHolding() && di.wasCorpHolding()) )//|| (di.adjustCorpHolding() && !di.wasCorpHolding()) )
                  {
                     HexCord objHex = di.objHex();
                     if(objHex != di.cp()->leftHex() && objHex != HexCord(0, 0))
                     {
#ifdef DEBUG
                        gameInfo()->log("%s is to move to hex(%d,%d) (in line)",
                           di.cp()->getName(), static_cast<int>(objHex.x()), static_cast<int>(objHex.y()));
#endif
                        setMoveOrder(di, di.objHex(), di.cp()->facing(), True);
                     }

                     di.wasCorpHolding(False);//!(inLine || (!di.adjustCorpHolding() && di.wasCorpHolding())));
                  }

                  else if(di.adjustCorpHolding() && !di.wasCorpHolding())
                  {
#ifdef DEBUG
                     gameInfo()->log("%s is to Hold (not in line)", di.cp()->getName());
#endif
                     setHoldOrder(di);
                     di.wasCorpHolding(True);
                  }
               }
               // Need to play catchup
               else
               {
                  if(!inLine && di.adjustCorpHolding())
                  {
#ifdef DEBUG
                     gameInfo()->log("%s is to Hold (not in line)", di.cp()->getName());
#endif
                     setHoldOrder(di);
                     //d_planner->d_flags |= MidLevelPlanner::CatchingUp;
                     di.wasCorpHolding(True);
                  }
               }
            }
         }
      }
   }

   if(!inLine)
   {
      d_planner->d_flags |= MidLevelPlanner::CatchingUp;
      return True;
   }
   else
      d_planner->d_flags &= ~ MidLevelPlanner::CatchingUp;

   return False;
}

// Test to see if HQ should attach to one of its subs
bool MidLevel_Op::shouldAttach(const CRefBattleCP& cp, MidLevel_Op::AttachWhy why)
{
   if(d_planner->d_cincDI.cp()->getRank().sameRank(Rank_Division))
      return False;

   RefGLeader leader = d_planner->d_cincDI.cp()->leader();
#ifdef DEBUG
   gameInfo()->log("Testing %s for attaching %s(%s)", cp->getName(), leader->getName(), d_planner->d_cincDI.cp()->getName());
#endif
   int chance = (leader->getCharisma() + leader->getInitiative() + leader->getAggression()) / 40;
#ifdef DEBUG
   gameInfo()->log("Raw chance = %d", chance);
#endif
   if(why == AW_Attacking)
   {
      // TODO: proper calculations
      chance += gameInfo()->random(5, 25);
   }

   else if(why == AW_Retreating)
   {
      // TODO: proper calculations
      chance += gameInfo()->random(10, 30);
   }

   if(d_planner->d_cincDI.cp()->attached() ||
      d_planner->d_cincDI.cp()->getCurrentOrder().attachTo() != NoBattleCP)
   {
      chance -= 20;
   }

#ifdef DEBUG
   gameInfo()->log("Final chance = %d", chance);
#endif
#if 1
   if(gameInfo()->random(100) < chance)
   {
#ifdef DEBUG
      gameInfo()->log("%s will attach to %s", leader->getName(), cp->getName());
#endif
      setAttachOrder(d_planner->d_cincDI, cp);
      return True;
   }
#endif
   return False;
}

bool MidLevel_Op::procHQ()
{
   MidUnitInfo& mi = d_planner->d_units->front();

   if(d_planner->d_cincDI.cp()->getRank().isHigher(Rank_Division) &&
      !d_planner->d_cincDI.cp()->attached() &&
      d_planner->d_cincDI.order().attachTo() == NoBattleCP)// &&
//      d_cincDI.cp()->moving())
   {
      //if(d_planner->d_cincDI.cp()->firstOrderSent())
      MU_DeployItem* cDI = &d_planner->d_cincDI;
      cDI->updateOrder();
      enum { Hold, Move, DoNothing } doWhat = DoNothing;

      for(int c = 0; c < mi.map().nFront(); c++)
      {
         MU_DeployItem& di = mi.map().item(c, 0);
         if(di.cp()->hasQuitBattle())
            continue;

         WG_BattleAI_Utils::AngleType at1 = WG_BattleAI_Utils::ObjectiveAngle;
         WG_BattleAI_Utils::AngleType at2 = WG_BattleAI_Utils::WideAngle;
         HexCord hqLeft = cDI->cp()->leftHex();
         HexCord xxLeft = di.cp()->leftHex();
         WG_BattleAI_Utils::MoveOrientation mo1 = WG_BattleAI_Utils::moveOrientation(di.cp()->facing(), hqLeft, xxLeft, at1);
         WG_BattleAI_Utils::MoveOrientation mo2 = WG_BattleAI_Utils::moveOrientation(di.cp()->facing(), hqLeft, xxLeft, at2);
         if(mo1 != WG_BattleAI_Utils::MO_Front)
         {
            doWhat = (cDI->cp()->moving() && cDI->order().mode() != BattleOrderInfo::Hold) ?
               Hold : DoNothing;

            break;
         }
         else if(mo2 == WG_BattleAI_Utils::MO_Front)
         {
            if(cDI->cp()->holding() &&
               cDI->cp()->hex() != d_planner->d_cincDI.objHex() &&
               cDI->order().mode() != BattleOrderInfo::Move)
            {
               doWhat = Move;
            }
         }
      }

      switch(doWhat)
      {
         case Hold:
            setHoldOrder(*cDI);
            break;
         case Move:
            setMoveOrder(*cDI, cDI->objHex(), cDI->cp()->facing(), True);
            break;
      }
   }

   return False;
}

bool MidLevel_Op::updateRetreatingUnits(MU_DeployItem& di, int r)
{

   RandomNumber& lrand = gameInfo()->random();
#ifdef DEBUG
   gameInfo()->log("Update retreating unit %s", di.cp()->getName());
#endif
   const Table1D<SWORD>& mTable = BattleAITables::replaceDivModifiers();
   enum {
      MoraleDiv,
      Retreating,
      Routing,
      CriticalLoss,
      Wavering,
      Disorder,
      MoraleLess65,
      MoraleLess100,
      MoraleLess150,
      MoraleOver150,
      NoAggChange,
      NoDisorder
   };

   int percentOfStart = (di.cp()->startMorale() > 0) ?
      (di.cp()->morale() * 100) / di.cp()->startMorale() : 0;
   int chance = (mTable.getValue(MoraleDiv) > 0) ? (100 - percentOfStart) / mTable.getValue(MoraleDiv) : 0;


   // If retreating or routing or fleeing the field, replace
   if(di.cp()->routing() ||
      di.cp()->fleeingTheField())
   {
      chance += mTable.getValue(Routing);
   }
   else
   {
      chance += mTable.getValue(Retreating);
   }

   // If shaken, maybe replace
   if(di.cp()->criticalLossReached())
      chance += mTable.getValue(CriticalLoss);

   if(di.cp()->wavering() || di.cp()->shaken())
      chance += mTable.getValue(Wavering);

   chance += (mTable.getValue(Disorder) * (3 - di.cp()->disorder()));

   if(di.cp()->noEffectCutoffReached())
   {
      if(di.cp()->morale() < 65)
         chance += mTable.getValue(MoraleLess65);//10;
      else if(di.cp()->morale() < 100)
         chance += mTable.getValue(MoraleLess100);//5;
      else if(di.cp()->morale() < 150)
         chance += mTable.getValue(MoraleLess150);
      else
         chance += mTable.getValue(MoraleOver150);//10;
   }

   if(di.cp()->aggression() == di.cp()->getCurrentOrder().aggression())
      chance += mTable.getValue(NoAggChange);//-10;

   if(di.cp()->disorder() == 3)
      chance += mTable.getValue(NoDisorder);

   chance = 100 - chance;
   if(lrand(100) < chance)
   {
      if(r == 1)
      {
#ifdef DEBUG
         gameInfo()->log("%s is retreating but we are stopping his retreat", di.cp()->getName());
#endif
         setHoldOrder(di);
         di.order().aggression(static_cast<BattleOrderInfo::Aggression>(minimum(3, di.order().aggression() + 1)));
      }
      else
      {
#ifdef DEBUG
         gameInfo()->log("%s is retreating but we are sending him back into the frey", di.cp()->getName());
#endif
         setMoveOrder(di, di.objHex(), di.cp()->facing(), True);
         di.order().aggression(static_cast<BattleOrderInfo::Aggression>(minimum(3, di.order().aggression() + 1)));
      }
   }

   return True;
}

bool MidLevel_Op::monitorUnits()
{
   // monitor status of divisions
   MidUnitInfo& mi = d_planner->d_units->front();

#ifdef DEBUG
   const char* name = mi.cp()->getName();
#endif
   for(int r = 0; r < 2; r++)
   {
      int nCols = (r== 0) ? mi.map().nFront() : mi.map().nRear();
      for(int c = 0; c < nCols; c++)
      {
         MU_DeployItem* di = &mi.map().item(c, r);
         if(!di->cp()->hasQuitBattle())
         {
            // monitor attacking units
            if(di->attacking())
            {
               bool moving = (!di->cp()->holding() || di->cp()->getCurrentOrder().wayPoints().entries() > 0);
               if(!di->startedMove())
               {
                  if(moving)
                  {
                     di->startedMove(True);
                  }
               }
               else if(!moving && !di->bombarding())
               {
                  bool reset = True;
                  if(!di->cp()->shootingMuskets())
                  {
                     HexCord objHex = di->objHex();
                     if(objHex != di->cp()->leftHex() && objHex != HexCord(0, 0))
                     {
#ifdef DEBUG
                        gameInfo()->log("%s should be attacking but isn't. Setting him on his way",   di->cp()->getName());
#endif
                        setMoveOrder((*di), objHex, di->cp()->facing(), True);
                        reset = False;
                     }
                  }

                  if(reset)
                  {
#ifdef DEBUG
                     gameInfo()->log("%s has finished attacking",   di->cp()->getName());
#endif
                     di->attacking(False);
                     di->startedMove(False);
                  }
               }
            }

            // retreating units
            if(!(d_planner->d_flags & MidLevelPlanner::Retreating) && di->cp()->movingBackwards())
            {
               updateRetreatingUnits(*di, r);
            }

            // units bombarding
            else if((d_planner->bombarding() || di->bombarding()) && di->startedMove())
            {
               if(di->bombarding())
               {
#if 0//def DEBUG
                  if(lstrcmpi("VI Corps Artillery", di->cp()->getName()) == 0)
                  {
                     int i = 0;
                  }
#endif
                  int range = WG_BattleAI_Utils::getRange(const_cast<BattleData*>(gameInfo()->battleData()), di->cp(), False);
                  bool inRange = (di->cp()->targetList().closeRangeFront() <= range);
                  if(di->cp()->moving() && inRange)
                  {
                     int rnHexes = range / BattleMeasure::XYardsPerHex;
                     int crnHexes = di->cp()->targetList().closeRangeFront() / BattleMeasure::XYardsPerHex;
                     int dif = (rnHexes - crnHexes) + 1;
                     int chance = (100 - ((di->cp()->leader()->getAggression() * 100) / 255)) + (dif * 20);
#ifdef DEBUG
                     gameInfo()->log("Bombarding unit %s is in range (range=%d,close-enemy=%d), chance to halt=%d",
                        di->cp()->getName(), rnHexes, crnHexes, chance);
#endif
                     if(gameInfo()->random(100) < chance)
                     {
#ifdef DEBUG
                        gameInfo()->log("Will halt!");
#endif
                        setHoldOrder((*di));
                     }
                  }

                  else if(!inRange &&
                          //di->cp()->generic()->isArtillery() &&
                          //!d_planner->bombarding() &&
                          di->cp()->holding())
                  {
                     //di->bombarding(False);
                     if(di->cp()->leftHex() != di->objHex())
                     {
#ifdef DEBUG
                        gameInfo()->log("Bombarding unit %s is out of range, moving", di->cp()->getName());
#endif
                        setMoveOrder((*di), di->objHex(), di->cp()->facing(), True);
                     }
                  }
               }
               else if(!di->cp()->holding())
               {
#ifdef DEBUG
                  gameInfo()->log("Non-Bombarding unit %s is halting", di->cp()->getName());
#endif
                  setHoldOrder((*di));
               }

#if 0
               HexCord startHex = di->cp()->leftHex();
               HexCord destHex;
               if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), di->cp()->facing(), 0, 2, startHex, destHex))
               {
                  return False;
               }
               else
               {
#ifdef DEBUG
                  gameInfo()->log("%s is bombarding but doesn't have a target. Moving forward to hex(%d,%d)",
                     di->cp()->getName(), static_cast<int>(destHex.x()), static_cast<int>(destHex.y()));
#endif
                  setMoveOrder((*di), destHex, di->cp()->facing(), True);
               }
#endif
            }
         }
      }
   }

   return True;
}

bool MidLevel_Op::process()
{
   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle())
      return False;

   // Make sure HQ isn't running ahead of units
   procHQ();

   // See if units need to deploy
   if(doDeploy())
      return True;

   // Insure units are moving cohesively
   if(doUpdateMove())
      return True;

   // monitor retreating and attacking units
   monitorUnits();

   if(d_planner->finishedBombard())
   {
#ifdef DEBUG
      if(!mi.cp()->hasQuitBattle())
         gameInfo()->log("%s has finished bombardment at tick %ld",
            mi.cp()->getName(),
            gameInfo()->battleData()->getTick());
#endif
      for(std::vector<MU_DeployItem>::iterator di = mi.map().container().begin();
          di != mi.map().container().end();
          ++di)
      {
         if(di->cp()->hasQuitBattle())
            continue;

         if(!di->cp()->generic()->isArtillery())
            di->bombarding(False);
      }
      d_planner->bombardFor(0);
      d_planner->d_flags |= MidLevelPlanner::FinishedBombard;
      d_planner->d_flags &= ~MidLevelPlanner::Reacting;
      //doAdvanceToEnemy(AdvToCC);
      if(checkForEnemyToFront())
         doEnemyToFront(True);

      return True;
   }

   // If we are setting around, make sure thats what we are supposed to be doing
   if(frontHolding(mi))
   {
      bool newObj = True;
      if(d_planner->d_flags & MidLevelPlanner::Retreating &&
         !allHolding(mi.cp()))
      {
         newObj = False;
         //d_planner->d_flags &= ~MidLevelPlanner::Reacting;
      }

      if(newObj)
      {
         d_planner->d_objective->getNewObjective(True);
      }
   }


   return True;
}


bool MidLevel_Op::countArtyAvailable(bool& hasArtyXX, bool& hasAttachedArty, int& nInXX, int& nAttached)
{
   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle())
      return False;

   // count only front-line
   for(int c = 0; c < mi.map().nFront(); c++)
   {
      MU_DeployItem& di = mi.map().item(c, 0);
      if(di.cp()->hasQuitBattle())
         continue;

      if(di.cp()->generic()->isArtillery())
      {
         hasArtyXX = True;
         nInXX += di.cp()->spCount();
      }
      else if(di.cp()->nArtillery() > 0)
      {
         hasAttachedArty = True;
         nAttached += di.cp()->nArtillery();
      }
   }

   return (hasArtyXX || hasAttachedArty);
}

bool MidLevel_Op::willBombard()
{
   MidUnitInfo& mi = d_planner->d_units->front();
   if( (mi.cp()->hasQuitBattle()) ||
       (d_planner->d_flags & MidLevelPlanner::FinishedBombard) )
   {
      return False;
   }

#ifdef DEBUG
   gameInfo()->log("Testing for preliminary bombardment for %s", mi.cp()->getName());
#endif

   /*
    * If here, then unit wants to advance to FireCombat or ShockCombat
    * Determine if there should be a preliminary bombardment
    */

   // First, see if we even have any artillery
   bool hasArtyXX = False;
   bool hasAttachedArty = False;
   int nInXX = 0;
   int nAttached = 0;
   if(countArtyAvailable(hasArtyXX, hasAttachedArty, nInXX, nAttached))
   {
      RefGLeader l = mi.cp()->leader();
      int chance = (hasArtyXX) ? MulDiv((l->getTactical() + l->getInitiative()) - l->getAggression(), 100, 256) :
         MulDiv((l->getTactical() + l->getInitiative()) - l->getAggression(), 100, 512);

#ifdef DEBUG
      gameInfo()->log("Initial chance for bombarding = %d", chance);
#endif
      chance = clipValue(chance, 0, 100);
      if(hasArtyXX)
      {
         chance += (nInXX * 15);
         chance += (nAttached * 5);
      }
      else
      {
         chance += (nAttached <= 2) ? (-20 * (3 - nAttached)) : ((nAttached - 2) * 10);
      }

#ifdef DEBUG
      gameInfo()->log("Final chance for bombarding = %d", chance);
      //if(lstrcmpi(mi.cp()->getName(), "VI Corps") == 0)
        // chance = 100;
#endif
      if(gameInfo()->random(100) <= chance)
      {
         // We will bombard
#ifdef DEBUG
         gameInfo()->log("%s will conduct preliminary bombardment", mi.cp()->getName());
#endif
         return True;
      }

   }

   return False;
}


bool MidLevel_Op::doAdvanceToEnemy(MidLevel_Op::Reactions how)
{
    ASSERT(how == AdvToFC || how == AdvToCC || how == AdvToBomb);
#ifdef DEBUG
    gameInfo()->log("Running doAdvanceToEnemy()");
#endif

    MidUnitInfo& mi = d_planner->d_units->front();
    if(mi.cp()->hasQuitBattle())
       return False;

    bool bombardOnly = False;
    bool hasArtXX = False;
    bool hasAttachedArt = False;
    int nXXArt = 0;
    int nAttached = 0;
    if(how == AdvToFC || how == AdvToCC)
    {
      if(willBombard())
      {
         bombardOnly = countArtyAvailable(hasArtXX, hasAttachedArt, nXXArt, nAttached);
         if(bombardOnly)
         {
            BattleMeasure::BattleTime::Tick howLong = BattleMeasure::minutes(10) + BattleMeasure::minutes(gameInfo()->random(15, 30));
#ifdef DEBUG
            gameInfo()->log("will bombard for %d minutes (%ld Ticks starting at %ld)",
               minutesPerTick(howLong),
               howLong,
               gameInfo()->battleData()->getTick());
#endif
            d_planner->bombardFor(gameInfo()->battleData()->getTick() + howLong);
         }
      }
    }

    // get area that enemy occupies
    enum { Left, Right };
    HexArea eArea;
    if(calcEnemyArea(eArea))
    {
#ifdef DEBUG
         gameInfo()->log("Enemy area -- Left=(%d, %d), Right=(%d, %d)",
            static_cast<int>(eArea[Left].x()), static_cast<int>(eArea[Left].y()),
            static_cast<int>(eArea[Right].x()), static_cast<int>(eArea[Right].y()));

#endif
         HexArea* ourAreaP = const_cast<HexArea*>(&d_planner->d_objective->area());
         HexArea& ourArea = *ourAreaP;
         HexPosition::Facing f = WG_BattleAI_Utils::closeFace(WG_BattleAI_Utils::frontFacing(ourArea[Left], ourArea[Right]));

         // calc ours and enemies frontage
         int enemyFrontage = WG_BattleAI_Utils::getDist(eArea[Left], eArea[Right]);
         int objFrontage   = WG_BattleAI_Utils::getDist(ourArea[Left], ourArea[Right]);
         int nUnits = 0;
         int gap = 0;
         int tFrontage = totalFrontage(nUnits, gap);
         if(nUnits == 0)
            return False;
         int dif = (tFrontage > objFrontage) ?  tFrontage - objFrontage : 0;
         int difPer = (tFrontage > objFrontage) ? maximum(1, (dif + (nUnits / 2)) / nUnits) : 0;
         int ourFrontage = minimum(tFrontage, objFrontage);

         const int spacing = ourFrontage / nUnits;

         // determine starting left position
         //int moveHowMany = 0;
         HexCord startLeft;
         if(!getStartingLeft(eArea[Left], /*moveHowMany,*/ spacing, f, startLeft))
            return False;

#ifdef DEBUG
         gameInfo()->log("Starting left=(%d, %d)",
            static_cast<int>(startLeft.x()), static_cast<int>(startLeft.y()));
#endif

//       int gap = 2; // gap between units
         BattleData* bd = const_cast<BattleData*>(gameInfo()->battleData());

         // go through each XX and assign position
         HexCord left = startLeft;
         MidUnitDeployMap& map = mi.map();

         //int bombardTime = 0;
#ifdef DEBUG
         if(lstrcmpi(mi.cp()->getName(), "VI Corps") == 0)
         {
            int i = 0;
         }
#endif
         HexCord sHex = left;
         for(int r = 0; r < 2; r++)
         {
            int nCols = (r == 0) ? map.nFront() : map.nRear();
            for(int c = 0; c < nCols; c++)
            {
               MU_DeployItem& di = map.item(c, r);
               if(di.cp()->hasQuitBattle())
                  continue;

               HexCord hex = left;
               //bool holding = False;
               if(bombardOnly)
               {

                  if(r == 0 &&
                     di.cp()->generic()->isArtillery() ||
                     di.cp()->nArtillery() > 0)
                  {
                     di.bombarding(True);
#ifdef DEBUG
                     gameInfo()->log("%s is to bombard", di.cp()->getName());
#endif
                  }
               }

               else if(how == AdvToBomb)
               {
                  int range = maximum(2,
                           (WG_BattleAI_Utils::getRange(bd, di.cp()) / BattleMeasure::XYardsPerHex) - gameInfo()->random(1, 2));

                  HexCord newHex;
                  if(!WG_BattleAI_Utils::moveHex(bd, f, 0, -range, hex, left))
                     return False;
                  else
                  {
                     hex = left;
                     if(c == 0)
                        sHex = hex;
                  }
               }

               if(needsNewOrder(di, hex, f))
               {
                  setMoveOrder(di, hex, f, True);

                  // set front-line artillery to bombarding
                  if(r == 0)
                  {
                     di.attacking(True);
                     if(!di.bombarding() && di.cp()->generic()->isArtillery())
                        di.bombarding(True);
                     else
                        shouldAttach(di.cp(), AW_Attacking);
                  }
                  else
                     di.attacking(False);

                  //WG_BattleAI_Utils::setMoveOrder(&di.order(), hex, f);

#ifdef DEBUG
                  gameInfo()->log("%s is ordered to move to hex (%d, %d)",
                        di.cp()->getName(), (int)hex.x(), (int)hex.y());
#endif
               }
               if(c < nCols - 1)
               {
                  HexCord oldLeft = left;
                  int goRight = maximum(0, (di.cp()->columns() + gap) - difPer);
                  if(!WG_BattleAI_Utils::moveHex(bd, rightFlank(f), goRight, oldLeft, left))
                     return False;
               }
            }

            if(r == 0)
            {
               if(!WG_BattleAI_Utils::moveHex(bd, f, 0, -4, sHex, left))
                  return False;
            }
         }

         // set posture of all front line units
         switch(how)
         {
            case AdvToCC:
               setAggPosture(BattleOrderInfo::EngageAtAllCost, BattleOrderInfo::Offensive, True);
               break;

            case AdvToFC:
               setAggPosture(BattleOrderInfo::LimitedEngagement, BattleOrderInfo::Defensive, True);
               break;
         }

         return True;
    }

    return False;
}

bool MidLevel_Op::doRetreatFromEnemy(int range)
{
    CRefBattleCP leaderCP = objLeader();

#ifdef DEBUG
    gameInfo()->log("%s(%s) -- Retreating from enemy",
         leaderCP->leader()->getName(), leaderCP->getName());
#endif

   const HexArea& objArea = d_planner->d_objective->area();
   HexPosition::Facing ourFace = WG_BattleAI_Utils::frontFacing(objArea[0], objArea[1]);

   // set Aggression and posture
   BattleOrderInfo::Aggression ag = static_cast<BattleOrderInfo::Aggression>(maximum(0, leaderCP->aggression() - 1));
   BattleOrderInfo::Posture pos = BattleOrderInfo::Defensive;

   // go through all units and issue move orders to the rearward
   for(MidUnitList::iterator it = d_planner->d_units->begin();
         it != d_planner->d_units->end();
         ++it)
   {
      MidUnitDeployMap& map = (*it).map();

      for(int r = 0; r < 2; r++)
      {
         int nCols = (r == 0) ? map.nFront() : map.nRear();
         for(int c = 0; c < nCols; c++)
         {
            MU_DeployItem& di = map.item(c, r);
            if(di.cp()->hasQuitBattle())
               continue;

            setAggPosture(di, ag, pos);

            HexCord sHex = di.cp()->leftHex();
            HexCord rHex;
            if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), ourFace, 0, -gameInfo()->random(4, 6), sHex, rHex))// &&
//              needsNewOrder(di, rHex, ourFace))
            {
#ifdef DEBUG
               gameInfo()->log("---- %s is ordered to retreat to hex %d, %d",
                  di.cp()->getName(), (int)rHex.x(), (int)rHex.y());
#endif
               setMoveOrder(di, rHex, ourFace, True);
            }
         }
      }
   }

   return True;
}

int MidLevel_Op::totalFrontage(int& nUnits, int& gap)
{
   int frontage = 0;
   nUnits = 0;
   gap = (gameInfo()->random(100) < gameInfo()->random(50)) ? 2 : 1;

   // hold all units in place
   MidUnitInfo& mi = d_planner->d_units->front();
   MidUnitDeployMap& map = mi.map();

   nUnits += map.nFront();
   for(int c = 0; c < map.nFront(); c++)
   {
      MU_DeployItem& di = map.item(c, 0);
      if(di.cp()->hasQuitBattle())
         continue;

      frontage += (di.cp()->columns() + gap);
   }

   return frontage;
}

int MidLevel_Op::getShortestRange()
{
   int shortestRange = UWORD_MAX;
   BattleData* bd = const_cast<BattleData*>(gameInfo()->battleData());
   for(MidUnitList::iterator it = d_planner->d_units->begin();
         it != d_planner->d_units->end();
         ++it)
   {
      MidUnitDeployMap& map = (*it).map();

      for(int r = 0; r < 2; r++)
      {
         int nCols = (r == 0) ? map.nFront() : map.nRear();
         for(int c = 0; c < nCols; c++)
         {
            MU_DeployItem& di = map.item(c, r);
            if(di.cp()->hasQuitBattle())
               continue;

            int range = WG_BattleAI_Utils::getRange(bd, di.cp());
            if(range < shortestRange)
               shortestRange = range;
         }
      }
   }

   return shortestRange;
}

bool MidLevel_Op::calcEnemyArea(HexArea& area)
    // Calculate area occupied by enemy
{
    if(d_planner->d_targets.entries() == 0)
         return False;

    RefBattleCP bestLeftCP = NoBattleCP;
    RefBattleCP bestRightCP = NoBattleCP;
    HexCord bestLeft;
    HexCord bestRight;

    const HexArea& objArea = d_planner->d_objective->area();
    HexPosition::Facing ourFace = WG_BattleAI_Utils::frontFacing(objArea[0], objArea[1]);
    HexPosition::Facing enemyFace = BattleMeasure::oppositeFace(ourFace);

    SListIterR<BattleItem> iter(&d_planner->d_targets);
    while(++iter)
    {
         if(iter.current()->d_cp->hasQuitBattle() ||
            iter.current()->d_position != BattleItem::Front)
         {
            continue;
         }

         if(bestLeftCP == NoBattleCP)
         {
             bestLeftCP = const_cast<BattleCP*>(iter.current()->d_cp);
             bestRightCP = bestLeftCP;
             bestLeft = bestLeftCP->leftHex();
             bestRight = bestRightCP->rightHex();
         }

         else
         {
             {
                  HexCord unitLeft = const_cast<BattleCP*>(iter.current()->d_cp)->leftHex();
                  WG_BattleAI_Utils::MoveOrientation mo = WG_BattleAI_Utils::moveOrientation(
                           enemyFace, bestLeft, unitLeft, WG_BattleAI_Utils::ShortAngle);

                  if( (mo == WG_BattleAI_Utils::MO_Left) ||
                      (mo == WG_BattleAI_Utils::MO_Front && WG_BattleAI_Utils::getDist(bestLeft, unitLeft) >= (3 * BattleMeasure::XYardsPerHex)) )
                  {
                      bestLeft = unitLeft;
                      bestLeftCP = const_cast<BattleCP*>(iter.current()->d_cp);
                  }
             }
             {
                  HexCord unitRight = const_cast<BattleCP*>(iter.current()->d_cp)->rightHex();
                  WG_BattleAI_Utils::MoveOrientation mo = WG_BattleAI_Utils::moveOrientation(
                           enemyFace, bestRight, unitRight, WG_BattleAI_Utils::ShortAngle);

                  if( (mo == WG_BattleAI_Utils::MO_Right) ||
                        (mo == WG_BattleAI_Utils::MO_Front && WG_BattleAI_Utils::getDist(bestRight, unitRight) >= (3 * BattleMeasure::XYardsPerHex)) )
                  {
                      bestRight = unitRight;
                      bestRightCP = const_cast<BattleCP*>(iter.current()->d_cp);
                  }
             }
         }
    }

    if(bestLeftCP != NoBattleCP && bestRightCP != NoBattleCP)
    {
       area[0] = bestRight;//bestRightCP->rightHex(); //bestRight; //const_cast<BattleCP*>(enemyLeftCP)->leftHex();
       area[1] = bestLeft;//bestLeftCP->leftHex(); //bestLeft; //const_cast<BattleCP*>(enemyRightCP)->rightHex();

       return True;
    }

    return False;
}

void MidLevel_Op::setAggPosture(MU_DeployItem& di, BattleOrderInfo::Aggression ag, BattleOrderInfo::Posture pos)
{
   di.ordered(False);

   if(ag < BattleOrderInfo::Aggress_HowMany)
      di.order().aggression(ag);
   if(pos < BattleOrderInfo::Posture_HowMany)
      di.order().posture(pos);
}

void MidLevel_Op::setAggPosture(BattleOrderInfo::Aggression ag, BattleOrderInfo::Posture pos, bool frontRowOnly)
{
    for(MidUnitList::iterator it = d_planner->d_units->begin();
             it != d_planner->d_units->end();
             ++it)
    {
         MidUnitDeployMap& map = (*it).map();

         for(int r = 0; r < 2; r++)
         {
             int nCols = (r == 0) ? map.nFront() : map.nRear();
             for(int c = 0; c < nCols; c++)
             {
                  MU_DeployItem& di = map.item(c, r);
                  if(di.cp()->hasQuitBattle())
                     continue;

                  setAggPosture(di, ag, pos);
             }

             if(frontRowOnly)
                  break;
         }
    }
}


MidLevel_Op::Reactions MidLevel_Op::reactHow()
{
   MidUnitInfo& mi = d_planner->d_units->front();

   const int maxLScore = 5;
   EnemyRatio er = getOurStrVsEnemy();
   int rAttrib = gameInfo()->random(255);
   int lScore = minimum(maxLScore - 1, (mi.cp()->leader()->getAggression() + rAttrib) / 104);

   const Reactions s_table[BattleOrderInfo::Posture_HowMany][BattleOrderInfo::Aggress_HowMany][maxLScore][Ratio_HowMany] = {
     // Defensive
     {
        // Agg 0
        {
           { Halt,    Halt,    Halt,    Retreat, Retreat, Retreat, Retreat, Retreat, Retreat },
           { Halt,    Halt,    Halt,    Halt,    Retreat, Retreat, Retreat, Retreat, Retreat },
           { Halt,    Halt,    Halt,    Halt,    Halt,    Halt,    Retreat, Retreat, Retreat },
           { Halt,    Halt,    Halt,    Halt,    Halt,    Halt,    Halt,    Retreat, Retreat },
           { Halt,    Halt,    Halt,    Halt,    Halt,    Halt,    Halt,    Retreat, Retreat }
        },
        // Agg 1
        {
           { Halt,    Halt,    Halt,    Halt, Halt, Retreat, Retreat, Retreat, Retreat },
           { Halt,    Halt,    Halt,    Halt, Halt, Halt,    Retreat, Retreat, Retreat },
           { AdvToFC, Halt,    Halt,    Halt, Halt, Halt,    Halt,    Retreat, Retreat },
           { AdvToFC, Halt,    Halt,    Halt, Halt, Halt,    Halt,    Halt,    Retreat },
           { AdvToFC, AdvToFC, Halt,    Halt, Halt, Halt,    Halt,    Halt,    Retreat }
        },
        // Agg 2
        {
           { AdvToFC, Halt,    Halt,    Halt, Halt, Halt, Halt, Retreat, Retreat },
           { AdvToFC, Halt,    Halt,    Halt, Halt, Halt, Halt, Halt,    Retreat },
           { AdvToFC, AdvToFC, Halt,    Halt, Halt, Halt, Halt, Halt,    Retreat },
           { AdvToFC, AdvToFC, Halt,    Halt, Halt, Halt, Halt, Halt,    Halt },
           { AdvToCC, AdvToFC, AdvToFC, Halt, Halt, Halt, Halt, Halt,    Halt }
        },
        // Agg 3
        {
           { AdvToFC, Halt,    Halt,    Halt, Halt, Halt, Halt, Halt, Halt },
           { AdvToFC, AdvToFC, Halt,    Halt, Halt, Halt, Halt, Halt, Halt },
           { AdvToFC, AdvToFC, Halt,    Halt, Halt, Halt, Halt, Halt, Halt },
           { AdvToCC, AdvToFC, AdvToFC, Halt, Halt, Halt, Halt, Halt, Halt },
           { AdvToCC, AdvToFC, AdvToFC, AdvToFC, Halt, Halt, Halt, Halt, Halt }
        }
     },
     // Offensive
     {
        // Agg 0
        {
           { Halt,      Halt,      Retreat, Retreat, Retreat, Retreat, Retreat, Retreat, Retreat },
           { Halt,      Halt,      Halt,    Retreat, Retreat, Retreat, Retreat, Retreat, Retreat },
           { Halt,      Halt,      Halt,    Halt,    Retreat, Retreat, Retreat, Retreat, Retreat },
           { AdvToBomb, Halt,      Halt,    Halt,    Halt,    Halt,    Retreat, Retreat, Retreat },
           { AdvToFC,   AdvToBomb, Halt,    Halt,    Halt,    Halt,    Halt,    Halt,    Retreat }
        },
        // Agg 1
        {
           { AdvToFC, AdvToFC, AdvToBomb, AdvToBomb, AdvToBomb, Halt,      Halt,      Halt, Halt },
           { AdvToCC, AdvToFC, AdvToFC,   AdvToBomb, AdvToBomb, Halt,      Halt,      Halt, Halt },
           { AdvToCC, AdvToFC, AdvToFC,   AdvToBomb, AdvToBomb, AdvToBomb, Halt,      Halt, Halt },
           { AdvToCC, AdvToCC, AdvToFC,   AdvToFC,   AdvToFC,   AdvToBomb, AdvToBomb, Halt, Halt },
           { AdvToCC, AdvToCC, AdvToFC,   AdvToFC,   AdvToFC,   AdvToBomb, AdvToBomb, Halt, Halt }
        },
        // Agg 2
        {
           { AdvToCC, AdvToCC, AdvToFC, AdvToFC, AdvToFC, AdvToBomb, Halt,      Halt,      Halt      },
           { AdvToCC, AdvToCC, AdvToCC, AdvToFC, AdvToFC, AdvToFC,   AdvToBomb, Halt,      Halt      },
           { AdvToCC, AdvToCC, AdvToCC, AdvToFC, AdvToFC, AdvToFC,   AdvToFC,   AdvToBomb, Halt      },
           { AdvToCC, AdvToCC, AdvToCC, AdvToCC, AdvToFC, AdvToFC,   AdvToBomb, AdvToBomb, Halt      },
           { AdvToCC, AdvToCC, AdvToCC, AdvToCC, AdvToFC, AdvToFC,   AdvToFC,   AdvToBomb, AdvToBomb }
        },
        // Agg 3
        {
           { AdvToCC, AdvToCC, AdvToCC, AdvToFC, AdvToFC, AdvToBomb, Halt,      Halt,      Halt      },
           { AdvToCC, AdvToCC, AdvToCC, AdvToCC, AdvToFC, AdvToFC,   AdvToBomb, Halt,      Halt      },
           { AdvToCC, AdvToCC, AdvToCC, AdvToCC, AdvToCC, AdvToFC,   AdvToFC,   AdvToBomb, Halt      },
           { AdvToCC, AdvToCC, AdvToCC, AdvToCC, AdvToCC, AdvToFC,   AdvToFC,   AdvToBomb, AdvToBomb },
           { AdvToCC, AdvToCC, AdvToCC, AdvToCC, AdvToCC, AdvToFC,   AdvToFC,   AdvToBomb, AdvToBomb }
        }
     }
   };

   Reactions rh = s_table[d_planner->d_objective->posture()][d_planner->d_objective->aggression()][lScore][er];

#ifdef DEBUG
   static const TCHAR* s_reactionTxt[Reactions_HowMany] = {
               "None",
               "Advance To Close Combat",
               "Advance To Fire Combat",
               "Advance To Bomb",
               "Halt",
               "Retreat",
   };

   static const TCHAR* s_ratioText[Ratio_HowMany] = {
      "3/1", "2/1", "1.5/1", "1.2/1", "1/1", "1/1.2", "1/1.5", "1/2", "1/3"
   };

   gameInfo()->log("Reaction to enemy -- %s", s_reactionTxt[rh]);
   gameInfo()->log("--- Agg = %d, Pos = %d, leader-score = %d, ratio = %s, LeaderAgg = %d, rand = %d",
         static_cast<int>(mi.cp()->aggression()),
         static_cast<int>(mi.cp()->posture()),
         lScore,
         s_ratioText[er],
         static_cast<int>(mi.cp()->leader()->getAggression()),
         rAttrib);
#endif

   return rh;
}

CRefBattleCP MidLevel_Op::objLeader()
{
    CRefBattleCP cp = NoBattleCP;
    for(MidUnitList::iterator it = d_planner->d_units->begin();
            it != d_planner->d_units->end();
            ++it)
    {
         if(cp == NoBattleCP)
             cp = (*it).cp();
         else
         {
             ConstRefGLeader l = gameInfo()->battleData()->ob()->ob()->whoShouldCommand(cp->leader(), (*it).cp()->leader());
             if(l == (*it).cp()->leader())
                  cp = (*it).cp();
         }
    }

    return cp;
}

void MidLevel_Op::setHighLevelFlags(int id)
{
   MidUnitInfo& mi = d_planner->d_units->front();
   int nUnits = mi.map().nFront() + mi.map().nRear();
   if(mi.cp()->hasQuitBattle() || nUnits == 0)
      return;

   int range = BattleMeasure::XYardsPerHex * 6;
   BattleData* bd = const_cast<BattleData*>(reinterpret_cast<const BattleData*>(gameInfo()->battleData()));

   switch(id)
   {
      // We are routing or fleeing
      case BattleMessageID::BMSG_Routing:
      case BattleMessageID::BMSG_FleeingTheField:
      {
         // if random percent of XX's are routing/fleeing, set flag
         int percentRouting = gameInfo()->random(30, 50);
         int nUnitsRouting = 0;
         for(int r = 0; r < 2; r++)
         {
            int nCols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
            for(int c = 0; c < nCols; c++)
            {
               MU_DeployItem& di = mi.map().item(c, r);
               if(di.cp()->hasQuitBattle() ||
                  di.cp()->routing() ||
                  di.cp()->fleeingTheField())
               {
                  nUnitsRouting++;
               }
            }
         }

         d_planner->d_objective->routingFleeing((MulDiv(nUnitsRouting, 100, nUnits) >= percentRouting));
         break;
      }

      // Taking losses
      case BattleMessageID::BMSG_CriticalLossReached:
      case BattleMessageID::BMSG_TakingLosses:
      {
         int percentLosses = gameInfo()->random(25, 40);
         int totalStartStr = 0;
         int totalStr = 0;
         for(int r = 0; r < 2; r++)
         {
            int nCols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
            for(int c = 0; c < nCols; c++)
            {
               MU_DeployItem& di = mi.map().item(c, r);
               totalStartStr += di.cp()->startStrength();
               totalStr += BobUtility::unitSPStrength(bd, const_cast<BattleCP*>(di.cp()));
            }
         }

         if(totalStartStr > 0)
         {
            d_planner->d_objective->takingHeavyLosses((MulDiv(totalStr, 100, totalStartStr) >= percentLosses));
         }
         break;
      }

      // retreating
      case BattleMessageID::BMSG_Retreating:
      {
//         d_planner->setFlag(MidLevelPlanner::ObjectiveAchieved, False);

         int percentRetreating = gameInfo()->random(40, 70);
         int totalRetreating = 0;
         for(int r = 0; r < 2; r++)
         {
            int nCols = (r == 0) ? mi.map().nFront() : mi.map().nRear();
            for(int c = 0; c < nCols; c++)
            {
               MU_DeployItem& di = mi.map().item(c, r);
               if(di.cp()->movingBackwards() ||
                  di.cp()->routing() ||
                  di.cp()->fleeingTheField())
               {
                  totalRetreating++;
               }
            }
         }

         d_planner->d_objective->retreating((MulDiv(totalRetreating, 100, nUnits) >= percentRetreating));
         break;
      }

      // enemy retreating
      case BattleMessageID::BMSG_EnemyRouting:
      case BattleMessageID::BMSG_EnemyFleeing:
      case BattleMessageID::BMSG_EnemyRetreating:
      {
         int percentRetreating = 30;//gameInfo()->random(30, 70);
         int nEnemy = 0;
         int nEnemyRetreating = 0;
         SListIter<BattleItem> iter(&d_planner->d_targets);
         while(++iter)
         {
            if(iter.current()->d_range <= (4 * BattleMeasure::XYardsPerHex))
            {
               nEnemy++;
               if(iter.current()->d_cp->routing() ||
                  iter.current()->d_cp->fleeingTheField() ||
                  iter.current()->d_cp->movingBackwards())
               {
                  nEnemyRetreating++;
               }
            }
         }

         d_planner->d_objective->enemyRetreating(((nEnemy > 0) && MulDiv(nEnemyRetreating, 100, nEnemy) >= percentRetreating));
         break;
      }

      // inflicting losses
      case BattleMessageID::BMSG_InflictingLosses:
      {
         int percentLosses = 25;//gameInfo()->random(25, 40);
         int startStr = 0;
         int curStr = 0;
         SListIter<BattleItem> iter(&d_planner->d_targets);
         while(++iter)
         {
            if(iter.current()->d_range <= (4 * BattleMeasure::XYardsPerHex))
            {
               startStr += iter.current()->d_cp->startStrength();
               curStr += BobUtility::unitSPStrength(bd, iter.current()->d_cp);
            }
         }

         d_planner->d_objective->inflictingHeavyLosses(((startStr > 0) && MulDiv(curStr, 100, startStr) >= percentLosses));
         break;
      }

      case BattleMessageID::BMSG_EnemySighted:
      {
         if(d_planner->d_objective->movingToFront() &&
            d_planner->d_targets.closeRange() <= (4 * BattleMeasure::XYardsPerHex))
         {
            d_planner->d_objective->mode(HiObjective::FrontLine);
            d_planner->d_objective->movingToFront(False);
         }
         break;
      }
   }


}

//  If we have more than 2 units and only one is up front,
//  move up another
bool MidLevel_Op::deployFromColumn(MidUnitInfo& mi)
{
#if 0
   int nInf = 0;
   int nCav = 0;
   int nArt = 0;
   int totalSP = 0;
   const BattleData* bd = d_planner->gameInfo()->battleData();
   corpsTypes(bd, mi.cp(), nInf, nCav, nArt, totalSP, False);
#endif
   return False;
}

bool MidLevel_Op::processPursue(CRefBattleCP enemyCP)
{
   if(enemyCP == NoBattleCP)
      return False;

   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle())
      return False;

#ifdef DEBUG
   gameInfo()->log("Processing if %s will pursue %s", mi.cp()->getName(), enemyCP->getName());
#endif
   int chance = 98;
   if(gameInfo()->random(100) < chance)
   {
      // find a cavalry to pursue
      MU_DeployItem* bestDI = 0;
      int closeDist = UWORD_MAX;
      HexCord targetHex;
      for(std::vector<MU_DeployItem>::iterator di = mi.map().container().begin();
          di != mi.map().container().end();
          ++di)
      {
         if(di->cp()->hasQuitBattle())
            continue;

         if(di->cp()->generic()->isCavalry())
         {
            int dist = WG_BattleAI_Utils::getDist(di->cp()->leftHex(), enemyCP->rightHex());
            if(dist < closeDist)
            {
               bestDI = &*di;
               closeDist = dist;
               targetHex = enemyCP->rightHex();
            }

         }
      }

      if(bestDI)
      {
#ifdef DEBUG
         gameInfo()->log("---> %s will pursue %s", bestDI->cp()->getName(), enemyCP->getName());
#endif
         launchSortie(bestDI, targetHex, targetHex, bestDI->cp()->facing());
         return True;
      }
   }

   return False;
}

// Determine if any units need to deploy
// Units that are in march deployment or march formation need to deploy
bool MidLevel_Op::doDeploy(CRefBattleCP cp)
{
   MidUnitInfo& mi = d_planner->d_units->front();
   int nUnits = mi.map().nFront() + mi.map().nRear();
   if(mi.cp()->hasQuitBattle() || nUnits == 0)
      return False;

   bool changingFormation = False;
   MU_DeployItem* di = mi.map().find(cp);
   if(di)
   {
      if(cp->formation() == CPF_March ||
         (cp->sp() && !cp->generic()->isArtillery() && cp->spFormation() == SP_MarchFormation))
      {
//       clearMove(*di);
         di->updateOrder();
         clearMove(*di);
         di->ordered(False);

         if(cp->sp() &&
            !cp->generic()->isArtillery() &&
            cp->spFormation() == SP_MarchFormation)
         {
            SPFormation spFor = (cp->moving() || d_planner->d_objective->posture() == BattleOrderInfo::Offensive || cp->generic()->isCavalry()) ?
               SP_ColumnFormation : SP_LineFormation;

            spFor = (di->order().spFormation() > spFor) ? di->order().spFormation() : spFor;

            di->order().spFormation(spFor);
            di->order().order().d_whatOrder.add(WhatOrder::SPFormation);
            di->changingSPFormation(True);
#ifdef DEBUG
            gameInfo()->log("%s is changing formation from SP March formation", cp->getName());
#endif
         }

         if(!di->changingDivFormation() && cp->formation() == CPF_March)
         {
            if(di->order().divFormation() == CPF_March)
            {
               di->order().divFormation(CPF_Massed);
               di->order().order().d_whatOrder.add(WhatOrder::CPFormation);
               di->changingDivFormation(True);
#ifdef DEBUG
               gameInfo()->log("%s is deploying from CP March formation", cp->getName());
#endif
            }
         }

         changingFormation = True;
      }
   }

   return ((nUnits > 2 && mi.map().nFront() == 1 && deployFromColumn(mi)) || changingFormation);
}

bool MidLevel_Op::doDeploy()
{
   bool ordered = False;
   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle())
      return False;

   // first see if any units are in march
   bool keepGoing = False;
   for(std::vector<MU_DeployItem>::iterator di = mi.map().container().begin();
       di != mi.map().container().end();
       ++di)
   {
      if(di->cp()->hasQuitBattle() || !di->cp()->sp())
         continue;

      if(di->cp()->formation() <= CPF_Massed ||
         (!di->cp()->generic()->isArtillery() && di->cp()->spFormation() == SP_MarchFormation) )
      {
         keepGoing = True;
         break;
      }
   }


   if(!keepGoing)
      return False;

   if(d_planner->d_targets.entries() > 0 ||
      enemyInSight(mi.cp(), gameInfo()->battleData()))
   {
#ifdef DEBUG
      gameInfo()->log("Checking %s for units that need to deploy", mi.cp()->getName());
#endif
      for(std::vector<MU_DeployItem>::iterator di = mi.map().container().begin();
         di != mi.map().container().end();
         ++di)
      {
         if(di->cp()->hasQuitBattle() || !di->cp()->sp())
            continue;

         if(di->changingSPFormation() &&
            (di->cp()->spFormation() > SP_MarchFormation) )// || di->cp()->liningUp()) )
         {
            di->changingSPFormation(False);
         }

         else if(!di->changingSPFormation() &&
                 !di->cp()->generic()->isArtillery() &&
                 di->cp()->spFormation() == SP_MarchFormation)
         {
            di->ordered(False);
            clearMove(*di);
            SPFormation spFor = (di->cp()->moving() || d_planner->d_objective->posture() == BattleOrderInfo::Offensive || di->cp()->generic()->isCavalry()) ?
                  SP_ColumnFormation : SP_LineFormation;

            di->order().spFormation(spFor);
            di->order().order().d_whatOrder.add(WhatOrder::SPFormation);
            di->changingSPFormation(True);
#ifdef DEBUG
            gameInfo()->log("%s is changing formation from SP March formation", di->cp()->getName());
#endif
         }


         if(di->changingDivFormation() && di->cp()->formation() > CPF_March)
         {
            di->changingDivFormation(False);
         }
         else if(!di->changingDivFormation() && di->cp()->formation() == CPF_March)
         {
            clearMove(*di);
            di->ordered(False);
            di->order().divFormation(CPF_Deployed);
            di->order().order().d_whatOrder.add(WhatOrder::CPFormation);
            di->changingDivFormation(True);
#ifdef DEBUG
            gameInfo()->log("%s is changing formation from CP March formation", di->cp()->getName());
#endif
         }

         if(!di->ordered())
            ordered = True;
      }
   }

   return ordered;
}

void MidLevel_Op::procUnableToDeploy(const RefBattleCP& cp)
{
   MidUnitInfo& mi = d_planner->d_units->front();
   if(mi.cp()->hasQuitBattle())
      return;

#ifdef DEBUG
   gameInfo()->log("%s is unable to deploy", cp->getName());
#endif

   MU_DeployItem* di = mi.map().find(cp);
   ASSERT(di);
   if(di && di->objHex() != HexCord(0, 0) && di->objHex() != cp->leftHex())
   {
#ifdef DEBUG
      gameInfo()->log("%s moving on to objective", cp->getName());
#endif
      setMoveOrder((*di), di->objHex(), cp->facing(), True);
   }
}

bool MidLevel_Op::procMessage(const BattleMessageInfo& msg)
{
#ifdef DEBUG
   String text = BattleMessageFormat::messageToText(msg, gameInfo()->battleData());
   gameInfo()->log("ProcMessage: %s", (const char*) text.c_str());
#endif

   bool enemyFront = checkForEnemyToFront();
   bool enemyFlank = checkFlanks();
   BattleMessageID::BMSG_ID id = msg.id().id();
   switch(id)
   {
       // Stuff where we should consider retreating
      case BattleMessageID::BMSG_Retreating:
      case BattleMessageID::BMSG_TakingLosses:
      case BattleMessageID::BMSG_Routing:
      case BattleMessageID::BMSG_FleeingTheField:
      case BattleMessageID::BMSG_CriticalLossReached:
      case BattleMessageID::BMSG_Wavering:
         shouldAttach(msg.cp(), AW_Retreating);
         if(!(d_planner->d_flags & MidLevelPlanner::Retreating) )
         {
            if(id == BattleMessageID::BMSG_TakingLosses)
            {
//               d_planner->setFlag(MidLevelPlanner::ConsiderRetreat);
               if(movingOutOfRange())
                  break;
            }

            if(!moveUpRear())
            {
               if(d_planner->d_flags & MidLevelPlanner::ConsiderRetreat)
               {
                  considerRetreat();
               }

               if((d_planner->d_flags & MidLevelPlanner::Retreating) == 0)
                  moveUpSupport();
            }
         }
         break;

       // Stuff where we should consider reacting to enemy to the front
       case BattleMessageID::BMSG_EnemySighted:
       case BattleMessageID::BMSG_TakingArtilleryFire:
       case BattleMessageID::BMSG_TakingMusketFire:
       case BattleMessageID::BMSG_IntoCloseCombat:
          if(id == BattleMessageID::BMSG_TakingArtilleryFire)
          {
             if(movingOutOfRange())
               break;
          }

          if(enemyFront)
          {
             if(!doEnemyToFront())
                moveUpSupport();
          }
          break;

      // Unimplemented messages
      case BattleMessageID::BMSG_EnemyRetreating:
          if(d_planner->bombarding())
            d_planner->bombardFor(1);
          processPursue(msg.target());
          break;
      case BattleMessageID::BMSG_LeaderHit:
          break;
      case BattleMessageID::BMSG_OutOfAmmo:
          break;
      case BattleMessageID::BMSG_AmmoRemaingin:
          break;
      case BattleMessageID::BMSG_UnableToDeploy:
          procUnableToDeploy(const_cast<BattleCP*>(msg.cp()));
          break;
      case BattleMessageID::BMSG_UnableToFindRoute:
          break;
      case BattleMessageID::BMSG_RefusingFlank:
          break;

   }

   // set high-level flags
   setHighLevelFlags(id);

   return True;
}

}; // end namespace


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2002/11/16 18:03:26  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
