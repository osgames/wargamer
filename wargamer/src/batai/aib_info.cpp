/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Battle AI Game Information
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "aib_info.hpp"
#include "scenario.hpp"
#include "app.hpp"
#include "control.hpp"
#include "hexdata.hpp"

#if defined(HAS_LOG_WINDOW)
// #include <string.hpp>
#include "logw_imp.hpp"
#endif


namespace WG_BattleAI_Internal
{


GameInfo::GameInfo(Side s) :
  d_side(s),
  d_random(),
  d_deploying(false)
{
#ifdef DEBUG        // Force seed to be same if in debug mode
  d_random.seed(s);
#endif
#if defined(HAS_LOG_WINDOW)
  String title = scenario->getSideName(d_side);
  title += " BattleAI";
  String logName = scenario->getSideName(d_side);
  logName += "_BattleAI.log";

  // WindowsLogWindow::make(&d_logWindow, APP::getMainHWND(), title.c_str(), logName.c_str());
  d_logWindow.set(WindowsLogWindow::make(APP::getMainHWND(), title.c_str(), logName.c_str()));
  // if(d_logWindow)
  d_logWindow.printf("BattleAI (side=%s) constructed",
                     scenario->getSideName(d_side));
#endif
}


#if defined(HAS_LOG_WINDOW)

void GameInfo::log(const char* fmt, ...)
{
//   if(d_logWindow != 0)
  {
    va_list vaList;
    va_start(vaList, fmt);
    d_logWindow.vprintf(fmt, vaList);
    va_end(vaList);
  }
}

void GameInfo::logStream()
{
  d_stream << '\0';
	std::string bufStr = d_stream.str();
  const char* buf = bufStr.c_str();
  d_logWindow.printf("%s", buf);
  // delete[] buf;
  // d_stream.rdbuf()->freeze(0);
  d_stream.seekp(0, std::ios::beg);
}

#endif

/*
 * Return the Y coordinate of our end of the battlefield
 * e.g. either 0 or 63 (mapHeight-1)
 *
 * The player always plays from the bottom (y=0)
 * If both sides are AI then side 0 is bottom, side 1 is top.
 * If both sides are Player then side 0 is bottom
 *
 * Thus... the only time side 0 does not play from the bottom
 * is when 0 is AI controlled and 1 is player controlled.
 */

HexCord::Cord GameInfo::getBattleEdge(Side side) const
{
  bool atTop = (side == 1);

  if( (GamePlayerControl::getControl(0) == GamePlayerControl::AI) &&
      (GamePlayerControl::getControl(1) != GamePlayerControl::AI) )
  {
    atTop = !atTop;
  }

  if(atTop)
    return map()->getSize().y() - 1;
  else
    return 0;

}

HexCord::Cord GameInfo::whichEdge() const
{
  return getBattleEdge(side());
}

HexCord::Cord GameInfo::otherEdge() const
{
  return getBattleEdge(otherSide());
}

}  // namespace WG_BattleAI_Internal




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.5  2002/11/16 18:03:26  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.4  2001/07/30 09:27:23  greenius
 * Various bug fixes
 *
 * Revision 1.3  2001/07/02 13:18:24  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
