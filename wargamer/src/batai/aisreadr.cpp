/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "aisreadr.hpp"
#include "filecnk.hpp"
#include "aib_hi.hpp"
#include "idstr.hpp"
#include "h_zlist.hpp"
#include "hi_zone.hpp"
#include "h_cplist.hpp"
#include "aib_data.hpp"
#include "aib_side.hpp"
#ifdef DEBUG
#include "clog.hpp"
#include "scenario.hpp"
LogFile errorLog("BAIScriptErrorLog.log");
#endif
namespace WG_BattleAI_Internal
{

#ifdef DEBUG
static bool s_error = False;
#endif

class ScriptReader : public WinFileAsciiChunkReader {
   public:
      ScriptReader(const TCHAR* fileName) :  WinFileAsciiChunkReader(fileName) {}
};

BAI_ScriptReader::BAI_ScriptReader(RPBattleData bd, const TCHAR* fileName, BattleSideAI* ai, FileWriter& bFile) :
   d_batData(bd),
   d_sideAI(ai),
   d_fileName(fileName),
   d_planner(0)
{
   ScriptReader f(fileName);
   convertToBinary(f, bFile);
}

BAI_ScriptReader::BAI_ScriptReader(RPBattleData bd, const TCHAR* fileName, BattleSideAI* ai) :
   d_batData(bd),
   d_sideAI(ai),
   d_fileName(fileName),
   d_planner(d_sideAI->d_planner)
{
//   ScriptReader f(fileName);
//   convertToBinary(f, bFile);
   WinFileBinaryChunkReader file(fileName);
   readBinary(file);
}

BAI_ScriptReader::BAI_ScriptReader(RPBattleData bd, const TCHAR* fileName, HighLevelPlanner* planner) :
   d_batData(bd),
   d_sideAI(0),
   d_fileName(fileName),
   d_planner(planner)
{
   ASSERT(d_planner);
   ScriptReader f(fileName);
   read(f);
}


// Keywords
const TCHAR* s_french = "[French]";
const TCHAR* s_allies = "[Allies]";
const TCHAR* s_plan  = "[Plan]";
const TCHAR* s_zone  = "[Zone]";
const TCHAR* s_obj   = "[Objective]";
const TCHAR* s_end   = "[End]";

// Tokens
const TCHAR* s_zoneBoundry = "ZoneBoundry";
const TCHAR* s_zoneAgg     = "ZoneAggression";
const TCHAR* s_zonePos     = "ZonePosture";
const TCHAR* s_objBoundry  = "ObjBoundry";
const TCHAR* s_unit        = "Unit";
const TCHAR* s_leader      = "Leader";
const TCHAR* s_type        = "Type";
const TCHAR* s_agg         = "Aggression";
const TCHAR* s_pos         = "Posture";
const TCHAR* s_cincAgg     = "CinCAggression";
const TCHAR* s_cincPos     = "CinCPosture";
const TCHAR* s_planType    = "PlanType";
const TCHAR* s_sideBoundry = "SideBoundry";

const TCHAR* s_sideChunk   = "Side";
const TCHAR* s_planChunk   = "Plan";
TCHAR s_strBuf[300]; // a useful buffer

static UWORD s_version = 0x0000;
void BAI_ScriptReader::convertToBinary(FileReader& f, FileWriter& bFile)
{
#ifdef DEBUG
   errorLog.printf("Converting script to binary for %s %s",
      scenario->getSideName(d_sideAI->d_aiData->side()), d_fileName);
#endif

   wsprintf(s_strBuf, "%s%d", s_sideChunk, static_cast<int>(d_sideAI->d_aiData->side()));
   FileChunkWriter fc(bFile, s_strBuf);
   bFile << s_version;

   getSideBoundries(f);
   HexCord leftHex;
   HexCord rightHex;

   for(int s = 0; s < 2; s++)
   {
      d_batData->getSideLeftRight(s, leftHex, rightHex);
      leftHex.writeData(bFile);
      rightHex.writeData(bFile);
   }

   getSide(f, d_sideAI->d_aiData->side());

   SeekPos startPos = f.getPos();

   // count number of plans
   int plans = 0;
   for(;;)
   {
      TCHAR* line = f.readLine();
      if(lstrcmp(s_plan, line) == 0)
      {
//         convertPlan(f, bFile, plan);
         plans++;
      }

      else if(lstrcmp(s_end, line) == 0 ||
                  lstrcmp(s_allies, line) == 0 ||
                  lstrcmp(s_french, line) == 0)
      {
         break;
      }
   }

   bFile << plans;

   f.seekTo(startPos);
   // now convert plans

   int plan = 0;
   for(;;)
   {
      TCHAR* line = f.readLine();
      if(lstrcmp(s_plan, line) == 0)
      {
         convertPlan(f, bFile, plan);
         plan++;
      }

      else if(lstrcmp(s_end, line) == 0 ||
                  lstrcmp(s_allies, line) == 0 ||
                  lstrcmp(s_french, line) == 0)
      {
         break;
      }

      if(plan == plans)
         break;
   }

   ASSERT(plan == plans);
}

static UWORD s_planVersion = 0x0000;
void BAI_ScriptReader::convertPlan(FileReader& f, FileWriter& bFile, int plan)
{
   HighLevelPlanner planner(d_sideAI->d_aiData);
   d_planner = &planner;
   d_planner->d_units->updateUnits();
   reinterpret_cast<BattleAIData*>(d_planner->gameInfo())->process();

   SeekPos startPos = bFile.getPos();

   // Put a place holder, to determine how far to advance to next plan
   ULONG placeHolder = 0;
   bFile << placeHolder;
   bFile << s_planVersion;

   // get army data
   StringParse parse;
   SeekPos lastPos = f.getPos();
   BattleOrderInfo::Aggression ag = static_cast<BattleOrderInfo::Aggression>(2);
   BattleOrderInfo::Posture pos = BattleOrderInfo::Defensive;
   for(;;)
   {
      TCHAR* s = f.readLine();
      if(lstrcmp(s, s_zone) == 0)
      {
         f.seekTo(lastPos);
         break;
      }
      else if(*s == '{' ||
              *s == '}')
      {
      }
      else
      {
         parse.init(s);
         TCHAR* tok = parse.getToken();
         if(lstrcmp(tok, s_planType) == 0)
         {
            unsigned int n;
            if(!parse.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("Plan not found for %s", scenario->getSideName(d_planner->gameInfo()->side()));
               s_error = True;
#endif
            }
            else
            {
               d_planner->plan(static_cast<Hi_OpPlan::Plan>(n));
            }
         }
         else if(lstrcmp(tok, s_cincAgg) == 0)
         {
            unsigned int n;
            if(!parse.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("CinC aggression not found for %s", scenario->getSideName(d_planner->gameInfo()->side()));
               s_error = True;
#endif
            }
            else
            {
               ag = static_cast<BattleOrderInfo::Aggression>(n);
            }
         }
         else if(lstrcmp(tok, s_cincPos) == 0)
         {
            unsigned int n;
            if(!parse.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("Cinc posture not found for %s", scenario->getSideName(d_planner->gameInfo()->side()));
               s_error = True;
#endif
            }
            else
            {
               pos = static_cast<BattleOrderInfo::Posture>(n);
            }
         }
      }

      lastPos = f.getPos();
   }

   UBYTE b = static_cast<UBYTE>(ag);
   bFile << b;
   b = static_cast<UBYTE>(pos);
   bFile << b;

   int nZones = countZones(f);
   while(nZones--)
   {
      readZone(f);
   }

#ifdef DEBUG
   // make sure all corps and independent divisions have been added
   for(HiUnitList::iterator unitIt = d_planner->d_units->begin();
       unitIt != d_planner->d_units->end();
       ++unitIt)
   {
      HiUnitInfo& unitInfo = *unitIt;
      if(!unitInfo.inZone())
      {
         errorLog.printf("%s has no zone. It is not in the script file (%s)",
               unitInfo.cp()->getName(), d_fileName);

         s_error = True;
      }
      if(!unitInfo.objective())
      {
         errorLog.printf("%s has no objective. It is not in the script file (%s)",
               unitInfo.cp()->getName(), d_fileName);

         s_error = True;
      }
   }


#if 0
   ConstBattleUnitIter unitIter = d_planner->gameInfo()->ourUnits();
   while(!unitIter.isFinished())
   {
      if(unitIter.cp()->getRank().sameRank(Rank_Corps) ||
         (unitIter.cp()->getRank().sameRank(Rank_Division) && (unitIter.cp()->parent() == NoBattleCP || unitIter.cp()->parent()->getRank().isHigher(Rank_Corps))) )
      {
         if(!d_planner->d_units->find(unitIter.cp()))
         {
            errorLog.printf("%s has not been added. It is not in the script file (%s)",
               unitIter.cp()->getName(), d_fileName);

            s_error = True;
         }
      }

      unitIter.next();
   }
#endif
   if(s_error)
      FORCEASSERT("Error reading script file");
#endif

   d_sideAI->setPlanner(d_planner);
   d_sideAI->writeData(bFile);
   d_sideAI->setPlanner(0);

   // go back to place holder and put number of bytes added
   SeekPos curPos = bFile.getPos();
   placeHolder = curPos - startPos;
   bFile.seekTo(startPos);
   bFile << placeHolder;
   bFile.seekTo(curPos);
}

void BAI_ScriptReader::readBinary(FileReader& f)
{
#ifdef DEBUG
   errorLog.printf("Reading binary script for %s %s",
      scenario->getSideName(d_planner->gameInfo()->side()), d_fileName);
#endif

   wsprintf(s_strBuf, "%s%d", s_sideChunk, static_cast<int>(d_sideAI->d_aiData->side()));
   FileChunkReader fc(f, s_strBuf);
   if(!fc.isOK())
   {
      GeneralError("Error reading chunk(%s) in binary AI script(%s)",  s_strBuf, d_fileName);
   }

   UWORD version;
   f >> version;
   ASSERT(version <= s_version);

   HexCord leftHex;
   HexCord rightHex;
   for(int s = 0; s < 2; s++)
   {
      leftHex.readData(f);
      rightHex.readData(f);
      d_batData->setSideLeftRight(s, leftHex, rightHex);
   }

   int plans;
   f >> plans;

   int plan = (plans > 1) ? d_planner->gameInfo()->random(plans) : 0;
   readBinaryPlan(f, plan);

#ifdef DEBUG
   // make sure all corps and independent divisions have been added
   ConstBattleUnitIter unitIter = d_planner->gameInfo()->ourUnits();
   while(!unitIter.isFinished())
   {
      if(unitIter.cp()->getRank().sameRank(Rank_Corps) ||
         (unitIter.cp()->getRank().sameRank(Rank_Division) && (unitIter.cp()->parent() == NoBattleCP || unitIter.cp()->parent()->getRank().isHigher(Rank_Corps))) )
      {
         if(!d_planner->d_units->find(unitIter.cp()))
         {
            errorLog.printf("%s has not been added. It is not in the script file (%s)",
               unitIter.cp()->getName(), d_fileName);

            s_error = True;
         }
      }

      unitIter.next();
   }

   if(s_error)
      FORCEASSERT("Error reading script file");
#endif
}

void BAI_ScriptReader::readBinaryPlan(FileReader& f, int plan)
{
   ULONG nextPlan;
#ifdef DEBUG
   SeekPos fpos = f.getPos();
#endif
   while(plan--)
   {
      f >> nextPlan;
      f.seekTo((f.getPos() + nextPlan) - sizeof(ULONG));
#ifdef DEBUG
      fpos = f.getPos();
#endif
   }

   f >> nextPlan;
   UWORD version;
   f >> version;
   ASSERT(version <= s_planVersion);

   UBYTE b;
   f >> b;
   BattleOrderInfo::Aggression ag = static_cast<BattleOrderInfo::Aggression>(b);
   f >> b;
   BattleOrderInfo::Posture pos = static_cast<BattleOrderInfo::Posture>(b);

   d_sideAI->readData(f);

   BattleCP* cinc = const_cast<BattleCP*>(d_planner->gameInfo()->cinc());
   ASSERT(cinc);
   if(cinc)
   {
      cinc->aggression(ag);
      cinc->getCurrentOrder().aggression(ag);
      cinc->posture(pos);
      cinc->getCurrentOrder().posture(pos);
   }

}

void BAI_ScriptReader::read(FileReader& f)
{
#ifdef DEBUG
   errorLog.printf("Reading script for %s %s",
      scenario->getSideName(d_planner->gameInfo()->side()), d_fileName);
#endif
   getSideBoundries(f);
   getSide(f, d_planner->gameInfo()->side());
   getPlan(f);

   // add units to main list
   d_planner->d_units->updateUnits();
   reinterpret_cast<BattleAIData*>(d_planner->gameInfo())->process();
#if 1
   // get army leaders
   StringParse parse;
   SeekPos lastPos = f.getPos();
   for(;;)
   {
      TCHAR* s = f.readLine();
      if(lstrcmp(s, s_zone) == 0)
      {
         f.seekTo(lastPos);
         break;
      }
      else if(*s == '{' ||
                  *s == '}')
      {
      }
      else
      {
         parse.init(s);
         TCHAR* tok = parse.getToken();
         if(lstrcmp(tok, s_planType) == 0)
         {
            unsigned int n;
            if(!parse.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("Plan not found for %s", scenario->getSideName(d_planner->gameInfo()->side()));
               s_error = True;
#endif
//             FORCEASSERT("Plan not found");
            }
            else
            {
               d_planner->plan(static_cast<Hi_OpPlan::Plan>(n));
            }
         }
         else if(lstrcmp(tok, s_cincAgg) == 0)
         {
            unsigned int n;
            if(!parse.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("CinC aggression not found for %s", scenario->getSideName(d_planner->gameInfo()->side()));
               s_error = True;
#endif
//             FORCEASSERT("CinCAggression not found");
            }
            else
            {
               BattleOrderInfo::Aggression ag = static_cast<BattleOrderInfo::Aggression>(n);
               BattleCP* cinc = const_cast<BattleCP*>(d_planner->gameInfo()->cinc());
               cinc->aggression(ag);
               cinc->getCurrentOrder().aggression(ag);
            }
         }
         else if(lstrcmp(tok, s_cincPos) == 0)
         {
            unsigned int n;
            if(!parse.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("Cinc posture not found for %s", scenario->getSideName(d_planner->gameInfo()->side()));
               s_error = True;
#endif
//             FORCEASSERT("Posture not found");
            }
            else
            {
               BattleOrderInfo::Posture pos = static_cast<BattleOrderInfo::Posture>(n);
               BattleCP* cinc = const_cast<BattleCP*>(d_planner->gameInfo()->cinc());
               cinc->posture(pos);
               cinc->getCurrentOrder().posture(pos);
            }
         }
      }

      lastPos = f.getPos();
   }
#endif
   int nZones = countZones(f);
   while(nZones--)
   {
      readZone(f);
   }

#ifdef DEBUG
   // make sure all corps and independent divisions have been added
   ConstBattleUnitIter unitIter = d_planner->gameInfo()->ourUnits();
   while(!unitIter.isFinished())
   {
      if(unitIter.cp()->getRank().sameRank(Rank_Corps) ||
         (unitIter.cp()->getRank().sameRank(Rank_Division) && (unitIter.cp()->parent() == NoBattleCP || unitIter.cp()->parent()->getRank().isHigher(Rank_Corps))) )
      {
         if(!d_planner->d_units->find(unitIter.cp()))
         {
            errorLog.printf("%s has not been added. It is not in the script file (%s)",
               unitIter.cp()->getName(), d_fileName);

            s_error = True;
         }
      }

      unitIter.next();
   }

   if(s_error)
      FORCEASSERT("Error reading script file");
#endif
}

void BAI_ScriptReader::getSide(FileReader& f, Side side)
{
   ASSERT(f.isAscii());
   f.seekTo(0);

   const TCHAR* sideStr = (side == 0) ? s_french : s_allies;
   for(;;)
   {
      TCHAR* s = f.readLine();
      if(lstrcmp(sideStr, s) == 0)
         break;

      else if(lstrcmp(s_end, s) == 0)
      {
#ifdef DEBUG
         errorLog.printf("Side (%s) not found", scenario->getSideName(side));
         s_error = True;
#endif
//       FORCEASSERT("Side not found");
         break;
      }
   }
}

void BAI_ScriptReader::getSideBoundries(FileReader& f)
{
   for(Side s = 0; s < 2; s++)
   {
      getSide(f, s);
      for(;;)
      {
         TCHAR* str = f.readLine();
         if(lstrcmp(s_end, str) == 0 ||
            lstrcmp(s_plan, str) == 0)
         {
#ifdef DEBUG
            errorLog.printf("Side (%s) boundry not found", scenario->getSideName(s));
            s_error = True;
#endif
//          FORCEASSERT("Side Boundry not found");
            break;
         }

         StringParse parser;
         parser.init(str);
         char* token = parser.getToken();
         if(lstrcmp(token, s_sideBoundry) == 0)
         {
            // get HexCord x and y values
            HexCord leftHex;
            HexCord rightHex;

            unsigned int n;

            // leftHex.x()
            if(!parser.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("Side boundry HexCord leftHex.x value not in script");
               s_error = True;
#endif
            }
            else
               leftHex.x(static_cast<HexCord::Cord>(n));

            // leftHex.y()
            if(!parser.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("Side boundry HexCord leftHex.y value not in script");
               s_error = True;
#endif
            }
            else
               leftHex.y(static_cast<HexCord::Cord>(n));

            // leftHex.x()
            if(!parser.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("Side boundry HexCord rightHex.x value not in script");
               s_error = True;
#endif
            }
            else
               rightHex.x(static_cast<HexCord::Cord>(n));

            // leftHex.x()
            if(!parser.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("Side boundry HexCord rightHex.y value not in script");
               s_error = True;
#endif
            }
            else
               rightHex.y(static_cast<HexCord::Cord>(n));

            d_batData->setSideLeftRight(s, leftHex, rightHex);
            break;
         }
      }
   }
}

void BAI_ScriptReader::getPlan(FileReader& f)
{
   ASSERT(f.isAscii());
   SeekPos startPos = f.getPos();

   // first, count number of plans
   int nPlans = 0;
   for(;;)
   {
      TCHAR* line = f.readLine();
      if(lstrcmp(s_plan, line) == 0)
         nPlans++;

      else if(lstrcmp(s_end, line) == 0 ||
                  lstrcmp(s_allies, line) == 0 ||
                  lstrcmp(s_french, line) == 0)
      {
         break;
      }
   }

   ASSERT(nPlans > 0);

   // now, pick plan
   int plan = (nPlans > 1) ? d_planner->gameInfo()->random(nPlans) + 1 : 1;

   // advance file to plan
   f.seekTo(startPos);
   do
   {
      TCHAR* line = f.readLine();
      if(lstrcmp(s_plan, line) == 0)
         plan--;

   } while(plan);
}

void BAI_ScriptReader::readZone(FileReader& f)
{
   // first, advance to zone keyword
   for(;;)
   {
      TCHAR* line = f.readLine();
      if(lstrcmp(line, s_zone) == 0)
         break;
   }

   // first, count and reserve objectives
   SeekPos pos = f.getPos();
   int nObj = 0;
   for(;;)
   {
      TCHAR* line = f.readLine();

      if(lstrcmp(line, s_zone)   == 0 ||
          lstrcmp(line, s_plan)   == 0 ||
          lstrcmp(line, s_end)    == 0 ||
          lstrcmp(line, s_allies) == 0 ||
          lstrcmp(line, s_french) == 0)
      {
         break;
      }

      if(lstrcmp(line, s_obj) == 0)
         nObj++;
   }

   ASSERT(nObj > 0);

   f.seekTo(pos);

   // get boundries
   StringParse parser;
   HiZone* zone = 0;
   SeekPos lastPos = pos;

// SeekPos lastPos = f.getPos();
   for(;;)
   {
      TCHAR* line = f.readLine();

      if(lstrcmp(line, s_zone) == 0 ||
         lstrcmp(line, s_plan) == 0)
      {
         f.seekTo(lastPos);
         break;
      }

      if(lstrcmp(line, s_end)    == 0 ||
         lstrcmp(line, s_allies) == 0 ||
         lstrcmp(line, s_french) == 0)
      {
         break;
      }

      if(//*line == '[' ||
          *line == '{' ||
          *line == '}')
      {
         lastPos = f.getPos();
         continue;
      }

      // if an objective keyword
      if(lstrcmp(line, s_obj) == 0)
      {
         ASSERT(zone);
         allocateObj(zone, f);
      }
      else
      {
         parser.init(line);
         char* token = parser.getToken();

         // if a zone
         if(lstrcmp(token, s_zoneBoundry) == 0)
         {
            bool newZone = (zone == 0);
            allocateZone(&zone, parser);
            if(newZone)
               zone->d_objectives.reserve(nObj);
         }

         else if(lstrcmp(token, s_zoneAgg) == 0)
         {
            ASSERT(zone);
            unsigned int n;
            if(!parser.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("No value found for Zone 'Aggression' token");
               s_error = True;
#endif
            }
            else
               zone->aggression(static_cast<BattleOrderInfo::Aggression>(n));
         }
         else if(lstrcmp(token, s_zonePos) == 0)
         {
            ASSERT(zone);
            unsigned int n;
            if(!parser.getInt(n))
            {
#ifdef DEBUG
               errorLog.printf("No value found for Zone 'Posture' token");
               s_error = True;
#endif
            }
            else
               zone->posture(static_cast<BattleOrderInfo::Posture>(n));
         }
      }

      lastPos = f.getPos();
   }

   // Set as current obj achieved
   if(zone)
   {
      for(int i = 0; i < zone->d_objectives.size(); i++)
      {
         zone->d_objectives[i]->d_planner->addUnits();
         zone->d_objectives[i]->d_planner->initDeployMap();
         zone->d_objectives[i]->d_planner->onObjectiveAchieved();
         zone->d_objectives[i]->d_planner->sendCPAggPos();
      }
   }
}

int BAI_ScriptReader::countZones(FileReader& f)
{
   SeekPos startPos = f.getPos();
   int nZones = 0;

   for(;;)
   {
      TCHAR* line = f.readLine();
      if(lstrcmp(s_zone, line) == 0)
         nZones++;

      else if(lstrcmp(s_plan, line)   == 0 ||
                  lstrcmp(s_end,  line)   == 0 ||
                  lstrcmp(s_allies, line) == 0 ||
                  lstrcmp(s_french, line) == 0)
      {
         break;
      }
   }

   f.seekTo(startPos);
   return nZones;
}

void BAI_ScriptReader::setObjBoundry(RealHiObjective* obj, StringParse& parser)
{
   // get HexCord x and y values
   HexCord leftHex;
   HexCord rightHex;

   unsigned int n;

   // leftHex.x()
   if(!parser.getInt(n))
   {
#ifdef DEBUG
      errorLog.printf("Objective boundry HexCord leftHex.x value not in script");
      s_error = True;
#endif
   }
   else
      leftHex.x(static_cast<HexCord::Cord>(n));

   // leftHex.y()
   if(!parser.getInt(n))
   {
#ifdef DEBUG
      errorLog.printf("Objective boundry HexCord leftHex.y value not in script");
      s_error = True;
#endif
   }
   else
      leftHex.y(static_cast<HexCord::Cord>(n));

   // leftHex.x()
   if(!parser.getInt(n))
   {
#ifdef DEBUG
      errorLog.printf("Objective boundry HexCord rightHex.x value not in script");
      s_error = True;
#endif
   }
   else
      rightHex.x(static_cast<HexCord::Cord>(n));

   // leftHex.x()
   if(!parser.getInt(n))
   {
#ifdef DEBUG
      errorLog.printf("Objective boundry HexCord rightHex.y value not in script");
      s_error = True;
#endif
   }
   else
      rightHex.y(static_cast<HexCord::Cord>(n));

   obj->area(HexArea(leftHex, rightHex, HexCord(), HexCord()), False);
}

void BAI_ScriptReader::allocateZone(HiZone** zone, StringParse& parser)
{
   // get HexCord x and y values
   HexCord leftHex;
   HexCord rightHex;

   unsigned int n;

   // leftHex.x()
   if(!parser.getInt(n))
   {
#ifdef DEBUG
      errorLog.printf("Zone boundry HexCord leftHex.x value not in script");
      s_error = True;
#endif
   }
   else
      leftHex.x(static_cast<HexCord::Cord>(n));

   // leftHex.y()
   if(!parser.getInt(n))
   {
#ifdef DEBUG
      errorLog.printf("Zone boundry HexCord leftHex.y value not in script");
      s_error = True;
#endif
   }
   else
      leftHex.y(static_cast<HexCord::Cord>(n));

   // leftHex.x()
   if(!parser.getInt(n))
   {
#ifdef DEBUG
      errorLog.printf("Zone boundry HexCord rightHex.x value not in script");
      s_error = True;
#endif
   }
   else
      rightHex.x(static_cast<HexCord::Cord>(n));

   // leftHex.x()
   if(!parser.getInt(n))
   {
#ifdef DEBUG
      errorLog.printf("Zone boundry HexCord rightHex.y value not in script");
      s_error = True;
#endif
   }
   else
      rightHex.y(static_cast<HexCord::Cord>(n));

   if(!*zone)
      (*zone) = d_planner->d_zones->createZone(leftHex, rightHex);
   else
      (*zone)->addBoundry(leftHex, rightHex);
}

// Is string1 = to string2, ignoring spaces
const char space = 32;
bool isSame(const char* string1, const char* string2)
{
   const char* c1 = string1;
   const char* c2 = string2;

   while(*c1 != '\0' && *c2 != '\0')
   {
      if(*c1 == space)
      {
         c1++;
         continue;
      }
      if(*c2 == space)
      {
         c2++;
         continue;
      }

      if(*c1 != *c2)
         return False;

      c1++;
      c2++;
   }

   if(*c1 != '\0')
   {
      while(*c1 != '\0')
      {
         if(*c1 != space)
            return False;
         c1++;
      }
   }

   if(*c2 != '\0')
   {
      while(*c2 != '\0')
      {
         if(*c2 != space)
            return False;
         c2++;
      }
   }

   return True;
}

void BAI_ScriptReader::allocateUnit(HiZone* zone, FileReader& f, StringParse& parse)
{
   // get name from script
   char* unitName = parse.getToken();
   lstrcpy(s_strBuf, unitName);
   unitName = s_strBuf;

   // now get leader from next line
   char* s = f.readLine();
   parse.init(s);
   char* tok = parse.getToken();
   ASSERT(lstrcmp(tok, s_leader) == 0);

   char* leaderName = parse.getToken();

   // now go through units and find cp
   CRefBattleCP cp = NoBattleCP;
   ConstBattleUnitIter unitIter = d_planner->gameInfo()->ourUnits();
   while(!unitIter.isFinished())
   {
      if(isSame(unitIter.cp()->getName(), unitName) &&//lstrcmpi(unitIter.cp()->getName(), unitName) == 0 &&
         isSame(unitIter.cp()->leader()->getName(), leaderName))//lstrcmpi(unitIter.cp()->leader()->getName(), leaderName) == 0)
      {
         cp = unitIter.cp();
         break;
      }

      unitIter.next();
   }

// ASSERT(cp != NoBattleCP);

   // Add to main AI list
   if(cp != NoBattleCP)
   {
      if(!zone)
      {
         d_planner->d_units->addUnit(cp);
      }
      else
      {
         HiUnitInfo* hi = d_planner->d_units->find(cp);
         ASSERT(hi);

#ifdef DEBUG
         if(hi->objective())
         {
            errorLog.printf("%s %s has already been added to objective",
               cp->leader()->getName(), cp->getName());

            s_error = True;
         }
#endif
         // now add to zone list
         zone->addUnit(hi);
      }
   }
#ifdef DEBUG
   else
   {
      errorLog.printf("Error reading %s %s in allocate unit", leaderName, unitName);
      s_error = True;
   }
#endif
}

void BAI_ScriptReader::assignObjUnit(HiZone* zone, RealHiObjective* obj, FileReader& f, StringParse& parser)
{
   // get name from script
   char* unitName = parser.getToken();
   lstrcpy(s_strBuf, unitName);
   unitName = s_strBuf;

   // now get leader from next line
   char* s = f.readLine();
   parser.init(s);
   char* tok = parser.getToken();
   ASSERT(lstrcmp(tok, s_leader) == 0);

   char* leaderName = parser.getToken();

   // now go through units and find cp
   CRefBattleCP cp = NoBattleCP;
   ConstBattleUnitIter unitIter = d_planner->gameInfo()->ourUnits();
   while(!unitIter.isFinished())
   {
      if(isSame(unitIter.cp()->getName(), unitName) &&//lstrcmpi(unitIter.cp()->getName(), unitName) == 0 &&
         isSame(unitIter.cp()->leader()->getName(), leaderName))//lstrcmpi(unitIter.cp()->leader()->getName(), leaderName) == 0)
      {
         cp = unitIter.cp();
         break;
      }

      unitIter.next();
   }

// ASSERT(cp != NoBattleCP);

   if(cp != NoBattleCP)
   {
      HiUnitInfo* hi = d_planner->d_units->find(cp);
//    ASSERT(hi);
#ifdef DEBUG
      if(!hi)
      {
         errorLog.printf("%s %s is not a Corps or Independent Division", cp->leader()->getName(), cp->getName());
         s_error = True;
         return;
      }

      if(zone->d_units.isMember(hi) || hi->zone())
      {
         errorLog.printf("%s %s has been added more than once to zone", cp->leader()->getName(), cp->getName());
         s_error = True;
      }
      if(hi->objective())
      {
         errorLog.printf("%s %s has been added more than once to objective", cp->leader()->getName(), cp->getName());
         s_error = True;
      }
#endif
      zone->addUnit(hi);
      zone->addUnitToObjective(hi, obj);
   }
#ifdef DEBUG
   else
   {
      errorLog.printf("Error reading %s %s in allocate unit", leaderName, unitName);
      s_error = True;
   }
#endif

}

void BAI_ScriptReader::allocateObj(HiZone* zone, FileReader& f)
{
   StringParse parser;
   RealHiObjective* obj = 0;
   for(;;)
   {
      char* s = f.readLine();
      if(*s == '{')
         ;

      else if(*s == '}')
         break;

      else
      {
         parser.init(s);
         char* token = parser.getToken();

//    int type = -1;
         {
            if(lstrcmp(token, s_objBoundry) == 0)
            {
          ASSERT(obj);
               setObjBoundry(obj, parser);
            }
            else if(lstrcmp(token, s_type) == 0)
            {
               unsigned int n;
               if(!parser.getInt(n))
               {
#ifdef DEBUG
                  errorLog.printf("No Value for 'Type' token");
                  s_error = True;
#endif
                  //FORCEASSERT("No value found for 'Type' token");
               }
               else
               {
                  obj = zone->d_objectives.create(zone->gameInfo(), static_cast<HiObjective::Mode>(n));
               }
            }
            else if(lstrcmp(token, s_unit) == 0)
            {
               ASSERT(obj);
               assignObjUnit(zone, obj, f, parser);
            }
            else if(lstrcmp(token, s_agg) == 0)
            {
               ASSERT(obj);
               unsigned int n;
               if(!parser.getInt(n))
               {
#ifdef DEBUG
                  errorLog.printf("No value found for Objective 'Aggression' token");
                  s_error = True;
#endif
                  //FORCEASSERT("No value found for 'Aggression' token");
               }
               else
                  obj->aggression(static_cast<BattleOrderInfo::Aggression>(n));
            }
            else if(lstrcmp(token, s_pos) == 0)
            {
               ASSERT(obj);
               unsigned int n;
               if(!parser.getInt(n))
               {
#ifdef DEBUG
                  errorLog.printf("No value found for Objective 'Posture' token");
                  s_error = True;
#endif
                  //FORCEASSERT("No value found for 'Aggression' token");
               }
               else
                  obj->posture(static_cast<BattleOrderInfo::Posture>(n));
            }
         }
      }
   }
}

}; // end namespace
/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
