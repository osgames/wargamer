/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * High level Zones
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "hi_zone.hpp"
#include "hi_unit.hpp"
#include "bobiter.hpp"
#include "hexdata.hpp"
#include "aib_util.hpp"
#include "b_send.hpp"
#include "filebase.hpp"
#include "batmsg.hpp"
#include <numeric>

//#include "batdata.hpp"
#ifdef DEBUG
// #include <strstrea.h>
#include <sstream>
#endif

namespace WG_BattleAI_Internal
{

static UWORD s_boundryVersion = 0x0000;
bool Boundry::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_boundryVersion);

   d_leftHex.readData(f);
   d_rightHex.readData(f);

   return f.isOK();
}

bool Boundry::writeData(FileWriter& f) const
{
   f << s_boundryVersion;

   d_leftHex.writeData(f);
   d_rightHex.writeData(f);

   return f.isOK();
}

static UWORD s_boundryListVersion = 0x0000;
bool BoundryList::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_boundryListVersion);

   int items;
   f >> items;

   while(items--)
   {
      Boundry* b = new Boundry;
      ASSERT(b);

      b->readData(f);
      append(b);
   }

   return f.isOK();
}

bool BoundryList::writeData(FileWriter& f) const
{
   f << s_boundryListVersion;

   int items = entries();
   f << items;

   SListIterR<Boundry> iter(this);
   while(++iter)
   {
      iter.current()->writeData(f);
   }

   return f.isOK();
}

HiZone::HiZone(GameInfo* gameInfo, const HexCord& left, const HexCord& right) :
      GameInfoUser(gameInfo),
      d_units(),
      d_objectives(),
      d_priority(0),
      d_startWhen(0),
      d_nextReserveCheck(0),
      d_nextUpdate(0),
      d_flags(0),
      d_hiFlags(0)
{
//    ASSERT(leftX() <= rightX());
   addBoundry(left, right);
#ifdef DEBUG
   std::ostringstream buf;
      buf << "Zone:" << (int) leftX() << ".." << (int) rightX() << '\0';
      d_name = buf.str();
#endif

   /*
    * Create frontline and reserve objectives
    */

}

HiZone::HiZone(GameInfo* gameInfo) :
      GameInfoUser(gameInfo),
      d_units(),
      d_objectives(),
      d_priority(0),
      d_startWhen(0),
      d_nextReserveCheck(0),
      d_flags(0),
      d_whichWay(HiZone::WhichWay_Undefined)
#if 0
      d_moveLR(0),
      d_moveFB(0)
#endif
{
}

HiZone::~HiZone()
{
      // Remove all units
      while(d_units.size())
      {
            removeUnit(d_units[0]);
      }
}

void HiZone::newDay()
{
   d_nextReserveCheck = 0;
   d_startWhen = 0;
   d_nextUpdate = 0;
   for(int i = 0; i < d_objectives.size(); i++)
   {
      d_objectives[i]->newDay();
      d_objectives[i]->getNewObjective(True);
   }
}


/*
 * Count all SPs underneath a CP including its children
 */

int countSP(const HiUnitInfo* unit)
{
      int total = 0;

      for(ConstBattleUnitIter iter(unit->cp());
            !iter.isFinished();
            iter.next())
      {
            CRefBattleCP cp = iter.cp();

            // count SP chain

            CRefBattleSP sp = cp->sp();
            while(sp != NoBattleSP)
            {
                  ++total;
                  sp = sp->sister();
            }

            //------ would be better to do this instead of the above
            //------ but unfortunately spCount is not declared const and
            //------ and is checked out by Paul.
            // total += cp->spCount(true);

      }

      return total;
}

struct AddCountSP : public std::binary_function<int, const HiUnitInfo*, int>
{
      int operator()(int count, const HiUnitInfo* unit)
      {
            return count + countSP(unit);
      }
};

int HiZone::spCount() const
{
   return std::accumulate(d_units.begin(), d_units.end(), 0, AddCountSP());
}

/*
 * Add Unit to Zone...
 */

void HiZone::addUnit(HiUnitInfo* unit)
{
   unit->zone(this);
   d_units.insert(d_units.begin(), unit);
   unitsAdded(True);
}

/*
 * remove unit from zone
 * Also removes it from objective
 */

void HiZone::removeUnit(HiUnitInfo* unit)
{
      d_units.erase(unit);
      unit->zone(0);

      if(unit->objective())
      {
            unit->objective()->removeUnit(unit->cp());
            unit->objective(0);
      }
}


/*
 * Update an objective's area
 */

void HiZone::setObjectiveArea(RealHiObjective* obj, HexCord tLeftHex, HexCord tRightHex, int depth, bool downMap)
{
   HexCord bLeftHex(tLeftHex.x(), (downMap) ? tLeftHex.y() + depth : tLeftHex.y() - depth);
   HexCord bRightHex(tRightHex.x(), (downMap) ? tRightHex.y() + depth : tRightHex.y() - depth);

   obj->area(HexArea(tLeftHex, tRightHex, bLeftHex, bRightHex), False);
}

#ifdef _MSC_VER
      struct isCP
      {
          isCP(CRefBattleCP cp) : d_cp(cp) { }
          bool operator()(const HiUnitInfo* unit) { return unit->cp() == d_cp; }

          CRefBattleCP d_cp;
      };
#endif

HiUnitInfo* HiZone::getUnit(CRefBattleCP cp)
{
#ifndef _MSC_VER
      struct isCP
      {
          isCP(CRefBattleCP cp) : d_cp(cp) { }
          bool operator()(const HiUnitInfo* unit) { return unit->cp() == d_cp; }

          CRefBattleCP d_cp;
      };
#endif

      HiUnitPList::iterator it = std::find_if(d_units.begin(), d_units.end(), isCP(cp));

      if(it != d_units.end())
          return *it;
      else
          return 0;
}

void HiZone::addUnitToObjective(HiUnitInfo* unit, RealHiObjective* obj)
{
      ASSERT(!unit->objective());
      obj->addUnit(unit->cp());
      unit->objective(obj);
}

void HiZone::removeUnitFromObjective(HiUnitInfo* unit, RealHiObjective* obj)
{
      ASSERT(unit->objective() == obj);
      obj->removeUnit(unit->cp());
      unit->objective(0);
}

void HiZone::moveUnitToObjective(HiUnitInfo* unit, RealHiObjective* from, RealHiObjective* to)
{
      ASSERT(unit->objective() == from);

      removeUnitFromObjective(unit, from);
      addUnitToObjective(unit, to);
}

void HiZone::procMessage(const BattleMessageInfo& msg)
{
   BattleMessageID::BMSG_ID id = msg.id().id();
   if(id == BattleMessageID::BMSG_EnemyTookVP)
   {
#ifdef DEBUG
      gameInfo()->log("Zone ->%s -- Launching counter attack to retake VP hex(%d,%d)",
         name(), static_cast<int>(msg.hex().x()), static_cast<int>(msg.hex().y()));
#endif
      launchCounterAttack(msg.hex());
   }
}

bool HiZone::engaged(RealHiObjective* obj)
{
   CRefBattleCP cp = (*obj->units().begin());
   if(cp->hasQuitBattle())
      return True;

   for(ConstBattleUnitIter iter(cp); !iter.isFinished(); iter.next())
   {
      if(iter.cp()->getRank().sameRank(Rank_Division))
      {
         if(iter.cp()->targetList().closeRangeFront() <= (3 * BattleMeasure::XYardsPerHex))
            return True;
      }
   }

   return False;
}

RealHiObjective* HiZone::findAttackObj(HexCord hex)
{
   // Find an objective to launch counter attack with
   int bestPoints = 0;
   RealHiObjective* bestObj = 0;
   int bestDist = UWORD_MAX;
   for(int i = 0; i < d_objectives.size(); i++)
   {
      RealHiObjective* obj = d_objectives[i];
      if(obj->launchingCounter() ||
         obj->retreating() ||
         obj->movingToFront())
      {
         continue;
      }

      CRefBattleCP cp = (*obj->units().begin());
      if(cp->hasQuitBattle())
         return 0;

      if(cp->getRank().sameRank(Rank_Division) && cp->generic()->isArtillery())
         continue;

      HexCord ourLeft = obj->leftFront();
      const HexArea& area = obj->area();
      HexPosition::Facing objFace = WG_BattleAI_Utils::frontFacing(area[0], area[1]);
      WG_BattleAI_Utils::MoveOrientation mo = WG_BattleAI_Utils::moveOrientation(objFace, ourLeft, hex, WG_BattleAI_Utils::NormalAngle);
      int dist = WG_BattleAI_Utils::getDist(ourLeft, hex);
      bool objEngaged = engaged(obj);
      int points = 0;

      if(mo != WG_BattleAI_Utils::MO_Front &&
         (obj->mode() == HiObjective::FrontLine || objEngaged) )
      {
         continue;
      }

      if(mo == WG_BattleAI_Utils::MO_Front)
      {
         points += (dist <= 4) ? 20 : 10;
      }

      if(!objEngaged)//engaged(obj))
      {
         points += (obj->mode() == HiObjective::Reserves) ? 10 : 5;
      }

      if(WG_BattleAI_Utils::hasGuard(cp))
      {
         points += (cp->getRank().sameRank(Rank_Division)) ? 10 : 3;
      }

      if(dist < bestDist)
         points += 5;

      if(points > bestPoints)
      {
         bestPoints = points;
         bestDist = dist;
         bestObj = obj;
      }
   }

#ifdef DEBUG
   if(bestObj)
   {
      CRefBattleCP cp = (*bestObj->units().begin());
      gameInfo()->log("%s will launch attack to take back Hex(%d,%d)",
         cp->getName(),
         static_cast<int>(hex.x()),
         static_cast<int>(hex.y()));
   }
#endif
   return bestObj;
}

bool HiZone::launchCounterAttack(HexCord hex)
{
   RealHiObjective* obj1 = findAttackObj(hex);
   RealHiObjective* obj2 = 0;
   if(obj1)
   {
      obj1->launchingCounter(True);
      CRefBattleCP cp1 = (*obj1->units().begin());
      if(cp1->getRank().sameRank(Rank_Division))
      {
         obj2 = findAttackObj(hex);
      }
      obj1->launchingCounter(False);

      // launch attack!
      HexPosition::Facing avgFace = WG_BattleAI_Utils::averageFacing(cp1);
      HexCord leftHex;
      HexCord rightHex;
      int objFWidth1 = obj1->frontWidth();
      int objFWidth2 = (obj2 != 0) ? obj2->frontWidth() : 0;
      int fWidth = objFWidth1 + objFWidth2;
      if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), avgFace, -(fWidth / 2), 0, hex, leftHex))
      {
         if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), avgFace, objFWidth1, 0, hex, rightHex))
         {
#ifdef DEBUG
            gameInfo()->log("Launching counter attack with %s", cp1->getName());
#endif
            obj1->area(HexArea(leftHex, rightHex, HexCord(0, 0), HexCord(0, 0)));
            obj1->getNewObjective(True);
            obj1->aggression(BattleOrderInfo::EngageAtAllCost);
            obj1->posture(BattleOrderInfo::Offensive);
            obj1->process();
            obj1->movingToFront(True);
         }
         else
            return False;

         if(obj2)
         {
            leftHex = rightHex;
            CRefBattleCP cp2 = (*obj2->units().begin());
            HexPosition::Facing avgFace2 = WG_BattleAI_Utils::averageFacing(cp2);
            if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), avgFace2, objFWidth2, 0, leftHex, rightHex))
            {
#ifdef DEBUG
               gameInfo()->log("Launching counter attack with %s", cp2->getName());
#endif
               obj2->area(HexArea(leftHex, rightHex, HexCord(0, 0), HexCord(0, 0)));
               obj2->getNewObjective(True);
               obj2->aggression(BattleOrderInfo::EngageAtAllCost);
               obj2->posture(BattleOrderInfo::Offensive);
               obj2->process();
               obj2->movingToFront(True);
            }
            else
               return False;
         }
      }
      else
         return False;
   }

   return (obj1 != 0);

}

// Launch counter attack to support this objective
bool HiZone::launchCounterAttack(RealHiObjective* obj)
{
   // A random chance of not doing it
   CRefBattleCP objCP = (*obj->units().begin());
#ifdef DEBUG
   gameInfo()->log("Wants to launch counter attack to support %s", objCP->getName());
#endif
   CRefBattleCP parentCP = (objCP->parent() == NoBattleCP) ?
      gameInfo()->cinc() : objCP->parent();

   if(parentCP != NoBattleCP)
   {
      int v = 100 - (((parentCP->leader()->getTactical() + parentCP->leader()->getAggression() + parentCP->leader()->getInitiative()) * 100) / 768);
      int chance = v / 10;
      if(gameInfo()->random(100) < chance)
      {
#ifdef DEBUG
         gameInfo()->log("Leader failed test. Chance of failing=%d", chance);
#endif
         return False;
      }
   }

   HexCord lHex = obj->leftFront();
   if(lHex == HexCord(0, 0))
      return False;

   HexCord rHex = obj->rightFront();
   if(rHex == HexCord(0, 0))
      return False;

   HexPosition::Facing facing = WG_BattleAI_Utils::frontFacing(lHex, rHex);

   if(obj->flankedLeft())
   {
      HexCord destHex;
      HexCord::HexDirection hd = BattleMeasure::leftFlank(facing);
      if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), hd, gameInfo()->random(4, 6), lHex, destHex))
      {
#ifdef DEBUG
         gameInfo()->log("Wants to launch counterattack to left flank");
#endif
         return launchCounterAttack(destHex);
      }
   }

   else if(obj->flankedRight())
   {
      HexCord destHex;
      HexCord::HexDirection hd = BattleMeasure::rightFlank(facing);
      if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), hd, gameInfo()->random(4, 6), rHex, destHex))
      {
#ifdef DEBUG
         gameInfo()->log("Wants to launch counterattack to right flank");
#endif
         return launchCounterAttack(destHex);
      }
   }

   else if(obj->flankedRear())
   {
      HexCord hex((lHex.x() + rHex.x()) / 2, (lHex.y() + rHex.y()) / 2);
#ifdef DEBUG
      gameInfo()->log("Wants to launch counterattack to rear flank");
#endif
      return launchCounterAttack(hex);
   }

   return False;
}

void HiZone::initObjectives()
{
   unitsAdded(False);

      /*
       * Calc frontline and reserve areas
       */

      const BattleMap* map = gameInfo()->map();
//    HexCord::Cord mapHeight = map->getSize().y();
      HexCord bLeft;
      HexCord mSize;
      gameInfo()->battleData()->getPlayingArea(&bLeft, &mSize);
      HexCord tRight(bLeft.x() + mSize.x(), bLeft.y() + mSize.y());
      int mapHeight = mSize.y();

      HexCord::Cord ourEdge = gameInfo()->whichEdge();
      HexCord::Cord otherEdge = gameInfo()->otherEdge();

      {
         // TODO: will need to do a more extensive calculation
         //       splitting up objectives if need be to go around restrictive terrain

         // first, count needed objectives
         int nFront = (d_units.size() + 1) / 2;
         int nRear = d_units.size() - nFront;
         const HexPosition::Facing facing = WG_BattleAI_Utils::frontFacing(leftHex(), rightHex());

         d_objectives.reserve(nFront + nRear);

         int loop = nFront;
         while(loop--)
            d_objectives.create(gameInfo(), HiObjective::FrontLine);

         loop = nRear;
         while(loop--)
            d_objectives.create(gameInfo(), HiObjective::Reserves);

         int frontDepth = (mapHeight * 36) / 100;
         int reserveDepth = (mapHeight * 18) / 100;

         int frontY = leftY();//(ourEdge > otherEdge) ? mapHeight - frontDepth : frontDepth;//((ourEdge + (otherEdge - ourEdge)) * 4) / 10; //ourEdge + (otherEdge - ourEdge) / 3;
         int reserveY = (ourEdge > otherEdge) ? frontY + reserveDepth : frontY - reserveDepth;//mapHeight - reserveDepth : reserveDepth;//((ourEdge + (otherEdge - ourEdge)) * 2) / 10;//ourEdge + (otherEdge - ourEdge) / 6;

         HexCord::Cord frontHeight = 12;   // arbitary size of frontline area
         HexCord::Cord reserveHeight = frontHeight;   // should be calculated from SP count and zone width

         // do front-row objectives
         if(nFront > 0)
         {
            SListIter<Boundry> iter(&d_boundries);
            while(++iter)
            {
               int leftX = iter.current()->leftHex().x();
               int rightX = iter.current()->rightHex().x();
               int leftY = iter.current()->leftHex().y();

               int mult = gameInfo()->random(70, 80);
               int width = (rightX - leftX) / nFront;
               int objWidth =  (width * mult) / 100;
               int lX = (zoneOrientation() == ZO_Centered) ? leftX + ((width - objWidth) / 2) :
                         (zoneOrientation() == ZO_Left) ? leftX : leftX + (width - objWidth);

               for(int i = 0; i < d_objectives.size(); i++)
               {
                  RealHiObjective* obj = d_objectives[i];
                  if(obj->mode() == HiObjective::FrontLine)
                  {
                     int rX = clipValue<int>(lX + objWidth, bLeft.x(), tRight.x() - 1);//minimum(map->getSize().x() - 1, lX + objWidth);
                     setObjectiveArea(obj, HexCord(lX, leftY), HexCord(rX, leftY), frontHeight, ourEdge > otherEdge);
                     lX += width; //rX;
                     lX = (zoneOrientation() == ZO_Centered) ? lX + ((width - objWidth) / 2) :
                         (zoneOrientation() == ZO_Left) ? lX : lX + (width - objWidth);
                  }
               }
            }
#ifdef DEBUG
            for(int i = 0; i < d_objectives.size(); i++)
            {
               RealHiObjective* obj = d_objectives[i];
               if(obj->mode() == HiObjective::FrontLine)
               {
                  gameInfo()->log("Objectives for front-line objective:");
                  HexAreaList& list = obj->areaList();
                  for(HexAreaList::iterator it = list.begin();
                      it != list.end();
                      ++it)
                  {
                     HexArea& area = (*it);
                     gameInfo()->log("------ Left=(%d, %d), Right=(%d, %d)",
                        static_cast<int>(area[0].x()), static_cast<int>(area[0].y()),
                        static_cast<int>(area[1].x()), static_cast<int>(area[1].y()));
                  }
               }
            }
#endif
         }

         // do rear objectives
         if(nRear > 0)
         {
            int mult = gameInfo()->random(70, 80);
            int width = (rightX() - leftX()) / nRear;
            int objWidth =  (width * mult) / 100;
            int lX = (zoneOrientation() == ZO_Centered) ? leftX() + ((width - objWidth) / 2) :
                         (zoneOrientation() == ZO_Left) ? leftX() : leftX() + (width - objWidth);

            for(int i = 0; i < d_objectives.size(); i++)
            {
               RealHiObjective* obj = d_objectives[i];
               if(obj->mode() == HiObjective::Reserves)
               {
                  int rX = clipValue<int>(lX + objWidth, bLeft.x(), tRight.x() - 1);//minimum(map->getSize().x() - 1, lX + objWidth);
                  setObjectiveArea(obj, HexCord(lX, reserveY), HexCord(rX, reserveY), reserveHeight, ourEdge > otherEdge);
                  lX += width; //rX;
                  lX = (zoneOrientation() == ZO_Centered) ? lX + ((width - objWidth) / 2) :
                         (zoneOrientation() == ZO_Left) ? lX : lX + (width - objWidth);
               }
            }
#ifdef DEBUG
            for(i = 0; i < d_objectives.size(); i++)
            {
               RealHiObjective* obj = d_objectives[i];
               if(obj->mode() == HiObjective::Reserves)
               {
                  gameInfo()->log("Objectives for reserve objective:");
                  HexAreaList& list = obj->areaList();
                  for(HexAreaList::iterator it = list.begin();
                      it != list.end();
                      ++it)
                  {
                     HexArea& area = (*it);
                     gameInfo()->log("------ Left=(%d, %d), Right=(%d, %d)",
                        static_cast<int>(area[0].x()), static_cast<int>(area[0].y()),
                        static_cast<int>(area[1].x()), static_cast<int>(area[1].y()));
                  }
               }
            }
#endif
         }

         // assign units. higher priority units go to front-line objectives
         WG_BattleAI_Utils::LocalCPList cpList;
         while(cpList.entries() != d_units.size())
         {
            int bestPriority = -1;
            CRefBattleCP bestCP = NoBattleCP;
            for(HiUnitPList::iterator unitIt = d_units.begin(); unitIt != d_units.end(); ++unitIt)
            {
               HiUnitInfo* unitInfo = *unitIt;
               if(!cpList.find(unitInfo->cp()) && unitInfo->priority() > bestPriority)
               {
                  bestCP = unitInfo->cp();
                  bestPriority = unitInfo->priority();
               }
            }

            ASSERT(bestCP != NoBattleCP);
            cpList.add(bestCP);
         }

#ifdef DEBUG
         SListIter<WG_BattleAI_Utils::LocalCP> iter(&cpList);
         gameInfo()->log("Order of priority -------------");
         while(++iter)
            gameInfo()->log("%s", iter.current()->d_cp->getName());
#endif
         ASSERT(d_units.size() == d_objectives.size());
         for(int i = 0; i < d_objectives.size(); i++)
         {
            HiUnitInfo* unit = getUnit(cpList.first()->d_cp);
            ASSERT(unit);
            RealHiObjective* obj = d_objectives[i];
            obj->aggression(d_agPos.d_aggression);
            obj->posture(d_agPos.d_posture);
            addUnitToObjective(unit, obj);
//          assignOrder(unit);
            cpList.remove(cpList.first());
         }
      }
}

bool HiZone::shouldUpdateObjective(RealHiObjective* obj, bool achieved, bool& removeOld)
{
   // Special cases:
   // If all units have achieved there objective
   if( (achieved && obj->mode() == HiObjective::FrontLine) )
   {
      removeOld = True;
      return True;
   }

   // If this is a reinforcement
   if(obj->reinforcement())
   {
      removeOld = True;
      return True;
   }

   // A reserve unit moving to the front
   if(obj->mode() == HiObjective::Reserves &&
      obj->movingToFront() &&
      obj->objectiveAchieved())
   {
      removeOld = True;
      return True;
   }

   if(obj->mode() == HiObjective::Reserves)
      return False;

   // Otherwise, test to see if we update
   bool shouldUpdate = False;
   if( (obj->objectiveAchieved() && obj->nBoundries() > 1) ||
       (obj->retreating()) )
   {
      int nObjsAchieved = 0;
      int nObjs = 0;
      for(int i = 0; i < d_objectives.size(); i++)
      {
         if(obj != d_objectives[i] && d_objectives[i]->mode() == HiObjective::FrontLine)
         {
            nObjs++;
            if(d_objectives[i]->objectiveAchieved() ||
               d_objectives[i]->nBoundries() < obj->nBoundries())
            {
               nObjsAchieved++;
            }
         }
      }

      // Top unit in obj
      CRefBattleCP cp = (*obj->units().begin());
#ifdef DEBUG
      gameInfo()->log("Processing updating objective test for %s", cp->getName());
      gameInfo()->log("objectives=%d, achieved=%d", nObjs, nObjsAchieved);
#endif
      CRefBattleCP parentCP = (cp->parent() != NoBattleCP) ? cp->parent() : gameInfo()->cinc();
      RefGLeader l = (parentCP != NoBattleCP) ? parentCP->leader() : 0;

      int chance = (obj->objectiveAchieved() && obj->nBoundries() > 1) ? 5 : 2;
      if(nObjs > 0)
      {
         int percentAchieved = MulDiv(nObjsAchieved, 100, nObjs);
         if(percentAchieved >= 75)
            chance += 15;
         else if(percentAchieved >= 50)
            chance += 10;
         else if(percentAchieved >= 25)
            chance += 5;
         else if(percentAchieved > 0)
            chance += 2;
      }

#ifdef DEBUG
      gameInfo()->log("chance after other objectives modifier = %d", chance);
#endif
      if(obj->posture() == BattleOrderInfo::Offensive)
      {
         chance += 4; // raw chance of updating is 0
#ifdef DEBUG
         gameInfo()->log("chance after offensive posture modifier = %d", chance);
#endif
      }

      if(l != 0)
      {
         if(l->getAggression() >= 200)
            chance += 4;
         else if(l->getAggression() >= 150)
            chance += 2;
      }

#ifdef DEBUG
      gameInfo()->log("chance after leader aggression modifier = %d", chance);
#endif

      if(obj->enemyRetreating())
      {
         chance += (obj->inflictingHeavyLosses()) ? 30 : 20;
#ifdef DEBUG
         gameInfo()->log("chance after enemy retreating modifier = %d", chance);
#endif
      }

      if(obj->retreating())
      {
         chance += 10;
#ifdef DEBUG
         gameInfo()->log("chance after retreating modifier = %d", chance);
#endif
      }

      #ifdef DEBUG
      gameInfo()->log("Final chance (out of 200) = %d", chance);
#endif
      if(gameInfo()->random(200) < chance)
      {
         shouldUpdate = True;
         removeOld = obj->objectiveAchieved();

#ifdef DEBUG
         gameInfo()->log("Passed Updating objective test");
#endif
      }
   }

   return shouldUpdate;
}

// End

// Should we add a new objective
void HiZone::setZoneFlags()
{
   int nEnemyAtCriticalLoss = 0;
   int nEnemyAtNoEffectCutoff = 0;
   int nEnemyRetreating = 0;
   int nEnemyLowMorale = 0;
   int nEnemyWavering = 0;
   int nEnemy = 0;
   int nAtCriticalLoss = 0;
   int nRetreating = 0;
   int nLowMorale = 0;
   int nWavering = 0;
   int nUs = 0;
   bool noneWithin8 = True;
   bool noneWithin3 = True;
   for(int i = 0; i < d_objectives.size(); i++)
   {
      // check only front-line objs
      if(d_objectives[i]->mode() == HiObjective::FrontLine)
      {
         // Go through each target unit and evaluate
         nEnemy += d_objectives[i]->planner()->targets().entries();
         SListIterR<BattleItem> iter(&d_objectives[i]->planner()->targets());
         while(++iter)
         {
            if(noneWithin8 &&
               iter.current()->d_position == BattleItem::Front &&
               iter.current()->d_range <= (8 * BattleMeasure::XYardsPerHex))
            {
               noneWithin8 = False;
            }

            if(noneWithin3 &&
               iter.current()->d_position == BattleItem::Front &&
               iter.current()->d_range <= (3 * BattleMeasure::XYardsPerHex))
            {
               noneWithin3 = False;
            }

            if(iter.current()->d_cp->criticalLossReached())
               nEnemyAtCriticalLoss++;
            else if(iter.current()->d_cp->noEffectCutoffReached())
               nEnemyAtNoEffectCutoff++;
            if(iter.current()->d_cp->morale() <= 80)
               nEnemyLowMorale++;
            if(iter.current()->d_cp->movingBackwards() ||
               iter.current()->d_cp->routing() ||
               iter.current()->d_cp->fleeingTheField())
            {
               nEnemyRetreating++;
            }
            if(iter.current()->d_cp->wavering() ||
               iter.current()->d_cp->shaken())
            {
               nEnemyWavering++;
            }
         }
      }
   }

   noEnemyNear(noneWithin8);
   inCombat(!noneWithin3);

   if(nEnemy > 0)
   {
   // Set Inflicting losses
   // Set if 60% or more of enemy front-line XX have taken critical loss
      inflictedCriticalLoss((nEnemyAtCriticalLoss * 100) / nEnemy >= 50);
      inflictedLoss((nEnemyAtNoEffectCutoff * 100) / nEnemy >= 50);
      // if enemy wavering, retreating, low morale
      enemyWavering((nEnemyWavering * 100) / nEnemy >= 50);
      enemyRetreating((nEnemyRetreating * 100) / nEnemy >= 40);
      enemyLowMorale((nEnemyLowMorale * 100) / nEnemy >= 50);
   }
}


// Has 2/3 of front-line units reached last obj
bool HiZone::lastObjReached() const
{
   int nFront = 0;
   int nFrontLastObj = 0;

   for(int i = 0; i < d_objectives.size(); i++)
   {
      const RealHiObjective* obj = d_objectives[i];
      if(obj->mode() == HiObjective::FrontLine)
      {
         nFront++;
         if(obj->nBoundries() == 1)
            nFrontLastObj++;
      }
   }

   if(nFront > 0)
   {
      return (((nFrontLastObj * 100) / nFront) >= 66);
   }

   return False;
}

bool HiZone::procConsiderUpdate()
{
   if(whichWay() == WhichWay_Undefined)//moveFB() == 0 && moveLR() == 0)
      return False;

   if(noEnemyNear() || lastObjReached())
      updateZone();
   else
   {
#ifdef DEBUG
      gameInfo()->log("Testing to update posture / aggression");
#endif
      for(int i = 0; i < d_objectives.size(); i++)
      {
         RealHiObjective* obj = d_objectives[i];
         if(obj->mode() == HiObjective::FrontLine)
         {
            int chanceToChangePosture = (obj->posture() == BattleOrderInfo::Defensive && obj->aggression() == 3) ? 20 : 0;
            int chanceToChangeAgg = (obj->aggression() < 3) ? 50 : 0;
            if(gameInfo()->random(100) < chanceToChangePosture)
            {
#ifdef DEBUG
               gameInfo()->log("------changing posture to offensive");
#endif
               obj->posture(BattleOrderInfo::Offensive);
               obj->getNewObjective(True);
            }

            if(gameInfo()->random(100) < chanceToChangeAgg)
            {
#ifdef DEBUG
               gameInfo()->log("------increasing aggression 1");
#endif
               obj->aggression(static_cast<BattleOrderInfo::Aggression>(minimum(3, obj->aggression() + 1)));
               obj->getNewObjective(True);
            }
         }
      }

      considerUpdate(False);
   }

   whichWay(WhichWay_Undefined);
#if 0
   moveFB(0);
   moveLR(0);
#endif
   return True;
}

bool HiZone::shouldCreateNewBoundries(RealHiObjective* obj)
{
   if(obj->nBoundries() <= 1)
      return True;

   int chance = 100 - minimum(99, (obj->nBoundries() * (10 + (3 * obj->nBoundries()))));
   if(whichWay() == Left || whichWay() == Right)
      chance += 25;

   return (gameInfo()->random(100) <= chance);
}

bool HiZone::shouldReorient(
   RealHiObjective* obj,
   HexPosition::Facing& f,
   HexPosition::Facing& newFace)
{
   if(shouldCreateNewBoundries(obj))
   {
      HexCord sLeft;
      HexCord sRight;
      const_cast<BattleData*>(gameInfo()->battleData())->getSideLeftRight(gameInfo()->side(), sLeft, sRight);
      HexPosition::Facing sideF = WG_BattleAI_Utils::frontFacing(sLeft, sRight);
      if(whichWay() == Left)
      {
         newFace = BattleMeasure::leftTurn(sideF);
      }
      else if(whichWay() == Right)
      {
         newFace = BattleMeasure::rightTurn(sideF);
      }
   }

   return (newFace != f);
}

bool HiZone::updateZone()
{
   // advance each objective 8 hexes from current position
   // First build up a list of objectives in order from left to right
   // TODO: write routine for get objectives left to right
   RealHiObjective** objectives = new RealHiObjective*[d_objectives.size()];
   ASSERT(objectives);
   int nFront = 0;
   {
      for(int i = 0; i < d_objectives.size(); i++)
      {
         if(d_objectives[i]->mode() == HiObjective::FrontLine)
         {
            objectives[nFront++] = d_objectives[i];
         }
      }
   }

   if(nFront != 0)
   {
      int start = (whichWay() == Right) ? nFront - 1 : 0;
      int end = (whichWay() == Right) ? -1 : nFront;
      int inc = (whichWay() == Right) ? -1 : 1;
      for(int i = start; i != end; i += inc)
      {
         RealHiObjective* obj = objectives[i];//frontObjs[i];

#ifdef DEBUG
         CRefBattleCP cp = (*obj->units().begin());
            gameInfo()->log("Updating Zone for %s", cp->getName());
         static const char* s_whichWayText[WhichWay_HowMany] = {
            "Left",
            "Front",
            "Right",
            "Undefined"
         };
         gameInfo()->log("---------- orient %s", s_whichWayText[whichWay()]);
#endif
         HexCord leftHex = obj->leftFront();
         HexCord rightHex = obj->rightFront();
         int dist = WG_BattleAI_Utils::getDist(leftHex, rightHex) + 2;
#ifdef DEBUG
         gameInfo()->log("---------- startleft(%d,%d), startright(%d,%d)",
            static_cast<int>(leftHex.x()), static_cast<int>(leftHex.y()),
            static_cast<int>(rightHex.x()), static_cast<int>(rightHex.y()));
#endif
         HexPosition::Facing f = WG_BattleAI_Utils::frontFacing(leftHex, rightHex);
         HexPosition::Facing newFace = f;
         // see if we should do a facing change
         if(whichWay() != WhichWay_Undefined)//moveLR() != 0)
         {
            //int chance = gameInfo()->random(80, 90);
            if(whichWay() != Front &&
               shouldReorient(obj, f, newFace))//gameInfo()->random(100) < chance)
            {
#ifdef DEBUG
               gameInfo()->log("Will reorient");
#endif
            }
            else if(whichWay() == Front &&
                    shouldCreateNewBoundries(obj))
            {
#ifdef DEBUG
               gameInfo()->log("Will create new boundry to front");
#endif
            }
            else
            {
#ifdef DEBUG
               gameInfo()->log("Will use existing boundry to front");
#endif
               whichWay(WhichWay_Undefined);//moveLR(0);
               obj->aggression(BattleOrderInfo::EngageAtAllCost);
               obj->posture(BattleOrderInfo::Offensive);
               getNewObjective(obj, obj->objectiveAchieved());
               continue;
            }
         }

         int moveLR = (whichWay() == Left) ? -2 :
                   (whichWay() == Right) ? 2 : 0;
         int moveFB = 10;
         HexCord destLeft;
         HexCord destRight;
         if( (moveLR > 0 &&
              WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), f, moveLR, moveFB, rightHex, destRight) &&
              WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), newFace, -dist, 0, destRight, destLeft)) ||
             (WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), f, moveLR, moveFB, leftHex, destLeft) &&
              WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), newFace, dist, 0, destLeft, destRight)))
         {
            if(obj->mode() == HiObjective::FrontLine)
            {
               obj->aggression(BattleOrderInfo::EngageAtAllCost);
               obj->posture(BattleOrderInfo::Offensive);
            }

#ifdef DEBUG
            gameInfo()->log("---------- destleft(%d,%d), destright(%d,%d)",
               static_cast<int>(destLeft.x()), static_cast<int>(destLeft.y()),
               static_cast<int>(destRight.x()), static_cast<int>(destRight.y()));
#endif
            obj->area(HexArea(destLeft, destRight, HexCord(0, 0), HexCord(0, 0)));
            if(newFace != f)
               obj->reorienting(True);
            getNewObjective(obj, False);
         }

         return obj->getNewObjective();
      }

      delete[] objectives;
      return True;
   }

   return False;
}

/*
 * Set/Update Objectives for units in the zone
 */

void HiZone::updateObjectives()
{
#ifdef DEBUG
   gameInfo()->log("\n ------- UpdateObjectives for %s", name());
#endif

   // If deploying, deploy
   if(gameInfo()->isDeploying())
   {
      initObjectives();
      return;
   }

   else if(!canStart(gameInfo()->battleData()->getTick()))
      return;

   if(unitsAdded())
   {
      addNewUnits();
   }

   if(considerUpdate())
   {
      procConsiderUpdate();
   }

   // See if we need to bring up any reserves
   if(gameInfo()->battleData()->getTick() >= d_nextReserveCheck)
   {
      setZoneFlags();
      checkReserves();

      // check again between 5 - 10 minutes
      d_nextReserveCheck = gameInfo()->battleData()->getTick() + BattleMeasure::seconds(300);
   }

   // If zone objective has not been achieved return
   bool achieved = objectiveAchieved();

#if 0//def DEBUG
   {
      for(int i = 0; i < d_objectives.size(); i++)
      {
         CRefBattleCP cp = (*d_objectives[i]->units().begin());
         if(lstrcmpi("1st Corps", cp->getName()) == 0)
         {
            int i = 0;
         }
      }
   }

#endif
   // get new objectives
   if(achieved)
   {
      considerUpdate(False);
#ifdef DEBUG
      gameInfo()->log("Starting phase for %s --> %d remaining", name(), d_boundries.entries());
#endif
   }

   /*
    * Get new objectives
    */

   for(int i = 0; i < d_objectives.size(); i++)
   {
      bool removeOld = False;
      if(shouldUpdateObjective(d_objectives[i], achieved, removeOld))
         getNewObjective(d_objectives[i], removeOld);
   }
}

void HiZone::countObjTypes(int& nFront, int& nReserve)
{
   nFront = 0;
   nReserve = 0;
   for(int i = 0; i < d_objectives.size(); i++)
   {
      if(d_objectives[i]->mode() == HiObjective::FrontLine)
         nFront++;
      else if(d_objectives[i]->mode() == HiObjective::Reserves)
      {
         if(!d_objectives[i]->movingToFront())
         {
            nReserve++;
         }
      }
#ifdef DEBUG
      else
        FORCEASSERT("Zone has no mode");
#endif
   }
}

bool HiZone::objectiveAchieved() const
{
// if(gameInfo()->battleData()->getTick() < d_startWhen)
//    return False;

   for(int i = 0; i < d_objectives.size(); i++)
   {
      if(!d_objectives[i]->objectiveAchieved())
         return False;
   }
   return True;
}

void HiZone::getNewObjective(RealHiObjective* obj, bool removeOld)
{
#ifdef DEBUG
   CRefBattleCP cp = (*obj->units().begin());
   const HexArea& area = obj->area();
   gameInfo()->log("Setting new objective for %s (%d boundries remaining)",
      cp->getName(), obj->nBoundries());
   gameInfo()->log("------ Old objective -- Left=(%d, %d), Right=(%d, %d)",
                  static_cast<int>(area[0].x()), static_cast<int>(area[0].y()),
                  static_cast<int>(area[1].x()), static_cast<int>(area[1].y()));
#endif
   // if objective has list of boundries, get next boundry
   if(removeOld && obj->nBoundries() > 1)
   {
      obj->removeBoundry();
   }

   obj->getNewObjective(True);
   obj->reinforcement(False);

#ifdef DEBUG
   {
      const HexArea& area = obj->area();
      gameInfo()->log("------ New objective -- Left=(%d, %d), Right=(%d, %d)",
                        static_cast<int>(area[0].x()), static_cast<int>(area[0].y()),
                        static_cast<int>(area[1].x()), static_cast<int>(area[1].y()));
   }
#endif
}

void HiZone::bringUpReserve(RealHiObjective* rObj, RealHiObjective* fObj)
{
#ifdef DEBUG
   CRefBattleCP cp = (*rObj->units().begin());
   gameInfo()->log("Reserve unit %s will move to the front",   cp->getName());
#endif

   // We are bringing up this reserve objective to the front
   // We we are a good distance away, we will do this in 2 stages (if enemy is not near),
   // otherwise, in 1 stage
   int nStages = 0;
   int distToNextStage = 0;
   const HexArea& rObjArea = rObj->area();
   const HexArea& fObjArea = fObj->area();
   if(rObj->planner()->targets().entries() > 0)
      nStages = 1;
   else
   {
      int dist = minimum(
         minimum(WG_BattleAI_Utils::getDist(rObjArea[0], fObjArea[0]), WG_BattleAI_Utils::getDist(rObjArea[1], fObjArea[1])),
         minimum(WG_BattleAI_Utils::getDist(rObjArea[1], fObjArea[0]), WG_BattleAI_Utils::getDist(rObjArea[0], fObjArea[1])));

      if(dist > 12)
      {
         nStages = 2;
         distToNextStage = dist - 8;
      }
      else
         nStages = 1;
   }

   // determine objective boundries
   // for now final boundry will be same as front objective (more or less)
   // though it should be improved to take gaps and whatnot into account
   enum { Center, Left, Right } moveTo;
   if(fObj->flankedLeft())
      moveTo = Left;
   else if(fObj->flankedRight())
      moveTo = Right;
   else
      moveTo = Center;

   HexCord nextLeft;
   HexCord nextRight;
   int rFrontage = WG_BattleAI_Utils::getDist(rObjArea[0], rObjArea[1]);
   if(nStages == 2)
   {
#ifdef DEBUG
      gameInfo()->log("%s will move in 2 stages to the front", cp->getName());
#endif
      HexPosition::Facing rFacing = WG_BattleAI_Utils::frontFacing(rObjArea[0], rObjArea[1]);
      if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), rFacing, 0, distToNextStage, rObjArea[0], nextLeft))
      {
         FORCEASSERT("Hex not found in bringUpReserve()");
         return;
      }
      else
      {
         HexCord::HexDirection hd = BattleMeasure::rightFlank(rFacing);
         if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), hd, rFrontage, nextLeft, nextRight))
         {
            FORCEASSERT("Hex not found in bringUpReserve()");
            return;
         }
      }

      // add boundry
      rObj->area(HexArea(nextLeft, nextRight, HexCord(0, 0), HexCord(0, 0)), False);
#ifdef DEBUG
      gameInfo()->log("------Boundry to 1st stage --- left=(%d, %d), right=(%d, %d)",
           static_cast<int>(nextLeft.x()), static_cast<int>(nextLeft.y()),
           static_cast<int>(nextRight.x()), static_cast<int>(nextRight.y()));
#endif
   }

   // get final stage to front
   // add all Front objectives boundries to reserve obj
   HexAreaList& fList = fObj->areaList();
   for(HexAreaList::iterator it = fList.begin();
       it != fList.end();
       ++it)
   {
      int fFrontage = WG_BattleAI_Utils::getDist((*it)[0], (*it)[1]);
      HexPosition::Facing fFacing = WG_BattleAI_Utils::frontFacing((*it)[0], (*it)[1]);

      // Move through front obj
      int goLateral = (moveTo == Left) ? -rFrontage :
                      (moveTo == Right)? rFrontage : 0;

      // Go left or right
      if(goLateral)
      {
         if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), fFacing, goLateral, 0, (*it)[0], nextLeft))
         {
            // if we can't, try center
            moveTo = Center;
         }
         else
         {
            HexCord::HexDirection hd = BattleMeasure::rightFlank(fFacing);
            if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), hd, rFrontage, nextLeft, nextRight))
            {
            // if we can't, try center
               moveTo = Center;
            }
         }
      }

#ifdef DEBUG
      gameInfo()->log("%s is moving to the %s of front obj",
         cp->getName(),
         (moveTo == Left) ? "left" :
         (moveTo == Right) ? "right" : "center");
#endif
      // move to center of obj
      if(moveTo == Center)
      {
         goLateral = (fFrontage - rFrontage) / 2;
         if(goLateral)
         {
            if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), fFacing, goLateral, 0, (*it)[0], nextLeft))
            {
               FORCEASSERT("Hex not found in bringUpReserve()");
               return;
            }
            else
            {
               HexCord::HexDirection hd = BattleMeasure::rightFlank(fFacing);
               if(!WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), hd, rFrontage, nextLeft, nextRight))
               {
                  FORCEASSERT("Hex not found in bringUpReserve()");
                  return;
               }
            }
         }
         else
         {
            nextLeft = (*it)[0];
            nextRight = (*it)[1];
         }
      }

      // add final boundry
      rObj->area(HexArea(nextLeft, nextRight, HexCord(0, 0), HexCord(0, 0)), False);
#ifdef DEBUG
      gameInfo()->log("------Boundry to next stage --- left=(%d, %d), right=(%d, %d)",
           static_cast<int>(nextLeft.x()), static_cast<int>(nextLeft.y()),
           static_cast<int>(nextRight.x()), static_cast<int>(nextRight.y()));
#endif
   }

   rObj->movingToFront(True);
   rObj->objectiveAchieved(True);

   // in case we haven't started yet
   d_startWhen = gameInfo()->battleData()->getTick();
}

void HiZone::checkReserves()
{
   // first, make sure we have reserve objectives
   int nFront = 0;
   int nReserve = 0;
   countObjTypes(nFront, nReserve);
   if(nReserve == 0)
      return;

#ifdef DEBUG
   gameInfo()->log("Checking %s for bringing up reserves", name());
#endif

   // should we bring up reserve units?
   // We should consider bringing them up if
   // 1. Our front-line units are getting beat up pretty good
   // 2. Our front-line units have beat-up the enemy pretty good and we
   //    want to really put the pressure on (reinforcing success!)

   // determine the status of front-line units
   RealHiObjective* bestObj = 0;
   RealHiObjective* worstObj = 0;
   int bestPositive = 0;
   int bestNegative = 0;
   for(int i = 0; i < d_objectives.size(); i++)
   {
      // first, if we are being flanked, try to launch counter-attack
      if(d_objectives[i]->flankedLeft() ||
         d_objectives[i]->flankedRight() ||
         d_objectives[i]->flankedRear())
      {
         if(launchCounterAttack(d_objectives[i]))
            continue;
      }

      int positivePoints = 0;  // kicking butt points
      int negativePoints = 0;  // getting butt kicked points
      if(d_objectives[i]->mode() == HiObjective::FrontLine)
      {
         if(d_objectives[i]->inflictingHeavyLosses())
            positivePoints +=  10;
         if(d_objectives[i]->enemyRetreating())
            positivePoints += 15;
         if(d_objectives[i]->takingHeavyLosses())
            negativePoints += 10;
         if(d_objectives[i]->retreating())
            negativePoints += 15;
         if(d_objectives[i]->routingFleeing())
            negativePoints += 20;
#if 1
         if(d_objectives[i]->flankedLeft() ||
            d_objectives[i]->flankedRight() ||
            d_objectives[i]->flankedRear())
         {
            negativePoints += 10;
         }
#endif
         if(positivePoints > bestPositive)
         {
            bestPositive = positivePoints;
            bestObj = d_objectives[i];
         }
         if(negativePoints > bestNegative)
         {
            bestNegative = negativePoints;
            worstObj = d_objectives[i];
         }
#ifdef DEBUG
         CRefBattleCP cp = (*d_objectives[i]->units().begin());
         gameInfo()->log("%s has %d positive points, %d negative points",
            cp->getName(), positivePoints, negativePoints);
#endif
      }
   }

   // determine which front-line objective we may support
   RealHiObjective* obj = 0;
   int points = 0;
   if(bestObj || worstObj)
   {
      // if we have negative and positive objectives, pick one
      if(bestObj && worstObj)
      {
         int pickValue = 50;
         pickValue += bestPositive - bestNegative;
         obj = (gameInfo()->random(100) >= pickValue) ? worstObj : bestObj;
      }
      else
         obj = (bestObj != 0) ? bestObj : worstObj;

      points = (obj == bestObj) ? bestPositive : bestNegative;

      // now determine if we will actually support obj
      if(gameInfo()->random(100) > points)
         obj = 0;
   }

   // if we have selected an objective to support
   if(obj)
   {
#ifdef DEBUG
      CRefBattleCP cp = (*obj->units().begin());
      gameInfo()->log("%s will be supported",   cp->getName());
#endif

      // now we need to select a reserve objective to move up
      RealHiObjective* reserveObj = 0;

      // for now we will select the closest reserve
      int bestDist = UWORD_MAX;
      const HexArea& fObjArea = obj->area();
      for(int i = 0; i < d_objectives.size(); i++)
      {
         if(d_objectives[i]->mode() == HiObjective::Reserves &&
            !d_objectives[i]->movingToFront())
         {
            const HexArea& rObjArea = d_objectives[i]->area();
            int dist = minimum(
                  minimum(WG_BattleAI_Utils::getDist(rObjArea[0], fObjArea[0]), WG_BattleAI_Utils::getDist(rObjArea[1], fObjArea[1])),
                  minimum(WG_BattleAI_Utils::getDist(rObjArea[1], fObjArea[0]), WG_BattleAI_Utils::getDist(rObjArea[0], fObjArea[1])));

            if(dist < bestDist)
            {
               bestDist = dist;
               reserveObj = d_objectives[i];
            }
         }
      }

      if(reserveObj)
      {
         bringUpReserve(reserveObj, obj);
      }

      // clear front-line objectives flags
      obj->inflictingHeavyLosses(False);
      obj->routingFleeing(False);
      obj->takingHeavyLosses(False);
      obj->retreating(False);
      obj->enemyRetreating(False);
      obj->flankedLeft(False);
      obj->flankedRight(False);
      obj->flankedRear(False);
   }
}

void HiZone::addNewUnit(HiUnitInfo* unit)
{
#ifdef DEBUG
   gameInfo()->log("Assigning reinforcement unit(%s) to objective", unit->cp()->getName());
#endif

   // TODO: determine if front-line or reserve
   HiObjective::Mode mode = HiObjective::Reserves;

   //HexPosition::Facing corpAvgFace = WG_BattleAI_Utils::averageFacing(unit->cp());
   HexPosition::Facing zoneFace = WG_BattleAI_Utils::frontFacing(leftHex(), rightHex());
#ifdef DEBUG
   gameInfo()->log("Zone facing=%d", static_cast<int>(zoneFace));
#endif
   HexCord corpLeft;
   HexCord corpRight;
   WG_BattleAI_Utils::getCorpLeftRightBoundry(unit->cp(), zoneFace, corpLeft, corpRight);

   // are we to the rear of zone for front?
   RealHiObjective* frontObj = 0;
   for(int i = 0; i < d_objectives.size(); i++)
   {
      if(d_objectives[i]->mode() == HiObjective::FrontLine)
      {
         frontObj = d_objectives[i];
         break;
      }
   }

   if(frontObj)
   {
      const HexArea& area = frontObj->area();
      HexPosition::Facing fFace = WG_BattleAI_Utils::frontFacing(area[0], area[1]);
      WG_BattleAI_Utils::MoveOrientation moL = WG_BattleAI_Utils::moveOrientation(fFace, corpLeft, area[0], WG_BattleAI_Utils::WideAngle);
      if(moL != WG_BattleAI_Utils::MO_Front)
         mode = HiObjective::FrontLine;
   }

   // create new objective
   RealHiObjective* obj = d_objectives.create(gameInfo(), mode);
   obj->area(HexArea(corpLeft, corpRight, HexCord(0, 0), HexCord(0, 0)), False);
   // move forward a bit
   HexCord newLeft;
   HexCord newRight;
   if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), zoneFace, -2, 12, corpLeft, newLeft) &&
      WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), zoneFace, 2, 12, corpRight, newRight))
   {
      obj->area(HexArea(newLeft, newRight, HexCord(0, 0), HexCord(0, 0)), False);
   }

   if(mode == HiObjective::FrontLine)
   {
      int nObjs = 3;
      int moveForward = 10;
      while(nObjs--)
      {
         HexCord left;
         HexCord right;
         if(WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), zoneFace, 0, moveForward, newLeft, left) &&
            WG_BattleAI_Utils::moveHex(gameInfo()->battleData(), zoneFace, 0, moveForward, newRight, right))
         {
            obj->area(HexArea(left, right, HexCord(0, 0), HexCord(0, 0)), False);
            newLeft = left;
            newRight = right;
         }
         else
            break;
      }
   }


#ifdef DEBUG
   //const HexArea& area = obj->area();
   const HexAreaList& areaList = obj->areaList();
   for(HexAreaList::const_iterator hi = areaList.begin();
       hi != areaList.end();
       ++hi)
   {
      const HexArea& area = (*hi);
      gameInfo()->log("New Objective Boundry --- left=(%d, %d), right=(%d, %d)",
         static_cast<int>(area[0].x()), static_cast<int>(area[0].y()),
         static_cast<int>(area[1].x()), static_cast<int>(area[1].y()));
   }
#endif

   // add unit to objective
   addUnitToObjective(unit, obj);

   // process objective to get it started
   obj->reinforcement(True);
   obj->process();
}

void HiZone::addNewUnits()
{
#ifdef DEBUG
   gameInfo()->log("Adding new units to zone %s", name());
#endif
   unitsAdded(False);

   for(HiUnitPList::iterator unitIt = d_units.begin(); unitIt != d_units.end(); ++unitIt)
   {
      HiUnitInfo* unitInfo = *unitIt;
      if(!unitInfo->objective())
         addNewUnit(unitInfo);
   }
}

void HiZone::assignOrder(HiUnitInfo* unit)
{
   /*
    * Allocate any units without an objective into the reserve list
    */

   BattleOrder order;
   unit->cp()->lastOrder(&order);
#ifdef DEBUG
   gameInfo()->log("%s - pos = %s, agg = %s",
      unit->cp()->getName(),
      BattleOrderInfo::postureName(d_agPos.d_posture),
      BattleOrderInfo::aggressionName(d_agPos.d_aggression));
#endif
   order.posture(d_agPos.d_posture);
   order.aggression(d_agPos.d_aggression);
   sendBattleOrder(unit->cp(), &order);
}

void HiZone::executeObjectives()
{
      if(!gameInfo()->isDeploying() && !canStart(gameInfo()->battleData()->getTick()))
         return;

#ifdef DEBUG
      gameInfo()->log("\n ------- ExecuteObjectives for %s", name());
#endif
      for(HiObjectiveList::iterator it = d_objectives.begin();
            it != d_objectives.end();
            ++it)
      {
         RealHiObjective* ob = *it;
         if(ob->units().size())
            ob->process();
      }
}

HexCord HiZone::leftFront() const
{
   HexCord bestLeft;
   return bestLeft;
}

HexCord HiZone::rightFront() const
{
   HexCord bestRight;
   return bestRight;
}


static UWORD s_zoneVersion = 0x0006;
bool HiZone::readData(FileReader& f)
{
   UWORD version;
   f >> version;
   ASSERT(version <= s_zoneVersion);

   d_boundries.readData(f);
   d_objectives.readData(f, gameInfo());

   UBYTE b;
   f >> b;
   d_zoneOrientation = static_cast<ZoneOrientation>(b);
   f >> d_priority;
   f >> d_startWhen;
   if(version < 0x0003)
   {
      f >> b;
      d_flags = b;
   }
   else
      f >> d_flags;

   if(version >= 0x0001)
   {
      f >> d_nextReserveCheck;
   }
   if(version >= 0x0002)
   {
      f >> d_nextUpdate;
   }
   if(version == 0x0004)
   {
      int rubbish;
      f >> rubbish;//d_moveLR;
      f >> rubbish;//d_moveFB;
   }
   if(version >= 0x0005)
   {
      UBYTE b;
      f >> b;
      whichWay(static_cast<GoWhichWay>(b));
   }
   if(version >= 0x0006)
      f >> d_hiFlags;

   d_agPos.readData(f);

#ifdef DEBUG
   std::ostringstream buf;
      buf << "Zone:" << (int) leftX() << ".." << (int) rightX() << '\0';
      d_name = buf.str();
#endif

   return f.isOK();
}

bool HiZone::writeData(FileWriter& f) const
{
   f << s_zoneVersion;

   d_boundries.writeData(f);
   d_objectives.writeData(f, gameInfo());

   UBYTE b = static_cast<UBYTE>(d_zoneOrientation);
   f << b;
   f << d_priority;
   f << d_startWhen;
   f << d_flags;
   f << d_nextReserveCheck;
   f << d_nextUpdate;
   //f << d_moveLR;
   //f << d_moveFB;
   b = static_cast<UBYTE>(d_whichWay);
   f << b;
   f << d_hiFlags;
   d_agPos.writeData(f);
   return f.isOK();
}

#ifdef DEBUG
void HiZone::logHistorical()
{
   gameInfo()->log("\n --- Processing Zone");
   gameInfo()->log("-----------------------");

   SListIter<Boundry> bIter(&d_boundries);
   while(++bIter)
   {
      gameInfo()->log("Zone Boundry --- left=(%d, %d), right=(%d, %d)",
         static_cast<int>(bIter.current()->leftHex().x()),
         static_cast<int>(bIter.current()->leftHex().y()),
         static_cast<int>(bIter.current()->rightHex().x()),
         static_cast<int>(bIter.current()->rightHex().y()));
   }

   for(HiUnitPList::iterator uit = d_units.begin();
         uit != d_units.end();
         ++uit)
   {
      HiUnitInfo* unit = *uit;
      gameInfo()->log("-------- %s", unit->cp()->getName());
   }

   for(HiObjectiveList::iterator it = d_objectives.begin();
       it != d_objectives.end();
       ++it)
   {
         RealHiObjective* ob = *it;
         ob->logHistorical(gameInfo());
   }
}
#endif
};  // namespace WG_BattleAI_Internal


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2002/11/16 18:03:26  greenius
 * Various changes so that it will compile with Visual Studio .net
 *
 * Revision 1.2  2001/06/18 08:25:43  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
