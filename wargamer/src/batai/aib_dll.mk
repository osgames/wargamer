##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

###########################################
# Battle Game Control Library Makefile
###########################################

NAME = batAI
lnk_dependencies += aib_dll.mk

!include ..\wgpaths.mif

CFLAGS += -DEXPORT_BATAI_DLL


OBJS =  aib_area.ob  j    &
        aib_data.obj    &
        aib_mid.obj     &
        aib_hiob.obj    &
        aib_hdat.obj    &
        aib_msg.obj     &
        aib_plan.obj    &
        aib_hi.obj      &
        aib_side.obj    &
        batai.obj

!ifndef NODEBUG
OBJS += aib_log.obj
!endif


SYSLIBS += COMCTL32.LIB VFW32.LIB DSOUND.LIB
SYSLIBS +=  USER32.LIB KERNEL32.LIB GDI32.LIB WINMM.LIB SHELL32.LIB

LIBS += system.lib
LIBS += gamesup.lib
LIBS += batdata.lib
LIBS += ob.lib

!include ..\dll95.mif


.before:
        @echo Making $(TARGETS)
