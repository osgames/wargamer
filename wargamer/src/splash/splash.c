/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
// Windows Header Files:
#include <windows.h>
#include "resource.h"

#define APPNAME     "WG_Splash"
#define PALVERSION  0x300
#define MAXPALETTE  256

// Global Variables:
HINSTANCE hInst;      // current instance
char szAppName[32];  // Name of the app

// Change these to customize the splash screen behavior
LPSTR lpszApplication    = "wargamer.exe"; // Name of the application to launch
BOOL  bMakeSplashTopmost = TRUE;          // Keep the splash screen in front
DWORD dwSplashDuration   = 5000;          // Time (in ms) to display the splash screen

// Foward declaration for our WndProc function
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//  FUNCTION: InitApplication(HANDLE)
//
//  PURPOSE: Initializes window data and registers window class
//
BOOL InitApplication(HINSTANCE hInstance)
{
    WNDCLASS  wc = {0};

    // Fill in window class structure with parameters that describe
    // the main window.
    wc.style         = 0;
    wc.lpfnWndProc   = (WNDPROC)WndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance;
    wc.hIcon         = LoadIcon (hInstance, szAppName);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = GetStockObject(NULL_BRUSH);
    wc.lpszClassName = szAppName;

    // Register the window class and return success/failure code.
    return RegisterClass(&wc);
}

//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
    HWND hWnd;
    RECT rect;
    HBITMAP hbm;
    BITMAP bm;

    hInst = hInstance; // Store instance handle in our global variable

    // Load the bitmap and fill out a BITMAP structure for it
	hbm = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BITMAP));
    GetObject(hbm, sizeof(bm), &bm);
	DeleteObject(hbm);

    // Get the extents of the desktop window
	GetWindowRect(GetDesktopWindow(), &rect);

    // Create a centered window the size of our bitmap
	hWnd = CreateWindow(szAppName, NULL, WS_POPUP | WS_BORDER,
//                       (rect.right  / 2) - (bm.bmWidth  / 2),
//                       (rect.bottom / 2) - (bm.bmHeight / 2),
//                       bm.bmWidth,
//                       bm.bmHeight,
                        rect.left,
                        rect.top,
                        rect.right-rect.left,
                        rect.bottom-rect.top,
                       NULL, NULL, hInstance, NULL);

    // Check to see if our window was created
	if (!hWnd)
        return (FALSE);

    // Make the window TOPMOST if specified
	if (bMakeSplashTopmost)
        SetWindowPos(hWnd, HWND_TOPMOST, 0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);

//    ShowWindow(hWnd, SW_SHOWNORMAL);
    ShowWindow(hWnd, SW_SHOWMAXIMIZED);
    UpdateWindow(hWnd);

    // Launch the application that this is the splash screen for
    WinExec(lpszApplication, SW_SHOWNORMAL);

    return (TRUE);
}

//  FUNCTION: WinMain(HANDLE, HANDLE, LPSTR, int)
//
// This function initializes the application and processes the
// message loop.
//
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
    MSG msg;

    // Initialize global strings
    lstrcpy (szAppName, APPNAME);

    if (!hPrevInstance) // Perform instance initialization:
        if (!InitApplication(hInstance))
            return (FALSE);

    // Perform application initialization:
    if (!InitInstance(hInstance, nCmdShow))
        return (FALSE);

    // Main message loop:
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (msg.wParam);

    // Assure that the desktop is redrawn when the splash screen goes away
    RedrawWindow(NULL, NULL, NULL,  RDW_ERASE | RDW_UPDATENOW | RDW_ALLCHILDREN);

    lpCmdLine; // This will prevent 'unused formal parameter' warnings
}

/*****************************************************************************
*                                                                            *
* HPALETTE CreateSpectrumPalette()                                           *
*                                                                            *
* Parameter:                                                                 *
*                                                                            *
* (none)                                                                     *
*                                                                            *
* Return Value:                                                              *
*                                                                            *
* HPALETTE         - Returns a handle to a spectrum palette or NULL if it    *
*                    fails.                                                  *
*                                                                            *
* Description:                                                               *
*                                                                            *
* This function will build a palette with a spectrum of colors.  It is       *
* useful when you want to display a number of DIBs each with a different     *
* palette yet still have an a good selection of colors for the DIBs to map   *
* to.                                                                        *
*                                                                            *
*****************************************************************************/
HPALETTE CreateSpectrumPalette()
{
    HPALETTE hPal;
    LPLOGPALETTE lplgPal;
    BYTE red, green, blue;
    int i;

    lplgPal = (LPLOGPALETTE)GlobalAlloc(GPTR, sizeof(LOGPALETTE) + sizeof(PALETTEENTRY) * MAXPALETTE);
    if (!lplgPal)
        return NULL;

    lplgPal->palVersion = PALVERSION;
    lplgPal->palNumEntries = MAXPALETTE;

    red = green = blue = 0;
    for (i = 0; i < MAXPALETTE; i++) {
        lplgPal->palPalEntry[i].peRed   = red;
        lplgPal->palPalEntry[i].peGreen = green;
        lplgPal->palPalEntry[i].peBlue  = blue;
        lplgPal->palPalEntry[i].peFlags = (BYTE)0;

        if (!(red += 32))
            if (!(green += 32))
                blue += 64;
    }

    hPal = CreatePalette(lplgPal);
    GlobalFree(lplgPal);

    return hPal;
}

/*****************************************************************************
*                                                                            *
* HPALETTE CreatePaletteFromRGBQUAD(LPRGBQUAD rgbqPalette, WORD wEntries)    *
*                                                                            *
* Parameter:                                                                 *
*                                                                            *
* LPRGBQUAD        - pointer to RGBQUADs                                     *
* WORD             - the number of RGBQUADs we are pointing to               *
*                                                                            *
* Return Value:                                                              *
*                                                                            *
* HPALETTE         - returns a handle to a palette or NULL if it failes      *
*                                                                            *
* Description:                                                               *
*                                                                            *
* This function will build a valid HPALETTE when given an array of RGBQUADs  *
*                                                                            *
*****************************************************************************/
HPALETTE CreatePaletteFromRGBQUAD(LPRGBQUAD rgbqPalette, WORD wEntries)
{
    HPALETTE hPal;
    LPLOGPALETTE lplgPal;
    int i;

    lplgPal = (LPLOGPALETTE)GlobalAlloc(GPTR, sizeof(LOGPALETTE) + sizeof(PALETTEENTRY) * wEntries);
    if (!lplgPal)
        return NULL;

    lplgPal->palVersion = PALVERSION;
    lplgPal->palNumEntries = wEntries;

    for (i=0; i<wEntries; i++) {
        lplgPal->palPalEntry[i].peRed   = rgbqPalette[i].rgbRed;
        lplgPal->palPalEntry[i].peGreen = rgbqPalette[i].rgbGreen;
        lplgPal->palPalEntry[i].peBlue  = rgbqPalette[i].rgbBlue;
        lplgPal->palPalEntry[i].peFlags = 0;
    }

    hPal = CreatePalette(lplgPal);
    GlobalFree(lplgPal);

    return hPal;
}

/*****************************************************************************
*                                                                            *
* void ShowBitmap(HWND hWnd, DWORD dwResId)                                  *
*                                                                            *
* Parameter:                                                                 *
*                                                                            *
* HWND             - handle of the target window                             *
* DWORD            - the ID of the resource bitmap to display                *
*                                                                            *
* Description:                                                               *
*                                                                            *
* This function loads and displays a resource bitmap into a specified window *
*                                                                            *
*****************************************************************************/
void ShowBitmap(HWND hWnd, DWORD dwResId)
{
    HBITMAP  hBitmap;
    HPALETTE hPalette;
    HDC      hdcMemory,
        hdcWindow;
    BITMAP   bm;
    RECT     rect;
    RGBQUAD  rgbq[256];

    // Get the extents of our window
    GetClientRect(hWnd, &rect);

    // Load the resource as a DIB section
    hBitmap = LoadImage(hInst,
        MAKEINTRESOURCE(dwResId),
        IMAGE_BITMAP,
        0,0,
        LR_CREATEDIBSECTION);
    GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);

    // Get the DC for the window
    hdcWindow = GetDC(hWnd);

    // Create a DC to hold our surface and selct our surface into it
    hdcMemory = CreateCompatibleDC(hdcWindow);
    SelectObject(hdcMemory, hBitmap);

    // Retrieve the color table (if there is one) and create a palette
    // that reflects it
    if (GetDIBColorTable(hdcMemory, 0, 256, rgbq))
        hPalette = CreatePaletteFromRGBQUAD(rgbq, 256);
    else
        hPalette = CreateSpectrumPalette();

    // Select and realize the palette into our window DC
    SelectPalette(hdcWindow,hPalette,FALSE);
    RealizePalette(hdcWindow);

    // Display the bitmap
	SetStretchBltMode(hdcWindow, COLORONCOLOR);
    StretchBlt(hdcWindow, 0, 0, rect.right, rect.bottom,
               hdcMemory, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);

	// Clean up our objects
    DeleteDC(hdcMemory);
    DeleteObject(hBitmap);
    ReleaseDC(hWnd, hdcWindow);
    DeleteObject(hPalette);
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    switch (message) {
    case WM_CREATE:
        // Set the timer for the specified number of ms
        SetTimer(hWnd, 0, dwSplashDuration, NULL);
        break;

	// Handle the palette messages in case
	// another app takes over the palette
    case WM_PALETTECHANGED:
        if ((HWND) wParam == hWnd)
            return 0;
    case WM_QUERYNEWPALETTE:
        InvalidateRect(hWnd, NULL, FALSE);
		UpdateWindow(hWnd);
		return TRUE;

    // Destroy the window if...
    case WM_LBUTTONDOWN:      // ...the user pressed the left mouse button
    case WM_RBUTTONDOWN:      // ...the user pressed the right mouse button
    case WM_TIMER:            // ...the timer timed out
        DestroyWindow(hWnd);  // Close the window
        break;

        // Draw the window
    case WM_PAINT:
        hdc = BeginPaint (hWnd, &ps);
        ShowBitmap(hWnd, IDB_BITMAP);
        EndPaint (hWnd, &ps);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    default:
        return (DefWindowProc(hWnd, message, wParam, lParam));
    }
    return (0);
}

