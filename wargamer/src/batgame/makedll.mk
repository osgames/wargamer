##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

###########################################
# Battle Game Control Library Makefile
###########################################

NAME = batGame
lnk_dependencies += makedll.mk

!include ..\wgpaths.mif

CFLAGS += -DEXPORT_BATGAME_DLL

OBJS = batgame.obj

CFLAGS += -i=$(ROOT)\gamesup
CFLAGS += -i=$(ROOT)\system
CFLAGS += -i=$(ROOT)\batwind
CFLAGS += -i=$(ROOT)\batdata
CFLAGS += -i=$(ROOT)\batlogic
CFLAGS += -i=$(ROOT)\batai
CFLAGS += -i=$(ROOT)\batdisp
CFLAGS += -i=$(ROOT)\ob

SYSLIBS += COMCTL32.LIB VFW32.LIB DSOUND.LIB
# SYSLIBS += USER32.LIB KERNEL32.LIB GDI32.LIB WINMM.LIB SHELL32.LIB

LIBS += system.lib
LIBS += gamesup.lib
LIBS += batwind.lib
LIBS += batdata.lib
LIBS += batlogic.lib
LIBS += batai.lib
LIBS += batdisp.lib
LIBS += ob.lib

TARGETS += $(LIBNAME)

!include ..\dll95.mif


.before:
        @echo Making $(TARGETS)




$(LIBNAME) : $(TARGETNAME)
        wlib -n -b $(LIBNAME) $(TARGETNAME)
