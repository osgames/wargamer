/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Edit Date
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "editdate.hpp"

#include "resdef.h"
#include "dialog.hpp"
#include "gamectrl.hpp"
#include "camptime.hpp"
#include "winctrl.hpp"
#include "ctimdata.hpp"

const int YearMin = 1700;
const int YearMax = 1999;

// using namespace Greenius_System;

static ComboInit comboMonthData[] = {
   { DateDefinitions::January,   "January"   },
   { DateDefinitions::February,  "February"  },
   { DateDefinitions::March,     "March"     },
   { DateDefinitions::April,     "April"     },
   { DateDefinitions::May,       "May"    },
   { DateDefinitions::June,      "June"      },
   { DateDefinitions::July,      "July"      },
   { DateDefinitions::August,       "August" },
   { DateDefinitions::September,    "September" },
   { DateDefinitions::October,   "October"   },
   { DateDefinitions::November,  "November"  },
   { DateDefinitions::December,  "December"  },
   { -1, 0                    }
};


class EditDate : public ModalDialog {
   Date* date;       // Fill in new date here
public:
   EditDate(Date* datePtr);
   ~EditDate();

   int run(HWND hParent);
private:
   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   BOOL EditDate::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void EditDate::onDestroy(HWND hwnd);
   void EditDate::onClose(HWND hwnd);
   void EditDate::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
   void EditDate::onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized);
};


EditDate::EditDate(Date* datePtr)
{
   date = datePtr;
}

EditDate::~EditDate()
{
}

int EditDate::run(HWND parent)
{
   // HWND parent = gApp.getHwnd();

   int result = createDialog(MAKEINTRESOURCE(DLG_DATE_EDIT), parent);

   return result;
}


BOOL EditDate::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_DLG_MSG(WM_INITDIALOG, onInitDialog);
      HANDLE_DLG_MSG(WM_DESTROY, onDestroy);
      HANDLE_DLG_MSG(WM_COMMAND, onCommand);
      HANDLE_DLG_MSG(WM_CLOSE, onClose);
      HANDLE_DLG_MSG(WM_ACTIVATE, onActivate);
      default:
         return FALSE;
   }
   return TRUE;
}

BOOL EditDate::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   /*
    * Set Date
    */

   {
      Spinner daySpin(hwnd, ID_DATE_DAYSPIN);
      daySpin.setRange(1, 31);
      daySpin.setValue(date->day);
   }

   /*
    * Set Month
    */

   {
      ComboBox monthCombo(hwnd, ID_DATE_MONTH);
      monthCombo.initItems(comboMonthData);
      monthCombo.setValue(date->month);
   }

   /*
    * Set Year
    */

   {
      Spinner yearSpin(hwnd, ID_DATE_YEARSPIN);
      yearSpin.setRange(YearMin, YearMax);
      yearSpin.setValue(date->year);
   }

   return TRUE;
}

void EditDate::onDestroy(HWND hwnd)
{
}

void EditDate::onClose(HWND hwnd)
{
   onCommand(hwnd, IDCANCEL, 0, 0);
}

void EditDate::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
   switch(id)
   {
      case IDOK:
         if(codeNotify == BN_CLICKED)
         {
            // Copy data from dialog items into Date

            {
               Spinner daySpin(hWnd, ID_DATE_DAYSPIN);
               date->day = (Day) daySpin.getValue();
            }
            {
               ComboBox monthCombo(hWnd, ID_DATE_MONTH);
               date->month = (Month) monthCombo.getValue();
            }
            {
               Spinner yearSpin(hWnd, ID_DATE_YEARSPIN);
               date->year = (Year) yearSpin.getValue();
            }
         }
         else
            break;
      case IDCANCEL:
         if(codeNotify == BN_CLICKED)
         {
            EndDialog(hwnd, id);
         }
         break;

      case ID_DATE_DAY:
      case ID_DATE_DAYSPIN:
      case ID_DATE_MONTH:
      case ID_DATE_YEARSPIN:
         break;
      case ID_DATE_YEAR:
         // Validate Date

         {
            Spinner yearSpin(hWnd, ID_DATE_YEARSPIN);
            Year year = (Year) yearSpin.getValue();

         }

         break;

#ifdef DEBUG
      default:
         debugLog("EditDate::onCommand(), unknown id %d %d\n", id, codeNotify);
#endif
   }
}

void EditDate::onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized)
{
}


/*
 * Create and run editor
 * This is the only entry point to this module
 */

Boolean DateEditor::edit(HWND parent, CampaignTimeData* campaignDate)
{
   Boolean oldPause = GameControl::pause(True);

   Date date = campaignDate->getDate();      // Copy of campaign Date

   EditDate* edit = new EditDate(&date);
   int result = edit->run(parent);

   if(result == IDOK)
   {
      campaignDate->initTime(date);
   }

   delete edit;

   GameControl::pause(oldPause);

   return (result == IDOK);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
