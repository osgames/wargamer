/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MWED_INT_HPP
#define MWED_INT_HPP

#ifndef __cplusplus
#error mwed_int.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Interface for Map Window Editors to pass information back to Manager
 *
 *----------------------------------------------------------------------
 */

#include "gamedefs.hpp"
#include "mw_user.hpp"		// Needed for TrackMode

class EditDialog;

/*
 * Enumerations for edit mode
 */


class MWEditInterface {
 public:
 	virtual ~MWEditInterface() = 0;

	virtual void editDestroyed(EditDialog* edit) = 0;
	virtual void itemChanged() = 0;
	virtual void townSelected(ITown ob) = 0;
	virtual void setTrackMode(TrackMode mode) = 0;

#ifdef CUSTOMIZE   // used by Data Sanity Check routines
	virtual void runOBEdit(ICommandPosition cpi) = 0;
	virtual void runTownEdit(ITown iTown) = 0;
	virtual void runProvinceEdit(IProvince iProv) = 0;
	virtual void runConnectionEdit(ITown iTown) = 0;
#endif
};

inline MWEditInterface::~MWEditInterface() { }

#endif /* MWED_INT_HPP */

