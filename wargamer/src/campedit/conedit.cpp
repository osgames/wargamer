/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Connection Editor
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.1  2001/06/13 08:52:37  greenius
 * Initial Import
 *
 * Revision 1.5  1996/06/22 19:06:06  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1996/02/19 16:16:55  Steven_Green
 * Use TownID and ProvinceID rather than Pointers
 *
 * Revision 1.3  1996/01/19 11:30:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/12/11 11:50:58  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.1  1995/12/07 09:15:20  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "conedit.hpp"
#include "mwed_int.hpp"
#include "mapwind.hpp"

#include "resdef.h"
#include "scenario.hpp"
#include "misc.hpp"
#include "campdint.hpp"
#include "winctrl.hpp"
#include "help.h"

/*
 * Combo Box data
 */

static ComboInit comboHowData[] = {
   { CT_Road,  "Road"   },
   { CT_Rail,  "Rail"   },
   { CT_River, "River"  },
   { -1, 0              }
};

static ComboInit comboQualityData[] = {
   { CQ_Poor,     "Poor"      },
   { CQ_Average,  "Average"   },
   { CQ_Good,     "Good"      },
   { -1, 0                    }
};

static ComboInit comboWhichSideData[] = {
   { WhichSideChokePoint::NoChokePoint, "No Chokepoint" },
   { WhichSideChokePoint::ThisSide,     "This side" },
   { WhichSideChokePoint::ThatSide,     "That side" },
   { -1, 0 }
};

/*
 * Constructor
 */

ConnectionEdit::ConnectionEdit(MWEditInterface* editControl, CampaignData* cData, MapWindow* p, ITown it, const PixelPoint& pos) :
   d_editControl(editControl),
   d_campData(cData)
{
   parent = p;
   townID = it;
   town = &campaignData->getTown(it);
   POINT pt = pos;
   ClientToScreen(parent->getHWND(), &pt);
   position = pt;
   current = 0;
   lbSel = -1;
   tiSel = -1;

   oldHow = CT_Road;
   oldQuality = CQ_Average;
   oldCapacity = MaxConnectCapacity;
   oldOffScreen = False;
   oldWhichSide = WhichSideChokePoint::NoChokePoint;
   oldDistance = 0;

   defaultHow = CT_Road;
   defaultQuality = CQ_Average;
   defaultCapacity = MaxConnectCapacity;
   defaultOffScreen = False;
   defaultWhichSide = WhichSideChokePoint::NoChokePoint;
   defaultDistance = 0;

   HWND dhwnd = createDialog(MAKEINTRESOURCE(DLG_CONNECTEDIT), parent->getHWND());
   ASSERT(dhwnd != NULL);
}

ConnectionEdit::~ConnectionEdit()
{
}

BOOL ConnectionEdit::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;

      case WM_CLOSE:
         HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
         break;

      case WM_ACTIVATE:
         HANDLE_WM_ACTIVATE(hWnd, wParam, lParam, onActivate);
         break;

      case WM_CONTEXTMENU:
         HANDLE_WM_CONTEXTMENU(hWnd, wParam, lParam, onContextMenu);
         break;

      case WM_HELP:
         HANDLE_WM_HELP(hWnd, wParam, lParam, onHelp);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

static const DWORD ids[] = {
    IDCE_LISTBOX,    IDH_CE_List,
    IDCE_OFFSCREEN,  IDH_CE_OffScreen,
    IDCE_DISTANCE,   IDH_CE_Distance,
    IDCE_HOW,        IDH_CE_Type,
    IDCE_QUALITY,    IDH_CE_Quality,
    IDCE_CAPACITY,   IDH_CE_Capacity,
    IDCE_DELETE,     IDH_CE_Delete,
    IDCE_CLOSE,      IDH_CE_Close,

    0, 0
};

void ConnectionEdit::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
//  WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
}

BOOL ConnectionEdit::onHelp(HWND hwnd, LPHELPINFO lparam)
{
  LPHELPINFO lphi = (LPHELPINFO)lparam;
  if (lphi->iContextType == HELPINFO_WINDOW)   // must be for a control
  {
//  WinHelp ((HWND)lphi->hItemHandle, "help\\Wg95Help.hlp", HELP_WM_HELP, (DWORD)(LPVOID)ids);
  }
  return TRUE;
}

#if 0
Town* ConnectionEdit::getOtherTown(const Connection* c)
{
   TownList& tl = campaignData->getTowns();
   ITown tid = tl.getID(town);

   if(c->node1 == tid)
      return &tl[c->node2];
   else if(c->node2 == tid)
      return &tl[c->node1];
   else
      throw GeneralError("Connection doesn;t go to our town");
}
#else
ITown ConnectionEdit::getOtherTown(const Connection* c)
{
   TownList& tl = campaignData->getTowns();

   if(c->node1 == townID)
      return c->node2;
   else if(c->node2 == townID)
      return c->node1;
   else
      throw GeneralError("Connection doesn;t go to our town");
}
#endif

/*
 * Setup dialog for selected Connection
 * sel is <0 for no selection
 *
 * sel is the index into the town's connection list
 */

void ConnectionEdit::select(int sel)
{
#ifdef DEBUG
   debugLog("select(%d)\n", sel);
#endif

   updateCurrent();

   tiSel = sel;

   if(sel < 0)
   {
      current = 0;
      lbSel = -1;

      // Disable settings and delete,
      // because they are only meaningful if a connection is selected

      EnableWindow(GetDlgItem(hWnd, IDCE_SETTINGSTITLE), FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_HOWTITLE),      FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_HOW),           FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_QUALITYTITLE),  FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_QUALITY),       FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_CAPACITYTITLE), FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_CAPACITY),      FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_CAPACITYSPIN),  FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_DELETE),        FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_OFFSCREEN),     FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_DISTANCE),      FALSE);
      EnableWindow(GetDlgItem(hWnd, IDCE_WHICHSIDE),     FALSE);

      d_editControl->townSelected(NoTown);
   }
   else
   {
      // Set the values

      ConnectionList& cl = campaignData->getConnections();
      ASSERT(sel < MaxConnections);
      IConnection ci = town->getConnection(sel);
      ASSERT(ci != NoConnection);
      current = &cl[ci];
      ITown otherTown = getOtherTown(current);
      d_editControl->townSelected(otherTown);

#ifdef DEBUG
      debugLog("select: sel=%d, ci=%d\n", (int) sel, (int) ci);
#endif

      setComboIDValue(IDCE_HOW, current->how);
      setComboIDValue(IDCE_QUALITY, current->quality);
      setComboIDValue(IDCE_WHICHSIDE, current->whichSideChokePoint(townID));
      setSpinIDValue(IDCE_CAPACITYSPIN, current->capacity);

      setButtonIDCheck(IDCE_OFFSCREEN, current->offScreen);

      SetDlgItemInt(hWnd, IDCE_DISTANCE, DistanceToMile(current->distance), FALSE);

      getOldValues();

      EnableWindow(GetDlgItem(hWnd, IDCE_SETTINGSTITLE), TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_HOWTITLE),      TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_HOW),           TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_QUALITYTITLE),  TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_QUALITY),       TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_CAPACITYTITLE), TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_CAPACITY),      TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_CAPACITYSPIN),  TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_DELETE),        TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_OFFSCREEN),     TRUE);
      //---- Distance always enabled
      // EnableWindow(GetDlgItem(hWnd, IDCE_DISTANCE),      current->offScreen);
      EnableWindow(GetDlgItem(hWnd, IDCE_DISTANCE),      TRUE);
      EnableWindow(GetDlgItem(hWnd, IDCE_WHICHSIDE),     TRUE);
   }

   parent->requestRedraw(FALSE);
}

void ConnectionEdit::updateCurrent()
{
   if(current &&
      (
       (oldHow != current->how) ||
       (oldQuality != current->quality) ||
       (oldOffScreen != current->offScreen) ||
       (oldCapacity != current->capacity) ||
       (oldWhichSide != current->whichSideChokePoint(townID)) ||
       (oldDistance != current->distance)
      ) 
     )
   {
      parent->requestRedraw(TRUE);
      getOldValues();
   }
}

void ConnectionEdit::makeConnectionName(char* buffer, const Connection* c)
{
   static char* connectionTypeName[CT_Max] = {
      "Road",
      "Rail",
      "River"
   };

   static char* connectionQualityName[CQ_Max] = {
      "Poor",
      "Average",
      "Good"
   };

   static char* whichSideName[WhichSideChokePoint::HowMany] = {
      "NC",   // NoConnection
      "This", // ThisSide
      "That"  // ThatSide
   };

   ITown idestTown = getOtherTown(c);
   Town* destTown = &campaignData->getTown(idestTown);

   const char* destTownName = destTown->getName();

   if(destTownName == 0)
      destTownName = "Unnamed";

   wsprintf(buffer, "%s\t%s\t%s\t%s\t%d",
      destTownName,
      connectionQualityName[c->quality],
      connectionTypeName[c->how],
      whichSideName[c->whichSideChokePoint(townID)],
      (int) c->capacity);
}

#if 0
void ConnectionEdit::insertConnection(IConnection iCon)
{
   HWND hList = GetDlgItem(getHWND(), IDCE_LISTBOX);

   Connection* current = &d_campData->getConnection(iCon);

   char buffer[100];

   makeConnectionName(buffer, current);
   lbSel = SendMessage(hList, LB_INSERTSTRING, lbSel, (LPARAM) buffer);
   ASSERT(lbSel != LB_ERR);
   ASSERT(lbSel != LB_ERRSPACE);
   SendMessage(hList, LB_SETITEMDATA, lbSel, tiSel);
   SendMessage(hList, LB_SETCURSEL, lbSel, 0);

   debugLog("insertConnection lbSel=%d, tiSel=%d\n", (int) lbSel, (int) tiSel);
}
#endif

/*
 * Updates selected item
 * If no item was selected (lbSel < 0)
 * then a nwe item is made and selected
 */

void ConnectionEdit::updateConnectionName()
{
   if(current)
   {
      HWND hList = GetDlgItem(getHWND(), IDCE_LISTBOX);

      if(lbSel >= 0)
         SendMessage(hList, LB_DELETESTRING, lbSel, 0);

      char buffer[100];

      makeConnectionName(buffer, current);
      lbSel = SendMessage(hList, LB_INSERTSTRING, lbSel, (LPARAM) buffer);
      ASSERT(lbSel != LB_ERR);
      ASSERT(lbSel != LB_ERRSPACE);
      SendMessage(hList, LB_SETITEMDATA, lbSel, tiSel);
      SendMessage(hList, LB_SETCURSEL, lbSel, 0);

      debugLog("updateConnectionName lbSel=%d, tiSel=%d\n", (int) lbSel, (int) tiSel);
   }
}


void ConnectionEdit::setupListBox()
{
   // Fill in List box

   HWND hList = GetDlgItem(hWnd, IDCE_LISTBOX);
   ASSERT(hList != NULL);

   // Setup the tabs

   INT tabs[] = { 64, 96, 128 };
   SendMessage(hList, LB_SETTABSTOPS, (WPARAM) (sizeof(tabs) / sizeof(INT)), (LPARAM) (LPINT) tabs);

   // Setup the items

   ConnectionList& cl = campaignData->getConnections();

   for(int r = 0; r < MaxConnections; r++)
   {
      IConnection con = town->getConnection(r);

      if(con != NoConnection)
      {
         const Connection* c = &cl[con];

         char buffer[100];

         makeConnectionName(buffer, &cl[con]);
         int index = SendMessage(hList, LB_ADDSTRING, 0, (LPARAM) buffer);
         ASSERT(index != LB_ERR);
         ASSERT(index != LB_ERRSPACE);
         SendMessage(hList, LB_SETITEMDATA, index, r);
      }
   }

}

BOOL ConnectionEdit::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   // Setup title

   char buffer[100];
   const char* s = town->getName();
   if(s == 0)
      s = "Unnamed";
   wsprintf(buffer, "Connections from %s", s);
   SetDlgItemText(hwnd, IDCE_TITLE, buffer);

   initComboIDItems(IDCE_HOW, comboHowData);
   initComboIDItems(IDCE_QUALITY, comboQualityData);
   initComboIDItems(IDCE_WHICHSIDE, comboWhichSideData);
   SendDlgItemMessage(hWnd, IDCE_CAPACITYSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, UBYTE_MIN));

   setupListBox();
   select(-1);    // Start off with no selection

   d_editControl->setTrackMode(MWTM_Town);   // wantTown();

   return TRUE;
}

void ConnectionEdit::onDestroy(HWND hwnd)
{
   d_editControl->editDestroyed(this);
}

void ConnectionEdit::onClose(HWND hwnd)
{
   onCommand(hwnd, IDCE_CANCEL, 0, 0);
}

void ConnectionEdit::onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized)
{
}


void ConnectionEdit::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
   Boolean changed = False;

   switch(id)
   {
      case IDCE_OK:
      case IDCE_CLOSE:
      case IDCE_CANCEL:
#ifdef DEBUG
         debugLog("IDCE_OK/CLOSE/CANCEL %d\n", codeNotify);
#endif
         if(codeNotify == BN_CLICKED)
         {
            updateCurrent();
            DestroyWindow(hwnd);
         }
         break;

#if 0
      case IDCE_NEW:
#ifdef DEBUG
         debugLog("IDCE_NEW %d\n", codeNotify);
#endif
         if(codeNotify == BN_CLICKED)
            d_editControl->setTrackMode(MWTM_Town);   // wantTown(this);
         break;
#endif

      case IDCE_DELETE:
#ifdef DEBUG
         debugLog("IDCE_DELETE %d\n", codeNotify);
#endif
         onDelete(codeNotify);
         break;
      case IDCE_LISTBOX:
         onListBox(hwnd, id, hwndCtl, codeNotify);
         break;
      case IDCE_HOW:
#ifdef DEBUG
         debugLog("IDCE_HOW %d\n", codeNotify);
#endif
         if(codeNotify == CBN_SELCHANGE)
         {
            ConnectType newType = (ConnectType) getComboIDValue(IDCE_HOW);
            if(newType != current->how)
            {
               current->how = newType;
               defaultHow = current->how;
               updateConnectionName();
               campaignData->setChanged();
               changed = True;
            }
         }
         break;
      case IDCE_QUALITY:
#ifdef DEBUG
         debugLog("IDCE_QUALITY %d\n", codeNotify);
#endif
         if(codeNotify == CBN_SELCHANGE)
         {
            ConnectQuality newQuality = (ConnectQuality) getComboIDValue(IDCE_QUALITY);
            if(current->quality != newQuality)
            {
               current->quality = newQuality;
               defaultQuality = current->quality;
               updateConnectionName();
               campaignData->setChanged();
               changed = True;
            }
         }
         break;
      case IDCE_WHICHSIDE:
#ifdef DEBUG
         debugLog("IDCE_WHICHSIDE %d\n", codeNotify);
#endif
         if(codeNotify == CBN_SELCHANGE)
         {
            WhichSideChokePoint::Type newType = static_cast<WhichSideChokePoint::Type>(getComboIDValue(IDCE_WHICHSIDE));
            if(newType != current->whichSideChokePoint(townID))
            {
               current->setWhichSideChokePoint(townID, newType);
               defaultWhichSide = newType;
               updateConnectionName();
               campaignData->setChanged();
               changed = True;
            }
         }
         break;
      case IDCE_CAPACITY:
#ifdef DEBUG
         debugLog("IDCE_CAPACITY %d\n", codeNotify);
#endif
         if(codeNotify == EN_CHANGE)
         {
            ConnectCapacity newCapacity = (ConnectCapacity) getSpinIDValue(IDCE_CAPACITYSPIN);
            if(newCapacity != current->capacity)
            {
               current->capacity = newCapacity;
               defaultCapacity = current->capacity;
               updateConnectionName();
               campaignData->setChanged();
               changed = True;
            }
         }
         break;
      case IDCE_CAPACITYSPIN:
#ifdef DEBUG
         debugLog("IDCE_CAPACITYSPIN %d\n", codeNotify);
#endif
         break;

      case IDCE_OFFSCREEN:
#ifdef DEBUG
         debugLog("IDCE_OFFSCREEN %d\n", codeNotify);
#endif
         if(codeNotify == BN_CLICKED)
         {
           Boolean offScreen = getButtonIDCheck(IDCE_OFFSCREEN);
           if(offScreen != current->offScreen)
           {
             current->offScreen = offScreen;
             defaultOffScreen = current->offScreen;
             updateConnectionName();
             campaignData->setChanged();
             changed = True;

             //--- Distance always enabled
             // EnableWindow(GetDlgItem(hWnd, IDCE_DISTANCE), current->offScreen);
           }
         }
         break;

      case IDCE_DISTANCE:
#ifdef DEBUG
         debugLog("IDCE_DISTANCE %d\n", codeNotify);
#endif
         if(codeNotify == EN_CHANGE)
         {
           current->distance = MilesToDistance(GetDlgItemInt(hWnd, id, NULL, FALSE));
         }
         break;


#ifdef DEBUG
      default:
         debugLog("ConnectionEdit::onCommand(), unknown id %d %d\n", id, codeNotify);
#endif
   }

   if(changed)
      parent->requestRedraw(TRUE);

}

void ConnectionEdit::onListBox(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
   debugLog("IDCE_LISTBOX\n");

   if(codeNotify == LBN_SELCHANGE)
   {
      HWND hList = GetDlgItem(hwnd, id);
      int nItem = SendMessage(hList, LB_GETCURSEL, 0, 0);
      int i = SendMessage(hList, LB_GETITEMDATA, nItem, 0); 
#ifdef DEBUG
      debugLog("onListBox: nItem=%d, i=%d\n", nItem, i);
#endif
      lbSel = nItem;
      select(i);
      campaignData->setChanged();
   }
}

void ConnectionEdit::kill()
{
   onClose(getHWND());
}

TrackMode ConnectionEdit::onSelect(TrackMode mode, const MapSelect& info)
{
   if(mode == MWTM_Town)
   {
      if(info.selectedTown != NoTown)
      {
         Town* t = &campaignData->getTown(info.selectedTown);

         const char* s = t->getNameNotNull("Unnamed");

#ifdef DEBUG
         debugLog("ConnectionEdit::gotTown(%s)\n", s);
#endif

         /*
         * Make a new entry in connection table
         */

         ConnectionList& cl = campaignData->getConnections();
         IConnection ci = cl.addNew();
         ASSERT(ci != NoConnection);
         Connection* con = &cl[ci];

#ifdef DEBUG
         debugLog("ci=%d\n", (int) ci);
#endif

         TownList& tl = campaignData->getTowns();

         con->node1 = tl.getID(town);
         con->node2 = tl.getID(t);

         // Fill values with most recent settings

         con->how = defaultHow;
         con->quality = defaultQuality;
         con->capacity = defaultCapacity;
         con->offScreen = defaultOffScreen;
         con->setWhichSideChokePoint(info.selectedTown, defaultWhichSide);

         // Set Distance
         //--- May as well still calculate it
         // if(!con->offScreen)
            campaignData->calculateConnectionLength(ci);

         /*
         * Add entry into the two towns
         */

         int id = t->addConnection(ci);
         debugLog("id(%s)=%d\n", t->getNameNotNull(), id);

         id = town->addConnection(ci);
#ifdef DEBUG
         debugLog("id(%s)=%d\n", town->getNameNotNull(), id);
#endif

         /*
         * Update the boxlist
         */

         lbSel = -1;
         // insertConnection(ci);
         select(id);
         updateConnectionName();

         parent->requestRedraw(TRUE);
         campaignData->setChanged();
      }

      return mode;
   }
#ifdef DEBUG
   else if(mode != MWTM_None)
      FORCEASSERT("Unexpected TrackMode");
#endif

   return mode;
}


void ConnectionEdit::onDelete(UINT codeNotify)
{
   if(codeNotify == BN_CLICKED)
   {
      /*
       * Remove from List box
       */

      HWND hList = GetDlgItem(getHWND(), IDCE_LISTBOX);
      ASSERT(hList != NULL);
      ASSERT(lbSel >= 0);
      SendMessage(hList, LB_DELETESTRING, lbSel, 0);

      /*
       * Delete the current selected connection
       */

      int oldSel = tiSel;

      select(-1);

      ConnectionList& cl = campaignData->getConnections();
      ASSERT(oldSel >= 0);
      IConnection ci = town->getConnection(oldSel);
      ASSERT(ci != NoConnection);
      campaignData->removeConnection(ci);

      parent->requestRedraw(TRUE);
      campaignData->setChanged();
   }
}

void ConnectionEdit::getOldValues()
{
   oldHow = current->how;
   oldCapacity = current->capacity;
   oldQuality = current->quality;
   oldOffScreen = current->offScreen;
   oldWhichSide = current->whichSideChokePoint(townID);
   oldDistance = current->distance;

   defaultHow = oldHow;
   defaultCapacity = oldCapacity;
   defaultQuality = oldQuality;
   defaultOffScreen = oldOffScreen;
   defaultWhichSide = oldWhichSide;
   defaultDistance = oldDistance;
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
