/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *
 * Started by Paul Sample.
 *
 *----------------------------------------------------------------------
 *
 * Order of Battle Editor
 *
 *----------------------------------------------------------------------
 */


#include "stdinc.hpp"
#include "obedit.hpp"
#include "mwed_int.hpp"

#include "app.hpp"
#include "resdef.h"
#include "campdint.hpp"
#include "scenario.hpp"
#include "grtypes.hpp"
#include "editsup.hpp"
#include "obwin.hpp"
#include "misc.hpp"
#include "scn_res.h"
#include "winctrl.hpp"
// #include "bargraph.hpp"
#include "fonts.hpp"
#include "scenario.hpp"
#include "town.hpp"
#include "dc_man.hpp"
#include "imglib.hpp"
#include "mapwind.hpp"
#include "help.h"
#include "memptr.hpp"
#include "armyutil.hpp"
#include "fileiter.hpp"
#include "morale.hpp"
#include "random.hpp"
#ifdef DEBUG
#if !defined(EDITOR)
#include "logwin.hpp"
#endif
#endif


/*
 * New Version that uses my own custom property sheet style dialogues
 */

/*
 * Classes for each page
 */

class OBEditPage : public ModelessDialog {
   DLGTEMPLATE* dialog;
protected:
   OBEdit* edit;        // Pointer to master data
public:
   OBEditPage(OBEdit* p, const char* dlgName);

   DLGTEMPLATE* getDialog() const { return dialog; }
   virtual const char* getTitle() const = 0;
   HWND create();
   // void enable();
   // void disable();
   virtual void updateValues() = 0;
   virtual void initControls() = 0;
protected:
   void setPosition();

   CampaignData* getCampaignData() const { return edit->getCampaignData(); }
};


class OBEditAllUnits : public OBEditPage {
   ImageList images;
   HBITMAP icons;
public:
   OBEditAllUnits(OBEdit* p) : OBEditPage(p, obEditAllUnitsPage) { }
   const char* getTitle() const { return "Units"; }
private:
    void initControls();
   void updateValues();

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
   void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);


   // virtual function from OBTreeview
// void orderUnit(unsigned int id, unsigned code);

};

class OBEditPosition : public OBEditPage, public OBTreeView {
public:
   OBEditPosition(OBEdit* p) : OBEditPage(p, obEditPositionPage) { }
   const char* getTitle() const { return "Position"; }
   void updateValues();
private:
   void initControls();

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
   // virtual function from OBTreeview
   void orderUnit(unsigned int id, unsigned code);
};


class OBEditUnit : public OBEditPage {
   int d_pos;
public:
   OBEditUnit(OBEdit* p) : OBEditPage(p, obEditUnitPage) { }
   const char* getTitle() const { return "Unit"; }
   void updateValues();
private:
   void initControls();

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
   void onDestroy(HWND hwnd);
};

class OBEditLeader : public OBEditPage {
   int d_pos;
public:
   OBEditLeader(OBEdit* p) : OBEditPage(p, obEditLeaderPage) { }
   const char* getTitle() const { return "Leader"; }
   void updateValues();
private:
   void initControls();

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
    void onDestroy(HWND hwnd);

   void setRankLevel();
   Rank getRankLevel();

   static const char s_defaultPortrait[];
};


//=========================================================================
// callback function for listbox subclass procedure

WNDPROC lpfnListWndProc = 0;

LRESULT CALLBACK subClassProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  ASSERT(lpfnListWndProc != 0);

  OBEdit* ob = (OBEdit*)GetWindowLong(hwnd, GWL_USERDATA);
  ASSERT(ob != 0);

  switch(msg)
  {
    case WM_RBUTTONDOWN:
      HANDLE_WM_RBUTTONDOWN(hwnd, wParam, lParam, ob->onRButtonDown);
      break;
  }

  return CallWindowProc(lpfnListWndProc, hwnd, msg, wParam, lParam);
}

WNDPROC lpfnListWndProc2 = 0;

LRESULT CALLBACK subClassProc2(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  ASSERT(lpfnListWndProc2 != 0);

  OBEdit* ob = (OBEdit*)GetWindowLong(hwnd, GWL_USERDATA);
  ASSERT(ob != 0);

  switch(msg)
  {
    case WM_RBUTTONDOWN:
      HANDLE_WM_RBUTTONDOWN(hwnd, wParam, lParam, ob->onRButtonDown);
      break;
  }

  return CallWindowProc(lpfnListWndProc2, hwnd, msg, wParam, lParam);
}

VOID APIENTRY OBEdit::obEditPopupMenu(HWND hwnd, PixelPoint& pt, int id)
{
   HMENU hmenu;            // top-level menu
   HMENU hmenuTrackPopup;  // pop-up menu

   // Load the menu resource.

   hmenu = LoadMenu(APP::instance(), MAKEINTRESOURCE(id));

   ASSERT(hmenu != NULL);

   // TrackPopupMenu cannot display the top-level menu, so get
   // the handle of the first pop-up menu.

   hmenuTrackPopup = GetSubMenu(hmenu, 0);
   ASSERT(hmenuTrackPopup != NULL);

   // Display the floating pop-up menu. Track the right mouse
   // button on the assumption that this function is called
   // during WM_CONTEXTMENU processing.

   POINT p = pt;
   if(id == MENU_OBEDITPOPUP)
   {
     HMENU hCreateUnit = GetSubMenu(hmenuTrackPopup, 3);
     ASSERT(hCreateUnit != NULL);
     HMENU hDeleteUnit = GetSubMenu(hmenuTrackPopup, 7);
     ASSERT(hDeleteUnit != NULL);

     ListBox listBox(hwnd, IDOBE_UNITS_NAME);
     ClientToScreen(listBox.getHWND(), &p);

     ASSERT(cp != 0);
     RankEnum rank = cp->getRankEnum();

     EnableMenuItem(hCreateUnit, IDM_OBE_CREATEARMY,
         rank < Rank_Army ? MF_BYCOMMAND | MF_ENABLED : MF_BYCOMMAND | MF_GRAYED);
       EnableMenuItem(hCreateUnit, IDM_OBE_CREATEARMYWING,
         False ? MF_BYCOMMAND | MF_ENABLED : MF_BYCOMMAND | MF_GRAYED);
     EnableMenuItem(hCreateUnit, IDM_OBE_CREATECORPS,
         rank < Rank_Corps ?  MF_BYCOMMAND | MF_ENABLED : MF_BYCOMMAND | MF_GRAYED);
     EnableMenuItem(hCreateUnit, IDM_OBE_CREATEDIVISION,
         rank < Rank_Division ?  MF_BYCOMMAND | MF_ENABLED : MF_BYCOMMAND | MF_GRAYED);
   }
   else if(id == MENU_DELETESPPOPUP)
   {
     ListBox box(hwnd, IDOBE_UNITS_SPLIST);
     ClientToScreen(box.getHWND(), &p);
   }
   else if(id == MENU_OBEDITPOPUP2)
   {
     ListBox box(hwnd, IDOBE_LEADERS_LIST);
     ClientToScreen(box.getHWND(), &p);
   }

   TrackPopupMenuEx(hmenuTrackPopup,
         TPM_LEFTALIGN | TPM_RIGHTBUTTON,
         p.x, p.y, hwnd, NULL);


   DestroyMenu(hmenu);
}

/*=========================================================================
 * TownEdit class
 */


OBEdit::OBEdit(MWEditInterface* editControl, CampaignData* cData, MapWindow* p, const StackedUnitList* list, const PixelPoint& pos) :
   d_editControl(editControl)
{
   campData = cData;
   parent = p;

   unitList = list;
   unitID = 0;

   const Armies* armies = &campData->getArmies();

   cpi = NoCommandPosition;
   iLeader = NoLeader;
   cp = 0;
   leader = 0;
   president = NoCommandPosition;
   isp = NoStrengthPoint;
   sp = 0;
   isPresident = False;
   created = False;

   ICommandPosition iCommand = NoCommandPosition;
   if(unitList->unitCount() == 0)
   {
     side = 0;
   }
   else
    {
     iCommand = constCPtoCP(unitList->getUnit((UnitListID)unitID));
     ASSERT(iCommand != NoCommandPosition);
     const CommandPosition* cp = armies->getCommand(iCommand);
     side = cp->getSide();
   }

   // Nation* nation = armies->getNation(side);
   // president = nation->getPresident();
   president = armies->getPresident(side);
   ASSERT(president != NoCommandPosition);
   resetCurrentUnit(unitList->unitCount() > 0 ? iCommand : president);

   spListID = 0;
   iTown = NoTown;
   attachTo = NoCommandPosition;

   attachingUnit = False;
   attachingLeader = False;
   attachingSP = False;
   detaching = False;

   independent = False;
   newSP = False;
   newLeader = False;

// rank = Rank_Army;

   POINT pt = pos;
    ClientToScreen(parent->getHWND(), &pt);
   position = pt;

   pages = new OBEditPage*[EditPages_HowMany];

   for(int i = 0; i < EditPages_HowMany; i++)
      pages[i] = 0;
   tabHwnd = NULL;                  // Tab Dialog handle
   curPage = 0;
   SetRectEmpty(&tdRect);

   obImages.loadBitmap(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_OBICONS), OBI_CX, RGB(255, 255, 255));

   HWND dhwnd = createDialog(obEditDialogName, parent->getHWND());
   ASSERT(dhwnd != NULL);

   ASSERT(cpi != NoCommandPosition);
// resetCurrentUnit(cpi);
   addSPToList();
}

OBEdit::~OBEdit()
{
   if(pages)
   {
      delete[] pages;
      pages = 0;
   }
}

TrackMode OBEdit::onSelect(TrackMode mode, const MapSelect& info)
{
   if(mode == MWTM_Town)
   {
      if(info.selectedTown != NoTown)
      {
         // ASSERT(info.selectedTown != NoTown);

         ASSERT(cp != 0);

         if(cp != 0)
         {
            if(cpi == president)
            {
               iTown = info.selectedTown;
            }
            else
               cp->setPosition(info.selectedTown);

            const Town& bestTown = campData->getTown(info.selectedTown);
            const Province& prov = campData->getProvince(bestTown.getProvince());
            MemPtr<char> buf(200);
            wsprintf(buf.get(), "%s, %s", bestTown.getName(), prov.getName());
            SetDlgItemText(pages[1]->getHWND(), IDOBE_NEWTOWN, buf.get());
            SetDlgItemText(pages[1]->getHWND(), IDOBE_CURTOWN, buf.get());

            ShowWindow(getHWND(), SW_SHOW);
         }
         }

      return MWTM_None;
   }
#ifdef DEBUG
   else if(mode != MWTM_None)
      FORCEASSERT("Unexpected TrackMode");
#endif

   return mode;
}


BOOL OBEdit::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;

      case WM_CLOSE:
             HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
         break;

      case WM_NOTIFY:
         return HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);

      case WM_ACTIVATE:
         HANDLE_WM_ACTIVATE(hWnd, wParam, lParam, onActivate);
         break;

      case WM_CONTEXTMENU:
         HANDLE_WM_CONTEXTMENU(hWnd, wParam, lParam, onContextMenu);
         break;

      case WM_HELP:
         HANDLE_WM_HELP(hWnd, wParam, lParam, onHelp);
         break;

      case WM_MEASUREITEM:
         HANDLE_WM_MEASUREITEM(hWnd, wParam, lParam, onMeasureItem);
         break;

      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

static const DWORD ids[] = {

    /*
     * from main dialog
     */

    IDOBE_UNITS_NAME,        IDH_OBE_UnitsListBox,
    IDOBE_OK,                IDH_OBE_OK,
    IDOBE_CANCEL,            IDH_OBE_Cancel,

    /*
     * from units tab
     */

    IDOBE_UNITS_RANKLEVEL,   IDH_OBE_UnitRankLevel,
    IDOBE_UNITS_SIDE,        IDH_OBE_UnitSide,
    IDOBE_UNITS_NATIONALITY, IDH_OBE_UnitNation,
    IDOBE_UNITS_SPLIST,      IDH_OBE_SPList,
    IDOBE_SP_CURRENT,        IDH_OBE_UnitType,
    IDOBE_SP_INFANTRY,       IDH_OBE_BasicTypes,
    IDOBE_SP_CAVALRY,        IDH_OBE_BasicTypes,
    IDOBE_SP_ARTILLERY,      IDH_OBE_BasicTypes,
    IDOBE_SP_OTHER,          IDH_OBE_BasicTypes,
    IDOBE_UNITS_UNIT,        IDH_OBE_EditSP,
    IDOBE_NEW,               IDH_OBE_NewSP,

    /*
     * from position tab
     */

    IDOBE_CURTOWN,           IDH_OBE_CurrentPosition,
    IDOBE_NEWTOWN,           IDH_OBE_NewPosition,
    IDOBE_UNITS_NEWTOWN,     IDH_OBE_TownList,
    IDOBE_MOVE,              IDH_OBE_Move,

    /*
     * from values tab
     */

    IDOBE_UNIT_NAME,            IDH_OBE_UnitName,
    IDOBE_MORALE,               IDH_OBE_Morale,
    IDOBE_FATIGUE,              IDH_OBE_Fatigue,
    IDOBE_SUPPLY,               IDH_OBE_Supply,
    IDOBE_LEADER_CURRENT,       IDH_OBE_LeaderName,
    IDOBE_LEADER_NATION,        IDH_OBE_LeaderNation,
    IDOBE_LEADER_INIT,          IDH_OBE_Initiative,
    IDOBE_LEADER_STAFF,         IDH_OBE_Staff,
    IDOBE_LEADER_SUB,           IDH_OBE_Subordination,
    IDOBE_LEADER_AGGRESS,       IDH_OBE_Aggression,
    IDOBE_LEADER_CHARISMA,      IDH_OBE_Charisma,
    IDOBE_LEADER_TACTICAL,      IDH_OBE_Ability,
    IDOBE_LEADER_RADIUS,        IDH_OBE_Radius,
    // IDOBE_LEADER_RANK,          IDH_OBE_RankRating,
    IDOBE_LEADER_HEALTH,        IDH_OBE_Health,
      IDOBE_LEADER_RANKLEVEL,     IDH_OBE_RankLevel,
    IDOBE_LEADER_HELPID,        IDH_EDIT_HelpIDEdit,
    IDOBE_LEADER_DEFAULTHELPID, IDH_EDIT_DefaultHelpID,

    0, 0
};

void OBEdit::onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y)
{
//  ListBox box(getHWND(), IDOBE_UNITS_NAME);
//  ListBox spBox(pages[EditAllUnitsPage]->getHWND(), IDOBE_UNITS_SPLIST);
//  if(hwndCtl != box.getHWND() && hwndCtl != spBox.getHWND())
//    WinHelp(hwndCtl, "help\\Wg95Help.hlp", HELP_CONTEXTMENU, (DWORD)(LPVOID)ids);
}

BOOL OBEdit::onHelp(HWND hwnd, LPHELPINFO lparam)
{
  LPHELPINFO lphi = (LPHELPINFO)lparam;
  if (lphi->iContextType == HELPINFO_WINDOW)   // must be for a control
  {
//    WinHelp ((HWND)lphi->hItemHandle, "help\\Wg95Help.hlp", HELP_WM_HELP, (DWORD)(LPVOID)ids);
  }
  return TRUE;
}

void OBEdit::initControls()
{
   addStackedUnitsToList();
}

BOOL OBEdit::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
#ifdef DEBUG
   debugLog("OBEdit::onInitDialog()\n");
#endif

   {
      ListBox box(getHWND(), IDOBE_UNITS_NAME);
      lpfnListWndProc = (WNDPROC) SetWindowLong(box.getHWND(), GWL_WNDPROC, (DWORD) subClassProc);
      SetWindowLong(box.getHWND(), GWL_USERDATA, (DWORD)this);
   }
   {
      ListBox box(getHWND(), IDOBE_LEADERS_LIST);
      lpfnListWndProc2 = (WNDPROC) SetWindowLong(box.getHWND(), GWL_WNDPROC, (DWORD) subClassProc2);
      SetWindowLong(box.getHWND(), GWL_USERDATA, (DWORD)this);
   }

   /*
    * Setup the tabbled dialog
    */

   /*
    * Set up a minimum size (this is in dialog units)
    */

   RECT rcTab;
   rcTab.left = 0;
   rcTab.top = 0;
    rcTab.right = 184;
   rcTab.bottom = 140;

   pages[EditAllUnitsPage] = new OBEditAllUnits(this);
   pages[EditPositionPage] = new OBEditPosition(this);
   pages[EditUnitPage] =     new OBEditUnit(this);
   pages[EditLeaderPage] =   new OBEditLeader(this);

   for(int i = 0; i < EditPages_HowMany; i++)
   {
      DLGTEMPLATE* dialog = pages[i]->getDialog();

      ASSERT(dialog != NULL);

      /*
       * Adjust for maximum size
       */

      if(dialog->cx > rcTab.right)
         rcTab.right = dialog->cx;
      if(dialog->cy > rcTab.bottom)
         rcTab.bottom = dialog->cy;
   }

#ifdef DEBUG
   debugLog("Largest Dialog (Dialog Units): %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

    // Get the position of the box (convert from dialog units)

   LONG dbUnits = GetDialogBaseUnits();
   LONG dbX = LOWORD(dbUnits);
   LONG dbY = HIWORD(dbUnits);
   LONG xMargin = (135 * dbX) / 4;
   LONG yTop = (20 * dbY) / 8;        //was20, 33
   LONG yMargin = (4 * dbY) / 8;      //was4

   // Convert to pixel coordinates

   MapDialogRect(hwnd, &rcTab);

#ifdef DEBUG
   debugLog("Largest Dialog (Pixels): %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   /*
    * Create a tabbed window
    * Don't be concerened with the size, because that will
    * be set up in a little while, but we need a handle to
    * a tabbed control to use AdjustRect
    *
    * Do need to set up width, so that AdjustRect doesn't think
    * it needs to do several rows of buttons
    */

   tabHwnd = CreateWindow(
         WC_TABCONTROL, "",
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
         TCS_TOOLTIPS | TCS_RIGHTJUSTIFY | TCS_MULTILINE,
      0, 0, rcTab.right, 100,
      hwnd,
      (HMENU) IDOBE_TABBED,
      APP::instance(),
      NULL);

   ASSERT(tabHwnd != NULL);

   debugLog("TabHwnd = %08lx\n", (ULONG) tabHwnd);

   /*
    * Set up the tabbed titles
    * If possible try and get the title from the Dialog's CAPTION
    *
    * This must be done before using AdjustRect
    */

   TC_ITEM tie;
   tie.mask = TCIF_TEXT | TCIF_IMAGE;
   tie.iImage = -1;

   for(i = 0; i < EditPages_HowMany; i++)
   {
      tie.pszText = (char*) pages[i]->getTitle();

      TabCtrl_InsertItem(tabHwnd, i, &tie);
    }

   /*
    * Convert coordinates to pixel coordinates
    * and Move the window
    */

   // Work out complete size with tabs, and shift to desired location

   TabCtrl_AdjustRect(tabHwnd, TRUE, &rcTab);
#ifdef DEBUG
   debugLog("Tab Size: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif
   OffsetRect(&rcTab, xMargin - rcTab.left, yTop - rcTab.top);
#ifdef DEBUG
   debugLog("Offset Tab: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   // Move tabbed Window

   SetWindowPos(tabHwnd, NULL,
      rcTab.left, rcTab.top,
      rcTab.right - rcTab.left, rcTab.bottom - rcTab.top,
      // rcTab.right - rcTab.left, 50,
      SWP_NOZORDER);

   // Get display area

   CopyRect(&tdRect, &rcTab);
   TabCtrl_AdjustRect(tabHwnd, FALSE, &tdRect);    // Get display area
#ifdef DEBUG
   debugLog("Display Area: %ld,%ld,%ld,%ld\n",
      tdRect.left, tdRect.top, tdRect.right, tdRect.bottom);
#endif

   /*
    * Move the lower buttons (to rcTab.bottom + a bit)
    */

   RECT rcButton;
   LONG x = rcTab.right-2;
   LONG y = rcTab.bottom + yMargin;

   HWND hwndButton = GetDlgItem(hwnd, IDOBE_OK);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   x -= rcButton.right - rcButton.left;
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

#ifdef DEBUG
   debugLog("OK: %ld,%ld,%ld,%ld\n",
      x, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif


   hwndButton = GetDlgItem(hwnd, IDOBE_CANCEL);
    ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   x -= rcButton.right - rcButton.left + 2;
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#ifdef DEBUG
   debugLog("Cancel: %ld,%ld,%ld,%ld\n",
      x, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif

#if 0
   hwndButton = GetDlgItem(hwnd, IDOBE_DELETE);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, xMargin, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#ifdef DEBUG
   debugLog("Delete: %ld,%ld,%ld,%ld\n",
      xMargin, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif

   hwndButton = GetDlgItem(hwnd, IDOBE_NEW);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, (xMargin*2) + rcButton.right-rcButton.left, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
#ifdef DEBUG
   debugLog("Delete: %ld,%ld,%ld,%ld\n",
      (xMargin * 2) + rcButton.right-rcButton.left, y, rcButton.right-rcButton.left, rcButton.bottom-rcButton.top);
#endif
#endif


   /*
    * Adjust size of overall dialogue box
    *
    * Would be a good idea to adjust coordinates
    * so that box is on screen.
    *
    * e.g. if off right, adjust coords to be left of given position
    * if off bottom, adjust to be above.
    */

   rcTab.bottom = y + yMargin + rcButton.bottom - rcButton.top;
   rcTab.bottom += GetSystemMetrics(SM_CYCAPTION);
   rcTab.bottom += GetSystemMetrics(SM_CYDLGFRAME) * 2;
   // rcTab.bottom -= rcTab.top;

   rcTab.right += xMargin + 5; // * 2;
   rcTab.right += GetSystemMetrics(SM_CXDLGFRAME) * 2;
   rcTab.right -= rcTab.left;

#ifdef DEBUG
   debugLog("Overall Window: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   SetWindowPos(hwnd, NULL,
      position.getX(),
      position.getY(),
      rcTab.right,
         rcTab.bottom,
      SWP_NOZORDER);

   /*
    * Initialise pages
    */

   for(i = 0; i < EditPages_HowMany; i++)
      pages[i]->create();

   /*
    * For testing: create an initial dialog
    */

   onSelChanged();

   /*
    * End of tabbed dialog setup
    * Now get on with positioning ourselves and setting the controls
    */

   /*===============================================================
    * Set the position
    * Ought to do a bit of checking to see if it will fit on screen
    */


   initControls();
   created = True;

   return TRUE;
}

void OBEdit::onDestroy(HWND hwnd)
{
   d_editControl->editDestroyed(this);
   notifyParent();
}

void OBEdit::onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   ListBox list(getHWND(), IDOBE_UNITS_NAME);
   ListBox sp(pages[EditAllUnitsPage]->getHWND(), IDOBE_UNITS_SPLIST);
   ListBox leaders(getHWND(), IDOBE_LEADERS_LIST);
   PixelPoint p(x, y);
   if(hwnd == list.getHWND())
   {
      int index = list.getItemFromPoint(x, y);

      if(index < list.getNItems())
      {
         list.set(index);

         // onStackedUnits((ICommandPosition)list.getValue());
         onStackedUnits(lparamToCP(list.getValue()));

         obEditPopupMenu(getHWND(), p, MENU_OBEDITPOPUP);
      }
    }
   else if(hwnd == sp.getHWND())
   {
      int index = sp.getItemFromPoint(x, y);
      if(index < sp.getNItems())
      {
         sp.set(index);

         onSP();
         obEditPopupMenu(pages[EditAllUnitsPage]->getHWND(), p, MENU_DELETESPPOPUP);
      }
   }
   else if(hwnd == leaders.getHWND())
   {
      int index = leaders.getItemFromPoint(x, y);
      if(index < leaders.getNItems())
      {
         leaders.set(index);

//         onSP();
         obEditPopupMenu(getHWND(), p, MENU_OBEDITPOPUP2);
      }
   }
}

/*
 * All the child handlers are done here as well... to make moving
 * things about easier
 */

void OBEdit::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{

   switch(id)
   {
      case IDOBE_OK:
         onOK();
         break;

      case IDOBE_CANCEL:

         if(checkLeaderHelpID())
         {
           parent->requestRedraw(TRUE);
           DestroyWindow(hwnd);
         }
         break;

      case IDM_OBE_DUPLICATE:
         duplicateOrganization();
         break;

      case IDOBE_LEADERS_LIST:
      {
         updateValues();
         resetCurrentUnit(president);
         ListBox list(hwndCtl);
         // ICommandPosition newCpi = (ICommandPosition)list.getValue();
             iLeader = lparamToLeader(list.getValue());
         leader = iLeader;
         pages[EditLeaderPage]->initControls();
         break;
      }

      case IDM_OBE_NEWINACTIVELEADER:
         createNewLeader(side, False);
         pages[EditLeaderPage]->initControls();
//         pages[EditLeaderPage]->updateValues();
         break;

      case IDM_OBE_DELETEINDLEADER:
      {
        updateValues();
        resetCurrentUnit(president);
        ListBox list(getHWND(), IDOBE_LEADERS_LIST);
        // ICommandPosition newCpi = (ICommandPosition)list.getValue();
        iLeader = lparamToLeader(list.getValue());
        leader = iLeader;
        pages[EditLeaderPage]->initControls();
        deleteLeader();
        break;
      }

      case IDM_OBE_ATTACHUNIT:
        attachingUnit = True;
        break;

         case IDM_OBE_ATTACHLEADER:
        attachingLeader = True;
        break;

      case IDM_OBE_ATTACHSP:
        attachingSP = True;
        break;


      case IDM_OBE_CREATEARMY:
        attachTo = cpi;
        createNewUnit(side, Rank_Army);
        break;


      case IDM_OBE_CREATEARMYWING:
#if 0
        attachTo = cpi;
        createNewUnit(side, Rank_ArmyWing);
#endif
        break;

      case IDM_OBE_CREATECORPS:
        attachTo = cpi;
        createNewUnit(side, Rank_Corps);
        break;

      case IDM_OBE_CREATEDIVISION:
        attachTo = cpi;
            createNewUnit(side, Rank_Division);
        break;

      case IDM_OBE_CREATELEADER:
        attachTo = cpi;
        createNewLeader(side);
        pages[EditLeaderPage]->initControls();
        break;

      case IDOBE_NEW:
      case IDM_OBE_CREATESP:
        attachTo = cpi;
        createNewSP();
        break;

      case IDM_OBE_DELETEUNITALL:
        deleteUnits();
        addStackedUnitsToList();
        break;

      case IDM_OBE_DELETEUNIT:
        deleteUnit();
        addStackedUnitsToList();
        break;

      case IDM_OBE_DELETELEADER:
        deleteLeader();
        break;

         case IDM_OBE_DELETESP:
        deleteSP();
        break;

      case IDOBE_MOVE:
        ShowWindow(hWnd, SW_HIDE);
        // parent->wantPosition(this);
        d_editControl->setTrackMode(MWTM_Town); // wantTown(this);
        break;

      case IDOBE_UNITS_SPLIST:
      {
         if(codeNotify == LBN_SELCHANGE)
         {
           onSP();
         }
         break;
      }

      case IDOBE_UNITS_NAME:
        if(codeNotify == LBN_SELCHANGE)
        {
          ListBox list(hwndCtl);
          // ICommandPosition newCpi = (ICommandPosition)list.getValue();
          ICommandPosition newCpi = lparamToCP(list.getValue());
          ASSERT(unitID < list.getNItems());
          ASSERT(president != NoCommandPosition);

//        int index = list.getTop();
               if(attachingUnit)
          {
            attachTo = newCpi;
            if(attachTo == president)
              independent = True;
            attachUnit();
            list.set(unitID);
//          list.setTop(index);
          }
          else if(attachingLeader)
          {
            attachTo = newCpi;
            if(attachTo == president)
              independent = True;
            attachLeader();
            list.set(unitID);
//          list.setTop(index);
          }
          else if(attachingSP)
          {
            if(sp)
            {
              attachTo = newCpi;
              if(attachTo == president)
                independent = True;
              attachSP();
              list.set(unitID);
            }
          }
               else
          {
            unitID = list.getIndex();
            onStackedUnits(newCpi);
          }
        }
        break;

      case IDOBE_UNITS_RANKLEVEL:
      {
         if(codeNotify == CBN_SELCHANGE)
         {
           rank = (RankEnum)getComboValue(hwndCtl);
           if(cp)
             cp->setRank(rank);

           addStackedUnitsToList();

         }
         break;
      }
      case IDOBE_UNITS_SIDE:
      {
         if(codeNotify == CBN_SELCHANGE)
         {
           side = (Side)getComboValue(hwndCtl);
           setSides();
         }
         break;
         }

      case IDOBE_SP_INFANTRY:
        if(codeNotify == BN_CLICKED)
          addUnitTypeCombo(BasicUnitType::Infantry);

        break;

      case IDOBE_SP_CAVALRY:
        if(codeNotify == BN_CLICKED)
          addUnitTypeCombo(BasicUnitType::Cavalry);

        break;

      case IDOBE_SP_ARTILLERY:
        if(codeNotify == BN_CLICKED)
          addUnitTypeCombo(BasicUnitType::Artillery);

        break;

      case IDOBE_SP_OTHER:
        if(codeNotify == BN_CLICKED)
          addUnitTypeCombo(BasicUnitType::Special);

        break;

      case IDOBE_SP_CURRENT:
      {
         if(codeNotify == CBN_SELCHANGE)
             {
           Button button(pages[EditAllUnitsPage]->getHWND(), IDOBE_UNITS_UNIT);

           if(button.getCheck())
           {
             ASSERT(sp != 0);
             ASSERT(isp != NoStrengthPoint);
             ComboBox combo(hwndCtl);
             sp->setUnitType((UnitType)combo.getValue());

             addSPToList();
           }
         }
         break;
      }

      case IDOBE_UNIT_NAME:
      case IDOBE_LEADER_CURRENT:
      {
        if(codeNotify == EN_CHANGE)
        {
          char buf[200];
          if(!isPresident)
          {
            ASSERT(cp != 0);
            ASSERT(leader != 0);
            if(id == IDOBE_UNIT_NAME)
            {
              GetDlgItemText(pages[EditUnitPage]->getHWND(), id, buf, 200);
                     cp->setName(buf);
            }
            else if(id == IDOBE_LEADER_CURRENT)
            {
              GetDlgItemText(pages[EditLeaderPage]->getHWND(), id, buf, 200);
              leader->setName(buf);
            }

            ListBox list(hWnd, IDOBE_UNITS_NAME);
            RECT r;
            SendMessage(list.getHWND(), LB_GETITEMRECT, list.getIndex(), (LPARAM)&r);
            redrawList(r, IDOBE_UNITS_NAME);
          }
          else
          {
            if(id == IDOBE_LEADER_CURRENT)
            {
              GetDlgItemText(pages[EditLeaderPage]->getHWND(), id, buf, 200);
              leader->setName(buf);
            }
            ListBox list(hWnd, IDOBE_LEADERS_LIST);
            RECT r;

            SendMessage(list.getHWND(), LB_GETITEMRECT, list.getIndex(), (LPARAM)&r);
            redrawList(r, IDOBE_LEADERS_LIST);
          }
        }
        break;
      }

      case IDOBE_LEADER_HELPID:
      {
         if(codeNotify == EN_CHANGE)
         {
           ASSERT(cp != 0);
           ASSERT(leader != 0);

           HelpID helpID = (HelpID)GetDlgItemInt(pages[EditLeaderPage]->getHWND(), id, NULL, FALSE);
           leader->setHelpID(helpID);
         }

         break;
      }

      case IDOBE_LEADER_NATION:
      {
        if(codeNotify == CBN_SELCHANGE)
        {
          ASSERT(leader != 0);
          if(leader)
          {
            ComboBox combo(hwndCtl);
            leader->setNation((Nationality)combo.getValue());

            if(cp->getSPEntry() == NoStrengthPoint)
              cp->setNation(leader->getNation());

            pages[EditAllUnitsPage]->initControls();
                  pages[EditLeaderPage]->initControls();
            addSPToList();
          }
        }

        break;
      }

      case IDOBE_UNITS_NATIONALITY:
      {
        if(codeNotify == CBN_SELCHANGE)
        {
          ASSERT(cp != 0);
          if(cp)
          {
            ComboBox combo(hwndCtl);
            cp->setNation((Nationality)combo.getValue());
            ASSERT(leader != 0);
            addSPToList();

            Armies* armies = &campaignData->getArmies();

            if(cp->getSPEntry() == NoStrengthPoint ||
               !armies->canLeaderTakeControl(iLeader, cpi))

            {
              leader->setNation(cp->getNation());
            }
            pages[EditLeaderPage]->initControls();
                  pages[EditAllUnitsPage]->initControls();
          }
        }

        break;
      }

      case IDOBE_SETMORALE:
      {
         // Recalculate units morale...

         const Armies* army = &campaignData->getArmies();
         Attribute morale = MoraleUtility::calcMorale(army, cp);
         cp->setMorale(morale);
         SendDlgItemMessage(pages[EditUnitPage]->getHWND(), IDOBE_MORALESPIN, UDM_SETPOS, 0, MAKELONG(cp->getMorale(), 0));
         break;
      }

      case IDOBE_LEADER_DEFAULTHELPID:
      {
        if(leader)
        {
          SetDlgItemInt(pages[EditLeaderPage]->getHWND(), IDOBE_LEADER_HELPID, NoHelpID, FALSE);
          leader->setHelpID(NoHelpID);
        }
        break;
      }

      /*
          * Central HQ flag set
       */

      case IDOBE_LEADER_SHQ:
         if(codeNotify == BN_CLICKED)
         {
            if( (iLeader != NoLeader) && leader && !leader->isSupremeLeader())
            {
               Armies* armies = &campaignData->getArmies();
               armies->setSupremeLeader(side, iLeader);

               Button shqButton(hwndCtl);
               shqButton.setCheck(leader->isSupremeLeader());
            }
         }
         break;

      /*
       * Set Leader Values to default
       */

      case IDOBE_LEADER_DEFAULT:
        if(codeNotify == BN_CLICKED)
        {
            if( (iLeader != NoLeader) && leader)
            {
              leader->setDefault(0);
              pages[EditLeaderPage]->initControls();
            }
            }
        break;

      case IDOBE_UNIT_ORDERS:
      {
        if(codeNotify == CBN_SELCHANGE)
        {
          if(cpi != NoCommandPosition && cp)
          {
            ComboBox b(hwndCtl);

            if(canGiveOrder(b.getValue()))
            {
              cp->startOrder(static_cast<Orders::Type::Value>(b.getValue()));
            }
            else
            {
              b.setValue(Orders::Type::Hold);
              MessageBox(getHWND(), "Unit is not at a Garrison Town!", "Error!", MB_OK | MB_ICONWARNING);
            }
          }
        }
        break;
      }

      case IDOBE_UNIT_AGGRESSION:
      {
        if(codeNotify == CBN_SELCHANGE)
        {
               if(cpi != NoCommandPosition && cp)
          {
            ComboBox b(hwndCtl);
            cp->startAggression(static_cast<Orders::Aggression::Value>(b.getValue()));
          }
        }
        break;
      }

#ifdef DEBUG
      default:
         debugLog("OBEdit::onCommand(), unknown id %d\n", id);
#endif
   }

}

void OBEdit::onOK()
{
  updateValues();

  if(checkLeaderHelpID())
  {
    parent->requestRedraw(True);
    campData->setChanged();
    DestroyWindow(getHWND());
  }
}

/*
 * Check leader help id value to make sure it is in range
 *
 * Each help id must be unique. Leader IDs will for now be in the range of 1000-1199
 */

Boolean OBEdit::checkLeaderHelpID()
{

  const UINT minRange = 1000;
  const UINT maxRange = 1199;

  static const char msgOutOfRange[] = "Leader's Help ID must be in the range of 1000 and 1199, or press default-ID button";

  if(leader)
  {
    HelpID helpID = (HelpID)GetDlgItemInt(pages[EditLeaderPage]->getHWND(), IDOBE_LEADER_HELPID, NULL, FALSE);

    if( (helpID != NoHelpID) && (helpID < minRange || helpID > maxRange) )
    {
      int result = MessageBox(getHWND(), msgOutOfRange, "Out of Range", MB_OK);
      ASSERT(result == IDOK);
      return False;
    }

    /*
     * Go through all leaders and make sure Help ID is not duplicated
     */

      const Armies* armies = &campData->getArmies();

    static const char msgIDRepeated[] = "Leader's HelpID(%d) has already been defined. Choose a different value in the range of 1000 to 1199, or press default-ID button";

    ConstCPIter uIter(armies);
    while(uIter.next())
    {
      const CommandPosition* cp = uIter.currentCommand();

      ASSERT(cp->getLeader() != NoLeader);
      const Leader* leader = campData->getLeader(cp->getLeader());

      if( (leader->getHelpID() != NoHelpID) && (helpID == leader->getHelpID() && iLeader != cp->getLeader()) )
      {
        char buf[200];
        wsprintf(buf, msgIDRepeated, helpID);

        int result = MessageBox(getHWND(), buf, "ID Duplicated", MB_OK);
        ASSERT(result == IDOK);
        return False;
      }
    }
  }

  return True;
}

void OBEdit::onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem)
{
   lpMeasureItem->itemHeight = 10;
}


void OBEdit::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
  switch(lpDrawItem->CtlID)
  {
    case IDOBE_UNITS_NAME:
    {
      ListBox listBox(lpDrawItem->hwndItem);
      if(listBox.getNItems() > 0)
      {
        const Armies* armies = &campData->getArmies();
        // ICommandPosition cpi = (ICommandPosition)listBox.getValue(lpDrawItem->itemID);
        ICommandPosition cpi = lparamToCP(listBox.getValue(lpDrawItem->itemID));
        const CommandPosition* cp = armies->getCommand(cpi);
        const Leader* leader = 0;
        if(cp->getLeader() != NoLeader)
          leader = armies->getLeader(cp->getLeader());

        Fonts font;
        font.setFont(lpDrawItem->hDC, 13, 5);

        RankEnum rank = cp->getRankEnum();

        int nParents = 0;

        ASSERT(president != 0);
            if(cpi != president)
        {
          ASSERT(cp->getParent() != NoCommandPosition);
          const CommandPosition* parent = cp;
          while(parent->getParent() != president)
          {
            ASSERT(parent->getParent() != NoCommandPosition);
            parent = armies->getCommand(parent->getParent());
            nParents++;
          }
        }

        int x = 2 + (nParents * 13);

        int offset = rank <= Rank_ArmyGroup ? FI_ArmyGroup :
                     rank <= Rank_Army ? FI_Army :
                     rank <= Rank_Corps ? FI_Corps :
                     FI_Division;

        TEXTMETRIC tm;
        GetTextMetrics(lpDrawItem->hDC, &tm);
        int y = (lpDrawItem->rcItem.bottom + lpDrawItem->rcItem.top -
                tm.tmHeight) / 2;

        COLORREF oldBkColor = GetBkColor(lpDrawItem->hDC);
        COLORREF oldTextColor = GetTextColor(lpDrawItem->hDC);
        if (lpDrawItem->itemState & ODS_SELECTED)
        {
          DrawFocusRect(lpDrawItem->hDC, &lpDrawItem->rcItem);
               SetBkColor(lpDrawItem->hDC, scenario->getSideColour(side));
          SetTextColor(lpDrawItem->hDC, RGB(255, 255, 255));
        }

        char buf[200];
        wsprintf(buf, "%s, %s",
                cp->isRealUnit() ? cp->getName() : leader ? leader->getName() : "Unknown",
                cp->isRealUnit() && leader ? leader->getName() : "");

        ExtTextOut(lpDrawItem->hDC, x + OBI_CX + 3, y,
                ETO_CLIPPED | ETO_OPAQUE, &lpDrawItem->rcItem,
                buf, lstrlen(buf), NULL);


        ImageLibrary* il = scenario->getNationFlag(cp->getNation(), True);
        il->blit(lpDrawItem->hDC, (UWORD)offset, x, y);

        SetBkColor(lpDrawItem->hDC, oldBkColor);
        SetTextColor(lpDrawItem->hDC, oldTextColor);

        font.deleteFont();

      }
      break;
    }
    case IDOBE_LEADERS_LIST:
    {
      ListBox listBox(lpDrawItem->hwndItem);
      if(listBox.getNItems() > 0)
         {
        const Armies* armies = &campData->getArmies();
        // ICommandPosition cpi = (ICommandPosition)listBox.getValue(lpDrawItem->itemID);
        ILeader leader = lparamToLeader(listBox.getValue(lpDrawItem->itemID));
        const CommandPosition* cp = armies->getCommand(cpi);

        Fonts font;
        font.setFont(lpDrawItem->hDC, 13, 5);

        RankEnum rank = leader->getRankLevel().getRankEnum();

        ASSERT(president != 0);

        int x = 2;
        int offset = rank <= Rank_ArmyGroup ? FI_ArmyGroup :
                     rank <= Rank_Army ? FI_Army :
                     rank <= Rank_Corps ? FI_Corps :
                     FI_Division;

        TEXTMETRIC tm;
        GetTextMetrics(lpDrawItem->hDC, &tm);
        int y = (lpDrawItem->rcItem.bottom + lpDrawItem->rcItem.top -
                tm.tmHeight) / 2;

        COLORREF oldBkColor = GetBkColor(lpDrawItem->hDC);
        COLORREF oldTextColor = GetTextColor(lpDrawItem->hDC);
        if (lpDrawItem->itemState & ODS_SELECTED)
        {
          DrawFocusRect(lpDrawItem->hDC, &lpDrawItem->rcItem);
               SetBkColor(lpDrawItem->hDC, scenario->getSideColour(side));
          SetTextColor(lpDrawItem->hDC, RGB(255, 255, 255));
        }

        char buf[100];
        lstrcpy(buf, leader->getName());
        bool active = True;
        IndependentLeaderList& llist = campData->getArmies().independentLeaders();
        SListIter<IndependentLeader> iter(&llist);
        while(++iter)
        {
           if(iter.current()->leader() == leader)
           {
             active = iter.current()->active();
             break;
           }
        }

        if(!active)
           lstrcat(buf, " -- Inactive");

//        SendMessage(listBox.getHWND(), LB_GETTEXT, lpDrawItem->itemID, (LPARAM)buf);
//        GetDlgItemText(listBox.getHWND(), buf, 100);
        ExtTextOut(lpDrawItem->hDC, x + OBI_CX + 3, y,
                ETO_CLIPPED | ETO_OPAQUE, &lpDrawItem->rcItem,
                buf, lstrlen(buf), NULL);


        ImageLibrary* il = scenario->getNationFlag(cp->getNation(), True);
            il->blit(lpDrawItem->hDC, (UWORD)offset, x, y);

        SetBkColor(lpDrawItem->hDC, oldBkColor);
        SetTextColor(lpDrawItem->hDC, oldTextColor);

        font.deleteFont();

      }
      break;
    }
  }
}


void OBEdit::onClose(HWND hwnd)
{
   onCommand(hwnd, IDOBE_CANCEL, 0, 0);
}

void OBEdit::kill()
{
   onClose(getHWND());
}

void OBEdit::onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized)
{
}

LRESULT OBEdit::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{
#ifdef DEBUG
   debugLog("TownEdit::onNotify(%08lx %d %08lx %d %d)\n",
      (ULONG) hWnd,
      (int) id,
      (ULONG) lpNMHDR->hwndFrom,
      (int) lpNMHDR->idFrom,
      (int) lpNMHDR->code);
#endif

   // Assume its from the tabbed dialog

   if(lpNMHDR->idFrom == IDOBE_TABBED)
   {
      switch(lpNMHDR->code)
      {
      case TCN_SELCHANGING:
         return FALSE;

      case TCN_SELCHANGE:
         onSelChanged();
         break;
      }
   }

   return TRUE;
}

void OBEdit::onSelChanged()
{
   int iSel = TabCtrl_GetCurSel(tabHwnd);
   if(curPage != iSel)
      pages[curPage]->show(false);   // disable();

   pages[iSel]->show(true); // enable();
   curPage = iSel;
}


void OBEdit::createNewUnit(Side side, RankEnum rank)
{
   Armies* armies = &campData->getArmies();

   TownList &tl = campData->getTowns();
   ITown town = (ITown) random(0, tl.entries());
   ITown iTown = town;

   ASSERT(president != NoCommandPosition);
   cpi = armies->createChildCP(president);
   ASSERT(cpi != NoCommandPosition);

#ifdef DEBUG
#if !defined(EDITOR)
   if(logWindow)
   {
      logWindow->printf("New cpi = %d", (int)cpi->getSelf());
      Town t = tl[iTown];
      logWindow->printf("ITown = %d", (int)iTown);
         logWindow->printf("Location = %s", t.getName());
   }
#endif
#endif
   cp = armies->getCommand(cpi);

#ifdef DEBUG
#if !defined(EDITOR)
   if(logWindow)
      logWindow->printf("side = %s", scenario->getSideName(side));
#endif
#endif

   cp->setSide(side);
   cp->setPosition(town);
   cp->setRank(rank);
   armies->makeUnitName(cpi, rank);
   cp->setNation(scenario->getDefaultNation(side));
   cp->isActive(true);

   iLeader = armies->createLeader(side, cp->getNation(), rank, True);
   ASSERT(iLeader != NoLeader);
   leader = armies->getLeader(iLeader);

   armies->attachLeader(cpi, iLeader);
   leader = armies->getLeader(iLeader);

   if(attachTo != president)
   {
       attachingUnit = True;
     attachUnit();
   }
   else
   {
     addStackedUnitsToList();

     // Bodge because resetCurrentUnit does a check for cpi being different
     ICommandPosition newCPI = cpi;
     cpi = NoCommandPosition;
     resetCurrentUnit(newCPI);
   }
}

void OBEdit::createNewLeader(Side side, bool active)
{
  Armies* armies = &campData->getArmies();
  iLeader = armies->createLeader(side, scenario->getDefaultNation(side), Rank_Division, True);
  ASSERT(iLeader != NoLeader);
  leader = iLeader;

  if(attachTo != president && cpi != president)
  {
    cpi = leader->getCommand();

    ASSERT(cpi != NoCommandPosition);
    cp = armies->getCommand(cpi);
    attachingLeader = True;
    attachLeader();
   }
  else
  {
    CampaignPosition pos;
    TownList& tl = campData->getTowns();
    ITown iTown = (ITown) random(0, tl.entries());
    Town town = tl[iTown];
    pos.setPosition(iTown);
    CampaignArmy_Util::makeIndependentLeader(campData, iLeader, pos, active);
    addStackedUnitsToList();
  }

#ifdef DEBUG
#if !defined(EDITOR)
    if(logWindow)
       logWindow->printf("New Leader ID = %d", (int)armies->getIndex(iLeader));
#endif
#endif
}

void OBEdit::createNewSP()
{
   if(cpi == president && iTown == NoTown)
   {
       MessageBox(getHWND(), "Please select desired location for SP from \"Position\" page", "OB Editor", MB_OK);
       return;
   }

   Nationality nation = cpi->getNation();
   if(cpi == president)
   {
    ASSERT(iTown != NoTown);
      const Town& town = campData->getTown(iTown);
      const Province prov = campData->getProvince(town.getProvince());

      nation = prov.getNationality();
   }

   ComboBox combo(pages[EditAllUnitsPage]->getHWND(), IDOBE_SP_CURRENT);
   const UnitTypeItem& uti = campData->getUnitType((UnitType)combo.getValue());

   if(!uti.isNationality(nation))
   {
      MessageBox(getHWND(), "You are trying to build Illegal unit-type. \nTry selection a new unit-type", "Error", MB_OK);
      return;
   }

// if(canAdd)
   {
      Armies* armies = &campData->getArmies();
      isp = armies->createStrengthPoint();
      ASSERT(isp != NoStrengthPoint);
      sp = armies->getStrengthPoint(isp);

      sp->setUnitType((UnitType)combo.getValue());

      if(cpi == president)
      {
         armies->addRepoSP(sp, iTown);
#if 0
         createNewUnit(side, Rank_Division);
         ASSERT(cpi != president);
         attachTo = cpi;
#endif
      }
      else
      {
         newSP = True;
         attachingSP = True;
         attachSP();
         newSP = False;
         spListID = 0;
      }

      addSPToList();

      /*
       * Update parent morale
       */

      pages[EditAllUnitsPage]->initControls();
   }
}

void OBEdit::onSP()
{
   const Armies* armies = &campData->getArmies();
   ListBox listBox(pages[EditAllUnitsPage]->getHWND(), IDOBE_UNITS_SPLIST);
  spListID = (UWORD)listBox.getIndex();
  // isp = (ISP)listBox.getValue();
  isp = lparamToSP(listBox.getValue());
  ASSERT(isp != NoStrengthPoint);
  sp = armies->getStrengthPoint(isp);
}


void OBEdit::onStackedUnits(ICommandPosition from)
{
    const Armies* armies = &campData->getArmies();
    ASSERT(from != NoCommandPosition);
    updateValues();
      resetCurrentUnit(from);
}

void OBEdit::updateValues()
{
   for(int i = 0; i < EditPages_HowMany; i++)
      pages[i]->updateValues();
}

void OBEdit::attachUnit()
{
  if(attachingUnit)
  {
    Armies* armies = &campData->getArmies();
    if(independent)
      {
      armies->detachCommand(cpi);
      if(iTown != NoTown)
      {
        if(cp->getChild() != NoCommandPosition)
          updateChildPos(cpi, iTown);
        else
        {
          cp->setPosition(iTown);
#ifdef OLD_ORDERS
          CampaignOrderList& col = cp->getOrders();
          const OrderBase* order = col.getLastOrder();
          order.destTown = iTown;
#endif
        }
      }
    }

    else if(attachTo != NoCommandPosition)
    {
      CommandPosition* aCP = campData->getCommand(attachTo);
      armies->detachCommand(cpi);
      Rank r1 = cp->getRank();
      Rank r2 = aCP->getRank();
      if(!r1.isHigher(r2))
      {
        if(cp->getChild() != NoCommandPosition)
          updateChildPos(cpi, aCP->getCloseTown());
        else
               cp->setPosition(aCP->getCloseTown());
      }
      else
      {
        if(aCP->getChild() != NoCommandPosition)
          updateChildPos(attachTo, cp->getCloseTown());
        else
          aCP->setPosition(cp->getCloseTown());
      }

      CampaignArmy_Util::attachUnits(campData, attachTo, cpi);
    }
  }
  if(!detaching)
    addStackedUnitsToList();

  for(int i = 0; i < EditPages_HowMany; i++)
    pages[i]->initControls();

  attachingUnit = False;
  independent = False;

  detaching = False;
  iTown = NoTown;
}

void OBEdit::resetCurrentUnit(ICommandPosition from)
{
  if(from != cpi)
   {
    // const Armies* armies = &campData->getArmies();
    Armies* armies = &campData->getArmies();
    cpi = from;
    spListID = 0;
    cp = armies->getCommand(cpi);
    iLeader = cp->getLeader();
    if(iLeader != NoLeader)
      leader = armies->getLeader(iLeader);
    else
      leader = 0;

    isp = NoStrengthPoint;
    sp = 0;

    side = cp->getSide();
    rank = cp->getRankEnum();

    iTown = NoTown;
    independent = False;
    attachingUnit = False;

    ASSERT(president != NoCommandPosition);
    isPresident = cpi == president ? True : False;
    if(created)
    {
      addSPToList();

      for(int i = 0; i < EditPages_HowMany; i++)
            pages[i]->initControls();
    }
  }
}

void OBEdit::addStackedUnitsToList()
{
   {
      ListBox listBox(getHWND(), IDOBE_UNITS_NAME);
      listBox.reset();

      if(side != SIDE_Neutral)
      {
         Armies* armies = &campData->getArmies();
         UnitIter unitIter(armies, armies->getPresident(side), True);

         int i = 0;

         while(unitIter.next())
         {
            CommandPosition* cp = unitIter.currentCommand();
            // listBox.add(cp->getName(), unitIter.current());
            listBox.add(cp->getName(), cpToLPARAM(unitIter.current()));
            if(unitIter.current() == cpi)
               unitID = i;

            i++;
         }
         if(unitID >= listBox.getNItems())
                  unitID = 0;
         ASSERT(unitID < listBox.getNItems());
         listBox.set(unitID);
      }
   }

   // add independent leaders to that list
   {
      ListBox listBox(getHWND(), IDOBE_LEADERS_LIST);
      listBox.reset();

      if(side != SIDE_Neutral)
      {
         Armies* armies = &campData->getArmies();
         const IndependentLeaderList& llist = armies->independentLeaders();
         SListIterR<IndependentLeader> iter(&llist);
         while(++iter)
             {
                  if(iter.current()->leader()->getSide() == side)
                  {
                     char buf[100];
                     wsprintf(buf, "%s %s", iter.current()->leader()->getName(), (iter.current()->active()) ? " " : "-- Inactive");
                     listBox.add(buf, leaderToLPARAM(iter.current()->leader()));
                  }
         }

         if(listBox.getNItems() > 0)
         {
            listBox.set(listBox.getNItems() - 1);
             }
      }
   }


}

void OBEdit::attachLeader()
{
  if(attachingLeader)
  {
    ASSERT(iLeader != NoLeader);
    ASSERT(leader != 0);
    ASSERT(cpi != NoCommandPosition);
    ASSERT(cp != 0);

    Armies* armies = &campData->getArmies();

    if(independent)  // We're trying to attach to President
    {
      CampaignArmy_Util::transferLeader(campData, iLeader, NoCommandPosition, True);
      ASSERT(leader->getCommand() != NoCommandPosition);
      resetCurrentUnit(leader->getCommand());
    }
    else
    {
      if(armies->canLeaderTakeControl(iLeader, attachTo))
      {
        CampaignArmy_Util::transferLeader(campData, iLeader, attachTo, True);
            resetCurrentUnit(attachTo);
      }
    }
  }
  attachingLeader = False;
  independent = False;
  addStackedUnitsToList();
}

void OBEdit::attachSP()
{
  ASSERT(attachTo != NoCommandPosition);
  ASSERT(cpi != NoCommandPosition);
  if(attachingSP)
  {
    Armies* armies = &campData->getArmies();
    if(!independent && !newSP)
      armies->transferStrengthPoint(cpi, attachTo, isp);
    else if(newSP)
      armies->attachStrengthPoint(attachTo, isp);
    else if (independent)
    {
      ICommandPosition i = cpi;
      createNewUnit(side, Rank_Division);
      armies->transferStrengthPoint(i, cpi, isp);
      resetCurrentUnit(i);
    }

    for(int i = 0; i < EditPages_HowMany; i++)
         pages[i]->initControls();
  }
  addSPToList();
  attachingSP = False;
  independent = False;
}


void OBEdit::unlinkUnit(ITown to)
{
  ASSERT(to != NoTown);
  ASSERT(cpi != NoCommandPosition);
  ASSERT(cp != 0);
  iTown = to;
  independent = True;
  attachTo = NoCommandPosition;
  attachingUnit = True;
  attachUnit();
  parent->requestRedraw(True);

}


void OBEdit::updateChildPos(ICommandPosition icp, ITown to)
{

   ASSERT(to != NoTown);
   // const Armies* armies = &campData->getArmies();
   Armies* armies = &campData->getArmies();
    UnitIter iter(armies, icp, True);
   while(iter.next())
   {
     CommandPosition* childCP = iter.currentCommand();
     childCP->setPosition(to);
#ifdef OLD_ORDERS
     CampaignOrderList& col = cp->getOrders();
     OrderBase* order = col.getLastOrder();
     order.destTown = to;
#endif
   }

}


void OBEdit::addSPToList()
{

  ListBox list(pages[EditAllUnitsPage]->getHWND(), IDOBE_UNITS_SPLIST);
  list.reset();

  Armies* armies = &campData->getArmies();
  if(cp)
  {
    if(cpi == president)
    {
      const RepoSPList& rpl = armies->repoSPList();
      const UnitTypeTable& table = campData->getUnitTypes();

         SListIterR<RepoSP> iter(&rpl);
      while(++iter)
         {
            for(Nationality n = 0; n < scenario->getNumNations(); n++)
            {
               ASSERT(iter.current()->sp()->getUnitType() < table.entries());
               const UnitTypeItem& item = table[iter.current()->sp()->getUnitType()];
               if(scenario->nationToSide(n) == cpi->getSide() &&
                   item.isNationality(n))
               {
                  list.insert(item.getName(), -1, spToLPARAM(iter.current()->sp()));
                  break;
               }
            }
         }
      }

      else
      {
         ISP isp = cp->getSPEntry();
         while(isp != NoStrengthPoint)
         {

            MemPtr<char> buf(200);
            StrengthPointItem* sp = (StrengthPointItem*)armies->getStrengthPoint(isp);

            ISP next = sp->getNext();

            const UnitTypeTable& table = campData->getUnitTypes();
            ASSERT(sp->getUnitType() < table.entries());

            const UnitTypeItem& item = table[sp->getUnitType()];

            if(item.isNationality(cp->getNation()))
            {
               wsprintf(buf.get(), "%s", item.getName());
               // list.insert(buf, 0, isp);
               list.insert(buf.get(), -1, spToLPARAM(isp));
            }
            else
            {
               ASSERT(cpi != NoCommandPosition);
               armies->detachStrengthPoint(cpi, isp);
               armies->deleteStrengthPoint(isp);
            }

            isp = next;
         }
      }

//    if(spListID >= list.getNItems())
//       spListID = 0;

//    if(spListID < list.getNItems())
      spListID = list.getNItems() - 1;
    if(spListID >= 0)
      {
         list.set(spListID);
         // OBEdit::isp = (ISP)list.getValue();
         OBEdit::isp = lparamToSP(list.getValue());
         ASSERT(OBEdit::isp != NoStrengthPoint);

         sp = armies->getStrengthPoint(OBEdit::isp);
      }

      else
    {
      sp = 0;
      isp = NoStrengthPoint;
    }
    list.enable(cp->getSPEntry() != NoStrengthPoint);
  }
}

void OBEdit::addUnitTypeCombo(BasicUnitType::value basicType)
{
  if(cp && pages[EditAllUnitsPage])
   {
      ComboBox combo(pages[EditAllUnitsPage]->getHWND(), IDOBE_SP_CURRENT);

      int index = (combo.getNItems() > 0) ? combo.getIndex() : 0;
      combo.reset();

      const UnitTypeTable& table = campData->getUnitTypes();

      UnitType nTypes = table.entries();
    int i = 0;
//    int index = 0;
      for(UnitType t = 0; t < nTypes; t++)
      {
          // const Armies& army = campData->getArmies();
          const UnitTypeItem& unitType = table[t];

          /*
            * Don't bother with nationality rules for editor
            */


          // if(unitType.isNationality(cp->getNation()) && unitType.getBasicType() == basicType)

          if(unitType.getBasicType() == basicType)
          {
             combo.add(unitType.getName(), t);
             if(sp && (t == sp->getUnitType()))
                index = i;
             i++;
          }
      }
      if(index < combo.getNItems())
         combo.set(index);
   }
}

void OBEdit::addNationsToCombo(HWND hWnd, int id, Nationality nation, bool setEnable)
{
   ASSERT(cpi != NoCommandPosition);
   ASSERT(cp != 0);

   ComboBox combo(hWnd, id);
   combo.reset();

   Boolean unitBox = (id == IDOBE_UNITS_NATIONALITY);

   Armies* armies = &campaignData->getArmies();

   /*
    *  Add nations to combo.
    *  If combo is unit-nation combo and unit has SP's or no subordinates add all sides nations
    *  otherwise add only nations that that unit can control.
    *  Any Sp's that are not of that nations type will be deleted
    */

   for(Nationality n = 0; n < scenario->getNumNations(); n++)
   {
      combo.add(scenario->getNationName(n), n);
   }

#if 0
#if 0
      if(  (side == scenario->nationToSide(n)) &&
             (
                  (
                      ( unitBox &&
                           (
                               (cp->getSPEntry() != NoStrengthPoint) ||
                               (cp->getChild() == NoCommandPosition)
                  )
               ) ||
               armies->canNationControlThisUnit(cpi, n)
            )
         )
      )
#else
    if(
         ( unitBox &&
            (
               (cp->getSPEntry() != NoStrengthPoint) ||
               (cp->getChild() == NoCommandPosition)
            )
         ) ||
         armies->canNationControlThisUnit(cpi, n)
      )
#endif
      {
         combo.add(scenario->getNationName(n), n);
      }

  }
#endif

  ASSERT(combo.getNItems() > 0);
  combo.setValue(nation);

   if(setEnable)
      combo.enable(!isPresident);
}

void OBEdit::setSides()
{
  Armies* armies = &campData->getArmies();
  // Nation* nation = armies->getNation(side);
  // president = nation->getPresident();
  president = armies->getPresident(side);
  addStackedUnitsToList();
}

void OBEdit::deleteUnit()
{
  ASSERT(cpi != NoCommandPosition);
  ASSERT(cp != 0);
  ASSERT(leader != 0);
  ASSERT(iLeader != NoLeader);
  ASSERT(president != NoCommandPosition);

  Armies* armies = &campData->getArmies();

  ICommandPosition detachCPI = cpi;
  CommandPosition* detachCP = cp;

  ICommandPosition iParent = cp->getParent();
  ASSERT(iParent != NoCommandPosition);
  cpi = cp->getChild();

   // attach children to parent, or if parent is president make independent
  while(cpi != NoCommandPosition)
  {
    cp = armies->getCommand(cpi);
    ICommandPosition iSister = cp->getSister();

    attachTo = iParent;

    if(iParent == president)
      independent = True;

    attachingUnit = True;
    detaching = True;

    attachUnit();
    cpi = iSister;
  }

  cpi = detachCPI;
  cp = detachCP;

   // armies->removeUnit(cpi);
   armies->removeUnitAndChildren(cpi);

  resetCurrentUnit(iParent);
  addStackedUnitsToList();
}

// function is recursive, deletes commandPosition and all subordinates
void OBEdit::deleteUnits()
{

  Armies* armies = &campData->getArmies();
  if(cp)
  {
    ASSERT(cpi != NoCommandPosition);

    armies->removeUnitAndChildren(cpi);
  }
}

void OBEdit::deleteLeader()
{
  ASSERT(cpi != NoCommandPosition);
  ASSERT(cp != 0);
  ASSERT(iLeader != NoLeader);
  ASSERT(leader != 0);
  Armies* armies = &campData->getArmies();
  if(cpi != president)
  {
    armies->killAndReplaceLeader(iLeader);
    // armies->unassignLeader(cpi);
    // armies->autoPromote(cpi, side);
    // armies->removeLeader(iLeader);
    iLeader = cp->getLeader();
    ASSERT(iLeader != NoLeader);
    leader = armies->getLeader(iLeader);
  }
   else
  {
     IndependentLeaderList& llist = armies->independentLeaders();
     SListIter<IndependentLeader> iter(&llist);
     while(++iter)
     {
        if(iter.current()->leader() == iLeader)
        {
           iter.remove();
           break;
        }
     }
  }

  addStackedUnitsToList();
}

void OBEdit::deleteSP()
{
  ASSERT(isp != NoStrengthPoint);
  ASSERT(sp != 0);
  ASSERT(cpi != NoCommandPosition);

  Armies* armies = &campData->getArmies();

  if(cpi != president)
  {
      armies->detachStrengthPoint(cpi, isp);
      armies->deleteStrengthPoint(isp);
   }
  else
  {
     RepoSPList& list = armies->repoSPList();
     SListIter<RepoSP> iter(&list);
     while(++iter)
     {
        if(iter.current()->sp() == isp)
        {
           iter.remove();
           break;
        }
     }
  }

  addSPToList();
}

void OBEdit::redrawList(RECT& r, int id)
{
  ListBox list(getHWND(), id);
  InvalidateRect(list.getHWND(), &r, TRUE);

}

/*
 * Make sure a unit is at a town with fortifications
 */

Boolean OBEdit::canGiveOrder(int v)
{
  ASSERT(v < Orders::Type::HowMany);
  ASSERT(cp != 0);
  ASSERT(cpi != NoCommandPosition);

  if(cp && cpi != NoCommandPosition)
  {
    if(v == Orders::Type::Garrison)
    {
      ASSERT(cp->atTown());
      if(cp->atTown())
      {
        Town& t = campData->getTown(cp->getTown());
        return static_cast<Boolean>(t.getFortifications() > 0);
      }
    }

  }
  return True;
}
/*=================================================================
 * Sub-Dialog Implementation
 */

OBEditPage::OBEditPage(OBEdit* p, const char* dlgName)
{
   edit = p;

    HRSRC rhTep = FindResource(NULL, dlgName, RT_DIALOG);
   ASSERT(rhTep != NULL);
   HGLOBAL lphTep = (HGLOBAL) LoadResource(NULL, rhTep);
   ASSERT(lphTep != NULL);
   dialog = (LPDLGTEMPLATE) LockResource(lphTep);
   ASSERT(dialog != NULL);

}

HWND OBEditPage::create()
{
   ASSERT(hWnd == NULL);      // Already created?

   HWND hDialog = createDialogIndirect(dialog, edit->getHWND());

   ASSERT(hDialog != NULL);

   return hDialog;
}

// void OBEditPage::enable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_SHOW);
// }
//
// void OBEditPage::disable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_HIDE);
// }


void OBEditPage::setPosition()
{
   const RECT& r = edit->getDialogRect();

#ifdef DEBUG
   debugLog("OBEditPage::setPosition(%ld,%ld,%ld,%ld)\n",
      r.left, r.top, r.right, r.bottom);
#endif

   SetWindowPos(getHWND(), HWND_TOP, r.left, r.top, r.right-r.left, r.bottom-r.top, 0);
}

/*=========================================================================
 * General sub-Dialog
 */

BOOL OBEditAllUnits::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
             HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;
      case WM_MEASUREITEM:
         HANDLE_WM_MEASUREITEM(hWnd, wParam, lParam, onMeasureItem);
         break;
      case WM_DRAWITEM:
         HANDLE_WM_DRAWITEM(hWnd, wParam, lParam, onDrawItem);
         break;

#if 0
      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;
#endif
      default:
         return FALSE;
   }

   return TRUE;
}

void OBEditAllUnits::updateValues()
{
   debugLog("OBEditAllUnits: Restored values\n");
}

struct AllowedRank{
  const char* name;
  RankEnum rank;
};

const int UnitTypeIconWidth = 24;

const int numAllowedRank = 4;

static AllowedRank allowedRank[numAllowedRank] =  {
  {"Army Group", Rank_ArmyGroup},
  {"Army",       Rank_Army},
  {"Corps",      Rank_Corps},
  {"Division",   Rank_Division},
};

int rankToCombo(RankEnum rank)
{
  switch(rank)
  {
    case Rank_God:
    case Rank_President:
    case Rank_ArmyGroup:
      return 0;
    case Rank_Army:
      return 1;
    case Rank_Corps:
      return 2;
    case Rank_Division:
         return 3;
#ifdef DEBUG
    default:
      GeneralError error("Bad RankEnum value(%d) in rankToCombo() - obedit.cpp", (int)rank);
#endif
  }
  return 0;
}

void OBEditAllUnits::initControls()
{
   ListBox spList(getHWND(), IDOBE_UNITS_SPLIST);
   spList.enable(edit->getISP() != NoStrengthPoint);

// const Armies* armies = &campData->getArmies();
   ComboBox cRank(getHWND(), IDOBE_UNITS_RANKLEVEL);
   cRank.enable(!edit->getIsPresident());

   if(!edit->getIsPresident())
   {
     CommandPosition* cp = edit->getCP();
     ASSERT(cp != 0);
     Leader* leader = edit->getLeader();
     ASSERT(leader != 0);

     cRank.set(rankToCombo(edit->getRank()));
   }

   ComboBox cSide(getHWND(), IDOBE_UNITS_SIDE);
    ASSERT((int)edit->getSide() < scenario->getNumSides());
   cSide.set((int)edit->getSide());

   if(edit->getCP())
   {
     BasicUnitType::value type;

     if(Button(hWnd, IDOBE_SP_INFANTRY).getCheck())
       type = BasicUnitType::Infantry;
     else if(Button(hWnd, IDOBE_SP_CAVALRY).getCheck())
       type = BasicUnitType::Cavalry;
     else if(Button(hWnd, IDOBE_SP_ARTILLERY).getCheck())
       type = BasicUnitType::Artillery;
     else if(Button(hWnd, IDOBE_SP_OTHER).getCheck())
       type = BasicUnitType::Special;

     ComboBox comboType(hWnd, IDOBE_SP_CURRENT);
     edit->addUnitTypeCombo(type);
     edit->addNationsToCombo(hWnd, IDOBE_UNITS_NATIONALITY, edit->getCP()->getNation());
   }

   Button bEdit(getHWND(), IDOBE_UNITS_UNIT);
   bEdit.enable(edit->getISP() != NoStrengthPoint);

// Button bNew(getHWND(), IDOBE_NEW);
// bNew.enable(edit->getISP() != NoStrengthPoint);

}


BOOL OBEditAllUnits::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   debugLog("OBEditAllUnits::onInit()\n");
   setPosition();


   ComboBox comboRank(hwnd, IDOBE_UNITS_RANKLEVEL);
   ComboBox comboSide(hwnd, IDOBE_UNITS_SIDE);
   ComboBox comboNation(hwnd, IDOBE_UNITS_NATIONALITY);

   for(int i = 0; i < numAllowedRank; i++)
     comboRank.add(allowedRank[i].name, (int)allowedRank[i].rank);


   for(i = 0; i < scenario->getNumSides(); i++)
       comboSide.add(scenario->getSideName((Side)i), i);


   Button inf(hWnd, IDOBE_SP_INFANTRY);

   inf.setCheck(True);

   images.loadBitmap(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_BUILD_TYPE), UnitTypeIconWidth);
   icons = LoadBitmap(scenario->getScenarioResDLL(), MAKEINTRESOURCE(BM_BUILD_TYPE));

   inf.setIcon(images.getIcon(0));

   Button cavalry(hwnd, IDOBE_SP_CAVALRY);
    cavalry.setIcon(images.getIcon(1));

   Button artillery(hwnd, IDOBE_SP_ARTILLERY);
   artillery.setIcon(images.getIcon(2));

   Button other(hwnd, IDOBE_SP_OTHER);
   other.setIcon(images.getIcon(3));

   ListBox box(getHWND(), IDOBE_UNITS_SPLIST);
   SetWindowLong(box.getHWND(), GWL_WNDPROC, (DWORD) subClassProc);
   SetWindowLong(box.getHWND(), GWL_USERDATA, (DWORD)edit);

   initControls();
   return TRUE;
}

void OBEditAllUnits::onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem)
{
  lpMeasureItem->itemHeight = 10;
}

static RECT iconPositions[] = {
   {   0,  0, 24, 24 }, // Resource Mode
   {  24,  0, 24, 24 }, // Unit Mode
   {  48,  0, 24, 24 }, // Zoom In
   {  72,  0, 24, 24 }, // Zoom Out
};

const RECT* getImagePosition(BasicUnitType::value id)
{
   ASSERT(id >= BasicUnitType::Infantry);
   ASSERT(id < BasicUnitType::HowMany);
   return &iconPositions[id];
}

void OBEditAllUnits::onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem)
{
   switch(lpDrawItem->CtlID)
   {
      case IDOBE_UNITS_SPLIST:
      {
         ListBox list(lpDrawItem->hwndItem);
         if(list.getNItems() > 0)
         {

            LPARAM lparam = SendMessage(lpDrawItem->hwndItem, LB_GETITEMDATA, lpDrawItem->itemID, 0);

            if(lparam != 0)
            {
               ISP isp = lparamToSP(lparam);
               ASSERT(isp != NoStrengthPoint);

               const Armies* armies = &getCampaignData()->getArmies();
               StrengthPoint* sp = (StrengthPoint*)armies->getStrengthPoint(isp);

               const UnitTypeTable& table = getCampaignData()->getUnitTypes();
               ASSERT(sp->getUnitType() < table.entries());

                      const UnitTypeItem& item = table[sp->getUnitType()];


               char text[500];
               SendMessage(lpDrawItem->hwndItem, LB_GETTEXT, lpDrawItem->itemID, (LPARAM)(LPCSTR)text);
               Fonts font;
               font.setFont(lpDrawItem->hDC, 12, 4);

               TEXTMETRIC tm;
               GetTextMetrics(lpDrawItem->hDC, &tm);
               int y = ((lpDrawItem->rcItem.bottom + lpDrawItem->rcItem.top -
                        tm.tmHeight) / 2);

               COLORREF oldBkColor = GetBkColor(lpDrawItem->hDC);
               COLORREF oldTextColor = GetTextColor(lpDrawItem->hDC);
               if (lpDrawItem->itemState & ODS_SELECTED)
               {
                  DrawFocusRect(lpDrawItem->hDC, &lpDrawItem->rcItem);
                  SetBkColor(lpDrawItem->hDC, scenario->getSideColour(edit->getSide()));
                  SetTextColor(lpDrawItem->hDC, RGB(255, 255, 255));
               }

               ExtTextOut(lpDrawItem->hDC, 14 , y,
                        ETO_CLIPPED | ETO_OPAQUE, &lpDrawItem->rcItem,
                        text, lstrlen(text), NULL);
               font.deleteFont();

               {
                  DeviceContext hdcMem;
                           // gApp.saveDibDC();
                  // HDC hdcMem = gApp.getDibDC();
                  // ASSERT(hdcMem != NULL);
                  ASSERT(icons != NULL);
                  SelectObject(hdcMem, icons);

                  const RECT* r = getImagePosition(item.getBasicType());

                  int oldStretchMode = SetStretchBltMode(lpDrawItem->hDC, COLORONCOLOR);
                  ASSERT(oldStretchMode != 0);

                  StretchBlt(lpDrawItem->hDC, 2, y, 12, 12,
                     hdcMem, r->left, r->top, r->right, r->bottom,
                     SRCCOPY);

                  SetStretchBltMode(lpDrawItem->hDC, oldStretchMode);

               }

               SetBkColor(lpDrawItem->hDC, oldBkColor);
               SetTextColor(lpDrawItem->hDC, oldTextColor);

            }  // if lparam != 0
         }  // if nIteme > 0
         break;
      }  // case
   }
}

void OBEditAllUnits::onDestroy(HWND hwnd)
{
   debugLog("OBEditAllUnits::onDestroy()\n");
}
/*======================================================================
 * Position Sub-Dialog
 */


BOOL OBEditPosition::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;

         default:
         return FALSE;
   }

   return TRUE;
}

void OBEditPosition::updateValues()
{
}

void OBEditPosition::initControls()
{
  CommandPosition* cp = edit->getCP();
  if(cp && !edit->getIsPresident())
  {
    ITown iTown = cp->getCloseTown();
    Town town = getCampaignData()->getTown(iTown);
    Province prov = getCampaignData()->getProvince(town.getProvince());
    MemPtr<char> buf(200);
    wsprintf(buf.get(), "%s, %s", town.getName(), prov.getName());
    SetDlgItemText(getHWND(), IDOBE_CURTOWN, buf.get());
  }
  else
    SetDlgItemText(getHWND(), IDOBE_CURTOWN, "No Town");

  SetDlgItemText(getHWND(), IDOBE_NEWTOWN, "No Town");

//  Button button(getHWND(), IDOBE_MOVE);
//  button.enable(cp && !edit->getIsPresident());

}

BOOL OBEditPosition::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   debugLog("OBEditPosition::onInit()\n");
   setPosition();

   HWND hTV = GetDlgItem(hwnd, IDOBE_UNITS_NEWTOWN);
   ASSERT(hTV != 0);

   HIMAGELIST imgList = ImageList_LoadBitmap(
      // APP::instance(),
//    wndInstance(hwnd),
      scenario->getScenarioResDLL(),
      MAKEINTRESOURCE(BM_OBICONS),
      OBI_CX,
      0,
      CLR_NONE);
   ASSERT(imgList != NULL);

   TreeView_SetImageList(hTV, imgList, TVSIL_NORMAL);
   addTownItems(hTV);


   initControls();
   return TRUE;
}


void OBEditPosition::orderUnit(unsigned int id, unsigned int code)
{
  ASSERT(id == IDOBE_UNITS_NEWTOWN);

  if(getITown() != NoTown)
  {
    edit->unlinkUnit(getITown());
    Town town = getCampaignData()->getTown(getITown());
    Province prov = getCampaignData()->getProvince(town.getProvince());
    MemPtr<char> buf(200);
    wsprintf(buf.get(), "%s, %s", town.getName(), prov.getName());
    SetDlgItemText(getHWND(), IDOBE_NEWTOWN, buf.get());
  }

}

void OBEditPosition::onDestroy(HWND hwnd)
{
   debugLog("OBEditPosition::onDestroy()\n");
}

/*=========================================================================
 * Unit sub-Dialog
 */

BOOL OBEditUnit::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;
      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}


void OBEditUnit::updateValues()
{
   CommandPosition* cp = edit->getCP();
   if(cp)
   {
     if(!edit->getIsPresident())
     {
       cp->setMorale( (Attribute) SendDlgItemMessage(hWnd, IDOBE_MORALESPIN, UDM_GETPOS, 0, 0));
       cp->setFatigue( (Attribute) SendDlgItemMessage(hWnd, IDOBE_FATIGUESPIN, UDM_GETPOS, 0, 0));
          cp->setSupply( (Attribute) SendDlgItemMessage(hWnd, IDOBE_SUPPLYSPIN, UDM_GETPOS, 0, 0));
       cp->setFieldWorks( (Attribute) SendDlgItemMessage(hWnd, IDOBE_UNIT_FIELDWORKSSPIN, UDM_GETPOS, 0, 0));

       {
         Button b(getHWND(), IDOBE_UNIT_ACTIVE);

         cp->isActive(static_cast<Boolean>(b.getCheck()));
       }

         {
         Button b(getHWND(), IDOBE_UNIT_SIEGE);

            bool active = b.getCheck();

            // cp->setSiegeActive(active);
            cp->getCurrentOrder()->setSiegeActive(active);
         }
     }
   }
}

void OBEditUnit::initControls()
{
   CommandPosition* cp = edit->getCP();

   if(cp)
   {

     // for(int i = IDOBE_MORALE; i < IDOBE_SUPPLYSPIN + 1; i++)
       for(int i = IDOBE_MORALE; i <= IDOBE_SETMORALE; i++)
     {
       WindowControl control(hWnd, i);
       control.enable(cp->isRealUnit() && !edit->getIsPresident());
     }

     if(cp->isRealUnit() && !edit->getIsPresident())
     {
       SendDlgItemMessage(hWnd, IDOBE_MORALESPIN, UDM_SETPOS, 0, MAKELONG(cp->getMorale(), 0));
       SendDlgItemMessage(hWnd, IDOBE_UNIT_FIELDWORKSSPIN, UDM_SETPOS, 0, MAKELONG(cp->getFieldWorks(), 0));
       SendDlgItemMessage(hWnd, IDOBE_FATIGUESPIN, UDM_SETPOS, 0, MAKELONG(cp->getFatigue(), 0));
       SendDlgItemMessage(hWnd, IDOBE_SUPPLYSPIN, UDM_SETPOS, 0, MAKELONG(cp->getSupply(), 0));
     }
     Leader* leader = 0;
     if(!edit->getIsPresident())
     {
       ASSERT(cp->getLeader() != NoLeader);
       leader = getCampaignData()->getLeader(cp->getLeader());
     }
     SetDlgItemText(hWnd, IDOBE_UNIT_NAME, cp->isRealUnit() ? cp->getName() : leader ? leader->getName() : "unknown");


     {
       ComboBox b(getHWND(), IDOBE_UNIT_ORDERS);
       ASSERT(b.getNItems() > 0);
       if(b.getNItems() > 0)
       {
         // Added by Steven
         Orders::Type::Value ord = cp->startOrder();
             if(!Orders::Type::canGiveOnArrival(ord))
            ord = Orders::Type::Hold;

         b.setValue(ord);
       }
     }

     {
       ComboBox b(getHWND(), IDOBE_UNIT_AGGRESSION);
       ASSERT(b.getNItems() > 0);
       if(b.getNItems() > 0)
       {
         b.setValue(cp->startAggression());
       }
     }

     {
       Button b(getHWND(), IDOBE_UNIT_ACTIVE);
       b.setCheck(cp->isActive());
     }

     {
       Button b(getHWND(), IDOBE_UNIT_SIEGE);
       b.setCheck(cp->getCurrentOrder()->getSiegeActive());
       // b.setCheck(cp->isSiegeActive());
     }

   }
}


BOOL OBEditUnit::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   debugLog("OBEditUnit::onInitDialog()\n");
   setPosition();
   SendDlgItemMessage(hwnd, IDOBE_MORALESPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDOBE_UNIT_FIELDWORKSSPIN, UDM_SETRANGE, 0, MAKELONG(CommandPosition::MaxFieldWorks, 0));
   SendDlgItemMessage(hwnd, IDOBE_FATIGUESPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDOBE_SUPPLYSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));

   {
     ComboBox b(getHWND(), IDOBE_UNIT_ORDERS);
     for(Orders::Type::Value o = Orders::Type::First;
         o < Orders::Type::HowMany; INCREMENT(o))
     {
       if(Orders::Type::canGiveOnArrival(o))
         b.add(Orders::Type::text(o), o);
     }
   }

   {
     ComboBox b(getHWND(), IDOBE_UNIT_AGGRESSION);
     for(Orders::Aggression::Value v = Orders::Aggression::First;
         v < Orders::Aggression::HowMany; INCREMENT(v))
     {
       b.add(Orders::Aggression::getName(v), v);
     }
   }

   initControls();

   return TRUE;
}


void OBEditUnit::onDestroy(HWND hwnd)
{
   debugLog("OBEditUnit::onInitDestroy()\n");
}

/*=========================================================================
 * Unit sub-Dialog
 */

const char OBEditLeader::s_defaultPortrait[] = "<Default>";

BOOL OBEditLeader::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
         case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;
      case WM_VSCROLL:
         HANDLE_WM_VSCROLL(hWnd, wParam, lParam, onVScroll);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}


void OBEditLeader::updateValues()
{
   Leader* leader = edit->getLeader();
   if(leader)
   {
      leader->setInitiative((Attribute)SendDlgItemMessage(hWnd, IDOBE_LEADER_INITSPIN, UDM_GETPOS, 0, 0));
      leader->setStaff((Attribute)SendDlgItemMessage(hWnd, IDOBE_LEADER_STAFFSPIN, UDM_GETPOS, 0, 0));
      leader->setSubordination((Attribute)SendDlgItemMessage(hWnd, IDOBE_LEADER_SUBSPIN, UDM_GETPOS, 0, 0));
      leader->setAggression((Attribute)SendDlgItemMessage(hWnd, IDOBE_LEADER_AGGRESSSPIN, UDM_GETPOS, 0, 0));
      leader->setCharisma((Attribute)SendDlgItemMessage(hWnd, IDOBE_LEADER_CHARISMASPIN, UDM_GETPOS, 0, 0));
      leader->setTactical((Attribute)SendDlgItemMessage(hWnd, IDOBE_LEADER_TACTICALSPIN, UDM_GETPOS, 0, 0));
      leader->setCommandRadius((Attribute)SendDlgItemMessage(hWnd, IDOBE_LEADER_RADIUSSPIN, UDM_GETPOS, 0, 0));
      // leader->setRankRating((Attribute)SendDlgItemMessage(hWnd, IDOBE_LEADER_RANKSPIN, UDM_GETPOS, 0, 0));
      leader->setHealth((Attribute)SendDlgItemMessage(hWnd, IDOBE_LEADER_HEALTHSPIN, UDM_GETPOS, 0, 0));

      leader->setRankLevel(getRankLevel());

      ComboBox combo(hWnd, IDOBE_LEADER_SPECIALISTTYPE);
      leader->setAsSpecialist(static_cast<Specialist::Type>(combo.getValue()));

      ComboBox pCombo(hWnd, IDOBE_LEADER_PORTRAIT);
      StringPtr portText = pCombo.getText();
      if(strcmp(portText, s_defaultPortrait) == 0)
         leader->portrait(0);
      else
         leader->portrait(copyString(portText));

      leader->isLucky(Button(hWnd, IDOBE_LEADER_LUCKY).getCheck());
   }
}

void OBEditLeader::initControls()
{
   Leader* leader = edit->getLeader();

   for(int i = IDOBE_LEADER_CURRENT; i < IDOBE_LEADER_NATION + 1; i++)
   {
     WindowControl control(hWnd, i);
     control.enable((leader != 0));
   }

   if(leader)
   {
       edit->addNationsToCombo(hWnd, IDOBE_LEADER_NATION, leader->getNation(), False);

     ComboBox combo(hWnd, IDOBE_LEADER_SPECIALISTTYPE);
     combo.set(static_cast<int>(leader->getSpecialistType()));

     ComboBox pCombo(hWnd, IDOBE_LEADER_PORTRAIT);
     if(leader->portrait() == 0)
      pCombo.setText(s_defaultPortrait);
     else
      pCombo.setText(leader->portrait());

     SendDlgItemMessage(hWnd, IDOBE_LEADER_INITSPIN, UDM_SETPOS, 0, MAKELONG(leader->getInitiative(True), 0));
     SendDlgItemMessage(hWnd, IDOBE_LEADER_STAFFSPIN, UDM_SETPOS, 0, MAKELONG(leader->getStaff(True), 0));
     SendDlgItemMessage(hWnd, IDOBE_LEADER_SUBSPIN, UDM_SETPOS, 0, MAKELONG(leader->getSubordination(), 0));
     SendDlgItemMessage(hWnd, IDOBE_LEADER_AGGRESSSPIN, UDM_SETPOS, 0, MAKELONG(leader->getAggression(), 0));
     SendDlgItemMessage(hWnd, IDOBE_LEADER_CHARISMASPIN, UDM_SETPOS, 0, MAKELONG(leader->getCharisma(True), 0));
     SendDlgItemMessage(hWnd, IDOBE_LEADER_TACTICALSPIN, UDM_SETPOS, 0, MAKELONG(leader->getTactical(True), 0));
     SendDlgItemMessage(hWnd, IDOBE_LEADER_RADIUSSPIN, UDM_SETPOS, 0, MAKELONG(leader->getCommandRadiusAttribute(), 0));
     // SendDlgItemMessage(hWnd, IDOBE_LEADER_RANKSPIN, UDM_SETPOS, 0, MAKELONG(leader->getRankRating(), 0));
     SendDlgItemMessage(hWnd, IDOBE_LEADER_HEALTHSPIN, UDM_SETPOS, 0, MAKELONG(leader->getHealth(), 0));

     HWND hTBar = GetDlgItem(getHWND(), IDOBE_LEADER_RANKLEVEL);
     ASSERT(hTBar != 0);
     setRankLevel();
     SendMessage(hTBar, TBM_SETPOS, TRUE, d_pos);

     SetDlgItemText(getHWND(), IDOBE_LEADER_CURRENT, leader->getName());

     SetDlgItemInt(getHWND(), IDOBE_LEADER_HELPID, leader->getHelpID(), FALSE);

     Button shqButton(getHWND(), IDOBE_LEADER_SHQ);
     shqButton.setCheck(leader->isSupremeLeader());

     Button(hWnd, IDOBE_LEADER_LUCKY).setCheck(leader->isLucky());
   }

}


BOOL OBEditLeader::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
   debugLog("OBEditLeader::onInitDialog()\n");
   setPosition();

   ComboBox combo(hWnd, IDOBE_LEADER_SPECIALISTTYPE);
   for(Specialist::Type type = Specialist::First; type < Specialist::HowMany; INCREMENT(type))
   {
     combo.add(Specialist::typeName(type), static_cast<int>(type));
   }

   /*
    * fill Portrait Combo box
    */

   {
      ComboBox pCombo(hWnd, IDOBE_LEADER_PORTRAIT);

      int index = SendMessage(pCombo.getHWND(), CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(s_defaultPortrait));
      ASSERT(index != CB_ERR);
      ASSERT(index != CB_ERRSPACE);

      SimpleString path = scenario->getPortraitPath();
      path += "\\*.bmp";

      for(FindFileIterator fIter(path.toStr()); fIter(); ++fIter)
      {
         const char* name = fIter.name();
         char buffer[MAX_PATH];
         strcpy(buffer, name);
         char* s = strrchr(buffer, '.');
         if(s)
            *s = 0;

         int index = SendMessage(pCombo.getHWND(), CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(buffer));
         ASSERT(index != CB_ERR);
         ASSERT(index != CB_ERRSPACE);
      }
   }


   SendDlgItemMessage(hwnd, IDOBE_LEADER_INITSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDOBE_LEADER_STAFFSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDOBE_LEADER_SUBSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDOBE_LEADER_AGGRESSSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDOBE_LEADER_CHARISMASPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDOBE_LEADER_TACTICALSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDOBE_LEADER_RADIUSSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   // SendDlgItemMessage(hwnd, IDOBE_LEADER_RANKSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));
   SendDlgItemMessage(hwnd, IDOBE_LEADER_HEALTHSPIN, UDM_SETRANGE, 0, MAKELONG(UBYTE_MAX, 0));


   HWND hTBar = GetDlgItem(hwnd, IDOBE_LEADER_RANKLEVEL);
   ASSERT(hTBar != 0);
   SendMessage(hTBar, TBM_SETRANGE, TRUE, (LPARAM)MAKELONG(0, 3));

   initControls();

   return TRUE;
}

void OBEditLeader::onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos)
{
#ifdef DEBUG
   debugLog("OBEditLeader::onVScroll(%p %p %u %d)\n",
      hwnd, hwndCtl, code, pos);

   HWND hTrack = GetDlgItem(hwnd, IDOBE_LEADER_RANKLEVEL);
   ASSERT(hTrack != NULL);
   ASSERT(hTrack == hwndCtl);
#endif


   /*
    * Simplest method is to simply get current position on any message
    * Alternatively, if only want to change speed when dragging has finished
    * then check on TB_ENDTRACK
    */

   switch(code)
   {
   case TB_ENDTRACK:
   case TB_BOTTOM:
   case TB_LINEDOWN:
   case TB_LINEUP:
   case TB_PAGEDOWN:
   case TB_PAGEUP:
   case TB_TOP:
      d_pos = pos = SendMessage(hwndCtl, TBM_GETPOS, 0, 0);
      break;
   case TB_THUMBPOSITION:
   case TB_THUMBTRACK:
      break;
   }
}


Rank OBEditLeader::getRankLevel()
{
   static Rank ranks[4] =
   {
      Rank_ArmyGroup,
      Rank_Army,
      Rank_Corps,
      Rank_Division
   };

   ASSERT(d_pos >= 0);
   ASSERT(d_pos < 4);

   if((d_pos >= 0) && (d_pos < 4))
      return ranks[d_pos];
   else
      return Rank_Division;
}

void OBEditLeader::setRankLevel()
{
   Leader* leader = edit->getLeader();
   if(leader)
   {
      Rank rank = leader->getRankLevel();
      RankEnum rankEnum = rank.getRankEnum();
      switch(rankEnum)
      {
         case Rank_God:
         case Rank_President:
         case Rank_ArmyGroup:
            d_pos = 0;
            break;
         case Rank_Army:
            d_pos = 1;
            break;
         case Rank_Corps:
            d_pos = 2;
            break;
         case Rank_Division:
         default:
            d_pos = 3;
            break;
      }
   }
}

void OBEditLeader::onDestroy(HWND hwnd)
{
   debugLog("OBEditLeader::onInitDestroy()\n");
}




/*
 * Recursive Function to clone an organization
 */

static void copyUnit(Armies* armies, ICommandPosition iParent, ConstICommandPosition cpi)
{
   const CommandPosition* cp = armies->getCommand(cpi);
#if 0
   ICommandPosition iClone = armies->createCommand();
#else
   ICommandPosition iClone = armies->createChildCP(iParent);
#endif
   CommandPosition* clone = armies->getCommand(iClone);
   clone->setSide(cp->getSide());
   clone->setNation(cp->getNation());
   clone->setPosition(cp->getPosition());
   clone->setRank(cp->getRankEnum());
   armies->makeUnitName(iClone, cp->getRank());

   ILeader iLeader = armies->createLeader(cp->getSide(), cp->getNation(), cp->getRankEnum(), True);
   ASSERT(iLeader != NoLeader);
   armies->attachLeader(iClone, iLeader);

   /*
    * Clone Strength Points
    */

   ConstStrengthPointIter spIter(armies, cpi);
   while(++spIter)
   {
      const StrengthPoint* sp = spIter.current();

      ISP newISP = armies->createStrengthPoint();
      StrengthPoint* newSP = armies->getStrengthPoint(newISP);
      newSP->setUnitType(sp->getUnitType());
      armies->attachStrengthPoint(iClone, newISP);
   }

   /*
    * Clone children
    */

   ConstICommandPosition iChild = cp->getChild();
   while(iChild != NoCommandPosition)
   {
      copyUnit(armies, iClone, iChild);

      // const CommandPosition* cpChild = armies->getCommand(iChild);
      // iChild = cpChild->getSister();
      iChild = iChild->getSister();
   }
}

/*
 * Copy the current unit including children
 */

void OBEdit::duplicateOrganization()
{
   ASSERT(cpi != NoCommandPosition);
   if(cpi != NoCommandPosition)
   {
      Armies* armies = &campData->getArmies();
      const CommandPosition* cp = armies->getCommand(cpi);


      copyUnit(armies, cp->getParent(), cpi);

      addStackedUnitsToList();
   }

}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
