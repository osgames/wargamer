/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef _OBEDIT_HPP
#define _OBEDIT_HPP

#include "dialog.hpp"
// #include "town.hpp"
#include "grtypes.hpp"
#include "mw_user.hpp"
// #include "sllist.hpp"
#include "armies.hpp"
#include "unittype.hpp"
#include "winctrl.hpp"

/*
 * New version, uses tabbed dialog controls
 */

class OBEditPage;


class MWEditInterface;
class MapWindow;
class CampaignData;

class OBEdit : public ModelessDialog, public EditDialog {
   enum {
     EditAllUnitsPage,
     EditPositionPage,
     EditUnitPage,
     EditLeaderPage,

     EditPages_HowMany
   };

   MWEditInterface* d_editControl;

   const StackedUnitList* unitList;
   int unitID;

   int spListID;

   CommandPosition* cp;
   ICommandPosition cpi;
   ILeader iLeader;
   Leader* leader;
   ICommandPosition president;
   ISP isp;
   StrengthPoint* sp;
   Boolean independent;
   Boolean newSP;
   Boolean newLeader;

   // used for creating temp indexes for new items
   ICommandPosition newUnitID;
   ILeader newLeaderID;
   ISP newSPID;


   ICommandPosition attachTo;
   ICommandPosition attachSPTo;
   Side side;
   ITown iTown;

   RankEnum rank;

   Boolean created;

   Boolean attachingUnit;
   Boolean attachingLeader;
   Boolean attachingSP;
   Boolean detaching;

   Boolean isPresident;

   CampaignData* campData;
   MapWindow* parent;
   PixelPoint position;          // Where it starts

   OBEditPage** pages;
   HWND tabHwnd;                 // Tab Dialog handle
   int curPage;                  // Current tabbed dialog Page
   RECT tdRect;                  // Area for tabbed dialogues to fit into

   ImageList obImages;

public:
   OBEdit(MWEditInterface* editControl, CampaignData* cData, MapWindow* p, const StackedUnitList* list, const PixelPoint& pos);
   ~OBEdit();
//   LRESULT CALLBACK subClassProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
   void onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
   void onRButtonDown2(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
   VOID APIENTRY obEditPopupMenu(HWND hwnd, PixelPoint& pt, int id);
   VOID APIENTRY obEditPopupMenu2(HWND hwnd, PixelPoint& pt, int id);

   CampaignData* getCampaignData() const { return campData; }

   // Callback functions from MapWindow

   void kill();
   // void gotPosition(Location* l);
   TrackMode onSelect(TrackMode mode, const MapSelect& info);

   // Functions used by Child Dialog controls

   CommandPosition* getCP() const { return cp; }
   ICommandPosition getCPI() const { return cpi; }
   Leader* getLeader() const { return leader; }
   ICommandPosition getPresident() const { return president; }
   ICommandPosition getAttachTo() const { return attachTo; }
   ITown getITown() const {return iTown;}
   Side getSide() {return side;}
   Boolean getIsPresident() { return isPresident; }
   RankEnum getRank() { return rank;}

   void onStackedUnits(ICommandPosition from);
   void resetCurrentUnit(ICommandPosition from);
   void onTrackedUnits(ICommandPosition to);
   void onSP();
// void attach();
// void onAttach();
   void attachUnit();
   void attachLeader();
   void attachSP();
   void unlinkUnit(ITown to);
   void updateChildPos(ICommandPosition icp, ITown to);
   void sendNewOrders();
   void onNewUnit();
   void onNewLeader();
   void onNewSP();
   void createNewUnit(Side side, RankEnum rank);
   void createNewLeader(Side side, bool active = True);
   void createNewSP();
   void addSPToList();
   void addUnitTypeCombo(BasicUnitType::value type);
	 void addNationsToCombo(HWND hWnd, int id, Nationality nation, bool setEnable = True);
   void addStackedUnitsToList();
   void setSides();

   Boolean canGiveOrder(int v);

   void deleteUnit();
   void deleteUnits();
   void deleteLeader();
   void deleteSP();

   ISP getISP() {return isp;}
   StrengthPoint* getSP() { return sp; }
   void setISP(ISP i) {isp = i;}
   void resetISP() {isp = NoStrengthPoint;}
   int getSPListID() {return spListID;}

   void redrawList(RECT &r, int id);
   const RECT& getDialogRect() const { return tdRect; }

   void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
   void onMeasureItem(HWND hwnd, MEASUREITEMSTRUCT* lpMeasureItem);
   void onDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);

private:
   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
   void onClose(HWND hwnd);
   LRESULT onNotify(HWND hWnd, int id, NMHDR* lpNMHDR);
   void onActivate(HWND hwnd, UINT state, HWND hwndActDeact, BOOL fMinimized);
   void onContextMenu(HWND hwnd, HWND hwndCtl, int x, int y);
   BOOL onHelp(HWND hwnd, LPHELPINFO lParam);


   void initControls();
   void updateValues();
   void onSelChanged();
   void onOK();
   Boolean checkLeaderHelpID();

   void duplicateOrganization();
      // Create a sister that is indentical to current unit

};

//OBEdit* editOB(ICommandPosition cpi, PixelPoint& p);


#endif /* OBEDIT_HPP */
