# Microsoft Developer Studio Project File - Name="campedit" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=campedit - Win32 Editor Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "campedit.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "campedit.mak" CFG="campedit - Win32 Editor Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "campedit - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "campedit - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "campedit - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "campedit___Win32_Editor_Debug"
# PROP BASE Intermediate_Dir "campedit___Win32_Editor_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPEDIT_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\mapwind" /I "..\campwind" /D "_USRDLL" /D "EXPORT_CAMPEDIT_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /dll /debug /machine:I386 /out:"..\..\exe/campeditDB.dll" /pdbtype:sept
# ADD LINK32 gdi32.lib user32.lib comctl32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/campeditEDDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "campedit - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "campedit___Win32_Editor_Release"
# PROP BASE Intermediate_Dir "campedit___Win32_Editor_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_CAMPEDIT_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\mapwind" /I "..\campwind" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_CAMPEDIT_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /dll /debug /machine:I386 /out:"..\..\exe/campeditDB.dll" /pdbtype:sept
# ADD LINK32 gdi32.lib user32.lib comctl32.lib /nologo /dll /incremental:no /debug /machine:I386 /out:"..\..\exe/campeditED.dll"

!ENDIF 

# Begin Target

# Name "campedit - Win32 Editor Debug"
# Name "campedit - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\condedit.cpp
# End Source File
# Begin Source File

SOURCE=.\conedit.cpp
# End Source File
# Begin Source File

SOURCE=.\editdate.cpp
# End Source File
# Begin Source File

SOURCE=.\editinfo.cpp
# End Source File
# Begin Source File

SOURCE=.\editnat.cpp
# End Source File
# Begin Source File

SOURCE=.\editsup.cpp
# End Source File
# Begin Source File

SOURCE=.\mw_edit.cpp
# End Source File
# Begin Source File

SOURCE=.\obedit.cpp
# End Source File
# Begin Source File

SOURCE=..\campwind\obwin.cpp
# End Source File
# Begin Source File

SOURCE=.\provedit.cpp
# End Source File
# Begin Source File

SOURCE=.\townedit.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\campEditDll.h
# End Source File
# Begin Source File

SOURCE=.\condedit.hpp
# End Source File
# Begin Source File

SOURCE=.\conedit.hpp
# End Source File
# Begin Source File

SOURCE=.\editdate.hpp
# End Source File
# Begin Source File

SOURCE=.\editinfo.hpp
# End Source File
# Begin Source File

SOURCE=.\editnat.hpp
# End Source File
# Begin Source File

SOURCE=.\editsup.hpp
# End Source File
# Begin Source File

SOURCE=.\mw_edit.hpp
# End Source File
# Begin Source File

SOURCE=.\mwed_int.hpp
# End Source File
# Begin Source File

SOURCE=.\obedit.hpp
# End Source File
# Begin Source File

SOURCE=.\provedit.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=.\townedit.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
