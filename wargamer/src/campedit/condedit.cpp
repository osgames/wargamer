/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "condedit.hpp"
#include "campdint.hpp"
#include "resdef.h"
#include "town.hpp"
#include "winctrl.hpp"
#include "scenario.hpp"
#include "camptime.hpp"
#include "armies.hpp"
//#include "mw_edit.hpp"        // cyclic dependancy!
#include "mwed_int.hpp"
#include "app.hpp"



class ConditionEditPage : public ModelessDialog {
   DLGTEMPLATE* dialog;
protected:
   ConditionEdit* edit;       // Pointer to master data
public:
   ConditionEditPage(ConditionEdit* p, const char* dlgName);

   DLGTEMPLATE* getDialog() const { return dialog; }
   virtual const char* getTitle() const = 0;
   HWND create();
   // void enable();
   // void disable();
   virtual void updateValues() = 0;
   virtual void initControls() = 0;
   virtual void enableControls() = 0;
   virtual void setCurrent(ConditionType* current) = 0;
   virtual ConditionType* getCurrentType() const = 0;
protected:
   void setPosition();

   CampaignData* getCampaignData() const { return edit->getCampaignData(); }
};

class TownChangesPage : public ConditionEditPage {
   TownChangesSide* d_current;
public:
   TownChangesPage(ConditionEdit* p) : ConditionEditPage(p, townChangePageDlg) { d_current = 0; }
   const char* getTitle() const { return "TownChanges"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_TownChangesSide);
#endif
     d_current = static_cast<TownChangesSide*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class CasualtyPage : public ConditionEditPage {
   CasualtyLevelReached* d_current;
public:
   CasualtyPage(ConditionEdit* p) : ConditionEditPage(p, casualtyPageDlg) { d_current = 0; }
   const char* getTitle() const { return "Casualty-Level"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_CasualtyLevelReached);
#endif
     d_current = static_cast<CasualtyLevelReached*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class VictoryPage : public ConditionEditPage {
   SideVPTotal* d_current;
public:
   VictoryPage(ConditionEdit* p) : ConditionEditPage(p, victoryPageDlg) { d_current = 0; }
   const char* getTitle() const { return "VP Level"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_SideVPTotal);
#endif
     d_current = static_cast<SideVPTotal*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class NeutralTownPage : public ConditionEditPage {
   MovesIntoNeutral* d_current;
public:
   NeutralTownPage(ConditionEdit* p) : ConditionEditPage(p, neutralTownPageDlg) { d_current = 0; }
   const char* getTitle() const { return "Neutral Town"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_MovesIntoNeutral);
#endif
     d_current = static_cast<MovesIntoNeutral*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class DeathOfLeaderPage : public ConditionEditPage {
   LeaderIsDead* d_current;
public:
   DeathOfLeaderPage(ConditionEdit* p) : ConditionEditPage(p, deathOfLeaderPageDlg) { d_current = 0; }
   const char* getTitle() const { return "Death Of Leader"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_LeaderIsDead);
#endif
     d_current = static_cast<LeaderIsDead*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class DateReachedPage : public ConditionEditPage {
   DateReached* d_current;
public:
   DateReachedPage(ConditionEdit* p) : ConditionEditPage(p, dateReachedPageDlg) { d_current = 0; }
   const char* getTitle() const { return "Date-Reached"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_DateReached);
#endif
     d_current = static_cast<DateReached*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class NationAtWarPage : public ConditionEditPage {
   NationAtWar* d_current;
public:
   NationAtWarPage(ConditionEdit* p) : ConditionEditPage(p, nationAtWarPageDlg) { d_current = 0; }
   const char* getTitle() const { return "At War"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_NationAtWar);
#endif
     d_current = static_cast<NationAtWar*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class SideOwnsTownPage : public ConditionEditPage {
   SideOwnsTown* d_current;
public:
   SideOwnsTownPage(ConditionEdit* p) : ConditionEditPage(p, sideOwnsTownPageDlg) { d_current = 0; }
   const char* getTitle() const { return "Side Owns Town"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_SideOwnsTown);
#endif
     d_current = static_cast<SideOwnsTown*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class ArmisticeEndsPage : public ConditionEditPage {
   ArmisticeEnds* d_current;
public:
   ArmisticeEndsPage(ConditionEdit* p) : ConditionEditPage(p, armisticeEndsPageDlg) { d_current = 0; }
   const char* getTitle() const { return "Armistice Ends"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_ArmisticeEnds);
#endif
     d_current = static_cast<ArmisticeEnds*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class ArmisticeStartsPage : public ConditionEditPage {
   ArmisticeStarts* d_current;
public:
   ArmisticeStartsPage(ConditionEdit* p) : ConditionEditPage(p, MAKEINTRESOURCE(DLG_ARMISTICESTARTSPAGE)) { d_current = 0; }
   const char* getTitle() const { return "Armistice Starts"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ConditionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ConditionType::ID_ArmisticeStarts);
#endif
     d_current = static_cast<ArmisticeStarts*>(current);
   }

   ConditionType* getCurrentType() const { return d_current; }

   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};


class ActionEditPage : public ModelessDialog {
   DLGTEMPLATE* dialog;
protected:
   ConditionEdit* edit;       // Pointer to master data
public:
   ActionEditPage(ConditionEdit* p, const char* dlgName);

   DLGTEMPLATE* getDialog() const { return dialog; }
   virtual const char* getTitle() const = 0;
   HWND create();
   // void enable();
   // void disable();
   virtual void updateValues() = 0;
   virtual void initControls() = 0;
   virtual void enableControls() = 0;
   virtual void setCurrent(ActionType* current) = 0;
   virtual ActionType* getCurrentType() const = 0;
protected:
   void setPosition();

   CampaignData* getCampaignData() const { return edit->getCampaignData(); }
};

class SideHasVictoryPage : public ActionEditPage {
   SideHasVictory* d_current;
public:
   SideHasVictoryPage(ConditionEdit* p) :
     ActionEditPage(p, actionsVictoryPageDlg),
     d_current(0) {}

   const char* getTitle() const { return "Victory"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ActionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ActionType::ID_Victory);
#endif
     d_current = static_cast<SideHasVictory*>(current);
   }

   ActionType* getCurrentType() const { return d_current; }


   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class ArmisticeReachedPage : public ActionEditPage {
   ArmisticeReached* d_current;
public:
   ArmisticeReachedPage(ConditionEdit* p) :
     ActionEditPage(p, actionsArmisticePageDlg),
     d_current(0) {}

   const char* getTitle() const { return "Armistice"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ActionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ActionType::ID_Armistice);
#endif
     d_current = static_cast<ArmisticeReached*>(current);
   }

   ActionType* getCurrentType() const { return d_current; }


   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class NationEntersWarPage : public ActionEditPage {
   NationEntersWar* d_current;
public:
   NationEntersWarPage(ConditionEdit* p) :
     ActionEditPage(p, actionsNationEntersPageDlg),
     d_current(0) {}

   const char* getTitle() const { return "Nation Enters"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ActionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ActionType::ID_NationEntersWar);
#endif
     d_current = static_cast<NationEntersWar*>(current);
   }

   ActionType* getCurrentType() const { return d_current; }


   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class ReinforcementsArrivePage : public ActionEditPage {
   ReinforcementsArrive* d_current;
public:
   ReinforcementsArrivePage(ConditionEdit* p) :
     ActionEditPage(p, actionsNewUnitsPageDlg),
     d_current(0) {}

   const char* getTitle() const { return "New Unit"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ActionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ActionType::ID_Reinforcements);
#endif
     d_current = static_cast<ReinforcementsArrive*>(current);
   }

   ActionType* getCurrentType() const { return d_current; }


   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class LeaderLeavesPage : public ActionEditPage {
   LeaderLeaves* d_current;
public:
   LeaderLeavesPage(ConditionEdit* p) :
     ActionEditPage(p, actionsLeaderExitsPageDlg),
     d_current(0) {}

   const char* getTitle() const { return "Leader Exits"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ActionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ActionType::ID_LeaderLeaves);
#endif
     d_current = static_cast<LeaderLeaves*>(current);
   }

   ActionType* getCurrentType() const { return d_current; }


   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class LeaderEntersPage : public ActionEditPage {
   LeaderEnters* d_current;
public:
   LeaderEntersPage(ConditionEdit* p) :
     ActionEditPage(p, MAKEINTRESOURCE(DLG_LEADERENTERSPAGE)),
     d_current(0) {}

   const char* getTitle() const { return "Leader Enters"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ActionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ActionType::ID_LeaderEnters);
#endif
     d_current = static_cast<LeaderEnters*>(current);
   }

   ActionType* getCurrentType() const { return d_current; }


   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

class AllyDefectsPage : public ActionEditPage {
   AllyDefects* d_current;
public:
   AllyDefectsPage(ConditionEdit* p) :
     ActionEditPage(p, actionsAllyDefectsPageDlg),
     d_current(0) {}

   const char* getTitle() const { return "Ally Defects"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ActionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ActionType::ID_AllyDefects);
#endif
     d_current = static_cast<AllyDefects*>(current);
   }

   ActionType* getCurrentType() const { return d_current; }


   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
};

class AdjustResourcesPage : public ActionEditPage {
   AdjustResources* d_current;
public:
   AdjustResourcesPage(ConditionEdit* p) :
     ActionEditPage(p, actionsAdjustResourcesPageDlg),
     d_current(0) {}

   const char* getTitle() const { return "Adjust Resources"; }
private:
   void initControls();
   void updateValues();
   void enableControls();
   void setCurrent(ActionType* current)
   {
#ifdef DEBUG
     if(current != 0)
       ASSERT(current->getType() == ActionType::ID_AdjustResources);
#endif
     d_current = static_cast<AdjustResources*>(current);
   }

   ActionType* getCurrentType() const { return d_current; }


   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
};

/*
 * Implementation
 */

const int YearMin = 1700;
const int YearMax = 1999;

ConditionEdit::ConditionEdit(HWND parent, MWEditInterface* editControl, CampaignData* campData) :
  d_editControl(editControl),
  d_campData(campData),
  d_conditionList(&campData->getConditions()),
  d_currentCondition(0),
  d_conditionPages(0),
  d_curTypePage(ConditionTypePages::First),
  d_conditionTabHwnd(NULL),
  d_actionPages(0),
  d_curActionPage(ActionTypePages::First),
  d_actionTabHwnd(NULL)
{
  ASSERT(d_campData != 0);

//  d_conditionList = &d_campData->getConditions();
  ASSERT(d_conditionList != 0);

  d_currentCondition = d_conditionList->first();

  d_conditionPages = new ConditionEditPage*[ConditionTypePages::Pages_HowMany];
  ASSERT(d_conditionPages != 0);

  for(int i = 0; i < ConditionTypePages::Pages_HowMany; i++)
    d_conditionPages[i] = 0;

  SetRectEmpty(&d_conditionTDRect);

  d_actionPages = new ActionEditPage*[ActionTypePages::HowMany];
  ASSERT(d_actionPages != 0);

  for(i = 0; i < ActionTypePages::HowMany; i++)
    d_actionPages[i] = 0;

  SetRectEmpty(&d_actionTDRect);

  HWND hwnd = createDialog(conditionsEditDlg, parent);
  ASSERT(hwnd != 0);
}

ConditionEdit::ConditionEdit(HWND parent, MWEditInterface* editControl, CampaignData* campData, Conditions* condition) :
  d_editControl(editControl),
  d_campData(campData),
  d_conditionList(&campData->getConditions()),
  d_currentCondition(condition),
  d_conditionPages(0),
  d_curTypePage(ConditionTypePages::First),
  d_conditionTabHwnd(NULL),
  d_actionPages(0),
  d_curActionPage(ActionTypePages::First),
  d_actionTabHwnd(NULL)
{
  ASSERT(d_campData != 0);

//  d_conditionList = &d_campData->getConditions();
  ASSERT(d_conditionList != 0);

//  d_currentCondition = d_conditionList->first();

  d_conditionPages = new ConditionEditPage*[ConditionTypePages::Pages_HowMany];
  ASSERT(d_conditionPages != 0);

  for(int i = 0; i < ConditionTypePages::Pages_HowMany; i++)
    d_conditionPages[i] = 0;

  SetRectEmpty(&d_conditionTDRect);

  d_actionPages = new ActionEditPage*[ActionTypePages::HowMany];
  ASSERT(d_actionPages != 0);

  for(i = 0; i < ActionTypePages::HowMany; i++)
    d_actionPages[i] = 0;

  SetRectEmpty(&d_actionTDRect);

  HWND hwnd = createDialog(conditionsEditDlg, parent);
  ASSERT(hwnd != 0);
}

ConditionEdit::~ConditionEdit()
{
  if(d_conditionPages)
    delete[] d_conditionPages;

  if(d_actionPages)
    delete[] d_actionPages;
}

BOOL ConditionEdit::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_DESTROY:
         HANDLE_WM_DESTROY(hWnd, wParam, lParam, onDestroy);
         break;
      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;

      case WM_CLOSE:
         HANDLE_WM_CLOSE(hWnd, wParam, lParam, onClose);
         break;

      case WM_NOTIFY:
         HANDLE_WM_NOTIFY(hWnd, wParam, lParam, onNotify);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

BOOL ConditionEdit::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

   // Get the position of the box (convert from dialog units)

   LONG dbUnits = GetDialogBaseUnits();
   LONG dbX = LOWORD(dbUnits);
   LONG dbY = HIWORD(dbUnits);
   LONG xMargin = (2 * dbX) / 4;
   LONG xActionMargin = (180 * dbX) / 4;
   LONG yTop = (10 * dbY) / 8;        //was20, 33
   LONG yMargin = (1 * dbY) / 8;      //was4

   /*
    * Setup the ConditionType and ActionType tabbed dialogs
    */

   RECT rcTab;

   createConditionTypePages(xMargin, yTop, rcTab);
   createActionTypePages(xActionMargin, yTop, rcTab);

   /*
    * Move the lower buttons (to rcTab.bottom + a bit)
    */

   RECT rcButton;
   SetRect(&rcButton, 0, 0, 0, 0);
   LONG x = rcTab.left;
   LONG y = rcTab.bottom + yMargin;

   HWND hwndButton = GetDlgItem(hwnd, CNDE_ALLCONDITIONS);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

   x += 5 + (rcButton.right - rcButton.left);
   hwndButton = GetDlgItem(hwnd, CNDE_INSTANTUPDATEONLY);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

   y += rcButton.bottom-rcButton.top+2;
   hwndButton = GetDlgItem(hwnd, CNDE_PREV);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
   x += rcButton.right - rcButton.left + 2;

   hwndButton = GetDlgItem(hwnd, CNDE_NEXT);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
   x += rcButton.right - rcButton.left + 2;

   hwndButton = GetDlgItem(hwnd, CNDE_NEW);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
   x += rcButton.right - rcButton.left + 2;

   hwndButton = GetDlgItem(hwnd, CNDE_DELETE);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
   x += rcButton.right - rcButton.left + 2;

   hwndButton = GetDlgItem(hwnd, CNDE_OK);
   ASSERT(hwndButton != 0);
   GetWindowRect(hwndButton, &rcButton);
   SetWindowPos(hwndButton, NULL, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
   x += rcButton.right - rcButton.left + 2;


   /*
    * Adjust size of overall dialogue box
    *
    * Would be a good idea to adjust coordinates
    * so that box is on screen.
    *
    * e.g. if off right, adjust coords to be left of given position
    * if off bottom, adjust to be above.
    */

   rcTab.bottom = y + yMargin + rcButton.bottom - rcButton.top;
   rcTab.bottom += GetSystemMetrics(SM_CYCAPTION);
   rcTab.bottom += GetSystemMetrics(SM_CYDLGFRAME) * 2;
   // rcTab.bottom -= rcTab.top;

   rcTab.right += xMargin + 5; // * 2;
   rcTab.right += GetSystemMetrics(SM_CXDLGFRAME) * 2;
   rcTab.right -= rcTab.left;
   if(x > rcTab.right)
     rcTab.right += x-rcTab.right+xMargin;

#ifdef DEBUG
   debugLog("Overall Window: %ld,%ld,%ld,%ld\n",
      rcTab.left, rcTab.top, rcTab.right, rcTab.bottom);
#endif

   SetWindowPos(hwnd, NULL,
      100,
      100,
      rcTab.right,
      rcTab.bottom,
      SWP_NOZORDER);

   /*
    * Initialise pages
    */

   for(int i = 0; i < ConditionTypePages::Pages_HowMany; i++)
      d_conditionPages[i]->create();

   for(i = 0; i < ActionTypePages::HowMany; i++)
   {
      d_actionPages[i]->create();
   }

   /*
    * For testing: create an initial dialog
    */

   onTypeSelChanged();
   onActionSelChanged();

   initControls();
   enableControls();

   return TRUE;
}

void ConditionEdit::createConditionTypePages(LONG xMargin, LONG yTop, RECT& rcTab)
{
   /*
    * Set up a minimum size (this is in dialog units)
    */

   SetRect(&rcTab, 0, 0, 184, 140);

   /*
    * Alloc ConditionTypePages
    */

   d_conditionPages[ConditionTypePages::TownChangesSidePage] = new TownChangesPage(this);
   d_conditionPages[ConditionTypePages::CasualtyLevelReachedPage] = new CasualtyPage(this);
   d_conditionPages[ConditionTypePages::MovesIntoNeutralPage] = new NeutralTownPage(this);
   d_conditionPages[ConditionTypePages::LeaderIsDeadPage] = new DeathOfLeaderPage(this);
   d_conditionPages[ConditionTypePages::DateReachedPage] = new DateReachedPage(this);
   d_conditionPages[ConditionTypePages::NationAtWarPage] = new NationAtWarPage(this);
   d_conditionPages[ConditionTypePages::SideVPTotalPage] = new VictoryPage(this);
   d_conditionPages[ConditionTypePages::SideOwnsTownPage] = new SideOwnsTownPage(this);
   d_conditionPages[ConditionTypePages::ArmisticeEndsPage] = new ArmisticeEndsPage(this);
   d_conditionPages[ConditionTypePages::ArmisticeStartsPage] = new ArmisticeStartsPage(this);
   for(int i = 0; i < ConditionTypePages::Pages_HowMany; i++)
   {
      DLGTEMPLATE* dialog = d_conditionPages[i]->getDialog();

      ASSERT(dialog != NULL);

      /*
       * Adjust for maximum size
       */

      if(dialog->cx > rcTab.right)
         rcTab.right = dialog->cx;
      if(dialog->cy > rcTab.bottom)
         rcTab.bottom = dialog->cy;
   }

   // Convert to pixel coordinates

   MapDialogRect(getHWND(), &rcTab);


   /*
    * Create a tabbed window
    * Don't be concerened with the size, because that will
    * be set up in a little while, but we need a handle to
    * a tabbed control to use AdjustRect
    *
    * Do need to set up width, so that AdjustRect doesn't think
    * it needs to do several rows of buttons
    */

   d_conditionTabHwnd = CreateWindow(
      WC_TABCONTROL, "",
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
         TCS_TOOLTIPS | TCS_RIGHTJUSTIFY | TCS_MULTILINE,
      0, 0, rcTab.right, 100,
      getHWND(),
      (HMENU) CNDE_CONDITION_TABBED,
      APP::instance(),
      NULL);

   ASSERT(d_conditionTabHwnd != NULL);

   /*
    * Set up the tabbed titles
    * If possible try and get the title from the Dialog's CAPTION
    *
    * This must be done before using AdjustRect
    */

   TC_ITEM tie;
   tie.mask = TCIF_TEXT | TCIF_IMAGE;
   tie.iImage = -1;

   for(i = 0; i < ConditionTypePages::Pages_HowMany; i++)
   {
      tie.pszText = (char*) d_conditionPages[i]->getTitle();

      TabCtrl_InsertItem(d_conditionTabHwnd, i, &tie);
   }

   /*
    * Convert coordinates to pixel coordinates
    * and Move the window
    */

   // Work out complete size with tabs, and shift to desired location

   TabCtrl_AdjustRect(d_conditionTabHwnd, TRUE, &rcTab);

   OffsetRect(&rcTab, xMargin - rcTab.left, yTop - rcTab.top);

   // Move tabbed Window

   SetWindowPos(d_conditionTabHwnd, NULL,
      rcTab.left, rcTab.top,
      rcTab.right - rcTab.left, rcTab.bottom - rcTab.top,
      SWP_NOZORDER);

   // Get display area

   CopyRect(&d_conditionTDRect, &rcTab);
   TabCtrl_AdjustRect(d_conditionTabHwnd, FALSE, &d_conditionTDRect);      // Get display area
}

void ConditionEdit::createActionTypePages(LONG xMargin, LONG yTop, RECT& rcTab)
{
   /*
    * Set up a minimum size (this is in dialog units)
    */

   SetRect(&rcTab, 0, 0, 184, 140);

   /*
    * Alloc ConditionTypePages
    */

   d_actionPages[ActionTypePages::SideHasVictoryPage] = new SideHasVictoryPage(this);
   d_actionPages[ActionTypePages::ArmisticePage] = new ArmisticeReachedPage(this);
   d_actionPages[ActionTypePages::NationEntersWarPage] = new NationEntersWarPage(this);
   d_actionPages[ActionTypePages::ReinforcementsPage] = new ReinforcementsArrivePage(this);
   d_actionPages[ActionTypePages::LeaderLeavesPage] = new LeaderLeavesPage(this);
   d_actionPages[ActionTypePages::AllyDefectsPage] = new AllyDefectsPage(this);
   d_actionPages[ActionTypePages::AdjustResourcesPage] = new AdjustResourcesPage(this);
   d_actionPages[ActionTypePages::LeaderEntersPage] = new LeaderEntersPage(this);

   for(int i = 0; i < ActionTypePages::HowMany; i++)
   {
      DLGTEMPLATE* dialog = d_actionPages[i]->getDialog();

      ASSERT(dialog != NULL);

      /*
       * Adjust for maximum size
       */

      if(dialog->cx > rcTab.right)
         rcTab.right = dialog->cx;
      if(dialog->cy > rcTab.bottom)
         rcTab.bottom = dialog->cy;
   }

   MapDialogRect(getHWND(), &rcTab);

   /*
    * Create a tabbed window
    * Don't be concerened with the size, because that will
    * be set up in a little while, but we need a handle to
    * a tabbed control to use AdjustRect
    *
    * Do need to set up width, so that AdjustRect doesn't think
    * it needs to do several rows of buttons
    */

   d_actionTabHwnd = CreateWindow(
      WC_TABCONTROL, "",
      WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE |
         TCS_TOOLTIPS | TCS_RIGHTJUSTIFY | TCS_MULTILINE,
      0, 0, rcTab.right, 100,
      getHWND(),
      (HMENU) CNDE_ACTION_TABBED,
      APP::instance(),
      NULL);

   ASSERT(d_actionTabHwnd != NULL);

   /*
    * Set up the tabbed titles
    * If possible try and get the title from the Dialog's CAPTION
    *
    * This must be done before using AdjustRect
    */

   TC_ITEM tie;
   tie.mask = TCIF_TEXT | TCIF_IMAGE;
   tie.iImage = -1;

   for(i = 0; i < ActionTypePages::HowMany; i++)
   {
      tie.pszText = (char*) d_actionPages[i]->getTitle();

      TabCtrl_InsertItem(d_actionTabHwnd, i, &tie);
   }

   /*
    * Convert coordinates to pixel coordinates
    * and Move the window
    */

   // Work out complete size with tabs, and shift to desired location

   TabCtrl_AdjustRect(d_actionTabHwnd, TRUE, &rcTab);

   OffsetRect(&rcTab, xMargin - rcTab.left, yTop - rcTab.top);

   // Move tabbed Window

   SetWindowPos(d_actionTabHwnd, NULL,
      rcTab.left, rcTab.top,
      rcTab.right - rcTab.left, rcTab.bottom - rcTab.top,
      SWP_NOZORDER);

   // Get display area

   CopyRect(&d_actionTDRect, &rcTab);
   TabCtrl_AdjustRect(d_actionTabHwnd, FALSE, &d_actionTDRect);      // Get display area
}

int ConditionEdit::typeToPage(ConditionType::Type typeID)
{
  switch(typeID)
  {
    case ConditionType::ID_TownChangesSide:
      return ConditionTypePages::TownChangesSidePage;

    case ConditionType::ID_CasualtyLevelReached:
      return ConditionTypePages::CasualtyLevelReachedPage;

    case ConditionType::ID_SideVPTotal:
      return ConditionTypePages::SideVPTotalPage;

    case ConditionType::ID_MovesIntoNeutral:
      return ConditionTypePages::MovesIntoNeutralPage;

    case ConditionType::ID_LeaderIsDead:
      return ConditionTypePages::LeaderIsDeadPage;

    case ConditionType::ID_DateReached:
      return ConditionTypePages::DateReachedPage;

    case ConditionType::ID_NationAtWar:
      return ConditionTypePages::NationAtWarPage;

    case ConditionType::ID_SideOwnsTown:
      return ConditionTypePages::SideOwnsTownPage;

    case ConditionType::ID_ArmisticeEnds:
      return ConditionTypePages::ArmisticeEndsPage;

    case ConditionType::ID_ArmisticeStarts:
      return ConditionTypePages::ArmisticeStartsPage;
#ifdef DEBUG
//    default:
//      FORCEASSERT("Improper ConditionType ID");
#endif
  }
  return -1;
}

int ConditionEdit::actionTypeToPage(ActionType::Type typeID)
{
  switch(typeID)
  {
    case ActionType::ID_Victory:
      return ActionTypePages::SideHasVictoryPage;

    case ActionType::ID_Armistice:
      return ActionTypePages::ArmisticePage;

    case ActionType::ID_NationEntersWar:
      return ActionTypePages::NationEntersWarPage;

    case ActionType::ID_Reinforcements:
      return ActionTypePages::ReinforcementsPage;

    case ActionType::ID_LeaderLeaves:
      return ActionTypePages::LeaderLeavesPage;

    case ActionType::ID_AllyDefects:
      return ActionTypePages::AllyDefectsPage;

    case ActionType::ID_AdjustResources:
      return ActionTypePages::AdjustResourcesPage;

    case ActionType::ID_LeaderEnters:
      return ActionTypePages::LeaderEntersPage;
#ifdef DEBUG
//    default:
//      FORCEASSERT("Improper ActionType ID");
#endif
  }
  return -1;
}

void ConditionEdit::initControls()
{
  Boolean enable = False;

  if(d_currentCondition)
  {
    enable = d_currentCondition->allConditions();
    for(ConditionType::Type typeID = ConditionType::ID_First; typeID < ConditionType::Type_HowMany; INCREMENT(typeID))
    {
      int page = typeToPage(typeID);
      if(page != -1)
      {
         d_conditionPages[page]->setCurrent(getFirstType(typeID));
         d_conditionPages[page]->initControls();
      }
    }

    for(ActionType::Type actionID = ActionType::ID_First; actionID < ActionType::Type_HowMany; INCREMENT(actionID))
    {
      int page = actionTypeToPage(actionID);
      if(page != -1)
      {
         d_actionPages[page]->setCurrent(getFirstActionType(actionID));
         d_actionPages[page]->initControls();
      }
    }

    {
      Button b(getHWND(), CNDE_ALLCONDITIONS);
      b.setCheck(d_currentCondition->allConditions());
    }
    {
      Button b(getHWND(), CNDE_INSTANTUPDATEONLY);
      b.setCheck(d_currentCondition->instantOnly());
    }

  }

}

LRESULT ConditionEdit::onNotify(HWND hWnd, int id, NMHDR* lpNMHDR)
{

   // Assume its from the tabbed dialog

   if(lpNMHDR->hwndFrom == d_conditionTabHwnd)
   {
      switch(lpNMHDR->code)
      {
      case TCN_SELCHANGING:
         return FALSE;

      case TCN_SELCHANGE:
         onTypeSelChanged();
         break;
      }
   }

   if(lpNMHDR->hwndFrom == d_actionTabHwnd)
   {
      switch(lpNMHDR->code)
      {
      case TCN_SELCHANGING:
         return FALSE;

      case TCN_SELCHANGE:
         onActionSelChanged();
         break;
      }
   }

   return TRUE;
}

void ConditionEdit::onTypeSelChanged()
{
   ASSERT(d_conditionTabHwnd != NULL);
   int iSel = TabCtrl_GetCurSel(d_conditionTabHwnd);
   if(d_curTypePage != iSel)
      d_conditionPages[d_curTypePage]->show(false); // disable();

   d_conditionPages[iSel]->show(true);  // enable();
   d_curTypePage = static_cast<ConditionTypePages::Pages>(iSel);
}

void ConditionEdit::onActionSelChanged()
{
   ASSERT(d_actionTabHwnd != NULL);
   int iSel = TabCtrl_GetCurSel(d_actionTabHwnd);
   if(d_curActionPage != iSel)
      d_actionPages[d_curActionPage]->show(false);  // disable();

   d_actionPages[iSel]->show(true); // enable();
   d_curActionPage = static_cast<ActionTypePages::Pages>(iSel);
}

void ConditionEdit::onDestroy(HWND hwnd)
{
}

void ConditionEdit::onClose(HWND hwnd)
{
  d_editControl->editDestroyed(this);
  DestroyWindow(getHWND());
}

ConditionType::Type idToConditionType(int id)
{
  switch(id)
  {
    case CNDE_TC_NEW:
    case CNDE_TC_PREV:
    case CNDE_TC_NEXT:
    case CNDE_TC_DELETE:
      return ConditionType::ID_TownChangesSide;

    case CNDE_C_NEW:
    case CNDE_C_PREV:
    case CNDE_C_NEXT:
    case CNDE_C_DELETE:
      return ConditionType::ID_CasualtyLevelReached;

    case CNDE_V_NEW:
    case CNDE_V_PREV:
    case CNDE_V_NEXT:
    case CNDE_V_DELETE:
      return ConditionType::ID_SideVPTotal;

    case CNDE_NT_NEW:
    case CNDE_NT_PREV:
    case CNDE_NT_NEXT:
    case CNDE_NT_DELETE:
      return ConditionType::ID_MovesIntoNeutral;

    case CNDE_LD_NEW:
    case CNDE_LD_PREV:
    case CNDE_LD_NEXT:
    case CNDE_LD_DELETE:
      return ConditionType::ID_LeaderIsDead;

    case CNDE_DR_NEW:
    case CNDE_DR_PREV:
    case CNDE_DR_NEXT:
    case CNDE_DR_DELETE:
      return ConditionType::ID_DateReached;

    case CNDE_NW_NEW:
    case CNDE_NW_PREV:
    case CNDE_NW_NEXT:
    case CNDE_NW_DELETE:
      return ConditionType::ID_NationAtWar;

    case CNDE_SOT_NEW:
    case CNDE_SOT_PREV:
    case CNDE_SOT_NEXT:
    case CNDE_SOT_DELETE:
      return ConditionType::ID_SideOwnsTown;

    case CNDE_AE_NEW:
    case CNDE_AE_PREV:
    case CNDE_AE_NEXT:
    case CNDE_AE_DELETE:
      return ConditionType::ID_ArmisticeEnds;

    case CNDE_AS_NEW:
    case CNDE_AS_PREV:
    case CNDE_AS_NEXT:
    case CNDE_AS_DELETE:
      return ConditionType::ID_ArmisticeStarts;

#ifdef DEBUG
    default:
      FORCEASSERT("Improper ID in idTOConditionType");
#endif

  }

  return ConditionType::Type_HowMany;
}

ActionType::Type idToActionType(int id)
{
  switch(id)
  {
    case CNDE_ACTION_VP_NEW:
    case CNDE_ACTION_VP_DELETE:
      return ActionType::ID_Victory;

    case CNDE_ACTION_AR_NEW:
    case CNDE_ACTION_AR_DELETE:
      return ActionType::ID_Armistice;

    case CNDE_ACTION_NE_NEW:
    case CNDE_ACTION_NE_DELETE:
      return ActionType::ID_NationEntersWar;

    case CNDE_ACTION_RA_NEW:
    case CNDE_ACTION_RA_DELETE:
      return ActionType::ID_Reinforcements;

    case CNDE_ACTION_LE_NEW:
    case CNDE_ACTION_LE_DELETE:
      return ActionType::ID_LeaderLeaves;

    case CNDE_ACTION_AD_NEW:
    case CNDE_ACTION_AD_DELETE:
      return ActionType::ID_AllyDefects;

    case CNDE_ACTION_ADR_NEW:
    case CNDE_ACTION_ADR_DELETE:
      return ActionType::ID_AdjustResources;

    case CNDE_ACTION_ENTER_NEW:
    case CNDE_ACTION_ENTER_DELETE:
      return ActionType::ID_LeaderEnters;
#ifdef DEBUG
    default:
      FORCEASSERT("Improper ID in idTOActionType");
#endif

  }

  return ActionType::Type_HowMany;
}

void ConditionEdit::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case CNDE_PREV:
      onPrev();
      break;

    case CNDE_NEXT:
      onNext();
      break;

    case CNDE_NEW:
      onNewCondition();
      break;

    case CNDE_DELETE:
      onDelete();
      break;

    case CNDE_OK:
      onOK();
      break;

    case CNDE_TC_NEW:
    case CNDE_C_NEW:
    case CNDE_V_NEW:
    case CNDE_NT_NEW:
    case CNDE_LD_NEW:
    case CNDE_DR_NEW:
    case CNDE_NW_NEW:
    case CNDE_SOT_NEW:
    case CNDE_AE_NEW:
    case CNDE_AS_NEW:
      onNewConditionType(idToConditionType(id));
      break;

    case CNDE_TC_PREV:
    case CNDE_C_PREV:
    case CNDE_V_PREV:
    case CNDE_NT_PREV:
    case CNDE_LD_PREV:
    case CNDE_DR_PREV:
    case CNDE_NW_PREV:
    case CNDE_SOT_PREV:
    case CNDE_AE_PREV:
    case CNDE_AS_PREV:
      onPrevType(idToConditionType(id));
      break;

    case CNDE_TC_NEXT:
    case CNDE_C_NEXT:
    case CNDE_V_NEXT:
    case CNDE_NT_NEXT:
    case CNDE_LD_NEXT:
    case CNDE_DR_NEXT:
    case CNDE_NW_NEXT:
    case CNDE_SOT_NEXT:
    case CNDE_AE_NEXT:
    case CNDE_AS_NEXT:
      onNextType(idToConditionType(id));
      break;

    case CNDE_TC_DELETE:
    case CNDE_C_DELETE:
    case CNDE_V_DELETE:
    case CNDE_NT_DELETE:
    case CNDE_LD_DELETE:
    case CNDE_DR_DELETE:
    case CNDE_NW_DELETE:
    case CNDE_SOT_DELETE:
    case CNDE_AE_DELETE:
    case CNDE_AS_DELETE:
      onDeleteType(idToConditionType(id));
      break;

    case CNDE_ACTION_VP_NEW:
    case CNDE_ACTION_AR_NEW:
    case CNDE_ACTION_NE_NEW:
    case CNDE_ACTION_RA_NEW:
    case CNDE_ACTION_LE_NEW:
    case CNDE_ACTION_AD_NEW:
    case CNDE_ACTION_ADR_NEW:
    case CNDE_ACTION_ENTER_NEW:
      onNewActionType(idToActionType(id));
      break;

    case CNDE_ACTION_VP_DELETE:
    case CNDE_ACTION_AR_DELETE:
    case CNDE_ACTION_NE_DELETE:
    case CNDE_ACTION_RA_DELETE:
    case CNDE_ACTION_LE_DELETE:
    case CNDE_ACTION_AD_DELETE:
    case CNDE_ACTION_ADR_DELETE:
    case CNDE_ACTION_ENTER_DELETE:
      onDeleteActionType(idToActionType(id));
      break;

#if 0
    case CNDE_ACTIONS_NATIONENTERS:
      if(d_pages)
        d_pages[ID_ActionsPage]->enableControls();
      break;
#endif

  }
}

void ConditionEdit::kill()
{
  DestroyWindow(getHWND());
}

void ConditionEdit::enableControls()
{
  Boolean hasCondition = static_cast<Boolean>(d_currentCondition != 0);
  Boolean hasNext = False;
  Boolean hasPrev = False;

  if(hasCondition)
  {
    Conditions* node = d_conditionList->first();
    while(node)
    {
      if(node == d_currentCondition)
      {
        hasPrev = static_cast<Boolean>( (d_conditionList->prev() &&
                                         d_conditionList->prev() != d_currentCondition &&
                                         d_conditionList->prev() != d_conditionList->getLast()) );
        break;
      }

      node = d_conditionList->next();
    }

    hasNext = static_cast<Boolean>(d_conditionList->next(d_currentCondition) != 0);
  }

  {
    Button b(getHWND(), CNDE_ALLCONDITIONS);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_PREV);
    b.enable(hasPrev);
  }

  {
    Button b(getHWND(), CNDE_NEXT);
    b.enable(hasNext);
  }

  {
    Button b(getHWND(), CNDE_DELETE);
    b.enable(hasCondition);
  }

  for(int i = 0; i < ConditionTypePages::Pages_HowMany; i++)
  {
    d_conditionPages[i]->enableControls();
  }

  for(i = 0; i < ActionTypePages::HowMany; i++)
  {
    d_actionPages[i]->enableControls();
  }
}


void ConditionEdit::getConditionTypeFlags(ConditionType::Type typeID, Boolean& hasCondition, Boolean& hasType)
{
  hasCondition = static_cast<Boolean>(d_currentCondition != 0);
  hasType = False;

  if(hasCondition)
  {
    ConditionTypeList& tl = d_currentCondition->getTypes();

    SListIter<ConditionType> iter(&tl);

    while(++iter)
    {
      ConditionType* type = iter.current();

      if(type->getType() == typeID)
      {
        hasType = True;
        break;
      }
    }
  }
}

Boolean ConditionEdit::hasNext(ConditionType::Type typeID, ConditionType* current)
{
  if(!d_currentCondition)
    return False;

  if(!current)
    return False;

  SListIter<ConditionType> iter(&d_currentCondition->getTypes());
  Boolean after = False;
  while(++iter)
  {
    ConditionType* item = iter.current();
    if(item->getType() == typeID)
    {
      if(after)
      {
        ASSERT(current != item);
        return True;
      }

      if(current == item)
        after = True;
    }
  }

  return False;
}

Boolean ConditionEdit::hasPrev(ConditionType::Type typeID, ConditionType* current)
{
  if(!d_currentCondition)
    return False;

  if(!current)
    return False;

  SListIter<ConditionType> iter(&d_currentCondition->getTypes());
  while(++iter)
  {
    ConditionType* item = iter.current();
    if(item->getType() == typeID)
    {
      return static_cast<Boolean>(current != item);
    }
  }

  return False;
}


void ConditionEdit::enableTypeControl(ConditionType::Type typeID)
{
  if(d_conditionPages)
  {
     int page = typeToPage(typeID);
     if(page != -1)
        d_conditionPages[page]->enableControls();
  }
}

void ConditionEdit::setCurrentType(ConditionType::Type typeID, ConditionType* type)
{
  ASSERT(type != 0);
  if(d_conditionPages)
  {
    int page = typeToPage(typeID);
    if(page != -1)
    {   
       d_conditionPages[page]->updateValues();
       d_conditionPages[page]->setCurrent(type);
       d_conditionPages[page]->initControls();
    }
  }
}

void ConditionEdit::getActionTypeFlags(ActionType::Type typeID, Boolean& hasCondition, Boolean& hasType)
{
  hasCondition = static_cast<Boolean>(d_currentCondition != 0);
  hasType = False;

  if(hasCondition)
  {
    ActionTypeList& al = d_currentCondition->getActions();

    SListIter<ActionType> iter(&al);

    while(++iter)
    {
      ActionType* type = iter.current();

      if(type->getType() == typeID)
      {
        hasType = True;
        break;
      }
    }
  }
}

void ConditionEdit::setCurrentActionType(ActionType::Type typeID, ActionType* type)
{
  ASSERT(type != 0);
  if(d_actionPages)
  {
    int page = actionTypeToPage(typeID);
    if(page != -1)
    {
      d_actionPages[page]->updateValues();
      d_actionPages[page]->setCurrent(type);
      d_actionPages[page]->initControls();
    }
  }
}

void ConditionEdit::onNewCondition()
{
  if(d_currentCondition)
    update();

  d_currentCondition = new Conditions;
  ASSERT(d_currentCondition != 0);

  d_conditionList->append(d_currentCondition);

  initControls();
  enableControls();
}

void ConditionEdit::onNewConditionType(ConditionType::Type typeID)
{
  ASSERT(d_currentCondition != 0);

  if(d_currentCondition)
  {
    ConditionTypeList& types = d_currentCondition->getTypes();

    ConditionType* newType = ConditionTypeAllocator::newConditionType(typeID);
    ASSERT(newType != 0);

    types.append(newType);
    setCurrentType(typeID, newType);
  }

}

void ConditionEdit::onNewActionType(ActionType::Type typeID)
{
  ASSERT(d_currentCondition != 0);

  if(d_currentCondition)
  {
    ActionTypeList& types = d_currentCondition->getActions();

    ActionType* newType = ActionTypeAllocator::newActionType(typeID);
    ASSERT(newType != 0);

    types.append(newType);
    setCurrentActionType(typeID, newType);
  }

}

void ConditionEdit::onPrev()
{
  ASSERT(d_currentCondition != 0);

  if(d_currentCondition)
  {
    update();
    Conditions* node = d_conditionList->first();
    while(node)
    {
      if(node == d_currentCondition)
      {
        d_currentCondition = d_conditionList->prev();
        ASSERT(d_currentCondition != 0);
        break;
      }

      node = d_conditionList->next();
    }
    initControls();
    enableControls();
  }
}

void ConditionEdit::onNext()
{
  ASSERT(d_currentCondition != 0);

  if(d_currentCondition)
  {
    update();

    d_currentCondition = d_conditionList->next(d_currentCondition);
    ASSERT(d_currentCondition != 0);

    initControls();

    enableControls();
  }
}

void ConditionEdit::onNextType(ConditionType::Type typeID)
{
  ASSERT(d_currentCondition != 0);

  if(d_currentCondition)
  {
    int page = typeToPage(typeID);
    if(page != -1)
    {
      ConditionType* type = d_conditionPages[page]->getCurrentType();
      ASSERT(type != 0);

      if(type)
      {
         ConditionTypeList& list = d_currentCondition->getTypes();

         ConditionType* next = list.next(type);
         ASSERT(next != 0);

         setCurrentType(typeID, next);
      }
    }
  }
}

void ConditionEdit::onPrevType(ConditionType::Type typeID)
{
   ASSERT(d_currentCondition != 0);

   if(d_currentCondition)
   {
      int page = typeToPage(typeID);
      if(page != -1)
      {  
         ConditionType* type = d_conditionPages[page]->getCurrentType();
         ASSERT(type != 0);

         if(type)
         {

            ConditionTypeList& list = d_currentCondition->getTypes();

            ConditionType* prev = list.prev(type);
            ASSERT(prev != 0);

            while(prev && prev->getType() != typeID)
            {
               prev = list.prev(prev);
               ASSERT(prev != 0);
            }
            setCurrentType(typeID, prev);
         }
      }
   }
}

void ConditionEdit::onOK()
{
  update();
  d_campData->setChanged();
  onClose(getHWND());
}

void ConditionEdit::update()
{
  if(d_currentCondition)
  {
    {
      Button b(getHWND(), CNDE_ALLCONDITIONS);
      d_currentCondition->allConditions(static_cast<Boolean>(b.getCheck()));
    }

    {
      Button b(getHWND(), CNDE_INSTANTUPDATEONLY);
      d_currentCondition->instantOnly(static_cast<Boolean>(b.getCheck()));
    }

    for(int i = 0; i < ConditionTypePages::Pages_HowMany; i++)
    {
      d_conditionPages[i]->updateValues();
    }

    for(i = 0; i < ActionTypePages::HowMany; i++)
    {
      d_actionPages[i]->updateValues();
    }
  }
}


ActionTypeList* ConditionEdit::getCurrentActions() const
{
  if(d_currentCondition)
  {
    return &d_currentCondition->getActions();
  }

  return 0;
}

ConditionTypeList* ConditionEdit::getTypes() const
{
  if(d_currentCondition)
  {
    return &d_currentCondition->getTypes();
  }
  return 0;
}

ConditionType* ConditionEdit::getFirstType(ConditionType::Type typeID)
{
  if(d_currentCondition)
  {
    SListIter<ConditionType> iter(&d_currentCondition->getTypes());
    while(++iter)
    {
      ConditionType* item = iter.current();
      if(item->getType() == typeID)
        return item;
    }
  }

  return 0;
}

ActionType* ConditionEdit::getFirstActionType(ActionType::Type typeID)
{
  if(d_currentCondition)
  {
    SListIter<ActionType> iter(&d_currentCondition->getActions());
    while(++iter)
    {
      ActionType* item = iter.current();
      if(item->getType() == typeID)
        return item;

    }

  }
  return 0;
}

void ConditionEdit::onDelete()
{
  if(d_currentCondition)
  {
    d_conditionList->remove(d_currentCondition);
    d_currentCondition = d_conditionList->first();

    initControls();
  }
}

void ConditionEdit::onDeleteType(ConditionType::Type typeID)
{
  ASSERT(d_currentCondition != 0);

  if(d_currentCondition)
  {
    int page = typeToPage(typeID);
    if(page != -1)
    {
      ConditionType* type = d_conditionPages[page]->getCurrentType();
      ASSERT(type != 0);

      ConditionTypeList& list = d_currentCondition->getTypes();

      list.remove(type);

      d_conditionPages[page]->setCurrent(getFirstType(typeID));
      d_conditionPages[page]->initControls();
    }
//  d_pages[page]->enableControls();
  }
}

void ConditionEdit::onDeleteActionType(ActionType::Type typeID)
{
  ASSERT(d_currentCondition != 0);

  if(d_currentCondition)
  {
    int page = actionTypeToPage(typeID);
    if(page != -1)
    {  
      ActionType* type = d_actionPages[page]->getCurrentType();
      ASSERT(type != 0);

      ActionTypeList& list = d_currentCondition->getActions();

      list.remove(type);

      d_actionPages[page]->setCurrent(getFirstActionType(typeID));
      d_actionPages[page]->initControls();
    }
  }
}

/*=================================================================
 * Sub-Dialog Implementation
 */

ConditionEditPage::ConditionEditPage(ConditionEdit* p, const char* dlgName)
{
   edit = p;

   HRSRC rhTep = FindResource(NULL, dlgName, RT_DIALOG);
   ASSERT(rhTep != NULL);
   HGLOBAL lphTep = (HGLOBAL) LoadResource(NULL, rhTep);
   ASSERT(lphTep != NULL);
   dialog = (LPDLGTEMPLATE) LockResource(lphTep);
   ASSERT(dialog != NULL);

}

HWND ConditionEditPage::create()
{
   ASSERT(hWnd == NULL);      // Already created?

   HWND hDialog = createDialogIndirect(dialog, edit->getHWND());

   ASSERT(hDialog != NULL);

   return hDialog;
}

// void ConditionEditPage::enable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_SHOW);
// }
// 
// void ConditionEditPage::disable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_HIDE);
// }


void ConditionEditPage::setPosition()
{
   const RECT& r = edit->getConditionDialogRect();

   SetWindowPos(getHWND(), HWND_TOP, r.left, r.top, r.right-r.left, r.bottom-r.top, 0);
}

//============================= TownChangesPage implementation ==============

BOOL TownChangesPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void TownChangesPage::updateValues()
{
  if(d_current)
  {
    ComboBox townCB(getHWND(), CNDE_TOWNNAME);
    d_current->setTown(static_cast<ITown>(townCB.getValue()));

    ComboBox sideCB(getHWND(), CNDE_TOWNSIDE);
    d_current->setSide(static_cast<Side>(sideCB.getValue()));
  }
}

void TownChangesPage::initControls()
{

  ComboBox townCB(getHWND(), CNDE_TOWNNAME);
  ASSERT(townCB.getNItems() > 0);

  ComboBox sideCB(getHWND(), CNDE_TOWNSIDE);
  ASSERT(sideCB.getNItems() > 0);

  if(d_current)
  {
    townCB.setValue(d_current->getTown());
    sideCB.setValue(d_current->getSide());
  }
  else
  {
    townCB.set(0);
    sideCB.set(0);
  }

  enableControls();
}

void TownChangesPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_TownChangesSide, hasCondition, hasType);
  {
    ComboBox b(getHWND(), CNDE_TOWNNAME);
    b.enable(hasType);
  }

  {
    ComboBox b(getHWND(), CNDE_TOWNSIDE);
    b.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_TC_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_TownChangesSide, d_current));
  }

  {
    Button b(getHWND(), CNDE_TC_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_TownChangesSide, d_current));
  }

  {
    Button b(getHWND(), CNDE_TC_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_TC_DELETE);
    b.enable(hasType);
  }

}

BOOL TownChangesPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  /*
   *  Add towns to town combo, sorted by province
   */

  ComboBox townCB(getHWND(), CNDE_TOWNNAME);
  townCB.add("No Town", NoTown);

  ProvinceList& provinces = edit->getCampaignData()->getProvinces();

  for(IProvince pIndex = 0; pIndex < provinces.entries(); pIndex++)
  {
    Province& prov = provinces[pIndex];

    UWORD nTowns = prov.getNTowns();

    ITown stop = ITown(prov.getTown() + nTowns);

    for(ITown tIndex = prov.getTown(); tIndex < stop; tIndex++)
    {
      const Town& town = edit->getCampaignData()->getTown(tIndex);

      char buf[300];
      wsprintf(buf, "%s, %s", town.getNameNotNull(), prov.getNameNotNull());

      townCB.add(buf, tIndex);
    }
  }

  /*
   *  Add sides to side combo
   */

  ComboBox sideCB(getHWND(), CNDE_TOWNSIDE);
  sideCB.add("Neutral Side", SIDE_Neutral);

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    sideCB.add(scenario->getSideName(s), s);
  }

  setPosition();
  return TRUE;
}

//============================= CasualtyPage implementation ==============

BOOL CasualtyPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void CasualtyPage::updateValues()
{
  if(d_current)
  {
    BOOL result;
    d_current->setSide0Casualties(static_cast<SPCount>(GetDlgItemInt(getHWND(), CNDE_C_SIDE0CASUALTIES, &result, FALSE)));
    ASSERT(result);

    d_current->setSide1Casualties(static_cast<SPCount>(GetDlgItemInt(getHWND(), CNDE_C_SIDE1CASUALTIES, &result, FALSE)));
    ASSERT(result);
  }
}

void CasualtyPage::initControls()
{
  int casualties = d_current ? d_current->getSide0Casualties() : 0;

  BOOL result = SetDlgItemInt(getHWND(), CNDE_C_SIDE0CASUALTIES, casualties, FALSE);
  ASSERT(result);

  casualties = d_current ? d_current->getSide1Casualties() : 0;
  result = SetDlgItemInt(getHWND(), CNDE_C_SIDE1CASUALTIES, casualties, FALSE);
  ASSERT(result);

  enableControls();
}

void CasualtyPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_CasualtyLevelReached, hasCondition, hasType);

  {
    WindowControl c(getHWND(), CNDE_C_SIDE0CASUALTIES);
    c.enable(hasType);
  }

  {
    WindowControl c(getHWND(), CNDE_C_SIDE1CASUALTIES);
    c.enable(hasType);
  }


  {
    Button b(getHWND(), CNDE_C_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_CasualtyLevelReached, d_current));
  }

  {
    Button b(getHWND(), CNDE_C_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_CasualtyLevelReached, d_current));
  }

  {
    Button b(getHWND(), CNDE_C_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_C_DELETE);
    b.enable(hasType);
  }


}

BOOL CasualtyPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

  char buf[100];

  wsprintf(buf, "%s Casualties", scenario->getSideName(0));
  BOOL result = SetDlgItemText(getHWND(), CNDE_C_SIDE0NAME, buf);
  ASSERT(result);

  wsprintf(buf, "%s Casualties", scenario->getSideName(1));
  result = SetDlgItemText(getHWND(), CNDE_C_SIDE1NAME, buf);
  ASSERT(result);

  setPosition();

  return TRUE;
}

//============================= VictoryPage implementation ==============

BOOL VictoryPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

void VictoryPage::updateValues()
{
  if(d_current)
  {
    ComboBox cb(getHWND(), CNDE_V_SIDE0NAME);

    d_current->setSide(static_cast<Side>(cb.getValue()));

    BOOL result;
    d_current->setVPTotal(static_cast<UWORD>(GetDlgItemInt(getHWND(), CNDE_V_SIDE0VICTORY, &result, FALSE)));
    ASSERT(result);
  }
}

void VictoryPage::initControls()
{
  ComboBox cb(getHWND(), CNDE_V_SIDE0NAME);
  int value = 300;

  if(d_current)
  {
    cb.setValue(d_current->getSide());
    value = d_current->getVPTotal();
  }
  else
  {
    cb.set(0);
  }

  SetDlgItemInt(getHWND(), CNDE_V_SIDE0VICTORY, value, FALSE);

  enableControls();
}

void VictoryPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_SideVPTotal, hasCondition, hasType);

  {
    ComboBox cb(getHWND(), CNDE_V_SIDE0NAME);
    cb.enable(hasType);
  }

  {
    WindowControl c(getHWND(), CNDE_V_SIDE0VICTORY);
    c.enable(hasType);
  }


  {
    Button b(getHWND(), CNDE_V_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_SideVPTotal, d_current));
  }

  {
    Button b(getHWND(), CNDE_V_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_SideVPTotal, d_current));
  }

  {
    Button b(getHWND(), CNDE_V_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_V_DELETE);
    b.enable(hasType);
  }
}

BOOL VictoryPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  ComboBox cb(hwnd, CNDE_V_SIDE0NAME);

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    cb.add(scenario->getSideName(s), s);
  }

  cb.add("Neutral", SIDE_Neutral);

  setPosition();
  return TRUE;
}

//============================= NationAtWarPage implementation ==============

BOOL NationAtWarPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void NationAtWarPage::updateValues()
{
  if(d_current)
  {
    ComboBox cb(getHWND(), CNDE_NW_NATION);
    d_current->setNation(static_cast<Nationality>(cb.getValue()));

    Button b(getHWND(), CNDE_NW_ATWAR);
    d_current->atWar(b.getCheck());
  }
}

void NationAtWarPage::initControls()
{
  ComboBox cb(getHWND(), CNDE_NW_NATION);
  Button b(getHWND(), CNDE_NW_ATWAR);

  if(d_current)
  {
    cb.setValue(d_current->getNation());
    b.setCheck(d_current->atWar());
  }
  else
  {
    cb.set(0);
    b.setCheck(True);
  }
  enableControls();
}

void NationAtWarPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_NationAtWar, hasCondition, hasType);

  {
    ComboBox cb(getHWND(), CNDE_NW_NATION);
    cb.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_NW_ATWAR);
    b.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_NW_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_NationAtWar, d_current));
  }

  {
    Button b(getHWND(), CNDE_NW_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_NationAtWar, d_current));
  }

  {
    Button b(getHWND(), CNDE_NW_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_NW_DELETE);
    b.enable(hasType);
  }

}

BOOL NationAtWarPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  ComboBox cb(hwnd, CNDE_NW_NATION);

  for(Nationality n = 0; n < scenario->getNumNations(); n++)
  {
    cb.add(scenario->getNationName(n), n);
  }

  cb.add("Neutral", NATION_Neutral);

  setPosition();

  return TRUE;
}

//============================= NeutralTownPage implementation ==============

BOOL NeutralTownPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

void NeutralTownPage::updateValues()
{
  if(d_current)
  {
    ComboBox nationCB(getHWND(), CNDE_NT_NATION);
    d_current->setNation(static_cast<Nationality>(nationCB.getValue()));

    ComboBox sideCB(getHWND(), CNDE_NT_SIDE);
    d_current->setSide(static_cast<Side>(sideCB.getValue()));
  }
}

void NeutralTownPage::initControls()
{

  ComboBox nationCB(getHWND(), CNDE_NT_NATION);
  ASSERT(nationCB.getNItems() > 0);

  ComboBox sideCB(getHWND(), CNDE_NT_SIDE);
  ASSERT(sideCB.getNItems() > 0);

  if(d_current)
  {
    nationCB.setValue(d_current->getNation());
    sideCB.setValue(d_current->getSide());
  }
  else
  {
    nationCB.set(0);
    sideCB.set(0);
  }

  enableControls();
}

void NeutralTownPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_MovesIntoNeutral, hasCondition, hasType);

  {
    ComboBox b(getHWND(), CNDE_NT_NATION);
    b.enable(hasType);
  }

  {
    ComboBox b(getHWND(), CNDE_NT_SIDE);
    b.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_NT_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_MovesIntoNeutral, d_current));
  }

  {
    Button b(getHWND(), CNDE_NT_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_MovesIntoNeutral, d_current));
  }

  {
    Button b(getHWND(), CNDE_NT_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_NT_DELETE);
    b.enable(hasType);
  }


}

BOOL NeutralTownPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  /*
   * Add neutral nations to combo
   */

  ComboBox nationCB(getHWND(), CNDE_NT_NATION);
  nationCB.add("No Nation", NATION_Neutral);

  for(Nationality n = 0; n < scenario->getNumNations(); n++)
  {
    nationCB.add(scenario->getNationName(n), n);
  }

  /*
   * Add sides to combo
   */

  ComboBox sideCB(getHWND(), CNDE_NT_SIDE);
  sideCB.add("No Side", SIDE_Neutral);

  for(Side s = 0; s < scenario->getNumSides(); s++)
  {
    sideCB.add(scenario->getSideName(s), s);
  }

  setPosition();

  return TRUE;
}

//============================= DeathOfLeaderPage implementation ==============

BOOL DeathOfLeaderPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

void DeathOfLeaderPage::updateValues()
{
  if(d_current)
  {
    ComboBox b(getHWND(), CNDE_LD_LEADER);
    // d_current->setLeader(static_cast<ILeader>(b.getValue()));
    d_current->setLeader(lparamToLeader(b.getValue()));
  }
}

void DeathOfLeaderPage::initControls()
{

  ComboBox leaderCB(getHWND(), CNDE_LD_LEADER);
  ASSERT(leaderCB.getNItems() > 0);

  if(d_current)
  {
    // leaderCB.setValue(d_current->getLeader());
    leaderCB.setValue(leaderToLPARAM(d_current->getLeader()));
  }
  else
    leaderCB.set(0);

  enableControls();
}

void DeathOfLeaderPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_LeaderIsDead, hasCondition, hasType);

  {
    ComboBox b(getHWND(), CNDE_LD_LEADER);
    b.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_LD_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_LeaderIsDead, d_current));
  }

  {
    Button b(getHWND(), CNDE_LD_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_LeaderIsDead, d_current));
  }

  {
    Button b(getHWND(), CNDE_LD_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_LD_DELETE);
    b.enable(hasType);
  }


}

BOOL DeathOfLeaderPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  /*
   * Add leaders to combo
   */

  ComboBox leaderCB(getHWND(), CNDE_LD_LEADER);
  // leaderCB.add("No Leader", NoLeader);
  leaderCB.add("No Leader", leaderToLPARAM(NoLeader));

  CPIter iter(&edit->getCampaignData()->getArmies());

  while(iter.next())
  {
    CommandPosition* cp = iter.currentCommand();
    Leader* leader = edit->getCampaignData()->getArmies().getUnitLeader(iter.current());

    char buf[200];
    wsprintf(buf, "%s(%s)", leader->getNameNotNull(), cp->getNameNotNull());

    // leaderCB.add(buf, cp->getLeader());
    leaderCB.add(buf, leaderToLPARAM(cp->getLeader()));
  }

  setPosition();

  return TRUE;
}

//============================= DateReachedPage implementation ==============

BOOL DateReachedPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

void DateReachedPage::updateValues()
{
  if(d_current)
  {
    Spinner daySpin(getHWND(), CNDE_DR_DAYSPIN);
    Day d = static_cast<Day>(daySpin.getValue());

    Spinner yearSpin(getHWND(), CNDE_DR_YEARSPIN);
    Year y = static_cast<Year>(yearSpin.getValue());

    ComboBox monthCB(getHWND(), CNDE_DR_MONTH);
    Month m = static_cast<Month>(monthCB.getValue());

    d_current->setDate(d, m, y);

    Button b(getHWND(), CNDE_DR_HASTOREACH);
    d_current->mustReach(static_cast<Boolean>(b.getCheck()));
  }
}

void DateReachedPage::initControls()
{

  Spinner daySpin(getHWND(), CNDE_DR_DAYSPIN);
  Spinner yearSpin(getHWND(), CNDE_DR_YEARSPIN);
  ComboBox monthCB(getHWND(), CNDE_DR_MONTH);
  ASSERT(monthCB.getNItems() > 0);
  Button b(getHWND(), CNDE_DR_HASTOREACH);

  if(d_current)
  {
    Date& date = d_current->getDate();

    daySpin.setValue(date.day);
    yearSpin.setValue(date.year);
    monthCB.setValue(date.month);
    b.setCheck(d_current->mustReach());
  }
  else
  {
    daySpin.setValue(1);
    yearSpin.setValue(1813);
    monthCB.set(0);
    b.setCheck(True);
  }

  enableControls();
}

void DateReachedPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_DateReached, hasCondition, hasType);

  for(int i = CNDE_DR_DAY; i < CNDE_DR_HASTOREACH+1; i++)
  {
    WindowControl c(getHWND(), i);
    c.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_DR_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_DateReached, d_current));
  }

  {
    Button b(getHWND(), CNDE_DR_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_DateReached, d_current));
  }

  {
    Button b(getHWND(), CNDE_DR_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_DR_DELETE);
    b.enable(hasType);
  }


}

BOOL DateReachedPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  Spinner daySpin(getHWND(), CNDE_DR_DAYSPIN);
  daySpin.setRange(1, 31);

  Spinner yearSpin(getHWND(), CNDE_DR_YEARSPIN);
  yearSpin.setRange(YearMin, YearMax);

  ComboBox monthCB(getHWND(), CNDE_DR_MONTH);
  for(DateDefinitions::MonthEnum m = DateDefinitions::January; m <= DateDefinitions::December; INCREMENT(m))
  {
    monthCB.add(getMonthName(m, False), m);
  }

  Button b(getHWND(), CNDE_DR_HASTOREACH);
  b.setCheck(True);

  setPosition();

  return TRUE;
}

//============================= SideOwnsToPage implementation ==============

BOOL SideOwnsTownPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

void SideOwnsTownPage::updateValues()
{
  if(d_current)
  {
    {
      ComboBox b(getHWND(), CNDE_SOT_SIDE);
      d_current->setSide(static_cast<Side>(b.getValue()));
    }

    {
      ComboBox b(getHWND(), CNDE_SOT_TOWN);
      d_current->setTown(static_cast<ITown>(b.getValue()));
    }
  }
}

void SideOwnsTownPage::initControls()
{
  if(d_current)
  {
    {
      ComboBox b(getHWND(), CNDE_SOT_SIDE);
      b.setValue(d_current->getSide());
    }

    {
      ComboBox b(getHWND(), CNDE_SOT_TOWN);
      b.setValue(d_current->getTown());
    }
    enableControls();
  }
}

void SideOwnsTownPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_SideOwnsTown, hasCondition, hasType);

  {
    ComboBox b(getHWND(), CNDE_SOT_SIDE);
    b.enable(hasType);
  }

  {
    ComboBox b(getHWND(), CNDE_SOT_TOWN);
    b.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_SOT_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_SideOwnsTown, d_current));
  }

  {
    Button b(getHWND(), CNDE_SOT_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_SideOwnsTown, d_current));
  }

  {
    Button b(getHWND(), CNDE_SOT_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_SOT_DELETE);
    b.enable(hasType);
  }
}

BOOL SideOwnsTownPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  {
    ComboBox cb(hwnd, CNDE_SOT_SIDE);

    cb.add("Neutral", SIDE_Neutral);

    for(Side s = 0; s < scenario->getNumSides(); s++)
    {
      cb.add(scenario->getSideName(s), s);
    }
  }

  {
    ComboBox cb(hwnd, CNDE_SOT_TOWN);

    cb.add("No Town", NoTown);

    ProvinceList& provinces = edit->getCampaignData()->getProvinces();

    for(IProvince pIndex = 0; pIndex < provinces.entries(); pIndex++)
    {
      Province& prov = provinces[pIndex];

      UWORD nTowns = prov.getNTowns();

      ITown stop = ITown(prov.getTown() + nTowns);

      for(ITown tIndex = prov.getTown(); tIndex < stop; tIndex++)
      {
        const Town& town = edit->getCampaignData()->getTown(tIndex);

        char buf[300];
        wsprintf(buf, "%s, %s", town.getNameNotNull(), prov.getNameNotNull());

        cb.add(buf, tIndex);
      }
    }
  }

  setPosition();

  return TRUE;
}

//============================= ArmisticeEndsPage implementation ==============

BOOL ArmisticeEndsPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

void ArmisticeEndsPage::updateValues()
{
  if(d_current)
  {
  }
}

void ArmisticeEndsPage::initControls()
{

  enableControls();
}

void ArmisticeEndsPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_ArmisticeEnds, hasCondition, hasType);


  {
    Button b(getHWND(), CNDE_AE_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_ArmisticeEnds, d_current));
  }

  {
    Button b(getHWND(), CNDE_AE_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_ArmisticeEnds, d_current));
  }

  {
    Button b(getHWND(), CNDE_AE_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_AE_DELETE);
    b.enable(hasType);
  }


}

BOOL ArmisticeEndsPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

  setPosition();

  return TRUE;
}

//============================= ArmisticeStartsPage implementation ==============

BOOL ArmisticeStartsPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;
}

void ArmisticeStartsPage::updateValues()
{
  if(d_current)
  {
  }
}

void ArmisticeStartsPage::initControls()
{

  enableControls();
}

void ArmisticeStartsPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getConditionTypeFlags(ConditionType::ID_ArmisticeStarts, hasCondition, hasType);


  {
    Button b(getHWND(), CNDE_AS_PREV);
    b.enable(edit->hasPrev(ConditionType::ID_ArmisticeStarts, d_current));
  }

  {
    Button b(getHWND(), CNDE_AS_NEXT);
    b.enable(edit->hasNext(ConditionType::ID_ArmisticeStarts, d_current));
  }

  {
    Button b(getHWND(), CNDE_AS_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_AS_DELETE);
    b.enable(hasType);
  }


}

BOOL ArmisticeStartsPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

  setPosition();

  return TRUE;
}

//============================= ActionPages derived classes ==============

/*=================================================================
 * Sub-Dialog Implementation
 */

ActionEditPage::ActionEditPage(ConditionEdit* p, const char* dlgName)
{
   edit = p;

   HRSRC rhTep = FindResource(NULL, dlgName, RT_DIALOG);
   ASSERT(rhTep != NULL);
   HGLOBAL lphTep = (HGLOBAL) LoadResource(NULL, rhTep);
   ASSERT(lphTep != NULL);
   dialog = (LPDLGTEMPLATE) LockResource(lphTep);
   ASSERT(dialog != NULL);

}

HWND ActionEditPage::create()
{
   ASSERT(hWnd == NULL);      // Already created?

   HWND hDialog = createDialogIndirect(dialog, edit->getHWND());

   ASSERT(hDialog != NULL);

   return hDialog;
}

// void ActionEditPage::enable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_SHOW);
// }
// 
// void ActionEditPage::disable()
// {
//    ASSERT(hWnd != NULL);
//    ShowWindow(hWnd, SW_HIDE);
// }


void ActionEditPage::setPosition()
{
   const RECT& r = edit->getActionDialogRect();

   SetWindowPos(getHWND(), HWND_TOP, r.left, r.top, r.right-r.left, r.bottom-r.top, 0);
}

//============================= SideHasVictoryPage =============================
BOOL SideHasVictoryPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void SideHasVictoryPage::updateValues()
{
  if(d_current)
  {

    /*
     * Get which side wins
     */

    ComboBox cb(getHWND(), CNDE_ACTION_VP_WHOWINS);
    d_current->whoWins(static_cast<Side>(cb.getValue()));
  }
}

void SideHasVictoryPage::initControls()
{
  ComboBox cb(getHWND(), CNDE_ACTION_VP_WHOWINS);
  ASSERT(cb.getNItems() > 0);

  if(d_current)
  {
    cb.setValue(d_current->whoWins());
  }
  else
  {
    cb.set(0);
  }

  enableControls();
}

void SideHasVictoryPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getActionTypeFlags(ActionType::ID_Victory, hasCondition, hasType);

  {
    ComboBox cb(getHWND(), CNDE_ACTION_VP_WHOWINS);
    cb.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_ACTION_VP_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_ACTION_VP_DELETE);
    b.enable(hasType);
  }
}

BOOL SideHasVictoryPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  {
    ComboBox cb(hwnd, CNDE_ACTION_VP_WHOWINS);
    cb.add("Neutral", SIDE_Neutral);

    for(Side s = 0; s < scenario->getNumSides(); s++)
    {
      cb.add(scenario->getSideName(s), s);
    }
  }

  setPosition();
  return TRUE;
}

//============================= ArmisticeReachedPage =============================
BOOL ArmisticeReachedPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void ArmisticeReachedPage::updateValues()
{
  if(d_current)
  {

    /*
     * Get Armistice time
     */

    BOOL result;
    UINT days = GetDlgItemInt(getHWND(), CNDE_ACTION_AR_DAYS, &result, FALSE);
    ASSERT(result);

    d_current->armisticeTime(DaysToTicks(days));
  }
}

void ArmisticeReachedPage::initControls()
{
  LONG value = d_current ? d_current->armisticeTime()/(TicksPerHour*SysTick::HoursPerDay) : 0;
  SetDlgItemInt(getHWND(), CNDE_ACTION_AR_DAYS, static_cast<int>(value), False);

  enableControls();
}

void ArmisticeReachedPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getActionTypeFlags(ActionType::ID_Armistice, hasCondition, hasType);

  {
    WindowControl wc(getHWND(), CNDE_ACTION_AR_DAYS);
    wc.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_ACTION_AR_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_ACTION_AR_DELETE);
    b.enable(hasType);
  }
}

BOOL ArmisticeReachedPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{

  setPosition();
  return TRUE;
}

//============================= NationEntersWarPage =============================
BOOL NationEntersWarPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void NationEntersWarPage::updateValues()
{
  if(d_current)
  {
    /*
     * Enters the war or never enters the war?
     */

    {
      Button b(getHWND(), CNDE_ACTION_NE_ENTERS);
      d_current->entersWar(b.getCheck());
    }

    /*
     * Get which nation
     */

    {
      ComboBox cb(getHWND(), CNDE_ACTION_NE_NATION);
      d_current->whichNation(static_cast<Nationality>(cb.getValue()));
    }

    /*
     * Get which side
     */

    {
      ComboBox cb(getHWND(), CNDE_ACTION_NE_SIDE);
      d_current->whichSide(static_cast<Side>(cb.getValue()));
    }
  }
}

void NationEntersWarPage::initControls()
{
  ComboBox nationCB(getHWND(), CNDE_ACTION_NE_NATION);
  ASSERT(nationCB.getNItems() > 0);

  ComboBox sideCB(getHWND(), CNDE_ACTION_NE_SIDE);
  ASSERT(sideCB.getNItems() > 0);

  Button b(getHWND(), CNDE_ACTION_NE_ENTERS);

  if(d_current)
  {
    nationCB.setValue(d_current->whichNation());
    sideCB.setValue(d_current->whichSide());
    b.setCheck(d_current->entersWar());
  }
  else
  {
    nationCB.set(0);
    sideCB.set(0);
    b.setCheck(True);
  }

  enableControls();
}

void NationEntersWarPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getActionTypeFlags(ActionType::ID_NationEntersWar, hasCondition, hasType);

  {
    ComboBox cb(getHWND(), CNDE_ACTION_NE_NATION);
    cb.enable(hasType);
  }

  {
    ComboBox cb(getHWND(), CNDE_ACTION_NE_SIDE);
    cb.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_ACTION_NE_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_ACTION_NE_DELETE);
    b.enable(hasType);
  }
}

BOOL NationEntersWarPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  {
    ComboBox cb(hwnd, CNDE_ACTION_NE_NATION);
    cb.add("Neutral", NATION_Neutral);

    for(Nationality n = 0; n < scenario->getNumNations(); n++)
    {
      cb.add(scenario->getNationName(n), n);
    }
  }

  {
    ComboBox cb(hwnd, CNDE_ACTION_NE_SIDE);
    cb.add("Neutral", SIDE_Neutral);

    for(Side s = 0; s < scenario->getNumSides(); s++)
    {
      cb.add(scenario->getSideName(s), s);
    }
  }

  setPosition();
  return TRUE;
}

//===================== ReinforcementsArrivePage ========================

BOOL ReinforcementsArrivePage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void ReinforcementsArrivePage::updateValues()
{
  if(d_current)
  {

    /*
     * Get which side wins
     */

    ComboBox cb(getHWND(), CNDE_ACTION_RA_UNIT);
    // d_current->whichUnit(static_cast<ICommandPosition>(cb.getValue()));
    d_current->whichUnit(lparamToCP(cb.getValue()));
  }
}

void ReinforcementsArrivePage::initControls()
{
  ComboBox cb(getHWND(), CNDE_ACTION_RA_UNIT);
  ASSERT(cb.getNItems() > 0);

  if(d_current)
  {
    // cb.setValue(d_current->whichUnit());
    cb.setValue(cpToLPARAM(d_current->whichUnit()));
  }
  else
  {
    cb.set(0);
  }

  enableControls();
}

void ReinforcementsArrivePage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getActionTypeFlags(ActionType::ID_Reinforcements, hasCondition, hasType);

  {
    ComboBox cb(getHWND(), CNDE_ACTION_RA_UNIT);
    cb.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_ACTION_RA_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_ACTION_RA_DELETE);
    b.enable(hasType);
  }
}

BOOL ReinforcementsArrivePage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  {
    ComboBox cb(hwnd, CNDE_ACTION_RA_UNIT);

    CampaignData* campData = edit->getCampaignData();

    CPIter iter(&campData->getArmies(), True);

    // cb.add("No Unit", NoCommandPosition);
    cb.add("No Unit", cpToLPARAM(NoCommandPosition));
    while(iter.sister())
    {
      // cb.add(campData->getUnitName(iter.current()), iter.current());
      cb.add(campData->getUnitName(iter.current()).toStr(), cpToLPARAM(iter.current()));
    }

  }

  setPosition();
  return TRUE;
}

//===================== LeaderLeavesPage ========================

BOOL LeaderLeavesPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void LeaderLeavesPage::updateValues()
{
  if(d_current)
  {
    /*
     * Which leader
     */

    ComboBox cb(getHWND(), CNDE_ACTION_LE_LEADER);
    // d_current->whichLeader(static_cast<ILeader>(cb.getValue()));
    d_current->whichLeader(lparamToLeader(cb.getValue()));
  }
}

void LeaderLeavesPage::initControls()
{
  ComboBox cb(getHWND(), CNDE_ACTION_LE_LEADER);
  ASSERT(cb.getNItems() > 0);

  if(d_current)
  {
    // cb.setValue(d_current->whichLeader());
    cb.setValue(leaderToLPARAM(d_current->whichLeader()));
  }
  else
  {
    cb.set(0);
  }

  enableControls();
}

void LeaderLeavesPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getActionTypeFlags(ActionType::ID_LeaderLeaves, hasCondition, hasType);

  {
    ComboBox cb(getHWND(), CNDE_ACTION_LE_LEADER);
    cb.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_ACTION_LE_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_ACTION_LE_DELETE);
    b.enable(hasType);
  }
}

BOOL LeaderLeavesPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  {
    ComboBox cb(hwnd, CNDE_ACTION_LE_LEADER);
    // cb.add("No Leader", NoLeader);
    cb.add("No Leader", leaderToLPARAM(NoLeader));

    CampaignData* campData = edit->getCampaignData();

    UnitIter uiter(&campData->getArmies(), campData->getArmies().getTop(), True);
    while(uiter.next())
    {
      ICommandPosition cpi = uiter.current();
      CommandPosition* cp = uiter.currentCommand();

      /*
       *  No need to do President or God
       */

      if(cp->isLower(Rank_President))
      {
        // cb.add(campData->getUnitName(cpi), cp->getLeader());
        cb.add(campData->getUnitName(cpi).toStr(), leaderToLPARAM(cp->getLeader()));
      }
    }

  }

  setPosition();
  return TRUE;
}

//===================== LeaderLeavesPage ========================

BOOL LeaderEntersPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void LeaderEntersPage::updateValues()
{
  if(d_current)
  {
    /*
     * Which leader
     */

    ComboBox cb(getHWND(), CNDE_ACTION_ENTER_LEADER);
    // d_current->whichLeader(static_cast<ILeader>(cb.getValue()));
    d_current->whichLeader(lparamToLeader(cb.getValue()));
  }
}

void LeaderEntersPage::initControls()
{
  ComboBox cb(getHWND(), CNDE_ACTION_ENTER_LEADER);
  ASSERT(cb.getNItems() > 0);

  if(d_current)
  {
    // cb.setValue(d_current->whichLeader());
    cb.setValue(leaderToLPARAM(d_current->whichLeader()));
  }
  else
  {
    cb.set(0);
  }

  enableControls();
}

void LeaderEntersPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getActionTypeFlags(ActionType::ID_LeaderEnters, hasCondition, hasType);

  {
    ComboBox cb(getHWND(), CNDE_ACTION_ENTER_LEADER);
    cb.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_ACTION_ENTER_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_ACTION_ENTER_DELETE);
    b.enable(hasType);
  }
}

BOOL LeaderEntersPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  {
    ComboBox cb(hwnd, CNDE_ACTION_ENTER_LEADER);
    // cb.add("No Leader", NoLeader);
    cb.add("No Leader", leaderToLPARAM(NoLeader));

    CampaignData* campData = edit->getCampaignData();
#if 0
    UnitIter uiter(&campData->getArmies(), campData->getArmies().getTop(), True);
    while(uiter.next())
    {
      ICommandPosition cpi = uiter.current();
      CommandPosition* cp = uiter.currentCommand();

      /*
       *  No need to do President or God
       */

      if(cp->isLower(Rank_President))
      {
        // cb.add(campData->getUnitName(cpi), cp->getLeader());
        cb.add(campData->getUnitName(cpi).toStr(), leaderToLPARAM(cp->getLeader()));
      }
    }
#endif

    // add only inactive independent leaders
    IndependentLeaderList& llist = campData->getArmies().independentLeaders();
    SListIterR<IndependentLeader> iter(&llist);
    while(++iter)
    {
       if(!iter.current()->active())
          cb.add(iter.current()->leader()->getName(), leaderToLPARAM(iter.current()->leader()));
    }  

  }

  setPosition();
  return TRUE;
}

//===================== AllyDefectsPage ========================

BOOL AllyDefectsPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void AllyDefectsPage::updateValues()
{
  if(d_current)
  {
    ListBox lb(getHWND(), CNDE_ACTION_AD_DEFECTS);

    for(int i = 0; i < lb.getNItems(); i++)
    {
      Nationality n = static_cast<Nationality>(lb.getValue(i));
      d_current->addNation(n);
    }
  }
}

void AllyDefectsPage::initControls()
{

  if(d_current)
  {
    ListBox lb(getHWND(), CNDE_ACTION_AD_DEFECTS);
    lb.reset();

    int nNations = d_current->nationEntries();

    for(int i = 0; i < d_current->nationEntries(); i++)
    {
      Nationality n = d_current->whichNation(i);
      lb.add(scenario->getNationName(n), n);
    }
  }

  enableControls();
}

void AllyDefectsPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getActionTypeFlags(ActionType::ID_AllyDefects, hasCondition, hasType);

  {
    ListBox lb(getHWND(), CNDE_ACTION_AD_NATION);
    lb.enable(hasType);
  }

  {
    ListBox lb(getHWND(), CNDE_ACTION_AD_DEFECTS);
    lb.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_ACTION_AD_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_ACTION_AD_DELETE);
    b.enable(hasType);
  }
}

BOOL AllyDefectsPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  {
    ListBox lb(hwnd, CNDE_ACTION_AD_NATION);


    CampaignData* campData = edit->getCampaignData();

    for(Nationality n = 0; n < scenario->getNumNations(); n++)
    {
      lb.add(scenario->getNationName(n), n);
    }

  }

  setPosition();
  return TRUE;
}

void AllyDefectsPage::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case CNDE_ACTION_AD_NATION:
    {
      if(codeNotify == LBN_DBLCLK)
      {
        ListBox from(hwndCtl);
        ListBox to(hwnd, CNDE_ACTION_AD_DEFECTS);

        Nationality n = static_cast<Nationality>(from.getValue());
        Boolean alreadyHas = False;

        for(int i = 0; i < to.getNItems(); i++)
        {
          if(to.getValue(i) == n)
          {
            alreadyHas = True;
            break;
          }
        }
        if(!alreadyHas)
          to.add(scenario->getNationName(n), n);
      }
      break;
    }

    case CNDE_ACTION_AD_DEFECTS:
    {
      if(codeNotify == LBN_DBLCLK)
      {
        ListBox lb(hwndCtl);
        lb.remove(lb.getIndex());
      }
      break;
    }

    default:
      edit->onCommand(hwnd, id, hwndCtl, codeNotify);
  }
}

//===================== AllyDefectsPage ========================

BOOL AdjustResourcesPage::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_INITDIALOG:
         HANDLE_WM_INITDIALOG(hWnd, wParam, lParam, onInitDialog);
         break;

      case WM_COMMAND:
         HANDLE_WM_COMMAND(hWnd, wParam, lParam, edit->onCommand);
         break;

      default:
         return FALSE;
   }

   return TRUE;

}

void AdjustResourcesPage::updateValues()
{
  if(d_current)
  {
    ComboBox cb(getHWND(), CNDE_ACTION_ADR_NATION);

    Nationality n = static_cast<Nationality>(cb.getValue());
    d_current->addNation(n);

    SWORD value = static_cast<SWORD>(GetDlgItemInt(getHWND(), CNDE_ACTION_ADR_PERCENT, NULL, TRUE));
    d_current->whatPercent(value);
  }
}

void AdjustResourcesPage::initControls()
{

  if(d_current)
  {
    ComboBox cb(getHWND(), CNDE_ACTION_ADR_NATION);
    cb.reset();

    for(Nationality n = 0; n < scenario->getNumNations(); n++)
    {
      cb.add(scenario->getNationName(n), n);
    }

    cb.setValue((d_current->whichNation() != NATION_Neutral) ? d_current->whichNation() : 0);

    SetDlgItemInt(getHWND(), CNDE_ACTION_ADR_PERCENT, d_current->whatPercent(), TRUE);
  }

  enableControls();
}

void AdjustResourcesPage::enableControls()
{
  Boolean hasCondition;
  Boolean hasType;
  edit->getActionTypeFlags(ActionType::ID_AdjustResources, hasCondition, hasType);

  {
    ComboBox lb(getHWND(), CNDE_ACTION_ADR_NATION);
    lb.enable(hasType);
  }

  {
    WindowControl ed(getHWND(), CNDE_ACTION_ADR_PERCENT);
    ed.enable(hasType);
  }

  {
    Button b(getHWND(), CNDE_ACTION_ADR_NEW);
    b.enable(hasCondition);
  }

  {
    Button b(getHWND(), CNDE_ACTION_ADR_DELETE);
    b.enable(hasType);
  }
}

BOOL AdjustResourcesPage::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  setPosition();
  return TRUE;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:44  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
