/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			      *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								            *
 *----------------------------------------------------------------------------*/
#if !defined(campEditDll_H)
#define campEditDll_H

/*
 *------------------------------------------------------------------------------
 * $Header$
 *------------------------------------------------------------------------------
 * $Author$
 *------------------------------------------------------------------------------
 * Copyright (C) 2001, Steven Green, All rights reserved.
 *------------------------------------------------------------------------------
 */

#if defined(NO_WG_DLL)
    #define CAMPEDIT_DLL
#elif defined(EXPORT_CAMPEDIT_DLL)
    #define CAMPEDIT_DLL __declspec(dllexport)
#else
    #define CAMPEDIT_DLL __declspec(dllimport)
#endif


/*
 *------------------------------------------------------------------------------
 * $Log$
 *------------------------------------------------------------------------------
 */

#endif   // defined(campEditDll_H)