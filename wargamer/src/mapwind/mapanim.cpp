/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "mapanim.hpp"
#include "scn_res.h"
#include "scenario.hpp"
#include "animate.hpp"
#include "mw_data.hpp"
#include "cbattle.hpp"
#include "campdint.hpp"


/*
 * Map Window Animation Base class
 */

class MW_Animation {
    const MapWindowData* d_mapData;
    Frames d_frames;
  public:
    MW_Animation(const MapWindowData* mapData) : d_mapData(mapData) {}
    virtual ~MW_Animation() {}

    virtual void run(const DrawDIBDC* srcDib, HDC hdc, Boolean sameFrame) = 0;
    const MapWindowData* mapData() { return d_mapData; }
    Frames& frames() { return d_frames; }
};

/*------------------------------------------------------------
 *  Campaign Battle Animation
 */

class MW_BattleAnimation : public MW_Animation {
  public:
    MW_BattleAnimation(const MapWindowData* mapData) :
        MW_Animation(mapData)
    {
      frames().init(scenario->getScenarioResDLL(), CampaignBattleAnimation, MAKEINTRESOURCE(PR_CBATTLEANIMATIONS), RT_IMAGEPOS, NumCampBattleFrames);
    }

    void run(const DrawDIBDC* srcDib, HDC hdc, Boolean sameFrame);
};

void MW_BattleAnimation::run(const DrawDIBDC* srcDib, HDC hdc, Boolean sameFrame)
{
   //------ Changed SWG 3Dec98 : Needs to be locked for reading
   // or else who knows what can happen if logic processes one
   // 
  // CampaignBattleIterNLR iter(&mapData()->d_campData->getBattleList());
  CampaignBattleIterR iter(&mapData()->d_campData->getBattleList());
  while(++iter)
  {
    const CampaignBattle* battle = iter.current();
    ASSERT(battle != 0);

    /*
     *  Convert battle position to pixel coordinates
     */

    const CampaignPosition& pos = battle->getPosition();
    Location l;
    pos.getLocation(l);
    PixelPoint p;
    mapData()->locationToPixel(l, p);

    /*
     * If we're on the display
     */

    if(mapData()->onDisplay(p))
    {
      if(sameFrame)
      {
        frames().blitCurrent(srcDib, hdc,
           p.getX() - (frames().frameWidth() / 2),
           p.getY() - (frames().frameHeight() / 2));
      }
      else
      {
        frames().animate(srcDib, hdc,
           p.getX() - (frames().frameWidth() / 2),
           p.getY() - (frames().frameHeight() / 2));
      }
    }
  }
}

/*----------------------------------------------------------------
 *  Map Window Animation Manager
 */

MW_AnimationManager::MW_AnimationManager()
{
  for(int i = 0; i < Animation_HowMany; i++)
  {
    d_mwAnimations[i] = 0;
  }
}

void MW_AnimationManager::init(const MapWindowData* mapData)
{
  /*
   * Add our animations. They will probably eventually include:
   *   1. Battle
   *   2. March Columns
   *   3. Town Fires
   *   4. Fort / Supply Depot constructions
   *   5. Sieges
   */

  d_mwAnimations[Battle] = new MW_BattleAnimation(mapData);
}

MW_AnimationManager::~MW_AnimationManager()
{
  for(int i = 0; i < Animation_HowMany; i++)
  {
    if(d_mwAnimations[i])
      delete d_mwAnimations[i];
  }
}

void MW_AnimationManager::run(const DrawDIBDC* srcDib, HDC hdc, Boolean sameFrame)
{
  for(int i = 0; i < Animation_HowMany; i++)
  {
    if(d_mwAnimations[i])
      d_mwAnimations[i]->run(srcDib, hdc, sameFrame);
  }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
