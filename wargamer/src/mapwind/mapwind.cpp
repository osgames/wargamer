/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Zoomed Map Window
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mapwind.hpp"
#include "mw_data.hpp"
#include "mapgui.hpp"
#include "campdint.hpp"
#include "cscroll.hpp"
#if defined(DEBUG)
#include "todolog.hpp"
#endif


/*
 * Constructor
 */

MapWindow::MapWindow(MapWindowUser* user, const CampaignData* data)
{
   d_mapData = new MapWindowData(user, data);
   d_mapGui = new MapGUI(d_mapData);
}

void MapWindow::init(HWND parent)
{
   d_mapGui->makeWindow(parent);
}


MapWindow::~MapWindow()
{
   delete d_mapGui;
   delete d_mapData;
}

MapWindow* MapWindow::make(MapWindowUser* user, const CampaignData* data, HWND parent)
{
   MapWindow* mw = new MapWindow(user, data);
   ASSERT(mw != 0);
   mw->init(parent);
   return mw;
}

// HWND MapWindow::getHWND() const
// {
//    return d_mapGui->getHWND();
// }

void MapWindow::requestRedraw(BOOL all)
{
   d_mapData->countTimedHighLight();      // Update timeHighlight counter
   d_mapGui->requestRedraw(all);
}

void MapWindow::forceDraw()
{
   d_mapGui->forceDraw();
}


#if defined(CUSTOMIZE)
void MapWindow::printMap()
{
  d_mapGui->printMap();
}
#endif


#ifdef FINISHED_MAPWINDOW_REWRITE
void MapWindow::toggleOrderTB()
{
  if(orderTB)
  {
    DestroyWindow(orderTB->getHWND());
    orderTB = 0;
  }
  ASSERT(globWin.mainWind != 0);
  if(gApp.getCampaignMode() == CampaignInterface::CM_UNITS)
    orderTB = new UnitOrderTB(globWin.mainWind->getHWND(), this);
  else
    orderTB = new TownOrderTB(globWin.mainWind->getHWND(), this);

  ASSERT(orderTB != 0);
}
#endif




#if defined(CUSTOMIZE)        // these don't seem to be used anywhere?
/*======================================================
 * Interface to town editor
 *
 * Virtual functions of TownEditList
 */

/*
 * Next time button pressed on map, call:
 *    edit->gotPosition(&location);
 * or to cancel it:
 *    edit->gotPosition(0);
 */

void MapWindow::setTrackMode(TrackMode mode)
{
   d_mapGui->setTrackMode(mode);
}

Location MapWindow::getClickLocation() const
{
   return d_mapGui->getClickLocation();
}


void MapWindow::editDestroyed()
{
   d_mapData->editDial = 0;
   d_mapData->edittingTown = NoTown;
   setTrackMode(MWTM_None);
}


void MapWindow::townSelected(ITown ob)
{
   d_mapData->townSelected(ob);
}

void MapWindow::provinceSelected(IProvince ob)
{
   d_mapData->selectedProvince(ob);
}

#endif   // CUSTOMIZE

#ifdef DEBUG
void MapWindow::toggleGrid()
{
   // gridOn = !gridOn;
   d_mapData->toggleGrid();
   forceDraw();
}
#endif




// Initialise zooming to specific zoom level
// If already zooming, then cancel it
// If zoom level lower than current level, then cancel


void MapWindow::initZoom(MagEnum magLevel)
{
   d_mapGui->zoomTo(magLevel);
}

// Initialise zoom to next zoom level

void MapWindow::initZoomIn()
{
   d_mapGui->toggleZoomMode();
}

// Immediately zoom out to next level, at current coordinates

void MapWindow::zoomOut()
{
   d_mapGui->zoomOut();
}

// Immediately zoom to given location and zoom level

void MapWindow::zoomTo(MagEnum magLevel, const MapPoint& mc)
{
   d_mapGui->doZoom(magLevel, mc);
}

// Immediately zoom to zoom level, centering on current coordinates

void MapWindow::zoomTo(MagEnum magLevel)
{
   d_mapGui->zoomOut(magLevel);
}

// Immediately zoom to given coordinates at current zoom level

void MapWindow::zoomTo(const MapPoint& mc)
{
   d_mapGui->doZoom(mc);
}


void MapWindow::setMapPos(MapCord x, MapCord y)
{
   d_mapGui->setMapPos(x, y);
}

void MapWindow::centerOnMap(const Location& l, MagEnum level)
{
   d_mapGui->centerOnMap(l, level);
}


MagEnum MapWindow::getZoomEnum() const { return d_mapData->getZoomEnum(); }
Boolean MapWindow::canZoomOut() const { return d_mapData->canZoomOut(); }
Boolean MapWindow::canZoomIn() const { return d_mapData->canZoomIn(); }
BOOL MapWindow::isZooming() const { return d_mapGui->isZooming(); }

// static ATOM MapWindow::registerClass()
// {
//    return MapGUI::registerClass();
// }



void MapWindow::setHighlightedTown(ITown iTown) { d_mapData->setHighlightedTown(iTown); }
void MapWindow::setHighlightedUnit(ConstICommandPosition cpi) {d_mapData->setHighlightedUnit(cpi); }
ITown MapWindow::getHighlightedTown() const { return d_mapData->getHighlightedTown(); }


void MapWindow::locationToMap(const Location& lc, MapPoint& mc) const { d_mapData->locationToMap(lc, mc); }
void MapWindow::locationToPixel(const Location& lc, PixelPoint& pc) const { d_mapData->locationToPixel(lc, pc); }
MapCord MapWindow::getTopX() const { return d_mapData->getTopX(); }
MapCord MapWindow::getTopY() const { return d_mapData->getTopY(); }
MapCord MapWindow::getFullWidth() const { return d_mapData->getFullWidth(); }
MapCord MapWindow::getFullHeight() const { return d_mapData->getFullHeight(); }
MapCord MapWindow::getViewWidth() const { return d_mapData->getViewWidth(); }
MapCord MapWindow::getViewHeight() const { return d_mapData->getViewHeight(); }
void MapWindow::toggleShowAllRoute() { d_mapData->toggleShowAllRoute(); }
void MapWindow::toggleShowAllAttribs() { d_mapData->toggleShowAllAttribs(); }

#ifdef DEBUG
BOOL MapWindow::isGridOn() const { return d_mapData->gridOn(); }
#endif

#if defined(CUSTOMIZE)

EditMode MapWindow::getEditMode() const { return d_mapData->editMode; }
void MapWindow::setEditMode(EditMode newMode) { d_mapData->editMode = newMode; }
EditDialog* MapWindow::getEditDial() { return d_mapData->editDial; }
void MapWindow::setEditDial(EditDialog* dial) { d_mapData->editDial = dial; }
ITown MapWindow::getEdittingTown() const { return d_mapData->edittingTown; }
void MapWindow::setEdittingTown(ITown itown) { d_mapData->edittingTown = itown; }

#endif   // CUSTOMIZE

#if !defined(EDITOR)

void MapWindow::drawRoute(DrawDIBDC* dib, const CampaignPosition& from, const CampaignOrder& order, Side side)
{
   d_mapGui->drawRoute(dib, *d_mapData, from, order, side);
}

void MapWindow::drawRoute(DrawDIBDC* dib, const CampaignPosition& from, const CampaignOrder& order, Side side, const RouteList& rl)
{
   d_mapGui->drawRoute(dib, *d_mapData, from, order, side, rl);
}

#endif



HWND MapWindow::getHWND() const
{
    return d_mapGui->getHWND();
}

bool MapWindow::isVisible() const
{
  return d_mapGui->isVisible();
}

bool MapWindow::isEnabled() const
{
  return d_mapGui->isEnabled();
}

void MapWindow::show(bool visible)
{
  d_mapGui->show(visible);
}

void MapWindow::enable(bool enable)
{
  d_mapGui->enable(enable);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
