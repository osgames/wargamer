/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MW_USER_HPP
#define MW_USER_HPP

#ifndef __cplusplus
#error mw_user.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Map Window User class
 * Provides virtual functions that MapWindow calls when certain
 * things happen
 *
 *----------------------------------------------------------------------
 */

#include "mwdll.h"
#include "gamedefs.hpp"
#include "unitlist.hpp"
#include "measure.hpp"

struct MapSelect;
class PixelPoint;
class DrawDIBDC;
// class Grapple;

enum TrackMode {
	MWTM_None,
	MWTM_Town,
	MWTM_Unit,
	MWTM_Province,
	MWTM_Position,			// Just sets x,y
	MWTM_Dragging
};


enum EditMode {
#if !defined(EDITOR)
	PM_Playing,
#endif
#if defined(CUSTOMIZE)
	PM_EditTown,
	PM_EditProvince,
	PM_EditConnection,
	PM_EditOB,
   PM_EditConditions
#endif
};



class MapWindowUser {
 public:
	virtual ~MapWindowUser()= 0;
		// Destructor

	virtual TrackMode getTrack() = 0;
		// Return how to do tracking

	virtual void onLClick(MapSelect& info) = 0;
		// Left button clicked

	virtual void onRClick(MapSelect& info) = 0;
		// Right Button clicked

	// virtual void onTrack(MapSelect& info) = 0;
		// New Item under Mouse

	virtual void onDraw(DrawDIBDC* dib, MapSelect& info) = 0;
		// Map is being drawn into dib
		// User can do any drawing it wants to do

	// virtual void update() = 0;
		// Info may have changed

	// virtual void addObject(ConstICommandPosition cpi) = 0;
		// ?

	virtual void onButtonDown(MapSelect& info) = 0;
		// Left button is pressed

#ifdef CUSTOMIZE
	virtual void editObject(const MapSelect& info, const PixelPoint& p) = 0;
		// Edit object under mouse
#endif

	virtual bool onStartDrag(MapSelect& info) = 0;
		// Start dragging, return true if dragging is allowed

	virtual void onDrag(MapSelect& info) = 0;
		// Mouse has moved while dragging

	virtual void onEndDrag(MapSelect& info) = 0;
		// Stop dragging

	virtual void updateZoom() = 0;        // update map arrows when zooming
		// Zoom Area has changed
	virtual void magnificationUpdated() = 0;
		// Magnification level has changed

	virtual void overObject(MapSelect& info) = 0;
   virtual void notOverObject() = 0;
//	virtual void overTown(MapSelect& info) = 0;
		// Mouse is over a town
//	virtual void overUnit(MapSelect& info) = 0;
		// Mouse is over a unit

	virtual void mapRedrawn() = 0;
		// Map Window has been redrawn...
		// Update tiny map Window

	// virtual void clearHint() = 0;
		// Clear HintLine... this should not be here!!!!!
};

inline MapWindowUser::~MapWindowUser() { }

#ifdef CUSTOMIZE
/*
 * Virtual base Class for editting dialogs
 */

class EditDialog {
public:
	virtual ~EditDialog() { }
	virtual void kill() = 0;
	virtual TrackMode onSelect(TrackMode mode, const MapSelect& info) = 0;
};
#endif

/*
 * Structure with info needed by Control Dialogs
 */

struct MapSelect {
	ITown selectedTown;
	IProvince selectedProvince;
	const StackedUnitList* trackedUnits;
	UnitListID currentUnit;					// Entry into tracked Units
	UnitListID currentGeneral;				// Entry into trackedUnits
	Location mouseLocation;					// Physical Location of mouse pointer
	// const Grapple* grapple;             // Location of current grapple, if any
	Boolean ctrlKey;							// Control Key is down
	Boolean shiftKey;							// Shift key is down

	MAPWIND_DLL MapSelect();
	MAPWIND_DLL ConstICommandPosition getCP() const;

	MAPWIND_DLL Boolean hasUnit() const;
	MAPWIND_DLL Boolean hasTown() const { return Boolean(selectedTown != NoTown); }
};

#endif /* MW_USER_HPP */

