/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TINYMAP_H
#define TINYMAP_H

#ifndef __cplusplus
#error tinymap.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Tiny Map Window
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.6  1996/02/15 10:51:21  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.5  1995/11/13 11:11:05  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.4  1995/10/20 11:05:41  Steven_Green
 * Use pointers instead of Handles when making Windows.
 *
 * Revision 1.3  1995/10/18 10:59:53  Steven_Green
 * *** empty log message ***
 *
 * Revision 1.2  1995/10/17 12:03:08  Steven_Green
 * Uses DIB bitmaps
 *
 * Revision 1.1  1995/10/16 11:33:16  Steven_Green
 * Initial revision
 *
 *
 *----------------------------------------------------------------------
 */


#include "mwdll.h"
#include "wind.hpp"
//#include "campwind.hpp"
#include "palwind.hpp"
#include "gamedefs.hpp"
#include "cpdef.hpp"

class CampaignWindowsInterface;
class DIB;
class DrawDIBDC;
class MapWindow;
class Location;
class PixelPoint;

/*
 * DIB Version of TinyMapWindow
 */

/*
 * Little Map Window that uses DIBS instead
 */

class TinyMapWindow : public WindowBaseND, public PaletteWindow {
	DrawDIBDC* d_dib;
	DrawDIB*   d_staticDIB;

	BOOL d_dragging;				// Set when in drag mode
	RECT d_zoomArea;				// Position/Size of draggable rectangle (Client coords)

	MapWindow* d_mapWindow;

public:
	MAPWIND_DLL TinyMapWindow(HWND parent, MapWindow* map);// const RECT& r);
	~TinyMapWindow();

	MAPWIND_DLL void areaChanged();						// Tell TinyMap that zoom area has changed
	MAPWIND_DLL void draw(ConstICommandPosition cpi);		// Thread function to do the actual draw

private:
	LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	/*
	 * Message Handlers
	 */

	void onDestroy(HWND hWnd);
	void onPaint(HWND hWnd);
	BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
	void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
	void onMove(HWND hWnd, int x, int y);
	void onSize(HWND hWnd, UINT state, int cx, int cy);
	BOOL onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg);

	/*
	 * Helper Functions
	 */

	void updateMapPos(int x, int y);
	MapWindow* getMapWindow() const { return d_mapWindow; }
	void updateZoomArea(int x, int y);
	void locationToTinyMap(const Location& l, PixelPoint& p);
	void drawUnits(ConstICommandPosition cpi);
    void drawTownsOnStatic();
	void drawMapRect(HDC hdc);

#ifdef DEBUG
	void checkPalette() { }
#endif
};

#endif /* TINYMAP_H */

