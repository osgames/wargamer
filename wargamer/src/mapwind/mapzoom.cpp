/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Manipulation of large zoomed maps
 *
 *----------------------------------------------------------------------
 *
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 * Revision 1.1  2001/06/13 08:52:38  greenius
 * Initial Import
 *
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mapzoom.hpp"
// #include "dib.hpp"         // Added 12Feb97
#include "grtypes.hpp"
#include "mapfile.hpp"
#include "scenario.hpp"

#ifdef DEBUG
#include "todolog.hpp"
#include "todolog.hpp"
#endif

// ------------------------------------------------------------------------

/*
 * Conversion between magEnum and pixelWidth
 */

// static const MagEnum MapZoom::MaxMagLevel = 5;
const MagEnum MapZoom::MaxMagLevel = 3;     // 0,1,2,3

#if 0 // Removed 12Feb97
static const LONG MapZoom::magLevels[] = {
   // 512,
   1024,
   2048,
   4096,
   8192, 
   16384
};
#endif

/*
 * Constructor
 */

MapZoom::MapZoom() :
   d_displayWidth(0)
{
   /*
    * For now we fill in the values...
    * They should be obtained from the scenario data
    */

   // ToDo("Put Map details in Scenario File");

   const MapScaleInfo* info = scenario->getMapScaleInfo();

   top = MapPoint(0, 0);
#if 0
   size = MapPoint(4096, 3072);
   total = MapPoint(4096, 3072);

   p1 = MapPoint(153*4, 114*4);
   p2 = MapPoint(875*4, 108*4);
   p3 = MapPoint( 58*4, 728*4);
   p4 = MapPoint(973*4, 715*4);

   physicalWidth = MilesToDistance(1280);
#endif
   total = info->s_total;
   size = info->s_total;
   p1 = info->s_p[0];
   p2 = info->s_p[1];
   p3 = info->s_p[2];
   p4 = info->s_p[3];
   physicalWidth = MilesToDistance(info->s_widthMiles);
   physicalHeight = MilesToDistance(info->s_heightMiles);

   pixelWidth = 1024;         // Start off with a 4:1 (1024 pixels total)
   magEnum = 0;

   mapFile = new MapFileSystem();
}

MapZoom::~MapZoom()
{
   if(mapFile)
   {
      delete mapFile;
      mapFile = 0;
   }
}

MapCord MapZoom::getMidX() const
{
   return top.getX() + size.getX() / 2;
}

MapCord MapZoom::getMidY() const
{
   return top.getY() + size.getY() / 2;
}

/*
 * Added 12Feb97:
 *
 * Set up the viewed size
 * this used to be done using Map Coordinates
 * Doing it with pixel coordinates, allows us to also set up d_displayWidth
 */

void MapZoom::setViewSize(const PixelPoint& p)
{
   // Width is set so that the largest of width or height represents the complete map

   LONG w1 = MulDiv(p.getY(), total.getX(), total.getY());
   d_displayWidth = maximum(p.getX(), w1);

   pixelWidth = d_displayWidth * (magEnum + 1);

   pixelSizeToMap(p, size);      // Get Map Coordinate size
}

/*
 * Set X position to centre on x
 */

Boolean MapZoom::setMapX(MapCord x)
{
   MapCord maxX = total.getX() - size.getX();
   x -= size.getX() / 2;
   if(x > maxX)
      x = maxX;
   if(x < 0)
      x = 0;

   if(x != top.getX())
   {
      top.setX(x);
      return True;
   }
   else
      return False;
}

Boolean MapZoom::setMapY(MapCord y)
{
   MapCord maxY = total.getY() - size.getY();
   y -= size.getY() / 2;
   if(y > maxY)
      y = maxY;
   if(y < 0)
      y = 0;

   if(y != top.getY())
   {
      top.setY(y);
      return True;
   }
   else
      return False;
}

/*
 * Coordinate Conversion Functions
 */

MapCord MapZoom::pixelToMap(LONG p) const
{
   return MulDiv(p, total.getX(), pixelWidth);
}

LONG MapZoom::mapToPixel(MapCord m) const
{
   return MulDiv(m, pixelWidth, total.getX());
}

void MapZoom::mapToPixel(const MapPoint& mc, PixelPoint& pc) const
{
   pc.setX(mapToPixel(mc.getX() - top.getX()));
   pc.setY(mapToPixel(mc.getY() - top.getY()));
}

void MapZoom::pixelToMap(const PixelPoint& pc, MapPoint& mc) const
{
   mc.setX(pixelToMap(pc.getX()) + top.getX());
   mc.setY(pixelToMap(pc.getY()) + top.getY());
}

void MapZoom::pixelSizeToMap(const PixelPoint& pc, MapPoint& mc) const
{
   mc.setX(pixelToMap(pc.getX()));
   mc.setY(pixelToMap(pc.getY()));
}

LONG MapZoom::distanceToPixel(Distance d) const
{
   return MulDiv(d, pixelWidth, physicalWidth);
}

void MapZoom::mapToLocation(const MapPoint& mc, Location& lc) const
{
   const MapCord topY = (p1.getY() + p2.getY()) / 2;     // Average Y coordinates
   const MapCord botY = (p3.getY() + p4.getY()) / 2;
   const MapCord h = botY - topY;            // Overall height

   const MapCord topW = p2.getX() - p1.getX();           // Width at top
   const MapCord botW = p4.getX() - p3.getX();           // Width at bottom


   MapCord y = mc.getY();
   y -= topY;
   // y = MulDiv(y, physicalWidth, total.getX());
   y = MulDiv(y, physicalHeight, total.getY());
   y = MulDiv(y, total.getY(), h);
   lc.setY(y);

   MapCord x = mc.getX();
   MapCord xLeft = p1.getX() - MulDiv(mc.getY() - topY, p1.getX() - p3.getX(), h);
   x  -= xLeft;

   MapCord xW = topW + MulDiv(mc.getY() - topY, botW - topW, h);
   x = MulDiv(x, physicalWidth, xW);
   lc.setX(x);
}

void MapZoom::locationToMap(const Location& lc, MapPoint& mc) const
{
   const MapCord topY = (p1.getY() + p2.getY()) / 2;     // Average Y coordinates
   const MapCord botY = (p3.getY() + p4.getY()) / 2;
   const MapCord h = botY - topY;            // Overall height

   const MapCord topW = p2.getX() - p1.getX();           // Width at top
   const MapCord botW = p4.getX() - p3.getX();           // Width at bottom

   LONG y = lc.getY();
   y = MulDiv(y, h, total.getY());
   // y = MulDiv(y, total.getX(), physicalWidth);
   y = MulDiv(y, total.getY(), physicalHeight);
   y += topY;
   mc.setY(y);

   LONG x = lc.getX();
   MapCord xLeft = p1.getX() - MulDiv(y - topY, p1.getX() - p3.getX(), h);
   MapCord xW = topW + MulDiv(y - topY, botW - topW, h);
   x = MulDiv(x, xW, physicalWidth);
   x += xLeft;
   mc.setX(x);
}

void MapZoom::pixelToLocation(const PixelPoint& pc, Location& lc) const
{
   MapPoint mc;
   pixelToMap(pc, mc);
   mapToLocation(mc, lc);
}

void MapZoom::locationToPixel(const Location& lc, PixelPoint& pc) const
{
   MapPoint mc;
   locationToMap(lc, mc);
   mapToPixel(mc, pc);
}

/*
 * Is a pixel on the map window?
 */

Boolean MapZoom::onDisplay(const PixelPoint& p) const
{
#if 0
   RECT r;
   GetClientRect(getHWND(), &r);
   SetRect(&r, r.left, r.top, r.right-15, r.bottom-15);
   return (Boolean) PtInRect(&r, p);
#else

   if( (p.getX() < 0) || (p.getY() < 0) )
      return False;

   MapCord x = pixelToMap(p.getX());
   MapCord y = pixelToMap(p.getY());

   return (x < size.getX()) &&
          (y < size.getY());

#endif
}

#if 0

static Boolean MapWindowData::onDisplay(const DIB* dib, const PixelPoint& p)
{
   return (p.getX() >= 0) &&
          (p.getY() >= 0) &&
          (p.getX() <= dib->getWidth()) &&
          (p.getY() <= dib->getHeight());
}

#endif


/*
 * Get a rectangle of physical coordinates that includes all of display
 */

void MapZoom::getViewRect(Rect<Distance>& r) const
{
   Location l1;
   Location l2;
   Location l3;
   Location l4;

   mapToLocation(top, l1);
   mapToLocation(MapPoint(top.getX()+size.getX(), top.getY()), l2);
   mapToLocation(MapPoint(top.getX(), top.getY()+size.getY()), l3);
   mapToLocation(MapPoint(top.getX()+size.getX(), top.getY()+size.getY()), l4);

#if 0
   r.left(minimum(l1.getX(), l3.getX());
   r.top(minimum(l1.getY(), l2.getY());
   r.right(maximum(l2.getX(), l4.getX());
   r.bottom(maximum(l3.getY(), l4.getY());
#elif 0  // this may cause width to be -ve
   r.left  (minimum(minimum(l1.getX(), l2.getX()), minimum(l3.getX(), l4.getX())));
   r.right (maximum(maximum(l1.getX(), l2.getX()), maximum(l3.getX(), l4.getX())));
   r.top   (minimum(minimum(l1.getY(), l2.getY()), minimum(l3.getY(), l4.getY())));
   r.bottom(maximum(maximum(l1.getY(), l2.getY()), maximum(l3.getY(), l4.getY())));
#else
   
   Distance left   = minimum(minimum(l1.getX(), l2.getX()), minimum(l3.getX(), l4.getX()));
   Distance right  = maximum(maximum(l1.getX(), l2.getX()), maximum(l3.getX(), l4.getX()));
   Distance top    = minimum(minimum(l1.getY(), l2.getY()), minimum(l3.getY(), l4.getY()));
   Distance bottom = maximum(maximum(l1.getY(), l2.getY()), maximum(l3.getY(), l4.getY()));

   ASSERT(right > left);
   ASSERT(bottom > top);

   Distance width = right - left + 1;
   Distance height = bottom - top + 1;

   r = Rect<Distance>(left, top, width, height);
#endif
}

/*
 * Magnification Functions
 */

Boolean MapZoom::canZoomOut() const
{
   return (magEnum > 0);
}

Boolean MapZoom::canZoomIn() const
{
   // return (magEnum < MagEnum(MaxMagLevel - 1));
   return (magEnum < MaxMagLevel);
}

void MapZoom::setZoomEnum(MagEnum m)
{
   ASSERT(m <= MaxMagLevel);
   magEnum = m;
#if 0    // 12Feb97
   pixelWidth = magLevels[m];
#else
   ASSERT(d_displayWidth != 0);
   pixelWidth = d_displayWidth * (m + 1);
#endif
}

MagEnum MapZoom::getZoomOutEnum() const
{
   ASSERT(magEnum > 0);
   return MagEnum(magEnum - 1);
}

MagEnum MapZoom::getZoomInEnum() const
{
   ASSERT(magEnum < MaxMagLevel);
   return MagEnum(magEnum + 1);
}

/*
 * return zoom level in pixels relative to level 0
 */

LONG MapZoom::getZoomLevel() const
{
#if 0    // 12Feb97
   return pixelWidth / magLevels[0];
#else
   ASSERT(d_displayWidth != 0);
   return pixelWidth / d_displayWidth;
#endif
}

/*
 * Fill DIB with pixels from map
 */

void MapZoom::drawStaticMap(DrawDIBDC* dib) const
{
   ASSERT(mapFile != 0);

#if 0 // Now done using setViewSize
   // Added 12Feb97

   /*
    * Check dib for being a new size
    */

   if(dib->getWidth() != d_displayWidth)
   {
      MapZoom* ncThis = (MapZoom*) this;

      ncThis->d_displayWidth = dib->getWidth();
      ncThis->pixelWidth = d_displayWidth * (magEnum + 1);
   }
#endif
   ASSERT(d_displayWidth != 0);
   ASSERT(pixelWidth != 0);

   // End of 12Feb97 addition

   if(mapFile)
      mapFile->displayMapSection(dib, top, total.getX(), pixelWidth);
}


//--------------------------------------------------------------------



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
