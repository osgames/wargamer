/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 * Display Supply Lines on map
 */

#include "stdinc.hpp"
#include "supplyDisp.hpp"
#include "supplyLine.hpp"
#include "mw_data.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "control.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "wmisc.hpp"
#include "thicklin.hpp"
#include "locengin.hpp"
#include "options.hpp"
#include "scenario.hpp"


SupplyDisplay::SupplyDisplay() :
//   d_supplyLines(0),
   d_dirty(true),
   d_overlayDIB(0)
{
}

SupplyDisplay::~SupplyDisplay()
{
//   delete d_supplyLines;
//   d_supplyLines = 0;
   delete d_overlayDIB;
   d_overlayDIB = 0;
}

void SupplyDisplay::draw(DrawDIBDC* dib, MapWindowData* mapData)
{
//   if(!d_supplyLines)
//      d_supplyLines = new CampaignSupplyLine;

   const CampaignData* campData = mapData->d_campData;

   if (d_dirty)
   {
      d_dirty = false;

      if(!d_overlayDIB)
         d_overlayDIB = new DrawDIB(dib->getWidth(), dib->getHeight());
      else
         d_overlayDIB->resize(dib->getWidth(), dib->getHeight());

      const ColourIndex MaskColor = 0;
      d_overlayDIB->setMaskColour(MaskColor);
      d_overlayDIB->fill(MaskColor);

      CampaignSupplyLine d_supplyLines;

      for (Side side = 0; side < scenario->getNumSides(); ++side)
      {
         if (!CampaignOptions::get(OPT_FogOfWar) ||
   #ifdef DEBUG
            Options::get(OPT_OrderEnemy) ||
   #endif
            side == GamePlayerControl::getSide(GamePlayerControl::Player) )
         {
            d_supplyLines.calculate(campData, side, false);

            CArrayIter<Connection> cl = campData->getConnections();

            const TownList& tl = campData->getTowns();

            ColourIndex RoadIndex = side + 1;      // Index used for main road
         // const ColourIndex RoadDark  = 2;    // Index used for Dark edge of road
         // const ColourIndex RoadLight = 3;    // Index used for Light edge of road

            /*
             * Set up a temporary DIB
             */

            // DrawDIB* bufDIB = mapData->getFXDIB(dib);
            // bufDIB->setMaskColour(MaskColor);
            // bufDIB->fill(MaskColor);

            ThickLineEngine lineEngine(d_overlayDIB);
            lineEngine.thickness(5);
            lineEngine.color(RoadIndex);


            /*
             * Draw connections into temporary DIB
             */


            LocationEngine conEngine(lineEngine, *mapData);

            while(++cl)
            {
               const Connection& con = cl.current();

               CampaignSupplyLine::Level l1 = d_supplyLines[con.node1];
               CampaignSupplyLine::Level l2 = d_supplyLines[con.node2];

               if ((l1 != 0) && (l2 != 0))
               {
                  const Location& loc1 = tl[con.node1].getLocation();
                  const Location& loc2 = tl[con.node2].getLocation();

                  // Set thickness

                  const int MinThick = 2;
                  const int MaxThick = 6;

                  int thick = MinThick + MulDiv( (l1+l2)/2, MaxThick-MinThick, CampaignSupplyLine::FullSupply);
                  lineEngine.thickness(thick);

                  conEngine.addPoint(GraphicEngine::GPoint(loc1.getX(), loc1.getY()));
                  conEngine.addPoint(GraphicEngine::GPoint(loc2.getX(), loc2.getY()));
                  conEngine.close();
               }
            }
         }
      }
   }

   PixelRect rect = dib->getRect();

   for (Side side = 0; side < scenario->getNumSides(); ++side)
   {
      ColourIndex colIndex = side + 1;

      CPalette::RemapTablePtr remapTable = dib->getPalette()->getColorizeTable(scenario->getSideColour(side), 50);

      DIB_Utility::remapColor(dib, d_overlayDIB, remapTable, colIndex,
                  rect.left(), rect.top(), rect.width(), rect.height(),
                  rect.left(), rect.top());

//            DIB_Utility::colorize(dib, bufDIB, scenario->getSideColour(side), 50,
//                  rect.left(), rect.top(), rect.width(), rect.height(),
//                  rect.left(), rect.top());

            /*
             * Delete DIB
             */

//          mapData->releaseFXDIB(bufDIB);
//         }
    }

}

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
