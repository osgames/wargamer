/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef TOWNDISP_H
#define TOWNDISP_H

#ifndef __cplusplus
#error towndisp.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 *	Base class for drawing towns onto windows
 *
 * MapWindow is derived from this
 *
 *----------------------------------------------------------------------
 */

#include "town.hpp"
#include "grtypes.hpp"

class DrawDIBDC;
class MapWindowData;

/*
 * Class for displaying towns
 */

class TownDisplayImp;

class TownDisplay {
#if defined(CUSTOMIZE)
 public:
	enum {
		TownNames = 0x01,
		ImageMode = 0x02,
		DotMode   = 0x04,
		Capitals  = 0x08,
		Cities    = 0x10,
		Towns     = 0x20,
		Others    = 0x40
	};

 private:
#endif  // CUSTOMIZE

	TownDisplayImp* d_townImp;
 public:
	TownDisplay();
	~TownDisplay();

	void drawStatic(DrawDIBDC* dib, const MapWindowData& mapData);
		// Draw all the Towns onto the given DIB

#if defined(CUSTOMIZE)
	void drawStatic(DrawDIBDC* dib, const MapWindowData& mapData, UBYTE townFlags);
		// Draw all the Towns onto the given DIB (used for printing map)
#endif

	void drawDynamic(DrawDIBDC* dib, const MapWindowData& mapData);
		// Draw changed parts of towns into give DIB

};


#endif /* TOWNDISP_H */

