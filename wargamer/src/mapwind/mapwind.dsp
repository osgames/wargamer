# Microsoft Developer Studio Project File - Name="mapwind" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=mapwind - Win32 Editor Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "mapwind.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "mapwind.mak" CFG="mapwind - Win32 Editor Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "mapwind - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "mapwind - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "mapwind - Win32 Editor Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "mapwind - Win32 Editor Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 1
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "mapwind - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MAPWIND_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zi /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 gdi32.lib user32.lib /nologo /dll /debug /machine:I386

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MAPWIND_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 gdi32.lib user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/mapwindDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "mapwind___Win32_Editor_Debug"
# PROP BASE Intermediate_Dir "mapwind___Win32_Editor_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /YX"stdinc.hpp" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 gdi32.lib user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/mapwindDB.dll" /pdbtype:sept
# ADD LINK32 gdi32.lib user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/mapwindEDDB.dll" /pdbtype:sept

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "mapwind___Win32_Editor_Release"
# PROP BASE Intermediate_Dir "mapwind___Win32_Editor_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\exe"
# PROP Intermediate_Dir "o\Editor\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /YX"stdinc.hpp" /FD /c
# ADD CPP /nologo /MD /W3 /Gi /GR /GX /Zd /O2 /I "..\h" /I "..\..\res" /I "..\system" /I "..\gamesup" /I "..\ob" /I "..\campdata" /I "..\campwind" /D "NDEBUG" /D "_USRDLL" /D "EXPORT_MAPWIND_DLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "EDITOR" /YX"stdinc.hpp" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 gdi32.lib user32.lib /nologo /dll /debug /machine:I386
# ADD LINK32 gdi32.lib user32.lib /nologo /dll /debug /machine:I386 /out:"..\..\exe/mapwindED.dll"

!ENDIF 

# Begin Target

# Name "mapwind - Win32 Release"
# Name "mapwind - Win32 Debug"
# Name "mapwind - Win32 Editor Debug"
# Name "mapwind - Win32 Editor Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\condisp.cpp
# End Source File
# Begin Source File

SOURCE=.\dispunit.cpp
# End Source File
# Begin Source File

SOURCE=.\grapple.cpp
# End Source File
# Begin Source File

SOURCE=.\inserts.cpp
# End Source File
# Begin Source File

SOURCE=.\locengin.cpp
# End Source File
# Begin Source File

SOURCE=.\mapanim.cpp

!IF  "$(CFG)" == "mapwind - Win32 Release"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\mapdraw.cpp
# End Source File
# Begin Source File

SOURCE=.\mapfile.cpp
# End Source File
# Begin Source File

SOURCE=.\mapgui.cpp
# End Source File
# Begin Source File

SOURCE=.\mapwind.cpp
# End Source File
# Begin Source File

SOURCE=.\mapzoom.cpp
# End Source File
# Begin Source File

SOURCE=.\mw_data.cpp
# End Source File
# Begin Source File

SOURCE=.\mw_route.cpp

!IF  "$(CFG)" == "mapwind - Win32 Release"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\mw_track.cpp
# End Source File
# Begin Source File

SOURCE=.\mw_user.cpp
# End Source File
# Begin Source File

SOURCE=.\printmap.cpp

!IF  "$(CFG)" == "mapwind - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "mapwind - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Debug"

!ELSEIF  "$(CFG)" == "mapwind - Win32 Editor Release"

# PROP BASE Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\provdisp.cpp
# End Source File
# Begin Source File

SOURCE=.\supplydisp.cpp
# End Source File
# Begin Source File

SOURCE=.\tinymap.cpp
# End Source File
# Begin Source File

SOURCE=.\towndisp.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\condisp.hpp
# End Source File
# Begin Source File

SOURCE=.\dispunit.hpp
# End Source File
# Begin Source File

SOURCE=.\grapple.hpp
# End Source File
# Begin Source File

SOURCE=.\inserts.hpp
# End Source File
# Begin Source File

SOURCE=.\locengin.hpp
# End Source File
# Begin Source File

SOURCE=.\mapanim.hpp
# End Source File
# Begin Source File

SOURCE=.\mapdraw.hpp
# End Source File
# Begin Source File

SOURCE=.\mapfile.hpp
# End Source File
# Begin Source File

SOURCE=.\mapgui.hpp
# End Source File
# Begin Source File

SOURCE=.\mapwind.hpp
# End Source File
# Begin Source File

SOURCE=.\mapzoom.hpp
# End Source File
# Begin Source File

SOURCE=.\mw_data.hpp
# End Source File
# Begin Source File

SOURCE=.\mw_route.hpp
# End Source File
# Begin Source File

SOURCE=.\mw_track.hpp
# End Source File
# Begin Source File

SOURCE=.\mw_user.hpp
# End Source File
# Begin Source File

SOURCE=.\mwdll.h
# End Source File
# Begin Source File

SOURCE=.\printmap.hpp
# End Source File
# Begin Source File

SOURCE=.\provdisp.hpp
# End Source File
# Begin Source File

SOURCE=.\stdinc.hpp
# End Source File
# Begin Source File

SOURCE=.\supplydisp.hpp
# End Source File
# Begin Source File

SOURCE=.\tinymap.hpp
# End Source File
# Begin Source File

SOURCE=.\towndisp.hpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
