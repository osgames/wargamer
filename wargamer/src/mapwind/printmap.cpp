/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
#include "stdinc.hpp"
#include "printmap.hpp"
#include "resdef.h"
#include "dialog.hpp"
#include "towndisp.hpp"
#include "provdisp.hpp"
#include "inserts.hpp"
#include "condisp.hpp"
#include "campdint.hpp"
#include "dib.hpp"
#include "print.hpp"
#include "winctrl.hpp"
#include "mw_data.hpp"
//#include "app.hpp"

/*
 *-----------------------------------------------------------
 *
 * Print Map Dialog
 */

class PrintMapDial : public ModelessDialog {
    const MapWindowData* d_gameMapData;   // acutal map data from mapwindow
    MapWindowData* d_mapData;             // mapdata for our local map
    DrawDIBDC* d_mapDib;
    HWND d_mapHWND;
    HDC d_printDC;
    DOCINFO d_docInfo;
    MagEnum d_magLevel;

    TownDisplay d_townDisp;
    ProvinceDisplay d_provDisp;
    InsertDisplay d_inserts;
    ConnectionDisplay d_conDisp;

  public:

    PrintMapDial(const MapWindowData* mapData, HWND mapHWND) :
      d_gameMapData(mapData),
      d_mapData(new MapWindowData(NULL, mapData->d_campData)),
      d_mapDib(0),
      d_mapHWND(mapHWND),
      d_printDC(0),
      d_magLevel(mapData->getZoomEnum())
    {
      ASSERT(d_mapData);
      ASSERT(d_mapHWND);

      HWND hwnd = createDialog(printMapDialog, d_mapHWND, False);
      ASSERT(hwnd);
    }

    ~PrintMapDial()
    {
      if(d_mapData)
        delete d_mapData;

      if(d_mapDib)
        delete d_mapDib;
    }

private:
   BOOL procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

   BOOL onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
   void onDestroy(HWND hwnd);
   void onClose(HWND hwnd);
   void onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

   void printSetup();
   void printMap();
   void drawMap();
   int numPages();
   void enableControls();
//   void cancelPrint();
};

BOOL PrintMapDial::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      HANDLE_DLG_MSG(WM_INITDIALOG, onInitDialog);
      HANDLE_DLG_MSG(WM_DESTROY, onDestroy);
      HANDLE_DLG_MSG(WM_COMMAND, onCommand);
      HANDLE_DLG_MSG(WM_CLOSE, onClose);

      default:
         return FALSE;
   }
   return TRUE;
}

static const int SB_Width = 15;
static const int SB_Height = 15;

BOOL PrintMapDial::onInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
  RECT r;
  GetClientRect(d_mapHWND, &r);

  const width = (r.right - r.left) - SB_Width;
  const height = (r.bottom - r.top) - SB_Height;

  d_mapDib = new DrawDIBDC(width, height);
  ASSERT(d_mapDib);

  PixelPoint p(width, height);
  d_mapData->setViewSize(p);
  d_mapData->setZoomEnum(d_magLevel);
  d_mapData->setMapX(0);
  d_mapData->setMapY(0);

  memset(&d_docInfo, 0, sizeof(DOCINFO));
  d_docInfo.cbSize = sizeof(DOCINFO);
  d_docInfo.lpszDocName = "Campaign Map";

  enableControls();

  return TRUE;
}

void PrintMapDial::onDestroy(HWND hwnd)
{
  if(d_printDC)
    DeleteDC(d_printDC);
}

void PrintMapDial::onClose(HWND hwnd)
{
}

void PrintMapDial::onCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
  switch(id)
  {
    case PM_PRINTSETUP:
      printSetup();
      break;

    case PM_PRINT:
      printMap();
      break;

    case PM_CLOSE:
      DestroyWindow(hwnd);
      break;
  }
}

/*
 * Detemine the number of pages it will take to print the map
 */

int PrintMapDial::numPages()
{
  const int fw = d_mapData->getFullWidth();
  const int fh = d_mapData->getFullHeight();
  const int vw = d_mapData->getViewWidth();
  const int vh = d_mapData->getViewHeight();

  ASSERT(vw > 0);
  ASSERT(vh > 0);

  int nColumns = (fw == vw) ? 1 : (fw / vw) + 1;
  int nRows = (fh == vh) ? 1 : (fh / vh) + 1;

  return (nColumns * nRows);
}

/*
 * Call the Print setup dialog, so user can select printer
 */

void PrintMapDial::printSetup()
{
  if(Print::print(getHWND(), d_printDC, numPages()))
  {
    ASSERT(d_printDC);

    /*
     * Make sure driver does what it needs to do
     */

    int flags = GetDeviceCaps(d_printDC, RASTERCAPS);
    if ( ((flags & RC_STRETCHBLT) == 0) ||
         ((flags & RC_BITMAP64) == 0) )
    {
//    DeleteDC(d_printDC);
//    d_printDC = 0;

      static const char text[] = "Printer incapable of printing map!";
      MessageBox(getHWND(), text, "Error", MB_OK | MB_ICONERROR);
    }
  }

  enableControls();
}

void PrintMapDial::enableControls()
{
  {
    Button b(getHWND(), PM_PRINT);
    b.enable(d_printDC != 0);
  }
}

/*
 * Print out the map
 */

void PrintMapDial::printMap()
{
  /*
   * Start Print Job
   */

#if 1
  ASSERT(d_printDC);
  int result = StartDoc(d_printDC, &d_docInfo);
  ASSERT(result);
#endif

//  d_printing = True;

  /*
   * Set view size
   */

  RECT rect;
  GetClientRect(d_mapHWND, &rect);
  const width = (rect.right - rect.left) - SB_Width;
  const height = (rect.bottom - rect.top) - SB_Height;
  ASSERT(width == d_mapDib->getWidth());
  ASSERT(height == d_mapDib->getHeight());
  PixelPoint p(width, height);
  d_mapData->setZoomEnum(d_gameMapData->getZoomEnum());
  d_mapData->setViewSize(p);

  /*
   * Some useful values
   */

  const int fw = d_mapData->getFullWidth();
  const int fh = d_mapData->getFullHeight();
  const int vw = d_mapData->getViewWidth();
  const int vh = d_mapData->getViewHeight();
  int nColumns = (fw / vw);
  int nRows = (fh / vh);

  if((fw % vw) != 0)
    nColumns++;
  if((fh % vh) != 0)
    nRows++;

  /*
   * Retrieve the number of pixels-per-logical-inch
   * in the horizontal and vertical directions
   * for the display upon which the bitmap
   * was created.
   */

  float fLogPelsX1 = (float) GetDeviceCaps(d_mapDib->getDC(), LOGPIXELSX);
  float fLogPelsY1 = (float) GetDeviceCaps(d_mapDib->getDC(), LOGPIXELSY);

  /*
   * Retrieve the number of pixels-per-logical-inch
   * in the horizontal and vertical directions
   * for the printer upon which the bitmap
   * will be printed.
   */

  float fLogPelsX2 = (float) GetDeviceCaps(d_printDC, LOGPIXELSX);
  float fLogPelsY2 = (float) GetDeviceCaps(d_printDC, LOGPIXELSY);

  /*
   * Determine the scaling factors required to
   * print the bitmap and retain its original
   * proportions.
   */

  float fScaleX = (fLogPelsX1 > fLogPelsX2) ?
         fLogPelsX1 / fLogPelsX2 : fLogPelsX2 / fLogPelsX1;

  float fScaleY = (fLogPelsY1 > fLogPelsY2) ?
         fLogPelsY1 / fLogPelsY2 : fLogPelsY2 / fLogPelsY1;

  // Top corner of the map (relative to mapfile)
  const int midX = d_mapData->getMidX();
  const int midY = d_mapData->getMidY();

  /*
   * Initialize progress bar
   */

  HWND hProg = GetDlgItem(getHWND(), PM_PROGRESS);
  ASSERT(hProg);

  UBYTE pos = 0;
  SendMessage(hProg, PBM_SETRANGE, 0, MAKELPARAM(pos, nRows * nColumns));

  /*
   * Print pages
   */

  for(int r = 0; r < nRows; r++)
  {
    // calc topY positions
    const int topYFile = (r * vh) + midY;

    for(int c = 0; c < nColumns; c++)
    {
#if 1
      // start page
      result = StartPage(d_printDC);
      ASSERT(result > 0);
#endif
      // calc topX positions
      const int topXFile = (c * vw) + midX;

      // set mapfile position
      d_mapData->setMapX(topXFile);
      d_mapData->setMapY(topYFile);

      // get map from the mapfile
      d_mapData->drawStaticMap(d_mapDib);

      // draw items on the map
      drawMap();

      /*
       * Compute the coordinate of the upper left
       * corner of the centered bitmap.
       */

      int dibX = 0;
      int dibY = 0;
      int dibW = d_mapDib->getWidth();
      int dibH = d_mapDib->getHeight();

      if((topXFile - midX) > (fw - vw))
      {
        dibX = d_mapData->mapToPixel((topXFile - midX) - (fw - vw));
        dibW -= dibX;
      }

      if((topYFile - midY) > (fh - vh))
      {
        dibY = d_mapData->mapToPixel((topYFile - midY) - (fh - vh));
        dibH -= dibY;
      }

      int cWidthPels = GetDeviceCaps(d_printDC, HORZRES);
      int xLeft = ((cWidthPels / 2) - ((int) (((float) (dibW)) * fScaleX)) / 2);
      int cHeightPels = GetDeviceCaps(d_printDC, VERTRES);
      int yTop = ((cHeightPels / 2) - ((int) (((float) (dibH)) * fScaleY)) / 2);
      int xWidth =  (int) ((float) dibW * fScaleX);
      int yHeight = (int) ((float) dibH * fScaleY);

      /*
       * Clip
       */

      if(xLeft < 0 ||
         yTop < 0  ||
         xWidth >= cWidthPels ||
         yHeight >= cHeightPels)
      {
        /*
         * Clip for width
         */

        if(xLeft < 0 || xWidth > cWidthPels)
        {
          // get total width
          int w = xWidth - xLeft;
          ASSERT(w > 0);
          ASSERT(w > cWidthPels);

          // get difference percent
          int cwDif = w - cWidthPels;
          int percent = MulDiv(cwDif, 100, w);

          // adjust y axis
          int h = yHeight - yTop;
          int cyDif = MulDiv(percent, h, 100);
          yTop += cyDif/2;
          yTop += ((cyDif % 2) != 0) ? 1 : 0;
          yHeight -= cyDif/2;

          xLeft = 0;
          xWidth = cWidthPels;
        }

        /*
         * Clip for height
         */

        if(yTop < 0 || yHeight > cHeightPels)
        {
          // get total height
          int h = yHeight - yTop;
          ASSERT(h > 0);
          ASSERT(h > cHeightPels);

          // get difference percent
          int cyDif = h - cHeightPels;
          int percent = MulDiv(cyDif, 100, h);

          // adjust x axis
          int w = xWidth - xLeft;
          int cxDif = MulDiv(percent, w, 100);
          xLeft += cxDif/2;
          xLeft += ((cxDif % 2) != 0) ? 1 : 0;
          xWidth -= cxDif/2;

          yTop = 0;
          yHeight = cHeightPels;
        }
      }

      /*
       * Blit dib to printDC
       */
#if 1
      StretchBlt(d_printDC, xLeft, yTop, xWidth, yHeight,
          d_mapDib->getDC(), dibX, dibY, dibW, dibH, SRCCOPY);


      // end page
      result = EndPage(d_printDC);
      ASSERT(result > 0);
#endif
      SendMessage(hProg, PBM_SETPOS, static_cast<WPARAM>(++pos), 0);
    }
  }

  /*
   * End print job and delete print dc
   */

#if 1
  EndDoc(d_printDC);
#endif

  SendMessage(hProg, PBM_SETPOS, 0, 0);
}

void PrintMapDial::drawMap()
{
  // should we draw inserts?
  {
    Button b(getHWND(), PM_OFFSCREEN);
    if(b.getCheck())
      d_inserts.drawInserts(d_mapDib, *d_mapData);
  }

  // should we draw connections?
  {
    Button b(getHWND(), PM_CONNECTIONS);
    if(b.getCheck())
      d_conDisp.draw(d_mapDib, *d_mapData, *d_mapData->d_campData);
  }

  // should we draw province names?
  {
    Button b(getHWND(), PM_PROVINCENAMES);
    if(b.getCheck())
      d_provDisp.drawStatic(d_mapDib, *d_mapData);
  }

  // town flags
  UBYTE townFlags = 0;

  // should we draw town names
  {
    Button b(getHWND(), PM_TOWNNAMES);
    if(b.getCheck())
      townFlags |= TownDisplay::TownNames;
  }

  // should we use dot mode
  {
    Button b(getHWND(), PM_DOTMODE);
    if(b.getCheck())
      townFlags |= TownDisplay::DotMode;
  }

  // should we draw Capitals
  {
    Button b(getHWND(), PM_CAPITALS);
    if(b.getCheck())
      townFlags |= TownDisplay::Capitals;
  }

  // should we draw Cities
  {
    Button b(getHWND(), PM_CITIES);
    if(b.getCheck())
      townFlags |= TownDisplay::Cities;
  }

  // should we draw Capitals
  {
    Button b(getHWND(), PM_TOWNS);
    if(b.getCheck())
      townFlags |= TownDisplay::Towns;
  }

  // should we draw Capitals
  {
    Button b(getHWND(), PM_OTHER);
    if(b.getCheck())
      townFlags |= TownDisplay::Others;
  }

  d_townDisp.drawStatic(d_mapDib, *d_mapData, townFlags);
}

#if 0
void PrintMapDial::cancelPrint()
{
  if(d_printDC)
  {
    int result = AbortDoc(d_printDC);
    ASSERT(result);
//  d_printing = False;
  }
}
#endif

/*--------------------------------------------------------------
 *    OutSide access
 */

void PrintMap::printMap(const MapWindowData* mapData, HWND mapHWND)
{
  new PrintMapDial(mapData, mapHWND);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
