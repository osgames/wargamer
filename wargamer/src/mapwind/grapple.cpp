/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Implementation of Grapples
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "grapple.hpp"

/*-----------------------------------------
 * Grapple Class Implementation
 */


Grapple::Grapple()
{
   left = 0;
   top = 0;
   right = 0;
   bottom = 0;
   cx = 0;
   cy = 0;
   nVia = 0;
   active = False;
}

void Grapple::setGrapple(ULONG l, ULONG t, ULONG r, ULONG b, ULONG x, ULONG y, int n)
{
   left = l;
   top = t;
   right = r;
   bottom = b;
   cx = x;
   cy = y;
   nVia = n;
   active = True;
}

Boolean Grapple::checkGrapple(int x, int y) const
{
   if(x > left && y > top && x < right && y < bottom)
      return True;
   else
      return False;
}

void Grapple::clear()
{
   left = 0;
   top = 0;
   right = 0;
   bottom = 0;
   cx = 0;
   cy = 0;
   nVia = 0;
   active = False;
}




/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
