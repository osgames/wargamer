/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef LOCENGIN_HPP
#define LOCENGIN_HPP

#ifndef __cplusplus
#error locengin.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	graphic pipeline engine for converting physical coordinates to pixels
 *
 *----------------------------------------------------------------------
 */

#include "gengine.hpp"

class MapWindowData;
class Location;

/*
 * Connection Drawing Engine
 * Receives Physical Points and outputs Pixel Points
 */

class LocationEngine : public GraphicEngine
{
	public:
		LocationEngine(GraphicEngine& output, const MapWindowData& mapData);
		~LocationEngine();

		void addPoint(const GPoint& p);
		void addPoint(const Location& l);

		void close();
		// void setPen(int style, int width, COLORREF col);

	private:
		const MapWindowData& d_mapData;
		GraphicEngine& d_output;
};



#endif /* LOCENGIN_HPP */

