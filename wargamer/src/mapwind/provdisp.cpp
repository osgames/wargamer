/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Province Display
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "provdisp.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "mw_data.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "wmisc.hpp"
#include "fonts.hpp"
#include "scenario.hpp"
#include "myassert.hpp"

/*
 * Flags used when drawing on map
 *  OR Values together.
 */

enum MapDrawFlags {
   MD_NORMAL = 0,
   MD_HIGHLIGHT = 1,
};



class ProvinceDisplayImp {
// DrawDIBDC* d_textMask;
   DrawDIBDC* dib;
   BOOL started;

   int fontHeight;
   // int offScreenFontHeight;

   // HFONT hfont;
   Font hfont;
   // Font hfontOffScreen;

public:
   ProvinceDisplayImp();
   ~ProvinceDisplayImp();

   void drawProvince(DrawDIBDC* dib, const MapWindowData& mapData, IProvince iprov, MapDrawFlags flags);
      // Draw a single Province

   void draw(DrawDIBDC* dib);
      // Draw all the provinces onto the given DIB

   BOOL startDisplay(DrawDIBDC* dib, const MapWindowData& mapData);
   void endDisplay();

   int getFontHeight() const { return fontHeight; }
   // int getOffScreenFontHeight() const { return offScreenFontHeight; }
private:
// void setTextMaskDIB(const CampaignData* campData);  // allocate a dib for a text mask
};



ProvinceDisplayImp::ProvinceDisplayImp()
{
// d_textMask = 0;
   started = FALSE;
   // hfont = NULL;
}

ProvinceDisplayImp::~ProvinceDisplayImp()
{
   ASSERT(!started);

   if(started)
      endDisplay();

}

#if 0
void ProvinceDisplayImp::setTextMaskDIB(const CampaignData* campData)
{
   dib->selectObject(hfont);

  /*
   * Iterate through provinces and get the longest
   */

  const ProvinceList& pl = campData->getProvinces();

  int cx = 0;
  int cy = 0;

  for(IProvince  i = 0; i < pl.entries(); i++)
  {
    const Province& p = pl[i];

    SIZE s;
    GetTextExtentPoint32(dib->getDC(), p.getNameNotNull(), lstrlen(p.getNameNotNull()), &s);

    if(s.cx > cx)
    {
#ifdef DEBUG
      if(cy != 0)
        ASSERT(s.cy == cy);
#endif
      cx = s.cx;
      cy = s.cy;
    }
  }

  if(!d_textMask || (d_textMask->getWidth() != cx || d_textMask->getHeight() != cy))
  {
    if(d_textMask)
    {
      delete d_textMask;
      d_textMask = 0;
    }

    d_textMask = new DrawDIBDC(cx+2, cy+2);
    ASSERT(d_textMask != 0);
  }

  /*
   * fill in mask with white
   */

  ColourIndex ci = d_textMask->getColour(PALETTERGB(255, 255, 255));
  d_textMask->fill(ci);
  d_textMask->setMaskColour(ci);

  d_textMask->setBkMode(TRANSPARENT);
  d_textMask->setTextColor(PALETTERGB(0, 0, 0));
  d_textMask->selectObject(hfont);
}
#endif

BOOL ProvinceDisplayImp::startDisplay(DrawDIBDC* _dib, const MapWindowData& mapData)
{
   dib = _dib;
   ASSERT(dib != 0);

   ASSERT(started == FALSE);

   LONG magLevel = mapData.getZoomLevel();

   if(magLevel <= 4)
   {
      dib->saveDC();

      // fontHeight = magLevel * 18; //24;
      fontHeight = magLevel * 24;
      fontHeight = (fontHeight * mapData.displayWidth()) / BaseDisplayWidth;
      fontHeight = maximum(fontHeight, MinFontHeight);

      LogFont lf;
      lf.height(fontHeight);
      lf.weight(FW_MEDIUM);

      if(fontHeight > 20)
         lf.face(scenario->fontName(Font_Decorative));
      else if(fontHeight > 12)
         lf.face(scenario->fontName(Font_Bold));
      else
         lf.style(FF_SWISS);

      hfont.set(lf);

      dib->setBkMode(TRANSPARENT);
      dib->setTextColor(PALETTERGB(0, 0, 0));
      dib->setTextAlign(TA_TOP | TA_CENTER);
      dib->selectObject(hfont);

      started = TRUE;

      return TRUE;
   }
   else
      return FALSE;
}

void ProvinceDisplayImp::endDisplay()
{
   ASSERT(started == TRUE);

   if(started)
   {
      dib->restoreDC();
      // gApp.restoreDibDC();
//    DeleteObject(hfont);
      // hfont = NULL;
      hfont.reset();

      started = FALSE;
   }
}




void ProvinceDisplayImp::drawProvince(DrawDIBDC* dib, const MapWindowData& mapData, IProvince iprov, MapDrawFlags flags)
{
   ASSERT(dib != 0);

   const Province* prov = &mapData.d_campData->getProvince(iprov);


   if(prov->isOffScreen())
      return;

   Location l = prov->getLocation();
   const char* name = prov->getName();

   ASSERT(name != 0);

   // const ProvinceDisplayImp& pd = getProvinceDisplayImp();

   PixelPoint p;
   mapData.locationToPixel(l, p);

   int fontHeight;

   fontHeight = getFontHeight();
   dib->selectObject(hfont);
// d_textMask->selectObject(hfont);

   p.setY(p.getY() - fontHeight / 2);
   SIZE s;
   GetTextExtentPoint32(dib->getDC(), name, lstrlen(name), &s);

   DIB_Utility::Draw3DTextData data;
   data.d_text = name;
   data.d_remapPercent = (flags & MD_HIGHLIGHT) ? 50 : 40;
   data.d_hFont = hfont;
   data.d_x = p.getX()-s.cx/2;
   data.d_y = p.getY();
   data.d_flags = (flags & MD_HIGHLIGHT) ? DIB_Utility::Draw3DTextData::Imbedded | DIB_Utility::Draw3DTextData::Colorize :
      DIB_Utility::Draw3DTextData::Imbedded;
   data.d_color = RGB(255,192,192);

   DIB_Utility::draw3DText(dib, data);

#if 0
   dib->setTextColor(PALETTERGB(0, 0, 0));

   ASSERT(d_textMask != 0);
   ColourIndex ci = d_textMask->getColour(PALETTERGB(255, 255, 255));
   d_textMask->fill(ci);

   wTextOut(d_textMask->getDC(), 1, 1, name);

   SIZE s;
   GetTextExtentPoint32(d_textMask->getDC(), name, lstrlen(name), &s);

   dib->setMaskColour(ci);
   if(flags & MD_HIGHLIGHT)
   {
      // dib->darken(d_textMask, (p.getX()+2)-(s.cx/2), p.getY()+2, 35);
      // dib->lighten(d_textMask, p.getX()-(s.cx/2), p.getY(), 40);
      DIB_Utility::colorize(dib, d_textMask, RGB(255,192,192), 50,
         p.getX()-s.cx/2, p.getY(), s.cx, s.cy, 0, 0);
   }
   else
   {
      dib->lighten(d_textMask, (p.getX()+2)-(s.cx/2), p.getY()+2, 35);
      dib->darken(d_textMask, p.getX()-(s.cx/2), p.getY(), 40);
   }
#endif
}



ProvinceDisplay::ProvinceDisplay() :
   d_provImp(new ProvinceDisplayImp)
{
  ASSERT(d_provImp != 0);
}

ProvinceDisplay::~ProvinceDisplay()
{
  if(d_provImp)
    delete d_provImp;
}


/*
 * Draw all Provinces onto static map
 */


void ProvinceDisplay::drawStatic(DrawDIBDC* dib, const MapWindowData& mapData)
{
   ASSERT(d_provImp != 0);
   ASSERT(dib != 0);

   if(d_provImp->startDisplay(dib, mapData))
   {
      ProvinceIter pl = mapData.d_campData->getProvinces();

      while(++pl)
      {
         // Province& prov = pl.current();

         d_provImp->drawProvince(dib, mapData, pl.currentID(), MD_NORMAL);
      }

      d_provImp->endDisplay();
   }
}

void ProvinceDisplay::drawDynamic(DrawDIBDC* dib, const MapWindowData& mapData)
{
   ASSERT(d_provImp != 0);
   ASSERT(dib != 0);

   if(mapData.d_selectedProvince != NoProvince)
   {
      if(d_provImp->startDisplay(dib, mapData))
      {
         d_provImp->drawProvince(dib, mapData, mapData.d_selectedProvince, MD_HIGHLIGHT);
         d_provImp->endDisplay();
      }
   }
}


/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
