/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef GRAPPLE_HPP
#define GRAPPLE_HPP

#ifndef __cplusplus
#error grapple.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	grapples are things that can be dragged
 *
 *----------------------------------------------------------------------
 */

#include "mytypes.h"

class Grapple {
public:
	ULONG left;
	ULONG top;
	ULONG right;
	ULONG bottom;
	ULONG cx;
	ULONG cy;
	Boolean active;
	int nVia;
public:
	Grapple();
	void setGrapple(ULONG l, ULONG t, ULONG r, ULONG b, ULONG x, ULONG y, int n);
	Boolean checkGrapple(int x, int y) const;
	void clear();
};


#endif /* GRAPPLE_HPP */

