/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef MAPGUI_HPP
#define MAPGUI_HPP

#ifndef __cplusplus
#error mapgui.hpp is for use with C++
#endif

/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Map Window User Interface
 *
 *----------------------------------------------------------------------
 */

#include "mapzoom.hpp"		// For magenum
#include "mw_user.hpp"		// for TrackMode
#include "mw_track.hpp"
#include "mapdraw.hpp"
#include "wind.hpp"
#include "autoptr.hpp"
#include "grtypes.hpp"
// #include "critical.hpp"

class MapDraw;
class ImageLibrary;
class RouteList;

/*
 * Implementation class
 */

class MapGUI :
	public WindowBaseND
	// public SharedData			// prevent multiple threads from accessing data
{
	static const char s_className[];
	static const char mapWindowRegName[];
	static const char regNameZoom[];
	static const char regNamePosition[];
	static ATOM classAtom;

	MapWindowData* d_mapData;			// Pointer to MapWindowData
	MapWindowTrack d_mapTrack;			// Map Tracker

   std::auto_ptr<MapDraw> d_mapDraw;				// Drawing

	HWND d_hwndVScroll;						// Scroll Bar Handle
	HWND d_hwndHScroll;
	PixelPoint d_mouseStartingLocation;		// Point where mouse button was pressed
	MagEnum d_wantedZoomLevel;					// Magnification level to zoom to
	Boolean d_zooming;							// User is zooming in
	Boolean d_buttonPressed;					// Left button is pressed
	Boolean d_ctrlKey;							// Control Key is pressed

	Location d_mouseClickLocation;			// Position clicked for new object
	Boolean d_gotClickLocation;				// mouseClickLocation is valid

   ImageLibrary* d_weatherButtons;        // Button images for Show Weather Button

	MapGUI(const MapGUI&);
	MapGUI& operator = (const MapGUI);
 public:
 	MapGUI(MapWindowData* mapData);
	~MapGUI(); // { }

    virtual const char* className() const { return s_className; }

	void makeWindow(HWND parent);

	BOOL isZooming() const { return d_zooming; }
	void doZoom(MagEnum magLevel, const MapPoint& mc);
	void doZoom(const MapPoint& mc);

	void setMapPos(MapCord x, MapCord y);
	void requestRedraw(BOOL all);
	void forceDraw();


#if defined(CUSTOMIZE)
	void printMap();
	void setTrackMode(TrackMode mode);
#endif
	Location getClickLocation() const;

	void toggleZoomMode();
	void zoomOut();
	void zoomTo(MagEnum magLevel);
	void zoomOut(MagEnum magLevel);
	void centerOnMap(const Location& l, MagEnum magLevel);

#if !defined(EDITOR)
	void drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from, const CampaignOrder& order, Side side)
	{
		d_mapDraw->drawRoute(dib, mapData, from, order, side);
	}
	void drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from, const CampaignOrder& order, Side side, const RouteList& rl)
	{
		d_mapDraw->drawRoute(dib, mapData, from, order, side, rl);
	}
#endif

	// Declare all functions private initially, then we
	// can find out what really is used by other components
 private:
	static ATOM registerClass();
	// static LPCSTR getClassName() { return className; }

	LRESULT procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	void onDestroy(HWND hWnd);
	void onPaint(HWND hWnd);
	BOOL onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct);
	void onSize(HWND hwnd, UINT state, int cx, int cy);
	//---- Removed 12Feb97
	// void onGetMinMaxInfo(HWND hWnd, LPMINMAXINFO lpMinMaxInfo);
	void onHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
	void onVScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);
	void onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	void onRButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	void onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
	// void onKey(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);

	void onMouseMove(HWND hwnd, int x, int y, UINT keyFlags);
	BOOL onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg);
	void onCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
   void onDrawItem(HWND hWnd, const DRAWITEMSTRUCT* lpDrawItem);
#if !defined(EDITOR)
	void onTimer(HWND hwnd, UINT id);   // for animations
#endif

	void doZoom(const PixelPoint& p);


	void setScrollRange();
	void setXScroll();
	void setYScroll();
	void setMapSize();

	void doPopupMenu(HWND hwnd, PixelPoint& pt);
};



#endif /* MAPGUI_HPP */

