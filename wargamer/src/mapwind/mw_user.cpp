/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Map Window User Implementation
 *
 * Implements functions belonging to data classes that MapWindow user's
 * make use of.
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mw_user.hpp"


/*-----------------------------------------
 * MapSelect Implementation
 */
MapSelect::MapSelect()
{
  selectedTown = NoTown;
  selectedProvince = NoProvince;
  trackedUnits = 0;
  currentUnit = NoUnitListID;
  currentGeneral = NoUnitListID;
  // grapple = 0;
  ctrlKey = False;
  shiftKey = False;
}


ConstICommandPosition MapSelect::getCP() const
{
   if(currentUnit != NoUnitListID)
      return trackedUnits->getUnit(currentUnit);
   else
      return NoCommandPosition;
}

Boolean MapSelect::hasUnit() const
{
  return Boolean(trackedUnits->unitCount() > 0);
}


/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
