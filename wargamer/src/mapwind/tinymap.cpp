/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Tiny Map Window Implementation
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "tinymap.hpp"
#include "town.hpp"
#include "app.hpp"
#include "scenario.hpp"
#include "mapwind.hpp"
#include "mw_data.hpp"
#include "resdef.h"
#include "dib.hpp"
#include "bmp.hpp"
#include "wmisc.hpp"
#include "campdint.hpp"
#include "armies.hpp"
#include "bargraph.hpp"
#include "palette.hpp"
#include "armyutil.hpp"
//#include "tooltip.hpp"
//#include "res_str.h"        // Resource Strings
//#include "scn_res.h"
//#include "imglib.hpp"
// #include "generic.hpp"
#include "help.h"
#include "button.hpp"
//#include "registry.hpp"


                              
#if 0
static const char tinyMapWindowRegName[] = "tinyMapWindow";
#endif


TinyMapWindow::TinyMapWindow(HWND parent, MapWindow* map) :
   d_dib(0),
   d_staticDIB(0),
   d_dragging(False),
   d_mapWindow(map)
{
   ASSERT(map != 0);

   HWND hWnd = createWindow(
      0,
      // GenericClass::className(),
      "Tiny Map",
         WS_CHILD |
         WS_CLIPSIBLINGS,
      0,
      0,
      0,
      0,
      parent,
      NULL     // (HMENU) WID_TinyMap,
      // APP::instance()
   );

   ASSERT(hWnd != NULL);
}

TinyMapWindow::~TinyMapWindow()
{
  if(d_dib)
    delete d_dib;

  if(d_staticDIB)
    delete d_staticDIB;
}

BOOL TinyMapWindow::onCreate(HWND hWnd, CREATESTRUCT* lpCreateStruct)
{
   debugLog("TinyMapWindow::onCreate(%p)\n", hWnd);

   ASSERT(scenario != 0);
   const char* fileName = scenario->getTinyMapFileName();
   ASSERT(fileName != 0);

   /*
    * Oooerr.. this looks a bit dodgy because readBMP will create DIB
    * rather than  DrawDIBDC.
    */

   d_staticDIB = BMP::newDrawDIB(fileName, BMP::RBMP_Normal);

   d_dib = new DrawDIBDC(d_staticDIB->getWidth(), d_staticDIB->getHeight());
   ASSERT(d_dib != 0);

   drawTownsOnStatic();

   debugLog("TinyMapWindow::onCreate()... End\n");

   return TRUE;
}


void TinyMapWindow::onDestroy(HWND hWnd)
{
   debugLog("TinyMapWindow::onDestroy()\n");
}


BOOL TinyMapWindow::onSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg)
{
   if(d_mapWindow->isZooming())
   {
      ASSERT(scenario->getZoomCursor() != NULL);
      SetCursor(scenario->getZoomCursor());
      return TRUE;
   }
   else
      return FORWARD_WM_SETCURSOR(hwnd, hwndCursor, codeHitTest, msg, defProc);
}


void TinyMapWindow::onSize(HWND hWnd, UINT state, int cx, int cy)
{
}

/*
 * Draw onto the DIB
 * (called from MapWindow thread
 */

void TinyMapWindow::draw(ConstICommandPosition cpi)
{
   ASSERT(d_dib);
   ASSERT(d_staticDIB);

   if(d_staticDIB)
      d_dib->blitFrom(d_staticDIB);

   drawUnits(cpi);

   areaChanged();
}

void TinyMapWindow::onPaint(HWND hWnd)
{
   PAINTSTRUCT ps;
   HDC hdc = BeginPaint(hWnd, &ps);

   SetBkMode(hdc, TRANSPARENT);

   HPALETTE oldPal = SelectPalette(hdc, Palette::get(), FALSE);
   RealizePalette(hdc);

   // Copy the DIB onto screen
   // SelectObject(hDibDC, dib->getHandle());
   BitBlt(hdc, 0,0, d_dib->getWidth(), d_dib->getHeight(), d_dib->getDC(), 0,0, SRCCOPY);

   /*
    * Restore DC settings
    */

// SelectPalette(hdc, oldPal, FALSE);
   EndPaint(hWnd, &ps);
}

// draw towns on the static map
void TinyMapWindow::drawTownsOnStatic()
{
   const CampaignData* campData = d_mapWindow->mapData().d_campData;
   CArrayIter<Province> pl = campData->getProvinces();

   while(++pl)
   {
     const Province& prov = pl.current();
     ITown iCapitol = prov.getCapital();
     if(iCapitol != NoTown)
     {
       const TownList& tl = campData->getTowns();
       const Town& town = tl[iCapitol];
       const Location& l = town.getLocation();
       PixelPoint p;
       locationToTinyMap(l, p);
       Side side = town.getSide();
       ColourIndex color = d_staticDIB->getColour(scenario->getSideColour(side));      // Yellow

       d_staticDIB->hLine(p.getX()-2, p.getY(), 5, color);
       d_staticDIB->vLine(p.getX(), p.getY()-2, 5, color);

     }
   }
}

/*
 * This may be better if it uses the DIB draw functions instead of GDI.
 */

void TinyMapWindow::drawUnits(ConstICommandPosition cpi)
{
   ASSERT(d_mapWindow);
   HDC hdc = d_dib->getDC();

   const CampaignData* campData = d_mapWindow->mapData().d_campData;

   static ColourIndex black = d_dib->getColour(RGB(0,0,0));
   static ColourIndex white = d_dib->getColour(RGB(255,255,255));

   /*
    * highlight current town, if any
    */

   if(d_mapWindow->getHighlightedTown() != NoTown)
   {
     const Town& town = campData->getTown(d_mapWindow->getHighlightedTown());

     const Location& l = town.getLocation();
     PixelPoint p;
     locationToTinyMap(l, p);
     d_dib->frame(p.getX()-2, p.getY()-2, 5, 5, white);
   }

   const Armies& armies = campData->getArmies();
   NationIter nationIter = &armies;

   while(++nationIter)
   {
      Side side = nationIter.current();

      COLORREF rgb = scenario->getSideColour(side);
      COLORREF brightRGB = Palette::brightColour(rgb, 255, 128);
      ColourIndex normalC = d_dib->getColour(rgb);    // Yellow
      ColourIndex brightC = d_dib->getColour(brightRGB);    // Yellow

      ConstUnitIter unitIter(&armies, armies.getFirstUnit(side));
      while(unitIter.sister())
      {
         ConstICommandPosition unit = unitIter.current();
         // CommandPosition* cp = armies.getCommand(unit);

         if(CampaignArmy_Util::isUnitVisible(campData, unit))
         {
           const CampaignPosition& pos = CampaignArmy_Util::getUnitDisplayPosition(unit);

           Location l;
           unit->getLocation(l);
           PixelPoint p;
           locationToTinyMap(l, p);

           d_dib->rect(p.getX()-1, p.getY()-1, 3, 3, (unit == cpi) ? brightC : normalC);
           if(unit == cpi)
             d_dib->frame(p.getX()-2, p.getY()-2, 5, 5, white);
           else
             d_dib->frame(p.getX()-2, p.getY()-2, 5, 5, black);
         }

      }
   }

   drawMapRect(hdc);
}

void TinyMapWindow::drawMapRect(HDC hdc)
{
   RECT cRect;
   GetClientRect(hWnd, &cRect);

   MapWindow* mw = getMapWindow();

   HPEN pen;

   if(d_dragging)
      pen = CreatePen(PS_SOLID, 1, RGB(255,0,255));
   else
   {
      d_zoomArea.left    = (mw->getTopX() * (cRect.right  - cRect.left)) / mw->getFullWidth();
      d_zoomArea.top     = (mw->getTopY() * (cRect.bottom - cRect.top))  / mw->getFullHeight();
      d_zoomArea.right   = (mw->getViewWidth() * d_dib->getWidth())  / mw->getFullWidth();
      d_zoomArea.bottom  = (mw->getViewHeight() * d_dib->getHeight()) / mw->getFullHeight();

      pen = CreatePen(PS_SOLID, 1, RGB(0,0,0));
   }

   int x1 = d_zoomArea.left;
   int y1 = d_zoomArea.top;
   int x2 = x1 + d_zoomArea.right - 1;
   int y2 = y1 + d_zoomArea.bottom - 1;

   HPEN oldPen = (HPEN) SelectObject(hdc, pen);

   POINT oldP;
   MoveToEx(hdc, x1, y1, &oldP);
   LineTo(hdc, x2, y1);
   LineTo(hdc, x2, y2);
   LineTo(hdc, x1, y2);
   LineTo(hdc, x1, y1);

   SelectObject(hdc, oldPen);
   DeleteObject(pen);
}

void TinyMapWindow::locationToTinyMap(const Location& l, PixelPoint& p)
{
  LONG tinyWidth = d_dib->getWidth();
  MapPoint mp;
  MapWindow* mw = getMapWindow();
  mw->locationToMap(l, mp);
  p.setX((mp.getX() * tinyWidth) / mw->getFullWidth());
  p.setY((mp.getY() * tinyWidth) / mw->getFullWidth());
}

void TinyMapWindow::onLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
   if(!d_dragging)
   {
      d_dragging = TRUE;
      SetCapture(hwnd);
   }

   updateZoomArea(x, y);
}

void TinyMapWindow::onLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
   if(d_dragging)
   {
      d_dragging = FALSE;
      ReleaseCapture();
   }

   updateMapPos(x,y);
}

void TinyMapWindow::onMouseMove(HWND hwnd, int x, int y, UINT keyFlags)
{
   if(d_dragging)
      updateZoomArea(x, y);
      // updateMapPos(x,y);
}

/*
 * Update the position of the viewed area
 */

void TinyMapWindow::updateMapPos(int x, int y)
{
   RECT cRect;
   GetClientRect(hWnd, &cRect);

   /*
    * Make sure x,y are in the client area
    */

   if(x < cRect.left)
      x = cRect.left;
   if(y < cRect.top)
      y = cRect.top;
   if(x >= cRect.right)
      x = cRect.right - 1;
   if(y >= cRect.bottom)
      y = cRect.bottom - 1;

   /*
    * Convert to world coordinates
    */

   MapWindow* mw = getMapWindow();

   MapCord wx = (x * mw->getFullWidth())  / (cRect.right - cRect.left);
   MapCord wy = (y * mw->getFullHeight()) / (cRect.bottom - cRect.top);


   if(mw->isZooming())
   {
      MapPoint mc;
      mc.setX(wx);
      mc.setY(wy);
      mw->zoomTo(mc);
   }
   else
   {
      mw->setMapPos(wx, wy);
   }
   InvalidateRect(hWnd, NULL, FALSE);
}

void TinyMapWindow::updateZoomArea(int x, int y)
{
   RECT cRect;
   GetClientRect(hWnd, &cRect);

   x -= d_zoomArea.right/2;
   y -= d_zoomArea.bottom/2;

   int maxX = cRect.right - d_zoomArea.right;
   int maxY = cRect.bottom - d_zoomArea.bottom;

   if(x > maxX)
      x = maxX;
   if(y > maxY)
      y = maxY;
   if(x < cRect.left)
      x = cRect.left;
   if(y < cRect.top)
      y = cRect.top;

   // xorRect();

   d_zoomArea.left = x;
   d_zoomArea.top = y;

   // xorRect();

   InvalidateRect(hWnd, NULL, FALSE);
}

/*
 * Function for mapwindow to call to say that area has changed
 */

void TinyMapWindow::areaChanged()
{
   ASSERT(this != NULL);

#ifdef DEBUG_TINYMAPWINDOW
   debugLog("TinyMapWindow::areaChanged()\n");
#endif

   InvalidateRect(hWnd, NULL, FALSE);
}

LRESULT TinyMapWindow::procMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   LRESULT l;
   if(handlePalette(hWnd, msg, wParam, lParam, l))
      return l;

   switch(msg)
   {
      HANDLE_MSG(hWnd, WM_CREATE,         onCreate);
      HANDLE_MSG(hWnd, WM_DESTROY,        onDestroy);
      HANDLE_MSG(hWnd, WM_PAINT,          onPaint);
      // HANDLE_MSG(hWnd, WM_MOUSEACTIVATE,  onMouseActivate);
      HANDLE_MSG(hWnd, WM_LBUTTONDOWN,    onLButtonDown);
      HANDLE_MSG(hWnd, WM_LBUTTONUP,      onLButtonUp);
      HANDLE_MSG(hWnd, WM_MOUSEMOVE,      onMouseMove);
      HANDLE_MSG(hWnd, WM_SETCURSOR,      onSetCursor);
//      HANDLE_MSG(hWnd, WM_MOVE,      onMove);
//    HANDLE_MSG(hWnd, WM_SIZE,           onSize);
   default:
      return defProc(hWnd, msg, wParam, lParam);
   }
}



/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
