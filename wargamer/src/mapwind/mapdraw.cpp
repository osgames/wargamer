/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * MapWindow Drawing Functions
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mapdraw.hpp"
#include "mw_data.hpp"
#include "mw_user.hpp"  // for MapSelect
#include "dib.hpp"
#include "campdint.hpp"
#include "town.hpp"
#include "wmisc.hpp"
#include "sync.hpp"
#include "print.hpp"
#include "options.hpp"
#include "bmp.hpp"
#include "scenario.hpp"
#include "resstr.hpp"
#if !defined(EDITOR)
#include "mapanim.hpp"
#include "route.hpp"
#endif
#if defined(DEBUG) && !defined(EDITOR)
#include "todolog.hpp"
#include "IMapWindow.hpp"
#include "campint.hpp"
#endif


/*
 * MapDIB class functions
 */

MapDIB::MapDIB() :
   d_static(0),
   d_dynamic(0)
{
}

MapDIB::~MapDIB()
{
   delete d_static;
   d_static = 0;

   delete d_dynamic;
   d_dynamic = 0;
}

DrawDIBDC* MapDIB::getStaticDIB()
{
   staticLock.get();
   return d_static;
}

void MapDIB::releaseStaticDIB()
{
   staticLock.release();
}

DrawDIBDC* MapDIB::getMainDIB()
{
   mainLock.get();
   return d_dynamic;
}

void MapDIB::releaseMainDIB()
{
   mainLock.release();
}


/*
 * Makes sure that the DIBs are the correct size
 * Returns True if changed
 */

Boolean MapDIB::setSize(const PixelPoint& p)
{
   LONG width = p.getX();
   LONG height = p.getY();

#ifdef DEBUG
   debugLog("MapDIB::setSize(%ld,%ld)\n", (long) width, (long) height);
#endif


   width = (width + 3) & ~3;        // Round up to a dword

   if( (d_static == 0) || (width != d_static->getWidth()) || (height != d_static->getHeight()))
   {
#ifdef DEBUG_MAPDIB
      debugLog("+MapDIB::setSize changing size to %ld,%ld\n", (long) width, (long) height);
#endif

      getStaticDIB();
      getMainDIB();

      if(d_static)
         delete d_static;
      d_static = new DrawDIBDC(width, height);

      ASSERT(d_static != 0);

      if(d_dynamic)
         delete d_dynamic;
      d_dynamic = new DrawDIBDC(width, height);

      ASSERT(d_dynamic != 0);

      releaseMainDIB();
      releaseStaticDIB();

#ifdef DEBUG_MAPDIB
      debugLog("-MapDIB::setSize finished changing size\n");
#endif

      return True;
   }

   return False;
}

/*
 * Map Draw Functions
 */

int MapDraw::s_instanceCount = 0;
DIB* MapDraw::s_bmScale = 0;


MapDraw::MapDraw() :
   d_mapDibs(),
   staticDirty(True)
{
   ++s_instanceCount;
}

MapDraw::~MapDraw()
{
   ASSERT(s_instanceCount > 0);
   if (!s_instanceCount--)
   {
      if (s_bmScale)
      {
         delete s_bmScale;
         s_bmScale = 0;
      }
   }
}

#if !defined(EDITOR)
void MapDraw::initAnimations(const MapWindowData* mapData)
{
  d_animationManager.init(mapData);
}
#endif

void MapDraw::setSize(const PixelPoint& p)
{
   if(d_mapDibs.setSize(p))
   {
      forceDraw();                     // Wait until draw thread draws it
   }
}

/*
 * Redraw the map window
 * If all is True, then complete map must be updated
 *
 * What this will do is to signal the MapWindow display thread
 * but for now, it will directly call it's draw() function
 */

void MapDraw::requestRedraw(BOOL all)
{
   if(all)
      staticDirty = TRUE;
}


void MapDraw::forceDraw()
{
   requestRedraw(TRUE);
}

/*
 * Draw changed objects onto DIBs
 *
 * Note that this could be executed as a seperate thread
 */

void MapDraw::draw(MapWindowData& mapData, const CampaignData& campData)
{
#ifdef DEBUG_MAPWINDOW
   debugLog("+MapDraw::draw.. start\n");
#endif

   // DeviceContext dcDIB;    // This gets autodeleted when this function exits

   DrawDIBDC* staticDIB = d_mapDibs.getStaticDIB();
   ASSERT(staticDIB != 0);

   if(staticDirty)
   {
#ifdef DEBUG_MAPWINDOW
   debugLog("+MapDraw::draw static.. start\n");
#endif

      mapData.drawStaticMap(staticDIB);

      // SelectObject(dcDIB, staticDIB->getHandle());

      #ifdef DEBUG_MAPWINDOW
         debugLog("Drawing Grid\n");
      #endif

#if defined(DEBUG)
      if(mapData.gridOn())
         drawGrid(staticDIB, mapData);
#endif

      #ifdef DEBUG_MAPWINDOW
         debugLog("Drawing Off-screen locations\n");
      #endif

      d_inserts.drawInserts(staticDIB, mapData);


      #ifdef DEBUG_MAPWINDOW
         debugLog("Drawing Connections\n");
      #endif

//    #if defined(CUSTOMIZE)
//          #if !defined(EDITOR)
//            if(mapData.editMode != PM_Playing)
//          #endif
//              drawConnections(staticDIB, mapData);
//    #endif
      d_conDisp.draw(staticDIB, mapData, campData);

      #ifdef DEBUG_MAPWINDOW
         debugLog("Drawing Provinces\n");
      #endif

      // drawProvinces(staticDIB);
      d_provDisp.drawStatic(staticDIB, mapData);

      #ifdef DEBUG_MAPWINDOW
         debugLog("Drawing Towns\n");
      #endif

      // drawStaticTowns(staticDIB);
      d_townDisp.drawStatic(staticDIB, mapData);

      d_supplyDisp.reset();

      #ifdef DEBUG_MAPWINDOW
         debugLog("Drawing Scale\n");
      #endif

        if(Options::get(OPT_DrawMapScale))
         drawScale(staticDIB, mapData);

      staticDirty = FALSE;

#ifdef DEBUG_MAPWINDOW
   debugLog("-MapDraw::draw static.. finish\n");
#endif
   }

   /*
    * Update dynamic Area
    *
    * Copy static into dynamic
    * Draw moveable things
    */

   DrawDIBDC* dDIB = d_mapDibs.getMainDIB();

   dDIB->blitFrom(staticDIB);
   d_mapDibs.releaseStaticDIB();


   d_provDisp.drawDynamic(dDIB, mapData);
   d_townDisp.drawDynamic(dDIB, mapData);

#if !defined(EDITOR)
   d_unitDisp.drawUnits(dDIB, mapData, &d_routeDisp);
#else
   d_unitDisp.drawUnits(dDIB, mapData, 0);
#endif

#if defined(DEBUG) && !defined(EDITOR)
   if(Options::get(OPT_ShowAI))
   {
      class AIMapDraw : public IMapWindow
      {
         public:
            AIMapDraw(MapWindowData* mapData, DrawDIBDC* dib) : d_mapData(mapData), d_dib(dib)
            {
               ASSERT(d_dib != 0);
               ASSERT(d_mapData != 0);
            }

            void locationToPixel(const Location& lc, PixelPoint& pc) const { d_mapData->locationToPixel(lc, pc); }
            DrawDIBDC* dib() const { return d_dib; }

         private:
            MapWindowData* d_mapData;
            DrawDIBDC* d_dib;
      };

      campaign->drawAI(AIMapDraw(&mapData, dDIB));
   }
#endif

   if (CampaignOptions::get(OPT_Supply) &&
      Options::get(OPT_ShowSupplyLines) )
   {
      d_supplyDisp.draw(dDIB, &mapData);
   }


// d_animationManager.run(dDIB, dDIB->getDC());

   MapSelect info;
   mapData.fillMapSelect(info);
   mapData.d_user->onDraw(dDIB, info);

   d_mapDibs.releaseMainDIB();

   // Do this in onPaint in mapgui
   // Actually it doesn't need doing at all

   // InvalidateRect(getHWND(), NULL, TRUE);

#ifdef DEBUG_MAPWINDOW
   debugLog("-MapDraw::draw.. finish\n");
#endif
}

#if !defined(EDITOR)
void MapDraw::animate(HDC hdc)
{
   // RWLock lock;
   // lock.startWrite();

   d_animationManager.run(d_mapDibs.getMainDIB(), hdc);
   d_mapDibs.releaseMainDIB();

   // lock.endWrite();
}
#endif

/*
 * Draw scale bar into staticDIB window
 */

void MapDraw::drawScale(DrawDIBDC* dib, const MapWindowData& mapData)
{
   /*
    * Calculate constants
    */

   LONG totalWidth;        // Pixel size of scale
   Distance barSize;       // Size of 1 line on scale

   static struct {
      int totalMile;          // Total length of bar
      int barCount;           // Number of subdivisions
      int numberedMile;       // Which bars are numbered
   } scales[] = {
      {   5,  5,  1 },
      {  10, 10,  5 },
      {  20, 10,  5 },
      {  50,  5, 10 },
      { 100, 10, 50 },
      {  -1, -1, -1 }
   };

   int nScale = 0;
   while(scales[nScale+1].totalMile != -1)
   {
      int pixelPerTry = mapData.distanceToPixel(MilesToDistance(scales[nScale].totalMile));

      if(pixelPerTry >= 50)
         break;

      nScale++;
   }

   totalWidth = mapData.distanceToPixel(MilesToDistance(scales[nScale].totalMile));
   int nBars = scales[nScale].barCount;
   barSize = MilesToDistance(scales[nScale].totalMile) / nBars;
   int nEmph = (scales[nScale].numberedMile * scales[nScale].barCount) / scales[nScale].totalMile;

   ASSERT(nEmph != 0);

   /*
    * Fill a rectangle of white
    */

   RECT area;
   area.right = totalWidth + 8 + 40;      // +40 is width of "Miles"
   area.bottom = 22 + 5;                  // 10 for bar, 10 for font, 3 extra
   area.left = 10;
   // area.top = r.bottom - 10 - area.bottom - 15;
   area.top = dib->getHeight() - 10 - area.bottom - 15;

   // staticDIB->rect(area.left, area.top, area.right, area.bottom, staticDIB->getColour(PALETTERGB(192,192,192)));
   // staticDIB->frame(area.left, area.top, area.right, area.bottom, staticDIB->getColour(PALETTERGB(255,255,255)), dib->getColour(PALETTERGB(0,0,0)));

   LONG startX = area.left + 8;
// LONG startY = area.top + 22;     // 10 for bar, 10 for font, 3 extra
   LONG startY = area.top + 10;     // 10 for bar, 10 for font, 3 extra

//   LONG textY = startY - 10;
   LONG textY = startY + 2;

   ColourIndex color = dib->getColour(PALETTERGB(0,0,0));      // Use black

   dib->hLine(startX, startY, totalWidth+1, color);
   dib->hLine(startX, startY+1, totalWidth+1, color);

   if(!s_bmScale)
   {
      s_bmScale = BMP::newDIB(scenario->getScenarioResDLL(), "MapScale", BMP::RBMP_Normal);
      s_bmScale->setMaskColor(*s_bmScale->getBits());
   }
   ASSERT(s_bmScale);

   for(int i = 0; i <= nBars; i++)
   {
      LONG x = startX + mapData.distanceToPixel(barSize * i);

//    COLORREF colour;
      LONG h;

      // if((i % nBars) == 0)
      if( (i == 0) || (i == nBars) )
      {
         if (s_bmScale)
         {
            dib->blit(s_bmScale, x - s_bmScale->getWidth() / 2, startY - s_bmScale->getHeight());
            continue;
         }

         // TODO: Display graphic here in instead of line
         h = 10;
//       colour = PALETTERGB(255,224,224);
      }
      else if((i % nEmph) == 0)
      {
         h = 8;
//       colour = PALETTERGB(255,192,192);
      }
      else
      {
         h = 5;
//       colour = PALETTERGB(255,0,0);
      }

      dib->vLine(x, startY-h, h, color);
   }

   /*
    * Put text on
    */

   int scaleMile = scales[nScale].totalMile;
   dib->saveDC();

   dib->setBkMode(TRANSPARENT);
   dib->setTextColor(PALETTERGB(0,0,0));
// dib->setTextAlign(TA_BOTTOM | TA_CENTER);
   dib->setTextAlign(TA_TOP | TA_CENTER);

   HFONT newFont = CreateFont(
      10,                        // nHeight
      0,                         // nWidth
      0,                         // nEscapement
      0,                         // nOrientation
      FW_DONTCARE,               // fnWeight
      FALSE,                     // fdwItalic
      FALSE,                     // fdwUnderline
      FALSE,                     // fdwStrikeOut
      DEFAULT_CHARSET,           // fdwCharSet
      OUT_DEFAULT_PRECIS,        // fdwOutputPrecision
      CLIP_DEFAULT_PRECIS,       // fdwClipPrecision
      DEFAULT_QUALITY,           // fdwQuality
      DEFAULT_PITCH | FF_SWISS,  // fdwPitchAndFamily
      NULL                       // lpszFace
   );

   dib->setFont(newFont);

   char buffer[100];

   int nMile = scales[nScale].numberedMile;
   Distance numSize = MilesToDistance(nMile);
   int nNumbered = scales[nScale].totalMile / scales[nScale].numberedMile;


   for(i = 0; i <= nNumbered; i++)
   {
      LONG x = startX + mapData.distanceToPixel(numSize * i);

      wsprintf(buffer, "%d", i * nMile);
      wTextOut(dib->getDC(), x, textY, buffer);

   }

   // dib->setTextAlign(TA_BOTTOM | TA_LEFT);
   dib->setTextAlign(TA_TOP | TA_LEFT);
   wsprintf(buffer, InGameText::get(IDS_Miles));
   wTextOut(dib->getDC(), startX + totalWidth + 10, textY, buffer);

   dib->restoreDC();
   DeleteObject(newFont);
}

#ifdef DEBUG
void MapDraw::drawGrid(DrawDIBDC* dib, const MapWindowData& mapData)
{
   if(mapData.gridMode() == MapWindowData::GridVertical)
   {
      ColourIndex purple = dib->getColour(PALETTERGB(255,0,255));

      for(Distance yd = 0; yd < mapData.getPhysicalHeight(); yd += MilesToDistance(50))
      {
         Location l;
         PixelPoint p1;
         PixelPoint p2;

         l.setY(yd);
         l.setX(0);
         mapData.locationToPixel(l, p1);

         l.setY(yd);
         l.setX(mapData.getPhysicalWidth());
         mapData.locationToPixel(l, p2);
         dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), purple);
      }

      for(Distance xd = 0; xd < mapData.getPhysicalWidth(); xd += MilesToDistance(50))
      {
         Location l;
         PixelPoint p1;
         PixelPoint p2;

         l.setX(xd);
         l.setY(0);
         mapData.locationToPixel(l, p1);

         l.setX(xd);
         l.setY(mapData.getPhysicalHeight());
         mapData.locationToPixel(l, p2);

         dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), purple);
      }
   }
   else if(mapData.gridMode() == MapWindowData::GridNorth)
   {
      ColourIndex purple = dib->getColour(PALETTERGB(255,0,255));

      const MapScaleInfo* mapInfo = scenario->getMapScaleInfo();
      Wangle wangle = Degree(mapInfo->s_viewAngle);

      int height2 = 2 * mapData.getPhysicalHeight();
      int width2 = 2 * mapData.getPhysicalWidth();

      for(Distance yd = -height2; yd < height2; yd += MilesToDistance(50))
      {
         Location l;
         PixelPoint p1;
         PixelPoint p2;

         l.setY(mcos(yd, wangle) - msin(-width2, wangle));
         l.setX(msin(yd, wangle) + mcos(-width2, wangle));
         mapData.locationToPixel(l, p1);

         l.setY(mcos(yd, wangle) - msin(width2, wangle));
         l.setX(msin(yd, wangle) + mcos(width2, wangle));
         mapData.locationToPixel(l, p2);
         dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), purple);
      }

      for(Distance xd = -width2; xd < width2; xd += MilesToDistance(50))
      {
         Location l;
         PixelPoint p1;
         PixelPoint p2;

         l.setY(mcos(-height2, wangle) - msin(xd, wangle));
         l.setX(msin(-height2, wangle) + mcos(xd, wangle));
         mapData.locationToPixel(l, p1);

         l.setY(mcos(height2, wangle) - msin(xd, wangle));
         l.setX(msin(height2, wangle) + mcos(xd, wangle));
         mapData.locationToPixel(l, p2);

         dib->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), purple);
      }
   }
}
#endif

void MapDraw::paint(HDC hdc, const RECT& r)
{
   // RWLock lock;
   // lock.startWrite();

   DrawDIBDC* dDIB = d_mapDibs.getMainDIB();
   ASSERT(dDIB != 0);

#if !defined(EDITOR)
   d_animationManager.run(dDIB, dDIB->getDC(), True);
#endif

   BitBlt(hdc, 0,0, r.right-15, r.bottom-15,
         dDIB->getDC(), 0,0,
         SRCCOPY);

   d_mapDibs.releaseMainDIB();

   // lock.endWrite();
}

/*
 *----------------------------------------------------------------------
 * $Log$
 *----------------------------------------------------------------------
 */
