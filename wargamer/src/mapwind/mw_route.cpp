/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 * Map Window Route Display
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "mw_route.hpp"
#include "mw_data.hpp"
// #include "grapple.hpp"
#include "armies.hpp"
#include "scenario.hpp"
#include "palette.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "campdint.hpp"
#include "route.hpp"
#include "town.hpp"
#include "thicklin.hpp"
#include "locengin.hpp"
#include "bezier.hpp"
#include "pt_util.hpp"
#include "options.hpp"
#include "routelst.hpp"
#include <utility>

#ifdef _MSC_VER
using namespace std::rel_ops;
#endif

class MapRouteDisplayImp
{
   public:
      MapRouteDisplayImp();
      ~MapRouteDisplayImp();

      void drawRoute(DrawDIBDC* dib, int thick, Side side, MapWindowData& mapData, const CampaignPosition& from, const CampaignPosition& dest);
      void drawRoute(DrawDIBDC* dib, int thick, Side side, MapWindowData& mapData, const CampaignPosition& from, const CampaignPosition& dest, const RouteList& rl);
};

#if 0    // Old obsolete function
static void MapRouteDisplay::drawViaArrow(DrawDIBDC* dib, MapWindowData& mapData, ConstICommandPosition cpi, const Location& sLocation, const Location& eLocation, const ArrowInfo& ai, int nVia)
{
   ASSERT(cpi != NoCommandPosition);
   const CommandPosition* cp = mapData.d_campData->getCommand(cpi);
   COLORREF rgb = scenario->getSideColour(cp->getSide());

   PixelPoint p1;
   PixelPoint p2;

   mapData.locationToPixel(sLocation, p1);
   mapData.locationToPixel(eLocation, p2);

   const int y = mapData.d_grapples[nVia].cy - p1.getY();
   const int x = mapData.d_grapples[nVia].cx - p1.getX();

   const int dist = aproxDistance(x, y);

   if(dist > 30)
   {
     const int startX = ((ai.gap * x) / dist) + p1.getX();
     const int startY = ((ai.gap * y) / dist) + p1.getY();

     const int endX = mapData.d_grapples[nVia].cx - (x / dist);
     const int endY = mapData.d_grapples[nVia].cy - (y / dist);

     const int gX = endX - ((ai.length * x) / dist);
     const int gY = endY - ((ai.length * y) / dist);

     POINT p[4];

     p[0].x = startX + ((ai.width * y) / (2 * dist));
     p[0].y = startY - ((ai.width * x) / (2 * dist));

     p[1].x = startX - ((ai.width * y) / (2 * dist));
     p[1].y = startY + ((ai.width * x) / (2 * dist));

     p[2].x = gX - ((ai.width * y) / (2 * dist));
     p[2].y = gY + ((ai.width * x) / (2 * dist));

     p[3].x = gX + ((ai.width * y) / (2 * dist));
     p[3].y = gY - ((ai.width * x) / (2 * dist));

     dib->saveDC();

     // HPEN hOldPen;
     HPEN hNewPen = CreatePen(PS_SOLID, 1, rgb);
     ASSERT(hNewPen != 0);

     // hOldPen = (HPEN)SelectObject(hdc, hNewPen);
     dib->setPen(hNewPen);

     // HBRUSH hOldBrush;
     HBRUSH hNewBrush = CreateSolidBrush(rgb);
     ASSERT(hNewBrush != 0);

     // hOldBrush = (HBRUSH)SelectObject(hdc, hNewBrush);
     dib->setBrush(hNewBrush);

     Polygon(dib->getDC(), p, 4);

     dib->restoreDC();

     // SelectObject(hdc, hOldPen);
     DeleteObject(hNewPen);
     // SelectObject(hdc, hOldBrush);
     DeleteObject(hNewBrush);

     Location l;

     PixelPoint p3;
     p3.setX(mapData.d_grapples[nVia].cx);
     p3.setY(mapData.d_grapples[nVia].cy);
     mapData.pixelToLocation(p3, l);

     drawArrow(dib, mapData, cpi, l, eLocation, ai);

//   if(mapData.d_grapples[nVia].active)
     {
      dib->saveDC();

       // HBRUSH hOldBrush;
       HBRUSH hNewBrush = CreateSolidBrush(mapData.d_grapples[nVia].active?rgb:RGB(0, 0, 0));
       ASSERT(hNewBrush != 0);
       // hOldBrush = (HBRUSH)SelectObject(hdc, hNewBrush);
       dib->setBrush(hNewBrush);

       Rectangle(dib->getDC(), mapData.d_grapples[nVia].left, mapData.d_grapples[nVia].top, mapData.d_grapples[nVia].right, mapData.d_grapples[nVia].bottom);

       dib->restoreDC();
       // SelectObject(hdc, hOldBrush);
       DeleteObject(hNewBrush);
     }

#if 0
     else
     {
       HBRUSH hOldBrush;
       HBRUSH hNewBrush = CreateSolidBrush(RGB(0, 0, 0));
       ASSERT(hNewBrush != 0);

       hOldBrush = (HBRUSH)SelectObject(hdc, hNewBrush);
       Rectangle(hdc, mapData.d_grapples[nVia].left, mapData.d_grapples[nVia].top, mapData.d_grapples[nVia].right, mapData.d_grapples[nVia].bottom);

       SelectObject(hdc, hOldBrush);
       DeleteObject(hNewBrush);
     }
#endif
   }
   else
   {
     if(dist > 15)
       drawArrow(dib, mapData, cpi, sLocation, eLocation, ai);

     mapData.d_grapples[nVia].active = False;
   }
}

#endif


/*
 * Original Arrow drawing function
 * Retained for backwards compatibility until Paul finishes with unitint.cpp
 */

void MapRouteDisplay::drawArrow(DrawDIBDC* dib, MapWindowData& mapData, ConstICommandPosition cpi, const Location& sLocation, const Location& eLocation, const ArrowInfo& ai)
{
   ASSERT(cpi != NoCommandPosition);
   const CommandPosition* cp = mapData.d_campData->getCommand(cpi);
   COLORREF rgb = scenario->getSideColour(cp->getSide());

   PixelPoint p1;
   PixelPoint p2;

    mapData.locationToPixel(sLocation, p1);
    mapData.locationToPixel(eLocation, p2);

   const  int y = p2.getY() - p1.getY();
   const  int x = p2.getX() - p1.getX();

   const  int dist = aproxDistance(x, y);

   int minDist = 2 * ai.gap + ai.length;

   // if(dist > 0) // make sure we don't divide by 0!
   if(dist > minDist) // make sure we don't divide by 0!
   {
     int startX = ((ai.gap * x) / dist) + p1.getX();
     int startY = ((ai.gap * y) / dist) + p1.getY();

     int endX = p2.getX() - ((ai.gap * x) / dist);
     int endY = p2.getY() - ((ai.gap * y) / dist);

     int gX = endX - ((ai.length * x) / dist);
     int gY = endY - ((ai.length * y) / dist);

     POINT p[7];

     p[0].x = startX + ((ai.width * y) / (2 * dist));
     p[0].y = startY - ((ai.width * x) / (2 * dist));

     p[1].x = startX - ((ai.width * y) / (2 * dist));
     p[1].y = startY + ((ai.width * x) / (2 * dist));

     p[2].x = gX - ((ai.width * y) / (2 * dist));
     p[2].y = gY + ((ai.width * x) / (2 * dist));

     p[3].x = gX - ((ai.height * y) / (2 * dist));
     p[3].y = gY + ((ai.height * x) / (2 * dist));

     p[4].x = endX;
     p[4].y = endY;

     p[5].x = gX + ((ai.height * y) / (2 * dist));
     p[5].y = gY - ((ai.height * x) / (2 * dist));

     p[6].x = gX + ((ai.width * y) / (2 * dist));
     p[6].y = gY - ((ai.width * x) / (2 * dist));

     dib->saveDC();

     HPEN hNewPen = CreatePen(PS_SOLID, 1, rgb);
     ASSERT(hNewPen != 0);

     // HPEN hOldPen = (HPEN)SelectObject(hdc, hNewPen);
     dib->setPen(hNewPen);

     HBRUSH hNewBrush = CreateSolidBrush(rgb);
     ASSERT(hNewBrush != 0);

     // HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, hNewBrush);
     dib->setBrush(hNewBrush);

     Polygon(dib->getDC(), p, 7);

     dib->restoreDC();

     // SelectObject(hdc, hOldPen);
     DeleteObject(hNewPen);
     // SelectObject(hdc, hOldBrush);
     DeleteObject(hNewBrush);
   }
}

MapRouteDisplayImp::MapRouteDisplayImp()
{
}

MapRouteDisplayImp::~MapRouteDisplayImp()
{
}


/*
 * Display Route on Map
 */

class RouteEngine : public GraphicEngine
{
   public:
      RouteEngine(GraphicEngine& output, DrawDIBDC* dib, int width, int gap, int arrowWidth, int arrowLength, COLORREF color);
      ~RouteEngine();
      void addPoint(const GPoint& p);
      void close();

      bool getRect(Rect<int>& rect)
      {
         if((d_maxX <= d_minX) || (d_maxY <= d_minY))
            return false;

         d_minX -= d_width + 1;
         d_maxX += d_width + 1;
         d_minY -= d_width + 1;
         d_maxY += d_width + 1;

         d_minX = maximum(0, d_minX);
         d_maxX = minimum(d_dib->getWidth(), d_maxX);
         d_minY = maximum(0, d_minY);
         d_maxY = minimum(d_dib->getHeight(), d_maxY);

         if( (d_minX < d_maxX) && (d_minY < d_maxY) )
         {
            rect = Rect<int>(d_minX, d_minY, d_maxX-d_minX, d_maxY-d_minY);
            return true;
         }
         else
            return false;
      }

   private:
      void output(const GPoint& p);
      void setMinMax(int x, int y);

   private:
      GraphicEngine& d_output;
      DrawDIBDC* d_dib;
      int d_gap;
      int d_width;            // Width of line
      int d_arrowWidth;
      int d_arrowLength;
      COLORREF d_color;

      GPoint d_lastPoint;
      GPoint d_prevPoint;

      int d_frontGap;
      int d_endGap;

      enum {
         Start,
         First,
         Normal
      } d_mode;

      int d_minX;    // Area drawn into to help speed up blitting later on
      int d_maxX;
      int d_minY;
      int d_maxY;
};

RouteEngine::RouteEngine(GraphicEngine& output, DrawDIBDC* dib, int width, int gap, int arrowWidth, int arrowLength, COLORREF color) :
   d_output(output),
   d_dib(dib),
   d_gap(gap),
   d_arrowWidth(arrowWidth),
   d_arrowLength(arrowLength),
   d_color(color),
   d_mode(Start),
   d_minX(dib->getWidth()),
   d_maxX(0),
   d_minY(dib->getHeight()),
   d_maxY(0)
{
   d_width = 1 + width / 2;
   d_frontGap = d_gap + d_width;
   d_endGap = d_gap + d_arrowLength + d_width;
}

RouteEngine::~RouteEngine()
{
}

void RouteEngine::setMinMax(int x, int y)
{
   d_minX = minimum(x, d_minX);
   d_maxX = maximum(x, d_maxX);
   d_minY = minimum(y, d_minY);
   d_maxY = maximum(y, d_maxY);
}

void RouteEngine::output(const GPoint& p)
{
   setMinMax(p.x(), p.y());
   d_output.addPoint(p);
}

void RouteEngine::addPoint(const GPoint& p)
{
   // Split all points in 2 to make curves get closer to towns

   if(d_mode == Start)
   {
      d_mode = First;
      d_lastPoint = p;
   }
   else
   {
      if(p == d_lastPoint)
         return;

      int length = lineLength(d_lastPoint, p);

      if(d_mode == First)
      {
         /*
          * Calculate 1st gap
          */

         if(length >= (d_frontGap + d_endGap))
         {
            GPoint gap = midPoint(d_lastPoint, p, d_frontGap, length);
            output(gap);
            d_prevPoint = gap;

            // Draw start edge

            int dx = p.x() - d_lastPoint.x();
            int dy = p.y() - d_lastPoint.y();

            int length = lineLength(p, d_lastPoint);
            POINT pt[4];

            int aWid = d_width;
            int aLen = d_width;


            pt[0].x = gap.x() + (aWid * dy) / length;
            pt[0].y = gap.y() - (aWid * dx) / length;

            pt[1].x = gap.x() - (aWid * dy) / length;
            pt[1].y = gap.y() + (aWid * dx) / length;

            pt[2].x = pt[1].x - (aLen * dx) / length;
            pt[2].y = pt[1].y - (aLen * dy) / length;

            pt[3].x = pt[0].x - (aLen * dx) / length;
            pt[3].y = pt[0].y - (aLen * dy) / length;

            setMinMax(pt[0].x, pt[0].y);
            setMinMax(pt[1].x, pt[1].y);
            setMinMax(pt[2].x, pt[2].y);
            setMinMax(pt[3].x, pt[3].y);


            d_dib->saveDC();
            HPEN pen = CreatePen(PS_SOLID, 1, d_color);
            ASSERT(pen != NULL);
            d_dib->setPen(pen);
            HBRUSH hNewBrush = CreateSolidBrush(d_color);
            ASSERT(hNewBrush != 0);
            d_dib->setBrush(hNewBrush);
            Polygon(d_dib->getDC(), pt, 4);
            d_dib->restoreDC();
            DeleteObject(pen);
            DeleteObject(hNewBrush);

            d_mode = Normal;
            d_lastPoint = p;
         }
      }
      else
      {
         ASSERT(d_mode == Normal);

         if(length >= d_endGap)
         {
            GPoint p2 = midPoint(d_lastPoint, d_prevPoint);
            if((p2 != d_lastPoint) && (p2 != d_prevPoint))
               output(p2);

            output(d_lastPoint);
            d_prevPoint = d_lastPoint;
         }
         d_lastPoint = p;
      }
   }
}

void RouteEngine::close()
{

   // Handle last point to leave gap

   if(d_mode == Normal)
   {
      // Calculate last gap between prevPoint and lastPoint

      int length = lineLength(d_prevPoint, d_lastPoint);

      if(length > d_endGap)
      {
         GPoint gap = midPoint(d_lastPoint, d_prevPoint, d_endGap, length);
         output(gap);

         // Draw arrow

         int dx = d_lastPoint.x() - d_prevPoint.x();
         int dy = d_lastPoint.y() - d_prevPoint.y();

         POINT pt[3];

         pt[0].x = gap.x() + (d_arrowLength * dx) / length;
         pt[0].y = gap.y() + (d_arrowLength * dy) / length;

         pt[1].x = gap.x() + (d_arrowWidth * dy) / length;
         pt[1].y = gap.y() - (d_arrowWidth * dx) / length;

         pt[2].x = gap.x() - (d_arrowWidth * dy) / length;
         pt[2].y = gap.y() + (d_arrowWidth * dx) / length;

         setMinMax(pt[0].x, pt[0].y);
         setMinMax(pt[1].x, pt[1].y);
         setMinMax(pt[2].x, pt[2].y);

         d_dib->saveDC();
         HPEN pen = CreatePen(PS_SOLID, 1, d_color);
         ASSERT(pen != NULL);
         d_dib->setPen(pen);
         HBRUSH hNewBrush = CreateSolidBrush(d_color);
         ASSERT(hNewBrush != 0);
         d_dib->setBrush(hNewBrush);
         Polygon(d_dib->getDC(), pt, 3);
         d_dib->restoreDC();
         DeleteObject(pen);
         DeleteObject(hNewBrush);
      }

   }

   d_output.close();
   d_mode = Start;
}


// void MapRouteDisplayImp::drawRoute(DrawDIBDC* dib, int thick, COLORREF rgb, const CampaignRoute* route, ITown start, ITown finish, MapWindowData& mapData)
void MapRouteDisplayImp::drawRoute(DrawDIBDC* dib, int thick, Side side, MapWindowData& mapData, const CampaignPosition& from, const CampaignPosition& dest)
{
   ASSERT(dib != 0);

   /*
    * Set up some colours
    */

   COLORREF rgb = scenario->getSideColour(side);
   ColourIndex colour = dib->getColour(rgb);

   COLORREF brightRGB = Palette::brighten(rgb, 70);
   COLORREF darkRGB = Palette::darken(rgb, 70);

   ColourIndex brightColour = dib->getColour(brightRGB);
   ColourIndex darkColour = dib->getColour(darkRGB);


   // Pick a mask that is different from any other colour

   ColourIndex maskColour = colour ^ 128;
   while( (maskColour == brightColour) || (maskColour == darkColour) || (maskColour == colour) )
      ++maskColour;

   // Ensure our buffer DIB is the correct size

   // makeDIB(dib->getWidth(), dib->getHeight());
   DrawDIBDC* fxDIB = mapData.getFXDIB(dib);
   ASSERT(fxDIB != 0);

   /*
    * Clear out buffer DIB
    */

   ASSERT(fxDIB != 0);
   fxDIB->setMaskColour(maskColour);
   fxDIB->fill(maskColour);

   /*
    * Set up Graphic Engine
    *
    * For now, use thickLine
    */

   ThickLineEngine lineEngine(fxDIB);
   lineEngine.thickness(thick);        // 8 pixels thick

   // TODO: Get colour from side

   lineEngine.color(colour);

   /*
    * Curve engine
    */

   BezierEngine curveEngine(lineEngine);

   //Engine to handle end gaps, arrows, etc

   const int gap = 15;  // 15 pixel gap at ends
   const int arrowLength = 15;
   const int arrowWidth = 12;
   RouteEngine routeEngine(curveEngine, fxDIB, thick, gap, arrowLength, arrowWidth, rgb);

   // Engine to convert physical coordinates to map pixels

   LocationEngine locEngine(routeEngine, mapData);

   /*
    * Add starting position if not at town
    */

   if(!from.atTown())
   {
      Location l;
      from.getHeadLocation(l);
      locEngine.addPoint(l);
   }

   /*
    * Process Route nodes
    */

   const TownList& tl = mapData.d_campData->getTowns();

   ITown startTown = from.getDestTown();
   ITown endTown = dest.getLastTown();

   ASSERT(startTown != NoTown);
   ASSERT(endTown != NoTown);

   if(startTown == endTown)
   {
      if(dest.atTown())
      {
         locEngine.addPoint(tl[endTown].getLocation());
      }
   }
   else
   {
      static CampaignRoute route(mapData.d_campData);

      CampaignRoute::RouteFlag flags = CampaignRoute::UseCosts;
//    if(Options::get(OPT_FullRoute))
//      flags |= CampaignRoute::FindBest;
      route.setup(flags, side, TicksPerHour*8);

      route.planRoute(startTown, endTown);

      const CampaignRouteNode* nodes = route.getNodes();

      const ConnectionList& cl = mapData.d_campData->getConnections();

      ITown t = startTown;
      const Town* town1 = &tl[t];

      locEngine.addPoint(town1->getLocation());

      while( (t != endTown) && (t != NoTown) )
      {
         IConnection c = nodes[t].connection;

         if(c == NoConnection)
            break;

         ITown nextTown = cl[c].getOtherTown(t);
         const Town* town2 = &tl[nextTown];
         t = nextTown;

         locEngine.addPoint(town2->getLocation());

      }
   }

   /*
    * Add Ending position if not at town
    */

   if(!dest.atTown())
   {
      Location l;
      dest.getHeadLocation(l);
      locEngine.addPoint(l);
   }

   locEngine.close();

   /*
    * Apply fxDIB to real dib
    */

   Rect<int> rect;
   if(routeEngine.getRect(rect))
   {
#ifdef DEBUG_ROUTE
      dib->lightenRectangle(rect.left(), rect.top(), rect.width(), rect.height(), 30);
#endif


      DIB_Utility::colorEdge(fxDIB, -1,-1, brightColour, rect);
      DIB_Utility::colorEdge(fxDIB, +1,+1, darkColour, rect);
      // dib->darken(fxDIB, 4, 4, 50);    // Shadow
      dib->darken(fxDIB, rect.left() + 4, rect.top() + 4, rect.width(), rect.height(),
         rect.left(), rect.top(), 50);    // Shadow
      // dib->blit(fxDIB, 0, 0);
      dib->blit(rect.left(), rect.top(), rect.width(), rect.height(),
         fxDIB, rect.left(), rect.top());
   }

   mapData.releaseFXDIB(fxDIB);
}

void MapRouteDisplayImp::drawRoute(DrawDIBDC* dib, int thick, Side side, MapWindowData& mapData,
         const CampaignPosition& from, const CampaignPosition& dest, const RouteList& rl)
{
   ASSERT(dib != 0);
   const CampaignData* campData = mapData.d_campData;


   /*
    * Set up some colours
    */

   COLORREF rgb = scenario->getSideColour(side);
   ColourIndex colour = dib->getColour(rgb);

   COLORREF brightRGB = Palette::brighten(rgb, 70);
   COLORREF darkRGB = Palette::darken(rgb, 70);

   ColourIndex brightColour = dib->getColour(brightRGB);
   ColourIndex darkColour = dib->getColour(darkRGB);


   // Pick a mask that is different from any other colour

   ColourIndex maskColour = colour ^ 128;
   while( (maskColour == brightColour) || (maskColour == darkColour) || (maskColour == colour) )
      ++maskColour;

   // Ensure our buffer DIB is the correct size

   // makeDIB(dib->getWidth(), dib->getHeight());
   DrawDIBDC* fxDIB = mapData.getFXDIB(dib);
   ASSERT(fxDIB != 0);

   /*
    * Clear out buffer DIB
    */

   ASSERT(fxDIB != 0);
   fxDIB->setMaskColour(maskColour);
   fxDIB->fill(maskColour);

   /*
    * Set up Graphic Engine
    *
    * For now, use thickLine
    */

   ThickLineEngine lineEngine(fxDIB);
   lineEngine.thickness(thick);        // 8 pixels thick

   // TODO: Get colour from side

   lineEngine.color(colour);

   /*
    * Curve engine
    */

   BezierEngine curveEngine(lineEngine);

   //Engine to handle end gaps, arrows, etc

   const int gap = 15;  // 15 pixel gap at ends
   const int arrowLength = 15;
   const int arrowWidth = 12;
   RouteEngine routeEngine(curveEngine, fxDIB, thick, gap, arrowLength, arrowWidth, rgb);

   // Engine to convert physical coordinates to map pixels

   LocationEngine locEngine(routeEngine, mapData);

   /*
    * Add starting position
    */

   Location l;
   from.getHeadLocation(l);
   locEngine.addPoint(l);

   /*
    * Process Route nodes
    */

   RouteListIterNLR iter(&rl);

   // advance list to 'from' town
   Boolean townInList = False;
   if(from.atTown())
   {
      while(++iter)
      {
         ITown town = iter.current()->d_town;
         if(town == from.getTown())
         {
            townInList = True;
            break;
         }
      }
   }

   // if 'from' town is not in list, then 'from' is units location, so rewind list
   if(!townInList)
      iter.rewind();

    const RouteNode* prevNode = 0;

   ITown lastTown = dest.getCloseTown();
   ASSERT(lastTown != NoTown);
   while(++iter)
   {
      const RouteNode* node = iter.current();
      ASSERT(node);
      ASSERT(node->d_town != NoTown);
      const Town& t = campData->getTown(node->d_town);
      Location l = t.getLocation();

      /*
       * add point if node is not lastTown
       */

      if(node->d_town != lastTown)
         locEngine.addPoint(l);

      // otherwise, add only if dest is not a town and dest is on a
      // different connection than the last two nodes, otherwise the
      // arrow may double back on itself
      else
      {
         if(!dest.atTown())
         {
            // const RouteNode* prevNode = iter.prev();
                if(prevNode)
                {
                IConnection con = CampaignRouteUtil::commonConnection(campData, prevNode->d_town, node->d_town);
                ASSERT(con != NoConnection);
                if(con != dest.getConnection())
                   locEngine.addPoint(l);
                }
         }
         break;
      }
        prevNode = node;
   }

   /*
    * Add Ending position
    */

   dest.getHeadLocation(l);
   locEngine.addPoint(l);


   locEngine.close();

   /*
    * Apply fxDIB to real dib
    */

   Rect<int> rect;
   if(routeEngine.getRect(rect))
   {
#ifdef DEBUG_ROUTE
      dib->lightenRectangle(rect.left(), rect.top(), rect.width(), rect.height(), 30);
#endif


      DIB_Utility::colorEdge(fxDIB, -1,-1, brightColour, rect);
      DIB_Utility::colorEdge(fxDIB, +1,+1, darkColour, rect);
      // dib->darken(fxDIB, 4, 4, 50);    // Shadow
      dib->darken(fxDIB, rect.left() + 4, rect.top() + 4, rect.width(), rect.height(),
         rect.left(), rect.top(), 50);    // Shadow
      // dib->blit(fxDIB, 0, 0);
      dib->blit(rect.left(), rect.top(), rect.width(), rect.height(),
         fxDIB, rect.left(), rect.top());
   }

   mapData.releaseFXDIB(fxDIB);
}

/*-----------------------------------------------------------------------------
 * Global Functions
 */

MapRouteDisplay::MapRouteDisplay() :
   d_imp(new MapRouteDisplayImp)
{
   ASSERT(d_imp != 0);
}


MapRouteDisplay::~MapRouteDisplay()
{
   delete d_imp;
}


void MapRouteDisplay::drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from, const CampaignPosition& dest, Side side)
{
   ASSERT(d_imp != 0);

   int thick = 8;

   d_imp->drawRoute(dib, thick, side, mapData, from, dest);
}


void MapRouteDisplay::drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from, const CampaignOrder& order, Side side)
{

   CampaignPosition lastPos = from;

   for(int i = 0; (i < Orders::Vias::MaxVia) && (order.getVia(i) != NoTown); i++)
   {
      const CampaignPosition& newPos = order.getVia(i);

      drawRoute(dib, mapData, lastPos, newPos, side);
      lastPos = newPos;
   }

   // Do Dest Town

   if( (order.getDestTown() != NoTown) && (order.getDestTown() != from.getCloseTown()))
   {
      const CampaignPosition& newPos = order.getDestTown();
      drawRoute(dib, mapData, lastPos, newPos, side);
      lastPos = newPos;
   }

   // And finally the destination target

   if(order.getTarget() != NoCommandPosition)
   {
      // const CommandPosition* target = mapData.d_campData->getCommand(order.getTarget());
      ConstICommandPosition target = order.getTarget();
      const CampaignPosition& newPos = target->getPosition();

      drawRoute(dib, mapData, lastPos, newPos, side);
      lastPos = newPos;
   }
}

void MapRouteDisplay::drawRoute(DrawDIBDC* dib, MapWindowData& mapData, ConstICommandPosition& cpi)
{
   const RouteList& rl = cpi->currentRouteList();
   const OrderBase* order = cpi->getCurrentOrder();

   /*
    * if we currently have a target unit, and target unit != orders.getTargetUnit()
    * plot route to that unit, otherwise use current order to plot route
    */

   if(cpi->getTarget() != NoCommandPosition && order->getTargetUnit() != cpi->getTarget())
   {
      ICommandPosition targetCPI = cpi->getTarget();

      ITown startTown = cpi->getLastTown();
      ITown targetTown = targetCPI->getDestTown();

      // if towns are the same
      if(startTown != targetTown)
      {
         drawRoute(dib, mapData, cpi->getPosition(), targetCPI->getPosition(), cpi->getSide(), rl);
      }
   }
   else
      drawRoute(dib, mapData, cpi->getPosition(), *order, cpi->getSide(), rl);
}

void MapRouteDisplay::drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from,
          const CampaignOrder& order, Side side, const RouteList& rl)
{
   CampaignPosition lastPos = from;

   for(int i = 0; (i < Orders::Vias::MaxVia) && (order.getVia(i) != NoTown); i++)
   {
      const CampaignPosition& newPos = order.getVia(i);

      drawRoute(dib, mapData, lastPos, newPos, side, rl);
      lastPos = newPos;
   }

   // Do Dest Town

   if( (order.hasDestTown()) && (order.getDestTown() != from.getCloseTown()))
   {
      const CampaignPosition& newPos = order.getDestTown();
      drawRoute(dib, mapData, lastPos, newPos, side, rl);
      lastPos = newPos;
   }

   // And finally the destination target

   else if(order.hasTargetUnit())
   {
      // const CommandPosition* target = mapData.d_campData->getCommand(order.getTarget());
      ConstICommandPosition target = order.getTarget();
      const CampaignPosition& newPos = target->getPosition();

      drawRoute(dib, mapData, lastPos, newPos, side, rl);
      lastPos = newPos;
   }
}

void MapRouteDisplay::drawRoute(DrawDIBDC* dib, MapWindowData& mapData, const CampaignPosition& from,
          const CampaignPosition& dest, Side side, const RouteList& rl)
{
   ASSERT(d_imp != 0);
   const int thick = 8;
   d_imp->drawRoute(dib, thick, side, mapData, from, dest, rl);
}



/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
