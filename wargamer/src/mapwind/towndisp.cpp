/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.                *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                                  *
 *----------------------------------------------------------------------------*/
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1995, Steven Morle-Green, All Rights Reserved
 *----------------------------------------------------------------------
 *
 * Display of towns on map
 *
 *----------------------------------------------------------------------
 */

#include "stdinc.hpp"
#include "towndisp.hpp"
#include "mw_data.hpp"

#include "gamectrl.hpp"
#include "mapzoom.hpp"
#include "dib.hpp"
#include "dib_util.hpp"
#include "scenario.hpp"
#include "wmisc.hpp"
#include "campdint.hpp"
#include "scn_res.h"
#include "winctrl.hpp"
#include "options.hpp"
#include "imglib.hpp"
#include "palette.hpp"
#include "campint.hpp"
#include "fonts.hpp"
#include "terrain.hpp"
#include "sllist.hpp"
#include "fsalloc.hpp"

#ifdef __WATCOM_CPLUSPLUS__
#pragma warning 549 9;     // Disable sizeof contains compiler generated info warning
#endif

class GraphicEffect
{
      DrawDIBDC d_dib;
   public:
      GraphicEffect() : d_dib() { }
      ~GraphicEffect() { }

      void drawText(DrawDIB* destDib, const PixelPoint& p, const char* text, COLORREF colour, HFONT font, int bkMode);
         // Draw text to destDIB masking against original colours

      void drawDot(DrawDIB* destDIB, const PixelPoint& p, int dotSize, COLORREF color);
         // Draw town dot

      void hilight(DrawDIB* destDIB, const PixelPoint& p, int dotSize);
         // Hilight a circle

   private:
      void makeDIB(int width, int height);
};

void GraphicEffect::makeDIB(int width, int height)
{
   //--- No need for all this because empty DIB has zero width/height
   // if(!d_dib || (d_dib.getWidth() < width) || (d_dib.getHeight() < height) )
   if((d_dib.getWidth() < width) || (d_dib.getHeight() < height) )
   {
      // if(!!d_dib)
      // {
         width = maximum(width, d_dib.getWidth());
         height = maximum(height, d_dib.getHeight());
      // }

      d_dib.resize(width, height);
      ColourIndex ci = d_dib.getColour(PALETTERGB(255,0,255));
      d_dib.setMaskColour(ci);
   }
}

/*
 * Draw shadowed text
 * p is top left of text.
 */

void GraphicEffect::drawText(DrawDIB* destDib, const PixelPoint& p, const char* text, COLORREF colour, HFONT font, int bkMode)
{
   ASSERT(text != 0);
   if(text == 0)
      return;

   d_dib.setBkMode(TRANSPARENT); // bkMode);
   d_dib.setTextColor(colour);
   d_dib.setFont(font);
   d_dib.setTextAlign(TA_LEFT | TA_TOP);

   SIZE s;
   GetTextExtentPoint32(d_dib.getDC(), text, lstrlen(text), &s);

   int width = s.cx + 1;      // Space for borders
   int height = s.cy + 1;

   int destX = p.getX();   // - s.cx / 2;
   int destY = p.getY();   // - s.cy / 2;

   if(bkMode == OPAQUE)
   {
      destDib->lightenRectangle(destX-1, destY-1, s.cx+2, s.cy+2, 50);
      destDib->frame(destX-2, destY-2, s.cx+4, s.cy+4, destDib->getColour(PALETTERGB(0,0,0)), destDib->getColour(PALETTERGB(255,255,255)));
   }

   makeDIB(width, height);

   // Set mask to either 0xff or 0xfe
   // doesn't matter as long as different to the text colour

   ColourIndex paletteIndex = d_dib.getColour(colour);
   ColourIndex maskIndex = paletteIndex + 1;
   d_dib.setMaskColour(maskIndex);

   d_dib.fill(maskIndex);

   wTextOut(d_dib.getDC(), 0, 0, text);

   // destDib->lighten(&d_dib, destX + 1, destY + 1, 35);
   // destDib->darken(&d_dib, destX, destY, 40);

   DIB_Utility::colorize(destDib, &d_dib, colour, 60, destX, destY, s.cx, s.cy, 0, 0);

   if(bkMode == TRANSPARENT)
   {
      DIB_Utility::edge(&d_dib, -1, -1);
      destDib->lighten(&d_dib, destX-1, destY-1, s.cx, s.cy, 0, 0, 35);

      d_dib.setTextColor(colour);
      wTextOut(d_dib.getDC(), 0, 0, text);
      DIB_Utility::edge(&d_dib, +1, +1);
      destDib->darken(&d_dib, destX+1, destY+1, s.cx, s.cy, 0, 0, 50);
   }
}

void GraphicEffect::drawDot(DrawDIB* destDIB, const PixelPoint& p, int dotSize, COLORREF color)
{
   ColorIndex c = destDIB->getColour(color);

   int dibSize = dotSize * 2 + 1 + 2;

   makeDIB(dibSize, dibSize);

   ColourIndex maskIndex = c + 1;      // Make a mask that is different from color
   d_dib.setMaskColour(maskIndex);

   int destX = p.x() - dotSize;
   int destY = p.y() - dotSize;

   // Draw circle around it if large enough

   if(dotSize > 2)
   {
      d_dib.fill(maskIndex);
      d_dib.circle(dotSize, dotSize, dotSize, c);
      destDIB->darken(&d_dib, destX, destY, dibSize, dibSize, 0, 0, 40);
      --dotSize;
      ++destX;
      ++destY;
   }


   d_dib.fill(maskIndex);
   d_dib.disc(dotSize, dotSize, dotSize, c);
   DIB_Utility::colorize(destDIB, &d_dib, color, 30, destX, destY, dibSize, dibSize, 0, 0);
}

void GraphicEffect::hilight(DrawDIB* destDIB, const PixelPoint& p, int dotSize)
{

   int dibSize = dotSize * 2 + 1 + 2;

   makeDIB(dibSize, dibSize);

   ColorIndex c = 0;                   // Any arbitary colour
   ColourIndex maskIndex = c + 1;      // Make a mask that is different from color
   d_dib.setMaskColour(maskIndex);

   int destX = p.x() - dotSize;
   int destY = p.y() - dotSize;

   // Draw circle around it if large enough

   if(dotSize > 2)
   {
      destDIB->circle(p.x(), p.y(), dotSize, destDIB->getColour(PALETTERGB(255,255,255)));
      --dotSize;
      ++destX;
      ++destY;
   }

   d_dib.fill(maskIndex);
   d_dib.disc(dotSize, dotSize, dotSize, c);
   destDIB->lighten(&d_dib, destX, destY, dibSize, dibSize, 0, 0, 50);
}



enum ShowHow {
   Dot,
   Graphic,
   Hidden
};

class TownDisplayImp
{

   DrawDIBDC* d_dib;
   GraphicEffect d_gfx;
   ImageLibrary* imgLib;
   ImageLibrary* dotModeImgLib;

   /*
    * Useful things for displaying things
    */

   struct DispInfo {
      // HFONT font;                   // Prepared font
      Font font;
      int dotSize;                  // size to display dot
      int fontHeight;

      DispInfo() : font(), dotSize(0), fontHeight(0) { }

   } dispInfo[TownSize_MAX];

   TownSize minTownToDisplay;

   MagEnum magLevel;
   MagEnum d_prevMagLevel;
   int d_displayWidth;
   int d_prevDisplayWidth;

   BOOL started;
   BOOL fontInitialised;

   struct TownOnMap : public SLink {
      enum { ChunkSize = 50 };
      static FixedSizeAlloc<TownOnMap> allocator;

      ITown iTown;
      PixelPoint p;

      TownOnMap(ITown i, const PixelPoint& _p) { iTown = i; p = _p; }

      void* operator new(size_t size);
#ifdef _MSC_VER
      void operator delete(void* deadObject);
#else
      void operator delete(void* deadObject, size_t size);
#endif
   };

   SList<TownOnMap> townsOnMap;
   int d_totalTowns;

#ifdef CUSTOMIZE
   UBYTE d_flags;
#endif
   const MapWindowData* d_mapData;


public:
   enum TownDrawMode
   {
      StaticMap,
      DynamicMap
      // OffScreenMap
   };

public:
   TownDisplayImp();
   ~TownDisplayImp();

   BOOL startDisplay(DrawDIBDC* dib, const MapWindowData& mapData);
   void endDisplay();

   // void putTown(DrawDIBDC* dib, const Town* town, const PixelPoint& p, Boolean highlight, Boolean dyn, ShowHow how);

   Boolean isTownShown(const Town& town, TownDrawMode mode) const
   {
#if defined(EDITOR)
      return True;
#else
#if defined(CUSTOMIZE)
      ASSERT(d_mapData != 0);
      if(d_mapData->editMode != PM_Playing)
         return True;
#endif

      if (mode == DynamicMap)
      {
         if(town.isSieged() || town.isRaided() || town.isGarrisoned())
            return true;

//       if(town.hasFortUpgrader() || town.hasSupplyInstaller())
         if(town.isBuildingFort() || town.isBuildingDepot())
            return true;

      }

      if(town.getFortifications() > 0)
         return true;


      const TerrainTypeItem& townTerrain = d_mapData->d_campData->getTerrainType(town.getTerrain());
      if (Options::get(OPT_ShowChokePoints) &&
         townTerrain.isChokePoint() &&
         (minTownToDisplay >= TOWN_City))
      {
            return true;
      }

      if (CampaignOptions::get(OPT_Supply) &&
         Options::get(OPT_ShowSupply) &&
         (town.getIsDepot() || town.getIsSupplySource()))
      {
         return true;
      }

      TownSize s = town.getSize();

      return s <= minTownToDisplay;
#endif
   }

   bool shouldDrawName(const Town& town)
   {
      return (Options::get(OPT_TownNames) &&
         (town.getSize() <= minTownToDisplay));
   }

   int getDotSize(TownSize s) const { return dispInfo[s].dotSize; }
   HFONT getFont(TownSize s) const { return dispInfo[s].font; }


   void addTownOnMap(ITown it, const PixelPoint& p);


   void drawTowns(DrawDIBDC* dib, const MapWindowData& mapData, TownDrawMode mode);
   void drawHighlightedTown(DrawDIBDC* dib, const MapWindowData& map, ITown itown);

   void setup(DrawDIBDC* dib, const MapWindowData& mapData);
#if defined(CUSTOMIZE)
   void setup(DrawDIBDC* dib, const MapWindowData& mapData, UBYTE flags);
#endif

private:
   void drawTown(const TownOnMap* tom, DrawDIBDC* dib, Boolean hl, TownDrawMode mode, const MapWindowData& mapData);
   void clearFonts();
   void makeTownsOnMap(const DIB* dib, const MapWindowData& mapData);
   void initFonts(const MapWindowData& mapData);

   // void drawTownDot(DrawDIBDC* dib, const Town& town, const PixelPoint& p, Boolean highlight, Boolean dyn, const CampaignData& campData);
   // void drawTownGraphic(DrawDIBDC* dib, const Town& town, const PixelPoint& p, Boolean highlight, Boolean dyn, const CampaignData& campData);
   // void drawTownText(DrawDIBDC* dib, const Town& town, const PixelPoint& p, Boolean highlight, Boolean dyn, int xAdjust, int yAdjust);

};











const int TownSizes = 4;
const int ZoomLevels = 5;


static const ShowHow displayTypes[TownSizes][ZoomLevels] = {
  { Dot, Graphic, Graphic, Graphic, Graphic },     // Capital
  { Dot, Dot,     Graphic, Graphic, Graphic },     // City
  { Dot, Dot,     Dot,     Graphic, Graphic },     // Town
  { Dot, Dot,     Dot,     Dot,     Dot     }
};

ShowHow getDisplayType(TownSize size, MagEnum zoom)
{
  ASSERT(size < TownSizes);
  ASSERT(zoom < ZoomLevels);

  return displayTypes[size][zoom];
}

#ifdef DEBUG
FixedSizeAlloc<TownDisplayImp::TownOnMap> TownDisplayImp::TownOnMap::allocator("TownOnMap");
#else
FixedSizeAlloc<TownDisplayImp::TownOnMap> TownDisplayImp::TownOnMap::allocator;
#endif

void* TownDisplayImp::TownOnMap::operator new(size_t size)
{
  return allocator.alloc(size);
}

#ifdef _MSC_VER
void TownDisplayImp::TownOnMap::operator delete(void* deadObject)
{
  allocator.release(deadObject);
}
#else
void TownDisplayImp::TownOnMap::operator delete(void* deadObject, size_t size)
{
  allocator.release(deadObject, size);
}
#endif



/*
 * TownDisplayImp class functions
 * MapWindow is derived from this
 */

const int TownIconWidth = 10;


TownDisplayImp::TownDisplayImp() :
   d_dib(0),
   d_gfx(),
   imgLib(0),
   dotModeImgLib(0),
   minTownToDisplay(TOWN_Capital),
   magLevel(InvalidZoom),
   d_prevMagLevel(InvalidZoom),
   d_displayWidth(0),
   d_prevDisplayWidth(0),
   started(FALSE),
   fontInitialised(FALSE),
#if defined(CUSTOMIZE)
   d_flags(0),
#endif
   d_mapData(0),
   townsOnMap(),
   d_totalTowns(0)
{
}

TownDisplayImp::~TownDisplayImp()
{
   ASSERT(started == FALSE);
   // ASSERT(fontInitialised = FALSE);

   if(started)
      endDisplay();

   if(fontInitialised)
      clearFonts();

   if(imgLib)
     delete imgLib;

   if(dotModeImgLib)
     delete dotModeImgLib;
}

void TownDisplayImp::setup(DrawDIBDC* dib, const MapWindowData& mapData)
{
   d_mapData = &mapData;

   magLevel = mapData.getZoomEnum();
   d_displayWidth = mapData.displayWidth();

   if(magLevel <= 0)
      minTownToDisplay = TOWN_Capital;
   else if(magLevel <= 1)
      minTownToDisplay = TOWN_City;
   else if(magLevel <= 2)
      minTownToDisplay = TOWN_Town;
   else
      minTownToDisplay = TOWN_Other;

   initFonts(mapData);
   makeTownsOnMap(dib, mapData);

   d_prevMagLevel = magLevel;
   d_prevDisplayWidth = d_displayWidth;
}

/*
 * For use by the printMap routines
 */

#if defined(CUSTOMIZE)
void TownDisplayImp::setup(DrawDIBDC* dib, const MapWindowData& mapData, UBYTE flags)
{
   d_mapData = &mapData;
   d_flags = flags;

   magLevel = mapData.getZoomEnum();
   d_displayWidth = mapData.displayWidth();

   initFonts(mapData);
   makeTownsOnMap(dib, mapData);

   d_prevMagLevel = magLevel;
   d_prevDisplayWidth = d_displayWidth;
}
#endif

void TownDisplayImp::initFonts(const MapWindowData& mapData)
{
   magLevel = mapData.getZoomEnum();

   if(!fontInitialised || (magLevel != d_prevMagLevel) || (d_displayWidth != d_prevDisplayWidth) )
   {

      if(fontInitialised)
         clearFonts();

      fontInitialised = TRUE;


      /*
       * Create a font and get set up to display info
       */

      for(int i = 0; i < TownSize_MAX; i++)
      {
         // Size are setup for 1024 by 768 resolution
         // They are automatically scaled depending on size of window

         static UBYTE fontHeights[TownSize_MAX][8] = {
            { 20, 24, 28, 32, 36, 40, 44, 48 },    // Capital
            { 20, 20, 24, 28, 32, 36, 40, 44 },    // City
            { 20, 20, 20, 24, 28, 32, 36, 40 },    // Town
            { 20, 20, 20, 20, 24, 28, 32, 36 }     // Other
         };

         static UBYTE dotSizes[TownSize_MAX][8] = {
            {  4, 5, 6, 7, 8, 9, 9, 9 },
            {  3, 4, 5, 6, 7, 8, 8, 8 },
            {  2, 3, 4, 5, 6, 7, 7, 7 },
            {  2, 2, 3, 4, 5, 6, 6, 6 }
         };

         static DWORD fontWeights[TownSize_MAX] = {
            FW_BOLD,
            FW_DEMIBOLD,
            FW_MEDIUM,
            FW_NORMAL
         };

         int magEnum = magLevel;

         int fontHeight = fontHeights[i][magEnum];
         fontHeight = (fontHeight * d_displayWidth) / BaseDisplayWidth;
         fontHeight = maximum(fontHeight, MinFontHeight);

         DWORD fontWeight = fontWeights[i];

         int dotSize = dotSizes[i][magEnum];
         dotSize = (dotSize * d_displayWidth) / BaseDisplayWidth;
         dotSize = maximum(dotSize, 2);
         dispInfo[i].dotSize = dotSize;

         dispInfo[i].fontHeight = fontHeight;

         LogFont lf;
         lf.height(fontHeight);
         lf.weight(fontWeight);

         if(fontHeight < 16)
         {
            lf.style(FF_SWISS);
            lf.weight(FW_NORMAL);
         }
         else
         {
            lf.face(scenario->fontName(Font_Normal));
            if(fontWeight < FW_DEMIBOLD)
               lf.italic(true);
         }

         dispInfo[i].font.set(lf);
      }
   }
}

void TownDisplayImp::clearFonts()
{
   ASSERT(fontInitialised == TRUE);

   fontInitialised = FALSE;

   for(int i = 0; i < TownSize_MAX; i++)
   {
      if(dispInfo[i].font != NULL)
      {
//       DeleteObject(dispInfo[i].font);
//       dispInfo[i].font = NULL;
         dispInfo[i].font.reset();
      }
   }
}

class TownImages {
public:

   static const int nNations;
   static const int nSides;
   static const int nSizes;
   static const int TownImagesPerSize;
   static const int FortImagesPerSize;
   static const int FortImageOffset;
   static const int TotalImages;
   static const int DotModeTotalImages;
// static const int ChokePointOffset;

   static UWORD getBaseImage(TownSize size);
   static UWORD getNationalityImage(Nationality n);
   static UWORD getFlag(Side s);
   static UWORD getFortImageIndex(TownSize size,  UBYTE fortLevel);
   static int getDotFortImageIndex(const Town& town);
      // get image index or -1 for no image
   static int getDotChokeImageIndex(const CampaignData* campData, const Town& town);

   static const char* townImageName;
   static const char* dotModeImageName;

// static const ImagePos* townRects;
};

const int TownImages::nNations = 3;
const int TownImages::nSides = 2;
const int TownImages::nSizes = 4;

const int TownImages::TownImagesPerSize = nNations;
const int TownImages::FortImagesPerSize = 4;
const int TownImages::FortImageOffset = (TownImagesPerSize * nSizes);
const int TownImages::TotalImages =  FortImageOffset + (FortImagesPerSize * nSizes);
const int TownImages::DotModeTotalImages = DotMode_HowMany;
//const int TownImages::ChokePointOffset = DotMode_FrenchChoke;

const char* TownImages::townImageName = MAKEINTRESOURCE(BM_ALLCITIES);
const char* TownImages::dotModeImageName = MAKEINTRESOURCE(BM_DOTMODEIMAGES);

//const ImagePos* TownImages::townRects = (ImagePos*)loadResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(PR_TOWNICONS), RT_IMAGEPOS);


UWORD TownImages::getBaseImage(TownSize size)
{
   ASSERT(size >= 0);
   ASSERT(size < 4);
   if((size < 0) || (size >= 4))
      size = TownSize(0);

   return UWORD(size * TownImagesPerSize);
}

UWORD TownImages::getNationalityImage(Nationality n)
{
   // ASSERT(n >= 0);
   // ASSERT(n < nNations);

   if(n == NATION_Neutral)
      n = 0;

   if((n < 0) || (n >= nNations))
      n = Nationality(0);
// return UWORD(nSides + (n * ImagesPerNation));

   return UWORD(n);
}

UWORD TownImages::getFlag(Side s)
{
   ASSERT((s >= 0) && (s < nSides));
   if((s < 0) || (s >= nSides))
      s = Side(0);

   return s;
}

UWORD TownImages::getFortImageIndex(TownSize size,  UBYTE fortLevel)
{

  UWORD index = UWORD(FortImageOffset + (size * FortImagesPerSize) + (fortLevel - 1));

  ASSERT(index < TotalImages);

  return index;

}


int TownImages::getDotFortImageIndex(const Town& town)
{
   UWORD base = 0;

   if(town.isSieged() || town.isRaided())
   {
      if(town.getSide() == 0)
         return DotMode_FrenchSiege0 + town.getFortifications();
      else if(town.getSide() == 1)
         return DotMode_AlliedSiege0 + town.getFortifications();
   }

   if(town.getFortifications())
      return DotMode_Fort1 + town.getFortifications() - 1;

   return -1;
}


int TownImages::getDotChokeImageIndex(const CampaignData* campData, const Town& town)
{
   int i = -1;

   const TerrainTypeItem& townTerrain = campData->getTerrainType(town.getTerrain());
   if (townTerrain.isChokePoint())
   {
      if(townTerrain.isPass())
         i = DotMode_FrenchMountain;
      else // if (townTerrain.isBridge())
      {
         if(town.isBridgeUp())
            i = DotMode_FrenchChoke;
         else
            i = DotMode_BrokenFrenchChoke;
      }


      if(town.getSide() == SIDE_Neutral)
         i += 2;
      else
         i += town.getSide();
   }

   return i;


//          UWORD index = static_cast<UWORD>( (town.getSide() != SIDE_Neutral) ? town.getSide()+TownImages::ChokePointOffset : 2+TownImages::ChokePointOffset);
}

BOOL TownDisplayImp::startDisplay(DrawDIBDC* dib, const MapWindowData& mapData)
{
   ASSERT(started == FALSE);
   ASSERT(dib != 0);
   ASSERT(d_dib == 0);

   d_dib = dib;

   dib->saveDC();

   dib->setBkMode(TRANSPARENT);

   // initFonts(mapWind);

   if(imgLib == 0)
   {
      ImagePos* imagePos = (ImagePos*)loadResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(PR_TOWNICONS), RT_IMAGEPOS);
      imgLib = scenario->getImageLibrary(TownImages::townImageName, TownImages::TotalImages, imagePos);
      ASSERT(imgLib != 0);
   }

   if(dotModeImgLib == 0)
   {
     ImagePos* imagePos = reinterpret_cast<ImagePos*>(loadResource(scenario->getScenarioResDLL(), MAKEINTRESOURCE(PR_DOTMODEIMAGES), RT_IMAGEPOS));
     dotModeImgLib = scenario->getImageLibrary(TownImages::dotModeImageName, TownImages::DotModeTotalImages, imagePos);
     ASSERT(dotModeImgLib != 0);
   }

   started = TRUE;

   return TRUE;
}

void TownDisplayImp::endDisplay()
{
   ASSERT(started == TRUE);
   ASSERT(d_dib != 0);

   if(started)
   {
      // gApp.restoreDibDC();
      d_dib->restoreDC();
      started = FALSE;
      d_dib = 0;
   }
}

/*
 * Create the list of towns that are displayed in the current map window
 */

void TownDisplayImp::makeTownsOnMap(const DIB* dib, const MapWindowData& mapData)
{
   townsOnMap.reset();

   d_totalTowns = mapData.d_campData->getTowns().entries();

   TownIter tl = mapData.d_campData->getTowns();

   while(++tl)
   {
      ITown itown = tl.currentID();
      const Town& town = tl.current();
      TownSize size = town.getSize();

// #if defined(DEBUG)
//       Boolean showTown = False;
//       if(d_flags != 0)
//       {
//         if(size == TOWN_Capital)
//           showTown = (d_flags & TownDisplay::Capitals);
//         else if(size == TOWN_City)
//           showTown = (d_flags & TownDisplay::Cities);
//         else if(size == TOWN_Town)
//           showTown = (d_flags & TownDisplay::Towns);
//         else if(size == TOWN_Other)
//           showTown = (d_flags & TownDisplay::Others);
//       }
//       else
//         showTown = isTownShown(town);
//
//       if(showTown)
// #else
//       if(isTownShown(town))
// #endif
      {
         Location l = town.getLocation();
         PixelPoint p;
         mapData.locationToPixel(l, p);

         if(mapData.onDisplay(p))   // This ought to take into account size of graphic!
         {
            // Add it to the townsOnMap
            // This should keep them sorted by Y coordinate

            SListIter<TownOnMap> tomIter = &townsOnMap;
            TownOnMap* lastTom = 0;
            while(++tomIter)
            {
               TownOnMap* tom = tomIter.current();
               if(tom->p.getY() > p.getY())
                  break;
               lastTom = tom;
            }

            TownOnMap* newTom = new TownOnMap(itown, p);
            townsOnMap.insertAfter(lastTom, newTom);
         }
      }
   }
}

/*
 * Some of this will need to be pulled out to be shared by the
 * highlight town code, and some will need to be moved to the dynamic
 * town display function.
 */


void TownDisplayImp::drawTowns(DrawDIBDC* dib, const MapWindowData& mapData, TownDrawMode mode)
{
   if(d_totalTowns != mapData.d_campData->getTowns().entries())
      makeTownsOnMap(dib, mapData);

   if(townsOnMap.entries() > 0)
   {
      if(startDisplay(dib, mapData))
      {
         // mapData.d_campData->startReadLock();
         mapData.d_campData->getProvinces().startRead();
         mapData.d_campData->getTowns().startRead();

         SListIter<TownOnMap> iter = &townsOnMap;
         while(++iter)
         {
            const TownOnMap* tom = iter.current();

            drawTown(tom, dib, False, mode, mapData);

         }
         mapData.d_campData->getTowns().endRead();
         mapData.d_campData->getProvinces().endRead();
         // mapData.d_campData->endReadLock();

         endDisplay();
      }
   }
}


void TownDisplayImp::drawTown(const TownOnMap* tom, DrawDIBDC* dib, Boolean highlight, TownDrawMode mode, const MapWindowData& mapData)
{
   const Town& town = mapData.d_campData->getTown(tom->iTown);

   if(!highlight && !isTownShown(town, mode))
      return;


   const PixelPoint& p = tom->p;

#if defined(EDITOR)
    bool isSeen = true;
#else
    bool isSeen = ( (!CampaignOptions::get(OPT_FogOfWar)) ||
             (GamePlayerControl::canControl(town.getSide())) ||
             (town.isSeen()));
#endif


   /*
    * Work out sizes and colours
    */

   ShowHow showHow = getDisplayType(town.getSize(), magLevel);

   if(!Options::get(OPT_TownIcons))
      showHow = Dot;

   if(highlight && (Options::get(OPT_TownHilightIcons)))
      showHow = Graphic;

   ASSERT( (showHow == Graphic) || (showHow == Dot) );

#if defined(CUSTOMIZE)
   if(d_flags)
   {
     if(d_flags & TownDisplay::DotMode)
       showHow = Dot;
     if(d_flags & TownDisplay::TownNames)
       highlight = True;
   }
#endif  // DEBUG

   /*
    * Get text alignment
    */

   int xAdjust = 0;
   int yAdjust = 0;

   LONG minX = p.x();
   LONG maxX = p.x();
   LONG minY = p.y();
   LONG maxY = p.y();

   /*
    * Info used later by graphic mode
    */

   UWORD imgIndex = 0;
   int hotX = 0;
   int hotY = 0;
   int townW = 0;
   int townH = 0;

   if(showHow == Dot)
   {
      xAdjust = getDotSize(town.getSize());
      yAdjust = getDotSize(town.getSize());

      minX = p.x() - xAdjust;
      maxX = p.x() + xAdjust;
      minY = p.y() - yAdjust;
      maxY = p.y() + yAdjust;
   }
   else
   {
      Nationality nationality = NATION_Neutral;

      IProvince iprov = town.getProvince();
      if(iprov != NoProvince)
      {
         const Province& prov = mapData.d_campData->getProvince(iprov);
         nationality = prov.getNationality();
      }


      UWORD baseImage = TownImages::getBaseImage(town.getSize());
      imgIndex = UWORD(baseImage + TownImages::getNationalityImage(nationality));

      /*
       *  Get town image info for placement of pennant or flag
       */

      imgLib->getHotSpots(imgIndex, hotX, hotY);
      imgLib->getImageSize(imgIndex, townW, townH);

      xAdjust = townW / 2;
      yAdjust = townH / 2;

      minX = p.x() - xAdjust;
      maxX = p.x() + xAdjust;
      minY = p.y() - yAdjust;
      maxY = p.y() + yAdjust;
   }

   /*
    * get text position and size
    */

   LONG tx = p.getX() + town.getNameX();
   LONG ty = p.getY() + town.getNameY();
   LONG tw = 0;
   LONG th = 0;

   const char* name = town.getName();

   HFONT font = NULL;

   if(name)
   {
      int dotSize = getDotSize(town.getSize());

      font = getFont(town.getSize());
      SIZE tSize;
      int sLen = lstrlen(name);
      dib->setFont(font);
      GetTextExtentPoint32(dib->getDC(), name, sLen, &tSize);

      tw = tSize.cx;
      th = tSize.cy;

      switch(town.getNameAlign())
      {
         case NA_TOPLEFT:
            tx += -tSize.cx - dotSize - xAdjust;
            ty += -tSize.cy - dotSize - yAdjust;
            break;
         case NA_TOP:
            tx += -tSize.cx / 2;
            ty += -tSize.cy - dotSize - xAdjust;
            break;
         case NA_TOPRIGHT:
            tx += dotSize + xAdjust;
            ty += -tSize.cy - dotSize - yAdjust;
            break;
         case NA_RIGHT:
            tx += dotSize + xAdjust;
            ty += -tSize.cy / 2;
            break;
         case NA_BOTTOMRIGHT:
            tx += dotSize + xAdjust;
            ty += dotSize + yAdjust;
            break;
         case NA_BOTTOM:
            tx += -tSize.cx / 2;
            ty += dotSize + yAdjust;
            break;
         case NA_BOTTOMLEFT:
            tx += -tSize.cx - dotSize - xAdjust;
            ty += dotSize + yAdjust;
            break;
         case NA_LEFT:
            tx += -tSize.cx - dotSize - xAdjust;
            ty += -tSize.cy / 2;
            break;
         default:
#ifdef DEBUG
            throw GeneralError("Illegal name alignment for %s (%d)",
               name,
               (int) town.getNameAlign());
#else
            break;
#endif
      }  // switch

      minX = minimum(tx, minX);
      minY = minimum(ty, minY);
      maxX = maximum(tx + tSize.cx, maxX);
      maxY = maximum(ty + tSize.cy, maxY);
   }

   if(showHow == Dot)
   {
      if(highlight || (mode == StaticMap))
      {
         const TerrainTypeItem& townTerrain = mapData.d_campData->getTerrainType(town.getTerrain());
         if(!Options::get(OPT_ShowChokePoints) || !townTerrain.isChokePoint())
         {
            COLORREF cr = scenario->getSideColour(town.getSide());

            if(highlight)
            {
               cr = Palette::brightColour(cr, 255, 192);
            }
            else
            {
               cr = Palette::brightColour(cr, 192, 192);
            }


            int dotSize = getDotSize(town.getSize());

            d_gfx.drawDot(dib, p, dotSize, cr);

         }
         else
         {
//          UWORD index = static_cast<UWORD>( (town.getSide() != SIDE_Neutral) ? town.getSide()+TownImages::ChokePointOffset : 2+TownImages::ChokePointOffset);
            int index = TownImages::getDotChokeImageIndex(mapData.d_campData, town);
            if(index >= 0)
               dotModeImgLib->blit(dib, index, p.getX(), p.getY());
         }

         /*
          * Put fort-level, if any
          */

         // if(town.getFortifications() > 0)       {
            int fortImage = TownImages::getDotFortImageIndex(town);
            if (fortImage >= 0)
            {
               dotModeImgLib->blit(dib, fortImage,  //static_cast<UWORD>(town.getFortifications()-1),
                              p.getX(), p.getY());
            }
      }
   }

#if 0
      if(mode == DynamicMap)
      {
         /*
          * If there is a garrison
          */

         // TODO

         /*
          * Put flashing forts for any that are being built?
          */

         if(town.hasFortUpgrader())
         {
            static Boolean show = False;
            const int maxFort = 4;

            show = !show;
            if(show && town.getFortifications() < maxFort)
               dotModeImgLib->blit(dib, static_cast<UWORD>(town.getFortifications()),
                                 p.getX(), p.getY());

         }
      }
#endif


   else {   // Graphic

      if(highlight || (mode == StaticMap))
      {
         if (Options::get(OPT_ShowChokePoints))
         {
            int index = TownImages::getDotChokeImageIndex(mapData.d_campData, town);
            if(index >= 0)
               dotModeImgLib->blit(dib, index, p.getX(), p.getY());
         }

         imgLib->blit(dib, UWORD(imgIndex), p.getX(), p.getY(), True);

         /*
          * Flag for who owns it
          */

         if(town.getSide() != SIDE_Neutral)
         {

            Nationality n = scenario->getDefaultNation(town.getSide());
            ImageLibrary* flagImages = scenario->getNationFlag(n, True);

            int flagW;
            int flagH;
            flagImages->getImageSize((UWORD)FI_Pennant, flagW, flagH);

            flagImages->flipBlit(dib, (UWORD)FI_Pennant, (p.getX()-hotX)+((townW/2)-flagW), (p.getY()-hotY)+((townH/2)-flagH));
         }

         /*
          * If there is a fort...
          */

         if(town.getFortifications() != 0)
         {
            UWORD imageIndex = TownImages::getFortImageIndex(town.getSize(), town.getFortifications());
            imgLib->blit(dib, imageIndex, p.getX(), p.getY(), True);
         }

      }
   }

   if(!highlight && (mode == DynamicMap))
   {
      if(town.isGarrisoned() && isSeen)
      {
         ASSERT(town.getSide() != SIDE_Neutral);
         Nationality n = scenario->getDefaultNation(town.getSide());
         ImageLibrary* flagImages = scenario->getNationFlag(n, True);

         int flagW;
         int flagH;
         flagImages->getImageSize((UWORD)FI_Garrison, flagW, flagH);
         flagImages->blit(dib, (UWORD)FI_Garrison, (p.getX()-hotX)+((townW/2)-flagW), (p.getY()-hotY)+((townH/2)-flagH));
      }
   }

   /*
    * Draw Text
    */

// if(highlight || (mode == StaticMap))
   if(mode == StaticMap)
   {
//    if(name && (highlight || Options::get(OPT_TownNames)))
      if(name &&
         shouldDrawName(town))
      {
         COLORREF textColour;

         COLORREF rgb = scenario->getSideColour(town.getSide());

         int bkMode;

         if(highlight)
         {
            textColour = rgb;
            bkMode = OPAQUE;
            dib->setBkColor(PALETTERGB(255,255,255));
         }
         else
         {
            textColour = Palette::brightColour(rgb, 192, 255);
            bkMode = TRANSPARENT;
         }

         ASSERT(font != NULL);

         d_gfx.drawText(dib, PixelPoint(tx,ty), name, textColour, font, bkMode);
      }
   }


   /*
    * Show supply depots and source differently
    *
    * For now let's draw a circle around it
    */

   /*
   * Only draw if in options
   */
   if(Options::get(OPT_ShowSupply) && isSeen) {

      if((highlight || (mode == StaticMap)) && CampaignOptions::get(OPT_Supply))
      {
         if(town.getIsDepot())
         {
          dotModeImgLib->blit(dib, DotMode_SupplyDepot, p.getX(), p.getY());
   //       ColourIndex c = dib->getColour(PALETTERGB(255, 255, 255));
   //       dib->circle(p.getX(), p.getY(), 8, c);
         }

         if(town.getIsSupplySource())
         {
          dotModeImgLib->blit(dib, DotMode_SupplySource, p.getX(), p.getY());
   //       ColourIndex c = dib->getColour(PALETTERGB(192, 255, 192));
   //       ColourIndex c1 = dib->getColour(PALETTERGB(128, 255, 128));
   //       dib->frame(p.getX() - 10, p.getY() - 10, 21, 21, c);
   //       dib->frame(p.getX() - 9, p.getY() - 9, 19, 19, c1);
         }
      }
   }



   /*
    * Put engineer
    */

   if(isSeen && (mode == DynamicMap))
   {
      /*
       * If there is a garrison
       */

      // TODO

      /*
       * Put flashing forts for any that are being built?
       */

//    if(town.hasFortUpgrader() || town.hasSupplyInstaller())
      if(town.isBuildingFort() || town.isBuildingDepot())
      {
//       static Boolean show = False;
//       const int maxFort = 4;

//       show = !show;
//       if(show && town.getFortifications() < maxFort)
//          dotModeImgLib->blit(dib, static_cast<UWORD>(town.getFortifications()),
            dotModeImgLib->blit(dib, DotMode_Engineering,
                              p.getX(), p.getY());

      }
   }



#if !defined(EDITOR)
#ifdef DEBUG
   // if(highlight || (mode == StaticMap))
   if(mode == DynamicMap)
   {
      if(Options::get(OPT_ShowAI))
         campaign->showAI(dib, tom->iTown, p);
   }
#endif
#endif   // !EDITOR


   if(highlight)
   {
      d_gfx.hilight(dib, p, (10 * d_displayWidth) / BaseDisplayWidth);
   }

}


/*
 * Draw a single highlighted town
 * The highlighted town may not already be in the townsOnMap list
 * so everything needs to be calculated
 */

void TownDisplayImp::drawHighlightedTown(DrawDIBDC* dib, const MapWindowData& mapData, ITown itown)
{
   const Town& town = mapData.d_campData->getTown(itown);


   Location l = town.getLocation();
   PixelPoint p;
   mapData.locationToPixel(l, p);
   if(mapData.onDisplay(p))
   {
      TownOnMap tom(itown, p);
      drawTown(&tom, dib, True, DynamicMap, mapData);
   }
}

/*========================================================================
 * Insert Display
 */

// moved to inserts.cpp
/*========================================================================
 * Province Display
 */

// moved to provdisp.cpp

/*========================================================================
 * Global Access Functions
 */

TownDisplay::TownDisplay() :
   d_townImp(new TownDisplayImp)
{
  ASSERT(d_townImp != 0);
}

TownDisplay::~TownDisplay()
{
  if(d_townImp)
    delete d_townImp;
}


/*
 * Draw all towns onto static map
 */

void TownDisplay::drawStatic(DrawDIBDC* dib, const MapWindowData& mapData)
{
   ASSERT(dib != 0);
   ASSERT(d_townImp != 0);

   /*
    * Step through towns and add to TownsOnMap
    */

   d_townImp->setup(dib, mapData);
   d_townImp->drawTowns(dib, mapData, TownDisplayImp::StaticMap);
}

#if defined(CUSTOMIZE)
void TownDisplay::drawStatic(DrawDIBDC* dib, const MapWindowData& mapData, UBYTE townFlags)
{
   ASSERT(dib != 0);
   ASSERT(d_townImp != 0);

   /*
    * Step through towns and add to TownsOnMap
    */

   d_townImp->setup(dib, mapData, townFlags);
   d_townImp->drawTowns(dib, mapData, TownDisplayImp::DynamicMap);
}
#endif // DEBUG

void TownDisplay::drawDynamic(DrawDIBDC* dDIB, const MapWindowData& mapData)
{
   ASSERT(d_townImp != 0);
   ASSERT(dDIB != 0);

   /*
    * Draw dynamic parts of towns on map
    */

   d_townImp->drawTowns(dDIB, mapData, TownDisplayImp::DynamicMap);

   /*
    * Draw any highlighted towns
    */


   if(d_townImp->startDisplay(dDIB, mapData))
   {
      mapData.d_campData->getProvinces().startRead();
      mapData.d_campData->getTowns().startRead();

      if(mapData.d_selectedProvince != NoProvince)
      {
#if defined(CUSTOMIZE)
         if(mapData.editMode == PM_EditProvince)
         {
            // IProvince pi = campaign->getProvinces().getID(d_selectedProvince);
            // ASSERT(pi != NoProvince);

            CArrayIter<Town> tl = mapData.d_campData->getTowns();

            while(++tl)
            {
               const Town& town = tl.current();

               if( d_townImp->isTownShown(town, TownDisplayImp::DynamicMap) &&
                  (town.getProvince() == mapData.d_selectedProvince) )
                  d_townImp->drawHighlightedTown(dDIB, mapData, tl.currentID());
            }
         }
#endif

      }

      if(mapData.timedHighlight && mapData.highlightedTown != NoTown)
      {
         d_townImp->drawHighlightedTown(dDIB, mapData, mapData.highlightedTown);
         //---- handled during requestRedraw
         // countTimedHighLight();
      }
      else if(mapData.d_selectedTown != NoTown)
      {
         d_townImp->drawHighlightedTown(dDIB, mapData, mapData.d_selectedTown);
      }

#if defined(CUSTOMIZE)
      if((mapData.edittingTown != NoTown) && (mapData.edittingTown != mapData.d_selectedTown))
      {
         d_townImp->drawHighlightedTown(dDIB, mapData, mapData.edittingTown);
      }

      if(mapData.editMode == PM_EditConnection)
      {
   #ifdef DEBUG
         debugLog("selectedTown=%d, edittingTown=%d\n",
            (int) mapData.d_selectedTown,
            (int) mapData.edittingTown);
   #endif

         if( (mapData.d_selectedTown != NoTown) && (mapData.edittingTown != NoTown) )
         {
            // draw line from selectedTown to edittingTown

            const Town* sTown = &mapData.d_campData->getTown(mapData.d_selectedTown);
            const Town* eTown = &mapData.d_campData->getTown(mapData.edittingTown);


            const Location& l1 = sTown->getLocation();
            const Location& l2 = eTown->getLocation();

            PixelPoint p1;
            PixelPoint p2;

            mapData.locationToPixel(l1, p1);
            mapData.locationToPixel(l2, p2);

            ColourIndex colour;
            if(GameControl::getTick() & 1)
               colour = dDIB->getColour(PALETTERGB(255,128,128));
            else
               colour = dDIB->getColour(PALETTERGB(255,255,255));
            dDIB->line(p1.getX(), p1.getY(), p2.getX(), p2.getY(), colour);
         }
      }
#endif   // CUSTOMIZE

      mapData.d_campData->getTowns().endRead();
      mapData.d_campData->getProvinces().endRead();
      d_townImp->endDisplay();
   }
}





/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.3  2001/07/02 15:23:17  greenius
 * Battle, Campaign, Combined game and Campaign Editor now compile and run... mostly
 *
 * Revision 1.2  2001/06/18 08:25:45  greenius
 * Added Visual C++ Project files... half way to compiling
 *
 *----------------------------------------------------------------------
 */
