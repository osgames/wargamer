##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

######################################################
# $Id$
######################################################
#
# Makefile for testing PoolArray structure.
#
######################################################
#
# $Log$
# Revision 1.1  1996/01/19 11:30:57  Steven_Green
# Initial revision
#
#
######################################################

name = testpool

CDIR=c
HDIR=h
ODIR=o

LNKEXT=lk

LNK = $(name).$(LNKEXT)
lnk_dependencies = tile.mk dos32.mif

CFLAGS = -DTESTPOOL
OBJS = poolarry.obj

!include dos32.mif

linkit : .PROCEDURE
    @%append $(LNK) system dos4g
    @%append $(LNK) option stack=8k
    
