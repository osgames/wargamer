##############################################################################
# Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)	 #
# This Software is subject to the GNU General Public License.  				 #
# For License information see the file COPYING in the project root directory #
# For more information see the file README.									 #
##############################################################################

##########################################
# Makefile for nap1813\batres.dll
#
# Contains resources for nap1813 Battle game


name = batedres
ROOT=.
DLL_DIR=game\nap1813
RDIR=res
CFLAGS += -i=$(RDIR)
CDIR=res

!include wgpaths.mif

# EXT=DLL
rcname=batedit.rc
RC=1
OBJS = scn_dll.obj

lnk_dependencies += batedres.mk

all :: $(TARGETS) .SYMBOLIC
	@%null

!include dll95.mif

