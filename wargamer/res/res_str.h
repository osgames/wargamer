/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.            		*
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.                        				*
 *----------------------------------------------------------------------------*/
#ifndef RES_STR_H
#define RES_STR_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *      Strings in Resource File
 *
 * Prefix usage:
 *              IDM_ : Menu Item (These may use between 1..999
 *              IDS_ : String ID
 *              TTS_ : Tool Tip String
 *              SWS_ : Status Window String
 *
 * In many cases, these prefixes are followed by another that indicates
 * what dialog box or window they belong to.
 *
 *----------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Menu ID's range from 100 to 171
 * IDM_FIRST to IDM_LAST
 */

/*
 * UnitTB Menu Items range from 400->444
 * (UTB_IDM_FIRST->UTB_IDM_LAST)
 */

/*
 * We will assume Menu's can use 0..999
 */

/*
 * Scream... why cant rc use enums? or some similar mechanism?
 * Get too many nested #defines with the way I was doing it.
 */


//convRes: #define IDS_FIRST                       1000
#define IDS_FIRST                       1000

//convRes: #define IDS_MISC_FIRST  IDS_FIRST
#define IDS_MISC_FIRST  1000
//convRes: #define IDS_Caption             IDS_MISC_FIRST + 0
#define IDS_Caption             1000
//convRes: #define IDS_GRID                        IDS_MISC_FIRST + 1
#define IDS_GRID                        1001
//convRes: #define IDS_LOCATOR             IDS_MISC_FIRST  + 2
#define IDS_LOCATOR             1002
//convRes: #define IDS_PALETTE             IDS_MISC_FIRST  + 3
#define IDS_PALETTE             1003
//convRes: #define IDS_Overview            IDS_MISC_FIRST + 4
#define IDS_Overview            1004
// #define IDS_MISC_LAST   IDS_MISC_FIRST + 5
/*
 * Find Town and Find Leader Dialog Strings
 */

/* #define IDS_FTD_FIRST                             IDS_MISC_LAST           */
/* #define IDS_FTD_COLSTR_TOWN        IDS_FTD_FIRST+0                        */
/* #define IDS_FTD_COLSTR_STATE       IDS_FTD_FIRST+1                        */
/* #define IDS_FTD_COLSTR_TYPE        IDS_FTD_FIRST+2                        */
/*                                                                           */
/* #define IDS_FTD_COLSTR_LEADER      IDS_FTD_FIRST+3                        */
/* #define IDS_FTD_COLSTR_UNIT        IDS_FTD_FIRST+4                        */
/* #define IDS_FTD_COLSTR_RANK        IDS_FTD_FIRST+5                        */
/* #define IDS_FTD_LAST                                      IDS_FTD_FIRST+6 */

// strings for UnitInfo attribute headers

// #define IDS_UI_FIRST          IDS_FTD_LAST
// #define IDS_UI_FIRST          IDS_MISC_LAST
//convRes: #define IDS_UI_FIRST          IDS_FIRST+10
#define IDS_UI_FIRST          1010

/*
 * String definitions for User Interface Resource strings
 */

//convRes: #define IDS_UI_STRENGTH       IDS_UI_FIRST+0
#define IDS_UI_STRENGTH       1010
//convRes: #define IDS_UI_STRAGGLERS     IDS_UI_FIRST+1
#define IDS_UI_STRAGGLERS     1011
//convRes: #define IDS_UI_MORALE         IDS_UI_FIRST+2
#define IDS_UI_MORALE         1012
//convRes: #define IDS_UI_FATIGUE        IDS_UI_FIRST+3
#define IDS_UI_FATIGUE        1013
//convRes: #define IDS_UI_SUPPLY         IDS_UI_FIRST+4
#define IDS_UI_SUPPLY         1014
//convRes: #define IDS_UI_INITIATIVE     IDS_UI_FIRST+5
#define IDS_UI_INITIATIVE     1015
//convRes: #define IDS_UI_AGGRESSION     IDS_UI_FIRST+6
#define IDS_UI_AGGRESSION     1016
//convRes: #define IDS_UI_CHARISMA       IDS_UI_FIRST+7
#define IDS_UI_CHARISMA       1017
//convRes: #define IDS_UI_ABILITY        IDS_UI_FIRST+8
#define IDS_UI_ABILITY        1018
//convRes: #define IDS_UI_LEADER         IDS_UI_FIRST+9
#define IDS_UI_LEADER         1019
//convRes: #define IDS_UI_ILEADER        IDS_UI_FIRST+10
#define IDS_UI_ILEADER        1020
//convRes: #define IDS_UI_HEALTH         IDS_UI_FIRST+11
#define IDS_UI_HEALTH         1021
//convRes: #define IDS_UI_RANK           IDS_UI_FIRST+12
#define IDS_UI_RANK           1022
//convRes: #define IDS_UI_TABILITY       IDS_UI_FIRST+13
#define IDS_UI_TABILITY       1023
//convRes: #define IDS_UI_CORPS          IDS_UI_FIRST+14
#define IDS_UI_CORPS          1024
//convRes: #define IDS_UI_DIVISIONS      IDS_UI_FIRST+15
#define IDS_UI_DIVISIONS      1025
//convRes: #define IDS_UI_INFANTRY       IDS_UI_FIRST+16
#define IDS_UI_INFANTRY       1026
//convRes: #define IDS_UI_CAVALRY        IDS_UI_FIRST+17
#define IDS_UI_CAVALRY        1027
//convRes: #define IDS_UI_ARTY           IDS_UI_FIRST+18
#define IDS_UI_ARTY           1028
//convRes: #define IDS_UI_OTHER          IDS_UI_FIRST+19
#define IDS_UI_OTHER          1029
//convRes: #define IDS_UI_UNITTYPE       IDS_UI_FIRST+20
#define IDS_UI_UNITTYPE       1030
//convRes: #define IDS_UI_ORDERS         IDS_UI_FIRST+21
#define IDS_UI_ORDERS         1031
//convRes: #define IDS_UI_ACTION         IDS_UI_FIRST+22
#define IDS_UI_ACTION         1032
//convRes: #define IDS_UI_POOR           IDS_UI_FIRST+23
#define IDS_UI_POOR           1033
//convRes: #define IDS_UI_FAIR           IDS_UI_FIRST+24
#define IDS_UI_FAIR           1034
//convRes: #define IDS_UI_GOOD           IDS_UI_FIRST+25
#define IDS_UI_GOOD           1035
//convRes: #define IDS_UI_EXCELLENT      IDS_UI_FIRST+26
#define IDS_UI_EXCELLENT      1036
//convRes: #define IDS_UI_EXHAUSTED      IDS_UI_FIRST+27
#define IDS_UI_EXHAUSTED      1037
//convRes: #define IDS_UI_TIRED          IDS_UI_FIRST+28
#define IDS_UI_TIRED          1038
//convRes: #define IDS_UI_STIRED         IDS_UI_FIRST+29
#define IDS_UI_STIRED         1039
//convRes: #define IDS_UI_FRESH          IDS_UI_FIRST+30
#define IDS_UI_FRESH          1040
//convRes: #define IDS_UI_TIMID          IDS_UI_FIRST+31
#define IDS_UI_TIMID          1041
//convRes: #define IDS_UI_CAUTIOUS       IDS_UI_FIRST+32
#define IDS_UI_CAUTIOUS       1042
//convRes: #define IDS_UI_FAGGRESSIVE    IDS_UI_FIRST+33
#define IDS_UI_FAGGRESSIVE    1043
//convRes: #define IDS_UI_AGGRESSIVE     IDS_UI_FIRST+34
#define IDS_UI_AGGRESSIVE     1044
//convRes: #define IDS_UI_DEAD           IDS_UI_FIRST+35
#define IDS_UI_DEAD           1045

//convRes: #define IDS_UI_LEADERUNKNOWN   IDS_UI_FIRST+36
#define IDS_UI_LEADERUNKNOWN   1046
//convRes: #define IDS_UI_UNIDUNIT        IDS_UI_FIRST+37
#define IDS_UI_UNIDUNIT        1047
//convRes: #define IDS_UI_MOVINGTO        IDS_UI_FIRST+38
#define IDS_UI_MOVINGTO        1048
//convRes: #define IDS_UI_FORCEMARCHTO    IDS_UI_FIRST+39
#define IDS_UI_FORCEMARCHTO    1049
//convRes: #define IDS_UI_EASYMARCHTO     IDS_UI_FIRST+40
#define IDS_UI_EASYMARCHTO     1050
//convRes: #define IDS_UI_NORMALMARCHTO   IDS_UI_FIRST+41
#define IDS_UI_NORMALMARCHTO   1051
//convRes: #define IDS_UI_INFOUNAVAILABLE IDS_UI_FIRST+42
#define IDS_UI_INFOUNAVAILABLE 1052

//convRes: #define IDS_UI_ORDERPARAGRAPH   IDS_UI_FIRST+43
#define IDS_UI_ORDERPARAGRAPH   1053
//convRes: #define IDS_UI_POSTUREPARAGRAPH IDS_UI_FIRST+44
#define IDS_UI_POSTUREPARAGRAPH 1054
//convRes: #define IDS_UI_ENEMYPARAGRAPH   IDS_UI_FIRST+45
#define IDS_UI_ENEMYPARAGRAPH   1055
//convRes: #define IDS_UI_AOPARAGRAPH      IDS_UI_FIRST+46
#define IDS_UI_AOPARAGRAPH      1056

//convRes: #define IDS_UI_UNIT             IDS_UI_FIRST+47
#define IDS_UI_UNIT             1057
//convRes: #define IDS_UI_FORMATIONS       IDS_UI_FIRST+48
#define IDS_UI_FORMATIONS       1058
// #define IDS_UI_GUNS             IDS_UI_FIRST+49
//convRes: #define IDS_GunsAbrev           IDS_UI_FIRST+49
#define IDS_GunsAbrev           1059
//convRes: #define IDS_UI_CANCONTROL       IDS_UI_FIRST+50
#define IDS_UI_CANCONTROL       1060
//convRes: #define IDS_UI_STAFF            IDS_UI_FIRST+51
#define IDS_UI_STAFF            1061
//convRes: #define IDS_UI_SUBORDINATION    IDS_UI_FIRST+52
#define IDS_UI_SUBORDINATION    1062
//convRes: #define IDS_UI_COMMANDABILITY   IDS_UI_FIRST+53
#define IDS_UI_COMMANDABILITY   1063
//convRes: #define IDS_UI_STRENGTHUNKNOWN  IDS_UI_FIRST+54
#define IDS_UI_STRENGTHUNKNOWN  1064
//convRes: #define IDS_UI_STR              IDS_UI_FIRST+55
#define IDS_UI_STR              1065
//convRes: #define IDS_UI_ACTIVITY         IDS_UI_FIRST+56
#define IDS_UI_ACTIVITY         1066
//convRes: #define IDS_UI_CURRENT          IDS_UI_FIRST+57
#define IDS_UI_CURRENT          1067
//convRes: #define IDS_UI_INROUTE          IDS_UI_FIRST+58
#define IDS_UI_INROUTE          1068
//convRes: #define IDS_UI_ACTIONING        IDS_UI_FIRST+59
#define IDS_UI_ACTIONING        1069
//convRes: #define IDS_UI_SUMMARY          IDS_UI_FIRST+60
#define IDS_UI_SUMMARY          1070
//convRes: #define IDS_UI_OB               IDS_UI_FIRST+61
#define IDS_UI_OB               1071

//convRes: #define IDS_UI_SUPREMELEADER  IDS_UI_FIRST+62
#define IDS_UI_SUPREMELEADER  1072
//convRes: #define IDS_UI_INF                              IDS_UI_FIRST+63
#define IDS_UI_INF                              1073
//convRes: #define IDS_UI_CAV                              IDS_UI_FIRST+64
#define IDS_UI_CAV                              1074
//convRes: #define IDS_UI_NATIONALITY              IDS_UI_FIRST+65
#define IDS_UI_NATIONALITY              1075
//convRes: #define IDS_UI_UNKNOWN                  IDS_UI_FIRST+66
#define IDS_UI_UNKNOWN                  1076
//convRes: #define IDS_UI_CombinedArms             IDS_UI_FIRST+67
#define IDS_UI_CombinedArms             1077
// #define IDS_UI_BaseMorale               IDS_UI_FIRST+68


// #define IDS_UI_LAST            IDS_UI_FIRST+69

// strings for UnitOrderTB tooltips and stat wind
//convRes: #define TTS_FIRST           IDS_FIRST+100
#define TTS_FIRST           1100
// #define TTS_FIRST           IDS_UI_LAST
//convRes: #define TTS_CURRENTUNIT     TTS_FIRST + 0
#define TTS_CURRENTUNIT     1100
//convRes: #define TTS_ORDERS          TTS_FIRST + 1
#define TTS_ORDERS          1101
//convRes: #define TTS_TRACKBAR        TTS_FIRST + 2
#define TTS_TRACKBAR        1102
//convRes: #define TTS_DETACH          TTS_FIRST + 3
#define TTS_DETACH          1103
//convRes: #define TTS_CURRENTORDER    TTS_FIRST + 4
#define TTS_CURRENTORDER    1104
//convRes: #define TTS_UNITMODE        TTS_FIRST + 5
#define TTS_UNITMODE        1105
//convRes: #define TTS_TOWNMODE        TTS_FIRST + 6
#define TTS_TOWNMODE        1106
//convRes: #define TTS_LEADER          TTS_FIRST + 7
#define TTS_LEADER          1107
//convRes: #define TTS_SP              TTS_FIRST + 8
#define TTS_SP              1108
//convRes: #define TTS_SEND            TTS_FIRST + 9
#define TTS_SEND            1109
//convRes: #define TTS_RESTORE         TTS_FIRST + 10
#define TTS_RESTORE         1110
//convRes: #define TTS_UNITINFO        TTS_FIRST + 11
#define TTS_UNITINFO        1111
//convRes: #define TTS_BASICINFO       TTS_FIRST + 12
#define TTS_BASICINFO       1112
//convRes: #define TTS_ADDITIONALINFO  TTS_FIRST + 13
#define TTS_ADDITIONALINFO  1113
//convRes: #define TTS_LAST            TTS_FIRST + 14
#define TTS_LAST            1114
// #define IDS_NEXT IDS_NEXT+14

//convRes: #define SWS_FIRST           IDS_FIRST+120
#define SWS_FIRST           1120
// #define SWS_FIRST           IDS_NEXT	// TTS_LAST
//convRes: #define SWS_CURRENTUNIT     SWS_FIRST + 0
#define SWS_CURRENTUNIT     1120
//convRes: #define SWS_ORDERS          SWS_FIRST + 1
#define SWS_ORDERS          1121
//convRes: #define SWS_TRACKBAR        SWS_FIRST + 2
#define SWS_TRACKBAR        1122
//convRes: #define SWS_DETACH          SWS_FIRST + 3
#define SWS_DETACH          1123
//convRes: #define SWS_CURRENTORDER    SWS_FIRST + 4
#define SWS_CURRENTORDER    1124
//convRes: #define SWS_UNITMODE        SWS_FIRST + 5
#define SWS_UNITMODE        1125
//convRes: #define SWS_TOWNMODE        SWS_FIRST + 6
#define SWS_TOWNMODE        1126
//convRes: #define SWS_LEADER          SWS_FIRST + 7
#define SWS_LEADER          1127
//convRes: #define SWS_SP              SWS_FIRST + 8
#define SWS_SP              1128
//convRes: #define SWS_SEND            SWS_FIRST + 9
#define SWS_SEND            1129
//convRes: #define SWS_RESTORE         SWS_FIRST + 10
#define SWS_RESTORE         1130
//convRes: #define SWS_UNITINFO        SWS_FIRST + 11
#define SWS_UNITINFO        1131
//convRes: #define SWS_BASICINFO       SWS_FIRST + 12
#define SWS_BASICINFO       1132
//convRes: #define SWS_ADDITIONALINFO  SWS_FIRST + 13
#define SWS_ADDITIONALINFO  1133
// #define SWS_LAST            SWS_FIRST + 14

//convRes: #define TTS_FIRST_BW        IDS_FIRST+140
#define TTS_FIRST_BW        1140
// #define TTS_FIRST_BW        SWS_LAST
//convRes: #define TTS_MARCHTO         TTS_FIRST_BW + 0
#define TTS_MARCHTO         1140
//convRes: #define TTS_PURSUE          TTS_FIRST_BW + 1
#define TTS_PURSUE          1141
//convRes: #define TTS_DIGIN           TTS_FIRST_BW + 2
#define TTS_DIGIN           1142
//convRes: #define TTS_SIEGEACTIVE     TTS_FIRST_BW + 3
#define TTS_SIEGEACTIVE     1143
//convRes: #define TTS_AUTOSIEGE       TTS_FIRST_BW + 4
#define TTS_AUTOSIEGE       1144
//convRes: #define TTS_MOVEHOW         TTS_FIRST_BW + 5
#define TTS_MOVEHOW         1145
//convRes: #define TTS_ONARRIVAL       TTS_FIRST_BW + 6
#define TTS_ONARRIVAL       1146
// #define TTS_LAST_BW         TTS_FIRST_BW + 7

// #define SWS_FIRST_BW        TTS_LAST_BW
//convRes: #define SWS_FIRST_BW        IDS_FIRST+150
#define SWS_FIRST_BW        1150
//convRes: #define SWS_MARCHTO         SWS_FIRST_BW + 0
#define SWS_MARCHTO         1150
//convRes: #define SWS_PURSUE          SWS_FIRST_BW + 1
#define SWS_PURSUE          1151
//convRes: #define SWS_DIGIN           SWS_FIRST_BW + 2
#define SWS_DIGIN           1152
//convRes: #define SWS_SIEGEACTIVE     SWS_FIRST_BW + 3
#define SWS_SIEGEACTIVE     1153
//convRes: #define SWS_AUTOSIEGE       SWS_FIRST_BW + 4
#define SWS_AUTOSIEGE       1154
//convRes: #define SWS_MOVEHOW         SWS_FIRST_BW + 5
#define SWS_MOVEHOW         1155
//convRes: #define SWS_ONARRIVAL       SWS_FIRST_BW + 6
#define SWS_ONARRIVAL       1156
// #define SWS_LAST_BW         SWS_FIRST_BW + 7

// #define TTS_UD_FIRST_AG     SWS_LAST_BW
//convRes: #define TTS_UD_FIRST_AG     IDS_FIRST+160
#define TTS_UD_FIRST_AG     1160
//convRes: #define TTS_UD_TIMID        TTS_UD_FIRST_AG + 0
#define TTS_UD_TIMID        1160
//convRes: #define TTS_UD_DEFEND       TTS_UD_FIRST_AG + 1
#define TTS_UD_DEFEND       1161
//convRes: #define TTS_UD_OFFENSIVE    TTS_UD_FIRST_AG + 2
#define TTS_UD_OFFENSIVE    1162
//convRes: #define TTS_UD_AGGRESSIVE   TTS_UD_FIRST_AG + 3
#define TTS_UD_AGGRESSIVE   1163
// #define TTS_UD_LAST_AG      TTS_UD_FIRST_AG + 4

// #define SWS_UD_FIRST_AG     TTS_UD_LAST_AG
//convRes: #define SWS_UD_FIRST_AG     IDS_FIRST+170
#define SWS_UD_FIRST_AG     1170
//convRes: #define SWS_UD_TIMID        SWS_UD_FIRST_AG + 0
#define SWS_UD_TIMID        1170
//convRes: #define SWS_UD_DEFEND       SWS_UD_FIRST_AG + 1
#define SWS_UD_DEFEND       1171
//convRes: #define SWS_UD_OFFENSIVE    SWS_UD_FIRST_AG + 2
#define SWS_UD_OFFENSIVE    1172
//convRes: #define SWS_UD_AGGRESSIVE   SWS_UD_FIRST_AG + 3
#define SWS_UD_AGGRESSIVE   1173
// #define SWS_UD_LAST_AG      SWS_UD_FIRST_AG + 4

        // tooltip & statwind strings for orders popupicon
// #define TTS_UD_FIRST_PUI    SWS_UD_LAST_AG
//convRes: #define TTS_UD_FIRST_PUI    IDS_FIRST+180
#define TTS_UD_FIRST_PUI    1180
//convRes: #define TTS_UD_MOVETO       TTS_UD_FIRST_PUI    + 0
#define TTS_UD_MOVETO       1180
//convRes: #define TTS_UD_HOLD         TTS_UD_FIRST_PUI + 1
#define TTS_UD_HOLD         1181
//convRes: #define TTS_UD_REST_RALLY   TTS_UD_FIRST_PUI + 3
#define TTS_UD_REST_RALLY   1183
//convRes: #define TTS_UD_ROUT         TTS_UD_FIRST_PUI + 4
#define TTS_UD_ROUT         1184
//convRes: #define TTS_UD_ATTACH       TTS_UD_FIRST_PUI + 5
#define TTS_UD_ATTACH       1185
//convRes: #define TTS_UD_ATTACHSP     TTS_UD_FIRST_PUI + 6
#define TTS_UD_ATTACHSP     1186
//convRes: #define TTS_UD_LEADER       TTS_UD_FIRST_PUI + 7
#define TTS_UD_LEADER       1187
//convRes: #define TTS_UD_GARRISON     TTS_UD_FIRST_PUI + 8
#define TTS_UD_GARRISON     1188
//convRes: #define TTS_UD_SURRENDER    TTS_UD_FIRST_PUI + 9
#define TTS_UD_SURRENDER    1189
//convRes: #define TTS_UD_STORM        TTS_UD_FIRST_PUI + 10
#define TTS_UD_STORM        1190
//convRes: #define TTS_UD_SALLY        TTS_UD_FIRST_PUI + 11
#define TTS_UD_SALLY        1191
//convRes: #define TTS_UD_INSTALLSUP   TTS_UD_FIRST_PUI + 12
#define TTS_UD_INSTALLSUP   1192
//convRes: #define TTS_UD_UPGRADEFORT  TTS_UD_FIRST_PUI + 13
#define TTS_UD_UPGRADEFORT  1193
// #define TTS_UD_LAST_PUI     TTS_UD_FIRST_PUI + 14

// #define SWS_UD_FIRST_PUI    TTS_UD_LAST_PUI
//convRes: #define SWS_UD_FIRST_PUI    IDS_FIRST+200
#define SWS_UD_FIRST_PUI    1200
//convRes: #define SWS_UD_MOVETO       SWS_UD_FIRST_PUI    + 0
#define SWS_UD_MOVETO       1200
//convRes: #define SWS_UD_HOLD         SWS_UD_FIRST_PUI + 1
#define SWS_UD_HOLD         1201
//convRes: #define SWS_UD_REST_RALLY   SWS_UD_FIRST_PUI + 3
#define SWS_UD_REST_RALLY   1203
//convRes: #define SWS_UD_ROUT         SWS_UD_FIRST_PUI + 4
#define SWS_UD_ROUT         1204
//convRes: #define SWS_UD_ATTACH       SWS_UD_FIRST_PUI + 5
#define SWS_UD_ATTACH       1205
//convRes: #define SWS_UD_ATTACHSP     SWS_UD_FIRST_PUI + 6
#define SWS_UD_ATTACHSP     1206
//convRes: #define SWS_UD_LEADER       SWS_UD_FIRST_PUI + 7
#define SWS_UD_LEADER       1207
//convRes: #define SWS_UD_GARRISON     SWS_UD_FIRST_PUI + 8
#define SWS_UD_GARRISON     1208
//convRes: #define SWS_UD_SURRENDER    SWS_UD_FIRST_PUI + 9
#define SWS_UD_SURRENDER    1209
//convRes: #define SWS_UD_SIEGE        SWS_UD_FIRST_PUI + 10
#define SWS_UD_SIEGE        1210
//convRes: #define SWS_UD_BREAKSIEGE   SWS_UD_FIRST_PUI + 11
#define SWS_UD_BREAKSIEGE   1211
//convRes: #define SWS_UD_STORM        SWS_UD_FIRST_PUI + 12
#define SWS_UD_STORM        1212
//convRes: #define SWS_UD_SALLY        SWS_UD_FIRST_PUI + 13
#define SWS_UD_SALLY        1213
//convRes: #define SWS_UD_INSTALLSUP   SWS_UD_FIRST_PUI + 14
#define SWS_UD_INSTALLSUP   1214
//convRes: #define SWS_UD_UPGRADEFORT  SWS_UD_FIRST_PUI + 15
#define SWS_UD_UPGRADEFORT  1215
// #define SWS_UD_LAST_PUI     SWS_UD_FIRST_PUI + 16

//strings for SPDial Tooltips and statwind

// #define TTS_SP_FIRST             SWS_UD_LAST_PUI                // AO + 1
//convRes: #define TTS_SP_FIRST             IDS_FIRST+220
#define TTS_SP_FIRST             1220
//convRes: #define TTS_SP_ATTACHSPTO        TTS_SP_FIRST + 0
#define TTS_SP_ATTACHSPTO        1220
//convRes: #define TTS_SP_ATTACHSPFROM      TTS_SP_FIRST + 1
#define TTS_SP_ATTACHSPFROM      1221
//convRes: #define TTS_SP_ATTACHSPTO_BTN    TTS_SP_FIRST + 2
#define TTS_SP_ATTACHSPTO_BTN    1222
//convRes: #define TTS_SP_ATTACHSPFROM_BTN  TTS_SP_FIRST + 3
#define TTS_SP_ATTACHSPFROM_BTN  1223
//convRes: #define TTS_SP_SEND              TTS_SP_FIRST + 4
#define TTS_SP_SEND              1224
//convRes: #define TTS_SP_SPFROM            TTS_SP_FIRST + 5
#define TTS_SP_SPFROM            1225
//convRes: #define TTS_SP_SPTO              TTS_SP_FIRST + 6
#define TTS_SP_SPTO              1226
// #define TTS_SP_LAST              TTS_SP_FIRST + 7               // TTS_SP_SPTO

// #define SWS_SP_FIRST             TTS_SP_LAST
//convRes: #define SWS_SP_FIRST             IDS_FIRST+230
#define SWS_SP_FIRST             1230
//convRes: #define SWS_SP_ATTACHSPTO        SWS_SP_FIRST + 0
#define SWS_SP_ATTACHSPTO        1230
//convRes: #define SWS_SP_ATTACHSPFROM      SWS_SP_FIRST + 1
#define SWS_SP_ATTACHSPFROM      1231
//convRes: #define SWS_SP_ATTACHSPTO_BTN    SWS_SP_FIRST + 2
#define SWS_SP_ATTACHSPTO_BTN    1232
//convRes: #define SWS_SP_ATTACHSPFROM_BTN  SWS_SP_FIRST + 3
#define SWS_SP_ATTACHSPFROM_BTN  1233
//convRes: #define SWS_SP_SEND              SWS_SP_FIRST + 4
#define SWS_SP_SEND              1234
//convRes: #define SWS_SP_SPFROM            SWS_SP_FIRST + 5
#define SWS_SP_SPFROM            1235
//convRes: #define SWS_SP_SPTO              SWS_SP_FIRST + 6
#define SWS_SP_SPTO              1236
// #define SWS_SP_LAST              SWS_SP_FIRST + 7

/*
 * TownDial
 */

// tooltip & statwind string id's
// #define TTS_IDTC_FIRST         SWS_SP_LAST              // SWS_IDMW_LAST
//convRes: #define TTS_IDTC_FIRST         IDS_FIRST+240
#define TTS_IDTC_FIRST         1240
//convRes: #define TTS_IDTC_PROGRESS      TTS_IDTC_FIRST + 0
#define TTS_IDTC_PROGRESS      1240
//convRes: #define TTS_IDTC_INFANTRY      TTS_IDTC_FIRST + 1
#define TTS_IDTC_INFANTRY      1241
//convRes: #define TTS_IDTC_CAVALRY       TTS_IDTC_FIRST + 2
#define TTS_IDTC_CAVALRY       1242
//convRes: #define TTS_IDTC_ARTILLERY     TTS_IDTC_FIRST + 3
#define TTS_IDTC_ARTILLERY     1243
//convRes: #define TTS_IDTC_SPECIAL       TTS_IDTC_FIRST + 4
#define TTS_IDTC_SPECIAL       1244
//convRes: #define TTS_IDTC_ENABLE        TTS_IDTC_FIRST + 5
#define TTS_IDTC_ENABLE        1245
//convRes: #define TTS_IDTC_AUTO          TTS_IDTC_FIRST + 6
#define TTS_IDTC_AUTO          1246
//convRes: #define TTS_IDTC_BUILD_WHAT    TTS_IDTC_FIRST + 7
#define TTS_IDTC_BUILD_WHAT    1247
//convRes: #define TTS_IDTC_PRIORITY      TTS_IDTC_FIRST + 8
#define TTS_IDTC_PRIORITY      1248
//convRes: #define TTS_IDTC_QUALITY       TTS_IDTC_FIRST + 9
#define TTS_IDTC_QUALITY       1249
//convRes: #define TTS_IDTC_APPLY         TTS_IDTC_FIRST + 10
#define TTS_IDTC_APPLY         1250
//convRes: #define TTS_IDTC_UNDO          TTS_IDTC_FIRST + 11
#define TTS_IDTC_UNDO          1251
// #define TTS_IDTC_LAST          TTS_IDTC_FIRST + 12

// #define SWS_IDTC_FIRST         TTS_IDTC_LAST
//convRes: #define SWS_IDTC_FIRST         IDS_FIRST+260
#define SWS_IDTC_FIRST         1260
//convRes: #define SWS_IDTC_PROGRESS      SWS_IDTC_FIRST + 0
#define SWS_IDTC_PROGRESS      1260
//convRes: #define SWS_IDTC_INFANTRY      SWS_IDTC_FIRST + 1
#define SWS_IDTC_INFANTRY      1261
//convRes: #define SWS_IDTC_CAVALRY       SWS_IDTC_FIRST + 2
#define SWS_IDTC_CAVALRY       1262
//convRes: #define SWS_IDTC_ARTILLERY     SWS_IDTC_FIRST + 3
#define SWS_IDTC_ARTILLERY     1263
//convRes: #define SWS_IDTC_SPECIAL       SWS_IDTC_FIRST + 4
#define SWS_IDTC_SPECIAL       1264
//convRes: #define SWS_IDTC_ENABLE        SWS_IDTC_FIRST + 5
#define SWS_IDTC_ENABLE        1265
//convRes: #define SWS_IDTC_AUTO          SWS_IDTC_FIRST + 6
#define SWS_IDTC_AUTO          1266
//convRes: #define SWS_IDTC_BUILD_WHAT    SWS_IDTC_FIRST + 7
#define SWS_IDTC_BUILD_WHAT    1267
//convRes: #define SWS_IDTC_PRIORITY      SWS_IDTC_FIRST + 8
#define SWS_IDTC_PRIORITY      1268
//convRes: #define SWS_IDTC_QUALITY       SWS_IDTC_FIRST + 9
#define SWS_IDTC_QUALITY       1269
//convRes: #define SWS_IDTC_APPLY         SWS_IDTC_FIRST + 10
#define SWS_IDTC_APPLY         1270
//convRes: #define SWS_IDTC_UNDO          SWS_IDTC_FIRST + 11
#define SWS_IDTC_UNDO          1271
// #define SWS_IDTC_LAST          SWS_IDTC_FIRST + 12

// string id's for FTWindow  tooltips and statwind
//convRes: #define TTS_FTI_FIRST            IDS_FIRST+280
#define TTS_FTI_FIRST            1280
// #define TTS_FTI_FIRST            SWS_IDTC_LAST          // SP_LAST + 1
// #define TTS_FTI_RESOURCE_MODE    TTS_FTI_FIRST + 0
// #define TTS_FTI_UNIT_MODE        TTS_FTI_FIRST + 1
//convRes: #define TTS_FTI_ZOOMIN           TTS_FTI_FIRST + 2
#define TTS_FTI_ZOOMIN           1282
//convRes: #define TTS_FTI_ZOOMOUT          TTS_FTI_FIRST + 3
#define TTS_FTI_ZOOMOUT          1283
//convRes: #define TTS_FTI_ZOOM0            TTS_FTI_FIRST + 4
#define TTS_FTI_ZOOM0            1284
//convRes: #define TTS_FTI_ZOOM1            TTS_FTI_FIRST + 5
#define TTS_FTI_ZOOM1            1285
//convRes: #define TTS_FTI_ZOOM2            TTS_FTI_FIRST + 6
#define TTS_FTI_ZOOM2            1286
//convRes: #define TTS_FTI_ZOOM3            TTS_FTI_FIRST + 7
#define TTS_FTI_ZOOM3            1287
//convRes: #define TTS_FTI_ZOOM4            TTS_FTI_FIRST + 8
#define TTS_FTI_ZOOM4            1288
//convRes: #define TTS_FTI_TINYMAP          TTS_FTI_FIRST + 9
#define TTS_FTI_TINYMAP          1289
//convRes: #define TTS_FTI_INFOWIND         TTS_FTI_FIRST + 10
#define TTS_FTI_INFOWIND         1290
// #define TTS_FTI_LAST             TTS_FTI_FIRST + 11

// #define SWS_FTI_FIRST            TTS_FTI_LAST
//convRes: #define SWS_FTI_FIRST            IDS_FIRST+300
#define SWS_FTI_FIRST            1300
// #define SWS_FTI_RESOURCE_MODE    SWS_FTI_FIRST + 0
// #define SWS_FTI_UNIT_MODE        SWS_FTI_FIRST + 1
//convRes: #define SWS_FTI_ZOOMIN           SWS_FTI_FIRST + 2
#define SWS_FTI_ZOOMIN           1302
//convRes: #define SWS_FTI_ZOOMOUT          SWS_FTI_FIRST + 3
#define SWS_FTI_ZOOMOUT          1303
//convRes: #define SWS_FTI_ZOOM0            SWS_FTI_FIRST + 4
#define SWS_FTI_ZOOM0            1304
//convRes: #define SWS_FTI_ZOOM1            SWS_FTI_FIRST + 5
#define SWS_FTI_ZOOM1            1305
//convRes: #define SWS_FTI_ZOOM2            SWS_FTI_FIRST + 6
#define SWS_FTI_ZOOM2            1306
//convRes: #define SWS_FTI_ZOOM3            SWS_FTI_FIRST + 7
#define SWS_FTI_ZOOM3            1307
//convRes: #define SWS_FTI_ZOOM4            SWS_FTI_FIRST + 8
#define SWS_FTI_ZOOM4            1308
//convRes: #define SWS_FTI_TINYMAP          SWS_FTI_FIRST + 9
#define SWS_FTI_TINYMAP          1309
//convRes: #define SWS_FTI_INFOWIND         SWS_FTI_FIRST + 10
#define SWS_FTI_INFOWIND         1310
// #define SWS_FTI_LAST             SWS_FTI_FIRST + 11

// string ids for clock dialog tooltips & statwind
// #define TTS_CC_FIRST          SWS_FTI_LAST
//convRes: #define TTS_CC_FIRST          IDS_FIRST+320
#define TTS_CC_FIRST          1320
//convRes: #define TTS_CC_SPEED          TTS_CC_FIRST + 0
#define TTS_CC_SPEED          1320
//convRes: #define TTS_CC_PAUSE          TTS_CC_FIRST + 1
#define TTS_CC_PAUSE          1321
//convRes: #define TTS_CC_DATE1          TTS_CC_FIRST + 2
#define TTS_CC_DATE1          1322
//convRes: #define TTS_CC_DATE2          TTS_CC_FIRST + 3
#define TTS_CC_DATE2          1323
//convRes: #define TTS_CC_DATE3          TTS_CC_FIRST + 4
#define TTS_CC_DATE3          1324
//convRes: #define TTS_CC_DATE4          TTS_CC_FIRST + 5
#define TTS_CC_DATE4          1325
//convRes: #define TTS_CC_DATE5          TTS_CC_FIRST + 6
#define TTS_CC_DATE5          1326
//convRes: #define TTS_CC_DATE6          TTS_CC_FIRST + 7
#define TTS_CC_DATE6          1327
//convRes: #define TTS_CC_DATE7          TTS_CC_FIRST + 8
#define TTS_CC_DATE7          1328
//convRes: #define TTS_CC_DATE8          TTS_CC_FIRST + 9
#define TTS_CC_DATE8          1329
//convRes: #define TTS_CC_DATE9          TTS_CC_FIRST + 10
#define TTS_CC_DATE9          1330
//convRes: #define TTS_CC_DATE10         TTS_CC_FIRST + 11
#define TTS_CC_DATE10         1331
//convRes: #define TTS_CC_DATE11         TTS_CC_FIRST + 12
#define TTS_CC_DATE11         1332
//convRes: #define TTS_CC_DATE12         TTS_CC_FIRST + 13
#define TTS_CC_DATE12         1333
//convRes: #define TTS_CC_DATE13         TTS_CC_FIRST + 14
#define TTS_CC_DATE13         1334
//convRes: #define TTS_CC_DATE14         TTS_CC_FIRST + 15
#define TTS_CC_DATE14         1335
// #define TTS_CC_LAST           TTS_CC_FIRST + 16

// #define SWS_CC_FIRST          TTS_CC_LAST
//convRes: #define SWS_CC_FIRST          IDS_FIRST+340
#define SWS_CC_FIRST          1340
//convRes: #define SWS_CC_SPEED          SWS_CC_FIRST + 0
#define SWS_CC_SPEED          1340
//convRes: #define SWS_CC_PAUSE          SWS_CC_FIRST + 1
#define SWS_CC_PAUSE          1341
//convRes: #define SWS_CC_DATE1          SWS_CC_FIRST + 2
#define SWS_CC_DATE1          1342
//convRes: #define SWS_CC_DATE2          SWS_CC_FIRST + 3
#define SWS_CC_DATE2          1343
//convRes: #define SWS_CC_DATE3          SWS_CC_FIRST + 4
#define SWS_CC_DATE3          1344
//convRes: #define SWS_CC_DATE4          SWS_CC_FIRST + 5
#define SWS_CC_DATE4          1345
//convRes: #define SWS_CC_DATE5          SWS_CC_FIRST + 6
#define SWS_CC_DATE5          1346
//convRes: #define SWS_CC_DATE6          SWS_CC_FIRST + 7
#define SWS_CC_DATE6          1347
//convRes: #define SWS_CC_DATE7          SWS_CC_FIRST + 8
#define SWS_CC_DATE7          1348
//convRes: #define SWS_CC_DATE8          SWS_CC_FIRST + 9
#define SWS_CC_DATE8          1349
//convRes: #define SWS_CC_DATE9          SWS_CC_FIRST + 10
#define SWS_CC_DATE9          1350
//convRes: #define SWS_CC_DATE10         SWS_CC_FIRST + 11
#define SWS_CC_DATE10         1351
//convRes: #define SWS_CC_DATE11         SWS_CC_FIRST + 12
#define SWS_CC_DATE11         1352
//convRes: #define SWS_CC_DATE12         SWS_CC_FIRST + 13
#define SWS_CC_DATE12         1353
//convRes: #define SWS_CC_DATE13         SWS_CC_FIRST + 14
#define SWS_CC_DATE13         1354
//convRes: #define SWS_CC_DATE14         SWS_CC_FIRST + 15
#define SWS_CC_DATE14         1355
// #define SWS_CC_LAST           SWS_CC_FIRST + 16

// string id's for MessageWindow tootips & statwind

// #define TTS_IDMW_FIRST     SWS_CC_LAST
//convRes: #define TTS_IDMW_FIRST     IDS_FIRST+360
#define TTS_IDMW_FIRST     1360
//convRes: #define TTS_IDMW_PREV      TTS_IDMW_FIRST + 0
#define TTS_IDMW_PREV      1360
//convRes: #define TTS_IDMW_NEXT      TTS_IDMW_FIRST + 1
#define TTS_IDMW_NEXT      1361
//convRes: #define TTS_IDMW_DELETE    TTS_IDMW_FIRST + 2
#define TTS_IDMW_DELETE    1362
//convRes: #define TTS_IDMW_ORDER     TTS_IDMW_FIRST + 3
#define TTS_IDMW_ORDER     1363
//convRes: #define TTS_IDMW_DELETEALL TTS_IDMW_FIRST + 4
#define TTS_IDMW_DELETEALL 1364
//convRes: #define TTS_IDMW_SETTINGS  TTS_IDMW_FIRST + 5
#define TTS_IDMW_SETTINGS  1365
//convRes: #define TTS_IDMW_NOSHOW    TTS_IDMW_FIRST + 6
#define TTS_IDMW_NOSHOW    1366
//convRes: #define TTS_IDMW_LIST      TTS_IDMW_FIRST + 7
#define TTS_IDMW_LIST      1367
// #define TTS_IDMW_LAST      TTS_IDMW_FIRST + 8

// #define SWS_IDMW_FIRST     TTS_IDMW_LAST
//convRes: #define SWS_IDMW_FIRST     IDS_FIRST+370
#define SWS_IDMW_FIRST     1370
//convRes: #define SWS_IDMW_PREV      SWS_IDMW_FIRST + 0
#define SWS_IDMW_PREV      1370
//convRes: #define SWS_IDMW_NEXT      SWS_IDMW_FIRST + 1
#define SWS_IDMW_NEXT      1371
//convRes: #define SWS_IDMW_DELETE    SWS_IDMW_FIRST + 2
#define SWS_IDMW_DELETE    1372
//convRes: #define SWS_IDMW_ORDER     SWS_IDMW_FIRST + 3
#define SWS_IDMW_ORDER     1373
//convRes: #define SWS_IDMW_DELETEALL SWS_IDMW_FIRST + 4
#define SWS_IDMW_DELETEALL 1374
//convRes: #define SWS_IDMW_SETTINGS  SWS_IDMW_FIRST + 5
#define SWS_IDMW_SETTINGS  1375
//convRes: #define SWS_IDMW_NOSHOW    SWS_IDMW_FIRST + 6
#define SWS_IDMW_NOSHOW    1376
//convRes: #define SWS_IDMW_LIST      SWS_IDMW_FIRST + 7
#define SWS_IDMW_LIST      1377
// #define SWS_IDMW_LAST      SWS_IDMW_FIRST + 8

// #define TTS_UD_FIRST_AO          SWS_IDMW_LAST
//convRes: #define TTS_UD_FIRST_AO          IDS_FIRST+380
#define TTS_UD_FIRST_AO          1380
//convRes: #define TTS_UD_NORMALMARCH       TTS_UD_FIRST_AO + 0
#define TTS_UD_NORMALMARCH       1380
//convRes: #define TTS_UD_EASYMARCH         TTS_UD_FIRST_AO + 1
#define TTS_UD_EASYMARCH         1381
//convRes: #define TTS_UD_FORCEMARCH        TTS_UD_FIRST_AO + 2
#define TTS_UD_FORCEMARCH        1382
//convRes: #define TTS_UD_HOLDONARRIVAL     TTS_UD_FIRST_AO + 3
#define TTS_UD_HOLDONARRIVAL     1383
//convRes: #define TTS_UD_RESTONARRIVAL     TTS_UD_FIRST_AO + 4
#define TTS_UD_RESTONARRIVAL     1384
//convRes: #define TTS_UD_GARRISONONARRIVAL TTS_UD_FIRST_AO + 5
#define TTS_UD_GARRISONONARRIVAL 1385
// #define TTS_UD_LAST_AO           TTS_UD_FIRST_AO + 6

// #define SWS_UD_FIRST_AO          TTS_UD_LAST_AO
//convRes: #define SWS_UD_FIRST_AO          IDS_FIRST+390
#define SWS_UD_FIRST_AO          1390
//convRes: #define SWS_UD_NORMALMARCH       SWS_UD_FIRST_AO + 0
#define SWS_UD_NORMALMARCH       1390
//convRes: #define SWS_UD_EASYMARCH         SWS_UD_FIRST_AO + 1
#define SWS_UD_EASYMARCH         1391
//convRes: #define SWS_UD_FORCEMARCH        SWS_UD_FIRST_AO + 2
#define SWS_UD_FORCEMARCH        1392
//convRes: #define SWS_UD_HOLDONARRIVAL     SWS_UD_FIRST_AO + 3
#define SWS_UD_HOLDONARRIVAL     1393
//convRes: #define SWS_UD_RESTONARRIVAL     SWS_UD_FIRST_AO + 4
#define SWS_UD_RESTONARRIVAL     1394
//convRes: #define SWS_UD_GARRISONONARRIVAL SWS_UD_FIRST_AO + 5
#define SWS_UD_GARRISONONARRIVAL 1395
// #define SWS_UD_LAST_AO           SWS_UD_FIRST_AO + 6

/*
 * String id's for WeatherWindow
 */

/* #define TTS_WW_FIRST             SWS_UD_LAST_AO */
/* #define TTS_WW_CLEAR             TTS_WW_FIRST+1 */
/* #define TTS_WW_LIGHTRAIN         TTS_WW_FIRST+2 */
/* #define TTS_WW_HEAVYRAIN         TTS_WW_FIRST+3 */
/* #define TTS_WW_DRY               TTS_WW_FIRST+4 */
/* #define TTS_WW_MUDDY             TTS_WW_FIRST+5 */
/* #define TTS_WW_VERYMUDDY         TTS_WW_FIRST+6 */
/* #define TTS_WW_LAST              TTS_WW_FIRST+7 */

// #define IDS_WEATHER              SWS_UD_LAST_AO
//convRes: #define IDS_WEATHER              IDS_FIRST+400
#define IDS_WEATHER              1400
//convRes: #define IDS_WEATHER_1            IDS_WEATHER + 0
#define IDS_WEATHER_1            1400
//convRes: #define IDS_WEATHER_2            IDS_WEATHER + 1
#define IDS_WEATHER_2            1401
//convRes: #define IDS_WEATHER_3            IDS_WEATHER + 2
#define IDS_WEATHER_3            1402

//convRes: #define IDS_GROUNDCONDITION_1    IDS_WEATHER + 3
#define IDS_GROUNDCONDITION_1    1403
//convRes: #define IDS_GROUNDCONDITION_2    IDS_WEATHER + 4
#define IDS_GROUNDCONDITION_2    1404
//convRes: #define IDS_GROUNDCONDITION_3    IDS_WEATHER + 5
#define IDS_GROUNDCONDITION_3    1405

/* #define SWS_WW_FIRST             TTS_WW_LAST    */
/* #define SWS_WW_CLEAR             SWS_WW_FIRST+1 */
/* #define SWS_WW_LIGHTRAIN         SWS_WW_FIRST+2 */
/* #define SWS_WW_HEAVYRAIN         SWS_WW_FIRST+3 */
/* #define SWS_WW_DRY               SWS_WW_FIRST+4 */
/* #define SWS_WW_MUDDY             SWS_WW_FIRST+5 */
/* #define SWS_WW_VERYMUDDY         SWS_WW_FIRST+6 */
/* #define SWS_WW_LAST              SWS_WW_FIRST+7 */


// campaign message string ids
// #define IDS_CMSG_FIRST              IDS_WEATHER + 6
//convRes: #define IDS_CMSG_FIRST              	IDS_FIRST+500
#define IDS_CMSG_FIRST              	1500
//convRes: #define IDS_CMSG_ArrivedAtTown      	IDS_CMSG_FIRST+0
#define IDS_CMSG_ArrivedAtTown      	1500
//convRes: #define IDS_CMSG_NewUnitBuilt       	IDS_CMSG_FIRST+1
#define IDS_CMSG_NewUnitBuilt       	1501
//convRes: #define IDS_CMSG_MetAtAndAwait      	IDS_CMSG_FIRST+2
#define IDS_CMSG_MetAtAndAwait      	1502
//convRes: #define IDS_CMSG_TakenCommand       	IDS_CMSG_FIRST+3
#define IDS_CMSG_TakenCommand       	1503
//convRes: #define IDS_CMSG_MetAndAwait        	IDS_CMSG_FIRST+4
#define IDS_CMSG_MetAndAwait        	1504
//convRes: #define IDS_CMSG_Stranded           	IDS_CMSG_FIRST+5
#define IDS_CMSG_Stranded           	1505
//convRes: #define IDS_CMSG_MovingInForBattle  	IDS_CMSG_FIRST+6
#define IDS_CMSG_MovingInForBattle  	1506
//convRes: #define IDS_CMSG_HoldingBackAt8     	IDS_CMSG_FIRST+7
#define IDS_CMSG_HoldingBackAt8     	1507
//convRes: #define IDS_CMSG_AvoidingContact    	IDS_CMSG_FIRST+8
#define IDS_CMSG_AvoidingContact    	1508
//convRes: #define IDS_CMSG_PreparingForBattle 	IDS_CMSG_FIRST+9
#define IDS_CMSG_PreparingForBattle 	1509
//convRes: #define IDS_CMSG_JoiningBattle      	IDS_CMSG_FIRST+10
#define IDS_CMSG_JoiningBattle      	1510
//convRes: #define IDS_CMSG_EngagingInBattle   	IDS_CMSG_FIRST+11
#define IDS_CMSG_EngagingInBattle   	1511
//convRes: #define IDS_CMSG_GeneralKilled         IDS_CMSG_FIRST+12
#define IDS_CMSG_GeneralKilled         1512
//convRes: #define IDS_CMSG_UnitDestroyed         IDS_CMSG_FIRST+13
#define IDS_CMSG_UnitDestroyed         1513
//convRes: #define IDS_CMSG_CannotCommand         IDS_CMSG_FIRST+14
#define IDS_CMSG_CannotCommand         1514
//convRes: #define IDS_CMSG_NotNoble              IDS_CMSG_FIRST+15
#define IDS_CMSG_NotNoble              1515
//convRes: #define IDS_CMSG_TownTaken             IDS_CMSG_FIRST+16
#define IDS_CMSG_TownTaken             1516
//convRes: #define IDS_CMSG_TownLost              IDS_CMSG_FIRST+17
#define IDS_CMSG_TownLost              1517
//convRes: #define IDS_CMSG_HearBattle            IDS_CMSG_FIRST+18
#define IDS_CMSG_HearBattle            1518
//convRes: #define IDS_CMSG_EncounteredEnemy      IDS_CMSG_FIRST+19
#define IDS_CMSG_EncounteredEnemy      1519
//convRes: #define IDS_CMSG_ChangedAggression     IDS_CMSG_FIRST+20
#define IDS_CMSG_ChangedAggression     1520
//convRes: #define IDS_CMSG_MortarShellExploded   IDS_CMSG_FIRST+21
#define IDS_CMSG_MortarShellExploded   1521
//convRes: #define IDS_CMSG_PoorConstruction      IDS_CMSG_FIRST+22
#define IDS_CMSG_PoorConstruction      1522
//convRes: #define IDS_CMSG_BreachedFort          IDS_CMSG_FIRST+23
#define IDS_CMSG_BreachedFort          1523
//convRes: #define IDS_CMSG_FortBreached          IDS_CMSG_FIRST+24
#define IDS_CMSG_FortBreached          1524
//convRes: #define IDS_CMSG_TownSurrender         IDS_CMSG_FIRST+25
#define IDS_CMSG_TownSurrender         1525
//convRes: #define IDS_CMSG_FortDestroyed         IDS_CMSG_FIRST+26
#define IDS_CMSG_FortDestroyed         1526
//convRes: #define IDS_CMSG_BrokenSiege           IDS_CMSG_FIRST+27
#define IDS_CMSG_BrokenSiege           1527
//convRes: #define IDS_CMSG_SiegeEnded            IDS_CMSG_FIRST+28
#define IDS_CMSG_SiegeEnded            1528
//convRes: #define IDS_CMSG_StartSiege            IDS_CMSG_FIRST+29
#define IDS_CMSG_StartSiege            1529
//convRes: #define IDS_CMSG_SiegeStarted          IDS_CMSG_FIRST+30
#define IDS_CMSG_SiegeStarted          1530
//convRes: #define IDS_CMSG_AttemptedSiege        IDS_CMSG_FIRST+31
#define IDS_CMSG_AttemptedSiege        1531
//convRes: #define IDS_CMSG_SiegeAttempted        IDS_CMSG_FIRST+32
#define IDS_CMSG_SiegeAttempted        1532
//convRes: #define IDS_CMSG_RaidingTown           IDS_CMSG_FIRST+33
#define IDS_CMSG_RaidingTown           1533
//convRes: #define IDS_CMSG_TownRaided            IDS_CMSG_FIRST+34
#define IDS_CMSG_TownRaided            1534
//convRes: #define IDS_CMSG_NotInstallSupply      IDS_CMSG_FIRST+35
#define IDS_CMSG_NotInstallSupply      1535
//convRes: #define IDS_CMSG_NotInstallSupplyDead  IDS_CMSG_FIRST+36
#define IDS_CMSG_NotInstallSupplyDead  1536
//convRes: #define IDS_CMSG_SupplyDepotInstalled  IDS_CMSG_FIRST+37
#define IDS_CMSG_SupplyDepotInstalled  1537
//convRes: #define IDS_CMSG_NotInstallFort        IDS_CMSG_FIRST+38
#define IDS_CMSG_NotInstallFort        1538
//convRes: #define IDS_CMSG_NotInstallFortDead    IDS_CMSG_FIRST+39
#define IDS_CMSG_NotInstallFortDead    1539
//convRes: #define IDS_CMSG_FortUpgraded          IDS_CMSG_FIRST+40
#define IDS_CMSG_FortUpgraded          1540
//convRes: #define IDS_CMSG_EngineerNotFieldWork  IDS_CMSG_FIRST+41
#define IDS_CMSG_EngineerNotFieldWork  1541
//convRes: #define IDS_CMSG_FieldWorkUpgraded     IDS_CMSG_FIRST+42
#define IDS_CMSG_FieldWorkUpgraded     1542
//convRes: #define IDS_CMSG_FieldWorkComplete     IDS_CMSG_FIRST+43
#define IDS_CMSG_FieldWorkComplete     1543
//convRes: #define IDS_CMSG_BridgeCollapse        IDS_CMSG_FIRST+44
#define IDS_CMSG_BridgeCollapse        1544
//convRes: #define IDS_CMSG_TownFire              IDS_CMSG_FIRST+45
#define IDS_CMSG_TownFire              1545
//convRes: #define IDS_CMSG_LeaderSick            IDS_CMSG_FIRST+46
#define IDS_CMSG_LeaderSick            1546
//convRes: #define IDS_CMSG_LeaderRecovered       IDS_CMSG_FIRST+47
#define IDS_CMSG_LeaderRecovered       1547
//convRes: #define IDS_CMSG_AlliedDisillusion     IDS_CMSG_FIRST+48
#define IDS_CMSG_AlliedDisillusion     1548
//convRes: #define IDS_CMSG_SHQInChaos            IDS_CMSG_FIRST+49
#define IDS_CMSG_SHQInChaos            1549
//convRes: #define IDS_CMSG_EnemyPlansCaptured    IDS_CMSG_FIRST+50
#define IDS_CMSG_EnemyPlansCaptured    1550
//convRes: #define IDS_CMSG_SquabblingLeaders     IDS_CMSG_FIRST+51
#define IDS_CMSG_SquabblingLeaders     1551
//convRes: #define IDS_CMSG_PopularEnthusiasm     IDS_CMSG_FIRST+52
#define IDS_CMSG_PopularEnthusiasm     1552
//convRes: #define IDS_CMSG_ArmisticeBegun        IDS_CMSG_FIRST+53
#define IDS_CMSG_ArmisticeBegun        1553
//convRes: #define IDS_CMSG_ArmisticeEnded        IDS_CMSG_FIRST+54
#define IDS_CMSG_ArmisticeEnded        1554


// #define IDS_CMSG_LAST                  IDS_CMSG_FIRST+55

// offset for message type description

//convRes: #define IDS_CMSG_DESCRIPTION        100
#define IDS_CMSG_DESCRIPTION        100
//convRes: #define IDS_CMSG_DESCRIPTION_LAST   IDS_CMSG_LAST+IDS_CMSG_DESCRIPTION
#define IDS_CMSG_DESCRIPTION_LAST   0

/*
 * Options and Settings
 */

// #define IDS_OPT_FIRST           	   IDS_CMSG_DESCRIPTION_LAST
//convRes: #define IDS_OPT_FIRST               IDS_FIRST+700
#define IDS_OPT_FIRST               1700

//convRes: #define IDS_OPT_OrderPage           IDS_OPT_FIRST+0
#define IDS_OPT_OrderPage           1700
//convRes: #define IDS_OPT_RealismPage         IDS_OPT_FIRST+1
#define IDS_OPT_RealismPage         1701
//convRes: #define IDS_OPT_MiscPage            IDS_OPT_FIRST+2
#define IDS_OPT_MiscPage            1702
//convRes: #define IDS_OPT_SkillLevel          IDS_OPT_FIRST+3
#define IDS_OPT_SkillLevel          1703

// #define IDS_OPT_MessagePage      IDS_OPT_FIRST+3

//convRes: #define IDS_OPT_InstantOrders       IDS_OPT_FIRST+4
#define IDS_OPT_InstantOrders       1704
//convRes: #define IDS_OPT_InstantMove         IDS_OPT_FIRST+5
#define IDS_OPT_InstantMove         1705
//convRes: #define IDS_OPT_FullRoute           IDS_OPT_FIRST+6
#define IDS_OPT_FullRoute           1706
//convRes: #define IDS_OPT_ColorCursor         IDS_OPT_FIRST+7
#define IDS_OPT_ColorCursor         1707
//convRes: #define IDS_OPT_ViewEnemy           IDS_OPT_FIRST+8
#define IDS_OPT_ViewEnemy           1708
//convRes: #define IDS_OPT_OrderEnemy          IDS_OPT_FIRST+9
#define IDS_OPT_OrderEnemy          1709
//convRes: #define IDS_OPT_ToolTips            IDS_OPT_FIRST+10
#define IDS_OPT_ToolTips            1710
//convRes: #define IDS_OPT_AlertBox            IDS_OPT_FIRST+11
#define IDS_OPT_AlertBox            1711
//convRes: #define IDS_OPT_TownNames           IDS_OPT_FIRST+12
#define IDS_OPT_TownNames           1712
//convRes: #define IDS_OPT_TimedOrder0         IDS_OPT_FIRST+13
#define IDS_OPT_TimedOrder0         1713
//convRes: #define IDS_OPT_TimedOrder5         IDS_OPT_FIRST+14
#define IDS_OPT_TimedOrder5         1714
//convRes: #define IDS_OPT_TimedOrder10        IDS_OPT_FIRST+15
#define IDS_OPT_TimedOrder10        1715
//convRes: #define IDS_OPT_AgressionChange     IDS_OPT_FIRST+16
#define IDS_OPT_AgressionChange     1716

//#define IDS_OPT_LimitedInfoUnits  IDS_OPT_FIRST+17
//#define IDS_OPT_LimitedInfoTowns  IDS_OPT_FIRST+18

//convRes: #define IDS_OPT_ShowAI              IDS_OPT_FIRST+19
#define IDS_OPT_ShowAI              1719
//convRes: #define IDS_OPT_Supply              IDS_OPT_FIRST+20
#define IDS_OPT_Supply              1720
//convRes: #define IDS_OPT_Attrition           IDS_OPT_FIRST+21
#define IDS_OPT_Attrition           1721
//convRes: #define IDS_OPT_NationRules         IDS_OPT_FIRST+22
#define IDS_OPT_NationRules         1722
//convRes: #define IDS_OPT_NationDisplay       IDS_OPT_FIRST+23
#define IDS_OPT_NationDisplay       1723
//convRes: #define IDS_OPT_QuickMove           IDS_OPT_FIRST+24
#define IDS_OPT_QuickMove           1724
//convRes: #define IDS_OPT_TownIcons           IDS_OPT_FIRST+25
#define IDS_OPT_TownIcons           1725
//convRes: #define IDS_OPT_TownHilightIcons    IDS_OPT_FIRST+26
#define IDS_OPT_TownHilightIcons    1726
//convRes: #define IDS_OPT_DrawMapScale        IDS_OPT_FIRST+27
#define IDS_OPT_DrawMapScale        1727
//convRes: #define IDS_OPT_Novice              IDS_OPT_FIRST+28
#define IDS_OPT_Novice              1728
//convRes: #define IDS_OPT_Normal              IDS_OPT_FIRST+29
#define IDS_OPT_Normal              1729
//convRes: #define IDS_OPT_Expert              IDS_OPT_FIRST+30
#define IDS_OPT_Expert              1730
//convRes: #define IDS_OPT_Custom              IDS_OPT_FIRST+31
#define IDS_OPT_Custom              1731
//convRes: #define IDS_OPT_DeadBodies      		IDS_OPT_FIRST+32
#define IDS_OPT_DeadBodies      		1732
//convRes: #define IDS_OPT_FogOfWar      		IDS_OPT_FIRST+33
#define IDS_OPT_FogOfWar      		1733
//convRes: #define IDS_OPT_Labels      			IDS_OPT_FIRST+34
#define IDS_OPT_Labels      			1734
//convRes: #define IDS_OPT_ShowSupply      		IDS_OPT_FIRST+35
#define IDS_OPT_ShowSupply      		1735
//convRes: #define IDS_OPT_ShowChokePoints     IDS_OPT_FIRST+36
#define IDS_OPT_ShowChokePoints     1736
//convRes: #define IDS_OPT_ShowSupplyLines     IDS_OPT_FIRST+37
#define IDS_OPT_ShowSupplyLines     1737
// #define IDS_OPT_LAST            		IDS_OPT_FIRST+38

// #define IDS_MSG_FIRST               IDS_OPT_LAST
//convRes: #define IDS_MSG_FIRST               IDS_FIRST+750
#define IDS_MSG_FIRST               1750
//convRes: #define IDS_MSG_GROUP0              IDS_MSG_FIRST+0
#define IDS_MSG_GROUP0              1750
//convRes: #define IDS_MSG_GROUP1              IDS_MSG_FIRST+1
#define IDS_MSG_GROUP1              1751
//convRes: #define IDS_MSG_GROUP2              IDS_MSG_FIRST+2
#define IDS_MSG_GROUP2              1752
//convRes: #define IDS_MSG_GROUP3              IDS_MSG_FIRST+3
#define IDS_MSG_GROUP3              1753
// #define IDS_MSG_LAST                IDS_MSG_FIRST+4

/*
 * Saved Game strings
 */

// #define IDS_SAVE_FIRST           IDS_MSG_LAST
//convRes: #define IDS_SAVE_FIRST              IDS_FIRST+760
#define IDS_SAVE_FIRST              1760
//convRes: #define IDS_SAVE_SAVEDGAME          IDS_SAVE_FIRST+0
#define IDS_SAVE_SAVEDGAME          1760
//convRes: #define IDS_SAVE_LOADGAME           IDS_SAVE_FIRST+1
#define IDS_SAVE_LOADGAME           1761
//convRes: #define IDS_SAVE_SAVEGAME           IDS_SAVE_FIRST+2
#define IDS_SAVE_SAVEGAME           1762
//convRes: #define IDS_SAVE_SCENARIO_BIN       IDS_SAVE_FIRST+4
#define IDS_SAVE_SCENARIO_BIN       1764
//convRes: #define IDS_SAVE_SCENARIO_ASC       IDS_SAVE_FIRST+5
#define IDS_SAVE_SCENARIO_ASC       1765
//convRes: #define IDS_SAVE_LOADWORLD          IDS_SAVE_FIRST+6
#define IDS_SAVE_LOADWORLD          1766
//convRes: #define IDS_SAVE_SAVEWORLD          IDS_SAVE_FIRST+7
#define IDS_SAVE_SAVEWORLD          1767
//convRes: #define IDS_SAVE_FINDFILE           IDS_SAVE_FIRST+8
#define IDS_SAVE_FINDFILE           1768
// #define IDS_SAVE_LAST               IDS_SAVE_FIRST+9


/*
 * Orders related strings
 */

// Order::Type
// #define IDS_ORDER_FIRST             IDS_SAVE_LAST+10
//convRes: #define IDS_ORDER_FIRST             IDS_FIRST+780
#define IDS_ORDER_FIRST             1780
//convRes: #define IDS_ORDER_MOVE              IDS_ORDER_FIRST
#define IDS_ORDER_MOVE              1780
//convRes: #define IDS_ORDER_HOLD              IDS_ORDER_FIRST+1
#define IDS_ORDER_HOLD              1781
//convRes: #define IDS_ORDER_RESTRALLY         IDS_ORDER_FIRST+2
#define IDS_ORDER_RESTRALLY         1782
//convRes: #define IDS_ORDER_ATTACH            IDS_ORDER_FIRST+3
#define IDS_ORDER_ATTACH            1783
//convRes: #define IDS_ORDER_ATTACHSP          IDS_ORDER_FIRST+4
#define IDS_ORDER_ATTACHSP          1784
//convRes: #define IDS_ORDER_DETACHLEADER      IDS_ORDER_FIRST+5
#define IDS_ORDER_DETACHLEADER      1785
//convRes: #define IDS_ORDER_GARRISON          IDS_ORDER_FIRST+6
#define IDS_ORDER_GARRISON          1786
//convRes: #define IDS_ORDER_INSTALLSUPPLY     IDS_ORDER_FIRST+7
#define IDS_ORDER_INSTALLSUPPLY     1787
//convRes: #define IDS_ORDER_UPGRADEFORT       IDS_ORDER_FIRST+8
#define IDS_ORDER_UPGRADEFORT       1788
//convRes: #define IDS_ORDER_BlowBridge        IDS_ORDER_FIRST+9
#define IDS_ORDER_BlowBridge        1789
//convRes: #define IDS_ORDER_RepairBridge      IDS_ORDER_FIRST+10
#define IDS_ORDER_RepairBridge      1790
//convRes: #define IDS_ORDER_RemoveDepot       IDS_ORDER_FIRST+11
#define IDS_ORDER_RemoveDepot       1791



// #define IDS_ORDER_LAST              IDS_ORDER_FIRST+12   // leave a little space

//Orders::Aggression
// #define IDS_AGGRESSION_FIRST        IDS_ORDER_LAST
//convRes: #define IDS_AGGRESSION_FIRST        IDS_FIRST+800
#define IDS_AGGRESSION_FIRST        1800
//convRes: #define IDS_AGGRESSION_LEVEL1       IDS_AGGRESSION_FIRST
#define IDS_AGGRESSION_LEVEL1       1800
//convRes: #define IDS_AGGRESSION_LEVEL2       IDS_AGGRESSION_FIRST+1
#define IDS_AGGRESSION_LEVEL2       1801
//convRes: #define IDS_AGGRESSION_LEVEL3       IDS_AGGRESSION_FIRST+2
#define IDS_AGGRESSION_LEVEL3       1802
//convRes: #define IDS_AGGRESSION_LEVEL4       IDS_AGGRESSION_FIRST+3
#define IDS_AGGRESSION_LEVEL4       1803
// #define IDS_AGGRESSION_LAST         IDS_AGGRESSION_FIRST+4

//Orders::Posture
// #define IDS_POSTURE_FIRST           IDS_AGGRESSION_LAST
//convRes: #define IDS_POSTURE_FIRST           IDS_FIRST+810
#define IDS_POSTURE_FIRST           1810
//convRes: #define IDS_POSTURE_OFFENSIVE       IDS_POSTURE_FIRST
#define IDS_POSTURE_OFFENSIVE       1810
//convRes: #define IDS_POSTURE_DEFENSIVE       IDS_POSTURE_FIRST+1
#define IDS_POSTURE_DEFENSIVE       1811
// #define IDS_POSTURE_LAST            IDS_POSTURE_FIRST+2

//Orders::MoveHow
// #define IDS_MOVEHOW_FIRST           IDS_POSTURE_LAST
//convRes: #define IDS_MOVEHOW_FIRST           IDS_FIRST+820
#define IDS_MOVEHOW_FIRST           1820
//convRes: #define IDS_MOVEHOW_NORMAL          IDS_MOVEHOW_FIRST
#define IDS_MOVEHOW_NORMAL          1820
//convRes: #define IDS_MOVEHOW_EASY            IDS_MOVEHOW_FIRST+1
#define IDS_MOVEHOW_EASY            1821
//convRes: #define IDS_MOVEHOW_FORCE           IDS_MOVEHOW_FIRST+2
#define IDS_MOVEHOW_FORCE           1822
// #define IDS_MOVEHOW_LAST            IDS_MOVEHOW_FIRST+3

//Orders::OrderOnArrival
// #define IDS_ONARRIVAL_FIRST         IDS_MOVEHOW_LAST
//convRes: #define IDS_ONARRIVAL_FIRST         IDS_FIRST+830
#define IDS_ONARRIVAL_FIRST         1830
//convRes: #define IDS_ONARRIVAL_HOLD          IDS_ONARRIVAL_FIRST
#define IDS_ONARRIVAL_HOLD          1830
//convRes: #define IDS_ONARRIVAL_REST          IDS_ONARRIVAL_FIRST+1
#define IDS_ONARRIVAL_REST          1831
//convRes: #define IDS_ONARRIVAL_GARRISON      IDS_ONARRIVAL_FIRST+2
#define IDS_ONARRIVAL_GARRISON      1832
//convRes: #define IDS_ONARRIVAL_ATTACH        IDS_ONARRIVAL_FIRST+3
#define IDS_ONARRIVAL_ATTACH        1833
// #define IDS_ONARRIVAL_LAST          IDS_ONARRIVAL_FIRST+4

//Advanced Options
// #define IDS_AO_FIRST                IDS_ONARRIVAL_LAST
//convRes: #define IDS_AO_FIRST                IDS_FIRST+840
#define IDS_AO_FIRST                1840
//convRes: #define IDS_AO_SOUNDGUNS            IDS_AO_FIRST
#define IDS_AO_SOUNDGUNS            1840
//convRes: #define IDS_AO_PURSUE               IDS_AO_FIRST+1
#define IDS_AO_PURSUE               1841
//convRes: #define IDS_AO_SIEGEACTIVE          IDS_AO_FIRST+2
#define IDS_AO_SIEGEACTIVE          1842
//convRes: #define IDS_AO_DropOff              IDS_AO_FIRST+3
#define IDS_AO_DropOff              1843
//convRes: #define IDS_AO_AutoStorm            IDS_AO_FIRST+4
#define IDS_AO_AutoStorm            1844
// #define IDS_AO_LAST                 IDS_AO_FIRST+5

// vias
// #define IDS_MISC_ORDERS_FIRST              IDS_AO_LAST
//convRes: #define IDS_MISC_ORDERS_FIRST              IDS_FIRST+850
#define IDS_MISC_ORDERS_FIRST              1850
//convRes: #define IDS_MISC_ORDERS_VIA1               IDS_MISC_ORDERS_FIRST
#define IDS_MISC_ORDERS_VIA1               1850
//convRes: #define IDS_MISC_ORDERS_VIA2               IDS_MISC_ORDERS_FIRST+1
#define IDS_MISC_ORDERS_VIA2               1851
//convRes: #define IDS_MISC_ORDERS_TO                 IDS_MISC_ORDERS_FIRST+2
#define IDS_MISC_ORDERS_TO                 1852
//convRes: #define IDS_MISC_ORDERS_MOVETO             IDS_MISC_ORDERS_FIRST+3
#define IDS_MISC_ORDERS_MOVETO             1853
//convRes: #define IDS_MISC_ORDERS_AND                IDS_MISC_ORDERS_FIRST+4
#define IDS_MISC_ORDERS_AND                1854
//convRes: #define IDS_MISC_ORDERS_ATTACHTO           IDS_MISC_ORDERS_FIRST+5
#define IDS_MISC_ORDERS_ATTACHTO           1855
//convRes: #define IDS_MISC_ORDERS_ATTACHSP           IDS_MISC_ORDERS_FIRST+6
#define IDS_MISC_ORDERS_ATTACHSP           1856
//convRes: #define IDS_MISC_ORDERS_DETACHLEADER       IDS_MISC_ORDERS_FIRST+7
#define IDS_MISC_ORDERS_DETACHLEADER       1857
//convRes: #define IDS_MISC_ORDERS_INSTALLSUPPLY      IDS_MISC_ORDERS_FIRST+8
#define IDS_MISC_ORDERS_INSTALLSUPPLY      1858
//convRes: #define IDS_MISC_ORDERS_UPGRADEFORT        IDS_MISC_ORDERS_FIRST+9
#define IDS_MISC_ORDERS_UPGRADEFORT        1859

// #define IDS_MISC_ORDERS_LAST               IDS_MISC_ORDERS_FIRST+10

// #define IDS_CU_FIRST                       IDS_MISC_ORDERS_LAST
//convRes: #define IDS_CU_FIRST                       IDS_FIRST+870
#define IDS_CU_FIRST                       1870
//convRes: #define IDS_CU_HOLDING                     IDS_CU_FIRST
#define IDS_CU_HOLDING                     1870
//convRes: #define IDS_CU_RESTING                     IDS_CU_FIRST+1
#define IDS_CU_RESTING                     1871
//convRes: #define IDS_CU_RALLYING                    IDS_CU_FIRST+2
#define IDS_CU_RALLYING                    1872
//convRes: #define IDS_CU_MOVING                      IDS_CU_FIRST+3
#define IDS_CU_MOVING                      1873
//convRes: #define IDS_CU_SIEGING                     IDS_CU_FIRST+4
#define IDS_CU_SIEGING                     1874
//convRes: #define IDS_CU_RAIDING                     IDS_CU_FIRST+5
#define IDS_CU_RAIDING                     1875
//convRes: #define IDS_CU_WAITBATTLE                  IDS_CU_FIRST+6
#define IDS_CU_WAITBATTLE                  1876
//convRes: #define IDS_CU_MOVEINTOBATTLE              IDS_CU_FIRST+7
#define IDS_CU_MOVEINTOBATTLE              1877
//convRes: #define IDS_CU_PURSUING                    IDS_CU_FIRST+8
#define IDS_CU_PURSUING                    1878
//convRes: #define IDS_CU_WITHINRANGE                 IDS_CU_FIRST+9
#define IDS_CU_WITHINRANGE                 1879
//convRes: #define IDS_CU_HALTNEARENEMY               IDS_CU_FIRST+10
#define IDS_CU_HALTNEARENEMY               1880
//convRes: #define IDS_CU_RETREATFROMENEMY            IDS_CU_FIRST+11
#define IDS_CU_RETREATFROMENEMY            1881
//convRes: #define IDS_CU_DISTANTENEMYRANGE           IDS_CU_FIRST+12
#define IDS_CU_DISTANTENEMYRANGE           1882
//convRes: #define IDS_CU_HALTAWAYFROMENEMY           IDS_CU_FIRST+13
#define IDS_CU_HALTAWAYFROMENEMY           1883
//convRes: #define IDS_CU_AVOIDING                    IDS_CU_FIRST+14
#define IDS_CU_AVOIDING                    1884
//convRes: #define IDS_CU_STARTWITHDRAW               IDS_CU_FIRST+15
#define IDS_CU_STARTWITHDRAW               1885
//convRes: #define IDS_CU_WITHDRAWING                 IDS_CU_FIRST+16
#define IDS_CU_WITHDRAWING                 1886
//convRes: #define IDS_CU_INBATTLE                    IDS_CU_FIRST+17
#define IDS_CU_INBATTLE                    1887
//convRes: #define IDS_CU_STARTRETREAT                IDS_CU_FIRST+18
#define IDS_CU_STARTRETREAT                1888
//convRes: #define IDS_CU_RETREATING                  IDS_CU_FIRST+19
#define IDS_CU_RETREATING                  1889
//convRes: #define IDS_CU_REGROUPING                  IDS_CU_FIRST+20
#define IDS_CU_REGROUPING                  1890
//convRes: #define IDS_CU_INSTALLSUPPLY               IDS_CU_FIRST+21
#define IDS_CU_INSTALLSUPPLY               1891
//convRes: #define IDS_CU_INSTALLFORT                 IDS_CU_FIRST+22
#define IDS_CU_INSTALLFORT                 1892
//convRes: #define IDS_CU_SURRENDERING                IDS_CU_FIRST+23
#define IDS_CU_SURRENDERING                1893

//convRes: #define IDS_CU_MISC_COMPLETED              IDS_CU_FIRST+24
#define IDS_CU_MISC_COMPLETED              1894
// #define IDS_CU_LAST                        IDS_CU_FIRST+25

// #define IDS_ORDERIDS_LAST           IDS_CU_LAST+20    // leave some space

/*
 * Unit Dialog strings
 */

// main dialog tootips
// #define TTS_UID_FIRST               IDS_ORDERIDS_LAST
//convRes: #define TTS_UID_FIRST               IDS_FIRST+900
#define TTS_UID_FIRST               1900
//convRes: #define TTS_UID_UNITS               TTS_UID_FIRST
#define TTS_UID_UNITS               1900
//convRes: #define TTS_UID_SHOWALL             TTS_UID_FIRST+1
#define TTS_UID_SHOWALL             1901
//convRes: #define TTS_UID_SEND                TTS_UID_FIRST+2
#define TTS_UID_SEND                1902
//convRes: #define TTS_UID_CANCEL              TTS_UID_FIRST+3
#define TTS_UID_CANCEL              1903
//convRes: #define TTS_UID_CLOSE               TTS_UID_FIRST+4
#define TTS_UID_CLOSE               1904
// #define TTS_UID_LAST                TTS_UID_FIRST+5

// main dialog tootips
// #define SWS_UID_FIRST               TTS_UID_LAST
//convRes: #define SWS_UID_FIRST               IDS_FIRST+910
#define SWS_UID_FIRST               1910
//convRes: #define SWS_UID_UNITS               SWS_UID_FIRST
#define SWS_UID_UNITS               1910
//convRes: #define SWS_UID_SHOWALL             SWS_UID_FIRST+1
#define SWS_UID_SHOWALL             1911
//convRes: #define SWS_UID_SEND                SWS_UID_FIRST+2
#define SWS_UID_SEND                1912
//convRes: #define SWS_UID_CANCEL              SWS_UID_FIRST+3
#define SWS_UID_CANCEL              1913
//convRes: #define SWS_UID_CLOSE               SWS_UID_FIRST+4
#define SWS_UID_CLOSE               1914
// #define SWS_UID_LAST                SWS_UID_FIRST+5

// pop-up move dialog tootips
// #define TTS_UMD_FIRST               SWS_UID_LAST
//convRes: #define TTS_UMD_FIRST               IDS_FIRST+920
#define TTS_UMD_FIRST               1920
//convRes: #define TTS_UMD_UNITS               TTS_UMD_FIRST
#define TTS_UMD_UNITS               1920
//convRes: #define TTS_UMD_SEND                TTS_UMD_FIRST+1
#define TTS_UMD_SEND                1921
//convRes: #define TTS_UMD_CANCEL              TTS_UMD_FIRST+2
#define TTS_UMD_CANCEL              1922
//convRes: #define TTS_UMD_MAINDIAL            TTS_UMD_FIRST+3
#define TTS_UMD_MAINDIAL            1923
// #define TTS_UMD_LAST                TTS_UMD_FIRST+4

// #define SWS_UMD_FIRST               TTS_UMD_LAST
//convRes: #define SWS_UMD_FIRST               IDS_FIRST+930
#define SWS_UMD_FIRST               1930
//convRes: #define SWS_UMD_UNITS               SWS_UMD_FIRST
#define SWS_UMD_UNITS               1930
//convRes: #define SWS_UMD_SEND                SWS_UMD_FIRST+1
#define SWS_UMD_SEND                1931
//convRes: #define SWS_UMD_CANCEL              SWS_UMD_FIRST+2
#define SWS_UMD_CANCEL              1932
//convRes: #define SWS_UMD_MAINDIAL            SWS_UMD_FIRST+3
#define SWS_UMD_MAINDIAL            1933
// #define SWS_UMD_LAST                SWS_UMD_FIRST+4


// order page
// #define IDS_UOP_FIRST               SWS_UMD_LAST
//convRes: #define IDS_UOP_FIRST               IDS_FIRST+940
#define IDS_UOP_FIRST               1940
//convRes: #define IDS_UOP_PURSUE              IDS_UOP_FIRST
#define IDS_UOP_PURSUE              1940
//convRes: #define IDS_UOP_SOUNDGUNS           IDS_UOP_FIRST+1
#define IDS_UOP_SOUNDGUNS           1941
//convRes: #define IDS_UOP_SIEGEACTIVE         IDS_UOP_FIRST+2
#define IDS_UOP_SIEGEACTIVE         1942
//convRes: #define IDS_UOP_TOUNIT              IDS_UOP_FIRST+3
#define IDS_UOP_TOUNIT              1943
//convRes: #define IDS_UOP_OFFENSIVE           IDS_UOP_FIRST+4
#define IDS_UOP_OFFENSIVE           1944
//convRes: #define IDS_UOP_DEFENSIVE           IDS_UOP_FIRST+5
#define IDS_UOP_DEFENSIVE           1945


//convRes: #define IDS_UOP_Via                 IDS_UOP_FIRST+6
#define IDS_UOP_Via                 1946
//convRes: #define IDS_UOP_On                  IDS_UOP_FIRST+7
#define IDS_UOP_On                  1947
//convRes: #define IDS_UOP_Inf                 IDS_UOP_FIRST+8
#define IDS_UOP_Inf                 1948
//convRes: #define IDS_UOP_Cav                 IDS_UOP_FIRST+9
#define IDS_UOP_Cav                 1949
// #define IDS_UOP_Guns                IDS_UOP_FIRST+10
//convRes: #define IDS_Guns                    IDS_UOP_FIRST+10
#define IDS_Guns                    1950
//convRes: #define IDS_UOP_SoundOfGuns         IDS_UOP_FIRST+11
#define IDS_UOP_SoundOfGuns         1951
//convRes: #define IDS_UOP_Pursue              IDS_UOP_FIRST+12
#define IDS_UOP_Pursue              1952
//convRes: #define IDS_UOP_SiegeActive         IDS_UOP_FIRST+13
#define IDS_UOP_SiegeActive         1953
//convRes: #define IDS_UOP_DropOff             IDS_UOP_FIRST+14
#define IDS_UOP_DropOff             1954
//convRes: #define IDS_UOP_AutoStorm           IDS_UOP_FIRST+15
#define IDS_UOP_AutoStorm           1955
//convRes: #define IDS_UOP_Send                IDS_UOP_FIRST+16
#define IDS_UOP_Send                1956
//convRes: #define IDS_UOP_OK                  IDS_UOP_FIRST+17
#define IDS_UOP_OK                  1957
//convRes: #define IDS_UOP_Static              IDS_UOP_FIRST+18
#define IDS_UOP_Static              1958
//convRes: #define IDS_UOP_Info                IDS_UOP_FIRST+19
#define IDS_UOP_Info                1959
//convRes: #define IDS_UOP_OB                  IDS_UOP_FIRST+20
#define IDS_UOP_OB                  1960
//convRes: #define IDS_UOP_Date                IDS_UOP_FIRST+21
#define IDS_UOP_Date                1961
//convRes: #define IDS_UOP_Full                IDS_UOP_FIRST+22
#define IDS_UOP_Full                1962
//convRes: #define IDS_UOP_Cancel              IDS_UOP_FIRST+24
#define IDS_UOP_Cancel              1964
//convRes: #define IDS_UOP_ToUnit              IDS_UOP_FIRST+25
#define IDS_UOP_ToUnit              1965
//convRes: #define IDS_UOP_Off                 IDS_UOP_FIRST+26
#define IDS_UOP_Off                 1966
//convRes: #define IDS_UOP_Def                 IDS_UOP_FIRST+27
#define IDS_UOP_Def                 1967

//convRes: #define IDS_UOP_FullDot             IDS_UOP_FIRST+28
#define IDS_UOP_FullDot             1968
//convRes: #define IDS_UOP_LessDot             IDS_UOP_FIRST+29
#define IDS_UOP_LessDot             1969
//convRes: #define IDS_UOP_Add                 IDS_UOP_FIRST+30
#define IDS_UOP_Add                 1970
//convRes: #define IDS_UOP_Del                 IDS_UOP_FIRST+31
#define IDS_UOP_Del                 1971
//convRes: #define IDS_RefuseLeft              IDS_UOP_FIRST+32
#define IDS_RefuseLeft              1972
//convRes: #define IDS_EchelonLeft             IDS_UOP_FIRST+33
#define IDS_EchelonLeft             1973
//convRes: #define IDS_RefuseRight             IDS_UOP_FIRST+34
#define IDS_RefuseRight             1974
//convRes: #define IDS_EchelonRight            IDS_UOP_FIRST+35
#define IDS_EchelonRight            1975

// #define IDS_UOP_LAST                IDS_UOP_FIRST+36



// orders page    tooltips
// #define TTS_UOP_FIRST               IDS_UOP_LAST
//convRes: #define TTS_UOP_FIRST               IDS_FIRST+1000
#define TTS_UOP_FIRST               2000
//convRes: #define TTS_UOP_ORDER               TTS_UOP_FIRST
#define TTS_UOP_ORDER               2000
//convRes: #define TTS_UOP_MOVEHOW             TTS_UOP_FIRST+1
#define TTS_UOP_MOVEHOW             2001
//convRes: #define TTS_UOP_ONARRIVAL           TTS_UOP_FIRST+2
#define TTS_UOP_ONARRIVAL           2002
//convRes: #define TTS_UOP_PURSUE              TTS_UOP_FIRST+3
#define TTS_UOP_PURSUE              2003
//convRes: #define TTS_UOP_SIEGEACTIVE         TTS_UOP_FIRST+4
#define TTS_UOP_SIEGEACTIVE         2004
//convRes: #define TTS_UOP_SOUNDGUNS           TTS_UOP_FIRST+5
#define TTS_UOP_SOUNDGUNS           2005
//convRes: #define TTS_UOP_ORDERTEXT           TTS_UOP_FIRST+6
#define TTS_UOP_ORDERTEXT           2006
//convRes: #define TTS_UOP_ORDERDATE           TTS_UOP_FIRST+7
#define TTS_UOP_ORDERDATE           2007
//convRes: #define TTS_UOP_DESTINATION         TTS_UOP_FIRST+8
#define TTS_UOP_DESTINATION         2008
//convRes: #define TTS_UOP_TOUNIT              TTS_UOP_FIRST+9
#define TTS_UOP_TOUNIT              2009
//convRes: #define TTS_UOP_AGGRESSION          TTS_UOP_FIRST+10
#define TTS_UOP_AGGRESSION          2010
//convRes: #define TTS_UOP_OFFENSIVE           TTS_UOP_FIRST+11
#define TTS_UOP_OFFENSIVE           2011
//convRes: #define TTS_UOP_DEFENSIVE           TTS_UOP_FIRST+12
#define TTS_UOP_DEFENSIVE           2012
// #define TTS_UOP_LAST                TTS_UOP_FIRST+13

// #define SWS_UOP_FIRST               TTS_UOP_LAST
//convRes: #define SWS_UOP_FIRST               IDS_FIRST+1020
#define SWS_UOP_FIRST               2020
//convRes: #define SWS_UOP_ORDER               SWS_UOP_FIRST
#define SWS_UOP_ORDER               2020
//convRes: #define SWS_UOP_MOVEHOW             SWS_UOP_FIRST+1
#define SWS_UOP_MOVEHOW             2021
//convRes: #define SWS_UOP_ONARRIVAL           SWS_UOP_FIRST+2
#define SWS_UOP_ONARRIVAL           2022
//convRes: #define SWS_UOP_PURSUE              SWS_UOP_FIRST+3
#define SWS_UOP_PURSUE              2023
//convRes: #define SWS_UOP_SIEGEACTIVE         SWS_UOP_FIRST+4
#define SWS_UOP_SIEGEACTIVE         2024
//convRes: #define SWS_UOP_SOUNDGUNS           SWS_UOP_FIRST+5
#define SWS_UOP_SOUNDGUNS           2025
//convRes: #define SWS_UOP_ORDERTEXT           SWS_UOP_FIRST+6
#define SWS_UOP_ORDERTEXT           2026
//convRes: #define SWS_UOP_ORDERDATE           SWS_UOP_FIRST+7
#define SWS_UOP_ORDERDATE           2027
//convRes: #define SWS_UOP_DESTINATION         SWS_UOP_FIRST+8
#define SWS_UOP_DESTINATION         2028
//convRes: #define SWS_UOP_TOUNIT              SWS_UOP_FIRST+9
#define SWS_UOP_TOUNIT              2029
//convRes: #define SWS_UOP_AGGRESSION          SWS_UOP_FIRST+10
#define SWS_UOP_AGGRESSION          2030
//convRes: #define SWS_UOP_OFFENSIVE           SWS_UOP_FIRST+11
#define SWS_UOP_OFFENSIVE           2031
//convRes: #define SWS_UOP_DEFENSIVE           SWS_UOP_FIRST+12
#define SWS_UOP_DEFENSIVE           2032
// #define SWS_UOP_LAST                SWS_UOP_FIRST+13

/*
 * Town Dialog strings
 */

// #define IDS_TOWNINFO_FIRST           SWS_UOP_LAST
//convRes: #define IDS_TOWNINFO_FIRST              IDS_FIRST+1040
#define IDS_TOWNINFO_FIRST              2040
/*
 * Town Dialog Strings
 */

//convRes: #define IDS_ControlledBy_S              IDS_TOWNINFO_FIRST+0
#define IDS_ControlledBy_S              2040
//convRes: #define IDS_Garrison                    IDS_TOWNINFO_FIRST+1
#define IDS_Garrison                    2041
//convRes: #define IDS_Supply                      IDS_TOWNINFO_FIRST+2
#define IDS_Supply                      2042
//convRes: #define IDS_Victory                     IDS_TOWNINFO_FIRST+3
#define IDS_Victory                     2043
//convRes: #define IDS_Source                      IDS_TOWNINFO_FIRST+4
#define IDS_Source                      2044
//convRes: #define IDS_Depot                       IDS_TOWNINFO_FIRST+5
#define IDS_Depot                       2045
//convRes: #define IDS_BuildingDepot               IDS_TOWNINFO_FIRST+6
#define IDS_BuildingDepot               2046

//convRes: #define IDS_Summary                     IDS_TOWNINFO_FIRST+10
#define IDS_Summary                     2050
//convRes: #define IDS_Build                       IDS_TOWNINFO_FIRST+11
#define IDS_Build                       2051
//convRes: #define IDS_Status                      IDS_TOWNINFO_FIRST+12
#define IDS_Status                      2052
//convRes: #define IDS_Town                        IDS_TOWNINFO_FIRST+13
#define IDS_Town                        2053
//convRes: #define IDS_Province                    IDS_TOWNINFO_FIRST+14
#define IDS_Province                    2054
//convRes: #define IDS_Capital                     IDS_TOWNINFO_FIRST+15
#define IDS_Capital                     2055
//convRes: #define IDS_n_WeeksToCompletion         IDS_TOWNINFO_FIRST+16
#define IDS_n_WeeksToCompletion         2056
//convRes: #define IDS_WeekAbrev                   IDS_TOWNINFO_FIRST+17
#define IDS_WeekAbrev                   2057
//convRes: #define IDS_NTowns                      IDS_TOWNINFO_FIRST+18
#define IDS_NTowns                      2058
//convRes: #define IDS_Manpower                    IDS_TOWNINFO_FIRST+19
#define IDS_Manpower                    2059
//convRes: #define IDS_Resources                   IDS_TOWNINFO_FIRST+20
#define IDS_Resources                   2060
//convRes: #define IDS_VictoryPoints               IDS_TOWNINFO_FIRST+21
#define IDS_VictoryPoints               2061
//convRes: #define IDS_FortLevel                   IDS_TOWNINFO_FIRST+22
#define IDS_FortLevel                   2062
//convRes: #define IDS_Terrain                     IDS_TOWNINFO_FIRST+23
#define IDS_Terrain                     2063
//convRes: #define IDS_PERCENT_complete            IDS_TOWNINFO_FIRST+24
#define IDS_PERCENT_complete            2064
//convRes: #define IDS_ForageValue                 IDS_TOWNINFO_FIRST+25
#define IDS_ForageValue                 2065
//convRes: #define IDS_InGarrison                  IDS_TOWNINFO_FIRST+26
#define IDS_InGarrison                  2066
//convRes: #define IDS_Situation                   IDS_TOWNINFO_FIRST+27
#define IDS_Situation                   2067
//convRes: #define IDS_BridgeCollapsed             IDS_TOWNINFO_FIRST+28
#define IDS_BridgeCollapsed             2068
//convRes: #define IDS_PoliticalPoints             IDS_TOWNINFO_FIRST+29
#define IDS_PoliticalPoints             2069
//convRes: #define IDS_SupplySource                IDS_TOWNINFO_FIRST+30
#define IDS_SupplySource                2070
//convRes: #define IDS_TownStrength                IDS_TOWNINFO_FIRST+31
#define IDS_TownStrength                2071


//convRes: #define IDS_TOWNTYPE_CAPITAL            IDS_TOWNINFO_FIRST+40
#define IDS_TOWNTYPE_CAPITAL            2080
//convRes: #define IDS_TOWNTYPE_CITY               IDS_TOWNINFO_FIRST+41
#define IDS_TOWNTYPE_CITY               2081
//convRes: #define IDS_TOWNTYPE_TOWN               IDS_TOWNINFO_FIRST+42
#define IDS_TOWNTYPE_TOWN               2082
//convRes: #define IDS_TOWNTYPE_OTHER              IDS_TOWNINFO_FIRST+43
#define IDS_TOWNTYPE_OTHER              2083

/*
 * String IDs for Build-Item dialog
 */

//convRes: #define IDS_BID_DESCRIPTION             IDS_TOWNINFO_FIRST+50
#define IDS_BID_DESCRIPTION             2090
//convRes: #define IDS_BID_TIME                    IDS_TOWNINFO_FIRST+51
#define IDS_BID_TIME                    2091
//convRes: #define IDS_BID_MP                      IDS_TOWNINFO_FIRST+52
#define IDS_BID_MP                      2092
//convRes: #define IDS_BID_RES                     IDS_TOWNINFO_FIRST+53
#define IDS_BID_RES                     2093
//convRes: #define IDS_BID_EFF                     IDS_TOWNINFO_FIRST+54
#define IDS_BID_EFF                     2094
//convRes: #define IDS_BID_MORALE                  IDS_TOWNINFO_FIRST+55
#define IDS_BID_MORALE                  2095
//convRes: #define IDS_BID_NOTBUILDING             IDS_TOWNINFO_FIRST+56
#define IDS_BID_NOTBUILDING             2096

//convRes: #define IDS_TI_UNOCCUPPIED              IDS_TOWNINFO_FIRST+60
#define IDS_TI_UNOCCUPPIED              2100
//convRes: #define IDS_TI_OCCUPIED                 IDS_TOWNINFO_FIRST+61
#define IDS_TI_OCCUPIED                 2101
//convRes: #define IDS_TI_RAIDED                   IDS_TOWNINFO_FIRST+62
#define IDS_TI_RAIDED                   2102
//convRes: #define IDS_TI_BESIEGED                 IDS_TOWNINFO_FIRST+63
#define IDS_TI_BESIEGED                 2103



// #define IDS_TOWNINFO_LAST              IDS_TOWNINFO_FIRST+70


/*
 * String IDs for Specialist Leaders/Units
 */

// #define IDS_SPECIALIST_FIRST        IDS_TOWNINFO_LAST
//convRes: #define IDS_SPECIALIST_FIRST        IDS_FIRST+1200
#define IDS_SPECIALIST_FIRST        2200
//convRes: #define IDS_SPECIALIST_CAVALRY      IDS_SPECIALIST_FIRST
#define IDS_SPECIALIST_CAVALRY      2200
//convRes: #define IDS_SPECIALIST_ARTILLERY    IDS_SPECIALIST_FIRST+1
#define IDS_SPECIALIST_ARTILLERY    2201
//convRes: #define IDS_SPECIALIST_NOTSPECIAL   IDS_SPECIALIST_FIRST+2
#define IDS_SPECIALIST_NOTSPECIAL   2202
// #define IDS_SPECIALIST_LAST         IDS_SPECIALIST_FIRST+3

/*
 * String IDs for frontend
 */

// #define IDS_FE_FIRST                IDS_SPECIALIST_LAST
//convRes: #define IDS_FE_FIRST                IDS_FIRST+1210
#define IDS_FE_FIRST                2210
//convRes: #define IDS_FE_NEWCAMPAIGN          IDS_FE_FIRST
#define IDS_FE_NEWCAMPAIGN          2210
//convRes: #define IDS_FE_NEWBATTLE            IDS_FE_FIRST+1
#define IDS_FE_NEWBATTLE            2211
//convRes: #define IDS_FE_SAVEDGAME            IDS_FE_FIRST+2
#define IDS_FE_SAVEDGAME            2212
//convRes: #define IDS_FE_LASTGAME             IDS_FE_FIRST+3
#define IDS_FE_LASTGAME             2213
//convRes: #define IDS_FE_CHOOSECAMPAIGN       IDS_FE_FIRST+4
#define IDS_FE_CHOOSECAMPAIGN       2214
//convRes: #define IDS_FE_CHOOSESIDE           IDS_FE_FIRST+5
#define IDS_FE_CHOOSESIDE           2215
//convRes: #define IDS_FE_OPTIONS              IDS_FE_FIRST+6
#define IDS_FE_OPTIONS              2216
//convRes: #define IDS_FE_NULL                 IDS_FE_FIRST+7
#define IDS_FE_NULL                 2217
//convRes: #define IDS_FE_PREV                 IDS_FE_FIRST+8
#define IDS_FE_PREV                 2218
//convRes: #define IDS_FE_NEXT                 IDS_FE_FIRST+9
#define IDS_FE_NEXT                 2219
//convRes: #define IDS_FE_QUIT                 IDS_FE_FIRST+10
#define IDS_FE_QUIT                 2220
//convRes: #define IDS_FE_HELP                 IDS_FE_FIRST+11
#define IDS_FE_HELP                 2221
//convRes: #define IDS_FE_START                IDS_FE_FIRST+12
#define IDS_FE_START                2222
//convRes: #define IDS_FE_MISSION              IDS_FE_FIRST+13
#define IDS_FE_MISSION              2223
//convRes: #define IDS_FE_LoadGame             IDS_FE_FIRST+14
#define IDS_FE_LoadGame             2224
//convRes: #define IDS_FE_LastGame             IDS_FE_FIRST+15
#define IDS_FE_LastGame             2225
// #define IDS_FE_LAST                 IDS_FE_FIRST+16

/*
 * String IDs for CClock
 */

// #define IDS_CCLOCK_FIRST            IDS_FE_LAST
//convRes: #define IDS_CCLOCK_FIRST            IDS_FIRST+1230
#define IDS_CCLOCK_FIRST            2230
//convRes: #define IDS_CCLOCK_FROZEN           IDS_CCLOCK_FIRST
#define IDS_CCLOCK_FROZEN           2230
//convRes: #define IDS_CCLOCK_SECPERDAY        IDS_CCLOCK_FIRST+1
#define IDS_CCLOCK_SECPERDAY        2231
// #define IDS_CCLOCK_LAST             IDS_CCLOCK_FIRST+2

        /*
         * Campaign Toolbar tooltips
         */

        // misc
// #define  TTS_CTB_First          IDS_CCLOCK_LAST // SWS_CTB_LAST
//convRes: #define  TTS_CTB_First         	IDS_FIRST+1300
#define  TTS_CTB_First         	2300
//convRes: #define  TTS_CTB_Open          	TTS_CTB_First
#define  TTS_CTB_Open          	2300
//convRes: #define  TTS_CTB_Save          	TTS_CTB_First+1
#define  TTS_CTB_Save          	2301
//convRes: #define TTS_CTB_Palette        	TTS_CTB_First+2
#define TTS_CTB_Palette        	2302
//convRes: #define TTS_CTB_HistoricalRef  	TTS_CTB_First+3
#define TTS_CTB_HistoricalRef  	2303
//convRes: #define TTS_CTB_Help           	TTS_CTB_First+4
#define TTS_CTB_Help           	2304

        // campaign tools
//convRes: #define TTS_CTB_ResourceMode   	TTS_CTB_First+5
#define TTS_CTB_ResourceMode   	2305
//convRes: #define TTS_CTB_UnitMode       	TTS_CTB_First+6
#define TTS_CTB_UnitMode       	2306
//convRes: #define TTS_CTB_TinyMap        	TTS_CTB_First+7
#define TTS_CTB_TinyMap        	2307
//convRes: #define TTS_CTB_WeatherWindow  	TTS_CTB_First+8
#define TTS_CTB_WeatherWindow  	2308
//convRes: #define TTS_CTB_MessageWindow  	TTS_CTB_First+9
#define TTS_CTB_MessageWindow  	2309
//convRes: #define TTS_CTB_OrderBattle    	TTS_CTB_First+10
#define TTS_CTB_OrderBattle    	2310
//convRes: #define TTS_CTB_IUnit          	TTS_CTB_First+11
#define TTS_CTB_IUnit          	2311
//convRes: #define TTS_CTB_ILeaders       	TTS_CTB_First+12
#define TTS_CTB_ILeaders       	2312
//convRes: #define TTS_CTB_FindTown       	TTS_CTB_First+13
#define TTS_CTB_FindTown       	2313
//convRes: #define TTS_CTB_FindLeader     	TTS_CTB_First+14
#define TTS_CTB_FindLeader     	2314

        // map display
//convRes: #define TTS_CTB_Grid           	TTS_CTB_First+15
#define TTS_CTB_Grid           	2315
//convRes: #define TTS_CTB_ZoomIn         	TTS_CTB_First+16
#define TTS_CTB_ZoomIn         	2316
//convRes: #define TTS_CTB_ZoomOut        	TTS_CTB_First+17
#define TTS_CTB_ZoomOut        	2317
//convRes: #define TTS_CTB_ShowAllOrders  	TTS_CTB_First+18
#define TTS_CTB_ShowAllOrders  	2318
//convRes: #define TTS_CTB_ShowSupply     	TTS_CTB_First+19
#define TTS_CTB_ShowSupply     	2319
//convRes: #define TTS_CTB_ShowChokePoints	TTS_CTB_First+20
#define TTS_CTB_ShowChokePoints	2320
//convRes: #define TTS_CTB_ShowSupplyLines	TTS_CTB_First+21
#define TTS_CTB_ShowSupplyLines	2321

// #define  TTS_CTB_LAST          	 TTS_CTB_First+22

/*
 * Miscellaneous
 */

// #define IDS_MISC                     TTS_CTB_LAST
//convRes: #define IDS_MISC                        IDS_FIRST+1400
#define IDS_MISC                        2400
//convRes: #define IDS_MORALE                      IDS_MISC+0
#define IDS_MORALE                      2400
//convRes: #define IDS_VICTORYLEVEL                IDS_MISC+1
#define IDS_VICTORYLEVEL                2401

//convRes: #define IDS_OK       IDS_UOP_OK
#define IDS_OK       1957
//convRes: #define IDS_Cancel   IDS_UOP_Cancel
#define IDS_Cancel   1964

//convRes: #define IDS_Yes                         IDS_MISC+2
#define IDS_Yes                         2402
//convRes: #define IDS_No                          IDS_MISC+3
#define IDS_No                          2403
//convRes: #define IDS_None                        IDS_MISC+4
#define IDS_None                        2404
//convRes: #define IDS_Armistice                   IDS_MISC+5
#define IDS_Armistice                   2405
//convRes: #define IDS_Messages                    IDS_MISC+6
#define IDS_Messages                    2406


// #define IDS_BATTLEWINDOW_TITLE          2000
//convRes: #define IDS_BATTLEWINDOW_TITLE          IDS_FIRST+2000
#define IDS_BATTLEWINDOW_TITLE          3000

// #define TTS_BZ_FIRST                    IDS_BATTLEWINDOW_TITLE+1
//convRes: #define TTS_BZ_FIRST                    IDS_FIRST+2001
#define TTS_BZ_FIRST                    3001
//convRes: #define TTS_BZ_ZoomTo                   TTS_BZ_FIRST+0
#define TTS_BZ_ZoomTo                   3001
//convRes: #define TTS_BZ_ZoomOverview             TTS_BZ_FIRST+1
#define TTS_BZ_ZoomOverview             3002
//convRes: #define TTS_BZ_ZoomFourMile             TTS_BZ_FIRST+2
#define TTS_BZ_ZoomFourMile             3003
//convRes: #define TTS_BZ_ZoomTwoMile              TTS_BZ_FIRST+3
#define TTS_BZ_ZoomTwoMile              3004
//convRes: #define TTS_BZ_ZoomDetail               TTS_BZ_FIRST+4
#define TTS_BZ_ZoomDetail               3005
// #define TTS_BZ_END                      TTS_BZ_FIRST+5

// #define SWS_BZ_FIRST                    TTS_BZ_END
//convRes: #define SWS_BZ_FIRST                    IDS_FIRST+2010
#define SWS_BZ_FIRST                    3010
//convRes: #define SWS_BZ_ZoomTo                   SWS_BZ_FIRST+0
#define SWS_BZ_ZoomTo                   3010
//convRes: #define SWS_BZ_ZoomOverview             SWS_BZ_FIRST+1
#define SWS_BZ_ZoomOverview             3011
//convRes: #define SWS_BZ_ZoomFourMile             SWS_BZ_FIRST+2
#define SWS_BZ_ZoomFourMile             3012
//convRes: #define SWS_BZ_ZoomTwoMile              SWS_BZ_FIRST+3
#define SWS_BZ_ZoomTwoMile              3013
//convRes: #define SWS_BZ_ZoomDetail               SWS_BZ_FIRST+4
#define SWS_BZ_ZoomDetail               3014
// #define SWS_BZ_END                      SWS_BZ_FIRST+5

// pop-up move dialog tootips
// These may turn out to be identical to Campaign's TTS_UMD_*

// #define TTS_BUDM_FIRST                                  SWS_BZ_END
//convRes: #define TTS_BUDM_FIRST                                  IDS_FIRST+2020
#define TTS_BUDM_FIRST                                  3020
//convRes: #define TTS_BUDM_UNITS                                  TTS_BUDM_FIRST
#define TTS_BUDM_UNITS                                  3020
//convRes: #define TTS_BUDM_SEND                                   TTS_BUDM_FIRST+1
#define TTS_BUDM_SEND                                   3021
//convRes: #define TTS_BUDM_CANCEL                                 TTS_BUDM_FIRST+2
#define TTS_BUDM_CANCEL                                 3022
//convRes: #define TTS_BUDM_MAINDIAL                               TTS_BUDM_FIRST+3
#define TTS_BUDM_MAINDIAL                               3023
// #define TTS_BUDM_END                                    TTS_BUDM_FIRST+4

// #define SWS_BUDM_FIRST                                  TTS_BUDM_END
//convRes: #define SWS_BUDM_FIRST                                  IDS_FIRST+2030
#define SWS_BUDM_FIRST                                  3030
//convRes: #define SWS_BUDM_UNITS                                  SWS_BUDM_FIRST
#define SWS_BUDM_UNITS                                  3030
//convRes: #define SWS_BUDM_SEND                                   SWS_BUDM_FIRST+1
#define SWS_BUDM_SEND                                   3031
//convRes: #define SWS_BUDM_CANCEL                                 SWS_BUDM_FIRST+2
#define SWS_BUDM_CANCEL                                 3032
//convRes: #define SWS_BUDM_MAINDIAL                               SWS_BUDM_FIRST+3
#define SWS_BUDM_MAINDIAL                               3033
// #define SWS_BUDM_END                                    SWS_BUDM_FIRST+4

// Battle Toolbar tooltips
// #define TTS_BTB_First               SWS_BUDM_END
//convRes: #define TTS_BTB_First             	IDS_FIRST+2040
#define TTS_BTB_First             	3040
//convRes: #define TTS_BTB_Open              	TTS_BTB_First
#define TTS_BTB_Open              	3040
//convRes: #define TTS_BTB_Save              	TTS_BTB_First+1
#define TTS_BTB_Save              	3041
//convRes: #define TTS_BTB_ZoomTo            	TTS_BTB_First+2
#define TTS_BTB_ZoomTo            	3042
//convRes: #define TTS_BTB_ZoomOverview      	TTS_BTB_First+3
#define TTS_BTB_ZoomOverview      	3043
//convRes: #define TTS_BTB_ZoomDetail        	TTS_BTB_First+4
#define TTS_BTB_ZoomDetail        	3044
//convRes: #define TTS_BTB_ZoomFourMile      	TTS_BTB_First+5
#define TTS_BTB_ZoomFourMile      	3045
//convRes: #define TTS_BTB_ZoomTwoMile       	TTS_BTB_First+6
#define TTS_BTB_ZoomTwoMile       	3046
//convRes: #define TTS_BTB_TinyMap           	TTS_BTB_First+7
#define TTS_BTB_TinyMap           	3047
//convRes: #define TTS_BTB_HexOutLine        	TTS_BTB_First+8
#define TTS_BTB_HexOutLine        	3048
//convRes: #define TTS_BTB_MessageWindow     	TTS_BTB_First+9
#define TTS_BTB_MessageWindow     	3049
//convRes: #define TTS_BTB_OBWindow          	TTS_BTB_First+10
#define TTS_BTB_OBWindow          	3050
//convRes: #define TTS_BTB_FindLeader        	TTS_BTB_First+11
#define TTS_BTB_FindLeader        	3051
//convRes: #define TTS_BTB_OHR               	TTS_BTB_First+12
#define TTS_BTB_OHR               	3052
//convRes: #define TTS_BTB_Help              	TTS_BTB_First+13
#define TTS_BTB_Help              	3053
//convRes: #define TTS_BTB_Last              	TTS_BTB_First+14
#define TTS_BTB_Last              	3054
//convRes: #define TTS_BTB_TrackingWindow    	TTS_BTB_First+15
#define TTS_BTB_TrackingWindow    	3055

// #define IDS_BMSG_FIRST                  TTS_BTB_First+30
//convRes: #define IDS_BMSG_FIRST                  IDS_FIRST+2100
#define IDS_BMSG_FIRST                  3100
// combat related messages
//convRes: #define IDS_BMSG_ENGAGINGWITHARTILLERY  IDS_BMSG_FIRST
#define IDS_BMSG_ENGAGINGWITHARTILLERY  3100
//convRes: #define IDS_BMSG_ENGAGINGWITHMUSKETS    IDS_BMSG_FIRST+1
#define IDS_BMSG_ENGAGINGWITHMUSKETS    3101
//convRes: #define IDS_BMSG_TAKINGARTILLERYFIRE    IDS_BMSG_FIRST+2
#define IDS_BMSG_TAKINGARTILLERYFIRE    3102
//convRes: #define IDS_BMSG_TAKINGMUSKETFIRE       IDS_BMSG_FIRST+3
#define IDS_BMSG_TAKINGMUSKETFIRE       3103
//convRes: #define IDS_BMSG_INTOCLOSECOMBAT        IDS_BMSG_FIRST+4
#define IDS_BMSG_INTOCLOSECOMBAT        3104
//convRes: #define IDS_BMSG_TAKINGLOSSES           IDS_BMSG_FIRST+5
#define IDS_BMSG_TAKINGLOSSES           3105
//convRes: #define IDS_BMSG_INFLICTINGLOSSES       IDS_BMSG_FIRST+6
#define IDS_BMSG_INFLICTINGLOSSES       3106
//convRes: #define IDS_BMSG_CAPTUREDTROPHY         IDS_BMSG_FIRST+7
#define IDS_BMSG_CAPTUREDTROPHY         3107
//convRes: #define IDS_BMSG_ENEMYCAPTUREDTROPHY    IDS_BMSG_FIRST+8
#define IDS_BMSG_ENEMYCAPTUREDTROPHY    3108
//convRes: #define IDS_BMSG_ROUTING                IDS_BMSG_FIRST+9
#define IDS_BMSG_ROUTING                3109
//convRes: #define IDS_BMSG_ENEMYROUTING           IDS_BMSG_FIRST+10
#define IDS_BMSG_ENEMYROUTING           3110
//convRes: #define IDS_BMSG_ENEMYSIGHTED           IDS_BMSG_FIRST+11
#define IDS_BMSG_ENEMYSIGHTED           3111
//convRes: #define IDS_BMSG_CARRIEDOUTORDER        IDS_BMSG_FIRST+12
#define IDS_BMSG_CARRIEDOUTORDER        3112
//convRes: #define IDS_BMSG_AVOIDINGENEMY          IDS_BMSG_FIRST+13
#define IDS_BMSG_AVOIDINGENEMY          3113
//convRes: #define IDS_BMSG_ENEMYAVOIDING          IDS_BMSG_FIRST+14
#define IDS_BMSG_ENEMYAVOIDING          3114
//convRes: #define IDS_BMSG_RETREATINGFROMENEMY    IDS_BMSG_FIRST+15
#define IDS_BMSG_RETREATINGFROMENEMY    3115
//convRes: #define IDS_BMSG_ENEMYRETREATING        IDS_BMSG_FIRST+16
#define IDS_BMSG_ENEMYRETREATING        3116
//convRes: #define IDS_BMSG_FLEEINGTHEFIELD        IDS_BMSG_FIRST+17
#define IDS_BMSG_FLEEINGTHEFIELD        3117
//convRes: #define IDS_BMSG_ENEMYFLEEING           IDS_BMSG_FIRST+18
#define IDS_BMSG_ENEMYFLEEING           3118
//convRes: #define IDS_BMSG_CRITICALLOSSREACHED    IDS_BMSG_FIRST+19
#define IDS_BMSG_CRITICALLOSSREACHED    3119
//convRes: #define IDS_BMSG_WAVERING               IDS_BMSG_FIRST+20
#define IDS_BMSG_WAVERING               3120
//convRes: #define IDS_BMSG_ENEMYWAVERING          IDS_BMSG_FIRST+21
#define IDS_BMSG_ENEMYWAVERING          3121

// leader related messages
//convRes: #define IDS_BMSG_LEADERHIT              IDS_BMSG_FIRST+30
#define IDS_BMSG_LEADERHIT              3130
//convRes: #define IDS_BMSG_HITENEMYLEADER         IDS_BMSG_FIRST+31
#define IDS_BMSG_HITENEMYLEADER         3131
//convRes: #define IDS_BMSG_TAKENCOMMAND           IDS_BMSG_FIRST+32
#define IDS_BMSG_TAKENCOMMAND           3132

// misc
//convRes: #define IDS_BMSG_OUTOFAMMO              IDS_BMSG_FIRST+50
#define IDS_BMSG_OUTOFAMMO              3150
//convRes: #define IDS_BMSG_AMMOREMAINING          IDS_BMSG_FIRST+51
#define IDS_BMSG_AMMOREMAINING          3151

// move related
//convRes: #define IDS_BMSG_UNABLETODEPLOY         IDS_BMSG_FIRST+60
#define IDS_BMSG_UNABLETODEPLOY         3160
//convRes: #define IDS_BMSG_UNABLETOFINDROUTE      IDS_BMSG_FIRST+61
#define IDS_BMSG_UNABLETOFINDROUTE      3161
//convRes: #define IDS_BMSG_REFUSINGFLANK          IDS_BMSG_FIRST+62
#define IDS_BMSG_REFUSINGFLANK          3162
//convRes: #define IDS_BMSG_CHANGINGSPFORMATION    IDS_BMSG_FIRST+63
#define IDS_BMSG_CHANGINGSPFORMATION    3163
//convRes: #define IDS_BMSG_CHANGINGXXDEPLOYMENT   IDS_BMSG_FIRST+64
#define IDS_BMSG_CHANGINGXXDEPLOYMENT   3164

// #define IDS_BMSG_LAST                   IDS_BMSG_FIRST+100

//convRes: #define IDS_BMSG_DESCRIPTION        100
#define IDS_BMSG_DESCRIPTION        100
// #define IDS_BMSG_DESCRIPTION_LAST  IDS_BMSG_LAST+IDS_BMSG_DESCRIPTION

/*
 * BattleItem::positionText in batdata\batlist.cpp
 */

// #define IDS_BATMISC           IDS_BMSG_DESCRIPTION_LAST + 1
//convRes: #define IDS_BATMISC           IDS_FIRST+2300
#define IDS_BATMISC           3300

//convRes: #define IDS_Front             IDS_BATMISC + 0
#define IDS_Front             3300
//convRes: #define IDS_RightFront        IDS_BATMISC + 1
#define IDS_RightFront        3301
//convRes: #define IDS_LeftFront         IDS_BATMISC + 2
#define IDS_LeftFront         3302
//convRes: #define IDS_RightFlank        IDS_BATMISC + 3
#define IDS_RightFlank        3303
//convRes: #define IDS_LeftFlank         IDS_BATMISC + 4
#define IDS_LeftFlank         3304
//convRes: #define IDS_Rear              IDS_BATMISC + 5
#define IDS_Rear              3305
//convRes: #define IDS_RightRear         IDS_BATMISC + 6
#define IDS_RightRear         3306
//convRes: #define IDS_LeftRear          IDS_BATMISC + 7
#define IDS_LeftRear          3307

/*
 * Batgame.cpp
 */

//convRes: #define IDS_CreatingBattleWindows                  IDS_BATMISC + 10
#define IDS_CreatingBattleWindows                  3310
//convRes: #define IDS_CreatingBattlefield                    IDS_BATMISC + 11
#define IDS_CreatingBattlefield                    3311
//convRes: #define IDS_ReadingBattlefield                     IDS_BATMISC + 12
#define IDS_ReadingBattlefield                     3312
//convRes: #define IDS_DeployingTroops                        IDS_BATMISC + 13
#define IDS_DeployingTroops                        3313
//convRes: #define IDS_ResizingMapReinforcementsArrived       IDS_BATMISC + 14
#define IDS_ResizingMapReinforcementsArrived       3314

/*
 * B_Info
 */

//convRes: #define IDS_RoadOnly          IDS_BATMISC + 15
#define IDS_RoadOnly          3315
//convRes: #define IDS_NoCost            IDS_BATMISC + 16
#define IDS_NoCost            3316
//convRes: #define IDS_Prohibited        IDS_BATMISC + 17
#define IDS_Prohibited        3317
//convRes: #define IDS_Multiplier        IDS_BATMISC + 18
#define IDS_Multiplier        3318

//convRes: #define IDS_Inf               IDS_UOP_Inf
#define IDS_Inf               1948
//convRes: #define IDS_Cav               IDS_UOP_Cav
#define IDS_Cav               1949
// #define IDS_Guns              IDS_UOP_Guns
//convRes: #define IDS_Morale            IDS_MORALE
#define IDS_Morale            2400
//convRes: #define IDS_Fatigue           IDS_UI_FATIGUE
#define IDS_Fatigue           1013
//convRes: #define IDS_Quality           IDS_BATMISC + 19
#define IDS_Quality           3319
//convRes: #define IDS_CoverColon        IDS_BATMISC + 20
#define IDS_CoverColon        3320
//convRes: #define IDS_MovementCost      IDS_BATMISC + 21
#define IDS_MovementCost      3321
//convRes: #define IDS_InfColon          IDS_UI_INF
#define IDS_InfColon          1073
//convRes: #define IDS_CavColon          IDS_UI_CAV
#define IDS_CavColon          1074
//convRes: #define IDS_ArtColon          IDS_BATMISC + 22
#define IDS_ArtColon          3322
//convRes: #define IDS_Location          IDS_BATMISC + 23
#define IDS_Location          3323
//convRes: #define IDS_Elev              IDS_BATMISC + 24
#define IDS_Elev              3324
//convRes: #define IDS_Ft                IDS_BATMISC + 25
#define IDS_Ft                3325

//convRes: #define IDS_NoMessage         IDS_BATMISC + 26
#define IDS_NoMessage         3326

//convRes: #define IDS_Hold              IDS_ORDER_HOLD
#define IDS_Hold              1781
//convRes: #define IDS_Move              IDS_BATMISC + 27
#define IDS_Move              3327
//convRes: #define IDS_RestRally         IDS_ORDER_RESTRALLY
#define IDS_RestRally         1782
//convRes: #define IDS_Attach            IDS_ORDER_ATTACH
#define IDS_Attach            1783
//convRes: #define IDS_Detach            IDS_BATMISC + 28
#define IDS_Detach            3328
//convRes: #define IDS_Bombard           IDS_BATMISC + 29
#define IDS_Bombard           3329
//convRes: #define IDS_Rout              IDS_BATMISC + 30
#define IDS_Rout              3330

//convRes: #define IDS_SoundInUse                      IDS_BATMISC + 31
#define IDS_SoundInUse                      3331
//convRes: #define IDS_InvalidParameter                IDS_BATMISC + 32
#define IDS_InvalidParameter                3332
//convRes: #define IDS_ObjectNoAggregation             IDS_BATMISC + 33
#define IDS_ObjectNoAggregation             3333
//convRes: #define IDS_NoSoundDriver                   IDS_BATMISC + 34
#define IDS_NoSoundDriver                   3334
//convRes: #define IDS_NoMemory                        IDS_BATMISC + 35
#define IDS_NoMemory                        3335
//convRes: #define IDS_DSoundNotInitialized            IDS_BATMISC + 36
#define IDS_DSoundNotInitialized            3336
//convRes: #define IDS_DSound_FunctionUnSupported      IDS_BATMISC + 37
#define IDS_DSound_FunctionUnSupported      3337
//convRes: #define IDS_DSoundError                     IDS_BATMISC + 38
#define IDS_DSoundError                     3338
//convRes: #define IDS_DSoundInternalError             IDS_BATMISC + 39
#define IDS_DSoundInternalError             3339

//convRes: #define IDS_Left                            IDS_BATMISC + 41
#define IDS_Left                            3341
//convRes: #define IDS_Right                           IDS_BATMISC + 42
#define IDS_Right                           3342

//convRes: #define IDS_File                            IDS_BATMISC + 43
#define IDS_File                            3343
//convRes: #define IDS_Zoom                            IDS_BATMISC + 44
#define IDS_Zoom                            3344
//convRes: #define IDS_MapDisplay                      IDS_BATMISC + 45
#define IDS_MapDisplay                      3345
//convRes: #define IDS_Info                            IDS_BATMISC + 46
#define IDS_Info                            3346

//convRes: #define IDS_UnitInfo                        IDS_BATMISC + 47
#define IDS_UnitInfo                        3347
//convRes: #define IDS_Formation                       IDS_BATMISC + 48
#define IDS_Formation                       3348
//convRes: #define IDS_Deployment                      IDS_BATMISC + 49
#define IDS_Deployment                      3349
//convRes: #define IDS_Maneuver                        IDS_BATMISC + 50
#define IDS_Maneuver                        3350

//convRes: #define IDS_OrdersColon                     IDS_BATMISC + 51
#define IDS_OrdersColon                     3351
//convRes: #define IDS_AggressionColon                 IDS_BATMISC + 52
#define IDS_AggressionColon                 3352
//convRes: #define IDS_PostureColon                    IDS_BATMISC + 53
#define IDS_PostureColon                    3353
//convRes: #define IDS_DeploymentColon                 IDS_BATMISC + 54
#define IDS_DeploymentColon                 3354
//convRes: #define IDS_FormationColon                  IDS_BATMISC + 55
#define IDS_FormationColon                  3355
//convRes: #define IDS_FacingColon                     IDS_BATMISC + 56
#define IDS_FacingColon                     3356
//convRes: #define IDS_DeployHowColon                  IDS_BATMISC + 57
#define IDS_DeployHowColon                  3357
//convRes: #define IDS_ManueverColon                   IDS_BATMISC + 58
#define IDS_ManueverColon                   3358

//convRes: #define IDS_Corps                           IDS_BATMISC + 59
#define IDS_Corps                           3359
//convRes: #define IDS_Divs                            IDS_BATMISC + 60
#define IDS_Divs                            3360
//convRes: #define IDS_Art                             IDS_BATMISC + 61
#define IDS_Art                             3361
//convRes: #define IDS_HQCanControlUnits               IDS_BATMISC + 62
#define IDS_HQCanControlUnits               3362

//convRes: #define IDS_TitleTroopDeployment            IDS_BATMISC + 63
#define IDS_TitleTroopDeployment            3363
//convRes: #define IDS_FinishDeployment                IDS_BATMISC + 64
#define IDS_FinishDeployment                3364
//convRes: #define IDS_TitleSelectArea                 IDS_BATMISC + 65
#define IDS_TitleSelectArea                 3365

//convRes: #define IDS_Flat                            IDS_BATMISC + 66
#define IDS_Flat                            3366
//convRes: #define IDS_Rolling                         IDS_BATMISC + 67
#define IDS_Rolling                         3367
//convRes: #define IDS_Hilly                           IDS_BATMISC + 68
#define IDS_Hilly                           3368
//convRes: #define IDS_Steep                           IDS_BATMISC + 69
#define IDS_Steep                           3369
//convRes: #define IDS_Sparse                          IDS_BATMISC + 70
#define IDS_Sparse                          3370
//convRes: #define IDS_Moderate                        IDS_BATMISC + 71
#define IDS_Moderate                        3371
//convRes: #define IDS_High                            IDS_BATMISC + 72
#define IDS_High                            3372
//convRes: #define IDS_Dense                           IDS_BATMISC + 73
#define IDS_Dense                           3373

//convRes: #define IDS_SunnyDry                        IDS_BATMISC + 74
#define IDS_SunnyDry                        3374
//convRes: #define IDS_SunnyMuddy                      IDS_BATMISC + 75
#define IDS_SunnyMuddy                      3375
//convRes: #define IDS_RainyDry                        IDS_BATMISC + 76
#define IDS_RainyDry                        3376
//convRes: #define IDS_RainyMuddy                      IDS_BATMISC + 77
#define IDS_RainyMuddy                      3377
//convRes: #define IDS_StormyDry                       IDS_BATMISC + 78
#define IDS_StormyDry                       3378
//convRes: #define IDS_StormyMuddy                     IDS_BATMISC + 79
#define IDS_StormyMuddy                     3379

//convRes: #define IDS_SelectBattlefield               IDS_BATMISC + 80
#define IDS_SelectBattlefield               3380
//convRes: #define IDS_Battlefield                     IDS_BATMISC + 81
#define IDS_Battlefield                     3381
//convRes: #define IDS_Region                          IDS_BATMISC + 82
#define IDS_Region                          3382
//convRes: #define IDS_Elevation                       IDS_BATMISC + 83
#define IDS_Elevation                       3383
//convRes: #define IDS_Population                      IDS_BATMISC + 84
#define IDS_Population                      3384
//convRes: #define IDS_Date                            IDS_BATMISC + 85
#define IDS_Date                            3385
//convRes: #define IDS_Weather                         IDS_BATMISC + 86
#define IDS_Weather                         3386
//convRes: #define IDS_Armies                          IDS_BATMISC + 87
#define IDS_Armies                          3387

//convRes: #define IDS_BadDisplaySetting               IDS_BATMISC + 88
#define IDS_BadDisplaySetting               3388
//convRes: #define IDS_ChangeDisplaySettings           IDS_BATMISC + 89
#define IDS_ChangeDisplaySettings           3389
//convRes: #define IDS_OnArrival                       IDS_BATMISC + 90
#define IDS_OnArrival                       3390

//convRes: #define IDS_BlowingBridge                   IDS_BATMISC + 91
#define IDS_BlowingBridge                   3391
//convRes: #define IDS_RepairingBridge                 IDS_BATMISC + 92
#define IDS_RepairingBridge                 3392
//convRes: #define IDS_RemovingDepot                   IDS_BATMISC + 93
#define IDS_RemovingDepot                   3393

//convRes: #define IDS_LoadingCampData                 IDS_BATMISC + 94
#define IDS_LoadingCampData                 3394
//convRes: #define IDS_CreatingCampWind                IDS_BATMISC + 95
#define IDS_CreatingCampWind                3395
//convRes: #define IDS_SureAbandonGame                 IDS_BATMISC + 96
#define IDS_SureAbandonGame                 3396
//convRes: #define IDS_AbandonGame                     IDS_BATMISC + 97
#define IDS_AbandonGame                     3397
//convRes: #define IDS_Calculated                      IDS_BATMISC + 98
#define IDS_Calculated                      3398
//convRes: #define IDS_Tactical                        IDS_BATMISC + 99
#define IDS_Tactical                        3399
//convRes: #define IDS_Withdraw                        IDS_BATMISC + 100
#define IDS_Withdraw                        3400
//convRes: #define IDS_Continue                        IDS_BATMISC + 101
#define IDS_Continue                        3401
//convRes: #define IDS_ChooseBattleMethod              IDS_BATMISC + 102
#define IDS_ChooseBattleMethod              3402
//convRes: #define IDS_VersusAbrev                     IDS_BATMISC + 103
#define IDS_VersusAbrev                     3403
//convRes: #define IDS_BattleDayEnds                   IDS_BATMISC + 104
#define IDS_BattleDayEnds                   3404
//convRes: #define IDS_BattleOfTown                    IDS_BATMISC + 105
#define IDS_BattleOfTown                    3405
//convRes: #define IDS_BattleResults                   IDS_BATMISC + 106
#define IDS_BattleResults                   3406
//convRes: #define IDS_Losses                          IDS_BATMISC + 107
#define IDS_Losses                          3407
//convRes: #define IDS_StartStrength                   IDS_BATMISC + 108
#define IDS_StartStrength                   3408
//convRes: #define IDS_CurrentStrength                 IDS_BATMISC + 109
#define IDS_CurrentStrength                 3409
//convRes: #define IDS_TotalLosses                     IDS_BATMISC + 110
#define IDS_TotalLosses                     3410
//convRes: #define IDS_GunLosses                       IDS_BATMISC + 111
#define IDS_GunLosses                       3411
//convRes: #define IDS_Total                           IDS_BATMISC + 112
#define IDS_Total                           3412
//convRes: #define IDS_EstimatedLosses                 IDS_BATMISC + 113
#define IDS_EstimatedLosses                 3413
//convRes: #define IDS_Men                             IDS_BATMISC + 114
#define IDS_Men                             3414
//convRes: #define IDS_LeaderCasualtyReport            IDS_BATMISC + 115
#define IDS_LeaderCasualtyReport            3415
//convRes: #define IDS_CapturedLeaders                 IDS_BATMISC + 116
#define IDS_CapturedLeaders                 3416
//convRes: #define IDS_Type                            IDS_BATMISC + 117
#define IDS_Type                            3417
//convRes: #define IDS_Find                            IDS_BATMISC + 118
#define IDS_Find                            3418
//convRes: #define IDS_Command                         IDS_BATMISC + 119
#define IDS_Command                         3419
//convRes: #define IDS_SearchFor                       IDS_BATMISC + 120
#define IDS_SearchFor                       3420
//convRes: #define IDS_Rank                            IDS_BATMISC + 121
#define IDS_Rank                            3421
//convRes: #define IDS_OB_Window                       IDS_BATMISC + 122
#define IDS_OB_Window                       3422
//convRes: #define IDS_SureDetachUnit                  IDS_BATMISC + 123
#define IDS_SureDetachUnit                  3423
//convRes: #define IDS_DetachUnits                     IDS_BATMISC + 124
#define IDS_DetachUnits                     3424
//convRes: #define IDS_DetachUnit                      IDS_BATMISC + 125
#define IDS_DetachUnit                      3425
//convRes: #define IDS_NoLeader                        IDS_BATMISC + 126
#define IDS_NoLeader                        3426
//convRes: #define IDS_General                         IDS_BATMISC + 127
#define IDS_General                         3427
//convRes: #define IDS_ReorganizeOB                    IDS_BATMISC + 128
#define IDS_ReorganizeOB                    3428
//convRes: #define IDS_AnItem                          IDS_BATMISC + 129
#define IDS_AnItem                          3429
//convRes: #define IDS_Transfer                        IDS_BATMISC + 130
#define IDS_Transfer                        3430
//convRes: #define IDS_Merge                           IDS_BATMISC + 131
#define IDS_Merge                           3431
//convRes: #define IDS_InfoAbout                       IDS_BATMISC + 132
#define IDS_InfoAbout                       3432
//convRes: #define IDS_Root                            IDS_BATMISC + 133
#define IDS_Root                            3433
//convRes: #define IDS_NewCommand                      IDS_BATMISC + 134
#define IDS_NewCommand                      3434
//convRes: #define IDS_NewLeader                       IDS_BATMISC + 135
#define IDS_NewLeader                       3435
//convRes: #define IDS_NewStrengthPoint                IDS_BATMISC + 136
#define IDS_NewStrengthPoint                3436
//convRes: #define IDS_IndependentLeaders              IDS_BATMISC + 137
#define IDS_IndependentLeaders              3437
//convRes: #define IDS_AvailableAt                     IDS_BATMISC + 138
#define IDS_AvailableAt                     3438
//convRes: #define IDS_TravellingTo                    IDS_BATMISC + 139
#define IDS_TravellingTo                    3439
//convRes: #define IDS_Hours                           IDS_BATMISC + 140
#define IDS_Hours                           3440
//convRes: #define IDS_SHQ                             IDS_BATMISC + 141
#define IDS_SHQ                             3441
//convRes: #define IDS_TacticalAbility                 IDS_BATMISC + 142
#define IDS_TacticalAbility                 3442
//convRes: #define IDS_CommandAbility                  IDS_BATMISC + 143
#define IDS_CommandAbility                  3443
//convRes: #define IDS_SiegeOf                         IDS_BATMISC + 144
#define IDS_SiegeOf                         3444
//convRes: #define IDS_UnderSiege                      IDS_BATMISC + 145
#define IDS_UnderSiege                      3445
//convRes: #define IDS_ReplacementPool                 IDS_BATMISC + 146
#define IDS_ReplacementPool                 3446
//convRes: #define IDS_Attached                        IDS_BATMISC + 147
#define IDS_Attached                        3447
//convRes: #define IDS_BaseMorale                      IDS_BATMISC + 148
#define IDS_BaseMorale                      3448
//convRes: #define IDS_Amount                          IDS_BATMISC + 149
#define IDS_Amount                          3449
//convRes: #define IDS_Available                       IDS_BATMISC + 150
#define IDS_Available                       3450

//convRes: #define IDS_CampaignResults                 IDS_BATMISC + 151
#define IDS_CampaignResults                 3451
//convRes: #define IDS_InfantryLosses                  IDS_BATMISC + 152
#define IDS_InfantryLosses                  3452
//convRes: #define IDS_ArtilleryLosses                 IDS_BATMISC + 153
#define IDS_ArtilleryLosses                 3453
//convRes: #define IDS_CavalryLosses                   IDS_BATMISC + 154
#define IDS_CavalryLosses                   3454
//convRes: #define IDS_OtherLosses                     IDS_BATMISC + 155
#define IDS_OtherLosses                     3455

//convRes: #define IDS_Draw                            IDS_BATMISC + 156
#define IDS_Draw                            3456
//convRes: #define IDS_WinningDraw                     IDS_BATMISC + 157
#define IDS_WinningDraw                     3457
//convRes: #define IDS_MinorVictory                    IDS_BATMISC + 158
#define IDS_MinorVictory                    3458
//convRes: #define IDS_MajorVictory                    IDS_BATMISC + 159
#define IDS_MajorVictory                    3459
//convRes: #define IDS_CrushingVictory                 IDS_BATMISC + 160
#define IDS_CrushingVictory                 3460
//convRes: #define IDS_MinorDefeat                     IDS_BATMISC + 161
#define IDS_MinorDefeat                     3461
//convRes: #define IDS_MajorDefeat                     IDS_BATMISC + 162
#define IDS_MajorDefeat                     3462
//convRes: #define IDS_CrushingDefeat                  IDS_BATMISC + 163
#define IDS_CrushingDefeat                  3463
//convRes: #define IDS_Engineers                       IDS_BATMISC + 164
#define IDS_Engineers                       3464

//convRes: #define IDS_TITLE_ChooseCampaign            IDS_BATMISC + 165
#define IDS_TITLE_ChooseCampaign            3465
//convRes: #define IDS_TITLE_ChooseSides               IDS_BATMISC + 166
#define IDS_TITLE_ChooseSides               3466
//convRes: #define IDS_TITLE_GameOptions               IDS_BATMISC + 167
#define IDS_TITLE_GameOptions               3467
//convRes: #define IDS_TITLE_ChooseBattle              IDS_BATMISC + 168
#define IDS_TITLE_ChooseBattle              3468
//convRes: #define IDS_TITLE_LoadSavedGame             IDS_BATMISC + 169
#define IDS_TITLE_LoadSavedGame             3469
//convRes: #define IDS_TITLE_PlayLastGame              IDS_BATMISC + 170
#define IDS_TITLE_PlayLastGame              3470

//convRes: #define IDS_Scenario                        IDS_BATMISC + 171
#define IDS_Scenario                        3471
//convRes: #define IDS_NoGamePlayed                    IDS_BATMISC + 172
#define IDS_NoGamePlayed                    3472
//convRes: #define IDS_NoGame                          IDS_BATMISC + 173
#define IDS_NoGame                          3473

//convRes: #define IDS_SelectDifficultyLevel           IDS_BATMISC + 174
#define IDS_SelectDifficultyLevel           3474
//convRes: #define IDS_CustomGameSettings              IDS_BATMISC + 175
#define IDS_CustomGameSettings              3475
//convRes: #define IDS_Novice                          IDS_BATMISC + 176
#define IDS_Novice                          3476
//convRes: #define IDS_Normal                          IDS_BATMISC + 177
#define IDS_Normal                          3477
//convRes: #define IDS_Expert                          IDS_BATMISC + 178
#define IDS_Expert                          3478

//convRes: #define IDS_Battle                          IDS_BATMISC + 179
#define IDS_Battle                          3479

//convRes: #define IDS_Version                         IDS_BATMISC + 180
#define IDS_Version                         3480
//convRes: #define IDS_CompiledOn                      IDS_BATMISC + 181
#define IDS_CompiledOn                      3481
//convRes: #define IDS_At                              IDS_BATMISC + 182
#define IDS_At                              3482
//convRes: #define IDS_Copyright                       IDS_BATMISC + 183
#define IDS_Copyright                       3483

//convRes: #define IDS_CampaignOptions                 IDS_BATMISC + 184
#define IDS_CampaignOptions                 3484
//convRes: #define IDS_GeneralOptions                  IDS_BATMISC + 185
#define IDS_GeneralOptions                  3485
//convRes: #define IDS_BattleOptions                   IDS_BATMISC + 186
#define IDS_BattleOptions                   3486
//convRes: #define IDS_PlayerSettings                  IDS_BATMISC + 187
#define IDS_PlayerSettings                  3487
//convRes: #define IDS_SureExitGame                    IDS_BATMISC + 188
#define IDS_SureExitGame                    3488
//convRes: #define IDS_ExitGame                        IDS_BATMISC + 189
#define IDS_ExitGame                        3489
//convRes: #define IDS_MissingFile                     IDS_BATMISC + 190
#define IDS_MissingFile                     3490
//convRes: #define IDS_HelpFileNotAvailable            IDS_BATMISC + 191
#define IDS_HelpFileNotAvailable            3491
//convRes: #define IDS_ReferenceNotAvailable           IDS_BATMISC + 192
#define IDS_ReferenceNotAvailable           3492
//convRes: #define IDS_Miles                           IDS_BATMISC + 193
#define IDS_Miles                           3493
//convRes: #define IDS_SideArmy                        IDS_BATMISC + 194
#define IDS_SideArmy                        3494
//convRes: #define IDS_FileNotFound                    IDS_BATMISC + 195
#define IDS_FileNotFound                    3495
//convRes: #define IDS_BrowseFolders                   IDS_BATMISC + 196
#define IDS_BrowseFolders                   3496
//convRes: #define IDS_NoCDAudio                       IDS_BATMISC + 197
#define IDS_NoCDAudio                       3497
//convRes: #define IDS_MCIError                        IDS_BATMISC + 198
#define IDS_MCIError                        3498

//convRes: #define IDS_ConvertingOB                    IDS_BATMISC + 199
#define IDS_ConvertingOB                    3499
//convRes: #define IDS_BattleNear                      IDS_BATMISC + 200
#define IDS_BattleNear                      3500
//convRes: #define IDS_Ratio                           IDS_BATMISC + 201
#define IDS_Ratio                           3501
//convRes: #define IDS_RUSureSurrenderBattle           IDS_BATMISC + 202
#define IDS_RUSureSurrenderBattle           3502
//convRes: #define IDS_SurrenderBattle                 IDS_BATMISC + 203
#define IDS_SurrenderBattle                 3503

// #define IDS_PATH1             IDS_BATMISC + 300
//convRes: #define IDS_PATH1             IDS_FIRST+2600
#define IDS_PATH1             3600
// #define IDS_PATH1             3000
//convRes: #define IDS_PATH1_ABREV       IDS_PATH1 + 1
#define IDS_PATH1_ABREV       3601
//convRes: #define IDS_PATH2             IDS_PATH1 + 2
#define IDS_PATH2             3602
//convRes: #define IDS_PATH2_ABREV       IDS_PATH1 + 3
#define IDS_PATH2_ABREV       3603
//convRes: #define IDS_PATH3             IDS_PATH1 + 4
#define IDS_PATH3             3604
//convRes: #define IDS_PATH3_ABREV       IDS_PATH1 + 5
#define IDS_PATH3_ABREV       3605
//convRes: #define IDS_PATH4             IDS_PATH1 + 6
#define IDS_PATH4             3606
//convRes: #define IDS_PATH4_ABREV       IDS_PATH1 + 7
#define IDS_PATH4_ABREV       3607
//convRes: #define IDS_PATH5             IDS_PATH1 + 8
#define IDS_PATH5             3608
//convRes: #define IDS_PATH5_ABREV       IDS_PATH1 + 9
#define IDS_PATH5_ABREV       3609
//convRes: #define IDS_PATH6             IDS_PATH1 + 10
#define IDS_PATH6             3610
//convRes: #define IDS_PATH6_ABREV       IDS_PATH1 + 11
#define IDS_PATH6_ABREV       3611
//convRes: #define IDS_PATH7             IDS_PATH1 + 12
#define IDS_PATH7             3612
//convRes: #define IDS_PATH7_ABREV       IDS_PATH1 + 13
#define IDS_PATH7_ABREV       3613
//convRes: #define IDS_PATH8             IDS_PATH1 + 14
#define IDS_PATH8             3614
//convRes: #define IDS_PATH8_ABREV       IDS_PATH1 + 15
#define IDS_PATH8_ABREV       3615
//convRes: #define IDS_PATH9             IDS_PATH1 + 16
#define IDS_PATH9             3616
//convRes: #define IDS_PATH9_ABREV       IDS_PATH1 + 17
#define IDS_PATH9_ABREV       3617
//convRes: #define IDS_PATH10            IDS_PATH1 + 18
#define IDS_PATH10            3618
//convRes: #define IDS_PATH10_ABREV       IDS_PATH1 + 19
#define IDS_PATH10_ABREV       3619

// #define IDS_TERRAIN_1         IDS_PATH1 + 20
//convRes: #define IDS_TERRAIN_1         IDS_FIRST+2620
#define IDS_TERRAIN_1         3620
//convRes: #define IDS_TERRAIN_2         IDS_TERRAIN_1 + 1
#define IDS_TERRAIN_2         3621
//convRes: #define IDS_TERRAIN_3         IDS_TERRAIN_1 + 2
#define IDS_TERRAIN_3         3622
//convRes: #define IDS_TERRAIN_4         IDS_TERRAIN_1 + 3
#define IDS_TERRAIN_4         3623
//convRes: #define IDS_TERRAIN_5         IDS_TERRAIN_1 + 4
#define IDS_TERRAIN_5         3624
//convRes: #define IDS_TERRAIN_6         IDS_TERRAIN_1 + 5
#define IDS_TERRAIN_6         3625
//convRes: #define IDS_TERRAIN_7         IDS_TERRAIN_1 + 6
#define IDS_TERRAIN_7         3626
//convRes: #define IDS_TERRAIN_8         IDS_TERRAIN_1 + 7
#define IDS_TERRAIN_8         3627
//convRes: #define IDS_TERRAIN_9         IDS_TERRAIN_1 + 8
#define IDS_TERRAIN_9         3628
//convRes: #define IDS_TERRAIN_10        IDS_TERRAIN_1 + 9
#define IDS_TERRAIN_10        3629
//convRes: #define IDS_TERRAIN_11        IDS_TERRAIN_1 + 10
#define IDS_TERRAIN_11        3630
//convRes: #define IDS_TERRAIN_12        IDS_TERRAIN_1 + 11
#define IDS_TERRAIN_12        3631
//convRes: #define IDS_TERRAIN_13        IDS_TERRAIN_1 + 12
#define IDS_TERRAIN_13        3632
//convRes: #define IDS_TERRAIN_14        IDS_TERRAIN_1 + 13
#define IDS_TERRAIN_14        3633
//convRes: #define IDS_TERRAIN_15        IDS_TERRAIN_1 + 14
#define IDS_TERRAIN_15        3634

// #define IDS_EDGE_1            IDS_TERRAIN_1 + 15
//convRes: #define IDS_EDGE_1            IDS_FIRST+2635
#define IDS_EDGE_1            3635
//convRes: #define IDS_EDGE_2            IDS_EDGE_1 + 1
#define IDS_EDGE_2            3636
//convRes: #define IDS_EDGE_3            IDS_EDGE_1 + 2
#define IDS_EDGE_3            3637

// #define IDS_BAT_AGRESSION     IDS_EDGE_1 + 3
//convRes: #define IDS_BAT_AGRESSION     IDS_FIRST+2640
#define IDS_BAT_AGRESSION     3640
//convRes: #define IDS_BAT_AGRESSION_1   IDS_BAT_AGRESSION + 0
#define IDS_BAT_AGRESSION_1   3640
//convRes: #define IDS_BAT_AGRESSION_2   IDS_BAT_AGRESSION + 1
#define IDS_BAT_AGRESSION_2   3641
//convRes: #define IDS_BAT_AGRESSION_3   IDS_BAT_AGRESSION + 2
#define IDS_BAT_AGRESSION_3   3642
//convRes: #define IDS_BAT_AGRESSION_4   IDS_BAT_AGRESSION + 3
#define IDS_BAT_AGRESSION_4   3643

// #define IDS_BAT_SP_FORMATION  IDS_BAT_AGRESSION + 4
//convRes: #define IDS_BAT_SP_FORMATION  IDS_FIRST+2650
#define IDS_BAT_SP_FORMATION  3650
//convRes: #define IDS_BAT_SP_FORMATION_1  IDS_BAT_SP_FORMATION + 0
#define IDS_BAT_SP_FORMATION_1  3650
//convRes: #define IDS_BAT_SP_FORMATION_2  IDS_BAT_SP_FORMATION + 1
#define IDS_BAT_SP_FORMATION_2  3651
//convRes: #define IDS_BAT_SP_FORMATION_3  IDS_BAT_SP_FORMATION + 2
#define IDS_BAT_SP_FORMATION_3  3652
//convRes: #define IDS_BAT_SP_FORMATION_4  IDS_BAT_SP_FORMATION + 3
#define IDS_BAT_SP_FORMATION_4  3653
//convRes: #define IDS_BAT_SP_FORMATION_5  IDS_BAT_SP_FORMATION + 4
#define IDS_BAT_SP_FORMATION_5  3654

// #define IDS_BAT_ART_FORMATION  IDS_BAT_SP_FORMATION + 5
//convRes: #define IDS_BAT_ART_FORMATION  	IDS_FIRST+2660
#define IDS_BAT_ART_FORMATION  	3660
//convRes: #define IDS_BAT_ART_FORMATION_1  IDS_BAT_ART_FORMATION + 0
#define IDS_BAT_ART_FORMATION_1  3660
//convRes: #define IDS_BAT_ART_FORMATION_2  IDS_BAT_ART_FORMATION + 1
#define IDS_BAT_ART_FORMATION_2  3661
//convRes: #define IDS_BAT_ART_FORMATION_3  IDS_BAT_ART_FORMATION + 2
#define IDS_BAT_ART_FORMATION_3  3662

// #define IDS_BAT_DIV_FORMATION IDS_BAT_ART_FORMATION + 3
//convRes: #define IDS_BAT_DIV_FORMATION 	IDS_FIRST+2670
#define IDS_BAT_DIV_FORMATION 	3670
//convRes: #define IDS_BAT_DIV_FORMATION_1  IDS_BAT_DIV_FORMATION + 0
#define IDS_BAT_DIV_FORMATION_1  3670
//convRes: #define IDS_BAT_DIV_FORMATION_2  IDS_BAT_DIV_FORMATION + 1
#define IDS_BAT_DIV_FORMATION_2  3671
//convRes: #define IDS_BAT_DIV_FORMATION_3  IDS_BAT_DIV_FORMATION + 2
#define IDS_BAT_DIV_FORMATION_3  3672
//convRes: #define IDS_BAT_DIV_FORMATION_4  IDS_BAT_DIV_FORMATION + 3
#define IDS_BAT_DIV_FORMATION_4  3673

// #define IDS_BAT_DEPLOY        IDS_BAT_DIV_FORMATION + 4
//convRes: #define IDS_BAT_DEPLOY        IDS_FIRST+2680
#define IDS_BAT_DEPLOY        3680
//convRes: #define IDS_BAT_DEPLOY_1      IDS_BAT_DEPLOY + 0
#define IDS_BAT_DEPLOY_1      3680
//convRes: #define IDS_BAT_DEPLOY_2      IDS_BAT_DEPLOY + 1
#define IDS_BAT_DEPLOY_2      3681
//convRes: #define IDS_BAT_DEPLOY_3      IDS_BAT_DEPLOY + 2
#define IDS_BAT_DEPLOY_3      3682


// #define IDS_BAT_MANUEVRE      IDS_BAT_DEPLOY + 3
//convRes: #define IDS_BAT_MANUEVRE      IDS_FIRST+2690
#define IDS_BAT_MANUEVRE      3690
//convRes: #define IDS_BAT_MANUEVRE_1    IDS_BAT_MANUEVRE + 0
#define IDS_BAT_MANUEVRE_1    3690
//convRes: #define IDS_BAT_MANUEVRE_2    IDS_BAT_MANUEVRE + 1
#define IDS_BAT_MANUEVRE_2    3691
//convRes: #define IDS_BAT_MANUEVRE_3    IDS_BAT_MANUEVRE + 2
#define IDS_BAT_MANUEVRE_3    3692
//convRes: #define IDS_BAT_MANUEVRE_4    IDS_BAT_MANUEVRE + 3
#define IDS_BAT_MANUEVRE_4    3693
//convRes: #define IDS_BAT_MANUEVRE_5    IDS_BAT_MANUEVRE + 4
#define IDS_BAT_MANUEVRE_5    3694
//convRes: #define IDS_BAT_MANUEVRE_6    IDS_BAT_MANUEVRE + 5
#define IDS_BAT_MANUEVRE_6    3695
//convRes: #define IDS_BAT_MANUEVRE_7    IDS_BAT_MANUEVRE + 6
#define IDS_BAT_MANUEVRE_7    3696
//convRes: #define IDS_BAT_MANUEVRE_8    IDS_BAT_MANUEVRE + 7
#define IDS_BAT_MANUEVRE_8    3697
//convRes: #define IDS_BAT_MANUEVRE_9    IDS_BAT_MANUEVRE + 8
#define IDS_BAT_MANUEVRE_9    3698

// #define IDS_BAT_LEADERLOSS    IDS_BAT_MANUEVRE + 9
//convRes: #define IDS_BAT_LEADERLOSS    IDS_FIRST+2700
#define IDS_BAT_LEADERLOSS    3700
//convRes: #define IDS_BAT_LEADERLOSS_1  IDS_BAT_LEADERLOSS + 0
#define IDS_BAT_LEADERLOSS_1  3700
//convRes: #define IDS_BAT_LEADERLOSS_2  IDS_BAT_LEADERLOSS + 1
#define IDS_BAT_LEADERLOSS_2  3701
//convRes: #define IDS_BAT_LEADERLOSS_3  IDS_BAT_LEADERLOSS + 2
#define IDS_BAT_LEADERLOSS_3  3702
//convRes: #define IDS_BAT_LEADERLOSS_4  IDS_BAT_LEADERLOSS + 3
#define IDS_BAT_LEADERLOSS_4  3703
//convRes: #define IDS_BAT_LEADERLOSS_5  IDS_BAT_LEADERLOSS + 4
#define IDS_BAT_LEADERLOSS_5  3704

// #define IDS_BAT_COVERTYPE     IDS_BAT_LEADERLOSS + 5
//convRes: #define IDS_BAT_COVERTYPE     IDS_FIRST+2710
#define IDS_BAT_COVERTYPE     3710
//convRes: #define IDS_BAT_COVERTYPE_1   IDS_BAT_COVERTYPE + 0
#define IDS_BAT_COVERTYPE_1   3710
//convRes: #define IDS_BAT_COVERTYPE_2   IDS_BAT_COVERTYPE + 1
#define IDS_BAT_COVERTYPE_2   3711
//convRes: #define IDS_BAT_COVERTYPE_3   IDS_BAT_COVERTYPE + 2
#define IDS_BAT_COVERTYPE_3   3712
//convRes: #define IDS_BAT_COVERTYPE_4   IDS_BAT_COVERTYPE + 3
#define IDS_BAT_COVERTYPE_4   3713
//convRes: #define IDS_BAT_COVERTYPE_5   IDS_BAT_COVERTYPE + 4
#define IDS_BAT_COVERTYPE_5   3714

// #define IDS_CAMP_TERRAIN      IDS_BAT_COVERTYPE + 5
//convRes: #define IDS_CAMP_TERRAIN      IDS_FIRST+2720
#define IDS_CAMP_TERRAIN      3720
//convRes: #define IDS_CAMP_TERRAIN_1    IDS_CAMP_TERRAIN + 0
#define IDS_CAMP_TERRAIN_1    3720
//convRes: #define IDS_CAMP_TERRAIN_2    IDS_CAMP_TERRAIN + 1
#define IDS_CAMP_TERRAIN_2    3721
//convRes: #define IDS_CAMP_TERRAIN_3    IDS_CAMP_TERRAIN + 2
#define IDS_CAMP_TERRAIN_3    3722
//convRes: #define IDS_CAMP_TERRAIN_4    IDS_CAMP_TERRAIN + 3
#define IDS_CAMP_TERRAIN_4    3723
//convRes: #define IDS_CAMP_TERRAIN_5    IDS_CAMP_TERRAIN + 4
#define IDS_CAMP_TERRAIN_5    3724
//convRes: #define IDS_CAMP_TERRAIN_6    IDS_CAMP_TERRAIN + 5
#define IDS_CAMP_TERRAIN_6    3725
//convRes: #define IDS_CAMP_TERRAIN_7    IDS_CAMP_TERRAIN + 6
#define IDS_CAMP_TERRAIN_7    3726

// #define IDS_CAMP_RIVER        IDS_CAMP_TERRAIN + 7
//convRes: #define IDS_CAMP_RIVER        IDS_FIRST+2730
#define IDS_CAMP_RIVER        3730
//convRes: #define IDS_CAMP_RIVER_1      IDS_CAMP_RIVER + 0
#define IDS_CAMP_RIVER_1      3730
//convRes: #define IDS_CAMP_RIVER_2      IDS_CAMP_RIVER + 1
#define IDS_CAMP_RIVER_2      3731
//convRes: #define IDS_CAMP_RIVER_3      IDS_CAMP_RIVER + 2
#define IDS_CAMP_RIVER_3      3732

// #define IDS_CBAT_SITUATION    IDS_CAMP_RIVER + 3
//convRes: #define IDS_CBAT_SITUATION    IDS_FIRST+2740
#define IDS_CBAT_SITUATION    3740
//convRes: #define IDS_CBAT_SITUATION_1  IDS_CBAT_SITUATION + 0
#define IDS_CBAT_SITUATION_1  3740
//convRes: #define IDS_CBAT_SITUATION_2  IDS_CBAT_SITUATION + 1
#define IDS_CBAT_SITUATION_2  3741
//convRes: #define IDS_CBAT_SITUATION_3  IDS_CBAT_SITUATION + 2
#define IDS_CBAT_SITUATION_3  3742
//convRes: #define IDS_CBAT_SITUATION_4  IDS_CBAT_SITUATION + 3
#define IDS_CBAT_SITUATION_4  3743
//convRes: #define IDS_CBAT_SITUATION_5  IDS_CBAT_SITUATION + 4
#define IDS_CBAT_SITUATION_5  3744

// #define IDS_CBR_SITUATION     IDS_CBAT_SITUATION + 5
//convRes: #define IDS_CBR_SITUATION     IDS_FIRST+2750
#define IDS_CBR_SITUATION     3750
//convRes: #define IDS_CBR_SITUATION_1   IDS_CBR_SITUATION + 0
#define IDS_CBR_SITUATION_1   3750
//convRes: #define IDS_CBR_SITUATION_2   IDS_CBR_SITUATION + 1
#define IDS_CBR_SITUATION_2   3751
//convRes: #define IDS_CBR_SITUATION_3   IDS_CBR_SITUATION + 2
#define IDS_CBR_SITUATION_3   3752
//convRes: #define IDS_CBR_SITUATION_4   IDS_CBR_SITUATION + 3
#define IDS_CBR_SITUATION_4   3753
//convRes: #define IDS_CBR_SITUATION_5   IDS_CBR_SITUATION + 4
#define IDS_CBR_SITUATION_5   3754

// #define IDS_CBR_OUTCOME       IDS_CBR_SITUATION + 5
//convRes: #define IDS_CBR_OUTCOME       IDS_FIRST+2760
#define IDS_CBR_OUTCOME       3760
//convRes: #define IDS_CBR_OUTCOME_1     IDS_CBR_OUTCOME + 0
#define IDS_CBR_OUTCOME_1     3760
//convRes: #define IDS_CBR_OUTCOME_2     IDS_CBR_OUTCOME + 1
#define IDS_CBR_OUTCOME_2     3761
//convRes: #define IDS_CBR_OUTCOME_3     IDS_CBR_OUTCOME + 2
#define IDS_CBR_OUTCOME_3     3762
//convRes: #define IDS_CBR_OUTCOME_4     IDS_CBR_OUTCOME + 3
#define IDS_CBR_OUTCOME_4     3763
//convRes: #define IDS_CBR_OUTCOME_5     IDS_CBR_OUTCOME + 4
#define IDS_CBR_OUTCOME_5     3764

// #define IDS_CBR_LEADERHIT     IDS_CBR_OUTCOME + 5
//convRes: #define IDS_CBR_LEADERHIT     IDS_FIRST+2770
#define IDS_CBR_LEADERHIT     3770
//convRes: #define IDS_CBR_LEADERHIT_1   IDS_CBR_LEADERHIT + 0
#define IDS_CBR_LEADERHIT_1   3770
//convRes: #define IDS_CBR_LEADERHIT_2   IDS_CBR_LEADERHIT + 1
#define IDS_CBR_LEADERHIT_2   3771
//convRes: #define IDS_CBR_LEADERHIT_3   IDS_CBR_LEADERHIT + 2
#define IDS_CBR_LEADERHIT_3   3772
//convRes: #define IDS_CBR_LEADERHIT_4   IDS_CBR_LEADERHIT + 3
#define IDS_CBR_LEADERHIT_4   3773
//convRes: #define IDS_CBR_LEADERHIT_5   IDS_CBR_LEADERHIT + 4
#define IDS_CBR_LEADERHIT_5   3774
//convRes: #define IDS_CBR_LEADERHIT_6   IDS_CBR_LEADERHIT + 5
#define IDS_CBR_LEADERHIT_6   3775

// #define IDS_SIEGE_SITUATION   IDS_CBR_LEADERHIT + 6
//convRes: #define IDS_SIEGE_SITUATION   IDS_FIRST+2780
#define IDS_SIEGE_SITUATION   3780
//convRes: #define IDS_SIEGE_SITUATION_1 IDS_SIEGE_SITUATION + 0
#define IDS_SIEGE_SITUATION_1 3780
//convRes: #define IDS_SIEGE_SITUATION_2 IDS_SIEGE_SITUATION + 1
#define IDS_SIEGE_SITUATION_2 3781
//convRes: #define IDS_SIEGE_SITUATION_3 IDS_SIEGE_SITUATION + 2
#define IDS_SIEGE_SITUATION_3 3782
//convRes: #define IDS_SIEGE_SITUATION_4 IDS_SIEGE_SITUATION + 3
#define IDS_SIEGE_SITUATION_4 3783
//convRes: #define IDS_SIEGE_SITUATION_5 IDS_SIEGE_SITUATION + 4
#define IDS_SIEGE_SITUATION_5 3784
//convRes: #define IDS_NoActionInactiveSiege  IDS_SIEGE_SITUATION + 5
#define IDS_NoActionInactiveSiege  3785

// #define IDS_SIEGE_OUTCOME  IDS_SIEGE_SITUATION + 6
//convRes: #define IDS_SIEGE_OUTCOME  IDS_FIRST+2790
#define IDS_SIEGE_OUTCOME  3790
//convRes: #define IDS_SIEGE_OUTCOME_1 IDS_SIEGE_OUTCOME + 0
#define IDS_SIEGE_OUTCOME_1 3790
//convRes: #define IDS_SIEGE_OUTCOME_2 IDS_SIEGE_OUTCOME + 1
#define IDS_SIEGE_OUTCOME_2 3791
//convRes: #define IDS_SIEGE_OUTCOME_3 IDS_SIEGE_OUTCOME + 2
#define IDS_SIEGE_OUTCOME_3 3792
//convRes: #define IDS_SIEGE_OUTCOME_4 IDS_SIEGE_OUTCOME + 3
#define IDS_SIEGE_OUTCOME_4 3793
//convRes: #define IDS_SIEGE_OUTCOME_5 IDS_SIEGE_OUTCOME + 4
#define IDS_SIEGE_OUTCOME_5 3794

// #define IDS_DAY_INITIAL IDS_SIEGE_OUTCOME + 5
//convRes: #define IDS_DAY_INITIAL IDS_FIRST+2800
#define IDS_DAY_INITIAL 3800
//convRes: #define IDS_DAY_INITIAL_1  IDS_DAY_INITIAL + 0
#define IDS_DAY_INITIAL_1  3800
//convRes: #define IDS_DAY_INITIAL_2  IDS_DAY_INITIAL + 1
#define IDS_DAY_INITIAL_2  3801
//convRes: #define IDS_DAY_INITIAL_3  IDS_DAY_INITIAL + 2
#define IDS_DAY_INITIAL_3  3802
//convRes: #define IDS_DAY_INITIAL_4  IDS_DAY_INITIAL + 3
#define IDS_DAY_INITIAL_4  3803
//convRes: #define IDS_DAY_INITIAL_5  IDS_DAY_INITIAL + 4
#define IDS_DAY_INITIAL_5  3804
//convRes: #define IDS_DAY_INITIAL_6  IDS_DAY_INITIAL + 5
#define IDS_DAY_INITIAL_6  3805
//convRes: #define IDS_DAY_INITIAL_7  IDS_DAY_INITIAL + 6
#define IDS_DAY_INITIAL_7  3806

// #define IDS_CB_TERRAIN_TYPE  IDS_DAY_INITIAL + 7
//convRes: #define IDS_CB_TERRAIN_TYPE  IDS_FIRST+2810
#define IDS_CB_TERRAIN_TYPE  3810
//convRes: #define IDS_CB_TERRAIN_TYPE_1 IDS_CB_TERRAIN_TYPE + 0
#define IDS_CB_TERRAIN_TYPE_1 3810
//convRes: #define IDS_CB_TERRAIN_TYPE_2 IDS_CB_TERRAIN_TYPE + 1
#define IDS_CB_TERRAIN_TYPE_2 3811
//convRes: #define IDS_CB_TERRAIN_TYPE_3 IDS_CB_TERRAIN_TYPE + 2
#define IDS_CB_TERRAIN_TYPE_3 3812
//convRes: #define IDS_CB_TERRAIN_TYPE_4 IDS_CB_TERRAIN_TYPE + 3
#define IDS_CB_TERRAIN_TYPE_4 3813
//convRes: #define IDS_CB_TERRAIN_TYPE_5 IDS_CB_TERRAIN_TYPE + 4
#define IDS_CB_TERRAIN_TYPE_5 3814
//convRes: #define IDS_CB_TERRAIN_TYPE_6 IDS_CB_TERRAIN_TYPE + 5
#define IDS_CB_TERRAIN_TYPE_6 3815
//convRes: #define IDS_CB_TERRAIN_TYPE_7 IDS_CB_TERRAIN_TYPE + 6
#define IDS_CB_TERRAIN_TYPE_7 3816
//convRes: #define IDS_CB_TERRAIN_TYPE_8 IDS_CB_TERRAIN_TYPE + 7
#define IDS_CB_TERRAIN_TYPE_8 3817
//convRes: #define IDS_CB_TERRAIN_TYPE_9 IDS_CB_TERRAIN_TYPE + 8
#define IDS_CB_TERRAIN_TYPE_9 3818
//convRes: #define IDS_CB_TERRAIN_TYPE_10 IDS_CB_TERRAIN_TYPE + 9
#define IDS_CB_TERRAIN_TYPE_10 3819
//convRes: #define IDS_CB_TERRAIN_TYPE_11 IDS_CB_TERRAIN_TYPE + 10
#define IDS_CB_TERRAIN_TYPE_11 3820
//convRes: #define IDS_CB_TERRAIN_TYPE_12 IDS_CB_TERRAIN_TYPE + 11
#define IDS_CB_TERRAIN_TYPE_12 3821
//convRes: #define IDS_CB_TERRAIN_TYPE_13 IDS_CB_TERRAIN_TYPE + 12
#define IDS_CB_TERRAIN_TYPE_13 3822

// #define IDS_CB_GROUND_TYPE         IDS_CB_TERRAIN_TYPE + 13
//convRes: #define IDS_CB_GROUND_TYPE         IDS_FIRST+2830
#define IDS_CB_GROUND_TYPE         3830
//convRes: #define IDS_CB_GROUND_TYPE_1       IDS_CB_GROUND_TYPE + 0
#define IDS_CB_GROUND_TYPE_1       3830
//convRes: #define IDS_CB_GROUND_TYPE_2       IDS_CB_GROUND_TYPE + 1
#define IDS_CB_GROUND_TYPE_2       3831
//convRes: #define IDS_CB_GROUND_TYPE_3       IDS_CB_GROUND_TYPE + 2
#define IDS_CB_GROUND_TYPE_3       3832
//convRes: #define IDS_CB_GROUND_TYPE_4       IDS_CB_GROUND_TYPE + 3
#define IDS_CB_GROUND_TYPE_4       3833
//convRes: #define IDS_CB_GROUND_TYPE_5       IDS_CB_GROUND_TYPE + 4
#define IDS_CB_GROUND_TYPE_5       3834
//convRes: #define IDS_CB_GROUND_TYPE_6       IDS_CB_GROUND_TYPE + 5
#define IDS_CB_GROUND_TYPE_6       3835
//convRes: #define IDS_CB_GROUND_TYPE_7       IDS_CB_GROUND_TYPE + 6
#define IDS_CB_GROUND_TYPE_7       3836
//convRes: #define IDS_CB_GROUND_TYPE_8       IDS_CB_GROUND_TYPE + 7
#define IDS_CB_GROUND_TYPE_8       3837
//convRes: #define IDS_CB_GROUND_TYPE_9       IDS_CB_GROUND_TYPE + 8
#define IDS_CB_GROUND_TYPE_9       3838
//convRes: #define IDS_CB_GROUND_TYPE_10      IDS_CB_GROUND_TYPE + 9
#define IDS_CB_GROUND_TYPE_10      3839
//convRes: #define IDS_CB_GROUND_TYPE_11      IDS_CB_GROUND_TYPE + 10
#define IDS_CB_GROUND_TYPE_11      3840
//convRes: #define IDS_CB_GROUND_TYPE_12      IDS_CB_GROUND_TYPE + 11
#define IDS_CB_GROUND_TYPE_12      3841
//convRes: #define IDS_CB_GROUND_TYPE_13      IDS_CB_GROUND_TYPE + 12
#define IDS_CB_GROUND_TYPE_13      3842

// #define IDS_RankEnum               IDS_CB_GROUND_TYPE + 13
//convRes: #define IDS_RankEnum                IDS_FIRST+2850
#define IDS_RankEnum                3850
//convRes: #define IDS_RANK_1                  IDS_RankEnum + 0
#define IDS_RANK_1                  3850
//convRes: #define IDS_RANK_2                  IDS_RankEnum + 1
#define IDS_RANK_2                  3851
//convRes: #define IDS_RANK_3                  IDS_RankEnum + 2
#define IDS_RANK_3                  3852
//convRes: #define IDS_RANK_4                  IDS_RankEnum + 3
#define IDS_RANK_4                  3853
//convRes: #define IDS_RANK_5                  IDS_RankEnum + 4
#define IDS_RANK_5                  3854
//convRes: #define IDS_RANK_6                  IDS_RankEnum + 5
#define IDS_RANK_6                  3855

// #define IDS_MonthNameAbrevs         IDS_RankEnum + 6
//convRes: #define IDS_MonthNameAbrevs         IDS_FIRST+2860
#define IDS_MonthNameAbrevs         3860
//convRes: #define IDS_MONTH_ABREV_1           IDS_MonthNameAbrevs + 0
#define IDS_MONTH_ABREV_1           3860
//convRes: #define IDS_MONTH_ABREV_2           IDS_MonthNameAbrevs + 1
#define IDS_MONTH_ABREV_2           3861
//convRes: #define IDS_MONTH_ABREV_3           IDS_MonthNameAbrevs + 2
#define IDS_MONTH_ABREV_3           3862
//convRes: #define IDS_MONTH_ABREV_4           IDS_MonthNameAbrevs + 3
#define IDS_MONTH_ABREV_4           3863
//convRes: #define IDS_MONTH_ABREV_5           IDS_MonthNameAbrevs + 4
#define IDS_MONTH_ABREV_5           3864
//convRes: #define IDS_MONTH_ABREV_6           IDS_MonthNameAbrevs + 5
#define IDS_MONTH_ABREV_6           3865
//convRes: #define IDS_MONTH_ABREV_7           IDS_MonthNameAbrevs + 6
#define IDS_MONTH_ABREV_7           3866
//convRes: #define IDS_MONTH_ABREV_8           IDS_MonthNameAbrevs + 7
#define IDS_MONTH_ABREV_8           3867
//convRes: #define IDS_MONTH_ABREV_9           IDS_MonthNameAbrevs + 8
#define IDS_MONTH_ABREV_9           3868
//convRes: #define IDS_MONTH_ABREV_10          IDS_MonthNameAbrevs + 9
#define IDS_MONTH_ABREV_10          3869
//convRes: #define IDS_MONTH_ABREV_11          IDS_MonthNameAbrevs + 10
#define IDS_MONTH_ABREV_11          3870
//convRes: #define IDS_MONTH_ABREV_12          IDS_MonthNameAbrevs + 11
#define IDS_MONTH_ABREV_12          3871

// #define IDS_MonthNames              IDS_MonthNameAbrevs + 12
//convRes: #define IDS_MonthNames              IDS_FIRST+2880
#define IDS_MonthNames              3880
//convRes: #define IDS_MONTH_1                 IDS_MonthNames + 0
#define IDS_MONTH_1                 3880
//convRes: #define IDS_MONTH_2                 IDS_MonthNames + 1
#define IDS_MONTH_2                 3881
//convRes: #define IDS_MONTH_3                 IDS_MonthNames + 2
#define IDS_MONTH_3                 3882
//convRes: #define IDS_MONTH_4                 IDS_MonthNames + 3
#define IDS_MONTH_4                 3883
//convRes: #define IDS_MONTH_5                 IDS_MonthNames + 4
#define IDS_MONTH_5                 3884
//convRes: #define IDS_MONTH_6                 IDS_MonthNames + 5
#define IDS_MONTH_6                 3885
//convRes: #define IDS_MONTH_7                 IDS_MonthNames + 6
#define IDS_MONTH_7                 3886
//convRes: #define IDS_MONTH_8                 IDS_MonthNames + 7
#define IDS_MONTH_8                 3887
//convRes: #define IDS_MONTH_9                 IDS_MonthNames + 8
#define IDS_MONTH_9                 3888
//convRes: #define IDS_MONTH_10                IDS_MonthNames + 9
#define IDS_MONTH_10                3889
//convRes: #define IDS_MONTH_11                IDS_MonthNames + 10
#define IDS_MONTH_11                3890
//convRes: #define IDS_MONTH_12                IDS_MonthNames + 11
#define IDS_MONTH_12                3891

#ifdef __cplusplus
};
#endif


#endif /* RES_STR_H */

/*
 *----------------------------------------------------------------------
 * $Log$
 * Revision 1.2  2001/06/18 20:22:23  greenius
 * runBattle Project added, along with resources.  Compiles and almost runs!
 *
 *----------------------------------------------------------------------
 */
