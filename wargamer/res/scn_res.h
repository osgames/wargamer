/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef _SCN_RES_H
#define _SCN_RES_H

#ifdef __cplusplus
extern "C" {
#endif


//convRes: #define BM_OBICONS                  2
#define BM_OBICONS                  2
//convRes: #define BM_ORDERICONS       3
#define BM_ORDERICONS       3
//convRes: #define BM_SPDIALICONS      4
#define BM_SPDIALICONS      4
//convRes: #define BM_BATTLEICONS      5
#define BM_BATTLEICONS      5
//convRes: #define BM_MSGWINICONS      6
#define BM_MSGWINICONS      6

//convRes: #define BM_BK_SIDE1         7
#define BM_BK_SIDE1         7
//convRes: #define BM_BK_SIDE2         8
#define BM_BK_SIDE2         8
//convRes: #define BM_BK_NEUTRAL            9
#define BM_BK_NEUTRAL            9

//convRes: #define BM_MESSAGEWINDOW    10
#define BM_MESSAGEWINDOW    10
//convRes: #define BM_CCLOCKICONS      11
#define BM_CCLOCKICONS      11
//convRes: #define BM_TOWNMAP                       12
#define BM_TOWNMAP                       12

/*
 *   Images for Unit Order Window
 */

//convRes: #define BM_CAMPICONS        13
#define BM_CAMPICONS        13
//convRes: #define BM_AGRESSICONS      14
#define BM_AGRESSICONS      14

//convRes: #define BM_BUILD_TYPE       15
#define BM_BUILD_TYPE       15
//convRes: #define BM_MESSAGEICONS     16
#define BM_MESSAGEICONS     16
//convRes: #define BM_TOWNINFOICONS    17
#define BM_TOWNINFOICONS    17
//convRes: #define BM_ZOOMICONS        18
#define BM_ZOOMICONS        18
//convRes: #define BM_ALLCITIES        19
#define BM_ALLCITIES        19
//convRes: #define BM_BUILDBUTTONS     20
#define BM_BUILDBUTTONS     20

//convRes: #define BM_STARTICONS       21
#define BM_STARTICONS       21

//convRes: #define BM_MSGWINSTATIC     22
#define BM_MSGWINSTATIC     22
//convRes: #define BM_MSGWINSELECT     23
#define BM_MSGWINSELECT     23
//convRes: #define BM_MSGWINHASHED     24
#define BM_MSGWINHASHED     24

//convRes: #define BM_AOICONS          25
#define BM_AOICONS          25

/*
 * Bitmaps for scroll bars
 */

//convRes: #define BM_MAPSCROLLBTNS    36
#define BM_MAPSCROLLBTNS    36
//convRes: #define BM_HMAPSCROLLBTNS   37
#define BM_HMAPSCROLLBTNS   37
//convRes: #define BM_INFOSCROLLBTNS   38
#define BM_INFOSCROLLBTNS   38
//convRes: #define BM_VTHUMBBK         39
#define BM_VTHUMBBK         39
//convRes: #define BM_HTHUMBBK         40
#define BM_HTHUMBBK         40
//convRes: #define BM_VSCROLLBK        41
#define BM_VSCROLLBK        41
//convRes: #define BM_HSCROLLBK        42
#define BM_HSCROLLBK        42

//convRes: #define BM_TBSCROLLBTNS     43
#define BM_TBSCROLLBTNS     43
//convRes: #define BM_MOREINFOBTNS     44
#define BM_MOREINFOBTNS     44

//convRes: #define BM_WEATHERICONS     45
#define BM_WEATHERICONS     45
//convRes: #define BM_WEATHERBUTTONS   46
#define BM_WEATHERBUTTONS   46

//convRes: #define BM_BARCHARTBK       47
#define BM_BARCHARTBK       47
//convRes: #define BM_BARCHARTFILLER   48
#define BM_BARCHARTFILLER   48

//convRes: #define BM_DOTMODEIMAGES    49
#define BM_DOTMODEIMAGES    49

//convRes: #define BM_TOOLBOXBUTTONS   50
#define BM_TOOLBOXBUTTONS   50
//convRes: #define BM_COMBOBUTTON      51
#define BM_COMBOBUTTON      51

//convRes: #define BM_TABBACKGROUND    52
#define BM_TABBACKGROUND    52
//convRes: #define BM_CHECKBUTTONS     53
#define BM_CHECKBUTTONS     53
//convRes: #define BM_AGGRESSIMAGES    54
#define BM_AGGRESSIMAGES    54
//convRes: #define BM_SPECIALISTIMAGES 55
#define BM_SPECIALISTIMAGES 55

//convRes: #define BM_BUILDPAGEICONS   56
#define BM_BUILDPAGEICONS   56
//convRes: #define BM_SMALLTOWNICONS   57
#define BM_SMALLTOWNICONS   57

//convRes: #define BM_TABIMAGES        58
#define BM_TABIMAGES        58

//convRes: #define BM_CBATTLEANIMATIONS 59
#define BM_CBATTLEANIMATIONS 59

//convRes: #define BM_CHERRYWOOD        60
#define BM_CHERRYWOOD        60
//convRes: #define BM_MENUIMAGES        61
#define BM_MENUIMAGES        61
//convRes: #define BM_TOOLBARIMAGES           62
#define BM_TOOLBARIMAGES           62
//convRes: #define BM_BTOOLBARIMAGES           63
#define BM_BTOOLBARIMAGES           63

//convRes: #define BM_MENUBARBK 64
#define BM_MENUBARBK 64
//convRes: #define BM_BFACEIMAGES 65
#define BM_BFACEIMAGES 65

//convRes: #define BM_BATTLE_ZOOMICONS     66
#define BM_BATTLE_ZOOMICONS     66

/*
 *  Cursors
 */

//convRes: #define CURS_GLOVE_MONO       3
#define CURS_GLOVE_MONO       3
//convRes: #define CURS_GLOVE_COLOR      4
#define CURS_GLOVE_COLOR      4

//convRes: #define OBI_Side                                        0
#define OBI_Side                                        0
//convRes: #define OBI_SideSelected                0
#define OBI_SideSelected                0
//convRes: #define OBI_Army                                        1
#define OBI_Army                                        1
//convRes: #define OBI_ArmySelected                1
#define OBI_ArmySelected                1
//convRes: #define OBI_Corps                                       2
#define OBI_Corps                                       2
//convRes: #define OBI_CorpsSelected               2
#define OBI_CorpsSelected               2
//convRes: #define OBI_Division                            3
#define OBI_Division                            3
//convRes: #define OBI_DivisionSelected    3
#define OBI_DivisionSelected    3
//convRes: #define OBI_Brigade                             4
#define OBI_Brigade                             4
//convRes: #define OBI_BrigadeSelected     4
#define OBI_BrigadeSelected     4
//convRes: #define OBI_IconsPerSide                5
#define OBI_IconsPerSide                5

//convRes: #define OBI_CX                                  22
#define OBI_CX                                  22

/*
 *  Flag image related defines
 */

//convRes: #define FI_ArmyGroup    0
#define FI_ArmyGroup    0
//convRes: #define FI_Army         1
#define FI_Army         1
//convRes: #define FI_Corps        2
#define FI_Corps        2
//convRes: #define FI_Division     3
#define FI_Division     3
//convRes: #define FI_Garrison     4
#define FI_Garrison     4
//convRes: #define FI_Pennant      5
#define FI_Pennant      5
//convRes: #define FI_Background   6
#define FI_Background   6

//convRes: #define FI_ArmyGroup_CX   21
#define FI_ArmyGroup_CX   21
//convRes: #define FI_ArmyGroup_CY   18
#define FI_ArmyGroup_CY   18

//convRes: #define FI_Army_CX        18
#define FI_Army_CX        18
//convRes: #define FI_Army_CY        16
#define FI_Army_CY        16

//convRes: #define FI_Corps_CX       15
#define FI_Corps_CX       15
//convRes: #define FI_Corps_CY       14
#define FI_Corps_CY       14

//convRes: #define FI_Division_CX    12
#define FI_Division_CX    12
//convRes: #define FI_Division_CY    12
#define FI_Division_CY    12

//convRes: #define FI_Garrison_CX    17
#define FI_Garrison_CX    17
//convRes: #define FI_Garrison_CY    14
#define FI_Garrison_CY    14

//convRes: #define FI_Pennant_CX     19
#define FI_Pennant_CX     19
//convRes: #define FI_Pennant_CY     17
#define FI_Pennant_CY     17

//convRes: #define FI_Background_CX 124
#define FI_Background_CX 124
//convRes: #define FI_Background_CY  47
#define FI_Background_CY  47


/*
 * Battle Images
 */

//convRes: #define BATTLEICON_WIDTH 32
#define BATTLEICON_WIDTH 32
//convRes: #define BATTLEICON_HEIGHT 32
#define BATTLEICON_HEIGHT 32

//convRes: #define BATTLEICON_NORMAL       0
#define BATTLEICON_NORMAL       0
//convRes: #define BATTLEICON_HILIGHT      1
#define BATTLEICON_HILIGHT      1

#define BATTLEICON_MASK RGB(255, 0, 255)        /* Purple */


/*
 * TownInfo Images
 */

//convRes: #define IWI_CAPITAL             0
#define IWI_CAPITAL             0
//convRes: #define IWI_CITY                1
#define IWI_CITY                1
//convRes: #define IWI_TOWN                2
#define IWI_TOWN                2
//convRes: #define IWI_CAMPSITE            3
#define IWI_CAMPSITE            3

// implemented as follows:  town.getFortLevel() + IWI_OFFSET + (town.getSize() * IWI_OFFSET+1)

//convRes: #define IWI_FORTOFFSET      3
#define IWI_FORTOFFSET      3
//convRes: #define NumTownInfoImages  20
#define NumTownInfoImages  20
#define TownInfoImages     MAKEINTRESOURCE(BM_TOWNINFOICONS)

/*
 *  Defines for ZoomIcons
 */
//convRes: #define NumZoomImages      15
#define NumZoomImages      15
#define ZoomImages         MAKEINTRESOURCE(BM_ZOOMICONS)

/*
 * Defines for ToolBox buttons
 */

//convRes: #define NumToolBoxButtonImages  11
#define NumToolBoxButtonImages  11
#define ToolBoxButtonImages     MAKEINTRESOURCE(BM_TOOLBOXBUTTONS)

//convRes: #define NumBattleZoomImages     15
#define NumBattleZoomImages     15

/*
 * Defines for ToolBox buttons
 */

//convRes: #define NumCheckButtonImages    2
#define NumCheckButtonImages    2
#define CheckButtonImages       MAKEINTRESOURCE(BM_CHECKBUTTONS)

/*
 * Aggression Images
 */

//convRes: #define NumAggressImages        8
#define NumAggressImages        8
#define AggressIconImages       MAKEINTRESOURCE(BM_AGGRESSIMAGES)

/*
 * Specialist Images
 */

//convRes: #define NumSpecialistImages     4
#define NumSpecialistImages     4
#define SpecialistImages        MAKEINTRESOURCE(BM_SPECIALISTIMAGES)

/*
 * Build Page Icons
 */

//convRes: #define NumBuildPageImages      21
#define NumBuildPageImages      21
#define BuildPageImages         MAKEINTRESOURCE(BM_BUILDPAGEICONS)

/*
 * Small Town Icons
 */

//convRes: #define NumSmallTownImages      4
#define NumSmallTownImages      4
#define SmallTownImages         MAKEINTRESOURCE(BM_SMALLTOWNICONS)

/*
 * tab images
 */

//convRes: #define NumTabImages            2
#define NumTabImages            2
#define CustomTabImages         MAKEINTRESOURCE(BM_TABIMAGES)

/*
 * Battle Animations
 */

// #define NumCampBattleFrames     8
//convRes: #define NumCampBattleFrames     9
#define NumCampBattleFrames     9
#define CampaignBattleAnimation MAKEINTRESOURCE(BM_CBATTLEANIMATIONS)

/*
 * Menu Imagmes
 */

//convRes: #define NumMenuImages          2
#define NumMenuImages          2
#define MenuImages             MAKEINTRESOURCE(BM_MENUIMAGES)

// battle facing images

//convRes: #define NumBFaceImages          6
#define NumBFaceImages          6
#define BFaceImages             MAKEINTRESOURCE(BM_BFACEIMAGES)

/*
 * Menu Imagmes
 */

#define ToolBarImages          MAKEINTRESOURCE(BM_TOOLBARIMAGES)

#ifdef __cplusplus
enum ToolBarImageEnum
{
        TBI_Open,
        TBI_Save,
        TBI_ZoomIn,
        TBI_ZoomOut,
        TBI_TinyMap,
        TBI_Weather,
        TBI_Movement,
        TBI_Resource,
        TBI_Unit,
        TBI_OB,
        TBI_IUnit,
        TBI_ILeader,
        TBI_FindTown,
        TBI_FindLeader,
        TBI_OHR,
        TBI_Help,
        TBI_MessageWindow,
        TBI_Supply,
        TBI_ChokePoints,
        TBI_SupplyLines,

        TBI_HowMany
};

//convRes: #define NumToolBarImages       TBI_HowMany
#define NumToolBarImages       

#else

//convRes: #define NumToolBarImages       16
#define NumToolBarImages       16

#endif

// ----------- Unchecked update
#define BToolBarImages          MAKEINTRESOURCE(BM_BTOOLBARIMAGES)

#ifdef __cplusplus
enum BToolBarImageEnum
{
        BTBI_Open,
        BTBI_Save,
        BTBI_ZoomIn,
        BTBI_ZoomOverview,
        BTBI_ZoomDetail,
        BTBI_Zoom2Mile,
        BTBI_Zoom4Mile,
        BTBI_HexOutLine,
        BTBI_TinyMap,
        BTBI_MessageWindow,
        BTBI_OB,
        BTBI_FindLeader,
        BTBI_OHR,
        BTBI_Help,
        BTBI_TrackingWindow,

        BTBI_HowMany
};

//convRes: #define NumBToolBarImages       BTBI_HowMany
#define NumBToolBarImages       

#else

//convRes: #define NumBToolBarImages       15
#define NumBToolBarImages       15

#endif
//--------- end unchecked update

/*
 *  define for Image Position resource (user-defined)
 */

//convRes: #define IMAGEPOSITION       300      // resource type id.  must be greater than 255
#define IMAGEPOSITION       300 // resource type id.  must be greater than 255
#define RT_IMAGEPOS         MAKEINTRESOURCE(IMAGEPOSITION)



/*
 *   Position resource's
 */

//convRes: #define PR_FIRST            100
#define PR_FIRST            100
//convRes: #define PR_CAMPICONS         PR_FIRST
#define PR_CAMPICONS         100
//convRes: #define PR_AGGRESSICONS      PR_FIRST+1
#define PR_AGGRESSICONS      101
//convRes: #define PR_ORDERICONS        PR_FIRST+2
#define PR_ORDERICONS        102
//convRes: #define PR_UNITICONS         PR_FIRST+3
#define PR_UNITICONS         103
//convRes: #define PR_CCLOCKICONS       PR_FIRST+4
#define PR_CCLOCKICONS       104
//convRes: #define PR_MESSAGEBUTTONS    PR_FIRST+5
#define PR_MESSAGEBUTTONS    105
//convRes: #define PR_TOWNINFOICONS     PR_FIRST+6
#define PR_TOWNINFOICONS     106
//convRes: #define PR_ZOOMICONS         PR_FIRST+7
#define PR_ZOOMICONS         107
//convRes: #define PR_TOWNICONS         PR_FIRST+8
#define PR_TOWNICONS         108
//convRes: #define PR_BUILDBUTTONS      PR_FIRST+9
#define PR_BUILDBUTTONS      109
//convRes: #define PR_STARTICONS        PR_FIRST+10
#define PR_STARTICONS        110
//convRes: #define PR_MSGWINICONS       PR_FIRST+11
#define PR_MSGWINICONS       111
//convRes: #define PR_AOICONS           PR_FIRST+12
#define PR_AOICONS           112
//convRes: #define PR_MAPSCROLLBTNS     PR_FIRST+13
#define PR_MAPSCROLLBTNS     113
//convRes: #define PR_INFOSCROLLBTNS    PR_FIRST+14
#define PR_INFOSCROLLBTNS    114
//convRes: #define PR_MOREINFOBTNS      PR_FIRST+15
#define PR_MOREINFOBTNS      115
//convRes: #define PR_TBSCROLLBTNS      PR_FIRST+16
#define PR_TBSCROLLBTNS      116
//convRes: #define PR_WEATHERICONS      PR_FIRST+17
#define PR_WEATHERICONS      117
//convRes: #define PR_WEATHERBUTTONS    PR_FIRST+18
#define PR_WEATHERBUTTONS    118
//convRes: #define PR_DOTMODEIMAGES     PR_FIRST+19
#define PR_DOTMODEIMAGES     119
//convRes: #define PR_TOOLBOXBUTTONS    PR_FIRST+20
#define PR_TOOLBOXBUTTONS    120
//convRes: #define PR_CHECKBUTTONS      PR_FIRST+21
#define PR_CHECKBUTTONS      121
//convRes: #define PR_AGGRESSIMAGES     PR_FIRST+22
#define PR_AGGRESSIMAGES     122
//convRes: #define PR_SPECIALISTIMAGES  PR_FIRST+23
#define PR_SPECIALISTIMAGES  123
//convRes: #define PR_BUILDPAGEICONS    PR_FIRST+24
#define PR_BUILDPAGEICONS    124
//convRes: #define PR_SMALLTOWNICONS    PR_FIRST+25
#define PR_SMALLTOWNICONS    125
//convRes: #define PR_TABIMAGES         PR_FIRST+26
#define PR_TABIMAGES         126
//convRes: #define PR_CBATTLEANIMATIONS PR_FIRST+27
#define PR_CBATTLEANIMATIONS 127
//convRes: #define PR_MENUICONS         PR_FIRST+28
#define PR_MENUICONS         128
//convRes: #define PR_TOOLBARICONS      PR_FIRST+29
#define PR_TOOLBARICONS      129
//convRes: #define PR_COMBOBUTTON       PR_FIRST+30
#define PR_COMBOBUTTON       130
//convRes: #define PR_BTOOLBARICONS     PR_FIRST+31
#define PR_BTOOLBARICONS     131
//convRes: #define PR_BFACEIMAGES       PR_FIRST+32
#define PR_BFACEIMAGES       132
//convRes: #define PR_LAST              PR_FIRST+33
#define PR_LAST              133
//convRes: #define PR_BATTLE_ZOOMICONS  PR_FIRST+34
#define PR_BATTLE_ZOOMICONS  134

/*
 * Icons in DOTModeImages
 */

enum DotModeImages {
    DotMode_Fort1,
    DotMode_Fort2,
    DotMode_Fort3,
    DotMode_Fort4,
    DotMode_FrenchSiege0,
    DotMode_FrenchSiege1,
    DotMode_FrenchSiege2,
    DotMode_FrenchSiege3,
    DotMode_FrenchSiege4,
    DotMode_AlliedSiege0,
    DotMode_AlliedSiege1,
    DotMode_AlliedSiege2,
    DotMode_AlliedSiege3,
    DotMode_AlliedSiege4,
    DotMode_FrenchChoke,
    DotMode_AlliedChoke,
    DotMode_NeutralChoke,
    DotMode_BrokenFrenchChoke,
    DotMode_BrokenAlliedChoke,
    DotMode_BrokenNeutralChoke,
    DotMode_FrenchMountain,
    DotMode_AlliedMountain,
    DotMode_NeutralMountain,
    DotMode_Engineering,
    DotMode_SupplyDepot,
    DotMode_SupplySource,
    DotMode_Battle,

    DotMode_HowMany
};

/*
 * Icons in BuildPageIcons7
 */

struct BuildPageImageEnum
{
    enum Value
    {
        InfantryImage,
        CavalryImage,
        ArtilleryImage,
        OtherImage,

        UnpressedPool,
        PressedPool,

        UnpressedPlus,
        PressedPlus,
        UnpressedMinus,
        PressedMinus,

        ManpowerImage,
        ResourceImage,

        UnpressedAuto,
        PressedAuto,

        // ClockImage,

        // CapitalImage,
        BridgeImage,
        BarrelImage,
        CornImage,
        UnderConstructionImage,
        AllowedInGarrisonImage,

        ImageCount,
    };
};


#ifdef __cplusplus
};
#endif

#endif

