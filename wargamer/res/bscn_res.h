/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#error "Obsolete"

#ifndef BATRES_H
#define BATRES_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Definitions for Battle Game Resources that go into scenario DLL
 *
 *----------------------------------------------------------------------
 */

#endif /* BATRES_H */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Special Data types
 */

#define IMAGEPOSITION       300      // resource type id.  must be greater than 255
#define RT_IMAGEPOS         MAKEINTRESOURCE(IMAGEPOSITION)

/*
 * Icons
 */


/*
 * Menus
 */


/*
 * Dialogs
 */

/*
 * Bitmaps and Image Positions
 */

#define BM_BATTLE_ZOOMICONS	100
#define PR_BATTLE_ZOOMICONS	100
#define NumBattleZoomImages	15

#define BM_BR_COMBOBUTTON        101
#define PR_BR_COMBOBUTTON	      101
#define NumComboButtonImages	1

#define BM_BR_CHECKBUTTONS     102
#define PR_BR_CHECKBUTTONS	      102
#define NumCheckButtonImages	2

/*
 * Menu IDs
 */


#ifdef __cplusplus
};
#endif

