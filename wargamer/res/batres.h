/*----------------------------------------------------------------------------*
 * Wargamer: Copyright (c) 1995-2001, Steven Green (wargamer@greenius.co.uk)  *
 * This Software is subject to the GNU General Public License.  			  *
 * For License information see the file COPYING in the project root directory *
 * For more information see the file README.								  *
 *----------------------------------------------------------------------------*/
#ifndef BATRES_H
#define BATRES_H
/*
 *----------------------------------------------------------------------
 * $Id$
 *----------------------------------------------------------------------
 * Copyright (C) 1996, Steven Morle-Green, All Rights Reserved
 * Parts of this code may have been written/modified by Paul Sample
 *----------------------------------------------------------------------
 *
 *	Definitions for Battle Game Resources
 *
 *----------------------------------------------------------------------
 */

#endif /* BATRES_H */

#ifdef __cplusplus
extern "C" {
#endif

#define BATTLE

#include "resource.h"

#ifdef __cplusplus
};
#endif

