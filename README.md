# Wargamer

Originally developed at https://sourceforge.net/projects/wargamer/ by greenius and quartermaster and published under GPL 2.0.

Conversion of the CVS repository at a.cvs.sourceforge.net::cvsroot/wargamer/ (https://sourceforge.net/projects/wargamer/) to Git with cvs2git (http://cvs2svn.tigris.org/cvs2git.html).

Removed the acw folder from the repository history, which is unrelated to Wargamer. The cutout history has been added to https://gitlab.com/osgames/civilwar (Greenius' Civil War).